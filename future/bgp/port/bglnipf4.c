/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bglnipf4.c,v 1.27 2016/12/27 12:35:54 siva Exp $
 *
 * Description: These routines interface with the underlying linux SLI 
 *              layer.
 *
 *******************************************************************/
#ifndef BGLNIPF4_C
#define BGLNIPF4_C

#include "bgp4com.h"

/*****************************************************************************/
/* Function Name : Bgp4Ipv4GetDefaultRouteFromFDB                            */
/* Description   : This routine will get the default route if present in IPv4*/
/*                 FDB and if present then add the default route to BGP IPv4 */
/*                 RIB.                                                      */
/* Input(s)      : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4Ipv4GetDefaultRouteFromFDB (UINT4 u4ContextId)
{
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
    tNetAddress         NetAddr;
    tAddrPrefix         AddrPrefix;
    INT4                i4RetStatus = BGP4_SUCCESS;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
    Bgp4InitNetAddressStruct (&NetAddr, BGP4_INET_AFI_IPV4,
                              BGP4_INET_SAFI_UNICAST);
    Bgp4InitAddrPrefixStruct (&AddrPrefix, BGP4_INET_AFI_IPV4);

    RtQuery.u4DestinationIpAddress = 0;
    RtQuery.u4DestinationSubnetMask = 0;

    if (BGP4_RRD_PROTO_MASK (BGP4_DFLT_VRFID) == 0)
    {
        /* Redistribution is not enabled. No route will be learnt from FDB. */
        return BGP4_FAILURE;
    }

    if ((BGP4_RRD_PROTO_MASK (BGP4_DFLT_VRFID) & BGP4_IMPORT_DIRECT) ==
        BGP4_IMPORT_DIRECT)
    {
        RtQuery.u2AppIds |= RTM_DIRECT_MASK;
    }
    if ((BGP4_RRD_PROTO_MASK (BGP4_DFLT_VRFID) & BGP4_IMPORT_STATIC) ==
        BGP4_IMPORT_STATIC)
    {
        RtQuery.u2AppIds |= RTM_STATIC_MASK;
    }
    if ((BGP4_RRD_PROTO_MASK (BGP4_DFLT_VRFID) & BGP4_IMPORT_RIP) ==
        BGP4_IMPORT_RIP)
    {
        RtQuery.u2AppIds |= RTM_RIP_MASK;
    }
    if ((BGP4_RRD_PROTO_MASK (BGP4_DFLT_VRFID) & BGP4_IMPORT_OSPF) ==
        BGP4_IMPORT_OSPF)
    {
        RtQuery.u2AppIds |= RTM_OSPF_MASK;
    }

    RtQuery.u1QueryFlag = QUERY_FOR_DESTINATION;

    RtQuery.u4ContextId = u4ContextId;
    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_FAILURE)
    {
        /* Default route is not present in the FDB. */
        return BGP4_FAILURE;
    }

    /* Default route is present in FDB. Add it to BGP. */
    PTR_ASSIGN_4 (&(AddrPrefix.au1Address), NetIpRtInfo.u4NextHop);
    i4RetStatus = Bgp4RouteLeak (u4ContextId, NetAddr,
                                 NetIpRtInfo.u2RtProto, AddrPrefix,
                                 BGP4_IMPORT_RT_ADD, 0, NetIpRtInfo.i4Metric1,
                                 NetIpRtInfo.u4RtIfIndx, 0,NetIpRtInfo.u1CommunityCnt,
                                 NetIpRtInfo.pCommunity->au4Community, NetIpRtInfo.u4SetFlag);
    if (i4RetStatus == BGP4_FAILURE)
    {
        /* Adding default route to BGP fails. */
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : BGP4CanRepIpv4RtAddedToFIB                                */
/* Description   : This routine checks whether the new replacmentment route  */
/*                 can be directly added to IPv4 FIB, without deleting the   */
/*                 existing old route from FIB.                              */
/* Input(s)      : New Route profile to be added - pNewRtProfile             */
/*                 Old Route profile existing in FIB - pOldRtProfile         */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE - If the new route can be successfully added.   */
/*                 BGP4_FALSE - If the old route needs to be deleted before  */
/*                              adding the new route.                        */
/*****************************************************************************/
INT4
BGP4CanRepIpv4RtAddedToFIB (tRouteProfile * pNewRtProfile,
                            tRouteProfile * pOldRtProfile)
{
    /* For Linux OS currently, we are not able to replace the old route with
     * the new route because RT_MODIFY function is unable to do it. So 
     * return BGP4_FALSE. If RT_MODIFY function is updated, then update
     * the route accordingly. */
    UNUSED_PARAM (pNewRtProfile);
    UNUSED_PARAM (pOldRtProfile);
    return BGP4_FALSE;
}

/*****************************************************************************/
/* Function Name :  BGP4RTAddRouteToCommIp4RtTbl                             */
/* Description   :  This routine is used to add the route entry to the common*/
/*                  routing table (TRIE) for IP Forwarding.                  */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/* Input(s)      : xxx : Route entry that has to be added.                   */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/* Output(s)     :                                                           */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
INT4
BGP4RTAddRouteToCommIp4RtTbl (tRouteProfile * pRtProfile, UINT4 u4VrfId)
{
    tNetIpv4RtInfo      RtInfo;
    tBgp4Info          *pBgpInfo = BGP4_RT_BGP_INFO (pRtProfile);
    tBgp4PeerEntry     *pTmpPeer = NULL;
    UINT4               u4Addr;
    INT4                i4RetVal;

    /* Non-bgp routes are learnt from RTM. Should not be added back to RTM */
    if (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID)
    {                            /* Route is a non-bgp route learnt from RTM */
        return (BGP4_SUCCESS);
    }

    MEMSET (&RtInfo, 0, sizeof RtInfo);
    PTR_FETCH_4 (RtInfo.u4NextHop, BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile));
    PTR_FETCH_4 (RtInfo.u4DestNet, BGP4_RT_IP_PREFIX (pRtProfile));
    RtInfo.u4DestMask = (Bgp4GetSubnetmask (BGP4_RT_PREFIXLEN (pRtProfile)));
    RtInfo.i4Metric1 = (BGP4_RT_MED (pRtProfile));
    RtInfo.u2RtType = 4;        /* unicast route */
    RtInfo.u4ContextId = BGP4_RT_CXT_ID (pRtProfile);
    if (u4VrfId != 0)
    {
        RtInfo.u4ContextId = u4VrfId;
    }

    RtInfo.u2RtProto = BGP4_RT_PROTOCOL (pRtProfile);
    RtInfo.u4RtIfIndx = pRtProfile->u4RtIfIndx;
    RtInfo.u4RowStatus = (UINT4) IPFWD_ACTIVE;

    PTR_FETCH_4 (u4Addr,
                 BGP4_PEER_REMOTE_ADDR (BGP4_RT_PEER_ENTRY (pRtProfile)));
    RtInfo.u1Preference =
        BgpIp4FilterRouteSource (BGP4_RT_CXT_ID (pRtProfile), pRtProfile);

    /* If route is learnt from a internal peer, it should not be
     * redistributed to IGPs */
    pTmpPeer = BGP4_RT_PEER_ENTRY (pRtProfile);
    if (pTmpPeer != NULL)
    {
        if (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pTmpPeer), pTmpPeer) ==
            BGP4_INTERNAL_PEER)
        {
            if (gBgpCxtNode[BGP4_PEER_CXT_ID (pTmpPeer)]->
                u1IBGPRedistributeStatus == BGP4_IBGP_REDISTRIBUTE_DISABLE)
            {
                RtInfo.u1BitMask |= RTM_FILTER_BASED_ON_AS_MASK;
            }
        }

        /* Construct TAG field */
        RtInfo.u4RouteTag =
            Bgp4SetTaginfo (pBgpInfo, BGP4_GET_PEER_TYPE
                            (BGP4_PEER_CXT_ID (pTmpPeer), pTmpPeer));
    }

    if ((pTmpPeer == NULL) || (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID))
    {
        RtInfo.u4RtNxtHopAs = 0;
    }
    else
    {                            /* Route learnt from BGP peer */
        RtInfo.u4RtNxtHopAs = BGP4_PEER_ASNO (pTmpPeer);
    }
    
    BgpUnLock ();
    i4RetVal = NetIpv4LeakRoute (NETIPV4_ADD_ROUTE, &RtInfo);
    BgpLock ();

    if (i4RetVal < 0)
    {
        BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tUpdating IP FWD Table with  Dest = %s Mask = %s\n\t"
                       "Nexthop = %s FAILED !!!\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));

        return BGP4_FAILURE;
    }

    BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                   BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                   "\tUpdated IP FWD Table with  Dest = %s Mask = %s\n\t"
                   "Nexthop = %s Successfully\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)));
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : BGP4RTModifyRouteInCommIp4RtTbl                           */
/* Description   : This routine is used to modify/replace route entry, added */
/*                 previously to the common routing table (TRIE) for IP      */
/*                 Forwarding.                                               */
/* Input(s)      : Route entry that has to be added - pRtProfile             */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
BGP4RTModifyRouteInCommIp4RtTbl (tRouteProfile * pRtProfile)
{
    tNetIpv4RtInfo      RtInfo;
    tNetIpv4RtInfo      NetRtInfo;
    tBgp4Info          *pBgpInfo = BGP4_RT_BGP_INFO (pRtProfile);
    tBgp4PeerEntry     *pTmpPeer = NULL;
    INT4                i4RetVal;

    /* Non-bgp routes are learnt from RTM. Should not be added back to RTM */
    if (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID)
    {                            /* Route is a non-bgp route learnt from RTM */
        return (BGP4_SUCCESS);
    }

    MEMSET (&RtInfo, 0, sizeof RtInfo);
    MEMSET (&NetRtInfo, 0, sizeof RtInfo);

    PTR_FETCH_4 (RtInfo.u4NextHop, BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile));
    PTR_FETCH_4 (RtInfo.u4DestNet, BGP4_RT_IP_PREFIX (pRtProfile));
    RtInfo.u4DestMask = (Bgp4GetSubnetmask (BGP4_RT_PREFIXLEN (pRtProfile)));
    RtInfo.i4Metric1 = (BGP4_RT_MED (pRtProfile));
    RtInfo.u2RtType = 4;        /* unicast route */
    RtInfo.u2RtProto = BGP4_RT_PROTOCOL (pRtProfile);
    RtInfo.u4ContextId = BGP4_RT_CXT_ID (pRtProfile);
    RtInfo.u4RtIfIndx = pRtProfile->u4RtIfIndx;
    RtInfo.u4RowStatus = (UINT4) IPFWD_ACTIVE;

    /* If route is learnt from a internal peer, it should not be
     * redistributed to IGPs */
    pTmpPeer = BGP4_RT_PEER_ENTRY (pRtProfile);
    if (pTmpPeer != NULL)
    {
        if (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pTmpPeer), pTmpPeer) ==
            BGP4_INTERNAL_PEER)
        {
            if (gBgpCxtNode[BGP4_PEER_CXT_ID (pTmpPeer)]->
                u1IBGPRedistributeStatus == BGP4_IBGP_REDISTRIBUTE_DISABLE)
            {
                RtInfo.u1BitMask |= RTM_FILTER_BASED_ON_AS_MASK;
            }
        }

        /* Construct TAG field */
        RtInfo.u4RouteTag =
            Bgp4SetTaginfo (pBgpInfo,
                            BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pTmpPeer),
                                                pTmpPeer));
    }

    if ((pTmpPeer == NULL) || (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID))
    {
        RtInfo.u4RtNxtHopAs = 0;
    }
    else
    {                            /* Route learnt from BGP peer */
        RtInfo.u4RtNxtHopAs = BGP4_PEER_ASNO (pTmpPeer);
    }

    i4RetVal = NetIpv4LeakRoute (NETIPV4_MODIFY_ROUTE, &RtInfo);
    if (i4RetVal < 0)
    {
        BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tUpdating IP FWD Table with  Dest = %s Mask = %s\n\t"
                       "Nexthop = %s FAILED !!!\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_FAILURE;
    }

    BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                   BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                   "\tUpdated IP FWD Table with  Dest = %s Mask = %s\n\t"
                   "Nexthop = %s Successfully\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)));
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name :  BGP4RTDeleteRouteInCommIp4RtTbl                          */
/* Description   :  This routine is used to delete an entry present in the   */
/*                  TRIE routing table.                                      */
/*                                                                           */
/* Input(s)      : xxx : Route entry that has to be deleted.                 */
/*                                                                           */
/* Output(s)     :                                                           */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
INT4
BGP4RTDeleteRouteInCommIp4RtTbl (tRouteProfile * pRtProfile, UINT4 u4VrfId)
{
    tNetIpv4RtInfo      RtInfo;
    tBgp4Info          *pBgpInfo = BGP4_RT_BGP_INFO (pRtProfile);
    tBgp4PeerEntry     *pTmpPeer = NULL;
    INT4                i4RetVal;

    /* Non-bgp routes are learnt from RTM. Should not be added back to RTM */
    if (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID)
    {                            /* Route is a non-bgp route learnt from RTM */
        return (BGP4_SUCCESS);
    }

    MEMSET (&RtInfo, 0, sizeof RtInfo);

    PTR_FETCH_4 (RtInfo.u4NextHop, BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile));
    PTR_FETCH_4 (RtInfo.u4DestNet, BGP4_RT_IP_PREFIX (pRtProfile));
    RtInfo.u4DestMask = (Bgp4GetSubnetmask (BGP4_RT_PREFIXLEN (pRtProfile)));
    RtInfo.i4Metric1 = (BGP4_RT_MED (pRtProfile));
    RtInfo.u2RtType = 4;        /* unicast route */
    RtInfo.u2RtProto = BGP4_RT_PROTOCOL (pRtProfile);
    RtInfo.u4ContextId = BGP4_RT_CXT_ID (pRtProfile);
    if (u4VrfId != 0)
    {
        RtInfo.u4ContextId = u4VrfId;
    }
    RtInfo.u4RtIfIndx = pRtProfile->u4RtIfIndx;
    RtInfo.u4RowStatus = (UINT4) IPFWD_DESTROY;

    /* If route is learnt from a internal peer, it should not be
     * redistributed to IGPs */
    pTmpPeer = BGP4_RT_PEER_ENTRY (pRtProfile);
    if (pTmpPeer != NULL)
    {
        if (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pTmpPeer), pTmpPeer) ==
            BGP4_INTERNAL_PEER)
        {
            RtInfo.u1BitMask |= RTM_FILTER_BASED_ON_AS_MASK;
        }

        /* Construct TAG field */
        RtInfo.u4RouteTag =
            Bgp4SetTaginfo (pBgpInfo,
                            BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pTmpPeer),
                                                pTmpPeer));
    }

    if ((pTmpPeer == NULL) || (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID))
    {
        RtInfo.u4RtNxtHopAs = 0;
    }
    else
    {                            /* Route learnt from BGP peer */
        RtInfo.u4RtNxtHopAs = BGP4_PEER_ASNO (pTmpPeer);
    }

    BgpUnLock ();
    i4RetVal = NetIpv4LeakRoute (NETIPV4_DELETE_ROUTE, &RtInfo);
    BgpLock ();
    if (i4RetVal < 0)
    {
        BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tDeleting from IP FWD Table - Dest = %s Mask = %s\n\t"
                       "Nexthop = %s FAILED !!!\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_FAILURE;
    }

    BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                   BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                   "\tDeleting from IP FWD Table - Dest = %s Mask = %s\n\t"
                   "Nexthop = %s Successful\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)));
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name :  BgpIp4RtLookup                                           */
/* Description   :  This routine is used to search for a route entry in the  */
/*                  common IP Fwd Table                                      */
/* Input(s)      :  Route entry that has to be searched. (u4Ipaddr)          */
/*                  Protocol Id of the route entry       (i1AppId)           */
/* Output(s)     :  NextHop for the route entry          (pu4Rt)             */
/*                  Interface Index for reaching the nexthop(pRtIfindx)      */
/*                  Metrics associated with that route. (pi4Metric)          */
/* Return(s)     :  BGP4_SUCCESS/BGP4_FAILURE.                               */
/*****************************************************************************/
INT4
BgpIp4RtLookup (UINT4 u4VrfId, UINT4 u4Ipaddr, UINT2 u2PrefixLen, UINT4 *pu4Rt,
                INT4 *pi4Metric, UINT4 *pu4RtIfIndx, INT1 i1AppId)
{
    tNetIpv4RtInfo      NetRtInfo;
    tRtInfoQueryMsg     RtQuery;
    UINT4               u4Mask;
    INT4                i4RetVal;

    UNUSED_PARAM (i1AppId);

    MEMSET (&NetRtInfo, 0, sizeof NetRtInfo);

    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    u4Mask = Bgp4GetSubnetmask (u2PrefixLen);

    RtQuery.u4DestinationIpAddress = u4Ipaddr;
    RtQuery.u4DestinationSubnetMask = u4Mask;
    RtQuery.u2AppIds = ALL_ROUTING_PROTOCOL;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
    RtQuery.u4ContextId = u4VrfId;

    BgpUnLock();
    i4RetVal = NetIpv4GetRoute (&RtQuery, &NetRtInfo);
    BgpLock();
    if (i4RetVal == BGP4_FAILURE)
    {
        *pu4Rt = BGP4_INVALID_ROUTE;
        *pi4Metric = BGP4_INVALID_NH_METRIC;
        *pu4RtIfIndx = BGP4_INVALID_IFINDX;
        return BGP4_FAILURE;
    }
    else
    {
        *pu4Rt = NetRtInfo.u4NextHop;
        *pi4Metric = NetRtInfo.i4Metric1;
        *pu4RtIfIndx = NetRtInfo.u4RtIfIndx;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv4IsOnSameSubnet                                    */
/* Description   : This function tells whether the given two IP addresses    */
/*                 belong to the same subnet.                                */
/* Input(s)      : IP Addresses (u4Ipaddr1, u4Ipaddr2)                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRUE if they belong to the same subnet,                   */
/*                 FALSE if they don't.                                      */
/*****************************************************************************/
BOOL1
Bgp4Ipv4IsOnSameSubnet (UINT4 u4ContextId, UINT4 u4NextHopAddr,
                        UINT4 u4PeerAddr)
{
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT4               u4IfMask = BGP4_INV_IPADDRESS;
    UINT4               u4IfIndex = BGP4_INVALID_IFINDX;
    UINT1               u1SameSubnet = FALSE;

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetFirstIfInfoInCxt (u4ContextId,
                                    &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return FALSE;
    }

    for (;;)
    {
        u4IfIndex = NetIpIfInfo.u4IfIndex;
        u4IfMask = NetIpIfInfo.u4NetMask;

        /* Check whether the received address is directly
         * connected to router. */
        if ((u4NextHopAddr & u4IfMask) == (u4PeerAddr & u4IfMask))
        {
            u1SameSubnet = TRUE;
            break;
        }

        MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
        if (NetIpv4GetNextIfInfoInCxt (u4ContextId,
                                       u4IfIndex,
                                       &NetIpIfInfo) == NETIPV4_FAILURE)
        {
            break;
        }

    }

    return u1SameSubnet;
}

/******************************************************************************/
/* Function Name : BgpGetIpIfAddr                                             */
/* Description   : This routine Gets the local-address of given interface     */
/* Input(s)         : Interface index no.                                     */
/* Output(s)     : None.                                                      */
/* Return(s)     : Interface Address or Zero                                  */
/******************************************************************************/
UINT4
BgpGetIpIfAddr (UINT2 u2IfIndex)
{
    tNetIpv4IfInfo      NetIpIfInfo;

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    if (NetIpv4GetIfInfo (u2IfIndex, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return 0;
    }

    return (NetIpIfInfo.u4Addr);
}

/******************************************************************************/
/* Function Name : BgpGetIpIfMask                                             */
/* Description   : This routine Gets the local-address mask of given interface*/
/* Input(s)      : Interface index no.                                        */
/* Output(s)     : None.                                                      */
/* Return(s)     : Interface Address or Zero                                  */
/******************************************************************************/
UINT4
BgpGetIpIfMask (UINT2 u2IfIndex)
{
    tNetIpv4IfInfo      NetIpIfInfo;

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    if (NetIpv4GetIfInfo (u2IfIndex, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return 0;
    }

    return (NetIpIfInfo.u4NetMask);
}

/******************************************************************************/
/* Function Name : Bgp4GetIpv4LocalAddrForRemoteAddr                          */
/* Description   : This function fills the local interface address            */
/*                 corresponding to input  remote address                     */
/* Input(s)      : RemAddr (Remote address of peer)                           */
/* Output(s)     : Local Address .                                            */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                  */
/******************************************************************************/
INT4
Bgp4GetIpv4LocalAddrForRemoteAddr (UINT4 u4Context, tAddrPrefix RemAddr,
                                   tNetAddress * pLocalAddr)
{
    UINT4               u4Rt = BGP4_INVALID_ROUTE;
    UINT4               u4RtIfIndx = BGP4_INVALID_IFINDX;
    INT4                i4Metric = BGP4_INVALID_NH_METRIC;
    UINT4               u4RemAddr = 0;
    UINT4               u4LocalAddr = 0;
    UINT4               u4LocalMask = 0;

    PTR_FETCH_4 (u4RemAddr, BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (RemAddr));

    BgpIp4RtLookup (u4Context, u4RemAddr, BGP4_MAX_PREFIXLEN,
                    &u4Rt, &i4Metric, &u4RtIfIndx, BGP4_ALL_APPIDS);
    if ((u4Rt != BGP4_INVALID_ROUTE) && (u4RtIfIndx != BGP4_INVALID_IFINDX))
    {
        u4LocalAddr = BgpGetIpIfAddr ((UINT2) (u4RtIfIndx));
        u4LocalMask = BgpGetIpIfMask ((UINT2) (u4RtIfIndx));
    }
    if (u4LocalAddr == 0)
    {
        return BGP4_FAILURE;
    }
    PTR_ASSIGN_4 (pLocalAddr->NetAddr.au1Address, u4LocalAddr);
    pLocalAddr->u2PrefixLen = (UINT2) Bgp4GetSubnetmasklen (u4LocalMask);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4GetDefaultBgpIdentifier                               */
/* Description   : FutureBgp4 uses the highest interface address as the      */
/*               : default Bgp Identifier. This function checks the          */
/*               : interface table and gets the highest interface address.   */
/*               : If no interface is configured, then "0" is returned.      */
/*               : as the Bgp Identifier                                     */
/* Input(s)      : None.                                                     */
/* Output(s)     : Default BGP Identifier (pu4BgpIdentifier)                 */
/* Return(s)     : BGP4_SUCCESS - if a valid interface address is availble   */
/*               : BGP4_FAILURE - if no interface address is available. In   */
/*                                this case the pu4BgpIdentifier value is 0  */
/*****************************************************************************/
INT1
Bgp4GetDefaultBgpIdentifier (UINT4 u4Context, UINT4 *pu4BgpIdentifier)
{

#if (CUST_BGP_ROUTER_ID_SELECT == OSIX_TRUE)
    if(UtilSelectRouterId(u4Context,
				pu4BgpIdentifier) == OSIX_FAILURE)
#else
    if (NetIpv4GetHighestIpAddrInCxt (u4Context,
                                      pu4BgpIdentifier) == NETIPV4_FAILURE)
#endif
    {
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv4IsDirectlyConnected                               */
/* Description   : This function checks where the given input address is     */
/*               : reachable via any of the directly connected interfaces    */
/* Input(s)      : Ip address that needs to be checked (u4IpAddress)         */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE - if input belongs to directly connected network*/
/*               : BGP4_FALSE- otherwise.                                    */
/*****************************************************************************/
UINT1
Bgp4Ipv4IsDirectlyConnected (UINT4 u4PeerAddr, UINT4 u4VrfId)
{
    UINT1               u1IsDirect = BGP4_FALSE;
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT4               u4IfAddr = BGP4_INV_IPADDRESS;
    UINT4               u4ContextId = u4VrfId;
    UINT4               u4IfMask = BGP4_INV_IPADDRESS;
    UINT4               u4IfIndex = BGP4_INVALID_IFINDX;

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    BgpUnLock ();
    if (NetIpv4GetFirstIfInfoInCxt (u4ContextId,
                                    &NetIpIfInfo) == NETIPV4_FAILURE)
    {  
        BgpLock ();
        return BGP4_FALSE;
    }
    BgpLock ();

    for (;;)
    {
        u4IfIndex = NetIpIfInfo.u4IfIndex;
        u4IfAddr = NetIpIfInfo.u4Addr;
        u4IfMask = NetIpIfInfo.u4NetMask;

        /* Check whether the received address is directly
         * connected to router. */
        if ((u4PeerAddr & u4IfMask) == (u4IfAddr & u4IfMask))
        {
            u1IsDirect = BGP4_TRUE;
            break;
        }

        MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
        if (NetIpv4GetNextIfInfoInCxt (u4ContextId,
                                       u4IfIndex,
                                       &NetIpIfInfo) == NETIPV4_FAILURE)
        {
            break;
        }
    }
    return u1IsDirect;
}

/*****************************************************************************/
/* Function Name : Bgp4Ipv4GetNetAddrPrefixLen                               */
/* Description   : This function get the interface through which the given   */
/*               : address is reachable and return the prefix length of that */
/*               : interface.                                                */
/* Input(s)      : Ip address that needs to be checked (u4RemAddr)           */
/* Output(s)     : None.                                                     */
/* Return(s)     : 0 - if the prefix length cannot be found. Else the valid  */
/*               : prefix length is return.                                  */
/*****************************************************************************/
UINT2
Bgp4Ipv4GetNetAddrPrefixLen (UINT4 u4RemAddr)
{
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT4               u4IfAddr = BGP4_INV_IPADDRESS;
    UINT4               u4IfMask = BGP4_INV_IPADDRESS;
    UINT4               u4IfIndex = BGP4_INVALID_IFINDX;
    UINT4               u4ContextId = 0;
    UINT2               u2PrefixLen = 0;

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    BgpUnLock ();
    if (NetIpv4GetFirstIfInfoInCxt (u4ContextId,
                                    &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        BgpLock ();
        return 0;
    }
    BgpLock ();

    for (;;)
    {
        u4IfIndex = NetIpIfInfo.u4IfIndex;
        u4IfAddr = NetIpIfInfo.u4Addr;
        u4IfMask = NetIpIfInfo.u4NetMask;

        /* Check whether the received address is directly
         * connected to router. */
        if ((u4RemAddr & u4IfMask) == (u4IfAddr & u4IfMask))
        {
            break;
        }

        MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
        if (NetIpv4GetNextIfInfoInCxt (u4ContextId,
                                       u4IfIndex,
                                       &NetIpIfInfo) == NETIPV4_FAILURE)
        {
            /* No matching interface found. */
            return 0;
        }
    }

    u2PrefixLen = (UINT2) Bgp4GetSubnetmasklen (u4IfMask);
    return u2PrefixLen;
}

/*****************************************************************************/
/* Function Name :  BgpIp4GetNextRtEntry                                     */
/* Description   :  This routine is used to get the next alternate route     */
/*                  for a given destination                                  */
/* Input(s)      :  Route entry that has to be searched. (u4Ipaddr)          */
/*                  Mask of the route entry              (u4IpMask)          */
/*                  Protocol Id of the route entry       (i1AppId)           */
/* Output(s)     :  Reachability for the route entry     (pu4Rt)             */
/*                  Metric associated with the route     (pi4Metric)         */
/*                  Interface Index for the reachability (pu4RtIfindx)       */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
BgpIp4GetNextRtEntry (UINT4 u4VrfId, UINT4 u4Ipaddr,
                      UINT2 u2PrefixLen, UINT4 u4IfIndex, UINT4 *pu4Rt,
                      INT4 *pi4Metric, UINT4 *pu4RtIfIndx)
{
    INT4                i4RetVal = BGP4_FAILURE;

#ifdef RRD_WANTED
    UNUSED_PARAM (u4VrfId);
    UNUSED_PARAM (u4Ipaddr);
    UNUSED_PARAM (u2PrefixLen);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4Rt);
    UNUSED_PARAM (pi4Metric);
    UNUSED_PARAM (pu4RtIfIndx);
    return i4RetVal;
#else
    return i4RetVal;
#endif

}

INT4
ipMask2PrefixLen (UINT4 u4Mask)
{
    UINT1               u1Tmp;
    UINT1              *pu1Byte;
    UINT1               u1TestByte = 0;
    INT4                i4Len = 0;
    INT4                byte, bit;

    u4Mask = ntohl (u4Mask);
    pu1Byte = (UINT1 *) &u4Mask;
    for (byte = 0; byte < 4; byte++)
    {
        u1TestByte = *pu1Byte;
        u1Tmp = 0x80;
        for (bit = 0; bit < 8; bit++)
        {
            if (!(u1TestByte & u1Tmp))
            {
                break;
            }
            i4Len++;
            u1TestByte = u1TestByte << 1;
        }
        if (bit != 8)
            break;
        pu1Byte++;
    }
    return (i4Len);
}

UINT4
ipPrefixLen2Mask (UINT4 u4PrefixLen)
{
    UINT4               u4Tmp = 0x80000000;
    UINT4               u4Mask = 0;

    while (u4PrefixLen--)
    {
        u4Mask |= u4Tmp;
        u4Tmp >>= 1;
    }
    return (u4Mask);
}

/******************************************************************************/
/* Function Name : Bgp4IsLocalAddr                                            */
/* Description   : This routine checks whether the given address belongs to   */
/*                 any of the interface address or not.                       */
/* Input(s)      : u4Addr - Address to be verified.                           */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_TRUE - if given address matches interface address     */
/*                 BGP4_FALSE - if does not match                             */
/******************************************************************************/
UINT4
Bgp4IsLocalAddr (UINT4 u4Addr)
{
    UNUSED_PARAM (u4Addr);
    return BGP4_FALSE;
}

/******************************************************************************/
/* Function Name : Bgp4GetIfPort                                              */
/* Description   : This routine checks retrieves the interface index          */
/*                 corresponding to the ip address                            */
/* Input(s)      : u4IpAddr - Address .                                       */
/* Output(s)     : u4IfIndex - Interface index id                             */
/* Return(s)     : BGP4_TRUE/BGP4_FALSE                                       */
/******************************************************************************/
INT4
Bgp4GetIfPort (UINT4 u4IpAddr, UINT4 *pu4IfIndex)
{

    if (NetIpv4GetIfIndexFromAddr (u4IpAddr, pu4IfIndex) == NETIPV4_SUCCESS)
    {
        return BGP4_TRUE;
    }
    return BGP4_FALSE;
}

/******************************************************************************/
/* Function Name : Bgp4CbRouteChangeHandler                                   */
/* Description   : This routine gets route change notification                */
/* Input(s)      : u4IpAddr - Address .                                       */
/* Output(s)     : u4IfIndex - Interface index id                             */
/* Return(s)     : BGP4_TRUE/BGP4_FALSE                                       */
/******************************************************************************/
VOID
Bgp4CbRouteChangeHandler  (tNetIpv4RtInfo * pRtChg, tNetIpv4RtInfo * pRtChg1, UINT1 u1BitMap)
{
    if (u1BitMap != NETIPV4_MODIFY_ROUTE)
    {
        Bgp4CbRtChangeHdlr(pRtChg, u1BitMap);
        return;
    }
    Bgp4CbRtChangeHdlr(pRtChg, NETIPV4_DELETE_ROUTE);
    Bgp4CbRtChangeHdlr(pRtChg1, NETIPV4_ADD_ROUTE);


}

/*****************************************************************************/
/* Function Name : Bgp4CbIfChgHdlr                                           */
/* Description   : Callback function to get the information about interfaces */
/* Input(s)      : u4IfIndex - Interface Index                               */
/*                 u4BitMap -  Circuit Attribute Identifier BitMap           */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
PUBLIC VOID
Bgp4CbIfChgHdlr (tNetIpv4IfInfo * pNetIpv4GetInfo, UINT4 u4BitMap)
{
    tBgp4QMsg          *pQMsg = NULL;
    UINT4               u4OperStatus;

    /* OPER_STATE is defined in future/inc/ip.h
     * IFACE_DELETED is defined in future/ip/ipif/inc/ipifutls.h
     * since IFACE_DELETED is not within the scope to use, define
     * local constant which is having the same value as is defined by IP
     */
    if (u4BitMap == IP_ADDR_BIT_MASK)
    {
        if (pNetIpv4GetInfo->u4Addr == 0)
        {
            pNetIpv4GetInfo->u4Oper = BGP4_IPIF_OPER_DISABLE;
        }

        else
        {
            pNetIpv4GetInfo->u4Oper = BGP4_IPIF_OPER_ENABLE;
        }
    }

    switch (u4BitMap)
    {
        case OPER_STATE:
        case IP_ADDR_BIT_MASK:
            u4OperStatus = pNetIpv4GetInfo->u4Oper;
            break;
        case BGP4_IFACE_DELETE:
            u4OperStatus = BGP4_IFACE_DELETE;
            break;
        default:
            return;
    }

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting Inteface Chnage Status Msg "
                  "to BGP Queue FAILED!!!\n");
        return;
    }
    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_IFACE_CHG_EVENT;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
    BGP4_INPUTQ_IFACE_INDEX (pQMsg) = pNetIpv4GetInfo->u4IfIndex;
    BGP4_INPUTQ_IFACE_STATUS (pQMsg) = u4OperStatus;
    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME, "\tPosting an Event to BGP FAILED!!!\n");
        return;
    }
    return;
}

/********************************************************************/
/* Function Name   : Bgp4GetIfInfoFromIp                            */
/* Description     : Gets the interface related information from IP */
/* Input(s)        : u4IfIndex - interface index                    */
/*                   pIfEntry - interface entry                     */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : None.                                */
/********************************************************************/
INT4
Bgp4GetIfInfoFromIp (UINT4 u4IfIndex, tBgp4IfInfo * pIfEntry)
{
    tNetIpv4IfInfo      NetIpv4Info;
    INT4                i4RetVal;

    /* call the netipv4 routine to get the interface related information */
    i4RetVal = NetIpv4GetIfInfo (u4IfIndex, &NetIpv4Info);
    if (i4RetVal != NETIPV4_SUCCESS)
    {
        return BGP4_FAILURE;
    }

    /* Fill out the address and operational status */
    BGP4_IFACE_ADDR (pIfEntry) = NetIpv4Info.u4Addr;
    BGP4_IFACE_ENTRY_INDEX (pIfEntry) = u4IfIndex;
    BGP4_IFACE_ENTRY_OPER_STATUS (pIfEntry) = (UINT1) NetIpv4Info.u4Oper;
    BGP4_IFACE_ENTRY_IFTYPE (pIfEntry) = NetIpv4Info.u4IfType;
    BGP4_IFACE_ENTRY_VRFID (pIfEntry) = NetIpv4Info.u4ContextId;

    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4CbRtChangeHdlr                                         */
/* Description   : This routine gets route change notification                */
/* Input(s)      : u4IpAddr - Address .                                       */
/* Output(s)     : u4IfIndex - Interface index id                             */
/* Return(s)     : BGP4_TRUE/BGP4_FALSE                                       */
/******************************************************************************/
VOID
Bgp4CbRtChangeHdlr  (tNetIpv4RtInfo * pRtChg, UINT1 u1BitMap)
{
    tNetworkAddr        *pNetworkRt = NULL;
    tNetworkAddr         NetworkAddr;
    tNetworkAddr         NetworkRoute;
    tBgp4QMsg          *pQMsg = NULL;
    UINT4               u4Action;
    UINT4               u4PrefixLen;
    UINT4               u4DestAddr;
    UINT4               u4NextHopAddr;
    UINT4               u4Context = 0;

     MEMSET (&NetworkRoute, 0, sizeof (tNetworkAddr));
     MEMSET (&NetworkAddr, 0, sizeof (tNetworkAddr));

    if(pRtChg->u2RtProto == BGP_ID)
    {
        return;
    }
    switch (u1BitMap)
    {
        case NETIPV4_ADD_ROUTE:
            u4Action = BGP4_IMPORT_RT_ADD;
            break;
        case NETIPV4_DELETE_ROUTE:
            u4Action  = BGP4_IMPORT_RT_DELETE;
            break;
        default:
            return;
    }

    IPV4_MASK_TO_MASKLEN (u4PrefixLen, pRtChg->u4DestMask);
    u4DestAddr = OSIX_NTOHL (pRtChg->u4DestNet);
    u4NextHopAddr = (pRtChg->u4NextHop);
    u4Context = pRtChg->u4ContextId;
    PTR_ASSIGN_4 (NetworkAddr.NetworkAddr.au1Address, u4DestAddr);
    NetworkAddr.NetworkAddr.u2AddressLen = BGP4_IPV4_PREFIX_LEN;
    NetworkAddr.NetworkAddr.u2Afi =  BGP4_INET_AFI_IPV4;
    pNetworkRt = Bgp4NetworkRouteGetEntry (u4Context, &NetworkAddr ,&NetworkRoute);

    if (pNetworkRt == NULL)
    {
        /*Network entry is not matched with the incoming route indication from RTM*/
        return;
    }

     pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting Inteface Chnage Status Msg "
                  "to BGP Queue FAILED!!!\n");
        return;
    }
    Bgp4InitAddrPrefixStruct (& (BGP4_INPUTQ_ROUTE_ADDR_INFO (pQMsg)), BGP4_INET_AFI_IPV4);
    Bgp4InitAddrPrefixStruct (& (BGP4_INPUTQ_ROUTE_NEXTHOP_INFO (pQMsg)), BGP4_INET_AFI_IPV4);

    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_ROUTE_CHG_EVENT;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
    BGP4_INPUTQ_ROUTE_ADDR_PREFIXLEN_INFO(pQMsg) = u4PrefixLen;
    BGP4_INPUTQ_ROUTE_ACTION (pQMsg) = u4Action;
    BGP4_INPUTQ_ROUTE_IF_INDEX (pQMsg) = pRtChg->u4RtIfIndx;
    BGP4_INPUTQ_ROUTE_METRIC (pQMsg) = pRtChg->i4Metric1;
    BGP4_INPUTQ_ROUTE_PROTO_ID (pQMsg) = pRtChg->u2RtProto;
    /*    BGP4_INPUTQ_ROUTE_ADDR_INFO (pQMsg), IPVX_ADDR_FMLY_IPV4,
     *                  (UINT1 *) &(u4DestAddr));
     *                  IPVX_ADDR_INIT_IPV4 (BGP4_INPUTQ_ROUTE_NEXTHOP_INFO (pQMsg), IPVX_ADDR_FMLY_IPV4,
     *                  (UINT1 *) &(u4NextHopAddr)); */
     PTR_ASSIGN_4 (pQMsg->QMsg.Bgp4RtInfoMsg.DestAddr.au1Address, u4DestAddr);
     PTR_ASSIGN_4 (pQMsg->QMsg.Bgp4RtInfoMsg.NextHop.au1Address, u4NextHopAddr);

    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME, "\tPosting an Event to BGP FAILED!!!\n");
        return;
    }
    return;

}

#endif /* BGLNIPF4_C */
