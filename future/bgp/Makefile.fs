#####################################################################
##########################################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved                   #
# ----------------------------------------------------                   #
# $Id: Makefile.fs,v 1.3 2016/02/22 12:06:21 siva Exp $                  #
# DESCRIPTION   : The makefile for builing Future BGP                    #
##########################################################################

#####################################################################
####  MAKEFILE HEADER ::                                         ####
##|###############################################################|##
##|    FILE NAME               ::  Makefile (BGP)                 |##
##|                                                               |##
##|    PRINCIPAL    AUTHOR     ::  Future Software Pvt. Ltd.      |##
##|                                                               |##
##|    SUBSYSTEM NAME          ::  BGP                            |##
##|                                                               |##
##|    MODULE NAME             ::  Top Most Level Makefile        |##
##|                                                               |##
##|    TARGET ENVIRONMENT      ::  UNIX                           |##
##|                                                               |##
##|    DATE OF FIRST RELEASE   ::  20.01.2K                       |##
##|                                                               |##
##|    DESCRIPTION             ::  MAKEFILE for FutureBGP4        |##
##|                                Router Final Object            |##
##|###############################################################|##
#### CHANGE RECORD ::                                            ####
##|#######|#############|##########|##############################|##
##|VERSION|    DATE     |  AUTHOR  |  DESCRIPTION OF CHANGE       |##
##|#######|#############|##########|##############################|##
##|#######|###############|#######################################|##
##|#######|###############|#######################################|##

include ../LR/make.h
include ../LR/make.rule
include make.h

.PHONY : clean

C_FLAGS = ${CC_FLAGS} \
	-I${SNMP_BASE_DIR}/common/inc\
	-I${SNMP_MDL_BASE_DIR}/inc -I${BASE_DIR}/sli\
	-I${BASE_DIR}/mpls/mplsinc\
	-I${BGP_INC_DIR} -I${BGP_PORT_DIR} -I${BGP_TRIEINC_DIR} ${GLOBAL_INCLUDES}

#Object Module List
BGP_OBJS=\
	$(BGP_OBJ_DIR)/bgp4dech.o\
	$(BGP_OBJ_DIR)/bgp4vpn.o\
        $(BGP_OBJ_DIR)/bgp4evpn.o\
	$(BGP_OBJ_DIR)/bgp4ah.o\
	$(BGP_OBJ_DIR)/bgp4trie.o\
	$(BGP_OBJ_DIR)/bgtrieapif.o\
	$(BGP_OBJ_DIR)/bgtriemem.o\
	$(BGP_OBJ_DIR)/bgtrieutil.o\
	$(BGP_OBJ_DIR)/bgp4init.o\
	$(BGP_OBJ_DIR)/bgp4prcs.o\
	$(BGP_OBJ_DIR)/bgp4eh.o\
	$(BGP_OBJ_DIR)/bgp4ih.o\
	$(BGP_OBJ_DIR)/bgp4msgh.o\
	$(BGP_OBJ_DIR)/bgp4mem.o\
	$(BGP_OBJ_DIR)/bgp4semh.o\
	$(BGP_OBJ_DIR)/bgp4ribh.o\
	$(BGP_OBJ_DIR)/bgp4igph.o\
	$(BGP_OBJ_DIR)/bgp4sync.o\
	$(BGP_OBJ_DIR)/bgp4util.o\
	$(BGP_OBJ_DIR)/bgp4tmrh.o\
	$(BGP_OBJ_DIR)/bgp4peer.o\
	$(BGP_OBJ_DIR)/bgp4tcph.o\
	$(BGP_OBJ_DIR)/bgconfed.o\
	$(BGP_OBJ_DIR)/bgrtref.o\
	$(BGP_OBJ_DIR)/bgrfladv.o\
	$(BGP_OBJ_DIR)/bgp4dsh.o\
	$(BGP_OBJ_DIR)/bgp4attr.o\
	$(BGP_OBJ_DIR)/bgp4aggr.o\
	$(BGP_OBJ_DIR)/bgrbifun.o\
	$(BGP_OBJ_DIR)/bgp4filt.o\
	$(BGP_OBJ_DIR)/bgcapfil.o\
	$(BGP_OBJ_DIR)/bgcaprcv.o\
	$(BGP_OBJ_DIR)/bgcapntf.o\
	$(BGP_OBJ_DIR)/bgrflfil.o\
	$(BGP_OBJ_DIR)/bgrflinp.o\
	$(BGP_OBJ_DIR)/bgrflini.o\
	$(BGP_OBJ_DIR)/bgrflrts.o\
	$(BGP_OBJ_DIR)/bgcmattr.o\
	$(BGP_OBJ_DIR)/bgcmfilt.o\
	$(BGP_OBJ_DIR)/bgcminit.o\
	$(BGP_OBJ_DIR)/bgecattr.o\
	$(BGP_OBJ_DIR)/bgecfilt.o\
	$(BGP_OBJ_DIR)/bgecinit.o\
	$(BGP_OBJ_DIR)/bgp4main.o\
	$(BGP_OBJ_DIR)/bgmpinit.o\
	$(BGP_OBJ_DIR)/bgmpfill.o\
	$(BGP_OBJ_DIR)/bgmputl.o\
	$(BGP_OBJ_DIR)/bgmpprcs.o\
	$(BGP_OBJ_DIR)/bgfsrrd4.o\
	$(BGP_OBJ_DIR)/bgcomlow.o\
	$(BGP_OBJ_DIR)/bgeclow.o\
	$(BGP_OBJ_DIR)/bgcaplow.o\
	$(BGP_OBJ_DIR)/bgrfdlow.o\
	$(BGP_OBJ_DIR)/bgrfllow.o\
	$(BGP_OBJ_DIR)/asconlow.o\
	$(BGP_OBJ_DIR)/bgrrlow.o\
	$(BGP_OBJ_DIR)/bgpsnif4.o\
	$(BGP_OBJ_DIR)/fsbgp4lw.o\
	$(BGP_OBJ_DIR)/bgp4low.o\
	$(BGP_OBJ_DIR)/bgvpnlow.o\
	$(BGP_OBJ_DIR)/bgp4tst.o\
	
	
ifeq (DRFD_WANTED, $(findstring DRFD_WANTED,$(PROJECT_COMPILATION_SWITCHES)))
BGP_OBJS+=\
	$(BGP_OBJ_DIR)/bgrfdini.o\
	$(BGP_OBJ_DIR)/bgrfdutl.o\
	$(BGP_OBJ_DIR)/bgrfdfrt.o\
	$(BGP_OBJ_DIR)/bgrfdwrt.o\
	$(BGP_OBJ_DIR)/bgrfdpr.o\
	$(BGP_OBJ_DIR)/bgrfdru.o
endif

ifeq (DBGP4_IPV6_WANTED, $(findstring DBGP4_IPV6_WANTED,$(PROJECT_COMPILATION_SWITCHES)))
BGP_OBJS+=\
	$(BGP_OBJ_DIR)/bgpsnif6.o
endif

ifeq (${SNMP}, YES)
BGP_OBJS+=\
	$(BGP_OBJ_DIR)/stdbgmid.o\
	$(BGP_OBJ_DIR)/fsbgpmid.o
endif

ifeq (${SNMP_2}, YES)
BGP_OBJS+=\
	$(BGP_OBJ_DIR)/stdbgpwr.o\
	$(BGP_OBJ_DIR)/fsbgp4wr.o
endif

ifeq (${SLI}, YES)
BGP_OBJS+=\
	$(BGP_OBJ_DIR)/bgp4port.o\
	$(BGP_OBJ_DIR)/bgfsipf4.o\
	$(BGP_OBJ_DIR)/bgpfstcp.o

ifeq (DBGP4_IPV6_WANTED, $(findstring DBGP4_IPV6_WANTED,$(PROJECT_COMPILATION_SWITCHES)))
BGP_OBJS+= $(BGP_OBJ_DIR)/bgfsipf6.o
endif
endif

ifeq (${CLI}, YES)
BGP_OBJS+=\
	$(BGP_OBJ_DIR)/bgp4cli.o \
	$(BGP_OBJ_DIR)/bgclipkg.o \
	$(BGP_OBJ_DIR)/bgpfscli.o
endif

BGP_DEPENDENCIES+= ${BGP_PORT_DIR}/bgp4port.h\
                   ${BGP_PORT_DIR}/bgfsipf4.h\
                   ${BGP_PORT_DIR}/bgpfstcp.h

ifeq (DBGP4_IPV6_WANTED, $(findstring DBGP4_IPV6_WANTED,$(PROJECT_COMPILATION_SWITCHES)))
BGP_DEPENDENCIES+= ${BGP_PORT_DIR}/bgfsipf6.h
endif

all: CHECK_OBJ_DIR $(BGP_OBJS)
	$(LINKER) $(LINK_FLAGS) $(BGP_OBJ_DIR)/FutureBGP.o $(BGP_OBJS)
 
CHECK_OBJ_DIR:
#ifdef MKDIR
	$(MKDIR) $(MKDIR_FLAGS) $(BGP_OBJ_DIR)  
#endif
 
 bgp4.o :$(BGP_OBJS) $(BGP_DEPENDENCIES)
	$(LINKER) $(LINK_FLAGS) $(BGP_OBJ_DIR)/bgp4.o $(BGP_OBJS)

 $(BGP_OBJ_DIR)/bgp4tst.o : $(BGP_SRC_DIR)/test/bgp4tst.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/test/bgp4tst.c -o $(BGP_OBJ_DIR)/bgp4tst.o

 $(BGP_OBJ_DIR)/bgp4init.o : $(BGP_CHSRC_DIR)/bgp4init.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_CHSRC_DIR)/bgp4init.c -o $(BGP_OBJ_DIR)/bgp4init.o

 $(BGP_OBJ_DIR)/bgp4prcs.o : $(BGP_CHSRC_DIR)/bgp4prcs.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_CHSRC_DIR)/bgp4prcs.c -o $(BGP_OBJ_DIR)/bgp4prcs.o

 $(BGP_OBJ_DIR)/bgp4tcph.o : $(BGP_CHSRC_DIR)/bgp4tcph.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_CHSRC_DIR)/bgp4tcph.c -o $(BGP_OBJ_DIR)/bgp4tcph.o

 $(BGP_OBJ_DIR)/bgp4ih.o : $(BGP_CHSRC_DIR)/bgp4ih.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_CHSRC_DIR)/bgp4ih.c -o $(BGP_OBJ_DIR)/bgp4ih.o

 $(BGP_OBJ_DIR)/bgp4eh.o:$(BGP_CHSRC_DIR)/bgp4eh.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_CHSRC_DIR)/bgp4eh.c -o $(BGP_OBJ_DIR)/bgp4eh.o

 $(BGP_OBJ_DIR)/bgp4tmrh.o : $(BGP_CHSRC_DIR)/bgp4tmrh.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_CHSRC_DIR)/bgp4tmrh.c -o $(BGP_OBJ_DIR)/bgp4tmrh.o

 $(BGP_OBJ_DIR)/bgp4msgh.o : $(BGP_CHSRC_DIR)/bgp4msgh.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_CHSRC_DIR)/bgp4msgh.c -o $(BGP_OBJ_DIR)/bgp4msgh.o

 $(BGP_OBJ_DIR)/bgp4semh.o : $(BGP_CHSRC_DIR)/bgp4semh.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_CHSRC_DIR)/bgp4semh.c -o $(BGP_OBJ_DIR)/bgp4semh.o

 $(BGP_OBJ_DIR)/bgp4peer.o : $(BGP_CHSRC_DIR)/bgp4peer.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_CHSRC_DIR)/bgp4peer.c -o $(BGP_OBJ_DIR)/bgp4peer.o

 $(BGP_OBJ_DIR)/bgp4mem.o : $(BGP_PORT_DIR)/bgp4mem.c $(BGP_DEPENDENCIES)
	 $(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_PORT_DIR)/bgp4mem.c -o $(BGP_OBJ_DIR)/bgp4mem.o

 $(BGP_OBJ_DIR)/bgp4snmph.o : $(BGP_CHSRC_DIR)/bgp4snmph.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_CHSRC_DIR)/bgp4snmph.c -o $(BGP_OBJ_DIR)/bgp4snmph.o

 $(BGP_OBJ_DIR)/bgp4main.o : $(BGP_PORT_DIR)/bgp4main.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_PORT_DIR)/bgp4main.c -o $(BGP_OBJ_DIR)/bgp4main.o

 $(BGP_OBJ_DIR)/bgp4port.o : $(BGP_PORT_DIR)/bgp4port.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_PORT_DIR)/bgp4port.c -o $(BGP_OBJ_DIR)/bgp4port.o

 $(BGP_OBJ_DIR)/bgfsipf4.o : $(BGP_PORT_DIR)/bgfsipf4.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_PORT_DIR)/bgfsipf4.c -o $(BGP_OBJ_DIR)/bgfsipf4.o

 $(BGP_OBJ_DIR)/bgfsipf6.o : $(BGP_PORT_DIR)/bgfsipf6.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_PORT_DIR)/bgfsipf6.c -o $(BGP_OBJ_DIR)/bgfsipf6.o

 $(BGP_OBJ_DIR)/bgpfstcp.o : $(BGP_PORT_DIR)/bgpfstcp.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_PORT_DIR)/bgpfstcp.c -o $(BGP_OBJ_DIR)/bgpfstcp.o

 $(BGP_OBJ_DIR)/bgfsrrd4.o : $(BGP_PORT_DIR)/bgfsrrd4.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_PORT_DIR)/bgfsrrd4.c -o $(BGP_OBJ_DIR)/bgfsrrd4.o

###########RIB HANDLER ######################################

 $(BGP_OBJ_DIR)/bgp4ribh.o : $(BGP_RIBSRC_DIR)/bgp4ribh.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RIBSRC_DIR)/bgp4ribh.c -o $(BGP_OBJ_DIR)/bgp4ribh.o

 $(BGP_OBJ_DIR)/bgp4dech.o : $(BGP_RIBSRC_DIR)/bgp4dech.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RIBSRC_DIR)/bgp4dech.c -o $(BGP_OBJ_DIR)/bgp4dech.o

 $(BGP_OBJ_DIR)/bgp4dsh.o : $(BGP_RIBSRC_DIR)/bgp4dsh.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RIBSRC_DIR)/bgp4dsh.c -o $(BGP_OBJ_DIR)/bgp4dsh.o

 $(BGP_OBJ_DIR)/bgp4tree.o : $(BGP_RIBSRC_DIR)/bgp4tree.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RIBSRC_DIR)/bgp4tree.c -o $(BGP_OBJ_DIR)/bgp4tree.o

 $(BGP_OBJ_DIR)/bgp4trie.o : $(BGP_RIBSRC_DIR)/bgp4trie.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RIBSRC_DIR)/bgp4trie.c -o $(BGP_OBJ_DIR)/bgp4trie.o

 $(BGP_OBJ_DIR)/bgrbifun.o : $(BGP_RIBSRC_DIR)/bgrbifun.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RIBSRC_DIR)/bgrbifun.c -o $(BGP_OBJ_DIR)/bgrbifun.o

 $(BGP_OBJ_DIR)/bgp4ah.o : $(BGP_RIBSRC_DIR)/bgp4ah.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RIBSRC_DIR)/bgp4ah.c -o $(BGP_OBJ_DIR)/bgp4ah.o

 $(BGP_OBJ_DIR)/bgp4aggr.o : $(BGP_RIBSRC_DIR)/bgp4aggr.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RIBSRC_DIR)/bgp4aggr.c -o $(BGP_OBJ_DIR)/bgp4aggr.o

 $(BGP_OBJ_DIR)/bgp4attr.o : $(BGP_RIBSRC_DIR)/bgp4attr.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RIBSRC_DIR)/bgp4attr.c -o $(BGP_OBJ_DIR)/bgp4attr.o

 $(BGP_OBJ_DIR)/bgp4filt.o : $(BGP_RIBSRC_DIR)/bgp4filt.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RIBSRC_DIR)/bgp4filt.c -o $(BGP_OBJ_DIR)/bgp4filt.o

 $(BGP_OBJ_DIR)/bgp4igph.o : $(BGP_RIBSRC_DIR)/bgp4igph.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RIBSRC_DIR)/bgp4igph.c -o $(BGP_OBJ_DIR)/bgp4igph.o

 $(BGP_OBJ_DIR)/bgp4sync.o : $(BGP_RIBSRC_DIR)/bgp4sync.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RIBSRC_DIR)/bgp4sync.c -o $(BGP_OBJ_DIR)/bgp4sync.o

 $(BGP_OBJ_DIR)/bgp4util.o : $(BGP_RIBSRC_DIR)/bgp4util.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RIBSRC_DIR)/bgp4util.c -o $(BGP_OBJ_DIR)/bgp4util.o

 $(BGP_OBJ_DIR)/bgrfladv.o : $(BGP_RFLSRC_DIR)/bgrfladv.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RFLSRC_DIR)/bgrfladv.c -o $(BGP_OBJ_DIR)/bgrfladv.o

 $(BGP_OBJ_DIR)/bgrflfil.o : $(BGP_RFLSRC_DIR)/bgrflfil.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RFLSRC_DIR)/bgrflfil.c -o $(BGP_OBJ_DIR)/bgrflfil.o

 $(BGP_OBJ_DIR)/bgrflini.o : $(BGP_RFLSRC_DIR)/bgrflini.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RFLSRC_DIR)/bgrflini.c -o $(BGP_OBJ_DIR)/bgrflini.o

 $(BGP_OBJ_DIR)/bgrflinp.o : $(BGP_RFLSRC_DIR)/bgrflinp.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RFLSRC_DIR)/bgrflinp.c -o $(BGP_OBJ_DIR)/bgrflinp.o

 $(BGP_OBJ_DIR)/bgrflrts.o : $(BGP_RFLSRC_DIR)/bgrflrts.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RFLSRC_DIR)/bgrflrts.c -o $(BGP_OBJ_DIR)/bgrflrts.o

 $(BGP_OBJ_DIR)/bgrfdini.o : $(BGP_RFDSRC_DIR)/bgrfdini.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RFDSRC_DIR)/bgrfdini.c -o $(BGP_OBJ_DIR)/bgrfdini.o

 $(BGP_OBJ_DIR)/bgrfdutl.o : $(BGP_RFDSRC_DIR)/bgrfdutl.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RFDSRC_DIR)/bgrfdutl.c -o $(BGP_OBJ_DIR)/bgrfdutl.o

 $(BGP_OBJ_DIR)/bgrfdfrt.o : $(BGP_RFDSRC_DIR)/bgrfdfrt.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RFDSRC_DIR)/bgrfdfrt.c -o $(BGP_OBJ_DIR)/bgrfdfrt.o

 $(BGP_OBJ_DIR)/bgrfdwrt.o : $(BGP_RFDSRC_DIR)/bgrfdwrt.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RFDSRC_DIR)/bgrfdwrt.c -o $(BGP_OBJ_DIR)/bgrfdwrt.o

 $(BGP_OBJ_DIR)/bgrfdpr.o : $(BGP_RFDSRC_DIR)/bgrfdpr.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RFDSRC_DIR)/bgrfdpr.c -o $(BGP_OBJ_DIR)/bgrfdpr.o

 $(BGP_OBJ_DIR)/bgrfdru.o : $(BGP_RFDSRC_DIR)/bgrfdru.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RFDSRC_DIR)/bgrfdru.c -o $(BGP_OBJ_DIR)/bgrfdru.o

 $(BGP_OBJ_DIR)/bgcmattr.o : $(BGP_COMSRC_DIR)/bgcmattr.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_COMSRC_DIR)/bgcmattr.c -o $(BGP_OBJ_DIR)/bgcmattr.o

 $(BGP_OBJ_DIR)/bgcmfilt.o : $(BGP_COMSRC_DIR)/bgcmfilt.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_COMSRC_DIR)/bgcmfilt.c -o $(BGP_OBJ_DIR)/bgcmfilt.o

 $(BGP_OBJ_DIR)/bgcminit.o : $(BGP_COMSRC_DIR)/bgcminit.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_COMSRC_DIR)/bgcminit.c -o $(BGP_OBJ_DIR)/bgcminit.o

 $(BGP_OBJ_DIR)/bgecattr.o : $(BGP_ECOMSRC_DIR)/bgecattr.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_ECOMSRC_DIR)/bgecattr.c -o $(BGP_OBJ_DIR)/bgecattr.o

 $(BGP_OBJ_DIR)/bgecfilt.o : $(BGP_ECOMSRC_DIR)/bgecfilt.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_ECOMSRC_DIR)/bgecfilt.c -o $(BGP_OBJ_DIR)/bgecfilt.o

 $(BGP_OBJ_DIR)/bgecinit.o : $(BGP_ECOMSRC_DIR)/bgecinit.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_ECOMSRC_DIR)/bgecinit.c -o $(BGP_OBJ_DIR)/bgecinit.o

 $(BGP_OBJ_DIR)/bgcapfil.o : $(BGP_CAPSSRC_DIR)/bgcapfil.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_CAPSSRC_DIR)/bgcapfil.c -o $(BGP_OBJ_DIR)/bgcapfil.o

 $(BGP_OBJ_DIR)/bgcaprcv.o : $(BGP_CAPSSRC_DIR)/bgcaprcv.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_CAPSSRC_DIR)/bgcaprcv.c -o $(BGP_OBJ_DIR)/bgcaprcv.o

 $(BGP_OBJ_DIR)/bgcapntf.o : $(BGP_CAPSSRC_DIR)/bgcapntf.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_CAPSSRC_DIR)/bgcapntf.c -o $(BGP_OBJ_DIR)/bgcapntf.o

####################### Route-Refresh ######################################
 $(BGP_OBJ_DIR)/bgrtref.o : $(BGP_RTREFSRC_DIR)/bgrtref.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_RTREFSRC_DIR)/bgrtref.c -o $(BGP_OBJ_DIR)/bgrtref.o
 
####################### Multiprotocl Extensions ############################
 $(BGP_OBJ_DIR)/bgmpinit.o : $(BGP_MPESRC_DIR)/bgmpinit.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_MPESRC_DIR)/bgmpinit.c -o $(BGP_OBJ_DIR)/bgmpinit.o

 $(BGP_OBJ_DIR)/bgmpfill.o : $(BGP_MPESRC_DIR)/bgmpfill.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_MPESRC_DIR)/bgmpfill.c -o $(BGP_OBJ_DIR)/bgmpfill.o

 $(BGP_OBJ_DIR)/bgmpprcs.o : $(BGP_MPESRC_DIR)/bgmpprcs.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_MPESRC_DIR)/bgmpprcs.c -o $(BGP_OBJ_DIR)/bgmpprcs.o

 $(BGP_OBJ_DIR)/bgmputl.o : $(BGP_MPESRC_DIR)/bgmputl.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_MPESRC_DIR)/bgmputl.c -o $(BGP_OBJ_DIR)/bgmputl.o

####################### AS-CONFED ######################################
$(BGP_OBJ_DIR)/bgconfed.o : $(BGP_CONFED_DIR)/bgconfed.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_CONFED_DIR)/bgconfed.c -o $(BGP_OBJ_DIR)/bgconfed.o

################### BGP/MPLS VPNs #########################
 $(BGP_OBJ_DIR)/bgp4vpn.o : $(BGP_VPN4_DIR)/bgp4vpn.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_VPN4_DIR)/bgp4vpn.c -o $(BGP_OBJ_DIR)/bgp4vpn.o

+################### BGP/VXLAN EVPN #########################
 $(BGP_OBJ_DIR)/bgp4evpn.o : $(BGP_EVPN_DIR)/bgp4evpn.c $(BGP_DEPENDENCIES)
	$(CC) $(ISS_C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_EVPN_DIR)/bgp4evpn.c -o $(BGP_OBJ_DIR)/bgp4evpn.o

 ##################### SNMP ######################################
 $(BGP_OBJ_DIR)/bgpsnif4.o : $(BGP_SRC_DIR)/bgpsnif4.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/bgpsnif4.c -o $(BGP_OBJ_DIR)/bgpsnif4.o

 $(BGP_OBJ_DIR)/bgpsnif6.o : $(BGP_SRC_DIR)/bgpsnif6.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/bgpsnif6.c -o $(BGP_OBJ_DIR)/bgpsnif6.o

$(BGP_OBJ_DIR)/BGtriemem.o : $(BGP_SRC_DIR)/BGtriemem.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/BGtriemem.c -o $(BGP_OBJ_DIR)/BGtriemem.o

 $(BGP_OBJ_DIR)/BGtrieutil.o : $(BGP_SRC_DIR)/BGtrieutil.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/BGtrieutil.c -o $(BGP_OBJ_DIR)/BGtrieutil.o

 $(BGP_OBJ_DIR)/fsbgp4lw.o : $(BGP_SRC_DIR)/fsbgplow.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/fsbgplow.c -o $(BGP_OBJ_DIR)/fsbgp4lw.o

 $(BGP_OBJ_DIR)/bgp4low.o : $(BGP_SRC_DIR)/bgp4low.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/bgp4low.c -o $(BGP_OBJ_DIR)/bgp4low.o

 $(BGP_OBJ_DIR)/bgrfllow.o : $(BGP_SRC_DIR)/bgrfllow.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/bgrfllow.c -o $(BGP_OBJ_DIR)/bgrfllow.o

 $(BGP_OBJ_DIR)/bgrfdlow.o : $(BGP_SRC_DIR)/bgrfdlow.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/bgrfdlow.c -o $(BGP_OBJ_DIR)/bgrfdlow.o

 $(BGP_OBJ_DIR)/bgcomlow.o : $(BGP_SRC_DIR)/bgcomlow.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/bgcomlow.c -o $(BGP_OBJ_DIR)/bgcomlow.o

 $(BGP_OBJ_DIR)/bgeclow.o : $(BGP_SRC_DIR)/bgeclow.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/bgeclow.c -o $(BGP_OBJ_DIR)/bgeclow.o

 $(BGP_OBJ_DIR)/bgcaplow.o : $(BGP_SRC_DIR)/bgcaplow.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/bgcaplow.c -o $(BGP_OBJ_DIR)/bgcaplow.o

 $(BGP_OBJ_DIR)/bgrrlow.o : $(BGP_SRC_DIR)/bgrrlow.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/bgrrlow.c -o $(BGP_OBJ_DIR)/bgrrlow.o

 $(BGP_OBJ_DIR)/asconlow.o : $(BGP_SRC_DIR)/asconlow.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/asconlow.c -o $(BGP_OBJ_DIR)/asconlow.o

 $(BGP_OBJ_DIR)/bgmpelow.o : $(BGP_SRC_DIR)/bgmpelow.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/bgmpelow.c -o $(BGP_OBJ_DIR)/bgmpelow.o

 $(BGP_OBJ_DIR)/bgvpnlow.o : $(BGP_SRC_DIR)/bgvpnlow.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/bgvpnlow.c -o $(BGP_OBJ_DIR)/bgvpnlow.o


 $(BGP_OBJ_DIR)/bgmpemid.o : $(BGP_SRC_DIR)/bgmpemid.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/bgmpemid.c -o $(BGP_OBJ_DIR)/bgmpemid.o

############################### SNMP ##############################

 $(BGP_OBJ_DIR)/stdbgmid.o : $(BGP_SRC_DIR)/stdbgmid.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/stdbgmid.c -o $(BGP_OBJ_DIR)/stdbgmid.o

 $(BGP_OBJ_DIR)/fsbgpmid.o : $(BGP_SRC_DIR)/fsbgpmid.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/fsbgpmid.c -o $(BGP_OBJ_DIR)/fsbgpmid.o

 ############################### SNMP_2 ##############################

 $(BGP_OBJ_DIR)/stdbgpwr.o : $(BGP_SRC_DIR)/stdbgpwr.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/stdbgpwr.c -o $(BGP_OBJ_DIR)/stdbgpwr.o

 $(BGP_OBJ_DIR)/fsbgp4wr.o : $(BGP_SRC_DIR)/fsbgp4wr.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/fsbgp4wr.c -o $(BGP_OBJ_DIR)/fsbgp4wr.o

 ############################### CLI #########################################

ifeq (${CLI}, YES)
 $(BGP_OBJ_DIR)/bgclipkg.o : $(BGP_SRC_DIR)/bgclipkg.c $(BGP_CLI_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_SRC_DIR)/bgclipkg.c -o $(BGP_OBJ_DIR)/bgclipkg.o

 $(BGP_OBJ_DIR)/bgp4cli.o : $(BGP_PORT_DIR)/bgp4cli.c $(BGP_CLI_DEPENDENCIES)
	$(CC) $(C_FLAGS) -I$(CLI_INC) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_PORT_DIR)/bgp4cli.c -o $(BGP_OBJ_DIR)/bgp4cli.o

 $(BGP_OBJ_DIR)/bgpfscli.o : $(BGP_PORT_DIR)/bgpfscli.c $(BGP_CLI_DEPENDENCIES)
	$(CC) $(C_FLAGS) -I$(CLI_INC) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_PORT_DIR)/bgpfscli.c -o $(BGP_OBJ_DIR)/bgpfscli.o

endif

############################## TRIE #########################################
$(BGP_OBJ_DIR)/bgtrieapif.o : $(BGP_TRIESRC_DIR)/bgtrieapif.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_TRIESRC_DIR)/bgtrieapif.c -o $(BGP_OBJ_DIR)/bgtrieapif.o

$(BGP_OBJ_DIR)/bgtriemem.o : $(BGP_TRIESRC_DIR)/bgtriemem.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_TRIESRC_DIR)/bgtriemem.c -o $(BGP_OBJ_DIR)/bgtriemem.o

$(BGP_OBJ_DIR)/bgtrieutil.o : $(BGP_TRIESRC_DIR)/bgtrieutil.c $(BGP_DEPENDENCIES)
	$(CC) $(C_FLAGS) $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES) $(PROJECT_COMPILATION_SWITCHES) $(BGP_TRIESRC_DIR)/bgtrieutil.c -o $(BGP_OBJ_DIR)/bgtrieutil.o

clean :
	@echo "**************************"
	@echo "cleaning BGP ............ "
	rm `find . -name "*.o"`
	@echo "DONE"
	@echo "**************************"
