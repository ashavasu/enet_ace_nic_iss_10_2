# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : MAKE.H                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       :                                               |
# |                                                                          |
# |   MAKE TOOL{S} USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX { Slackware 1.2.1 }                     |
# |                                                                          |
# |   DATE                   :                                               |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  | 
# |                1} Clean Options                             | 
# |                             2} Compilation Switches and AGENTX Directory |
# |                                Structures                                |
# +--------------------------------------------------------------------------+
#
#     CHANGE RECORD :
# +--------------------------------------------------------------------------+
# | VERSION | AUTHOR/    | DESCRIPTION OF CHANGE                             |
# |         | DATE       |                                                   |
# +---------|------------|---------------------------------------------------+
# |   1     |            | Creation of makefile                              |
# |         |            |                                                   |
# |         | 19/10/1999 |                                                   |
# +--------------------------------------------------------------------------+

#######################################################################
####     Common Directory Structure                                 ###
#######################################################################

include ../LR/make.h
include ../LR/make.rule

PSS_CONFIG=C:/ISIPPC/pssppc.250/configs/std
BGP_BASE_DIR     = ${BASE_DIR}/bgp

BGP_SRC_DIR      = ${BGP_BASE_DIR}/src
BGP_PORT_DIR     = ${BGP_BASE_DIR}/port
BGP_INC_DIR      = ${BGP_BASE_DIR}/inc
BGP_OBJ_DIR      = ${BGP_BASE_DIR}/obj
BGP_CHSRC_DIR    = ${BGP_SRC_DIR}/ch
BGP_RIBSRC_DIR   = ${BGP_SRC_DIR}/rib
BGP_RFLSRC_DIR   = ${BGP_SRC_DIR}/rfl
BGP_RFDSRC_DIR   = ${BGP_SRC_DIR}/rfd
BGP_COMSRC_DIR   = ${BGP_SRC_DIR}/comm
BGP_ECOMSRC_DIR  = ${BGP_SRC_DIR}/ecomm
BGP_CAPSSRC_DIR  = ${BGP_SRC_DIR}/caps
BGP_RTREFSRC_DIR = ${BGP_SRC_DIR}/rtref
BGP_MPESRC_DIR   = ${BGP_SRC_DIR}/mpe
BGP_CONFED_DIR   = ${BGP_SRC_DIR}/confed


BGP_SNMP_DIR     = ${BASE_DIR}/snmp

BGP_MDL_DIR      = ${BGP_SNMP_DIR}/mdl
BGP_MDL_INC_DIR  = ${BGP_MDL_DIR}/inc

GLOBAL_INCLUDES  = ${COMMON_INCLUDE_DIRS}

BGP_INCLUDE_FILES=               \
    ${BGP_INC_DIR}/bgp4com.h     \
    ${BGP_INC_DIR}/bgp4dfs.h     \
    ${BGP_INC_DIR}/bgp4tdfs.h    \
    ${BGP_INC_DIR}/bgp4rib.h     \
    ${BGP_INC_DIR}/bgp4rts.h     \
    ${BGP_INC_DIR}/bgp4semh.h    \
    ${BGP_INC_DIR}/bgp4task.h    \
    ${BGP_INC_DIR}/bgp4var.h     \
    ${BGP_INC_DIR}/bgp4mac.h     \
    ${BGP_PORT_DIR}/bgp4port.h    \
    ${BGP_INC_DIR}/fsbgplow.h     \
    ${BGP_INC_DIR}/stdbglow.h  \
    ${BGP_INC_DIR}/bgrfl.h      \
    ${BGP_INC_DIR}/bgrfd.h     \
    ${BGP_INC_DIR}/bgcm.h      \
    ${BGP_INC_DIR}/bgecm.h     \
    ${BGP_INC_DIR}/bgcap.h     \
    ${BGP_INC_DIR}/bgrtref.h     

BGP_MAKEFILE_DEP=                \
        ${BGP_BASE_DIR}/make.h   \
    ${BGP_BASE_DIR}/Makefile

BGP_DEPENDENCIES = ${BGP_INCLUDE_FILES} ${BGP_MAKEFILE_DEP} ${COMMON_DEPENDENCIES}

ifeq (${CLI}, YES)
BGP_CLI_INCLUDE_FILES=         \
    $(BGP_INC_DIR)/fsbgpcli.h   \
    $(CLI_INCL_DIR)/bgp4cli.h

BGP_CLI_DEPENDENCIES = $(BGP_DEPENDENCIES) $(BGP_CLI_INCLUDE_FILES)
endif

PROJECT_COMPILATION_SWITCHES = -DNO_CAPABILITY_MATCH_REPEERING \
                               -DRFD_WANTED
