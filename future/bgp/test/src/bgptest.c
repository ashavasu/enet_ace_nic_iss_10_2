/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: bgptest.c,v 1.7 2014/10/14 12:13:16 siva Exp $
 **
 ** Description: BGP  UT Test cases file.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/

#include "bgptest.h"
#include "iss.h"
#include "params.h"
#include "bgp.h"
#include "../../../routemap/inc/rmapinc.h"
#include "../../../routemap/inc/rmapcliu.h"
#include "../../../bfd/inc/bfdinc.h"
#include "../../../rm/inc/rmmacs.h"
#include "../../../rm/inc/rmtdfs.h"
#include "../../../rm/inc/rmglob.h"
#include "srmmemi.h"

#include "../../../mpls/l2vpn/inc/l2vpnsz.h"
#include "../../../mpls/l2vpn/inc/l2vpmacs.h"

VOID                AllocMem (tMemPoolId PoolId);
VOID                ReleaseMem (tMemPoolId PoolId);
static UINT1       *gu1Tmp[500];
extern tMemFreePoolRec *pMemFreePoolRecList;
extern UINT4        gu4RMapGlobalDbg;
extern tRMapGlobalInfo gRMapGlobalInfo;
static UINT4        u4Count = 0;

tIp6Addr           *CliStrToIp6Addr (UINT1 *pu1Str);

VOID
BgpExecuteUtCase (UINT4 u4FileNumber, UINT4 u4TestNumber)
{
    INT4                i4RetVal = -1;

    switch (u4FileNumber)
    {
            /* bgpfstcp */
        case 1:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = BgpUtBgpfstcp_1 ();
                    break;
                case 2:
                    i4RetVal = BgpUtBgpfstcp_2 ();
                    break;
                case 3:
                    i4RetVal = BgpUtBgpfstcp_3 ();
                    break;
                case 4:
                    i4RetVal = BgpUtBgpfstcp_4 ();
                    break;
                case 5:
                    i4RetVal = BgpUtBgpfstcp_5 ();
                    break;
                case 6:
                    i4RetVal = BgpUtBgpfstcp_6 ();
                    break;
                case 7:
                    i4RetVal = BgpUtBgpfstcp_7 ();
                    break;
                case 8:
                    i4RetVal = BgpUtBgpfstcp_8 ();
                    break;
                case 9:
                    i4RetVal = BgpUtBgpfstcp_9 ();
                    break;
                case 10:
                    i4RetVal = BgpUtBgpfstcp_10 ();
                    break;
                case 11:
                    i4RetVal = BgpUtBgpfstcp_11 ();
                    break;
                case 12:
                    i4RetVal = BgpUtBgpfstcp_12 ();
                    break;
                case 13:
                    i4RetVal = BgpUtBgpfstcp_13 ();
                    break;
                case 14:
                    i4RetVal = BgpUtBgpfstcp_14 ();
                    break;
                case 15:
                    i4RetVal = BgpUtBgpfstcp_15 ();
                    break;
                default:
                    printf ("Invalid Test for file bgpfstcp.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of bgpfstcp.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of bgpfstcp.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* file2 */
        case 2:
            switch (u4TestNumber)
            {
                case 1:
                    /*BGP_SHA_UT_1 */
                    i4RetVal = BgpUtFsSha_1 ();
                    break;
                case 2:
                    /*BGP_SHA_UT_2 */
                    i4RetVal = BgpUtFsSha_2 ();
                    break;
                case 3:
                    /*BGP_SHA_UT_3 */
                    i4RetVal = BgpUtFsSha_3 ();
                    break;
                case 4:
                    /*BGP_SHA_UT_4 */
                    i4RetVal = BgpUtFsSha_4 ();
                    break;
                case 5:
                    /*BGP_SHA_UT_5 */
                    i4RetVal = BgpUtFsSha_5 ();
                    break;
                case 6:
                    /*BGP_SHA_UT_6 */
                    i4RetVal = BgpUtFsSha_6 ();
                    break;
                case 7:
                    /*BGP_SHA_UT_7 */
                    i4RetVal = BgpUtFsSha_7 ();
                    break;
                case 8:
                    /*BGP_SHA_UT_8 */
                    i4RetVal = BgpUtFsSha_8 ();
                    break;
                case 9:
                    /*BGP_SHA_UT_9 */
                    i4RetVal = BgpUtFsSha_9 ();
                    break;
                case 10:
                    /*BGP_SHA_UT_10 */
                    i4RetVal = BgpUtFsSha_10 ();
                    break;
                case 11:
                    i4RetVal = BgpUtFsSha_11 ();
                    break;
                case 12:
                    i4RetVal = BgpUtFsSha_12 ();
                    break;
                case 13:
                    i4RetVal = BgpUtFsSha_13 ();
                    break;
                case 14:
                    i4RetVal = BgpUtFsSha_14 ();
                    break;
                case 15:
                    i4RetVal = BgpUtFsSha_15 ();
                    break;
                case 16:
                    i4RetVal = BgpUtFsSha_16 ();
                    break;
                case 17:
                    i4RetVal = BgpUtFsSha_17 ();
                    break;
                case 18:
                    i4RetVal = BgpUtFsSha_18 ();
                    break;
                case 19:
                    i4RetVal = BgpUtFsSha_19 ();
                    break;
                case 20:
                    i4RetVal = BgpUtFsSha_20 ();
                    break;
                case 21:
                    i4RetVal = BgpUtFsSha_21 ();
                    break;
                case 22:
                    i4RetVal = BgpUtFsSha_22 ();
                    break;
                case 23:
                    i4RetVal = BgpUtFsSha_23 ();
                    break;
                case 24:
                    i4RetVal = BgpUtFsSha_24 ();
                    break;
                case 25:
                    i4RetVal = BgpUtFsSha_25 ();
                    break;
                case 26:
                    i4RetVal = BgpUtFsSha_26 ();
                    break;
                case 27:
                    i4RetVal = BgpUtFsSha_27 ();
                    break;
                case 28:
                    i4RetVal = BgpUtFsSha_28 ();
                    break;
                case 29:
                    i4RetVal = BgpUtFsSha_29 ();
                    break;
                case 30:
                    i4RetVal = BgpUtFsSha_30 ();
                    break;
                case 31:
                    i4RetVal = BgpUtFsSha_31 ();
                    break;
                case 32:
                    i4RetVal = BgpUtFsSha_32 ();
                    break;
                case 33:
                    i4RetVal = BgpUtFsSha_33 ();
                    break;
                case 34:
                    i4RetVal = BgpUtFsSha_34 ();
                    break;
                case 35:
                    i4RetVal = BgpUtFsSha_35 ();
                    break;
                case 36:
                    i4RetVal = BgpUtFsSha_36 ();
                    break;
                case 37:
                    i4RetVal = BgpUtFsSha_37 ();
                    break;
                case 38:
                    i4RetVal = BgpUtFsSha_38 ();
                    break;
                case 39:
                    i4RetVal = BgpUtFsSha_39 ();
                    break;
                case 40:
                    i4RetVal = BgpUtFsSha_40 ();
                    break;
                case 41:
                    i4RetVal = BgpUtFsSha_41 ();
                    break;

                case 42:
                    i4RetVal = BgpUtFsSha_42 ();
                    break;
                case 43:
                    i4RetVal = BgpUtFsSha_43 ();
                    break;
                case 44:
                    i4RetVal = BgpUtFsSha_44 ();
                    break;
                case 45:
                    i4RetVal = BgpUtFsSha_45 ();
                    break;
                case 46:
                    i4RetVal = BgpUtFsSha_46 ();
                    break;
                case 47:
                    i4RetVal = BgpUtFsSha_47 ();
                    break;
                case 48:
                    i4RetVal = BgpUtFsSha_48 ();
                    break;
                case 49:
                    i4RetVal = BgpUtFsSha_49 ();
                    break;
                case 50:
                    i4RetVal = BgpUtFsSha_50 ();
                    break;
                case 51:
                    i4RetVal = BgpUtFsSha_51 ();
                    break;
                case 52:
                    i4RetVal = BgpUtFsSha_52 ();
                    break;
                case 53:
                    i4RetVal = BgpUtFsSha_53 ();
                    break;
                case 54:
                    i4RetVal = -1;
                    break;

                case 55:
                    i4RetVal = -1;
                    break;
                case 56:
                    i4RetVal = -1;
                    break;
                case 57:
                    i4RetVal = BgpUtFsSha_57 ();
                    break;
                case 58:
                    i4RetVal = BgpUtFsSha_58 ();
                    break;
                case 59:
                    i4RetVal = -1;
                    break;
                case 60:
                    i4RetVal = -1;
                    break;

                case 61:
                    i4RetVal = -1;
                    break;
                case 62:
                    i4RetVal = BgpUtFsSha_62 ();
                    break;
                case 63:
                    i4RetVal = BgpUtFsSha_63 ();
                    break;
                case 64:
                    i4RetVal = -1;
                    break;
                case 65:
                    i4RetVal = -1;
                    break;
                case 66:
                    i4RetVal = -1;
                    break;
                case 67:
                    i4RetVal = -1;
                    break;
                case 68:
                    i4RetVal = -1;
                    break;
                case 69:
                    i4RetVal = -1;
                    break;
                case 70:
                    i4RetVal = -1;
                    break;

                case 71:
                    i4RetVal = -1;
                    break;
                case 72:
                    i4RetVal = -1;
                    break;
                case 73:
                    i4RetVal = -1;
                    break;
                case 74:
                    i4RetVal = -1;
                    break;
                case 75:
                    i4RetVal = -1;
                    break;
                case 76:
                    i4RetVal = -1;
                    break;
                case 77:
                    i4RetVal = -1;
                    break;
                case 78:
                    i4RetVal = -1;
                    break;
                case 79:
                    i4RetVal = -1;
                    break;
                case 80:
                    i4RetVal = -1;
                    break;

                case 81:
                    i4RetVal = -1;
                    break;
                case 82:
                    i4RetVal = -1;
                    break;
                case 83:
                    i4RetVal = -1;
                    break;
                case 84:
                    i4RetVal = -1;
                    break;
                case 85:
                    i4RetVal = -1;
                    break;
                case 86:
                    i4RetVal = -1;
                    break;
                case 87:
                    i4RetVal = -1;
                    break;
                case 88:
                    i4RetVal = -1;
                    break;
                case 89:
                    i4RetVal = -1;
                    break;
                case 90:
                    i4RetVal = -1;
                    break;
                case 91:
                    i4RetVal = -1;
                    break;
                case 92:
                    i4RetVal = BgpUtFsSha_92 ();
                    break;
                case 93:
                    i4RetVal = BgpUtFsSha_93 ();
                    break;
                case 94:
                    i4RetVal = BgpUtFsSha_94 ();
                    break;
                case 95:
                    i4RetVal = BgpUtFsSha_95 ();
                    break;
                case 96:
                    i4RetVal = BgpUtFsSha_96 ();
                    break;
                case 97:
                    i4RetVal = BgpUtFsSha_97 ();
                    break;
                case 98:
                    i4RetVal = BgpUtFsSha_98 ();
                    break;
                case 99:
                    i4RetVal = BgpUtFsSha_99 ();
                    break;
                case 100:
                    i4RetVal = BgpUtFsSha_100 ();
                    break;
                case 101:
                    i4RetVal = BgpUtFsSha_101 ();
                    break;
                case 102:
                    i4RetVal = -1;
                    break;
                case 103:
                    i4RetVal = -1;
                    break;
                case 104:
                    i4RetVal = -1;
                    break;
                case 105:
                    i4RetVal = BgpUtFsSha_105 ();
                    break;
                case 106:
                    i4RetVal = BgpUtFsSha_106 ();
                    break;
                case 107:
                    i4RetVal = BgpUtFsSha_107 ();
                    break;
                case 108:
                    i4RetVal = BgpUtFsSha_108 ();
                    break;
                default:
                    printf ("Invalid Test for file bgpfile2.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("\r\nUnit Test Case: %d of bgpfssha is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("\r\nUnit Test Case: %d of bgpfssha is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* bgpfourbyteasn */
        case 3:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = BgpUt4ByteAsn_1 ();
                    break;
                case 2:
                    i4RetVal = BgpUt4ByteAsn_2 ();
                    break;
                case 3:
                    i4RetVal = BgpUt4ByteAsn_3 ();
                    break;
                case 4:
                    i4RetVal = BgpUt4ByteAsn_4 ();
                    break;
                case 5:
                    i4RetVal = BgpUt4ByteAsn_5 ();
                    break;
                case 6:
                    i4RetVal = BgpUt4ByteAsn_6 ();
                    break;
                case 7:
                    i4RetVal = BgpUt4ByteAsn_7 ();
                    break;
                case 8:
                    i4RetVal = BgpUt4ByteAsn_8 ();
                    break;
                case 9:
                    i4RetVal = BgpUt4ByteAsn_9 ();
                    break;
                case 10:
                    i4RetVal = BgpUt4ByteAsn_10 ();
                    break;
                case 11:
                    i4RetVal = BgpUt4ByteAsn_11 ();
                    break;
                case 12:
                    i4RetVal = BgpUt4ByteAsn_12 ();
                    break;
                default:
                    printf ("Invalid Test for BGP_4_BYTE_ASN \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of BGP_4_BYTE_ASN is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of BGP_4_BYTE_ASN is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* bgporf */
        case 4:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = BgpUtOrf_1 ();
                    break;
                case 2:
                    i4RetVal = BgpUtOrf_2 ();
                    break;
                case 3:
                    i4RetVal = BgpUtOrf_3 ();
                    break;
                case 4:
                    i4RetVal = BgpUtOrf_4 ();
                    break;
                case 5:
                    i4RetVal = BgpUtOrf_5 ();
                    break;
                case 6:
                    i4RetVal = BgpUtOrf_6 ();
                    break;
                case 7:
                    i4RetVal = BgpUtOrf_7 ();
                    break;
                case 8:
                    i4RetVal = BgpUtOrf_8 ();
                    break;
                case 9:
                    i4RetVal = BgpUtOrf_9 ();
                    break;
                case 10:
                    i4RetVal = BgpUtOrf_10 ();
                    break;
                case 11:
                    i4RetVal = BgpUtOrf_11 ();
                    break;
                case 12:
                    i4RetVal = BgpUtOrf_12 ();
                    break;
                case 13:
                    i4RetVal = BgpUtOrf_13 ();
                    break;
                case 14:
                    i4RetVal = BgpUtOrf_14 ();
                    break;
                case 15:
                    i4RetVal = BgpUtOrf_15 ();
                    break;
                case 16:
                    i4RetVal = BgpUtOrf_16 ();
                    break;
                case 17:
                    i4RetVal = BgpUtOrf_17 ();
                    break;
                case 18:
                    i4RetVal = BgpUtOrf_18 ();
                    break;
                case 19:
                    i4RetVal = BgpUtOrf_19 ();
                    break;
                case 20:
                    i4RetVal = BgpUtOrf_20 ();
                    break;
                case 21:
                    i4RetVal = BgpUtOrf_21 ();
                    break;
                case 22:
                    i4RetVal = BgpUtOrf_22 ();
                    break;
                case 23:
                    i4RetVal = BgpUtOrf_23 ();
                    break;
                case 24:
                    i4RetVal = BgpUtOrf_24 ();
                    break;
                case 25:
                    i4RetVal = BgpUtOrf_25 ();
                    break;
                case 26:
                    i4RetVal = BgpUtOrf_26 ();
                    break;
                case 27:
                    i4RetVal = BgpUtOrf_27 ();
                    break;
                case 28:
                    i4RetVal = BgpUtOrf_28 ();
                    break;
                case 29:
                    i4RetVal = BgpUtOrf_29 ();
                    break;
                case 30:
                    i4RetVal = BgpUtOrf_30 ();
                    break;
#ifdef ROUTEMAP_WANTED
                case 31:
                    i4RetVal = BgpUtOrf_31 ();
                    break;
                case 32:
                    i4RetVal = BgpUtOrf_32 ();
                    break;
                case 33:
                    i4RetVal = BgpUtOrf_33 ();
                    break;
                case 34:
                    i4RetVal = BgpUtOrf_34 ();
                    break;
                case 35:
                    i4RetVal = BgpUtOrf_35 ();
                    break;
                case 36:
                    i4RetVal = BgpUtOrf_36 ();
                    break;
                case 37:
                    i4RetVal = BgpUtOrf_37 ();
                    break;
                case 38:
                    i4RetVal = BgpUtOrf_38 ();
                    break;
                case 39:
                    i4RetVal = BgpUtOrf_39 ();
                    break;
                case 40:
                    i4RetVal = BgpUtOrf_40 ();
                    break;
                case 41:
                    i4RetVal = BgpUtOrf_41 ();
                    break;
#endif
                default:
                    printf ("Invalid Test for file bgporf \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of bgporf is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of bgporf is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* bgpbfd */
        case 5:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = BgpUtBfd_1 ();
                    break;
                case 2:
                    i4RetVal = BgpUtBfd_2 ();
                    break;
                case 3:
                    i4RetVal = BgpUtBfd_3 ();
                    break;
                case 4:
                    i4RetVal = BgpUtBfd_4 ();
                    break;
                case 5:
                    i4RetVal = BgpUtBfd_5 ();
                    break;
                case 6:
                    i4RetVal = BgpUtBfd_6 ();
                    break;
                case 7:
                    i4RetVal = BgpUtBfd_7 ();
                    break;
                case 8:
                    i4RetVal = BgpUtBfd_8 ();
                    break;
                case 9:
                    i4RetVal = BgpUtBfd_9 ();
                    break;
                case 10:
                    i4RetVal = BgpUtBfd_10 ();
                    break;
                case 11:
                    i4RetVal = BgpUtBfd_11 ();
                    break;
                case 12:
                    i4RetVal = BgpUtBfd_12 ();
                    break;
                case 13:
                    i4RetVal = BgpUtBfd_13 ();
                    break;
                case 14:
                    i4RetVal = BgpUtBfd_14 ();
                    break;
                case 15:
                    i4RetVal = BgpUtBfd_15 ();
                    break;
                case 16:
                    i4RetVal = BgpUtBfd_16 ();
                    break;
                case 17:
                    i4RetVal = BgpUtBfd_17 ();
                    break;
                case 18:
                    i4RetVal = BgpUtBfd_18 ();
                    break;
#ifdef BFD_WANTED
                case 19:
                    i4RetVal = BgpUtBfd_19 ();
                    break;
                case 20:
                    i4RetVal = BgpUtBfd_20 ();
                    break;
                case 21:
                    i4RetVal = BgpUtBfd_21 ();
                    break;
#endif
                default:
                    printf ("Invalid Test for bgpbfd \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of bgpbfd is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of bgpbfd is Failed \r\n",
                        u4TestNumber);
            }
            return;
#ifdef VPLSADS_WANTED
            /* file6 */
        case 6:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = BgpUtFileL2vpnVpls6_1 ();
                    break;
                case 2:
                    i4RetVal = BgpUtFileL2vpnVpls6_2 ();
                    break;
                case 3:
                    i4RetVal = BgpUtFileL2vpnVpls6_3 ();
                    break;
                case 4:
                    i4RetVal = BgpUtFileL2vpnVpls6_4 ();
                    break;
                case 5:
                    i4RetVal = BgpUtFileL2vpnVpls6_5 ();
                    break;
                case 6:
                    i4RetVal = BgpUtFileL2vpnVpls6_6 ();
                    break;
                case 7:
                    i4RetVal = BgpUtFileL2vpnVpls6_7 ();
                    break;
                case 8:
                    i4RetVal = BgpUtFileL2vpnVpls6_8 ();
                    break;
                case 9:
                    i4RetVal = BgpUtFileL2vpnVpls6_9 ();
                    break;
                case 10:
                    i4RetVal = BgpUtFileL2vpnVpls6_10 ();
                    break;
                case 11:
                    i4RetVal = BgpUtFileL2vpnVpls6_11 ();
                    break;
                case 12:
                    i4RetVal = BgpUtFileL2vpnVpls6_12 ();
                    break;
                case 13:
                    i4RetVal = BgpUtFileL2vpnVpls6_13 ();
                    break;
                case 14:
                    i4RetVal = BgpUtFileL2vpnVpls6_14 ();
                    break;
                case 15:
                    i4RetVal = BgpUtFileL2vpnVpls6_15 ();
                    break;
                case 16:
                    i4RetVal = BgpUtFileL2vpnVpls6_16 ();
                    break;
                case 17:
                    i4RetVal = BgpUtFileL2vpnVpls6_17 ();
                    break;
                case 18:
                    i4RetVal = BgpUtFileL2vpnVpls6_18 ();
                    break;
                case 19:
                    i4RetVal = BgpUtFileL2vpnVpls6_19 ();
                    break;
                case 20:
                    i4RetVal = BgpUtFileL2vpnVpls6_20 ();
                    break;
                case 21:
                    i4RetVal = BgpUtFileL2vpnVpls6_21 ();
                    break;
                case 22:
                    i4RetVal = BgpUtFileL2vpnVpls6_22 ();
                    break;
                case 23:
                    i4RetVal = BgpUtFileL2vpnVpls6_23 ();
                    break;
                case 24:
                    i4RetVal = BgpUtFileL2vpnVpls6_24 ();
                    break;
                case 25:
                    i4RetVal = BgpUtFileL2vpnVpls6_25 ();
                    break;
                case 26:
                    i4RetVal = BgpUtFileL2vpnVpls6_26 ();
                    break;
                case 27:
                    i4RetVal = BgpUtFileL2vpnVpls6_27 ();
                    break;
                case 28:
                    i4RetVal = BgpUtFileL2vpnVpls6_28 ();
                    break;
                case 29:
                    i4RetVal = BgpUtFileL2vpnVpls6_29 ();
                    break;
                case 30:
                    i4RetVal = BgpUtFileL2vpnVpls6_30 ();
                    break;
                case 31:
                    i4RetVal = BgpUtFileL2vpnVpls6_31 ();
                    break;
                case 32:
                    i4RetVal = BgpUtFileL2vpnVpls6_32 ();
                    break;
                case 33:
                    i4RetVal = BgpUtFileL2vpnVpls6_33 ();
                    break;
                case 34:
                    i4RetVal = BgpUtFileL2vpnVpls6_34 ();
                    break;
                case 35:
                    i4RetVal = BgpUtFileL2vpnVpls6_35 ();
                    break;
                case 36:
                    i4RetVal = BgpUtFileL2vpnVpls6_36 ();
                    break;
                case 37:
                    i4RetVal = BgpUtFileL2vpnVpls6_37 ();
                    break;
                case 38:
                    i4RetVal = BgpUtFileL2vpnVpls6_38 ();
                    break;
                case 39:
                    i4RetVal = BgpUtFileL2vpnVpls6_39 ();
                    break;
                case 40:
                    i4RetVal = BgpUtFileL2vpnVpls6_40 ();
                    break;
                case 41:
                    i4RetVal = BgpUtFileL2vpnVpls6_41 ();
                    break;
                case 42:
                    i4RetVal = BgpUtFileL2vpnVpls6_42 ();
                    break;
                case 43:
                    i4RetVal = BgpUtFileL2vpnVpls6_43 ();
                    break;
                case 44:
                    i4RetVal = BgpUtFileL2vpnVpls6_44 ();
                    break;
                case 45:
                    i4RetVal = BgpUtFileL2vpnVpls6_45 ();
                    break;
                case 46:
                    i4RetVal = BgpUtFileL2vpnVpls6_46 ();
                    break;
                case 47:
                    i4RetVal = BgpUtFileL2vpnVpls6_47 ();
                    break;
                case 48:
                    i4RetVal = BgpUtFileL2vpnVpls6_48 ();
                    break;
                case 49:
                    i4RetVal = BgpUtFileL2vpnVpls6_49 ();
                    break;
                case 50:
                    i4RetVal = BgpUtFileL2vpnVpls6_50 ();
                    break;
                case 51:
                    i4RetVal = BgpUtFileL2vpnVpls6_51 ();
                    break;
                case 52:
                    i4RetVal = BgpUtFileL2vpnVpls6_52 ();
                    break;
                case 53:
                    i4RetVal = BgpUtFileL2vpnVpls6_53 ();
                    break;
                case 54:
                    i4RetVal = BgpUtFileL2vpnVpls6_54 ();
                    break;
                case 55:
                    i4RetVal = BgpUtFileL2vpnVpls6_55 ();
                    break;
                case 56:
                    i4RetVal = BgpUtFileL2vpnVpls6_56 ();
                    break;
                case 57:
                    i4RetVal = BgpUtFileL2vpnVpls6_57 ();
                    break;
                case 58:
                    i4RetVal = BgpUtFileL2vpnVpls6_58 ();
                    break;
                case 59:
                    i4RetVal = BgpUtFileL2vpnVpls6_59 ();
                    break;
                case 60:
                    i4RetVal = BgpUtFileL2vpnVpls6_60 ();
                    break;
                case 61:
                    i4RetVal = BgpUtFileL2vpnVpls6_61 ();
                    break;
                case 62:
                    i4RetVal = BgpUtFileL2vpnVpls6_62 ();
                    break;
                case 63:
                    i4RetVal = BgpUtFileL2vpnVpls6_63 ();
                    break;
                case 64:
                    i4RetVal = BgpUtFileL2vpnVpls6_64 ();
                    break;
                case 65:
                    i4RetVal = BgpUtFileL2vpnVpls6_65 ();
                    break;
                case 66:
                    i4RetVal = BgpUtFileL2vpnVpls6_66 ();
                    break;
                case 67:
                    i4RetVal = BgpUtFileL2vpnVpls6_67 ();
                    break;
                case 68:
                    i4RetVal = BgpUtFileL2vpnVpls6_68 ();
                    break;
                case 69:
                    i4RetVal = BgpUtFileL2vpnVpls6_69 ();
                    break;
                case 70:
                    i4RetVal = BgpUtFileL2vpnVpls6_70 ();
                    break;
                case 71:
                    i4RetVal = BgpUtFileL2vpnVpls6_71 ();
                    break;
                case 72:
                    i4RetVal = BgpUtFileL2vpnVpls6_72 ();
                    break;
                case 73:
                    i4RetVal = BgpUtFileL2vpnVpls6_73 ();
                    break;
                case 74:
                    i4RetVal = BgpUtFileL2vpnVpls6_74 ();
                    break;
                case 75:
                    i4RetVal = BgpUtFileL2vpnVpls6_75 ();
                    break;
                case 76:
                    i4RetVal = BgpUtFileL2vpnVpls6_76 ();
                    break;
                case 77:
                    i4RetVal = BgpUtFileL2vpnVpls6_77 ();
                    break;
                case 78:
                    i4RetVal = BgpUtFileL2vpnVpls6_78 ();
                    break;
                case 79:
                    i4RetVal = BgpUtFileL2vpnVpls6_79 ();
                    break;
                case 80:
                    i4RetVal = BgpUtFileL2vpnVpls6_80 ();
                    break;
                case 81:
                    i4RetVal = BgpUtFileL2vpnVpls6_81 ();
                    break;
                case 82:
                    i4RetVal = BgpUtFileL2vpnVpls6_82 ();
                    break;
                case 83:
                    i4RetVal = BgpUtFileL2vpnVpls6_83 ();
                    break;
                case 84:
                    i4RetVal = BgpUtFileL2vpnVpls6_84 ();
                    break;
                case 85:
                    i4RetVal = BgpUtFileL2vpnVpls6_85 ();
                    break;
                case 86:
                    i4RetVal = BgpUtFileL2vpnVpls6_86 ();
                    break;
                case 87:
                    i4RetVal = BgpUtFileL2vpnVpls6_87 ();
                    break;
                case 88:
                    i4RetVal = BgpUtFileL2vpnVpls6_88 ();
                    break;
                case 89:
                    i4RetVal = BgpUtFileL2vpnVpls6_89 ();
                    break;
                case 90:
                    i4RetVal = BgpUtFileL2vpnVpls6_90 ();
                    break;
                case 91:
                    i4RetVal = BgpUtFileL2vpnVpls6_91 ();
                    break;
                case 92:
                    i4RetVal = BgpUtFileL2vpnVpls6_92 ();
                    break;
                case 93:
                    i4RetVal = BgpUtFileL2vpnVpls6_93 ();
                    break;
                case 94:
                    i4RetVal = BgpUtFileL2vpnVpls6_94 ();
                    break;
                case 95:
                    i4RetVal = BgpUtFileL2vpnVpls6_95 ();
                    break;
                case 96:
                    i4RetVal = BgpUtFileL2vpnVpls6_96 ();
                    break;
                case 97:
                    i4RetVal = BgpUtFileL2vpnVpls6_97 ();
                    break;
                case 98:
                    i4RetVal = BgpUtFileL2vpnVpls6_98 ();
                    break;
                case 99:
                    i4RetVal = BgpUtFileL2vpnVpls6_99 ();
                    break;
                case 100:
                    i4RetVal = BgpUtFileL2vpnVpls6_100 ();
                    break;
                case 101:
                    i4RetVal = BgpUtFileL2vpnVpls6_101 ();
                    break;
                case 102:
                    i4RetVal = BgpUtFileL2vpnVpls6_102 ();
                    break;
                case 103:
                    i4RetVal = BgpUtFileL2vpnVpls6_103 ();
                    break;
                case 104:
                    i4RetVal = BgpUtFileL2vpnVpls6_104 ();
                    break;
                case 105:
                    i4RetVal = BgpUtFileL2vpnVpls6_105 ();
                    break;

                default:
                    printf ("Invalid Test for file l2vpn-vpls \r\n");
                    return;
            }
            if (i4RetVal == BGP4_SUCCESS)
            {
                printf ("Unit Test Case: %d of l2vpn-vpls is Passed \r\n",
                        u4TestNumber);
            }
            else
            {
                printf ("Unit Test Case: %d of l2vpn-vpls is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* file6 */
        case 7:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = BgpUtFileVplsGRandHA7_1 ();
                    break;
                case 2:
                    i4RetVal = BgpUtFileVplsGRandHA7_2 ();
                    break;
                case 3:
                    i4RetVal = BgpUtFileVplsGRandHA7_3 ();
                    break;
                case 4:
                    i4RetVal = BgpUtFileVplsGRandHA7_4 ();
                    break;
                case 5:
                    i4RetVal = BgpUtFileVplsGRandHA7_5 ();
                    break;
                case 6:
                    i4RetVal = BgpUtFileVplsGRandHA7_6 ();
                    break;
                case 7:
                    i4RetVal = BgpUtFileVplsGRandHA7_7 ();
                    break;
                case 8:
                    i4RetVal = BgpUtFileVplsGRandHA7_8 ();
                    break;
                case 9:
                    i4RetVal = BgpUtFileVplsGRandHA7_9 ();
                    break;
                case 10:
                    i4RetVal = BgpUtFileVplsGRandHA7_10 ();
                    break;
                case 11:
                    i4RetVal = BgpUtFileVplsGRandHA7_11 ();
                    break;
                case 12:
                    i4RetVal = BgpUtFileVplsGRandHA7_12 ();
                    break;
                case 13:
                    i4RetVal = BgpUtFileVplsGRandHA7_13 ();
                    break;
                case 14:
                    i4RetVal = BgpUtFileVplsGRandHA7_14 ();
                    break;
                case 15:
                    i4RetVal = BgpUtFileVplsGRandHA7_15 ();
                    break;
                case 16:
                    i4RetVal = BgpUtFileVplsGRandHA7_16 ();
                    break;
                case 17:
                    i4RetVal = BgpUtFileVplsGRandHA7_17 ();
                    break;
                case 18:
                    i4RetVal = BgpUtFileVplsGRandHA7_18 ();
                    break;
                case 19:
                    i4RetVal = BgpUtFileVplsGRandHA7_19 ();
                    break;
                case 20:
                    i4RetVal = BgpUtFileVplsGRandHA7_20 ();
                    break;
                case 21:
                    i4RetVal = BgpUtFileVplsGRandHA7_21 ();
                    break;
                case 22:
                    i4RetVal = BgpUtFileVplsGRandHA7_22 ();
                    break;
                case 23:
                    i4RetVal = BgpUtFileVplsGRandHA7_23 ();
                    break;
                case 24:
                    i4RetVal = BgpUtFileVplsGRandHA7_24 ();
                    break;
                case 25:
                    i4RetVal = BgpUtFileVplsGRandHA7_25 ();
                    break;
                case 26:
                    i4RetVal = BgpUtFileVplsGRandHA7_26 ();
                    break;
                case 27:
                    i4RetVal = BgpUtFileVplsGRandHA7_27 ();
                    break;                             
                case 28:                               
                    i4RetVal = BgpUtFileVplsGRandHA7_28 ();
                    break;                             
                case 29:                               
                    i4RetVal = BgpUtFileVplsGRandHA7_29 ();
                    break;                             
                case 30:                               
                    i4RetVal = BgpUtFileVplsGRandHA7_30 ();
                    break;
                case 31:
                    i4RetVal = BgpUtFileVplsGRandHA7_31 ();
                    break;
                case 32:
                    i4RetVal = BgpUtFileVplsGRandHA7_32 ();
                    break;
                case 33:
                    i4RetVal = BgpUtFileVplsGRandHA7_33 ();
                    break;
                case 34:
                    i4RetVal = BgpUtFileVplsGRandHA7_34 ();
                    break;
                case 35:
                    i4RetVal = BgpUtFileVplsGRandHA7_35 ();
                    break;
                case 36:
                    i4RetVal = BgpUtFileVplsGRandHA7_36 ();
                    break;
                case 37:
                    i4RetVal = BgpUtFileVplsGRandHA7_37 ();
                    break;
                case 38:
                    i4RetVal = BgpUtFileVplsGRandHA7_38 ();
                    break;
                case 39:
                    i4RetVal = BgpUtFileVplsGRandHA7_39 ();
                    break;
                case 40:
                    i4RetVal = BgpUtFileVplsGRandHA7_40 ();
                    break;
                case 41:
                    i4RetVal = BgpUtFileVplsGRandHA7_41 ();
                    break;
                case 42:
                    i4RetVal = BgpUtFileVplsGRandHA7_42 ();
                    break;
                case 43:
                    i4RetVal = BgpUtFileVplsGRandHA7_43 ();
                    break;
                case 44:
                    i4RetVal = BgpUtFileVplsGRandHA7_44 ();
                    break;
                case 45:
                    i4RetVal = BgpUtFileVplsGRandHA7_45 ();
                    break;
                case 46:
                    i4RetVal = BgpUtFileVplsGRandHA7_46 ();
                    break;
                case 47:
                    i4RetVal = BgpUtFileVplsGRandHA7_47 ();
                    break;
                case 48:
                    i4RetVal = BgpUtFileVplsGRandHA7_48 ();
                    break;
                case 49:
                    i4RetVal = BgpUtFileVplsGRandHA7_49 ();
                    break;
                case 50:
                    i4RetVal = BgpUtFileVplsGRandHA7_50 ();
                    break;
                case 51:
                    i4RetVal = BgpUtFileVplsGRandHA7_51 ();
                    break;
                case 52:
                    i4RetVal = BgpUtFileVplsGRandHA7_52 ();
                    break;
                case 53:
                    i4RetVal = BgpUtFileVplsGRandHA7_53 ();
                    break;
                case 54:
                    i4RetVal = BgpUtFileVplsGRandHA7_54 ();
                    break;
                case 55:
                    i4RetVal = BgpUtFileVplsGRandHA7_55 ();
                    break;
                case 56:
                    i4RetVal = BgpUtFileVplsGRandHA7_56 ();
                    break;
                case 57:
                    i4RetVal = BgpUtFileVplsGRandHA7_57 ();
                    break;
                case 58:
                    i4RetVal = BgpUtFileVplsGRandHA7_58 ();
                    break;
                case 59:
                    i4RetVal = BgpUtFileVplsGRandHA7_59 ();
                    break;
                case 60:
                    i4RetVal = BgpUtFileVplsGRandHA7_60 ();
                    break;

                default:
                    printf ("Invalid Test for file VPLS GR HA \r\n");
                    return;
            }
            if (i4RetVal == BGP4_SUCCESS)
            {
                printf ("Unit Test Case: %d of vpls gr-ha is Passed \r\n",
                        u4TestNumber);
            }
            else
            {
                printf ("Unit Test Case: %d of vpls gr-ha is Failed \r\n",
                        u4TestNumber);
            }
            return;
#endif
    }
    return;
}

VOID
BgpExecuteUtAll ()
{
    UINT1               u1File = 0;

    for (u1File = 1; u1File <= FILE_COUNT; u1File++)
    {
        BgpExecuteUtFile (u1File);
    }
}

VOID
BgpExecuteUtFile (UINT4 u4File)
{
    UINT1               u1Case = 0;

    for (u1Case = 1; u1Case <= gTestCase2[u4File - 1].u1Case; u1Case++)
    {
        BgpExecuteUtCase (u4File, u1Case);
    }
}

/* bgpfstcp UT cases */
/* Bgp4TcphMD5AuthOptSet - When called 
 * with valid peer table entry for V4 peer, non-null password string
 * function must return success*/
INT4
BgpUtBgpfstcp_1 (VOID)
{
    tAddrPrefix         PeerAddress;
    tBgp4PeerEntry     *pPeerEntry = NULL;
    INT4                i4Ret = OSIX_SUCCESS;
    UINT1              *password = (UINT1 *) "secret";
    UINT1               u1Keylen = STRLEN (password);

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("as-num 100");
    CliExecuteAppCmd ("router-id 12.0.0.1");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 12.0.0.2 remote-as 100");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    PeerAddress.u2Afi = BGP4_INET_AFI_IPV4;
    PeerAddress.u2AddressLen = 4;
    PeerAddress.au1Address[0] = 2;
    PeerAddress.au1Address[1] = 0;
    PeerAddress.au1Address[2] = 0;
    PeerAddress.au1Address[3] = 12;

    pPeerEntry = Bgp4SnmphGetPeerEntry (0, PeerAddress);
    if (pPeerEntry == NULL)
    {
        printf ("\nUt case 1 for Bgp4TcphMD5AuthOptSet-- pPeerEntry is NULL");
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    if (BGP4_SUCCESS != Bgp4TcphMD5AuthOptSet (pPeerEntry, password, u1Keylen))
    {
        i4Ret = OSIX_FAILURE;
    }

  RETURN:
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    return i4Ret;
}

/* Bgp4TcphMD5AuthOptSet - When called 
 * with valid peer table entry for V6 peer, non-null password string
 * function must return success*/
INT4
BgpUtBgpfstcp_2 (VOID)
{
    tAddrPrefix         PeerAddress;
    tBgp4PeerEntry     *pPeerEntry = NULL;
    INT4                i4Ret = OSIX_SUCCESS;
    UINT1              *password = (UINT1 *) "secret";
    UINT1              *pIp6Addr = (UINT1 *) "1111::22";
    UINT1               u1Keylen = STRLEN (password);

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("in vlan 1");
    CliExecuteAppCmd ("ipv6 enable");
    CliExecuteAppCmd ("ipv6 address 1111::11 64");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("as-num 100");
    CliExecuteAppCmd ("router-id 12.0.0.1");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 1111::22 remote-as 100");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    if (str_to_ip6addr ((UINT1 *) pIp6Addr) != NULL)
    {
        PeerAddress.u2Afi = BGP4_INET_AFI_IPV6;
        MEMCPY (&(PeerAddress.au1Address),
                (tIp6Addr *) str_to_ip6addr ((UINT1 *) pIp6Addr),
                BGP4_IPV6_PREFIX_LEN);
        PeerAddress.u2AddressLen = BGP4_IPV6_PREFIX_LEN;
    }
    else
    {
        printf
            ("\nUt case 2 for Bgp4TcphMD5AuthOptSet-- str_to_ip6addr returned NULL");
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    pPeerEntry = Bgp4SnmphGetPeerEntry (0, PeerAddress);
    if (pPeerEntry == NULL)
    {
        printf ("\nUt case 2 for Bgp4TcphMD5AuthOptSet-- pPeerEntry is NULL");
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    if (BGP4_SUCCESS != Bgp4TcphMD5AuthOptSet (pPeerEntry, password, u1Keylen))
    {
        i4Ret = OSIX_FAILURE;
    }

  RETURN:
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    return i4Ret;
}

/* Bgp4TcphMD5AuthOptSet - When called 
 * with valid peer table entry  and null password string
 * function must return failure and errno must be -53 (SLI_ENOENT)*/
INT4
BgpUtBgpfstcp_3 (VOID)
{
    tAddrPrefix         PeerAddress;
    tBgp4PeerEntry     *pPeerEntry = NULL;
    INT4                i4Ret = OSIX_SUCCESS;
    UINT1              *pIp6Addr = (UINT1 *) "1111::22";

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("in vlan 1");
    CliExecuteAppCmd ("ipv6 enable");
    CliExecuteAppCmd ("ipv6 address 1111::11 64");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("as-num 100");
    CliExecuteAppCmd ("router-id 12.0.0.1");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 1111::22 remote-as 100");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    if (str_to_ip6addr ((UINT1 *) pIp6Addr) != NULL)
    {
        PeerAddress.u2Afi = BGP4_INET_AFI_IPV6;
        MEMCPY (&(PeerAddress.au1Address),
                (tIp6Addr *) str_to_ip6addr ((UINT1 *) pIp6Addr),
                BGP4_IPV6_PREFIX_LEN);
        PeerAddress.u2AddressLen = BGP4_IPV6_PREFIX_LEN;
    }
    else
    {
        printf
            ("\nUt case 3 for Bgp4TcphMD5AuthOptSet-- str_to_ip6addr returned NULL");
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    pPeerEntry = Bgp4SnmphGetPeerEntry (0, PeerAddress);
    if (pPeerEntry == NULL)
    {
        printf ("\nUt case 3 for Bgp4TcphMD5AuthOptSet-- pPeerEntry is NULL");
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    if (BGP4_SUCCESS != Bgp4TcphMD5AuthOptSet (pPeerEntry, NULL, 100))
    {
        if (errno != (-53))
        {
            i4Ret = OSIX_FAILURE;
        }
    }

  RETURN:
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    return i4Ret;
}

/* Bgp4TcphMD5AuthOptSet - When called 
 * with valid peer table entry but unspported address family
 * function must return failure*/
INT4
BgpUtBgpfstcp_4 (VOID)
{
    tUtlInAddr          InAddr;
    tAddrPrefix         PeerAddress;
    tBgp4PeerEntry     *pPeerEntry = NULL;
    INT4                i4Ret = OSIX_SUCCESS;
    UINT1              *pIp6Addr = (UINT1 *) "1111::22";

    UNUSED_PARAM (InAddr);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("in vlan 1");
    CliExecuteAppCmd ("ipv6 enable");
    CliExecuteAppCmd ("ipv6 address 1111::11 64");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("as-num 100");
    CliExecuteAppCmd ("router-id 12.0.0.1");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 1111::22 remote-as 100");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    if (str_to_ip6addr ((UINT1 *) pIp6Addr) != NULL)
    {
        PeerAddress.u2Afi = BGP4_INET_AFI_IPV6;
        MEMCPY (&(PeerAddress.au1Address),
                (tIp6Addr *) str_to_ip6addr ((UINT1 *) pIp6Addr),
                BGP4_IPV6_PREFIX_LEN);
        PeerAddress.u2AddressLen = BGP4_IPV6_PREFIX_LEN;
    }
    else
    {
        printf
            ("\nUt case 4 for Bgp4TcphMD5AuthOptSet-- str_to_ip6addr returned NULL");
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    pPeerEntry = Bgp4SnmphGetPeerEntry (0, PeerAddress);
    if (pPeerEntry == NULL)
    {
        printf ("\nUt case 4 for Bgp4TcphMD5AuthOptSet-- pPeerEntry is NULL");
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /* unsupported address family */
    BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)) = 3;

    if (BGP4_FAILURE != Bgp4TcphMD5AuthOptSet (pPeerEntry, NULL, 100))
    {
        i4Ret = OSIX_FAILURE;
    }

  RETURN:
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    return i4Ret;
}

/* Bgp4TcphMD5AuthOptSet - When called 
 * with NULL peer entry function must return failure*/
INT4
BgpUtBgpfstcp_5 (VOID)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    INT4                i4Ret = OSIX_SUCCESS;

    if (BGP4_FAILURE != Bgp4TcphMD5AuthOptSet (pPeerEntry, NULL, 100))
    {
        i4Ret = OSIX_FAILURE;
    }
    return i4Ret;
}

INT4
BgpUtBgpfstcp_6 (VOID)
{
    return -1;
}

INT4
BgpUtBgpfstcp_7 (VOID)
{
    return -1;
}

INT4
BgpUtBgpfstcp_8 (VOID)
{
    return -1;
}

INT4
BgpUtBgpfstcp_9 (VOID)
{
    return -1;
}

INT4
BgpUtBgpfstcp_10 (VOID)
{
    return -1;
}

INT4
BgpUtBgpfstcp_11 (VOID)
{
    return -1;
}

INT4
BgpUtBgpfstcp_12 (VOID)
{
    return -1;
}

INT4
BgpUtBgpfstcp_13 (VOID)
{
    return -1;
}

INT4
BgpUtBgpfstcp_14 (VOID)
{
    return -1;
}

INT4
BgpUtBgpfstcp_15 (VOID)
{
    return -1;
}

/* bgpfsSha UT cases */

INT4
BgpUtIPAddressConvert (UINT1 *pau1IpAddress, tSNMP_OCTET_STRING_TYPE * pOctet)
{

    tUtlInAddr          InAddr;
    UINT4               u4IpAddress;
    tAddrPrefix         PeerAddrPrefix;

    BGP4_INET_ATON (pau1IpAddress, &InAddr);
    u4IpAddress = InAddr.u4Addr;
    MEMCPY ((PeerAddrPrefix.au1Address), &u4IpAddress, sizeof (UINT4));
    PeerAddrPrefix.u2AddressLen = sizeof (UINT4);

    PeerAddrPrefix.u2Afi = BGP4_INET_AFI_IPV4;
    MEMCPY (pOctet->pu1_OctetList, PeerAddrPrefix.au1Address,
            PeerAddrPrefix.u2AddressLen);
    pOctet->i4_Length = PeerAddrPrefix.u2AddressLen;

    return CLI_SUCCESS;
}

INT4
BgpUtFsSha_1 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    UINT4               u4ErrorCode;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*BgpSetContext(u4VcId); */
    i4RetVal =
        nmhTestv2FsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (&u4ErrorCode, u4VcId,
                                                       BGP4_INET_AFI_IPV4,
                                                       &PeerAddr, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i4RetVal =
        nmhTestv2FsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (&u4ErrorCode, u4VcId,
                                                       BGP4_INET_AFI_IPV4,
                                                       &PeerAddr, 2);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_2 (VOID)
{

    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    UINT4               u4ErrorCode;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BGP not enabled */
/*    BgpSetContext(u4VcId);*/
    /*i4RetVal = nmhTestv2Fsbgp4mpePeerTCPAOAuthNoMKTDiscard(&u4ErrorCode, BGP4_INET_AFI_IPV4, &PeerAddr, 1); */
    i4RetVal =
        nmhTestv2FsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (&u4ErrorCode, u4VcId,
                                                       BGP4_INET_AFI_IPV4,
                                                       &PeerAddr, 1);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    /*Invalid Neighbor */

/*    BgpSetContext(u4VcId);*/
    STRCPY (au1IpAddress, "50.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    /*i4RetVal = nmhTestv2Fsbgp4mpePeerTCPAOAuthNoMKTDiscard(&u4ErrorCode, BGP4_INET_AFI_IPV4, &PeerAddr, 2); */
    i4RetVal =
        nmhTestv2FsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (&u4ErrorCode, u4VcId,
                                                       BGP4_INET_AFI_IPV4,
                                                       &PeerAddr, 2);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */
    STRCPY (au1IpAddress, "50.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    /*i4RetVal = nmhTestv2Fsbgp4mpePeerTCPAOAuthNoMKTDiscard(&u4ErrorCode, 10, &PeerAddr, 2); */
    i4RetVal =
        nmhTestv2FsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (&u4ErrorCode, u4VcId, 10,
                                                       &PeerAddr, 2);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Invalid Value */
    STRCPY (au1IpAddress, "20.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    /*i4RetVal = nmhTestv2Fsbgp4mpePeerTCPAOAuthNoMKTDiscard(&u4ErrorCode, BGP4_INET_AFI_IPV4, &PeerAddr, 8); */
    i4RetVal =
        nmhTestv2FsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (&u4ErrorCode, u4VcId,
                                                       BGP4_INET_AFI_IPV4,
                                                       &PeerAddr, 8);
    if (!((i4RetVal == SNMP_FAILURE) && (u4ErrorCode == SNMP_ERR_WRONG_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Invalid context id */
    i4RetVal =
        nmhTestv2FsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (&u4ErrorCode, 1,
                                                       BGP4_INET_AFI_IPV4,
                                                       &PeerAddr, 8);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_3 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

/*    BgpSetContext(u4VcId);*/
    /*i4RetVal = nmhSetFsbgp4mpePeerTCPAOAuthNoMKTDiscard(BGP4_INET_AFI_IPV4, &PeerAddr, 1); */
    i4RetVal =
        nmhSetFsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (u4VcId, BGP4_INET_AFI_IPV4,
                                                    &PeerAddr, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4mpePeerTCPAOAuthNoMKTDiscard(BGP4_INET_AFI_IPV4, &PeerAddr, 2); */
    i4RetVal =
        nmhSetFsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (u4VcId, BGP4_INET_AFI_IPV4,
                                                    &PeerAddr, 2);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /* BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_4 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*BgpSetContext(u4VcId); */
/*BGP is not enabled AdminStatus down*/
    /*i4RetVal = nmhSetFsbgp4mpePeerTCPAOAuthNoMKTDiscard(BGP4_INET_AFI_IPV4, &PeerAddr, 1); */
    i4RetVal =
        nmhSetFsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (u4VcId, BGP4_INET_AFI_IPV4,
                                                    &PeerAddr, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    /*BgpSetContext(u4VcId); */
    /*i4RetVal = nmhSetFsbgp4mpePeerTCPAOAuthNoMKTDiscard(10, &PeerAddr, 1); */
    i4RetVal =
        nmhSetFsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (u4VcId, 10, &PeerAddr, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    STRCPY (au1IpAddress, "50.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    /*i4RetVal = nmhSetFsbgp4mpePeerTCPAOAuthNoMKTDiscard(BGP4_INET_AFI_IPV4, &PeerAddr, 1); */
    i4RetVal =
        nmhSetFsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (u4VcId, BGP4_INET_AFI_IPV4,
                                                    &PeerAddr, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Invalid Context id */
    i4RetVal =
        nmhSetFsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (1, BGP4_INET_AFI_IPV4,
                                                    &PeerAddr, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_5 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;
    INT4                i4GetVal;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*BgpSetContext(u4VcId); */

    /*Default value */

    /*i4RetVal = nmhGetFsbgp4mpePeerTCPAOAuthNoMKTDiscard(BGP4_INET_AFI_IPV4, &PeerAddr, &i4GetVal); */
    i4RetVal =
        nmhGetFsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (u4VcId, BGP4_INET_AFI_IPV4,
                                                    &PeerAddr, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == 1)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4mpePeerTCPAOAuthNoMKTDiscard(BGP4_INET_AFI_IPV4, &PeerAddr, 1); */
    i4RetVal =
        nmhSetFsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (u4VcId, BGP4_INET_AFI_IPV4,
                                                    &PeerAddr, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhGetFsbgp4mpePeerTCPAOAuthNoMKTDiscard(BGP4_INET_AFI_IPV4, &PeerAddr, &i4GetVal); */
    i4RetVal =
        nmhGetFsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (u4VcId, BGP4_INET_AFI_IPV4,
                                                    &PeerAddr, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == 1)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4mpePeerTCPAOAuthNoMKTDiscard(BGP4_INET_AFI_IPV4, &PeerAddr, 2); */
    i4RetVal =
        nmhSetFsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (u4VcId, BGP4_INET_AFI_IPV4,
                                                    &PeerAddr, 2);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhGetFsbgp4mpePeerTCPAOAuthNoMKTDiscard(BGP4_INET_AFI_IPV4, &PeerAddr, &i4GetVal); */
    i4RetVal =
        nmhGetFsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (u4VcId, BGP4_INET_AFI_IPV4,
                                                    &PeerAddr, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == 2)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_6 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;
    INT4                i4GetVal;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*BgpSetContext(u4VcId); */
/*Admin down case*/
    /*i4RetVal = nmhGetFsbgp4mpePeerTCPAOAuthNoMKTDiscard(BGP4_INET_AFI_IPV4, &PeerAddr, &i4GetVal); */
    i4RetVal =
        nmhGetFsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (u4VcId, BGP4_INET_AFI_IPV4,
                                                    &PeerAddr, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    /*BgpSetContext(u4VcId); */
    /*Invalid Peer */
    STRCPY (au1IpAddress, "50.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    /*i4RetVal = nmhGetFsbgp4mpePeerTCPAOAuthNoMKTDiscard(BGP4_INET_AFI_IPV4, &PeerAddr, &i4GetVal); */
    i4RetVal =
        nmhGetFsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (u4VcId, BGP4_INET_AFI_IPV4,
                                                    &PeerAddr, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4mpePeerTCPAOAuthNoMKTDiscard(10, &PeerAddr, &i4GetVal); */
    i4RetVal =
        nmhGetFsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (u4VcId, 10, &PeerAddr,
                                                    &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Invalid context */
    i4RetVal =
        nmhGetFsMIBgp4mpePeerTCPAOAuthNoMKTDiscard (1, 10, &PeerAddr,
                                                    &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_7 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    UINT4               u4ErrorCode;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*BgpSetContext(u4VcId); */
    /*i4RetVal = nmhTestv2Fsbgp4mpePeerTCPAOAuthICMPAccept(&u4ErrorCode, BGP4_INET_AFI_IPV4, &PeerAddr, 1); */
    i4RetVal =
        nmhTestv2FsMIBgp4mpePeerTCPAOAuthICMPAccept (&u4ErrorCode, u4VcId,
                                                     BGP4_INET_AFI_IPV4,
                                                     &PeerAddr, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhTestv2Fsbgp4mpePeerTCPAOAuthICMPAccept(&u4ErrorCode, BGP4_INET_AFI_IPV4, &PeerAddr, 2); */
    i4RetVal =
        nmhTestv2FsMIBgp4mpePeerTCPAOAuthICMPAccept (&u4ErrorCode, u4VcId,
                                                     BGP4_INET_AFI_IPV4,
                                                     &PeerAddr, 2);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

/*rainy-day case*/
INT4
BgpUtFsSha_8 (VOID)
{

    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    UINT4               u4ErrorCode;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BGP not enabled */
    /*BgpSetContext(u4VcId); */
    /*i4RetVal = nmhTestv2Fsbgp4mpePeerTCPAOAuthICMPAccept(&u4ErrorCode, BGP4_INET_AFI_IPV4, &PeerAddr, 1); */
    i4RetVal =
        nmhTestv2FsMIBgp4mpePeerTCPAOAuthICMPAccept (&u4ErrorCode, u4VcId,
                                                     BGP4_INET_AFI_IPV4,
                                                     &PeerAddr, 1);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    /*Invalid Neighbor */

    /*BgpSetContext(u4VcId); */
    STRCPY (au1IpAddress, "50.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    /*i4RetVal = nmhTestv2Fsbgp4mpePeerTCPAOAuthICMPAccept(&u4ErrorCode, BGP4_INET_AFI_IPV4, &PeerAddr, 2); */
    i4RetVal =
        nmhTestv2FsMIBgp4mpePeerTCPAOAuthICMPAccept (&u4ErrorCode, u4VcId,
                                                     BGP4_INET_AFI_IPV4,
                                                     &PeerAddr, 2);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhTestv2Fsbgp4mpePeerTCPAOAuthICMPAccept(&u4ErrorCode, 10, &PeerAddr, 2); */
    i4RetVal =
        nmhTestv2FsMIBgp4mpePeerTCPAOAuthICMPAccept (&u4ErrorCode, u4VcId, 10,
                                                     &PeerAddr, 2);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Invalid Value */
    STRCPY (au1IpAddress, "20.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    /*i4RetVal = nmhTestv2Fsbgp4mpePeerTCPAOAuthICMPAccept(&u4ErrorCode, BGP4_INET_AFI_IPV4, &PeerAddr, 8); */
    i4RetVal =
        nmhTestv2FsMIBgp4mpePeerTCPAOAuthICMPAccept (&u4ErrorCode, u4VcId,
                                                     BGP4_INET_AFI_IPV4,
                                                     &PeerAddr, 8);
    if (!((i4RetVal == SNMP_FAILURE) && (u4ErrorCode == SNMP_ERR_WRONG_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Invalid context */
    i4RetVal =
        nmhTestv2FsMIBgp4mpePeerTCPAOAuthICMPAccept (&u4ErrorCode, 1,
                                                     BGP4_INET_AFI_IPV4,
                                                     &PeerAddr, 8);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_9 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*BgpSetContext(u4VcId); */
    /*i4RetVal = nmhSetFsbgp4mpePeerTCPAOAuthICMPAccept(BGP4_INET_AFI_IPV4, &PeerAddr, 1); */
    i4RetVal =
        nmhSetFsMIBgp4mpePeerTCPAOAuthICMPAccept (u4VcId, BGP4_INET_AFI_IPV4,
                                                  &PeerAddr, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4mpePeerTCPAOAuthICMPAccept(BGP4_INET_AFI_IPV4, &PeerAddr, 2); */
    i4RetVal =
        nmhSetFsMIBgp4mpePeerTCPAOAuthICMPAccept (u4VcId, BGP4_INET_AFI_IPV4,
                                                  &PeerAddr, 2);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

/*Rainy day case*/
INT4
BgpUtFsSha_10 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*BgpSetContext(u4VcId); */
    /*BGP is not enabled AdminStatus down */
    /*i4RetVal = nmhSetFsbgp4mpePeerTCPAOAuthICMPAccept(BGP4_INET_AFI_IPV4, &PeerAddr, 1); */
    i4RetVal =
        nmhSetFsMIBgp4mpePeerTCPAOAuthICMPAccept (u4VcId, BGP4_INET_AFI_IPV4,
                                                  &PeerAddr, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    /*BgpSetContext(u4VcId); */

    STRCPY (au1IpAddress, "50.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    /*i4RetVal = nmhSetFsbgp4mpePeerTCPAOAuthICMPAccept(BGP4_INET_AFI_IPV4, &PeerAddr, 1); */
    i4RetVal =
        nmhSetFsMIBgp4mpePeerTCPAOAuthICMPAccept (u4VcId, BGP4_INET_AFI_IPV4,
                                                  &PeerAddr, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4mpePeerTCPAOAuthICMPAccept(10, &PeerAddr, 1); */
    i4RetVal =
        nmhSetFsMIBgp4mpePeerTCPAOAuthICMPAccept (u4VcId, 10, &PeerAddr, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Invalid context */
    i4RetVal = nmhSetFsMIBgp4mpePeerTCPAOAuthICMPAccept (1, 10, &PeerAddr, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_11 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;
    INT4                i4GetVal;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*BgpSetContext(u4VcId); */

    /*Check for default value */
    /*i4RetVal = nmhGetFsbgp4mpePeerTCPAOAuthICMPAccept(BGP4_INET_AFI_IPV4, &PeerAddr, &i4GetVal); */
    i4RetVal =
        nmhGetFsMIBgp4mpePeerTCPAOAuthICMPAccept (u4VcId, BGP4_INET_AFI_IPV4,
                                                  &PeerAddr, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == 2)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4mpePeerTCPAOAuthICMPAccept(BGP4_INET_AFI_IPV4, &PeerAddr, 1); */
    i4RetVal =
        nmhSetFsMIBgp4mpePeerTCPAOAuthICMPAccept (u4VcId, BGP4_INET_AFI_IPV4,
                                                  &PeerAddr, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhGetFsbgp4mpePeerTCPAOAuthICMPAccept(BGP4_INET_AFI_IPV4, &PeerAddr, &i4GetVal); */
    i4RetVal =
        nmhGetFsMIBgp4mpePeerTCPAOAuthICMPAccept (u4VcId, BGP4_INET_AFI_IPV4,
                                                  &PeerAddr, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == 1)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4mpePeerTCPAOAuthICMPAccept(BGP4_INET_AFI_IPV4, &PeerAddr, 2); */
    i4RetVal =
        nmhSetFsMIBgp4mpePeerTCPAOAuthICMPAccept (u4VcId, BGP4_INET_AFI_IPV4,
                                                  &PeerAddr, 2);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhGetFsbgp4mpePeerTCPAOAuthICMPAccept(BGP4_INET_AFI_IPV4, &PeerAddr, &i4GetVal); */
    i4RetVal =
        nmhGetFsMIBgp4mpePeerTCPAOAuthICMPAccept (u4VcId, BGP4_INET_AFI_IPV4,
                                                  &PeerAddr, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == 2)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

/*rainy-day case*/
INT4
BgpUtFsSha_12 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;
    INT4                i4GetVal;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*BgpSetContext(u4VcId); */
/*Admin down case*/
    /*i4RetVal = nmhGetFsbgp4mpePeerTCPAOAuthICMPAccept(BGP4_INET_AFI_IPV4, &PeerAddr, &i4GetVal); */
    i4RetVal =
        nmhGetFsMIBgp4mpePeerTCPAOAuthICMPAccept (u4VcId, BGP4_INET_AFI_IPV4,
                                                  &PeerAddr, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    /*BgpSetContext(u4VcId); */
    /*Invalid Peer */
    STRCPY (au1IpAddress, "50.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    /*i4RetVal = nmhGetFsbgp4mpePeerTCPAOAuthICMPAccept(BGP4_INET_AFI_IPV4, &PeerAddr, &i4GetVal); */
    i4RetVal =
        nmhGetFsMIBgp4mpePeerTCPAOAuthICMPAccept (u4VcId, BGP4_INET_AFI_IPV4,
                                                  &PeerAddr, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4mpePeerTCPAOAuthICMPAccept(10, &PeerAddr, &i4GetVal); */
    i4RetVal =
        nmhGetFsMIBgp4mpePeerTCPAOAuthICMPAccept (u4VcId, 10, &PeerAddr,
                                                  &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Invalid context */
    i4RetVal =
        nmhGetFsMIBgp4mpePeerTCPAOAuthICMPAccept (1, 10, &PeerAddr, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_13 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*BgpSetContext(u4VcId); */

    /*i4RetVal =  nmhValidateIndexInstanceFsbgp4TCPMKTAuthTable(10); */
    i4RetVal = nmhValidateIndexInstanceFsMIBgp4TCPMKTAuthTable (u4VcId, 10);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i4RetVal = nmhValidateIndexInstanceFsMIBgp4TCPMKTAuthTable (1, 10);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_14 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 2 receive-key-id 5 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("neighbor 20.0.0.1 tcp-ao mkt 2");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    BgpSetContext (u4VcId);

    i4RetVal = nmhGetFirstIndexFsbgp4TCPMKTAuthTable (&i4KeyId);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 2)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

/*rainy day case*/
INT4
BgpUtFsSha_15 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    BgpSetContext (u4VcId);
    /*Admin down case */
    i4RetVal = nmhGetFirstIndexFsbgp4TCPMKTAuthTable (&i4KeyId);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    BgpSetContext (u4VcId);
    i4RetVal = nmhGetFirstIndexFsbgp4TCPMKTAuthTable (&i4KeyId);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;
}

INT4
BgpUtFsSha_16 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 2 receive-key-id 5 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 1 receive-key-id 5 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 3 receive-key-id 5 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("neighbor 20.0.0.1 tcp-ao mkt 2");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    BgpSetContext (u4VcId);

    i4RetVal = nmhGetFirstIndexFsbgp4TCPMKTAuthTable (&i4KeyId);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 1)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i4RetVal = nmhGetNextIndexFsbgp4TCPMKTAuthTable (i4KeyId, &i4KeyId);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 2)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i4RetVal = nmhGetNextIndexFsbgp4TCPMKTAuthTable (i4KeyId, &i4KeyId);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 3)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i4RetVal = nmhGetNextIndexFsbgp4TCPMKTAuthTable (i4KeyId, &i4KeyId);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i4RetVal = nmhGetNextIndexFsbgp4TCPMKTAuthTable (0, &i4KeyId);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 1)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;
}

/*rainy-day case*/
INT4
BgpUtFsSha_17 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    BgpSetContext (u4VcId);
    /*Admin down case */
    i4RetVal = nmhGetNextIndexFsbgp4TCPMKTAuthTable (i4KeyId, &i4KeyId);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    BgpSetContext (u4VcId);
    i4RetVal = nmhGetNextIndexFsbgp4TCPMKTAuthTable (0, &i4KeyId);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;
}

INT4
BgpUtFsSha_18 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT4               u4ErrorCode;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRecvKeyId(&u4ErrorCode, i4KeyId, 5); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRecvKeyId (&u4ErrorCode, u4VcId, i4KeyId, 5);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

/*Rainy day case*/
INT4
BgpUtFsSha_19 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT4               u4ErrorCode;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    /*BGP admin down */
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRecvKeyId(&u4ErrorCode, i4KeyId, 5); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRecvKeyId (&u4ErrorCode, u4VcId, i4KeyId, 5);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    /*BgpSetContext(u4VcId); */

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRecvKeyId(&u4ErrorCode, i4KeyId, 256); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRecvKeyId (&u4ErrorCode, u4VcId, i4KeyId,
                                              256);
    if (!((i4RetVal == SNMP_FAILURE) && (u4ErrorCode == SNMP_ERR_WRONG_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRecvKeyId(&u4ErrorCode, i4KeyId, -1); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRecvKeyId (&u4ErrorCode, u4VcId, i4KeyId,
                                              -1);
    if (!((i4RetVal == SNMP_FAILURE) && (u4ErrorCode == SNMP_ERR_WRONG_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRecvKeyId(&u4ErrorCode, 256, 1); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRecvKeyId (&u4ErrorCode, u4VcId, 256, 1);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRecvKeyId(&u4ErrorCode, -1, 1); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRecvKeyId (&u4ErrorCode, u4VcId, -1, 1);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRecvKeyId(&u4ErrorCode, 10, 5); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRecvKeyId (&u4ErrorCode, u4VcId, 10, 5);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRecvKeyId(&u4ErrorCode, i4KeyId, 5); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRecvKeyId (&u4ErrorCode, u4VcId, i4KeyId, 5);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey (i4KeyId, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, ACTIVE); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, ACTIVE);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRecvKeyId(&u4ErrorCode, i4KeyId, 15); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRecvKeyId (&u4ErrorCode, u4VcId, i4KeyId,
                                              15);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Invalid context */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRecvKeyId (&u4ErrorCode, 1, i4KeyId, 5);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_20 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRecvKeyId( i4KeyId, 5); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, i4KeyId, 5);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

/*Rainy day*/
INT4
BgpUtFsSha_21 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRecvKeyId(i4KeyId, 5); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, i4KeyId, 5);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    BgpSetContext (u4VcId);
    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRecvKeyId(i4KeyId, 256); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, i4KeyId, 256);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRecvKeyId(i4KeyId, -1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, i4KeyId, -1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRecvKeyId(-1, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, -1, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRecvKeyId(256, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, 256, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRecvKeyId(i4KeyId, 5); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, i4KeyId, 5);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey (i4KeyId, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, ACTIVE); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, ACTIVE);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRecvKeyId( i4KeyId, 10); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, i4KeyId, 10);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRecvKeyId( 10, 10); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, 10, 10);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRecvKeyId (1, 10, 10);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_22 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRecvKeyId( i4KeyId, 5); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, i4KeyId, 5);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRecvKeyId (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, i4KeyId, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == 5)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

/*Rainy day case*/
INT4
BgpUtFsSha_23 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */
    /*Admin down */
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRecvKeyId (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    /*BgpSetContext(u4VcId); */

    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRecvKeyId (256, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, 256, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRecvKeyId (-1, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, -1, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRecvKeyId( i4KeyId, 5); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, i4KeyId, 5);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRecvKeyId (255, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, 255, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRecvKeyId (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Invalid context */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRecvKeyId (1, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_24 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT4               u4ErrorCode;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthMasterKey(&u4ErrorCode,i4KeyId, &PassKey ); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthMasterKey (&u4ErrorCode, u4VcId, i4KeyId,
                                              &PassKey);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

/*rainy day case*/
INT4
BgpUtFsSha_25 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT4               u4ErrorCode;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;
    /*BgpSetContext(u4VcId); */

    /*BGP admin down */
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthMasterKey(&u4ErrorCode, i4KeyId, &PassKey); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthMasterKey (&u4ErrorCode, u4VcId, i4KeyId,
                                              &PassKey);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    /*BgpSetContext(u4VcId); */

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthMasterKey(&u4ErrorCode, -1, &PassKey); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthMasterKey (&u4ErrorCode, u4VcId, -1,
                                              &PassKey);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthMasterKey(&u4ErrorCode, 256, &PassKey); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthMasterKey (&u4ErrorCode, u4VcId, 256,
                                              &PassKey);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    MEMSET (PassKey.pu1_OctetList, '0', 11);
    PassKey.i4_Length = 0;

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthMasterKey(&u4ErrorCode, i4KeyId,&PassKey); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthMasterKey (&u4ErrorCode, u4VcId, i4KeyId,
                                              &PassKey);
    if (!((i4RetVal == SNMP_FAILURE) && (u4ErrorCode == SNMP_ERR_WRONG_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    PassKey.i4_Length = 81;
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthMasterKey(&u4ErrorCode, i4KeyId, &PassKey); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthMasterKey (&u4ErrorCode, u4VcId, i4KeyId,
                                              &PassKey);
    if (!((i4RetVal == SNMP_FAILURE) && (u4ErrorCode == SNMP_ERR_WRONG_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthMasterKey(&u4ErrorCode, i4KeyId, &PassKey); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthMasterKey (&u4ErrorCode, u4VcId, i4KeyId,
                                              &PassKey);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey (i4KeyId, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, ACTIVE); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, ACTIVE);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthMasterKey(&u4ErrorCode, i4KeyId,&PassKey); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthMasterKey (&u4ErrorCode, u4VcId, i4KeyId,
                                              &PassKey);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthMasterKey(&u4ErrorCode, 10, &PassKey); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthMasterKey (&u4ErrorCode, u4VcId, 10,
                                              &PassKey);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Invalid context */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthMasterKey (&u4ErrorCode, 1, 10, &PassKey);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_26 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey( i4KeyId, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_27 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey(i4KeyId, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    BgpSetContext (u4VcId);
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey( -1, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, -1, &PassKey);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey( 256, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, 256, &PassKey);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey( 255, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, 255, &PassKey);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey (i4KeyId, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, ACTIVE); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, ACTIVE);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey( i4KeyId, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey( i4KeyId, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*Invalid context */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (1, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;
}

INT4
BgpUtFsSha_28 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey( i4KeyId, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthMasterKey(i4KeyId, &PassKey); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (!
        ((i4RetVal == SNMP_SUCCESS) && (PassKey.i4_Length == 4)
         && (MEMCMP (PassKey.pu1_OctetList, "****", 4) == 0)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_29 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */
    /*Admin down */
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthMasterKey(i4KeyId, &PassKey); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    /*BgpSetContext(u4VcId); */

    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthMasterKey(256, &PassKey); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, 256, &PassKey);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthMasterKey(-1, &PassKey); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, -1, &PassKey);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey( i4KeyId, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthMasterKey (255, &PassKey); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, 255, &PassKey);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthMasterKey (i4KeyId, &PassKey); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*Invalid context */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthMasterKey (1, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_30 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT4               u4ErrorCode;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /* 1 HMAC-SHA-1 */
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthAlgo(&u4ErrorCode, i4KeyId, 1); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthAlgo (&u4ErrorCode, u4VcId, i4KeyId, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

/*Rainy day case*/
INT4
BgpUtFsSha_31 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT4               u4ErrorCode;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    /*BGP admin down */
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthAlgo(&u4ErrorCode, i4KeyId, 1); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthAlgo (&u4ErrorCode, u4VcId, i4KeyId, 1);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    /*BgpSetContext(u4VcId); */

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthAlgo(&u4ErrorCode, i4KeyId, 2); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthAlgo (&u4ErrorCode, u4VcId, i4KeyId, 2);
    if (!((i4RetVal == SNMP_FAILURE) && (u4ErrorCode == SNMP_ERR_WRONG_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthAlgo(&u4ErrorCode, i4KeyId, -1); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthAlgo (&u4ErrorCode, u4VcId, i4KeyId, -1);
    if (!((i4RetVal == SNMP_FAILURE) && (u4ErrorCode == SNMP_ERR_WRONG_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthAlgo(&u4ErrorCode, -1, 1); */
    i4RetVal = nmhTestv2FsMIBgp4TCPMKTAuthAlgo (&u4ErrorCode, u4VcId, -1, 1);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthAlgo(&u4ErrorCode, 256, 1); */
    i4RetVal = nmhTestv2FsMIBgp4TCPMKTAuthAlgo (&u4ErrorCode, u4VcId, 256, 1);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthAlgo(&u4ErrorCode, i4KeyId, 1); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthAlgo (&u4ErrorCode, u4VcId, i4KeyId, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey (i4KeyId, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, ACTIVE); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, ACTIVE);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /* i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthAlgo(&u4ErrorCode, i4KeyId, 1); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthAlgo (&u4ErrorCode, u4VcId, i4KeyId, 1);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthAlgo(&u4ErrorCode, 255, 1); */
    i4RetVal = nmhTestv2FsMIBgp4TCPMKTAuthAlgo (&u4ErrorCode, u4VcId, 255, 1);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*Invalid context */
    i4RetVal = nmhTestv2FsMIBgp4TCPMKTAuthAlgo (&u4ErrorCode, 1, 255, 1);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_32 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*HMAC-SHA-1 */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthAlgo( i4KeyId, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthAlgo (u4VcId, i4KeyId, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

/*rainy day*/
INT4
BgpUtFsSha_33 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthAlgo(i4KeyId, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthAlgo (u4VcId, i4KeyId, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    /*BgpSetContext(u4VcId); */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthAlgo(-1, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthAlgo (u4VcId, -1, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthAlgo(256, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthAlgo (u4VcId, 256, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthAlgo(255, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthAlgo (u4VcId, 255, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthAlgo(i4KeyId, 2); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthAlgo (u4VcId, i4KeyId, 2);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthAlgo(i4KeyId, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthAlgo (u4VcId, i4KeyId, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey (i4KeyId, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, ACTIVE); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, ACTIVE);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthAlgo( i4KeyId, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthAlgo (u4VcId, i4KeyId, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Invalid context */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthAlgo (1, i4KeyId, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_34 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthAlgo( i4KeyId, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthAlgo (u4VcId, i4KeyId, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthAlgo(i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthAlgo (u4VcId, i4KeyId, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == 1)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

/*Rainy day case*/
INT4
BgpUtFsSha_35 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */
    /*Admin down */
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthAlgo(i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthAlgo (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    /*BgpSetContext(u4VcId); */

    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthAlgo(256, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthAlgo (u4VcId, 256, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthAlgo(-1, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthAlgo (u4VcId, -1, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthAlgo(10, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthAlgo (u4VcId, 10, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthAlgo( i4KeyId, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthAlgo (u4VcId, i4KeyId, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRecvKeyId (255, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, 255, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthAlgo(i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthAlgo (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Invalid context */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthAlgo (1, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;
}

INT4
BgpUtFsSha_36 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT4               u4ErrorCode;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthTcpOptExc(&u4ErrorCode, i4KeyId, 1); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthTcpOptExc (&u4ErrorCode, u4VcId, i4KeyId, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthTcpOptExc(&u4ErrorCode, i4KeyId, 2); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthTcpOptExc (&u4ErrorCode, u4VcId, i4KeyId, 2);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;
}

/*rainy day case*/
INT4
BgpUtFsSha_37 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT4               u4ErrorCode;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    /*BGP admin down */
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthTcpOptExc(&u4ErrorCode, i4KeyId, 1); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthTcpOptExc (&u4ErrorCode, u4VcId, i4KeyId, 1);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    /*BgpSetContext(u4VcId); */

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthTcpOptExc(&u4ErrorCode, i4KeyId, 3); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthTcpOptExc (&u4ErrorCode, u4VcId, i4KeyId, 3);
    if (!((i4RetVal == SNMP_FAILURE) && (u4ErrorCode == SNMP_ERR_WRONG_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthTcpOptExc(&u4ErrorCode, -1, 1); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthTcpOptExc (&u4ErrorCode, u4VcId, -1, 1);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthTcpOptExc(&u4ErrorCode, 256, 3); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthTcpOptExc (&u4ErrorCode, u4VcId, 256, 3);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRecvKeyId(&u4ErrorCode, i4KeyId, 256); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRecvKeyId (&u4ErrorCode, u4VcId, i4KeyId,
                                              256);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthTcpOptExc(&u4ErrorCode, i4KeyId, -1); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthTcpOptExc (&u4ErrorCode, u4VcId, i4KeyId,
                                              -1);
    if (!((i4RetVal == SNMP_FAILURE) && (u4ErrorCode == SNMP_ERR_WRONG_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthTcpOptExc(&u4ErrorCode, i4KeyId, 1); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthTcpOptExc (&u4ErrorCode, u4VcId, i4KeyId, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey (i4KeyId, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, ACTIVE); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, ACTIVE);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthTcpOptExc(&u4ErrorCode, i4KeyId, 1); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthTcpOptExc (&u4ErrorCode, u4VcId, i4KeyId, 1);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthTcpOptExc(&u4ErrorCode, 5, 1); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthTcpOptExc (&u4ErrorCode, u4VcId, 5, 1);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
/*Invalid context*/
    i4RetVal = nmhTestv2FsMIBgp4TCPMKTAuthTcpOptExc (&u4ErrorCode, 1, 5, 1);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;
}

INT4
BgpUtFsSha_38 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthTcpOptExc( i4KeyId, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthTcpOptExc (u4VcId, i4KeyId, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthTcpOptExc( i4KeyId, 2); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthTcpOptExc (u4VcId, i4KeyId, 2);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

/*rainy day case*/
INT4
BgpUtFsSha_39 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthTcpOptExc(i4KeyId, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthTcpOptExc (u4VcId, i4KeyId, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    /*BgpSetContext(u4VcId); */

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthTcpOptExc(-1, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthTcpOptExc (u4VcId, -1, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthTcpOptExc(256, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthTcpOptExc (u4VcId, 256, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthTcpOptExc(255, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthTcpOptExc (u4VcId, 255, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthTcpOptExc(i4KeyId, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthTcpOptExc (u4VcId, i4KeyId, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey (i4KeyId, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, ACTIVE); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, ACTIVE);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthTcpOptExc( i4KeyId, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthTcpOptExc (u4VcId, i4KeyId, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Invalid context */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthTcpOptExc (1, i4KeyId, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_40 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthTcpOptExc( i4KeyId, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthTcpOptExc (u4VcId, i4KeyId, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthTcpOptExc(i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthTcpOptExc (u4VcId, i4KeyId, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == 1)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_41 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */
    /*Admin down */
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthTcpOptExc(i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthTcpOptExc (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    /*BgpSetContext(u4VcId); */

    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthTcpOptExc(256, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthTcpOptExc (u4VcId, 256, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthTcpOptExc(-1, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthTcpOptExc (u4VcId, -1, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthTcpOptExc(10, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthTcpOptExc (u4VcId, 10, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthTcpOptExc( i4KeyId, 2); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthTcpOptExc (u4VcId, i4KeyId, 2);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthTcpOptExc(255, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthTcpOptExc (u4VcId, 255, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthTcpOptExc(i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthTcpOptExc (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*Invalid context */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthTcpOptExc (1, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_42 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;
    UINT4               u4ErrorCode;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
/*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRowStatus (&u4ErrorCode, i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRowStatus (&u4ErrorCode, u4VcId, i4KeyId,
                                              CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRecvKeyId (i4KeyId, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, i4KeyId, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey (i4KeyId, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, ACTIVE); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, ACTIVE);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRowStatus (&u4ErrorCode, i4KeyId, NOT_IN_SERVICE); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRowStatus (&u4ErrorCode, u4VcId, i4KeyId,
                                              NOT_IN_SERVICE);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRowStatus (&u4ErrorCode, i4KeyId, DESTROY); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRowStatus (&u4ErrorCode, u4VcId, i4KeyId,
                                              DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
/*    CliExecuteAppCmd ("configure terminal"); */
/*    CliExecuteAppCmd ("router bgp 100"); */
/*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
/*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;
}

/*Rainy day testing*/
INT4
BgpUtFsSha_43 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;
    UINT4               u4ErrorCode;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 1;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRowStatus (&u4ErrorCode, i4KeyId, 10); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRowStatus (&u4ErrorCode, 1, i4KeyId, 10);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 1 receive-key-id 2 algorithm hmac-sha-1 key abcd");
    CliExecuteAppCmd ("neighbor 20.0.0.1 tcp-ao mkt 1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    /*BgpSetContext(u4VcId); */

    /*Test invalid snmp option */
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRowStatus (&u4ErrorCode, i4KeyId, 10); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRowStatus (&u4ErrorCode, u4VcId, i4KeyId,
                                              10);
    if (!((i4RetVal == SNMP_FAILURE) && (u4ErrorCode == SNMP_ERR_WRONG_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRowStatus (&u4ErrorCode, -1, 5); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRowStatus (&u4ErrorCode, u4VcId, -1, 5);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRowStatus (&u4ErrorCode, 256, 5); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRowStatus (&u4ErrorCode, u4VcId, 256, 5);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*NOT IN SERVICE  / DESTROY must fail in case associated with neighbor */
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRowStatus (&u4ErrorCode, i4KeyId, NOT_IN_SERVICE); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRowStatus (&u4ErrorCode, u4VcId, i4KeyId,
                                              NOT_IN_SERVICE);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRowStatus (&u4ErrorCode, i4KeyId, DESTROY); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRowStatus (&u4ErrorCode, u4VcId, i4KeyId,
                                              DESTROY);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRowStatus (&u4ErrorCode, i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRowStatus (&u4ErrorCode, u4VcId, i4KeyId,
                                              CREATE_AND_WAIT);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRowStatus (&u4ErrorCode, i4KeyId, CREATE_AND_GO); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRowStatus (&u4ErrorCode, u4VcId, i4KeyId,
                                              CREATE_AND_GO);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Test some unknown MKT */
    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRowStatus (&u4ErrorCode, 10, DESTROY); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRowStatus (&u4ErrorCode, u4VcId, 10,
                                              DESTROY);
    if (!((i4RetVal == SNMP_FAILURE) && (u4ErrorCode == SNMP_ERR_NO_CREATION)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhTestv2Fsbgp4TCPMKTAuthRowStatus (&u4ErrorCode, 10, ACTIVE); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRowStatus (&u4ErrorCode, u4VcId, 10, ACTIVE);
    if (!((i4RetVal == SNMP_FAILURE) && (u4ErrorCode == SNMP_ERR_NO_CREATION)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*Invalid context */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPMKTAuthRowStatus (&u4ErrorCode, 1, 10, ACTIVE);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("no tcp-ao mkt key-id 1");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_44 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;
    tSnmpIndexList      SnmpIndexList;
    tSNMP_VAR_BIND      SnmpVarBind;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;
    UINT4               u4ErrorCode;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
/*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpSetContext (u4VcId);

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    i4RetVal =
        nmhDepv2Fsbgp4TCPMKTAuthTable (&u4ErrorCode, &SnmpIndexList,
                                       &SnmpVarBind);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i4RetVal =
        nmhDepv2FsMIBgp4TCPMKTAuthTable (&u4ErrorCode, &SnmpIndexList,
                                         &SnmpVarBind);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
/*    CliExecuteAppCmd ("configure terminal"); */
/*    CliExecuteAppCmd ("router bgp 100"); */
/*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
/*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_45 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*BgpSetContext(u4VcId); */

    i4RetVal =
        nmhValidateIndexInstanceFsMIBgp4TCPAOAuthPeerTable (u4VcId, 1,
                                                            &PeerAddr, 10);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*rainy day case */
    i4RetVal =
        nmhValidateIndexInstanceFsMIBgp4TCPAOAuthPeerTable (1, 1, &PeerAddr,
                                                            10);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_46 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE GetPeerAddr;
    INT4                i4AuthPeerType;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 15.0.0.1 remote-as 200");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 5 receive-key-id 5 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd ("neighbor 20.0.0.1 tcp-ao mkt 5");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    BGP_CLI_MEM_BLK_ALLOC (GetPeerAddr.pu1_OctetList, UINT1);
    if (GetPeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    BgpSetContext (u4VcId);

    i4RetVal =
        nmhGetFirstIndexFsbgp4TCPAOAuthPeerTable (&i4AuthPeerType, &GetPeerAddr,
                                                  &i4KeyId);
    if (!
        ((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 5) && (i4AuthPeerType == 1)
         && (MEMCMP (PeerAddr.pu1_OctetList, GetPeerAddr.pu1_OctetList, 4) ==
             0)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 15.0.0.1 tcp-ao mkt 5");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    STRCPY (au1IpAddress, "15.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    BgpSetContext (u4VcId);
    i4RetVal =
        nmhGetFirstIndexFsbgp4TCPAOAuthPeerTable (&i4AuthPeerType, &GetPeerAddr,
                                                  &i4KeyId);
    if (!
        ((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 5) && (i4AuthPeerType == 1)
         && (MEMCMP (PeerAddr.pu1_OctetList, GetPeerAddr.pu1_OctetList, 4) ==
             0)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    BGP_CLI_MEM_BLK_FREE (GetPeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("no neighbor 15.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_47 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE GetPeerAddr;
    INT4                i4AuthPeerType;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    BGP_CLI_MEM_BLK_ALLOC (GetPeerAddr.pu1_OctetList, UINT1);
    if (GetPeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    BgpSetContext (u4VcId);

    i4RetVal =
        nmhGetFirstIndexFsbgp4TCPAOAuthPeerTable (&i4AuthPeerType, &GetPeerAddr,
                                                  &i4KeyId);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*CliExecuteAppCmd ("neighbor 20.0.0.1 tcp-ao mkt 5"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    BgpSetContext (u4VcId);

    /*no peer */
    i4RetVal =
        nmhGetFirstIndexFsbgp4TCPAOAuthPeerTable (&i4AuthPeerType, &GetPeerAddr,
                                                  &i4KeyId);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 15.0.0.1 remote-as 200");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 5 receive-key-id 5 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    BgpSetContext (u4VcId);

    /*no association of mkt to neighbor */
    i4RetVal =
        nmhGetFirstIndexFsbgp4TCPAOAuthPeerTable (&i4AuthPeerType, &GetPeerAddr,
                                                  &i4KeyId);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    BGP_CLI_MEM_BLK_FREE (GetPeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("no neighbor 15.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_48 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE GetPeerAddr;
    INT4                i4AuthPeerType;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 15.0.0.1 remote-as 200");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 5 receive-key-id 5 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd ("neighbor 20.0.0.1 tcp-ao mkt 5");
    CliExecuteAppCmd ("neighbor 15.0.0.1 tcp-ao mkt 5");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    BGP_CLI_MEM_BLK_ALLOC (GetPeerAddr.pu1_OctetList, UINT1);
    if (GetPeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    STRCPY (au1IpAddress, "15.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    BgpSetContext (u4VcId);
    i4RetVal =
        nmhGetFirstIndexFsbgp4TCPAOAuthPeerTable (&i4AuthPeerType, &GetPeerAddr,
                                                  &i4KeyId);
    if (!
        ((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 5) && (i4AuthPeerType == 1)
         && (MEMCMP (PeerAddr.pu1_OctetList, GetPeerAddr.pu1_OctetList, 4) ==
             0)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i4RetVal =
        nmhGetNextIndexFsbgp4TCPAOAuthPeerTable (i4AuthPeerType,
                                                 &i4AuthPeerType, &PeerAddr,
                                                 &GetPeerAddr, i4KeyId,
                                                 &i4KeyId);
    STRCPY (au1IpAddress, "20.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    if (!
        ((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 5) && (i4AuthPeerType == 1)
         && (MEMCMP (PeerAddr.pu1_OctetList, GetPeerAddr.pu1_OctetList, 4) ==
             0)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    BGP_CLI_MEM_BLK_FREE (GetPeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("no neighbor 15.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_49 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE GetPeerAddr;
    INT4                i4AuthPeerType;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    BGP_CLI_MEM_BLK_ALLOC (GetPeerAddr.pu1_OctetList, UINT1);
    if (GetPeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpSetContext (u4VcId);
    i4RetVal =
        nmhGetNextIndexFsbgp4TCPAOAuthPeerTable (i4AuthPeerType,
                                                 &i4AuthPeerType, &PeerAddr,
                                                 &GetPeerAddr, i4KeyId,
                                                 &i4KeyId);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 15.0.0.1 remote-as 200");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 5 receive-key-id 5 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd ("neighbor 20.0.0.1 tcp-ao mkt 5");
    CliExecuteAppCmd ("neighbor 15.0.0.1 tcp-ao mkt 5");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    STRCPY (au1IpAddress, "15.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    BgpSetContext (u4VcId);
    i4RetVal =
        nmhGetFirstIndexFsbgp4TCPAOAuthPeerTable (&i4AuthPeerType, &GetPeerAddr,
                                                  &i4KeyId);
    if (!
        ((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 5) && (i4AuthPeerType == 1)
         && (MEMCMP (PeerAddr.pu1_OctetList, GetPeerAddr.pu1_OctetList, 4) ==
             0)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i4KeyId = 0;
    i4RetVal =
        nmhGetNextIndexFsbgp4TCPAOAuthPeerTable (i4AuthPeerType,
                                                 &i4AuthPeerType, &PeerAddr,
                                                 &GetPeerAddr, 0, &i4KeyId);
    STRCPY (au1IpAddress, "15.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    if (!
        ((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 5) && (i4AuthPeerType == 1)
         && (MEMCMP (PeerAddr.pu1_OctetList, GetPeerAddr.pu1_OctetList, 4) ==
             0)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i4KeyId = 0;
    i4RetVal =
        nmhGetNextIndexFsbgp4TCPAOAuthPeerTable (i4AuthPeerType,
                                                 &i4AuthPeerType, &PeerAddr,
                                                 &GetPeerAddr, 5, &i4KeyId);
    STRCPY (au1IpAddress, "20.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    if (!
        ((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 5) && (i4AuthPeerType == 1)
         && (MEMCMP (PeerAddr.pu1_OctetList, GetPeerAddr.pu1_OctetList, 4) ==
             0)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i4RetVal =
        nmhGetNextIndexFsbgp4TCPAOAuthPeerTable (i4AuthPeerType,
                                                 &i4AuthPeerType, &PeerAddr,
                                                 &GetPeerAddr, i4KeyId,
                                                 &i4KeyId);
    if (i4RetVal != SNMP_FAILURE)
    {

        i4Ret = OSIX_FAILURE;
        goto RETURN;

    }
    /*remove association of 15.0.0.1 */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 15.0.0.1 tcp-ao mkt 5");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    BgpSetContext (u4VcId);

    STRCPY (au1IpAddress, "15.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    i4RetVal =
        nmhGetNextIndexFsbgp4TCPAOAuthPeerTable (i4AuthPeerType,
                                                 &i4AuthPeerType, &PeerAddr,
                                                 &GetPeerAddr, 5, &i4KeyId);
    STRCPY (au1IpAddress, "20.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    if (!
        ((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 5) && (i4AuthPeerType == 1)
         && (MEMCMP (PeerAddr.pu1_OctetList, GetPeerAddr.pu1_OctetList, 4) ==
             0)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    BGP_CLI_MEM_BLK_FREE (GetPeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("no neighbor 15.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_50 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT4               u4ErrorCode;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 1 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;
    STRCPY (au1IpAddress, "20.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*i4RetVal = nmhTestv2Fsbgp4TCPAOAuthKeyStatus(&u4ErrorCode,1, &PeerAddr, 1, 1);  */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPAOAuthKeyStatus (&u4ErrorCode, u4VcId, 1, &PeerAddr,
                                             1, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_51 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT4               u4ErrorCode;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    STRCPY (au1IpAddress, "20.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    /*BGP admin down */
    /*i4RetVal = nmhTestv2Fsbgp4TCPAOAuthKeyStatus(&u4ErrorCode,1, &PeerAddr, 1, 1);  */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPAOAuthKeyStatus (&u4ErrorCode, u4VcId, 1, &PeerAddr,
                                             1, 1);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    BgpSetContext (u4VcId);

    /*i4RetVal = nmhTestv2Fsbgp4TCPAOAuthKeyStatus(&u4ErrorCode,5, &PeerAddr, 1, 1);  */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPAOAuthKeyStatus (&u4ErrorCode, u4VcId, 5, &PeerAddr,
                                             1, 1);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhTestv2Fsbgp4TCPAOAuthKeyStatus(&u4ErrorCode,1, &PeerAddr, 1, 3);  */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPAOAuthKeyStatus (&u4ErrorCode, u4VcId, 1, &PeerAddr,
                                             1, 3);
    if (!((i4RetVal == SNMP_FAILURE) && (u4ErrorCode == SNMP_ERR_WRONG_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    STRCPY (au1IpAddress, "50.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*i4RetVal = nmhTestv2Fsbgp4TCPAOAuthKeyStatus(&u4ErrorCode,1, &PeerAddr, 1, 1);  */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPAOAuthKeyStatus (&u4ErrorCode, u4VcId, 1, &PeerAddr,
                                             1, 1);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Invalid context */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPAOAuthKeyStatus (&u4ErrorCode, 1, 1, &PeerAddr, 1,
                                             1);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_52 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT4               u4ErrorCode;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 1 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpSetContext (u4VcId);

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;
    STRCPY (au1IpAddress, "20.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*i4RetVal = nmhGetFsbgp4TCPAOAuthKeyStartAccept(1, &PeerAddr, 1, &PeerAddr); */
    i4RetVal =
        nmhGetFsMIBgp4TCPAOAuthKeyStartAccept (u4VcId, 1, &PeerAddr, 1,
                                               &PeerAddr);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i4RetVal =
        nmhGetFsMIBgp4TCPAOAuthKeyStartAccept (1, 1, &PeerAddr, 1, &PeerAddr);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhGetFsbgp4TCPAOAuthKeyStartGenerate(1, &PeerAddr, 1, &PeerAddr); */
    i4RetVal =
        nmhGetFsMIBgp4TCPAOAuthKeyStartGenerate (u4VcId, 1, &PeerAddr, 1,
                                                 &PeerAddr);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i4RetVal =
        nmhGetFsMIBgp4TCPAOAuthKeyStartGenerate (1, 1, &PeerAddr, 1, &PeerAddr);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhGetFsbgp4TCPAOAuthKeyStopAccept(1, &PeerAddr, 1, &PeerAddr); */
    i4RetVal =
        nmhGetFsMIBgp4TCPAOAuthKeyStopAccept (u4VcId, 1, &PeerAddr, 1,
                                              &PeerAddr);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i4RetVal =
        nmhGetFsMIBgp4TCPAOAuthKeyStopAccept (1, 1, &PeerAddr, 1, &PeerAddr);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhGetFsbgp4TCPAOAuthKeyStopGenerate(1, &PeerAddr, 1, &PeerAddr); */
    i4RetVal =
        nmhGetFsMIBgp4TCPAOAuthKeyStopGenerate (u4VcId, 1, &PeerAddr, 1,
                                                &PeerAddr);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i4RetVal =
        nmhGetFsMIBgp4TCPAOAuthKeyStopGenerate (1, 1, &PeerAddr, 1, &PeerAddr);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPAOAuthKeyStartAccept(1, &PeerAddr, 1, &PeerAddr); */
    i4RetVal =
        nmhSetFsMIBgp4TCPAOAuthKeyStartAccept (u4VcId, 1, &PeerAddr, 1,
                                               &PeerAddr);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i4RetVal =
        nmhSetFsMIBgp4TCPAOAuthKeyStartAccept (1, 1, &PeerAddr, 1, &PeerAddr);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPAOAuthKeyStartGenerate(1, &PeerAddr, 1, &PeerAddr); */
    i4RetVal =
        nmhSetFsMIBgp4TCPAOAuthKeyStartGenerate (u4VcId, 1, &PeerAddr, 1,
                                                 &PeerAddr);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i4RetVal =
        nmhSetFsMIBgp4TCPAOAuthKeyStartGenerate (1, 1, &PeerAddr, 1, &PeerAddr);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPAOAuthKeyStopAccept(1, &PeerAddr, 1, &PeerAddr); */
    i4RetVal =
        nmhSetFsMIBgp4TCPAOAuthKeyStopAccept (u4VcId, 1, &PeerAddr, 1,
                                              &PeerAddr);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i4RetVal =
        nmhSetFsMIBgp4TCPAOAuthKeyStopAccept (1, 1, &PeerAddr, 1, &PeerAddr);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPAOAuthKeyStopGenerate(1, &PeerAddr, 1, &PeerAddr); */
    i4RetVal =
        nmhSetFsMIBgp4TCPAOAuthKeyStopGenerate (u4VcId, 1, &PeerAddr, 1,
                                                &PeerAddr);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i4RetVal =
        nmhSetFsMIBgp4TCPAOAuthKeyStopGenerate (1, 1, &PeerAddr, 1, &PeerAddr);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhTestv2Fsbgp4TCPAOAuthKeyStartAccept(&u4ErrorCode, 1, &PeerAddr, 1, &PeerAddr); */
    i4RetVal =
        nmhTestv2FsMIBgp4TCPAOAuthKeyStartAccept (&u4ErrorCode, u4VcId, 1,
                                                  &PeerAddr, 1, &PeerAddr);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i4RetVal =
        nmhTestv2FsMIBgp4TCPAOAuthKeyStartAccept (&u4ErrorCode, 1, 1, &PeerAddr,
                                                  1, &PeerAddr);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

/*i4RetVal = nmhTestv2Fsbgp4TCPAOAuthKeyStartGenerate(&u4ErrorCode, 1, &PeerAddr, 1, &PeerAddr);*/
    i4RetVal =
        nmhTestv2FsMIBgp4TCPAOAuthKeyStartGenerate (&u4ErrorCode, u4VcId, 1,
                                                    &PeerAddr, 1, &PeerAddr);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i4RetVal =
        nmhTestv2FsMIBgp4TCPAOAuthKeyStartGenerate (&u4ErrorCode, 1, 1,
                                                    &PeerAddr, 1, &PeerAddr);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

/*i4RetVal = nmhTestv2Fsbgp4TCPAOAuthKeyStopAccept(&u4ErrorCode, 1, &PeerAddr, 1, &PeerAddr);*/
    i4RetVal =
        nmhTestv2FsMIBgp4TCPAOAuthKeyStopAccept (&u4ErrorCode, u4VcId, 1,
                                                 &PeerAddr, 1, &PeerAddr);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i4RetVal =
        nmhTestv2FsMIBgp4TCPAOAuthKeyStopAccept (&u4ErrorCode, 1, 1, &PeerAddr,
                                                 1, &PeerAddr);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

/*i4RetVal = nmhTestv2Fsbgp4TCPAOAuthKeyStopGenerate(&u4ErrorCode, 1, &PeerAddr, 1, &PeerAddr);*/
    i4RetVal =
        nmhTestv2FsMIBgp4TCPAOAuthKeyStopGenerate (&u4ErrorCode, u4VcId, 1,
                                                   &PeerAddr, 1, &PeerAddr);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i4RetVal =
        nmhTestv2FsMIBgp4TCPAOAuthKeyStopGenerate (&u4ErrorCode, 1, 1,
                                                   &PeerAddr, 1, &PeerAddr);
    if (!
        ((i4RetVal == SNMP_FAILURE)
         && (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_53 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;
    tSnmpIndexList      SnmpIndexList;
    tSNMP_VAR_BIND      SnmpVarBind;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;
    UINT4               u4ErrorCode;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
/*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpSetContext (u4VcId);

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    /*Test CREATE AND WAIT */
    i4RetVal =
        nmhDepv2Fsbgp4TCPAOAuthPeerTable (&u4ErrorCode, &SnmpIndexList,
                                          &SnmpVarBind);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i4RetVal =
        nmhDepv2FsMIBgp4TCPAOAuthPeerTable (&u4ErrorCode, &SnmpIndexList,
                                            &SnmpVarBind);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
/*    CliExecuteAppCmd ("configure terminal"); */
/*    CliExecuteAppCmd ("router bgp 100"); */
/*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
/*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;
}

INT4
BgpUtFsSha_57 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    INT4                i4VcId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 2 receive-key-id 5 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*BgpSetContext(u4VcId); */

    i4RetVal = nmhGetFirstIndexFsMIBgp4TCPMKTAuthTable (&i4VcId, &i4KeyId);
    if (!
        ((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 2)
         && (i4VcId == BGP4_DFLT_VRFID)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no tcp-ao mkt key-id 2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip vrf vrf1");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("address-family ipv4 vrf vrf1");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 5 receive-key-id 5 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    i4RetVal = nmhGetFirstIndexFsMIBgp4TCPMKTAuthTable (&i4VcId, &i4KeyId);
    if (!
        ((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 5)
         && (i4VcId != BGP4_DFLT_VRFID)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("no ip vrf vrf1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_58 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    INT4                i4VcId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 2 receive-key-id 5 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 3 receive-key-id 5 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*BgpSetContext(u4VcId); */

    i4RetVal = nmhGetFirstIndexFsMIBgp4TCPMKTAuthTable (&i4VcId, &i4KeyId);
    if (!
        ((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 2)
         && (i4VcId == BGP4_DFLT_VRFID)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i4RetVal =
        nmhGetNextIndexFsMIBgp4TCPMKTAuthTable (i4VcId, &i4VcId, i4KeyId,
                                                &i4KeyId);
    if (!
        ((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 3)
         && (i4VcId == BGP4_DFLT_VRFID)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip vrf vrf1");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("address-family ipv4 vrf vrf1");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 5 receive-key-id 5 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    i4RetVal =
        nmhGetNextIndexFsMIBgp4TCPMKTAuthTable (i4VcId, &i4VcId, i4KeyId,
                                                &i4KeyId);
    if (!
        ((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 5)
         && (i4VcId != BGP4_DFLT_VRFID)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i4RetVal =
        nmhGetNextIndexFsMIBgp4TCPMKTAuthTable (i4VcId, &i4VcId, i4KeyId,
                                                &i4KeyId);
    if (i4Ret != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i4RetVal =
        nmhGetNextIndexFsMIBgp4TCPMKTAuthTable (1, &i4VcId, i4KeyId, &i4KeyId);
    if (i4Ret != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("no ip vrf vrf1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_62 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    INT4                i4PeerType;
    INT4                i4VcId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE GetPeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 2 receive-key-id 5 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("neighbor 20.0.0.1 tcp-ao mkt 2");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    BGP_CLI_MEM_BLK_ALLOC (GetPeerAddr.pu1_OctetList, UINT1);
    if (GetPeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    STRCPY (au1IpAddress, "20.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*BgpSetContext(u4VcId); */

    i4RetVal =
        nmhGetFirstIndexFsMIBgp4TCPAOAuthPeerTable (&i4VcId, &i4PeerType,
                                                    &GetPeerAddr, &i4KeyId);
    if (!
        ((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 2)
         && (i4VcId == BGP4_DFLT_VRFID)
         && (MEMCMP (PeerAddr.pu1_OctetList, GetPeerAddr.pu1_OctetList, 4) ==
             0)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 tcp-ao mkt 2");
    CliExecuteAppCmd ("no tcp-ao mkt key-id 2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip vrf vrf1");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("address-family ipv4 vrf vrf1");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 5 receive-key-id 5 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd ("neighbor 30.0.0.1 remote-as 200");
    CliExecuteAppCmd ("neighbor 30.0.0.1 tcp-ao mkt 5");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    STRCPY (au1IpAddress, "30.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    i4RetVal =
        nmhGetFirstIndexFsMIBgp4TCPAOAuthPeerTable (&i4VcId, &i4PeerType,
                                                    &GetPeerAddr, &i4KeyId);
    if (!
        ((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 5)
         && (i4VcId != BGP4_DFLT_VRFID)
         && (MEMCMP (PeerAddr.pu1_OctetList, GetPeerAddr.pu1_OctetList, 4) ==
             0)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    BGP_CLI_MEM_BLK_FREE (GetPeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("no ip vrf vrf1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_63 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    INT4                i4PeerType;
    INT4                i4VcId;
    UINT4               u4VcId;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE GetPeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 2 receive-key-id 5 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("neighbor 20.0.0.2 remote-as 200");
    CliExecuteAppCmd ("neighbor 20.0.0.1 tcp-ao mkt 2");
    CliExecuteAppCmd ("neighbor 20.0.0.2 tcp-ao mkt 2");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    BGP_CLI_MEM_BLK_ALLOC (GetPeerAddr.pu1_OctetList, UINT1);
    if (GetPeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    STRCPY (au1IpAddress, "20.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*BgpSetContext(u4VcId); */

    i4RetVal =
        nmhGetFirstIndexFsMIBgp4TCPAOAuthPeerTable (&i4VcId, &i4PeerType,
                                                    &GetPeerAddr, &i4KeyId);
    if (!
        ((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 2)
         && (i4VcId == BGP4_DFLT_VRFID)
         && (MEMCMP (PeerAddr.pu1_OctetList, GetPeerAddr.pu1_OctetList, 4) ==
             0)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i4RetVal =
        nmhGetNextIndexFsMIBgp4TCPAOAuthPeerTable (i4VcId, &i4VcId, i4PeerType,
                                                   &i4PeerType, &PeerAddr,
                                                   &GetPeerAddr, i4KeyId,
                                                   &i4KeyId);

    STRCPY (au1IpAddress, "20.0.0.2");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    if (!
        ((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 2)
         && (i4VcId == BGP4_DFLT_VRFID)
         && (MEMCMP (PeerAddr.pu1_OctetList, GetPeerAddr.pu1_OctetList, 4) ==
             0)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip vrf vrf1");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("address-family ipv4 vrf vrf1");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 5 receive-key-id 5 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd ("neighbor 30.0.0.1 remote-as 200");
    CliExecuteAppCmd ("neighbor 30.0.0.1 tcp-ao mkt 5");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    i4RetVal =
        nmhGetNextIndexFsMIBgp4TCPAOAuthPeerTable (i4VcId, &i4VcId, i4PeerType,
                                                   &i4PeerType, &PeerAddr,
                                                   &GetPeerAddr, i4KeyId,
                                                   &i4KeyId);
    STRCPY (au1IpAddress, "30.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    if (!
        ((i4RetVal == SNMP_SUCCESS) && (i4KeyId == 5)
         && (i4VcId != BGP4_DFLT_VRFID)
         && (MEMCMP (PeerAddr.pu1_OctetList, GetPeerAddr.pu1_OctetList, 4) ==
             0)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i4RetVal =
        nmhGetNextIndexFsMIBgp4TCPAOAuthPeerTable (i4VcId, &i4VcId, i4PeerType,
                                                   &i4PeerType, &PeerAddr,
                                                   &GetPeerAddr, i4KeyId,
                                                   &i4KeyId);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i4RetVal =
        nmhGetNextIndexFsMIBgp4TCPAOAuthPeerTable (1, &i4VcId, i4PeerType,
                                                   &i4PeerType, &PeerAddr,
                                                   &GetPeerAddr, i4KeyId,
                                                   &i4KeyId);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    BGP_CLI_MEM_BLK_FREE (GetPeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("no ip vrf vrf1");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;
}

INT4
BgpUtFsSha_92 (VOID)
{

    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == CREATE_AND_WAIT)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRecvKeyId (i4KeyId, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, i4KeyId, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRecvKeyId(i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, i4KeyId, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == 1)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey (i4KeyId, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, NOT_IN_SERVICE); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, NOT_IN_SERVICE);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == NOT_IN_SERVICE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, ACTIVE); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, ACTIVE);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == ACTIVE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

/*Rainy day case*/
INT4
BgpUtFsSha_93 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*Admin down */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRecvKeyId (i4KeyId, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, i4KeyId, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, ACTIVE); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, ACTIVE);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey (i4KeyId, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (10, ACTIVE); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, 10, ACTIVE);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (10, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, 10, DESTROY);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (10, NOT_IN_SERVICE); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, 10, NOT_IN_SERVICE);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (1, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_94 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_GO);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRecvKeyId (i4KeyId, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, i4KeyId, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey (i4KeyId, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, NOT_IN_SERVICE); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, NOT_IN_SERVICE);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == NOT_IN_SERVICE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, ACTIVE); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, ACTIVE);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == ACTIVE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

/*rainy day case*/
INT4
BgpUtFsSha_95 (VOID)
{
    INT4                i4KeyId;
    UINT4               u4VcId;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    /*BgpSetContext(u4VcId); */

    /*BGP not enabled */
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    /*BgpSetContext(u4VcId); */
    /*MKT not configured */
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*Invalid context */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (1, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_96 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 1 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;
    STRCPY (au1IpAddress, "20.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*i4RetVal = nmhSetFsbgp4TCPAOAuthKeyStatus(1, &PeerAddr, 1, 1);  */
    i4RetVal = nmhSetFsMIBgp4TCPAOAuthKeyStatus (u4VcId, 1, &PeerAddr, 1, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPAOAuthKeyStatus(1, &PeerAddr, 1, 2);  */
    i4RetVal = nmhSetFsMIBgp4TCPAOAuthKeyStatus (u4VcId, 1, &PeerAddr, 1, 2);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

/*rainy day case*/
INT4
BgpUtFsSha_97 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    STRCPY (au1IpAddress, "20.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    /*BGP admin down */
    /*i4RetVal = nmhSetFsbgp4TCPAOAuthKeyStatus(1, &PeerAddr, 1, 1);  */
    i4RetVal = nmhSetFsMIBgp4TCPAOAuthKeyStatus (u4VcId, 1, &PeerAddr, 1, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    BgpSetContext (u4VcId);

    /*i4RetVal = nmhSetFsbgp4TCPAOAuthKeyStatus(5, &PeerAddr, 1, 1);  */
    i4RetVal = nmhSetFsMIBgp4TCPAOAuthKeyStatus (u4VcId, 5, &PeerAddr, 1, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPAOAuthKeyStatus(1, &PeerAddr, 1, 3);  */
    i4RetVal = nmhSetFsMIBgp4TCPAOAuthKeyStatus (u4VcId, 1, &PeerAddr, 1, 3);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    STRCPY (au1IpAddress, "50.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*i4RetVal = nmhSetFsbgp4TCPAOAuthKeyStatus(1, &PeerAddr, 1, 1);  */
    i4RetVal = nmhSetFsMIBgp4TCPAOAuthKeyStatus (u4VcId, 1, &PeerAddr, 1, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*Invalid context */
    i4RetVal = nmhSetFsMIBgp4TCPAOAuthKeyStatus (1, 1, &PeerAddr, 1, 1);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;
}

INT4
BgpUtFsSha_98 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 1 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;
    STRCPY (au1IpAddress, "20.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*i4RetVal = nmhSetFsbgp4TCPAOAuthKeyStatus(1, &PeerAddr, 1, 1);  */
    i4RetVal = nmhSetFsMIBgp4TCPAOAuthKeyStatus (u4VcId, 1, &PeerAddr, 1, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPAOAuthKeyStatus(1, &PeerAddr, 1, &i4GetVal);  */
    i4RetVal =
        nmhGetFsMIBgp4TCPAOAuthKeyStatus (u4VcId, 1, &PeerAddr, 1, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == 1)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPAOAuthKeyStatus(1, &PeerAddr, 1, 2);  */
    i4RetVal = nmhSetFsMIBgp4TCPAOAuthKeyStatus (u4VcId, 1, &PeerAddr, 1, 2);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPAOAuthKeyStatus(1, &PeerAddr, 1, &i4GetVal);  */
    i4RetVal =
        nmhGetFsMIBgp4TCPAOAuthKeyStatus (u4VcId, 1, &PeerAddr, 1, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_99 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    INT4                i4GetVal;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    STRCPY (au1IpAddress, "20.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    /*BGP admin down */
    /*i4RetVal = nmhGetFsbgp4TCPAOAuthKeyStatus(1, &PeerAddr, 1, &i4GetVal);  */
    i4RetVal =
        nmhGetFsMIBgp4TCPAOAuthKeyStatus (u4VcId, 1, &PeerAddr, 1, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    /*BgpSetContext(u4VcId); */

    /*i4RetVal = nmhGetFsbgp4TCPAOAuthKeyStatus(5, &PeerAddr, 1, &i4GetVal);  */
    i4RetVal =
        nmhGetFsMIBgp4TCPAOAuthKeyStatus (u4VcId, 5, &PeerAddr, 1, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    STRCPY (au1IpAddress, "50.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*i4RetVal = nmhGetFsbgp4TCPAOAuthKeyStatus(1, &PeerAddr, 1, &i4GetVal); */
    i4RetVal =
        nmhGetFsMIBgp4TCPAOAuthKeyStatus (u4VcId, 1, &PeerAddr, 1, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i4RetVal = nmhGetFsMIBgp4TCPAOAuthKeyStatus (1, 1, &PeerAddr, 1, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_100 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 1 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;
    STRCPY (au1IpAddress, "20.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*i4RetVal = nmhSetFsbgp4TCPAOAuthKeyStatus(1, &PeerAddr, 1, 1);  */
    i4RetVal = nmhSetFsMIBgp4TCPAOAuthKeyStatus (u4VcId, 1, &PeerAddr, 1, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPAOAuthKeyStatus(1, &PeerAddr, 1, &i4GetVal);  */
    i4RetVal =
        nmhGetFsMIBgp4TCPAOAuthKeyStatus (u4VcId, 1, &PeerAddr, 1, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == 1)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i4RetVal =
        nmhGetFsMIBgp4mpePeerTCPAOKeyIdInUse (u4VcId, 1, &PeerAddr, &i4GetVal);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPAOAuthKeyStatus(1, &PeerAddr, 1, 2);  */
    i4RetVal = nmhSetFsMIBgp4TCPAOAuthKeyStatus (u4VcId, 1, &PeerAddr, 1, 2);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPAOAuthKeyStatus(1, &PeerAddr, 1, &i4GetVal);  */
    i4RetVal =
        nmhGetFsMIBgp4TCPAOAuthKeyStatus (u4VcId, 1, &PeerAddr, 1, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    BgpReleaseContext ();
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_101 (VOID)
{
    INT4                i4KeyId;
    INT4                i4RecvId;
    INT4                i4GetVal;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;
    UINT1               au1IpAddress[10] = { "20.0.0.1" };
    tSNMP_OCTET_STRING_TYPE PeerAddr;

    INT4                i4RetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1);
    if (PeerAddr.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    STRCPY (au1IpAddress, "20.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);
    /*BGP admin down */
    /*i4RetVal = nmhGetFsbgp4TCPAOAuthKeyStatus(1, &PeerAddr, 1, &i4GetVal);  */
    i4RetVal =
        nmhGetFsMIBgp4TCPAOAuthKeyStatus (u4VcId, 1, &PeerAddr, 1, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    /*BgpSetContext(u4VcId); */

    /*i4RetVal = nmhGetFsbgp4TCPAOAuthKeyStatus(5, &PeerAddr, 1, &i4GetVal);  */
    i4RetVal =
        nmhGetFsMIBgp4TCPAOAuthKeyStatus (u4VcId, 5, &PeerAddr, 1, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    STRCPY (au1IpAddress, "50.0.0.1");
    BgpUtIPAddressConvert (au1IpAddress, &PeerAddr);

    /*i4RetVal = nmhGetFsbgp4TCPAOAuthKeyStatus(1, &PeerAddr, 1, &i4GetVal); */
    i4RetVal =
        nmhGetFsMIBgp4TCPAOAuthKeyStatus (u4VcId, 1, &PeerAddr, 1, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i4RetVal = nmhGetFsMIBgp4TCPAOAuthKeyStatus (1, 1, &PeerAddr, 1, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i4RetVal =
        nmhGetFsMIBgp4mpePeerTCPAOKeyIdInUse (1, 1, &PeerAddr, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

/*This script is used for displaying mkt as not-active in cli for coverage*/
INT4
BgpUtFsSha_105 (VOID)
{

    INT4                i4KeyId;
    INT4                i4RecvId;
    UINT4               u4VcId;
    UINT1               au1Passkey[15] = { "OctetString" };
    tSNMP_OCTET_STRING_TYPE PassKey;

    INT4                i4RetVal;
    INT4                i4GetVal;
    INT4                i4Ret = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    /*  CliExecuteAppCmd ("neighbor 20.0.0.1 remote-as 200"); */
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;
    i4KeyId = 0;
    i4RecvId = 0;
    /*To check the nmhTest functions. */
    /*Default VRF context */

    BGP_CLI_MEM_BLK_ALLOC (PassKey.pu1_OctetList, UINT1);
    if (PassKey.pu1_OctetList == NULL)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*BgpSetContext(u4VcId); */

    MEMCPY (PassKey.pu1_OctetList, au1Passkey, 11);
    PassKey.i4_Length = 11;

    /*Test CREATE AND WAIT */
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, CREATE_AND_WAIT); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, CREATE_AND_WAIT);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == CREATE_AND_WAIT)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRecvKeyId (i4KeyId, 1); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, i4KeyId, 1);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRecvKeyId(i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRecvKeyId (u4VcId, i4KeyId, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == 1)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthMasterKey (i4KeyId, &PassKey); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthMasterKey (u4VcId, i4KeyId, &PassKey);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, NOT_IN_SERVICE); */
    i4RetVal =
        nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, NOT_IN_SERVICE);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == NOT_IN_SERVICE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

#if 0
    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, ACTIVE); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, ACTIVE);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (!((i4RetVal == SNMP_SUCCESS) && (i4GetVal == ACTIVE)))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /*i4RetVal = nmhSetFsbgp4TCPMKTAuthRowStatus (i4KeyId, DESTROY); */
    i4RetVal = nmhSetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    /*i4RetVal = nmhGetFsbgp4TCPMKTAuthRowStatus (i4KeyId, &i4GetVal); */
    i4RetVal = nmhGetFsMIBgp4TCPMKTAuthRowStatus (u4VcId, i4KeyId, &i4GetVal);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
#endif

  RETURN:
    /*BgpReleaseContext(); */
    BGP_CLI_MEM_BLK_FREE (PassKey.pu1_OctetList);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    /*    CliExecuteAppCmd ("configure terminal"); */
    /*    CliExecuteAppCmd ("router bgp 100"); */
    /*    CliExecuteAppCmd ("no neighbor 20.0.0.1 remote-as 200"); */
    /*    CliExecuteAppCmd ("end");  */
#if 0
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
#endif
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;

}

INT4
BgpUtFsSha_106 (VOID)
{
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 1 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 2 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 3 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 4 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 5 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 6 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 7 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 8 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 9 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 10 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 11 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 12 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 13 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 14 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 15 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 16 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 17 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 18 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 19 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 20 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 21 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 22 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 23 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 24 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 25 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 26 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 27 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 28 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 29 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 30 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 31 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 32 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 33 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 34 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 35 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 36 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 37 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 38 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 39 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 40 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 41 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 42 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 43 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 44 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 45 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 46 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 47 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 48 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 49 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 50 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 51 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 52 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 53 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 54 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 55 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 56 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 57 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 58 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 59 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 60 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 61 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 62 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 63 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 64 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 65 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 66 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 67 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 68 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 69 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 70 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 71 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 72 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 73 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 74 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 75 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 76 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 77 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 78 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 79 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 80 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 81 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 82 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 83 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 84 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 85 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 86 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 87 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 88 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 89 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 90 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 91 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 92 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 93 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 94 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 95 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 96 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 97 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 98 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 99 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 100 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd
        ("tcp-ao mkt key-id 101 receive-key-id 2 algorithm hmac-sha-1 key zzzz");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;

}

/*TCP-AO memory allocation release failure*/
INT4
BgpUtFsSha_107 (VOID)
{
    tTcpAoAuthMKT      *pMkt;
    INT4                i4Ret = OSIX_SUCCESS;
    INT4                i4RetVal;
    INT4                i4KeyId;
    UINT4               u4VcId;
    tAddrPrefix         PeerAddrPrefix;
    tUtlInAddr          InAddr;
    UINT4               u4IpAddress;

    UINT1               au1IpAddress[10] = { "20.0.0.1" };

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4VcId = BGP4_DFLT_VRFID;

    pMkt = Bgp4MemAllocateTcpAoBuf (u4VcId, 10);

    if (pMkt != NULL)
    {
        pMkt += 1;

        i4RetVal = Bgp4MemReleaseTcpAoBuf (NULL, u4VcId);
        if (i4RetVal != BGP4_FAILURE)
        {
            i4Ret = OSIX_FAILURE;
            goto RETURN;
        }
    }

    /*Bgp4GetNextTCPAOKeyId */

    /*
       tUtlInAddr          InAddr;
       UINT4        u4IpAddress;
       tAddrPrefix  PeerAddrPrefix; 

       BGP4_INET_ATON (pau1IpAddress, &InAddr);
       u4IpAddress = InAddr.u4Addr;
       MEMCPY ((PeerAddrPrefix.au1Address),
       &u4IpAddress, sizeof (UINT4));
       PeerAddrPrefix.u2AddressLen = sizeof (UINT4);

       PeerAddrPrefix.u2Afi = BGP4_INET_AFI_IPV4;
     */
    BGP4_INET_ATON (au1IpAddress, &InAddr);
    u4IpAddress = InAddr.u4Addr;
    MEMCPY ((PeerAddrPrefix.au1Address), &u4IpAddress, sizeof (UINT4));
    PeerAddrPrefix.u2AddressLen = sizeof (UINT4);
    PeerAddrPrefix.u2Afi = BGP4_INET_AFI_IPV4;

    i4RetVal =
        Bgp4GetNextTCPAOKeyId (u4VcId, PeerAddrPrefix, i4KeyId, &i4KeyId);
    if (i4RetVal != BGP4_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

  RETURN:
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return i4Ret;
}

INT4
BgpUtFsSha_108 (VOID)
{
    INT4                i4Count = 0;
    UINT1              *pu1Ptr;

    for (i4Count = 0; i4Count <= 19; i4Count++)
    {
        BGP_CLI_MEM_BLK_ALLOC (pu1Ptr, UINT1);
    }
    return OSIX_SUCCESS;
}

/* bgpfourbyteasn UT cases */

INT4
BgpUt4ByteAsn_1 (VOID)
{
    /* nmhTestv2FsMIBgp4FourByteASNSupportStatus , 
     * nmhGetFsMIBgp4FourByteASNSupportStatus , 
     * nmhSetFsMIBgp4FourByteASNSupportStatus 
     * nmhTestv2fsbgp4FourByteASNSupportStatus ,
     * nmhGetfsbgp4FourByteASNSupportStatus ,
     * nmhSetfsbgp4FourByteASNSupportStatus */

    INT4                i4Status;
    INT4                i4Ret;
    UINT4               u4Context = 0;
    UINT4               u4ErrorCode;
    INT1                i1Return;

    CliTakeAppContext ();
    MGMT_UNLOCK ();

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("router bgp 100.1");
    CliExecuteAppCmd ("neighbor 12.0.0.7 remote-as 123; end;c t");
    CliExecuteAppCmd ("do shut ip bgp");
    CliExecuteAppCmd ("ip bgp four-byte-asn");
    CliExecuteAppCmd ("no ip bgp four-byte-asn");
    CliExecuteAppCmd ("no ip bgp four-byte-asn");
    CliExecuteAppCmd ("ip bgp four-byte-asn");
    CliExecuteAppCmd ("no shut ip bgp");
    CliExecuteAppCmd ("end");

    MGMT_LOCK ();
    CliGiveAppContext ();

    if (BgpSetContext (u4Context) == SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i1Return = nmhGetFsMIBgp4FourByteASNSupportStatus (u4Context, &i4Status);
    if (i4Status != BGP4_ENABLE_4BYTE_ASN_SUPPORT)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /* for invalid support status */
    i1Return =
        nmhTestv2FsMIBgp4FourByteASNSupportStatus (&u4ErrorCode, u4Context, 3);
    if ((i1Return != SNMP_FAILURE) && (u4ErrorCode != SNMP_ERR_WRONG_VALUE))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i1Return = nmhSetFsMIBgp4FourByteASNSupportStatus (u4Context, 3);
    if (i1Return != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /* when task init status is down */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t; no router bgp;end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    i1Return =
        nmhTestv2FsMIBgp4FourByteASNSupportStatus (&u4ErrorCode, u4Context,
                                                   BGP4_ENABLE_4BYTE_ASN_SUPPORT);
    if ((i1Return != SNMP_FAILURE)
        && (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i1Return =
        nmhSetFsMIBgp4FourByteASNSupportStatus (u4Context,
                                                BGP4_ENABLE_4BYTE_ASN_SUPPORT);
    if (i1Return != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i1Return = nmhGetFsMIBgp4FourByteASNSupportStatus (u4Context, &i4Status);
    if (i1Return != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /* when admin status is up */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t; router bgp 100.2; end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    i1Return =
        nmhTestv2FsMIBgp4FourByteASNSupportStatus (&u4ErrorCode, u4Context,
                                                   BGP4_ENABLE_4BYTE_ASN_SUPPORT);
    if ((i1Return != SNMP_FAILURE) && (u4ErrorCode != SNMP_ERR_WRONG_VALUE))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i1Return =
        nmhSetFsMIBgp4FourByteASNSupportStatus (u4Context,
                                                BGP4_ENABLE_4BYTE_ASN_SUPPORT);
    if (i1Return != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t; do sh ip bgp; end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    i1Return =
        nmhSetFsMIBgp4FourByteASNSupportStatus (u4Context,
                                                BGP4_ENABLE_4BYTE_ASN_SUPPORT);
    if (i1Return != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /* for an invalid context */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t; no sh ip bgp; end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4Context = 5;
    i1Return =
        nmhTestv2FsMIBgp4FourByteASNSupportStatus (&u4ErrorCode, u4Context,
                                                   BGP4_ENABLE_4BYTE_ASN_SUPPORT);
    if ((i1Return != SNMP_FAILURE)
        && (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i1Return =
        nmhSetFsMIBgp4FourByteASNSupportStatus (u4Context,
                                                BGP4_ENABLE_4BYTE_ASN_SUPPORT);
    if (i1Return != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i1Return = nmhGetFsMIBgp4FourByteASNSupportStatus (u4Context, &i4Status);
    if (i1Return != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i4Ret = OSIX_SUCCESS;
    goto RETURN;

  RETURN:
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;do shut ip bgp; ip bgp four-byte-asn; end");
    CliExecuteAppCmd ("c t; no router bgp; end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    return i4Ret;
}

INT4
BgpUt4ByteAsn_2 (VOID)
{
    /* nmhTestv2FsMIBgp4FourByteASNotationType , 
     * nmhGetFsMIBgp4FourByteASNotationType , 
     * nmhSetFsMIBgp4FourByteASNotationType, 
     * nmhTestv2fsbgp4FourByteASNotationType ,
     * nmhGetfsbgp4FourByteASNotationType ,
     * nmhSetfsbgp4FourByteASNotationType */

    INT4                i4NotationType;
    INT4                i4Ret;
    UINT4               u4Context = 0;
    UINT4               u4ErrorCode;
    INT1                i1Return;

    CliTakeAppContext ();
    MGMT_UNLOCK ();

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("router bgp 100.1");
    CliExecuteAppCmd ("neighbor 12.0.0.7 remote-as 123");
    CliExecuteAppCmd ("bgp asnotation dot");
    CliExecuteAppCmd ("no bgp asnotation dot");
    CliExecuteAppCmd ("no bgp asnotation dot");
    CliExecuteAppCmd ("bgp asnotation dot");
    CliExecuteAppCmd ("end");

    MGMT_LOCK ();
    CliGiveAppContext ();

    if (BgpSetContext (u4Context) == SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i1Return =
        nmhGetFsMIBgp4FourByteASNotationType (u4Context, &i4NotationType);
    if (i4NotationType != BGP4_ENABLE_4BYTE_ASN_ASDOT_NOTATION)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i1Return =
        nmhSetFsMIBgp4FourByteASNotationType (u4Context,
                                              BGP4_ENABLE_4BYTE_ASN_ASDOT_NOTATION);
    if (i1Return != SNMP_SUCCESS)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /* for invalid notationtype */
    i1Return =
        nmhTestv2FsMIBgp4FourByteASNotationType (&u4ErrorCode, u4Context, 3);
    if ((i1Return != SNMP_FAILURE) && (u4ErrorCode != SNMP_ERR_WRONG_VALUE))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /* when task init status is down */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t; no router bgp;end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    i1Return =
        nmhTestv2FsMIBgp4FourByteASNotationType (&u4ErrorCode, u4Context,
                                                 BGP4_ENABLE_4BYTE_ASN_ASDOT_NOTATION);
    if ((i1Return != SNMP_FAILURE)
        && (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }
    i1Return =
        nmhSetFsMIBgp4FourByteASNotationType (u4Context,
                                              BGP4_ENABLE_4BYTE_ASN_ASDOT_NOTATION);
    if (i1Return != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i1Return =
        nmhGetFsMIBgp4FourByteASNotationType (u4Context, &i4NotationType);
    if (i1Return != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /* when 4-byte asn is disabled */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t; router bgp 100.1; end");
    CliExecuteAppCmd ("c t; do sh ip bgp; no ip bgp four-byte-asn;end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    i1Return =
        nmhTestv2FsMIBgp4FourByteASNotationType (&u4ErrorCode, u4Context,
                                                 BGP4_ENABLE_4BYTE_ASN_ASDOT_NOTATION);
    if ((i1Return != SNMP_FAILURE) && (u4ErrorCode != SNMP_ERR_WRONG_VALUE))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i1Return =
        nmhSetFsMIBgp4FourByteASNotationType (u4Context,
                                              BGP4_ENABLE_4BYTE_ASN_ASDOT_NOTATION);
    if (i1Return != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    /* for an invalid context */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t; ip bgp four-byte-asn; end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    u4Context = 5;
    i1Return =
        nmhTestv2FsMIBgp4FourByteASNotationType (&u4ErrorCode, u4Context,
                                                 BGP4_ENABLE_4BYTE_ASN_ASDOT_NOTATION);
    if ((i1Return != SNMP_FAILURE)
        && (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE))
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i1Return =
        nmhSetFsMIBgp4FourByteASNotationType (u4Context,
                                              BGP4_ENABLE_4BYTE_ASN_ASDOT_NOTATION);
    if (i1Return != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i1Return =
        nmhGetFsMIBgp4FourByteASNotationType (u4Context, &i4NotationType);
    if (i1Return != SNMP_FAILURE)
    {
        i4Ret = OSIX_FAILURE;
        goto RETURN;
    }

    i4Ret = OSIX_SUCCESS;
    goto RETURN;

  RETURN:
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;do shut ip bgp; ip bgp four-byte-asn; end");
    CliExecuteAppCmd ("c t; no router bgp; end");
    MGMT_LOCK ();
    CliGiveAppContext ();
    return i4Ret;
}

INT4
BgpUt4ByteAsn_3 (VOID)
{
    /* nmhGetFsbgp4mpebgp4PathAttrAS4PathSegment */
    INT4                i4Afi;
    INT4                i4Safi;
    tSNMP_OCTET_STRING_TYPE addrprefix;
    INT4                prefixlen;
    INT4                peertype;
    tSNMP_OCTET_STRING_TYPE attrpeer;
    tSNMP_OCTET_STRING_TYPE as4path;
    UINT1               pathlist[100];
    tAddrPrefix         peer;
    tBgp4PeerEntry     *peerEntry;
    UINT1               octetlist[16];
    UINT1               addr[4];
    INT1                i1Ret;
    tBgp4Info           bgpInfo;
    tAsPath            *pAS4Path;
    UINT1               tempPath[12] =
        { 0, 0x01, 0x5b, 0xa0, 0, 0, 0, 0x64, 0x01, 0, 0x5b, 0xa0 };
    UINT4               u4Prefix;
    tRouteProfile      *pRtprofile = NULL;
    tLinkNode          *pLknode = NULL;
    tSupCapsInfo       *pPeerCapsRcvd = NULL;

    as4path.pu1_OctetList = pathlist;

    peer.u2Afi = 1;
    peer.u2AddressLen = 4;
    MEMSET (peer.au1Address, 0, 16);
    peer.au1Address[0] = 8;
    peer.au1Address[1] = 0;
    peer.au1Address[2] = 0;
    peer.au1Address[3] = 12;

    peerEntry = Bgp4SnmphAddPeerEntry (0, peer);
    if (peerEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    if (BgpSetContext (0) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    i4Afi = 1;
    i4Safi = 1;
    addrprefix.pu1_OctetList = addr;
    addrprefix.i4_Length = 4;
    addr[0] = 12;
    addr[1] = 0;
    addr[2] = 0;
    addr[3] = 8;
    /*MEMCPY(addr, peer.au1Address, 4); */
    prefixlen = 8;
    peertype = 1;
    attrpeer.pu1_OctetList = octetlist;
    attrpeer.i4_Length = 16;
    MEMCPY (octetlist, addr, 16);
    i1Ret =
        nmhGetFsbgp4mpebgp4PathAttrAS4PathSegment (i4Afi, i4Safi, &addrprefix,
                                                   prefixlen, peertype,
                                                   &attrpeer, &as4path);
    if (i1Ret != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    octetlist[0] = 8;
    i1Ret =
        nmhGetFsbgp4mpebgp4PathAttrAS4PathSegment (i4Afi, i4Safi, &addrprefix,
                                                   prefixlen, peertype,
                                                   &attrpeer, &as4path);
    if (i1Ret != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    octetlist[0] = 12;
    peertype = 3;
    i1Ret =
        nmhGetFsbgp4mpebgp4PathAttrAS4PathSegment (i4Afi, i4Safi, &addrprefix,
                                                   prefixlen, peertype,
                                                   &attrpeer, &as4path);
    if (i1Ret != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    i1Ret =
        nmhGetFsMIBgp4mpebgp4PathAttrAS4PathSegment (0, i4Afi, i4Safi,
                                                     &addrprefix, prefixlen,
                                                     peertype, &attrpeer,
                                                     &as4path);
    if (i1Ret != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    i1Ret =
        nmhGetFsMIBgp4mpebgp4PathAttrAS4PathSegment (5, i4Afi, i4Safi,
                                                     &addrprefix, prefixlen,
                                                     peertype, &attrpeer,
                                                     &as4path);
    if (i1Ret != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    peertype = 1;
    i4Afi = 1;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    pAS4Path = Bgp4MemGetASNode (sizeof (tAsPath));
    BGP4_ASPATH_TYPE (pAS4Path) = 1;
    BGP4_ASPATH_LEN (pAS4Path) = 3;
    MEMCPY (BGP4_ASPATH_NOS (pAS4Path), tempPath, 12);
    TMO_SLL_Init (BGP4_INFO_ASPATH ((&bgpInfo)));
    TMO_SLL_Add (BGP4_INFO_ASPATH ((&bgpInfo)), &pAS4Path->sllNode);
    pLknode = Bgp4MemAllocateLinkNode (sizeof (tLinkNode));
    pRtprofile = Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
    BGP4_LINK_INFO_TO_PROFILE ((&bgpInfo), pRtprofile);
    BGP4_LINK_PROFILE_TO_NODE (pRtprofile, pLknode);
    BGP4_RT_PEER_ENTRY (pRtprofile) = peerEntry;
    BGP4_RT_PROTOCOL (pRtprofile) = BGP_ID;
    BGP4_RT_PREFIXLEN (pRtprofile) = 8;
    /* Put the AFI */
    BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                  (BGP4_RT_NET_ADDRESS_INFO (pRtprofile))) =
        BGP4_INET_AFI_IPV4;
    /* Put the Address Length = 4 */
    BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO
        (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
         (BGP4_RT_NET_ADDRESS_INFO (pRtprofile))) = BGP4_IPV4_PREFIX_LEN;
    /* Put the SAFI */
    BGP4_SAFI_IN_NET_ADDRESS_INFO (BGP4_RT_NET_ADDRESS_INFO (pRtprofile))
        = BGP4_INET_SAFI_UNICAST;
    u4Prefix = 12;
    PTR_ASSIGN4 (BGP4_RT_IP_PREFIX (pRtprofile), (u4Prefix));
    BGP4_RT_RESET_FLAG (pRtprofile, (BGP4_RT_WITHDRAWN | BGP4_RT_REPLACEMENT));
    Bgp4RibhAddRtEntry (pRtprofile, 0);

    if (BgpSetContext (0) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    i1Ret =
        nmhGetFsbgp4mpebgp4PathAttrAS4PathSegment (i4Afi, i4Safi, &addrprefix,
                                                   prefixlen, peertype,
                                                   &attrpeer, &as4path);
    if (i1Ret != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    tempPath[1] = 0;
    tempPath[2] = 0x0b;
    MEMCPY (BGP4_ASPATH_NOS (pAS4Path), tempPath, 12);
    i1Ret =
        nmhGetFsbgp4mpebgp4PathAttrAS4PathSegment (i4Afi, i4Safi, &addrprefix,
                                                   prefixlen, peertype,
                                                   &attrpeer, &as4path);
    if (i1Ret != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    tempPath[0] = 0;
    tempPath[4] = 0;
    tempPath[5] = 0;
    tempPath[8] = 0;
    tempPath[9] = 0;
    MEMCPY (BGP4_ASPATH_NOS (pAS4Path), tempPath, 12);
    i1Ret =
        nmhGetFsbgp4mpebgp4PathAttrAS4PathSegment (i4Afi, i4Safi, &addrprefix,
                                                   prefixlen, peertype,
                                                   &attrpeer, &as4path);
    if (i1Ret != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    tempPath[0] = 0x01;
    tempPath[4] = 0x01;
    tempPath[5] = 0;
    tempPath[8] = 0;
    tempPath[9] = 0x01;
    BGP4_ASPATH_TYPE (pAS4Path) = 3;
    i1Ret =
        nmhGetFsbgp4mpebgp4PathAttrAS4PathSegment (i4Afi, i4Safi, &addrprefix,
                                                   prefixlen, peertype,
                                                   &attrpeer, &as4path);
    if (i1Ret != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    BGP4_ASPATH_TYPE (pAS4Path) = 4;
    i1Ret =
        nmhGetFsbgp4mpebgp4PathAttrAS4PathSegment (i4Afi, i4Safi, &addrprefix,
                                                   prefixlen, peertype,
                                                   &attrpeer, &as4path);
    if (i1Ret != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    PEER_CAPS_RCVD_ENTRY_CREATE (pPeerCapsRcvd);
    pPeerCapsRcvd->SupCapability.u1CapCode = CAP_CODE_4_OCTET_ASNO;
    pPeerCapsRcvd->SupCapability.u1CapLength = 4;
    pPeerCapsRcvd->SupCapability.au1CapValue[0] = 0;
    pPeerCapsRcvd->SupCapability.au1CapValue[1] = 0;
    pPeerCapsRcvd->SupCapability.au1CapValue[2] = 0x5b;
    pPeerCapsRcvd->SupCapability.au1CapValue[3] = 0xa1;
    TMO_SLL_Init (BGP4_PEER_RCVD_CAPS_LIST (peerEntry));
    TMO_SLL_Add (BGP4_PEER_RCVD_CAPS_LIST (peerEntry),
                 &pPeerCapsRcvd->NextCapability);
    BGP4_FOUR_BYTE_ASN_SUPPORT (0) = BGP4_ENABLE_4BYTE_ASN_SUPPORT;
    BGP4_PEER_ASNO (peerEntry) = 23457;
    BGP4_PEER_RCVD_CAPS_NEGOTIATED_STATUS (pPeerCapsRcvd) = 1;
    i1Ret =
        nmhGetFsbgp4mpebgp4PathAttrAS4PathSegment (i4Afi, i4Safi, &addrprefix,
                                                   prefixlen, peertype,
                                                   &attrpeer, &as4path);
    if (i1Ret != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    if (BgpSetContext (0) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    i1Ret =
        nmhGetFsbgp4mpebgp4PathAttrAS4PathSegment (i4Afi, i4Safi, &addrprefix,
                                                   prefixlen, peertype,
                                                   &attrpeer, &as4path);
    if (i1Ret != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
BgpUt4ByteAsn_4 (VOID)
{
    /* nmhGetFsbgp4mpebgp4PathAttrAggregatorAS4 */

    INT4                i4Afi;
    INT4                i4Safi;
    tSNMP_OCTET_STRING_TYPE addrprefix;
    INT4                prefixlen;
    INT4                peertype;
    tSNMP_OCTET_STRING_TYPE attrpeer;
    UINT4               as4aggr;
    tAddrPrefix         peer;
    tBgp4PeerEntry     *peerEntry;
    UINT1               octetlist[16];
    UINT1               addr[4];
    INT1                i1Ret;

    if (BgpSetContext (0) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    i4Afi = 1;
    i4Safi = 1;
    addrprefix.pu1_OctetList = addr;
    addrprefix.i4_Length = 4;
    addr[0] = 12;
    addr[1] = 0;
    addr[2] = 0;
    addr[3] = 8;
    /*MEMCPY(addr, peer.au1Address, 4); */
    prefixlen = 8;
    peertype = 1;
    attrpeer.pu1_OctetList = octetlist;
    attrpeer.i4_Length = 16;
    MEMCPY (octetlist, addr, 16);
    i1Ret =
        nmhGetFsbgp4mpebgp4PathAttrAggregatorAS4 (i4Afi, i4Safi, &addrprefix,
                                                  prefixlen, peertype,
                                                  &attrpeer, &as4aggr);
    if (i1Ret != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd
        ("ex; do sh ip bgp; no ip bgp four-byte-asn; no sh ip bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    peer.u2Afi = 1;
    peer.u2AddressLen = 4;
    MEMSET (peer.au1Address, 0, 16);
    peer.au1Address[0] = 8;
    peer.au1Address[1] = 0;
    peer.au1Address[2] = 0;
    peer.au1Address[3] = 12;

    peerEntry = Bgp4SnmphAddPeerEntry (0, peer);
    if (peerEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    if (BgpSetContext (0) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    i1Ret =
        nmhGetFsbgp4mpebgp4PathAttrAggregatorAS4 (i4Afi, i4Safi, &addrprefix,
                                                  prefixlen, peertype,
                                                  &attrpeer, &as4aggr);
    if (i1Ret != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    octetlist[0] = 8;
    i1Ret =
        nmhGetFsbgp4mpebgp4PathAttrAggregatorAS4 (i4Afi, i4Safi, &addrprefix,
                                                  prefixlen, peertype,
                                                  &attrpeer, &as4aggr);
    if (i1Ret != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    addr[0] = 12;
    peertype = 3;
    i1Ret =
        nmhGetFsbgp4mpebgp4PathAttrAggregatorAS4 (i4Afi, i4Safi, &addrprefix,
                                                  prefixlen, peertype,
                                                  &attrpeer, &as4aggr);
    if (i1Ret != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    peertype = 1;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("ex; no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    if (BgpSetContext (0) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    i1Ret =
        nmhGetFsbgp4mpebgp4PathAttrAggregatorAS4 (i4Afi, i4Safi, &addrprefix,
                                                  prefixlen, peertype,
                                                  &attrpeer, &as4aggr);
    if (i1Ret != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    i1Ret =
        nmhGetFsMIBgp4mpebgp4PathAttrAggregatorAS4 (0, i4Afi, i4Safi,
                                                    &addrprefix, prefixlen,
                                                    peertype, &attrpeer,
                                                    &as4aggr);
    if (i1Ret != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    i1Ret =
        nmhGetFsMIBgp4mpebgp4PathAttrAggregatorAS4 (5, i4Afi, i4Safi,
                                                    &addrprefix, prefixlen,
                                                    peertype, &attrpeer,
                                                    &as4aggr);
    if (i1Ret != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("ex; do sh ip bgp; ip bgp four-byte-asn;end; c t");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
BgpUt4ByteAsn_5 (VOID)
{
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("router bgp abc");
    CliExecuteAppCmd ("router bgp 1.2; exit; no router bgp");
    CliExecuteAppCmd ("router bgp 0.0");
    CliExecuteAppCmd ("router bgp 65535.65535; exit; no router bgp");
    CliExecuteAppCmd ("router bgp 0.1; exit; no router bgp");
    CliExecuteAppCmd ("router bgp 12$.");
    CliExecuteAppCmd ("router bgp 12345678.123");
    CliExecuteAppCmd ("router bgp .123");
    CliExecuteAppCmd ("router bgp 65536.65");
    CliExecuteAppCmd ("router bgp a.b");
    CliExecuteAppCmd ("router bgp 0");
    CliExecuteAppCmd ("router bgp 65535; exit; no router bgp");
    CliExecuteAppCmd ("router bgp 65536; exit; no router bgp");
    CliExecuteAppCmd ("router bgp 4294967296");
    CliExecuteAppCmd ("router bgp 4295967295");
    CliExecuteAppCmd ("router bgp 4294967295");
    CliExecuteAppCmd ("end; c t; do sh ip bgp; no ip bgp four-byte-asn; end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("router bgp 100.2");
    CliExecuteAppCmd ("router bgp 0");
    CliExecuteAppCmd ("router bgp 1;exit; no router bgp");
    CliExecuteAppCmd ("router bgp 65535; exit; no router bgp");
    CliExecuteAppCmd ("router bgp 65536; exit; no router bgp");
    CliExecuteAppCmd ("router bgp 23456");
    CliExecuteAppCmd ("router bgp 1.65536");
    CliExecuteAppCmd ("router bgp 12345678900.1");
    CliExecuteAppCmd ("router bgp 1234567890.123456");
    CliExecuteAppCmd ("router bgp 123.12.");
    CliExecuteAppCmd ("router bgp 345.");
    CliExecuteAppCmd ("c t; router bgp 100; ex;");
    CliExecuteAppCmd (" do sh ip bgp; ip bgp four-byte-asn;");
    CliExecuteAppCmd (" no sh ip bgp; no router bgp; end");

    return OSIX_SUCCESS;
}

INT4
BgpUt4ByteAsn_6 (VOID)
{
    /* Bgp4GetSpeakerPeer4ByteAsnCapability */
    tAddrPrefix         peer;
    INT4                i4Ret;
    tBgp4PeerEntry     *peerEntry;
    tSupCapsInfo       *pPeerCapsRcvd = NULL;

    peer.u2Afi = 1;
    peer.u2AddressLen = 4;
    MEMSET (peer.au1Address, 0, 16);
    peer.au1Address[0] = 8;
    peer.au1Address[1] = 0;
    peer.au1Address[2] = 0;
    peer.au1Address[3] = 12;

    peerEntry = Bgp4SnmphAddPeerEntry (0, peer);
    if (peerEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    BGP4_FOUR_BYTE_ASN_SUPPORT (0) = BGP4_ENABLE_4BYTE_ASN_SUPPORT;
    i4Ret = Bgp4GetSpeakerPeer4ByteAsnCapability (peerEntry);
    if (i4Ret != BGP4_4BYTE_ASN_SPEAKER_ONLY)
    {
        return OSIX_FAILURE;
    }

    PEER_CAPS_RCVD_ENTRY_CREATE (pPeerCapsRcvd);
    pPeerCapsRcvd->SupCapability.u1CapCode = CAP_CODE_4_OCTET_ASNO;
    pPeerCapsRcvd->SupCapability.u1CapLength = 4;
    pPeerCapsRcvd->SupCapability.au1CapValue[0] = 0;
    pPeerCapsRcvd->SupCapability.au1CapValue[1] = 0;
    pPeerCapsRcvd->SupCapability.au1CapValue[2] = 0x5b;
    pPeerCapsRcvd->SupCapability.au1CapValue[3] = 0xa1;
    TMO_SLL_Init (BGP4_PEER_RCVD_CAPS_LIST (peerEntry));
    TMO_SLL_Add (BGP4_PEER_RCVD_CAPS_LIST (peerEntry),
                 &pPeerCapsRcvd->NextCapability);
    BGP4_FOUR_BYTE_ASN_SUPPORT (0) = BGP4_ENABLE_4BYTE_ASN_SUPPORT;
    BGP4_PEER_ASNO (peerEntry) = 23457;
    i4Ret = Bgp4GetSpeakerPeer4ByteAsnCapability (peerEntry);
    if (i4Ret != BGP4_4BYTE_ASN_NO_SPEAKER)
    {
        return OSIX_FAILURE;
    }

    BGP4_PEER_RCVD_CAPS_NEGOTIATED_STATUS (pPeerCapsRcvd) = 1;
    i4Ret = Bgp4GetSpeakerPeer4ByteAsnCapability (peerEntry);
    if (i4Ret != BGP4_4BYTE_ASN_SPEAKER_AND_PEER)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;router bgp 100; end");
    CliExecuteAppCmd ("c t; do sh ip bgp; no ip bgp four-byte-asn");
    MGMT_LOCK ();
    CliGiveAppContext ();
    i4Ret = Bgp4GetSpeakerPeer4ByteAsnCapability (peerEntry);
    if (i4Ret != BGP4_4BYTE_ASN_NO_SPEAKER)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ip bgp four-byte-asn; no sh ip bgp; end");
    CliExecuteAppCmd ("c t; no router bgp; end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
BgpUt4ByteAsn_7 (VOID)
{
    /* Bgp4ConfigFourByteAsn,
       Bgp4Snmph4ByteAsnCapabilityEnable,
       Bgp4Snmph4ByteAsnCapabilityDisable,
       Bgp4SnmphGet4ByteAsnCapSupportStatus */

    tCliHandle          CliHandle;
    tAddrPrefix         pPeer;
    INT4                i4Ret;
    UINT1               u1Ret;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;router bgp 100");
    CliExecuteAppCmd ("neighbor 12.0.0.4 remote-as 234;end");
    CliExecuteAppCmd ("c t; do sh ip bgp; end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    if (BgpSetContext (0) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    i4Ret = Bgp4ConfigFourByteAsn (CliHandle, 3);
    if (i4Ret != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }

    i4Ret = Bgp4ConfigFourByteAsn (CliHandle, 1);
    if (i4Ret != CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    pPeer.u2Afi = 1;
    pPeer.u2AddressLen = 4;
    MEMSET (pPeer.au1Address, 0, 16);
    pPeer.au1Address[0] = 4;
    pPeer.au1Address[1] = 0;
    pPeer.au1Address[2] = 0;
    pPeer.au1Address[3] = 12;

    i4Ret = Bgp4Snmph4ByteAsnCapabilityEnable (0, pPeer);

    i4Ret = Bgp4ConfigFourByteAsn (CliHandle, 0);
    if (i4Ret != CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    i4Ret = Bgp4Snmph4ByteAsnCapabilityDisable (pPeer);
    if (i4Ret != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    pPeer.au1Address[0] = 6;
    u1Ret = Bgp4SnmphGet4ByteAsnCapSupportStatus (pPeer);
    if (u1Ret != BGP4_FALSE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t; do sh ip bgp;ip bgp four-byte-asn;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
BgpUt4ByteAsn_8 (VOID)
{
    /* Bgp4ConfigFourByteAsnNotation */
    tCliHandle          CliHandle;
    INT4                i4Ret;

    if (BgpSetContext (0) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    i4Ret = Bgp4ConfigFourByteAsnNotation (CliHandle, 3);
    if (i4Ret != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
BgpUt4ByteAsn_9 (VOID)
{
    /* Bgp4MsghProcessPathFour */
    tAddrPrefix         peer;
    INT4                i4Ret;
    UINT1               u1Buf[100];
    tBgp4Info           bgpInfo;
    tBgp4PeerEntry     *peerEntry;
    INT4                i4RemLen;
    tAsPath            *pAS4Path;
    tSupCapsInfo       *pPeerCapsRcvd = NULL;
    UINT1               tempPath[12] =
        { 0, 0, 0x5b, 0xa0, 0, 0, 0, 0x64, 0, 0, 0x5b, 0xa0 };

    peer.u2Afi = 1;
    peer.u2AddressLen = 4;
    MEMSET (peer.au1Address, 0, 16);
    peer.au1Address[0] = 8;
    peer.au1Address[1] = 0;
    peer.au1Address[2] = 0;
    peer.au1Address[3] = 12;

    peerEntry = Bgp4SnmphAddPeerEntry (0, peer);
    if (peerEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    i4RemLen = 30;
    MEMSET (u1Buf, 0, 100);
    u1Buf[0] = 192;
    u1Buf[1] = 17;
    u1Buf[2] = 6;
    u1Buf[3] = 2;
    u1Buf[4] = 3;
    u1Buf[5] = 0;
    u1Buf[6] = 1;
    u1Buf[7] = 95;
    u1Buf[8] = 144;
    u1Buf[12] = 100;
    u1Buf[16] = 0;
    u1Buf[17] = 1;
    u1Buf[18] = 95;
    u1Buf[19] = 145;

    BGP4_FOUR_BYTE_ASN_SUPPORT (0) = BGP4_DISABLE_4BYTE_ASN_SUPPORT;
    i4Ret = Bgp4MsghProcessPathFour (peerEntry, &bgpInfo, u1Buf, i4RemLen);

    pAS4Path = Bgp4MemGetASNode (sizeof (tAsPath));
    BGP4_ASPATH_TYPE (pAS4Path) = 1;
    BGP4_ASPATH_LEN (pAS4Path) = 3;
    MEMCPY (BGP4_ASPATH_NOS (pAS4Path), tempPath, 12);
    TMO_SLL_Init (BGP4_INFO_ASPATH ((&bgpInfo)));
    TMO_SLL_Add (BGP4_INFO_ASPATH ((&bgpInfo)), &pAS4Path->sllNode);
    i4Ret = Bgp4MsghProcessPathFour (peerEntry, &bgpInfo, u1Buf, i4RemLen);
    if (i4Ret <= 0)
    {
        return OSIX_FAILURE;
    }

    u1Buf[3] = 3;
    i4Ret = Bgp4MsghProcessPathFour (peerEntry, &bgpInfo, u1Buf, i4RemLen);
    if (i4Ret <= 0)
    {
        return OSIX_FAILURE;
    }

    u1Buf[3] = 1;
    i4Ret = Bgp4MsghProcessPathFour (peerEntry, &bgpInfo, u1Buf, i4RemLen);
    if (i4Ret <= 0)
    {
        return OSIX_FAILURE;
    }

    u1Buf[3] = 2;
    u1Buf[4] = 20;
    i4Ret = Bgp4MsghProcessPathFour (peerEntry, &bgpInfo, u1Buf, i4RemLen);
    if (i4Ret != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }

    u1Buf[4] = 3;
    i4RemLen = 0;
    i4Ret = Bgp4MsghProcessPathFour (peerEntry, &bgpInfo, u1Buf, i4RemLen);
    if (i4Ret <= 0)
    {
        return OSIX_FAILURE;
    }

    u1Buf[0] = 0x10;
    i4Ret = Bgp4MsghProcessPathFour (peerEntry, &bgpInfo, u1Buf, i4RemLen);
    if (i4Ret <= 0)
    {
        return OSIX_FAILURE;
    }

    u1Buf[0] = 192;
    i4RemLen = 30;
    PEER_CAPS_RCVD_ENTRY_CREATE (pPeerCapsRcvd);
    pPeerCapsRcvd->SupCapability.u1CapCode = CAP_CODE_4_OCTET_ASNO;
    pPeerCapsRcvd->SupCapability.u1CapLength = 4;
    pPeerCapsRcvd->SupCapability.au1CapValue[0] = 0;
    pPeerCapsRcvd->SupCapability.au1CapValue[1] = 0;
    pPeerCapsRcvd->SupCapability.au1CapValue[2] = 0x5b;
    pPeerCapsRcvd->SupCapability.au1CapValue[3] = 0xa1;
    BGP4_PEER_RCVD_CAPS_NEGOTIATED_STATUS (pPeerCapsRcvd) = 1;
    TMO_SLL_Init (BGP4_PEER_RCVD_CAPS_LIST (peerEntry));
    TMO_SLL_Add (BGP4_PEER_RCVD_CAPS_LIST (peerEntry),
                 &pPeerCapsRcvd->NextCapability);
    BGP4_FOUR_BYTE_ASN_SUPPORT (0) = BGP4_ENABLE_4BYTE_ASN_SUPPORT;
    BGP4_PEER_ASNO (peerEntry) = 23457;
    i4Ret = Bgp4MsghProcessPathFour (peerEntry, &bgpInfo, u1Buf, i4RemLen);
    if (i4Ret <= 0)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
BgpUt4ByteAsn_10 (VOID)
{
    /* Bgp4MsghProcessAggregatorFour */
    tAddrPrefix         peer;
    INT4                i4Ret;
    UINT1               u1Buf[100];
    tBgp4Info           bgpInfo;
    tBgp4PeerEntry     *peerEntry;
    INT4                i4RemLen;
    tSupCapsInfo       *pPeerCapsRcvd = NULL;

    peer.u2Afi = 1;
    peer.u2AddressLen = 4;
    MEMSET (peer.au1Address, 0, 16);
    peer.au1Address[0] = 8;
    peer.au1Address[1] = 0;
    peer.au1Address[2] = 0;
    peer.au1Address[3] = 12;

    peerEntry = Bgp4SnmphAddPeerEntry (0, peer);
    if (peerEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    i4RemLen = 30;
    MEMSET (u1Buf, 0, 100);
    u1Buf[0] = 192;                /*type */
    u1Buf[1] = 18;
    u1Buf[2] = 8 /*length */ ;
    u1Buf[3] = 0;
    u1Buf[4] = 0x01;
    u1Buf[5] = 0x5f;
    u1Buf[6] = 0x90;
    u1Buf[7] = 5;
    u1Buf[8] = 0;
    u1Buf[9] = 0;
    u1Buf[10] = 5;

    i4Ret =
        Bgp4MsghProcessAggregatorFour (peerEntry, &bgpInfo, u1Buf, i4RemLen);
    if (i4Ret != 11)
    {
        return OSIX_FAILURE;
    }

    AGGR_NODE_CREATE (BGP4_INFO_AGGREGATOR ((&bgpInfo)));
    BGP4_INFO_AGGREGATOR_AS ((&bgpInfo)) = 23456;
    BGP4_INFO_AGGREGATOR_NODE ((&bgpInfo)) = 83886085;

    i4Ret =
        Bgp4MsghProcessAggregatorFour (peerEntry, &bgpInfo, u1Buf, i4RemLen);
    if (i4Ret != 11)
    {
        return OSIX_FAILURE;
    }

    i4RemLen = 0;

    i4Ret =
        Bgp4MsghProcessAggregatorFour (peerEntry, &bgpInfo, u1Buf, i4RemLen);
    if (i4Ret != 11)
    {
        return OSIX_FAILURE;
    }

    u1Buf[2] = 9;

    i4Ret =
        Bgp4MsghProcessAggregatorFour (peerEntry, &bgpInfo, u1Buf, i4RemLen);
    if (i4Ret != 12)
    {
        return OSIX_FAILURE;
    }

    i4RemLen = 30;
    u1Buf[2] = 8;
    BGP4_INFO_AGGREGATOR_AS ((&bgpInfo)) = 1000;
    BGP4_INFO_AGGREGATOR_NODE ((&bgpInfo)) = 83886085;

    i4Ret =
        Bgp4MsghProcessAggregatorFour (peerEntry, &bgpInfo, u1Buf, i4RemLen);
    if (i4Ret != 11)
    {
        return OSIX_FAILURE;
    }

    BGP4_INFO_AGGREGATOR_AS ((&bgpInfo)) = 23456;
    BGP4_INFO_AGGREGATOR_NODE ((&bgpInfo)) = 83886086;

    i4Ret =
        Bgp4MsghProcessAggregatorFour (peerEntry, &bgpInfo, u1Buf, i4RemLen);
    if (i4Ret != 11)
    {
        return OSIX_FAILURE;
    }

    BGP4_INFO_AGGREGATOR_AS ((&bgpInfo)) = 23456;
    BGP4_INFO_AGGREGATOR_NODE ((&bgpInfo)) = 83886085;
    u1Buf[4] = 0;
    u1Buf[5] = 0;
    u1Buf[6] = 0;

    i4Ret =
        Bgp4MsghProcessAggregatorFour (peerEntry, &bgpInfo, u1Buf, i4RemLen);
    if (i4Ret != 11)
    {
        return OSIX_FAILURE;
    }

    BGP4_INFO_AGGREGATOR_AS ((&bgpInfo)) = 23456;
    BGP4_INFO_AGGREGATOR_NODE ((&bgpInfo)) = 83886085;
    u1Buf[4] = 0;
    u1Buf[5] = 12;
    u1Buf[6] = 10;

    i4Ret =
        Bgp4MsghProcessAggregatorFour (peerEntry, &bgpInfo, u1Buf, i4RemLen);
    if (i4Ret != 11)
    {
        return OSIX_FAILURE;
    }

    PEER_CAPS_RCVD_ENTRY_CREATE (pPeerCapsRcvd);
    pPeerCapsRcvd->SupCapability.u1CapCode = CAP_CODE_4_OCTET_ASNO;
    pPeerCapsRcvd->SupCapability.u1CapLength = 4;
    pPeerCapsRcvd->SupCapability.au1CapValue[0] = 0;
    pPeerCapsRcvd->SupCapability.au1CapValue[1] = 0;
    pPeerCapsRcvd->SupCapability.au1CapValue[2] = 0x5b;
    pPeerCapsRcvd->SupCapability.au1CapValue[3] = 0xa1;
    BGP4_PEER_RCVD_CAPS_NEGOTIATED_STATUS (pPeerCapsRcvd) = 1;
    TMO_SLL_Init (BGP4_PEER_RCVD_CAPS_LIST (peerEntry));
    TMO_SLL_Add (BGP4_PEER_RCVD_CAPS_LIST (peerEntry),
                 &pPeerCapsRcvd->NextCapability);
    BGP4_PEER_ASNO (peerEntry) = 23457;

    i4Ret =
        Bgp4MsghProcessAggregatorFour (peerEntry, &bgpInfo, u1Buf, i4RemLen);
    if (i4Ret != 11)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
BgpUt4ByteAsn_11 (VOID)
{
    /* Bgp4AddPathAttributeToUpdate,
     * Bgp4AttrAs4PathSize */

    tAddrPrefix         peer;
    tBgp4PeerEntry     *peerEntry;
    tBgp4Info           AdvtBgpInfo;
    tAsPath            *pAsPath, *pAsPath1, *pAsPath2;
    UINT1               offset[2000];
    UINT1               tempPath[16] = { 0, 0, 0, 0x64,
        0, 0x01, 0x01, 0x01,
        0x01, 0x01, 0, 0
    };
    tSupCapsInfo       *pPeerCapsRcvd = NULL;

    peer.u2Afi = 1;
    peer.u2AddressLen = 4;
    MEMSET (peer.au1Address, 0, 16);
    peer.au1Address[0] = 8;
    peer.au1Address[1] = 0;
    peer.au1Address[2] = 0;
    peer.au1Address[3] = 12;

    peerEntry = Bgp4SnmphAddPeerEntry (0, peer);
    if (peerEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    pAsPath = Bgp4MemGetASNode (sizeof (tAsPath));
    pAsPath1 = Bgp4MemGetASNode (sizeof (tAsPath));
    pAsPath2 = Bgp4MemGetASNode (sizeof (tAsPath));
    BGP4_ASPATH_TYPE (pAsPath) = 1;
    BGP4_ASPATH_LEN (pAsPath) = 3;
    MEMCPY (BGP4_ASPATH_NOS (pAsPath), tempPath, 12);
    TMO_SLL_Init (BGP4_INFO_ASPATH ((&AdvtBgpInfo)));
    TMO_SLL_Add (BGP4_INFO_ASPATH ((&AdvtBgpInfo)), &pAsPath->sllNode);
    Bgp4AddPathAttributeToUpdate (&AdvtBgpInfo, peerEntry, offset);

    TMO_SLL_Delete (BGP4_INFO_ASPATH ((&AdvtBgpInfo)), &pAsPath->sllNode);
    BGP4_ASPATH_TYPE (pAsPath) = 2;
    TMO_SLL_Add (BGP4_INFO_ASPATH ((&AdvtBgpInfo)), &pAsPath->sllNode);
    Bgp4AddPathAttributeToUpdate (&AdvtBgpInfo, peerEntry, offset);

    TMO_SLL_Delete (BGP4_INFO_ASPATH ((&AdvtBgpInfo)), &pAsPath->sllNode);
    BGP4_ASPATH_TYPE (pAsPath) = 3;
    TMO_SLL_Add (BGP4_INFO_ASPATH ((&AdvtBgpInfo)), &pAsPath->sllNode);
    Bgp4AddPathAttributeToUpdate (&AdvtBgpInfo, peerEntry, offset);

    TMO_SLL_Delete (BGP4_INFO_ASPATH ((&AdvtBgpInfo)), &pAsPath->sllNode);
    BGP4_ASPATH_TYPE (pAsPath) = 4;
    TMO_SLL_Add (BGP4_INFO_ASPATH ((&AdvtBgpInfo)), &pAsPath->sllNode);
    Bgp4AddPathAttributeToUpdate (&AdvtBgpInfo, peerEntry, offset);

    tempPath[5] = 0;
    tempPath[6] = 0;
    tempPath[8] = 0;
    tempPath[9] = 0;
    tempPath[10] = 0x12;
    TMO_SLL_Delete (BGP4_INFO_ASPATH ((&AdvtBgpInfo)), &pAsPath->sllNode);
    BGP4_ASPATH_LEN (pAsPath) = 3;
    MEMCPY (BGP4_ASPATH_NOS (pAsPath), tempPath, 12);
    TMO_SLL_Add (BGP4_INFO_ASPATH ((&AdvtBgpInfo)), &pAsPath->sllNode);
    Bgp4AddPathAttributeToUpdate (&AdvtBgpInfo, peerEntry, offset);

    TMO_SLL_Delete (BGP4_INFO_ASPATH ((&AdvtBgpInfo)), &pAsPath->sllNode);
    BGP4_ASPATH_TYPE (pAsPath) = 1;
    TMO_SLL_Add (BGP4_INFO_ASPATH ((&AdvtBgpInfo)), &pAsPath->sllNode);
    Bgp4AttrAs4PathSize (&AdvtBgpInfo, 1);

    TMO_SLL_Delete (BGP4_INFO_ASPATH ((&AdvtBgpInfo)), &pAsPath->sllNode);
    Bgp4AttrAs4PathSize (&AdvtBgpInfo, 1);

    BGP4_ASPATH_LEN (pAsPath) = 127;
    BGP4_ASPATH_TYPE (pAsPath) = 3;
    tempPath[5] = 0x01;
    tempPath[9] = 0x02;
    MEMCPY (BGP4_ASPATH_NOS (pAsPath), tempPath, 12);
    TMO_SLL_Add (BGP4_INFO_ASPATH ((&AdvtBgpInfo)), &pAsPath->sllNode);
    BGP4_ASPATH_LEN (pAsPath1) = 2;
    BGP4_ASPATH_TYPE (pAsPath1) = 4;
    MEMCPY (BGP4_ASPATH_NOS (pAsPath1), tempPath, 12);
    TMO_SLL_Add (BGP4_INFO_ASPATH ((&AdvtBgpInfo)), &pAsPath1->sllNode);
    BGP4_ASPATH_LEN (pAsPath2) = 127;
    BGP4_ASPATH_TYPE (pAsPath2) = 1;
    MEMCPY (BGP4_ASPATH_NOS (pAsPath2), tempPath, 12);
    TMO_SLL_Add (BGP4_INFO_ASPATH ((&AdvtBgpInfo)), &pAsPath2->sllNode);
    Bgp4AddPathAttributeToUpdate (&AdvtBgpInfo, peerEntry, offset);

    TMO_SLL_Delete (BGP4_INFO_ASPATH ((&AdvtBgpInfo)), &pAsPath->sllNode);
    TMO_SLL_Delete (BGP4_INFO_ASPATH ((&AdvtBgpInfo)), &pAsPath1->sllNode);
    TMO_SLL_Delete (BGP4_INFO_ASPATH ((&AdvtBgpInfo)), &pAsPath2->sllNode);
    BGP4_ASPATH_LEN (pAsPath1) = 2;
    BGP4_ASPATH_TYPE (pAsPath1) = 4;
    MEMCPY (BGP4_ASPATH_NOS (pAsPath1), tempPath, 12);
    TMO_SLL_Add (BGP4_INFO_ASPATH ((&AdvtBgpInfo)), &pAsPath1->sllNode);
    PEER_CAPS_RCVD_ENTRY_CREATE (pPeerCapsRcvd);
    pPeerCapsRcvd->SupCapability.u1CapCode = CAP_CODE_4_OCTET_ASNO;
    pPeerCapsRcvd->SupCapability.u1CapLength = 4;
    pPeerCapsRcvd->SupCapability.au1CapValue[0] = 0;
    pPeerCapsRcvd->SupCapability.au1CapValue[1] = 0;
    pPeerCapsRcvd->SupCapability.au1CapValue[2] = 0x5b;
    pPeerCapsRcvd->SupCapability.au1CapValue[3] = 0xa1;
    BGP4_PEER_RCVD_CAPS_NEGOTIATED_STATUS (pPeerCapsRcvd) = 1;
    TMO_SLL_Init (BGP4_PEER_RCVD_CAPS_LIST (peerEntry));
    TMO_SLL_Add (BGP4_PEER_RCVD_CAPS_LIST (peerEntry),
                 &pPeerCapsRcvd->NextCapability);
    BGP4_PEER_ASNO (peerEntry) = 23457;
    Bgp4AddPathAttributeToUpdate (&AdvtBgpInfo, peerEntry, offset);

    return OSIX_SUCCESS;
}

INT4
BgpUt4ByteAsn_12 (VOID)
{
    /* ExtCommValidateExtCommunityVal */
    tSNMP_OCTET_STRING_TYPE fsbgp4AddExtCommVal;
    INT4                i4Ret;
    UINT1               au1EcommList[8];
    fsbgp4AddExtCommVal.pu1_OctetList = au1EcommList;
    MEMSET (au1EcommList, 0, 8);
    fsbgp4AddExtCommVal.i4_Length = 8;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t; router bgp 100");
    CliExecuteAppCmd
        ("bgp ecomm-route additive 12.0.0.9 8 ecomm-value 02:02:00:00:00:64:0:0");
    CliExecuteAppCmd
        ("bgp ecomm-route additive 12.0.0.9 8 ecomm-value 02:03:00:00:00:64:0:0");
    CliExecuteAppCmd
        ("bgp ecomm-route additive 12.0.0.9 8 ecomm-value 42:02:00:00:00:64:0:0");
    CliExecuteAppCmd
        ("bgp ecomm-route additive 12.0.0.9 8 ecomm-value 42:03:00:00:00:64:0:0");
    CliExecuteAppCmd
        ("bgp ecomm-route additive 12.0.0.9 8 ecomm-value 00:02:00:64:0:0:0:0");
    CliExecuteAppCmd
        ("bgp ecomm-route additive 12.0.0.9 8 ecomm-value 42:04:00:00:00:64:0:0");
    CliExecuteAppCmd
        ("bgp ecomm-route additive 12.0.0.9 8 ecomm-value 42:03:00:00:00:34:0:0");
    CliExecuteAppCmd
        ("bgp ecomm-route additive 12.0.0.9 8 ecomm-value 00:04:00:00:00:34:0:0");
    CliExecuteAppCmd
        ("bgp ecomm-route additive 12.0.0.9 8 ecomm-value 41:03:00:00:00:34:0:0");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t; do sh ip bgp; no ip bgp four-byte-asn");
    CliExecuteAppCmd ("no sh ip bgp");
    CliExecuteAppCmd ("c t;router bgp 100");
    CliExecuteAppCmd
        ("bgp ecomm-route additive 12.0.0.9 8 ecomm-value 00:02:00:64:0:0:0:0");
    CliExecuteAppCmd
        ("bgp ecomm-route additive 12.0.0.9 8 ecomm-value 00:03:00:64:0:0:0:0");
    CliExecuteAppCmd
        ("bgp ecomm-route additive 12.0.0.9 8 ecomm-value 40:02:00:64:0:0:0:0");
    CliExecuteAppCmd
        ("bgp ecomm-route additive 12.0.0.9 8 ecomm-value 40:03:00:64:0:0:0:0");
    CliExecuteAppCmd
        ("bgp ecomm-route additive 12.0.0.9 8 ecomm-value 40:03:00:34:0:0:0:0");
    CliExecuteAppCmd
        ("bgp ecomm-route additive 12.0.0.9 8 ecomm-value 42:03:00:00:00:64:0:0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    if (BgpSetContext (0) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    fsbgp4AddExtCommVal.pu1_OctetList[0] = 66;
    fsbgp4AddExtCommVal.pu1_OctetList[1] = 4;
    i4Ret = ExtCommValidateExtCommunityVal (&fsbgp4AddExtCommVal);
    if (i4Ret != EXT_COMM_FAILURE)
    {
        return OSIX_FAILURE;
    }

    fsbgp4AddExtCommVal.pu1_OctetList[0] = 64;
    fsbgp4AddExtCommVal.pu1_OctetList[1] = 4;
    i4Ret = ExtCommValidateExtCommunityVal (&fsbgp4AddExtCommVal);
    if (i4Ret != EXT_COMM_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/* BGP-ORF UT cases */

VOID
AllocMem (tMemPoolId PoolId)
{
    UINT4               i = 0;

    if (PoolId == gBgpNode.Bgp4BufNodeMsgPoolId)
    {
        u4Count = MAX_BGP_BUFNODE_MSGS;
    }
    if (PoolId == BGP4_ORF_MEM_POOL_ID)
    {
        u4Count = MAX_BGP_ORF_ENTRIES;
    }
    if (PoolId == gBgpNode.Bgp4InputQPoolId)
    {
        u4Count =
            pMemFreePoolRecList[gBgpNode.Bgp4InputQPoolId - 1].u4FreeUnitsCount;
    }
#ifdef ROUTEMAP_WANTED
    if (PoolId == gRMapGlobalInfo.RMapPoolId)
    {
        u4Count =
            pMemFreePoolRecList[gRMapGlobalInfo.RMapPoolId -
                                1].u4FreeUnitsCount;
    }
    if (PoolId == gRMapGlobalInfo.IpPrefixPoolId)
    {
        u4Count =
            pMemFreePoolRecList[gRMapGlobalInfo.IpPrefixPoolId -
                                1].u4FreeUnitsCount;
    }
#endif
#ifdef BFD_WANTED
    if (PoolId == BFD_QUEMSG_POOLID)
    {
        u4Count = pMemFreePoolRecList[BFD_QUEMSG_POOLID - 1].u4FreeUnitsCount;
    }
#endif
    for (i = 0; i < u4Count; i++)
    {
        gu1Tmp[i] = MemAllocMemBlk (PoolId);
    }
}
VOID
ReleaseMem (tMemPoolId PoolId)
{
    UINT4               i = 0;

    for (i = 0; i < u4Count; i++)
    {
        MemReleaseMemBlock (PoolId, (UINT1 *) gu1Tmp[i]);
    }
}

INT4
BgpUtOrf_1 (VOID)
{
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tBgp4PeerEntry      peerEntry;
    tAfiSafiSpecInfo    Afi;
    tRtRefreshData      RefData;
    UINT1               au1Addr[4];
    INT4                u4Val = 0;

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    MEMSET (au1Addr, 0, sizeof (au1Addr));
    MEMSET (&Afi, 0, sizeof (tAfiSafiSpecInfo));
    MEMSET (&peerEntry, 0, sizeof (tBgp4PeerEntry));

    BGP4_TASK_INIT_STATUS = BGP4_FALSE;
    if (nmhGetFsbgp4mpeRtRefreshInboundPrefixFilter (0, NULL, 0, 1, &u4Val) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    BGP4_TASK_INIT_STATUS = BGP4_TRUE;
    if (nmhGetFsbgp4mpeRtRefreshInboundPrefixFilter (0, NULL, 0, 1, &u4Val) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    PeerAddr.i4_Length = 4;
    PeerAddr.pu1_OctetList = au1Addr;
    if (nmhGetFsbgp4mpeRtRefreshInboundPrefixFilter (1, &PeerAddr, 1, 0, &u4Val)
        != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsbgp4mpeRtRefreshInboundPrefixFilter (1, &PeerAddr, 1, 1, &u4Val)
        != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhGetFsbgp4mpeRtRefreshInboundPrefixFilter (1, &PeerAddr, 1, 1, &u4Val)
        != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    BGP4_PEER_REMOTE_ADDR_INFO ((&peerEntry)).u2AddressLen = 4;
    BGP4_PEER_REMOTE_ADDR_INFO ((&peerEntry)).u2Afi = 1;
    TMO_SLL_Add (BGP4_PEERENTRY_HEAD (0), &(peerEntry.TsnNextpeer));
    if (nmhGetFsbgp4mpeRtRefreshInboundPrefixFilter (1, &PeerAddr, 1, 1, &u4Val)
        != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    peerEntry.apAsafiInstance[0] = &Afi;
    if (nmhGetFsbgp4mpeRtRefreshInboundPrefixFilter (1, &PeerAddr, 1, 1, &u4Val)
        != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    peerEntry.apAsafiInstance[0]->pRtRefreshData = &RefData;
    if (nmhGetFsbgp4mpeRtRefreshInboundPrefixFilter (1, &PeerAddr, 1, 1, &u4Val)
        != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    TMO_SLL_Delete (BGP4_PEERENTRY_HEAD (0), (tTMO_SLL_NODE *) & peerEntry);
    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_2 (VOID)
{
    tAddrPrefix         AddrPrefix;
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tSupCapsInfo        CapsInfo1;
    tSupCapsInfo        CapsInfo2;
    tOrfCapsValue       OrfCapsValue;

    MEMSET (&CapsInfo1, 0, sizeof (tSupCapsInfo));
    MEMSET (&CapsInfo2, 0, sizeof (tSupCapsInfo));

    gpBgpCurrCxtNode = gBgpCxtNode[0];

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 12.0.0.2 remote-as 200");
    CliExecuteAppCmd ("neighbor 12.0.0.2 capability orf prefix-list send");
    CliExecuteAppCmd ("neighbor 12.0.0.2 capability orf prefix-list receive");
    CliExecuteAppCmd ("neighbor 12.0.0.2 capability orf prefix-list both");
    CliExecuteAppCmd ("neighbor 12.0.0.2 capability orf prefix-list send");
    CliExecuteAppCmd ("neighbor 12.0.0.2 capability orf prefix-list receive");
    CliExecuteAppCmd ("no neighbor 12.0.0.2 capability orf prefix-list send");
    CliExecuteAppCmd
        ("no neighbor 12.0.0.2 capability orf prefix-list receive");
    CliExecuteAppCmd ("no neighbor 12.0.0.2 capability orf prefix-list both");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("sleep 1");
    MGMT_LOCK ();
    CliGiveAppContext ();

    AddrPrefix.u2Afi = 1;
    AddrPrefix.u2AddressLen = 4;
    AddrPrefix.au1Address[3] = 12;
    AddrPrefix.au1Address[2] = 0;
    AddrPrefix.au1Address[1] = 0;
    AddrPrefix.au1Address[0] = 2;

    pPeerInfo = Bgp4SnmphGetPeerEntry (0, AddrPrefix);

    CapsInfo1.SupCapability.u1CapCode = CAP_CODE_ORF;
    CapsInfo1.SupCapability.u1CapLength = BGP_ORF_CAPS_LEN;

    MEMSET (&OrfCapsValue, 0, sizeof (tOrfCapsValue));

    OrfCapsValue.u2Afi = OSIX_HTONS (2);
    OrfCapsValue.u1Safi = 1;
    OrfCapsValue.u1OrfCount = 1;
    OrfCapsValue.u1OrfType = 1;
    OrfCapsValue.u1OrfMode = 1;

    MEMCPY (CapsInfo1.SupCapability.au1CapValue, &OrfCapsValue.u2Afi,
            sizeof (tOrfCapsValue));
    TMO_SLL_Init_Node (&(CapsInfo1.NextCapability));
    TMO_SLL_Add (BGP4_PEER_SUP_CAPS_LIST (pPeerInfo),
                 &(CapsInfo1.NextCapability));

    if (Bgp4SnmphGetOrfCapSupportStatus (0, AddrPrefix, 1, 1, 1, 1) !=
        BGP4_FALSE)
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Delete (BGP4_PEER_SUP_CAPS_LIST (pPeerInfo),
                    &(CapsInfo1.NextCapability));
    CapsInfo2.SupCapability.u1CapCode = CAP_CODE_ORF;
    CapsInfo2.SupCapability.u1CapLength = BGP_ORF_CAPS_LEN;

    OrfCapsValue.u2Afi = OSIX_HTONS (1);
    OrfCapsValue.u1Safi = 2;
    OrfCapsValue.u1OrfCount = 1;
    OrfCapsValue.u1OrfType = 1;
    OrfCapsValue.u1OrfMode = 1;

    MEMCPY (CapsInfo2.SupCapability.au1CapValue, &OrfCapsValue.u2Afi,
            sizeof (tOrfCapsValue));
    TMO_SLL_Init_Node (&(CapsInfo2.NextCapability));
    TMO_SLL_Add (BGP4_PEER_SUP_CAPS_LIST (pPeerInfo),
                 &(CapsInfo2.NextCapability));

    if (Bgp4SnmphGetOrfCapSupportStatus (0, AddrPrefix, 1, 1, 1, 1) !=
        BGP4_FALSE)
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Delete (BGP4_PEER_SUP_CAPS_LIST (pPeerInfo),
                    &(CapsInfo2.NextCapability));

    OrfCapsValue.u2Afi = OSIX_HTONS (1);
    OrfCapsValue.u1Safi = 1;
    OrfCapsValue.u1OrfCount = 1;
    OrfCapsValue.u1OrfType = 2;
    OrfCapsValue.u1OrfMode = 1;

    MEMCPY (CapsInfo1.SupCapability.au1CapValue, &OrfCapsValue.u2Afi,
            sizeof (tOrfCapsValue));
    TMO_SLL_Init_Node (&(CapsInfo1.NextCapability));
    TMO_SLL_Add (BGP4_PEER_SUP_CAPS_LIST (pPeerInfo),
                 &(CapsInfo1.NextCapability));

    if (Bgp4SnmphGetOrfCapSupportStatus (0, AddrPrefix, 1, 1, 1, 1) !=
        BGP4_FALSE)
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Delete (BGP4_PEER_SUP_CAPS_LIST (pPeerInfo),
                    &(CapsInfo1.NextCapability));

    if (Bgp4SnmphGetOrfCapSupportStatus (0, AddrPrefix, 1, 1, 1, 1) !=
        BGP4_FALSE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 12.0.0.2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("sleep 1");
    MGMT_LOCK ();
    CliGiveAppContext ();

    if (Bgp4SnmphOrfCapabilityEnable (0, AddrPrefix, 1, 1, 1, 1) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_3 (VOID)
{
    UINT4               u4Val = 0;

    if (Bgp4OrfGetOrfCapMask (0x00020001, BGP4_CLI_ORF_MODE_RECEIVE, &u4Val) !=
        BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (Bgp4OrfGetOrfCapMask (0x00020001, BGP4_CLI_ORF_MODE_SEND, &u4Val) !=
        BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (Bgp4OrfGetOrfCapMask (0xffffffff, BGP4_CLI_ORF_MODE_RECEIVE, &u4Val) !=
        BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_4 (VOID)
{
    tBgp4PeerEntry      PeerEntry;
    tBgp4OrfEntry      *pOrfEntry;

    MEMSET (&PeerEntry, 0, sizeof (tBgp4PeerEntry));

    pOrfEntry = (tBgp4OrfEntry *) MemAllocMemBlk (BGP4_ORF_MEM_POOL_ID);
    MEMSET (pOrfEntry, 0, sizeof (tBgp4OrfEntry));

    PeerEntry.pBgpCxtNode = gBgpCxtNode[0];
    BgpOrfDelRcvdOrfEntries (&PeerEntry);

    pOrfEntry->u1Match = BGP4_ORF_DENY;
    RBTreeAdd (BGP4_ORF_ENTRY_TABLE (0), (tRBElem *) pOrfEntry);
    BgpOrfDelRcvdOrfEntries (&PeerEntry);

    pOrfEntry = (tBgp4OrfEntry *) MemAllocMemBlk (BGP4_ORF_MEM_POOL_ID);
    MEMSET (pOrfEntry, 0, sizeof (tBgp4OrfEntry));

    pOrfEntry->PeerAddr.au1Address[0] = 1;
    RBTreeAdd (BGP4_ORF_ENTRY_TABLE (0), (tRBElem *) pOrfEntry);
    BgpOrfDelRcvdOrfEntries (&PeerEntry);
    RBTreeRemove (BGP4_ORF_ENTRY_TABLE (0), (tRBElem *) pOrfEntry);
    MemReleaseMemBlock (BGP4_ORF_MEM_POOL_ID, (UINT1 *) pOrfEntry);

    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_5 (VOID)
{
#ifdef ROUTEMAP_WANTED
    tBgp4PeerEntry      Peer;
    tFilteringRMap      IpPrefix;
    UINT1               au1Buf[200];

    MEMSET (au1Buf, 0, sizeof (au1Buf));
    MEMSET (&Peer, 0, sizeof (tBgp4PeerEntry));
    MEMSET (&IpPrefix, 0, sizeof (tFilteringRMap));
    Peer.IpPrefixFilterIn = IpPrefix;
    if (BgpOrfFillOrfEntries (&Peer, au1Buf, 1) != 23)
    {
        return OSIX_FAILURE;
    }
    Peer.u1IsOrfSent = BGP4_TRUE;
    if (BgpOrfFillOrfEntries (&Peer, au1Buf, 1) != 28)
    {
        return OSIX_FAILURE;
    }
    Peer.IpPrefixFilterIn.u1Status = FILTERNIG_STAT_ENABLE;
    if (BgpOrfFillOrfEntries (&Peer, au1Buf, 1) != 27)
    {
        return OSIX_FAILURE;
    }
    Peer.u1IsOrfSent = BGP4_TRUE;
    MEMCPY (Peer.IpPrefixFilterIn.au1DistInOutFilterRMapName, "abc", 3);

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip prefix-list abc permit 0.0.0.0/0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    if (BgpOrfFillOrfEntries (&Peer, au1Buf, 1) != 67)
    {
        return OSIX_FAILURE;
    }
    if (BgpOrfFillOrfEntries (&Peer, au1Buf, 2) != 55)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no ip prefix-list abc permit 0.0.0.0/0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();
#endif
    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_6 (VOID)
{
    tNetAddress         NetAddr;
    tBgp4OrfEntry       OrfEntry;

    MEMSET (&NetAddr, 0, sizeof (tNetAddress));
    MEMSET (&OrfEntry, 0, sizeof (tBgp4OrfEntry));

    if (BgpOrfIsAddrPrefixMatching (&NetAddr, &OrfEntry) != BGP4_TRUE)
    {
        return OSIX_FAILURE;
    }

    NetAddr.NetAddr.u2Afi = 1;
    if (BgpOrfIsAddrPrefixMatching (&NetAddr, &OrfEntry) != BGP4_FALSE)
    {
        return OSIX_FAILURE;
    }
    NetAddr.NetAddr.u2Afi = 0;
    OrfEntry.u1PrefixLen = 1;
    if (BgpOrfIsAddrPrefixMatching (&NetAddr, &OrfEntry) != BGP4_FALSE)
    {
        return OSIX_FAILURE;
    }
    OrfEntry.u1MinPrefixLen = 1;
    OrfEntry.u1PrefixLen = 0;
    if (BgpOrfIsAddrPrefixMatching (&NetAddr, &OrfEntry) != BGP4_FALSE)
    {
        return OSIX_FAILURE;
    }
    NetAddr.u2PrefixLen = 2;
    if (BgpOrfIsAddrPrefixMatching (&NetAddr, &OrfEntry) != BGP4_TRUE)
    {
        return OSIX_FAILURE;
    }
    OrfEntry.u1MinPrefixLen = 0;
    OrfEntry.u1MaxPrefixLen = 1;
    if (BgpOrfIsAddrPrefixMatching (&NetAddr, &OrfEntry) != BGP4_FALSE)
    {
        return OSIX_FAILURE;
    }
    OrfEntry.u1MaxPrefixLen = 3;
    OrfEntry.u1PrefixLen = 3;
    if (BgpOrfIsAddrPrefixMatching (&NetAddr, &OrfEntry) != BGP4_FALSE)
    {
        return OSIX_FAILURE;
    }
    NetAddr.u2PrefixLen = 3;
    if (BgpOrfIsAddrPrefixMatching (&NetAddr, &OrfEntry) != BGP4_TRUE)

    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_7 (VOID)
{
    tBgp4OrfEntry       OrfEntry;
    tBgp4OrfEntry      *pOrfEntry = NULL;
    tBgp4RBTree         OrfTable;

    MEMSET (&OrfTable, 0, sizeof (tBgp4RBTree));

    MEMSET (&OrfEntry, 0, sizeof (tBgp4OrfEntry));
    pOrfEntry = MemAllocMemBlk (BGP4_ORF_MEM_POOL_ID);
    MEMSET (pOrfEntry, 0, sizeof (tBgp4OrfEntry));

    if (BgpOrfProcessRcvdOrfEntry (&OrfEntry, BGP4_ORF_REMOVE) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    RBTreeAdd (gBgpCxtNode[0]->OrfTable, (tRBElem *) pOrfEntry);
    pOrfEntry->u1IsStatic = BGP4_TRUE;
    if (BgpOrfProcessRcvdOrfEntry (&OrfEntry, BGP4_ORF_REMOVE) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    pOrfEntry->u1IsStatic = BGP4_FALSE;
    if (BgpOrfProcessRcvdOrfEntry (&OrfEntry, BGP4_ORF_REMOVE) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    OrfEntry.u4SeqNo = 1;
    if (BgpOrfProcessRcvdOrfEntry (&OrfEntry, BGP4_ORF_ADD) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (BgpOrfProcessRcvdOrfEntry (&OrfEntry, BGP4_ORF_ADD) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    OrfEntry.u4SeqNo = 0;
    if (BgpOrfProcessRcvdOrfEntry (&OrfEntry, BGP4_ORF_REMOVE_ALL) !=
        BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (BgpOrfProcessRcvdOrfEntry (&OrfEntry, BGP4_ORF_REMOVE_ALL) !=
        BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    pOrfEntry = MemAllocMemBlk (BGP4_ORF_MEM_POOL_ID);
    MEMSET (pOrfEntry, 0, sizeof (tBgp4OrfEntry));
    RBTreeAdd (gBgpCxtNode[0]->OrfTable, (tRBElem *) pOrfEntry);
    pOrfEntry->u1IsStatic = BGP4_TRUE;
    pOrfEntry->u4SeqNo = 1;
    if (BgpOrfProcessRcvdOrfEntry (&OrfEntry, BGP4_ORF_REMOVE_ALL) !=
        BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    pOrfEntry->u1OrfType = 1;
    if (BgpOrfProcessRcvdOrfEntry (&OrfEntry, BGP4_ORF_REMOVE_ALL) !=
        BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    pOrfEntry->u1OrfType = 0;
    pOrfEntry->u2Afi = 1;
    if (BgpOrfProcessRcvdOrfEntry (&OrfEntry, BGP4_ORF_REMOVE_ALL) !=
        BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    pOrfEntry->u2Afi = 0;
    pOrfEntry->u1Safi = 1;
    if (BgpOrfProcessRcvdOrfEntry (&OrfEntry, BGP4_ORF_REMOVE_ALL) !=
        BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    pOrfEntry->PeerAddr.u2Afi = 1;
    if (BgpOrfProcessRcvdOrfEntry (&OrfEntry, BGP4_ORF_REMOVE_ALL) !=
        BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    RBTreeRem (gBgpCxtNode[0]->OrfTable, (tRBElem *) pOrfEntry);
    MemReleaseMemBlock (BGP4_ORF_MEM_POOL_ID, (UINT1 *) pOrfEntry);

    OrfTable = gBgpCxtNode[0]->OrfTable;
    MEMSET (&(gBgpCxtNode[0]->OrfTable), 0, sizeof (tBgp4RBTree));

    if (BgpOrfProcessRcvdOrfEntry (&OrfEntry, BGP4_ORF_ADD) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (&(gBgpCxtNode[0]->OrfTable), &OrfTable, sizeof (tBgp4RBTree));
    AllocMem (BGP4_ORF_MEM_POOL_ID);
    if (BgpOrfProcessRcvdOrfEntry (&OrfEntry, BGP4_ORF_ADD) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    ReleaseMem (BGP4_ORF_MEM_POOL_ID);
    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_8 (VOID)
{
    tBgp4PeerEntry      PeerEntry;
    UINT1               au1Buf[100];

    MEMSET (&au1Buf, 0, sizeof (au1Buf));
    /* Invalid buffer length */
    if (Bgp4OrfMsgRcvdHandler (&PeerEntry, au1Buf, 3, 0, 0) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    /* Invalid when-to-refresh field */
    if (Bgp4OrfMsgRcvdHandler (&PeerEntry, au1Buf, 5, 0, 0) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    /* Invalid ORF length */
    au1Buf[0] = BGP4_ORF_DEFER;
    if (Bgp4OrfMsgRcvdHandler (&PeerEntry, au1Buf, 5, 0, 0) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    au1Buf[0] = BGP4_ORF_IMMEDIATE;
    au1Buf[3] = 1;
    /* invalid ORF type */
    if (Bgp4OrfMsgRcvdHandler (&PeerEntry, au1Buf, 5, 0, 0) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    au1Buf[1] = BGP_ORF_TYPE_ADDRESS_PREFIX;
    PeerEntry.pBgpCxtNode = gBgpCxtNode[0];
    au1Buf[4] = 0xC0;
    /* Invalid ACTION field */
    if (Bgp4OrfMsgRcvdHandler (&PeerEntry, au1Buf, 5, 0, 0) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    au1Buf[4] = 0x80;
    /*  ACTION REMOVE_ALL */
    if (Bgp4OrfMsgRcvdHandler (&PeerEntry, au1Buf, 5, 0, 0) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    au1Buf[0] = BGP4_ORF_DEFER;
    au1Buf[3] = 13;                /* length of both ORF entries */
    au1Buf[5] = 0x00;            /* Action fr 2nd ORF entry ADD */
    au1Buf[10] = 1;                /* Min length */
    au1Buf[12] = 1;                /*  Prefix length */
    /* Two ORF entries, second one with ACTION add */
    if (Bgp4OrfMsgRcvdHandler (&PeerEntry, au1Buf, 17, 0, 0) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    au1Buf[5] = 0x40;            /* Action fr 2nd ORF entry REMOVE */
    au1Buf[10] = 2;                /* Min length */
    au1Buf[11] = 1;                /* Max length */
    au1Buf[12] = 1;                /*  Prefix length */
    if (Bgp4OrfMsgRcvdHandler (&PeerEntry, au1Buf, 17, 0, 0) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    au1Buf[10] = 0;                /* Min length */
    au1Buf[11] = 33;            /* Max length */
    /* Two ORF entries, second one with Invalid max prefix length */
    if (Bgp4OrfMsgRcvdHandler (&PeerEntry, au1Buf, 17, 1, 0) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }

    au1Buf[12] = 0;                /*  Prefix length */
    au1Buf[10] = 33;            /* Min length */
    au1Buf[11] = 0;                /* Max length */
    /* Two ORF entries, second one with Invalid min prefix length */
    if (Bgp4OrfMsgRcvdHandler (&PeerEntry, au1Buf, 17, 1, 0) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    au1Buf[11] = 2;                /* Max length */
    au1Buf[10] = 3;                /* Min length */
    /* Two ORF entries, second one with MIN prefix greatre than max prefix length */
    if (Bgp4OrfMsgRcvdHandler (&PeerEntry, au1Buf, 17, 1, 0) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    au1Buf[11] = 3;                /* Max length */
    au1Buf[10] = 2;                /* Min length */
    /* Two ORF entries, with proper values */
    if (Bgp4OrfMsgRcvdHandler (&PeerEntry, au1Buf, 17, 1, 0) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    au1Buf[11] = 130;            /* Max length */
    /* Two ORF entries, second one with invalid Max prefix length */
    if (Bgp4OrfMsgRcvdHandler (&PeerEntry, au1Buf, 17, 2, 0) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    au1Buf[11] = 0;                /* Max length */
    au1Buf[10] = 130;            /* Min length */
    /* Two ORF entries, second one with invalid Min prefix length */
    if (Bgp4OrfMsgRcvdHandler (&PeerEntry, au1Buf, 17, 2, 0) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    au1Buf[10] = 0;                /* Min length */
    /* Two ORF entries,  with proper values */
    if (Bgp4OrfMsgRcvdHandler (&PeerEntry, au1Buf, 17, 2, 0) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    au1Buf[0] = BGP4_ORF_IMMEDIATE;
    if (Bgp4OrfMsgRcvdHandler (&PeerEntry, au1Buf, 17, 3, 0) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_9 (VOID)
{
    tAddrPrefix         AddrPrefix;
    tBgp4PeerEntry     *pPeerEntry = NULL;

    MEMSET (&AddrPrefix, 0, sizeof (tAddrPrefix));

    if (Bgp4OrfRequestHandler (0, AddrPrefix, 1, 1) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 12.0.0.2 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("sleep 1");
    MGMT_LOCK ();
    CliGiveAppContext ();

    if (Bgp4OrfRequestHandler (0, AddrPrefix, 1, 0) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    AddrPrefix.u2Afi = 1;
    AddrPrefix.u2AddressLen = 4;
    AddrPrefix.au1Address[3] = 12;
    AddrPrefix.au1Address[2] = 0;
    AddrPrefix.au1Address[1] = 0;
    AddrPrefix.au1Address[0] = 2;

    if (Bgp4OrfRequestHandler (0, AddrPrefix, 0, 0) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (Bgp4OrfRequestHandler (0, AddrPrefix, 1, 1) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (Bgp4OrfRequestHandler (0, AddrPrefix, 2, 1) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    AddrPrefix.au1Address[0] = 1;
    if (Bgp4OrfRequestHandler (0, AddrPrefix, 1, 1) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    AddrPrefix.au1Address[0] = 2;
    if (Bgp4OrfRequestHandler (0, AddrPrefix, 1, 0) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    MEMSET (&AddrPrefix, 0, sizeof (tAddrPrefix));
    AddrPrefix.u2Afi = 1;
    if (Bgp4OrfRequestHandler (0, AddrPrefix, 1, 1) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (0), pPeerEntry, tBgp4PeerEntry *)
    {
        pPeerEntry->peerStatus.u1PeerState = BGP4_ESTABLISHED_STATE;
    }

    if (Bgp4OrfRequestHandler (0, AddrPrefix, 0, 0) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#ifdef BGP4_IPV6_WANTED
    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (0), pPeerEntry, tBgp4PeerEntry *)
    {
        BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) = (CAP_NEG_IPV6_UNI_MASK);
    }
    if (Bgp4OrfRequestHandler (0, AddrPrefix, 1, 1) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#endif
    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (0), pPeerEntry, tBgp4PeerEntry *)
    {
        BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) = (CAP_NEG_IPV4_UNI_MASK);
    }
    if (Bgp4OrfRequestHandler (0, AddrPrefix, 1, 0) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    AllocMem (gBgpNode.Bgp4BufNodeMsgPoolId);
    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (0), pPeerEntry, tBgp4PeerEntry *)
    {
        BGP4_PEER_NEG_CAP_MASK (pPeerEntry) = CAP_NEG_ORF_IPV4_UNICAST_SEND;
    }
    if (Bgp4OrfRequestHandler (0, AddrPrefix, 1, 1) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (0), pPeerEntry, tBgp4PeerEntry *)
    {
        BGP4_GET_PEER_CURRENT_STATE (pPeerEntry) =
            BGP4_PEER_SOFTCONFIG_INBOUND_INPROGRESS;
    }
    if (Bgp4OrfRequestHandler (0, AddrPrefix, 1, 1) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (0), pPeerEntry, tBgp4PeerEntry *)
    {
        BGP4_GET_PEER_CURRENT_STATE (pPeerEntry) =
            BGP4_PEER_SOFTCONFIG_OUTBOUND_INPROGRESS;
    }
    if (Bgp4OrfRequestHandler (0, AddrPrefix, 1, 1) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (0), pPeerEntry, tBgp4PeerEntry *)
    {
        BGP4_GET_PEER_CURRENT_STATE (pPeerEntry) = BGP4_PEER_REUSE_INPROGRESS;
    }
    if (Bgp4OrfRequestHandler (0, AddrPrefix, 1, 1) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    ReleaseMem (gBgpNode.Bgp4BufNodeMsgPoolId);
    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (0), pPeerEntry, tBgp4PeerEntry *)
    {
        BGP4_GET_PEER_CURRENT_STATE (pPeerEntry) = BGP4_PEER_INIT_INPROGRESS;
    }
    if (Bgp4OrfRequestHandler (0, AddrPrefix, 1, 1) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (0), pPeerEntry, tBgp4PeerEntry *)
    {
        BGP4_GET_PEER_CURRENT_STATE (pPeerEntry) = BGP4_PEER_DEINIT_INPROGRESS;
    }
    if (Bgp4OrfRequestHandler (0, AddrPrefix, 1, 1) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    AddrPrefix.u2Afi = 1;
    AddrPrefix.u2AddressLen = 4;
    AddrPrefix.au1Address[3] = 12;
    AddrPrefix.au1Address[2] = 0;
    AddrPrefix.au1Address[1] = 0;
    AddrPrefix.au1Address[0] = 2;

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (0), pPeerEntry, tBgp4PeerEntry *)
    {
        BGP4_GET_PEER_CURRENT_STATE (pPeerEntry) = 0xfffff;
    }
    if (Bgp4OrfRequestHandler (0, AddrPrefix, 1, 1) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 12.0.0.2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("sleep 1");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_10 (VOID)
{
    tBgp4OrfEntry       Orf1;
    tBgp4OrfEntry       Orf2;

    MEMSET (&Orf1, 0, sizeof (tBgp4OrfEntry));
    MEMSET (&Orf2, 0, sizeof (tBgp4OrfEntry));

    BGP4_AFI_IN_ADDR_PREFIX_INFO (Orf1.PeerAddr) = 1;
    if (BgpOrfListTableRBCmp ((tBgp4RBElem *) & Orf1, (tBgp4RBElem *) & Orf2) !=
        1)
    {
        return OSIX_FAILURE;
    }
    BGP4_AFI_IN_ADDR_PREFIX_INFO (Orf2.PeerAddr) = 1;
    Orf1.PeerAddr.au1Address[0] = 1;
    if (BgpOrfListTableRBCmp ((tBgp4RBElem *) & Orf1, (tBgp4RBElem *) & Orf2) !=
        1)
    {
        return OSIX_FAILURE;
    }
    Orf1.PeerAddr.au1Address[0] = 0;
    Orf1.u2Afi = 1;
    if (BgpOrfListTableRBCmp ((tBgp4RBElem *) & Orf1, (tBgp4RBElem *) & Orf2) !=
        1)
    {
        return OSIX_FAILURE;
    }
    Orf1.u2Afi = 0;
    Orf1.u1Safi = 1;
    if (BgpOrfListTableRBCmp ((tBgp4RBElem *) & Orf1, (tBgp4RBElem *) & Orf2) !=
        1)
    {
        return OSIX_FAILURE;
    }
    Orf1.u1Safi = 0;
    Orf1.u1OrfType = 1;
    if (BgpOrfListTableRBCmp ((tBgp4RBElem *) & Orf1, (tBgp4RBElem *) & Orf2) !=
        1)
    {
        return OSIX_FAILURE;
    }
    Orf1.u1OrfType = 0;
    Orf1.u4SeqNo = 1;
    if (BgpOrfListTableRBCmp ((tBgp4RBElem *) & Orf1, (tBgp4RBElem *) & Orf2) !=
        1)
    {
        return OSIX_FAILURE;
    }
    Orf1.u4SeqNo = 0;
    Orf1.AddrPrefix.au1Address[0] = 1;
    if (BgpOrfListTableRBCmp ((tBgp4RBElem *) & Orf1, (tBgp4RBElem *) & Orf2) !=
        1)
    {
        return OSIX_FAILURE;
    }
    Orf1.AddrPrefix.au1Address[0] = 0;
    Orf2.AddrPrefix.au1Address[0] = 1;
    if (BgpOrfListTableRBCmp ((tBgp4RBElem *) & Orf1, (tBgp4RBElem *) & Orf2) !=
        -1)
    {
        return OSIX_FAILURE;
    }
    Orf2.AddrPrefix.au1Address[0] = 0;
    Orf1.u1PrefixLen = 1;
    if (BgpOrfListTableRBCmp ((tBgp4RBElem *) & Orf1, (tBgp4RBElem *) & Orf2) !=
        1)
    {
        return OSIX_FAILURE;
    }
    Orf1.u1PrefixLen = 0;
    Orf2.u1PrefixLen = 1;
    if (BgpOrfListTableRBCmp ((tBgp4RBElem *) & Orf1, (tBgp4RBElem *) & Orf2) !=
        -1)
    {
        return OSIX_FAILURE;
    }
    Orf2.u1PrefixLen = 0;
    Orf1.u1MinPrefixLen = 1;
    if (BgpOrfListTableRBCmp ((tBgp4RBElem *) & Orf1, (tBgp4RBElem *) & Orf2) !=
        1)
    {
        return OSIX_FAILURE;
    }
    Orf1.u1MinPrefixLen = 0;
    Orf2.u1MinPrefixLen = 1;
    if (BgpOrfListTableRBCmp ((tBgp4RBElem *) & Orf1, (tBgp4RBElem *) & Orf2) !=
        -1)
    {
        return OSIX_FAILURE;
    }
    Orf2.u1MinPrefixLen = 0;
    Orf1.u1MaxPrefixLen = 1;
    if (BgpOrfListTableRBCmp ((tBgp4RBElem *) & Orf1, (tBgp4RBElem *) & Orf2) !=
        1)
    {
        return OSIX_FAILURE;
    }
    Orf1.u1MaxPrefixLen = 0;
    Orf2.u1MaxPrefixLen = 1;
    if (BgpOrfListTableRBCmp ((tBgp4RBElem *) & Orf1, (tBgp4RBElem *) & Orf2) !=
        -1)
    {
        return OSIX_FAILURE;
    }
    Orf2.u1MaxPrefixLen = 0;
    Orf1.u1Match = 1;
    if (BgpOrfListTableRBCmp ((tBgp4RBElem *) & Orf1, (tBgp4RBElem *) & Orf2) !=
        1)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_11 (VOID)
{
    tAddrPrefix         Addr;
    tBgp4OrfEntry       OrfEntry;

    MEMSET (&Addr, 0, sizeof (tAddrPrefix));
    MEMSET (&OrfEntry, 0, sizeof (tBgp4OrfEntry));

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    OrfEntry.u4SeqNo = 1;
    RBTreeAdd (BGP4_ORF_ENTRY_TABLE (0), (tRBElem *) & OrfEntry);
    Bgp4CliShowRcvdOrfFilters (0, &Addr);

    OrfEntry.PeerAddr.u2Afi = 1;
    Addr.u2Afi = 1;
    Bgp4CliShowRcvdOrfFilters (0, &Addr);

    OrfEntry.PeerAddr.au1Address[0] = 2;
    Addr.au1Address[0] = 1;
    Bgp4CliShowRcvdOrfFilters (0, &Addr);

    Addr.au1Address[0] = 0;
    OrfEntry.u1Match = BGP4_ORF_DENY;
    OrfEntry.u2Afi = 1;
    OrfEntry.PeerAddr.au1Address[0] = 0;
    OrfEntry.PeerAddr.u2Afi = 1;
    OrfEntry.u1MinPrefixLen = 1;
    OrfEntry.u1MaxPrefixLen = 1;

    Bgp4CliShowRcvdOrfFilters (1, &Addr);
    RBTreeRem (BGP4_ORF_ENTRY_TABLE (0), (tRBElem *) & OrfEntry);
    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_12 (VOID)
{
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    INT4                i4Val = 0;
    UINT4               u4Err = 0;

    if (nmhGetFsMIBgp4mpeRtRefreshInboundPrefixFilter
        (0x0fffffff, 1, &PeerAddr, 1, 1, &i4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIBgp4mpeRtRefreshInboundPrefixFilter
        (0x0fffffff, 1, &PeerAddr, 1, 1, i4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIBgp4mpeRtRefreshInboundPrefixFilter
        (&u4Err, 0x0fffffff, 1, &PeerAddr, 1, 1, i4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gBgpNode.u1Bgp4TaskInit = BGP4_FALSE;
    if (nmhGetFsMIBgp4mpeRtRefreshInboundPrefixFilter (0, 1, &PeerAddr, 1, 1,
                                                       &i4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIBgp4mpeRtRefreshInboundPrefixFilter (0, 1, &PeerAddr, 1, 1,
                                                       i4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIBgp4mpeRtRefreshInboundPrefixFilter
        (&u4Err, 0, 1, &PeerAddr, 1, 1, i4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gBgpNode.u1Bgp4TaskInit = BGP4_TRUE;
    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_13 (VOID)
{
    tSNMP_OCTET_STRING_TYPE String;
    UINT4               u4Err = 0;

    if (nmhGetFsMIBgp4mpePeerIpPrefixNameIn (0x0fffffff, 1, &String, &String) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsMIBgp4mpePeerIpPrefixNameOut (0x0fffffff, 1, &String, &String)
        != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIBgp4mpePeerIpPrefixNameIn (0x0fffffff, 1, &String, &String) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIBgp4mpePeerIpPrefixNameOut (0x0fffffff, 1, &String, &String)
        != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIBgp4mpePeerIpPrefixNameIn
        (&u4Err, 0x0fffffff, 1, &String, &String) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIBgp4mpePeerIpPrefixNameOut
        (&u4Err, 0x0fffffff, 1, &String, &String) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gBgpNode.u1Bgp4TaskInit = BGP4_FALSE;
    if (nmhGetFsMIBgp4mpePeerIpPrefixNameIn (0, 1, &String, &String) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsMIBgp4mpePeerIpPrefixNameOut (0, 1, &String, &String) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIBgp4mpePeerIpPrefixNameIn (0, 1, &String, &String) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIBgp4mpePeerIpPrefixNameOut (0, 1, &String, &String) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIBgp4mpePeerIpPrefixNameIn (&u4Err, 0, 1, &String, &String)
        != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIBgp4mpePeerIpPrefixNameOut (&u4Err, 0, 1, &String, &String)
        != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gBgpNode.u1Bgp4TaskInit = BGP4_TRUE;
    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_14 (VOID)
{
    tSNMP_OCTET_STRING_TYPE String;
    UINT4               u4Err = 0;

    if (nmhGetFsMIBgp4PeerGroupIpPrefixNameIn (0x0fffffff, &String, &String) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsMIBgp4PeerGroupIpPrefixNameOut (0x0fffffff, &String, &String) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIBgp4PeerGroupIpPrefixNameIn (0x0fffffff, &String, &String) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIBgp4PeerGroupIpPrefixNameOut (0x0fffffff, &String, &String) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIBgp4PeerGroupIpPrefixNameIn
        (&u4Err, 0x0fffffff, &String, &String) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIBgp4PeerGroupIpPrefixNameOut
        (&u4Err, 0x0fffffff, &String, &String) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gBgpNode.u1Bgp4TaskInit = BGP4_FALSE;
    if (nmhGetFsMIBgp4PeerGroupIpPrefixNameIn (0, &String, &String) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsMIBgp4PeerGroupIpPrefixNameOut (0, &String, &String) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIBgp4PeerGroupIpPrefixNameIn (0, &String, &String) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIBgp4PeerGroupIpPrefixNameOut (0, &String, &String) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIBgp4PeerGroupIpPrefixNameIn (&u4Err, 0, &String, &String)
        != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIBgp4PeerGroupIpPrefixNameOut (&u4Err, 0, &String, &String)
        != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gBgpNode.u1Bgp4TaskInit = BGP4_TRUE;
    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_15 (VOID)
{
    tSNMP_OCTET_STRING_TYPE String;
    UINT4               u4Err = 0;
    INT4                u4Val = 0;

    if (nmhGetFsMIBgp4PeerGroupOrfType (0x0fffffff, &String, (UINT4 *) &u4Val)
        != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsMIBgp4PeerGroupOrfCapMode (0x0fffffff, &String, &u4Val) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsMIBgp4PeerGroupOrfRequest (0x0fffffff, &String, &u4Val) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsMIBgp4PeerGroupOrfType (0x0fffffff, &String, u4Val) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIBgp4PeerGroupOrfCapMode (0x0fffffff, &String, u4Val) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIBgp4PeerGroupOrfRequest (0x0fffffff, &String, u4Val) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsMIBgp4PeerGroupOrfType (&u4Err, 0x0fffffff, &String, u4Val)
        != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIBgp4PeerGroupOrfCapMode
        (&u4Err, 0x0fffffff, &String, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIBgp4PeerGroupOrfRequest
        (&u4Err, 0x0fffffff, &String, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gBgpNode.u1Bgp4TaskInit = BGP4_FALSE;
    if (nmhGetFsMIBgp4PeerGroupOrfType (0, &String, (UINT4 *) &u4Val) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsMIBgp4PeerGroupOrfCapMode (0, &String, &u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsMIBgp4PeerGroupOrfRequest (0, &String, &u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsMIBgp4PeerGroupOrfType (0, &String, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIBgp4PeerGroupOrfCapMode (0, &String, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIBgp4PeerGroupOrfRequest (0, &String, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsMIBgp4PeerGroupOrfType (&u4Err, 0, &String, u4Val) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIBgp4PeerGroupOrfCapMode (&u4Err, 0, &String, u4Val) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIBgp4PeerGroupOrfRequest (&u4Err, 0, &String, u4Val) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gBgpNode.u1Bgp4TaskInit = BGP4_TRUE;
    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_16 (VOID)
{
    tSNMP_OCTET_STRING_TYPE Addr;

    if (nmhValidateIndexInstanceFsMIBgp4ORFListTable (0x0fffffff, 1, &Addr,
                                                      1, 1, 1, 1, &Addr, 8, 0,
                                                      0, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gBgpNode.u1Bgp4TaskInit = BGP4_FALSE;

    if (nmhValidateIndexInstanceFsMIBgp4ORFListTable (0, 1, &Addr,
                                                      1, 1, 1, 1, &Addr, 8, 0,
                                                      0, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gBgpNode.u1Bgp4TaskInit = BGP4_TRUE;
    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_17 (VOID)
{
    tBgpCxtNode       **BgpCxtNode = NULL;
    tSNMP_OCTET_STRING_TYPE Addr;
    tBgp4OrfEntry       OrfEntry;
    UINT4               u4Val = 0;
    INT4                i4Val = 0;

    BgpCxtNode = gBgpCxtNode;
    gBgpCxtNode = NULL;
    if (nmhGetFirstIndexFsMIBgp4ORFListTable
        (&i4Val, &i4Val, &Addr, &i4Val, &i4Val, &u4Val, &u4Val, &Addr, &u4Val,
         &u4Val, &u4Val, &i4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gBgpCxtNode = BgpCxtNode;

    if (nmhGetFirstIndexFsMIBgp4ORFListTable
        (&i4Val, &i4Val, &Addr, &i4Val, &i4Val, &u4Val, &u4Val, &Addr, &u4Val,
         &u4Val, &u4Val, &i4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&OrfEntry, 0, sizeof (tBgp4OrfEntry));
    RBTreeAdd (BGP4_ORF_ENTRY_TABLE (0), (tRBElem *) & OrfEntry);

    if (nmhGetFirstIndexFsMIBgp4ORFListTable
        (&i4Val, &i4Val, &Addr, &i4Val, &i4Val, &u4Val, &u4Val, &Addr, &u4Val,
         &u4Val, &u4Val, &i4Val) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    RBTreeRem (BGP4_ORF_ENTRY_TABLE (0), (tRBElem *) & OrfEntry);

    FsBGPSizingParams[MAX_BGP_CXT_NODES_SIZING_ID].u4PreAllocatedUnits = 2;
    gBgpCxtNode[1] = gBgpCxtNode[0];
    if (nmhGetFirstIndexFsMIBgp4ORFListTable
        (&i4Val, &i4Val, &Addr, &i4Val, &i4Val, &u4Val, &u4Val, &Addr, &u4Val,
         &u4Val, &u4Val, &i4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    FsBGPSizingParams[MAX_BGP_CXT_NODES_SIZING_ID].u4PreAllocatedUnits = 1;
    gBgpCxtNode[1] = NULL;
    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_18 (VOID)
{
    tSNMP_OCTET_STRING_TYPE Addr;
    tBgp4OrfEntry       OrfEntry;
    UINT4               u4Val = 0;
    INT4                i4Val = 0;
    UINT1               au1Tmp[16];

    MEMSET (au1Tmp, 0, sizeof (au1Tmp));
    MEMSET (&Addr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhGetNextIndexFsMIBgp4ORFListTable
        (0x0fffffff, &i4Val, 1, &i4Val, &Addr, &Addr, 1, &i4Val, 1, &i4Val, 1,
         &u4Val, 1, &u4Val, &Addr, &Addr, 4, &u4Val, 0, &u4Val, 0, &u4Val, 0,
         &i4Val) != SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (nmhGetNextIndexFsMIBgp4ORFListTable (0, &i4Val, 0, &i4Val, &Addr,
                                             &Addr, 0, &i4Val, 0, &i4Val, 0,
                                             &u4Val, 1, &u4Val, &Addr, &Addr, 4,
                                             &u4Val, 0, &u4Val, 0, &u4Val, 0,
                                             &i4Val) != SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMSET (&OrfEntry, 0, sizeof (tBgp4OrfEntry));
    OrfEntry.u4SeqNo = 2;
    RBTreeAdd (BGP4_ORF_ENTRY_TABLE (0), (tRBElem *) & OrfEntry);
    Addr.pu1_OctetList = (UINT1 *) &au1Tmp;
    if (nmhGetNextIndexFsMIBgp4ORFListTable (0, &i4Val, 0, &i4Val, &Addr,
                                             &Addr, 0, &i4Val, 0, &i4Val, 0,
                                             &u4Val, 1, &u4Val, &Addr, &Addr, 4,
                                             &u4Val, 0, &u4Val, 0, &u4Val, 0,
                                             &i4Val) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    RBTreeRem (BGP4_ORF_ENTRY_TABLE (0), (tRBElem *) & OrfEntry);

    gBgpCxtNode[1] = gBgpCxtNode[0];
    FsBGPSizingParams[MAX_BGP_CXT_NODES_SIZING_ID].u4PreAllocatedUnits = 2;
    if (nmhGetNextIndexFsMIBgp4ORFListTable (0, &i4Val, 1, &i4Val, &Addr,
                                             &Addr, 1, &i4Val, 1, &i4Val, 1,
                                             &u4Val, 1, &u4Val, &Addr, &Addr, 4,
                                             &u4Val, 0, &u4Val, 0, &u4Val, 0,
                                             &i4Val) != SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    FsBGPSizingParams[MAX_BGP_CXT_NODES_SIZING_ID].u4PreAllocatedUnits = 1;
    gBgpCxtNode[1] = NULL;
    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_19 (VOID)
{
    tSNMP_OCTET_STRING_TYPE Addr;
    UINT1               au1Tmp[16];
    tBgp4PeerEntry      Peer;
    tAfiSafiSpecInfo    Afi;
    tRtRefreshData      Ref;

    MEMSET (&Ref, 0, sizeof (tRtRefreshData));
    MEMSET (&Afi, 0, sizeof (tAfiSafiSpecInfo));
    MEMSET (&Peer, 0, sizeof (tBgp4PeerEntry));
    MEMSET (au1Tmp, 0, sizeof (au1Tmp));
    MEMSET (&Addr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    if (nmhSetFsbgp4mpeRtRefreshInboundPrefixFilter (0, &Addr, 1, 1, 1) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    Addr.pu1_OctetList = (UINT1 *) &au1Tmp;
    if (nmhSetFsbgp4mpeRtRefreshInboundPrefixFilter (1, &Addr, 0, 0, 1) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    AllocMem (gBgpNode.Bgp4InputQPoolId);
    if (nmhSetFsbgp4mpeRtRefreshInboundPrefixFilter (1, &Addr, 1, 1, 1) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    ReleaseMem (gBgpNode.Bgp4InputQPoolId);
    Addr.pu1_OctetList[0] = 1;
    if (nmhSetFsbgp4mpeRtRefreshInboundPrefixFilter (1, &Addr, 1, 1, 1) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    TMO_SLL_Add (BGP4_PEERENTRY_HEAD (0), (tTMO_SLL_NODE *) & Peer);
    BGP4_PEER_REMOTE_ADDR ((&Peer))[3] = 1;
    BGP4_PEER_REMOTE_ADDR_TYPE ((&Peer)) = 1;
    if (nmhSetFsbgp4mpeRtRefreshInboundPrefixFilter (1, &Addr, 1, 1, 1) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    Peer.apAsafiInstance[0] = &Afi;
    if (nmhSetFsbgp4mpeRtRefreshInboundPrefixFilter (1, &Addr, 1, 1, 1) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    Peer.apAsafiInstance[0]->pRtRefreshData = &Ref;
    AllocMem (gBgpNode.Bgp4InputQPoolId);
    if (nmhSetFsbgp4mpeRtRefreshInboundPrefixFilter (1, &Addr, 1, 1, 1) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    ReleaseMem (gBgpNode.Bgp4InputQPoolId);
    TMO_SLL_Delete (BGP4_PEERENTRY_HEAD (0), (tTMO_SLL_NODE *) & Peer);

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 12.0.0.2 remote-as 200");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("sleep 1");
    MGMT_LOCK ();
    CliGiveAppContext ();

    Addr.pu1_OctetList[0] = 12;
    Addr.pu1_OctetList[1] = 0;
    Addr.pu1_OctetList[2] = 0;
    Addr.pu1_OctetList[3] = 2;

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    if (nmhSetFsbgp4mpeRtRefreshInboundPrefixFilter (1, &Addr, 1, 1, 1) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 12.0.0.2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("sleep 1");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_20 (VOID)
{
    tSNMP_OCTET_STRING_TYPE Addr;
    UINT1               au1Tmp[16];
    UINT4               u4Err = 0;
    tBgp4PeerEntry      Peer;

    MEMSET (&Peer, 0, sizeof (tBgp4PeerEntry));
    MEMSET (au1Tmp, 0, sizeof (au1Tmp));
    MEMSET (&Addr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    gpBgpCurrCxtNode = gBgpCxtNode[0];

    Addr.pu1_OctetList = (UINT1 *) &au1Tmp;
    if (nmhTestv2Fsbgp4mpeRtRefreshInboundPrefixFilter
        (&u4Err, 0, &Addr, 0, 0, 1) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2Fsbgp4mpeRtRefreshInboundPrefixFilter
        (&u4Err, 1, &Addr, 0, 1, 1) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    Addr.pu1_OctetList[0] = 1;
    Addr.pu1_OctetList[1] = 1;
    Addr.pu1_OctetList[2] = 1;
    Addr.pu1_OctetList[3] = 1;
    if (nmhTestv2Fsbgp4mpeRtRefreshInboundPrefixFilter
        (&u4Err, 1, &Addr, 0, 1, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    Addr.pu1_OctetList[0] = 0;
    if (nmhTestv2Fsbgp4mpeRtRefreshInboundPrefixFilter
        (&u4Err, 1, &Addr, 0, 1, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    Addr.pu1_OctetList[0] = 1;
    if (nmhTestv2Fsbgp4mpeRtRefreshInboundPrefixFilter
        (&u4Err, 1, &Addr, 1, 0, 1) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    Peer.peerConfig.RemoteAddrInfo.u2Afi = 1;
    Peer.peerConfig.RemoteAddrInfo.u2AddressLen = 4;
    Peer.peerConfig.RemoteAddrInfo.au1Address[0] = 1;
    Peer.peerConfig.RemoteAddrInfo.au1Address[1] = 1;
    Peer.peerConfig.RemoteAddrInfo.au1Address[2] = 1;
    Peer.peerConfig.RemoteAddrInfo.au1Address[3] = 1;

    TMO_SLL_Add (BGP4_PEERENTRY_HEAD (0), (tTMO_SLL_NODE *) & Peer);
    if (nmhTestv2Fsbgp4mpeRtRefreshInboundPrefixFilter
        (&u4Err, 1, &Addr, 1, 1, 1) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    TMO_SLL_Delete (BGP4_PEERENTRY_HEAD (0), (tTMO_SLL_NODE *) & Peer);
    if (nmhTestv2Fsbgp4mpeRtRefreshInboundPrefixFilter
        (&u4Err, 1, &Addr, 3, 1, 1) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2Fsbgp4mpeRtRefreshInboundPrefixFilter
        (&u4Err, 1, &Addr, 1, 3, 1) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_21 (VOID)
{
    tSNMP_OCTET_STRING_TYPE PeerGrp;
    UINT1               au1Tmp[24];
    UINT4               u4Err = 0;
    INT4                i4Val = 0;

    MEMSET (au1Tmp, 0, sizeof (au1Tmp));
    MEMSET (&PeerGrp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    PeerGrp.pu1_OctetList = (UINT1 *) &au1Tmp;
    if (nmhTestv2FsBgp4PeerGroupOrfRequest (&u4Err, &PeerGrp, 1) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsBgp4PeerGroupOrfRequest (&PeerGrp, &i4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (au1Tmp, "abc", 3);
    PeerGrp.i4_Length = 3;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor abc peer-group");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    if (nmhTestv2FsBgp4PeerGroupOrfRequest (&u4Err, &PeerGrp, 0) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsBgp4PeerGroupOrfRequest (&u4Err, &PeerGrp, 1) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsBgp4PeerGroupOrfRequest (&PeerGrp, &i4Val) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gpBgpCurrCxtNode = gBgpCxtNode[0];
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor abc peer-group");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_22 (VOID)
{
    tSNMP_OCTET_STRING_TYPE PeerGrp;
    UINT1               au1Tmp[24];
    UINT4               u4Err = 0;
    INT4                i4Val = 0;

    MEMSET (au1Tmp, 0, sizeof (au1Tmp));
    MEMSET (&PeerGrp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    PeerGrp.pu1_OctetList = (UINT1 *) &au1Tmp;
    if (nmhTestv2FsBgp4PeerGroupOrfCapMode (&u4Err, &PeerGrp, 1) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsBgp4PeerGroupOrfCapMode (&PeerGrp, &i4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (au1Tmp, "abc", 3);
    PeerGrp.i4_Length = 3;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor abc peer-group");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    if (nmhTestv2FsBgp4PeerGroupOrfCapMode (&u4Err, &PeerGrp, 0) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsBgp4PeerGroupOrfCapMode (&u4Err, &PeerGrp, 1) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsBgp4PeerGroupOrfCapMode (&u4Err, &PeerGrp, 2) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsBgp4PeerGroupOrfCapMode (&u4Err, &PeerGrp, 3) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsBgp4PeerGroupOrfCapMode (&u4Err, &PeerGrp, 4) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsBgp4PeerGroupOrfCapMode (&PeerGrp, &i4Val) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gpBgpCurrCxtNode = gBgpCxtNode[0];
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor abc peer-group");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_23 (VOID)
{
    tSNMP_OCTET_STRING_TYPE PeerGrp;
    UINT1               au1Tmp[24];
    UINT4               u4Err = 0;
    UINT4               u4Val = 0;

    MEMSET (au1Tmp, 0, sizeof (au1Tmp));
    MEMSET (&PeerGrp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    PeerGrp.pu1_OctetList = (UINT1 *) &au1Tmp;

    if (nmhTestv2FsBgp4PeerGroupOrfType (&u4Err, &PeerGrp, 1) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsBgp4PeerGroupOrfType (&PeerGrp, &u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsBgp4PeerGroupOrfType (&PeerGrp, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (au1Tmp, "abc", 3);
    PeerGrp.i4_Length = 3;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor abc peer-group");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    if (nmhTestv2FsBgp4PeerGroupOrfType
        (&u4Err, &PeerGrp, BGP_ORF_TYPE_ADDRESS_PREFIX) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsBgp4PeerGroupOrfType (&u4Err, &PeerGrp, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsBgp4PeerGroupOrfType (&PeerGrp, &u4Val) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsBgp4PeerGroupOrfType (&PeerGrp, u4Val) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gpBgpCurrCxtNode = gBgpCxtNode[0];
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor abc peer-group");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_24 (VOID)
{
    tSNMP_OCTET_STRING_TYPE PeerGrp;
    tSNMP_OCTET_STRING_TYPE PrefixList;
    UINT1               au1Tmp[16];
    UINT1               au1Name[24];
    UINT4               u4Err = 0;

    MEMSET (au1Tmp, 0, sizeof (au1Tmp));
    MEMSET (au1Name, 0, sizeof (au1Name));
    MEMSET (&PeerGrp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PrefixList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    PeerGrp.pu1_OctetList = (UINT1 *) &au1Tmp;
    PrefixList.pu1_OctetList = (UINT1 *) &au1Name;

    if (nmhTestv2FsBgp4PeerGroupIpPrefixNameOut (&u4Err, &PeerGrp, &PrefixList)
        != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsBgp4PeerGroupIpPrefixNameOut (&PeerGrp, &PrefixList) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsBgp4PeerGroupIpPrefixNameIn (&u4Err, &PeerGrp, &PrefixList)
        != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsBgp4PeerGroupIpPrefixNameIn (&PeerGrp, &PrefixList) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (au1Tmp, "abc", 3);
    PeerGrp.i4_Length = 3;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor abc peer-group");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    if (nmhTestv2FsBgp4PeerGroupIpPrefixNameOut (&u4Err, &PeerGrp, &PrefixList)
        != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsBgp4PeerGroupIpPrefixNameOut (&PeerGrp, &PrefixList) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsBgp4PeerGroupIpPrefixNameIn (&u4Err, &PeerGrp, &PrefixList)
        != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsBgp4PeerGroupIpPrefixNameIn (&PeerGrp, &PrefixList) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gpBgpCurrCxtNode = gBgpCxtNode[0];
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor abc peer-group");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_25 (VOID)
{
    tSNMP_OCTET_STRING_TYPE Peer;
    tSNMP_OCTET_STRING_TYPE PrefixList;
    UINT1               au1Tmp[16];
    UINT1               au1Name[24];
    UINT4               u4Err = 0;

    MEMSET (au1Tmp, 0, sizeof (au1Tmp));
    MEMSET (au1Name, 0, sizeof (au1Name));
    MEMSET (&Peer, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PrefixList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    Peer.pu1_OctetList = (UINT1 *) &au1Tmp;
    PrefixList.pu1_OctetList = (UINT1 *) &au1Name;

    if (nmhTestv2Fsbgp4mpePeerIpPrefixNameOut (&u4Err, 1, &Peer, &PrefixList) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsbgp4mpePeerIpPrefixNameOut (1, &Peer, &PrefixList) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2Fsbgp4mpePeerIpPrefixNameIn (&u4Err, 1, &Peer, &PrefixList) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsbgp4mpePeerIpPrefixNameIn (1, &Peer, &PrefixList) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    Peer.pu1_OctetList[0] = 1;
    Peer.pu1_OctetList[1] = 1;
    Peer.pu1_OctetList[2] = 1;
    Peer.pu1_OctetList[3] = 1;
    Peer.i4_Length = 4;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 1.1.1.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    if (nmhTestv2Fsbgp4mpePeerIpPrefixNameOut (&u4Err, 1, &Peer, &PrefixList) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsbgp4mpePeerIpPrefixNameOut (1, &Peer, &PrefixList) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2Fsbgp4mpePeerIpPrefixNameIn (&u4Err, 1, &Peer, &PrefixList) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsbgp4mpePeerIpPrefixNameIn (1, &Peer, &PrefixList) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gpBgpCurrCxtNode = gBgpCxtNode[0];
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 1.1.1.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_26 (VOID)
{
    tSNMP_OCTET_STRING_TYPE PeerGrp;
    tBgpPeerGroupEntry *pPeerGroup = NULL;
    UINT1               au1Tmp[16];

    MEMSET (au1Tmp, 0, sizeof (au1Tmp));
    MEMSET (&PeerGrp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    PeerGrp.pu1_OctetList = (UINT1 *) &au1Tmp;

    if (nmhSetFsBgp4PeerGroupOrfCapMode (&PeerGrp, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor abc peer-group");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    gpBgpCurrCxtNode = gBgpCxtNode[0];

    MEMCPY (au1Tmp, "abc", 3);
    PeerGrp.i4_Length = 3;
    if (nmhSetFsBgp4PeerGroupOrfCapMode (&PeerGrp, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    pPeerGroup = Bgp4GetPeerGroupEntry (0, PeerGrp.pu1_OctetList);
    pPeerGroup->u1OrfCapMode = 1;
    pPeerGroup->u1OrfType = 1;
    if (nmhSetFsBgp4PeerGroupOrfCapMode (&PeerGrp, 1) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsBgp4PeerGroupOrfCapMode (&PeerGrp, 2) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsBgp4PeerGroupOrfCapMode (&PeerGrp, 0) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gpBgpCurrCxtNode = gBgpCxtNode[0];
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor abc peer-group");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_27 (VOID)
{
    tSNMP_OCTET_STRING_TYPE PeerGrp;
    UINT1               au1Tmp[16];

    MEMSET (au1Tmp, 0, sizeof (au1Tmp));
    MEMSET (&PeerGrp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    PeerGrp.pu1_OctetList = (UINT1 *) &au1Tmp;

    if (nmhSetFsBgp4PeerGroupOrfRequest (&PeerGrp, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor abc peer-group");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    gpBgpCurrCxtNode = gBgpCxtNode[0];

    MEMCPY (au1Tmp, "abc", 3);
    PeerGrp.i4_Length = 3;
    AllocMem (gBgpNode.Bgp4InputQPoolId);
    if (nmhSetFsBgp4PeerGroupOrfRequest (&PeerGrp, 1) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    ReleaseMem (gBgpNode.Bgp4InputQPoolId);
    if (nmhSetFsBgp4PeerGroupOrfRequest (&PeerGrp, 1) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gpBgpCurrCxtNode = gBgpCxtNode[0];
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor abc peer-group");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_28 (VOID)
{
    tSNMP_OCTET_STRING_TYPE PeerGrp;
    tSNMP_OCTET_STRING_TYPE PrefixList;
    UINT1               au1Tmp[16];
    UINT1               au1Name[24];

    MEMSET (au1Tmp, 0, sizeof (au1Tmp));
    MEMSET (au1Name, 0, sizeof (au1Name));
    MEMSET (&PeerGrp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PrefixList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    PeerGrp.pu1_OctetList = (UINT1 *) &au1Tmp;
    PrefixList.pu1_OctetList = (UINT1 *) &au1Name;

    if (nmhSetFsBgp4PeerGroupIpPrefixNameIn (&PeerGrp, &PrefixList) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsBgp4PeerGroupIpPrefixNameOut (&PeerGrp, &PrefixList) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor abc peer-group");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    gpBgpCurrCxtNode = gBgpCxtNode[0];

    MEMCPY (au1Tmp, "abc", 3);
    PeerGrp.i4_Length = 3;

    if (nmhSetFsBgp4PeerGroupIpPrefixNameIn (&PeerGrp, &PrefixList) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsBgp4PeerGroupIpPrefixNameOut (&PeerGrp, &PrefixList) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (au1Name, "prefix", 6);
    PrefixList.i4_Length = 6;
    if (nmhSetFsBgp4PeerGroupIpPrefixNameIn (&PeerGrp, &PrefixList) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsBgp4PeerGroupIpPrefixNameOut (&PeerGrp, &PrefixList) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip prefix-list prefix permit 0.0.0.0/0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    if (nmhSetFsBgp4PeerGroupIpPrefixNameIn (&PeerGrp, &PrefixList) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsBgp4PeerGroupIpPrefixNameOut (&PeerGrp, &PrefixList) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gpBgpCurrCxtNode = gBgpCxtNode[0];
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no ip prefix-list prefix permit 0.0.0.0/0");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor abc peer-group");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_29 (VOID)
{
    tSNMP_OCTET_STRING_TYPE Peer;
    tSNMP_OCTET_STRING_TYPE PrefixList;
    UINT1               au1Tmp[16];
    UINT1               au1Name[24];

    MEMSET (au1Tmp, 0, sizeof (au1Tmp));
    MEMSET (au1Name, 0, sizeof (au1Name));
    MEMSET (&Peer, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PrefixList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    Peer.pu1_OctetList = (UINT1 *) &au1Tmp;
    PrefixList.pu1_OctetList = (UINT1 *) &au1Name;

    if (nmhSetFsbgp4mpePeerIpPrefixNameIn (1, &Peer, &PrefixList) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsbgp4mpePeerIpPrefixNameOut (1, &Peer, &PrefixList) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    Peer.pu1_OctetList[0] = 1;
    Peer.pu1_OctetList[1] = 1;
    Peer.pu1_OctetList[2] = 1;
    Peer.pu1_OctetList[3] = 1;
    Peer.i4_Length = 4;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("neighbor 1.1.1.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    if (nmhSetFsbgp4mpePeerIpPrefixNameIn (1, &Peer, &PrefixList) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsbgp4mpePeerIpPrefixNameOut (1, &Peer, &PrefixList) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (au1Name, "prefix", 6);
    PrefixList.i4_Length = 6;
    if (nmhSetFsbgp4mpePeerIpPrefixNameIn (1, &Peer, &PrefixList) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsbgp4mpePeerIpPrefixNameOut (1, &Peer, &PrefixList) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip prefix-list prefix permit 0.0.0.0/0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    if (nmhSetFsbgp4mpePeerIpPrefixNameIn (1, &Peer, &PrefixList) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsbgp4mpePeerIpPrefixNameOut (1, &Peer, &PrefixList) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no ip prefix-list prefix permit 0.0.0.0/0");
    CliExecuteAppCmd ("router bgp 100");
    CliExecuteAppCmd ("no neighbor 1.1.1.1 remote-as 200");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_30 (VOID)
{
    tSNMP_OCTET_STRING_TYPE Peer;
    tSNMP_OCTET_STRING_TYPE Addr;
    UINT1               au1Tmp[16];
    UINT1               au1Addr[16];

    MEMSET (au1Tmp, 0, sizeof (au1Tmp));
    MEMSET (au1Addr, 0, sizeof (au1Addr));
    MEMSET (&Peer, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&Addr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    Peer.pu1_OctetList = (UINT1 *) &au1Tmp;
    Addr.pu1_OctetList = (UINT1 *) &au1Addr;

    if (nmhValidateIndexInstanceFsBgp4ORFListTable
        (0, &Peer, 0, 0, 1, 1, &Addr, 4, 0, 0, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhValidateIndexInstanceFsBgp4ORFListTable
        (1, &Peer, 0, 0, 1, 1, &Addr, 4, 0, 0, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    au1Tmp[0] = 1;
    if (nmhValidateIndexInstanceFsBgp4ORFListTable
        (1, &Peer, 0, 0, 1, 1, &Addr, 4, 0, 0, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhValidateIndexInstanceFsBgp4ORFListTable
        (1, &Peer, 2, 0, 1, 1, &Addr, 4, 0, 0, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhValidateIndexInstanceFsBgp4ORFListTable
        (1, &Peer, 1, 1, 1, 1, &Addr, 4, 0, 0, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    au1Addr[3] = 1;
    au1Addr[0] = 127;
    if (nmhValidateIndexInstanceFsBgp4ORFListTable
        (1, &Peer, 1, 1, BGP_ORF_TYPE_ADDRESS_PREFIX, 1, &Addr, 4, 0, 0,
         0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    au1Addr[3] = 0;
    au1Addr[0] = 0;
    if (nmhValidateIndexInstanceFsBgp4ORFListTable
        (1, &Peer, 1, 1, BGP_ORF_TYPE_ADDRESS_PREFIX, 1, &Addr, 33, 0, 0,
         0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhValidateIndexInstanceFsBgp4ORFListTable
        (1, &Peer, 1, 1, BGP_ORF_TYPE_ADDRESS_PREFIX, 1, &Addr, 8, 33, 0,
         0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhValidateIndexInstanceFsBgp4ORFListTable
        (1, &Peer, 1, 1, BGP_ORF_TYPE_ADDRESS_PREFIX, 1, &Addr, 8, 16, 33,
         0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhValidateIndexInstanceFsBgp4ORFListTable
        (1, &Peer, 1, 1, BGP_ORF_TYPE_ADDRESS_PREFIX, 1, &Addr, 8, 16, 24,
         0) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhValidateIndexInstanceFsBgp4ORFListTable
        (1, &Peer, 2, 1, BGP_ORF_TYPE_ADDRESS_PREFIX, 1, &Addr, 129, 16, 24,
         0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhValidateIndexInstanceFsBgp4ORFListTable
        (1, &Peer, 2, 1, BGP_ORF_TYPE_ADDRESS_PREFIX, 1, &Addr, 64, 129, 24,
         0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhValidateIndexInstanceFsBgp4ORFListTable
        (1, &Peer, 2, 1, BGP_ORF_TYPE_ADDRESS_PREFIX, 1, &Addr, 64, 70, 129,
         0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhValidateIndexInstanceFsBgp4ORFListTable
        (1, &Peer, 2, 1, BGP_ORF_TYPE_ADDRESS_PREFIX, 1, &Addr, 64, 70, 72,
         2) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhValidateIndexInstanceFsBgp4ORFListTable
        (1, &Peer, 2, 1, BGP_ORF_TYPE_ADDRESS_PREFIX, 1, &Addr, 64, 70, 72,
         0) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhValidateIndexInstanceFsBgp4ORFListTable
        (1, &Peer, 2, 1, BGP_ORF_TYPE_ADDRESS_PREFIX, 1, &Addr, 64, 70, 72,
         1) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

#ifdef ROUTEMAP_WANTED
INT4
BgpUtOrf_31 (VOID)
{
    tSNMP_OCTET_STRING_TYPE Name;
    UINT1               au1Name[20];
    INT4                i4Val = 0;
    UINT4               u4Err = 0;

    MEMSET (&au1Name, 0, sizeof (au1Name));
    MEMSET (&Name, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    Name.pu1_OctetList = (UINT1 *) &au1Name;

    if (nmhGetFsRMapIsIpPrefixList (&Name, 0, &i4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapIsIpPrefixList (&u4Err, &Name, 0, i4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsRMapIsIpPrefixList (&Name, 0, i4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gu4RMapGlobalDbg = 1;
    if (nmhGetFsRMapIsIpPrefixList (&Name, 0, &i4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapIsIpPrefixList (&u4Err, &Name, 0, i4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsRMapIsIpPrefixList (&Name, 0, i4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip prefix-list abc seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    MEMCPY (au1Name, "abc", 3);
    Name.i4_Length = 3;
    if (nmhGetFsRMapIsIpPrefixList (&Name, 1, &i4Val) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (i4Val != TRUE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapIsIpPrefixList (&u4Err, &Name, 1, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gu4RMapGlobalDbg = 0;
    if (nmhTestv2FsRMapIsIpPrefixList (&u4Err, &Name, 1, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapIsIpPrefixList (&u4Err, &Name, 1, TRUE) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsRMapIsIpPrefixList (&Name, 1, TRUE) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no ip prefix-list abc seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_32 (VOID)
{
    tSNMP_OCTET_STRING_TYPE Name;
    tSNMP_OCTET_STRING_TYPE Addr;
    tRMapNode          *pRMapNode;
    UINT1               au1Name[20];
    UINT1               au1Addr[20];
    UINT4               u4Val = 0;

    MEMSET (&au1Addr, 0, sizeof (au1Addr));
    MEMSET (&Addr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1Name, 0, sizeof (au1Name));
    MEMSET (&Name, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    Name.pu1_OctetList = (UINT1 *) &au1Name;
    Addr.pu1_OctetList = (UINT1 *) &au1Addr;

    Name.i4_Length = 21;
    if (nmhGetFsRMapMatchDestMaxPrefixLen
        (&Name, 0, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, &u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsRMapMatchDestMinPrefixLen
        (&Name, 0, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, &u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsRMapMatchDestMaxPrefixLen
        (&Name, 0, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsRMapMatchDestMinPrefixLen
        (&Name, 0, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    Name.i4_Length = 0;
    if (nmhGetFsRMapMatchDestMaxPrefixLen
        (&Name, 0, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, &u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsRMapMatchDestMinPrefixLen
        (&Name, 0, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, &u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsRMapMatchDestMaxPrefixLen
        (&Name, 0, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsRMapMatchDestMinPrefixLen
        (&Name, 0, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gu4RMapGlobalDbg = 1;
    if (nmhGetFsRMapMatchDestMaxPrefixLen
        (&Name, 0, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, &u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsRMapMatchDestMinPrefixLen
        (&Name, 0, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, &u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsRMapMatchDestMaxPrefixLen
        (&Name, 0, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsRMapMatchDestMinPrefixLen
        (&Name, 0, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip prefix-list abc seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    MEMCPY (au1Name, "abc", 3);
    Name.i4_Length = 3;

    if (nmhGetFsRMapMatchDestMaxPrefixLen
        (&Name, 1, 1, &Addr, 0, 0, &Addr, 0, 0, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, &u4Val) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsRMapMatchDestMinPrefixLen
        (&Name, 1, 1, &Addr, 0, 0, &Addr, 0, 0, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, &u4Val) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsRMapMatchDestMaxPrefixLen
        (&Name, 1, 1, &Addr, 0, 0, &Addr, 0, 0, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, u4Val) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsRMapMatchDestMinPrefixLen
        (&Name, 1, 1, &Addr, 0, 0, &Addr, 0, 0, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, u4Val) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    pRMapNode = RMapGetRouteMapNode (au1Name, 1);
    pRMapNode->u1IsIpPrefixInfo = FALSE;
    if (nmhGetFsRMapMatchDestMaxPrefixLen
        (&Name, 1, 1, &Addr, 0, 0, &Addr, 0, 0, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, &u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsRMapMatchDestMinPrefixLen
        (&Name, 1, 1, &Addr, 0, 0, &Addr, 0, 0, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, &u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsRMapMatchDestMaxPrefixLen
        (&Name, 1, 1, &Addr, 0, 0, &Addr, 0, 0, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsRMapMatchDestMinPrefixLen
        (&Name, 1, 1, &Addr, 0, 0, &Addr, 0, 0, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gu4RMapGlobalDbg = 0;

    if (nmhGetFsRMapMatchDestMaxPrefixLen
        (&Name, 1, 1, &Addr, 0, 0, &Addr, 0, 0, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, &u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsRMapMatchDestMinPrefixLen
        (&Name, 1, 1, &Addr, 0, 0, &Addr, 0, 0, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, &u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsRMapMatchDestMaxPrefixLen
        (&Name, 1, 1, &Addr, 0, 0, &Addr, 0, 0, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsRMapMatchDestMinPrefixLen
        (&Name, 1, 1, &Addr, 0, 0, &Addr, 0, 0, &Addr, 0, 0, 0, 0, 0, 0, 0, 0,
         0, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pRMapNode->u1IsIpPrefixInfo = TRUE;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no ip prefix-list abc seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_33 (VOID)
{
    tSNMP_OCTET_STRING_TYPE Name;
    tSNMP_OCTET_STRING_TYPE Addr;
    tRMapNode          *pRMapNode;
    UINT1               au1Name[20];
    UINT1               au1Addr[20];
    UINT4               u4Val = 0;
    UINT4               u4Err = 0;

    MEMSET (&au1Addr, 0, sizeof (au1Addr));
    MEMSET (&Addr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1Name, 0, sizeof (au1Name));
    MEMSET (&Name, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    Name.pu1_OctetList = (UINT1 *) &au1Name;
    Addr.pu1_OctetList = (UINT1 *) &au1Addr;

    Name.i4_Length = 21;
    if (nmhTestv2FsRMapMatchDestMaxPrefixLen
        (&u4Err, &Name, 0, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapMatchDestMinPrefixLen
        (&u4Err, &Name, 0, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    Name.i4_Length = 0;
    if (nmhTestv2FsRMapMatchDestMaxPrefixLen
        (&u4Err, &Name, 0, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapMatchDestMinPrefixLen
        (&u4Err, &Name, 0, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gu4RMapGlobalDbg = 1;
    if (nmhTestv2FsRMapMatchDestMaxPrefixLen
        (&u4Err, &Name, 0, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapMatchDestMinPrefixLen
        (&u4Err, &Name, 0, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip prefix-list abc seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("ipv6 prefix-list abc seq 2 permit 0::0/0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    MEMCPY (au1Name, "abc", 3);
    Name.i4_Length = 3;

    if (nmhTestv2FsRMapMatchDestMaxPrefixLen
        (&u4Err, &Name, 1, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapMatchDestMinPrefixLen
        (&u4Err, &Name, 1, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gu4RMapGlobalDbg = 0;
    if (nmhTestv2FsRMapMatchDestMaxPrefixLen
        (&u4Err, &Name, 1, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapMatchDestMinPrefixLen
        (&u4Err, &Name, 1, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, u4Val) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapMatchDestMaxPrefixLen
        (&u4Err, &Name, 1, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 33) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapMatchDestMinPrefixLen
        (&u4Err, &Name, 1, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 33) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gu4RMapGlobalDbg = 1;
    if (nmhTestv2FsRMapMatchDestMaxPrefixLen
        (&u4Err, &Name, 1, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 33) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapMatchDestMinPrefixLen
        (&u4Err, &Name, 1, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 33) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapMatchDestMaxPrefixLen
        (&u4Err, &Name, 1, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 1) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapMatchDestMinPrefixLen
        (&u4Err, &Name, 1, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 1) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsRMapMatchDestMaxPrefixLen
        (&u4Err, &Name, 2, 2, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 129) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapMatchDestMinPrefixLen
        (&u4Err, &Name, 2, 2, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 129) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gu4RMapGlobalDbg = 0;
    if (nmhTestv2FsRMapMatchDestMaxPrefixLen
        (&u4Err, &Name, 2, 2, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 129) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapMatchDestMinPrefixLen
        (&u4Err, &Name, 2, 2, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 129) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapMatchDestMaxPrefixLen
        (&u4Err, &Name, 2, 2, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 64) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapMatchDestMinPrefixLen
        (&u4Err, &Name, 2, 2, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 64) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    pRMapNode = RMapGetRouteMapNode (au1Name, 1);
    pRMapNode->u1IsIpPrefixInfo = FALSE;

    if (nmhTestv2FsRMapMatchDestMaxPrefixLen
        (&u4Err, &Name, 1, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapMatchDestMinPrefixLen
        (&u4Err, &Name, 1, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gu4RMapGlobalDbg = 1;

    if (nmhTestv2FsRMapMatchDestMaxPrefixLen
        (&u4Err, &Name, 1, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapMatchDestMinPrefixLen
        (&u4Err, &Name, 1, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pRMapNode->u1IsIpPrefixInfo = TRUE;
    au1Addr[0] = 1;
    if (nmhTestv2FsRMapMatchDestMaxPrefixLen
        (&u4Err, &Name, 1, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapMatchDestMinPrefixLen
        (&u4Err, &Name, 1, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gu4RMapGlobalDbg = 0;
    if (nmhTestv2FsRMapMatchDestMaxPrefixLen
        (&u4Err, &Name, 1, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsRMapMatchDestMinPrefixLen
        (&u4Err, &Name, 1, 1, &Addr, 0, 1, &Addr, 0, 1, &Addr, 0, 0, 0, 0, 0, 0,
         0, 0, 0, 0) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no ip prefix-list abc seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("no ipv6 prefix-list abc seq 2 permit 0::0/0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_34 (VOID)
{
    tRMapNode          *pRMapNode;
    tRMapMatchNode     *pRMapMatchNode = NULL;
    tBgp4OrfEntry       OrfEntry;
    UINT1               au1Name[20];

    MEMSET (&au1Name, 0, sizeof (au1Name));

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip prefix-list abc seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    MEMCPY (au1Name, "abc", 3);

    pRMapNode = RMapGetRouteMapNode (au1Name, 1);
    pRMapNode->u1IsIpPrefixInfo = FALSE;

    if (RMapGetNextValidIpPrefixEntry (au1Name, 0, &OrfEntry) != RMAP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    pRMapNode->u1RowStatus = NOT_IN_SERVICE;
    if (RMapGetNextValidIpPrefixEntry (au1Name, 0, &OrfEntry) != RMAP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    pRMapNode->u1RowStatus = ACTIVE;
    pRMapNode->u1IsIpPrefixInfo = TRUE;
    pRMapNode->u1Access = RMAP_DENY;
    if (RMapGetNextValidIpPrefixEntry (au1Name, 0, &OrfEntry) != RMAP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    pRMapNode->u1Access = 3;
    if (RMapGetNextValidIpPrefixEntry (au1Name, 0, &OrfEntry) != RMAP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    pRMapMatchNode =
        pRMapNode->MatchCmdArray[RMAP_MATCH_IP_PREFIX_ENTRY - 1].pNode;
    pRMapNode->MatchCmdArray[RMAP_MATCH_IP_PREFIX_ENTRY - 1].pNode = NULL;
    if (RMapGetNextValidIpPrefixEntry (au1Name, 0, &OrfEntry) != RMAP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    pRMapMatchNode->u1RowStatus = NOT_IN_SERVICE;
    pRMapNode->MatchCmdArray[RMAP_MATCH_IP_PREFIX_ENTRY - 1].pNode =
        pRMapMatchNode;
    if (RMapGetNextValidIpPrefixEntry (au1Name, 0, &OrfEntry) != RMAP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no ip prefix-list abc seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    if (RMapGetNextValidIpPrefixEntry (au1Name, 0, &OrfEntry) != RMAP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no ip prefix-list xyz seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_35 (VOID)
{
    tIPvXAddr           Addr1;
    tIPvXAddr           Addr2;

    MEMSET (&Addr1, 0, sizeof (tIPvXAddr));
    MEMSET (&Addr2, 0, sizeof (tIPvXAddr));

    Addr1.u1Afi = 1;
    if (MatchIpvxAddrWithPrefix (&Addr1, &Addr2, 0) != FALSE)
    {
        return OSIX_FAILURE;
    }
    Addr2.u1Afi = 1;
    if (MatchIpvxAddrWithPrefix (&Addr1, &Addr2, 0) != TRUE)
    {
        return OSIX_FAILURE;
    }
    if (MatchIpvxAddrWithPrefix (&Addr1, &Addr2, 8) != TRUE)
    {
        return OSIX_FAILURE;
    }
    Addr2.au1Addr[3] = 1;
    if (MatchIpvxAddrWithPrefix (&Addr1, &Addr2, 8) != FALSE)
    {
        return OSIX_FAILURE;
    }
    Addr2.au1Addr[3] = 0;
    Addr1.au1Addr[3] = 1;
    if (MatchIpvxAddrWithPrefix (&Addr1, &Addr2, 8) != FALSE)
    {
        return OSIX_FAILURE;
    }
    Addr2.au1Addr[3] = 1;
    if (MatchIpvxAddrWithPrefix (&Addr1, &Addr2, 8) != TRUE)
    {
        return OSIX_FAILURE;
    }
    Addr1.u1Afi = 2;
    Addr2.u1Afi = 2;
    Addr1.au1Addr[3] = 0;
    Addr2.au1Addr[3] = 0;
    if (MatchIpvxAddrWithPrefix (&Addr1, &Addr2, 8) != TRUE)
    {
        return OSIX_FAILURE;
    }
    Addr1.au1Addr[0] = 1;
    if (MatchIpvxAddrWithPrefix (&Addr1, &Addr2, 8) != FALSE)
    {
        return OSIX_FAILURE;
    }
    Addr1.u1Afi = 3;
    Addr2.u1Afi = 3;
    if (MatchIpvxAddrWithPrefix (&Addr1, &Addr2, 8) != FALSE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_36 (VOID)
{
    tSNMP_OCTET_STRING_TYPE Name;
    tIpPrefixInfo       IpPrefixInfo;
    UINT1               au1Name[20];
    tRMapNode          *pRMapNode = NULL;
    tRMapMatchNode     *pRMapMatchNode = NULL;

    MEMSET (&Name, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1Name, 0, sizeof (au1Name));
    MEMSET (&IpPrefixInfo, 0, sizeof (tIpPrefixInfo));

    Name.pu1_OctetList = (UINT1 *) &au1Name;
    Name.i4_Length = 21;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip prefix-list xyz seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    MEMCPY (au1Name, "abc", 3);

    if (RMapUtilIsIpPrefixExist (&Name, &IpPrefixInfo, 1) != 0)
    {
        return OSIX_FAILURE;
    }
    Name.i4_Length = 3;
    if (RMapUtilIsIpPrefixExist (&Name, &IpPrefixInfo, 1) != 0)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (au1Name, "xyz", 3);
    pRMapNode = RMapGetRouteMapNode (au1Name, 1);
    pRMapNode->u1IsIpPrefixInfo = FALSE;
    if (RMapUtilIsIpPrefixExist (&Name, &IpPrefixInfo, 1) != 0)
    {
        return OSIX_FAILURE;
    }
    pRMapNode->u1IsIpPrefixInfo = TRUE;
    pRMapMatchNode =
        pRMapNode->MatchCmdArray[RMAP_MATCH_IP_PREFIX_ENTRY - 1].pNode;
    pRMapNode->MatchCmdArray[RMAP_MATCH_IP_PREFIX_ENTRY - 1].pNode = NULL;
    if (RMapUtilIsIpPrefixExist (&Name, &IpPrefixInfo, 1) != 0)
    {
        return OSIX_FAILURE;
    }
    pRMapMatchNode->u1Cmd = 0;
    pRMapNode->MatchCmdArray[RMAP_MATCH_IP_PREFIX_ENTRY - 1].pNode =
        pRMapMatchNode;
    if (RMapUtilIsIpPrefixExist (&Name, &IpPrefixInfo, 1) != 0)
    {
        return OSIX_FAILURE;
    }
    pRMapMatchNode->u1Cmd = RMAP_MATCH_IP_PREFIX_ENTRY;
    if (RMapUtilIsIpPrefixExist (&Name, &IpPrefixInfo, 1) != 0)
    {
        return OSIX_FAILURE;
    }
    IpPrefixInfo.AddrInfo.InetXAddr.u1Afi = 1;
    IpPrefixInfo.AddrInfo.InetXAddr.u1AddrLen = 4;
    pRMapMatchNode->IpPrefixAddr[0] = 1;
    if (RMapUtilIsIpPrefixExist (&Name, &IpPrefixInfo, 1) != 0)
    {
        return OSIX_FAILURE;
    }
    pRMapMatchNode->IpPrefixAddr[0] = 0;
    pRMapMatchNode->IpPrefixLength = 1;
    if (RMapUtilIsIpPrefixExist (&Name, &IpPrefixInfo, 1) != 0)
    {
        return OSIX_FAILURE;
    }
    pRMapMatchNode->IpPrefixLength = 0;
    pRMapMatchNode->IpPrefixMinLen = 1;
    if (RMapUtilIsIpPrefixExist (&Name, &IpPrefixInfo, 1) != 0)
    {
        return OSIX_FAILURE;
    }
    pRMapMatchNode->IpPrefixMinLen = 0;
    pRMapMatchNode->IpPrefixMaxLen = 1;
    if (RMapUtilIsIpPrefixExist (&Name, &IpPrefixInfo, 1) != 0)
    {
        return OSIX_FAILURE;
    }
    pRMapMatchNode->IpPrefixMaxLen = 0;
    if (RMapUtilIsIpPrefixExist (&Name, &IpPrefixInfo, 2) != 0)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no ip prefix-list xyz seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_37 (VOID)
{
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip prefix-list xyz seq 1 permit 11111/2 > a");
    CliExecuteAppCmd ("ipv6 prefix-list xyz seq 1 permit 11111/2 > a");
    CliExecuteAppCmd ("ipv6 prefix-list xyz seq 1 permit 0::0/ > a");
    CliExecuteAppCmd ("ip prefix-list xyz seq 1 permit 0.0.0.0/33 > a");
    CliExecuteAppCmd ("ipv6 prefix-list xyz seq 1 permit 0::0/129 > a");
    CliExecuteAppCmd ("ipv6 prefix-list xyz seq 1 permit 0::0/64/11 > a");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    if (RmapCliValidateIpAddrAndPrefix (NULL, 0, NULL, NULL) != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_38 (VOID)
{
    tSNMP_OCTET_STRING_TYPE Name;
    tIpPrefixInfo       IpPrefix;
    UINT1               au1Name[20];
    tRMapNode          *pRMapNode = NULL;
    tRMapMatchNode     *pRMapMatchNode = NULL;

    MEMSET (&Name, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1Name, 0, sizeof (au1Name));
    MEMSET (&IpPrefix, 0, sizeof (tIpPrefixInfo));

    Name.pu1_OctetList = au1Name;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 0.0.0.0/0 > a");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    IpPrefix.AddrInfo.InetXAddr.u1Afi = 1;
    IpPrefix.AddrInfo.InetXAddr.u1AddrLen = 4;
    Name.i4_Length = 21;
    if (RMapCliDelIpPrefixList (0, &Name, 1, 1, &IpPrefix, 0, 0) != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip prefix-list xyz seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz seq 2 permit 0.0.0.0/0 ge 32");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    MEMCPY (au1Name, "xyz", 3);
    Name.i4_Length = 3;

    pRMapNode = RMapGetRouteMapNode (au1Name, 1);
    pRMapNode->u1IsIpPrefixInfo = FALSE;

    if (RMapCliDelIpPrefixList (0, &Name, 1, 1, &IpPrefix, 0, 0) != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pRMapNode->u1IsIpPrefixInfo = TRUE;

    IpPrefix.AddrInfo.InetXAddr.au1Addr[0] = 1;
    if (RMapCliDelIpPrefixList (1, &Name, 2, 1, &IpPrefix, 0, 0) != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    IpPrefix.AddrInfo.InetXAddr.au1Addr[0] = 0;
    pRMapNode->u1IsIpPrefixInfo = FALSE;
    if (RMapCliDelIpPrefixList (0, &Name, 2, 1, &IpPrefix, 0, 0) != CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    pRMapNode->u1IsIpPrefixInfo = TRUE;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip prefix-list xyz seq 2 permit 0.0.0.0/0 ge 32");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    if (RMapCliDelIpPrefixList (0, &Name, 2, 1, &IpPrefix, 0, 0) != CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no ip prefix-list xyz seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip prefix-list xyz seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list abc seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz");
    CliExecuteAppCmd ("no ip prefix-list xya > a");
    CliExecuteAppCmd ("ip prefix-list xya");
    CliExecuteAppCmd ("no ip prefix-list xya");
    CliExecuteAppCmd ("no ip prefix-list xyz seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list abc seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip prefix-list xyy seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    MEMCPY (au1Name, "xyy", 3);
    Name.i4_Length = 3;

    pRMapNode = RMapGetRouteMapNode (au1Name, 1);
    pRMapNode->u1IsIpPrefixInfo = FALSE;

    if (RMapCliDelIpPrefixList (0, &Name, 1, 1, NULL, 0, 0) != CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    pRMapNode->u1IsIpPrefixInfo = TRUE;
    pRMapMatchNode =
        pRMapNode->MatchCmdArray[RMAP_MATCH_IP_PREFIX_ENTRY - 1].pNode;
    pRMapNode->MatchCmdArray[RMAP_MATCH_IP_PREFIX_ENTRY - 1].pNode = NULL;
    if (RMapCliDelIpPrefixList (0, &Name, 1, 1, NULL, 0, 0) != CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    pRMapNode->MatchCmdArray[RMAP_MATCH_IP_PREFIX_ENTRY - 1].pNode =
        pRMapMatchNode;
    pRMapMatchNode->IpPrefixAddrType = 0;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no ip prefix-list xyz seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    if (RMapCliDelIpPrefixList (0, &Name, 1, 1, NULL, 0, 0) != CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    pRMapMatchNode->IpPrefixAddrType = 1;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no ip prefix-list xyy seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list abc seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list abc");
    CliExecuteAppCmd ("no ip prefix-list abc permit 1.1.1.1/0 > a");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_39 (VOID)
{
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show ip prefix-list > a");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip prefix-list xyz seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("show ip prefix-list > a");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip prefix-list xyz seq 2 permit 0.0.0.0/0 ge 32");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("show ip prefix-list > a");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip prefix-list abc seq 1 permit 0.0.0.0/0 ge 32");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("show ip prefix-list xyy > a");
    CliExecuteAppCmd ("show ip prefix-list xyz > a");
    CliExecuteAppCmd ("show ip prefix-list abc > a");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no ip prefix-list xyz seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz seq 2 permit 0.0.0.0/0 ge 32");
    CliExecuteAppCmd ("no ip prefix-list abc seq 1 permit 0.0.0.0/0 ge 32");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    if (RMapCliShowIpPrefixListName (0, NULL, 0, 2, NULL, 0) != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_40 (VOID)
{
    tSNMP_OCTET_STRING_TYPE Name;
    tIpPrefixInfo       IpPrefix;
    UINT1               au1Name[20];

    MEMSET (&Name, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1Name, 0, sizeof (au1Name));
    MEMSET (&IpPrefix, 0, sizeof (tIpPrefixInfo));

    Name.pu1_OctetList = au1Name;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip prefix-list xyz seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz seq 1 permit 0.0.0.0/0 > a");
    CliExecuteAppCmd ("ip prefix-list xyz seq 101 permit 0.0.0.0/0 ge 32 > a");
    CliExecuteAppCmd ("ip prefix-list xyz permit 1.0.0.0/0 le 32");
    CliExecuteAppCmd ("ip prefix-list xyz permit 2.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz permit 3.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz permit 4.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz permit 5.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz permit 6.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz permit 7.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz permit 8.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz permit 9.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz permit 10.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz permit 11.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz permit 12.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz permit 13.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz permit 14.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz permit 15.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz permit 16.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz permit 17.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz permit 18.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz permit 19.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz permit 20.0.0.0/0 > a");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    Name.i4_Length = 21;
    if (RMapCliCreateIpPrefixList (0, &Name, 1, &IpPrefix, 0, 0) != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (au1Name, "abc", 3);
    Name.i4_Length = 3;
    AllocMem (gRMapGlobalInfo.RMapPoolId);
    if (RMapCliCreateIpPrefixList (1, &Name, 1, &IpPrefix, 0, 0) != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    ReleaseMem (gRMapGlobalInfo.RMapPoolId);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no ip prefix-list xyz seq 1 permit 0.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 1.0.0.0/0 le 32");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 2.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 3.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 4.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 5.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 6.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 7.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 8.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 9.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 10.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 11.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 12.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 13.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 14.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 15.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 16.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 17.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 18.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 19.0.0.0/0");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    AllocMem (gRMapGlobalInfo.IpPrefixPoolId);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip prefix-list xyz seq 1 permit 0.0.0.0/0 > a");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    ReleaseMem (gRMapGlobalInfo.IpPrefixPoolId);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("ip prefix-list xyz");
    CliExecuteAppCmd ("ip prefix-list xyz permit 0.0.0.0/0");
    CliExecuteAppCmd ("ip prefix-list xyz seq 1 permit 0.0.0.0/0 le 32");
    CliExecuteAppCmd ("no ip prefix-list xyz permit 0.0.0.0/0");
    CliExecuteAppCmd ("no ip prefix-list xyz seq 1 permit 0.0.0.0/0 le 32");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    return OSIX_SUCCESS;
}

INT4
BgpUtOrf_41 (VOID)
{
    tRMapIpPrefix       Entry1;
    tRMapIpPrefix       Entry2;

    MEMSET (&Entry1, 0, sizeof (tRMapIpPrefix));
    MEMSET (&Entry2, 0, sizeof (tRMapIpPrefix));

    if (RMapIpPrefixCompareEntry (&Entry1, &Entry2) != RMAP_RB_EQUAL)
    {
        return OSIX_FAILURE;
    }
    Entry1.au1IpPrefixName[0] = 'a';
    Entry2.au1IpPrefixName[0] = 'b';
    if (RMapIpPrefixCompareEntry (&Entry1, &Entry2) != RMAP_RB_LESSER)
    {
        return OSIX_FAILURE;
    }
    Entry1.au1IpPrefixName[0] = 'b';
    Entry2.au1IpPrefixName[0] = 'a';
    if (RMapIpPrefixCompareEntry (&Entry1, &Entry2) != RMAP_RB_GREATER)
    {
        return OSIX_FAILURE;
    }
    Entry1.au1IpPrefixName[1] = 'a';
    if (RMapIpPrefixCompareEntry (&Entry1, &Entry2) != RMAP_RB_GREATER)
    {
        return OSIX_FAILURE;
    }
    Entry2.au1IpPrefixName[1] = 'a';
    Entry2.au1IpPrefixName[2] = 'a';
    if (RMapIpPrefixCompareEntry (&Entry1, &Entry2) != RMAP_RB_LESSER)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
#endif
/* BGP-BFD */
INT4
BgpUtBfd_1 (VOID)
{
    tSNMP_OCTET_STRING_TYPE IpAddr;
    tBgp4PeerEntry      Peer;
    tBgp4PeerEntry     *pPeer = NULL;
    UINT1               au1Addr[16];
    INT4                i4RetVal = 0;
    UINT1               u1Tmp = 0;

    MEMSET (&Peer, 0, sizeof (tBgp4PeerEntry));
    MEMSET (au1Addr, 0, 16);
    MEMSET (&IpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    IpAddr.pu1_OctetList = au1Addr;

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    u1Tmp = gBgpNode.u1Bgp4TaskInit;
    gBgpNode.u1Bgp4TaskInit = BGP4_FALSE;
    if (nmhGetFsbgp4mpePeerBfdStatus (1, &IpAddr, &i4RetVal) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gBgpNode.u1Bgp4TaskInit = u1Tmp;
    if (nmhGetFsbgp4mpePeerBfdStatus (1, &IpAddr, &i4RetVal) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gpBgpCurrCxtNode->u4ContextId = 0;
    pPeer = &Peer;
    BGP4_PEER_REMOTE_ADDR_INFO (pPeer).u2Afi = 1;
    TMO_SLL_Add (BGP4_PEERENTRY_HEAD (0), (tTMO_SLL_NODE *) & Peer);
    if (nmhGetFsbgp4mpePeerBfdStatus (1, &IpAddr, &i4RetVal) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    TMO_SLL_Delete (BGP4_PEERENTRY_HEAD (0), (tTMO_SLL_NODE *) & Peer);
    return OSIX_SUCCESS;
}

INT4
BgpUtBfd_2 (VOID)
{
    tSNMP_OCTET_STRING_TYPE IpAddr;
    tBgp4PeerEntry      Peer;
    tBgp4PeerEntry     *pPeer = NULL;
    UINT1               au1Addr[16];
    UINT1               u1Tmp = 0;

    MEMSET (&Peer, 0, sizeof (tBgp4PeerEntry));
    MEMSET (au1Addr, 0, 16);
    MEMSET (&IpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    IpAddr.pu1_OctetList = au1Addr;

    u1Tmp = gBgpNode.u1Bgp4TaskInit;
    gBgpNode.u1Bgp4TaskInit = BGP4_FALSE;
    gpBgpCurrCxtNode = gBgpCxtNode[0];
    if (nmhSetFsbgp4mpePeerBfdStatus (1, &IpAddr, BGP4_BFD_ENABLE) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gBgpNode.u1Bgp4TaskInit = u1Tmp;
    if (nmhSetFsbgp4mpePeerBfdStatus (1, &IpAddr, BGP4_BFD_ENABLE) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gpBgpCurrCxtNode->u4ContextId = 0;
    pPeer = &Peer;
    BGP4_PEER_REMOTE_ADDR_INFO (pPeer).u2Afi = 1;
    TMO_SLL_Add (BGP4_PEERENTRY_HEAD (0), (tTMO_SLL_NODE *) & Peer);
    if (nmhSetFsbgp4mpePeerBfdStatus (1, &IpAddr, BGP4_BFD_DISABLE) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsbgp4mpePeerBfdStatus (1, &IpAddr, BGP4_BFD_DISABLE) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    AllocMem (gBgpNode.Bgp4InputQPoolId);
    ReleaseMem (gBgpNode.Bgp4InputQPoolId);
    AllocMem (gBgpNode.Bgp4InputQPoolId);
    if (nmhSetFsbgp4mpePeerBfdStatus (1, &IpAddr, BGP4_BFD_ENABLE) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    ReleaseMem (gBgpNode.Bgp4InputQPoolId);
    TMO_SLL_Delete (BGP4_PEERENTRY_HEAD (0), (tTMO_SLL_NODE *) & Peer);
    return OSIX_SUCCESS;
}

INT4
BgpUtBfd_3 (VOID)
{
    tSNMP_OCTET_STRING_TYPE IpAddr;
    tBgp4PeerEntry      Peer;
    tBgp4PeerEntry     *pPeer = NULL;
    UINT4               u4Err = 0;
    UINT1               au1Addr[16];
    UINT1               u1Tmp = 0;

    MEMSET (&Peer, 0, sizeof (tBgp4PeerEntry));
    MEMSET (au1Addr, 0, 16);
    MEMSET (&IpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    IpAddr.pu1_OctetList = au1Addr;

    u1Tmp = gBgpNode.u1Bgp4TaskInit;
    gBgpNode.u1Bgp4TaskInit = BGP4_FALSE;

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    if (nmhTestv2Fsbgp4mpePeerBfdStatus (&u4Err, 1, &IpAddr, BGP4_BFD_DISABLE)
        != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gBgpNode.u1Bgp4TaskInit = u1Tmp;
    if (nmhTestv2Fsbgp4mpePeerBfdStatus (&u4Err, 1, &IpAddr, 3) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2Fsbgp4mpePeerBfdStatus (&u4Err, 1, &IpAddr, BGP4_BFD_ENABLE) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gpBgpCurrCxtNode->u4ContextId = 0;
    pPeer = &Peer;
    BGP4_PEER_REMOTE_ADDR_INFO (pPeer).u2Afi = 1;
    TMO_SLL_Add (BGP4_PEERENTRY_HEAD (0), (tTMO_SLL_NODE *) & Peer);

    if (nmhTestv2Fsbgp4mpePeerBfdStatus (&u4Err, 1, &IpAddr, BGP4_BFD_ENABLE) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2Fsbgp4mpePeerBfdStatus (&u4Err, 1, &IpAddr, BGP4_BFD_DISABLE)
        != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    TMO_SLL_Delete (BGP4_PEERENTRY_HEAD (0), (tTMO_SLL_NODE *) & Peer);
    return OSIX_SUCCESS;
}

INT4
BgpUtBfd_4 (VOID)
{
    tSNMP_OCTET_STRING_TYPE Name;
    tBgpPeerGroupEntry  PeerGroup;
    UINT1               au1String[32];
    INT4                i4RetVal = 0;
    UINT1               u1Tmp = 0;

    MEMSET (au1String, 0, 32);
    MEMSET (&PeerGroup, 0, sizeof (tBgpPeerGroupEntry));
    MEMSET (&Name, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    Name.pu1_OctetList = au1String;

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    u1Tmp = gBgpNode.u1Bgp4TaskInit;
    gBgpNode.u1Bgp4TaskInit = BGP4_FALSE;
    if (nmhGetFsBgp4PeerGroupBfdStatus (&Name, &i4RetVal) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gBgpNode.u1Bgp4TaskInit = u1Tmp;
    if (nmhGetFsBgp4PeerGroupBfdStatus (&Name, &i4RetVal) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gpBgpCurrCxtNode->u4ContextId = 0;
    TMO_SLL_Add (BGP4_PEERGROUPENTRY_HEAD (0), (tTMO_SLL_NODE *) & PeerGroup);
    if (nmhGetFsBgp4PeerGroupBfdStatus (&Name, &i4RetVal) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    TMO_SLL_Delete (BGP4_PEERGROUPENTRY_HEAD (0),
                    (tTMO_SLL_NODE *) & PeerGroup);
    return OSIX_SUCCESS;
}

INT4
BgpUtBfd_5 (VOID)
{
    tSNMP_OCTET_STRING_TYPE Name;
    tBgpPeerGroupEntry  PeerGroup;
    UINT1               au1String[32];
    UINT1               u1Tmp = 0;

    MEMSET (au1String, 0, 32);
    MEMSET (&PeerGroup, 0, sizeof (tBgpPeerGroupEntry));
    MEMSET (&Name, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    Name.pu1_OctetList = au1String;

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    u1Tmp = gBgpNode.u1Bgp4TaskInit;
    gBgpNode.u1Bgp4TaskInit = BGP4_FALSE;

    if (nmhSetFsBgp4PeerGroupBfdStatus (&Name, BGP4_BFD_ENABLE) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gBgpNode.u1Bgp4TaskInit = u1Tmp;
    if (nmhSetFsBgp4PeerGroupBfdStatus (&Name, BGP4_BFD_ENABLE) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gpBgpCurrCxtNode->u4ContextId = 0;
    TMO_SLL_Add (BGP4_PEERGROUPENTRY_HEAD (0), (tTMO_SLL_NODE *) & PeerGroup);
    if (nmhSetFsBgp4PeerGroupBfdStatus (&Name, BGP4_BFD_ENABLE) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsBgp4PeerGroupBfdStatus (&Name, BGP4_BFD_ENABLE) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    AllocMem (gBgpNode.Bgp4InputQPoolId);
    ReleaseMem (gBgpNode.Bgp4InputQPoolId);
    AllocMem (gBgpNode.Bgp4InputQPoolId);
    if (nmhSetFsBgp4PeerGroupBfdStatus (&Name, BGP4_BFD_DISABLE) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    ReleaseMem (gBgpNode.Bgp4InputQPoolId);
    TMO_SLL_Delete (BGP4_PEERGROUPENTRY_HEAD (0),
                    (tTMO_SLL_NODE *) & PeerGroup);

    return OSIX_SUCCESS;
}

INT4
BgpUtBfd_6 (VOID)
{
    tSNMP_OCTET_STRING_TYPE Name;
    tBgpPeerGroupEntry  PeerGroup;
    UINT4               u4Err = 0;
    UINT1               au1String[32];
    UINT1               u1Tmp = 0;

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    MEMSET (au1String, 0, 32);
    MEMSET (&PeerGroup, 0, sizeof (tBgpPeerGroupEntry));
    MEMSET (&Name, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    Name.pu1_OctetList = au1String;

    u1Tmp = gBgpNode.u1Bgp4TaskInit;
    gBgpNode.u1Bgp4TaskInit = BGP4_FALSE;

    if (nmhTestv2FsBgp4PeerGroupBfdStatus (&u4Err, &Name, BGP4_BFD_ENABLE) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gBgpNode.u1Bgp4TaskInit = u1Tmp;
    if (nmhTestv2FsBgp4PeerGroupBfdStatus (&u4Err, &Name, 3) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsBgp4PeerGroupBfdStatus (&u4Err, &Name, BGP4_BFD_ENABLE) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gpBgpCurrCxtNode->u4ContextId = 0;
    TMO_SLL_Add (BGP4_PEERGROUPENTRY_HEAD (0), (tTMO_SLL_NODE *) & PeerGroup);
    if (nmhTestv2FsBgp4PeerGroupBfdStatus (&u4Err, &Name, BGP4_BFD_DISABLE) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsBgp4PeerGroupBfdStatus (&u4Err, &Name, BGP4_BFD_ENABLE) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    TMO_SLL_Delete (BGP4_PEERGROUPENTRY_HEAD (0),
                    (tTMO_SLL_NODE *) & PeerGroup);
    return OSIX_SUCCESS;
}

INT4
BgpUtBfd_7 (VOID)
{
    tSNMP_OCTET_STRING_TYPE IpAddr;
    UINT1               au1Addr[16];
    INT4                i4Ret = 0;

    MEMSET (au1Addr, 0, 16);
    MEMSET (&IpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    IpAddr.pu1_OctetList = au1Addr;

    if (nmhGetFsMIBgp4mpePeerBfdStatus (1, 1, &IpAddr, &i4Ret) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (nmhGetFsMIBgp4mpePeerBfdStatus (0, 1, &IpAddr, &i4Ret) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
BgpUtBfd_8 (VOID)
{
    tSNMP_OCTET_STRING_TYPE IpAddr;
    UINT1               au1Addr[16];

    MEMSET (au1Addr, 0, 16);
    MEMSET (&IpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    IpAddr.pu1_OctetList = au1Addr;

    if (nmhSetFsMIBgp4mpePeerBfdStatus (1, 1, &IpAddr, BGP4_BFD_ENABLE) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIBgp4mpePeerBfdStatus (0, 1, &IpAddr, BGP4_BFD_ENABLE) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
BgpUtBfd_9 (VOID)
{
    tSNMP_OCTET_STRING_TYPE IpAddr;
    UINT1               au1Addr[16];
    UINT4               u4Err = 0;

    MEMSET (au1Addr, 0, 16);
    MEMSET (&IpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    IpAddr.pu1_OctetList = au1Addr;

    if (nmhTestv2FsMIBgp4mpePeerBfdStatus
        (&u4Err, 1, 1, &IpAddr, BGP4_BFD_ENABLE) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIBgp4mpePeerBfdStatus
        (&u4Err, 0, 1, &IpAddr, BGP4_BFD_ENABLE) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
BgpUtBfd_10 (VOID)
{
    tSNMP_OCTET_STRING_TYPE Name;
    UINT1               au1String[32];
    INT4                i4Ret = 0;

    MEMSET (au1String, 0, 32);
    MEMSET (&Name, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    Name.pu1_OctetList = au1String;

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    if (nmhGetFsMIBgp4PeerGroupBfdStatus (1, &Name, &i4Ret) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsMIBgp4PeerGroupBfdStatus (0, &Name, &i4Ret) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
BgpUtBfd_11 (VOID)
{
    tSNMP_OCTET_STRING_TYPE Name;
    UINT1               au1String[32];

    MEMSET (au1String, 0, 32);
    MEMSET (&Name, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    Name.pu1_OctetList = au1String;

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    if (nmhSetFsMIBgp4PeerGroupBfdStatus (1, &Name, 1) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsMIBgp4PeerGroupBfdStatus (0, &Name, 1) != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
BgpUtBfd_12 (VOID)
{
    tSNMP_OCTET_STRING_TYPE Name;
    UINT1               au1String[32];
    UINT4               u4Err = 0;

    MEMSET (au1String, 0, 32);
    MEMSET (&Name, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    Name.pu1_OctetList = au1String;

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    if (nmhTestv2FsMIBgp4PeerGroupBfdStatus (&u4Err, 1, &Name, 1) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMIBgp4PeerGroupBfdStatus (&u4Err, 0, &Name, 1) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
BgpUtBfd_13 (VOID)
{
    UINT1               au1String[32];
    tBgpPeerGroupEntry  PeerGroup;

    MEMSET (au1String, 0, 32);
    MEMSET (&PeerGroup, 0, sizeof (tBgpPeerGroupEntry));

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    if (Bgp4ConfigPeerGroupBfdStatus (0, NULL, BGP4_BFD_DISABLE) != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (Bgp4ConfigPeerGroupBfdStatus (0, au1String, BGP4_BFD_DISABLE) !=
        CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    TMO_SLL_Add (BGP4_PEERGROUPENTRY_HEAD (0), (tTMO_SLL_NODE *) & PeerGroup);
    AllocMem (gBgpNode.Bgp4InputQPoolId);
    if (Bgp4ConfigPeerGroupBfdStatus (0, au1String, BGP4_BFD_DISABLE) !=
        CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    ReleaseMem (gBgpNode.Bgp4InputQPoolId);
    if (Bgp4ConfigPeerGroupBfdStatus (0, au1String, BGP4_BFD_DISABLE) !=
        CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    TMO_SLL_Delete (BGP4_PEERGROUPENTRY_HEAD (0),
                    (tTMO_SLL_NODE *) & PeerGroup);
    return OSIX_SUCCESS;
}

INT4
BgpUtBfd_14 (VOID)
{
    tBgp4PeerEntry      Peer;
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         Addr;

    MEMSET (&Addr, 0, sizeof (tAddrPrefix));
    MEMSET (&Peer, 0, sizeof (tBgp4PeerEntry));

    gpBgpCurrCxtNode = gBgpCxtNode[0];
    if (Bgp4ConfigPeerBfdStatus (0, NULL, BGP4_BFD_DISABLE) != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (Bgp4ConfigPeerBfdStatus (0, &Addr, BGP4_BFD_DISABLE) != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    Addr.u2Afi = 1;
    if (Bgp4ConfigPeerBfdStatus (0, &Addr, BGP4_BFD_DISABLE) != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    Addr.u2Afi = 2;
    if (Bgp4ConfigPeerBfdStatus (0, &Addr, BGP4_BFD_DISABLE) != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    Addr.u2Afi = 1;
    pPeer = &Peer;
    BGP4_PEER_REMOTE_ADDR_INFO (pPeer).u2Afi = 1;
    TMO_SLL_Add (BGP4_PEERENTRY_HEAD (0), (tTMO_SLL_NODE *) & Peer);
    AllocMem (gBgpNode.Bgp4InputQPoolId);
    if (Bgp4ConfigPeerBfdStatus (0, &Addr, BGP4_BFD_DISABLE) != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    ReleaseMem (gBgpNode.Bgp4InputQPoolId);
    if (Bgp4ConfigPeerBfdStatus (0, &Addr, BGP4_BFD_DISABLE) != CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    TMO_SLL_Delete (BGP4_PEERENTRY_HEAD (0), (tTMO_SLL_NODE *) & Peer);
    return OSIX_SUCCESS;
}

INT4
BgpUtBfd_15 (VOID)
{
    tBgp4PeerEntry      Peer;

    MEMSET (&Peer, 0, sizeof (tBgp4PeerEntry));
    gpBgpCurrCxtNode = gBgpCxtNode[0];

    if (Bgp4SetBfdStatus (0, &Peer) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    Peer.u1BfdStatus = BGP4_BFD_ENABLE;
    if (Bgp4SetBfdStatus (0, &Peer) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    Peer.peerStatus.u1PeerState = BGP4_ESTABLISHED_STATE;
    if (Bgp4SetBfdStatus (0, &Peer) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    Peer.u1BfdStatus = BGP4_BFD_DISABLE;
    if (Bgp4SetBfdStatus (0, &Peer) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
BgpUtBfd_16 (VOID)
{
    tBgp4PeerEntry      Peer;

    MEMSET (&Peer, 0, sizeof (tBgp4PeerEntry));

    if (Bgp4ProcessPeerDownNotification (&Peer, OSIX_TRUE) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (Bgp4ProcessPeerDownNotification (&Peer, OSIX_FALSE) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
BgpUtBfd_17 (VOID)
{
    tBgp4PeerEntry      Peer;

    MEMSET (&Peer, 0, sizeof (tBgp4PeerEntry));

    if (Bgp4RegisterWithBfd (0, &Peer) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
#ifdef BFD_WANTED
    AllocMem (BFD_QUEMSG_POOLID);
#endif
    Peer.peerConfig.RemoteAddrInfo.u2Afi = 1;
    if (Bgp4RegisterWithBfd (0, &Peer) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    Peer.peerConfig.RemoteAddrInfo.u2Afi = 2;
    if (Bgp4RegisterWithBfd (0, &Peer) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("interface gi 0/1");
    CliExecuteAppCmd ("map switch default");
    CliExecuteAppCmd ("no shut");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    Peer.peerConfig.RemoteAddrInfo.u2Afi = 1;
    Peer.peerConfig.RemoteAddrInfo.u2AddressLen = 4;
    Peer.peerConfig.RemoteAddrInfo.au1Address[0] = 2;
    Peer.peerConfig.RemoteAddrInfo.au1Address[1] = 0;
    Peer.peerConfig.RemoteAddrInfo.au1Address[2] = 0;
    Peer.peerConfig.RemoteAddrInfo.au1Address[3] = 12;
    if (Bgp4RegisterWithBfd (0, &Peer) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
#ifdef BFD_WANTED
    ReleaseMem (BFD_QUEMSG_POOLID);
#endif
    return OSIX_SUCCESS;
}

INT4
BgpUtBfd_18 (VOID)
{
    tBgp4PeerEntry      Peer;

    MEMSET (&Peer, 0, sizeof (tBgp4PeerEntry));

    if (Bgp4DeRegisterWithBfd (0, &Peer, TRUE) != BGP4_FAILURE)
    {
        return OSIX_FAILURE;
    }
    Peer.peerConfig.RemoteAddrInfo.u2Afi = 1;
    if (Bgp4DeRegisterWithBfd (0, &Peer, FALSE) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    Peer.peerConfig.RemoteAddrInfo.u2Afi = 2;
    if (Bgp4DeRegisterWithBfd (0, &Peer, FALSE) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("interface gi 0/1");
    CliExecuteAppCmd ("map switch default");
    CliExecuteAppCmd ("no shut");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    CliGiveAppContext ();

    Peer.peerConfig.RemoteAddrInfo.u2Afi = 1;
    Peer.peerConfig.RemoteAddrInfo.u2AddressLen = 4;
    Peer.peerConfig.RemoteAddrInfo.au1Address[0] = 2;
    Peer.peerConfig.RemoteAddrInfo.au1Address[1] = 0;
    Peer.peerConfig.RemoteAddrInfo.au1Address[2] = 0;
    Peer.peerConfig.RemoteAddrInfo.au1Address[3] = 12;
    if (Bgp4DeRegisterWithBfd (0, &Peer, FALSE) != BGP4_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

#ifdef BFD_WANTED
INT4
BgpUtBfd_19 (VOID)
{
    tBfdFsMIStdBfdSessEntry SessEntry;
    tBfdFsMIStdBfdSessEntry SessEntry1;
    UINT1               au1Addr[16];

    MEMSET (&SessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (&SessEntry1, 0, sizeof (tBfdFsMIStdBfdSessEntry));
    MEMSET (au1Addr, 0, sizeof (au1Addr));

    if (BfdCliShowNeighbors
        (0, 0, BFD_CLIENT_ID_BGP, 1, au1Addr,
         CLI_BFD_SHOW_SUMMARY) != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    RBTreeAdd (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable, &SessEntry);
    if (BfdCliShowNeighbors
        (0, 0, BFD_CLIENT_ID_BGP, 1, au1Addr,
         CLI_BFD_SHOW_SUMMARY) != CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (BfdCliShowNeighbors (0, BFD_INVALID_CONTEXT, 0, 0,
                             au1Addr, CLI_BFD_SHOW_DETAILS) != CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    SessEntry.MibObject.u4FsMIStdBfdContextId = 1;
    if (BfdCliShowNeighbors (0, 0, 0, 2,
                             au1Addr, CLI_BFD_SHOW_DETAILS) != CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    SessEntry.MibObject.u4FsMIStdBfdContextId = 0;
    if (BfdCliShowNeighbors (0, 0, 0, 0,
                             au1Addr, CLI_BFD_SHOW_DETAILS) != CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    au1Addr[0] = 1;
    if (BfdCliShowNeighbors (0, 0, 0, 1,
                             au1Addr, CLI_BFD_SHOW_SUMMARY) != CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    au1Addr[0] = 0;
    SessEntry1.MibObject.u4FsMIStdBfdContextId = 1;
    RBTreeAdd (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable, &SessEntry1);
    if (BfdCliShowNeighbors (0, BFD_INVALID_CONTEXT, 0, 0,
                             au1Addr, CLI_BFD_SHOW_SUMMARY) != CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    SessEntry.MibObject.u4FsMIBfdSessRegisteredClients = 0x00000020;
    if (BfdCliShowNeighbors (0, 1, BFD_CLIENT_ID_BGP, 0,
                             au1Addr, CLI_BFD_SHOW_SUMMARY) != CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (BfdCliShowNeighbors (0, BFD_INVALID_CONTEXT, BFD_CLIENT_ID_BGP, 1,
                             au1Addr, CLI_BFD_SHOW_SUMMARY) != CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    SessEntry.MibObject.i4FsMIStdBfdSessDstAddrType = 1;
    SessEntry1.MibObject.i4FsMIStdBfdSessDstAddrType = 1;
    SessEntry1.MibObject.au1FsMIStdBfdSessDstAddr[0] = 1;
    if (BfdCliShowNeighbors (0, BFD_INVALID_CONTEXT, 0, 1,
                             au1Addr, CLI_BFD_SHOW_SUMMARY) != CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    SessEntry1.MibObject.au1FsMIStdBfdSessDstAddr[0] = 0;
    MEMSET (au1Addr, 0, sizeof (au1Addr));
    if (BfdCliShowNeighbors (0, BFD_INVALID_CONTEXT, 0, 1,
                             au1Addr, CLI_BFD_SHOW_SUMMARY) != CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    RBTreeRem (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable, &SessEntry);
    RBTreeRem (gBfdGlobals.BfdGlbMib.FsMIStdBfdSessTable, &SessEntry1);
    return OSIX_SUCCESS;
}

INT4
BgpUtBfd_20 (VOID)
{
    tBfdFsMIStdBfdSessEntry SessEntry;

    MEMSET (&SessEntry, 0, sizeof (tBfdFsMIStdBfdSessEntry));

    SessEntry.MibObject.i4FsMIStdBfdSessSrcAddrType = 1;
    BfdCliPrintNeighbors (0, &SessEntry, 1);

    SessEntry.MibObject.i4FsMIStdBfdSessSrcAddrType = 2;
    SessEntry.MibObject.i4FsMIStdBfdSessState = BFD_SESS_STATE_INIT;
    BfdCliPrintNeighbors (0, &SessEntry, 2);

    SessEntry.MibObject.i4FsMIStdBfdSessSrcAddrType = 1;
    SessEntry.MibObject.u4FsMIBfdSessRegisteredClients = 0x00000060;
    SessEntry.MibObject.i4FsMIStdBfdSessState = BFD_SESS_STATE_UP;
    BfdCliPrintNeighbors (0, &SessEntry, 1);

    SessEntry.MibObject.u4FsMIBfdSessRegisteredClients = 0x00000020;
    SessEntry.MibObject.i4FsMIStdBfdSessState = BFD_SESS_STATE_DOWN;
    BfdCliPrintNeighbors (0, &SessEntry, 1);

    return OSIX_SUCCESS;
}

INT4
BgpUtBfd_21 (VOID)
{
    tBfdClientNbrIpPathInfo NbrInfo;

    MEMSET (&NbrInfo, 0, sizeof (tBfdClientNbrIpPathInfo));

    BGP4_TASK_INIT_STATUS = BGP4_FALSE;
    if (Bgp4HandleNbrPathStatusChange (0, &NbrInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    BGP4_TASK_INIT_STATUS = BGP4_TRUE;
    if (Bgp4HandleNbrPathStatusChange (1, &NbrInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    NbrInfo.u4PathStatus = BFD_SESS_STATE_UP;
    if (Bgp4HandleNbrPathStatusChange (0, &NbrInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    NbrInfo.u4PathStatus = BFD_SESS_STATE_DOWN;
    AllocMem (gBgpNode.Bgp4InputQPoolId);
    if (Bgp4HandleNbrPathStatusChange (0, &NbrInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    ReleaseMem (gBgpNode.Bgp4InputQPoolId);
    NbrInfo.u1AddrType = 0;
    if (Bgp4HandleNbrPathStatusChange (0, &NbrInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    NbrInfo.u1AddrType = 1;
    if (Bgp4HandleNbrPathStatusChange (0, &NbrInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    NbrInfo.u1AddrType = 2;
    if (Bgp4HandleNbrPathStatusChange (0, &NbrInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
#endif

#ifdef VPLSADS_WANTED
/* l2vpn-vpls UT cases */
VOID
BgpUTBasicConf (VOID)
{

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("sh spanning-tree");
    CliExecuteAppCmd ("set gvrp dis");
    CliExecuteAppCmd ("set gmrp dis");
    CliExecuteAppCmd ("sh garp");
    CliExecuteAppCmd ("en");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int loopback 0");
    CliExecuteAppCmd ("ip address 2.2.2.2 255.255.255.255");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("en");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("i g 0/1");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("no sw");
    CliExecuteAppCmd ("ip address 10.0.0.2 255.0.0.0");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("en");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("i g 0/2");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("no sw");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("en");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("router ospf");
    CliExecuteAppCmd ("router-id 2.2.2.2");
    CliExecuteAppCmd ("network 10.0.0.2 area 1.1.1.1");
    CliExecuteAppCmd ("network 2.2.2.2 area 1.1.1.1");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("ip ospf network point-to-point");
    CliExecuteAppCmd ("en");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("router ospf");
    CliExecuteAppCmd ("router-id 2.2.2.2");
    CliExecuteAppCmd ("ASBR router");
    CliExecuteAppCmd ("en");
    CliExecuteAppCmd ("sleep 1");

    MGMT_LOCK ();
    CliGiveAppContext ();

}

VOID
BgpUTRemoveBasicConf (VOID)
{
    CliTakeAppContext ();
    MGMT_UNLOCK ();

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("router ospf");
    CliExecuteAppCmd ("router-id 2.2.2.2");
    CliExecuteAppCmd ("no network 10.0.0.2 area 1.1.1.1");
    CliExecuteAppCmd ("no network 2.2.2.2 area 1.1.1.1");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no router ospf");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("i g 0/2");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("no sw");
    CliExecuteAppCmd ("no ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("no mpls ip");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("no i g 0/2");
    CliExecuteAppCmd ("ex");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("i g 0/1");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("no sw");
    CliExecuteAppCmd ("no ip address 10.0.0.2 255.0.0.0");
    CliExecuteAppCmd ("no mpls ip");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("no i g 0/1");
    CliExecuteAppCmd ("ex");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int loopback 0");
    CliExecuteAppCmd ("no ip address 2.2.2.2 255.255.255.255");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("int loopback 0");
    CliExecuteAppCmd ("en");

    MGMT_LOCK ();
    CliGiveAppContext ();

}

INT4
BgpUtFileL2vpnVpls6_1 (VOID)
{
    /*Case : Bgp4L2vpnEventHandler interface testing */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };

    /*Base conf */
    BgpUTBasicConf ();

    EvtInfo.u4MsgType = BGP_L2VPN_ADVERTISE_MSG;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 1;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal = Bgp4L2vpnEventHandler (&EvtInfo);

    /*Removing Base Conf */
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_2 (VOID)
{

    /*Case : Bgp4L2vpnEventHandler trying to fail the mempool function */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    int                 i, j;
    tBgp4QMsg          *pQMsg[10000];

    for (i = 0; i < 10000; i++)
    {
        pQMsg[i] = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
        if (pQMsg[i] == NULL)
        {
            break;
        }
    }

    /*Base conf */
    BgpUTBasicConf ();

    EvtInfo.u4MsgType = BGP_L2VPN_ADVERTISE_MSG;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 11;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300020;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    /*call mempool function many time and try to fail it */
    i4RetVal = Bgp4L2vpnEventHandler (&EvtInfo);

    if (i4RetVal == BGP4_FAILURE)
    {
        /*Rainiday case passed */
        i4RetVal = BGP4_SUCCESS;
    }

    for (j = 0; j < i; j++)
    {
        MemReleaseMemBlock (gBgpNode.Bgp4InputQPoolId, (UINT1 *) (pQMsg[j]));
    }

    /*Removing Base Conf */
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_3 (VOID)
{
    /*Case : Bgp4L2vpnEventHandler Null Check */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;

    /*Base conf */
    BgpUTBasicConf ();

    MEMSET (&EvtInfo, '\0', sizeof (tBgp4L2VpnEvtInfo));
    i4RetVal = Bgp4L2vpnEventHandler (&EvtInfo);

    if (i4RetVal == BGP4_FAILURE)
    {
        /*Rainiday case passed */
        i4RetVal == BGP4_SUCCESS;
    }

    /*Removing Base Conf */
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_4 (VOID)
{
    /*Case : Testing for Bgp4VplsHandler */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_ADVERTISE_MSG;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 21;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300030;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal = Bgp4L2vpnEventHandler (&EvtInfo);

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_5 (VOID)
{
    /*Case : VPLS Handler Failure check */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    MEMSET (&EvtInfo, '\0', sizeof (tBgp4L2VpnEvtInfo));
    i4RetVal = Bgp4L2vpnEventHandler (&EvtInfo);

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_6 (VOID)
{

    /*Case : Test for BGP_L2VPN_ADVERTISE_MSG */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x01, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_ADVERTISE_MSG;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 31;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300040;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_BOTH_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_BOTH_RT_DELETE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_ADVERTISE_MSG,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_7 (VOID)
{
    /*Case : Test for BGP_L2VPN_ADVERTISE_MSG fail case */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    /*Wrong RD and RT */
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x01, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x70 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x06, 0x09, 0x00, 0x64, 0x00, 0x00, 0x01, 0x50 };

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_ADVERTISE_MSG;
    EvtInfo.u2VeId = 0;
    EvtInfo.u2VeBaseOffset = 00;
    EvtInfo.u2VeBaseSize = 00;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 0;
    EvtInfo.u2Mtu = 0;
    EvtInfo.u4VplsIndex = 0;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_ADVERTISE_MSG,
                               BGP4_DFLT_VRFID);

    if (i4RetVal == BGP4_FAILURE)
    {
        /*Rainy-day case passed */
        i4RetVal = BGP4_SUCCESS;
    }

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;

}

INT4
BgpUtFileL2vpnVpls6_8 (VOID)
{

    /*Case : Test for BGP_L2VPN_WITHDRAWN_MSG */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_WITHDRAWN_MSG;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 41;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300050;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_ADVERTISE_MSG,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_WITHDRAWN_MSG,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_9 (VOID)
{
    /*Case : Test for BGP_L2VPN_VPLS_CREATE */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 51;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300060;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_10 (VOID)
{
    /*Case : Test for BGP_L2VPN_VPLS_CREATE */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
        goto RETURN;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_11 (VOID)
{
#if 1
    /*Case : Test for BGP_L2VPN_VPLS_CREATE Mempool fail */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    int                 i, j;
    tVplsSpecInfo      *pVplsSpecInfo[20000];

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    for (i = 0; i < 20000; i++)
    {
        pVplsSpecInfo[i] =
            Bgp4MemAllocateVplsSpecEntry (sizeof (tVplsSpecInfo));
        if (pVplsSpecInfo[i] == NULL)
        {
            break;
        }
    }

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 51;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300060;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DELETE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
        goto RETURN;
    }

  RETURN:
    for (j = 0; j < i; j++)
    {
        MemReleaseMemBlock (BGP4_VPLS_SPEC_MEMPOOL_ID,
                            (UINT1 *) (pVplsSpecInfo[j]));
    }
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
#endif
}

INT4
BgpUtFileL2vpnVpls6_12 (VOID)
{

    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_UP;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 61;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300070;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_13 (VOID)
{

    /*Case : Test for BGP_L2VPN_VPLS_UP */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_UP;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 61;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300070;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_14 (VOID)
{

    /*Case : Test for BGP_L2VPN_VPLS_UP */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_UP;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;

}

INT4
BgpUtFileL2vpnVpls6_15 (VOID)
{
    /*Case : Test for BGP_L2VPN_VPLS_DOWN */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_DOWN;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 71;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300080;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DOWN, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_16 (VOID)
{
    /*Case : Test for BGP_L2VPN_VPLS_DOWN */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_DOWN;
    EvtInfo.u2VeId = 112;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DOWN, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_17 (VOID)
{
    /*Case : Test for BGP_L2VPN_VPLS_DELETE */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_DELETE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DOWN, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DELETE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_18 (VOID)
{
    /*Case : Test for BGP_L2VPN_VPLS_DELETE */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DELETE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_19 (VOID)
{
    /*Case : Test for BGP_L2VPN_IMPORT_RT_ADD */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_IMPORT_RT_ADD;
    EvtInfo.u2VeId = 1347;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_DELETE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_20 (VOID)
{

    /*Case : Test for BGP_L2VPN_IMPORT_RT_ADD */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_IMPORT_RT_ADD;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 1;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300100;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DOWN, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_DELETE,
                               BGP4_DFLT_VRFID);

    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_21 (VOID)
{

    /*Case : Test for BGP_L2VPN_EXPORT_RT_ADD */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_EXPORT_RT_ADD;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_22 (VOID)
{

    /*Case : Test for BGP_L2VPN_EXPORT_RT_DELETE */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_EXPORT_RT_DELETE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_DELETE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_23 (VOID)
{

    /*Case : Test for BGP_L2VPN_BOTH_RT_ADD */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_BOTH_RT_ADD;
    EvtInfo.u2VeId = 7;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_BOTH_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_24 (VOID)
{

    /*Case : Test for BGP_L2VPN_BOTH_RT_ADD */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_BOTH_RT_ADD;
    EvtInfo.u2VeId = 7;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    /*Deleting if it is already configured */
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_BOTH_RT_DELETE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_BOTH_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_25 (VOID)
{

    /*Case : Test for BGP_L2VPN_BOTH_RT_DELETE */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_BOTH_RT_DELETE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    /*RT can be added already, so no need to return BGP4_FAILURE Here */
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DOWN, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_BOTH_RT_DELETE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_26 (VOID)
{

    /*Case : Test for BGP_L2VPN_BOTH_RT_DELETE */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_BOTH_RT_DELETE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    /*RT can be added already, so no need to return BGP4_FAILURE Here */
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DOWN, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_BOTH_RT_DELETE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }
  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_27 (VOID)
{
#if 1
    /*Case : Sending BGP_L2VPN_DOWN  message from L2VPN to BGP */
    UINT4               i4RetVal = OSIX_SUCCESS;
    tBgp4L2VpnEvtInfo   EvtInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_DOWN;
    EvtInfo.u2VeId = 4;

    i4RetVal = Bgp4L2vpnEventHandler (&EvtInfo);

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
#endif

}

INT4
BgpUtFileL2vpnVpls6_28 (VOID)
{

    /*Case : Test for BGP_L2VPN_BOTH_RT_DELETE */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_BOTH_RT_DELETE;

    i4RetVal = Bgp4VplsUpdateHandler (&EvtInfo, 30, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_29 (VOID)
{

    /*Case : Test for BGP_L2VPN_ADVERTISE_MSG Mempool failure case */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    int                 i, j;
    tBgp4Info          *pBGP4Info[10000];

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    for (i = 0; i < 10000; i++)
    {
        pBGP4Info[i] = Bgp4MemAllocateBgp4info (sizeof (tBgp4Info));
        if (pBGP4Info[i] == NULL)
        {
            break;
        }
    }

    EvtInfo.u4MsgType = BGP_L2VPN_ADVERTISE_MSG;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 31;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300040;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_ADVERTISE_MSG,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
        goto RETURN;
    }
  RETURN:
    for (j = 0; j < i; j++)
    {
        MemReleaseMemBlock (gBgpNode.Bgp4BgpInfoPoolId,
                            (UINT1 *) (pBGP4Info[j]));
    }

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_30 (VOID)
{
    /*Case : Test for BGP_L2VPN_ADVERTISE_MSG Mempool failure case */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    int                 i, j;
    tRouteProfile      *pRtprofile[10000];

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    for (i = 0; i < 10000; i++)
    {
        pRtprofile[i] = Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
        if (pRtprofile[i] == NULL)
        {
            break;
        }
    }

    EvtInfo.u4MsgType = BGP_L2VPN_ADVERTISE_MSG;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 31;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300040;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_ADVERTISE_MSG,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
        goto RETURN;
    }

  RETURN:
    for (j = 0; j < i; j++)
    {
        MemReleaseMemBlock (gBgpNode.Bgp4RtProfilePoolId,
                            (UINT1 *) (pRtprofile[j]));
    }

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_31 (VOID)
{

    /*Case : Test for BGP_L2VPN_WITHDRAWN_MSG */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_WITHDRAWN_MSG;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 41;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300050;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_ADVERTISE_MSG,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    EvtInfo.u2VeId = 25;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_WITHDRAWN_MSG,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
  RETURN:

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_32 (VOID)
{
    /*Case : Test for BGP_L2VPN_WITHDRAWN_MSG  Memory Allocation fail */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    int                 i, j;
    tRouteProfile      *pRtprofile[10000];
    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

#if 1

    for (i = 0; i < 10000; i++)
    {
        pRtprofile[i] = Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
        if (pRtprofile[i] == NULL)
        {
            break;
        }
    }

    EvtInfo.u4MsgType = BGP_L2VPN_WITHDRAWN_MSG;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 41;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300050;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_WITHDRAWN_MSG,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }
  RETURN:

    for (j = 0; j < i; j++)
    {
        MemReleaseMemBlock (gBgpNode.Bgp4RtProfilePoolId,
                            (UINT1 *) (pRtprofile[j]));
    }

#endif
    /*Removing Base Conf */

    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;

}

INT4
BgpUtFileL2vpnVpls6_33 (VOID)
{
    /*Case : Test for BGP_L2VPN_VPLS_CREATE Mempool fail */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    int                 i, j;
    tVplsSpecInfo      *pVplsSpecInfo[10000];

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    for (i = 0; i < 10000; i++)
    {
        pVplsSpecInfo[i] =
            Bgp4MemAllocateVplsSpecEntry (sizeof (tVplsSpecInfo));
        if (pVplsSpecInfo[i] == NULL)
        {
            break;
        }
    }

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 51;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300060;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DELETE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
        goto RETURN;
    }

  RETURN:

    for (j = 0; j < i; j++)
    {
        MemReleaseMemBlock (BGP4_VPLS_SPEC_MEMPOOL_ID,
                            (UINT1 *) (pVplsSpecInfo[j]));
    }
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_34 (VOID)
{
    /*Case : Test for BGP_L2VPN_IMPORT_RT_ADD */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT1[MAX_LEN_RT_VALUE] =
        { 0x01, 0x01, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_IMPORT_RT_ADD;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 1;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300100;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    MEMCPY (EvtInfo.au1RouteTarget, au1RT1, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_DELETE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_35 (VOID)
{
    /*Case : Test for BGP_L2VPN_VPLS_DELETE */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    tVplsSpecInfo       pVplsInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DOWN, BGP4_DFLT_VRFID);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_36 (VOID)
{

    /*Case : Test for BGP_L2VPN_VPLS_UP */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_UP;
    EvtInfo.u2VeId = 1123;
    EvtInfo.u2VeBaseOffset = 61;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300070;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;

}

INT4
BgpUtFileL2vpnVpls6_37 (VOID)
{
    /*Case : Test for Bgp4VplsCreateAdvtMsgforL2VPN */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tRouteProfile       pRtProfile;
    UINT1               PeerAddress[4];
    UINT1              *pu1Str = NULL;
    tExtCommLayer2Info  ExtCommInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_WITHDRAWN_MSG;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 41;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300050;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_ADVERTISE_MSG,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    pRtProfile.pPEPeer = (tBgp4PeerEntry *) malloc (sizeof (tBgp4PeerEntry));
    pRtProfile.pRtInfo = (tBgp4Info *) malloc (sizeof (tBgp4Info));
    pRtProfile.pRtInfo->pExtCommunity =
        (tExtCommunity *) malloc (sizeof (tExtCommunity));
    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal =
        (UINT1 *) malloc (sizeof (UINT1));

    /*IP is 20.0.0.2 */
    PeerAddress[0] = 2;
    PeerAddress[1] = 0;
    PeerAddress[2] = 0;
    PeerAddress[3] = 20;
    MEMCPY (pRtProfile.pPEPeer->peerConfig.RemoteAddrInfo.au1Address,
            PeerAddress, sizeof (PeerAddress));

    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = NULL;
    pRtProfile.pRtInfo->pExtCommunity->u2EcommCnt = L2VPN_EXT_COMM_COUNT;
    pRtProfile.pRtInfo->pExtCommunity->u1CostFlag = BGP4_FALSE;

     /*RT*/ ATTRIBUTE_NODE_CREATE (pu1Str);
    MEMCPY (pu1Str, au1RT, EXT_COMM_VALUE_LEN);

    /*MTU and Control word */
    ExtCommInfo.extCommType = OSIX_HTONS (ECOMM_VALUE_LAYER2_INFO);
    ExtCommInfo.encapsType = ECOMM_LAYER2_INFO_ENCAPS_TYPE;
    ExtCommInfo.bControlWordFlag = 1;
    ExtCommInfo.u2Mtu = 50;
    ExtCommInfo.u2Reserved = 0;
    MEMCPY (pu1Str + EXT_COMM_VALUE_LEN, &ExtCommInfo, EXT_COMM_VALUE_LEN);

    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = pu1Str;
    pRtProfile.pRtInfo->u4AttrFlag |= (BGP4_ATTR_ECOMM_MASK);
    pRtProfile.pRtInfo->pExtCommunity->u1CostFlag = BGP4_FALSE;

    pRtProfile.VplsNlriInfo.u2VeId = 4;
    pRtProfile.VplsNlriInfo.u2VeBaseOffset = 41;
    pRtProfile.VplsNlriInfo.u2VeBaseSize = 10;
    pRtProfile.VplsNlriInfo.u4labelBase = 300050;

    MEMCPY (pRtProfile.VplsNlriInfo.au1RouteDistinguisher, au1RD,
            MAX_LEN_RD_VALUE);

    pRtProfile.pVplsSpecInfo =
        (tVplsSpecInfo *) malloc (sizeof (tVplsSpecInfo));
    STRNCPY (pRtProfile.pVplsSpecInfo->au1VplsName, au1VplsName,
             STRNLEN (au1VplsName, BGP4_VPLS_MAX_VPLS_NAME_SIZE));

    i4RetVal = Bgp4VplsCreateAdvtMsgforL2VPN (&pRtProfile);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_38 (VOID)
{
    /*Case : Test for Bgp4VplsCreateWithdMsgforL2VPN */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tRouteProfile       pRtProfile;
    UINT1               PeerAddress[4];
    UINT1              *pu1Str = NULL;
    tExtCommLayer2Info  ExtCommInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_WITHDRAWN_MSG;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 41;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300050;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_ADVERTISE_MSG,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    pRtProfile.pPEPeer = (tBgp4PeerEntry *) malloc (sizeof (tBgp4PeerEntry));
    pRtProfile.pRtInfo = (tBgp4Info *) malloc (sizeof (tBgp4Info));
    pRtProfile.pRtInfo->pExtCommunity =
        (tExtCommunity *) malloc (sizeof (tExtCommunity));
    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal =
        (UINT1 *) malloc (sizeof (UINT1));

    /*IP is 20.0.0.2 */
    PeerAddress[0] = 2;
    PeerAddress[1] = 0;
    PeerAddress[2] = 0;
    PeerAddress[3] = 20;
    MEMCPY (pRtProfile.pPEPeer->peerConfig.RemoteAddrInfo.au1Address,
            PeerAddress, sizeof (PeerAddress));

    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = NULL;
    pRtProfile.pRtInfo->pExtCommunity->u2EcommCnt = L2VPN_EXT_COMM_COUNT;
    pRtProfile.pRtInfo->pExtCommunity->u1CostFlag = BGP4_FALSE;

     /*RT*/ ATTRIBUTE_NODE_CREATE (pu1Str);
    MEMCPY (pu1Str, au1RT, EXT_COMM_VALUE_LEN);

    /*MTU and Control word */
    ExtCommInfo.extCommType = OSIX_HTONS (ECOMM_VALUE_LAYER2_INFO);
    ExtCommInfo.encapsType = ECOMM_LAYER2_INFO_ENCAPS_TYPE;
    ExtCommInfo.bControlWordFlag = 1;
    ExtCommInfo.u2Mtu = 50;
    ExtCommInfo.u2Reserved = 0;
    MEMCPY (pu1Str + EXT_COMM_VALUE_LEN, &ExtCommInfo, EXT_COMM_VALUE_LEN);

    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = pu1Str;
    pRtProfile.pRtInfo->u4AttrFlag |= (BGP4_ATTR_ECOMM_MASK);
    pRtProfile.pRtInfo->pExtCommunity->u1CostFlag = BGP4_FALSE;

    pRtProfile.VplsNlriInfo.u2VeId = 4;
    pRtProfile.VplsNlriInfo.u2VeBaseOffset = 41;
    pRtProfile.VplsNlriInfo.u2VeBaseSize = 10;
    pRtProfile.VplsNlriInfo.u4labelBase = 300050;

    MEMCPY (pRtProfile.VplsNlriInfo.au1RouteDistinguisher, au1RD,
            MAX_LEN_RD_VALUE);

    pRtProfile.pVplsSpecInfo =
        (tVplsSpecInfo *) malloc (sizeof (tVplsSpecInfo));
    STRNCPY (pRtProfile.pVplsSpecInfo->au1VplsName, au1VplsName,
             STRNLEN (au1VplsName, BGP4_VPLS_MAX_VPLS_NAME_SIZE));

    i4RetVal = Bgp4VplsCreateAdvtMsgforL2VPN (&pRtProfile);

    i4RetVal = Bgp4VplsCreateWithdMsgforL2VPN (&pRtProfile);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_39 (VOID)
{
    INT4                i4RetVal;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    i4RetVal = Bgp4AdminStatusEventToL2Vpn (BGP4_ADMIN_UP);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_40 (VOID)
{
    INT4                i4RetVal;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    i4RetVal = Bgp4AdminStatusEventToL2Vpn (BGP4_ADMIN_DOWN);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_41 (VOID)
{

    /*Case : Test for BGP_L2VPN_WITHDRAWN_MSG */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_WITHDRAWN_MSG;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 41;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300050;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_ADVERTISE_MSG,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    gpBgpCurrCxtNode->u4ContextId = 1;
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_WITHDRAWN_MSG,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }
  RETURN:

    gpBgpCurrCxtNode->u4ContextId = 0;
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_42 (VOID)
{
    /*Rainy day: invalid message type */
    INT4                i4RetVal;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    i4RetVal = Bgp4AdminStatusEventToL2Vpn (1);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_43 (VOID)
{
    tRouteProfile       pRtProfile;
    INT4                i4RetVal;
    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pRtProfile.pVplsSpecInfo =
        (tVplsSpecInfo *) malloc (sizeof (tVplsSpecInfo));

    pRtProfile.pVplsSpecInfo = NULL;
    i4RetVal = Bgp4VplsIsRouteCanBeProcessed (&pRtProfile);

    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_44 (VOID)
{
    tRouteProfile       pRtProfile;
    INT4                i4RetVal;
    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pRtProfile.pVplsSpecInfo =
        (tVplsSpecInfo *) malloc (sizeof (tVplsSpecInfo));

    pRtProfile.pVplsSpecInfo->u4VplsState = BGP4_VPLS_UP;

    i4RetVal = Bgp4VplsIsRouteCanBeProcessed (&pRtProfile);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_45 (VOID)
{
    tRouteProfile       pRtProfile;
    INT4                i4RetVal;
    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pRtProfile.pVplsSpecInfo =
        (tVplsSpecInfo *) malloc (sizeof (tVplsSpecInfo));

    pRtProfile.pVplsSpecInfo->u4VplsState = BGP4_VPLS_DOWN;

    i4RetVal = Bgp4VplsIsRouteCanBeProcessed (&pRtProfile);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_46 (VOID)
{
    INT4                i4RetVal;
    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    Bgp4VplsShutdown ();
    Bgp4VplsInit ();

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return BGP4_SUCCESS;
}

INT4
BgpUtFileL2vpnVpls6_47 (VOID)
{
#if 0
    tRouteProfile       pRt1;
    tRouteProfile       pRt2;

    tBgp4PeerEntry      pPeer;
    INT4                i4RetVal;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pPeer.pBgpCxtNode = (tBgpCxtNode *) malloc (sizeof (tBgpCxtNode));
    pPeer.pBgpCxtNode->u4ContextId = 0;
    pRt1.VplsNlriInfo.u2VeBaseSize = 10;
    pRt1.VplsNlriInfo.u4labelBase = 300050;

    pRt2.VplsNlriInfo.u2VeBaseSize = 10;
    pRt2.VplsNlriInfo.u4labelBase = 300050;

    BGP4_GET_NODE_STATUS = RM_STANDBY;

    if (Bgp4IsSameVplsRoute (&pPeer, &pRt1, &pRt2) == TRUE)
    {
        i4RetVal = BGP4_SUCCESS;
    }
    else
    {
        i4RetVal = BGP4_FAILURE;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
#endif
    return -1;
}

INT4
BgpUtFileL2vpnVpls6_48 (VOID)
{
    tRouteProfile       pRt1;
    tRouteProfile       pRt2;

    tBgp4PeerEntry      pPeer;
    INT4                i4RetVal;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pPeer.pBgpCxtNode = (tBgpCxtNode *) malloc (sizeof (tBgpCxtNode));
    pPeer.pBgpCxtNode->u4ContextId = 10;

    pRt1.VplsNlriInfo.u2VeBaseSize = 10;
    pRt1.VplsNlriInfo.u4labelBase = 300050;

    pRt2.VplsNlriInfo.u2VeBaseSize = 5;
    pRt2.VplsNlriInfo.u4labelBase = 300050;

    if (Bgp4IsSameVplsRoute (&pPeer, &pRt1, &pRt2) == FALSE)
    {
        i4RetVal = BGP4_SUCCESS;
    }
    else
    {
        i4RetVal = BGP4_FAILURE;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;

}

INT4
BgpUtFileL2vpnVpls6_49 (VOID)
{
    tRouteProfile       pRt1;
    tRouteProfile       pRt2;

    tBgp4PeerEntry      pPeer;
    INT4                i4RetVal;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pPeer.pBgpCxtNode = (tBgpCxtNode *) malloc (sizeof (tBgpCxtNode));
    pPeer.pBgpCxtNode->u4ContextId = 0;
    pRt1.VplsNlriInfo.u2VeBaseSize = 10;
    pRt1.VplsNlriInfo.u4labelBase = 300040;

    pRt2.VplsNlriInfo.u2VeBaseSize = 10;
    pRt2.VplsNlriInfo.u4labelBase = 300050;

    if (Bgp4IsSameVplsRoute (&pPeer, &pRt1, &pRt2) == FALSE)
    {
        i4RetVal = BGP4_SUCCESS;
    }
    else
    {
        i4RetVal = BGP4_FAILURE;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_50 (VOID)
{
    /*Case : Test for BGP_L2VPN_ADVERTISE_MSG Mempool failure case for Bgp4VplsGetPathAttrinfo */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    int                 i, j;
    tExtCommunity      *pExtCommNode[20000];

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    for (i = 0; i < 20000; i++)
    {
        pExtCommNode[i] =
            (tExtCommunity *) MemAllocMemBlk (BGP_EXT_COMMUNITY_NODE_POOL_ID);
        if (pExtCommNode[i] == NULL)
        {
            break;
        }
    }

    EvtInfo.u4MsgType = BGP_L2VPN_ADVERTISE_MSG;
    EvtInfo.u2VeId = 181;
    EvtInfo.u2VeBaseOffset = 31;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300040;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_ADVERTISE_MSG,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
        goto RETURN;
    }
  RETURN:
    for (j = 0; j < 20000; j++)
    {
        if (MemReleaseMemBlock
            (BGP_EXT_COMMUNITY_NODE_POOL_ID,
             (UINT1 *) (pExtCommNode[j])) == MEM_FAILURE)
        {
            break;
        }
    }

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_51 (VOID)
{
    /*Case : Test for BGP_L2VPN_ADVERTISE_MSG Mempool failure case for Bgp4VplsGetPathAttrinfo */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    int                 i, j;
    UINT1              *pu1Str[10000];

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    for (i = 0; i < 10000; i++)
    {
        pu1Str[i] = (UINT1 *) MemAllocMemBlk (BGP_ATTRIBUTE_LEN_POOL_ID);
        if (pu1Str[i] == NULL)
        {
            break;
        }
    }

    EvtInfo.u4MsgType = BGP_L2VPN_ADVERTISE_MSG;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 31;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300040;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_ADVERTISE_MSG,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
        goto RETURN;
    }
  RETURN:
    for (j = 0; j < i; j++)
    {
        MemReleaseMemBlock (BGP_ATTRIBUTE_LEN_POOL_ID, (UINT1 *) (pu1Str[j]));
    }

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_52 (VOID)
{
    /*Case : Test for Bgp4VplsCreateAdvtMsgforL2VPN */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tRouteProfile       pRtProfile;
    UINT1               PeerAddress[4];
    UINT1              *pu1Str = NULL;
    tExtCommLayer2Info  ExtCommInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_WITHDRAWN_MSG;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 41;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300050;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_ADVERTISE_MSG,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
    pRtProfile.pPEPeer = (tBgp4PeerEntry *) malloc (sizeof (tBgp4PeerEntry));
    pRtProfile.pRtInfo = (tBgp4Info *) malloc (sizeof (tBgp4Info));
    pRtProfile.pRtInfo->pExtCommunity =
        (tExtCommunity *) malloc (sizeof (tExtCommunity));
    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal =
        (UINT1 *) malloc (sizeof (UINT1));

    /*IP is 20.0.0.2 */
    PeerAddress[0] = 2;
    PeerAddress[1] = 0;
    PeerAddress[2] = 0;
    PeerAddress[3] = 20;
    MEMCPY (pRtProfile.pPEPeer->peerConfig.RemoteAddrInfo.au1Address,
            PeerAddress, sizeof (PeerAddress));

    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = NULL;
    pRtProfile.pRtInfo->pExtCommunity->u2EcommCnt = 0;
    pRtProfile.pRtInfo->pExtCommunity->u1CostFlag = BGP4_FALSE;

     /*RT*/ ATTRIBUTE_NODE_CREATE (pu1Str);
    MEMCPY (pu1Str, au1RT, EXT_COMM_VALUE_LEN);

    /*MTU and Control word */
    ExtCommInfo.extCommType = OSIX_HTONS (ECOMM_VALUE_LAYER2_INFO);
    ExtCommInfo.encapsType = ECOMM_LAYER2_INFO_ENCAPS_TYPE;
    ExtCommInfo.bControlWordFlag = 1;
    ExtCommInfo.u2Mtu = 50;
    ExtCommInfo.u2Reserved = 0;
    MEMCPY (pu1Str + EXT_COMM_VALUE_LEN, &ExtCommInfo, EXT_COMM_VALUE_LEN);

    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = pu1Str;
    pRtProfile.pRtInfo->u4AttrFlag |= (BGP4_ATTR_ECOMM_MASK);
    pRtProfile.pRtInfo->pExtCommunity->u1CostFlag = BGP4_FALSE;
    pRtProfile.VplsNlriInfo.u2VeId = 4;
    pRtProfile.VplsNlriInfo.u2VeBaseOffset = 41;
    pRtProfile.VplsNlriInfo.u2VeBaseSize = 10;
    pRtProfile.VplsNlriInfo.u4labelBase = 300050;

    MEMCPY (pRtProfile.VplsNlriInfo.au1RouteDistinguisher, au1RD,
            MAX_LEN_RD_VALUE);

    pRtProfile.pVplsSpecInfo =
        (tVplsSpecInfo *) malloc (sizeof (tVplsSpecInfo));
    STRNCPY (pRtProfile.pVplsSpecInfo->au1VplsName, au1VplsName,
             STRNLEN (au1VplsName, BGP4_VPLS_MAX_VPLS_NAME_SIZE));

    i4RetVal = Bgp4VplsCreateAdvtMsgforL2VPN (&pRtProfile);

    i4RetVal = Bgp4VplsCreateWithdMsgforL2VPN (&pRtProfile);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_53 (VOID)
{
    INT4                i4RetVal;
    tRouteProfile       pRtProfile;
    UINT1               au1RouteTarget[8];

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pRtProfile.pRtInfo = (tBgp4Info *) malloc (sizeof (tBgp4Info));

    pRtProfile.pRtInfo->pExtCommunity =
        (tExtCommunity *) malloc (sizeof (tExtCommunity));

    pRtProfile.pRtInfo->pExtCommunity = NULL;
    i4RetVal = Bgp4VplsGetRTfromRouteProfile (&pRtProfile, au1RouteTarget);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }

  RETURN:

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_54 (VOID)
{
    /*Case : Test for Bgp4VplsCreateAdvtMsgforL2VPN */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tRouteProfile       pRtProfile;
    UINT1               PeerAddress[4];
    UINT1              *pu1Str = NULL;
    tExtCommLayer2Info  ExtCommInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_WITHDRAWN_MSG;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 41;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300050;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_ADVERTISE_MSG,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    pRtProfile.pPEPeer = (tBgp4PeerEntry *) malloc (sizeof (tBgp4PeerEntry));
    pRtProfile.pRtInfo = (tBgp4Info *) malloc (sizeof (tBgp4Info));
    pRtProfile.pRtInfo->pExtCommunity =
        (tExtCommunity *) malloc (sizeof (tExtCommunity));
    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal =
        (UINT1 *) malloc (sizeof (UINT1));

    /*IP is 20.0.0.2 */
    PeerAddress[0] = 2;
    PeerAddress[1] = 0;
    PeerAddress[2] = 0;
    PeerAddress[3] = 20;
    MEMCPY (pRtProfile.pPEPeer->peerConfig.RemoteAddrInfo.au1Address,
            PeerAddress, sizeof (PeerAddress));

    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = NULL;
    pRtProfile.pRtInfo->pExtCommunity->u2EcommCnt = L2VPN_EXT_COMM_COUNT;
    pRtProfile.pRtInfo->pExtCommunity->u1CostFlag = BGP4_FALSE;

     /*RT*/ ATTRIBUTE_NODE_CREATE (pu1Str);
    MEMCPY (pu1Str, au1RT, EXT_COMM_VALUE_LEN);

    /*MTU and Control word */
    ExtCommInfo.extCommType = OSIX_HTONS (ECOMM_VALUE_LAYER2_INFO);
    ExtCommInfo.encapsType = 12;    /*Any invalid type */
    ExtCommInfo.bControlWordFlag = 1;
    ExtCommInfo.u2Mtu = 50;
    ExtCommInfo.u2Reserved = 0;
    MEMCPY (pu1Str + EXT_COMM_VALUE_LEN, &ExtCommInfo, EXT_COMM_VALUE_LEN);

    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = pu1Str;
    pRtProfile.pRtInfo->u4AttrFlag |= (BGP4_ATTR_ECOMM_MASK);
    pRtProfile.pRtInfo->pExtCommunity->u1CostFlag = BGP4_FALSE;

    pRtProfile.VplsNlriInfo.u2VeId = 4;
    pRtProfile.VplsNlriInfo.u2VeBaseOffset = 41;
    pRtProfile.VplsNlriInfo.u2VeBaseSize = 10;
    pRtProfile.VplsNlriInfo.u4labelBase = 300050;

    MEMCPY (pRtProfile.VplsNlriInfo.au1RouteDistinguisher, au1RD,
            MAX_LEN_RD_VALUE);

    pRtProfile.pVplsSpecInfo =
        (tVplsSpecInfo *) malloc (sizeof (tVplsSpecInfo));
    STRNCPY (pRtProfile.pVplsSpecInfo->au1VplsName, au1VplsName,
             STRNLEN (au1VplsName, BGP4_VPLS_MAX_VPLS_NAME_SIZE));

    i4RetVal = Bgp4VplsCreateAdvtMsgforL2VPN (&pRtProfile);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_55 (VOID)
{
    INT4                i4RetVal;
    tRouteProfile       pRtProfile;
    tExtCommLayer2Info  ExtCommInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pRtProfile.pRtInfo = (tBgp4Info *) malloc (sizeof (tBgp4Info));

    pRtProfile.pRtInfo->pExtCommunity =
        (tExtCommunity *) malloc (sizeof (tExtCommunity));

    pRtProfile.pRtInfo->pExtCommunity = NULL;
    i4RetVal = Bgp4VplsGetL2ExtCommInfo (&pRtProfile, &ExtCommInfo);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }

  RETURN:

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_56 (VOID)
{
    INT4                i4RetVal;
    tRouteProfile       pRtProfile;
    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pRtProfile.pRtInfo = (tBgp4Info *) malloc (sizeof (tBgp4Info));
    pRtProfile.u1Protocol = BGP_ID;

    i4RetVal = Bgp4VplsFillVplsExtComm (&pRtProfile, NULL, NULL);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;

}

INT4
BgpUtFileL2vpnVpls6_57 (VOID)
{
    INT4                i4RetVal;
    tRouteProfile       pRtProfile;
    tBgp4Info           pAdvtBgpInfo;
    tExtCommLayer2Info  ExtCommInfo;
    UINT1              *pu1Str = NULL;
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pRtProfile.pRtInfo = (tBgp4Info *) malloc (sizeof (tBgp4Info));
    pRtProfile.pRtInfo->pExtCommunity =
        (tExtCommunity *) malloc (sizeof (tExtCommunity));

    pAdvtBgpInfo.pExtCommunity =
        (tExtCommunity *) malloc (sizeof (tExtCommunity));

     /*RT*/ ATTRIBUTE_NODE_CREATE (pu1Str);
    MEMCPY (pu1Str, au1RT, EXT_COMM_VALUE_LEN);

    /*MTU and Control word */
    ExtCommInfo.extCommType = OSIX_HTONS (ECOMM_VALUE_LAYER2_INFO);
    ExtCommInfo.encapsType = 12;    /*Any invalid type */
    ExtCommInfo.bControlWordFlag = 1;
    ExtCommInfo.u2Mtu = 50;
    ExtCommInfo.u2Reserved = 0;
    MEMCPY (pu1Str + EXT_COMM_VALUE_LEN, &ExtCommInfo, EXT_COMM_VALUE_LEN);

    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = pu1Str;

    pRtProfile.pRtInfo->pExtCommunity->u2EcommCnt = L2VPN_EXT_COMM_COUNT;
    pRtProfile.pRtInfo->pExtCommunity->u1CostFlag = BGP4_FALSE;

    i4RetVal = Bgp4VplsFillVplsExtComm (&pRtProfile, NULL, &pAdvtBgpInfo);

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_58 (VOID)
{
    /*Case : Test for BGP_L2VPN_IMPORT_RT_ADD */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_IMPORT_RT_ADD;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 1;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300100;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_DELETE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;

}

INT4
BgpUtFileL2vpnVpls6_59 (VOID)
{
    /*Case : Test for BGP_L2VPN_IMPORT_RT_ADD */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    tRouteProfile       pRtProfile;
    tVplsSpecInfo      *pVplsSpecInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_IMPORT_RT_ADD;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

    pRtProfile.pVplsSpecInfo =
        (tVplsSpecInfo *) malloc (sizeof (tVplsSpecInfo));

    pVplsSpecInfo = Bgp4VplsFindMatchingImportRT (au1RT, &pRtProfile);
    if (pVplsSpecInfo != NULL)
    {
        i4RetVal = BGP4_SUCCESS;
    }
    else
    {
        i4RetVal = BGP4_FAILURE;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_60 (VOID)
{
    /*Case : Test for BGP_L2VPN_IMPORT_RT_ADD */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT1[MAX_LEN_RT_VALUE] =
        { 0x01, 0x01, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    tRouteProfile       pRtProfile;
    tVplsSpecInfo      *pVplsSpecInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_IMPORT_RT_ADD;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

    pRtProfile.pVplsSpecInfo =
        (tVplsSpecInfo *) malloc (sizeof (tVplsSpecInfo));

    pVplsSpecInfo = Bgp4VplsFindMatchingImportRT (au1RT1, &pRtProfile);
    if (pVplsSpecInfo == NULL)
    {
        i4RetVal = BGP4_SUCCESS;
    }
    else
    {
        i4RetVal = BGP4_FAILURE;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_61 (VOID)
{
    /*Case : Test for BGP_L2VPN_IMPORT_RT_ADD */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    tBgp4VplsRrImportTargetInfo *pRrImportTarget = NULL;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_IMPORT_RT_ADD;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

    pRrImportTarget = Bgp4VplsIsRTMatchInRRImportRTList (au1RT);

    if (pRrImportTarget != NULL)
    {
        i4RetVal = BGP4_FAILURE;
    }
    else
    {
        i4RetVal = BGP4_SUCCESS;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_62 (VOID)
{
    /*Case : Test for BGP_L2VPN_IMPORT_RT_ADD */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT1[MAX_LEN_RT_VALUE] =
        { 0x01, 0x01, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    tBgp4VplsRrImportTargetInfo *pRrImportTarget = NULL;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_IMPORT_RT_ADD;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

    pRrImportTarget = Bgp4VplsIsRTMatchInRRImportRTList (au1RT1);

    if (pRrImportTarget == NULL)
    {
        i4RetVal = BGP4_SUCCESS;
    }
    else
    {
        i4RetVal = BGP4_FAILURE;
    }
  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_63 (VOID)
{
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    tVplsSpecInfo       pVplsInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal = Bgp4ProcessVplsDown (&pVplsInfo, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FALSE)
    {
        i4RetVal = BGP4_SUCCESS;
    }
    else
    {
        i4RetVal = BGP4_FAILURE;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_64 (VOID)
{
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    tVplsSpecInfo       pVplsInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_65 (VOID)
{
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    tVplsSpecInfo       pVplsInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DOWN, BGP4_DFLT_VRFID);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_66 (VOID)
{
    /*Case : Test for BGP_L2VPN_VPLS_DELETE */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    tVplsSpecInfo       pVplsInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DOWN, BGP4_DFLT_VRFID);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_67 (VOID)
{
    /*Case : Test for BGP_L2VPN_IMPORT_RT_ADD */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_IMPORT_RT_ADD;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 1;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300100;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    EvtInfo.u2VeId = 10;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_DELETE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_70 (VOID)
{
    INT4                i4RetVal;
    tRouteProfile       pRtProfile;
    tExtCommLayer2Info  ExtCommInfo;
    tBgp4Info           pRtInfo;
    tExtCommunity       pExtCommunity;
    UINT1               pu1EcommVal[16];
    UINT1               au1RouteTarget[8];
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x03, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pRtProfile.pRtInfo = &pRtInfo;

    pRtProfile.pRtInfo->pExtCommunity = &pExtCommunity;
    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = pu1EcommVal;

    BGP4_INFO_ECOMM_COUNT (pRtProfile.pRtInfo) = L2VPN_EXT_COMM_COUNT;

    MEMCPY (pu1EcommVal, au1RT, EXT_COMM_VALUE_LEN);

    i4RetVal = Bgp4VplsGetRTfromRouteProfile (&pRtProfile, au1RouteTarget);

    /*Rainy Day */
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }

  RETURN:

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;

}

INT4
BgpUtFileL2vpnVpls6_69 (VOID)
{
    INT4                i4RetVal;
    tRouteProfile       pRtProfile;
    tExtCommLayer2Info  ExtCommInfo;
    tBgp4Info           pRtInfo;
    tExtCommunity       pExtCommunity;
    UINT1               pu1EcommVal[16];

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pRtProfile.pRtInfo = &pRtInfo;

    pRtProfile.pRtInfo->pExtCommunity = &pExtCommunity;
    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = pu1EcommVal;

    BGP4_INFO_ECOMM_COUNT (pRtProfile.pRtInfo) = L2VPN_EXT_COMM_COUNT;
    ExtCommInfo.extCommType = OSIX_HTONS (ECOMM_VALUE_LAYER2_INFO);
    ExtCommInfo.encapsType = ECOMM_LAYER2_INFO_ENCAPS_TYPE + 1;    /* invalid value */
    ExtCommInfo.bControlWordFlag = 1;
    ExtCommInfo.u2Mtu = 50;
    ExtCommInfo.u2Reserved = 0;

    MEMCPY (pu1EcommVal, &ExtCommInfo, EXT_COMM_VALUE_LEN);

    i4RetVal = Bgp4VplsGetL2ExtCommInfo (&pRtProfile, &ExtCommInfo);
    /* Rainy day */
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }

  RETURN:

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_71 (VOID)
{
    INT4                i4RetVal;
    tRouteProfile       pRtProfile;
    tExtCommLayer2Info  ExtCommInfo;
    tBgp4Info           pRtInfo;
    tExtCommunity       pExtCommunity;
    UINT1               pu1EcommVal[16];
    UINT1               au1RouteTarget[8];
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x05, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pRtProfile.pRtInfo = &pRtInfo;

    pRtProfile.pRtInfo->pExtCommunity = &pExtCommunity;
    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = pu1EcommVal;

    BGP4_INFO_ECOMM_COUNT (pRtProfile.pRtInfo) = L2VPN_EXT_COMM_COUNT;

    MEMCPY (pu1EcommVal, au1RT, EXT_COMM_VALUE_LEN);

    i4RetVal = Bgp4VplsGetRTfromRouteProfile (&pRtProfile, au1RouteTarget);

    /*Rainy Day */
    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }

  RETURN:

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_72 (VOID)
{
    INT4                i4RetVal;
    tRouteProfile       pRtProfile;
    tExtCommLayer2Info  ExtCommInfo;
    tBgp4Info           pRtInfo;
    tExtCommunity       pExtCommunity;
    UINT1               pu1EcommVal[16];
    UINT1               au1RouteTarget[8];
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x01, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pRtProfile.pRtInfo = &pRtInfo;

    pRtProfile.pRtInfo->pExtCommunity = &pExtCommunity;
    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = pu1EcommVal;

    BGP4_INFO_ECOMM_COUNT (pRtProfile.pRtInfo) = L2VPN_EXT_COMM_COUNT;

    MEMCPY (pu1EcommVal, au1RT, EXT_COMM_VALUE_LEN);

    i4RetVal = Bgp4VplsGetRTfromRouteProfile (&pRtProfile, au1RouteTarget);

  RETURN:

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;

}

INT4
BgpUtFileL2vpnVpls6_73 (VOID)
{
    INT4                i4RetVal;
    tRouteProfile       pRtProfile;
    tExtCommLayer2Info  ExtCommInfo;
    tBgp4Info           pRtInfo;
    tExtCommunity       pExtCommunity;
    UINT1               pu1EcommVal[16];
    UINT1               au1RouteTarget[8];
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x02, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pRtProfile.pRtInfo = &pRtInfo;

    pRtProfile.pRtInfo->pExtCommunity = &pExtCommunity;
    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = pu1EcommVal;

    BGP4_INFO_ECOMM_COUNT (pRtProfile.pRtInfo) = L2VPN_EXT_COMM_COUNT;

    MEMCPY (pu1EcommVal, au1RT, EXT_COMM_VALUE_LEN);

    i4RetVal = Bgp4VplsGetRTfromRouteProfile (&pRtProfile, au1RouteTarget);

  RETURN:

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_74 (VOID)
{
    INT4                i4RetVal;
    tRouteProfile       pRtProfile;
    tExtCommLayer2Info  ExtCommInfo;
    tBgp4Info           pRtInfo;
    tExtCommunity       pExtCommunity;
    tExtCommunity       pAdvtExtCommunity;
    UINT1               pu1EcommVal[16];
    UINT1               au1RouteTarget[8];
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    tBgp4Info           pAdvtBgpInfo;
    tBgp4Info          *pAdBgpInfo = &pAdvtBgpInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pRtProfile.u1Protocol = BGP_ID;
    BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) = 1;

    pRtProfile.pRtInfo = &pRtInfo;
    pRtProfile.pRtInfo->pExtCommunity = &pExtCommunity;
    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = pu1EcommVal;

    pAdvtBgpInfo.pExtCommunity = &pAdvtExtCommunity;

    BGP4_INFO_ECOMM_COUNT (pRtProfile.pRtInfo) = L2VPN_EXT_COMM_COUNT;

    MEMCPY (pu1EcommVal, au1RT, EXT_COMM_VALUE_LEN);

    ExtCommInfo.extCommType = OSIX_HTONS (ECOMM_VALUE_LAYER2_INFO);
    ExtCommInfo.encapsType = ECOMM_LAYER2_INFO_ENCAPS_TYPE;
    ExtCommInfo.bControlWordFlag = 1;
    ExtCommInfo.u2Mtu = 50;
    ExtCommInfo.u2Reserved = 0;

    MEMCPY (pu1EcommVal + EXT_COMM_VALUE_LEN, &ExtCommInfo, EXT_COMM_VALUE_LEN);

    i4RetVal = Bgp4VplsFillVplsExtComm (&pRtProfile, NULL, &pAdvtBgpInfo);

    ATTRIBUTE_NODE_FREE (BGP4_INFO_ECOMM_ATTR_VAL (pAdBgpInfo));
    BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) = 0;
  RETURN:

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_75 (VOID)
{
    INT4                i4RetVal;
    tRouteProfile       pRtProfile;
    tExtCommLayer2Info  ExtCommInfo;
    tBgp4Info           pRtInfo;
    tExtCommunity       pExtCommunity;
    tExtCommunity       pAdvtExtCommunity;
    UINT1               pu1EcommVal[16];
    UINT1               au1RouteTarget[8];
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    tBgp4Info           pAdvtBgpInfo;
    tBgp4Info          *pAdBgpInfo = &pAdvtBgpInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pRtProfile.u1Protocol = 2;
    BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) = 1;

    pRtProfile.pRtInfo = &pRtInfo;
    pRtProfile.pRtInfo->pExtCommunity = &pExtCommunity;
    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = pu1EcommVal;

    pAdvtBgpInfo.pExtCommunity = &pAdvtExtCommunity;

    BGP4_INFO_ECOMM_COUNT (pRtProfile.pRtInfo) = L2VPN_EXT_COMM_COUNT;

    MEMCPY (pu1EcommVal, au1RT, EXT_COMM_VALUE_LEN);

    ExtCommInfo.extCommType = OSIX_HTONS (ECOMM_VALUE_LAYER2_INFO);
    ExtCommInfo.encapsType = ECOMM_LAYER2_INFO_ENCAPS_TYPE;
    ExtCommInfo.bControlWordFlag = 1;
    ExtCommInfo.u2Mtu = 50;
    ExtCommInfo.u2Reserved = 0;

    MEMCPY (pu1EcommVal + EXT_COMM_VALUE_LEN, &ExtCommInfo, EXT_COMM_VALUE_LEN);

    i4RetVal = Bgp4VplsFillVplsExtComm (&pRtProfile, NULL, &pAdvtBgpInfo);

    ATTRIBUTE_NODE_FREE (BGP4_INFO_ECOMM_ATTR_VAL (pAdBgpInfo));
    BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) = 0;
  RETURN:

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_76 (VOID)
{
    /* UT for Bgp4VplsRrJoin */

    INT4                i4RetVal;
    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    i4RetVal = Bgp4VplsRrJoin ();

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_77 (VOID)
{
    /* UT Bgp4VplsAddNewRTInRRImportList */
    INT4                i4RetVal;
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    tBgp4VplsRrImportTargetInfo *pVplsRrImportTarget = NULL;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pVplsRrImportTarget = Bgp4VplsAddNewRTInRRImportList (au1RT);

    if (pVplsRrImportTarget != NULL)
    {
        i4RetVal = BGP4_SUCCESS;
    }
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();
    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_78 (VOID)
{
    /* Bgp4VplsRrDeleteTargetFromSet */
    INT4                i4RetVal;
    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    i4RetVal = Bgp4VplsRrDeleteTargetFromSet ();

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_79 (VOID)
{
    /* UT Bgp4VplsRrPrune */
    INT4                i4RetVal;
    tRouteProfile       pRtProfile;
    tExtCommLayer2Info  ExtCommInfo;
    tBgp4Info           pRtInfo;
    tExtCommunity       pExtCommunity;
    UINT1               pu1EcommVal[16];
    UINT1               au1RouteTarget[8];
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x02, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pRtProfile.pRtInfo = &pRtInfo;
    pRtProfile.pRtInfo->pExtCommunity = NULL;
    i4RetVal = Bgp4VplsRrPrune (&pRtProfile);

  RETURN:

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();
    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_68 (VOID)
{
    /* UT Bgp4VplsApplyImportRTFilter */
    tTMO_SLL            TsFeasibleroutes;
    TMO_SLL_Init (&TsFeasibleroutes);
    INT4                i4RetVal;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    i4RetVal = Bgp4VplsApplyImportRTFilter (&(TsFeasibleroutes), NULL);

    if (i4RetVal == BGP4_FAILURE)
    {
        /*Rainy Day */
        i4RetVal = BGP4_SUCCESS;
    }

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_80 (VOID)
{
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tRouteProfile       pRtProfile;
    UINT1               PeerAddress[4];
    UINT1              *pu1Str = NULL;
    tExtCommLayer2Info  ExtCommInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 41;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300050;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal = Bgp4VplsCreate (&EvtInfo, &pVplsInfo, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    if (pVplsInfo != NULL)
    {
        i4RetVal = Bgp4VplsDelete (pVplsInfo, BGP4_DFLT_VRFID);
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();
    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_81 (VOID)
{
    /* UT Bgp4VplsApplyImportRTFilter */
    tTMO_SLL            TsFeasibleroutes;
    INT4                i4RetVal;
    tLinkNode           pLknode;
    tRouteProfile       pRtprofile;
    tRouteProfile      *pRtp = &pRtprofile;
    tLinkNode          *pLk = &pLknode;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    TMO_SLL_Init (&TsFeasibleroutes);

    BGP4_RT_AFI_INFO (pRtp) = 1;    /*ipv4 */
    BGP4_RT_SAFI_INFO (pRtp) = 1;

    BGP4_LINK_PROFILE_TO_NODE (pRtp, pLk);
    TMO_SLL_Add (&TsFeasibleroutes, &pLk->TSNext);

    i4RetVal = Bgp4VplsApplyImportRTFilter (&(TsFeasibleroutes), NULL);

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_82 (VOID)
{
    /* UT Bgp4VplsApplyImportRTFilter */
    tTMO_SLL            TsFeasibleroutes;
    INT4                i4RetVal;
    tLinkNode           pLknode;
    tRouteProfile       pRtprofile;
    tRouteProfile      *pRtp = &pRtprofile;
    tLinkNode          *pLk = &pLknode;
    tBgp4Info           pRtInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    TMO_SLL_Init (&TsFeasibleroutes);

    BGP4_RT_AFI_INFO (pRtp) = BGP4_INET_AFI_L2VPN;
    BGP4_RT_SAFI_INFO (pRtp) = BGP4_INET_SAFI_VPLS;
    pRtprofile.pRtInfo = &pRtInfo;

    pRtprofile.pRtInfo->pExtCommunity = NULL;

    BGP4_LINK_PROFILE_TO_NODE (pRtp, pLk);
    TMO_SLL_Add (&TsFeasibleroutes, &pLk->TSNext);

    i4RetVal = Bgp4VplsApplyImportRTFilter (&(TsFeasibleroutes), NULL);

    if (BGP4_FAILURE == i4RetVal)
    {
        /* Rainy Day */
        i4RetVal = BGP4_SUCCESS;
    }

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_83 (VOID)
{
    /*Bgp4VplsMPWithdrawnHandler */
    INT4                i4RetVal;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    tPeerRouteAdvtInfo  pPeerRouteWithdInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pPeerRouteWithdInfo.u2VeId = 4;
    pPeerRouteWithdInfo.u2VeBaseOffset = 41;
    pPeerRouteWithdInfo.u2VeBaseSize = 10;
    pPeerRouteWithdInfo.u4labelBase = 300050;

    MEMCPY (pPeerRouteWithdInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);

    i4RetVal =
        Bgp4VplsMPWithdrawnHandler (&pPeerRouteWithdInfo,
                                    57 /*invalid vpls index */ , 0);

    if (BGP4_FAILURE == i4RetVal)
    {
        /*Rainy Day */
        i4RetVal = BGP4_SUCCESS;
    }

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();
    return i4RetVal;

}

INT4
BgpUtFileL2vpnVpls6_84 (VOID)
{
    /*Bgp4VplsConstructRouteProfile */
    INT4                i4RetVal;
    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    i4RetVal =
        Bgp4VplsConstructRouteProfile (5 /*invalid context */ , NULL, NULL,
                                       NULL, 0);
    if (BGP4_FAILURE == i4RetVal)
    {
        /* Rainy Day */
        i4RetVal = BGP4_SUCCESS;
    }

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();
    return i4RetVal;

}

INT4
BgpUtFileL2vpnVpls6_85 (VOID)
{
    /*Bgp4VplsCreateAdvtMsgforL2VPN */
    INT4                i4RetVal;
    tRouteProfile       pRtProfile;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pRtProfile.pVplsSpecInfo = NULL;
    i4RetVal = Bgp4VplsCreateAdvtMsgforL2VPN (&pRtProfile);

    if (BGP4_FAILURE == i4RetVal)
    {
        i4RetVal = BGP4_SUCCESS;
    }

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();
    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_86 (VOID)
{
    /* UT Bgp4VplsApplyImportRTFilter */
    tRouteProfile       pRtProfile;
    tExtCommLayer2Info  ExtCommInfo;
    tBgp4Info           pRtInfo;
    tExtCommunity       pExtCommunity;
    UINT1               pu1EcommVal[16];
    UINT1               au1RouteTarget[8];
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    tTMO_SLL            TsFeasibleroutes;
    INT4                i4RetVal;
    tLinkNode           pLknode;
    tRouteProfile      *pRtp = &pRtProfile;
    tLinkNode          *pLk = &pLknode;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_IMPORT_RT_ADD;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    TMO_SLL_Init (&TsFeasibleroutes);

    BGP4_RT_AFI_INFO (pRtp) = BGP4_INET_AFI_L2VPN;
    BGP4_RT_SAFI_INFO (pRtp) = BGP4_INET_SAFI_VPLS;
    pRtProfile.pRtInfo = &pRtInfo;
    pRtProfile.pRtInfo->pExtCommunity = &pExtCommunity;
    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = pu1EcommVal;
    BGP4_INFO_ECOMM_COUNT (pRtProfile.pRtInfo) = L2VPN_EXT_COMM_COUNT;
    MEMCPY (pu1EcommVal, au1RT, EXT_COMM_VALUE_LEN);

    BGP4_LINK_PROFILE_TO_NODE (pRtp, pLk);
    TMO_SLL_Add (&TsFeasibleroutes, &pLk->TSNext);

    i4RetVal = Bgp4VplsApplyImportRTFilter (&(TsFeasibleroutes), NULL);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_87 (VOID)
{
    /* UT Bgp4VplsApplyImportRTFilter */
    tRouteProfile       pRtProfile;
    tExtCommLayer2Info  ExtCommInfo;
    tBgp4Info           pRtInfo;
    tExtCommunity       pExtCommunity;
    UINT1               pu1EcommVal[16];
    UINT1               au1RouteTarget[8];
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    tTMO_SLL            TsFeasibleroutes;
    INT4                i4RetVal;
    tLinkNode           pLknode;
    tRouteProfile      *pRtp = &pRtProfile;
    tLinkNode          *pLk = &pLknode;
    tBgp4PeerEntry      peerInfo;
    tBgp4PeerEntry     *pPeer = &peerInfo;

    pPeer->peerConfig.u1RflClient = NON_CLIENT;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    TMO_SLL_Init (&TsFeasibleroutes);

    BGP4_RT_AFI_INFO (pRtp) = BGP4_INET_AFI_L2VPN;
    BGP4_RT_SAFI_INFO (pRtp) = BGP4_INET_SAFI_VPLS;
    pRtProfile.pRtInfo = &pRtInfo;
    pRtProfile.pRtInfo->pExtCommunity = &pExtCommunity;
    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = pu1EcommVal;
    BGP4_INFO_ECOMM_COUNT (pRtProfile.pRtInfo) = L2VPN_EXT_COMM_COUNT;
    MEMCPY (pu1EcommVal, au1RT, EXT_COMM_VALUE_LEN);

    BGP4_LINK_PROFILE_TO_NODE (pRtp, pLk);
    TMO_SLL_Add (&TsFeasibleroutes, &pLk->TSNext);
    BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) = 1;

    i4RetVal = Bgp4VplsApplyImportRTFilter (&(TsFeasibleroutes), pPeer);
    BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) = 0;

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();
    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_88 (VOID)
{
    /* Bgp4VplsIsRTMatchInRRImportRTList */
    INT4                i4RetVal;
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    tBgp4VplsRrImportTargetInfo *pVplsRrImportTarget = NULL;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pVplsRrImportTarget = Bgp4VplsAddNewRTInRRImportList (au1RT);

    if (NULL != pVplsRrImportTarget)
    {
        pVplsRrImportTarget = Bgp4VplsIsRTMatchInRRImportRTList (au1RT);
        if (NULL != pVplsRrImportTarget)
        {
            i4RetVal = BGP4_SUCCESS;
        }
        else
        {
            i4RetVal = BGP4_FAILURE;
        }
    }
    else
    {
        i4RetVal = BGP4_FAILURE;
    }

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();
    return i4RetVal;

}

INT4
BgpUtFileL2vpnVpls6_89 (VOID)
{
    /* Bgp4VplsIsRTMatchInRRImportRTList */
    INT4                i4RetVal;
    UINT1               au1RT1[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT2[MAX_LEN_RT_VALUE] =
        { 0x01, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    tBgp4VplsRrImportTargetInfo *pVplsRrImportTarget = NULL;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    pVplsRrImportTarget = Bgp4VplsAddNewRTInRRImportList (au1RT1);

    if (NULL == pVplsRrImportTarget)
    {
        i4RetVal = BGP4_FAILURE;
        goto RETURN;
    }
    pVplsRrImportTarget = Bgp4VplsIsRTMatchInRRImportRTList (au1RT2);
    if (NULL != pVplsRrImportTarget)
    {
        i4RetVal = BGP4_FAILURE;
    }
    else
    {
        /*RainyDay */
        i4RetVal = BGP4_SUCCESS;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();
    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_90 (VOID)
{
    /*Case : Test for Bgp4VplsCreate */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    UINT1               PeerAddress[4];
    UINT1               pu1Str[16];
    UINT1               index;
    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    for (index = 0; index < MAX_BGP_VPLS_INSTANCE; index++)
    {
        EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
        EvtInfo.u2VeId = index + 4;
        EvtInfo.u2VeBaseOffset = 41;
        EvtInfo.u2VeBaseSize = 10;
        EvtInfo.u4LabelBase = index + 300050;
        EvtInfo.bControlWordFlag = 1;
        EvtInfo.u2Mtu = 50;
        EvtInfo.u4VplsIndex = index;

        MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
        MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

        i4RetVal =
            Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                                   BGP4_DFLT_VRFID);
        if (i4RetVal == BGP4_FAILURE)
        {
            goto RETURN;
        }
    }
    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = index + 5;
    EvtInfo.u2VeBaseOffset = 41;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = index + 400050;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = index + 1;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        /*Rainy Day */
        i4RetVal = BGP4_SUCCESS;
    }
    for (index = 0; index < MAX_BGP_VPLS_INSTANCE; index++)
    {
        EvtInfo.u4MsgType = BGP_L2VPN_VPLS_DELETE;
        EvtInfo.u2VeId = index + 4;
        EvtInfo.u2VeBaseOffset = 41;
        EvtInfo.u2VeBaseSize = 10;
        EvtInfo.u4LabelBase = index + 300050;
        EvtInfo.bControlWordFlag = 1;
        EvtInfo.u2Mtu = 50;
        EvtInfo.u4VplsIndex = index;

        MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
        MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

        i4RetVal =
            Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DELETE,
                                   BGP4_DFLT_VRFID);
        if (i4RetVal == BGP4_FAILURE)
        {
            goto RETURN;
        }
    }
  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();
    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_91 (VOID)
{
    /* Bgp4VplsAddNewRTInRRImportList */
    INT4                i4RetVal;
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    tBgp4VplsRrImportTargetInfo
        *pVplsRrImportTarget[MAX_BGP_VPLS_RR_IMPORT_TARGETS_SET];
    tBgp4VplsRrImportTargetInfo *pVplsRrTarget;
    UINT1               index;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    for (index = 0; index < MAX_BGP_VPLS_RR_IMPORT_TARGETS_SET; index++)
    {
        pVplsRrImportTarget[index] = Bgp4VplsAddNewRTInRRImportList (au1RT);
        if (NULL == pVplsRrImportTarget)
        {
            i4RetVal = BGP4_FAILURE;
            goto RETURN;
        }
    }
    pVplsRrTarget = Bgp4VplsAddNewRTInRRImportList (au1RT);
    if (NULL == pVplsRrTarget)
    {
        /*Rainy Day */
        i4RetVal = BGP4_SUCCESS;
    }

    for (index = 0; index < MAX_BGP_VPLS_RR_IMPORT_TARGETS_SET; index++)
    {

        i4RetVal =
            Bgp4MemReleaseRrImportTargetVpls (pVplsRrImportTarget[index]);
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();
    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_92 (VOID)
{
    /* Bgp4VplsIsRouteCanBeProcessed */
    tRouteProfile       pRtProfile;
    tRouteProfile      *pRt = &pRtProfile;
    INT4                i4RetVal;
    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) = 1;

    pRtProfile.pVplsSpecInfo = NULL;
    i4RetVal = Bgp4VplsIsRouteCanBeProcessed (&pRtProfile);

    BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) = 0;

    BGP4_RT_GET_FLAGS (pRt) |= BGP4_RT_FILTERED_INPUT;
    i4RetVal = Bgp4VplsIsRouteCanBeProcessed (&pRtProfile);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_93 (VOID)
{
    /* UT Bgp4VplsRrPrune */
    INT4                i4RetVal;
    tRouteProfile       pRtProfile;
    tRouteProfile      *pRt = &pRtProfile;
    tBgp4PeerEntry      peerEntry;
    tExtCommLayer2Info  ExtCommInfo;
    tBgp4Info           pRtInfo;
    tExtCommunity       pExtCommunity;
    UINT1               pu1EcommVal[16];
    UINT1               au1RouteTarget[8];

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    BGP4_RT_PEER_ENTRY (pRt) = &peerEntry;
    pRtProfile.pRtInfo = &pRtInfo;
    pRtProfile.pRtInfo->pExtCommunity = &pExtCommunity;

    BGP4_RT_PROTOCOL (pRt) = 2;
    i4RetVal = Bgp4VplsRrPrune (&pRtProfile);
    BGP4_RT_PROTOCOL (pRt) = BGP_ID;
    BGP4_PEER_RFL_CLIENT (BGP4_RT_PEER_ENTRY (pRt)) = NON_CLIENT;
    i4RetVal = Bgp4VplsRrPrune (&pRtProfile);

  RETURN:

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();
    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_94 (VOID)
{
    /* UT Bgp4VplsRrPrune */
    INT4                i4RetVal;
    tRouteProfile       pRtProfile;
    tRouteProfile      *pRt = &pRtProfile;
    tBgp4PeerEntry      peerEntry;
    tExtCommLayer2Info  ExtCommInfo;
    tBgp4Info           pRtInfo;
    tExtCommunity       pExtCommunity;
    UINT1               pu1EcommVal[16];
    UINT1               au1RouteTarget[8];
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x07, 0x09, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    BGP4_RT_PEER_ENTRY (pRt) = &peerEntry;
    BGP4_RT_PROTOCOL (pRt) = BGP_ID;
    BGP4_PEER_RFL_CLIENT (BGP4_RT_PEER_ENTRY (pRt)) = CLIENT;

    pRtProfile.pRtInfo = &pRtInfo;
    pRtProfile.pRtInfo->pExtCommunity = &pExtCommunity;
    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = pu1EcommVal;
    BGP4_INFO_ECOMM_COUNT (pRtProfile.pRtInfo) = L2VPN_EXT_COMM_COUNT;
    MEMCPY (pu1EcommVal, au1RT, EXT_COMM_VALUE_LEN);

    i4RetVal = Bgp4VplsRrPrune (&pRtProfile);

  RETURN:

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();
    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_95 (VOID)
{
    /* UT Bgp4VplsRrPrune */
    INT4                i4RetVal;
    tRouteProfile       pRtProfile;
    tRouteProfile      *pRt = &pRtProfile;
    tBgp4PeerEntry      peerEntry;
    tExtCommLayer2Info  ExtCommInfo;
    tBgp4Info           pRtInfo;
    tExtCommunity       pExtCommunity;
    UINT1               pu1EcommVal[16];
    UINT1               au1RouteTarget[8];
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    BGP4_RT_PEER_ENTRY (pRt) = &peerEntry;
    BGP4_RT_PROTOCOL (pRt) = BGP_ID;
    BGP4_PEER_RFL_CLIENT (BGP4_RT_PEER_ENTRY (pRt)) = CLIENT;

    pRtProfile.pRtInfo = &pRtInfo;
    pRtProfile.pRtInfo->pExtCommunity = &pExtCommunity;
    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = pu1EcommVal;
    BGP4_INFO_ECOMM_COUNT (pRtProfile.pRtInfo) = L2VPN_EXT_COMM_COUNT;
    MEMCPY (pu1EcommVal, au1RT, EXT_COMM_VALUE_LEN);

    i4RetVal = Bgp4VplsRrPrune (&pRtProfile);

  RETURN:

    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();
    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_96 (VOID)
{
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x01, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x70 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x50 };

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 0;
    EvtInfo.u2VeBaseOffset = 00;
    EvtInfo.u2VeBaseSize = 00;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 0;
    EvtInfo.u2Mtu = 0;
    EvtInfo.u4VplsIndex = 200;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DELETE,
                               BGP4_DFLT_VRFID);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
notifyFunction (UINT1 u1ASNType, UINT4 u4ASNValue)
{
    return 0;
}

INT4
BgpUtFileL2vpnVpls6_97 (VOID)
{
    /* Bgp4ASNRegister */
    tBgp4RegisterASNInfo pBgp4RegisterASNInfo;
    pBgp4RegisterASNInfo.pBgp4NotifyASN = notifyFunction;
    /*Base conf */
    BgpUTBasicConf ();
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("no ip bgp four-byte-asn");
    CliExecuteAppCmd ("en");
    MGMT_LOCK ();
    CliGiveAppContext ();
    BgpSetContext (0);
    Bgp4ASNRegister (&pBgp4RegisterASNInfo);
    /*Removing Base Conf */
    BgpReleaseContext ();
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("ip bgp four-byte-asn");
    CliExecuteAppCmd ("en");
    MGMT_LOCK ();
    CliGiveAppContext ();
    BgpUTRemoveBasicConf ();
    return BGP4_SUCCESS;

}

INT4
BgpUtFileL2vpnVpls6_98 (VOID)
{
    /* UT Bgp4VplsApplyImportRTFilter */
    tRouteProfile       pRtProfile;
    tExtCommLayer2Info  ExtCommInfo;
    tBgp4Info           pRtInfo;
    tExtCommunity       pExtCommunity;
    UINT1               pu1EcommVal[16];
    UINT1               au1RouteTarget[8];
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    tTMO_SLL            TsFeasibleroutes;
    INT4                i4RetVal;
    tLinkNode           pLknode;
    tRouteProfile      *pRtp = &pRtProfile;
    tLinkNode          *pLk = &pLknode;

    tBgp4PeerEntry      peerInfo;
    tBgp4PeerEntry     *pPeer = &peerInfo;

    pPeer->peerConfig.u1RflClient = CLIENT;
    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    TMO_SLL_Init (&TsFeasibleroutes);

    BGP4_RT_AFI_INFO (pRtp) = BGP4_INET_AFI_L2VPN;
    BGP4_RT_SAFI_INFO (pRtp) = BGP4_INET_SAFI_VPLS;
    pRtProfile.pRtInfo = &pRtInfo;
    pRtProfile.pRtInfo->pExtCommunity = &pExtCommunity;
    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = pu1EcommVal;
    BGP4_INFO_ECOMM_COUNT (pRtProfile.pRtInfo) = L2VPN_EXT_COMM_COUNT;
    MEMCPY (pu1EcommVal, au1RT, EXT_COMM_VALUE_LEN);

    BGP4_LINK_PROFILE_TO_NODE (pRtp, pLk);
    TMO_SLL_Add (&TsFeasibleroutes, &pLk->TSNext);
    BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) = 1;

    i4RetVal = Bgp4VplsApplyImportRTFilter (&(TsFeasibleroutes), &peerInfo);

    BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) = 0;

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();
    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_99 (VOID)
{
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x01, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x70 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x50 };
    tRouteProfile       pRtProfile;
    tRouteProfile      *pRt = &pRtProfile;
    tVplsNlriInfo       VplsNlri;
    tBgp4Info           pRtInfo;
    tBgp4Info          *pBgpInfo = &pRtInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    VOID               *pRibNode = NULL;

    MEMSET (pRt, 0, sizeof (tRouteProfile));
    MEMSET (pBgpInfo, 0, sizeof (tBgp4Info));
    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 0;
    EvtInfo.u2VeBaseOffset = 10;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 0;
    EvtInfo.u2Mtu = 0;
    EvtInfo.u4VplsIndex = 201;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
    VplsNlri.u2Length = BGP4_VPLS_NLRI_PREFIX_LEN;
    MEMCPY (VplsNlri.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    VplsNlri.u2VeId = 0;
    VplsNlri.u2VeBaseOffset = 10;
    VplsNlri.u2VeBaseSize = 10;
    VplsNlri.u4labelBase = 300000;

    pRt->pRtInfo = &pRtInfo;
    pBgpInfo->pRcvdPA = NULL;

    i4RetVal = Bgp4VplsConstructRouteProfile (0, pRt, NULL, &VplsNlri, 201);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal = Bgp4RibhAddRtEntry (pRt, 0);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal = Bgp4VplsGetVplsInfoFromIndex (201, &pVplsInfo);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal = Bgp4ProcessVplsDown (pVplsInfo, 0);
    if (i4RetVal == BGP4_FALSE)
    {
        /*Rainy-Day */
        i4RetVal = BGP4_SUCCESS;
    }

    Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DELETE, BGP4_DFLT_VRFID);
    Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_DELETE,
                           BGP4_DFLT_VRFID);

    pRibNode = BGP4_VPLS_RIB_TREE (0);

    Bgp4RibhDelRtEntry (pRt, pRibNode, 0, BGP4_TRUE);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_100 (VOID)
{
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x01, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x70 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x50 };
    tRouteProfile       pRtProfile;
    tRouteProfile      *pRt = &pRtProfile;
    tVplsNlriInfo       VplsNlri;
    tBgp4Info           pRtInfo;
    tBgp4Info          *pBgpInfo = &pRtInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgp4PeerEntry      peer;
    tBgp4PeerEntry     *pPeerInfo = &peer;
    tAfiSafiSpecInfo    pAfiSafiInstance;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    MEMSET (pRt, 0, sizeof (tRouteProfile));
    MEMSET (pBgpInfo, 0, sizeof (tBgp4Info));
    pPeerInfo->u4NegCapMask = 0;
    BGP4_PEER_VPLS_AFISAFI_INSTANCE (pPeerInfo) = &pAfiSafiInstance;
    /* pRt->pPEPeer = pPeerInfo; */

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 1;
    EvtInfo.u2VeBaseOffset = 10;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 0;
    EvtInfo.u2Mtu = 0;
    EvtInfo.u4VplsIndex = 202;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
    VplsNlri.u2Length = BGP4_VPLS_NLRI_PREFIX_LEN;
    MEMCPY (VplsNlri.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    VplsNlri.u2VeId = 1;
    VplsNlri.u2VeBaseOffset = 10;
    VplsNlri.u2VeBaseSize = 10;
    VplsNlri.u4labelBase = 300000;

    pRt->pRtInfo = &pRtInfo;
    pBgpInfo->pRcvdPA = NULL;

    /* i4RetVal = Bgp4MpeInitAfiSafiInstance(pPeerInfo,BGP4_L2VPN_VPLS_INDEX);
       if (i4RetVal == BGP4_FAILURE)
       {
       goto RETURN;
       }
     */
    i4RetVal = Bgp4VplsConstructRouteProfile (0, pRt, NULL, &VplsNlri, 202);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal = Bgp4RibhAddRtEntry (pRt, 0);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    BGP4_RT_PROTOCOL (pRt) = BGP_ID;

    i4RetVal = Bgp4VplsGetVplsInfoFromIndex (202, &pVplsInfo);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal = Bgp4ProcessVplsDown (pVplsInfo, 0);
    if (i4RetVal == BGP4_TRUE)
    {
        i4RetVal = BGP4_SUCCESS;
    }

    Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DELETE, BGP4_DFLT_VRFID);
    Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_DELETE,
                           BGP4_DFLT_VRFID);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_101 (VOID)
{
    /*Case : Test for BGP_L2VPN_VPLS_CREATE */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 51;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300060;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 203;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DELETE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_DELETE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileL2vpnVpls6_102 (VOID)
{
    return -1;
}

INT4
BgpUtFileL2vpnVpls6_103 (VOID)
{
    return -1;
}

INT4
BgpUtFileL2vpnVpls6_104 (VOID)
{
#if 1
    /*Case : Test for Bgp4VplsCreateWithdMsgforL2VPN */
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tRouteProfile       pRtProfile;
    UINT1               PeerAddress[4];
    UINT1               pu1Str[16];
    tExtCommLayer2Info  ExtCommInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_WITHDRAWN_MSG;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 41;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300050;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_ADVERTISE_MSG,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    pRtProfile.pPEPeer = (tBgp4PeerEntry *) malloc (sizeof (tBgp4PeerEntry));
    pRtProfile.pRtInfo = (tBgp4Info *) malloc (sizeof (tBgp4Info));
    pRtProfile.pRtInfo->pExtCommunity =
        (tExtCommunity *) malloc (sizeof (tExtCommunity));
    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal =
        (UINT1 *) malloc (sizeof (UINT1));

    /*IP is 20.0.0.2 */
    PeerAddress[0] = 2;
    PeerAddress[1] = 0;
    PeerAddress[2] = 0;
    PeerAddress[3] = 20;
    MEMCPY (pRtProfile.pPEPeer->peerConfig.RemoteAddrInfo.au1Address,
            PeerAddress, sizeof (PeerAddress));

    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = NULL;
    pRtProfile.pRtInfo->pExtCommunity->u2EcommCnt = L2VPN_EXT_COMM_COUNT;
    pRtProfile.pRtInfo->pExtCommunity->u1CostFlag = BGP4_FALSE;

     /*RT*/
        /*ATTRIBUTE_NODE_CREATE (pu1Str); */
        MEMCPY (pu1Str, au1RT, EXT_COMM_VALUE_LEN);

    /*MTU and Control word */
    ExtCommInfo.extCommType = OSIX_HTONS (ECOMM_VALUE_LAYER2_INFO);
    ExtCommInfo.encapsType = ECOMM_LAYER2_INFO_ENCAPS_TYPE;
    ExtCommInfo.bControlWordFlag = 1;
    ExtCommInfo.u2Mtu = 50;
    ExtCommInfo.u2Reserved = 0;
    MEMCPY (pu1Str + EXT_COMM_VALUE_LEN, &ExtCommInfo, EXT_COMM_VALUE_LEN);

    pRtProfile.pRtInfo->pExtCommunity->pu1EcommVal = pu1Str;
    pRtProfile.pRtInfo->u4AttrFlag |= (BGP4_ATTR_ECOMM_MASK);
    pRtProfile.pRtInfo->pExtCommunity->u1CostFlag = BGP4_FALSE;

    pRtProfile.VplsNlriInfo.u2VeId = 4;
    pRtProfile.VplsNlriInfo.u2VeBaseOffset = 41;
    pRtProfile.VplsNlriInfo.u2VeBaseSize = 10;
    pRtProfile.VplsNlriInfo.u4labelBase = 300050;

    MEMCPY (pRtProfile.VplsNlriInfo.au1RouteDistinguisher, au1RD,
            MAX_LEN_RD_VALUE);

    pRtProfile.pVplsSpecInfo =
        (tVplsSpecInfo *) malloc (sizeof (tVplsSpecInfo));
    STRNCPY (pRtProfile.pVplsSpecInfo->au1VplsName, au1VplsName,
             STRNLEN (au1VplsName, BGP4_VPLS_MAX_VPLS_NAME_SIZE));

    i4RetVal = Bgp4VplsCreateAdvtMsgforL2VPN (&pRtProfile);

    /*  L2VPN_ADMIN_STATUS = ADMIN_DOWN; */
    L2VpnDisableL2VpnService (2);

    i4RetVal = Bgp4VplsCreateWithdMsgforL2VPN (&pRtProfile);
    /*Rainy day */
    if (BGP4_FAILURE == i4RetVal)
    {
        i4RetVal = BGP4_SUCCESS;
    }
    L2VpnEnableL2VpnService ();

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
#endif
}

INT4
BgpUtFileL2vpnVpls6_105 (VOID)
{
#if 1
    tBgp4QMsg          *pQMsg = NULL;
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };

    UINT2               index = 0;
    for (index = 0; index < BGP4_Q_DEPTH; index++)
    {
        pQMsg = malloc (sizeof (tBgp4QMsg));
        if (pQMsg == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC | BGP4_VPLS_CRI_TRC,
                      BGP4_MOD_NAME,
                      "\ttBgpL2vpnEventHandler() : Unable to"
                      "trigger the L2VPN message to VPLS handler\n");
            return BGP4_FAILURE;
        }

        BGP4_INPUTQ_MSGTYPE (pQMsg) = 100;    /*invalid msg */
        if (OsixQueSend (BGP4_Q_ID, (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN)
            != OSIX_SUCCESS)
        {
            printf ("\n OsixQueSend fails for index = %d", index);
        }
    }

    /*Base conf */
    BgpUTBasicConf ();

    EvtInfo.u4MsgType = BGP_L2VPN_ADVERTISE_MSG;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 1;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal = Bgp4L2vpnEventHandler (&EvtInfo);

    if (i4RetVal == BGP4_FAILURE)
    {
        i4RetVal = BGP4_SUCCESS;
    }
    /* OsixEvtSend (BGP4_TASK_ID, BGP4_INPUT_Q_EVENT); */
    /*Removing Base Conf */
    BgpUTRemoveBasicConf ();
    return i4RetVal;
#endif
}


/*******************************************************
 *               VPLS GR-HA UT Cases                   * 
 *                                                     *
 *******************************************************/
VOID
BgpUTBasicGRHAConf (VOID)
{

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("sh spanning-tree");
    CliExecuteAppCmd ("set gvrp dis");
    CliExecuteAppCmd ("set gmrp dis");
    CliExecuteAppCmd ("sh garp");
    CliExecuteAppCmd ("en");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int loopback 0");
    CliExecuteAppCmd ("ip address 2.2.2.2 255.255.255.255");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("en");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("i g 0/1");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("no sw");
    CliExecuteAppCmd ("ip address 10.0.0.2 255.0.0.0");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("en");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("i g 0/2");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("no sw");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("en");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("router ospf");
    CliExecuteAppCmd ("router-id 2.2.2.2");
    CliExecuteAppCmd ("network 10.0.0.2 area 1.1.1.1");
    CliExecuteAppCmd ("network 2.2.2.2 area 1.1.1.1");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("ip ospf network point-to-point");
    CliExecuteAppCmd ("en");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("router ospf");
    CliExecuteAppCmd ("router-id 2.2.2.2");
    CliExecuteAppCmd ("ASBR router");
    CliExecuteAppCmd ("en");
    CliExecuteAppCmd ("sleep 1");

    MGMT_LOCK ();
    CliGiveAppContext ();

}

VOID
BgpUTRemoveGRHABasicConf (VOID)
{
    CliTakeAppContext ();
    MGMT_UNLOCK ();

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("router ospf");
    CliExecuteAppCmd ("router-id 2.2.2.2");
    CliExecuteAppCmd ("no network 10.0.0.2 area 1.1.1.1");
    CliExecuteAppCmd ("no network 2.2.2.2 area 1.1.1.1");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no router ospf");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("i g 0/2");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("no sw");
    CliExecuteAppCmd ("no ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("no mpls ip");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("no i g 0/2");
    CliExecuteAppCmd ("ex");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("i g 0/1");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("no sw");
    CliExecuteAppCmd ("no ip address 10.0.0.2 255.0.0.0");
    CliExecuteAppCmd ("no mpls ip");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("no i g 0/1");
    CliExecuteAppCmd ("ex");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int loopback 0");
    CliExecuteAppCmd ("no ip address 2.2.2.2 255.255.255.255");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("int loopback 0");
    CliExecuteAppCmd ("en");

    MGMT_LOCK ();
    CliGiveAppContext ();

}
INT4
BgpUtFileVplsGRandHA7_1 (VOID)
{
    INT4 i4RetVal;
    /*Base conf */
    BgpUTBasicGRHAConf();

    i4RetVal = Bgp4VplsGRInProgress (BGP4_DFLT_VRFID);

    /*Removing Base Conf */
    BgpUTRemoveGRHABasicConf();

    return i4RetVal;
}

INT4
BgpUtFileVplsGRandHA7_2 (VOID)
{
    INT4 i4RetVal;
    /*Base conf */
    BgpUTBasicGRHAConf();

    i4RetVal = Bgp4VplsGREORFromL2VPN (BGP4_DFLT_VRFID);

    /*Removing Base Conf */
    BgpUTRemoveGRHABasicConf();

    return i4RetVal;
}

INT4
BgpUtFileVplsGRandHA7_3 (VOID)
{
    INT4 i4RetVal;
    /*Base conf */
    BgpUTBasicGRHAConf();

    i4RetVal = Bgp4VplsNotifyGRToL2VPN (BGP4_DFLT_VRFID);

    /*Removing Base Conf */
    BgpUTRemoveGRHABasicConf();

    return i4RetVal;
}

INT4
BgpUtFileVplsGRandHA7_4 (VOID)
{
    INT4 i4RetVal;
    /*Base conf */
    BgpUTBasicGRHAConf();

    i4RetVal = Bgp4VplsNotifyRestartToL2VPN (BGP4_DFLT_VRFID);

    /*Removing Base Conf */
    BgpUTRemoveGRHABasicConf();

    return i4RetVal;
}

INT4
BgpUtFileVplsGRandHA7_5 (VOID)
{
    tRouteProfile       RtProfile;
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
    { 0x01, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x70 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
    { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x50 };

    /*Base conf */
    BgpUTBasicGRHAConf();

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 0;
    EvtInfo.u2VeBaseOffset = 10;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 0;
    EvtInfo.u2Mtu = 0;
    EvtInfo.u4VplsIndex = 1;


    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                BGP4_DFLT_VRFID);

    RtProfile.pVplsSpecInfo = (tVplsSpecInfo *)malloc (sizeof (tVplsSpecInfo));

    Bgp4LinkVplsSpecToRouteInStandby(&RtProfile, 1, BGP4_DFLT_VRFID);
    i4RetVal = Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DELETE,
                BGP4_DFLT_VRFID);

    /*Removing Base Conf */
    BgpUTRemoveGRHABasicConf();

    return 0;
}

INT4
BgpUtFileVplsGRandHA7_6 (VOID)
{
    tRouteProfile       RtProfile;
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
    { 0x01, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x70 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
    { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x50 };

    /*Base conf */
    BgpUTBasicGRHAConf();

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 0;
    EvtInfo.u2VeBaseOffset = 10;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 0;
    EvtInfo.u2Mtu = 0;
    EvtInfo.u4VplsIndex = 1;


    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                BGP4_DFLT_VRFID);

    RtProfile.pVplsSpecInfo = (tVplsSpecInfo *)malloc (sizeof (tVplsSpecInfo));

    Bgp4LinkVplsSpecToRouteInStandby(&RtProfile, 10, BGP4_DFLT_VRFID);
    i4RetVal = Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DELETE,
                BGP4_DFLT_VRFID);

    /*Removing Base Conf */
    BgpUTRemoveGRHABasicConf();

    return 0;
}

INT4
BgpUtFileVplsGRandHA7_7 (VOID)
{
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    
    if (BGP4_FAILURE ==
        Bgp4VplsGetVplsInfoFromIndex (10, &pVplsInfo))
    {
        goto RETURN;
    }

    VplsSyncInfo.u4VplsMsgType = BGP_L2VPN_VPLS_CREATE;

    i4RetVal = Bgp4RedSyncVplsInfo(pVplsInfo, &VplsSyncInfo);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    if (i4RetVal == BGP4_FAILURE)
    {
        return 0;
    }
    else
    {
        return -1;
    } 
}

INT4
BgpUtFileVplsGRandHA7_8 (VOID)
{
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_DELETE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);
    
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    if (BGP4_FAILURE ==
        Bgp4VplsGetVplsInfoFromIndex (10, &pVplsInfo))
    {
        goto RETURN;
    }
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    VplsSyncInfo.u4VplsMsgType = BGP_L2VPN_VPLS_CREATE;

    i4RetVal = Bgp4RedSyncVplsInfo(pVplsInfo, &VplsSyncInfo);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileVplsGRandHA7_9 (VOID)
{
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 15;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_BOTH_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_DELETE,
                               BGP4_DFLT_VRFID);
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_DELETE,
                               BGP4_DFLT_VRFID);
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_BOTH_RT_DELETE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DOWN, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DELETE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileVplsGRandHA7_10 (VOID)
{

    INT4                i4RetVal = 0;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
    { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
    { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    UINT2               u2Length = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, BGP_L2VPN_VPLS_CREATE);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, 20);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1VplsName,
            BGP4_VPLS_MAX_VPLS_NAME_SIZE);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1RD,
            BGP4_VPLS_ROUTE_DISTING_SIZE);

    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, BGP4_RED_UPD_LEN_OFFSET, u4OffSet);

    u2Length = u4OffSet;
    u4OffSet = 3;

    i4RetVal = Bgp4RedProcessVplsInfoSyncMsg (pMsg->pBuf, &u4OffSet, u2Length);

RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileVplsGRandHA7_11 (VOID)
{                       

    INT4                i4RetVal;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
    { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
    { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, BGP_L2VPN_VPLS_UP);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, 20);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1VplsName,
            BGP4_VPLS_MAX_VPLS_NAME_SIZE);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1RD,
            BGP4_VPLS_ROUTE_DISTING_SIZE);

    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, BGP4_RED_UPD_LEN_OFFSET, u4OffSet);

    u2Length = u4OffSet;
    u4OffSet = 3;

    i4RetVal = Bgp4RedProcessVplsInfoSyncMsg (pMsg->pBuf, &u4OffSet, u2Length);

RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}
                        
INT4                    
BgpUtFileVplsGRandHA7_12 (VOID)
{                       

    INT4                i4RetVal;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
    { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
    { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    UINT2               u2Length = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, BGP_L2VPN_VPLS_DOWN);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, 20);

    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, BGP4_RED_UPD_LEN_OFFSET, u4OffSet);

    u2Length = u4OffSet;
    u4OffSet = 3;

    i4RetVal = Bgp4RedProcessVplsInfoSyncMsg (pMsg->pBuf, &u4OffSet, u2Length);

RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}                       
                        
INT4                    
BgpUtFileVplsGRandHA7_13 (VOID)
{                       

    INT4                i4RetVal;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
    { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
    { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    UINT2               u2Length = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, BGP_L2VPN_VPLS_DELETE);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, 20);

    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, BGP4_RED_UPD_LEN_OFFSET, u4OffSet);

    u2Length = u4OffSet;
    u4OffSet = 3;

    i4RetVal = Bgp4RedProcessVplsInfoSyncMsg (pMsg->pBuf, &u4OffSet, u2Length);

RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}                       
                        
INT4                    
BgpUtFileVplsGRandHA7_14 (VOID)
{                       
    INT4                i4RetVal;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
    { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
    { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    UINT2               u2Length = 0;
    tBgp4L2VpnEvtInfo   EvtInfo;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);


    EvtInfo.u4MsgType = BGP_L2VPN_ADVERTISE_MSG;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 31;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300040;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 20;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, BGP_L2VPN_IMPORT_RT_ADD);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, 20);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1RT, MAX_LEN_RT_VALUE);

    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, BGP4_RED_UPD_LEN_OFFSET, u4OffSet);

    u2Length = u4OffSet;
    u4OffSet = 3;

    i4RetVal = Bgp4RedProcessVplsInfoSyncMsg (pMsg->pBuf, &u4OffSet, u2Length);


RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}                       
                        
INT4                    
BgpUtFileVplsGRandHA7_15 (VOID)
{                       
    INT4                i4RetVal;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
    { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
    { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    UINT2               u2Length = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, BGP_L2VPN_IMPORT_RT_DELETE);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, 20);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1RT, MAX_LEN_RT_VALUE);

    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, BGP4_RED_UPD_LEN_OFFSET, u4OffSet);

    u2Length = u4OffSet;
    u4OffSet = 3;

    i4RetVal = Bgp4RedProcessVplsInfoSyncMsg (pMsg->pBuf, &u4OffSet, u2Length);

RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}
                        
INT4                    
BgpUtFileVplsGRandHA7_16 (VOID)
{                       
                        
    INT4                i4RetVal;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
    { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
    { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    UINT2               u2Length = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, BGP_L2VPN_EXPORT_RT_ADD);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, 20);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1RT, MAX_LEN_RT_VALUE);

    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, BGP4_RED_UPD_LEN_OFFSET, u4OffSet);

    u2Length = u4OffSet;
    u4OffSet = 3;

    i4RetVal = Bgp4RedProcessVplsInfoSyncMsg (pMsg->pBuf, &u4OffSet, u2Length);

RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}                       
                        
INT4                    
BgpUtFileVplsGRandHA7_17 (VOID)
{                       
                        
    INT4                i4RetVal;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
    { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
    { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    UINT2               u2Length = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, BGP_L2VPN_EXPORT_RT_DELETE);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, 20);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1RT, MAX_LEN_RT_VALUE);

    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, BGP4_RED_UPD_LEN_OFFSET, u4OffSet);

    u2Length = u4OffSet;
    u4OffSet = 3;

    i4RetVal = Bgp4RedProcessVplsInfoSyncMsg (pMsg->pBuf, &u4OffSet, u2Length);

RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}                       
                        
INT4                    
BgpUtFileVplsGRandHA7_18 (VOID)
{                       

    INT4                i4RetVal;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
    { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
    { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, BGP_L2VPN_BOTH_RT_ADD);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, 20);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1RT, MAX_LEN_RT_VALUE);

    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, BGP4_RED_UPD_LEN_OFFSET, u4OffSet);

    u2Length = u4OffSet;
    u4OffSet = 3;

    i4RetVal = Bgp4RedProcessVplsInfoSyncMsg (pMsg->pBuf, &u4OffSet, u2Length);

RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}                       
                        
INT4                    
BgpUtFileVplsGRandHA7_19 (VOID)
{

#if 1
    INT4                i4RetVal;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
    { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
    { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    UINT2               u2Length = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, BGP_L2VPN_BOTH_RT_DELETE);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, 20);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1RT, MAX_LEN_RT_VALUE);

    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, BGP4_RED_UPD_LEN_OFFSET, u4OffSet);

    u2Length = u4OffSet;
    u4OffSet = 3;

    i4RetVal = Bgp4RedProcessVplsInfoSyncMsg (pMsg->pBuf, &u4OffSet, u2Length);

RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
#else
    return -1;
#endif

}

INT4
BgpUtFileVplsGRandHA7_20 (VOID)
{
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 30;
    MEMCPY(EvtInfo.au1VplsName, au1VplsName, BGP4_VPLS_MAX_VPLS_NAME_SIZE);

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_BOTH_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;
    Bgp4RedSyncVplsInfoBulkMsg (pMsg, &u4OffSet);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileVplsGRandHA7_21 (VOID)
{
    INT4                i4RetVal;
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    int                 i4Count = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;
    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    i4Count = BGP4_VPLS_SPEC_CNT;
    BGP4_VPLS_SPEC_CNT = 0;
    i4RetVal = Bgp4RedSyncVplsInfoBulkMsg (pMsg, &u4OffSet);

    BGP4_VPLS_SPEC_CNT = i4Count;

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileVplsGRandHA7_22 (VOID)
{
#if 1
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 35;
    MEMCPY(EvtInfo.au1VplsName, au1VplsName, BGP4_VPLS_MAX_VPLS_NAME_SIZE);

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_BOTH_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    Bgp4VplsMarkVplsEntryAsStale (0);

    
    /*For cleaning stale routes*/
    gBgpNode.u1PrevNodeStatus = RM_STANDBY;
    gBgpCxtNode[0]->u1RestartMode = BGP4_RESTARTING_MODE;

    Bgp4VplsAuditVplsSpecEntry (0);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
#else
    return -1;
#endif
}

INT4
BgpUtFileVplsGRandHA7_23 (VOID)
{
#if 1
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 7;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 45;
    MEMCPY(EvtInfo.au1VplsName, au1VplsName, BGP4_VPLS_MAX_VPLS_NAME_SIZE);

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_BOTH_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    Bgp4VplsMarkVplsEntryAsStale (0);

    gBgpNode.u1PrevNodeStatus = RM_STANDBY;
    gBgpCxtNode[0]->u1RestartMode = BGP4_RESTARTING_MODE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    Bgp4VplsAuditVplsSpecEntry (0);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
#else
    return -1;
#endif
}

INT4
BgpUtFileVplsGRandHA7_24 (VOID)
{
#if 1
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;
    UINT1               u1MsgType =0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 30;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_BOTH_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;
    Bgp4RedSyncVplsInfoBulkMsg (pMsg, &u4OffSet);

    u4OffSet = 0;
    BGP4_RM_GET_1_BYTE (pMsg->pBuf, &u4OffSet, u1MsgType);
    BGP4_RM_GET_2_BYTE (pMsg->pBuf, &u4OffSet, u2Length);

    i4RetVal = Bgp4RedProcessVplsInfoBulkMsg (pMsg->pBuf, &u4OffSet, u2Length);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
#else
    return -1;
#endif
    
}

INT4
BgpUtFileVplsGRandHA7_25 (VOID)
{
#if 1
    INT4 i4RetVal, i, j;
    UINT1   *pu1QMsg[2000];
    /*Base conf */
    BgpUTBasicGRHAConf();

    for (i = 0; i < 20000; i++)
    {
        pu1QMsg[i] = (UINT1 *) MemAllocMemBlk (L2VPN_Q_POOL_ID);
        if (pu1QMsg[i] == NULL)
        {
            break;
        }
    }

    i4RetVal = Bgp4VplsGRInProgress (BGP4_DFLT_VRFID);


    for (j = 0; j < i; j++)
    {
        MemReleaseMemBlock (L2VPN_Q_POOL_ID,
                            (UINT1 *) (pu1QMsg[j]));
    }
    /*Removing Base Conf */
    BgpUTRemoveGRHABasicConf();

    if (i4RetVal == BGP4_FAILURE)
    {
        return BGP4_SUCCESS;
    }
    else
    {
        return BGP4_FAILURE;
    }
#else
    return -1;
#endif 
}

INT4
BgpUtFileVplsGRandHA7_26 (VOID)
{
#if 1
    INT4 i4RetVal, i, j;
    UINT1   *pu1QMsg[2000];
    /*Base conf */
    BgpUTBasicGRHAConf();

    for (i = 0; i < 20000; i++)
    {
        pu1QMsg[i] = (UINT1 *) MemAllocMemBlk (L2VPN_Q_POOL_ID);
        if (pu1QMsg[i] == NULL)
        {
            break;
        }
    }

    i4RetVal = Bgp4VplsNotifyGRToL2VPN(BGP4_DFLT_VRFID);


    for (j = 0; j < i; j++)
    {
        MemReleaseMemBlock (L2VPN_Q_POOL_ID,
                            (UINT1 *) (pu1QMsg[j]));
    }
    /*Removing Base Conf */
    BgpUTRemoveGRHABasicConf();

    if (i4RetVal == BGP4_FAILURE)
    {
        return BGP4_SUCCESS;
    }
    else
    {
        return BGP4_FAILURE;
    }
#else
    return -1;
#endif
}

INT4
BgpUtFileVplsGRandHA7_27 (VOID)
{
#if 1
    INT4 i4RetVal, i, j;
    UINT1   *pu1QMsg[2000];
    /*Base conf */
    BgpUTBasicGRHAConf();

    for (i = 0; i < 20000; i++)
    {
        pu1QMsg[i] = (UINT1 *) MemAllocMemBlk (L2VPN_Q_POOL_ID);
        if (pu1QMsg[i] == NULL)
        {
            break;
        }
    }

    i4RetVal = Bgp4VplsNotifyRestartToL2VPN (BGP4_DFLT_VRFID);


    for (j = 0; j < i; j++)
    {
        MemReleaseMemBlock (L2VPN_Q_POOL_ID,
                            (UINT1 *) (pu1QMsg[j]));
    }
    /*Removing Base Conf */
    BgpUTRemoveGRHABasicConf();

    if (i4RetVal == BGP4_FAILURE)
    {
        return BGP4_SUCCESS;
    }
    else
    {
        return BGP4_FAILURE;
    }
#else
    return -1;
#endif

}

INT4
BgpUtFileVplsGRandHA7_28 (VOID)
{
#if 1
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;
    tVplsSpecInfo      *pVplsSpecInfo[20000];

    int i,j;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300100;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 44;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_BOTH_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;
    Bgp4RedSyncVplsInfoBulkMsg (pMsg, &u4OffSet);

    u2Length = u4OffSet;
    u4OffSet = 3;

    for (i = 0; i < 20000; i++)
    {
        pVplsSpecInfo[i] =
            Bgp4MemAllocateVplsSpecEntry (sizeof (tVplsSpecInfo));
        if (pVplsSpecInfo[i] == NULL)
        {
            break;
        }
    }
    i4RetVal = Bgp4RedProcessVplsInfoBulkMsg (pMsg->pBuf, &u4OffSet, u2Length);

  RETURN:
    for (j = 0; j < i; j++)
    {
        MemReleaseMemBlock (BGP4_VPLS_SPEC_MEMPOOL_ID,
                            (UINT1 *) (pVplsSpecInfo[j]));
    }
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    if (i4RetVal == BGP4_FAILURE)
    {
        return BGP4_SUCCESS;
    }
    else
    {
        return BGP4_FAILURE;
    }
#else
    return -1;
#endif
}

INT4
BgpUtFileVplsGRandHA7_29 (VOID)
{

#if 1
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 30;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_BOTH_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    gBgpNode.u1StandbyNodes = 0;
    Bgp4RedSyncVplsInfoBulkMsg (pMsg, &u4OffSet);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    if (i4RetVal == BGP4_FAILURE)
    {
        return BGP4_SUCCESS;
    }
    else
    {
        return BGP4_FAILURE;
    }
#else
    return -1;
#endif
}

INT4
BgpUtFileVplsGRandHA7_30 (VOID)
{
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_WITHDRAWN_MSG;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 41;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300050;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 10;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /*RT can be added already, so no need to return BGP4_FAILURE Here */

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_ADVERTISE_MSG,
                               BGP4_DFLT_VRFID);

    Bgp4GRMarkAllRoutesAsStale (BGP4_DFLT_VRFID);
    Bgp4VplsDeleteStaleVplsLocalRoutes (BGP4_DFLT_VRFID);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileVplsGRandHA7_31 (VOID)
{
#if 1
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 14;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 29;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_BOTH_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    Bgp4VplsMarkVplsEntryAsStale (0);

    gBgpNode.u1PrevNodeStatus = RM_STANDBY;
    gBgpCxtNode[0]->u1RestartMode = BGP4_RESTARTING_MODE;

    Bgp4VplsAuditVplsSpecEntry (0);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
#else
    return -1;
#endif

}
INT4
BgpUtFileVplsGRandHA7_32 (VOID)
{
#if 1
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 45;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

#if 1
    Bgp4VplsMarkVplsEntryAsStale (0);
#endif

    gBgpNode.u1PrevNodeStatus = RM_STANDBY;
    gBgpCxtNode[0]->u1RestartMode = BGP4_RESTARTING_MODE;

    Bgp4VplsAuditVplsSpecEntry (0);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
#else
    return -1;
#endif
}
INT4
BgpUtFileVplsGRandHA7_33 (VOID)
{
    INT4                i4RetVal;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
    { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
    { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    UINT2               u2Length = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, BGP_L2VPN_BOTH_RT_DELETE);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, 20);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1RT, MAX_LEN_RT_VALUE);

    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, BGP4_RED_UPD_LEN_OFFSET, u4OffSet);

    u2Length = u4OffSet;
    u4OffSet = 20000;

    i4RetVal = Bgp4RedProcessVplsInfoSyncMsg (pMsg->pBuf, &u4OffSet, u2Length);

RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    if (i4RetVal == BGP4_FAILURE)
    {
        return BGP4_SUCCESS;
    }
    else
    {
        return BGP4_FAILURE;
    }
}

INT4
BgpUtFileVplsGRandHA7_34 (VOID)
{
    INT4                i4RetVal;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
    { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
    { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    UINT2               u2Length = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, BGP_L2VPN_BOTH_RT_DELETE);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, 20);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1RT, MAX_LEN_RT_VALUE);

    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, BGP4_RED_UPD_LEN_OFFSET, u4OffSet);

    u2Length = u4OffSet;
    u4OffSet = u2Length + 5;

    i4RetVal = Bgp4RedProcessVplsInfoSyncMsg (pMsg->pBuf, &u4OffSet, u2Length);

RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    if (i4RetVal == BGP4_FAILURE)
    {
        return BGP4_SUCCESS;
    }
    else
    {
        return BGP4_FAILURE;
    }
}
INT4
BgpUtFileVplsGRandHA7_35 (VOID)
{
#if 1
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);


    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    Bgp4VplsShutdown();
    Bgp4VplsAuditVplsSpecEntry (0);
    Bgp4VplsInit();
    
  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
#else
    return -1;
#endif
}
INT4
BgpUtFileVplsGRandHA7_36 (VOID)
{
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 15;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_DELETE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DOWN, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DELETE,
                               BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}
INT4
BgpUtFileVplsGRandHA7_37 (VOID)
{
#if 1
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 35;
    MEMCPY(EvtInfo.au1VplsName, au1VplsName, BGP4_VPLS_MAX_VPLS_NAME_SIZE);

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    Bgp4VplsMarkVplsEntryAsStale (0);
    
    /*For cleaning stale routes*/
    gBgpNode.u1PrevNodeStatus = RM_STANDBY;
    gBgpCxtNode[0]->u1RestartMode = BGP4_RESTARTING_MODE;

    Bgp4VplsAuditVplsSpecEntry (0);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
#else
    return -1;
#endif

}
INT4
BgpUtFileVplsGRandHA7_38 (VOID)
{
#if 1
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 35;
    MEMCPY(EvtInfo.au1VplsName, au1VplsName, BGP4_VPLS_MAX_VPLS_NAME_SIZE);

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    Bgp4VplsMarkVplsEntryAsStale (0);
    
    /*For cleaning stale routes*/
    gBgpNode.u1PrevNodeStatus = RM_STANDBY;
    gBgpCxtNode[0]->u1RestartMode = BGP4_RESTARTING_MODE;

    Bgp4VplsAuditVplsSpecEntry (0);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
#else
    return -1;
#endif
}
INT4
BgpUtFileVplsGRandHA7_39 (VOID)
{

    INT4    i4RetVal;
    tVplsSpecInfo VplsSpecInfo;
    tVplsSyncEventInfo VplsSyncEventInfo;

    MEMSET (&VplsSpecInfo, 0, sizeof(tVplsSpecInfo));
    MEMSET (&VplsSyncEventInfo, 0, sizeof(tVplsSyncEventInfo));

    VplsSpecInfo.u4VplsIndex = 1;
    VplsSyncEventInfo.u4VplsMsgType = 26;
    
    i4RetVal = Bgp4RedSyncVplsInfo(&VplsSpecInfo, &VplsSyncEventInfo);

    return i4RetVal;
}
INT4
BgpUtFileVplsGRandHA7_40 (VOID)
{
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x01, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 41;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 46;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_DELETE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_DELETE,
                               BGP4_DFLT_VRFID);
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_BOTH_RT_ADD,
                               BGP4_DFLT_VRFID);
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_BOTH_RT_DELETE,
                               BGP4_DFLT_VRFID);
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileVplsGRandHA7_41 (VOID)
{
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    tBgp4Info           pRtInfo;
    tBgp4Info          *pBgpInfo = &pRtInfo;


    tBgp4PeerEntry      Peer;
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         Addr;
    MEMSET (&Addr, 0, sizeof (tAddrPrefix));
    MEMSET (&Peer, 0, sizeof (tBgp4PeerEntry));
    gpBgpCurrCxtNode = gBgpCxtNode[0];

    Addr.u2Afi = 1;
    pPeer = &Peer;
    BGP4_PEER_REMOTE_ADDR_INFO (pPeer).u2Afi = 1;
    TMO_SLL_Add (BGP4_PEERENTRY_HEAD (0), (tTMO_SLL_NODE *) & Peer);

    Bgp4VplsDeleteStaleVplsLocalRoutes(0);


    return i4RetVal;
}
INT4
BgpUtFileVplsGRandHA7_42 (VOID)
{
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x01, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x70 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x50 };
    tRouteProfile       pRtProfile;
    tRouteProfile      *pRt = &pRtProfile;
    tVplsNlriInfo       VplsNlri;
    tBgp4Info           pRtInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tAfiSafiSpecInfo    pAfiSafiInstance;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    MEMSET (pRt, 0, sizeof (tRouteProfile));

    tBgp4PeerEntry      Peer;
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         Addr;
    MEMSET (&Addr, 0, sizeof (tAddrPrefix));
    MEMSET (&Peer, 0, sizeof (tBgp4PeerEntry));
    gpBgpCurrCxtNode = gBgpCxtNode[0];


    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 1;
    EvtInfo.u2VeBaseOffset = 10;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 0;
    EvtInfo.u2Mtu = 0;
    EvtInfo.u4VplsIndex = 20;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);

    VplsNlri.u2Length = BGP4_VPLS_NLRI_PREFIX_LEN;
    MEMCPY (VplsNlri.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    VplsNlri.u2VeId = 1;
    VplsNlri.u2VeBaseOffset = 10;
    VplsNlri.u2VeBaseSize = 10;
    VplsNlri.u4labelBase = 300000;

    pRt->pRtInfo = &pRtInfo;

    i4RetVal = Bgp4VplsConstructRouteProfile (0, pRt, NULL, &VplsNlri, 202);

    i4RetVal = Bgp4RibhAddRtEntry (pRt, 0);

    BGP4_RT_PROTOCOL (pRt) = 22;
    BGP4_RT_SET_FLAG(pRt, BGP4_RT_STALE);

    i4RetVal = Bgp4VplsGetVplsInfoFromIndex (20, &pVplsInfo);
    Addr.u2Afi = 1;
    pPeer = &Peer;
    BGP4_PEER_VPLS_AFISAFI_INSTANCE (pPeer) = &pAfiSafiInstance;
    BGP4_PEER_REMOTE_ADDR_INFO (pPeer).u2Afi = 1;
    TMO_SLL_Add (BGP4_PEERENTRY_HEAD (0), (tTMO_SLL_NODE *) & Peer);


    Bgp4VplsDeleteStaleVplsLocalRoutes(0);

    Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DELETE, BGP4_DFLT_VRFID);
    Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_DELETE,
                           BGP4_DFLT_VRFID);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileVplsGRandHA7_43 (VOID)
{                       

    INT4                i4RetVal;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
    { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
    { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, 15);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, 19);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1VplsName,
            BGP4_VPLS_MAX_VPLS_NAME_SIZE);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1RD,
            BGP4_VPLS_ROUTE_DISTING_SIZE);

    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, BGP4_RED_UPD_LEN_OFFSET, u4OffSet);

    u2Length = u4OffSet;
    u4OffSet = 3;

    i4RetVal = Bgp4RedProcessVplsInfoSyncMsg (pMsg->pBuf, &u4OffSet, u2Length);

RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}
INT4
BgpUtFileVplsGRandHA7_44 (VOID)
{ 
    INT4                i4RetVal;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
    { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
    { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, BGP_L2VPN_VPLS_DELETE);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, 111);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1VplsName,
            BGP4_VPLS_MAX_VPLS_NAME_SIZE);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1RD,
            BGP4_VPLS_ROUTE_DISTING_SIZE);

    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, BGP4_RED_UPD_LEN_OFFSET, u4OffSet);

    u2Length = u4OffSet;
    u4OffSet = 3;

    i4RetVal = Bgp4RedProcessVplsInfoSyncMsg (pMsg->pBuf, &u4OffSet, u2Length);

RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileVplsGRandHA7_45 (VOID)
{ 
    INT4                i4RetVal;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
    { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
    { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, BGP_L2VPN_VPLS_DOWN);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, 111);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1VplsName,
            BGP4_VPLS_MAX_VPLS_NAME_SIZE);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1RD,
            BGP4_VPLS_ROUTE_DISTING_SIZE);

    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, BGP4_RED_UPD_LEN_OFFSET, u4OffSet);

    u2Length = u4OffSet;
    u4OffSet = 3;

    i4RetVal = Bgp4RedProcessVplsInfoSyncMsg (pMsg->pBuf, &u4OffSet, u2Length);

RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}
INT4
BgpUtFileVplsGRandHA7_46 (VOID)
{ 
    INT4                i4RetVal;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
    { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
    { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, BGP_L2VPN_VPLS_UP);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, 111);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1VplsName,
            BGP4_VPLS_MAX_VPLS_NAME_SIZE);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1RD,
            BGP4_VPLS_ROUTE_DISTING_SIZE);

    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, BGP4_RED_UPD_LEN_OFFSET, u4OffSet);

    u2Length = u4OffSet;
    u4OffSet = 3;

    i4RetVal = Bgp4RedProcessVplsInfoSyncMsg (pMsg->pBuf, &u4OffSet, u2Length);

RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileVplsGRandHA7_47 (VOID)
{                       
#if 1
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT2               u2Length;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_IMPORT_RT_ADD;
    EvtInfo.u2VeId = 55;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 55;
    MEMCPY(EvtInfo.au1VplsName, au1VplsName, BGP4_VPLS_MAX_VPLS_NAME_SIZE);

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);
    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, BGP_L2VPN_VPLS_CREATE);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, 55);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1VplsName,
            BGP4_VPLS_MAX_VPLS_NAME_SIZE);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1RD,
            BGP4_VPLS_ROUTE_DISTING_SIZE);

    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, BGP4_RED_UPD_LEN_OFFSET, u4OffSet);

    u2Length = u4OffSet;
    u4OffSet = 3;

    i4RetVal = Bgp4RedProcessVplsInfoSyncMsg (pMsg->pBuf, &u4OffSet, u2Length);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
#else
    return -1;
#endif
}
INT4
BgpUtFileVplsGRandHA7_48 (VOID)
{                       
#if 1
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT2               u2Length;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 55;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 55;
    MEMCPY(EvtInfo.au1VplsName, au1VplsName, BGP4_VPLS_MAX_VPLS_NAME_SIZE);

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, BGP_L2VPN_VPLS_CREATE);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, 55);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1VplsName,
            BGP4_VPLS_MAX_VPLS_NAME_SIZE);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1RD,
            BGP4_VPLS_ROUTE_DISTING_SIZE);

    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, BGP4_RED_UPD_LEN_OFFSET, u4OffSet);

    u2Length = u4OffSet;
    u4OffSet = 3;

    i4RetVal = Bgp4RedProcessVplsInfoSyncMsg (pMsg->pBuf, &u4OffSet, u2Length);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
#else
    return -1;
#endif
}

INT4
BgpUtFileVplsGRandHA7_49 (VOID)
{
#if 1
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;
    tVplsSpecInfo      *pVplsSpecInfo[20000];

    int i,j;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 100;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300100;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 56;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);


    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, BGP_L2VPN_VPLS_CREATE);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, 55);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1VplsName,
            BGP4_VPLS_MAX_VPLS_NAME_SIZE);
    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet, au1RD,
            BGP4_VPLS_ROUTE_DISTING_SIZE);

    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, BGP4_RED_UPD_LEN_OFFSET, u4OffSet);

    u2Length = u4OffSet;
    u4OffSet = 3;

    for (i = 0; i < 20000; i++)
    {
        pVplsSpecInfo[i] =
            Bgp4MemAllocateVplsSpecEntry (sizeof (tVplsSpecInfo));
        if (pVplsSpecInfo[i] == NULL)
        {
            break;
        }
    }
    i4RetVal = Bgp4RedProcessVplsInfoSyncMsg (pMsg->pBuf, &u4OffSet, u2Length);

  RETURN:
    for (j = 0; j < i; j++)
    {
        MemReleaseMemBlock (BGP4_VPLS_SPEC_MEMPOOL_ID,
                            (UINT1 *) (pVplsSpecInfo[j]));
    }
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    if (i4RetVal == BGP4_FAILURE)
    {
        return BGP4_SUCCESS;
    }
    else
    {
        return BGP4_FAILURE;
    }
#else
    return -1;
#endif
}
INT4
BgpUtFileVplsGRandHA7_50 (VOID)
{
#if 1
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 4;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 35;
    MEMCPY(EvtInfo.au1VplsName, au1VplsName, BGP4_VPLS_MAX_VPLS_NAME_SIZE);

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_BOTH_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    Bgp4VplsMarkVplsEntryAsStale (0);

    
    /*For cleaning stale routes*/
    gBgpNode.u1PrevNodeStatus = RM_STANDBY;
    gBgpCxtNode[0]->u1RestartMode = BGP4_RESTARTING_MODE;
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    Bgp4VplsAuditVplsSpecEntry (0);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
#else
    return -1;
#endif
}

INT4
BgpUtFileVplsGRandHA7_51 (VOID)
{
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    int                 i;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);


    for (i=0; i<50; i++)
    {
        EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
        EvtInfo.u2VeId = 60 + i;
        EvtInfo.u2VeBaseOffset = 20;
        EvtInfo.u2VeBaseSize = 10;
        EvtInfo.u4LabelBase = 300000;
        EvtInfo.bControlWordFlag = 1;
        EvtInfo.u2Mtu = 50;
        EvtInfo.u4VplsIndex = 60 + i;
        MEMCPY(EvtInfo.au1VplsName, au1VplsName, BGP4_VPLS_MAX_VPLS_NAME_SIZE);

        MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
        MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

        i4RetVal =
            Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                    BGP4_DFLT_VRFID);

        i4RetVal =
            Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_ADD,
                    BGP4_DFLT_VRFID);

        i4RetVal =
            Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_BOTH_RT_ADD,
                    BGP4_DFLT_VRFID);

        i4RetVal =
            Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
        if (i4RetVal == BGP4_FAILURE)
        {
            goto RETURN;
        }
    }
    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;
    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;
    gBgpNode.u1NodeStatus = RM_STANDBY;
    Bgp4RedSyncVplsInfoBulkMsg (pMsg, &u4OffSet);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;

}
INT4
BgpUtFileVplsGRandHA7_52 (VOID)
{
#if 1
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 69;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 69;
    MEMCPY(EvtInfo.au1VplsName, au1VplsName, BGP4_VPLS_MAX_VPLS_NAME_SIZE);

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    Bgp4VplsMarkVplsEntryAsStale (0);

    
    /*For cleaning stale routes*/
    gBgpNode.u1PrevNodeStatus = RM_STANDBY;
    gBgpCxtNode[0]->u1RestartMode = BGP4_RESTARTING_MODE;
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_EXPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    Bgp4VplsAuditVplsSpecEntry (0);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
#else
    return -1;
#endif
}
INT4
BgpUtFileVplsGRandHA7_53 (VOID)
{
#if 1
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 70;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 70;
    MEMCPY(EvtInfo.au1VplsName, au1VplsName, BGP4_VPLS_MAX_VPLS_NAME_SIZE);

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    Bgp4VplsMarkVplsEntryAsStale (0);

    
    /*For cleaning stale routes*/
    gBgpNode.u1PrevNodeStatus = RM_STANDBY;
    gBgpCxtNode[0]->u1RestartMode = BGP4_RESTARTING_MODE;
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);
    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);


    Bgp4VplsAuditVplsSpecEntry (0);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
#else
    return -1;
#endif
}
INT4
BgpUtFileVplsGRandHA7_54 (VOID)
{
#if 1
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x90 };
    UINT1               au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE] = "vplsName1";
    tVplsSyncEventInfo  VplsSyncInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;
    UINT1               u1MsgType =0;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 70;
    EvtInfo.u2VeBaseOffset = 20;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 1;
    EvtInfo.u2Mtu = 50;
    EvtInfo.u4VplsIndex = 70;
    MEMCPY(EvtInfo.au1VplsName, au1VplsName, BGP4_VPLS_MAX_VPLS_NAME_SIZE);

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    gRmInfo.u1NumPeers = 1;
    gBgpNode.u1StandbyNodes = 1;
    gBgpNode.u1NodeStatus = RM_ACTIVE;

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);
    if (i4RetVal == BGP4_FAILURE)
    {
        goto RETURN;
    }

    /* Assign a RM Message*/
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
            == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;
    Bgp4RedSyncVplsInfoBulkMsg (pMsg, &u4OffSet);

    u4OffSet = 0;
    BGP4_RM_GET_1_BYTE (pMsg->pBuf, &u4OffSet, u1MsgType);
    BGP4_RM_GET_2_BYTE (pMsg->pBuf, &u4OffSet, u2Length);

    i4RetVal = Bgp4RedProcessVplsInfoBulkMsg (pMsg->pBuf, &u4OffSet, u2Length);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
#else
    return -1;
#endif
}

INT4
BgpUtFileVplsGRandHA7_55 (VOID)
{
    INT4                i4RetVal;
    tBgp4L2VpnEvtInfo   EvtInfo;
    UINT1               au1RD[MAX_LEN_RD_VALUE] =
        { 0x01, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x70 };
    UINT1               au1RT[MAX_LEN_RT_VALUE] =
        { 0x00, 0x02, 0x00, 0x64, 0x00, 0x00, 0x01, 0x50 };
    tRouteProfile       pRtProfile;
    tRouteProfile      *pRt = &pRtProfile;
    tVplsNlriInfo       VplsNlri;
    tBgp4Info           pRtInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    tAfiSafiSpecInfo    pAfiSafiInstance;

    /*Base conf */
    BgpUTBasicConf ();
    BgpSetContext (0);

    MEMSET (pRt, 0, sizeof (tRouteProfile));

    tBgp4PeerEntry      Peer;
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         Addr;
    MEMSET (&Addr, 0, sizeof (tAddrPrefix));
    MEMSET (&Peer, 0, sizeof (tBgp4PeerEntry));
    gpBgpCurrCxtNode = gBgpCxtNode[0];


    EvtInfo.u4MsgType = BGP_L2VPN_VPLS_CREATE;
    EvtInfo.u2VeId = 1;
    EvtInfo.u2VeBaseOffset = 10;
    EvtInfo.u2VeBaseSize = 10;
    EvtInfo.u4LabelBase = 300000;
    EvtInfo.bControlWordFlag = 0;
    EvtInfo.u2Mtu = 0;
    EvtInfo.u4VplsIndex = 20;

    MEMCPY (EvtInfo.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    MEMCPY (EvtInfo.au1RouteTarget, au1RT, MAX_LEN_RT_VALUE);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_CREATE,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_ADD,
                               BGP4_DFLT_VRFID);

    i4RetVal =
        Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_UP, BGP4_DFLT_VRFID);

    VplsNlri.u2Length = BGP4_VPLS_NLRI_PREFIX_LEN;
    MEMCPY (VplsNlri.au1RouteDistinguisher, au1RD, MAX_LEN_RD_VALUE);
    VplsNlri.u2VeId = 1;
    VplsNlri.u2VeBaseOffset = 10;
    VplsNlri.u2VeBaseSize = 10;
    VplsNlri.u4labelBase = 300000;

    pRt->pRtInfo = &pRtInfo;

    i4RetVal = Bgp4VplsConstructRouteProfile (0, pRt, NULL, &VplsNlri, 202);

    i4RetVal = Bgp4RibhAddRtEntry (pRt, 0);

    BGP4_RT_PROTOCOL (pRt) = BGP_ID;
    BGP4_RT_SET_FLAG(pRt, BGP4_RT_STALE);

    i4RetVal = Bgp4VplsGetVplsInfoFromIndex (20, &pVplsInfo);
    Addr.u2Afi = 1;
    pPeer = &Peer;
    BGP4_PEER_VPLS_AFISAFI_INSTANCE (pPeer) = &pAfiSafiInstance;
    BGP4_PEER_REMOTE_ADDR_INFO (pPeer).u2Afi = 1;
    TMO_SLL_Add (BGP4_PEERENTRY_HEAD (0), (tTMO_SLL_NODE *) & Peer);


    Bgp4VplsDeleteStaleVplsLocalRoutes(0);

    Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_VPLS_DELETE, BGP4_DFLT_VRFID);
    Bgp4VplsUpdateHandler (&EvtInfo, BGP_L2VPN_IMPORT_RT_DELETE,
                           BGP4_DFLT_VRFID);

  RETURN:
    /*Removing Base Conf */
    BgpReleaseContext ();
    BgpUTRemoveBasicConf ();

    return i4RetVal;
}

INT4
BgpUtFileVplsGRandHA7_56 (VOID)
{
    return -1;
}

INT4
BgpUtFileVplsGRandHA7_57 (VOID)
{
    return -1;
}

INT4
BgpUtFileVplsGRandHA7_58 (VOID)
{
    return -1;
}

INT4
BgpUtFileVplsGRandHA7_59 (VOID)
{
    return -1;
}

INT4
BgpUtFileVplsGRandHA7_60 (VOID)
{
    return -1;
}


#endif
