/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: bgptstcli.c,v 1.3 2014/03/14 12:45:54 siva Exp $
 **
 ** Description: BGP  UT Test cases cli file.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/

#include "lr.h"
#include "cli.h"
#include "include.h"
#include "bgp4com.h"
#include "bgptstcli.h"

#define MAX_ARGS 3
#define BGP_UT_TEST 1

extern VOID         BgpExecuteUtCase (UINT4 u4FileNumber, UINT4 u4TestNumber);
extern VOID         BgpExecuteUtAll (VOID);
extern VOID         BgpExecuteUtFile (UINT4 u4File);

/*  Function is called from bgptstcmd.def file */

INT4
cli_process_bgp_test_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[MAX_ARGS];
    INT4                i4Inst = 0;
    INT1                argno = 0;
    /*CliRegisterLock (CliHandle, BgpMainTaskLock, BgpMainTaskUnLock);
       BGP_LOCK; */

    va_start (ap, u4Command);
    UNUSED_PARAM (CliHandle);
    i4Inst = va_arg (ap, INT4);

    /* Walk through the arguments and store in args array.
     */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == MAX_ARGS)
            break;
    }

    va_end (ap);
    switch (u4Command)
    {
        case BGP_UT_TEST:
            if (args[0] == NULL)
            {
                /* specified case on specified file */
                if (args[2] != NULL)
                {
                    BgpExecuteUtCase (*(UINT4 *) (args[1]),
                                      *(UINT4 *) (args[2]));
                }
                /* all cases on specified file */
                else
                {
                    BgpExecuteUtFile (*(UINT4 *) (args[1]));
                }
            }
            else
            {
                /* all cases in all the files */
                BgpExecuteUtAll ();
            }
            break;
    }
    /*CliUnRegisterLock (CliHandle);
       BGP_UNLOCK; */
    return CLI_SUCCESS;
}
