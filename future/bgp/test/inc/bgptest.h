/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: bgptest.h,v 1.7 2014/10/14 12:13:16 siva Exp $
 **
 ** Description: BGP  UT Test cases.
 ** NOTE: This file should not be included in release packaging.
 ** ***********************************************************************/

#ifndef _BGPTEST_H_
#define _BGPTEST_H_
#include "include.h"
#include "bgp4com.h"
#include "cli.h"
#include "utilcli.h"


VOID BgpExecuteUtCase(UINT4 u4FileNumber, UINT4 u4TestNumber);
VOID BgpExecuteUtAll (VOID);
VOID BgpExecuteUtFile (UINT4 u4File);

typedef struct
{
    UINT1     u1File;
    UINT1     u1Case;
}tTestCase;

#define FILE_COUNT     15
tTestCase gTestCase2[] = {
    {1,15},  /* bgpfstcp */
    {2,101}, /* bgpfsSha */
    {3,12},  /* bgpfourbyteasn */
    {4,41},  /* bgporf */
    {5,21},  /* bgpbfd */
    {6,105}, /* L2VPN_VPLS */
    {7,60}   /* VPLS GR-HA */
};
INT4 BgpUtIPAddressConvert(UINT1* , tSNMP_OCTET_STRING_TYPE *);

/* bgpfstcp UT cases */
INT4 BgpUtBgpfstcp_1 (VOID);
INT4 BgpUtBgpfstcp_2 (VOID);
INT4 BgpUtBgpfstcp_3 (VOID);
INT4 BgpUtBgpfstcp_4 (VOID);
INT4 BgpUtBgpfstcp_5 (VOID);
INT4 BgpUtBgpfstcp_6 (VOID);
INT4 BgpUtBgpfstcp_7 (VOID);
INT4 BgpUtBgpfstcp_8 (VOID);
INT4 BgpUtBgpfstcp_9 (VOID);
INT4 BgpUtBgpfstcp_10 (VOID);
INT4 BgpUtBgpfstcp_11 (VOID);
INT4 BgpUtBgpfstcp_12 (VOID);
INT4 BgpUtBgpfstcp_13 (VOID);
INT4 BgpUtBgpfstcp_14 (VOID);
INT4 BgpUtBgpfstcp_15 (VOID);


/* file2 UT cases */
INT4 BgpUtFsSha_1 (VOID);
INT4 BgpUtFsSha_2 (VOID);
INT4 BgpUtFsSha_3 (VOID);
INT4 BgpUtFsSha_4 (VOID);
INT4 BgpUtFsSha_5 (VOID);
INT4 BgpUtFsSha_6 (VOID);
INT4 BgpUtFsSha_7 (VOID);
INT4 BgpUtFsSha_8 (VOID);
INT4 BgpUtFsSha_9 (VOID);
INT4 BgpUtFsSha_10 (VOID);
INT4 BgpUtFsSha_11 (VOID);
INT4 BgpUtFsSha_12 (VOID);
INT4 BgpUtFsSha_13 (VOID);
INT4 BgpUtFsSha_14 (VOID);
INT4 BgpUtFsSha_15 (VOID);
INT4 BgpUtFsSha_16 (VOID);
INT4 BgpUtFsSha_17 (VOID);
INT4 BgpUtFsSha_18 (VOID);
INT4 BgpUtFsSha_19 (VOID);
INT4 BgpUtFsSha_20 (VOID);
INT4 BgpUtFsSha_21 (VOID);
INT4 BgpUtFsSha_22 (VOID);
INT4 BgpUtFsSha_23 (VOID);
INT4 BgpUtFsSha_24 (VOID);
INT4 BgpUtFsSha_25 (VOID);
INT4 BgpUtFsSha_26 (VOID);
INT4 BgpUtFsSha_27 (VOID);
INT4 BgpUtFsSha_28 (VOID);
INT4 BgpUtFsSha_29 (VOID);
INT4 BgpUtFsSha_30 (VOID);
INT4 BgpUtFsSha_31 (VOID);
INT4 BgpUtFsSha_32 (VOID);
INT4 BgpUtFsSha_33 (VOID);
INT4 BgpUtFsSha_34 (VOID);
INT4 BgpUtFsSha_35 (VOID);
INT4 BgpUtFsSha_36 (VOID);
INT4 BgpUtFsSha_37 (VOID);
INT4 BgpUtFsSha_38 (VOID);
INT4 BgpUtFsSha_39 (VOID);
INT4 BgpUtFsSha_40 (VOID);
INT4 BgpUtFsSha_41 (VOID);
INT4 BgpUtFsSha_42 (VOID);
INT4 BgpUtFsSha_43 (VOID);
INT4 BgpUtFsSha_44 (VOID);
INT4 BgpUtFsSha_45 (VOID);
INT4 BgpUtFsSha_46 (VOID);
INT4 BgpUtFsSha_47 (VOID);
INT4 BgpUtFsSha_48 (VOID);
INT4 BgpUtFsSha_49 (VOID);
INT4 BgpUtFsSha_50 (VOID);
INT4 BgpUtFsSha_51 (VOID);
INT4 BgpUtFsSha_52 (VOID);
INT4 BgpUtFsSha_53 (VOID);
INT4 BgpUtFsSha_57 (VOID);
INT4 BgpUtFsSha_58 (VOID);
INT4 BgpUtFsSha_62 (VOID);
INT4 BgpUtFsSha_63 (VOID);
INT4 BgpUtFsSha_92 (VOID);
INT4 BgpUtFsSha_93 (VOID);
INT4 BgpUtFsSha_94 (VOID);
INT4 BgpUtFsSha_95 (VOID);
INT4 BgpUtFsSha_96 (VOID);
INT4 BgpUtFsSha_97 (VOID);
INT4 BgpUtFsSha_98 (VOID);
INT4 BgpUtFsSha_99 (VOID);
INT4 BgpUtFsSha_100 (VOID);
INT4 BgpUtFsSha_101 (VOID);
INT4 BgpUtFsSha_105 (VOID);
INT4 BgpUtFsSha_106 (VOID);
INT4 BgpUtFsSha_107 (VOID);
INT4 BgpUtFsSha_108 (VOID);
INT4 BgpUtFile2_13 (VOID);
INT4 BgpUtFile2_14 (VOID);
INT4 BgpUtFile2_15 (VOID);

/* bgpfourbyteasn UT cases */
INT4 BgpUt4ByteAsn_1 (VOID);
INT4 BgpUt4ByteAsn_2 (VOID);
INT4 BgpUt4ByteAsn_3 (VOID);
INT4 BgpUt4ByteAsn_4 (VOID);
INT4 BgpUt4ByteAsn_5 (VOID);
INT4 BgpUt4ByteAsn_6 (VOID);
INT4 BgpUt4ByteAsn_7 (VOID);
INT4 BgpUt4ByteAsn_8 (VOID);
INT4 BgpUt4ByteAsn_9 (VOID);
INT4 BgpUt4ByteAsn_10 (VOID);
INT4 BgpUt4ByteAsn_11 (VOID);
INT4 BgpUt4ByteAsn_12 (VOID);


/* L2VPN-VPLS UT cases */
VOID BgpUTRemoveBasicConf(VOID);
VOID BgpUTBasicConf(VOID);
INT4 BgpUtFileL2vpnVpls6_1 (VOID);
INT4 BgpUtFileL2vpnVpls6_2 (VOID);
INT4 BgpUtFileL2vpnVpls6_3 (VOID);
INT4 BgpUtFileL2vpnVpls6_4 (VOID);
INT4 BgpUtFileL2vpnVpls6_5 (VOID);
INT4 BgpUtFileL2vpnVpls6_6 (VOID);
INT4 BgpUtFileL2vpnVpls6_7 (VOID);
INT4 BgpUtFileL2vpnVpls6_8 (VOID);
INT4 BgpUtFileL2vpnVpls6_9 (VOID);
INT4 BgpUtFileL2vpnVpls6_10 (VOID);
INT4 BgpUtFileL2vpnVpls6_11 (VOID);
INT4 BgpUtFileL2vpnVpls6_12 (VOID);
INT4 BgpUtFileL2vpnVpls6_13 (VOID);
INT4 BgpUtFileL2vpnVpls6_14 (VOID);
INT4 BgpUtFileL2vpnVpls6_15 (VOID);
INT4 BgpUtFileL2vpnVpls6_16 (VOID);
INT4 BgpUtFileL2vpnVpls6_17 (VOID);
INT4 BgpUtFileL2vpnVpls6_18 (VOID);
INT4 BgpUtFileL2vpnVpls6_19 (VOID);
INT4 BgpUtFileL2vpnVpls6_20 (VOID);
INT4 BgpUtFileL2vpnVpls6_21 (VOID);
INT4 BgpUtFileL2vpnVpls6_22 (VOID);
INT4 BgpUtFileL2vpnVpls6_23 (VOID);
INT4 BgpUtFileL2vpnVpls6_24 (VOID);
INT4 BgpUtFileL2vpnVpls6_25 (VOID);
INT4 BgpUtFileL2vpnVpls6_26 (VOID);
INT4 BgpUtFileL2vpnVpls6_27 (VOID);
INT4 BgpUtFileL2vpnVpls6_28 (VOID);
INT4 BgpUtFileL2vpnVpls6_29 (VOID);
INT4 BgpUtFileL2vpnVpls6_30 (VOID);
INT4 BgpUtFileL2vpnVpls6_31 (VOID);
INT4 BgpUtFileL2vpnVpls6_32 (VOID);
INT4 BgpUtFileL2vpnVpls6_33 (VOID);
INT4 BgpUtFileL2vpnVpls6_34 (VOID);
INT4 BgpUtFileL2vpnVpls6_35 (VOID);
INT4 BgpUtFileL2vpnVpls6_36 (VOID);
INT4 BgpUtFileL2vpnVpls6_37 (VOID);
INT4 BgpUtFileL2vpnVpls6_38 (VOID);
INT4 BgpUtFileL2vpnVpls6_39 (VOID);
INT4 BgpUtFileL2vpnVpls6_40 (VOID);
INT4 BgpUtFileL2vpnVpls6_41 (VOID);
INT4 BgpUtFileL2vpnVpls6_42 (VOID);
INT4 BgpUtFileL2vpnVpls6_43 (VOID);
INT4 BgpUtFileL2vpnVpls6_44 (VOID);
INT4 BgpUtFileL2vpnVpls6_45 (VOID);
INT4 BgpUtFileL2vpnVpls6_46 (VOID);
INT4 BgpUtFileL2vpnVpls6_47 (VOID);
INT4 BgpUtFileL2vpnVpls6_48 (VOID);
INT4 BgpUtFileL2vpnVpls6_49 (VOID);
INT4 BgpUtFileL2vpnVpls6_50 (VOID);
INT4 BgpUtFileL2vpnVpls6_51 (VOID);
INT4 BgpUtFileL2vpnVpls6_52 (VOID);
INT4 BgpUtFileL2vpnVpls6_53 (VOID);
INT4 BgpUtFileL2vpnVpls6_54 (VOID);
INT4 BgpUtFileL2vpnVpls6_55 (VOID);
INT4 BgpUtFileL2vpnVpls6_56 (VOID);
INT4 BgpUtFileL2vpnVpls6_57 (VOID);
INT4 BgpUtFileL2vpnVpls6_58 (VOID);
INT4 BgpUtFileL2vpnVpls6_59 (VOID);
INT4 BgpUtFileL2vpnVpls6_60 (VOID);
INT4 BgpUtFileL2vpnVpls6_61 (VOID);
INT4 BgpUtFileL2vpnVpls6_62 (VOID);
INT4 BgpUtFileL2vpnVpls6_63 (VOID);
INT4 BgpUtFileL2vpnVpls6_64 (VOID);
INT4 BgpUtFileL2vpnVpls6_65 (VOID);
INT4 BgpUtFileL2vpnVpls6_66 (VOID);
INT4 BgpUtFileL2vpnVpls6_67 (VOID);
INT4 BgpUtFileL2vpnVpls6_68 (VOID);
INT4 BgpUtFileL2vpnVpls6_69 (VOID);
INT4 BgpUtFileL2vpnVpls6_70 (VOID);
INT4 BgpUtFileL2vpnVpls6_71 (VOID);
INT4 BgpUtFileL2vpnVpls6_72 (VOID);
INT4 BgpUtFileL2vpnVpls6_73 (VOID);
INT4 BgpUtFileL2vpnVpls6_74 (VOID);
INT4 BgpUtFileL2vpnVpls6_75 (VOID);
INT4 BgpUtFileL2vpnVpls6_76 (VOID);
INT4 BgpUtFileL2vpnVpls6_77 (VOID);
INT4 BgpUtFileL2vpnVpls6_78 (VOID);
INT4 BgpUtFileL2vpnVpls6_79 (VOID);
INT4 BgpUtFileL2vpnVpls6_80 (VOID);
INT4 BgpUtFileL2vpnVpls6_81 (VOID);
INT4 BgpUtFileL2vpnVpls6_82 (VOID);
INT4 BgpUtFileL2vpnVpls6_83 (VOID);
INT4 BgpUtFileL2vpnVpls6_84 (VOID);
INT4 BgpUtFileL2vpnVpls6_85 (VOID);


/* VPLS GR and HA UT cases */
VOID BgpUTRemoveGRHABasicConf(VOID);
VOID BgpUTBasicGRHAConf(VOID);
INT4 BgpUtFileVplsGRandHA7_1 (VOID);
INT4 BgpUtFileVplsGRandHA7_2 (VOID);
INT4 BgpUtFileVplsGRandHA7_3 (VOID);
INT4 BgpUtFileVplsGRandHA7_4 (VOID);
INT4 BgpUtFileVplsGRandHA7_5 (VOID);
INT4 BgpUtFileVplsGRandHA7_6 (VOID);
INT4 BgpUtFileVplsGRandHA7_7 (VOID);
INT4 BgpUtFileVplsGRandHA7_8 (VOID);
INT4 BgpUtFileVplsGRandHA7_9 (VOID);
INT4 BgpUtFileVplsGRandHA7_10 (VOID);
INT4 BgpUtFileVplsGRandHA7_11 (VOID);
INT4 BgpUtFileVplsGRandHA7_12 (VOID);
INT4 BgpUtFileVplsGRandHA7_13 (VOID);
INT4 BgpUtFileVplsGRandHA7_14 (VOID);
INT4 BgpUtFileVplsGRandHA7_15 (VOID);
INT4 BgpUtFileVplsGRandHA7_16 (VOID);
INT4 BgpUtFileVplsGRandHA7_17 (VOID);
INT4 BgpUtFileVplsGRandHA7_18 (VOID);
INT4 BgpUtFileVplsGRandHA7_19 (VOID);
INT4 BgpUtFileVplsGRandHA7_20 (VOID);
INT4 BgpUtFileVplsGRandHA7_21 (VOID);
INT4 BgpUtFileVplsGRandHA7_22 (VOID);
INT4 BgpUtFileVplsGRandHA7_23 (VOID);
INT4 BgpUtFileVplsGRandHA7_24 (VOID);
INT4 BgpUtFileVplsGRandHA7_25 (VOID);
INT4 BgpUtFileVplsGRandHA7_26 (VOID);
INT4 BgpUtFileVplsGRandHA7_27 (VOID);
INT4 BgpUtFileVplsGRandHA7_28 (VOID);
INT4 BgpUtFileVplsGRandHA7_29 (VOID);
INT4 BgpUtFileVplsGRandHA7_30 (VOID);
INT4 BgpUtFileVplsGRandHA7_31 (VOID);
INT4 BgpUtFileVplsGRandHA7_32 (VOID);
INT4 BgpUtFileVplsGRandHA7_33 (VOID);
INT4 BgpUtFileVplsGRandHA7_34 (VOID);
INT4 BgpUtFileVplsGRandHA7_35 (VOID);
INT4 BgpUtFileVplsGRandHA7_36 (VOID);
INT4 BgpUtFileVplsGRandHA7_37 (VOID);
INT4 BgpUtFileVplsGRandHA7_38 (VOID);
INT4 BgpUtFileVplsGRandHA7_39 (VOID);
INT4 BgpUtFileVplsGRandHA7_41 (VOID);
INT4 BgpUtFileVplsGRandHA7_42 (VOID);
INT4 BgpUtFileVplsGRandHA7_43 (VOID);
INT4 BgpUtFileVplsGRandHA7_44 (VOID);
INT4 BgpUtFileVplsGRandHA7_45 (VOID);
INT4 BgpUtFileVplsGRandHA7_46 (VOID);
INT4 BgpUtFileVplsGRandHA7_47 (VOID);
INT4 BgpUtFileVplsGRandHA7_48 (VOID);
INT4 BgpUtFileVplsGRandHA7_49 (VOID);
INT4 BgpUtFileVplsGRandHA7_50 (VOID);
INT4 BgpUtFileVplsGRandHA7_51 (VOID);
INT4 BgpUtFileVplsGRandHA7_52 (VOID);
INT4 BgpUtFileVplsGRandHA7_53 (VOID);
INT4 BgpUtFileVplsGRandHA7_54 (VOID);
INT4 BgpUtFileVplsGRandHA7_55 (VOID);
INT4 BgpUtFileVplsGRandHA7_56 (VOID);
INT4 BgpUtFileVplsGRandHA7_57 (VOID);
INT4 BgpUtFileVplsGRandHA7_58 (VOID);
INT4 BgpUtFileVplsGRandHA7_59 (VOID);
INT4 BgpUtFileVplsGRandHA7_60 (VOID);

/* bgp-orf cases */
INT4 BgpUtOrf_1 (VOID);
INT4 BgpUtOrf_2 (VOID);
INT4 BgpUtOrf_3 (VOID);
INT4 BgpUtOrf_4 (VOID);
INT4 BgpUtOrf_5 (VOID);
INT4 BgpUtOrf_6 (VOID);
INT4 BgpUtOrf_7 (VOID);
INT4 BgpUtOrf_8 (VOID);
INT4 BgpUtOrf_9 (VOID);
INT4 BgpUtOrf_10 (VOID);
INT4 BgpUtOrf_11 (VOID);
INT4 BgpUtOrf_12 (VOID);
INT4 BgpUtOrf_13 (VOID);
INT4 BgpUtOrf_14 (VOID);
INT4 BgpUtOrf_15 (VOID);
INT4 BgpUtOrf_16 (VOID);
INT4 BgpUtOrf_17 (VOID);
INT4 BgpUtOrf_18 (VOID);
INT4 BgpUtOrf_19 (VOID);
INT4 BgpUtOrf_20 (VOID);
INT4 BgpUtOrf_21 (VOID);
INT4 BgpUtOrf_22 (VOID);
INT4 BgpUtOrf_23 (VOID);
INT4 BgpUtOrf_24 (VOID);
INT4 BgpUtOrf_25 (VOID);
INT4 BgpUtOrf_26 (VOID);
INT4 BgpUtOrf_27 (VOID);
INT4 BgpUtOrf_28 (VOID);
INT4 BgpUtOrf_29 (VOID);
INT4 BgpUtOrf_30 (VOID);
INT4 BgpUtOrf_31 (VOID);
INT4 BgpUtOrf_32 (VOID);
INT4 BgpUtOrf_33 (VOID);
INT4 BgpUtOrf_34 (VOID);
INT4 BgpUtOrf_35 (VOID);
INT4 BgpUtOrf_36 (VOID);
INT4 BgpUtOrf_37 (VOID);
INT4 BgpUtOrf_38 (VOID);
INT4 BgpUtOrf_39 (VOID);
INT4 BgpUtOrf_40 (VOID);
INT4 BgpUtOrf_41 (VOID);

/* BGP-BFD UT case */
INT4 BgpUtBfd_1 (VOID);
INT4 BgpUtBfd_2 (VOID);
INT4 BgpUtBfd_3 (VOID);
INT4 BgpUtBfd_4 (VOID);
INT4 BgpUtBfd_5 (VOID);
INT4 BgpUtBfd_6 (VOID);
INT4 BgpUtBfd_7 (VOID);
INT4 BgpUtBfd_8 (VOID);
INT4 BgpUtBfd_9 (VOID);
INT4 BgpUtBfd_10 (VOID);
INT4 BgpUtBfd_11 (VOID);
INT4 BgpUtBfd_12 (VOID);
INT4 BgpUtBfd_13 (VOID);
INT4 BgpUtBfd_14 (VOID);
INT4 BgpUtBfd_15 (VOID);
INT4 BgpUtBfd_16 (VOID);
INT4 BgpUtBfd_17 (VOID);
INT4 BgpUtBfd_18 (VOID);
INT4 BgpUtBfd_19 (VOID);
INT4 BgpUtBfd_20 (VOID);
INT4 BgpUtBfd_21 (VOID);

#endif /* _BGPTEST_H_ */ 
