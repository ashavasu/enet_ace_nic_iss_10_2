
/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: bgptstcli.h,v 1.2 2013/06/07 13:27:44 siva Exp $
 **
 ** Description: BGP  UT Test cases.
 ** NOTE: This file should not be included in release packaging.
 ** ***********************************************************************/
#ifndef BGPTSCLI_H
#define BGPTSCLI_H

#include "lr.h"
#include "cli.h"

#if 0
INT4 cli_process_bgp_test_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);
#endif
INT4
BgpUt(INT4 );

#endif
