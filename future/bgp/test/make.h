##########################################################################
# Copyright (C) Future Software Limited,2010                    #
#                                                                        #
# $Id: make.h,v 1.3 2014/03/14 12:45:53 siva Exp $                     #
#                                                 #
# Description : Contains information for creating the make file          #
#  for this module                                       #
#                                                  #
##########################################################################

#include the make.h and make.rule from LR
include ../../LR/make.h
include ../../LR/make.rule

#Compilation switches
COMP_SWITCHES = $(GENERAL_COMPILATION_SWITCHES)\
                $(SYSTEM_COMPILATION_SWITCHES)\
                $(PROJECT_COMPILATION_SWITCHES)

#project directories
BASE_INC_DIR           = $(BASE_DIR)/inc                
BGP_INCL_DIR           = $(BASE_DIR)/bgp/inc
BGP_PORT_DIR     =  $(BASE_DIR)/bgp/port
TRIEINCD        = ${BASE_DIR}/util/trie2/inc

BGP_TEST_BASE_DIR  = ${BASE_DIR}/bgp/test
BGP_TEST_SRC_DIR   = ${BGP_TEST_BASE_DIR}/src
BGP_TEST_INC_DIR   = ${BGP_TEST_BASE_DIR}/inc
BGP_TEST_OBJ_DIR   = ${BGP_TEST_BASE_DIR}/obj

BGP_TEST_INCLUDES  = -I$(BASE_INC_DIR) \
                     -I${TRIEINCD} \
                     -I$(BGP_PORT_DIR) \
                     -I$(BGP_TEST_INC_DIR) \
                     -I$(BGP_INCL_DIR) \
                     -I$(COMMON_INCLUDE_DIRS)

PROJECT_COMPILATION_SWITCHES = -DRFD_WANTED\
                               -UBGP_TCP6_WANTED\
                               -DRRD_WANTED\
                               -DBGP_TCP4_WANTED\
                               -UBGP_DEBUG\
                               -UL3VPN\
                               -DVPLSADS_WANTED\
                               -UBGP_TEST_WANTED

##########################################################################
#                    End of file make.h      #
##########################################################################
