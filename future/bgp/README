Copyright (C) Future Software Limited, 2002-2003

Introduction
============
FutureBGP4 is a portable implementation of the BGP4 protocol.

Contents
========
The contents of this distribution are as follows:

inc/		- BGP Header files
src/		- BGP Source files
Makefile	- The FutureBGP4 module level makefile
Makefile.vxw    - This is to run FutureBGP4 over Vxworks TCP/IP stack
Makefile.linux  - This is to run FutureBGP4 over Linux TCP/IP stack
Makefile.diab   - This is to compile FutureBGP4 in diab compiler 
make.h	        - Contains compile flags and -I path used during make
make.diab.h	- Contains compile flags and -I path used during diab compilation
bgp4.dsp  -  This Project to be used for VC++ compilation
RELEASE         - contains release information
VERSION         - contains versions of infrastructure modules

The base path needs to be updated to the current base in $HOME/LR/make.h
(referenced by BASE_DIR)

Ensure that the following modules under $HOME/future folder are included 
(dependency modules) for compiling BGP  with any stack -  
1. fsap2 
2. sli
3. inc
4. LR 
5. snmp/snmp_2 
6. util

The following modules should be included in $HOME/future/LR/make.h and in the
build package for building BGP with Future TCP/IP stack -
1. BGP
2. CFA
3. FFM
4. IP
5. SLI
6. SNMP/SNMP_2
7. TCP
8. IP6RTR when IP6 module is needed

The following modules should be included in $HOME/future/LR/make.h :
(For building Linux TCP/IP Stack ) -
1. BGP
2. SNMP
The following compilation switches need to be enabled in $HOME/future/LR/make.h
4. LINUX_SLI_WANTED,for compiling BGP in Linux stack

At future/bgp, the following change needs to be done -
1. Makefile.linux needs to be the default makefile

In addition,
a. RRD_WANTED switch needs to be enabled for testing the RRD feature
b. SIZING_WANTED switch needs to be enabled in $HOME/future/LR/make.h,
   if the start-up parameters need to be changed after building the
   executable. (By reading from a config file).
c. RFD_WANTED switch is enabled by default in $HOME/future/bgp/make.h
    It can be disabled to disabe the RFD feature. 
   If RFD_WANTED switch is enabled, then the macro "RFD_CONFIG_DATA_FILE_NAME" 
   in inc/bgrfd.h needs to be updated with the correct path.
   Also the BGP4_RFD_CUTOFF_THRESHOLD defined in 
   $HOME/future/bgp/inc/rfdconfig.size needs to be set to a very high value -
   e.g. "999999" while executing the BGP STP testcases with RFD_WANTED switch
   enabled.
d. NO_CAPABILITY_MATCH_REPEERING switch needs to be enabled to allow BGP peering
   (without capabilities) even if there are no common capabilities between two 
   BGP peers.
e. BGP4_IPV6_WANTED needs to be enabled, when IPV6 routing needs to be 
   supported.
f. BGP_TCP4_WANTED  needs to be enabled, when V4 BGP peers need to be supported.
g. BGP_TCP6_WANTED needs to be enabled, when V6 BGP peers need to be supported.

FutureBGP4 CLI support:
a) If CLI support is needed, then future/cli module should be checked out and
   CLI should be included while building the executable.
b) For including the FutureBGP4 supported CLI command follow the instruction
   below:
   *) Go to folder future/cli/tools and run make.
   *) Get the file bgp4cmds.def from future/bgp/port and place it in
      future/cli/tools/exe.
   *) Execute the command "./cmdscan bgp4cmds.def clicmds.c".
   *) File clicmds.c will be generated. Place this file in future/cli/src.
   *) On building the executable, the FutureBGP4 CLI commands can be executed
      from CLI prompt.

For compiling BGP for pthreads, the following needs to be done -
a) In $HOME/future/LR/Makefile, uncomment the line 
   "make -w -C $(BASE_DIR)/fsap2" and add the following -
   "make -w -C $(BASE_DIR)/fsap2 -f makefile.pth". 
    This compiles fsap2 for pthreads

b) In $HOME/future/LR/make.h, assign FSAP_OSIX_BASE_DIR to 
   ${FSAP_BASE_DIR}/pthreads

c) In $HOME/future/LR/make.rule, pthread library needs to be added for 
   linking by adding "-lpthread" to LR_EXE_LIBS 

For compiling BGP for VC++, the following needs to be done -
a)Create any workspace and include the project bgp/bgp4.dsp.

For compiling BGP for DIAB, the following needs to be done -
a)Replace Makefile by Makefile.diab & make.h by make.diab.h.

b)Change LR/make.h and LR/Makefile as described in Diab Compilation help on 
  product server. Then do the necessary changes (as described above) to include
  supporting modules.

Please refer the FutureBGP Porting Guide for instructions on building 
the module.

NOTE -
In make.h, either BGP_TCP4_WANTED/BGP_TCP6_WANTED compilation switches MUST be
enabled. However, both the switches can be enabled when dual peers (V4 and V6) 
need to be supported.
