/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgpsz.c,v 1.6 2017/09/15 06:19:52 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/
#define _BGPSZ_C
#include "bgp4com.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
/*UINT4 gu4BgpDebugCnt[BGP_MAX_SIZING_ID];*/
INT4
BgpSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < BGP_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsBGPSizingParams[i4SizingId].u4StructSize,
                                     FsBGPSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(BGPMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            BgpSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
BgpSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsBGPSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, BGPMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
BgpSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < BGP_MAX_SIZING_ID; i4SizingId++)
    {
        if (BGPMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (BGPMemPoolIds[i4SizingId]);
            BGPMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
