/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: asconlow.c,v 1.16 2013/07/17 13:00:53 siva Exp $
 *
 * Description: This file contains functions related to AS
 *              Confederation feature.
 *
 *******************************************************************/

# include  "include.h"
# include  "bgp4com.h"
# include  "fsmpbgcli.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgpAscConfedId
 Input       :  The Indices

                The Object 
                retValFsbgpAscConfedId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgpAscConfedId (UINT4 *pu4RetValFsbgpAscConfedId)
{
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    *pu4RetValFsbgpAscConfedId = BGP4_CONFED_ID (u4Context);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgpAscConfedBestPathCompareMED
 Input       :  The Indices

                The Object 
                retValFsbgpAscConfedBestPathCompareMED
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgpAscConfedBestPathCompareMED (INT4
                                        *pi4RetValFsbgpAscConfedBestPathCompareMED)
{

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    if (BGP4_CONFED_BESTPATH_COMPARE_MED (u4Context) == BGP4_TRUE)
    {
        *pi4RetValFsbgpAscConfedBestPathCompareMED =
            BGP4_CONFED_COMPARE_MED_SET;
    }
    else
    {
        *pi4RetValFsbgpAscConfedBestPathCompareMED =
            BGP4_CONFED_COMPARE_MED_CLEAR;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgpAscConfedId
 Input       :  The Indices

                The Object 
                setValFsbgpAscConfedId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgpAscConfedId (UINT4 u4SetValFsbgpAscConfedId)
{
    tBgp4PeerEntry     *pPeer = NULL;
    UINT4               u4Indx;

    UINT4               u4Context;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    /* Confed Id is configured already */
    if (BGP4_CONFED_ID (u4Context) != BGP4_INV_AS)
    {
        /* Confed Id is configured already to same value */
        if (u4SetValFsbgpAscConfedId == BGP4_CONFED_ID (u4Context))
        {
            return SNMP_SUCCESS;
        }
        else if (u4SetValFsbgpAscConfedId != BGP4_INV_AS)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tBGP CONFED Id already configured.\n");
            return SNMP_FAILURE;
        }
    }

    /* Confed is already disabled */
    if ((BGP4_CONFED_ID (u4Context) == BGP4_INV_AS) &&
        (u4SetValFsbgpAscConfedId == BGP4_INV_AS))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP CONFED Id already disabled.\n");
        return SNMP_SUCCESS;
    }

    BGP4_CONFED_ID (u4Context) = u4SetValFsbgpAscConfedId;

    if (u4SetValFsbgpAscConfedId == BGP4_INV_AS)
    {
        /* Reset the Confed peer status of all nbr entries  */
        TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
        {
            BGP4_CONFED_PEER_STATUS (pPeer) = BGP4_FALSE;
        }
    }
    else
    {
        for (u4Indx = 0; u4Indx < BGP4_CONFED_MAX_MEMBER_AS; u4Indx++)
        {
            if (BGP4_CONFED_PEER (u4Context, u4Indx) == BGP4_INV_AS)
            {
                continue;
            }
            /* Mark the peers belongs to this AS as Confed peer */
            TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer,
                          tBgp4PeerEntry *)
            {
                if (BGP4_PEER_ASNO (pPeer) ==
                    BGP4_CONFED_PEER (u4Context, u4Indx))
                {
                    BGP4_CONFED_PEER_STATUS (pPeer) = BGP4_TRUE;
                }
            }
        }
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4AscConfedId, u4SeqNum, FALSE,
                          BgpLock, BgpUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u", gpBgpCurrCxtNode->u4ContextId,
                      u4SetValFsbgpAscConfedId));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsbgpAscConfedBestPathCompareMED
 Input       :  The Indices

                The Object 
                setValFsbgpAscConfedBestPathCompareMED
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgpAscConfedBestPathCompareMED (INT4
                                        i4SetValFsbgpAscConfedBestPathCompareMED)
{

    UINT4               u4Context;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    if (i4SetValFsbgpAscConfedBestPathCompareMED == BGP4_CONFED_COMPARE_MED_SET)
    {
        BGP4_CONFED_BESTPATH_COMPARE_MED (u4Context) = BGP4_TRUE;
    }
    else
    {
        BGP4_CONFED_BESTPATH_COMPARE_MED (u4Context) = BGP4_FALSE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4AscConfedBestPathCompareMED,
                          u4SeqNum, FALSE, BgpLock, BgpUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gpBgpCurrCxtNode->u4ContextId,
                      i4SetValFsbgpAscConfedBestPathCompareMED));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsbgpAscConfedId
 Input       :  The Indices

                The Object 
                testValFsbgpAscConfedId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsbgpAscConfedId (UINT4 *pu4ErrorCode, UINT4 u4TestValFsbgpAscConfedId)
{

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }

    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Local AS number not configured.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }

    if ((u4TestValFsbgpAscConfedId > BGP4_MAX_AS) ||
        (BGP4_LOCAL_AS_NO (u4Context) == u4TestValFsbgpAscConfedId))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tInvalid Confed Identifier value.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_CONFED_ID_ERR);
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsbgpAscConfedBestPathCompareMED
 Input       :  The Indices

                The Object 
                testValFsbgpAscConfedBestPathCompareMED
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsbgpAscConfedBestPathCompareMED (UINT4 *pu4ErrorCode,
                                           INT4
                                           i4TestValFsbgpAscConfedBestPathCompareMED)
{
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgpAscConfedBestPathCompareMED ==
         BGP4_CONFED_COMPARE_MED_SET) ||
        (i4TestValFsbgpAscConfedBestPathCompareMED ==
         BGP4_CONFED_COMPARE_MED_CLEAR))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tInvalid Confed Best Path Compare MED option value.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_MED_OPT_VALUE_ERR);
        return SNMP_FAILURE;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsbgpAscConfedId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsbgpAscConfedId (UINT4 *pu4ErrorCode,
                          tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsbgpAscConfedBestPathCompareMED
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsbgpAscConfedBestPathCompareMED (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsbgpAscConfedPeerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgpAscConfedPeerTable
 Input       :  The Indices
                FsbgpAscConfedPeerASNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgpAscConfedPeerTable (UINT4 u4FsbgpAscConfedPeerASNo)
{
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    if (u4FsbgpAscConfedPeerASNo > BGP4_MAX_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tInvalid Confed Peer AS number.\n");
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgpAscConfedPeerTable
 Input       :  The Indices
                FsbgpAscConfedPeerASNo
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgpAscConfedPeerTable (UINT4 *pu4FsbgpAscConfedPeerASNo)
{
    UINT4               u4Indx;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    for (u4Indx = 0; u4Indx < BGP4_CONFED_MAX_MEMBER_AS; u4Indx++)
    {
        if (BGP4_CONFED_PEER (u4Context, u4Indx) != BGP4_INV_AS)
        {
            *pu4FsbgpAscConfedPeerASNo = BGP4_CONFED_PEER (u4Context, u4Indx);
            return SNMP_SUCCESS;
        }
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tNo Confed Peer is present.\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgpAscConfedPeerTable
 Input       :  The Indices
                FsbgpAscConfedPeerASNo
                nextFsbgpAscConfedPeerASNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgpAscConfedPeerTable (UINT4 u4FsbgpAscConfedPeerASNo,
                                        UINT4 *pu4NextFsbgpAscConfedPeerASNo)
{
    UINT4               u4Indx;
    UINT1               u1PeerASFound = BGP4_FALSE;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    if (u4FsbgpAscConfedPeerASNo > BGP4_MAX_AS)
    {
        return SNMP_FAILURE;
    }
    for (u4Indx = 0; u4Indx < BGP4_CONFED_MAX_MEMBER_AS; u4Indx++)
    {
        if (BGP4_CONFED_PEER (u4Context, u4Indx) == u4FsbgpAscConfedPeerASNo)
        {
            u1PeerASFound = BGP4_TRUE;
            continue;
        }
        if ((u1PeerASFound == BGP4_TRUE) &&
            (BGP4_CONFED_PEER (u4Context, u4Indx) != BGP4_INV_AS))
        {

            *pu4NextFsbgpAscConfedPeerASNo =
                BGP4_CONFED_PEER (u4Context, u4Indx);
            return SNMP_SUCCESS;
        }
        else if ((u1PeerASFound == BGP4_FALSE) &&
                 (BGP4_CONFED_PEER (u4Context, u4Indx) >
                  u4FsbgpAscConfedPeerASNo))
        {
            /* Input index Confed Peer AS number not present. Returning
             * the next higher index. */
            *pu4NextFsbgpAscConfedPeerASNo =
                BGP4_CONFED_PEER (u4Context, u4Indx);
            return SNMP_SUCCESS;
        }
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tNo Confed Peer is present.\n");
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgpAscConfedPeerStatus
 Input       :  The Indices
                FsbgpAscConfedPeerASNo

                The Object 
                retValFsbgpAscConfedPeerStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgpAscConfedPeerStatus (UINT4 u4FsbgpAscConfedPeerASNo,
                                INT4 *pi4RetValFsbgpAscConfedPeerStatus)
{
    UINT4               u4Indx;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    if (u4FsbgpAscConfedPeerASNo == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tInvalid Input Confed Peer AS number.\n");
        return SNMP_FAILURE;
    }

    for (u4Indx = 0; u4Indx < BGP4_CONFED_MAX_MEMBER_AS; u4Indx++)
    {
        if (BGP4_CONFED_PEER (u4Context, u4Indx) == u4FsbgpAscConfedPeerASNo)
        {
            *pi4RetValFsbgpAscConfedPeerStatus = BGP4_CONFED_PEER_AS_ENABLE;
            return SNMP_SUCCESS;
        }
    }
    *pi4RetValFsbgpAscConfedPeerStatus = BGP4_CONFED_PEER_AS_DISABLE;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgpAscConfedPeerStatus
 Input       :  The Indices
                FsbgpAscConfedPeerASNo

                The Object 
                setValFsbgpAscConfedPeerStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgpAscConfedPeerStatus (UINT4 u4FsbgpAscConfedPeerASNo,
                                INT4 i4SetValFsbgpAscConfedPeerStatus)
{
    tBgp4PeerEntry     *pPeer = NULL;
    UINT4               u4Indx = 0;
    UINT4               u4TempIndx = 0;

    UINT4               u4Context;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    /* if confed peer is to be enabled */
    /* The CONFED Peer AS number are to be stored in the ascesdening
     * order. */
    if (i4SetValFsbgpAscConfedPeerStatus == BGP4_CONFED_PEER_AS_ENABLE)
    {
        /* Confed Peer AS number to be enabled. Check if peer AS already
         * present. If already present, then just return. */
        for (u4Indx = 0; ((u4Indx < BGP4_CONFED_MAX_MEMBER_AS) &&
                          (BGP4_CONFED_PEER (u4Context, u4Indx) !=
                           BGP4_INV_AS)); u4Indx++)
        {
            if (BGP4_CONFED_PEER (u4Context, u4Indx) ==
                u4FsbgpAscConfedPeerASNo)
            {
                /* Confed Peer AS already enabled. */
                return SNMP_SUCCESS;
            }
            else if (BGP4_CONFED_PEER (u4Context, u4Indx) >
                     u4FsbgpAscConfedPeerASNo)
            {
                /* Current Index Confed AS number is greater than given Confed
                 * AS. Input Confed AS needs to be added at this index. Need to
                 * shift all other index to right. */
                break;
            }
            else
            {
                /* Current Index Confed AS less than input Confed AS. */
                continue;
            }
        }
        if (u4Indx == BGP4_CONFED_MAX_MEMBER_AS)
        {
            /* Already Maximum Confed AS configured. Cant configure new
             * Confed AS. */
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tError - Exceeding Maximum Configurable Confed Peer "
                      "AS\n");
            return SNMP_FAILURE;
        }
        if (BGP4_CONFED_PEER (u4Context, u4Indx) == BGP4_INV_AS)
        {
            /* Current Index is empty. Store the current AS in this index. */
            BGP4_CONFED_PEER (u4Context, u4Indx) = u4FsbgpAscConfedPeerASNo;
        }
        else
        {
            /* Right Shift all the remaining AS and store the input Confed
             * AS at this index. */
            for (u4TempIndx = BGP4_CONFED_MAX_MEMBER_AS - BGP4_DECREMENT_BY_ONE;
                 u4TempIndx > u4Indx; u4TempIndx--)
            {
                BGP4_CONFED_PEER (u4Context, u4TempIndx) =
                    BGP4_CONFED_PEER (u4Context,
                                      u4TempIndx - BGP4_DECREMENT_BY_ONE);
            }
            /* Store the input Confed AS to the current Index */
            BGP4_CONFED_PEER (u4Context, u4Indx) = u4FsbgpAscConfedPeerASNo;
        }

        /* Mark the peers belongs to this AS as Confed peer, if 
         * Confed Id is already configured */
        if (BGP4_CONFED_ID (u4Context) == BGP4_INV_AS)
        {
            RM_GET_SEQ_NUM (&u4SeqNum);
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgpAscConfedPeerStatus,
                                  u4SeqNum, FALSE, BgpLock, BgpUnLock, 2,
                                  SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i",
                              gpBgpCurrCxtNode->u4ContextId,
                              u4FsbgpAscConfedPeerASNo,
                              i4SetValFsbgpAscConfedPeerStatus));
            return SNMP_SUCCESS;
        }
        TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
        {
            if (BGP4_PEER_ASNO (pPeer) == BGP4_CONFED_PEER (u4Context, u4Indx))
            {
                BGP4_CONFED_PEER_STATUS (pPeer) = BGP4_TRUE;
            }
        }
        RM_GET_SEQ_NUM (&u4SeqNum);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgpAscConfedPeerStatus,
                              u4SeqNum, FALSE, BgpLock, BgpUnLock, 2,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i",
                          gpBgpCurrCxtNode->u4ContextId,
                          u4FsbgpAscConfedPeerASNo,
                          i4SetValFsbgpAscConfedPeerStatus));
        return SNMP_SUCCESS;
    }
    else
    {
        /* Confed Peer AS number to be disabled. Check if peer AS already
         * present. If not present, then just return. Else Clear the
         * peer AS, reset the Confed peers, and re-arrange the Confed
         * AS structure. */
        for (u4Indx = 0; ((u4Indx < BGP4_CONFED_MAX_MEMBER_AS) &&
                          (BGP4_CONFED_PEER (u4Context, u4Indx) !=
                           BGP4_INV_AS)); u4Indx++)
        {
            if (BGP4_CONFED_PEER (u4Context, u4Indx) ==
                u4FsbgpAscConfedPeerASNo)
            {
                /* Mark the peers belongs to this AS as Non-Confed peer */
                TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer,
                              tBgp4PeerEntry *)
                {
                    if (BGP4_PEER_ASNO (pPeer) ==
                        BGP4_CONFED_PEER (u4Context, u4Indx))
                    {
                        BGP4_CONFED_PEER_STATUS (pPeer) = BGP4_FALSE;
                    }
                }

                BGP4_CONFED_PEER (u4Context, u4Indx) = BGP4_INV_AS;

                /* Swift all the remining AS value to left. */
                for (u4TempIndx = u4Indx + BGP4_INCREMENT_BY_ONE;
                     ((u4TempIndx < BGP4_CONFED_MAX_MEMBER_AS) &&
                      (BGP4_CONFED_PEER (u4Context, u4TempIndx) !=
                       BGP4_INV_AS)); u4TempIndx++)
                {
                    BGP4_CONFED_PEER (u4Context,
                                      u4TempIndx - BGP4_DECREMENT_BY_ONE) =
                        BGP4_CONFED_PEER (u4Context, u4TempIndx);
                }
                /* Reset the last entry because all the entries are
                 * shifting left. */
                BGP4_CONFED_PEER (u4Context,
                                  u4TempIndx - BGP4_DECREMENT_BY_ONE) =
                    BGP4_INV_AS;
                RM_GET_SEQ_NUM (&u4SeqNum);
                SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                                      FsMIBgpAscConfedPeerStatus, u4SeqNum,
                                      FALSE, BgpLock, BgpUnLock, 2,
                                      SNMP_SUCCESS);
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i",
                                  gpBgpCurrCxtNode->u4ContextId,
                                  u4FsbgpAscConfedPeerASNo,
                                  i4SetValFsbgpAscConfedPeerStatus));
                return SNMP_SUCCESS;
            }
        }
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tError - Unable Reset Confed Peer Status.\n");
        return SNMP_FAILURE;
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsbgpAscConfedPeerStatus
 Input       :  The Indices
                FsbgpAscConfedPeerASNo

                The Object 
                testValFsbgpAscConfedPeerStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsbgpAscConfedPeerStatus (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsbgpAscConfedPeerASNo,
                                   INT4 i4TestValFsbgpAscConfedPeerStatus)
{
    UINT4               u4Context = 0;
    UINT4               i4FsbgpAscConfedPeerStatus =
        BGP4_CONFED_PEER_AS_DISABLE;
    UINT4               u4Index = 0;
    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }

    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Local AS number not configured.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }
    /* Check whether already maximum number of Confed AS is configured
     * or not. If already maximum Confed AS is configured, reject 
     * further configuration. */
    if (BGP4_CONFED_PEER
        (u4Context,
         BGP4_CONFED_MAX_MEMBER_AS - BGP4_DECREMENT_BY_ONE) != BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tError - Exceeding Maximum Configurable Confed Peer AS\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_EXC_MAX_CONFED_PEER_AS_ERR);
        return SNMP_FAILURE;
    }

    if ((u4FsbgpAscConfedPeerASNo > BGP4_MAX_AS) ||
        (BGP4_LOCAL_AS_NO (u4Context) == u4FsbgpAscConfedPeerASNo))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tInvalid Confed Peer AS number.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_CONFED_PEER_AS_ERR);
        return SNMP_FAILURE;
    }

    /* check if input is own AS number, or Confed Id */
    if ((BGP4_LOCAL_AS_NO (u4Context) == u4FsbgpAscConfedPeerASNo) ||
        (BGP4_CONFED_ID (u4Context) == u4FsbgpAscConfedPeerASNo))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tInvalid Confed Peer AS number.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_CONFED_PEER_AS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsbgpAscConfedPeerStatus != BGP4_CONFED_PEER_AS_ENABLE) &&
        (i4TestValFsbgpAscConfedPeerStatus != BGP4_CONFED_PEER_AS_DISABLE))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tInvalid Confed Peer Status.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_CONFED_PEER_STATUS_ERR);
        return SNMP_FAILURE;
    }

    for (u4Index = 0; u4Index < BGP4_CONFED_MAX_MEMBER_AS; u4Index++)
    {
        if (BGP4_CONFED_PEER (u4Context, u4Index) == u4FsbgpAscConfedPeerASNo)
        {
            i4FsbgpAscConfedPeerStatus = BGP4_CONFED_PEER_AS_ENABLE;
            break;
        }
    }

    /* Confederation Peer is already Disabled */
    if ((i4FsbgpAscConfedPeerStatus == BGP4_CONFED_PEER_AS_DISABLE &&
         i4TestValFsbgpAscConfedPeerStatus == BGP4_CONFED_PEER_AS_DISABLE))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tConfed Peer AS number not configured.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsbgpAscConfedPeerTable
 Input       :  The Indices
                FsbgpAscConfedPeerASNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsbgpAscConfedPeerTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
