/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4evpn.c,v 1.13 2017/09/15 06:19:53 siva Exp $
 *
 * Description:
 ********************************************************************/
#ifndef BGEVPNFIL_C
#define BGEVPNFIL_C

#include "bgp4com.h"

#ifdef EVPN_WANTED
#include "bgevpndfs.h"
/*************************************************************************/
/*  Function Name   : Bgp4RegisterWithVxlan                              */
/*  Input(s)        : None.                                              */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE                                    */
/*  Description     : Registers with Vxlan for                           */
/*                    1. RouteParams addition/deletion                   */
/*                    2. VRF status Handling                             */
/*************************************************************************/
INT4
Bgp4RegisterWithVxlan (VOID)
{
    tEvpnBgpRegEntry    EvpnRegInfo;

    MEMSET ((UINT1 *) &EvpnRegInfo, 0, sizeof (tEvpnBgpRegEntry));
    EvpnRegInfo.pBgp4EvpnNotifyRouteParams = BgpVxlanRouteParamsNotification;
    EvpnRegInfo.pBgp4EvpnNotifyAdminStatusChange
        = BgpVxlanVrfAdminStatusChangeNotification;
    EvpnRegInfo.pBgp4EvpnNotifyMACUpdate = Bgp4VxlanEvpnNotifyMac;
    EvpnRegInfo.pBgp4EvpnGetMACNotification = Bgp4VxlanEvpnGetMACNotification;
    EvpnRegInfo.pBgp4EvpnNotifyESIUpdate = Bgp4VxlanEvpnESINotification;
    EvpnRegInfo.pBgp4EvpnNotifyADRoute = Bgp4VxlanEvpnEthADNotification;

    EvpnRegInfo.u2InfoMask |=
        (EVPN_BGP4_ROUTE_PARAMS_REQ | EVPN_BGP4_VRF_ADMIN_STATUS_REQ
         | EVPN_BGP4_NOTIFY_MAC_UPDT | EVPN_BGP4_GET_MAC_NOTIF |
         EVPN_BGP4_NOTIFY_ESI | EVPN_BGP4_NOTIFY_ETH_AD);
    if (BGP4_SUCCESS == EvpnRegisterCallback (&EvpnRegInfo))
    {
        return BGP4_SUCCESS;
    }
    return BGP4_FAILURE;
}

/*************************************************************************/
/*  Function Name   : Bgp4DeRegisterWithVxlan                            */
/*  Input(s)        : None.                                              */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE                                    */
/*  Description     : DeRegisters with Vxlan for                         */
/*                    1. RouteParams addition/deletion                   */
/*                    2. VRF status Handling                             */
/*************************************************************************/
INT4
Bgp4DeRegisterWithVxlan (VOID)
{
    tEvpnBgpRegEntry    EvpnRegInfo;

    MEMSET ((UINT1 *) &EvpnRegInfo, 0, sizeof (tEvpnBgpRegEntry));
    EvpnRegInfo.u2InfoMask |=
        (EVPN_BGP4_ROUTE_PARAMS_REQ | EVPN_BGP4_VRF_ADMIN_STATUS_REQ
         | EVPN_BGP4_NOTIFY_MAC_UPDT | EVPN_BGP4_GET_MAC_NOTIF
         | EVPN_BGP4_NOTIFY_ESI | EVPN_BGP4_NOTIFY_ETH_AD);

    if (OSIX_SUCCESS == EvpnDeRegisterCallback (&EvpnRegInfo))
    {
        return BGP4_SUCCESS;
    }
    return BGP4_FAILURE;
}

/********************************************************************/
/* Function Name   : Bgp4EvpnRouteTargetChgHandler                  */
/* Description     : If the import route target is added/deleted    */
/*                   route-refresh message will be sent to PE peers */
/*                   else if the export route target is             */
/*                   added/deleted then this policy is applied on   */
/*                   the routes store in the corresponding VRF      */
/*                   and the best routes will be advertised to PEs  */
/* Input(s)        : u4VrfId  - vrf info                            */
/*                   i4RTType - route target type                   */
/*                   pu1ExtComm - route target value                */
/*                   u1RTType - route target type (import/export)   */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : Route Target pointer                 */
/********************************************************************/
INT4
Bgp4EvpnRouteTargetChgHandler (UINT4 u4VrfId, UINT4 u4VNId, INT4 i4RTType,
                               UINT1 u1OperType, UINT1 *pu1RouteParam)
{
    tTMO_SLL           *pRTComList = NULL;
    tExtCommProfile    *pExtRTCom = NULL;
    tEvpnVrfNode        EvpnVrfNode;
    tEvpnVrfNode       *pEvpnVrfNode = NULL;
    UINT4               u4State = 0;
    UINT1               au1RDtemp[BGP4_EVPN_ROUTE_DISTING_SIZE];
    UINT1              *pu1RDtemp = au1RDtemp;
    INT4                i4RetVal = BGP4_FAILURE;

    if (gBgpCxtNode[u4VrfId] == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\tBGP is not enabled for the Context!! \n");
        return BGP4_SUCCESS;
    }

    MEMSET (&EvpnVrfNode, 0, sizeof (tEvpnVrfNode));
    MEMSET (&au1RDtemp, 0, sizeof (UINT1));

    EvpnVrfNode.u4VnId = u4VNId;
    pEvpnVrfNode =
        RBTreeGet (gBgpCxtNode[u4VrfId]->EvpnRoutes,
                   (tBgp4RBElem *) & EvpnVrfNode);

    if (i4RTType == EVPN_BGP4_RD)
    {
        if (pEvpnVrfNode != NULL)
        {
            MEMSET (pEvpnVrfNode->au1RouteDistinguisher, 0,
                    BGP4_EVPN_ROUTE_DISTING_SIZE);
            if (u1OperType == EVPN_BGP4_RD_ADD)
            {
                MEMCPY (pu1RDtemp, pu1RouteParam, BGP4_EVPN_ROUTE_DISTING_SIZE);
                MEMCPY (pEvpnVrfNode->au1RouteDistinguisher, pu1RDtemp,
                        BGP4_EVPN_ROUTE_DISTING_SIZE);
            }
            else
            {
                if (MEMCMP (pEvpnVrfNode->au1RouteDistinguisher, pu1RDtemp,
                            BGP4_EVPN_ROUTE_DISTING_SIZE) == 0)
                {
                    MEMSET (pEvpnVrfNode->au1RouteDistinguisher, 0,
                            BGP4_EVPN_ROUTE_DISTING_SIZE);
                }
            }
        }
        return BGP4_SUCCESS;
    }
    else
    {
        if ((i4RTType == EVPN_BGP4_BOTH_RT) && (u1OperType == EVPN_BGP4_RT_ADD))
        {
            if (pEvpnVrfNode != NULL)
            {
                pRTComList = &(pEvpnVrfNode->ImportTargets);
                if (Bgp4EvpnAddRT
                    (pRTComList, pu1RouteParam,
                     EVPN_BGP4_IMPORT_RT) == BGP4_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG,
                              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                              BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC,
                              BGP4_MOD_NAME,
                              "\tAddition of Import Route Target Failed!! \n");
                    return BGP4_FAILURE;
                }

                pRTComList = &(pEvpnVrfNode->ExportTargets);
                if (Bgp4EvpnAddRT
                    (pRTComList, pu1RouteParam,
                     EVPN_BGP4_EXPORT_RT) == BGP4_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG,
                              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                              BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC,
                              BGP4_MOD_NAME,
                              "\tAddition of Export Route Target Failed!! \n");
                    return BGP4_FAILURE;
                }
            }

        }
        else
        {
            if (pEvpnVrfNode != NULL)
            {
                if (u1OperType == EVPN_BGP4_RT_ADD)
                {
                    pRTComList = (i4RTType == EVPN_BGP4_IMPORT_RT) ?
                        &(pEvpnVrfNode->ImportTargets) :
                        &(pEvpnVrfNode->ExportTargets);
                    if (Bgp4EvpnAddRT (pRTComList, pu1RouteParam, i4RTType) ==
                        BGP4_FAILURE)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC,
                                  BGP4_MOD_NAME,
                                  "\tAddtion of Import/Export Route Target Failed !! \n");
                        return BGP4_FAILURE;
                    }

                }
            }
        }
        if ((i4RTType == EVPN_BGP4_IMPORT_RT) ||
            (i4RTType == EVPN_BGP4_BOTH_RT))
        {
            /* Operation is for import route targets.
             * If any import route target is added or deleted, we should send
             * route refresh message to PE peers.
             */
            if (pEvpnVrfNode != NULL)
            {
                if (u1OperType == EVPN_BGP4_RT_DEL)
                {
                    pExtRTCom =
                        Bgp4EvpnGetRouteTargetFromVrf (u4VrfId, u4VNId,
                                                       pu1RouteParam,
                                                       (UINT1)
                                                       EVPN_BGP4_IMPORT_RT);
                    if (pExtRTCom != NULL)
                    {
                        TMO_SLL_Delete (&(pEvpnVrfNode->ImportTargets),
                                        (tTMO_SLL_NODE *) pExtRTCom);
                        EXT_COMM_PROFILE_FREE (pExtRTCom);
                    }
                }
                Bgp4GetVrfVniCurrentState (u4VrfId, u4VNId, &u4State);
                if (u4State == BGP4_EVPN_VRF_UP)
                {
                    Bgp4EvpnSendRtRefreshToPEPeers ();
                }
            }
        }
        if ((i4RTType == EVPN_BGP4_EXPORT_RT) ||
            (i4RTType == EVPN_BGP4_BOTH_RT))
        {
            /* Operation is for export route targets. Apply this new policy
             * to the routes present in this vrf and advertise to PE peers.
             */
            if (pEvpnVrfNode != NULL)
            {
                if (u1OperType == EVPN_BGP4_RT_DEL)
                {
                    pExtRTCom =
                        Bgp4EvpnGetRouteTargetFromVrf (u4VrfId, u4VNId,
                                                       pu1RouteParam,
                                                       (UINT1)
                                                       EVPN_BGP4_EXPORT_RT);
                    if (pExtRTCom != NULL)
                    {
                        TMO_SLL_Delete (&(pEvpnVrfNode->ExportTargets),
                                        (tTMO_SLL_NODE *) pExtRTCom);
                        EXT_COMM_PROFILE_FREE (pExtRTCom);
                    }
                }
            }
            if (u1OperType == EVPN_BGP4_RT_ADD)
            {
                /* If the VRF is operationally up, then process this change */
                Bgp4GetVrfVniCurrentState (u4VrfId, u4VNId, &u4State);
                if (u4State == BGP4_EVPN_VRF_UP)
                {
                    if (BGP4_EVPN_AFI_FLAG == BGP4_TRUE)
                    {
                        Bgp4SendEvpnRtsToPeer (u4VrfId, u4VNId);
                    }
                }
            }
            /* indicate whether export targets are configured are not */
            if (pEvpnVrfNode != NULL)
            {
                if (TMO_SLL_Count (&(pEvpnVrfNode->ExportTargets)) == 0)
                {
                    BGP4_EVPN_VRF_SPEC_SET_FLAG (u4VrfId,
                                                 BGP4_EVPN_VRF_NO_EXP_TARGETS);
                    Bgp4EvpnVrfDeInit (u4VrfId, u4VNId, u4State);
                    BGP4_EVPN_VRF_SPEC_RESET_FLAG (u4VrfId,
                                                   BGP4_EVPN_VRF_NO_EXP_TARGETS);
                }
            }
        }
    }
    UNUSED_PARAM (i4RetVal);
    return BGP4_SUCCESS;
}

INT4
Bgp4EvpnAddRT (tTMO_SLL * pRTComList, UINT1 *pu1RouteParam, INT4 i4RTType)
{
    tExtCommProfile    *pExtRTCom = NULL;
    EXT_COMM_PROFILE_CREATE (pExtRTCom);
    if (pExtRTCom == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t BGP External Community Node Creation Failed !! \n");
        return BGP4_FAILURE;
    }
    TMO_SLL_Init_Node (&(pExtRTCom->ExtCommProfileNext));
    MEMCPY (pExtRTCom->au1ExtComm, pu1RouteParam, EXT_COMM_VALUE_LEN);
    pExtRTCom->au1ExtComm[1] = 2;
    if (i4RTType == EVPN_BGP4_EXPORT_RT)
    {
        BGP4_EVPN_EXT_COMM_ROW_STATUS (pExtRTCom) = ACTIVE;
    }
    TMO_SLL_Add (pRTComList, (tTMO_SLL_NODE *) pExtRTCom);
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4EvpnGetRouteTargetFromVrf                  */
/* Description     : Gets route target pointer from a give vrf      */
/* Input(s)        : u4VrfId  - vrf id                              */
/*                   u4VNId   - VN Id                               */
/*                   pu1ExtComm - route target value                */
/*                   u1RTType - route target type (import/export)   */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : Route Target pointer                 */
/********************************************************************/
tExtCommProfile    *
Bgp4EvpnGetRouteTargetFromVrf (UINT4 u4VrfId, UINT4 u4VNId, UINT1 *pu1ExtComm,
                               UINT1 u1RTType)
{
    tTMO_SLL           *pRTComList = NULL;
    tExtCommProfile    *pExtRTCom = NULL;
    tEvpnVrfNode        EvpnVrfNode;
    tEvpnVrfNode       *pEvpnVrfNode = NULL;

    MEMSET (&EvpnVrfNode, 0, sizeof (tEvpnVrfNode));

    EvpnVrfNode.u4VnId = u4VNId;
    pEvpnVrfNode =
        RBTreeGet (gBgpCxtNode[u4VrfId]->EvpnRoutes, (tRBElem *) & EvpnVrfNode);
    if (pEvpnVrfNode != NULL)
    {
        pRTComList = (u1RTType == EVPN_BGP4_IMPORT_RT) ?
            &(pEvpnVrfNode->ImportTargets) : &(pEvpnVrfNode->ExportTargets);

        TMO_SLL_Scan (pRTComList, pExtRTCom, tExtCommProfile *)
        {
            if ((MEMCMP (pExtRTCom->au1ExtComm,
                         pu1ExtComm, EXT_COMM_VALUE_LEN)) == 0)
            {
                return (pExtRTCom);
            }
        }
    }
    return (NULL);
}

/********************************************************************/
/* Function Name   : Bgp4EvpnSendRtRefreshToPEPeers                 */
/* Description     : Posts event to BGP task to send route-refresh  */
/*                   message to PE peers                            */
/* Input(s)        : None.                                          */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4EvpnSendRtRefreshToPEPeers ()
{
    tBgp4QMsg          *pQMsg = NULL;
    tAddrPrefix         PeerAddress;

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting BGP Route Refresh "
                  "Msg to BGP Queue FAILED!!!\n");
        return BGP4_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&(PeerAddress), BGP4_INET_AFI_IPV4);
    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_SNMP_INBOUND_SOFTCONFIG_RTREF_EVENT;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
    Bgp4CopyAddrPrefixStruct (&(BGP4_INPUTQ_RTREF_PEER_ADDR_INFO (pQMsg)),
                              PeerAddress);
    BGP4_INPUTQ_RTREF_ASAFI_MASK (pQMsg) = CAP_MP_L2VPN_EVPN;
    BGP4_INPUTQ_RTREF_OPER_TYPE (pQMsg) = BGP4_RR_ADVT_EVPN_PEERS;
    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t BGP Enqueue of Queue Message Failed !! \n");
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4EvpnVrfChangeHandler                       */
/* Description     : Handles the VRF notifications from VXLAN       */
/* Input(s)        : u4VrfId - vrf id                               */
/*                 : u4VNId  - VN id                                */
/*                   u1VrfState - new state of the VRF              */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4EvpnVrfChangeHandler (UINT4 u4VrfId, UINT4 u4VNId, UINT1 u1VrfState)
{
    INT4                i4RetVal = BGP4_SUCCESS;
    UINT4               u4State = 0;

    if (gBgpCxtNode[u4VrfId] == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t BGP is not enabled for this Context !! \n");
        return BGP4_SUCCESS;
    }

    switch (u1VrfState)
    {
        case BGP4_EVPN_VRF_CREATE:
            i4RetVal = Bgp4EvpnVrfCreate (u4VrfId, u4VNId);
            break;
        case BGP4_EVPN_VRF_UP:
            i4RetVal = Bgp4GetVrfVniCurrentState (u4VrfId, u4VNId, &u4State);
            if (i4RetVal == BGP4_FAILURE)
            {
                i4RetVal = Bgp4EvpnVrfCreate (u4VrfId, u4VNId);
            }
            i4RetVal = Bgp4EvpnVrfUpHdlr (u4VrfId, u4VNId, u1VrfState);
            break;
        case BGP4_EVPN_VRF_DOWN:
            i4RetVal = Bgp4GetVrfVniCurrentState (u4VrfId, u4VNId, &u4State);
            if (i4RetVal == BGP4_SUCCESS)
            {
                i4RetVal = Bgp4EvpnVrfDownHdlr (u4VrfId, u4VNId, u1VrfState);
            }
            else
            {
                i4RetVal = Bgp4EvpnVrfCreate (u4VrfId, u4VNId);
                if (i4RetVal == BGP4_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG,
                              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                              BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC,
                              BGP4_MOD_NAME,
                              "\t VRF Creation Failed for VRF %d!! \n",
                              u4VrfId);
                    return BGP4_FAILURE;
                }
                /* make the current state down */
                i4RetVal =
                    Bgp4SetVrfVniCurrentState (u4VrfId, u4VNId,
                                               BGP4_EVPN_VRF_DOWN);
            }
            break;
        case BGP4_EVPN_VRF_DELETE:
            Bgp4EvpnVrfDownHdlr (u4VrfId, u4VNId, u1VrfState);
            break;
        default:
            /* for any other event received from IP-EVPN return failure */
            return BGP4_FAILURE;
    }
    return i4RetVal;
}

/********************************************************************/
/* Function Name : Bgp4EvpnNotifyMacHandler                         */
/* Description   : Handles the Mac Address notifications from VXLAN */
/* Input(s)      : u4VrfId - vrf id                                 */
/*               : u4VNId  - VN id                                  */
/*                 u1VrfState - new state of the VRF                */
/* Output(s)     : None.                                            */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4EvpnNotifyMacHandler (UINT4 u4VrfId, UINT4 u4VnId, tMacAddr MacAddress,
                          UINT1 u1AddrLen, UINT1 *pau1Addr, UINT1 u1Action,
                          UINT1 u1EntryStatus)
{
    tBgp4Info          *pBGP4Info = NULL;
    INT4                i4RetValue = BGP4_FAILURE;
    tRouteProfile      *pRtprofile = NULL;
    tEvpnNlriInfo       EvpnNlri;
    tEvpnVrfNode        EvpnVrfNode;
    tEvpnVrfNode       *pEvpnVrfNode = NULL;
    tRouteProfile      *pTempRtProfile = NULL;
    VOID               *pRibNode = NULL;
    tRouteProfile      *pRtProfileList = NULL;
    INT4                i4Status = BGP4_FAILURE;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT4               u4MacSeqNumExist = 0;
    UINT4               u4MacSeqNum = 0;
    tExtCommunity      *pExtComm = NULL;
    UINT2               u2EcommType = 0;
    UINT4               u4Index = 0;
    tBgp4AppTimer      *pBgpTmr = NULL;
    tBgpMacDup         *pBgpMacDup = NULL;
    tBgpMacDup          BgpMacDupDel;
    tBgpMacDup         *pBgpMacDupDel = NULL;
    UINT4               u4BgpId = 0;
    UINT4               u4Context = 0;
    BOOL1               bIsMacRtAvailable = BGP4_FALSE;
    BOOL1               bStaticMacExtFlag = BGP4_FALSE;    /* Check for Static MAC */

    MEMSET (&EvpnVrfNode, 0, sizeof (tEvpnVrfNode));
    MEMSET (&EvpnNlri, 0, sizeof (tEvpnNlriInfo));
    MEMSET (au1ExtComm, 0, EXT_COMM_VALUE_LEN);
    MEMSET (&BgpMacDupDel, 0, sizeof (tBgpMacDup));
    EvpnVrfNode.u4VnId = u4VnId;
    pEvpnVrfNode =
        RBTreeGet (gBgpCxtNode[u4VrfId]->EvpnRoutes, (tRBElem *) & EvpnVrfNode);
    if ((pEvpnVrfNode == NULL)
        || (pEvpnVrfNode->u1EvpnStatus != BGP4_EVPN_VRF_UP))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t VRF Node is NULL/Not Active for this Context!! \n");
        return BGP4_SUCCESS;
    }
    pBGP4Info = Bgp4MemAllocateBgp4info (sizeof (tBgp4Info));
    if (pBGP4Info == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t%s:%d Memory Allocation for Route's BGP-Info FAILED!!!\n",
                  __func__, __LINE__);
        return BGP4_FAILURE;
    }

    BGP4_INFO_REF_COUNT (pBGP4Info)++;

    /* Update Mac Address in NLRI */
    MEMCPY (EvpnNlri.MacAddress, MacAddress, sizeof (tMacAddr));
    MEMCPY (EvpnNlri.au1RouteDistinguisher,
            &(pEvpnVrfNode->au1RouteDistinguisher), MAX_LEN_RD_VALUE);
    EvpnNlri.u4VnId = pEvpnVrfNode->u4VnId;
    MEMCPY (EvpnNlri.au1EthernetSegmentID,
            &(pEvpnVrfNode->au1EthSegId), MAX_LEN_ETH_SEG_ID);
    EvpnNlri.u1RouteType = EVPN_MAC_ROUTE;
    MEMCPY (&EvpnNlri.MacAddress, MacAddress, sizeof (tMacAddr));
    EvpnNlri.u1IpAddrLen = u1AddrLen;
    if (u1AddrLen == BGP4_EVPN_IP_ADDR_SIZE)
    {
        MEMCPY (EvpnNlri.au1IpAddr, pau1Addr, BGP4_EVPN_IP_ADDR_SIZE);
    }
    else if (u1AddrLen == BGP4_EVPN_IP6_ADDR_SIZE)
    {
        MEMCPY (EvpnNlri.au1IpAddr, pau1Addr, BGP4_EVPN_IP6_ADDR_SIZE);
    }

    /* create route profile Information here */
    pRtprofile = Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
    if (pRtprofile == NULL)
    {
        Bgp4DshReleaseBgpInfo (pBGP4Info);
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t BGP Memory Allocation for Route Profile Failed!! \n");
        return BGP4_FAILURE;
    }

    MEMSET (pRtprofile, 0, sizeof (tRouteProfile));
    if (u1Action == BGP4_UPDATE_MAC)
    {
        /* Check if this route is alreay learnt in bgp RIB
         * as a bgp-protocol route */
        u4Context = u4VrfId;
        Bgp4GetRibNode (u4Context, CAP_MP_L2VPN_EVPN, &pRibNode);
        if (pRibNode != NULL)
        {
            i4Status = Bgp4RibhGetFirstEntry (CAP_MP_L2VPN_EVPN,
                                              &pRtProfileList, &pRibNode, TRUE);
            if (i4Status == BGP4_SUCCESS)
            {
                for (;;)
                {
                    pTempRtProfile = pRtProfileList;
                    u4Context = BGP4_RT_CXT_ID (pTempRtProfile);
                    while (pTempRtProfile)
                    {
                        if (BGP4_RT_SAFI_INFO (pTempRtProfile) ==
                            BGP4_INET_SAFI_EVPN)
                        {
                            if ((BGP4_EVPN_VNID (pTempRtProfile) == u4VnId) &&
                                (MEMCMP (MacAddress,
                                         BGP4_EVPN_MACADDR (pTempRtProfile),
                                         sizeof (tMacAddr)) == 0))
                            {
                                bIsMacRtAvailable = BGP4_TRUE;
                                break;
                            }
                        }
                        pTempRtProfile = BGP4_RT_NEXT (pTempRtProfile);
                    }
                    if (bIsMacRtAvailable == BGP4_TRUE)
                    {
                        break;
                    }
                    i4Status = Bgp4RibhGetNextEntry (pRtProfileList, u4Context,
                                                     &pRtProfileList,
                                                     &pRibNode, TRUE);
                    if (i4Status == BGP4_FAILURE)
                    {
                        break;
                    }
                }
            }
        }
        if (bIsMacRtAvailable == BGP4_TRUE)
        {
            u4BgpId = OSIX_HTONL (BGP4_LOCAL_BGP_ID (BGP4_RT_CXT_ID
                                                     (pTempRtProfile)));
        }
        if ((bIsMacRtAvailable == BGP4_TRUE) && (u1AddrLen == 0))
        {
            if ((BGP4_RT_PROTOCOL (pTempRtProfile) == LOCAL_ID) &&
                (u1EntryStatus != EVPN_BGP4_STATIC_MAC))
            {
                Bgp4DshReleaseBgpInfo (pBGP4Info);
                Bgp4MemReleaseRouteProfile (pRtprofile);
                return BGP4_SUCCESS;
            }

            /* Check for Static MAC: BGP already learnt route for this MAC
             * as static and now Got connected MAC */
            if (BGP4_EVPN_MAC_TYPE (pTempRtProfile) == EVPN_BGP4_STATIC_MAC)
            {
                /* Check for Static MAC: If BGP learnt is static and 
                 * connected for the same MAC is also static, Lower IP
                 * should get the preference */
                if ((u1EntryStatus == EVPN_BGP4_STATIC_MAC)
                    &&
                    (MEMCMP
                     (&u4BgpId, pTempRtProfile->pRtInfo->NextHopInfo.au1Address,
                      sizeof (UINT4)) < 0))
                {
                    bStaticMacExtFlag = BGP4_TRUE;
                }
                else
                {
                    /* No Action required because existing route is preferred */
                    return BGP4_SUCCESS;
                }
            }
            else
            {
                /* Check for Static MAC: BGP already learnt route for this MAC
                 * as dynamic and now Got connected MAC as static */
                if (u1EntryStatus == EVPN_BGP4_STATIC_MAC)
                {
                    bStaticMacExtFlag = BGP4_TRUE;
                }
            }
            /* BGP already learnt route for this MAC, and now got 
             * Connected MAC route from EVPN */
            /* Retrieve the MAC-Mobility sequence number from the
             * existing route if it is non-zero number*/
            if (BGP4_INFO_ECOMM_ATTR (pTempRtProfile->pRtInfo) != NULL)
            {
                for (u4Index = 0; u4Index < BGP4_INFO_ECOMM_COUNT
                     (pTempRtProfile->pRtInfo); u4Index++)
                {
                    MEMCPY (au1ExtComm,
                            (BGP4_INFO_ECOMM_ATTR_VAL
                             (pTempRtProfile->pRtInfo) +
                             +(u4Index * EXT_COMM_VALUE_LEN)),
                            EXT_COMM_VALUE_LEN);
                    PTR_FETCH2 (u2EcommType, au1ExtComm);

                    if (BGP4_IS_MAC_MOB_EXT_COMMUNITY (u2EcommType) ==
                        BGP4_TRUE)
                    {
                        MEMCPY (&u4MacSeqNumExist, &(au1ExtComm[4]),
                                sizeof (UINT4));
                        u4MacSeqNumExist = OSIX_HTONL (u4MacSeqNumExist);
                        break;
                    }
                }
            }
            /* If the MAC entry is Duplicate */
            if (BGP4_EVPN_MAC_DUPLICATE (pTempRtProfile) == BGP4_TRUE)
            {
                u4BgpId = OSIX_HTONL (BGP4_LOCAL_BGP_ID (BGP4_RT_CXT_ID
                                                         (pTempRtProfile)));
                /* if bgp-IP is bigger than peer-IP, delete the new entry
                 * from VLAN Mac Table.*/
                /* if peer-IP is smaller, then MAC to be connected with
                 * peer node only, So delete the entry from MAC table */
                if ((BGP4_RT_PROTOCOL (pTempRtProfile) == BGP_ID) &&
                    (MEMCMP (&u4BgpId,
                             pTempRtProfile->pRtInfo->NextHopInfo.au1Address,
                             sizeof (UINT4)) > 0))
                {
                    Bgp4DshReleaseBgpInfo (pBGP4Info);
                    Bgp4MemReleaseRouteProfile (pRtprofile);
                    return BGP4_SUCCESS;

                }
                else if (BGP4_RT_PROTOCOL (pTempRtProfile) == LOCAL_ID)
                {
                    /* delete Mac entry from mac-address-table */
                    Bgp4DshReleaseBgpInfo (pBGP4Info);
                    Bgp4MemReleaseRouteProfile (pRtprofile);
                    return BGP4_SUCCESS;
                }
                /* else if bgp-IP is smaller, then MAC to be connected
                 * with this node only, So follow the procedure to delete
                 * the bgp-learnt route and sending this MAC update to Peer */
                else
                {
                    EvpnNlri.bIsMacDuplicate = BGP4_TRUE;
                }
            }

            /* Delete the BGP learnt route from bgp-RIB and add the
             * new connected route in BGP-RIB*/

            BGP4_RT_REF_COUNT (pTempRtProfile)++;
            BGP4_RT_SET_FLAG (pTempRtProfile, BGP4_RT_WITHDRAWN);
            Bgp4EvpnCreateWithdMsgforVXLAN (pTempRtProfile);
            Bgp4AddWithdRouteToIntPeerAdvtList (pTempRtProfile);
            Bgp4RibhDelRtEntry (pTempRtProfile, pRibNode, u4VrfId, BGP4_FALSE);
            Bgp4DshReleaseRtInfo (pTempRtProfile);

            /* check for the Mac entry in MAC-SeqNum list */
            /*  if MAC entry found in bgpcxtnode's data increment the
             *  sequence number and apply in route profile.
             *  else apply sequence number as 1 in route profile. */
            if (bStaticMacExtFlag != BGP4_TRUE)
            {
                MEMSET (au1ExtComm, 0, EXT_COMM_VALUE_LEN);
                au1ExtComm[0] = 6;
                u4MacSeqNum = u4MacSeqNumExist + 1;
                u4MacSeqNum = OSIX_HTONL (u4MacSeqNum);
                MEMCPY (&(au1ExtComm[4]), &u4MacSeqNum, sizeof (UINT4));
                if (Bgp4EvpnCreatMacMobCommunity
                    (au1ExtComm, pExtComm, pBGP4Info) == BGP4_FAILURE)
                {
                    Bgp4DshReleaseBgpInfo (pBGP4Info);
                    Bgp4MemReleaseRouteProfile (pRtprofile);
                    return BGP4_FAILURE;
                }
            }
        }
        else
        {
            /* Check for Static MAC: MAC is newly connected as static */
            if (u1EntryStatus == EVPN_BGP4_STATIC_MAC)
            {
                bStaticMacExtFlag = BGP4_TRUE;
            }
        }

        /* if seq number is non-zero, then start Mac-Mobility Timer */
        if ((OSIX_HTONL (u4MacSeqNum) >= 1) && (bStaticMacExtFlag != BGP4_TRUE))
        {
            BgpMacDupDel.u4VnId = u4VnId;
            MEMCPY (BgpMacDupDel.MacAddress, MacAddress, sizeof (tMacAddr));
            pBgpMacDupDel = RBTreeGet
                (gBgpCxtNode[u4VrfId]->BgpMacDupTimer,
                 (tRBElem *) & BgpMacDupDel);
            if ((pBgpMacDupDel == NULL) ||
                ((pBgpMacDupDel != NULL) && (u4MacSeqNumExist == 0)))
            {
                if (pBgpMacDupDel != NULL)
                {
                    pBgpMacDupDel->tMacMobDupTmr.u4Flag = BGP4_INACTIVE;
                    pBgpTmr = &(pBgpMacDupDel->tMacMobDupTmr);
                    pBgpTmr->Tmr.u4Data = (FS_ULONG) ((VOID *) pBgpMacDupDel);;
                    BGP4_TIMER_STOP (BGP4_TIMER_LISTID, &pBgpTmr->Tmr);
                    RBTreeRem (gBgpCxtNode[u4Context]->BgpMacDupTimer,
                               (tRBElem *) pBgpMacDupDel);
                    MemReleaseMemBlock (BGP4_MAC_DUP_TIMER_MEM_POOL_ID,
                                        (UINT1 *) pBgpMacDupDel);
                    pBgpTmr = NULL;
                }

                pBgpMacDup = (tBgpMacDup *)
                    MemAllocMemBlk (BGP4_MAC_DUP_TIMER_MEM_POOL_ID);
                if (pBgpMacDup == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG,
                              BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                              BGP4_EVPN_TRC, BGP4_MOD_NAME,
                              "\tMemory Allocation for Bgp Mac Duplication Timer node FAILED!!!\n");
                    Bgp4DshReleaseBgpInfo (pBGP4Info);
                    Bgp4MemReleaseRouteProfile (pRtprofile);
                    return BGP4_FAILURE;
                }
                MEMSET (pBgpMacDup, 0, sizeof (tBgpMacDup));
                MEMSET (&(pBgpMacDup->tMacMobDupTmr), 0,
                        sizeof (tBgp4AppTimer));
                pBgpMacDup->tMacMobDupTmr.u4TimerId = BGP4_MAC_MOB_DUP_TIMER;
                pBgpMacDup->u4VrfId = u4VrfId;
                pBgpMacDup->u4VnId = u4VnId;
                MEMCPY (pBgpMacDup->MacAddress, MacAddress, sizeof (tMacAddr));
                pBgpTmr = &(pBgpMacDup->tMacMobDupTmr);
                pBgpMacDup->tMacMobDupTmr.u4Flag = BGP4_ACTIVE;
                pBgpTmr->Tmr.u4Data = (FS_ULONG) ((VOID *) pBgpMacDup);

                if (RBTreeAdd (gBgpCxtNode[u4VrfId]->BgpMacDupTimer,
                               (tRBElem *) pBgpMacDup) == RB_FAILURE)
                {
                    MemReleaseMemBlock (BGP4_MAC_DUP_TIMER_MEM_POOL_ID,
                                        (UINT1 *) pBgpMacDup);
                    BGP4_TRC (NULL, BGP4_TRC_FLAG,
                              BGP4_ALL_FAILURE_TRC | BGP4_EVPN_TRC,
                              BGP4_MOD_NAME,
                              "\tRBTree Addition FAILED for MAC Timer entry!!!\n");
                    Bgp4DshReleaseBgpInfo (pBGP4Info);
                    Bgp4MemReleaseRouteProfile (pRtprofile);
                    return BGP4_FAILURE;
                }
                BGP4_TIMER_START (BGP4_TIMER_LISTID, &pBgpTmr->Tmr,
                                  BGP4_GLB_MAC_DUP_TIMER_INTERVAL);
            }
        }
    }
    /* Check for Static MAC: If connected MAC is STATIC MAC, MAC Mobility
     * extended community with static flag set and seq. no '0' should be
     * formed */
    if ((u1EntryStatus == EVPN_BGP4_STATIC_MAC) &&
        (bStaticMacExtFlag == BGP4_TRUE))
    {
        EvpnNlri.u1MacType = u1EntryStatus;
        MEMSET (au1ExtComm, 0, EXT_COMM_VALUE_LEN);

        au1ExtComm[0] = 6;
        au1ExtComm[2] = u1EntryStatus;

        if (Bgp4EvpnCreatMacMobCommunity (au1ExtComm, pExtComm, pBGP4Info)
            == BGP4_FAILURE)
        {
            Bgp4DshReleaseBgpInfo (pBGP4Info);
            Bgp4MemReleaseRouteProfile (pRtprofile);
            return BGP4_FAILURE;
        }
        /* Stop the Dup Timer if already running */
        MEMSET (&BgpMacDupDel, 0, sizeof (tBgpMacDup));
        BgpMacDupDel.u4VnId = u4VnId;
        MEMCPY (BgpMacDupDel.MacAddress, MacAddress, sizeof (tMacAddr));
        pBgpMacDupDel = NULL;
        pBgpMacDupDel = RBTreeGet (gBgpCxtNode[u4Context]->BgpMacDupTimer,
                                   (tRBElem *) & BgpMacDupDel);
        if (pBgpMacDupDel != NULL)
        {
            pBgpMacDupDel->tMacMobDupTmr.u4Flag = BGP4_INACTIVE;
            pBgpTmr = &(pBgpMacDupDel->tMacMobDupTmr);
            pBgpTmr->Tmr.u4Data = (FS_ULONG) ((VOID *) pBgpMacDupDel);;
            BGP4_TIMER_STOP (BGP4_TIMER_LISTID, &pBgpTmr->Tmr);
            RBTreeRem (gBgpCxtNode[u4Context]->BgpMacDupTimer,
                       (tRBElem *) pBgpMacDupDel);
            MemReleaseMemBlock (BGP4_MAC_DUP_TIMER_MEM_POOL_ID,
                                (UINT1 *) pBgpMacDupDel);
            pBgpTmr = NULL;
        }
    }
    i4RetValue = Bgp4EvpnConstructRouteProfile (u4VrfId, pRtprofile, &EvpnNlri);
    if (i4RetValue == BGP4_FAILURE)
    {
        Bgp4DshReleaseBgpInfo (pBGP4Info);
        Bgp4MemReleaseRouteProfile (pRtprofile);
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t BGP Construction Route Profile Failed!! \n");
        return BGP4_FAILURE;
    }

    BGP4_LINK_INFO_TO_PROFILE (pBGP4Info, pRtprofile);

    /* Needs to add the route to REDISTRIBUTION update list. */
    BGP4_RT_SET_FLAG (pRtprofile, BGP4_RT_IN_REDIST_LIST);

    if (u1Action == BGP4_REMOVE_MAC)
    {
        BGP4_RT_SET_FLAG (pRtprofile, BGP4_RT_WITHDRAWN);
    }
    if (BGP4_FAILURE ==
        Bgp4DshAddRouteToList (BGP4_REDIST_LIST (u4VrfId), pRtprofile, 0))
    {
        Bgp4DshReleaseBgpInfo (pBGP4Info);
        Bgp4MemReleaseRouteProfile (pRtprofile);
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t Addition of Route Profile to Redistribution List Failed!! \n");
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name : Bgp4EvpnESINotification                          */
/* Description   : Handles the Ethernet Segment ID notifications    */
/*                 from VXLAN                                       */
/* Input(s)      : u4VrfId - vrf id                                 */
/*                 u4VNId  - VN id                                  */
/*                 pu1EthSegId - Ethernet Segment Identifier        */
/*                 pu1IpAddr - VTEP Ip Address                      */
/*                 i4IpAddrLen - VTEP IP Address Length             */
/*                 u1Action - Add / Delete                          */
/* Output(s)     : None.                                            */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4EvpnESINotification (UINT4 u4VrfId, UINT4 u4VnId, UINT1 *pu1EthSegId,
                         UINT1 *pu1IpAddr, INT4 i4IpAddrLen, UINT1 u1Action)
{
    tBgp4Info          *pBGP4Info = NULL;
    INT4                i4RetValue = BGP4_FAILURE;
    tRouteProfile      *pRtprofile = NULL;
    tEvpnNlriInfo       EvpnNlri;
    tEvpnVrfNode        EvpnVrfNode;
    tEvpnVrfNode       *pEvpnVrfNode = NULL;
    UINT1               au1ESIdRt[BGP4_EVPN_ESI_RT_LEN];
    UINT1              *pu1ESIdRt = NULL;
    INT4                i4Flag = BGP4_FAILURE;
    MEMSET (&EvpnVrfNode, 0, sizeof (tEvpnVrfNode));
    MEMSET (&EvpnNlri, 0, sizeof (tEvpnNlriInfo));
    MEMSET (au1ESIdRt, 0, BGP4_EVPN_ESI_RT_LEN);
    EvpnVrfNode.u4VnId = u4VnId;
    pEvpnVrfNode =
        RBTreeGet (gBgpCxtNode[u4VrfId]->EvpnRoutes, (tRBElem *) & EvpnVrfNode);
    if ((pEvpnVrfNode == NULL)
        || (pEvpnVrfNode->u1EvpnStatus != BGP4_EVPN_VRF_UP))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t VRF Node is NULL/Not Active for this Context!! \n");
        return BGP4_SUCCESS;
    }
    pBGP4Info = Bgp4MemAllocateBgp4info (sizeof (tBgp4Info));
    if (pBGP4Info == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                  BGP4_MOD_NAME,
                  "\t%s:%d Memory Allocation for Route's BGP-Info FAILED!!!\n",
                  __func__, __LINE__);
        return BGP4_FAILURE;
    }

    BGP4_INFO_REF_COUNT (pBGP4Info)++;

    /* Update ES Id in NLRI */
    MEMCPY (EvpnNlri.au1EthernetSegmentID, pu1EthSegId, MAX_LEN_ETH_SEG_ID);
    /* Copy ES Id to the Vrf Node */
    MEMCPY (&(pEvpnVrfNode->au1EthSegId), pu1EthSegId, MAX_LEN_ETH_SEG_ID);
    MEMCPY (EvpnNlri.au1RouteDistinguisher,
            &(pEvpnVrfNode->au1RouteDistinguisher), MAX_LEN_RD_VALUE);
    EvpnNlri.u4VnId = 0;
    EvpnNlri.u1RouteType = EVPN_ETH_SEGMENT_ROUTE;
    EvpnNlri.u1IpAddrLen = (UINT1) i4IpAddrLen;
    /* Copy IpAddress Len to Vrf Node */
    pEvpnVrfNode->u1IpAddrLen = (UINT1) i4IpAddrLen;
    if (i4IpAddrLen == BGP4_EVPN_IP_ADDR_SIZE)
    {
        MEMCPY (EvpnNlri.au1IpAddr, pu1IpAddr, BGP4_EVPN_IP_ADDR_SIZE);
        /* Copy Ip address to the Vrf Node */
        MEMCPY (&(pEvpnVrfNode->au1IpAddr), pu1IpAddr, BGP4_EVPN_IP_ADDR_SIZE);
    }
    else if (i4IpAddrLen == BGP4_EVPN_IP6_ADDR_SIZE)
    {
        MEMCPY (EvpnNlri.au1IpAddr, pu1IpAddr, BGP4_EVPN_IP6_ADDR_SIZE);
        /* Copy IPv6 address to the Vrf Node */
        MEMCPY (&(pEvpnVrfNode->au1IpAddr), pu1IpAddr, BGP4_EVPN_IP6_ADDR_SIZE);
    }

    /* Fill Ethernet Segment RT */
    MEMCPY (&au1ESIdRt[2], &pu1EthSegId[1], MAX_LEN_MAC_ADDR);
    pu1ESIdRt = au1ESIdRt;
    pu1ESIdRt[0] = ETH_SEG_RT_TYPE;
    pu1ESIdRt[1] = ETH_SEG_RT_SUBTYPE;
    MEMCPY (pEvpnVrfNode->au1EthSegRT, pu1ESIdRt, MAX_LEN_RD_VALUE);

    /* Route-Refresh is sent to check if any ESI is received previously,
     * which could have been missed */
    Bgp4EvpnSendRtRefreshToPEPeers ();

    /* create route profile Information here */
    pRtprofile = Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
    if (pRtprofile == NULL)
    {
        Bgp4DshReleaseBgpInfo (pBGP4Info);
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t BGP Memory Allocation for Route Profile Failed!! \n");
        return BGP4_FAILURE;
    }

    MEMSET (pRtprofile, 0, sizeof (tRouteProfile));

    i4RetValue = Bgp4EvpnConstructRouteProfile (u4VrfId, pRtprofile, &EvpnNlri);
    if (i4RetValue == BGP4_FAILURE)
    {
        Bgp4DshReleaseBgpInfo (pBGP4Info);
        Bgp4MemReleaseRouteProfile (pRtprofile);
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t BGP Construction of Route Profile Failed!! \n");
        return BGP4_FAILURE;
    }
    BGP4_LINK_INFO_TO_PROFILE (pBGP4Info, pRtprofile);

    if (u1Action == BGP4_REMOVE_ESID)
    {
        /* Remove the MAC learnt via this ESI, before the ESI is withdrawn */
        MEMSET (&(pEvpnVrfNode->au1EthSegId), 0, MAX_LEN_ETH_SEG_ID);
        pEvpnVrfNode->u1IpAddrLen = 0;
        MEMSET (&(pEvpnVrfNode->au1IpAddr), 0, BGP4_EVPN_IP6_ADDR_SIZE);
        MEMSET (&(pEvpnVrfNode->au1EthSegRT), 0, BGP4_EVPN_ESI_RT_LEN);
        Bgp4DelMacAdRtFromESI (pRtprofile);
        /* Withdraw for ES Route is recieved, Trigger AD Withdraw
         * The Flag is set to 2 */
        i4Flag = AD_WITHDRAWN;
        i4RetValue = Bgp4EvpnEthADNotification (u4VrfId, u4VnId, pu1EthSegId,
                                                i4Flag);
        if (i4RetValue == BGP4_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                      BGP4_MOD_NAME,
                      "\t%s:%d EVPN AD Notification has FAILED !!!\n",
                      __func__, __LINE__);
            Bgp4DshReleaseBgpInfo (pBGP4Info);
            Bgp4MemReleaseRouteProfile (pRtprofile);
            return BGP4_FAILURE;
        }
        BGP4_RT_SET_FLAG (pRtprofile, BGP4_RT_WITHDRAWN);
    }
    else
    {
        /* Needs to add the route to REDISTRIBUTION update list. */
        BGP4_RT_SET_FLAG (pRtprofile, BGP4_RT_IN_REDIST_LIST);
    }
    if (BGP4_FAILURE ==
        Bgp4DshAddRouteToList (BGP4_REDIST_LIST (u4VrfId), pRtprofile, 0))
    {
        Bgp4DshReleaseBgpInfo (pBGP4Info);
        Bgp4MemReleaseRouteProfile (pRtprofile);
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t Addition of Route Profile to Redistribution List Failed!! \n");
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name : Bgp4EvpnEthADNotification                        */
/* Description   : Handles the Ethernet Auto Discovery route        */
/*                 notifications from VXLAN                         */
/* Input(s)      : u4VrfId - vrf id                                 */
/*                 u4VNId  - VN id                                  */
/*                 pu1EthSegId - Ethernet Segment Identifier        */
/*                 u1Flag - ( 0 - All Active / 1 - Single Active )  */
/* Output(s)     : None.                                            */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4EvpnEthADNotification (UINT4 u4VrfId, UINT4 u4VnId, UINT1 *pu1EthSegId,
                           INT4 i4Flag)
{
    INT4                i4RetValue = BGP4_FAILURE;
    tRouteProfile      *pRtprofile = NULL;
    tEvpnNlriInfo       EvpnNlri;
    tEvpnVrfNode        EvpnVrfNode;
    tEvpnVrfNode       *pEvpnVrfNode = NULL;
    UINT1              *pu1ESAdRt = NULL;
    UINT1               au1ESAdRt[BGP4_EVPN_ESI_RT_LEN];
    tBgp4Info          *pBGP4Info = NULL;

    MEMSET (&EvpnVrfNode, 0, sizeof (tEvpnVrfNode));
    MEMSET (&EvpnNlri, 0, sizeof (tEvpnNlriInfo));
    MEMSET (au1ESAdRt, 0, BGP4_EVPN_ESI_RT_LEN);

    EvpnVrfNode.u4VnId = u4VnId;
    pEvpnVrfNode =
        RBTreeGet (gBgpCxtNode[u4VrfId]->EvpnRoutes, (tRBElem *) & EvpnVrfNode);
    if ((pEvpnVrfNode == NULL)
        || (pEvpnVrfNode->u1EvpnStatus != BGP4_EVPN_VRF_UP))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t VRF Node is NULL/Not Active for this Context!! \n");
        return BGP4_SUCCESS;
    }

    MEMCPY (EvpnNlri.au1RouteDistinguisher,
            &(pEvpnVrfNode->au1RouteDistinguisher), MAX_LEN_RD_VALUE);
    MEMCPY (EvpnNlri.au1EthernetSegmentID, pu1EthSegId, MAX_LEN_ETH_SEG_ID);
    EvpnNlri.u4VnId = pEvpnVrfNode->u4VnId;
    EvpnNlri.u1RouteType = EVPN_AD_ROUTE;

    /* Construct the ESI Extended Community */
    pu1ESAdRt = au1ESAdRt;
    pu1ESAdRt[0] = ETH_AD_RT_TYPE;
    pu1ESAdRt[1] = ETH_AD_RT_SUB_TYPE;
    pu1ESAdRt[2] = (UINT1) i4Flag;

    MEMCPY (pEvpnVrfNode->au1EthAdRT, pu1ESAdRt, MAX_LEN_RD_VALUE);

    pBGP4Info = Bgp4MemAllocateBgp4info (sizeof (tBgp4Info));
    if (pBGP4Info == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                  BGP4_MOD_NAME,
                  "\t%s:%d Memory Allocation for Route's BGP-Info FAILED!!!\n",
                  __func__, __LINE__);
        return BGP4_FAILURE;
    }

    BGP4_INFO_REF_COUNT (pBGP4Info)++;

    /* create route profile Information here */
    pRtprofile = Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
    if (pRtprofile == NULL)
    {
        Bgp4DshReleaseBgpInfo (pBGP4Info);
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t BGP Memory Allocation for Route Profile Failed!! \n");
        return BGP4_FAILURE;
    }

    MEMSET (pRtprofile, 0, sizeof (tRouteProfile));

    i4RetValue = Bgp4EvpnConstructRouteProfile (u4VrfId, pRtprofile, &EvpnNlri);
    if (i4RetValue == BGP4_FAILURE)
    {
        Bgp4DshReleaseBgpInfo (pBGP4Info);
        Bgp4MemReleaseRouteProfile (pRtprofile);
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t BGP Construction of Route Profile Failed!! \n");
        return BGP4_FAILURE;
    }
    BGP4_LINK_INFO_TO_PROFILE (pBGP4Info, pRtprofile);

    if (i4Flag == AD_WITHDRAWN)
    {
        MEMSET (&(pEvpnVrfNode->au1EthSegId), 0, MAX_LEN_ETH_SEG_ID);
        MEMSET (&(pEvpnVrfNode->au1EthAdRT), 0, BGP4_EVPN_ESI_RT_LEN);
        /* Ethernet Tag ID should be set to MAX-ET */
        BGP4_EVPN_ETHTAG (pRtprofile) = BGP_MAX_ETH_RT_VALUE;
        BGP4_RT_SET_FLAG (pRtprofile, BGP4_RT_WITHDRAWN);
    }
    else
    {
        /* Needs to add the route to REDISTRIBUTION update list. */
        BGP4_RT_SET_FLAG (pRtprofile, BGP4_RT_IN_REDIST_LIST);
    }
    if (BGP4_FAILURE ==
        Bgp4DshAddRouteToList (BGP4_REDIST_LIST (u4VrfId), pRtprofile, 0))
    {
        Bgp4DshReleaseBgpInfo (pBGP4Info);
        Bgp4MemReleaseRouteProfile (pRtprofile);
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t Addition of Route Profile to Redistribution List Failed!! \n");
        return BGP4_FAILURE;
    }

    return i4RetValue;
}

/*****************************************************************************/
/* FUNCTION NAME : Bgp4DelMacAdRtFromESI                                     */
/* DESCRIPTION   : This functions deletes the MAC and the AD route learnt    */
/*                 through this ESI, and ESI Delete indication is given      */
/* INPUTS        : pRtprofile: Pointer to the route profile structue         */
/*                 pu1EthSegId : Pointer to te Eth Segment ID                */
/* OUTPUTS       : None                                                      */
/* Return        : BGP4_TRUE/BGP4_FALSE                                      */
/*****************************************************************************/
INT4
Bgp4DelMacAdRtFromESI (tRouteProfile * pRouteprofile)
{
    VOID               *pRibNode = NULL;
    tRouteProfile      *pNextRt = NULL;
    tRouteProfile      *pNextRoute = NULL;
    tRouteProfile      *pRtprofile = NULL;
    INT4                i4Sts = BGP4_FAILURE;
    INT4                i4Ret = BGP4_FAILURE;
    INT4                i4DelRtRet = BGP4_FAILURE;

    pRibNode = BGP4_EVPN_RIB_TREE (pRouteprofile->u4VrfId);
    i4Sts = Bgp4RibhGetFirstEntry (CAP_MP_L2VPN_EVPN, &pRtprofile,
                                   &pRibNode, TRUE);
    if (i4Sts == BGP4_SUCCESS)
    {
        for (;;)
        {
            i4Ret = Bgp4RibhGetNextEntry (pRtprofile, pRouteprofile->u4VrfId,
                                          &pNextRoute, &pRibNode, TRUE);
            pNextRt = pRtprofile;
            while (pNextRt != NULL)
            {
                pRtprofile = pNextRt;
                if ((pRtprofile->EvpnNlriInfo.u1RouteType ==
                     EVPN_ETH_SEGMENT_ROUTE)
                    || (pRtprofile->EvpnNlriInfo.u1RouteType == EVPN_AD_ROUTE))
                {
                    pNextRt = BGP4_RT_NEXT (pRtprofile);
                    continue;
                }
                else if ((MEMCMP (pRtprofile->EvpnNlriInfo.au1EthernetSegmentID,
                                  pRouteprofile->EvpnNlriInfo.
                                  au1EthernetSegmentID,
                                  MAX_LEN_ETH_SEG_ID) != 0)
                         && (BGP4_RT_PROTOCOL (pRtprofile) != BGP4_LOCAL_ID))
                {
                    pNextRt = BGP4_RT_NEXT (pRtprofile);
                    continue;
                }
                BGP4_RT_REF_COUNT (pRtprofile)++;
                pNextRt = BGP4_RT_NEXT (pRtprofile);
                i4DelRtRet = Bgp4RibhDelRtEntry (pRtprofile,
                                                 pRibNode,
                                                 pRouteprofile->u4VrfId,
                                                 BGP4_TRUE);

                /* Increment the processed route count. */
                Bgp4DshReleaseRtInfo (pRtprofile);
            }
            if (i4Ret == BGP4_FAILURE)
            {
                i4Sts = BGP4_FALSE;
                break;
            }
            pRtprofile = pNextRoute;
            pNextRoute = NULL;
        }
    }
    UNUSED_PARAM (i4DelRtRet);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME : Bgp4ProcessMacAddressWithdrawRoute                        */
/* DESCRIPTION   : This function handles the deletion of the MAC routes      */
/*                 learnt through the peer, when ESI deletion is processed   */
/* INPUTS        : pRtprofile: Pointer to the route profile structue         */
/*                 pu1EthSegId : Pointer to te Eth Segment ID                */
/* OUTPUTS       : None                                                      */
/* Return        : BGP4_TRUE/BGP4_FALSE                                      */
/*****************************************************************************/
INT4
Bgp4ProcessMacAddressWithdrawRoute (tRouteProfile * pRouteprofile)
{
    VOID               *pRibNode = NULL;
    tRouteProfile      *pNextRt = NULL;
    tRouteProfile      *pNextRoute = NULL;
    tRouteProfile      *pRtProfile = NULL;
    INT4                i4Sts = BGP4_FAILURE;
    INT4                i4Ret = BGP4_FAILURE;

    pRibNode = BGP4_EVPN_RIB_TREE (pRouteprofile->u4VrfId);
    i4Sts = Bgp4RibhGetFirstEntry (CAP_MP_L2VPN_EVPN, &pRtProfile,
                                   &pRibNode, TRUE);
    if (i4Sts == BGP4_SUCCESS)
    {
        for (;;)
        {
            i4Ret = Bgp4RibhGetNextEntry (pRtProfile, pRouteprofile->u4VrfId,
                                          &pNextRoute, &pRibNode, TRUE);
            pNextRt = pRtProfile;
            while (pNextRt != NULL)
            {
                pRtProfile = pNextRt;
                if (pRtProfile->EvpnNlriInfo.u1RouteType != EVPN_MAC_ROUTE)
                {
                    pNextRt = BGP4_RT_NEXT (pRtProfile);
                    continue;
                }
                else if ((MEMCMP (pRtProfile->EvpnNlriInfo.au1EthernetSegmentID,
                                  pRouteprofile->EvpnNlriInfo.
                                  au1EthernetSegmentID,
                                  MAX_LEN_ETH_SEG_ID) != 0)
                         || (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID))
                {
                    pNextRt = BGP4_RT_NEXT (pRtProfile);
                    continue;
                }

                BGP4_RT_REF_COUNT (pRtProfile)++;
                pNextRt = BGP4_RT_NEXT (pRtProfile);
                /* Set the Withdrawn flag and update the route in FIB */
                BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);
                Bgp4IgphUpdateRouteInFIB (pRtProfile);
                Bgp4DshReleaseRtInfo (pRtProfile);
            }
            if (i4Ret == BGP4_FAILURE)
            {
                i4Sts = BGP4_FALSE;
                break;
            }

            pRtProfile = pNextRoute;
            pNextRoute = NULL;
        }
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME : Bgp4EvpnConstructRouteProfile                             */
/* DESCRIPTION   : This function fills the route profile for the EVPN route  */
/*                 profile                                                   */
/* INPUTS        : pRtprofile: Pointer to the route profile structue         */
/*                 EvpnNlri: Pointer to EVPN Nlri Structure                  */
/* OUTPUTS       : None                                                      */
/* Return        : BGP4_TRUE/BGP4_FALSE                                      */
/*****************************************************************************/
INT4
Bgp4EvpnConstructRouteProfile (UINT4 u4VrfId,
                               tRouteProfile * pRtprofile,
                               tEvpnNlriInfo * EvpnNlri)
{
    tBgpCxtNode        *pBgpCxtNode = NULL;

    pBgpCxtNode = Bgp4GetContextEntry (u4VrfId);

    if (NULL == pBgpCxtNode)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t BGP is not anabled for this Context!! \n");
        return BGP4_FAILURE;
    }

    pRtprofile->pBgpCxtNode = pBgpCxtNode;

    BGP4_RT_PROTOCOL (pRtprofile) = BGP4_LOCAL_ID;
    BGP4_RT_AFI_INFO (pRtprofile) = BGP4_INET_AFI_L2VPN;
    BGP4_RT_SAFI_INFO (pRtprofile) = BGP4_INET_SAFI_EVPN;

    if (EvpnNlri->u1RouteType == EVPN_MAC_ROUTE)
    {
        pRtprofile->EvpnNlriInfo.u1RouteType = EVPN_MAC_ROUTE;
        pRtprofile->EvpnNlriInfo.u1MacAddrLen = BGP4_EVPN_MAC_ADDR_SIZE;
        BGP4_EVPN_VRFID (pRtprofile) = u4VrfId;
        BGP4_EVPN_RT_VN_ID (pRtprofile) = EvpnNlri->u4VnId;
        MEMCPY (BGP4_EVPN_RT_ROUTE_DISTING (pRtprofile),
                EvpnNlri->au1RouteDistinguisher, MAX_LEN_RD_VALUE);
        MEMCPY (pRtprofile->EvpnNlriInfo.MacAddress, EvpnNlri->MacAddress,
                sizeof (tMacAddr));
        MEMCPY (BGP4_EVPN_RT_ETH_SEG_ID (pRtprofile),
                EvpnNlri->au1EthernetSegmentID, MAX_LEN_ETH_SEG_ID);

        BGP4_EVPN_MAC_DUPLICATE (pRtprofile) = EvpnNlri->bIsMacDuplicate;
        BGP4_EVPN_IPADDR_LEN (pRtprofile) = EvpnNlri->u1IpAddrLen;
        BGP4_EVPN_MAC_TYPE (pRtprofile) = EvpnNlri->u1MacType;
        if (EvpnNlri->u1IpAddrLen != 0)
        {
            if (EvpnNlri->u1IpAddrLen == BGP4_EVPN_IP_ADDR_SIZE)
            {
                MEMCPY (&BGP4_EVPN_IPADDR (pRtprofile), &EvpnNlri->au1IpAddr,
                        BGP4_EVPN_IP_ADDR_SIZE);
                BGP4_EVPN_IPADDR_LEN (pRtprofile) =
                    (UINT1) (EvpnNlri->u1IpAddrLen * BGP4_ONE_BYTE_BITS);
                BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO
                    (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                     (BGP4_RT_NET_ADDRESS_INFO (pRtprofile))) =
                    BGP4_EVPN_MAC_NLRI_PREFIX_LEN + BGP4_EVPN_IP_ADDR_SIZE;
            }
            else if (EvpnNlri->u1IpAddrLen == BGP4_EVPN_IP6_ADDR_SIZE)
            {
                MEMCPY (&BGP4_EVPN_IPADDR (pRtprofile), &EvpnNlri->au1IpAddr,
                        BGP4_EVPN_IP6_ADDR_SIZE);
                BGP4_EVPN_IPADDR_LEN (pRtprofile) =
                    (UINT1) (EvpnNlri->u1IpAddrLen * BGP4_ONE_BYTE_BITS);
                BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO
                    (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                     (BGP4_RT_NET_ADDRESS_INFO (pRtprofile))) =
                    BGP4_EVPN_MAC_NLRI_PREFIX_LEN + BGP4_EVPN_IP6_ADDR_SIZE;
            }
        }
    }
    else if (EvpnNlri->u1RouteType == EVPN_ETH_SEGMENT_ROUTE)
    {
        pRtprofile->EvpnNlriInfo.u1RouteType = EVPN_ETH_SEGMENT_ROUTE;
        BGP4_EVPN_VRFID (pRtprofile) = u4VrfId;
        BGP4_EVPN_RT_VN_ID (pRtprofile) = EvpnNlri->u4VnId;
        MEMCPY (BGP4_EVPN_RT_ROUTE_DISTING (pRtprofile),
                EvpnNlri->au1RouteDistinguisher, MAX_LEN_RD_VALUE);
        MEMCPY (BGP4_EVPN_RT_ETH_SEG_ID (pRtprofile),
                EvpnNlri->au1EthernetSegmentID, MAX_LEN_ETH_SEG_ID);
        if (EvpnNlri->u1IpAddrLen != 0)
        {
            if (EvpnNlri->u1IpAddrLen == BGP4_EVPN_IP_ADDR_SIZE)
            {
                MEMCPY (&BGP4_EVPN_IPADDR (pRtprofile), &EvpnNlri->au1IpAddr,
                        BGP4_EVPN_IP_ADDR_SIZE);
                BGP4_EVPN_IPADDR_LEN (pRtprofile) =
                    (UINT1) (EvpnNlri->u1IpAddrLen * BGP4_ONE_BYTE_BITS);
                BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO
                    (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                     (BGP4_RT_NET_ADDRESS_INFO (pRtprofile))) =
                    BGP4_EVPN_ETH_SEG_NLRI_PREFIX_LEN + BGP4_EVPN_IP_ADDR_SIZE;
            }
            else if (EvpnNlri->u1IpAddrLen == BGP4_EVPN_IP6_ADDR_SIZE)
            {
                MEMCPY (&BGP4_EVPN_IPADDR (pRtprofile), &EvpnNlri->au1IpAddr,
                        BGP4_EVPN_IP6_ADDR_SIZE);
                BGP4_EVPN_IPADDR_LEN (pRtprofile) =
                    (UINT1) (EvpnNlri->u1IpAddrLen * BGP4_ONE_BYTE_BITS);
                BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO
                    (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                     (BGP4_RT_NET_ADDRESS_INFO (pRtprofile))) =
                    BGP4_EVPN_ETH_SEG_NLRI_PREFIX_LEN + BGP4_EVPN_IP6_ADDR_SIZE;
            }
        }
    }
    else if (EvpnNlri->u1RouteType == EVPN_AD_ROUTE)
    {
        pRtprofile->EvpnNlriInfo.u1RouteType = EVPN_AD_ROUTE;
        BGP4_EVPN_VRFID (pRtprofile) = u4VrfId;
        BGP4_EVPN_RT_VN_ID (pRtprofile) = EvpnNlri->u4VnId;
        MEMCPY (BGP4_EVPN_RT_ROUTE_DISTING (pRtprofile),
                EvpnNlri->au1RouteDistinguisher, MAX_LEN_RD_VALUE);
        MEMCPY (BGP4_EVPN_RT_ETH_SEG_ID (pRtprofile),
                EvpnNlri->au1EthernetSegmentID, MAX_LEN_ETH_SEG_ID);
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4EvpnVrfCreate                              */
/* Description     : Creates a new VRF                              */
/* Input(s)        : u4VrfId - vrf identifier.                      */
/*                 : u4VNId  - VN id                                */
/* Output(s)       : ppVrfInfo - address of vrf pointer             */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : None.                                */
/********************************************************************/
INT4
Bgp4EvpnVrfCreate (UINT4 u4VrfId, UINT4 u4VNId)
{
    tEvpnVrfNode        EvpnVrfNode;
    tEvpnVrfNode       *pEvpnVrfNode = NULL;
    INT4                i4RetVal = BGP4_FAILURE;

    MEMSET (&EvpnVrfNode, 0, sizeof (tEvpnVrfNode));
    EvpnVrfNode.u4VnId = u4VNId;
    pEvpnVrfNode =
        RBTreeGet (gBgpCxtNode[u4VrfId]->EvpnRoutes, (tRBElem *) & EvpnVrfNode);
    if (pEvpnVrfNode == NULL)
    {
        pEvpnVrfNode =
            (tEvpnVrfNode *) MemAllocMemBlk (BGP4_EVPN_VRF_VNI_MEM_POOL_ID);
        MEMSET (pEvpnVrfNode, 0, sizeof (tEvpnVrfNode));
        pEvpnVrfNode->u4VnId = u4VNId;
        TMO_SLL_Init (&(pEvpnVrfNode->ImportTargets));
        TMO_SLL_Init (&(pEvpnVrfNode->ExportTargets));
        BGP4_EVPN_VRF_SPEC_GET_FLAG (u4VrfId) = 0;
        if (RBTreeAdd (gBgpCxtNode[u4VrfId]->EvpnRoutes,
                       (tRBElem *) pEvpnVrfNode) == RB_FAILURE)
        {
            MemReleaseMemBlock (BGP4_EVPN_VRF_VNI_MEM_POOL_ID,
                                (UINT1 *) pEvpnVrfNode);
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                      "\t RBTree Addition of VRF Node Failed!! \n");
            return BGP4_FAILURE;
        }
        i4RetVal =
            Bgp4SetVrfVniCurrentState (u4VrfId, u4VNId, BGP4_EVPN_VRF_CREATE);
    }
    UNUSED_PARAM (i4RetVal);
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4GetVrfVniCurrentState                      */
/* Description     : Creates a new VRF                              */
/* Input(s)        : u4VrfId - vrf identifier.                      */
/*                 : u4VNId  - VN id                                */
/* Output(s)       : u4State - Current VNI State                    */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : None.                                */
/********************************************************************/
INT4
Bgp4GetVrfVniCurrentState (UINT4 u4VrfId, UINT4 u4VNId, UINT4 *pu4State)
{
    tEvpnVrfNode        EvpnVrfNode;
    tEvpnVrfNode       *pEvpnVrfNode = NULL;
    INT4                i4RetVal = BGP4_FAILURE;

    MEMSET (&EvpnVrfNode, 0, sizeof (tEvpnVrfNode));

    EvpnVrfNode.u4VnId = u4VNId;
    pEvpnVrfNode =
        RBTreeGet (gBgpCxtNode[u4VrfId]->EvpnRoutes, (tRBElem *) & EvpnVrfNode);
    if (pEvpnVrfNode != NULL)
    {
        *pu4State = (UINT4) pEvpnVrfNode->u1EvpnStatus;
        i4RetVal = BGP4_SUCCESS;
    }
    else
    {
        i4RetVal = BGP4_FAILURE;
    }

    return i4RetVal;
}

/********************************************************************/
/* Function Name   : Bgp4SetVrfVniCurrentState                      */
/* Description     : Creates a new VRF                              */
/* Input(s)        : u4VrfId - vrf identifier.                      */
/*                 : u4VNId  - VN id                                */
/* Output(s)       : u4State - Current VNI State                    */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : None.                                */
/********************************************************************/
INT4
Bgp4SetVrfVniCurrentState (UINT4 u4VrfId, UINT4 u4VNId, UINT4 u4State)
{
    tEvpnVrfNode        EvpnVrfNode;
    tEvpnVrfNode       *pEvpnVrfNode = NULL;

    MEMSET (&EvpnVrfNode, 0, sizeof (tEvpnVrfNode));

    EvpnVrfNode.u4VnId = u4VNId;
    pEvpnVrfNode =
        RBTreeGet (gBgpCxtNode[u4VrfId]->EvpnRoutes, (tRBElem *) & EvpnVrfNode);
    if (pEvpnVrfNode != NULL)
    {
        pEvpnVrfNode->u1EvpnStatus = (UINT1) u4State;
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4EvpnVrfUpHdlr                             */
/* Description     : Makes VRF up                                   */
/* Input(s)        : u4VrfId - vrf id                               */
/*                 : u4VNId - VN id                                 */
/*                   u1VrfState - the state of VRF                  */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4EvpnVrfUpHdlr (UINT4 u4VrfId, UINT4 u4VNId, UINT1 u1VrfState)
{
    UINT4               u4State = 0;
    /* case 1 : When the vrf is either in create/down, init the vrf 
     * case 2 : return success otherwise
     */
    Bgp4GetVrfVniCurrentState (u4VrfId, u4VNId, &u4State);
    if (((u4State == BGP4_EVPN_VRF_CREATE)
         || (u4State == BGP4_EVPN_VRF_DOWN))
        && (u1VrfState == BGP4_EVPN_VRF_UP))
    {
        /* case 1. */
        if (Bgp4EvpnVrfInit (u4VrfId, u4VNId) == BGP4_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                      "\t Init for VRF %d Failed!! \n", u4VrfId);
            return BGP4_FAILURE;
        }
    }

    /* case 2. */
    /* For Ethernet Segment Route, if the ES Identifier is already configured
     * and the VRF UP indication is received later, VRF Node is scaned and if
     * ES Identifier exists, ES Update Route is sent */

    Bgp4EvpnProbeandSendESUpdate (u4VrfId, u4VNId);

    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4EvpnProbeandSendESUpdate                   */
/* Description     : Checks if ES Identifier is configured for the  */
/*                   VRF. If Yes, ES Update is sent                 */
/* Input(s)        : u4VrfId - Vrf Id                               */
/*                   u4VNId  - VN Id                                */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS                         */
/********************************************************************/
VOID
Bgp4EvpnProbeandSendESUpdate (UINT4 u4VrfId, UINT4 u4VNId)
{
    tEvpnVrfNode        EvpnVrfNode;
    tEvpnVrfNode       *pEvpnVrfNode = NULL;
    UINT1               au1TmpIpAddr[BGP4_EVPN_IP6_ADDR_SIZE];
    UINT1               au1NullArray[BGP4_EVPN_IP6_ADDR_SIZE];
    BOOL1               b1Flag = BGP4_FALSE;
    INT4                i4RetVal = BGP4_FAILURE;

    MEMSET (au1TmpIpAddr, 0, BGP4_EVPN_IP6_ADDR_SIZE);
    MEMSET (au1NullArray, 0, BGP4_EVPN_IP6_ADDR_SIZE);
    MEMSET (&EvpnVrfNode, 0, sizeof (tEvpnVrfNode));

    pEvpnVrfNode = RBTreeGetFirst (gBgpCxtNode[u4VrfId]->EvpnRoutes);

    while (pEvpnVrfNode != NULL)
    {
        if (pEvpnVrfNode->u4VnId == u4VNId)
        {
            if (pEvpnVrfNode->u1IpAddrLen == BGP4_EVPN_IP_ADDR_SIZE)
            {
                MEMCPY (&au1TmpIpAddr, &pEvpnVrfNode->au1IpAddr,
                        BGP4_EVPN_IP_ADDR_SIZE);
                if (MEMCMP (au1TmpIpAddr, au1NullArray, BGP4_EVPN_IP_ADDR_SIZE)
                    != 0)
                {
                    /* Call the Ethernet Route Update Callback to send ES Update */
                    i4RetVal = Bgp4EvpnESINotification (u4VrfId, u4VNId,
                                                        pEvpnVrfNode->
                                                        au1EthSegId,
                                                        au1TmpIpAddr,
                                                        (INT4) pEvpnVrfNode->
                                                        u1IpAddrLen,
                                                        EVPN_PARAM_ESI_ADD);
                    b1Flag = BGP4_TRUE;
                }
            }
            else if (pEvpnVrfNode->u1IpAddrLen == BGP4_EVPN_IP6_ADDR_SIZE)
            {
                MEMCPY (&au1TmpIpAddr, &pEvpnVrfNode->au1IpAddr,
                        BGP4_EVPN_IP6_ADDR_SIZE);
                if (MEMCMP (au1TmpIpAddr, au1NullArray, BGP4_EVPN_IP6_ADDR_SIZE)
                    != 0)
                {
                    /* Call the Ethernet Route Update Callback to send ES Update */
                    i4RetVal = Bgp4EvpnESINotification (u4VrfId, u4VNId,
                                                        pEvpnVrfNode->
                                                        au1EthSegId,
                                                        au1TmpIpAddr,
                                                        (INT4) pEvpnVrfNode->
                                                        u1IpAddrLen,
                                                        EVPN_PARAM_ESI_ADD);
                    b1Flag = BGP4_TRUE;
                }
            }
        }
        pEvpnVrfNode = RBTreeGetNext (gBgpCxtNode[u4VrfId]->EvpnRoutes,
                                      (tBgp4RBElem *) pEvpnVrfNode, NULL);
    }

    /* Send Route Refresh Messgae to PE to get the list of updates already shared */
    if (b1Flag == BGP4_TRUE)
    {
        Bgp4EvpnSendRtRefreshToPEPeers ();
    }

    UNUSED_PARAM (i4RetVal);
    return;
}

/********************************************************************/
/* Function Name   : Bgp4EvpnVrfInit                                */
/* Description     : Initializes a particular vrf                   */
/* Input(s)        : u4VrfId - Vrf Id                               */
/*                   u4VNId  - VN Id                                */
/*                   u1VrfState - VRF State                         */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS                         */
/********************************************************************/
INT4
Bgp4EvpnVrfInit (UINT4 u4VrfId, UINT4 u4VNId)
{
    tAddrPrefix         RemAddr;
    tEvpnVrfNode        EvpnVrfNode;
    tEvpnVrfNode       *pEvpnVrfNode = NULL;
    UINT1               au1InvRD[BGP4_EVPN_ROUTE_DISTING_SIZE];
    UINT1               au1ZeroAddr[BGP4_EVPN_ESI_RT_LEN];
    INT4                i4RetVal = BGP4_FAILURE;

    MEMSET (au1InvRD, 0, BGP4_EVPN_ROUTE_DISTING_SIZE);
    MEMSET (&EvpnVrfNode, 0, sizeof (tEvpnVrfNode));
    MEMSET (au1ZeroAddr, 0, BGP4_EVPN_ESI_RT_LEN);

    EvpnVrfNode.u4VnId = u4VNId;
    pEvpnVrfNode =
        RBTreeGet (gBgpCxtNode[u4VrfId]->EvpnRoutes, (tRBElem *) & EvpnVrfNode);
    if (MEMCMP
        (pEvpnVrfNode->au1RouteDistinguisher, au1InvRD,
         BGP4_EVPN_ROUTE_DISTING_SIZE) == 0)
    {
        /* RD is not yet configured for the VR. Do not make it up */
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t Route Distinguisher is not configured for this Context!! \n");
        return BGP4_FAILURE;
    }
    /* initialization activities are over, reset the flag init-progress
     * and init-pending if any
     * or set to state to VRP UP
     */
    i4RetVal = Bgp4SetVrfVniCurrentState (u4VrfId, u4VNId, BGP4_EVPN_VRF_UP);

    /* VRF initialization activities:
     * # Enable all the CE neighbors attached to this VRF.
     * If the VRF has been already configured with import route targets,
     * then send Route-Refresh message to PE peers
     */

    if (BGP4_EVPN_AFI_FLAG == BGP4_TRUE)
    {
        Bgp4SendEvpnRtsToPeer (u4VrfId, u4VNId);
    }

    if (TMO_SLL_Count (&(pEvpnVrfNode->ImportTargets)) > 0)
    {
        Bgp4InitAddrPrefixStruct (&(RemAddr), BGP4_INET_AFI_IPV4);
        Bgp4RtRefRequestHandler (0, RemAddr, BGP4_INET_AFI_L2VPN,
                                 BGP4_INET_SAFI_EVPN, BGP4_RR_ADVT_EVPN_PEERS);
    }

    if (MEMCMP (pEvpnVrfNode->au1EthSegRT, au1ZeroAddr,
                BGP4_EVPN_ESI_RT_LEN) != 0)
    {
        Bgp4InitAddrPrefixStruct (&(RemAddr), BGP4_INET_AFI_IPV4);
        Bgp4RtRefRequestHandler (0, RemAddr, BGP4_INET_AFI_L2VPN,
                                 BGP4_INET_SAFI_EVPN, BGP4_RR_ADVT_EVPN_PEERS);
    }

    UNUSED_PARAM (i4RetVal);
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4EvpnVrfDownHdlr                            */
/* Description     : Makes VRF down                                 */
/* Input(s)        : u4VrfId - Vrf Id                               */
/*                   u4VNId  - VN Id                                */
/*                   u1VrfState - the state of VRF                  */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4EvpnVrfDownHdlr (UINT4 u4VrfId, UINT4 u4VNId, UINT1 u1VrfState)
{
    UINT4               u4State = 0;
    INT4                i4RetVal = BGP4_FAILURE;

    /* case 1. When the vrf is undergoing init or is created or is up,
     *         set oper-down
     * case 2. When the vrf is issued to be deleted, then call vrfdeinit
     * case 3. in any other state, just return
     */
    Bgp4GetVrfVniCurrentState (u4VrfId, u4VNId, &u4State);
    if (((u4State == BGP4_EVPN_VRF_CREATE)
         || (u4State == BGP4_EVPN_VRF_UP))
        && (u1VrfState == BGP4_EVPN_VRF_DOWN))
    {
        /* case 1. */
        i4RetVal =
            Bgp4SetVrfVniCurrentState (u4VrfId, u4VNId, BGP4_EVPN_VRF_DOWN);
        UNUSED_PARAM (i4RetVal);
        Bgp4EvpnVrfDeInit (u4VrfId, u4VNId, u1VrfState);
    }
    else if (u1VrfState == BGP4_EVPN_VRF_DELETE)
    {
        /* case 2. */
        Bgp4EvpnVrfDeInit (u4VrfId, u4VNId, u1VrfState);
    }
    /* case 3. */
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4EvpnVrfDeInit                              */
/* Description     : DeInitializes a particular vrf                 */
/* Input(s)        : u4VrfId - Vrf Id                               */
/*                   u4VNId  - VN Id                                */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS                         */
/********************************************************************/
INT4
Bgp4EvpnVrfDeInit (UINT4 u4VrfId, UINT4 u4VnId, UINT4 u4VrfState)
{
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pNextRt = NULL;
    tRouteProfile      *pNextRoute = NULL;
    tEvpnVrfNode        EvpnVrfNode;
    tEvpnVrfNode       *pEvpnVrfNode = NULL;
    VOID               *pRibNode = NULL;
    tBgpMacDup          BgpMacDupDel;
    tBgpMacDup         *pBgpMacDupDel = NULL;
    tBgp4AppTimer      *pBgpTmr = NULL;
    INT4                i4Sts = BGP4_TRUE;
    INT4                i4Ret;
    INT4                i4DelRtRet = BGP4_SUCCESS;
    UINT1               au1ZeroAddr[MAX_LEN_ETH_SEG_ID];

    /* VRF is going to be down. Remove all the routes that are present
     * in VRF.
     */
    MEMSET (&EvpnVrfNode, 0, sizeof (tEvpnVrfNode));
    MEMSET (&BgpMacDupDel, 0, sizeof (tBgpMacDup));
    MEMSET (au1ZeroAddr, 0, MAX_LEN_ETH_SEG_ID);

    pRibNode = BGP4_EVPN_RIB_TREE (u4VrfId);

    /* Get the first route */
    i4Sts = Bgp4RibhGetFirstEntry (CAP_MP_L2VPN_EVPN, &pRtProfile,
                                   &pRibNode, TRUE);
    if (i4Sts == BGP4_SUCCESS)
    {
        for (;;)
        {
            i4Ret = Bgp4RibhGetNextEntry (pRtProfile, u4VrfId,
                                          &pNextRoute, &pRibNode, TRUE);
            if (pRtProfile->EvpnNlriInfo.u1RouteType == EVPN_MAC_ROUTE)
            {
                pNextRt = pRtProfile;
                while (pNextRt != NULL)
                {
                    pRtProfile = pNextRt;
                    if (pNextRt->EvpnNlriInfo.u4VnId != u4VnId)
                    {
                        pNextRt = BGP4_RT_NEXT (pRtProfile);
                        continue;
                    }
                    /* 1. delete this route from BGP-VRF-RIB
                     * 2. Add this route to FIB update list to update IP
                     * 3. Add this route to internal peer advertisement list
                     */
                    if ((BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID) &&
                        (BGP4_EVPN_VRF_SPEC_GET_FLAG (u4VrfId) &
                         BGP4_EVPN_VRF_NO_EXP_TARGETS))
                    {
                        pNextRt = BGP4_RT_NEXT (pRtProfile);
                        continue;
                    }

                    BGP4_RT_REF_COUNT (pRtProfile)++;
                    if (MEMCMP (pRtProfile->EvpnNlriInfo.au1EthernetSegmentID,
                                au1ZeroAddr, MAX_LEN_ETH_SEG_ID) == 0)
                    {
                        if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
                        {
                            /* route is received from a PE peer, then add this route
                             * for FIB and advertisement
                             */
                            BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);
                            Bgp4IgphUpdateRouteInFIB (pRtProfile);
                            Bgp4AddWithdRouteToIntPeerAdvtList (pRtProfile);
                        }
                        if (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID)
                        {
                            BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);
                            Bgp4AddWithdRouteToIntPeerAdvtList (pRtProfile);
                        }
                    }
                    pNextRt = BGP4_RT_NEXT (pRtProfile);
                    /* Stop the Mac Duplication Timer if Running */
                    BgpMacDupDel.u4VrfId = u4VrfId;
                    BgpMacDupDel.u4VnId = BGP4_EVPN_VNID (pRtProfile);
                    MEMCPY (BgpMacDupDel.MacAddress,
                            BGP4_EVPN_MACADDR (pRtProfile), sizeof (tMacAddr));
                    pBgpMacDupDel =
                        RBTreeGet (gBgpCxtNode[u4VrfId]->BgpMacDupTimer,
                                   (tRBElem *) & BgpMacDupDel);
                    if (pBgpMacDupDel != NULL)
                    {
                        /* Mac Duplication Timer is Running */
                        pBgpMacDupDel->tMacMobDupTmr.u4Flag = BGP4_INACTIVE;
                        pBgpTmr = &(pBgpMacDupDel->tMacMobDupTmr);
                        pBgpTmr->Tmr.u4Data =
                            (FS_ULONG) ((VOID *) pBgpMacDupDel);
                        BGP4_TIMER_STOP (BGP4_TIMER_LISTID, &pBgpTmr->Tmr);
                        RBTreeRem (gBgpCxtNode[u4VrfId]->BgpMacDupTimer,
                                   (tRBElem *) pBgpMacDupDel);
                        MemReleaseMemBlock (BGP4_MAC_DUP_TIMER_MEM_POOL_ID,
                                            (UINT1 *) pBgpMacDupDel);
                    }

                    i4DelRtRet = Bgp4RibhDelRtEntry (pRtProfile,
                                                     pRibNode, u4VrfId,
                                                     BGP4_TRUE);
                }
            }
            else if ((pRtProfile->EvpnNlriInfo.u1RouteType == EVPN_AD_ROUTE) ||
                     (pRtProfile->EvpnNlriInfo.u1RouteType ==
                      EVPN_ETH_SEGMENT_ROUTE))
            {
                pNextRt = pRtProfile;
                while (pNextRt != NULL)
                {
                    pRtProfile = pNextRt;
                    EvpnVrfNode.u4VnId = u4VnId;
                    pEvpnVrfNode =
                        RBTreeGet (gBgpCxtNode[u4VrfId]->EvpnRoutes,
                                   (tRBElem *) & EvpnVrfNode);
                    if (pEvpnVrfNode != NULL)
                    {
                        if (MEMCMP
                            (pEvpnVrfNode->au1EthSegId,
                             pNextRt->EvpnNlriInfo.au1EthernetSegmentID,
                             MAX_LEN_ETH_SEG_ID) != 0)
                        {
                            pNextRt = BGP4_RT_NEXT (pRtProfile);
                            continue;
                        }
                    }
                    if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
                    {
                        /* route is received from a PE peer, then add this route
                         * for FIB and advertisement */
                        BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);
                        Bgp4IgphUpdateRouteInFIB (pRtProfile);
                        Bgp4AddWithdRouteToIntPeerAdvtList (pRtProfile);
                    }
                    if (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID)
                    {
                        BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);
                        Bgp4AddWithdRouteToIntPeerAdvtList (pRtProfile);
                    }
                    pNextRt = BGP4_RT_NEXT (pRtProfile);
                    i4DelRtRet = Bgp4RibhDelRtEntry (pRtProfile,
                                                     pRibNode, u4VrfId,
                                                     BGP4_TRUE);
                }
            }
            if (i4Ret == BGP4_FAILURE)
            {
                i4Sts = BGP4_FALSE;
                break;
            }

            pRtProfile = pNextRoute;
            pNextRoute = NULL;
        }
    }
    if (u4VrfState == BGP4_EVPN_VRF_DELETE)
    {
        EvpnVrfNode.u4VnId = u4VnId;
        pEvpnVrfNode =
            RBTreeGet (gBgpCxtNode[u4VrfId]->EvpnRoutes,
                       (tRBElem *) & EvpnVrfNode);
        if (pEvpnVrfNode != NULL)
        {
            Bgp4EvpnReleaseRouteTargetsFromVrf (&pEvpnVrfNode->ImportTargets,
                                                BGP4_IMPORT_ROUTE_TARGET_TYPE);
            Bgp4EvpnReleaseRouteTargetsFromVrf (&pEvpnVrfNode->ExportTargets,
                                                BGP4_EXPORT_ROUTE_TARGET_TYPE);
            RBTreeRem (gBgpCxtNode[u4VrfId]->EvpnRoutes,
                       (tRBElem *) pEvpnVrfNode);
            MemReleaseMemBlock (BGP4_EVPN_VRF_VNI_MEM_POOL_ID,
                                (UINT1 *) pEvpnVrfNode);
            return BGP4_TRUE;
        }
    }
    UNUSED_PARAM (i4DelRtRet);
    return BGP4_FALSE;
}

/****************************************************************************/
/* Function Name : Bgp4SendEvpnRtsToPeer                                    */
/* Description   : This function is called to get the Evpn routes for the   */
/*                 VRF and send the routes to PE peers. If Evpn routes are  */
/*                 not available, get the IPV4 routes for the VRF, convert  */
/*                 them into VPNV4 routes and send the routes to PE peers   */
/* Input(s)      : u4Context  - VRF number                                  */
/* Output(s)     : None                                                     */
/* Return(s)     : None                                                     */
/****************************************************************************/
VOID
Bgp4SendEvpnRtsToPeer (UINT4 u4Context, UINT4 u4VNId)
{
    tRouteProfile      *pEvpnRtProfile = NULL;
    tTMO_SLL            TsRouteList;
    BOOL1               bIsEvpnRtAvailable = BGP4_FALSE;
    INT4                i4RetVal = BGP4_FAILURE;

    TMO_SLL_Init (&TsRouteList);

    Bgp4RcvdPADBGetFirstBestRoute (u4Context, &pEvpnRtProfile,
                                   BGP4_INET_AFI_L2VPN, BGP4_INET_SAFI_EVPN);

    while (pEvpnRtProfile != NULL)
    {
        if ((pEvpnRtProfile->EvpnNlriInfo.u4VnId != u4VNId) ||
            (BGP4_RT_PROTOCOL (pEvpnRtProfile) == BGP_ID))
        {
            if (Bgp4RcvdPADBGetNextBestRoute (pEvpnRtProfile, &pEvpnRtProfile)
                == BGP4_FAILURE)
            {
                break;
            }
            continue;
        }
        Bgp4AddFeasRouteToIntPeerAdvtList (pEvpnRtProfile, NULL);
        bIsEvpnRtAvailable = BGP4_TRUE;
        if (Bgp4RcvdPADBGetNextBestRoute (pEvpnRtProfile, &pEvpnRtProfile)
            == BGP4_FAILURE)
        {
            break;
        }
    }

    if (bIsEvpnRtAvailable == BGP4_TRUE)
    {
        return;
    }

    /*VXLAN API to Audit and fetch the MAC be called */
    i4RetVal = EvpnUtlBgpGetMacUpdate (u4VNId);

    /* Send ES Update */
    Bgp4EvpnProbeandSendESUpdate (u4Context, u4VNId);

    UNUSED_PARAM (i4RetVal);
    return;
}

/********************************************************************/
/* Function Name   : Bgp4IsEvpnRouteCanBeProcessed                  */
/* Description     : This function checks whether a route can be    */
/*                   processed based on the VRFs the route has been */
/*                   installed in.                                  */
/* Input(s)        : pRtProfile - route profile pointer             */
/* Output(s)       : None.                                          */
/* Use of Recursion: None.                                          */
/* Returns         : BGP4_SUCCESS or BGP4_FAILURE                   */
/********************************************************************/
INT4
Bgp4IsEvpnRouteCanBeProcessed (tRouteProfile * pRtProfile)
{
    INT4                i4Ret = BGP4_FAILURE;
    UINT4               u4VrfId = 0;
    UINT4               u4VNId = 0;
    UINT4               u4State = 0;

    INT4                i4RtFound = BGP4_FAILURE;
    tRouteProfile      *pFndRtProfileList = NULL;
    tRouteProfile      *pTmpRtProfile = NULL;
    VOID               *pRibNode = NULL;

    pTmpRtProfile = Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
    if (pTmpRtProfile == NULL)
    {
        /* Memory Allocation Failed */
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t BGP Memory Allocation for Route Profile Failed!! \n");
        return BGP4_FAILURE;
    }

    if ((BGP4_RR_CLIENT_CNT (u4VrfId) > 0) ||
        (BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_FILTERED_INPUT))
    {
        /* the speaker is a route-reflector and some clients are
         * configred, then also return success
         */
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t EVPN : Route Reflector is enabled , Return Success!! \n");
        Bgp4MemReleaseRouteProfile (pTmpRtProfile);
        return (BGP4_SUCCESS);
    }

    if (pRtProfile->EvpnNlriInfo.u1RouteType == EVPN_ETH_SEGMENT_ROUTE)
    {
        /* The received route is Ethernet Segment Route, return success */
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t EVPN : Ethernet Segment Route is received , Return Success!! \n");
        Bgp4MemReleaseRouteProfile (pTmpRtProfile);
        return (BGP4_SUCCESS);
    }

    if ((pRtProfile->EvpnNlriInfo.u1RouteType == EVPN_AD_ROUTE) ||
        (pRtProfile->EvpnNlriInfo.u1RouteType == EVPN_MAC_ROUTE))
    {
        MEMCPY (pTmpRtProfile->EvpnNlriInfo.au1RouteDistinguisher,
                pRtProfile->EvpnNlriInfo.au1RouteDistinguisher,
                MAX_LEN_RD_VALUE);
        MEMCPY (pTmpRtProfile->EvpnNlriInfo.au1EthernetSegmentID,
                pRtProfile->EvpnNlriInfo.au1EthernetSegmentID,
                BGP4_EVPN_ETH_SEG_ID_SIZE);
        pTmpRtProfile->u4VrfId = pRtProfile->u4VrfId;
        BGP4_RT_AFI_INFO (pTmpRtProfile) = BGP4_INET_AFI_L2VPN;
        BGP4_RT_SAFI_INFO (pTmpRtProfile) = BGP4_INET_SAFI_EVPN;

        /* If the route profile is a AD Route or MAC route, the route should not be
         * processed if a ES Route is available for the same Ethernet Segment ID
         */
        i4RtFound =
            Bgp4RibhLookupRtEntry (pTmpRtProfile, pTmpRtProfile->u4VrfId,
                                   BGP4_TREE_FIND_EXACT, &pFndRtProfileList,
                                   &pRibNode);
        UNUSED_PARAM (i4RtFound);
        if (pFndRtProfileList != NULL)
        {
            /* ESI Entry Exists */
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                      "\t EVPN : ES Route Already Exists !! \n");
            Bgp4MemReleaseRouteProfile (pTmpRtProfile);
            return BGP4_FAILURE;
        }
    }

    u4VrfId = pRtProfile->u4VrfId;
    u4VNId = pRtProfile->EvpnNlriInfo.u4VnId;
    Bgp4GetVrfVniCurrentState (u4VrfId, u4VNId, &u4State);
    i4Ret = (((BGP4_EVPN_RIB_TREE (u4VrfId) == NULL)
              || (u4State != BGP4_EVPN_VRF_UP)
              || ((BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
                  && (BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_BEST)
                  && (BGP4_EVPN_VRF_SPEC_GET_FLAG (u4VrfId) &
                      BGP4_EVPN_VRF_NO_EXP_TARGETS)))) ? BGP4_FAILURE :
        BGP4_SUCCESS;
    Bgp4MemReleaseRouteProfile (pTmpRtProfile);
    return (i4Ret);
}

/*****************************************************************************/
/* Function Name : Bgp4IsSameEvpnRoute                                       */
/* Description   : This routine checks for the identity of the given two     */
/*                 BGP4 route profiles.                                      */
/* Input(s)      : BGP informations                                          */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRUE if the informations are identical,                   */
/*                 FALSE otherwise.                                          */
/*****************************************************************************/
BOOL1
Bgp4IsSameEvpnRoute (tRouteProfile * pRtProfile, tRouteProfile * pPeerRoute)
{

    if (BGP4_EVPN_VNID (pRtProfile) != BGP4_EVPN_VNID (pPeerRoute))
    {
        return FALSE;
    }
    if (MEMCMP
        (BGP4_EVPN_RD_DISTING (pRtProfile), BGP4_EVPN_RD_DISTING (pPeerRoute),
         BGP4_EVPN_ROUTE_DISTING_SIZE) != 0)
    {
        return FALSE;
    }
    if (BGP4_EVPN_VRFID (pRtProfile) != BGP4_EVPN_VRFID (pPeerRoute))
    {
        return FALSE;
    }
    if (BGP4_EVPN_ETHSEGID (pRtProfile) != BGP4_EVPN_ETHSEGID (pPeerRoute))
    {
        return FALSE;
    }
    return TRUE;
}

/****************************************************************************/
/* Function Name   : EvpnRouteTblCmpFunc                                    */
/* Description     : user compare function for Evpn Route TBL RBTrees       */
/* Input(s)        : Two RBTree Nodes be compared                           */
/* Output(s)       : None                                                   */
/* Returns         : 1/(-1)/0                                               */
/****************************************************************************/
INT4
EvpnRouteTblCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tEvpnVrfNode       *pEvpnVrfNode1 = (tEvpnVrfNode *) pRBElem1;
    tEvpnVrfNode       *pEvpnVrfNode2 = (tEvpnVrfNode *) pRBElem2;

    return (MEMCMP (&(pEvpnVrfNode1->u4VnId), &(pEvpnVrfNode2->u4VnId),
                    sizeof (UINT4)));
}

/*****************************************************************************/
/* FUNCTION NAME : Bgp4EvpnCreateWithdMsgforVXLAN                            */
/* DESCRIPTION   : This function create VXLAN_BGP_WITHDRAWN msg and send it  */
/*                 to VXLAN                                                  */
/* INPUTS        : pRouteProfile route profile                               */
/* OUTPUTS       : None                                                      */
/* Return        : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4EvpnCreateWithdMsgforVXLAN (tRouteProfile * pRtProfile)
{
    INT4                i4RetVal = BGP4_FAILURE;
    tEvpnRoute          Bgp4EvpnVxlanInfo;
    UINT4               u4NextHopAddress = 0;
    tEvpnVrfNode        EvpnVrfNode;
    UINT4               u4Index = 0;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT2               u2EcommType;

    /* For withdrawn messgae, pass the VRF Id, VNI Id, Mac Address and 
     * next hop information */
    MEMSET (&Bgp4EvpnVxlanInfo, 0, sizeof (Bgp4EvpnVxlanInfo));
    MEMSET (&EvpnVrfNode, 0, sizeof (tEvpnVrfNode));
    MEMSET (au1ExtComm, 0, EXT_COMM_VALUE_LEN);

    /* VRF Id */
    Bgp4EvpnVxlanInfo.u4EviNumber = BGP4_EVPN_VRFID (pRtProfile);
    /* VNI Id */
    Bgp4EvpnVxlanInfo.u4VxlanVniNumber = BGP4_EVPN_VNID (pRtProfile);
    /* Route type */
    Bgp4EvpnVxlanInfo.u1RouteType = BGP4_EVPN_RT_TYPE (pRtProfile);
    Bgp4EvpnVxlanInfo.i4VxlanRemoteVtepAddressType =
        BGP4_INFO_AFI (BGP4_RT_BGP_INFO (pRtProfile));
    /* Mac Address */
    MEMCPY (&(Bgp4EvpnVxlanInfo.VxlanDestVmMac), BGP4_EVPN_MACADDR (pRtProfile),
            sizeof (tMacAddr));
    /* Next hop address */
    if (Bgp4EvpnVxlanInfo.i4VxlanRemoteVtepAddressType == BGP4_INET_AFI_IPV4)
    {
        PTR_FETCH_4 (u4NextHopAddress,
                     BGP4_INFO_NEXTHOP (BGP4_RT_BGP_INFO (pRtProfile)));
        u4NextHopAddress = OSIX_HTONL (u4NextHopAddress);
        MEMCPY (&Bgp4EvpnVxlanInfo.au1VxlanRemoteVtepAddress, &u4NextHopAddress,
                BGP4_INFO_ADDR_LEN (BGP4_RT_BGP_INFO (pRtProfile)));
    }
    Bgp4EvpnVxlanInfo.u1VxlanDestAddressLen = BGP4_EVPN_IPADDR_LEN (pRtProfile);
    if (Bgp4EvpnVxlanInfo.u1VxlanDestAddressLen ==
        BGP4_EVPN_IP_ADDR_SIZE * BGP4_ONE_BYTE_BITS)
    {
        MEMCPY (&Bgp4EvpnVxlanInfo.au1VxlanDestAddress,
                &BGP4_EVPN_IPADDR (pRtProfile), BGP4_EVPN_IP_ADDR_SIZE);
    }
    else if (Bgp4EvpnVxlanInfo.u1VxlanDestAddressLen ==
             BGP4_EVPN_IP6_ADDR_SIZE * BGP4_ONE_BYTE_BITS)
    {
        MEMCPY (&Bgp4EvpnVxlanInfo.au1VxlanDestAddress,
                &BGP4_EVPN_IPADDR (pRtProfile), BGP4_EVPN_IP6_ADDR_SIZE);
    }
    /* Fill the Ethernet Segment Identifier */
    MEMCPY (&(Bgp4EvpnVxlanInfo.au1FsEvpnMHEviVniESI),
            BGP4_EVPN_ETHSEGID (pRtProfile), MAX_LEN_ETH_SEG_ID);

    /* In case of AD Route, Get the Load Balance flag from the Route Profile and pass it to VXLAN */
    if (pRtProfile->EvpnNlriInfo.u1RouteType == EVPN_AD_ROUTE)
    {
        for (u4Index = 0; u4Index < BGP4_INFO_ECOMM_COUNT (pRtProfile->pRtInfo);
             u4Index++)
        {
            MEMCPY ((UINT1 *) au1ExtComm,
                    (UINT1 *) (BGP4_INFO_ECOMM_ATTR_VAL (pRtProfile->pRtInfo) +
                               +(u4Index * EXT_COMM_VALUE_LEN)),
                    EXT_COMM_VALUE_LEN);
            PTR_FETCH2 (u2EcommType, au1ExtComm);
            if (u2EcommType == 0x0601)
            {
                MEMCPY (&(Bgp4EvpnVxlanInfo.u1LBFlag), &au1ExtComm[2],
                        sizeof (UINT1));
                break;
            }
        }
    }

    /* Pass the information to VXLAN */
    i4RetVal = EvpnVxlanDelRouteInDp (&Bgp4EvpnVxlanInfo);

    return i4RetVal;
}

/*****************************************************************************/
/* FUNCTION NAME : Bgp4EvpnCreateAdvtMsgforVXLAN                             */
/* DESCRIPTION   : This function create VXLAN_BGP_WITHDRAWN msg and send it  */
/*                 to VXLAN                                                  */
/* INPUTS        : pRouteProfile route profile                               */
/* OUTPUTS       : None                                                      */
/* Return        : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4EvpnCreateAdvtMsgforVXLAN (tRouteProfile * pRtProfile)
{
    INT4                i4RetVal = BGP4_FAILURE;
    tEvpnRoute          Bgp4EvpnVxlanInfo;
    UINT4               u4NextHopAddress = 0;
    tEvpnVrfNode        EvpnVrfNode;
    UINT4               u4Index = 0;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT2               u2EcommType;

    /* For advertise messgae, pass the VRF Id, VNI Id, Mac Address and
     * next hop information */

    MEMSET (&Bgp4EvpnVxlanInfo, 0, sizeof (Bgp4EvpnVxlanInfo));
    MEMSET (&EvpnVrfNode, 0, sizeof (tEvpnVrfNode));

    /* VRF Id */
    Bgp4EvpnVxlanInfo.u4EviNumber = BGP4_EVPN_VRFID (pRtProfile);
    /* VNI Id */
    Bgp4EvpnVxlanInfo.u4VxlanVniNumber = BGP4_EVPN_VNID (pRtProfile);
    /* Route type */
    Bgp4EvpnVxlanInfo.u1RouteType = BGP4_EVPN_RT_TYPE (pRtProfile);
    /* Mac Address */
    MEMCPY (&(Bgp4EvpnVxlanInfo.VxlanDestVmMac), BGP4_EVPN_MACADDR (pRtProfile),
            sizeof (tMacAddr));

    Bgp4EvpnVxlanInfo.i4VxlanRemoteVtepAddressType =
        BGP4_INFO_AFI (BGP4_RT_BGP_INFO (pRtProfile));
    /* Next hop address */
    if (Bgp4EvpnVxlanInfo.i4VxlanRemoteVtepAddressType == BGP4_INET_AFI_IPV4)
    {
        PTR_FETCH_4 (u4NextHopAddress,
                     BGP4_INFO_NEXTHOP (BGP4_RT_BGP_INFO (pRtProfile)));
        u4NextHopAddress = OSIX_HTONL (u4NextHopAddress);
        MEMCPY (&Bgp4EvpnVxlanInfo.au1VxlanRemoteVtepAddress, &u4NextHopAddress,
                BGP4_INFO_ADDR_LEN (BGP4_RT_BGP_INFO (pRtProfile)));
    }
    Bgp4EvpnVxlanInfo.u1VxlanDestAddressLen = BGP4_EVPN_IPADDR_LEN (pRtProfile);
    if (Bgp4EvpnVxlanInfo.u1VxlanDestAddressLen ==
        BGP4_EVPN_IP_ADDR_SIZE * BGP4_ONE_BYTE_BITS)
    {
        MEMCPY (&Bgp4EvpnVxlanInfo.au1VxlanDestAddress,
                &BGP4_EVPN_IPADDR (pRtProfile), BGP4_EVPN_IP_ADDR_SIZE);
    }
    else if (Bgp4EvpnVxlanInfo.u1VxlanDestAddressLen ==
             BGP4_EVPN_IP6_ADDR_SIZE * BGP4_ONE_BYTE_BITS)
    {
        MEMCPY (&Bgp4EvpnVxlanInfo.au1VxlanDestAddress,
                &BGP4_EVPN_IPADDR (pRtProfile), BGP4_EVPN_IP6_ADDR_SIZE);
    }

    /* Fill the Ethernet Segment Identifier */
    MEMCPY (&(Bgp4EvpnVxlanInfo.au1FsEvpnMHEviVniESI),
            BGP4_EVPN_ETHSEGID (pRtProfile), MAX_LEN_ETH_SEG_ID);

    /* In case of AD Route, Get the Load Balance flag from the Route Profile and pass it to VXLAN */
    if (pRtProfile->EvpnNlriInfo.u1RouteType == EVPN_AD_ROUTE)
    {
        for (u4Index = 0; u4Index < BGP4_INFO_ECOMM_COUNT (pRtProfile->pRtInfo);
             u4Index++)
        {
            MEMCPY ((UINT1 *) au1ExtComm,
                    (UINT1 *) (BGP4_INFO_ECOMM_ATTR_VAL (pRtProfile->pRtInfo) +
                               +(u4Index * EXT_COMM_VALUE_LEN)),
                    EXT_COMM_VALUE_LEN);
            PTR_FETCH2 (u2EcommType, au1ExtComm);
            if (u2EcommType == 0x0601)
            {
                MEMCPY (&(Bgp4EvpnVxlanInfo.u1LBFlag), &au1ExtComm[2],
                        sizeof (UINT1));
                break;
            }
        }
    }
    /* Pass the Information to VXLAN */
    i4RetVal = EvpnVxlanAddRouteInDp (&Bgp4EvpnVxlanInfo);

    return i4RetVal;
}

/*********************************************************************/
/* Function Name   : Bgp4EvpnApplyImporRTFilter                      */
/* Description     : This module applies the import Route Target     */
/*                   filter of each Vrf on the Route Targets received*/
/*                  with the EVPN routes from the PE peers.          */
/* Input(s)        : pInputRtsList - Pointer to the list of Vpnv4    */
/*                   routes reeived from the PE peer.                */
/*                   pPeerInfo- pointer to the peer information      */
/*                       from which the EVPN route  is received      */
/* Output(s)       : None                                            */
/* Global Variables Referred : None                                  */
/* Global variables Modified : None                                  */
/* Exceptions or Operating System Error Handling : None              */
/* Use of Recursion          : None.                                 */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE          */
/*********************************************************************/
INT4
Bgp4EvpnApplyImportRTFilter (tTMO_SLL * pInputRtsList,
                             tBgp4PeerEntry * pPeerInfo)
{
    tLinkNode          *pLinkNode = NULL;
    tRouteProfile      *pEvpnRoute = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tBgp4Info          *pBgp4Info = NULL;
    tEvpnVrfNode       *pEvpnVrfNode = NULL;
    tBgp4EvpnRrImportTargetInfo *pRrImportTarget = NULL;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT1               u1RrRTCommAdded = BGP4_FALSE;
    UINT1               u1RTCommInRRList = 0;
    UINT2               u2EcommType;
    UINT4               u4AsafiMask;
    UINT4               u4Index;
    INT4                i4RetStatus;
    UINT1               u1IsRtAvailable = BGP4_FALSE;

    if (TMO_SLL_Count (pInputRtsList) == 0)
    {
        return BGP4_SUCCESS;
    }

    pEvpnRoute = ((tLinkNode *) TMO_SLL_First (pInputRtsList))->pRouteProfile;

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pEvpnRoute),
                           BGP4_RT_SAFI_INFO (pEvpnRoute), u4AsafiMask);
    if (u4AsafiMask != CAP_MP_L2VPN_EVPN)
    {
        /* The routes list is not EVPN-unicast family. return success */
        return BGP4_SUCCESS;
    }
    pBgp4Info = BGP4_RT_BGP_INFO (pEvpnRoute);

    /* Check whether extended community is received or not along
     * with the path attributes
     */
    if (BGP4_INFO_ECOMM_ATTR (pBgp4Info) == NULL)
    {
        /* No route-target community is received along with Evpn routes.
         * hence filter those routes
         */
        TMO_SLL_Scan (pInputRtsList, pLinkNode, tLinkNode *)
        {
            pEvpnRoute = pLinkNode->pRouteProfile;
            BGP4_RT_SET_FLAG (pEvpnRoute, BGP4_RT_FILTERED_INPUT);
        }
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t EVPN : No RT Community Received, Filter the Route !! \n");
        return BGP4_FAILURE;
    }

    for (u4Index = 0; u4Index < BGP4_INFO_ECOMM_COUNT (pBgp4Info); u4Index++)
    {
        MEMCPY ((UINT1 *) au1ExtComm,
                (UINT1 *) (BGP4_INFO_ECOMM_ATTR_VAL (pBgp4Info) +
                           +(u4Index * EXT_COMM_VALUE_LEN)),
                EXT_COMM_VALUE_LEN);
        PTR_FETCH2 (u2EcommType, au1ExtComm);
        if (BGP4_IS_RT_EXT_COMMUNITY (u2EcommType) == BGP4_TRUE)
        {
            pEvpnVrfNode = Bgp4EvpnLinkMatchingImportRTVrfs (au1ExtComm,
                                                             pEvpnRoute);

            if ((pEvpnVrfNode == NULL) &&
                (BGP4_RR_CLIENT_CNT (pEvpnRoute->u4VrfId) == 0))
            {
                /* there is no matching vrf found */
                continue;
            }

            if (BGP4_RR_CLIENT_CNT (pEvpnRoute->u4VrfId) > 0)
            {
                pRrImportTarget =
                    Bgp4EvpnIsRTMatchInRRImportRTList (au1ExtComm);
                if (pRrImportTarget != NULL)
                {
                    /* If the speaker is route-reflector and we received
                     * a matching route taret community from the nonclient
                     * peer, then apply the filter
                     */
                    if (BGP4_PEER_RFL_CLIENT (pPeerInfo) == NON_CLIENT)
                    {
                        u1RTCommInRRList++;
                    }
                }
                else
                {
                    if (BGP4_PEER_RFL_CLIENT (pPeerInfo) == CLIENT)
                    {
                        /* If the speaker is route-reflector and we
                         * received a new route target community from
                         * the client peer, then add into the Import RT set
                         */
                        pRrImportTarget =
                            Bgp4EvpnAddNewRTInRRImportList (au1ExtComm);
                        if (pRrImportTarget == NULL)
                        {
                            return BGP4_FAILURE;
                        }
                        u1RrRTCommAdded = BGP4_TRUE;
                        /* Increment the route-count */
                        BGP4_EVPN_RR_IMPORT_TARGET_RTCNT (pRrImportTarget)
                            += TMO_SLL_Count (pInputRtsList);
                        BGP4_EVPN_RR_IMPORT_TARGET_TIMESTAMP
                            (pRrImportTarget) = 0;
                    }
                }
            }
            else
            {
                u1IsRtAvailable = BGP4_TRUE;
            }
        }
    }

    if (u1RrRTCommAdded == BGP4_TRUE)
    {
        /* The speaker is route-reflector and the route is received from a
         * client peer. If a new route target is added into the set,
         * then send Route-refresh message to the peers */
        i4RetStatus = Bgp4EvpnRrJoin ();
        if (i4RetStatus == BGP4_FAILURE)
        {
            return BGP4_FAILURE;
        }
    }

    TMO_SLL_Scan (pInputRtsList, pLinkNode, tLinkNode *)
    {
        pRtProfile = pLinkNode->pRouteProfile;
        if (((u1IsRtAvailable != BGP4_TRUE) &&
             (BGP4_RR_CLIENT_CNT (pEvpnRoute->u4VrfId) == 0)) ||
            ((BGP4_RR_CLIENT_CNT (pEvpnRoute->u4VrfId) > 0) &&
             (BGP4_PEER_RFL_CLIENT (pPeerInfo) == NON_CLIENT) &&
             (u1RTCommInRRList == 0)))
        {
            BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_FILTERED_INPUT);
        }
        else
        {
            if ((pRtProfile == pEvpnRoute) || (pEvpnVrfNode == NULL))
            {
                continue;
            }
            pRtProfile->u4VrfId = pEvpnRoute->u4VrfId;
        }
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4EvpnLinkMatchingImportRTVrfs               */
/* Description     : This function links the VRF information into   */
/*                   vrf-install list of route profile for a given  */
/*                   import route target. If the import route target*/
/*                   matches with any VRF, then that will be linked */
/* Input(s)        : pu1RTCommVal - import route target value       */
/*                   pRtProfile  - route information                */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
tEvpnVrfNode       *
Bgp4EvpnLinkMatchingImportRTVrfs (UINT1 *pu1RTCommVal,
                                  tRouteProfile * pRtProfile)
{
    tExtCommProfile    *pExtCommProfile = NULL;
    tEvpnVrfNode       *pEvpnVrfNode = NULL;
    tEvpnVrfNode        EvpnVrfNode;
    UINT4               u4Context = 0;
    INT4                i4RetVal = SNMP_FAILURE;

    MEMSET (&EvpnVrfNode, 0, sizeof (tEvpnVrfNode));
    i4RetVal = BgpGetFirstActiveContextID (&u4Context);
    while (i4RetVal == SNMP_SUCCESS)
    {
        if (BGP4_EVPN_RT_TYPE (pRtProfile) == EVPN_ETH_SEGMENT_ROUTE)
        {
            pEvpnVrfNode = Bgp4EvpnLinkMatchingEthRoute (u4Context,
                                                         pRtProfile,
                                                         pu1RTCommVal);
            if (pEvpnVrfNode != NULL)
            {
                return pEvpnVrfNode;
            }
        }
        else
        {
            EvpnVrfNode.u4VnId = BGP4_EVPN_VNID (pRtProfile);
            pEvpnVrfNode = RBTreeGet (gBgpCxtNode[u4Context]->EvpnRoutes,
                                      (tRBElem *) & EvpnVrfNode);
            if (pEvpnVrfNode != NULL)
            {
                if (TMO_SLL_Count (&pEvpnVrfNode->ImportTargets) == 0)
                {
                    i4RetVal =
                        BgpGetNextActiveContextID (u4Context, &u4Context);
                    continue;
                }
                TMO_SLL_Scan (&pEvpnVrfNode->ImportTargets,
                              pExtCommProfile, tExtCommProfile *)
                {
                    if (MEMCMP ((UINT1 *) pExtCommProfile->au1ExtComm,
                                pu1RTCommVal, EXT_COMM_VALUE_LEN) == 0)
                    {
                        BGP4_EVPN_VRFID (pRtProfile) = u4Context;
                        return pEvpnVrfNode;
                    }
                }
            }
        }
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
    }
    return NULL;
}

/********************************************************************/
/* Function Name   : Bgp4AddNewRTInRRImportList                     */
/* Description     : Adds a new route target into RR targets set    */
/* Input(s)        : pu1RTExtComm - route target value              */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
tBgp4EvpnRrImportTargetInfo *
Bgp4EvpnAddNewRTInRRImportList (UINT1 *pu1RTExtComm)
{
    tBgp4EvpnRrImportTargetInfo *pRrImportTarget = NULL;

    pRrImportTarget =
        Bgp4MemAllocateEvpnRrImportTarget (sizeof
                                           (tBgp4EvpnRrImportTargetInfo));
    if (pRrImportTarget == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tEVPN: Creating RR Import Target Node failed\n");
        return (NULL);
    }

    MEMCPY (BGP4_EVPN_RR_IMPORT_TARGET (pRrImportTarget),
            pu1RTExtComm, EXT_COMM_VALUE_LEN);
    TMO_SLL_Add (BGP4_EVPN_RR_TARGETS_SET, (tTMO_SLL_NODE *) pRrImportTarget);
    return (pRrImportTarget);
}

/********************************************************************/
/* Function Name   : Bgp4EvpnIsRTMatchInRRImportRTList              */
/* Description     : Finds whether a particular route target is     */
/*                   present in RR import targets set. If present,  */
/*                   returns success else failure                   */
/* Input(s)        : pu1RTCommVal - import route target             */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
tBgp4EvpnRrImportTargetInfo *
Bgp4EvpnIsRTMatchInRRImportRTList (UINT1 *pu1RTCommVal)
{
    tBgp4EvpnRrImportTargetInfo *pBgp4RrImportTargetInfo = NULL;

    TMO_SLL_Scan (BGP4_EVPN_RR_TARGETS_SET,
                  pBgp4RrImportTargetInfo, tBgp4EvpnRrImportTargetInfo *)
    {
        if ((MEMCMP
             ((UINT1 *) BGP4_EVPN_RR_IMPORT_TARGET (pBgp4RrImportTargetInfo),
              pu1RTCommVal, EXT_COMM_VALUE_LEN)) == 0)
        {
            return (pBgp4RrImportTargetInfo);
        }
    }
    return (NULL);
}

/********************************************************************/
/* Function Name   : Bgp4EvpnRrDeleteTargetFromSet                  */
/* Description     : This function actually deletes the Route Target*/
/*                   from the Set. This will be called when         */
/*                   BGP4_EVPN_RR_TARGETS_INTERVAL time equals to   */
/*                   the delay (few hours) it is supposed to be     */
/*                   allowed before deleting the route target from  */
/*                   the set.                                       */
/* Input(s)        : None.                                          */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS                         */
/********************************************************************/
INT4
Bgp4EvpnRrDeleteTargetFromSet (VOID)
{
    tBgp4EvpnRrImportTargetInfo *pRrImportTarget = NULL;
    tBgp4EvpnRrImportTargetInfo *pTmpRrImportTarget = NULL;
    UINT4               u4Delay;
    UINT4               u4NextTimeStamp = 0;

    BGP_SLL_DYN_Scan (BGP4_EVPN_RR_TARGETS_SET, pRrImportTarget,
                      pTmpRrImportTarget, tBgp4EvpnRrImportTargetInfo *)
    {
        if (BGP4_EVPN_RR_IMPORT_TARGET_RTCNT (pRrImportTarget) == 0)
        {
            u4Delay = ((UINT4) Bgp4ElapTime () -
                       BGP4_EVPN_RR_IMPORT_TARGET_TIMESTAMP (pRrImportTarget));
            if (u4Delay >= BGP4_EVPN_RR_TARGETS_DEL_INTERVAL)
            {
                /* Route target delay exceeds the time interval it should
                 * be delayed. hence remove this target from the set.
                 */
                TMO_SLL_Delete (BGP4_EVPN_RR_TARGETS_SET,
                                (tTMO_SLL_NODE *) pRrImportTarget);
                Bgp4MemReleaseEvpnRrImportTarget (pRrImportTarget);
            }
            else if (u4Delay > u4NextTimeStamp)
            {
                u4NextTimeStamp = u4Delay;
            }
        }
    }

    BGP4_EVPN_RR_TARGETS_INTERVAL = u4NextTimeStamp;
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name : Bgp4EvpnReleaseRouteTargetsFromVrf                       */
/* Description   : Releases the given list of route targets.                */
/* Input(s)      : List of route targets (pTsList)                          */
/*                 type of the route targets                                */
/* Output(s)     : None.                                                    */
/* Return(s)     : Number of Nodes removed.                                 */
/****************************************************************************/
UINT4
Bgp4EvpnReleaseRouteTargetsFromVrf (tTMO_SLL * pTsList, UINT1 u1RTType)
{

    tExtCommProfile    *pRouteTarget = NULL;
    tBgp4EvpnRrImportTargetInfo *pRrImportTarget = NULL;
    UINT4               u4TargetCnt = 0;

    if (TMO_SLL_Count (pTsList) == 0)
    {
        return 0;
    }

    for (;;)
    {
        pRouteTarget = (tExtCommProfile *) TMO_SLL_First (pTsList);
        if (pRouteTarget == NULL)
        {
            break;
        }
        if (u1RTType == BGP4_IMPORT_ROUTE_TARGET_TYPE)
        {
            /* delete this route target from the RR import route target set if
             * it is present
             */
            pRrImportTarget =
                Bgp4EvpnIsRTMatchInRRImportRTList (pRouteTarget->au1ExtComm);

            if (pRrImportTarget != NULL)
            {
                TMO_SLL_Delete (BGP4_EVPN_RR_TARGETS_SET,
                                (tTMO_SLL_NODE *) pRrImportTarget);
                Bgp4MemReleaseEvpnRrImportTarget (pRrImportTarget);
            }
        }
        TMO_SLL_Delete (pTsList, (tTMO_SLL_NODE *) pRouteTarget);
        EXT_COMM_EXT_COMM_PROFILE_NODE_FREE (pRouteTarget);
        u4TargetCnt++;
    }
    TMO_SLL_Init (pTsList);
    return (u4TargetCnt);
}

/********************************************************************/
/* Function Name   : Bgp4EvpnRrJoin                                 */
/* Description     : Sends route-refresh message to all the         */
/*                   non-clients if its negotiated, otherwise, the  */
/*                   session will be reset by send CEASE message    */
/* Input(s)        : None.                                          */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4EvpnRrJoin (VOID)
{
    tAddrPrefix         PeerAddress;
    /* New import route target is added. Send Route-Refresh message
     * to the Nonclients */
    Bgp4InitAddrPrefixStruct (&(PeerAddress), BGP4_INET_AFI_IPV4);
    Bgp4RtRefRequestHandler (BGP4_DFLT_VRFID, PeerAddress, BGP4_INET_AFI_L2VPN,
                             BGP4_INET_SAFI_EVPN, BGP4_RR_ADVT_NON_CLIENTS);
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name : Bgp4MemAllocateEvpnRrImportTarget                        */
/* Description   : This function allocates RR import target information     */
/* Input(s)      : size of buffer to be allocated                           */
/* Output(s)     : None.                                                    */
/* Return(s)     : Pointer to the allocated tBgp4RrImportTargetInfo         */
/****************************************************************************/
tBgp4EvpnRrImportTargetInfo *
Bgp4MemAllocateEvpnRrImportTarget (UINT4 u4Size)
{
    tBgp4EvpnRrImportTargetInfo *pRrImportTarget = NULL;
    UINT1              *pu1Block = NULL;

    UNUSED_PARAM (u4Size);
    pu1Block = (UINT1 *) MemAllocMemBlk (BGP4_EVPN_RR_TARGETS_SET_MEMPOOL_ID);
    if (pu1Block == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for Peer FAILED!!!\n");
        return (NULL);
    }
    pRrImportTarget = (tBgp4EvpnRrImportTargetInfo *) (VOID *) (pu1Block);
    TMO_SLL_Init_Node (&(pRrImportTarget->NextTarget));
    BGP4_EVPN_RR_IMPORT_TARGET_RTCNT (pRrImportTarget) = 0;
    BGP4_EVPN_RR_IMPORT_TARGET_TIMESTAMP (pRrImportTarget) = 0;
    BGP4_EVPN_RR_TARGETS_SET_CNT++;
    return (pRrImportTarget);
}

/****************************************************************************/
/* Function Name : Bgp4MemReleaseEvpnRrImportTarget                         */
/* Description   : This function releases the RR import target info         */
/* Input(s)      : Pointer to tBgp4RrImportTargetInfo                       */
/* Output(s)     : None.                                                    */
/* Return(s)     : BGP4_SUCCESS                                             */
/****************************************************************************/
INT4
Bgp4MemReleaseEvpnRrImportTarget (tBgp4EvpnRrImportTargetInfo * pRrImportTarget)
{
    MemReleaseMemBlock (BGP4_EVPN_RR_TARGETS_SET_MEMPOOL_ID,
                        (UINT1 *) pRrImportTarget);
    BGP4_EVPN_RR_TARGETS_SET_CNT--;
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name : Bgp4EvpnFillExtComm                                      */
/* Description   : This function fills the extended community values from   */
/*                 configured export route targets. If Site-of-orign is to  */
/*                 be filled, this also will be taken care                  */
/* Inputs        : Pointer to the route to be removed (pRtProfile)          */
/*               : Pointer to the peer entry (pPeerEntry)                   */
/*               : Pointer to the advertisement BGP info (pAdvtBgpInfo)     */
/* Output(s)       : None.                                                  */
/* Use of Recursion          : None.                                        */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE                    */
/****************************************************************************/
INT4
Bgp4EvpnFillExtComm (tRouteProfile * pRouteProfile,
                     tBgp4PeerEntry * pPeerEntry, tBgp4Info * pAdvtBgpInfo)
{
    tExtCommProfile    *pExportTarget = NULL;
    UINT1              *pu1EvpnExtComm = NULL;
    UINT4               u4Count;
    UINT4               u4VrfId = 0;
    UINT4               u4RcvdCount = 0;
    tEvpnVrfNode        EvpnVrfNode;
    tEvpnVrfNode       *pEvpnVrfNode = NULL;
    UINT4               u4Index = 0;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT2               u2EcommType = 0;

    UNUSED_PARAM (pPeerEntry);
    MEMSET (&EvpnVrfNode, 0, sizeof (tEvpnVrfNode));
    MEMSET (au1ExtComm, 0, EXT_COMM_VALUE_LEN);

    pu1EvpnExtComm = MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);
    if (pu1EvpnExtComm == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\t BGP EVPN Extended Community is NULL !! \n");
        return BGP4_FAILURE;
    }

    u4RcvdCount = BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo);

    if (u4RcvdCount > 0)
    {
        MEMCPY (pu1EvpnExtComm, BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo),
                u4RcvdCount * EXT_COMM_VALUE_LEN);
    }

    u4VrfId = pRouteProfile->u4VrfId;
    EvpnVrfNode.u4VnId = BGP4_EVPN_VNID (pRouteProfile);

    if (pRouteProfile->EvpnNlriInfo.u1RouteType == EVPN_ETH_SEGMENT_ROUTE)
    {
        pEvpnVrfNode = RBTreeGetFirst (gBgpCxtNode[u4VrfId]->EvpnRoutes);
        while (pEvpnVrfNode != NULL)
        {
            if (MEMCMP (pRouteProfile->EvpnNlriInfo.au1EthernetSegmentID,
                        pEvpnVrfNode->au1EthSegId, MAX_LEN_ETH_SEG_ID) == 0)
            {
                MEMCPY ((pu1EvpnExtComm +
                         (BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo) *
                          EXT_COMM_VALUE_LEN)),
                        (UINT1 *) pEvpnVrfNode->au1EthSegRT,
                        EXT_COMM_VALUE_LEN);
                BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo)++;
                break;
            }
            pEvpnVrfNode = RBTreeGetNext (gBgpCxtNode[u4VrfId]->EvpnRoutes,
                                          (tBgp4RBElem *) pEvpnVrfNode, NULL);
        }
    }
    else
    {
        pEvpnVrfNode =
            RBTreeGet (gBgpCxtNode[u4VrfId]->EvpnRoutes,
                       (tBgp4RBElem *) & EvpnVrfNode);
        if (pEvpnVrfNode != NULL)
        {
            u4Count = TMO_SLL_Count (&(pEvpnVrfNode->ExportTargets));
            if (u4Count > 0)
            {
                TMO_SLL_Scan (&(pEvpnVrfNode->ExportTargets),
                              pExportTarget, tExtCommProfile *)
                {
                    MEMCPY ((pu1EvpnExtComm +
                             (BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo) *
                              EXT_COMM_VALUE_LEN)),
                            (UINT1 *) pExportTarget->au1ExtComm,
                            EXT_COMM_VALUE_LEN);
                    BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo)++;
                }
            }
        }
        /* MAC Mobility Ext Comm */
        if (BGP4_INFO_ECOMM_ATTR (pRouteProfile->pRtInfo) &&
            BGP4_INFO_ECOMM_ATTR_VAL (pRouteProfile->pRtInfo))
        {
            /* check for the Mac mobility Ext community in route(profile) */
            for (u4Index = 0;
                 u4Index < BGP4_INFO_ECOMM_COUNT (pRouteProfile->pRtInfo);
                 u4Index++)
            {
                MEMCPY (au1ExtComm,
                        (BGP4_INFO_ECOMM_ATTR_VAL (pRouteProfile->pRtInfo) +
                         +(u4Index * EXT_COMM_VALUE_LEN)), EXT_COMM_VALUE_LEN);
                PTR_FETCH2 (u2EcommType, au1ExtComm);

                if (BGP4_IS_MAC_MOB_EXT_COMMUNITY (u2EcommType) == BGP4_TRUE)
                {
                    MEMCPY ((pu1EvpnExtComm +
                             (BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo) *
                              EXT_COMM_VALUE_LEN)), au1ExtComm,
                            EXT_COMM_VALUE_LEN);
                    BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo)++;
                }
            }
        }
        if (pRouteProfile->EvpnNlriInfo.u1RouteType == EVPN_AD_ROUTE)
        {
            if (pEvpnVrfNode != NULL)
            {
                if (MEMCMP (pRouteProfile->EvpnNlriInfo.au1EthernetSegmentID,
                            pEvpnVrfNode->au1EthSegId, MAX_LEN_ETH_SEG_ID) == 0)
                {
                    MEMCPY ((pu1EvpnExtComm +
                             (BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo) *
                              EXT_COMM_VALUE_LEN)),
                            (UINT1 *) pEvpnVrfNode->au1EthAdRT,
                            EXT_COMM_VALUE_LEN);
                    BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo)++;
                }
            }
        }
    }
    if (BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo) > 0)
    {
        if (u4RcvdCount > 0)
        {
            ATTRIBUTE_NODE_FREE (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo));
        }

        ATTRIBUTE_NODE_CREATE (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo));
        if (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo) == NULL)
        {
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1EvpnExtComm);
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                      "\t BGP Extended Community Attribute is NULL !! \n");
            return BGP4_FAILURE;
        }
        MEMCPY (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo), pu1EvpnExtComm,
                BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo) * EXT_COMM_VALUE_LEN);
        BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_ECOMM_MASK;
    }
    MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1EvpnExtComm);
    return BGP4_SUCCESS;

}

/********************************************************************/
/* Function Name   : Bgp4EvpnInit                                   */
/* Description     : Allocates memory for the pools used in         */
/*                   EVPN route processing and Initializes          */
/*                   global variables                               */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Global Variables Referred : BGP4_EVPN_GLOBAL_PARAMS              */
/* Global variables Modified : BGP4_EVPN_GLOBAL_PARAMS              */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   :  BGP4_SUCCESS or BGP4_FAILURE        */
/********************************************************************/
INT4
Bgp4EvpnInit (VOID)
{
    /* Initialize the global variables here */
    TMO_SLL_Init (BGP4_EVPN_RR_TARGETS_SET);
    TMO_SLL_Init (BGP4_EVPN_VRF_STS_CHNG_LIST);
    /* Initialize vpnv4 feasible/withdrawn route list */
    BGP4_EVPN_RR_TARGETS_INTERVAL = 0;
    BGP4_EVPN_RR_TARGETS_SET_CNT = 0;

    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name : Bgp4EvpnIndicateRtToPeers                                */
/* Description   : This function is called to get the EVPN routes for the   */
/*                 VRF and send the routes to PE peers. If EVPN routes are  */
/*                 not available, get the routes for the VRF, convert       */
/*                 them into EVPN routes and send the routes to PE peers   */
/* Input(s)      : u4Context  - VRF number                                  */
/* Output(s)     : None                                                     */
/* Return(s)     : None                                                     */
/****************************************************************************/
VOID
Bgp4EvpnIndicateRtToPeers (UINT4 u4Context)
{
    tEvpnVrfNode       *pEvpnVrfNode = NULL;

    pEvpnVrfNode = RBTreeGetFirst (gBgpCxtNode[u4Context]->EvpnRoutes);

    while (pEvpnVrfNode != NULL)
    {
        if (pEvpnVrfNode->u1EvpnStatus == BGP4_EVPN_VRF_UP)
        {
            Bgp4SendEvpnRtsToPeer (u4Context, pEvpnVrfNode->u4VnId);
        }
        pEvpnVrfNode = RBTreeGetNext (gBgpCxtNode[u4Context]->EvpnRoutes,
                                      (tBgp4RBElem *) pEvpnVrfNode, NULL);
    }
}

/******************************************************************************/
/* Function Name : Bgp4ProcessMacMobDupTimerExpiry                            */
/* Description   : This function is the called from the Timer Expiry handler, */
/*                 on expiry of the following timer -                         */
/*                       BGP4_MAC_MOB_DUP_TIMER                               */
/* Input(s)     : ID of the expired timer (u4TimerId),                        */
/*                Router profile Information (u4Ref).                         */
/* Output(s)    : None.                                                       */
/* Return(s)    : BGP4_SUCCESS/BGP4_FAILURE.                                  */
/******************************************************************************/
INT4
Bgp4ProcessMacMobDupTimerExpiry (UINT4 u4TimerId, FS_ULONG u4Tmr)
{
    tBgpMacDup         *pBgpMacDup = NULL;
    tBgp4AppTimer      *pBgpTmr = NULL;
    tRouteProfile      *pRtProfileList = NULL;
    tRouteProfile      *pTempRtProfile = NULL;
    VOID               *pRibNode = NULL;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT2               u2EcommType = 0;
    UINT4               u4MacSeqNum = 0;
    UINT4               u4Index = 0;
    UINT4               u4Context = 0;
    INT4                i4Status = 0;
    BOOL1               bIsMacRtAvailable = BGP4_FALSE;
    UINT1               au1MacStr[EVPN_MAC_STR_LEN];

    MEMSET (au1ExtComm, 0, EXT_COMM_VALUE_LEN);
    MEMSET (au1MacStr, 0, EVPN_MAC_STR_LEN);
    pBgpMacDup = (tBgpMacDup *) u4Tmr;
    u4Context = pBgpMacDup->u4VrfId;

    if (u4TimerId == BGP4_MAC_MOB_DUP_TIMER)
    {
        BGP4_TRC (NULL,
                  BGP4_TRC_FLAG, BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMac Mobility Duplication Timer Expired.\n");
        Bgp4GetRibNode (u4Context, CAP_MP_L2VPN_EVPN, &pRibNode);
        if (pRibNode != NULL)
        {
            i4Status =
                Bgp4RibhGetFirstEntry (CAP_MP_L2VPN_EVPN, &pRtProfileList,
                                       &pRibNode, TRUE);
            if (i4Status == BGP4_SUCCESS)
            {
                for (;;)
                {
                    pTempRtProfile = pRtProfileList;
                    u4Context = BGP4_RT_CXT_ID (pTempRtProfile);
                    while (pTempRtProfile)
                    {
                        if (BGP4_RT_SAFI_INFO (pTempRtProfile) ==
                            BGP4_INET_SAFI_EVPN)
                        {
                            if ((BGP4_EVPN_VNID (pTempRtProfile) ==
                                 pBgpMacDup->u4VnId)
                                &&
                                (MEMCMP
                                 (pBgpMacDup->MacAddress,
                                  BGP4_EVPN_MACADDR (pTempRtProfile),
                                  sizeof (tMacAddr)) == 0))
                            {
                                bIsMacRtAvailable = BGP4_TRUE;
                                break;
                            }
                        }
                        pTempRtProfile = BGP4_RT_NEXT (pTempRtProfile);
                    }
                    if (bIsMacRtAvailable == BGP4_TRUE)
                    {
                        break;
                    }
                    i4Status = Bgp4RibhGetNextEntry (pRtProfileList, u4Context,
                                                     &pRtProfileList, &pRibNode,
                                                     TRUE);
                    if (i4Status == BGP4_FAILURE)
                    {
                        break;
                    }
                }
            }
        }
        if (bIsMacRtAvailable == BGP4_TRUE)
        {
            if (BGP4_INFO_ECOMM_ATTR (pTempRtProfile->pRtInfo) != NULL)
            {
                /* check for the Mac mobility Ext community in route(profile) */
                for (u4Index = 0;
                     u4Index < BGP4_INFO_ECOMM_COUNT (pTempRtProfile->pRtInfo);
                     u4Index++)
                {
                    MEMCPY (au1ExtComm,
                            (BGP4_INFO_ECOMM_ATTR_VAL (pTempRtProfile->pRtInfo)
                             + +(u4Index * EXT_COMM_VALUE_LEN)),
                            EXT_COMM_VALUE_LEN);
                    PTR_FETCH2 (u2EcommType, au1ExtComm);
                    if (BGP4_IS_MAC_MOB_EXT_COMMUNITY (u2EcommType) ==
                        BGP4_TRUE)
                    {
                        MEMCPY (&u4MacSeqNum, &(au1ExtComm[4]), sizeof (UINT4));
                        u4MacSeqNum = OSIX_HTONL (u4MacSeqNum);
                        break;
                    }
                }
            }
            if ((u4MacSeqNum - pBgpMacDup->u4SeqNolastExp) >=
                BGP4_GLB_MAX_MAC_MOVES)
            {
                /* Alert BGP */
                /* Stop the Timer */
                pBgpMacDup->tMacMobDupTmr.u4Flag = BGP4_INACTIVE;
                BGP4_EVPN_MAC_DUPLICATE (pTempRtProfile) = BGP4_TRUE;
                CliMacToStr (pBgpMacDup->MacAddress, au1MacStr);
                BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                               BGP4_ALL_FAILURE_TRC | BGP4_EVENTS_TRC,
                               BGP4_MOD_NAME,
                               "\tBgp4ProcessMacMobDupTimerExpiry(): Mac:%s Duplication  occured.\n",
                               au1MacStr);
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, BGP4_SYSLOG_ID,
                              "Bgp4ProcessMacMobDupTimerExpiry: Mac:%s Duplication occured.\r\n",
                              au1MacStr));
                pBgpTmr = &(pBgpMacDup->tMacMobDupTmr);
                pBgpTmr->Tmr.u4Data = u4Tmr;
                RBTreeRem (gBgpCxtNode[u4Context]->BgpMacDupTimer,
                           (tRBElem *) pBgpMacDup);
                MemReleaseMemBlock (BGP4_MAC_DUP_TIMER_MEM_POOL_ID,
                                    (UINT1 *) pBgpMacDup);
            }
            else
            {
                pBgpMacDup->u4SeqNolastExp = u4MacSeqNum;
                pBgpTmr = &(pBgpMacDup->tMacMobDupTmr);
                pBgpTmr->Tmr.u4Data = (FS_ULONG) ((VOID *) pBgpMacDup);
                BGP4_TIMER_START (BGP4_TIMER_LISTID, &pBgpTmr->Tmr,
                                  BGP4_GLB_MAC_DUP_TIMER_INTERVAL);
            }
        }
        else
        {
            pBgpMacDup->tMacMobDupTmr.u4Flag = BGP4_INACTIVE;
            pBgpTmr = &(pBgpMacDup->tMacMobDupTmr);
            pBgpTmr->Tmr.u4Data = u4Tmr;
            RBTreeRem (gBgpCxtNode[u4Context]->BgpMacDupTimer,
                       (tRBElem *) pBgpMacDup);
            MemReleaseMemBlock (BGP4_MAC_DUP_TIMER_MEM_POOL_ID,
                                (UINT1 *) pBgpMacDup);
        }
    }
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name   : BgpMacDupTimerTblCmpFunc                               */
/* Description     : user compare function for Evpn Route TBL RBTrees       */
/* Input(s)        : Two RBTree Nodes be compared                           */
/* Output(s)       : None                                                   */
/* Returns         : 1/(-1)/0                                               */
/****************************************************************************/
INT4
BgpMacDupTimerTblCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tBgpMacDup         *pBgpMacDup1 = (tBgpMacDup *) pRBElem1;
    tBgpMacDup         *pBgpMacDup2 = (tBgpMacDup *) pRBElem2;
    if (pBgpMacDup1->u4VrfId > pBgpMacDup2->u4VrfId)
    {
        return 1;
    }
    else if (pBgpMacDup1->u4VrfId < pBgpMacDup2->u4VrfId)
    {
        return -1;
    }
    if (pBgpMacDup1->u4VnId > pBgpMacDup2->u4VnId)
    {
        return 1;
    }
    else if (pBgpMacDup1->u4VnId < pBgpMacDup2->u4VnId)
    {
        return -1;
    }
    if ((MEMCMP
         (pBgpMacDup1->MacAddress, pBgpMacDup2->MacAddress,
          sizeof (tMacAddr))) > 0)
    {
        return 1;
    }
    else if ((MEMCMP
              (pBgpMacDup1->MacAddress, pBgpMacDup2->MacAddress,
               sizeof (tMacAddr))) < 0)
    {
        return -1;
    }
    return 0;
}

/*****************************************************************************/
/* Function Name : Bgp4EvpnClearMacMobDupTimers                              */
/* Description   : This function stops the mac mobility Timers if running    */
/*               : and deletes the Timer entry from RBTree.                  */
/* Input(s)      : UINT4 u4Context.                                          */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS  if the operation is successful,             */
/*                 BGP4_FAILURE if it fails.                                 */
/*****************************************************************************/
INT4
Bgp4EvpnClearMacMobDupTimers (UINT4 u4Context)
{
    tBgpMacDup          BgpMacDup;
    tBgpMacDup         *pBgpMacDup = NULL;
    tBgpMacDup         *pBgpMacDupNext = NULL;
    tBgp4AppTimer      *pBgpTmr = NULL;

    MEMSET (&BgpMacDup, 0, sizeof (tBgpMacDup));
    /* Stop the Mac Duplication Timer if Running */
    BgpMacDup.u4VrfId = u4Context;
    pBgpMacDup = RBTreeGetNext (gBgpCxtNode[u4Context]->BgpMacDupTimer,
                                (tRBElem *) & BgpMacDup, NULL);
    while ((pBgpMacDup != NULL) && (pBgpMacDup->u4VrfId == u4Context))
    {
        /* Mac Duplication Timer is Running */
        pBgpMacDupNext = RBTreeGetNext (gBgpCxtNode[u4Context]->BgpMacDupTimer,
                                        (tRBElem *) pBgpMacDup, NULL);
        pBgpMacDup->tMacMobDupTmr.u4Flag = BGP4_INACTIVE;
        pBgpTmr = &(pBgpMacDup->tMacMobDupTmr);
        pBgpTmr->Tmr.u4Data = (FS_ULONG) ((VOID *) pBgpMacDup);
        BGP4_TIMER_STOP (BGP4_TIMER_LISTID, &pBgpTmr->Tmr);
        RBTreeRem (gBgpCxtNode[u4Context]->BgpMacDupTimer,
                   (tRBElem *) pBgpMacDup);
        MemReleaseMemBlock (BGP4_MAC_DUP_TIMER_MEM_POOL_ID,
                            (UINT1 *) pBgpMacDup);
        pBgpMacDup = pBgpMacDupNext;
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name : Bgp4EvpnGetAllRoutes                             */
/* Description   : Sends Mac Address Routes, Ethernet Segment Routes*/
/*                 and AD Routes to VXLAN                           */
/* Input(s)      : u4VrfId - vrf id                                 */
/*               : u4VNId  - VN id                                  */
/* Output(s)     : None.                                            */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4EvpnGetAllRoutes (UINT4 u4VrfId, UINT4 u4VnId)
{
    tRouteProfile      *pEvpnRtProfile = NULL;
    tEvpnVrfNode        EvpnVrfNode;
    tEvpnVrfNode       *pEvpnVrfNode = NULL;
    INT4                i4RetVal = BGP4_FAILURE;

    MEMSET (&EvpnVrfNode, 0, sizeof (tEvpnVrfNode));
    EvpnVrfNode.u4VnId = u4VnId;
    pEvpnVrfNode =
        RBTreeGet (gBgpCxtNode[u4VrfId]->EvpnRoutes, (tRBElem *) & EvpnVrfNode);
    if (pEvpnVrfNode != NULL)
    {
        if (pEvpnVrfNode->u1EvpnStatus == BGP4_EVPN_VRF_UP)
        {
            i4RetVal = Bgp4RcvdPADBGetFirstBestRoute (u4VrfId, &pEvpnRtProfile,
                                                      BGP4_INET_AFI_L2VPN,
                                                      BGP4_INET_SAFI_EVPN);
            while (pEvpnRtProfile != NULL)
            {
                if ((pEvpnRtProfile->EvpnNlriInfo.u4VnId != u4VnId) ||
                    (BGP4_RT_PROTOCOL (pEvpnRtProfile) != BGP_ID))
                {
                    if (Bgp4RcvdPADBGetNextBestRoute
                        (pEvpnRtProfile, &pEvpnRtProfile) == BGP4_FAILURE)
                    {
                        break;
                    }
                    continue;
                }
                i4RetVal = Bgp4EvpnCreateAdvtMsgforVXLAN (pEvpnRtProfile);
                if (Bgp4RcvdPADBGetNextBestRoute
                    (pEvpnRtProfile, &pEvpnRtProfile) == BGP4_FAILURE)
                {
                    break;
                }
            }
        }
    }

    UNUSED_PARAM (i4RetVal);
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name : Bgp4AttrFillEvpnNextHop                          */
/* Description   : Fill the Next Hop Attribute for EVPN Route       */
/* Input(s)      : Route profile which is going to be advertised    */
/*                 (pRtProfile)                                     */
/*               : Peer Information (pPeerInfo)                     */
/* Output(s)     : Updated BGP4-INFO to be advertised (pAdvtBgpInfo)*/
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4AttrFillEvpnNextHop (tRouteProfile * pRtProfile, tBgp4Info * pAdvtBgpInfo,
                         tBgp4PeerEntry * pPeerInfo)
{
    tBgp4Info          *pBgp4Info = NULL;
    tNetAddress         LocalAddr;
    tAddrPrefix         RemAddr;
    INT4                i4Ret = BGP4_FAILURE;

    pBgp4Info = BGP4_RT_BGP_INFO (pRtProfile);

    Bgp4InitAddrPrefixStruct (&(BGP4_INFO_NEXTHOP_INFO (pAdvtBgpInfo)),
                              BGP4_INET_AFI_IPV4);
    Bgp4CopyAddrPrefixStruct (&RemAddr, BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo));

    if ((BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) > 0) &&
        (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID))
    {
        PTR_FETCH_4 (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                     BGP4_INFO_NEXTHOP (pBgp4Info));
        BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_NEXT_HOP_MASK;
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_EVPN_TRC, BGP4_MOD_NAME,
                       "\tPEER %s :  Nexthop %s Attribute is successfully filled for"
                       "EVPN route\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))),
                       Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_INFO_NEXTHOP_INFO
                                         (pAdvtBgpInfo))));
        return BGP4_SUCCESS;

    }
    i4Ret = Bgp4GetLocalAddrForPeer (pPeerInfo, RemAddr, &LocalAddr, BGP4_TRUE);
    if (i4Ret == BGP4_FAILURE)
    {
        /* Unable to find the corresponding local address */
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_VPLS_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s : Failure in finding the corresponding local address "
                       "from the peer.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))));
        return BGP4_FAILURE;
    }
    PTR_FETCH_4 (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                 BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalAddr.NetAddr));
    BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_NEXT_HOP_MASK;
    BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                   BGP4_TRC_FLAG, BGP4_EVPN_TRC, BGP4_MOD_NAME,
                   "\tPEER %s :  Nexthop %s Attribute is successfully filled for"
                   "EVPN route\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo))),
                   Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_INFO_NEXTHOP_INFO (pAdvtBgpInfo))));
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name : Bgp4CheckandProcessMacDup                        */
/* Description   : Checks if same Mac is present in the RIB and     */
/*                 Duplication of Mac is done.                      */
/* Input(s)      : Route profile which is going to be advertised    */
/*                 (pRtProfile)                                     */
/* Output(s)     : None                                             */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4CheckandProcessMacDup (tRouteProfile * pRtProfile)
{

    VOID               *pRibNode = NULL;
    UINT4               u4Index = 0;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT2               u2EcommType = 0;
    UINT4               u4MacSeqNumExist = 0;
    UINT4               u4MacSeqNum = 0;
    tRouteProfile      *pRtProfileList = NULL;
    tRouteProfile      *pTempRtProfile = NULL;
    UINT4               u4Context = 0;
    BOOL1               bIsMacRtAvailable = BGP4_FALSE;
    tBgp4AppTimer      *pBgpTmr = NULL;
    tBgpMacDup         *pBgpMacDup = NULL;
    UINT1               au1TempAddress[BGP4_MAX_INET_ADDRESS_LEN];
    UINT4               u4BgpId = 0;
    INT4                i4Status = BGP4_FAILURE;
    tBgpMacDup          BgpMacDupDel;
    tBgpMacDup         *pBgpMacDupDel = NULL;
    BOOL1               bStaticMacExtFlag = BGP4_FALSE;    /* Check for Static MAC */

    MEMSET (au1ExtComm, 0, EXT_COMM_VALUE_LEN);
    MEMSET (au1TempAddress, 0, BGP4_MAX_INET_ADDRESS_LEN);
    MEMSET (&BgpMacDupDel, 0, sizeof (tBgpMacDup));

    u4Context = BGP4_RT_CXT_ID (pRtProfile);
    if (BGP4_INFO_ECOMM_ATTR (pRtProfile->pRtInfo) != NULL)
    {
        /* check for the Mac mobility Ext community in
         * recieved route(profile) */
        for (u4Index = 0; u4Index < BGP4_INFO_ECOMM_COUNT
             (pRtProfile->pRtInfo); u4Index++)
        {
            MEMCPY (au1ExtComm,
                    (BGP4_INFO_ECOMM_ATTR_VAL (pRtProfile->pRtInfo) +
                     +(u4Index * EXT_COMM_VALUE_LEN)), EXT_COMM_VALUE_LEN);
            PTR_FETCH2 (u2EcommType, au1ExtComm);
            if (BGP4_IS_MAC_MOB_EXT_COMMUNITY (u2EcommType) == BGP4_TRUE)
            {
                /* Check for Static MAC: Get static flag and fill the same */
                if (au1ExtComm[2] == EVPN_BGP4_STATIC_MAC)
                {
                    BGP4_EVPN_MAC_TYPE (pRtProfile) = EVPN_BGP4_STATIC_MAC;
                }
                else
                {
                    MEMCPY (&u4MacSeqNum, &(au1ExtComm[4]), sizeof (UINT4));
                    u4MacSeqNum = OSIX_HTONL (u4MacSeqNum);
                }
                break;
            }
        }
    }

    /* Retrieve the Existing route profile with the same MAC */
    Bgp4GetRibNode (u4Context, CAP_MP_L2VPN_EVPN, &pRibNode);
    if (pRibNode != NULL)
    {
        i4Status = Bgp4RibhGetFirstEntry (CAP_MP_L2VPN_EVPN,
                                          &pRtProfileList, &pRibNode, TRUE);
        if (i4Status == BGP4_SUCCESS)
        {
            for (;;)
            {
                pTempRtProfile = pRtProfileList;
                u4Context = BGP4_RT_CXT_ID (pTempRtProfile);
                while (pTempRtProfile)
                {
                    if (BGP4_RT_SAFI_INFO (pTempRtProfile) ==
                        BGP4_INET_SAFI_EVPN)
                    {
                        if ((BGP4_EVPN_VNID (pTempRtProfile) ==
                             BGP4_EVPN_VNID (pRtProfile)) &&
                            (MEMCMP (BGP4_EVPN_MACADDR (pRtProfile),
                                     BGP4_EVPN_MACADDR (pTempRtProfile),
                                     sizeof (tMacAddr)) == 0))
                        {
                            bIsMacRtAvailable = BGP4_TRUE;
                            break;
                        }
                    }
                    pTempRtProfile = BGP4_RT_NEXT (pTempRtProfile);
                }
                if (bIsMacRtAvailable == BGP4_TRUE)
                {
                    break;
                }
                i4Status = Bgp4RibhGetNextEntry (pRtProfileList,
                                                 u4Context,
                                                 &pRtProfileList,
                                                 &pRibNode, TRUE);
                if (i4Status == BGP4_FAILURE)
                {
                    break;
                }
            }
        }
    }

    if (bIsMacRtAvailable == BGP4_TRUE)
    {
        /* to retrieve the BGP-ID */
        if (BGP4_RT_PROTOCOL (pTempRtProfile) == LOCAL_ID)
        {
            u4BgpId = OSIX_HTONL (BGP4_LOCAL_BGP_ID
                                  (BGP4_RT_CXT_ID (pTempRtProfile)));
            MEMCPY (au1TempAddress, &u4BgpId, sizeof (UINT4));
            if (BGP4_EVPN_MAC_TYPE (pTempRtProfile) == EVPN_BGP4_STATIC_MAC)
            {
                if ((BGP4_EVPN_MAC_TYPE (pRtProfile) == EVPN_BGP4_STATIC_MAC)
                    && (MEMCMP (pRtProfile->pRtInfo->NextHopInfo.au1Address,
                                &au1TempAddress, sizeof (UINT4)) < 0))
                {
                    bStaticMacExtFlag = BGP4_TRUE;
                }
                else
                {
                    return BGP4_FAILURE;
                }
            }
            else
            {
                /* Check for Static MAC: BGP already learnt route for this MAC
                 * as dynamic and now Got connected MAC as static */
                if (BGP4_EVPN_MAC_TYPE (pRtProfile) == EVPN_BGP4_STATIC_MAC)
                {
                    bStaticMacExtFlag = BGP4_TRUE;
                }
            }
        }
        else if (BGP4_RT_PROTOCOL (pTempRtProfile) == BGP_ID)
        {
            MEMCPY (au1TempAddress,
                    pTempRtProfile->pRtInfo->NextHopInfo.au1Address,
                    sizeof (UINT4));
        }
        MEMSET (au1ExtComm, 0, sizeof (EXT_COMM_VALUE_LEN));
        if (BGP4_INFO_ECOMM_ATTR (pTempRtProfile->pRtInfo) != NULL)
        {
            for (u4Index = 0; u4Index < BGP4_INFO_ECOMM_COUNT
                 (pTempRtProfile->pRtInfo); u4Index++)
            {
                MEMCPY (au1ExtComm, (BGP4_INFO_ECOMM_ATTR_VAL
                                     (pTempRtProfile->pRtInfo) +
                                     (u4Index * EXT_COMM_VALUE_LEN)),
                        EXT_COMM_VALUE_LEN);
                PTR_FETCH2 (u2EcommType, au1ExtComm);
                if (BGP4_IS_MAC_MOB_EXT_COMMUNITY (u2EcommType) == BGP4_TRUE)
                {
                    MEMCPY (&u4MacSeqNumExist, &(au1ExtComm[4]),
                            sizeof (UINT4));
                    u4MacSeqNumExist = OSIX_HTONL (u4MacSeqNumExist);
                    break;
                }
            }
        }
    }
    else
    {
        /* Check for Static MAC: If static MAC is newly learnt, update
         * static flag */
        if (BGP4_EVPN_MAC_TYPE (pRtProfile) == EVPN_BGP4_STATIC_MAC)
        {
            bStaticMacExtFlag = BGP4_TRUE;
        }
    }

    /* Mac mobility Ext community exists */
    if ((u4MacSeqNum >= 1) && (bStaticMacExtFlag != BGP4_TRUE))
    {
        /* Stop the Timer if already running */
        BgpMacDupDel.u4VnId = BGP4_EVPN_VNID (pRtProfile);
        MEMCPY (BgpMacDupDel.MacAddress, BGP4_EVPN_MACADDR (pRtProfile),
                sizeof (tMacAddr));
        pBgpMacDupDel =
            RBTreeGet (gBgpCxtNode[u4Context]->BgpMacDupTimer,
                       (tRBElem *) & BgpMacDupDel);
        if ((pBgpMacDupDel == NULL)
            || ((pBgpMacDupDel != NULL) && (u4MacSeqNumExist == 0)))
        {
            if (pBgpMacDupDel != NULL)
            {
                pBgpMacDupDel->tMacMobDupTmr.u4Flag = BGP4_INACTIVE;
                pBgpTmr = &(pBgpMacDupDel->tMacMobDupTmr);
                pBgpTmr->Tmr.u4Data = (FS_ULONG) ((VOID *) pBgpMacDupDel);;
                BGP4_TIMER_STOP (BGP4_TIMER_LISTID, &pBgpTmr->Tmr);
                RBTreeRem (gBgpCxtNode[u4Context]->BgpMacDupTimer,
                           (tRBElem *) pBgpMacDupDel);
                MemReleaseMemBlock (BGP4_MAC_DUP_TIMER_MEM_POOL_ID,
                                    (UINT1 *) pBgpMacDupDel);
                pBgpTmr = NULL;
            }
            pBgpMacDup =
                (tBgpMacDup *) MemAllocMemBlk (BGP4_MAC_DUP_TIMER_MEM_POOL_ID);
            if (pBgpMacDup == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                          BGP4_MOD_NAME,
                          "\tMemory Allocation for Bgp Mac Duplication Timer node FAILED!!!\n");
                return BGP4_FAILURE;
            }
            MEMSET (pBgpMacDup, 0, sizeof (tBgpMacDup));
            MEMSET (&(pBgpMacDup->tMacMobDupTmr), 0, sizeof (tBgp4AppTimer));
            pBgpMacDup->tMacMobDupTmr.u4TimerId = BGP4_MAC_MOB_DUP_TIMER;
            pBgpMacDup->u4VrfId = u4Context;
            pBgpMacDup->u4VnId = BGP4_EVPN_VNID (pRtProfile);
            MEMCPY (pBgpMacDup->MacAddress, BGP4_EVPN_MACADDR (pRtProfile),
                    sizeof (tMacAddr));
            pBgpTmr = &(pBgpMacDup->tMacMobDupTmr);
            pBgpMacDup->tMacMobDupTmr.u4Flag = BGP4_ACTIVE;
            pBgpTmr->Tmr.u4Data = (FS_ULONG) ((VOID *) pBgpMacDup);

            if (RBTreeAdd (gBgpCxtNode[u4Context]->BgpMacDupTimer,
                           (tRBElem *) pBgpMacDup) == RB_FAILURE)
            {
                MemReleaseMemBlock (BGP4_MAC_DUP_TIMER_MEM_POOL_ID,
                                    (UINT1 *) pBgpMacDup);
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC,
                          BGP4_MOD_NAME,
                          "\tRBTree Addition FAILED for MAC Timer entry!!!\n");
                return BGP4_FAILURE;
            }

            BGP4_TIMER_START (BGP4_TIMER_LISTID, &pBgpTmr->Tmr,
                              BGP4_GLB_MAC_DUP_TIMER_INTERVAL);
        }
    }

    if ((bIsMacRtAvailable == BGP4_TRUE) && (bStaticMacExtFlag != BGP4_TRUE))
    {
        /* Duplicated Mac handling
         * Add existing router to the withdraw list */
        if ((BGP4_EVPN_MAC_DUPLICATE (pTempRtProfile) == BGP4_TRUE) ||
            ((BGP4_EVPN_MAC_DUPLICATE (pTempRtProfile) != BGP4_TRUE) &&
             (u4MacSeqNum == u4MacSeqNumExist)))
        {
            if (MEMCMP (au1TempAddress,
                        pRtProfile->pRtInfo->NextHopInfo.au1Address,
                        sizeof (UINT4)) > 0)
            {
                BGP4_EVPN_MAC_DUPLICATE (pRtProfile) =
                    BGP4_EVPN_MAC_DUPLICATE (pTempRtProfile);
                if (BGP4_RT_PROTOCOL (pTempRtProfile) != BGP_ID)
                {
                    BGP4_RT_REF_COUNT (pTempRtProfile)++;
                    BGP4_RT_SET_FLAG (pTempRtProfile, BGP4_RT_WITHDRAWN);
                    Bgp4AddWithdRouteToIntPeerAdvtList (pTempRtProfile);

                    Bgp4RibhDelRtEntry (pTempRtProfile,
                                        pRibNode, u4Context, BGP4_FALSE);
                    Bgp4DshReleaseRtInfo (pTempRtProfile);
                }
            }
            else if (MEMCMP
                     (au1TempAddress,
                      pRtProfile->pRtInfo->NextHopInfo.au1Address,
                      sizeof (UINT4)) < 0)
            {
                /* This may not happen in real, because Peer will not
                 * send an Update message for this MAC if it is
                 * Duplicate and peer-IP is bigger than this node's bgp-ip */
                return (BGP4_FAILURE);
            }
        }
        else
        {
            if (BGP4_RT_PROTOCOL (pTempRtProfile) != BGP_ID)
            {
                BGP4_RT_REF_COUNT (pTempRtProfile)++;
                BGP4_RT_SET_FLAG (pTempRtProfile, BGP4_RT_WITHDRAWN);
                Bgp4AddWithdRouteToIntPeerAdvtList (pTempRtProfile);

                Bgp4RibhDelRtEntry (pTempRtProfile,
                                    pRibNode, u4Context, BGP4_FALSE);
                Bgp4DshReleaseRtInfo (pTempRtProfile);
            }

        }

    }
    /* Check for Static MAC: If learnt MAC is STATIC MAC, stop MAC dup timer
     * and learn this MAC */
    if ((BGP4_EVPN_MAC_TYPE (pRtProfile) == EVPN_BGP4_STATIC_MAC) &&
        (bStaticMacExtFlag == BGP4_TRUE))
    {
        if ((bIsMacRtAvailable == BGP4_TRUE) &&
            (BGP4_RT_PROTOCOL (pTempRtProfile) != BGP_ID))
        {
            BGP4_RT_REF_COUNT (pTempRtProfile)++;
            BGP4_RT_SET_FLAG (pTempRtProfile, BGP4_RT_WITHDRAWN);
            Bgp4AddWithdRouteToIntPeerAdvtList (pTempRtProfile);

            Bgp4RibhDelRtEntry (pTempRtProfile,
                                pRibNode, u4Context, BGP4_FALSE);
            Bgp4DshReleaseRtInfo (pTempRtProfile);
        }
        /* Stop the Dup Timer if already running */
        MEMSET (&BgpMacDupDel, 0, sizeof (tBgpMacDup));
        BgpMacDupDel.u4VnId = BGP4_EVPN_VNID (pRtProfile);
        MEMCPY (BgpMacDupDel.MacAddress, BGP4_EVPN_MACADDR (pRtProfile),
                sizeof (tMacAddr));
        pBgpMacDupDel = NULL;
        pBgpMacDupDel = RBTreeGet (gBgpCxtNode[u4Context]->BgpMacDupTimer,
                                   (tRBElem *) & BgpMacDupDel);
        if (pBgpMacDupDel != NULL)
        {
            pBgpMacDupDel->tMacMobDupTmr.u4Flag = BGP4_INACTIVE;
            pBgpTmr = &(pBgpMacDupDel->tMacMobDupTmr);
            pBgpTmr->Tmr.u4Data = (FS_ULONG) ((VOID *) pBgpMacDupDel);;
            BGP4_TIMER_STOP (BGP4_TIMER_LISTID, &pBgpTmr->Tmr);
            RBTreeRem (gBgpCxtNode[u4Context]->BgpMacDupTimer,
                       (tRBElem *) pBgpMacDupDel);
            MemReleaseMemBlock (BGP4_MAC_DUP_TIMER_MEM_POOL_ID,
                                (UINT1 *) pBgpMacDupDel);
            pBgpTmr = NULL;
        }
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name : Bgp4EvpnLinkMatchingEthRoute                     */
/* Input(s)      : u4Context                                        */
/*                 pu1RTCommVal                                     */
/* Output(s)     : pRtProfile                                       */
/* Returns       : tEvpnVrfNode                                     */
/********************************************************************/
tEvpnVrfNode       *
Bgp4EvpnLinkMatchingEthRoute (UINT4 u4Context,
                              tRouteProfile * pRtProfile, UINT1 *pu1RTCommVal)
{
    tEvpnVrfNode       *pEvpnVrfNode = NULL;

    pEvpnVrfNode = RBTreeGetFirst (gBgpCxtNode[u4Context]->EvpnRoutes);
    while (pEvpnVrfNode != NULL)
    {
        if (MEMCMP ((UINT1 *) pEvpnVrfNode->au1EthSegRT,
                    pu1RTCommVal, EXT_COMM_VALUE_LEN) == 0)
        {
            pRtProfile->u4VrfId = u4Context;
            return pEvpnVrfNode;
        }
        pEvpnVrfNode = RBTreeGetNext (gBgpCxtNode[u4Context]->EvpnRoutes,
                                      (tBgp4RBElem *) pEvpnVrfNode, NULL);
    }
    return NULL;
}

/********************************************************************/
/* Function Name   : Bgp4EvpnRrPrune                               */
/* Description     : This module decrements the route count for the */
/*                   route target to be deleted. If the count       */
/*                   becomes zero, then this will inform BGP to wait*/
/*                   for few hours before deleting this route target*/
/*                   from the set. It marks time stamp when there   */
/*                   are no routes correspond to this route target. */
/*                   This function will be called if the route      */
/*                   received from client is deleted                */
/* Input(s)        : pEvpnRoute - Pointer to route profile          */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS                         */
/********************************************************************/
INT4
Bgp4EvpnRrPrune (tRouteProfile * pEvpnRoute)
{
    tBgp4Info          *pBgp4Info = NULL;
    tBgp4EvpnRrImportTargetInfo *pEvpnRrImportTarget = NULL;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT4               u4Index;
    UINT2               u2EcommType;

    if (pEvpnRoute == NULL)
    {
        return BGP4_SUCCESS;
    }
    pBgp4Info = BGP4_RT_BGP_INFO (pEvpnRoute);
    /* Check whether extended community is received or not along
     * with the path attributes
     */

    if (BGP4_INFO_ECOMM_ATTR (pBgp4Info) == NULL)
    {
        /* No ecomm received, hence return success */
        return BGP4_SUCCESS;
    }

    if ((BGP4_RT_PROTOCOL (pEvpnRoute) != BGP_ID) ||
        (BGP4_PEER_RFL_CLIENT (BGP4_RT_PEER_ENTRY (pEvpnRoute)) != CLIENT))
    {
        /* route is not received from client peer */
        return BGP4_SUCCESS;
    }
    for (u4Index = 0; u4Index < BGP4_INFO_ECOMM_COUNT (pBgp4Info); u4Index++)
    {
        MEMCPY ((UINT1 *) au1ExtComm,
                (UINT1 *) (BGP4_INFO_ECOMM_ATTR_VAL (pBgp4Info) +
                           +(u4Index * EXT_COMM_VALUE_LEN)),
                EXT_COMM_VALUE_LEN);
        PTR_FETCH2 (u2EcommType, au1ExtComm);
        if (BGP4_IS_RT_EXT_COMMUNITY (u2EcommType) == BGP4_TRUE)
        {
            pEvpnRrImportTarget =
                Bgp4EvpnIsRTMatchInRRImportRTList (au1ExtComm);
            if (pEvpnRrImportTarget != NULL)
            {
                /* Decrement the route-count */
                BGP4_EVPN_RR_IMPORT_TARGET_RTCNT (pEvpnRrImportTarget)--;
                if (BGP4_EVPN_RR_IMPORT_TARGET_RTCNT (pEvpnRrImportTarget) == 0)
                {
                    BGP4_EVPN_RR_IMPORT_TARGET_TIMESTAMP (pEvpnRrImportTarget) =
                        Bgp4ElapTime ();
                    /* Inform BGP to start timer to take care of deletion
                     * of route target from the set. If the timer is already
                     * running then no need to do anything.
                     */
                    if (BGP4_EVPN_RR_TARGETS_INTERVAL == 0)
                    {
                        BGP4_EVPN_RR_TARGETS_INTERVAL++;
                    }
                }
            }
        }
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4EvpnCreatMacMobCommunity                  */
/* Description     : This module do the memory allocation for MAC   */
/*                   Mobility extended community with the given     */
/*                   community value.                               */
/* Input(s)        : au1ExtComm - Extended community value          */
/*                   pExtComm - Pointer to ExtComm structure        */
/*                   pBGP4Info - Pointer to BGP4 Info               */
/* Output(s)       : None                                           */
/* Returns                   : BGP4_FAILURE / BGP4_SUCCESS          */
/********************************************************************/
INT4
Bgp4EvpnCreatMacMobCommunity (UINT1 *pu1ExtComm, tExtCommunity * pExtComm,
                              tBgp4Info * pBGP4Info)
{
    EXT_COMMUNITY_NODE_CREATE (pExtComm);
    if (pExtComm == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for Extended Community Attribute "
                  "FAILED!!!\n");
        return BGP4_FAILURE;
    }
    MEMSET (pExtComm, 0, sizeof (tExtCommunity));
    BGP4_INFO_ECOMM_ATTR (pBGP4Info) = pExtComm;
    ATTRIBUTE_NODE_CREATE (BGP4_INFO_ECOMM_ATTR_VAL (pBGP4Info));

    if (BGP4_INFO_ECOMM_ATTR_VAL (pBGP4Info) == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVPN_TRC, BGP4_MOD_NAME,
                  "Memory Allocation for Ext Community Attribute FAILED\n");
        return BGP4_FAILURE;
    }
    MEMCPY ((pBGP4Info->pExtCommunity->pu1EcommVal),
            pu1ExtComm, EXT_COMM_VALUE_LEN);
    BGP4_INFO_ATTR_FLAG (pBGP4Info) |= (BGP4_ATTR_ECOMM_MASK);
    BGP4_INFO_ECOMM_COUNT (pBGP4Info)++;

    return BGP4_SUCCESS;
}
#endif
#endif /* BGEVPNFIL_C */
