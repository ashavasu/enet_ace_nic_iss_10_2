/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4vpn.c,v 1.29.2.1 2018/05/15 12:37:28 siva Exp $
 *
 * Description:
 ********************************************************************/
#ifndef BGVPNFIL_C
#define BGVPNFIL_C

#include "bgp4com.h"
#ifdef L3VPN

/********************************************************************/
/* Function Name   : Bgp4Vpnv4Init                                  */
/* Description     : Allocates memory for the pools used in         */
/*                   VPNv4 route processing and Initializes         */
/*                   global variables                               */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Global Variables Referred : BGP4_VPN4_GLOBAL_PARAMS              */
/* Global variables Modified : BGP4_VPN4_GLOBAL_PARAMS              */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   :  BGP4_SUCCESS or BGP4_FAILURE        */
/********************************************************************/
INT4
Bgp4Vpnv4Init (VOID)
{
    /* Initialize the global variables here */
    TMO_SLL_Init (BGP4_VPN4_RR_TARGETS_SET);
    TMO_SLL_Init (BGP4_VRF_STS_CHNG_LIST);
    TMO_SLL_Init (BGP4_LSP_CHG_LIST);
    /* Initialize vpnv4 feasible/withdrawn route list */
    BGP4_VPN4_FEAS_WDRAW_LIST_FIRST_ROUTE = NULL;
    BGP4_VPN4_FEAS_WDRAW_LIST_LAST_ROUTE = NULL;
    BGP4_VPN4_FEAS_WDRAW_LIST_COUNT = 0;
    BGP4_VPN4_RR_TARGETS_INTERVAL = 0;
    BGP4_VPN4_VRF_SPEC_CNT = 0;
    BGP4_VPN4_RT_INSTALL_VRF_CNT = 0;
    BGP4_VPN4_RR_TARGETS_SET_CNT = 0;

    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4Shutdown                              */
/* Description     : De-allocates memory for the pools used in      */
/*                   Vpnv4 route processing and Initializes         */
/*                   global variables                               */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Global Variables Referred : BGP4_VPN4_GLOBAL_PARAMS              */
/* Global variables Modified : BGP4_VPN4_GLOBAL_PARAMS              */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   :  BGP4_SUCCESS or BGP4_FAILURE        */
/********************************************************************/
INT4
Bgp4Vpnv4Shutdown (VOID)
{
    /* Delete memory pools */
    if (BGP4_VPN4_VRF_NODE_CHG_MEMPOOL_ID != 0)
    {
        MemDeleteMemPool (BGP4_VPN4_VRF_NODE_CHG_MEMPOOL_ID);
        BGP4_VPN4_VRF_NODE_CHG_MEMPOOL_ID = 0;
    }
    if (BGP4_VPN4_RT_INSTALL_VRF_MEMPOOL_ID != 0)
    {
        MemDeleteMemPool (BGP4_VPN4_RT_INSTALL_VRF_MEMPOOL_ID);
        BGP4_VPN4_RT_INSTALL_VRF_MEMPOOL_ID = 0;
    }
    if (BGP4_VPN4_RR_TARGETS_SET_MEMPOOL_ID != 0)
    {
        MemDeleteMemPool (BGP4_VPN4_RR_TARGETS_SET_MEMPOOL_ID);
        BGP4_VPN4_RR_TARGETS_SET_MEMPOOL_ID = 0;
    }
    if (BGP4_VPN4_LSP_NODE_CHG_MEMPOOL_ID != 0)
    {
        MemDeleteMemPool (BGP4_VPN4_LSP_NODE_CHG_MEMPOOL_ID);
        BGP4_VPN4_LSP_NODE_CHG_MEMPOOL_ID = 0;
    }

    /* Initialize the global variables */
    BGP4_VPN4_RR_TARGETS_INTERVAL = 0;
    BGP4_VPN4_VRF_SPEC_CNT = 0;
    BGP4_VPN4_RT_INSTALL_VRF_CNT = 0;
    BGP4_VPN4_RR_TARGETS_SET_CNT = 0;

    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4MpeProcessVpn4Nexthop                      */
/* Description     : This module processes the received VpnIPv4     */
/*                   Nexthop information in MP_REACH_NLRI path      */
/*                   attribute.It does validation of IPv4 Next hop  */
/*                   and will be stored in path attribute           */
/*                   information structure                          */
/* Input(s)        : pPeerInfo - pointer to the peer information    */
/*                               from which the UPDATE is received  */
/*                   pu1RcvdUpdateMessage - received UPDATE message */
/*                               which contains Nexthop information */
/*                               in MP_REACH_NLRI path attribute    */
/*                   pRtPathAttribInfo - pointer to the path        */
/*                               attribute information structure    */
/* Output(s)       : pRtPathAttribInfo - pointer to the path        */
/*                               attribute information structure    */
/*                               in which received IPv4 Nexthop info*/
/*                               is stored                          */
/*                   pu2LenValidated - pointer to the number of     */
/*                               bytes validated                    */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4MpeProcessVpnv4Nexthop (tBgp4PeerEntry * pPeerInfo,
                            UINT1 *pu1RcvdUpdateMessage,
                            tBgp4Info * pRtPathAttribInfo,
                            UINT2 *pu2LenValidated)
{
    INT4                i4Ret;
    UINT1               u1NexthopLen = 0;

    u1NexthopLen = *(pu1RcvdUpdateMessage);
    if (u1NexthopLen != BGP4_VPN4_PREFIX_LEN)
    {
        BGP4_DBG1 (BGP4_DBG_ALL,
                   "\tBgp4MpeProcessVpnv4Nexthop() : Nexthop is received "
                   "with Wrong Length [%d]\n", u1NexthopLen);
        return BGP4_FAILURE;
    }

    BGP4_MPE_INFO_NEXTHOP_LEN (pRtPathAttribInfo) = (UINT2) (u1NexthopLen -
                                                             BGP4_VPN_RT_DISTING_SIZE);
    pu1RcvdUpdateMessage += BGP4_MPE_NEXTHOP_FIELD_LEN +
        BGP4_VPN_RT_DISTING_SIZE;
    i4Ret = Bgp4MsghValidateIpv4Nexthop (pPeerInfo, pRtPathAttribInfo,
                                         pu1RcvdUpdateMessage);
    if (i4Ret != BGP4_SUCCESS)
    {
        return i4Ret;
    }

    *pu2LenValidated = (UINT2) (u1NexthopLen + BGP4_MPE_NEXTHOP_FIELD_LEN);
    BGP4_DBG1 (BGP4_DBG_ALL,
               "\t\t<vpnv4, unicast> Nexthop - [%s]\n",
               Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP (pRtPathAttribInfo),
                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                (BGP4_INFO_NEXTHOP_INFO (pRtPathAttribInfo))));
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4MpeProcessVpnv4Prefix                      */
/* Description     : This module processes the received Vpnv4 NLRI  */
/*                   present in either MP_REACH_NLRI or             */
/*                   MP_UNREACH_NLRI. It does validation of received*/
/*                   IPv4 NLRI and is stored in the input route     */
/*                   profile.                                       */
/* Input(s)        : pPeerInfo - pointer to the peer information    */
/*                               from which the UPDATE is received  */
/*                   pu1RcvdUpdateMessage - received UPDATE message */
/*                               which contains NLRI information    */
/*                               in MPE path attribute              */
/*                   pRtProfile - pointer to the path route profile */
/*                               information structure              */
/* Output(s)       : pu2NlriLenValidated - pointer to the number of */
/*                                bytes validated                   */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4MpeProcessVpnv4Prefix (UINT1 *pu1RcvdUpdateMessage,
                           tRouteProfile * pRtProfile,
                           UINT2 *pu2NlriLenValidated)
{
    UINT4               u4Prefix = 0;
    UINT2               u2BytesForNlri = 0;
    UINT1               u1Sts = BGP4_FALSE;

    *pu2NlriLenValidated = 0;
    MEMCPY (BGP4_RT_ROUTE_DISTING (pRtProfile), pu1RcvdUpdateMessage,
            BGP4_VPN_RT_DISTING_SIZE);

    BGP4_VPN4_IS_VALID_RD (BGP4_RT_ROUTE_DISTING (pRtProfile), u1Sts);
    if (u1Sts == BGP4_FALSE)
    {
        /*  BGP4_DBG1 (BGP4_DBG_ALL,
           "\tBgp4MpeProcessVpnv4Prefix() : Prefix is received "
           "with Wrong Route Distinguisher[%s]\n",)
         */
        return BGP4_FAILURE;
    }

    u2BytesForNlri = (UINT2) BGP4_PREFIX_BYTES (BGP4_RT_PREFIXLEN (pRtProfile));
    pu1RcvdUpdateMessage += BGP4_VPN_RT_DISTING_SIZE;
    BGP4_GET_PREFIX (pu1RcvdUpdateMessage, &u4Prefix, u2BytesForNlri);

    /* Put the AFI */
    BGP4_RT_AFI_INFO (pRtProfile) = BGP4_INET_AFI_IPV4;
    /* Put the Address Length = 4 */
    BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                       (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)))
        = BGP4_IPV4_PREFIX_LEN;
    /* Put the SAFI */
    BGP4_RT_SAFI_INFO (pRtProfile) = BGP4_INET_SAFI_VPNV4_UNICAST;
    PTR_ASSIGN4 (BGP4_RT_IP_PREFIX (pRtProfile), (u4Prefix));

    if ((Bgp4IsValidAddress
         (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
          (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)), BGP4_TRUE)) != BGP4_TRUE)
    {
        BGP4_DBG1 (BGP4_DBG_ALL,
                   "\tBgp4MpeProcessVpnv4Prefix() : Prefix is received "
                   "with Wrong value [%s]\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_FAILURE;
    }

    *pu2NlriLenValidated = (UINT2) (BGP4_VPN_RT_DISTING_SIZE + u2BytesForNlri);
    BGP4_DBG2 (BGP4_DBG_ALL,
               "\t\t<vpnv4, unicast> NLRI - [%s/%d]\n",
               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                BGP4_RT_AFI_INFO (pRtProfile)),
               BGP4_RT_PREFIXLEN (pRtProfile));
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4ApplyCEToPERTFilter                   */
/* Description     : This module compares the Export Route Target   */
/*                   filter of Vrf to which the CE is associated    */
/*                   with the Route Targets received in Ipv4 routes */
/* Input(s)        :pTsInputRtsList - Pointer to the list of Ipv4   */
/*                  routes reeived from the CE peer.                */
/*                  pPeerEntry - pointer to the peer information    */
/*                       from which the Ipv4 route  is received     */
/* Output(s)       :None                                            */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4Vpnv4ApplyCEToPERTFilter (tTMO_SLL * pTsInputRtsList,
                              tBgp4PeerEntry * pPeerEntry,
                              tTMO_SLL * pTsSelectsRtsList)
{
    tExtCommProfile    *pRTExtComm = NULL;
    tRouteProfile      *pRouteProfile = NULL;
    tRouteProfile      *pVpnRtProfile = NULL;
    tLinkNode          *pLinkNode = NULL;
    tBgp4Info          *pBgp4Info = NULL;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT4               u4Index;
    INT4                i4RetStatus;
    UINT4               u4MemCmpStatus;
    UINT2               u2EcommType;
    UINT1               u1MatchFound = BGP4_ALLOW;
    UINT4               u4Label = 0;

    if (TMO_SLL_Count (pTsInputRtsList) == 0)
    {
        return BGP4_DENY;
    }

    pRouteProfile =
        ((tLinkNode *) TMO_SLL_First (pTsInputRtsList))->pRouteProfile;

    pBgp4Info = BGP4_RT_BGP_INFO (pRouteProfile);
    /* Check whether extended community is received or not along
     * with the path attributes
     */
    if ((BGP4_VPN4_PEER_CERT_STATUS (pPeerEntry) == BGP4_VPN4_CE_RT_SEND) &&
        (BGP4_INFO_ECOMM_ATTR (pBgp4Info) == NULL))
    {
        /* CE is allowed to send some set of route targets, but it didnt
         * send along with the routes. hence return failure.
         */
        TMO_SLL_Scan (pTsInputRtsList, pLinkNode, tLinkNode *)
        {
            pRouteProfile = pLinkNode->pRouteProfile;
            BGP4_RT_SET_FLAG (pRouteProfile, BGP4_RT_FILTERED_INPUT);
        }
        return BGP4_DENY;
    }

    /*Check whether the CE is allowed to send Route Targets or not */
    if (BGP4_VPN4_PEER_CERT_STATUS (pPeerEntry) == BGP4_VPN4_CE_RT_SEND)
    {
        for (u4Index = 0; u4Index <
             BGP4_INFO_ECOMM_COUNT (pBgp4Info); u4Index++)
        {
            MEMCPY ((UINT1 *) au1ExtComm,
                    (UINT1 *) (BGP4_INFO_ECOMM_ATTR_VAL (pBgp4Info) +
                               +(u4Index * EXT_COMM_VALUE_LEN)),
                    EXT_COMM_VALUE_LEN);
            PTR_FETCH2 (u2EcommType, au1ExtComm);
            if (BGP4_IS_RT_EXT_COMMUNITY (u2EcommType) == BGP4_TRUE)
            {
                u1MatchFound = BGP4_DENY;
                /* Check the received route target with the configured
                 * export route target communities of this PE with the VRF
                 * associated with this CE
                 */
                TMO_SLL_Scan (BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS
                              (BGP4_PEER_CXT_ID (pPeerEntry)), pRTExtComm,
                              tExtCommProfile *)
                {
                    u4MemCmpStatus = (UINT4)
                        (MEMCMP ((UINT1 *) pRTExtComm->au1ExtComm,
                                 (UINT1 *) au1ExtComm, EXT_COMM_VALUE_LEN));
                    if (u4MemCmpStatus == 0)
                    {
                        u1MatchFound = BGP4_ALLOW;
                        break;
                    }
                }
                /* Route target community does not match with the
                 * configured export route target list. */
                if (u1MatchFound == BGP4_DENY)
                {
                    break;
                }
            }
        }
    }

    TMO_SLL_Scan (pTsInputRtsList, pLinkNode, tLinkNode *)
    {
        pRouteProfile = pLinkNode->pRouteProfile;
        /* If all the route target communities received from CE are
         * accepted, then convert it to VPNv4 route */
        if ((BGP4_VPN4_PEER_CERT_STATUS (pPeerEntry) ==
             BGP4_VPN4_CE_RT_DONOTSEND) ||
            ((BGP4_VPN4_PEER_CERT_STATUS (pPeerEntry) == BGP4_VPN4_CE_RT_SEND)
             && (u1MatchFound == BGP4_ALLOW)))

        {
            if ((BGP4_VPN4_PEER_CERT_STATUS (pPeerEntry) ==
                 BGP4_VPN4_CE_RT_SEND))
            {
                BGP4_VPN4_RCVD_RT_ECOMM_FLAG (pRouteProfile) = BGP4_TRUE;
            }
            pVpnRtProfile = Bgp4DupCompleteRtProfile (pRouteProfile);

            if (pVpnRtProfile == NULL)
            {
                continue;
            }
            i4RetStatus =
                Bgp4Vpnv4LinkVrfIdToRouteProfile (pVpnRtProfile,
                                                  BGP4_PEER_CXT_ID
                                                  (pPeerEntry));
            if (i4RetStatus == BGP4_FAILURE)
            {
                BGP4_RT_SET_FLAG (pRouteProfile, BGP4_RT_FILTERED_INPUT);
                KW_FALSEPOSITIVE_FIX1 (pVpnRtProfile);
                continue;
            }
            /* Mark the route to be converted to VPNv4 route */
            BGP4_RT_SET_FLAG (pVpnRtProfile, BGP4_RT_CONVERTED_TO_VPNV4);
            /*BGP4_RT_LABEL_ALLOC_POLICY (pVpnRtProfile) =
               BGP4_LABEL_ALLOC_POLICY (0); */
            BGP4_RT_SAFI_INFO (pVpnRtProfile) = BGP4_INET_SAFI_VPNV4_UNICAST;
            MEMCPY (BGP4_RT_ROUTE_DISTING (pVpnRtProfile),
                    BGP4_VPN4_VRF_SPEC_ROUTE_DISTING (BGP4_PEER_CXT_ID
                                                      (pPeerEntry)),
                    BGP4_VPN4_ROUTE_DISTING_SIZE);
            pVpnRtProfile->pBgpCxtNode = Bgp4GetContextEntry (0);
            if ((BGP4_LABEL_ALLOC_POLICY (0) ==
                 BGP4_PERVRF_LABEL_ALLOC_POLICY) &&
                (BGP4_VPNV4_LABEL (BGP4_PEER_CXT_ID (pPeerEntry)) != 0))
            {
                u4Label = BGP4_VPNV4_LABEL (BGP4_PEER_CXT_ID (pPeerEntry));
            }
            else if (Bgp4GetBgp4Label (&u4Label) == BGP4_FAILURE)
            {
                KW_FALSEPOSITIVE_FIX1 (pVpnRtProfile);
                continue;
            }
            BGP4_RT_LABEL_CNT (pVpnRtProfile)++;
            BGP4_RT_LABEL_INFO (pVpnRtProfile) = u4Label;
            if (BGP4_LABEL_ALLOC_POLICY (0) == BGP4_PERVRF_LABEL_ALLOC_POLICY)
            {
                BGP4_VPNV4_LABEL (BGP4_PEER_CXT_ID (pPeerEntry)) = u4Label;
            }
            Bgp4DshAddRouteToList (pTsSelectsRtsList, pVpnRtProfile, 0);
        }
        /* If any of the received route target community is not matched
         * and CE is allowed to send RTs, then filter these routes */
        else
        {
            BGP4_RT_SET_FLAG (pRouteProfile, BGP4_RT_FILTERED_INPUT);
        }
    }
    if (TMO_SLL_Count (pTsSelectsRtsList))
    {
        TMO_SLL_Scan (pTsSelectsRtsList, pLinkNode, tLinkNode *)
        {
            pRouteProfile = pLinkNode->pRouteProfile;
            Bgp4DshAddRouteToList (pTsInputRtsList, pRouteProfile, 0);
        }
        KW_FALSEPOSITIVE_FIX1 (pVpnRtProfile);
        return BGP4_ALLOW;
    }
    KW_FALSEPOSITIVE_FIX1 (pVpnRtProfile);
    return BGP4_DENY;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4ApplyImporRTFilter                    */
/* Description     : This module applies the import Route Target    */
/*                   filter of each Vrf on the Route Targets received*/
/*                  with the Vpnv4 routes from the PE peers.        */
/* Input(s)        : pInputRtsList - Pointer to the list of Vpnv4   */
/*                   routes reeived from the PE peer.               */
/*                   pPeerInfo- pointer to the peer information     */
/*                       from which the Vpnv4 route  is received    */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4Vpnv4ApplyImportRTFilter (tTMO_SLL * pInputRtsList,
                              tBgp4PeerEntry * pPeerInfo)
{
    tLinkNode          *pLinkNode = NULL;
    tRouteProfile      *pVpnv4Route = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tBgp4Info          *pBgp4Info = NULL;
    tBgp4RrImportTargetInfo *pRrImportTarget = NULL;
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT4               u4AsafiMask;
    UINT4               u4Index;
    INT4                i4RetStatus;
    UINT2               u2EcommType;
    UINT1               u1RrRTCommAdded = BGP4_FALSE;
    UINT1               u1RTCommInRRList = 0;
    UINT4               u4VrfId;

    if (TMO_SLL_Count (pInputRtsList) == 0)
    {
        return BGP4_SUCCESS;
    }

    pVpnv4Route = ((tLinkNode *) TMO_SLL_First (pInputRtsList))->pRouteProfile;

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pVpnv4Route),
                           BGP4_RT_SAFI_INFO (pVpnv4Route), u4AsafiMask);
    if (u4AsafiMask != CAP_MP_VPN4_UNICAST)
    {
        /* The routes list is not VPNv4-unicast family. return success */
        return BGP4_SUCCESS;
    }
    BGP4_VPN4_GET_PROCESS_VRFID (pVpnv4Route, u4VrfId);
    pBgp4Info = BGP4_RT_BGP_INFO (pVpnv4Route);

    if (BGP4_VPN4_PEER_ROLE (pPeerInfo) == BGP4_VPN4_CE_PEER)
    {
        i4RetStatus = Bgp4Vpnv4VrfToVrfRedistribute (pInputRtsList);
        if (i4RetStatus == BGP4_FAILURE)
        {
            return BGP4_FAILURE;
        }
    }
    else if (BGP4_VPN4_PEER_ROLE (pPeerInfo) == BGP4_VPN4_PE_PEER)
    {
        /* Check whether extended community is received or not along
         * with the path attributes
         */
        if (BGP4_INFO_ECOMM_ATTR (pBgp4Info) == NULL)
        {
            /* No route-target community is received along with vpnv4 routes.
             * hence filter those routes
             */
            TMO_SLL_Scan (pInputRtsList, pLinkNode, tLinkNode *)
            {
                pVpnv4Route = pLinkNode->pRouteProfile;
                BGP4_RT_SET_FLAG (pVpnv4Route, BGP4_RT_FILTERED_INPUT);
            }
            return BGP4_FAILURE;
        }

        for (u4Index = 0; u4Index < BGP4_INFO_ECOMM_COUNT (pBgp4Info);
             u4Index++)
        {
            MEMCPY ((UINT1 *) au1ExtComm,
                    (UINT1 *) (BGP4_INFO_ECOMM_ATTR_VAL (pBgp4Info) +
                               +(u4Index * EXT_COMM_VALUE_LEN)),
                    EXT_COMM_VALUE_LEN);
            PTR_FETCH2 (u2EcommType, au1ExtComm);
            if (BGP4_IS_RT_EXT_COMMUNITY (u2EcommType) == BGP4_TRUE)
            {
                i4RetStatus = Bgp4Vpnv4LinkMatchingImportRTVrfs (au1ExtComm,
                                                                 pVpnv4Route);
                if (i4RetStatus == BGP4_FAILURE)
                {
                    /* Theres something wrong in linking VRF informations
                     * into route profile. hence return failure
                     */
                    return BGP4_FAILURE;
                }
                else if ((i4RetStatus == BGP4_VRF_MATCH_NOT_FOUND) &&
                         (BGP4_RR_CLIENT_CNT (u4VrfId) == 0))
                {
                    /* there is no matching vrf found */
                    continue;
                }

                if (BGP4_RR_CLIENT_CNT (u4VrfId) > 0)
                {
                    pRrImportTarget =
                        Bgp4Vpnv4IsRTMatchInRRImportRTList (au1ExtComm);
                    if (pRrImportTarget != NULL)
                    {
                        /* If the speaker is route-reflector and we received
                         * a matching route taret community from the nonclient 
                         * peer, then apply the filter
                         */
                        if (BGP4_PEER_RFL_CLIENT (pPeerInfo) == NON_CLIENT)
                        {
                            u1RTCommInRRList++;
                        }
                    }
                    else
                    {
                        if (BGP4_PEER_RFL_CLIENT (pPeerInfo) == CLIENT)
                        {
                            /* If the speaker is route-reflector and we 
                             * received a new route target community from 
                             * the client peer, then add into the Import RT set
                             */
                            pRrImportTarget =
                                Bgp4AddNewRTInRRImportList (au1ExtComm);
                            if (pRrImportTarget == NULL)
                            {
                                return BGP4_FAILURE;
                            }
                            u1RrRTCommAdded = BGP4_TRUE;
                            /* Increment the route-count */
                            BGP4_VPN4_RR_IMPORT_TARGET_RTCNT (pRrImportTarget)
                                += TMO_SLL_Count (pInputRtsList);
                            BGP4_VPN4_RR_IMPORT_TARGET_TIMESTAMP
                                (pRrImportTarget) = 0;
                        }
                    }
                }
            }
        }

        if (u1RrRTCommAdded == BGP4_TRUE)
        {
            /* The speaker is route-reflector and the route is received from a
             * client peer. If a new route target is added into the set,
             * then send Route-refresh message to the peers */
            i4RetStatus = Bgp4Vpnv4RrVpnJoin ();
            if (i4RetStatus == BGP4_FAILURE)
            {
                return BGP4_FAILURE;
            }
        }

        /* If route profile doesn't belong to any import route targets of the
         * speaker, then if the speaker is not a route-reflector, filter all
         * the routes, these belong to some other VPN which the speaker is not
         * intended to receive. If the speaker is a route-reflector and the
         * peer is non-client, then apply the filtering of route targets
         * against the stored route-target set received from the client peers
         */
        TMO_SLL_Scan (pInputRtsList, pLinkNode, tLinkNode *)
        {
            pRtProfile = pLinkNode->pRouteProfile;
            if (((TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pVpnv4Route)) ==
                  0) && (BGP4_RR_CLIENT_CNT (u4VrfId) == 0))
                || ((BGP4_RR_CLIENT_CNT (u4VrfId) > 0)
                    && (BGP4_PEER_RFL_CLIENT (pPeerInfo) == NON_CLIENT)
                    && (u1RTCommInRRList == 0)))
            {
                BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_FILTERED_INPUT);
            }
            else
            {
                BGP4_VPN4_RCVD_RT_ECOMM_FLAG (pRtProfile) = BGP4_TRUE;
                if ((pRtProfile == pVpnv4Route) ||
                    ((TMO_SLL_Count
                      (BGP4_VPN4_RT_INSTALL_VRF_LIST (pVpnv4Route)) == 0)))
                {
                    continue;
                }
                TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pVpnv4Route),
                              pRtVrfInfo, tRtInstallVrfInfo *)
                {
                    i4RetStatus =
                        Bgp4Vpnv4LinkVrfIdToRouteProfile (pRtProfile,
                                                          BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID
                                                          (pRtVrfInfo));
                    if (i4RetStatus == BGP4_FAILURE)
                    {
                        BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_FILTERED_INPUT);
                        return BGP4_FAILURE;
                    }
                }
            }
        }
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4VrfToVrfRedistribute                  */
/* Description     : This module applies the redistrubtion of routes*/
/*                   across the VRFs. If the export route targets   */
/*                   of the received VRF matches with the import    */
/*                   import route targets of other VRFs, then       */
/*                   it will install the routes into those VRFs     */
/* Input(s)        : pInputRtsList - Pointer to the list of Vpnv4   */
/*                   routes reeived from the PE peer.               */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4Vpnv4VrfToVrfRedistribute (tTMO_SLL * pInputRtsList)
{
    tRouteProfile      *pVpnv4Route = NULL;
    tBgp4Info          *pBgp4Info = NULL;
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
    tExtCommProfile    *pExtCom = NULL;
    tLinkNode          *pLinkNode = NULL;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT4               u4Index;
    INT4                i4RetStatus;
    UINT2               u2EcommType;
    UINT1               u1Redist = BGP4_FALSE;

    if (TMO_SLL_Count (pInputRtsList) == 0)
    {
        return BGP4_SUCCESS;
    }
    pVpnv4Route = ((tLinkNode *) TMO_SLL_First (pInputRtsList))->pRouteProfile;

    pBgp4Info = BGP4_RT_BGP_INFO (pVpnv4Route);

    if ((BGP4_INFO_ECOMM_ATTR (pBgp4Info) != NULL) &&
        (BGP4_VPN4_RCVD_RT_ECOMM_FLAG (pVpnv4Route) == BGP4_TRUE))
    {
        /* If CE sent some route-targets, then check whether those route
         * targets match with any import targets of other VRFs,
         *       CE1---\
         *              |---PE------
         *       CE2---/
         * If so, then attach those import route targets to the route so 
         * that the route will be installed in both the VRFs
         */
        for (u4Index = 0; u4Index < BGP4_INFO_ECOMM_COUNT (pBgp4Info);
             u4Index++)
        {
            MEMCPY ((UINT1 *) au1ExtComm,
                    (UINT1 *) (BGP4_INFO_ECOMM_ATTR_VAL (pBgp4Info) +
                               +(u4Index * EXT_COMM_VALUE_LEN)),
                    EXT_COMM_VALUE_LEN);
            PTR_FETCH2 (u2EcommType, au1ExtComm);
            if (BGP4_IS_RT_EXT_COMMUNITY (u2EcommType) == BGP4_TRUE)
            {
                i4RetStatus = Bgp4Vpnv4LinkMatchingImportRTVrfs (au1ExtComm,
                                                                 pVpnv4Route);
                if (i4RetStatus == BGP4_FAILURE)
                {
                    return BGP4_FAILURE;
                }
                else if (i4RetStatus == BGP4_SUCCESS)
                {
                    u1Redist = BGP4_TRUE;
                }
            }
        }
    }
    else
    {
        /* If CE did not send any route-targets, then check whether export
         * targets of installed VRF match with import targets of other VRFs,
         *       CE1---\
         *              |---PE------
         *       CE2---/
         * If so, then attach those import route targets to the route so 
         * that the route will be installed in both the VRFs
         */
        TMO_SLL_Scan (BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS
                      (BGP4_RT_CXT_ID (pVpnv4Route)), pExtCom,
                      tExtCommProfile *)
        {
            PTR_FETCH2 (u2EcommType, pExtCom->au1ExtComm);
            if (BGP4_IS_RT_EXT_COMMUNITY (u2EcommType) == BGP4_TRUE)
            {
                i4RetStatus =
                    Bgp4Vpnv4LinkMatchingImportRTVrfs (pExtCom->au1ExtComm,
                                                       pVpnv4Route);
                if (i4RetStatus == BGP4_FAILURE)
                {
                    return BGP4_FAILURE;
                }
                else if (i4RetStatus == BGP4_SUCCESS)
                {
                    u1Redist = BGP4_TRUE;
                }
            }
        }
    }

    /* For all the attached VRF information in the first route,
     * copy the same information to all the other routes present in
     * the list
     */
    if (u1Redist == BGP4_TRUE)
    {
        TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pVpnv4Route), pRtVrfInfo,
                      tRtInstallVrfInfo *)
        {
            TMO_SLL_Scan (pInputRtsList, pLinkNode, tLinkNode *)
            {
                /* skip the first route */
                if (pVpnv4Route == pLinkNode->pRouteProfile)
                {
                    continue;
                }
                i4RetStatus =
                    Bgp4Vpnv4LinkVrfIdToRouteProfile (pLinkNode->pRouteProfile,
                                                      BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID
                                                      (pRtVrfInfo));
                if (i4RetStatus == BGP4_FAILURE)
                {
                    return BGP4_FAILURE;
                }
            }
        }
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4AddNewRTInRRImportList                     */
/* Description     : Adds a new route target into RR targets set    */
/* Input(s)        : pu1RTExtComm - route target value              */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
tBgp4RrImportTargetInfo *
Bgp4AddNewRTInRRImportList (UINT1 *pu1RTExtComm)
{
    tBgp4RrImportTargetInfo *pRrImportTarget = NULL;

    pRrImportTarget =
        Bgp4MemAllocateRrImportTarget (sizeof (tBgp4RrImportTargetInfo));
    if (pRrImportTarget == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tVPN: Creating RR Import Target Node failed\n");
        return (NULL);
    }

    MEMCPY (BGP4_VPN4_RR_IMPORT_TARGET (pRrImportTarget),
            pu1RTExtComm, EXT_COMM_VALUE_LEN);
    TMO_SLL_Add (BGP4_VPN4_RR_TARGETS_SET, (tTMO_SLL_NODE *) pRrImportTarget);
    return (pRrImportTarget);
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4LinkVrfIdToRouteProfile               */
/* Description     : This function links the VRF information into   */
/*                   vrf-install list of route profile for a given  */
/*                   import route target. If the import route target*/
/*                   matches with any VRF, then that will be linked */
/* Input(s)        : pu1RTCommVal - import route target value       */
/*                   pRtProfile  - route information                */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4Vpnv4LinkMatchingImportRTVrfs (UINT1 *pu1RTCommVal,
                                   tRouteProfile * pRtProfile)
{
    tExtCommProfile    *pExtCommProfile = NULL;
    UINT4               u4Context;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT1               u1Linked = BGP4_FALSE;

    /* If the received import route-target matches with any configured
     * import route targets of the VRFs then link that particular
     * VRF information to the route profile.
     */

    i4RetVal = BgpGetFirstActiveContextID (&u4Context);
    while (i4RetVal == SNMP_SUCCESS)
    {
        if ((BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID) &&
            (BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED))
        {
            /* If route-leaking is enabled and incoming route is BGP Route */
            if (BGP4_PEER_CXT_ID (BGP4_RT_PEER_ENTRY (pRtProfile)) == u4Context)
            {
                /* If the vrf-ids do not match, then skip this entry */
                i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
                continue;
            }
        }
        else if ((BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID) &&
                 (BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED))
        {
            /*If route-Leaking is enabled ,and incoming route is Non-BGP route */
            if (BGP4_RT_CXT_ID (pRtProfile) == u4Context)
            {
                /* If the vrf-ids do not match, then skip this entry */
                i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
                continue;

            }

        }
        else
        {
            if ((BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRtProfile))
                 == BGP4_VPN4_CE_PEER) &&
                (BGP4_PEER_CXT_ID (BGP4_RT_PEER_ENTRY (pRtProfile)) ==
                 u4Context))
            {
                /* If the vrf-ids do not match, then skip this entry */
                i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
                continue;
            }
        }

        if (TMO_SLL_Count (BGP4_VPN4_VRF_SPEC_IMPORT_TARGETS (u4Context)) == 0)
        {
            i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
            continue;
        }

        if ((BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4Context) != BGP4_L3VPN_VRF_UP))
        {
            i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
            continue;
        }

        TMO_SLL_Scan (BGP4_VPN4_VRF_SPEC_IMPORT_TARGETS (u4Context),
                      pExtCommProfile, tExtCommProfile *)
        {
            if (MEMCMP ((UINT1 *) pExtCommProfile->au1ExtComm,
                        pu1RTCommVal, EXT_COMM_VALUE_LEN) == 0)
            {
                /* received import route-target matches with the
                 * configured import route-target of this VRF.
                 */
                if ((Bgp4Vpnv4LinkVrfIdToRouteProfile
                     (pRtProfile, u4Context)) == BGP4_FAILURE)
                {
                    return BGP4_FAILURE;
                }
                u1Linked = BGP4_TRUE;
                break;
            }
        }
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
    }

    if (u1Linked == BGP4_FALSE)
    {
        return BGP4_VRF_MATCH_NOT_FOUND;
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4LinkVrfIdToRouteProfile               */
/* Description     : This function links the VRF information into   */
/*                   vrf-install list of route profile for a given  */
/*                   vrf-id                                         */
/* Input(s)        : pRtProfile - route information                 */
/*                   u4VrfId - vrf-id                               */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4Vpnv4LinkVrfIdToRouteProfile (tRouteProfile * pRtProfile, UINT4 u4VrfId)
{
    tRtInstallVrfInfo  *pRtVrfNode = NULL;

    /* Check whether this VRF information is already present in the 
     * route?? If so, then no need to add it again.
     */
    Bgp4Vpnv4GetInstallVrfInfo (pRtProfile, u4VrfId, &pRtVrfNode);
    if (pRtVrfNode != NULL)
    {
        /* The route is already carrying the VRF inforamtion */
        return BGP4_SUCCESS;
    }

    pRtVrfNode =
        Bgp4MemAllocateVpnRtInstallVrfNode (sizeof (tRtInstallVrfInfo));
    if (pRtVrfNode == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tVPN: Creating Installed Vrf Node failed\n");
        return BGP4_FAILURE;
    }

    BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrfNode) = u4VrfId;
    /* Add this VRF information into vrf-install list of the route */
    TMO_SLL_Add (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile),
                 (tTMO_SLL_NODE *) pRtVrfNode);
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4IsSameVrfInfo                         */
/* Description     : Checks whether installed VRF info of input     */
/*                   route profile and the exisiting route profile  */
/*                   if both are having same VRF infos, it returns  */
/*                   TRUE, else FALSE                               */
/* Input(s)        : Input route, existing route                    */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : TRUE or FALSE                        */
/********************************************************************/
INT4
Bgp4Vpnv4IsSameVrfInfo (tRouteProfile * pRtProfile,
                        tRouteProfile * pFndRtProfile)
{
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
    tRtInstallVrfInfo  *pRtVrfNode = NULL;

    if (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pFndRtProfile))
        != TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)))
    {
        return FALSE;
    }

    TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pFndRtProfile),
                  pRtVrfInfo, tRtInstallVrfInfo *)
    {
        Bgp4Vpnv4GetInstallVrfInfo (pRtProfile,
                                    BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID
                                    (pRtVrfInfo), &pRtVrfNode);
        if (pRtVrfNode == NULL)
        {
            /* vrf information is not found */
            return FALSE;
        }
    }
    return TRUE;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4IsRTMatchInRRImportRTList             */
/* Description     : Finds whether a particular route target is     */
/*                   present in RR import targets set. If present,  */
/*                   returns success else failure                   */
/* Input(s)        : pu1RTCommVal - import route target             */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
tBgp4RrImportTargetInfo *
Bgp4Vpnv4IsRTMatchInRRImportRTList (UINT1 *pu1RTCommVal)
{
    tBgp4RrImportTargetInfo *pBgp4RrImportTargetInfo = NULL;

    TMO_SLL_Scan (BGP4_VPN4_RR_TARGETS_SET,
                  pBgp4RrImportTargetInfo, tBgp4RrImportTargetInfo *)
    {
        if ((MEMCMP
             ((UINT1 *) BGP4_VPN4_RR_IMPORT_TARGET (pBgp4RrImportTargetInfo),
              pu1RTCommVal, EXT_COMM_VALUE_LEN)) == 0)
        {
            return (pBgp4RrImportTargetInfo);
        }
    }
    return (NULL);
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4GetInstallVrfInfo                     */
/* Description     : This function gets the installed VRF info      */
/*                   if present from the route profile for a given  */
/*                   VRF identifier                                 */
/* Input(s)        : pRtProfile - route information                 */
/*                   u4VrfId - vrf-id                               */
/* Output(s)       : ppRtVrfInfo - address of Installed VRF info    */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4Vpnv4GetInstallVrfInfo (tRouteProfile * pRtProfile, UINT4 u4VrfId,
                            tRtInstallVrfInfo ** ppRtVrfInfo)
{
    tRtInstallVrfInfo  *pRtVrfNode = NULL;

    *ppRtVrfInfo = NULL;
    if (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) == 0)
    {
        /* Memory for installed vrf list has not been created */
        return BGP4_SUCCESS;
    }
    TMO_SLL_Scan ((BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)), pRtVrfNode,
                  tRtInstallVrfInfo *)
    {
        if (BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrfNode) == u4VrfId)
        {
            *ppRtVrfInfo = pRtVrfNode;
            break;
        }
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4ProcessFeasWdrawRts                   */
/* Description     : This function processes the vpnv4 feasible and */
/*                   withdrawn routes received from peers.          */
/* Input(s)        : None.                                          */
/* Output(s)       : None.                                          */
/* Global Variables Referred : BGP4_VPN4_FEAS_WDRAW_LIST            */
/* Global variables Modified : BGP4_VPN4_FEAS_WDRAW_LIST            */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_TRUE - still routes pending     */
/*                             BGP4_FALSE - routes processing over  */
/********************************************************************/
INT4
Bgp4Vpnv4ProcessFeasWdrawRts (VOID)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tRouteProfile      *pRtProfile = NULL;
    UINT4               u4Vpnv4RtCnt = 0;

    if (BGP4_VPN4_FEAS_WDRAW_LIST_COUNT == 0)
    {
        /* There are no vpnv4 routes for processing, hence return */
        return BGP4_FALSE;
    }

    for (;;)
    {
        if (u4Vpnv4RtCnt >= BGP4_MAX_VPNV4_RTS2PROCESS)
        {
            /* Maximum routes to process reached */
            break;
        }

        pRtProfile = BGP4_VPN4_FEAS_WDRAW_LIST_FIRST_ROUTE;
        if (pRtProfile == NULL)
        {
            /* List is empty. No need of any processing to be done. */
            break;
        }
        BGP4_ASSERT (BGP4_RT_GET_FLAGS (pRtProfile) &
                     BGP4_RT_IN_VPNV4_FEAS_WDRAW_LIST);
        if (bgp4RibLock () != OSIX_SUCCESS)
        {
            return BGP4_TRUE;
        }

        if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
        {
            pPeerEntry = BGP4_RT_PEER_ENTRY (pRtProfile);
            if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                 BGP4_RT_WITHDRAWN) == BGP4_RT_WITHDRAWN)
            {
                /* Route is a withdrawn route from the peer pPeerentry. */
                Bgp4Vpnv4ProcessWithdRoute (pRtProfile, pPeerEntry);
            }
            else
            {
                Bgp4Vpnv4ProcessFeasibleRoute (pRtProfile, pPeerEntry,
                                               BGP4_DFLT_VRFID);
            }
        }
        else
        {
            if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                 BGP4_RT_WITHDRAWN) == BGP4_RT_WITHDRAWN)
            {
                /* Route is a withdrawn route from the peer pPeerentry. */
                Bgp4Vpnv4ProcessNonBgpWithdRoute (pRtProfile, BGP4_DFLT_VRFID);
            }
            else
            {
                Bgp4Vpnv4ProcessNonBgpFeasRoute (pRtProfile, BGP4_DFLT_VRFID);
            }
        }
        bgp4RibUnlock ();

        Bgp4DshDelinkRouteFromList (BGP4_VPN4_FEAS_WDRAW_LIST, pRtProfile,
                                    BGP4_VPN4_FEAS_WDRAW_LIST_INDEX);
        BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_IN_VPNV4_FEAS_WDRAW_LIST);
        u4Vpnv4RtCnt++;
    }

    if (u4Vpnv4RtCnt >= BGP4_MAX_VPNV4_RTS2PROCESS)
    {
        /* Has reached the MAX VPNV4 routes to be processed. Check for
         * Tic Break event. If return value is BGP4_TURE, then the
         * environment may require the CPU. So BGP should relinquish
         * the CPU. */
        if (Bgp4IsCpuNeeded () == BGP4_TRUE)
        {
            /* External events requires CPU. Relinquish CPU. */
            return (BGP4_RELINQUISH);
        }
    }

    if (BGP4_VPN4_FEAS_WDRAW_LIST_COUNT > 0)
    {
        return (BGP4_TRUE);
    }

    return (BGP4_FALSE);
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4ProcessFeasibleRoute                  */
/* Description     : This function processes the vpnv4 feasible     */
/*                   route received from a peer.                    */
/* Input(s)        : None.                                          */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None.                                */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_TRUE or BGP4_FALSE              */
/********************************************************************/
INT4
Bgp4Vpnv4ProcessFeasibleRoute (tRouteProfile * pRtProfile,
                               tBgp4PeerEntry * pPeerEntry, UINT4 u4VrfIdIn)
{
    tTMO_SLL            TsRouteList;
    tTMO_SLL            TsFeasFIBList;
    tTMO_SLL            TsWithFIBList;
    tRouteProfile      *pFndRtProfileList = NULL;
    tRouteProfile      *pRtNext = NULL;
    VOID               *pRibNode = NULL;
    tRouteProfile      *pPeerRoute = NULL;
    tRouteProfile      *pBestRoute = NULL;
    tRouteProfile      *pNewBestRoute = NULL;
    tRouteProfile      *pGetRt = NULL;
    tLinkNode          *pLinkNode = NULL;
    INT4                i4RtFound;
    INT4                i4Ret;
    tRtInstallVrfInfo  *pRtVrfInfo;
    UINT4               u4VrfId;

    BGP4_TRC_ARG5 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC, BGP4_MOD_NAME,
                   "\t Received VPN route %s for processing with nexthop %s "
                   "immediate nexthop %s flags 0x%x and extended flags 0x%x\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP
                                    (BGP4_RT_BGP_INFO (pRtProfile)),
                                    BGP4_INFO_AFI (BGP4_RT_BGP_INFO
                                                   (pRtProfile))),
                   Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                     (pRtProfile))), pRtProfile->u4Flags,
                   pRtProfile->u4ExtFlags);

    /* If VRF route-leaking is enabled ,there will not be any Peer entry
     * so to avoid the crash below check is added*/
    if (BGP4_RT_PEER_ENTRY (pRtProfile) != NULL)
    {
        if (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRtProfile))
            == BGP4_VPN4_CE_PEER)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC, BGP4_MOD_NAME,
                      "\tVRF route-leaking is enabled and there is no any Peer entry\r\n");
            return (BGP4_SUCCESS);
        }
    }
    /* Check whethe the route can be installed.
     * If the route is destined to some VRF which is in meantime deleted,
     * so there is no point in adding the route here
     */
    if (Bgp4Vpnv4IsRouteCanBeProcessed (pRtProfile) == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC, BGP4_MOD_NAME,
                  "\tThe VRF to which route is destined is deleted.\r\n");
        Bgp4Vpnv4DelRouteFromVPNRIB (pRtProfile);
        return (BGP4_SUCCESS);
    }

    TMO_SLL_Init (&TsRouteList);
    TMO_SLL_Init (&TsFeasFIBList);
    TMO_SLL_Init (&TsWithFIBList);

    if (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) == 0)
    {
        if ((BGP4_RR_CLIENT_CNT (0) > 0) &&
            (!(BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_ADVT_NOTTO_INTERNAL)))
        {
            Bgp4AddFeasRouteToIntPeerAdvtList (pRtProfile, NULL);
            return BGP4_SUCCESS;
        }
        return BGP4_FAILURE;
    }

    TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile), pRtVrfInfo,
                  tRtInstallVrfInfo *)
    {
        pFndRtProfileList = NULL;
        u4VrfId = BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrfInfo);

        if ((BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED) &&
            (u4VrfIdIn != BGP4_DFLT_VRFID && u4VrfIdIn != u4VrfId))
        {
            continue;
        }

        if ((BGP4_RT_PEER_ENTRY (pRtProfile) != NULL)
            && (BGP4_PEER_CXT_ID (BGP4_RT_PEER_ENTRY (pRtProfile)) == u4VrfId)
            && (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRtProfile)) ==
                BGP4_VPN4_CE_PEER)
            && (BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED))
        {
            /* For this VRF, route would be added in Trie in Bgp4RibhProcessFeasibleRoute
             * function */
            continue;
        }
        if (BGP4_RR_CLIENT_CNT (u4VrfId) > 0)
        {
            /* The router is Route-Reflector. This may not have any VRFs, so the
             * * list of VRFs could be NULL. hence just append the route into
             * * internal peer advertisement list to advertise to other clients
             * * and non-clients if there are no VRFs configured. Else,
             * * process it normally. If CE is not connected to the Vrf, RR acts
             * * as a route server and the routes will not be added in FIB
             * */
            if (!(BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_ADVT_NOTTO_INTERNAL))
            {
                Bgp4AddFeasRouteToIntPeerAdvtList (pRtProfile, NULL);
            }
            if (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) == 0)
            {
                /* Set the best flag for the route here, because this route is
                 * * not going to be added into FIB and will not be advertised to
                 * * external peers also.
                 * */
                return BGP4_SUCCESS;
            }
        }

        i4RtFound = Bgp4RibhLookupRtEntry (pRtProfile, u4VrfId,
                                           BGP4_TREE_FIND_EXACT,
                                           &pFndRtProfileList, &pRibNode);
        pPeerRoute = NULL;
        if (pFndRtProfileList != NULL)
        {
            /* Route is found in this VRF */
            pPeerRoute =
                Bgp4DshGetPeerVrfRtFromProfileList (pFndRtProfileList,
                                                    pPeerEntry, pRtProfile);
            pBestRoute =
                Bgp4DshGetBestRouteFromProfileList (pFndRtProfileList, u4VrfId);
            /* Check whether the peer route is present or not. */
            if (pPeerRoute != NULL)
            {
                BGP4_RT_GET_NEXT (pFndRtProfileList, u4VrfId, pRtNext);
                if (pRtNext == NULL)
                {
                    /* There is only one route present in the RIB matching
                     * the incoming route and this route is the old route
                     * from peer. */

                    /* Add the replacement route to the
                     * FIB update list and Internal Peer Advertise list. Also
                     * delete the old route from the RIB. */
                    BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG,
                                   BGP4_FDB_TRC | BGP4_EVENTS_TRC,
                                   BGP4_MOD_NAME,
                                   "\tOnly one route %s present in the RIB matching the "
                                   "incoming route %s of VRF ID %d. Deleting the Old route "
                                   "from RIB and adding the replacement route to FIB update list "
                                   "and Internal peer advertisement list.\r\n",
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pPeerRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pPeerRoute)),
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pRtProfile)), u4VrfId);

                    Bgp4DshAddRouteToList (&TsRouteList, pRtProfile, 0);
                    Bgp4DshAddRouteToList (&TsRouteList, pPeerRoute, 0);
                    Bgp4RibhDelRtEntry (pPeerRoute, pRibNode,
                                        u4VrfId, BGP4_TRUE);
                    i4Ret = Bgp4RibhAddRtEntry (pRtProfile, u4VrfId);
                    if (i4Ret == BGP4_FAILURE)
                    {
                        /* Adding the route into RIB fails. 
                         * Advertise the Old route as withdrawn route*/
                        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                                       "\tADDING Route %s Mask %s to RIB FAILS!!!\n",
                                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                        (pRtProfile),
                                                        BGP4_RT_AFI_INFO
                                                        (pRtProfile)),
                                       Bgp4PrintIpMask ((UINT1)
                                                        BGP4_RT_PREFIXLEN
                                                        (pRtProfile),
                                                        BGP4_RT_AFI_INFO
                                                        (pRtProfile)));

                        Bgp4Vpnv4SetVrfFlags (pPeerRoute, u4VrfId,
                                              BGP4_RT_VRF_WITHDRAWN);
                        if (Bgp4DshGetRouteFromList (&TsWithFIBList, pPeerRoute)
                            == NULL)
                        {
                            /*If the route is not present in temp FIB lsit,
                             * add it*/
                            Bgp4DshAddRouteToList (&TsWithFIBList, pPeerRoute,
                                                   0);
                        }
                        Bgp4DshReleaseList (&TsRouteList, 0);
                        /*Bgp4Vpnv4DeleteRtFromVrfs (pPeerRoute);
                           Bgp4Vpnv4DeleteRtFromVrfs (pRtProfile);
                           return BGP4_FAILURE; */
                        break;
                    }

                    if (BGP4CanRepRouteBeAddedToFIB (pRtProfile, pPeerRoute)
                        == BGP4_FALSE)
                    {
                        /* Next hop not identical. old route must be removed
                         * and new route must be added.
                         **/
                        Bgp4Vpnv4SetVrfFlags (pPeerRoute, u4VrfId,
                                              BGP4_RT_VRF_WITHDRAWN |
                                              BGP4_RT_VRF_ADVT_NOTTO_EXTERNAL);
                        /* FIX:ST_43
                         * when adding replacement route to FIB, remove the old
                         * peer route and the new route from the FIB list, 
                         * if any and then add the old and new routes to 
                         * FIB list
                         */
                        pGetRt = Bgp4DshGetRouteFromList (&TsWithFIBList,
                                                          pPeerRoute);
                        if ((pGetRt != NULL) &&
                            (BGP4_RT_MED (pGetRt) == BGP4_RT_MED (pPeerRoute))
                            && (BGP4_RT_GET_RT_IF_INDEX (pGetRt) ==
                                BGP4_RT_GET_RT_IF_INDEX (pPeerRoute)))
                        {
                            Bgp4DshRemoveRouteFromList (&TsWithFIBList,
                                                        pPeerRoute);
                        }
                        pGetRt = Bgp4DshGetRouteFromList (&TsFeasFIBList,
                                                          pRtProfile);
                        if ((pGetRt != NULL) &&
                            (BGP4_RT_MED (pGetRt) == BGP4_RT_MED (pRtProfile))
                            && (BGP4_RT_GET_RT_IF_INDEX (pGetRt) ==
                                BGP4_RT_GET_RT_IF_INDEX (pRtProfile)))
                        {
                            Bgp4DshRemoveRouteFromList (&TsFeasFIBList,
                                                        pRtProfile);
                        }

                        Bgp4DshAddRouteToList (&TsWithFIBList, pPeerRoute, 0);
                        Bgp4DshAddRouteToList (&TsFeasFIBList, pRtProfile, 0);
                    }
                    else
                    {
                        /*NextHop is Identical */
                        Bgp4Vpnv4SetVrfFlags (pRtProfile, u4VrfId,
                                              BGP4_RT_VRF_NEWUPDATE_TOIP);
                        if (Bgp4DshGetRouteFromList (&TsFeasFIBList,
                                                     pRtProfile) == NULL)
                        {
                            Bgp4DshAddRouteToList (&TsFeasFIBList, pRtProfile,
                                                   0);
                        }
                    }

                    Bgp4DshReleaseList (&TsRouteList, 0);
                    continue;
                }
                else
                {
                    /* There are more routes present in the LOCAL RIB for
                     * the same destination. Remove the route from the
                     * Local RIB. */
                    Bgp4DshAddRouteToList (&TsRouteList, pPeerRoute, 0);
                    Bgp4RibhDelRtEntry (pPeerRoute, pRibNode,
                                        u4VrfId, BGP4_TRUE);
                }
            }

            /* Now the pRtProfile is either a REPLACEMENT route or new feasible
             * route. If it is a replacement route then the old route has
             * already been removed from the LOCAL RIB. So either pRtProfile
             * is a REPLACEMENT or new feasible route, the only operation left
             * to be done is adding this route to the LOCAL RIB and if selected
             * as best route, then this has to be added in the FIB update 
             * list and Internal peers update list.
             */

            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC | BGP4_EVENTS_TRC,
                           BGP4_MOD_NAME,
                           "\tAdding the received new feasible route %s of VRF ID %d to RIB\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           u4VrfId);

            i4Ret = Bgp4RibhAddRtEntry (pRtProfile, u4VrfId);
            if (i4Ret == BGP4_FAILURE)
            {
                /* Adding to tree fails. */
                BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC |
                               BGP4_CONTROL_PATH_TRC,
                               BGP4_MOD_NAME,
                               "\tADDING Route %s Mask %s to RIB\n"
                               "\tFAILS!!!\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               Bgp4PrintIpMask ((UINT1)
                                                BGP4_RT_PREFIXLEN (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)));
                Bgp4DshReleaseList (&TsRouteList, 0);
                break;
            }

            pRibNode = NULL;
            i4RtFound = Bgp4RibhLookupRtEntry (pRtProfile, u4VrfId,
                                               BGP4_TREE_FIND_EXACT,
                                               &pFndRtProfileList, &pRibNode);

            if (i4RtFound == BGP4_FAILURE)
            {
                /* This condition is possible only if adding new route to
                   RIB have failed. Check whether the old route exists. If
                   yes, then process the old route as withdrawn route. */
                if (pPeerRoute != NULL)
                {
                    Bgp4Vpnv4SetVrfFlags (pPeerRoute, u4VrfId,
                                          BGP4_RT_VRF_WITHDRAWN);
                    if (Bgp4DshGetRouteFromList (&TsWithFIBList, pPeerRoute)
                        == NULL)
                    {
                        Bgp4DshAddRouteToList (&TsWithFIBList, pPeerRoute, 0);
                    }
                }
                Bgp4DshReleaseList (&TsRouteList, 0);
                /*Bgp4Vpnv4DeleteRtFromVrfs (pPeerRoute);
                   Bgp4Vpnv4DeleteRtFromVrfs (pRtProfile); */
                return (BGP4_FAILURE);
            }
            /*Get the New Best route from RIB. If the Old best route and
             * New Best routei are not same, advertise the 
             * new best as replacement to Old best route*/
            pNewBestRoute =
                Bgp4DshGetBestRouteFromProfileList (pFndRtProfileList, u4VrfId);
            if ((pBestRoute != pNewBestRoute) && (pNewBestRoute != NULL))
            {
                if (BGP4_RT_PROTOCOL (pNewBestRoute) == BGP_ID)
                {
                    if (pBestRoute != NULL)
                    {
                        if (Bgp4AhCanRouteBeAdvtWithdrawn (pNewBestRoute,
                                                           pBestRoute) ==
                            BGP4_TRUE)
                        {
                            BGP4_RT_SET_FLAG (pNewBestRoute,
                                              BGP4_RT_ADVT_WITHDRAWN);
                            /* Reset the old routes ADVT_WITHDRAWN flag. */
                            BGP4_RT_RESET_FLAG (pBestRoute,
                                                BGP4_RT_ADVT_WITHDRAWN);
                        }
                    }
                }
                if (pBestRoute != NULL)
                {
                    if (BGP4CanRepRouteBeAddedToFIB (pNewBestRoute, pBestRoute)
                        == BGP4_FALSE)
                    {
                        /* Next hop not identical. old route must be removed
                         * and new route must be added.
                         **/
                        BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC | BGP4_FDB_TRC |
                                       BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                                       "\tThe Best route has been changed. Removing the old "
                                       "best route %s mask %s. Updating the new best route "
                                       "%s mask %s to FIB update list.\n",
                                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                        (pBestRoute),
                                                        BGP4_RT_AFI_INFO
                                                        (pBestRoute)),
                                       Bgp4PrintIpMask ((UINT1)
                                                        BGP4_RT_PREFIXLEN
                                                        (pBestRoute),
                                                        BGP4_RT_AFI_INFO
                                                        (pBestRoute)),
                                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                        (pNewBestRoute),
                                                        BGP4_RT_AFI_INFO
                                                        (pNewBestRoute)),
                                       Bgp4PrintIpMask ((UINT1)
                                                        BGP4_RT_PREFIXLEN
                                                        (pNewBestRoute),
                                                        BGP4_RT_AFI_INFO
                                                        (pNewBestRoute)));

                        Bgp4Vpnv4SetVrfFlags (pBestRoute, u4VrfId,
                                              (BGP4_RT_VRF_WITHDRAWN |
                                               BGP4_RT_VRF_ADVT_NOTTO_EXTERNAL));
                        Bgp4Vpnv4SetVrfFlags (pNewBestRoute, u4VrfId,
                                              BGP4_RT_VRF_NEWUPDATE_TOIP);
                        /*Add the routes in the temporary FIB List */
                        if (Bgp4DshGetRouteFromList (&TsWithFIBList,
                                                     pBestRoute) == NULL)
                        {
                            Bgp4DshAddRouteToList (&TsWithFIBList,
                                                   pBestRoute, 0);
                        }
                        if (Bgp4DshGetRouteFromList (&TsFeasFIBList,
                                                     pNewBestRoute) == NULL)
                        {
                            Bgp4DshAddRouteToList (&TsFeasFIBList,
                                                   pNewBestRoute, 0);
                        }
                    }
                    else
                    {
                        BGP4_RT_RESET_FLAG (pNewBestRoute,
                                            BGP4_RT_ADVT_WITHDRAWN);
                        Bgp4Vpnv4ResetVrfFlags (pNewBestRoute, u4VrfId,
                                                BGP4_RT_VRF_NEWUPDATE_TOIP);
                        Bgp4Vpnv4SetVrfFlags (pNewBestRoute, u4VrfId,
                                              BGP4_RT_VRF_REPLACEMENT);
                        if (Bgp4DshGetRouteFromList (&TsFeasFIBList,
                                                     pNewBestRoute) == NULL)
                        {
                            Bgp4DshAddRouteToList (&TsFeasFIBList,
                                                   pNewBestRoute, 0);
                        }
                    }
                }
                else
                {
                    /* pBestRoute is NULL. Means there is no best route
                     * avaiable to this destination earlier. So just
                     * add the pNewBestRoute to the FIB update list
                     * and Internal Peer update List */
                    BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_FDB_TRC | BGP4_EVENTS_TRC,
                                   BGP4_MOD_NAME,
                                   "\tNo best route available for this destination earlier. "
                                   "Updating the new best route %s mask %s to FIB update list.\n",
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pNewBestRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pNewBestRoute)),
                                   Bgp4PrintIpMask ((UINT1)
                                                    BGP4_RT_PREFIXLEN
                                                    (pNewBestRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pNewBestRoute)));
                    Bgp4Vpnv4SetVrfFlags (pNewBestRoute, u4VrfId,
                                          BGP4_RT_VRF_NEWUPDATE_TOIP);
                    if (Bgp4DshGetRouteFromList (&TsFeasFIBList, pNewBestRoute)
                        == NULL)
                    {
                        Bgp4DshAddRouteToList (&TsFeasFIBList,
                                               pNewBestRoute, 0);
                    }
                }
            }
        }
        else
        {
            /* Route for this destination does not exist earlier. So just
               update the RIB with the new route and add the route
               to FIB/UPDATE Lists if the route is valid. */
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                           "\tRoute for this destination does not exist earlier. "
                           "So Adding the route %s mask %s to RIB.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                                            (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            i4Ret = Bgp4RibhAddRtEntry (pRtProfile, u4VrfId);
            if (i4Ret == BGP4_FAILURE)
            {
                BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                               BGP4_EVENTS_TRC | BGP4_CONTROL_PATH_TRC,
                               BGP4_MOD_NAME,
                               "\tADDING Route %s Mask %s to RIB "
                               "\tFAILS!!!\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               Bgp4PrintIpMask ((UINT1)
                                                BGP4_RT_PREFIXLEN (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)));

                Bgp4DshReleaseList (&TsRouteList, 0);
                /*Bgp4Vpnv4DeleteRtFromVrfs (pRtProfile); */
                break;
            }
            /*Add the feasible route into temp FIB list */
            Bgp4Vpnv4SetVrfFlags (pRtProfile, u4VrfId,
                                  BGP4_RT_VRF_NEWUPDATE_TOIP);
            if (Bgp4DshGetRouteFromList (&TsFeasFIBList, pRtProfile) == NULL)
            {
                Bgp4DshAddRouteToList (&TsFeasFIBList, pRtProfile, 0);
            }
        }
        Bgp4DshReleaseList (&TsRouteList, 0);
    }
    /*Since one route may be present in more than one vrf, the route can't
     * be added in the FIB Update list and Internal Peer Advt list. So when
     * the route is processed for each Vrf, the route is added in temporary
     * List if not added previously. Finally the routes from the temp list
     * are added to FIB lsit and Internal Peer Advt list*/
    /* Update Routes in FIB Update List  and internal peer advt lists */
    TMO_SLL_Scan (&TsWithFIBList, pLinkNode, tLinkNode *)
    {
        pBestRoute = pLinkNode->pRouteProfile;
        /* when the route is a replacement route and FuturIP is used, we
         * should delete the old route explicitly and add new route, thats why
         * withdrawn routes are added into FIB list first
         */
        BGP4_RT_SET_FLAG (pBestRoute, BGP4_RT_WITHDRAWN);
        /* Add the route to FIB update list */
        Bgp4AddWithdRouteToFIBUpdList (pBestRoute);

        /* Add the route to Internal Peer Advertisement list */
        Bgp4AddWithdRouteToIntPeerAdvtList (pBestRoute);
    }

    TMO_SLL_Scan (&TsFeasFIBList, pLinkNode, tLinkNode *)
    {
        pBestRoute = pLinkNode->pRouteProfile;
        /* Add the route to FIB update list */
        Bgp4AddFeasRouteToFIBUpdList (pBestRoute, NULL);

        /* Add the route to Internal Peer Advertisement list */
        Bgp4AddFeasRouteToIntPeerAdvtList (pBestRoute, NULL);
    }

    Bgp4DshReleaseList (&TsFeasFIBList, 0);
    Bgp4DshReleaseList (&TsWithFIBList, 0);

    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4ProcessWithdRoute                     */
/* Description     : This function processes the vpnv4 withdrawn    */
/*                   route received from a peer.                    */
/* Input(s)        : None.                                          */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None.                                */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_TRUE or BGP4_FALSE              */
/********************************************************************/
INT4
Bgp4Vpnv4ProcessWithdRoute (tRouteProfile * pRtProfile, tBgp4PeerEntry * pPeer)
{
    tTMO_SLL            TsTempRouteList;
    tTMO_SLL            TsFeasFIBList;
    tTMO_SLL            TsWithFIBList;
    tRouteProfile      *pPeerRoute = NULL;
    tRouteProfile      *pRtNext = NULL;
    tRouteProfile      *pBestRoute = NULL;
    tRouteProfile      *pNewBestRoute = NULL;
    tRouteProfile      *pFndRtProfileList = NULL;
    tLinkNode          *pLinkNode = NULL;
    tRtInstallVrfInfo  *pRtVrfInfo;
    VOID               *pRibNode = NULL;
    INT4                i4RtFound = BGP4_FALSE;
    UINT4               u4VrfId;

    BGP4_TRC_ARG5 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC, BGP4_MOD_NAME,
                   "\t Received VPN withdrawn route %s for processing with nexthop %s "
                   "immediate nexthop %s flags 0x%x and extended flags 0x%x\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP
                                    (BGP4_RT_BGP_INFO (pRtProfile)),
                                    BGP4_INFO_AFI (BGP4_RT_BGP_INFO
                                                   (pRtProfile))),
                   Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                     (pRtProfile))), pRtProfile->u4Flags,
                   pRtProfile->u4ExtFlags);

    /* If the route is destined to some VRF which is in meantime deleted,
     * so we dont have to delete this route here
     */
    if (Bgp4Vpnv4IsRouteCanBeProcessed (pRtProfile) == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC, BGP4_MOD_NAME,
                  "\tThe VRF to which route is destined is deleted.\r\n");
        return (BGP4_SUCCESS);
    }

    TMO_SLL_Init (&TsTempRouteList);
    TMO_SLL_Init (&TsFeasFIBList);
    TMO_SLL_Init (&TsWithFIBList);

    /* Try and get the route from the RIB. If the route is present in the
     * RIB, then process the route. Else this route is just duplicate
     * and dont process it.
     */

    if (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) == 0)
    {
        if ((BGP4_RR_CLIENT_CNT (0) > 0) &&
            (!(BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_ADVT_NOTTO_INTERNAL)))
        {
            Bgp4AddWithdRouteToIntPeerAdvtList (pRtProfile);
            return BGP4_SUCCESS;
        }
        return BGP4_FAILURE;
    }

    TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile), pRtVrfInfo,
                  tRtInstallVrfInfo *)
    {
        u4VrfId = BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrfInfo);

        if ((BGP4_PEER_CXT_ID (BGP4_RT_PEER_ENTRY (pRtProfile)) == u4VrfId) &&
            (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRtProfile))
             == BGP4_VPN4_CE_PEER)
            && (BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED))
        {
            /* For this VRF, route would be added in Trie in Bgp4RibhProcessFeasibleRoute
             * function*/
            continue;
        }
        if (BGP4_RR_CLIENT_CNT (u4VrfId) > 0)
        {
            /* The router is Route-Reflector. This may not have any VRFs, so the
             * list of VRFs could be NULL. hence just append the route into
             * internal peer advertisement list to advertise to other clients
             * and non-clients if there are no VRFs configured. Else,
             * process it normally
             */
            if (!(BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_ADVT_NOTTO_INTERNAL))
            {
                Bgp4AddFeasRouteToIntPeerAdvtList (pRtProfile, NULL);
            }
            if (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) == 0)
            {
                /* Reset the best flag for the route here, because this route is
                 * not going to be deleted from FIB and will not be advertised to
                 * external peers also.
                 */
                return BGP4_SUCCESS;
            }
        }

        /*Check whether the route should be processed as withdrawn for this
         * Vrf*/
        if ((BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo) &
             BGP4_RT_VRF_WITHDRAWN) == BGP4_RT_VRF_WITHDRAWN)
        {
            i4RtFound = Bgp4RibhLookupRtEntry (pRtProfile, u4VrfId,
                                               BGP4_TREE_FIND_EXACT,
                                               &pFndRtProfileList, &pRibNode);
            if (i4RtFound == BGP4_FAILURE)
            {
                BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                               BGP4_TRC_FLAG,
                               BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC |
                               BGP4_FDB_TRC, BGP4_MOD_NAME,
                               "\tPEER %s - Matching Route not present in RIB "
                               "for the withdrawn route %s received\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeer))),
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)));
                continue;
            }
            /*Get the corresponding feasible for the withdrawn route */
            pPeerRoute = Bgp4DshGetPeerRtFromVrfProfileList (pFndRtProfileList,
                                                             pPeer, u4VrfId);
            if (pPeerRoute == NULL)
            {
                /* No matching route present in the RIB for this
                   destination. */
                BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                               BGP4_TRC_FLAG,
                               BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC |
                               BGP4_FDB_TRC, BGP4_MOD_NAME,
                               "\tPEER %s - Matching Route not present in RIB "
                               "for the withdrawn route %s received\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeer))),
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)));
                continue;
            }

            /* The Route is Valid route and present in RIB. Now remove the
             * route from the RIB and either advertise it as withdrawn or if
             * any replacement route exists, then advertise the replacement
             * route as withdrawn. Set the RT_WITHDRAWN flag to indiacte it.
             */
            BGP4_RT_GET_NEXT (pFndRtProfileList, u4VrfId, pRtNext);
            if (pRtNext == NULL)
            {
                BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                               BGP4_TRC_FLAG,
                               BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC |
                               BGP4_FDB_TRC, BGP4_MOD_NAME,
                               "\tPEER %s - Withdrawn Route %s received is a valid route."
                               " Removing it from the RIB and advertising it as withdrawn route\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeer))),
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pPeerRoute),
                                                BGP4_RT_AFI_INFO (pPeerRoute)));

                /* No alternative route is present in tree for this
                 * withdrawn route.  Delete the feasible rotue from RIB*/
                if (Bgp4DshGetRouteFromList (&TsWithFIBList, pPeerRoute) ==
                    NULL)
                {
                    BGP4_RT_SET_FLAG (pPeerRoute, BGP4_RT_WITHDRAWN);
                    Bgp4DshAddRouteToList (&TsWithFIBList, pPeerRoute, 0);
                }
                Bgp4RibhDelRtEntry (pPeerRoute, pRibNode, u4VrfId, BGP4_TRUE);
            }
            else
            {
                /*Alternate route is present tin RIB. Delete the rotue
                 * in RIBAlternate route is present tin RIB. Delete the rotue
                 * in RIB and advertise the new best rotue as replacement */
                Bgp4DshAddRouteToList (&TsTempRouteList, pPeerRoute, 0);
                pBestRoute =
                    Bgp4DshGetBestRouteFromProfileList (pFndRtProfileList,
                                                        u4VrfId);
                Bgp4RibhDelRtEntry (pPeerRoute, pRibNode, u4VrfId, BGP4_TRUE);
                if (pPeerRoute == pBestRoute)
                {
                    /* The Peer route removed is the Best Route. Check whether
                     * any alternate best route exists or not. If yes, then
                     * process it. */
                    pRibNode = NULL;
                    i4RtFound = Bgp4RibhLookupRtEntry (pPeerRoute, u4VrfId,
                                                       BGP4_TREE_FIND_EXACT,
                                                       &pFndRtProfileList,
                                                       &pRibNode);
                    if (i4RtFound == BGP4_FAILURE)
                    {
                        BGP4_DBG (BGP4_DBG_ERROR,
                                  "\tBgp4RibhProcessWithdRoute() : Matching Route not "
                                  "present in RIB\n");
                    }
                    if (pFndRtProfileList != NULL)
                    {
                        pNewBestRoute = Bgp4DshGetBestRouteFromProfileList
                            (pFndRtProfileList, u4VrfId);
                        if (pNewBestRoute == NULL)
                        {
                            BGP4_TRC_ARG2 (&
                                           (BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                           BGP4_TRC_FLAG,
                                           BGP4_CONTROL_PATH_TRC |
                                           BGP4_UPD_MSG_TRC | BGP4_FDB_TRC,
                                           BGP4_MOD_NAME,
                                           "\tPEER %s - No other valid alternative route is present "
                                           "for the best route %s. So route is processed for withdrawal\n",
                                           Bgp4PrintIpAddr
                                           (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeer))),
                                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                            (pPeerRoute),
                                                            BGP4_RT_AFI_INFO
                                                            (pPeerRoute)));
                            /* Only Invalid Routes are present in the RIB. No
                             * other valid alternate route is present for this
                             * route. So process the route for withdrawal. */
                            Bgp4Vpnv4SetVrfFlags (pPeerRoute, u4VrfId,
                                                  BGP4_RT_VRF_WITHDRAWN);
                            BGP4_RT_SET_FLAG (pPeerRoute, BGP4_RT_WITHDRAWN);
                            if (Bgp4DshGetRouteFromList (&TsWithFIBList,
                                                         pPeerRoute) == NULL)
                            {
                                Bgp4DshAddRouteToList (&TsWithFIBList,
                                                       pPeerRoute, 0);
                            }
                        }
                        else if (pBestRoute != pNewBestRoute)
                        {
                            /* Peer route is best route and this is removed.
                             * Alternate route is present for this withdrawn
                             * route. Need to send this routes as replacement
                             * route. */
                            BGP4_TRC_ARG3 (&
                                           (BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                           BGP4_TRC_FLAG,
                                           BGP4_CONTROL_PATH_TRC |
                                           BGP4_UPD_MSG_TRC | BGP4_FDB_TRC,
                                           BGP4_MOD_NAME,
                                           "\tPEER %s - Alternative route %s is present for the withdrawn route %s."
                                           " Processing that route as new best route\n",
                                           Bgp4PrintIpAddr
                                           (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeer))),
                                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                            (pNewBestRoute),
                                                            BGP4_RT_AFI_INFO
                                                            (pNewBestRoute)),
                                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                            (pBestRoute),
                                                            BGP4_RT_AFI_INFO
                                                            (pBestRoute)));

                            Bgp4Vpnv4ResetVrfFlags (pBestRoute, u4VrfId,
                                                    (BGP4_RT_BGP_VRF_BEST));
                            Bgp4Vpnv4SetVrfFlags (pNewBestRoute, u4VrfId,
                                                  (BGP4_RT_VRF_REPLACEMENT |
                                                   BGP4_RT_BGP_VRF_BEST));
                            if (Bgp4AhCanRouteBeAdvtWithdrawn
                                (pNewBestRoute, pBestRoute) == BGP4_TRUE)
                            {
                                /*The new route has to be advertised as 
                                 * withdrawn*/
                                Bgp4Vpnv4SetVrfFlags (pNewBestRoute, u4VrfId,
                                                      BGP4_RT_VRF_ADVT_WITHDRAWN);
                                /* Reset the old routes ADVT_WITHDRAWN flag. */
                                Bgp4Vpnv4ResetVrfFlags (pBestRoute, u4VrfId,
                                                        BGP4_RT_VRF_ADVT_WITHDRAWN);
                            }

                            if (BGP4CanRepRouteBeAddedToFIB (pNewBestRoute,
                                                             pPeerRoute)
                                == BGP4_FALSE)
                            {
                                /* This route was the best route. FOr
                                 * IP updation, set BGP_VRF_BEST flag 
                                 * is there any other way??
                                 */
                                Bgp4Vpnv4SetVrfFlags (pPeerRoute, u4VrfId,
                                                      (BGP4_RT_VRF_WITHDRAWN |
                                                       BGP4_RT_VRF_ADVT_NOTTO_EXTERNAL
                                                       | BGP4_RT_BGP_VRF_BEST));
                                Bgp4Vpnv4SetVrfFlags (pNewBestRoute, u4VrfId,
                                                      (BGP4_RT_BGP_VRF_BEST |
                                                       BGP4_RT_VRF_NEWUPDATE_TOIP));
                                if (Bgp4DshGetRouteFromList
                                    (&TsWithFIBList, pPeerRoute) == NULL)
                                {
                                    BGP4_RT_SET_FLAG (pPeerRoute,
                                                      BGP4_RT_WITHDRAWN);
                                    Bgp4DshAddRouteToList (&TsWithFIBList,
                                                           pPeerRoute, 0);
                                }
                                if (Bgp4DshGetRouteFromList (&TsFeasFIBList,
                                                             pNewBestRoute) ==
                                    NULL)
                                {
                                    Bgp4DshAddRouteToList (&TsFeasFIBList,
                                                           pNewBestRoute, 0);
                                }
                            }
                            else
                            {
                                if (Bgp4DshGetRouteFromList (&TsFeasFIBList,
                                                             pNewBestRoute) ==
                                    NULL)
                                {
                                    Bgp4DshAddRouteToList (&TsFeasFIBList,
                                                           pNewBestRoute, 0);
                                }
                            }
                        }
                    }
                }
                Bgp4DshReleaseList (&TsTempRouteList, 0);
            }
        }
    }

    /*Since one route may be present in more than one vrf, the route can't
     * be added in the FIB Update list and Internal Peer Advt list. So when
     * the route is processed for each Vrf, the route is added in temporary
     * List if not added previously. Finally the routes from the temp list
     * are added to FIB lsit and Internal Peer Advt list*/
    /* Update Routes in FIB Update List  and internal peer advt lists */
    /* Update Routes in FIB Update List  and internal peer advt lists */
    TMO_SLL_Scan (&TsWithFIBList, pLinkNode, tLinkNode *)
    {
        pPeerRoute = pLinkNode->pRouteProfile;
        /* Add the route to FIB update list */
        Bgp4AddWithdRouteToFIBUpdList (pPeerRoute);

        /* Add the route to Internal Peer Advertisement list */
        Bgp4AddWithdRouteToIntPeerAdvtList (pPeerRoute);
    }
    TMO_SLL_Scan (&TsFeasFIBList, pLinkNode, tLinkNode *)
    {
        pPeerRoute = pLinkNode->pRouteProfile;
        /* Add the route to FIB update list */
        Bgp4AddFeasRouteToFIBUpdList (pPeerRoute, NULL);

        /* Add the route to Internal Peer Advertisement list */
        Bgp4AddFeasRouteToIntPeerAdvtList (pPeerRoute, NULL);
    }

    Bgp4DshReleaseList (&TsFeasFIBList, 0);
    Bgp4DshReleaseList (&TsWithFIBList, 0);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4Vpnv4ProcessNonBgpWitdRoute                          */
/* Description   : This routine process the withdrawn import routes. Removes */
/*                 the route from the tree. If this some route was selected  */
/*                 as best route then that route will be added to the        */
/*                 internal and external peers advertisement list as new     */
/*                 feasible route. Else this route if best route earlier     */
/*                 will be advertised as withdrawn to the peers.             */
/* Input(s)      : pointer to the Withdrawn Route (pRtProfile)               */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS if the operation is successful,              */
/*                 BGP4_FAILURE if it fails.                                 */
/*****************************************************************************/
INT4
Bgp4Vpnv4ProcessNonBgpWithdRoute (tRouteProfile * pRtProfile, UINT4 u4VrfIdIn)
{
    VOID               *pRibNode = NULL;
    tRouteProfile      *pFndRtProfileList = NULL;
    tRouteProfile      *pBestRoute = NULL;
    tRouteProfile      *pNewBestRoute = NULL;
    tRouteProfile      *pNextRt = NULL;
    tRouteProfile      *pRtList = NULL;
    tTMO_SLL            TsFeasFIBList;
    tTMO_SLL            TsWithFIBList;
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
    tLinkNode          *pLinkNode = NULL;
    INT4                i4RtFound;
    UINT4               u4VrfId;

    if (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) == 0)
    {
        return BGP4_FAILURE;
    }

    /* If the route is destined to some VRF which is in meantime deleted,
     * so we dont have to delete this route here
     */
    if (Bgp4Vpnv4IsRouteCanBeProcessed (pRtProfile) == BGP4_FAILURE)
    {
        return (BGP4_SUCCESS);
    }

    TMO_SLL_Init (&TsFeasFIBList);
    TMO_SLL_Init (&TsWithFIBList);

    TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile), pRtVrfInfo,
                  tRtInstallVrfInfo *)
    {
        u4VrfId = BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrfInfo);

        if ((BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED) &&
            (u4VrfIdIn != BGP4_DFLT_VRFID) && (u4VrfIdIn != u4VrfId))
        {
            continue;
        }

        if ((BGP4_RT_CXT_ID (pRtProfile) == u4VrfId) &&
            (BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED))
        {
            continue;
        }

        if ((BGP4_VPN4_RIB_TREE (u4VrfId) == NULL) ||
            (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) != BGP4_L3VPN_VRF_UP))
        {
            /* By the time, this gets scheduled, VRF has been removed
             * with the IP-VPN notification. hence return failure
             */
            return (BGP4_FAILURE);
        }

        i4RtFound = Bgp4RibhLookupRtEntry (pRtProfile, u4VrfId,
                                           BGP4_TREE_FIND_EXACT,
                                           &pFndRtProfileList, &pRibNode);
        if (i4RtFound == BGP4_FAILURE)
        {
            /* Route is not present in RIB. */
            return (BGP4_FAILURE);
        }

        pRtList = pFndRtProfileList;
        while (pRtList != NULL)
        {
            if (BGP4_RT_PROTOCOL (pRtList) == BGP4_RT_PROTOCOL (pRtProfile))
            {
                break;
            }
            BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtList);
        }

        if (pRtList == NULL)
        {
            /* No matching route exists in RIB. */
            break;
        }
        /*Get the Best route */
        pBestRoute = Bgp4DshGetBestRouteFromProfileList (pFndRtProfileList,
                                                         u4VrfId);
        BGP4_RT_GET_NEXT (pFndRtProfileList, u4VrfId, pNextRt);
        if (pNextRt != NULL)
        {
            /* There are alternate routes for this destiation. So the next
             * route present in the list will become the new best-route
             */
            pNewBestRoute = pNextRt;
        }

        if (pBestRoute == pRtList)
        {
            /* Best route is to be removed from the RIB. */
            if (pNewBestRoute != NULL)
            {
                /* If the newly selected route is the replacement route, 
                 * set the REPLACEMENT flag in it. 
                 */
                if (pBestRoute != NULL)
                {
                    Bgp4Vpnv4SetVrfFlags (pNewBestRoute, u4VrfId,
                                          BGP4_RT_VRF_REPLACEMENT);
                    if (BGP4_RT_PROTOCOL (pNewBestRoute) == BGP_ID)
                    {
                        if (Bgp4AhCanRouteBeAdvtWithdrawn (pNewBestRoute,
                                                           pBestRoute) ==
                            BGP4_TRUE)
                        {
                            /*New Best route will be advertised as withdrawn */
                            Bgp4Vpnv4SetVrfFlags (pNewBestRoute, u4VrfId,
                                                  BGP4_RT_VRF_ADVT_WITHDRAWN);
                            /* Reset the old routes ADVT_WITHDRAWN flag. */
                            Bgp4Vpnv4ResetVrfFlags (pBestRoute, u4VrfId,
                                                    BGP4_RT_VRF_ADVT_WITHDRAWN);
                        }
                    }

                    if (BGP4_RT_PROTOCOL (pBestRoute) == BGP4_STATIC_ID)
                    {
                        Bgp4Vpnv4SetVrfFlags (pNewBestRoute, u4VrfId,
                                              BGP4_RT_VRF_NEWUPDATE_TOIP);
                    }
                    Bgp4IgphCheckFeasibleRouteForVrfUpdt (pNewBestRoute,
                                                          pBestRoute, u4VrfId,
                                                          &TsFeasFIBList,
                                                          &TsWithFIBList);
                }
                else
                {
                    /* There is no possibility for this else condition to be 
                     * reached. */
                }
            }
            else
            {
                if (BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED)
                {
                    Bgp4Vpnv4SetVrfFlags (pBestRoute, u4VrfId,
                                          BGP4_RT_VRF_WITHDRAWN);
                }

                /* pRtList  was previously selected as the best route and 
                 * advertised to other peers, but now it has been withdrawn.
                 */
                if (Bgp4DshGetRouteFromList (&TsWithFIBList, pBestRoute)
                    == NULL)
                {
                    Bgp4DshAddRouteToList (&TsWithFIBList, pBestRoute, 0);
                }
            }
        }
        /* Now delete the route from RIB */
        Bgp4RibhDelRtEntry (pRtList, pRibNode, u4VrfId, BGP4_FALSE);
    }

    TMO_SLL_Scan (&TsWithFIBList, pLinkNode, tLinkNode *)
    {
        Bgp4AddWithdRouteToFIBUpdList (pLinkNode->pRouteProfile);
        /* Add the route to Internal Peer Advertisement list */
        Bgp4AddWithdRouteToIntPeerAdvtList (pLinkNode->pRouteProfile);
    }

    TMO_SLL_Scan (&TsFeasFIBList, pLinkNode, tLinkNode *)
    {
        Bgp4AddFeasRouteToFIBUpdList (pLinkNode->pRouteProfile, 0);
        /* Add the route to Internal Peer Advertisement list */
        Bgp4AddFeasRouteToIntPeerAdvtList (pLinkNode->pRouteProfile, NULL);
    }

    Bgp4DshReleaseList (&TsFeasFIBList, 0);
    Bgp4DshReleaseList (&TsWithFIBList, 0);
    return BGP4_SUCCESS;
}

/********************************************************************** */
/* Function Name   : Bgp4Vpnv4ProcessNonBgpFeasRoute                   */
/* Description     : This function processes the Non-Bgp vpnv4          */
/*                   feasible route .                                   */
/* Input(s)        : None.                                              */
/* Output(s)       : None.                                              */
/* Global Variables Referred : None.                                    */
/* Global variables Modified : None.                                    */
/* Exceptions or Operating System Error Handling : None                 */
/* Use of Recursion          : None.                                    */
/* Returns                   : BGP4_TRUE or BGP4_FALSE                  */
/************************************************************************/
INT4
Bgp4Vpnv4ProcessNonBgpFeasRoute (tRouteProfile * pRtProfile, UINT4 u4VrfIdIn)
{
    tRouteProfile      *pBestRoute = NULL;
    tRouteProfile      *pNewBestRoute = NULL;
    tRouteProfile      *pRtList = NULL;
    tTMO_SLL            TsFeasFIBList;
    tTMO_SLL            TsWithFIBList;
    VOID               *pRibNode = NULL;
    tRouteProfile      *pFndRtProfileList = NULL;
    INT4                i4RtFound = BGP4_FAILURE;
    BOOL1               bIsIdenticalRoute = BGP4_FALSE;
    tLinkNode          *pLinkNode = NULL;
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
    UINT4               u4VrfId;

    if (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) == 0)
    {
        return BGP4_FAILURE;
    }
    /* Check whethe the route can be installed.
     * If the route is destined to some VRF which is in meantime deleted,
     * so there is no point in adding the route here
     */
    if (Bgp4Vpnv4IsRouteCanBeProcessed (pRtProfile) == BGP4_FAILURE)
    {
        Bgp4Vpnv4DelRouteFromVPNRIB (pRtProfile);
        return (BGP4_SUCCESS);
    }

    TMO_SLL_Init (&TsFeasFIBList);
    TMO_SLL_Init (&TsWithFIBList);

    TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile), pRtVrfInfo,
                  tRtInstallVrfInfo *)
    {
        pRtList = NULL;
        u4VrfId = BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrfInfo);

        if ((BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED) &&
            (u4VrfIdIn != BGP4_DFLT_VRFID) && (u4VrfIdIn != u4VrfId))
        {
            continue;
        }

        if ((BGP4_RT_CXT_ID (pRtProfile) == u4VrfId) &&
            (BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED))
        {
            continue;
        }
        if ((BGP4_VPN4_RIB_TREE (u4VrfId) == NULL) ||
            (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) != BGP4_L3VPN_VRF_UP))
        {
            return (BGP4_FAILURE);
        }

        i4RtFound = Bgp4RibhLookupRtEntry (pRtProfile, u4VrfId,
                                           BGP4_TREE_FIND_EXACT,
                                           &pFndRtProfileList, &pRibNode);
        if (i4RtFound == BGP4_SUCCESS)
        {
            /*Get the Best route */
            pBestRoute =
                Bgp4DshGetBestRouteFromProfileList (pFndRtProfileList, u4VrfId);
            if (pBestRoute != NULL)
            {
                BGP4_RT_REF_COUNT (pBestRoute)++;
            }
            /* Check for replacement route should be done. */
            pRtList = Bgp4DshGetNonBgpRtFromProfileList (pFndRtProfileList,
                                                         pRtProfile,
                                                         &bIsIdenticalRoute);
        }

        if (pRtList != NULL)
        {
            /* It is a replacement route/Identical route.  */
            if (bIsIdenticalRoute != BGP4_TRUE)
            {
                /* Replacement route received. Remove the old route and
                 * add the new route
                 */
                Bgp4RibhDelRtEntry (pRtList, pRibNode, u4VrfId, BGP4_FALSE);
                if ((Bgp4RibhAddRtEntry (pRtProfile, u4VrfId)) == BGP4_FAILURE)
                {
                    /* Adding route to Tree Fails. Dont return. May be we need
                     * to send the Old route as Withdrawn. */
                }
            }
            else
            {
                /* Identical route */
                if (pBestRoute != NULL)
                {
                    Bgp4DshReleaseRtInfo (pBestRoute);
                }
                continue;
            }
        }
        else
        {
            /* New route originated */
            if ((Bgp4RibhAddRtEntry (pRtProfile, u4VrfId)) == BGP4_FAILURE)
            {
                /* Adding of New route FAILS. RIB is unaffected. So no need
                 * for any further processing. */
                if (pBestRoute != NULL)
                {
                    Bgp4DshReleaseRtInfo (pBestRoute);
                }
                break;
            }
        }

        i4RtFound = Bgp4RibhLookupRtEntry (pRtProfile, u4VrfId,
                                           BGP4_TREE_FIND_EXACT,
                                           &pFndRtProfileList, &pRibNode);
        if (i4RtFound == BGP4_FAILURE)
        {
            /* Adding new route to RIB should have failed. */
            if ((pBestRoute != NULL) && (pBestRoute == pRtList))
            {
                /* The removed route was the best route earlier. Now
                 * we need to remove the old best route from FIB and 
                 * advertise it as withdrawn. */
                Bgp4Vpnv4SetVrfFlags (pBestRoute, u4VrfId,
                                      BGP4_RT_VRF_WITHDRAWN);
                if (Bgp4DshGetRouteFromList (&TsWithFIBList, pBestRoute)
                    == NULL)
                {
                    Bgp4DshAddRouteToList (&TsWithFIBList, pBestRoute, 0);
                }
            }
            if (pBestRoute != NULL)
            {
                Bgp4DshReleaseRtInfo (pBestRoute);
            }
            break;
        }

        pNewBestRoute =
            Bgp4DshGetBestRouteFromProfileList (pFndRtProfileList, u4VrfId);
        if (pBestRoute != pNewBestRoute)
        {
            if (pNewBestRoute != NULL)
            {
                /* If the newly selected route is the replacement route, 
                 * set the REPLACEMENT flag in it. 
                 */
                /* Update the FIB update/peer advt list */
                Bgp4IgphCheckFeasibleRouteForVrfUpdt (pNewBestRoute, pBestRoute,
                                                      u4VrfId, &TsFeasFIBList,
                                                      &TsWithFIBList);
            }
        }
        if (pBestRoute != NULL)
        {
            Bgp4DshReleaseRtInfo (pBestRoute);
        }
    }

    TMO_SLL_Scan (&TsWithFIBList, pLinkNode, tLinkNode *)
    {
        Bgp4AddWithdRouteToFIBUpdList (pLinkNode->pRouteProfile);
        Bgp4AddWithdRouteToIntPeerAdvtList (pLinkNode->pRouteProfile);
    }

    TMO_SLL_Scan (&TsFeasFIBList, pLinkNode, tLinkNode *)
    {
        Bgp4AddFeasRouteToFIBUpdList (pLinkNode->pRouteProfile, 0);
        Bgp4AddFeasRouteToIntPeerAdvtList (pLinkNode->pRouteProfile, NULL);
    }

    Bgp4DshReleaseList (&TsFeasFIBList, 0);
    Bgp4DshReleaseList (&TsWithFIBList, 0);
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4RrVpnJoin                             */
/* Description     : Sends route-refresh message to all the         */
/*                   non-clients if its negotiated, otherwise, the  */
/*                   session will be reset by send CEASE message    */
/* Input(s)        : None.                                          */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4Vpnv4RrVpnJoin (VOID)
{
    tAddrPrefix         PeerAddress;
    /* New import route target is added. Send Route-Refresh message 
     * to the Nonclients */
    Bgp4InitAddrPrefixStruct (&(PeerAddress), BGP4_INET_AFI_IPV4);
    Bgp4RtRefRequestHandler (BGP4_DFLT_VRFID, PeerAddress, BGP4_INET_AFI_IPV4,
                             BGP4_INET_SAFI_VPNV4_UNICAST,
                             BGP4_RR_ADVT_NON_CLIENTS);
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4RrPrune                               */
/* Description     : This module decrements the route count for the */
/*                   route target to be deleted. If the count       */
/*                   becomes zero, then this will inform BGP to wait*/
/*                   for few hours before deleting this route target*/
/*                   from the set. It marks time stamp when there   */
/*                   are no routes correspond to this route target. */
/*                   This function will be called if the route      */
/*                   received from client is deleted                */
/* Input(s)        : pVpnv4Route - Pointer to route profile         */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS                         */
/********************************************************************/
INT4
Bgp4Vpnv4RrPrune (tRouteProfile * pVpnv4Route)
{
    tBgp4Info          *pBgp4Info = NULL;
    tBgp4RrImportTargetInfo *pRrImportTarget = NULL;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT4               u4Index;
    UINT2               u2EcommType;

    pBgp4Info = BGP4_RT_BGP_INFO (pVpnv4Route);
    /* Check whether extended community is received or not along
     * with the path attributes
     */
    if (BGP4_INFO_ECOMM_ATTR (pBgp4Info) == NULL)
    {
        /* No ecomm received, hence return success */
        return BGP4_SUCCESS;
    }

    if ((BGP4_RT_PROTOCOL (pVpnv4Route) != BGP_ID) ||
        (BGP4_PEER_RFL_CLIENT (BGP4_RT_PEER_ENTRY (pVpnv4Route)) != CLIENT))
    {
        /* route is not received from client peer */
        return BGP4_SUCCESS;
    }

    for (u4Index = 0; u4Index < BGP4_INFO_ECOMM_COUNT (pBgp4Info); u4Index++)
    {
        MEMCPY ((UINT1 *) au1ExtComm,
                (UINT1 *) (BGP4_INFO_ECOMM_ATTR_VAL (pBgp4Info) +
                           +(u4Index * EXT_COMM_VALUE_LEN)),
                EXT_COMM_VALUE_LEN);
        PTR_FETCH2 (u2EcommType, au1ExtComm);
        if (BGP4_IS_RT_EXT_COMMUNITY (u2EcommType) == BGP4_TRUE)
        {
            pRrImportTarget = Bgp4Vpnv4IsRTMatchInRRImportRTList (au1ExtComm);
            if (pRrImportTarget != NULL)
            {
                /* Decrement the route-count */
                BGP4_VPN4_RR_IMPORT_TARGET_RTCNT (pRrImportTarget)--;
                if (BGP4_VPN4_RR_IMPORT_TARGET_RTCNT (pRrImportTarget) == 0)
                {
                    BGP4_VPN4_RR_IMPORT_TARGET_TIMESTAMP (pRrImportTarget) =
                        Bgp4ElapTime ();
                    /* Inform BGP to start timer to take care of deletion
                     * of route target from the set. If the timer is already
                     * running then no need to do anything.
                     */
                    if (BGP4_VPN4_RR_TARGETS_INTERVAL == 0)
                    {
                        BGP4_VPN4_RR_TARGETS_INTERVAL++;
                    }
                }
            }
        }
    }

    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4RrDeleteTargetFromSet                 */
/* Description     : This function actually deletes the Route Target*/
/*                   from the Set. This will be called when         */
/*                   BGP4_VPN4_RR_TARGETS_INTERVAL time equals to   */
/*                   the delay (few hours) it is supposed to be     */
/*                   allowed before deleting the route target from  */
/*                   the set.                                       */
/* Input(s)        : None.                                          */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS                         */
/********************************************************************/
INT4
Bgp4Vpnv4RrDeleteTargetFromSet (VOID)
{
    tBgp4RrImportTargetInfo *pRrImportTarget = NULL;
    tBgp4RrImportTargetInfo *pTmpRrImportTarget = NULL;
    UINT4               u4Delay;
    UINT4               u4NextTimeStamp = 0;

    BGP_SLL_DYN_Scan (BGP4_VPN4_RR_TARGETS_SET, pRrImportTarget,
                      pTmpRrImportTarget, tBgp4RrImportTargetInfo *)
    {
        if (BGP4_VPN4_RR_IMPORT_TARGET_RTCNT (pRrImportTarget) == 0)
        {
            u4Delay = ((UINT4) Bgp4ElapTime () -
                       BGP4_VPN4_RR_IMPORT_TARGET_TIMESTAMP (pRrImportTarget));
            if (u4Delay >= BGP4_VPN4_RR_TARGETS_DEL_INTERVAL)
            {
                /* Route target delay exceeds the time interval it should
                 * be delayed. hence remove this target from the set.
                 */
                TMO_SLL_Delete (BGP4_VPN4_RR_TARGETS_SET,
                                (tTMO_SLL_NODE *) pRrImportTarget);
                Bgp4MemReleaseRrImportTarget (pRrImportTarget);
            }
            else if (u4Delay > u4NextTimeStamp)
            {
                u4NextTimeStamp = u4Delay;
            }
        }
    }

    BGP4_VPN4_RR_TARGETS_INTERVAL = u4NextTimeStamp;
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4CheckAdvtToCEPeer                     */
/* Description     : Checks whether the route can be advertised to  */
/*                    CE or not based on Site Of Origin             */
/* Input(s)        : pVpn4Route- Vpnv4 Route                        */
/*                 : pPeer     - CE Pee entry                       */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_TRUE or BGP4_FALSE              */
/********************************************************************/
INT1
Bgp4Vpn4CheckAdvtToCEPeer (tRouteProfile * pVpn4Route, tBgp4PeerEntry * pPeer)
{
    tBgp4Info          *pBgp4Info = NULL;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT4               u4Index;
    UINT2               u2EcommType;

    pBgp4Info = BGP4_RT_BGP_INFO (pVpn4Route);
    if (BGP4_INFO_ECOMM_ATTR (pBgp4Info) == NULL)
    {
        return BGP4_TRUE;
    }
    for (u4Index = 0; u4Index < BGP4_INFO_ECOMM_COUNT (pBgp4Info); u4Index++)
    {
        MEMCPY ((UINT1 *) au1ExtComm,
                (UINT1 *) (BGP4_INFO_ECOMM_ATTR_VAL (pBgp4Info) +
                           +(u4Index * EXT_COMM_VALUE_LEN)),
                EXT_COMM_VALUE_LEN);
        PTR_FETCH2 (u2EcommType, au1ExtComm);
        if (BGP4_IS_SOO_EXT_COMMUNITY (u2EcommType) == BGP4_TRUE)
        {
            if ((MEMCMP (au1ExtComm,
                         BGP4_VPN4_CE_PEER_SOO_EXT_COM_VALUE (pPeer),
                         EXT_COMM_VALUE_LEN) == 0))
            {
                return BGP4_FALSE;
            }
        }
    }
    return BGP4_TRUE;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4UpdtRouteInIPVRF                      */
/* Description     : Function to update route-entry in VRF table    */
/* Input(s)        : pRtProfile - route information                 */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4Vpnv4UpdtRouteInIPVRF (tRouteProfile * pRtProfile)
{
    tTMO_SLL           *pVrfList = NULL;
    tRtInstallVrfInfo  *pInstVrf = NULL;
    tBgp4PeerEntry     *pPeer = NULL;
    INT4                i4Status = BGP4_SUCCESS;
    UINT2               u2ExtAdvtCnt = 0;
    UINT4               u4VrfId;
    tRouteProfile      *pAddRtProfile = NULL;

    pVrfList = BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile);

    TMO_SLL_Scan (pVrfList, pInstVrf, tRtInstallVrfInfo *)
    {
        u4VrfId = BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pInstVrf);
        if ((BGP4_RR_CLIENT_CNT (u4VrfId) > 0) && (pVrfList == NULL))
        {
            /* If the speaker is route-reflector and the route is not
             * installed in any VRF, then simply return
             */
            return (BGP4_SUCCESS);
        }

        if (BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pInstVrf) &
            BGP4_RT_VRF_WITHDRAWN)
        {
            /* Route is a withdrawn route. */
            BGP4_VPN4_RT_VRF_RESET_FLAG (pInstVrf, (UINT4) BGP4_RT_IP_VRF_BEST);
#ifdef L3VPN_TEST_WANTED
            Bgp4Vpnv4DelRouteFromIPVRF (BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID
                                        (pInstVrf), pRtProfile);
#endif
            if (BGP4_RT_LABEL_CNT (pRtProfile) > 0)
            {
                Bgp4UpdateRouteInMplsDb (pRtProfile, u4VrfId,
                                         L3VPN_BGP4_ROUTE_DEL,
                                         L3VPN_BGP4_LABEL_PUSH);
                BGP4RTDeleteRouteInCommIp4RtTbl (pRtProfile, u4VrfId);
            }
            /* Updating to FIB is done. Check whether the
             * peer from which the route is learnt is going through deinit.
             * If yes, then dont do any process. Else Check whether
             * the route needs to be sent to the external peer or not.
             * If required to send, then add the route to the external
             * peer advt list. */
            if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
            {
                pPeer = BGP4_RT_PEER_ENTRY (pRtProfile);
            }

            if ((pPeer != NULL) &&
                (BGP4_GET_PEER_CURRENT_STATE (pPeer) ==
                 BGP4_PEER_DEINIT_INPROGRESS))
            {
                /* No need to do any operation. */
            }
            else
            {
                if ((!(BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pInstVrf) &
                       BGP4_RT_VRF_ADVT_NOTTO_EXTERNAL)) &&
                    (BGP4_LOCAL_ADMIN_STATUS (u4VrfId) == BGP4_ADMIN_UP))
                {
                    /* Route can be advertised to external peer. */
                    u2ExtAdvtCnt++;
                }
                else
                {
                    /* Route should not be advertised to the external peer. */
                    BGP4_VPN4_RT_VRF_RESET_FLAG (pInstVrf,
                                                 (UINT4)
                                                 BGP4_RT_ADVT_NOTTO_EXTERNAL);
                    BGP4_VPN4_RT_VRF_RESET_FLAG (pInstVrf,
                                                 (UINT4) (BGP4_RT_VRF_WITHDRAWN
                                                          |
                                                          BGP4_RT_VRF_REPLACEMENT));
                }
            }
        }
        else
        {
            if (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) != BGP4_L3VPN_VRF_UP)
            {
                /* Either the VRF is deleted or it is not operationally up. */
                continue;
            }

            if ((BGP4_LOCAL_ADMIN_STATUS (u4VrfId) == BGP4_ADMIN_DOWN) ||
                (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId)
                 != BGP4_L3VPN_VRF_UP))
            {
                return (BGP4_SUCCESS);
            }
            if ((BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pInstVrf) &
                 BGP4_RT_VRF_REPLACEMENT) &&
                (!(BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pInstVrf) &
                   BGP4_RT_VRF_NEWUPDATE_TOIP)))
            {
#ifdef L3VPN_TEST_WANTED
                i4Status =
                    Bgp4Vpnv4ModifyRouteInIPVRF
                    (BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pInstVrf), pRtProfile);
#endif
            }
            else if (BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pInstVrf) &
                     BGP4_RT_VRF_NEWUPDATE_TOIP)
            {
#ifdef L3VPN_TEST_WANTED
                i4Status =
                    Bgp4Vpnv4AddRouteToIPVRF (BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID
                                              (pInstVrf), pRtProfile);
#endif
                if (BGP4_RT_LABEL_CNT (pRtProfile) > 0)
                {
                    BGP4_TRC_ARG2 (NULL,
                                   BGP4_TRC_FLAG, BGP4_DETAIL_TRC,
                                   BGP4_MOD_NAME,
                                   "\tCalling Bgp4UpdateRouteInMplsDb by FUNC[%s] LINE - [%d] \n",
                                   __func__, __LINE__);
                    Bgp4UpdateRouteInMplsDb (pRtProfile, u4VrfId,
                                             L3VPN_BGP4_ROUTE_ADD,
                                             L3VPN_BGP4_LABEL_PUSH);
                    BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_DETAIL_TRC,
                                   BGP4_MOD_NAME,
                                   "\tCalling BGP4RTDeleteRouteInCommIp4RtTbl from Func[%s] Line[%d]",
                                   __func__, __LINE__);
                    BGP4RTAddRouteToCommIp4RtTbl (pRtProfile, u4VrfId);
                }
            }
            else
            {
                continue;
            }
            if (i4Status == BGP4_FAILURE)
            {
                BGP4_VPN4_RT_VRF_RESET_FLAG (pInstVrf, (UINT4)
                                             (BGP4_RT_VRF_REPLACEMENT |
                                              BGP4_RT_VRF_NEWUPDATE_TOIP |
                                              BGP4_RT_IP_VRF_BEST));
                BGP4_VPN4_RT_VRF_SET_FLAG (pInstVrf,
                                           BGP4_RT_VRF_ADVT_NOTTO_EXTERNAL);
                continue;
            }
            else
            {
                /* Updating to FIB is done successfully. Now Check whether
                 * the route needs to be sent to the external peer or not.
                 * If required to send, then add the route to the external
                 * peer advt list. */
                /* Set the BEST flag to indicate the route is selected as
                 * best route and add to FIB. */
                BGP4_VPN4_RT_VRF_SET_FLAG (pInstVrf, BGP4_RT_IP_VRF_BEST);

                if (!(BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pInstVrf) &
                      BGP4_RT_VRF_ADVT_NOTTO_EXTERNAL))
                {

                    u2ExtAdvtCnt++;
                }
                else
                {
                    /* Route should not be advertised to the external peer.
                     * Reset the notto_external flag. */
                    BGP4_VPN4_RT_VRF_RESET_FLAG (pInstVrf, (UINT4)
                                                 BGP4_RT_VRF_REPLACEMENT);
                }
                BGP4_VPN4_RT_VRF_RESET_FLAG (pInstVrf, (UINT4)
                                             BGP4_RT_VRF_NEWUPDATE_TOIP);
            }
        }
        if (u2ExtAdvtCnt > 0)
        {
            /* Add the route into External peer advertisement list */
            /* Route is added in non default context. This route
             * would have added in some other list (internal of fib) for
             * non zero context Hence duplicate profile is created 
             * and added in the list. Once it is processed memory will be 
             * cleared in Bgp4ProcessExternalPeerAdvt function
             * */
            pAddRtProfile = Bgp4DupCompleteRtProfile (pRtProfile);
            if (pAddRtProfile == NULL)
            {
                return BGP4_SUCCESS;
            }
            BGP4_INFO_REF_COUNT (BGP4_RT_BGP_INFO (pAddRtProfile)) = 0;
            pAddRtProfile->u4ExtListCxtId = u4VrfId;
            BGP4_RT_SET_FLAG (pAddRtProfile, BGP4_RT_IN_EXT_PEER_ADVT_LIST);
            Bgp4DshLinkRouteToList (BGP4_EXT_PEERS_ADVT_LIST (u4VrfId),
                                    pAddRtProfile, 0, BGP4_EXT_PEER_LIST_INDEX);
        }
        u2ExtAdvtCnt = 0;
    }

    return BGP4_SUCCESS;
}

#ifdef L3VPN_TEST_WANTED
/********************************************************************/
/* Function Name   : Bgp4Vpnv4AddRouteToIPVRF                       */
/* Description     : Function to add the route-entry to VRF table   */
/* Input(s)        : u4VrfId - VRF identifer                        */
/*                   pRtProfile - route information                 */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4Vpnv4AddRouteToIPVRF (UINT4 u4VrfId, tRouteProfile * pRtProfile)
{
    tRtInfo            *pVpnRoute = NULL;
    tBgp4Info          *pRouteInfo = NULL;
    tAddrPrefix         InvPrefix;
    UINT4               u4Dest;
    UINT4               u4Gate;
    UINT4               u4BgpGate = 0;
    UINT4               u4Mask;
    UINT1               u1Protocol;
    UINT1               u1RcvdPrefLen;
#ifdef L3VPN_TEST_WANTED
    INT4                i4RetVal;
#endif
    UINT4               u4Index;

    Bgp4InitAddrPrefixStruct (&(InvPrefix), BGP4_RT_AFI_INFO (pRtProfile));
    /* Non-BGP Default routes are learnt from IP. Should not be added
     * back to IP */
    if (((PrefixMatch
          (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
           (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)), InvPrefix)) == BGP4_TRUE)
        && (BGP4_RT_PREFIXLEN (pRtProfile) == 0)
        && (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID))

    {
        /* Route is a non-BGP Default route learnt from IP */
        return (BGP4_SUCCESS);
    }

    pVpnRoute = (tRtInfo *) Bgp4MemAllocateMsg (sizeof (tRtInfo));
    if (pVpnRoute == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_FDB_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for adding Route to IP_VRF FAILED!!!\n");
        return BGP4_FAILURE;

    }
    MEMSET (pVpnRoute, 0, sizeof (tRtInfo));

    pRouteInfo = BGP4_RT_BGP_INFO (pRtProfile);
    PTR_FETCH_4 (u4Dest, BGP4_RT_IP_PREFIX (pRtProfile));
    PTR_FETCH_4 (u4Gate, BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile));
    if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
    {
        PTR_FETCH_4 (u4BgpGate,
                     BGP4_INFO_NEXTHOP (BGP4_RT_BGP_INFO (pRtProfile)));
    }

    u4Mask = Bgp4GetSubnetmask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile));
    /* For the aggregated route the protocol has to be BGP_ID.
     * For all other case, the protocol value will be as
     * present in the route profile. */
    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGREGATED)
        == BGP4_RT_AGGREGATED)
    {
        u1Protocol = BGP_ID;
    }
    else
    {
        u1Protocol = BGP4_RT_PROTOCOL (pRtProfile);
    }

    u1RcvdPrefLen = (UINT1) BGP4_RT_PREFIXLEN (pRtProfile);

    pVpnRoute->u4DestNet = u4Dest;
    pVpnRoute->u4DestMask = u4Mask;
    pVpnRoute->u4NextHop = u4Gate;
    pVpnRoute->u4BgpNextHop = u4BgpGate;
    pVpnRoute->u4RowStatus = IPFWD_ACTIVE;
    pVpnRoute->u2RtProto = u1Protocol;
    pVpnRoute->i4Metric1 = BGP4_RT_MED (pRtProfile);
    pVpnRoute->u4RtIfIndx = BGP4_RT_GET_RT_IF_INDEX (pRtProfile);
    pVpnRoute->u2RtType = CIDR_REMOTE_ROUTE_TYPE;
    if ((BGP4_RT_PEER_ENTRY (pRtProfile) == NULL) || (u1Protocol != BGP_ID))
    {
        pVpnRoute->u4RtNxtHopAS = 0;
    }
    else
    {                            /* Route learnt from BGP peer */
        pVpnRoute->u4RtNxtHopAS =
            BGP4_PEER_ASNO ((BGP4_RT_PEER_ENTRY (pRtProfile)));
    }

    /* Fill VRF id and BGP label */
    pVpnRoute->u4VRFIndex = u4VrfId;
    if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
    {
        if (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRtProfile)) ==
            BGP4_VPN4_CE_PEER)
        {
            if (BGP4_PEER_CXT_ID (BGP4_RT_PEER_ENTRY (pRtProfile)) != u4VrfId)
            {
#ifdef L3VPN_PORT
                pVpnRoute->u4NHVrfId = (BGP4_PEER_CXT_ID
                                        (BGP4_RT_PEER_ENTRY (pRtProfile))) + 1;
#endif
            }
        }
    }
    /*BGP Label */
    for (u4Index = 0; u4Index < BGP4_RT_LABEL_CNT (pRtProfile); u4Index++)
    {
#ifdef L3VPN_PORT
        pVpnRoute->au4BGPLabel[u4Index] = BGP4_RT_LABEL_INFO (pRtProfile);
#endif
    }
    /*IGP Label */
    if (BGP4_RT_LABEL_CNT (pRtProfile) > 0)
    {
#ifdef L3VPN_PORT
        pVpnRoute->u4IGPLblTunIdx = BGP4_RT_IGP_LSP_ID (pRtProfile);
#endif
    }
    else
    {
#ifdef L3VPN_PORT
        pVpnRoute->u4IGPLblTunIdx = BGP4_INVALID_IGP_LABEL;
#endif
    }

#ifdef L3VPN_TEST_WANTED
    /* Calling Routine to Add Route to the IP Table */
    /* i4RetVal = IpForwardingTableAddRoute (pVpnRoute); */
    if ((i4RetVal == IP_FAILURE) || (i4RetVal == IP_ROUTE_NOT_FOUND) ||
        (i4RetVal == IP_NO_ROOM))
    {
        BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tUpdating IP VRF[%d] Table with  Dest = %s "
                       "Mask = %s\n\t" "Nexthop = %s FAILED !!!\n",
                       u4VrfId,
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        Bgp4MemReleaseMsg ((UINT1 *) pVpnRoute);
        return BGP4_FAILURE;
    }
#endif
    BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                   BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                   "\tUpdating IP VRF[%d] Table with  Dest = %s "
                   "Mask = %s\n\t" "Nexthop = %s SUCCESS!!!\n",
                   u4VrfId,
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)));
    Bgp4MemReleaseMsg ((UINT1 *) pVpnRoute);
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4DelRouteFromIPVRF                     */
/* Description     : Function to delete the route from VRF table    */
/* Input(s)        : u4VrfId - VRF identifer                        */
/*                   pRtProfile - route information                 */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4Vpnv4DelRouteFromIPVRF (UINT4 u4VrfId, tRouteProfile * pRtProfile)
{
    tRtInfo            *pVpnRoute = NULL;
    tBgp4Info          *pRouteInfo = NULL;
    tAddrPrefix         InvPrefix;
    UINT4               u4Dest;
    UINT4               u4Gate;
    UINT4               u4BgpGate;
    UINT4               u4Mask;
    UINT1               u1Protocol;

    Bgp4InitAddrPrefixStruct (&(InvPrefix), BGP4_RT_AFI_INFO (pRtProfile));
    /* Non-BGP Default routes are learnt from IP. Should not be added
     * back to IP */
    if (((PrefixMatch
          (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
           (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)), InvPrefix)) == BGP4_TRUE)
        && (BGP4_RT_PREFIXLEN (pRtProfile) == 0)
        && (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID))

    {
        /* Route is a non-BGP Default route learnt from IP */
        return (BGP4_SUCCESS);
    }

    if ((BGP4_RT_PROTOCOL (pRtProfile) == BGP4_LOCAL_ID) ||
        (BGP4_RT_PROTOCOL (pRtProfile) == BGP4_RIP_ID) ||
        (BGP4_RT_PROTOCOL (pRtProfile) == BGP4_OSPF_ID))
    {
        /* Static routes can be added via BGP. For all other case,
         * dont add the route to FIB */
        return BGP4_SUCCESS;
    }

    pVpnRoute = (tRtInfo *) Bgp4MemAllocateMsg (sizeof (tRtInfo));
    if (pVpnRoute == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_FDB_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for deleting Route to IP_VRF "
                  "FAILED!!!\n");
        return BGP4_FAILURE;

    }
    MEMSET (pVpnRoute, 0, sizeof (tRtInfo));

    pRouteInfo = BGP4_RT_BGP_INFO (pRtProfile);
    PTR_FETCH_4 (u4Dest, BGP4_RT_IP_PREFIX (pRtProfile));
    PTR_FETCH_4 (u4Gate, BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile));
    if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
    {
        PTR_FETCH_4 (u4BgpGate,
                     BGP4_INFO_NEXTHOP (BGP4_RT_BGP_INFO (pRtProfile)));
    }
    else
    {
        u4BgpGate = 0;
    }

    u4Mask = Bgp4GetSubnetmask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile));
    /* For the aggregated route the protocol has to be BGP_ID.
     * For all other case, the protocol value will be as
     * present in the route profile. */
    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGREGATED)
        == BGP4_RT_AGGREGATED)
    {
        u1Protocol = BGP_ID;
    }
    else
    {
        u1Protocol = BGP4_RT_PROTOCOL (pRtProfile);
    }

    pVpnRoute->u4DestNet = u4Dest;
    pVpnRoute->u4DestMask = u4Mask;
    pVpnRoute->u4NextHop = u4Gate;

#ifdef L3VPN_PORT
    pVpnRoute->u4BgpNextHop = u4BgpGate;
    pVpnRoute->u4VRFIndex = u4VrfId;
#endif

    pVpnRoute->u2RtProto = u1Protocol;

/*    if (IpForwardingTableDeleteRoute (pVpnRoute) != IP_SUCCESS)*/
    /* Calling Interface Routine to Delete Route
     * from IP Fwd Table */
/*    {
        BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tDELETING Dest = %s Mask = %s Nexthop = %s \n\t"
                       "From IP VRF[%d] Table FAILED !!!\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       u4VrfId);
        Bgp4MemReleaseMsg ((UINT1 *) pVpnRoute);
        return BGP4_FAILURE;
    }*/
    BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                   BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                   "\tDELETED Dest = %s Mask = %s Nexthop = %s \n\t"
                   "From IP VRF[%d] Table Successfully\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)), u4VrfId);
    Bgp4MemReleaseMsg ((UINT1 *) pVpnRoute);
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4ModifyRouteInIPVRF                    */
/* Description     : Function to modify the route in VRF table      */
/* Input(s)        : u4VrfId - VRF identifer                        */
/*                   pRtProfile - route information                 */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4Vpnv4ModifyRouteInIPVRF (UINT4 u4VrfId, tRouteProfile * pRtProfile)
{
    tRtInfo            *pVpnRoute = NULL;
    tBgp4Info          *pRouteInfo = NULL;
    tAddrPrefix         InvPrefix;
    UINT4               u4Dest;
    UINT4               u4Gate;
    UINT4               u4BgpGate;
    UINT4               u4Mask;
    UINT1               u1Protocol;
    UINT1               u1RcvdPrefLen;
#ifdef L3VPN_TEST_WANTED
    INT4                i4RetVal;
#endif
    UINT4               u4Index;

    Bgp4InitAddrPrefixStruct (&(InvPrefix), BGP4_RT_AFI_INFO (pRtProfile));
    /* Non-BGP Default routes are learnt from IP. Should not be added
     * back to IP */
    if (((PrefixMatch
          (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
           (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)), InvPrefix)) == BGP4_TRUE)
        && (BGP4_RT_PREFIXLEN (pRtProfile) == 0)
        && (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID))

    {
        /* Route is a non-BGP Default route learnt from IP */
        return (BGP4_SUCCESS);
    }

    pVpnRoute = (tRtInfo *) Bgp4MemAllocateMsg (sizeof (tRtInfo));
    if (pVpnRoute == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_FDB_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for adding Route to IP_VRF FAILED!!!\n");
        return BGP4_FAILURE;

    }
    MEMSET (pVpnRoute, 0, sizeof (tRtInfo));

    pRouteInfo = BGP4_RT_BGP_INFO (pRtProfile);
    PTR_FETCH_4 (u4Dest, BGP4_RT_IP_PREFIX (pRtProfile));
    PTR_FETCH_4 (u4Gate, BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile));
    if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
    {
        PTR_FETCH_4 (u4BgpGate,
                     BGP4_INFO_NEXTHOP (BGP4_RT_BGP_INFO (pRtProfile)));
    }
    else
    {
        u4BgpGate = 0;
    }

    u4Mask = Bgp4GetSubnetmask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile));
    /* For the aggregated route the protocol has to be BGP_ID.
     * For all other case, the protocol value will be as
     * present in the route profile. */
    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGREGATED)
        == BGP4_RT_AGGREGATED)
    {
        u1Protocol = BGP_ID;
    }
    else
    {
        u1Protocol = BGP4_RT_PROTOCOL (pRtProfile);
    }

    u1RcvdPrefLen = (UINT1) BGP4_RT_PREFIXLEN (pRtProfile);

    pVpnRoute->u4DestNet = u4Dest;
    pVpnRoute->u4DestMask = u4Mask;
    pVpnRoute->u4NextHop = u4Gate;
    if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
    {
#ifdef L3VPN_PORT
        pVpnRoute->u4BgpNextHop = u4BgpGate;
#endif
    }
    pVpnRoute->u4RowStatus = IPFWD_ACTIVE;
    pVpnRoute->u2RtProto = u1Protocol;
    pVpnRoute->i4Metric1 = BGP4_RT_MED (pRtProfile);
    pVpnRoute->u4RtIfIndx = BGP4_RT_GET_RT_IF_INDEX (pRtProfile);
    pVpnRoute->u2RtType = CIDR_REMOTE_ROUTE_TYPE;
    if ((BGP4_RT_PEER_ENTRY (pRtProfile) == NULL) || (u1Protocol != BGP_ID))
    {
        pVpnRoute->u4RtNxtHopAS = 0;
    }
    else
    {                            /* Route learnt from BGP peer */
        pVpnRoute->u4RtNxtHopAS =
            BGP4_PEER_ASNO ((BGP4_RT_PEER_ENTRY (pRtProfile)));
        /* Fill IGP label */
    }

    /* Fill VRF id and BGP label */
#ifdef L3VPN_PORT
    pVpnRoute->u4VRFIndex = u4VrfId;
#endif
    if ((BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID) &&
        (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRtProfile)) ==
         BGP4_VPN4_CE_PEER))
    {
        if (BGP4_PEER_CXT_ID (BGP4_RT_PEER_ENTRY (pRtProfile)) == u4VrfId)
        {
#ifdef L3VPN_PORT
            pVpnRoute->u4NHVrfId = BGP4_PEER_CXT_ID
                (BGP4_RT_PEER_ENTRY (pRtProfile));
#endif
        }
    }
    for (u4Index = 0; u4Index < BGP4_RT_LABEL_CNT (pRtProfile); u4Index++)
    {
#ifdef L3VPN_PORT
        pVpnRoute->au4BGPLabel[u4Index] = BGP4_RT_LABEL_INFO (pRtProfile);
#endif
    }
#ifdef L3VPN_PORT
    pVpnRoute->u4IGPLblTunIdx = BGP4_RT_IGP_LSP_ID (pRtProfile);
#endif

#ifdef L3VPN_TEST_WANTED
    /* Calling Routine to Modify Route to the IP Table */
    /*i4RetVal = IpForwardingTableModifyRoute (pVpnRoute); */
    if ((i4RetVal == IP_FAILURE) || (i4RetVal == IP_ROUTE_NOT_FOUND))
        /* Calling Routine to Add Route to the IP Table */
    {
        BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tModifying IP VRF[%d] Table with  Dest = %s "
                       "Mask = %s\n\t" "Nexthop = %s FAILED !!!\n",
                       u4VrfId,
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        Bgp4MemReleaseMsg ((UINT1 *) pVpnRoute);
        return BGP4_FAILURE;
    }
#endif

    BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC |
                   BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                   "\tModifying IP VRF[%d] Table with  Dest = %s "
                   "Mask = %s\n\t" "Nexthop = %s SUCCESS!!!\n",
                   u4VrfId,
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)));
    Bgp4MemReleaseMsg ((UINT1 *) pVpnRoute);
    return BGP4_SUCCESS;
}
#endif
/********************************************************************/
/* Function Name   : Bgp4Vpnv4GetVrfFlags                           */
/* Description     : Gets the VRF specific flags for a give vrf-id  */
/* Input(s)        : u4VrfId - VRF identifer                        */
/*                   pRtProfile - route information                 */
/* Output(s)       : pu4Flags - pointer to the VRF specific flags   */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4Vpnv4GetVrfFlags (tRouteProfile * pRtProfile, UINT4 u4VrfId,
                      UINT4 *pu4Flags)
{
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;

    if ((BGP4_RR_CLIENT_CNT (u4VrfId) > 0) &&
        (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) == 0))
    {
        *pu4Flags = 0;
        return BGP4_SUCCESS;
    }
    if (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) == 0)
    {
        return BGP4_FAILURE;
    }
    TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile), pRtVrfInfo,
                  tRtInstallVrfInfo *)
    {
        if (BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrfInfo) == u4VrfId)
        {
            *pu4Flags = BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo);
            return BGP4_SUCCESS;
        }
    }
    return BGP4_FAILURE;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4SetVrfFlags                           */
/* Description     : Sets the VRF specific flags for a give vrf-id  */
/* Input(s)        : u4VrfId - VRF identifer                        */
/*                   pRtProfile - route information                 */
/*                   u4Flags - flag  to be set                      */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4Vpnv4SetVrfFlags (tRouteProfile * pRtProfile, UINT4 u4VrfId, UINT4 u4Flags)
{
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;

    if ((BGP4_RR_CLIENT_CNT (u4VrfId) > 0) &&
        (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) == 0))
    {
        return BGP4_SUCCESS;
    }
    if (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) == 0)
    {
        return BGP4_FAILURE;
    }
    TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile), pRtVrfInfo,
                  tRtInstallVrfInfo *)
    {
        if (BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrfInfo) == u4VrfId)
        {
            BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo) |= u4Flags;
            return BGP4_SUCCESS;
        }
    }
    return BGP4_FAILURE;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4ResetVrfFlags                         */
/* Description     : Resets the VRF specific flags for a give vrf-id*/
/* Input(s)        : u4VrfId - VRF identifer                        */
/*                   pRtProfile - route information                 */
/*                   u4Flags - flag  to be reset                    */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4Vpnv4ResetVrfFlags (tRouteProfile * pRtProfile, UINT4 u4VrfId,
                        UINT4 u4Flags)
{
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;

    if ((BGP4_RR_CLIENT_CNT (u4VrfId) > 0) &&
        (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) == 0))
    {
        return BGP4_SUCCESS;
    }
    if (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) == 0)
    {
        return BGP4_FAILURE;
    }
    TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile), pRtVrfInfo,
                  tRtInstallVrfInfo *)
    {
        if (BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrfInfo) == u4VrfId)
        {
            BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo) &= ~(u4Flags);
            return BGP4_SUCCESS;
        }
    }
    return BGP4_FAILURE;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4SetFlagsForAllVrf                     */
/* Description     : Sets the VRF specific flags for All Vrfs       */
/* Input(s)        : pRtProfile - route information                 */
/*                   u4Flags - flag  to be reset                    */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4Vpnv4SetFlagsForAllVrf (tRouteProfile * pRtProfile, UINT4 u4Flags)
{
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
    UINT4               u4VrfId;

    BGP4_VPN4_GET_PROCESS_VRFID (pRtProfile, u4VrfId)
        if ((BGP4_RR_CLIENT_CNT (u4VrfId) > 0) &&
            (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) == 0))
    {
        return BGP4_SUCCESS;
    }
    if (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) == 0)
    {
        return BGP4_FAILURE;
    }
    TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile), pRtVrfInfo,
                  tRtInstallVrfInfo *)
    {
        BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo) |= u4Flags;
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4ResetFlagsForAllVrf                   */
/* Description     : Resets the VRF specific flags for All Vrfs     */
/* Input(s)        : pRtProfile - route information                 */
/*                   u4Flags - flag  to be reset                    */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4Vpnv4ResetFlagsForAllVrf (tRouteProfile * pRtProfile, UINT4 u4Flags)
{
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
    UINT4               u4VrfId;
    BGP4_VPN4_GET_PROCESS_VRFID (pRtProfile, u4VrfId);

    if ((BGP4_RR_CLIENT_CNT (u4VrfId) > 0) &&
        (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) == 0))
    {
        return BGP4_SUCCESS;
    }
    if (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) == 0)
    {
        return BGP4_FAILURE;
    }
    TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile), pRtVrfInfo,
                  tRtInstallVrfInfo *)
    {
        BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo) &= ~(u4Flags);
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4GetVrfHashKey                         */
/* Description     : Gets the HashKey for a given vrf-id            */
/* Input(s)        : u4VrfId - VRF identifier                       */
/* Output(s)       : pu1HashKey - pointer to the hashkey value      */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4Vpnv4GetVrfHashKey (UINT4 u4VrfId, UINT1 *pu1HashKey)
{
    UINT1               u1Index;
    UINT1               u1BitCnt = 0;

    for (u1Index = 0; u1Index < BGP4_FOUR_BYTE_BITS; u1Index++)
    {
        if (u4VrfId & BGP4_MSB)
        {
            u1BitCnt++;
        }
        u4VrfId = u4VrfId << 1;
    }
    *pu1HashKey = (UINT1) (u1BitCnt % BGP4_MAX_VRF_HASH_TBL_SIZE);
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4GetLabelInfo                          */
/* Description     : Gets the Label inforamtion present in the      */
/*                   route profile and the length of the label info */
/* Input(s)        : pRtProfile - route information                 */
/* Output(s)       : pu1Label - label information                   */
/*                   pu4LabelLength - length of the label info      */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS                         */
/********************************************************************/
INT4
Bgp4Vpnv4GetLabelInfo (tRouteProfile * pRtProfile, UINT1 *pu1Label,
                       UINT4 *pu4LabelLength)
{
    UINT4               u4Label;

    if (BGP4_RT_LABEL_CNT (pRtProfile) > 0)
    {
        u4Label = BGP4_RT_LABEL_INFO (pRtProfile);
        MEMCPY (pu1Label, (UINT1 *) &u4Label, BGP4_VPN4_LABEL_SIZE);
        *pu4LabelLength = BGP4_VPN4_LABEL_SIZE;
    }
    else
    {
        *pu4LabelLength = 0;
    }
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name : Bgp4Vpnv4ReleaseRouteTargetsFromVrf                      */
/* Description   : Releases the given list of route targets.                */
/* Input(s)      : List of route targets (pTsList)                          */
/*                 type of the route targets                                */
/* Output(s)     : None.                                                    */
/* Return(s)     : Number of Nodes removed.                                 */
/****************************************************************************/
UINT4
Bgp4Vpnv4ReleaseRouteTargetsFromVrf (tTMO_SLL * pTsList, UINT1 u1RTType)
{
    tExtCommProfile    *pRouteTarget = NULL;
    tBgp4RrImportTargetInfo *pRrImportTarget = NULL;
    UINT4               u4TargetCnt = 0;

    if (TMO_SLL_Count (pTsList) == 0)
    {
        return 0;
    }

    for (;;)
    {
        pRouteTarget = (tExtCommProfile *) TMO_SLL_First (pTsList);
        if (pRouteTarget == NULL)
        {
            break;
        }
        if (u1RTType == BGP4_IMPORT_ROUTE_TARGET_TYPE)
        {
            /* delete this route target from the RR import route target set if
             * it is present
             */
            pRrImportTarget =
                Bgp4Vpnv4IsRTMatchInRRImportRTList (pRouteTarget->au1ExtComm);
            if (pRrImportTarget != NULL)
            {
                TMO_SLL_Delete (BGP4_VPN4_RR_TARGETS_SET,
                                (tTMO_SLL_NODE *) pRrImportTarget);
                Bgp4MemReleaseRrImportTarget (pRrImportTarget);
            }
        }
        TMO_SLL_Delete (pTsList, (tTMO_SLL_NODE *) pRouteTarget);
        EXT_COMM_EXT_COMM_PROFILE_NODE_FREE (pRouteTarget);
        u4TargetCnt++;
    }
    TMO_SLL_Init (pTsList);
    return (u4TargetCnt);
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4VrfCreate                             */
/* Description     : Creates a new VRF                              */
/* Input(s)        : u4VrfId - vrf identifier.                      */
/* Output(s)       : ppVrfInfo - address of vrf pointer             */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : None.                                */
/********************************************************************/
INT4
Bgp4Vpnv4VrfCreate (UINT4 u4VrfId)
{
    Bgp4InitNetAddressStruct (&(BGP4_LSP_CHG_INIT_ROUTE_NETADDR_INFO),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_VPNV4_UNICAST);
    BGP4_LSP_CHG_INIT_VRFID (u4VrfId) = BGP4_DFLT_VRFID;
    TMO_SLL_Init (BGP4_VPN4_VRF_SPEC_IMPORT_TARGETS (u4VrfId));
    TMO_SLL_Init (BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS (u4VrfId));
    /*MEMSET (BGP4_VPN4_VRF_SPEC_ROUTE_DISTING (u4VrfId), 0,
       BGP4_VPN4_ROUTE_DISTING_SIZE); */
    BGP4_VPN4_VRF_SPEC_GET_FLAG (u4VrfId) = 0;
    Bgp4InitNetAddressStruct (&(BGP4_VPN4_VRF_SPEC_INIT_NET_ADDR (u4VrfId)),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_VPNV4_UNICAST);
    BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) = BGP4_L3VPN_VRF_CREATE;
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4VrfDelete                             */
/* Description     : Deletes an existing VRF                        */
/* Input(s)        : u4VrfId - vrf identifier.                      */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : None.                                */
/********************************************************************/
INT4
Bgp4Vpnv4VrfDelete (UINT4 u4VrfId)
{
    Bgp4Vpnv4ReleaseRouteTargetsFromVrf
        (BGP4_VPN4_VRF_SPEC_IMPORT_TARGETS (u4VrfId),
         BGP4_IMPORT_ROUTE_TARGET_TYPE);
    Bgp4Vpnv4ReleaseRouteTargetsFromVrf
        (BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS (u4VrfId),
         BGP4_EXPORT_ROUTE_TARGET_TYPE);
    MEMSET (BGP4_VPN4_VRF_SPEC_ROUTE_DISTING (u4VrfId), 0,
            BGP4_VPN4_ROUTE_DISTING_SIZE);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4IfGetEntryByVrfId                                     */
/* Description   : Function to get an interface entry from global list for a */
/*                 given vrf index                                           */
/* Input(s)      : u4VrfIndex    - VRF index                                 */
/* Output(s)     : None.                                                     */
/* Return(s)     : pIfEntry - if present, else NULL                          */
/*****************************************************************************/
tBgp4IfInfo        *
Bgp4IfGetEntryByVrfId (UINT4 u4VrfIndex)
{
    tBgp4IfInfo        *pIfEntry = NULL;

    TMO_SLL_Scan (BGP4_IFACE_GLOBAL_LIST, pIfEntry, tBgp4IfInfo *)
    {
        if (BGP4_IFACE_ENTRY_VRFID (pIfEntry) == u4VrfIndex)
        {
            /* Entry found and return */
            return pIfEntry;
        }
    }
    return (NULL);
}

/****************************************************************************/
/* Function Name : Bgp4GetIgpLspFromMplsForNextHop                          */
/* Description   :                                                          */
/* Input(s)      :                                                          */
/* Output(s)     : None.                                                    */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                */
/****************************************************************************/
INT4
Bgp4GetIgpLspFromMplsForNextHop (tBgp4IgpMetricEntry * pIgpMetricEntry,
                                 UINT4 *pu4LspId, UINT1 *pu1LspStatus)
{
    UINT4               u4LspDestAddr;
#ifdef L3VPN_PORT
#ifdef MPLS_L3VPN_WANTED
    INT4                i4RetStatus = BGP4_FAILURE;
#endif
#endif
    UINT4               u4IfIndex = 0;

    if (BGP4_NH_METRIC_IFINDEX (pIgpMetricEntry) == BGP4_INVALID_IFINDX)
    {
        return BGP4_FAILURE;
    }
#ifdef L3VPN_PORT
    NetIpv4GetCfaIfIndexFromPort ((UINT2)
                                  (BGP4_NH_METRIC_IFINDEX (pIgpMetricEntry)),
                                  &u4IfIndex);
#endif
    PTR_FETCH_4 (u4LspDestAddr,
                 ((BGP4_NH_METRIC_NEXTHOP_INFO (pIgpMetricEntry)).au1Address));
#ifdef L3VPN_PORT
#ifdef MPLS_L3VPN_WANTED

    i4RetStatus = (UINT1) (MplsGetRsvpTeLspForDest (OSIX_HTONL (u4LspDestAddr),
                                                    pu4LspId, pu1LspStatus));
    if ((i4RetStatus == OSIX_FAILURE) || ((*pu1LspStatus) != BGP4_LSP_UP))
    {
        i4RetStatus =
            (UINT1) (MplsGetNonTeLspForDest
                     (OSIX_HTONL (u4LspDestAddr), pu4LspId, pu1LspStatus));
        if ((i4RetStatus == OSIX_FAILURE) || ((*pu1LspStatus) != BGP4_LSP_UP))
        {
            return BGP4_FAILURE;
        }
        MplsL3VpnBgp4LspUsageStatusUpdate (OSIX_HTONL (u4LspDestAddr),
                                           *pu4LspId, BGP4_MPLS_LSP_SET);
    }
#else
    UNUSED_PARAM (pu1LspStatus);
    UNUSED_PARAM (pu4LspId);
#endif
#else
    UNUSED_PARAM (pu1LspStatus);
    UNUSED_PARAM (pu4LspId);
#endif
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name : Bgp4GetAlternateLspFromMplsForNextHop                    */
/* Description   :                                                          */
/* Input(s)      :                                                          */
/* Output(s)     : None.                                                    */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                */
/****************************************************************************/
INT4
Bgp4GetAlternateLspFromMplsForNextHop (tBgp4IgpMetricEntry * pIgpMetricEntry,
                                       UINT4 *pu4LspId, UINT1 *pu1LspStatus)
{
    UINT4               u4LspDestAddr;
#ifdef L3VPN_PORT
#ifdef MPLS_L3VPN_WANTED
    INT4                i4RetStatus = BGP4_FAILURE;
#endif
#endif
    UINT4               u4IfIndex = 0;

#ifndef L3VPN_PORT
    UNUSED_PARAM (pu4LspId);
    UNUSED_PARAM (pu1LspStatus);
#endif

#ifdef L3VPN_PORT
    NetIpv4GetCfaIfIndexFromPort ((UINT2)
                                  (BGP4_NH_METRIC_IFINDEX (pIgpMetricEntry)),
                                  &u4IfIndex);
#endif
    PTR_FETCH_4 (u4LspDestAddr,
                 ((BGP4_NH_METRIC_NEXTHOP_INFO (pIgpMetricEntry)).au1Address));
#ifdef L3VPN_PORT
#ifdef MPLS_L3VPN_WANTED

    i4RetStatus = (UINT1) (MplsGetRsvpTeLspForDest (OSIX_HTONL (u4LspDestAddr),
                                                    pu4LspId, pu1LspStatus));
    if ((i4RetStatus == OSIX_FAILURE) || ((*pu1LspStatus) != BGP4_LSP_UP))
    {
        i4RetStatus =
            (UINT1) (MplsGetNonTeLspForDest
                     (OSIX_HTONL (u4LspDestAddr), pu4LspId, pu1LspStatus));
        if ((i4RetStatus == OSIX_FAILURE) || ((*pu1LspStatus) != BGP4_LSP_UP))
        {
            return BGP4_FAILURE;
        }
        MplsL3VpnBgp4LspUsageStatusUpdate (OSIX_HTONL (u4LspDestAddr),
                                           *pu4LspId, BGP4_MPLS_LSP_SET);
    }
#else
    UNUSED_PARAM (pu1LspStatus);
    UNUSED_PARAM (pu4LspId);
#endif
#endif
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4UpdateLspStatusforNextHop                  */
/* Description     : Gets the Lsp status from Mpls                  */
/* Input(s)        : pIgpMetricEntry - Igp Metric Entry for Nexthop */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : None.                                */
/********************************************************************/
INT4
Bgp4UpdateLspStatusforNextHop (tBgp4IgpMetricEntry * pIgpMetricEntry)
{
#ifdef L3VPN_PORT
    UINT4               u4LspId = 0;
    UINT1               u1LspStatus;
    INT4                i4RetStatus;
#endif

#ifndef L3VPN_PORT
    UNUSED_PARAM (pIgpMetricEntry);
#endif

#ifdef L3VPN_PORT
    i4RetStatus = Bgp4GetIgpLspFromMplsForNextHop (pIgpMetricEntry,
                                                   &u4LspId, &u1LspStatus);
    if (i4RetStatus == BGP4_FAILURE)
    {
        BGP4_NH_METRIC_LSP_STATUS (pIgpMetricEntry) = BGP4_LSP_DOWN;
        return BGP4_FAILURE;
    }
    BGP4_NH_METRIC_LSPID (pIgpMetricEntry) = u4LspId;
    BGP4_NH_METRIC_LSP_STATUS (pIgpMetricEntry) = BGP4_LSP_UP;
#endif
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4GetBgp4Label                               */
/* Description     : Get the label from the range already created   */
/* Input(s)        : None                                           */
/* Output(s)       : pu4BgpLabel - label                            */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : None.                                */
/********************************************************************/
INT4
Bgp4GetBgp4Label (UINT4 *pu4BgpLabel)
{
#ifndef LINUX_SLI_WANTED
#ifdef MPLS_WANTED
    UINT4               u4Key1 = 0;
    UINT4               u4Key2 = 0;
    INT4                i4RetStatus = BGP4_FAILURE;

    /* Get the label from the label group already created */
    i4RetStatus =
        lblMgrGetLblFromLblGroup (BGP4_LBL_GROUP_ID, &u4Key1, &u4Key2);
    if (i4RetStatus == LBL_FAILURE)
    {
        return BGP4_FAILURE;
    }
    *pu4BgpLabel = u4Key2;
#else
    UNUSED_PARAM (pu4BgpLabel);
#endif
#else
    UNUSED_PARAM (pu4BgpLabel);
#endif
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4DelBgp4Label                               */
/* Description     : Releases the label from the range              */
/* Input(s)        : None                                           */
/* Output(s)       : pu4BgpLabel - label                            */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : None.                                */
/********************************************************************/
INT4
Bgp4DelBgp4Label (UINT4 u4BgpLabel)
{
#ifndef LINUX_SLI_WANTED
#ifdef MPLS_WANTED
    UINT4               u4Key1 = 0;
    INT4                i4Ret;
    /* Release the label from the label group already created */
    i4Ret = LblMgrRelLblToLblGroup (BGP4_LBL_GROUP_ID, u4Key1, u4BgpLabel);
    if (i4Ret == LBL_FAILURE)
    {
        return BGP4_FAILURE;
    }
#else
    UNUSED_PARAM (u4BgpLabel);
#endif
#else
    UNUSED_PARAM (u4BgpLabel);
#endif
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name : Bgp4UpdateLabelInMplsFmTable                             */
/* Description   : Updates the ILM entries in MPLS                          */
/* Input(s)      : pPeer - peer information (PE)                            */
/*                 u4AfiSafiIndex - index to identify VPN/CLB               */
/*                 u1LabelActiion - label action POP/PUSH/SWAP              */
/*                 u2IlmAction - ILM action ADD/DELETE                      */
/* Output(s)     : None.                                                    */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                */
/****************************************************************************/
INT4
Bgp4UpdateLabelInMplsFmTable (tBgp4PeerEntry * pPeer, UINT4 u4AfiSafiIndex,
                              UINT1 u1LabelAction, UINT2 u2IlmAction)
{
    tBgp4IfInfo        *pIfEntry = NULL;
    tBgp4IfInfo        *pIfInfo = NULL;
    UINT4               u4Rt;
    UINT4               u4Addr;
    UINT4               u4RtPort = 0;
    UINT4               u4RtIfIndx;
    INT4                i4Metric;
    INT4                i4Ret = BGP4_SUCCESS;

    /* Right now we do support label carrying only for <ipv4, label>
     * and <vpnv4, label> address families. other than that, we should
     * return
     */
    if ((u4AfiSafiIndex != BGP4_IPV4_LBLD_INDEX) &&
        (u4AfiSafiIndex != BGP4_VPN4_UNI_INDEX))
    {
        return BGP4_FAILURE;
    }

    pIfEntry = Bgp4IfGetEntry (BGP4_PEER_IF_INDEX (pPeer));
    if (pIfEntry != NULL)
    {
        if (BGP4_IFACE_ENTRY_IFTYPE (pIfEntry) == BGP4_LOOPBACK_IFACE)
        {
            /* The peer session is established on the loop back interface.
             * We need to update the ILM entries with all the reachable
             * interfaces to this neighbor along with the outgoing labels
             */
            PTR_FETCH_4 (u4Addr, BGP4_PEER_REMOTE_ADDR (pPeer));
            i4Ret = BgpIp4RtLookup (BGP4_DFLT_VRFID,
                                    u4Addr,
                                    BGP4_MAX_PREFIXLEN, &u4Rt, &i4Metric,
                                    &u4RtPort, BGP4_ALL_APPIDS);
            if (i4Ret == BGP4_FAILURE)
            {
                /* ??????????? */
                return (i4Ret);
            }
#ifdef L3VPN_PORT
            NetIpv4GetCfaIfIndexFromPort ((UINT2) u4RtPort, &u4RtIfIndx);
#endif

            for (;;)
            {
                Bgp4GetIfInfoFromIfIndex (u4RtIfIndx, &pIfInfo);
                if (pIfInfo == NULL)
                {
                    return BGP4_FAILURE;
                }
                if (u2IlmAction == BGP4_FM_LABEL_ADD)
                {
                    BGP4_IFACE_LABEL_PEERS (pIfInfo)++;
                }
                else if (BGP4_IFACE_LABEL_PEERS (pIfEntry) > 0)
                {
                    BGP4_IFACE_LABEL_PEERS (pIfInfo)--;
                }
                if ((u2IlmAction == BGP4_FM_LABEL_ADD) ||
                    ((u2IlmAction == BGP4_FM_LABEL_DELETE) &&
                     (BGP4_IFACE_LABEL_PEERS (pIfEntry) == 0)))
                {
                    i4Ret = Bgp4UpdateILMEntryInFmTable (u4RtIfIndx,
                                                         u1LabelAction,
                                                         u2IlmAction,
                                                         u4AfiSafiIndex);
                    if (i4Ret == BGP4_FAILURE)
                    {
                        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                       BGP4_ALL_FAILURE_TRC |
                                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                                       "\tBgp4UpdateILMEntryInFmTable returned FAILURE\n");
                    }
                }
                i4Ret = BgpIp4GetNextRtEntry (BGP4_DFLT_VRFID, u4Rt,
                                              BGP4_MAX_PREFIXLEN, u4RtIfIndx,
                                              &u4RtPort, &i4Metric,
                                              &u4RtIfIndx);
                if (i4Ret == BGP4_FAILURE)
                {
                    i4Ret = BGP4_SUCCESS;
                    break;
                }
#ifdef L3VPN_PORT
                u4RtIfIndx =
                    (UINT4) (NetIpv4GetCfaIfIndexFromPort
                             ((UINT2) u4RtPort, &u4RtIfIndx));
#endif
            }
        }
        else
        {
            /* The peer session is established on nomral interface.
             * Just update the ILM entries with incoming interface as 
             * peer index and the outgoing labels
             */
            if (u2IlmAction == BGP4_FM_LABEL_ADD)
            {
                BGP4_IFACE_LABEL_PEERS (pIfEntry)++;
            }
            else if (BGP4_IFACE_LABEL_PEERS (pIfEntry) > 0)
            {
                BGP4_IFACE_LABEL_PEERS (pIfEntry)--;
            }
            if ((u2IlmAction == BGP4_FM_LABEL_ADD) ||
                ((u2IlmAction == BGP4_FM_LABEL_DELETE) &&
                 (BGP4_IFACE_LABEL_PEERS (pIfEntry) == 0)))
            {

                i4Ret = Bgp4UpdateILMEntryInFmTable (BGP4_PEER_IF_INDEX (pPeer),
                                                     u1LabelAction, u2IlmAction,
                                                     u4AfiSafiIndex);
            }
        }
    }
    return (i4Ret);
}

/****************************************************************************/
/* Function Name : Bgp4UpdateILMEntryInFmTable                              */
/* Description   : Updates the ILM entries in MPLS                          */
/* Input(s)      : pPeer - peer information (PE)                            */
/*                 u4IfIndex - incoming interface index                     */
/*                 u1LabelActiion - label action POP/PUSH/SWAP              */
/*                 u2IlmAction - ILM action ADD/DELETE                      */
/*                 u4AfiSafiIndex - index to identify VPN/CLB               */
/* Output(s)     : None.                                                    */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                */
/****************************************************************************/
INT4
Bgp4UpdateILMEntryInFmTable (UINT4 u4IfIndex, UINT1 u1LabelAction,
                             UINT2 u2IlmAction, UINT4 u4AfiSafiIndex)
{
    tBgp4IfInfo        *pIfEntry = NULL;
    INT4                i4Ret = BGP4_FAILURE;

    TMO_SLL_Scan (BGP4_IFACE_GLOBAL_LIST, pIfEntry, tBgp4IfInfo *)
    {
        /* If label is not present, ignore this interface */
        if (BGP4_IFACE_ENTRY_LABEL (pIfEntry) == BGP4_INVALID_LABEL)
        {
            continue;
        }

        /* If this is the incoming interface, continue with the next one */
        if (BGP4_IFACE_INDEX (pIfEntry) == u4IfIndex)
        {
            continue;
        }

        /* Update the MPLS about the ILM entry with (Incoming, Label)
         * Incoming - peer interface index and the corresponding label
         */
        switch (u4AfiSafiIndex)
        {
            case BGP4_IPV4_LBLD_INDEX:
                i4Ret =
                    Bgp4MplsLabelUpdateInFmTable (BGP4_IFACE_ENTRY_LABEL
                                                  (pIfEntry), u4IfIndex,
                                                  BGP4_DFLT_VRFID, u2IlmAction,
                                                  u1LabelAction);
                break;
            case BGP4_VPN4_UNI_INDEX:
                i4Ret =
                    Bgp4MplsLabelUpdateInFmTable (BGP4_IFACE_ENTRY_LABEL
                                                  (pIfEntry), u4IfIndex,
                                                  BGP4_IFACE_ENTRY_VRFID
                                                  (pIfEntry), u2IlmAction,
                                                  u1LabelAction);
                break;
            default:
                return BGP4_FAILURE;
        }
    }
    return (i4Ret);
}

/****************************************************************************/
/* Function Name : Bgp4MplsLabelUpdateInFmTable                             */
/* Description   : Updates the ILM entries in MPLS                          */
/* Input(s)      : u4Label - label                                          */
/*                 u4InIfIndex - incoming interface index                   */
/*                 u4VrfId - VRF identifier                                 */
/*                 u2IlmAction - ILM action ADD/DELETE                      */
/*                 u1LabelActiion - label action POP/PUSH/SWAP              */
/* Output(s)     : None.                                                    */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                */
/****************************************************************************/
INT4
Bgp4MplsLabelUpdateInFmTable (UINT4 u4Label, UINT4 u4InIfIndex,
                              UINT4 u4VrfId, UINT2 u2IlmAction,
                              UINT1 u1LabelAction)
{

    UNUSED_PARAM (u4Label);
    UNUSED_PARAM (u4InIfIndex);
    UNUSED_PARAM (u4VrfId);
    UNUSED_PARAM (u2IlmAction);
    UNUSED_PARAM (u1LabelAction);
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4RsvpTeLspChngHandler                       */
/* Description     : Process Lsp UP Event                           */
/* Input(s)        : u4LspLabel,u1LspStatus -UP/DOWN                */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : None.                                */
/********************************************************************/
INT4
Bgp4RsvpTeLspChngHandler (UINT4 u4LspLabel, UINT1 u1LspStatus)
{

    tLspNode           *pLspNode = NULL;
    tBgp4IgpMetricEntry *pIgpMetricEntry = NULL;
    INT4                i4RetVal;

    i4RetVal = Bgp4IgpMetricTblGetEntryForLspId (u4LspLabel, &pIgpMetricEntry);
    if ((i4RetVal == BGP4_FAILURE) || (pIgpMetricEntry == NULL))
    {
        return BGP4_FAILURE;
    }
    TMO_SLL_Scan (BGP4_LSP_CHG_LIST, pLspNode, tLspNode *)
    {
        if (pLspNode->u4LspId == u4LspLabel)
        {
            pLspNode->u1ChngSts = u1LspStatus;
            return BGP4_SUCCESS;
        }
    }
    pLspNode = (tLspNode *) MemAllocMemBlk (BGP4_VPN4_LSP_NODE_CHG_MEMPOOL_ID);

    if (pLspNode == NULL)
    {
        return BGP4_FAILURE;
    }

    pLspNode->u4LspId = u4LspLabel;
    pLspNode->u1ChngSts = u1LspStatus;
    TMO_SLL_Add (BGP4_LSP_CHG_LIST, (tTMO_SLL_NODE *) pLspNode);
    return (BGP4_SUCCESS);

}

/********************************************************************/
/* Function Name   : Bgp4LspChngHandler                             */
/* Description     : Process Lsp UP Event                           */
/* Input(s)        : pIgpMetricEntry                                */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : None.                                */
/********************************************************************/
INT4
Bgp4LspChngHandler (UINT4 u4LspId, UINT1 u1ChngSts)
{
    tLspNode           *pLspNode = NULL;
    tBgp4IgpMetricEntry *pIgpMetricEntry = NULL;
    INT4                i4RetVal;
    UINT1               u1MatchFound = BGP4_FALSE;

    i4RetVal = Bgp4IgpMetricTblGetEntryForLspId (u4LspId, &pIgpMetricEntry);
    if ((i4RetVal == BGP4_FAILURE) || (pIgpMetricEntry == NULL))
    {
        return BGP4_FAILURE;
    }
    TMO_SLL_Scan (BGP4_LSP_CHG_LIST, pLspNode, tLspNode *)
    {
        if (pLspNode->u4LspId == u4LspId)
        {
            u1MatchFound = BGP4_TRUE;
            break;
        }
    }
    if (u1MatchFound == BGP4_TRUE)
    {
        pLspNode->u1ChngSts = u1ChngSts;
        return BGP4_SUCCESS;
    }
    pLspNode = (tLspNode *) MemAllocMemBlk (BGP4_VPN4_LSP_NODE_CHG_MEMPOOL_ID);

    if (pLspNode == NULL)
    {
        return BGP4_FAILURE;
    }

    pLspNode->u4LspId = u4LspId;
    pLspNode->u1ChngSts = u1ChngSts;
    TMO_SLL_Add (BGP4_LSP_CHG_LIST, (tTMO_SLL_NODE *) pLspNode);
    return (BGP4_SUCCESS);
}

/********************************************************************/
/* Function Name   : Bgp4ProcessLspChngList                         */
/* Description     : Handles the LSP notifications from MPLS        */
/*                   This function is called periodically to handle */
/*                   any LSP notifications.                         */
/* Input(s)        : None.                                          */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_FALSE - LSP notifications over  */
/*                           : BGP4_TRUE -  LSP notifications pend  */
/********************************************************************/
INT4
Bgp4ProcessLspChngList (VOID)
{
    tBgp4IgpMetricEntry *pIgpMetricEntry = NULL;
    tLspNode           *pLspNode = NULL;
    tLspNode           *pTmpLspNode = NULL;
    UINT4               u4LspCnt = 0;
    INT4                i4Sts = BGP4_FALSE;

    BGP_SLL_DYN_Scan (BGP4_LSP_CHG_LIST, pLspNode, pTmpLspNode, tLspNode *)
    {
        Bgp4IgpMetricTblGetEntryForLspId (pLspNode->u4LspId, &pIgpMetricEntry);
        if (pIgpMetricEntry == NULL)
        {
            TMO_SLL_Delete (BGP4_LSP_CHG_LIST, (tTMO_SLL_NODE *) pLspNode);
            MemReleaseMemBlock (BGP4_VPN4_LSP_NODE_CHG_MEMPOOL_ID,
                                (UINT1 *) pLspNode);
            continue;
        }
        if (TMO_SLL_Count (BGP4_LSP_CHG_LIST) == 0)
        {
            return BGP4_FALSE;
        }
        switch (pLspNode->u1ChngSts)
        {
            case BGP4_LSP_UP:
                i4Sts = BGP4_FALSE;
                if ((BGP4_NH_METRIC_LSP_STATUS (pIgpMetricEntry) &
                     BGP4_LSP_UP) != BGP4_LSP_UP)
                {
                    BGP4_NH_METRIC_LSP_STATUS (pIgpMetricEntry) = BGP4_LSP_UP;
                    BGP4_TRC_ARG2 (NULL,
                                   BGP4_TRC_FLAG, BGP4_DETAIL_TRC,
                                   BGP4_MOD_NAME,
                                   "\tCalling Bgp4LspUpHandler Func[%s] Line[%d] \r\n\r",
                                   __func__, __LINE__);
                    i4Sts = Bgp4LspUpHandler (pIgpMetricEntry);
                }
                break;
            case BGP4_LSP_DOWN:
                i4Sts = BGP4_FALSE;
                if ((BGP4_NH_METRIC_LSP_STATUS (pIgpMetricEntry) &
                     BGP4_LSP_DOWN) != BGP4_LSP_DOWN)
                {
                    BGP4_TRC_ARG2 (NULL,
                                   BGP4_TRC_FLAG, BGP4_DETAIL_TRC,
                                   BGP4_MOD_NAME,
                                   "\tCalling Bgp4LspDownHandler Func[%s] Line[%d] \r\n\r",
                                   __func__, __LINE__);
                    i4Sts = Bgp4LspDownHandler (pIgpMetricEntry);
                }
                break;
            case BGP4_LSP_DESTROY:
                i4Sts = BGP4_FALSE;
                if ((BGP4_NH_METRIC_LSP_STATUS (pIgpMetricEntry) &
                     BGP4_LSP_DOWN) != BGP4_LSP_DOWN)
                {
                    BGP4_TRC_ARG2 (NULL,
                                   BGP4_TRC_FLAG, BGP4_DETAIL_TRC,
                                   BGP4_MOD_NAME,
                                   "\tCalling Bgp4LspDownHandler Func[%s] Line[%d] \r\n\r",
                                   __func__, __LINE__);
                    i4Sts = Bgp4LspDownHandler (pIgpMetricEntry);
                }
                BGP4_NH_METRIC_LSPID (pIgpMetricEntry) = BGP4_INVALID_IGP_LABEL;
                break;
            default:
                break;

        }

        if (i4Sts == BGP4_FALSE)    /* complete */
        {
            /*  BGP4_RESET_NH_METRIC_LSP_FLAG (pIgpMetricEntry, 
               BGP4_LSP_CHG_INPROGRESS);
             */
            Bgp4InitNetAddressStruct (&(BGP4_LSP_CHG_INIT_ROUTE_NETADDR_INFO),
                                      BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_VPNV4_UNICAST);
            TMO_SLL_Delete (BGP4_LSP_CHG_LIST, (tTMO_SLL_NODE *) pLspNode);
            MemReleaseMemBlock (BGP4_VPN4_LSP_NODE_CHG_MEMPOOL_ID,
                                (UINT1 *) pLspNode);
        }
        u4LspCnt++;
        if (u4LspCnt >= BGP4_MAX_PRCS_LSP_CHG)
        {
            break;
        }
    }
    if (Bgp4IsCpuNeeded () == BGP4_TRUE)
    {
        /* External events requires CPU. Relinquish CPU. */
        return (BGP4_RELINQUISH);
    }

    if (TMO_SLL_Count (BGP4_LSP_CHG_LIST) > 0)
    {
        return (BGP4_TRUE);
    }
    else
    {
        return (BGP4_FALSE);
    }
}

/********************************************************************/
/* Function Name   : Bgp4LspUpHandler                               */
/* Description     : Handles the LSP UP                             */
/* Input(s)        : pIgpMetricEntry - IGP metric entry in which    */
/*                   the LSP related informatin is present          */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : None.                                */
/********************************************************************/
INT4
Bgp4LspUpHandler (tBgp4IgpMetricEntry * pIgpMetricEntry)
{
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile       FindRtProfile;
    tRouteProfile      *pProfileList = NULL;
    VOID               *pRibNode = NULL;
    tAddrPrefix         InvNetAddr;
    tRouteProfile      *pNextRtProfile = NULL;
    tRouteProfile      *pAddRtProfile = NULL;
    tBgp4PeerEntry     *pPeer = NULL;
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
    UINT4               u4RtCnt = 0;
    INT4                i4RetVal;

    Bgp4InitAddrPrefixStruct (&(InvNetAddr),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO
                              (BGP4_LSP_CHG_INIT_ROUTE_PREFIX_INFO));
    if ((PrefixMatch (BGP4_LSP_CHG_INIT_ROUTE_PREFIX_INFO, InvNetAddr)) ==
        BGP4_TRUE)
    {
        pRtProfile = BGP4_DLL_FIRST_ROUTE (BGP4_NH_METRIC_ROUTES
                                           (pIgpMetricEntry));
    }
    else
    {
        /* Get the next route */
        MEMSET (&FindRtProfile, 0, sizeof (tRouteProfile));
        Bgp4CopyNetAddressStruct (&(FindRtProfile.NetAddress),
                                  BGP4_LSP_CHG_INIT_ROUTE_NETADDR_INFO);
        i4RetVal = Bgp4RibhLookupRtEntry (&FindRtProfile,
                                          BGP4_NH_METRIC_VRFID
                                          (pIgpMetricEntry),
                                          BGP4_TREE_FIND_EXACT, &pProfileList,
                                          &pRibNode);
        if (i4RetVal == BGP4_FAILURE)
        {
            Bgp4InitNetAddressStruct (&(BGP4_LSP_CHG_INIT_ROUTE_NETADDR_INFO),
                                      BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_VPNV4_UNICAST);
            return BGP4_FALSE;
        }
        pRtProfile =
            Bgp4DshGetRtForNextHopFromProfileList (pProfileList,
                                                   BGP4_LSP_CHG_INIT_ROUTE_PREFIX_INFO,
                                                   BGP4_NH_METRIC_VRFID
                                                   (pIgpMetricEntry));
    }

    /* Current status of the NEXT-HOP has changed. Need to process
     * all the routes with this NEXT-HOP. */
    while (pRtProfile)
    {
        pPeer = BGP4_RT_PEER_ENTRY (pRtProfile);
        BGP4_RT_REF_COUNT (pRtProfile)++;

        pNextRtProfile = BGP4_RT_NEXTHOP_LINK_NEXT (pRtProfile);
        pAddRtProfile = Bgp4DuplicateRouteProfile (pRtProfile);
        if (pAddRtProfile == NULL)
        {
            continue;
        }
        if (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pAddRtProfile)) != 0)
        {
            TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pAddRtProfile),
                          pRtVrfInfo, tRtInstallVrfInfo *)
            {
                /* Reset the flags of each installed VRF as this route
                 * is going to be processed as a newly received route
                 */
                BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo) = 0;
            }
        }

        Bgp4DshAddRouteToList (BGP4_PEER_RCVD_ROUTES (pPeer),
                               pAddRtProfile, BGP4_PREPEND);
        Bgp4DshReleaseRtInfo (pRtProfile);
        if (pNextRtProfile == NULL)
        {
            break;
        }

        if (u4RtCnt >= BGP4_MAX_NH_MET_RTS2PRCS)
        {
            Bgp4CopyNetAddressStruct (&(BGP4_LSP_CHG_INIT_ROUTE_NETADDR_INFO),
                                      BGP4_RT_NET_ADDRESS_INFO
                                      (pNextRtProfile));
            if (Bgp4IsCpuNeeded () == BGP4_TRUE)
            {
                /* External events requires CPU. Relinquish CPU. */
                return (BGP4_RELINQUISH);
            }
            else
            {
                /* Maximum entries processed. Can return. */
                return (BGP4_TRUE);
            }
        }
        pRtProfile = pNextRtProfile;
        pNextRtProfile = NULL;
        u4RtCnt++;
    }

    Bgp4InitNetAddressStruct (&(BGP4_LSP_CHG_INIT_ROUTE_NETADDR_INFO),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_VPNV4_UNICAST);
    BGP4_NH_METRIC_LSP_STATUS (pIgpMetricEntry) = BGP4_LSP_UP;
    return BGP4_FALSE;
}

/********************************************************************/
/* Function Name   : Bgp4LspDownHandler                             */
/* Description     : Handles the LSP DOWN                           */
/* Input(s)        : pIgpMetricEntry - IGP metric entry in which    */
/*                   the LSP related informatin is present          */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_FALSE/BGP4_TRUE                 */
/********************************************************************/
INT4
Bgp4LspDownHandler (tBgp4IgpMetricEntry * pIgpMetricEntry)
{
    tAddrPrefix         NextHop;
    tAddrPrefix         ImmediateNextHop;
    INT4                i4LkupStatus;
    INT4                i4Metric = BGP4_INVALID_NH_METRIC;
    UINT4               u4IfIndex = BGP4_INVALID_IFINDX;
    UINT2               u2PrefixLen = 0;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pNextRtProfile = NULL;
    tRouteProfile       FindRtProfile;
    VOID               *pRibNode = NULL;
    tAddrPrefix         InvNetAddr;
    UINT4               u4RtCnt = 0;
    INT4                i4RetSts;
    UINT4               u4LspId = 0;
    UINT1               u1LspStatus;
    UINT4               u4VrfId;
    tTMO_SLL           *pVrfList = NULL;
    tRtInstallVrfInfo  *pInstVrf = NULL;

    u4VrfId = BGP4_NH_METRIC_VRFID (pIgpMetricEntry);

    i4RetSts = Bgp4GetAlternateLspFromMplsForNextHop (pIgpMetricEntry,
                                                      &u4LspId, &u1LspStatus);
    BGP4_TRC_ARG4 (NULL,
                   BGP4_TRC_FLAG, BGP4_DETAIL_TRC, BGP4_MOD_NAME,
                   "\tFunc[Bgp4LspDownHandler] Line[%d] pIgpMetricEntry->u4IgpLspId=%d u4LspId=%d i4RetSts=%d \r\n\r",
                   __LINE__, pIgpMetricEntry->u4IgpLspId, u4LspId, i4RetSts);

    if (i4RetSts == BGP4_FAILURE)
    {
        BGP4_TRC_ARG2 (NULL,
                       BGP4_TRC_FLAG, BGP4_DETAIL_TRC, BGP4_MOD_NAME,
                       "\tCalling Bgp4ProcessLspDown Func[%s] Line[%d] \r\n\r",
                       __func__, __LINE__);
        i4RetSts = Bgp4ProcessLspDown (pIgpMetricEntry);
        if ((PrefixMatch (BGP4_LSP_CHG_INIT_ROUTE_PREFIX_INFO, InvNetAddr))
            == BGP4_TRUE)
        {
            BGP4_NH_METRIC_LSP_STATUS (pIgpMetricEntry) = BGP4_LSP_DOWN;
            BGP4_NH_METRIC_LSPID (pIgpMetricEntry) = BGP4_INVALID_IGP_LABEL;
        }

        return (i4RetSts);
    }
    /* Look up in FIB to check whether the next hop is resolvable or not. */

    Bgp4CopyAddrPrefixStruct (&(NextHop),
                              BGP4_NH_METRIC_NEXTHOP_INFO (pIgpMetricEntry));

    Bgp4InitAddrPrefixStruct (&(ImmediateNextHop),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO (NextHop));

    u2PrefixLen = Bgp4GetMaxPrefixLength (NextHop);

    /*  BGP4_SET_NH_METRIC_LSP_FLAG (pIgpMetricEntry, BGP4_LSP_CHG_INPROGRESS);
     */

    BGP4_NH_METRIC_LSP_STATUS (pIgpMetricEntry) = BGP4_LSP_UP;
    BGP4_NH_METRIC_LSPID (pIgpMetricEntry) = u4LspId;
    Bgp4InitAddrPrefixStruct (&(InvNetAddr),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO
                              (BGP4_LSP_CHG_INIT_ROUTE_PREFIX_INFO));

    if ((PrefixMatch (BGP4_LSP_CHG_INIT_ROUTE_PREFIX_INFO, InvNetAddr))
        == BGP4_TRUE)
    {
        pRtProfile = BGP4_DLL_FIRST_ROUTE (BGP4_NH_METRIC_ROUTES
                                           (pIgpMetricEntry));
    }
    else
    {
        /* Get the next route */
        MEMSET (&FindRtProfile, 0, sizeof (tRouteProfile));
        Bgp4CopyNetAddressStruct (&(FindRtProfile.NetAddress),
                                  BGP4_LSP_CHG_INIT_ROUTE_NETADDR_INFO);
        i4RetSts = Bgp4RibhLookupRtEntry (&FindRtProfile,
                                          BGP4_NH_METRIC_VRFID
                                          (pIgpMetricEntry),
                                          BGP4_TREE_FIND_EXACT, &pRtProfile,
                                          &pRibNode);
        if (i4RetSts == BGP4_FAILURE)
        {
            Bgp4InitNetAddressStruct (&(BGP4_LSP_CHG_INIT_ROUTE_NETADDR_INFO),
                                      BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_VPNV4_UNICAST);
            return (BGP4_FALSE);
        }
    }

    i4LkupStatus = BgpIpRtLookup (u4VrfId, &NextHop, u2PrefixLen,
                                  &ImmediateNextHop, &i4Metric, &u4IfIndex,
                                  BGP4_ALL_APPIDS);

    if (i4LkupStatus == BGP4_FAILURE)

    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tRoute Look UP FAILURE. NextHop %s NOT RESOLVABLE.\n",
                       Bgp4PrintIpAddr (NextHop.au1Address, NextHop.u2Afi));
    }

    /*The below logic is changed to ensure alternative LSP if present is handled properly.
     * and we need to scan BGP4_VPN4_RT_INSTALL_VRF_LIST and then notify L3VPN*/
    while (pRtProfile)
    {
        if (pRtProfile->pRtInfo != NULL)
        {
            BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tLsp Down process is handled for the route %s  with nexthop %s "
                           "immediate nexthop %s u4LspId %d\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP
                                            (BGP4_RT_BGP_INFO (pRtProfile)),
                                            BGP4_INFO_AFI (BGP4_RT_BGP_INFO
                                                           (pRtProfile))),
                           Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP
                                            (pRtProfile),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                             (pRtProfile))), u4LspId);

        }
        pNextRtProfile = BGP4_RT_NEXTHOP_LINK_NEXT (pRtProfile);
        /*If this decreases the scalability, has to eb throttled */
        pVrfList = BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile);

        TMO_SLL_Scan (pVrfList, pInstVrf, tRtInstallVrfInfo *)
        {
            u4VrfId = BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pInstVrf);
            BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);
            /*pRtProfile->u1LspOper =  BGP4_LSP_DOWN; */
            BGP4_TRC_ARG2 (NULL,
                           BGP4_TRC_FLAG, BGP4_DETAIL_TRC, BGP4_MOD_NAME,
                           "\tCalling Bgp4UpdateRouteInMplsDb by FUNC[%s] LINE - [%d] \n",
                           __func__, __LINE__);
            Bgp4UpdateRouteInMplsDb (pRtProfile, u4VrfId, L3VPN_BGP4_ROUTE_DEL,
                                     L3VPN_BGP4_LABEL_PUSH);
            BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);
            BGP4_RT_IGP_LSP_ID (pRtProfile) = u4LspId;
            /* Update to IP */
            BGP4RTDeleteRouteInCommIp4RtTbl (pRtProfile, u4VrfId);

            /* Changed route index and nexthop info to the route profile */
            pRtProfile->u4RtIfIndx = u4IfIndex;
            Bgp4CopyAddrPrefixStruct (&(pRtProfile->ImmediateNextHopInfo),
                                      ImmediateNextHop);

            BGP4_TRC_ARG2 (NULL,
                           BGP4_TRC_FLAG, BGP4_DETAIL_TRC, BGP4_MOD_NAME,
                           "\tCalling Bgp4UpdateRouteInMplsDb by FUNC[%s] LINE - [%d] \n",
                           __func__, __LINE__);
            Bgp4UpdateRouteInMplsDb (pRtProfile, u4VrfId,
                                     L3VPN_BGP4_ROUTE_ADD,
                                     L3VPN_BGP4_LABEL_PUSH);
            /* Update to IP */
            BGP4RTAddRouteToCommIp4RtTbl (pRtProfile, u4VrfId);
        }
        if (pNextRtProfile == NULL)
        {
            break;
        }

        if (u4RtCnt >= BGP4_MAX_NH_MET_RTS2PRCS)
        {
            Bgp4CopyNetAddressStruct (&(BGP4_LSP_CHG_INIT_ROUTE_NETADDR_INFO),
                                      BGP4_RT_NET_ADDRESS_INFO
                                      (pNextRtProfile));
            if (Bgp4IsCpuNeeded () == BGP4_TRUE)
            {
                /* External events requires CPU. Relinquish CPU. */
                return (BGP4_RELINQUISH);
            }
            else
            {
                /* Maximum entries processed. Can return. */
                return (BGP4_TRUE);
            }
        }
        pRtProfile = pNextRtProfile;
        pNextRtProfile = NULL;
        u4RtCnt++;
    }

    Bgp4InitNetAddressStruct (&(BGP4_LSP_CHG_INIT_ROUTE_NETADDR_INFO),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_VPNV4_UNICAST);

    BGP4_NH_METRIC_LSP_STATUS (pIgpMetricEntry) = BGP4_LSP_UP;
    BGP4_NH_METRIC_LSPID (pIgpMetricEntry) = u4LspId;
    return (BGP4_FALSE);
}

/********************************************************************/
/* Function Name   : Bgp4ProcessLspDown                             */
/* Description     : Deletes all the routes with the nexthop address*/
/*                   as the LSP address which became down and       */
/*                   process them as withdrawn routes               */
/* Input(s)        : pIgpMetricEntry - IGP metric entry in which    */
/*                   the LSP related informatin is present          */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_FALSE/BGP4_TRUE                 */
/********************************************************************/
INT4
Bgp4ProcessLspDown (tBgp4IgpMetricEntry * pIgpMetricEntry)
{
    tAddrPrefix         InvNetAddr;
    tRouteProfile       FindRtProfile;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pNextRtProfile = NULL;
    tRouteProfile      *pAddRtProfile = NULL;
    tBgp4PeerEntry     *pPeer = NULL;
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4RetVal;
    UINT4               u4RtCnt = 0;

    Bgp4InitAddrPrefixStruct (&(InvNetAddr),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO
                              (BGP4_LSP_CHG_INIT_ROUTE_PREFIX_INFO));
    if ((PrefixMatch (BGP4_LSP_CHG_INIT_ROUTE_PREFIX_INFO, InvNetAddr)) ==
        BGP4_TRUE)
    {
        pRtProfile =
            BGP4_DLL_FIRST_ROUTE (BGP4_NH_METRIC_ROUTES (pIgpMetricEntry));
    }
    else
    {
        /* Get the next route */
        MEMSET (&FindRtProfile, 0, sizeof (tRouteProfile));
        Bgp4CopyNetAddressStruct (&(FindRtProfile.NetAddress),
                                  BGP4_LSP_CHG_INIT_ROUTE_NETADDR_INFO);
        i4RetVal = Bgp4RibhLookupRtEntry (&FindRtProfile,
                                          BGP4_NH_METRIC_VRFID
                                          (pIgpMetricEntry),
                                          BGP4_TREE_FIND_EXACT, &pRtProfile,
                                          &pRibNode);
        if (i4RetVal == BGP4_FAILURE)
        {
            Bgp4InitNetAddressStruct (&(BGP4_LSP_CHG_INIT_ROUTE_NETADDR_INFO),
                                      BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_VPNV4_UNICAST);
            return (BGP4_FALSE);
        }
    }

    /* Current status of the NEXT-HOP has changed. Need to process
     * all the routes with this NEXT-HOP. */
    while (pRtProfile)
    {
        pPeer = BGP4_RT_PEER_ENTRY (pRtProfile);
        BGP4_RT_REF_COUNT (pRtProfile)++;
        pNextRtProfile = BGP4_RT_NEXTHOP_LINK_NEXT (pRtProfile);
        pAddRtProfile = Bgp4DuplicateRouteProfile (pRtProfile);
        if (pAddRtProfile == NULL)
        {
            /* Allocation FAILS. We are not able to handle the
             * status change for this route.
             */
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Allocate Route Profile for "
                           "NEXT-HOP Status Change FAILS!!!\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeer))));
            Bgp4DshReleaseRtInfo (pRtProfile);
            pRtProfile = pNextRtProfile;
            pNextRtProfile = NULL;
            continue;
        }
        if (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pAddRtProfile)) != 0)
        {
            TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pAddRtProfile),
                          pRtVrfInfo, tRtInstallVrfInfo *)
            {
                /* Reset the flags of each installed VRF as this route
                 * is going to be processed as a newly received route
                 */
                BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo) = 0;
            }
        }
        /* Add the route to the PEER's RCVD_ROUTE List */
        Bgp4DshDelinkRouteFromChgList (pAddRtProfile);
        BGP4_RT_RESET_FLAG (pAddRtProfile, BGP4_RT_BEST);
        Bgp4DshAddRouteToList (BGP4_PEER_RCVD_ROUTES (pPeer),
                               pAddRtProfile, BGP4_PREPEND);
        Bgp4DshReleaseRtInfo (pRtProfile);
        if (pNextRtProfile == NULL)
        {
            break;
        }

        if (u4RtCnt >= BGP4_MAX_NH_MET_RTS2PRCS)
        {
            Bgp4CopyNetAddressStruct (&(BGP4_LSP_CHG_INIT_ROUTE_NETADDR_INFO),
                                      BGP4_RT_NET_ADDRESS_INFO
                                      (pNextRtProfile));
            if (Bgp4IsCpuNeeded () == BGP4_TRUE)
            {
                /* External events requires CPU. Relinquish CPU. */
                return (BGP4_RELINQUISH);
            }
            else
            {
                /* Maximum entries processed. Can return. */
                return (BGP4_TRUE);
            }
        }
        pRtProfile = pNextRtProfile;
        pNextRtProfile = NULL;
        u4RtCnt++;
    }

    Bgp4InitNetAddressStruct (&(BGP4_LSP_CHG_INIT_ROUTE_NETADDR_INFO),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_VPNV4_UNICAST);
    return (BGP4_FALSE);
}

/********************************************************************/
/* Function Name   : Bgp4IgpMetricTblGetEntryForLspId               */
/* Description     : Gets the IGP metric entry given an LSP ID      */
/* Input(s)        : u4LspId - LSP identifier                       */
/* Output(s)       : ppIgpMetricEntry - address of IGP metric entry */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4IgpMetricTblGetEntryForLspId (UINT4 u4LspId,
                                  tBgp4IgpMetricEntry ** ppIgpMetricEntry)
{
    UINT4               u4HashKey = 0x0;
    tBgp4IgpMetricEntry *pTmpIgpMetricEntry = NULL;

    TMO_HASH_Scan_Table (BGP4_NEXTHOP_METRIC_TBL (BGP4_DFLT_VRFID), u4HashKey)
    {
        TMO_HASH_Scan_Bucket (BGP4_NEXTHOP_METRIC_TBL (BGP4_DFLT_VRFID),
                              u4HashKey, pTmpIgpMetricEntry,
                              tBgp4IgpMetricEntry *)
        {
            if (BGP4_NH_METRIC_LSPID (pTmpIgpMetricEntry) == u4LspId)
            {
                *ppIgpMetricEntry = pTmpIgpMetricEntry;
                return BGP4_SUCCESS;
            }
        }
    }

    return BGP4_FAILURE;
}

/********************************************************************/
/* Function Name   : Bgp4VrfChangeHandler                           */
/* Description     : Handles the VRF notifications from IP-VPN      */
/* Input(s)        : pVrfName - vrf name                            */
/*                   u1VrfState - new state of the VRF              */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4VrfChangeHandler (UINT4 u4VrfId, UINT1 u1VrfState)
{
    INT4                i4RetVal = BGP4_SUCCESS;

    if (gBgpCxtNode[u4VrfId] == NULL)
    {
        return BGP4_SUCCESS;
    }

    switch (u1VrfState)
    {
        case BGP4_L3VPN_VRF_CREATE:
            /* If the vrf is not there,then create a new one.
             * if it already exists, that means it must have been issued for 
             * delete and deletion is not yet over. In that case, just set
             * the flag to indicate that this must be created again
             * e.g. <down, delete, create ...>
             */
            if (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) ==
                BGP4_L3VPN_VRF_DELETE)
            {
                i4RetVal = Bgp4Vpnv4VrfCreate (u4VrfId);
            }
            BGP4_VPN4_VRF_SPEC_SET_FLAG (u4VrfId, BGP4_VRF_CREATE_PENDING);
            break;
        case BGP4_L3VPN_VRF_UP:
            if (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) ==
                BGP4_L3VPN_VRF_DELETE)
            {
                i4RetVal = Bgp4Vpnv4VrfCreate (u4VrfId);
                if (i4RetVal == BGP4_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                              "\tVPN:Bgp4Vpnv4VrfCreate Failed \n");
                }
            }
            i4RetVal = Bgp4Vpnv4VrfUpHdlr (u4VrfId, u1VrfState);
            break;
        case BGP4_L3VPN_VRF_DOWN:
            /* If the vrf is not there,then create a new one.
             * if it already exists, then make it down
             * e.g. <create, up, down ...>
             */
            if (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) !=
                BGP4_L3VPN_VRF_DELETE)
            {
                i4RetVal = Bgp4Vpnv4VrfDownHdlr (u4VrfId, u1VrfState);
            }
            else
            {
                i4RetVal = Bgp4Vpnv4VrfCreate (u4VrfId);
                if (i4RetVal == BGP4_FAILURE)
                {
                    return BGP4_FAILURE;
                }
                /* make the current state down */
                BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) =
                    BGP4_L3VPN_VRF_DOWN;
            }
            break;
        case BGP4_L3VPN_VRF_DELETE:
            /* If the vrf exists, then delete it from BGP data base
             * e.g. <create, up, delete ...>
             */
            if (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) !=
                BGP4_L3VPN_VRF_DELETE)
            {
                Bgp4Vpnv4VrfDownHdlr (u4VrfId, u1VrfState);
            }
            break;
        default:
            /* for any other event received from IP-VPN return failure */
            return BGP4_FAILURE;
    }
    return i4RetVal;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4VrfUpHdlr                             */
/* Description     : Makes VRF up                                   */
/* Input(s)        : u4VrfId - vrf id                               */
/*                   u1VrfState - the state of VRF                  */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4Vpnv4VrfUpHdlr (UINT4 u4VrfId, UINT1 u1VrfState)
{

    UNUSED_PARAM (u1VrfState);
    /* case 1. When the vrf is undergoing deinit, set init-pending
     * case 2. When the vrf is either in create/down, set init-in-progress
     * case 3. in any other state, just return
     */
    if ((BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) == BGP4_L3VPN_VRF_CREATE)
        || (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) == BGP4_L3VPN_VRF_DOWN))
    {
        /* case 2. */
        BGP4_VPN4_VRF_SPEC_SET_FLAG (u4VrfId, BGP4_VRF_INIT_INPROGRESS);
        if (Bgp4Vpnv4VrfInit (u4VrfId) == BGP4_FAILURE)
        {
            return BGP4_FAILURE;
        }
    }
    else if (BGP4_VPN4_VRF_SPEC_GET_FLAG (u4VrfId) & BGP4_VRF_DOWN_INPROGRESS)
    {
        /* case 1. */
        /* e.g <up, down, delete, create, up ...> */
        BGP4_VPN4_VRF_SPEC_RESET_FLAG (u4VrfId,
                                       (UINT4) BGP4_VRF_CREATE_PENDING);
        BGP4_VPN4_VRF_SPEC_SET_FLAG (u4VrfId, BGP4_VRF_INIT_PENDING);
    }

    /* case 3. */
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4VrfDownHdlr                           */
/* Description     : Makes VRF down                                 */
/* Input(s)        : u4VrfId - Vrf Id                               */
/*                   u1VrfState - the state of VRF                  */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4Vpnv4VrfDownHdlr (UINT4 u4VrfId, UINT1 u1VrfState)
{

    UNUSED_PARAM (u1VrfState);
    /* reset create pending. e. g <up, down, delete, create, down ...> */
    BGP4_VPN4_VRF_SPEC_RESET_FLAG (u4VrfId, (UINT4) BGP4_VRF_CREATE_PENDING);

    /* case 1. When the vrf is undergoing init or is created or is up, 
     *         set oper-down
     * case 2. When the vrf init is pending, reset init-pending
     * case 2. When the vrf is issued to be deleted, then call vrfdeinit
     * case 3. in any other state, just return
     */
    if ((BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) == BGP4_L3VPN_VRF_CREATE)
        || (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) == BGP4_L3VPN_VRF_UP)
        || (BGP4_VPN4_VRF_SPEC_GET_FLAG (u4VrfId) & BGP4_VRF_INIT_INPROGRESS))
    {
        /* case 1. */
        BGP4_VPN4_VRF_SPEC_RESET_FLAG (u4VrfId,
                                       (UINT4) BGP4_VRF_INIT_INPROGRESS);
        BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) = BGP4_L3VPN_VRF_DOWN;
        Bgp4Vpnv4VrfDeInit (u4VrfId, u1VrfState);
    }
    else if (BGP4_VPN4_VRF_SPEC_GET_FLAG (u4VrfId) & BGP4_VRF_INIT_PENDING)
    {
        /* case 2. */
        BGP4_VPN4_VRF_SPEC_RESET_FLAG (u4VrfId, (UINT4) BGP4_VRF_INIT_PENDING);
    }
    else if (u1VrfState == BGP4_L3VPN_VRF_DELETE)
    {
        /* case 3. */
        Bgp4Vpnv4VrfDeInit (u4VrfId, u1VrfState);
    }
    /* case 4. */
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4VrfInit                               */
/* Description     : Initializes a particular vrf                   */
/* Input(s)        : u4VrfId - Vrf Id                               */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS                         */
/********************************************************************/
INT4
Bgp4Vpnv4VrfInit (UINT4 u4VrfId)
{
    tAddrPrefix         RemAddr;
    UINT1               au1InvRD[BGP4_VPN_RT_DISTING_SIZE];

    if (!(BGP4_VPN4_VRF_SPEC_GET_FLAG (u4VrfId) & BGP4_VRF_INIT_INPROGRESS))
    {
        return BGP4_SUCCESS;
    }

    MEMSET (au1InvRD, 0, BGP4_VPN_RT_DISTING_SIZE);
    if (MEMCMP (BGP4_VPN4_VRF_SPEC_ROUTE_DISTING (u4VrfId), au1InvRD,
                BGP4_VPN_RT_DISTING_SIZE) == 0)
    {
        /* RD is not yet configured for the VR. Do not make it up */
        return BGP4_FAILURE;
    }
    /* VRF initialization activities:
     * # Enable all the CE neighbors attached to this VRF.
     * If the VRF has been already configured with import route targets,
     * then send Route-Refresh message to PE peers
     */

    if (BGP4_VPNV4_AFI_FLAG == BGP4_TRUE)
    {
        Bgp4Vpnv4SendVpnv4RtsToPeer (u4VrfId);
    }

    if (TMO_SLL_Count (BGP4_VPN4_VRF_SPEC_IMPORT_TARGETS (u4VrfId)) > 0)
    {
        Bgp4InitAddrPrefixStruct (&(RemAddr), BGP4_INET_AFI_IPV4);
        Bgp4RtRefRequestHandler (0, RemAddr, BGP4_INET_AFI_IPV4,
                                 BGP4_INET_SAFI_VPNV4_UNICAST,
                                 BGP4_RR_ADVT_PE_PEERS);
    }

    if (TMO_SLL_Count (BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS (u4VrfId)) > 0)
    {
        Bgp4InitAddrPrefixStruct (&(RemAddr), BGP4_INET_AFI_IPV4);
        Bgp4RtRefRequestHandler (u4VrfId, RemAddr, BGP4_INET_AFI_IPV4,
                                 BGP4_INET_SAFI_UNICAST, BGP4_RR_ADVT_DEFAULT);
    }

    /* initialization activities are over, reset the flag init-progress
     * and init-pending if any
     */
    BGP4_VPN4_VRF_SPEC_RESET_FLAG (u4VrfId, (UINT4) (BGP4_VRF_INIT_INPROGRESS |
                                                     BGP4_VRF_INIT_PENDING));
    BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) = BGP4_L3VPN_VRF_UP;

    /* If leaking enabled, When VRF comes up ,Get the leaked routes from peer */
    if (BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED)
    {
        Bgp4Vpnv4GetLeakRoutesFromPeer (u4VrfId);
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4VrfDeInit                             */
/* Description     : DeInitializes a particular vrf                 */
/* Input(s)        : u4VrfId - Vrf Id                               */
/*                   u1VrfState - state of the VRF                  */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS                         */
/********************************************************************/
INT4
Bgp4Vpnv4VrfDeInit (UINT4 u4VrfId, UINT1 u1VrfState)
{
    tVrfNode           *pVrfNode = NULL;
    /* FIX:ST_52: */
    if ((u1VrfState == BGP4_L3VPN_VRF_DOWN) &&
        (BGP4_VPN4_VRF_SPEC_GET_FLAG (u4VrfId) & BGP4_VRF_DOWN_INPROGRESS))
    {
        return BGP4_SUCCESS;
    }
    BGP4_VPN4_VRF_SPEC_RESET_FLAG (u4VrfId, (UINT4) BGP4_VRF_NO_EXP_TARGETS);
    BGP4_VPN4_VRF_SPEC_SET_FLAG (u4VrfId, BGP4_VRF_DOWN_INPROGRESS);
    pVrfNode = (tVrfNode *) MemAllocMemBlk (BGP4_VPN4_VRF_NODE_CHG_MEMPOOL_ID);

    if (pVrfNode == NULL)
    {
        return BGP4_FAILURE;
    }

    pVrfNode->u4VrfId = u4VrfId;
    TMO_SLL_Add (BGP4_VRF_STS_CHNG_LIST, (tTMO_SLL_NODE *) pVrfNode);
    return (BGP4_SUCCESS);
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4DelRtFromVrfs                         */
/* Description     : A given route will be deleted from all the VRFs*/
/*                   that is installed in                           */
/* Input(s)        : pRtProfile - route profile pointer             */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCESS/BGP4_FAILURE             */
/********************************************************************/
INT4
Bgp4Vpnv4DelRtFromVrfs (tRouteProfile * pRtProfile)
{
    tTMO_SLL           *pVrfList = NULL;
    tRtInstallVrfInfo  *pInstVrf = NULL;
    tRouteProfile      *pNextRtProfile = NULL;
    VOID               *pRibNode = NULL;
    UINT4               u4VrfId;

    pVrfList = BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile);

    if (pRtProfile != NULL)
    {
        if (pRtProfile->pRtInfo != NULL)
        {
            BGP4_TRC_ARG5 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\t Received VPN route %s for deleting from VRF with nexthop %s "
                           "immediate nexthop %s flags 0x%x and extended flags 0x%x\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP
                                            (BGP4_RT_BGP_INFO (pRtProfile)),
                                            BGP4_INFO_AFI (BGP4_RT_BGP_INFO
                                                           (pRtProfile))),
                           Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP
                                            (pRtProfile),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                             (pRtProfile))),
                           pRtProfile->u4Flags, pRtProfile->u4ExtFlags);

        }
    }
    TMO_SLL_Scan (pVrfList, pInstVrf, tRtInstallVrfInfo *)
    {
        u4VrfId = BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pInstVrf);

        if (gBgpCxtNode[u4VrfId] == NULL)
        {
            continue;
        }

        if ((BGP4_RR_CLIENT_CNT (u4VrfId) > 0) && (pVrfList == NULL))
        {
            /* If the speaker is route-reflector and the route is not
             * * installed in any VRF, then simply return
             * */
            return (BGP4_SUCCESS);
        }
        /* delete the route from the BGP-VRF-RIB */
        Bgp4RibhDelRtEntry (pRtProfile, &pRibNode, u4VrfId, BGP4_TRUE);
        for (;;)
        {
            BGP4_RT_GET_NEXT (pRtProfile, u4VrfId, pNextRtProfile);

            if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_IN_FIB_UPD_LIST)
                == BGP4_RT_IN_FIB_UPD_LIST)
            {
                /* Route not yet added to the FIB. */
                if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_WITHDRAWN) !=
                    BGP4_RT_WITHDRAWN)
                {
                    Bgp4DshDelinkRouteFromList (BGP4_FIB_UPD_LIST (u4VrfId),
                                                pRtProfile,
                                                BGP4_FIB_UPD_LIST_INDEX);
                    BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_IN_FIB_UPD_LIST);
                }
            }
            if (((BGP4_RT_PEER_ENTRY (pRtProfile) != NULL) &&
                 (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRtProfile))
                  == BGP4_VPN4_CE_PEER))
                || (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID))
            {
                BGP4_TRC_ARG2 (NULL,
                               BGP4_TRC_FLAG, BGP4_DETAIL_TRC, BGP4_MOD_NAME,
                               "\tCalling Bgp4UpdateRouteInMplsDb by FUNC[%s] LINE - [%d] \n",
                               __func__, __LINE__);
                Bgp4UpdateRouteInMplsDb (pRtProfile, u4VrfId,
                                         L3VPN_BGP4_ROUTE_DEL,
                                         L3VPN_BGP4_LABEL_POP);

                if (BGP4_RT_IS_LEAK_ROUTE (pRtProfile) == BGP4_TRUE)
                {
                    BGP4_RT_SET_FLAG (pRtProfile, (BGP4_RT_WITHDRAWN |
                                                   BGP4_RT_ADVT_NOTTO_EXTERNAL));
                    Bgp4IgphUpdateRouteInFIB (pRtProfile);
                }

            }
            if (pNextRtProfile == NULL)
            {
                break;
            }
            pRtProfile = pNextRtProfile;
            /* Delete route from BGP-VRF-RIB */
            Bgp4RibhDelRtEntry (pRtProfile, pRibNode, u4VrfId, BGP4_TRUE);
            Bgp4Vpnv4SetVrfFlags (pRtProfile, u4VrfId, BGP4_RT_VRF_WITHDRAWN);

            /* do not delete the installed VRF spec info of this vrf in the
             * route profile. all the installed VRF infos inthe route profile
             * will anyway be deleted, when route is released.
             */
        }
    }

    return (BGP4_SUCCESS);
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4CEPeerInit                            */
/* Description     : Initializes CE peer                            */
/* Input(s)        : pPeerInfo - peer pointer                       */
/*                   u4MaxRtCnt - maximum routes                    */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_INIT_COMPLETE      */
/********************************************************************/
INT4
Bgp4Vpnv4CEPeerInit (tBgp4PeerEntry * pPeerInfo, UINT4 u4MaxRtCnt)
{
    tTMO_SLL            PeerFeasRtList;
    tNetAddress         InvNetAddr;
    tRouteProfile       RouteProfile;
    tTMO_SLL            DummyWithRtList;
    VOID               *pRibNode = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pNextRt = NULL;
    tLinkNode          *pRtLink = NULL;
    tLinkNode          *pTempLinkNode = NULL;
    UINT4               u4RtCnt = 0;
    INT4                i4InitStatus = BGP4_FAILURE;;

    /* The peer is CE peer, so advertise the best routes
     * in the VRF associated with this CE
     */
    /* TODO verify if the rib node is correct */
    pRibNode = BGP4_VPN4_RIB_TREE (BGP4_PEER_CXT_ID (pPeerInfo));

    if (pRibNode == NULL)
    {
        return BGP4_SUCCESS;
    }

    Bgp4InitNetAddressStruct (&(InvNetAddr),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO
                              (BGP4_PEER_INIT_PREFIX_INFO (pPeerInfo)),
                              (UINT1)
                              BGP4_SAFI_IN_NET_ADDRESS_INFO
                              (BGP4_PEER_INIT_NETADDR_INFO (pPeerInfo)));
    if ((NetAddrMatch (BGP4_PEER_INIT_NETADDR_INFO (pPeerInfo), InvNetAddr)) ==
        BGP4_TRUE)
    {
        /* Get the first route */
        if (Bgp4RibhGetFirstEntry (CAP_MP_VPN4_UNICAST, &pRtProfile,
                                   &pRibNode, TRUE) == BGP4_FAILURE)
        {
            /* Update the peer init afi and safi properly */
            Bgp4InitNetAddressStruct (&
                                      (BGP4_PEER_INIT_NETADDR_INFO (pPeerInfo)),
                                      BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);
            return (BGP4_INIT_COMPLETE);
        }
    }
    else
    {
        /* Get the next route */
        Bgp4CopyNetAddressStruct (&(RouteProfile.NetAddress),
                                  BGP4_PEER_INIT_NETADDR_INFO (pPeerInfo));
        if (Bgp4RibhLookupRtEntry (&RouteProfile, BGP4_PEER_CXT_ID (pPeerInfo),
                                   BGP4_TREE_FIND_EXACT, &pRtProfile,
                                   &pRibNode) == BGP4_FAILURE)
        {
            /* Update the peer init afi and safi properly */
            Bgp4InitNetAddressStruct (&
                                      (BGP4_PEER_INIT_NETADDR_INFO (pPeerInfo)),
                                      BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);
            return (BGP4_INIT_COMPLETE);
        }
    }

    TMO_SLL_Init (&PeerFeasRtList);
    TMO_SLL_Init (&DummyWithRtList);

    for (;;)
    {
        /* Check for the Route can be advertised. */
        if (Bgp4PeerCanInitRouteBeAdvt (pPeerInfo, pRtProfile) == BGP4_TRUE)
        {
            /* Route can be advertised. */
            Bgp4DshAddRouteToList (&PeerFeasRtList, pRtProfile, 0);
        }

        if (u4RtCnt == 0)
        {
            pRibNode = NULL;
        }
        /* Increment the processed route count. */
        u4RtCnt++;

        if (Bgp4RibhGetNextEntry
            (pRtProfile, BGP4_PEER_CXT_ID (pPeerInfo),
             &pNextRt, &pRibNode, TRUE) == BGP4_FAILURE)
        {
            /* Update the peer init afi and safi properly */
            Bgp4InitNetAddressStruct (&
                                      (BGP4_PEER_INIT_NETADDR_INFO (pPeerInfo)),
                                      BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);
            /* No more routes present for processing. */
            i4InitStatus = BGP4_INIT_COMPLETE;
            break;
        }

        if (u4RtCnt > u4MaxRtCnt)
        {
            Bgp4CopyNetAddressStruct (&
                                      (BGP4_PEER_INIT_NETADDR_INFO (pPeerInfo)),
                                      BGP4_RT_NET_ADDRESS_INFO (pNextRt));
            break;
        }
        pRtProfile = pNextRt;
        pNextRt = NULL;
    }
    /* Add route list to peer Tx Q */
    Bgp4PeerSendRoutesToPeerTxQ (pPeerInfo, &PeerFeasRtList, &DummyWithRtList,
                                 BGP4_FALSE);

    /*  The flags NOTTO_EXTERNAL and NOTTO_INTERNAL needs to be reset now. */
    BGP_SLL_DYN_Scan (&PeerFeasRtList, pRtLink, pTempLinkNode, tLinkNode *)
    {
        BGP4_RT_RESET_FLAG (pRtLink->pRouteProfile,
                            (BGP4_RT_ADVT_NOTTO_INTERNAL |
                             BGP4_RT_ADVT_NOTTO_EXTERNAL));
        Bgp4DshRemoveNodeFromList (&PeerFeasRtList, pRtLink);
    }

    Bgp4DshReleaseList (&DummyWithRtList, 0);
    if (i4InitStatus == BGP4_INIT_COMPLETE)
    {
        return i4InitStatus;
    }
    else
    {
        return BGP4_SUCCESS;
    }
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4ExportTargetChgHandler                */
/* Description     : Applies new export target change to the  routes*/
/*                   present in vrf and advertises the best routes  */
/*                   to PE peers                                    */
/* Input(s)        : u4VrfId - VRF identifier                       */
/* Output(s)       : None.                                          */
/* Global Variables Referred : BGP4_VRF_STS_CHNG_LIST               */
/* Global variables Modified : BGP4_VRF_STS_CHNG_LIST               */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4Vpnv4ExportTargetChgHandler (UINT4 u4VrfId)
{
    tVrfNode           *pVrfNode = NULL;
    UINT1               u1MatchFound = BGP4_FALSE;

    TMO_SLL_Scan (BGP4_VRF_STS_CHNG_LIST, pVrfNode, tVrfNode *)
    {
        if (pVrfNode->u4VrfId == u4VrfId)
        {
            u1MatchFound = BGP4_TRUE;
            break;
        }
    }
    if (u1MatchFound == BGP4_TRUE)
    {
        /* this vrf-id is already there in the list. set the flag to indicate
         * the request is reissued only VRF is operationally up.
         * otherwise just keep it as a pending activity which will be taken
         * care when the VRF becomes up again
         */
        if (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) == BGP4_L3VPN_VRF_UP)
        {
            pVrfNode->u1Oper = BGP4_EXPTGT_EVENT;
            BGP4_VPN4_VRF_SPEC_SET_FLAG (u4VrfId, BGP4_VRF_EXP_TGT_CHG_REISSUE);
        }
        return BGP4_SUCCESS;
    }

    /* match not found case */
    if (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) == BGP4_L3VPN_VRF_UP)
    {
        BGP4_VPN4_VRF_SPEC_SET_FLAG (u4VrfId, BGP4_VRF_EXP_TGT_CHG_INPROGRESS);
        pVrfNode =
            (tVrfNode *) MemAllocMemBlk (BGP4_VPN4_VRF_NODE_CHG_MEMPOOL_ID);

        if (pVrfNode == NULL)
        {
            return BGP4_FAILURE;
        }

        pVrfNode->u1Oper = BGP4_EXPTGT_EVENT;
        pVrfNode->u4VrfId = u4VrfId;
        TMO_SLL_Add (BGP4_VRF_STS_CHNG_LIST, (tTMO_SLL_NODE *) pVrfNode);
    }
    return (BGP4_SUCCESS);
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4ProcessVrfChngList                    */
/* Description     : Processes the VRF change list for any notifica-*/
/*                   tions and export targets change                */
/* Input(s)        : None.                                          */
/* Output(s)       : None.                                          */
/* Global Variables Referred : BGP4_VRF_STS_CHNG_LIST               */
/* Global variables Modified : BGP4_VRF_STS_CHNG_LIST               */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_FALSE, if the list becomes empty*/
/*                             BGP4_TRUE, some VRFs are pending     */
/********************************************************************/
INT4
Bgp4Vpnv4ProcessVrfChngList (VOID)
{
    tVrfNode           *pVrfNode = NULL;
    tVrfNode           *pTmpVrfNode = NULL;
    UINT4               u4VrfsCnt = 0;
    UINT4               u4VrfId = 0;
    INT4                i4Sts = BGP4_FALSE;

    if (TMO_SLL_Count (BGP4_VRF_STS_CHNG_LIST) == 0)
    {
        /* if there are no VRFs to be processed, then return false */
        return BGP4_FALSE;
    }

    BGP_SLL_DYN_Scan (BGP4_VRF_STS_CHNG_LIST, pVrfNode, pTmpVrfNode, tVrfNode *)
    {
        u4VrfId = pVrfNode->u4VrfId;

        if (gBgpCxtNode[u4VrfId] == NULL)
        {
            continue;
        }

        if (pVrfNode->u1Oper == BGP4_VRF_CHANGE_EVENT)
        {
            Bgp4VrfChangeHandler (u4VrfId, pVrfNode->u1VrfChngSts);
        }
        if (BGP4_VPN4_VRF_SPEC_GET_FLAG (u4VrfId) &
            BGP4_VRF_EXP_TGT_CHG_REISSUE)
        {
            Bgp4InitNetAddressStruct (&(BGP4_VPN4_VRF_SPEC_INIT_NET_ADDR
                                        (pVrfNode->u4VrfId)),
                                      BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_VPNV4_UNICAST);
        }

        if ((BGP4_VPN4_VRF_SPEC_GET_FLAG (u4VrfId) & BGP4_VRF_DOWN_INPROGRESS)
            || (BGP4_VPN4_VRF_SPEC_GET_FLAG (u4VrfId) &
                BGP4_VRF_NO_EXP_TARGETS))
        {
            /* either VRF is going down, or the export route targets are
             * removed
             */
            i4Sts = Bgp4Vpn4ProcessVrfDown (u4VrfId);
        }
        else if ((BGP4_VPN4_VRF_SPEC_GET_FLAG (u4VrfId) &
                  BGP4_VRF_EXP_TGT_CHG_INPROGRESS) ||
                 (BGP4_VPN4_VRF_SPEC_GET_FLAG (u4VrfId) &
                  BGP4_VRF_EXP_TGT_CHG_REISSUE) ||
                 (BGP4_VPN4_VRF_SPEC_GET_FLAG (u4VrfId) &
                  BGP4_VRF_NO_EXP_TARGETS))
        {
            /* export target change is going on */
            i4Sts = Bgp4Vpnv4ApplyVrfExpTgtChg (u4VrfId);
        }

        if (i4Sts == BGP4_FALSE)    /* complete */
        {
            BGP4_VPN4_VRF_SPEC_RESET_FLAG (u4VrfId, (UINT4)
                                           (BGP4_VRF_EXP_TGT_CHG_INPROGRESS |
                                            BGP4_VRF_EXP_TGT_CHG_REISSUE |
                                            BGP4_VRF_DOWN_INPROGRESS |
                                            BGP4_VRF_NO_EXP_TARGETS));
            Bgp4InitNetAddressStruct (&(BGP4_VPN4_VRF_SPEC_INIT_NET_ADDR
                                        (u4VrfId)),
                                      BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_VPNV4_UNICAST);
            /* FIX:ST_52:
             * When the de-init is over, if the init is pending, then
             * do init again, or else, if the vrf is to be deleted, then
             * delete the vrf
             */
            if ((BGP4_VPN4_VRF_SPEC_GET_FLAG (u4VrfId) &
                 BGP4_VRF_INIT_PENDING) &&
                (pVrfNode->u1VrfChngSts != BGP4_L3VPN_VRF_DELETE))
            {
                /* Call vrf-up only when the vrf state is down. if the vrf
                 * is to be deleted, then we should wait till all the routes
                 * are deleted and advertised to the peers.
                 */
                Bgp4Vpnv4VrfUpHdlr (u4VrfId, BGP4_L3VPN_VRF_UP);
            }
            if ((pVrfNode->u1VrfChngSts == BGP4_L3VPN_VRF_DELETE) &&
                (BGP4_RIB_ROUTES_COUNT (u4VrfId) == 0))
            {
                /* vrf-delete is complete. now, check whether init is pending,
                 * if it is, then call the vrf-up otherwise. (next iteration)
                 * if create is pending, then just update the current state.
                 * otherwise, delete this node from the vrf-change list
                 */
                if (BGP4_VPN4_VRF_SPEC_GET_FLAG (u4VrfId) &
                    BGP4_VRF_INIT_PENDING)
                {
                    pVrfNode->u1VrfChngSts = BGP4_L3VPN_VRF_UP;
                }
                else
                {
                    if (BGP4_VPN4_VRF_SPEC_GET_FLAG (u4VrfId) &
                        BGP4_VRF_CREATE_PENDING)
                    {
                        /* If the create is pending, then just reset the flag
                         * and make the state as CREATE.
                         * This happends, when the events have come from IP-VPN
                         * as <...up, down, delete, create ...>
                         */
                        BGP4_VPN4_VRF_SPEC_RESET_FLAG (u4VrfId,
                                                       (UINT4)
                                                       BGP4_VRF_CREATE_PENDING);
                        BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) =
                            BGP4_L3VPN_VRF_CREATE;
                    }
                    else
                    {
                        /* if VRF is to be deleted, then delete it */
                        Bgp4Vpnv4VrfDelete (u4VrfId);
                    }
                    TMO_SLL_Delete (BGP4_VRF_STS_CHNG_LIST,
                                    (tTMO_SLL_NODE *) pVrfNode);
                    MemReleaseMemBlock (BGP4_VPN4_VRF_NODE_CHG_MEMPOOL_ID,
                                        (UINT1 *) pVrfNode);
                }
            }
            else if (pVrfNode->u1VrfChngSts != BGP4_L3VPN_VRF_DELETE)
            {
                /* If the request is not for vrf delete, then remove the node
                 * from the list.
                 */
                TMO_SLL_Delete (BGP4_VRF_STS_CHNG_LIST,
                                (tTMO_SLL_NODE *) pVrfNode);
                MemReleaseMemBlock (BGP4_VPN4_VRF_NODE_CHG_MEMPOOL_ID,
                                    (UINT1 *) pVrfNode);
            }
        }
        u4VrfsCnt++;
        if (u4VrfsCnt >= BGP4_MAX_PRCS_VRF_CHG)
        {
            break;
        }
    }
    if (Bgp4IsCpuNeeded () == BGP4_TRUE)
    {
        /* External events requires CPU. Relinquish CPU. */
        return (BGP4_RELINQUISH);
    }

    if (TMO_SLL_Count (BGP4_VRF_STS_CHNG_LIST) > 0)
    {
        return (BGP4_TRUE);
    }
    else
    {
        return (BGP4_FALSE);
    }
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4ApplyVrfExpTgtChg                     */
/* Description     : This will send route-refresh message to the CE */
/*                   peers that are associated with the input VRF   */
/*                   and applies softout for routes present in VRF  */
/* Input(s)        : u4VrfId  - VRF info                            */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None.                                */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_FALSE, if the list becomes empty*/
/*                             BGP4_TRUE, some VRFs are pending     */
/********************************************************************/
INT4
Bgp4Vpnv4ApplyVrfExpTgtChg (UINT4 u4VrfId)
{
    tTMO_SLL            FeasRtList;
    tTMO_SLL            PeerFeasRtList;
    tTMO_SLL            DummyWithRtList;
    tRouteProfile       RouteProfile;
    tNetAddress         InvNetAddr;
    tLinkNode          *pRtLink = NULL;
    tLinkNode          *pTempLinkNode = NULL;
    tRouteProfile      *pRtProfile = NULL;
    VOID               *pRibNode = NULL;
    tRouteProfile      *pNextRt = NULL;
    tBgp4PeerEntry     *pPeerInfo = NULL;
    UINT4               u4RtCnt = 0;
    INT4                i4Sts = BGP4_TRUE;
    UINT1               u1RtAdvertise;

    /* If the CE Peer is negotiated for Route-Refresh then, send the 
     * route-refresh message to get all <ipv4, unicast> routes.
     */
    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4VrfId), pPeerInfo, tBgp4PeerEntry *)
    {
        if (BGP4_PEER_NEG_CAP_MASK (pPeerInfo) & CAP_NEG_ROUTE_REFRESH_MASK)
        {
            /* send route-refresh */
            Bgp4RtRefRequestHandler (u4VrfId,
                                     BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo),
                                     BGP4_INET_AFI_IPV4,
                                     BGP4_INET_SAFI_UNICAST,
                                     BGP4_RR_ADVT_DEFAULT);

        }
    }

    BGP4_VPN4_VRF_SPEC_SET_FLAG (u4VrfId, BGP4_VRF_EXP_TGT_CHG_INPROGRESS);
    Bgp4InitNetAddressStruct (&(InvNetAddr), BGP4_INET_AFI_IPV4,
                              BGP4_INET_SAFI_VPNV4_UNICAST);
    pRibNode = BGP4_VPN4_RIB_TREE (u4VrfId);
    if ((NetAddrMatch (InvNetAddr,
                       BGP4_VPN4_VRF_SPEC_INIT_NET_ADDR (u4VrfId))) ==
        BGP4_TRUE)
    {
        /* Get the first route */
        if ((Bgp4RibhGetFirstEntry (CAP_MP_VPN4_UNICAST,
                                    &pRtProfile, &pRibNode, TRUE)
             == BGP4_FAILURE))
        {
            Bgp4InitNetAddressStruct (&
                                      (BGP4_VPN4_VRF_SPEC_INIT_NET_ADDR
                                       (u4VrfId)), BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_VPNV4_UNICAST);
            return (BGP4_FALSE);
        }
    }
    else
    {
        /* Get the next route */
        Bgp4CopyNetAddressStruct (&(RouteProfile.NetAddress),
                                  BGP4_VPN4_VRF_SPEC_INIT_NET_ADDR (u4VrfId));
        if (Bgp4RibhLookupRtEntry (&RouteProfile, u4VrfId,
                                   BGP4_TREE_FIND_EXACT,
                                   &pRtProfile, &pRibNode) == BGP4_FAILURE)
        {
            Bgp4InitNetAddressStruct (&(BGP4_VPN4_VRF_SPEC_INIT_NET_ADDR
                                        (u4VrfId)), BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_VPNV4_UNICAST);
            return (BGP4_FALSE);
        }
    }

    TMO_SLL_Init (&FeasRtList);

    for (;;)
    {
        u1RtAdvertise = BGP4_TRUE;
        if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
        {
            if (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRtProfile)) ==
                BGP4_VPN4_PE_PEER)
            {
                u1RtAdvertise = BGP4_FALSE;
            }
        }
        if (u1RtAdvertise == BGP4_TRUE)
        {
            Bgp4DshAddRouteToList (&FeasRtList, pRtProfile, 0);
            /* Increment the processed route count. */
        }

        if (u4RtCnt == 0)
        {
            /* make rib node as null for the first time we call getnext entry,
             * for the rest of the times, this will be modified as trie
             * context and will be used by trie internally to avoid scanning
             */
            pRibNode = NULL;
        }

        u4RtCnt++;
        if (Bgp4RibhGetNextEntry (pRtProfile, u4VrfId,
                                  &pNextRt, &pRibNode, TRUE) == BGP4_FAILURE)
        {
            i4Sts = BGP4_FALSE;
            Bgp4InitNetAddressStruct (&(BGP4_VPN4_VRF_SPEC_INIT_NET_ADDR
                                        (u4VrfId)), BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_VPNV4_UNICAST);
            break;
        }

        if (u4RtCnt > BGP4_VRF_CHG_MAX_RTS_CNT)
        {
            Bgp4CopyNetAddressStruct (&(BGP4_VPN4_VRF_SPEC_INIT_NET_ADDR
                                        (u4VrfId)),
                                      BGP4_RT_NET_ADDRESS_INFO (pNextRt));
            break;
        }
        pRtProfile = pNextRt;
        pNextRt = NULL;
    }

    TMO_SLL_Init (&DummyWithRtList);

    if (TMO_SLL_Count (&FeasRtList) > 0)
    {
        TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (BGP4_DFLT_VRFID), pPeerInfo,
                      tBgp4PeerEntry *)
        {
            if (BGP4_VPN4_PEER_ROLE (pPeerInfo) == BGP4_VPN4_PE_PEER)
            {
                TMO_SLL_Init (&PeerFeasRtList);
                TMO_SLL_Scan (&FeasRtList, pRtLink, tLinkNode *)
                {
                    pRtProfile = pRtLink->pRouteProfile;
                    /* Check for the Route can be advertised. */
                    if (Bgp4PeerCanInitRouteBeAdvt (pPeerInfo,
                                                    pRtProfile) == BGP4_TRUE)
                    {
                        /* Route can be advertised. */
                        Bgp4DshAddRouteToList (&PeerFeasRtList, pRtProfile, 0);
                        BGP4_RT_RESET_FLAG (pRtProfile,
                                            (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                             BGP4_RT_ADVT_NOTTO_EXTERNAL));
                    }
                }

                /* Add route list to peer Tx Q */
                Bgp4PeerSendRoutesToPeerTxQ (pPeerInfo, &PeerFeasRtList,
                                             &DummyWithRtList, BGP4_FALSE);
                Bgp4DshReleaseList (&PeerFeasRtList, 0);
            }
        }
    }

    /*  The flags NOTTO_EXTERNAL and NOTTO_INTERNAL needs to be reset now. */
    BGP_SLL_DYN_Scan (&FeasRtList, pRtLink, pTempLinkNode, tLinkNode *)
    {
        Bgp4DshRemoveNodeFromList (&FeasRtList, pRtLink);
    }

    Bgp4DshReleaseList (&DummyWithRtList, 0);
    return (i4Sts);
}

/********************************************************************/
/* Function Name   : Bgp4Vpn4ProcessVrfDown                         */
/* Description     : Process VRF for down. It removes all the routes*/
/*                   from the VRF only when it is not present in any*/
/*                   other VRFs                                     */
/*                 : When the export targets are removed for a VRF  */
/*                   completely, this function will delete the      */
/*                   routes that were received from CE peers, who   */
/*                   are allowed to send the export targets         */
/* Input(s)        : u4VrfId  - VRF info                            */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None.                                */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_FALSE, if the list becomes empty*/
/*                             BGP4_TRUE, some VRFs are pending     */
/********************************************************************/
INT4
Bgp4Vpn4ProcessVrfDown (UINT4 u4VrfId)
{
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pNextRt = NULL;
    tRouteProfile      *pNextRoute = NULL;
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
    tRtInstallVrfInfo  *pTempRtVrfInfo = NULL;
    VOID               *pRibNode = NULL;
    UINT4               u4RtCnt = 0;
    INT4                i4Sts = BGP4_TRUE;
    INT4                i4Ret = 0;
    INT4                i4DelRtRet = BGP4_FAILURE;
    UINT4               u4InstallVrfId = 0;

    /* VRF is going to be down. Remove all the routes that are present
     * in VRF. 
     */
    pRibNode = BGP4_VPN4_RIB_TREE (u4VrfId);

    /* Get the first route */
    i4Sts = Bgp4RibhGetFirstEntry (CAP_MP_VPN4_UNICAST, &pRtProfile,
                                   &pRibNode, TRUE);
    if (i4Sts == BGP4_FAILURE)
    {
        return (BGP4_FALSE);
    }

    for (;;)
    {
        i4Ret = Bgp4RibhGetNextEntry (pRtProfile, u4VrfId,
                                      &pNextRoute, &pRibNode, TRUE);
        pRtVrfInfo = NULL;
        pNextRt = pRtProfile;
        while (pNextRt != NULL)
        {
            pRtProfile = pNextRt;
            Bgp4Vpnv4GetInstallVrfInfo (pRtProfile, u4VrfId, &pRtVrfInfo);

            if (pRtVrfInfo == NULL)
            {
                BGP4_RT_GET_NEXT (pRtProfile, u4VrfId, pNextRt);
                continue;
            }

            /* 1. delete this route from VPN-RIB and BGP-VRF-RIB
             * 2. Add this route to FIB update list to update IP
             * 3. Add this route to internal peer advertisement list
             */
            if ((BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID) &&
                (BGP4_VPN4_VRF_SPEC_GET_FLAG (u4VrfId) &
                 BGP4_VRF_NO_EXP_TARGETS)
                && (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRtProfile)) ==
                    BGP4_VPN4_PE_PEER))
                /*&& (u4VrfId ==
                   BGP4_PEER_CXT_ID (BGP4_RT_PEER_ENTRY (pRtProfile)))) */
            {
                BGP4_RT_GET_NEXT (pRtProfile, u4VrfId, pNextRt);
                continue;
            }

            if (((BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID) ||
                 ((BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID) &&
                  (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRtProfile)) ==
                   BGP4_VPN4_CE_PEER))) &&
                (BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo) &
                 BGP4_RT_BGP_VRF_BEST))
            {
                BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);
                BGP4_VPN4_RT_VRF_SET_FLAG (pRtVrfInfo, BGP4_RT_VRF_WITHDRAWN);
                Bgp4AddWithdRouteToIntPeerAdvtList (pRtProfile);
                if (!
                    ((BGP4_VPN4_VRF_SPEC_GET_FLAG (u4VrfId) &
                      BGP4_VRF_DOWN_INPROGRESS) == BGP4_VRF_DOWN_INPROGRESS))
                {
                    BGP4_RT_GET_NEXT (pRtProfile, u4VrfId, pNextRt);
                    continue;
                }
            }

            BGP4_RT_REF_COUNT (pRtProfile)++;

            i4DelRtRet = Bgp4Vpnv4IsRouteCanBeProcessed (pRtProfile);
            if (i4DelRtRet == BGP4_FAILURE)
            {
                /* route is not present in any VRF, can be deleted */
                i4DelRtRet = Bgp4Vpnv4DelRouteFromVPNRIB (pRtProfile);
                if (i4DelRtRet == BGP4_FAILURE)
                {
                    BGP4_DBG (BGP4_DBG_ERROR,
                              "\tBgp4Vpn4ProcessVrfDown(): Route deletion"
                              "from VPNRIB failed.\n");
                }
                if ((BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID) &&
                    (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRtProfile)) ==
                     BGP4_VPN4_PE_PEER))
                {
                    /* route is received from a PE peer, then add this route
                     * for FIB and advertisement
                     */
                    BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);
                    BGP4_VPN4_RT_VRF_SET_FLAG (pRtVrfInfo,
                                               BGP4_RT_VRF_WITHDRAWN);
                    /* if (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRtProfile)) ==
                       BGP4_VPN4_PE_PEER)
                       { */
                    BGP4_TRC_ARG2 (NULL,
                                   BGP4_TRC_FLAG, BGP4_DETAIL_TRC,
                                   BGP4_MOD_NAME,
                                   "\tCalling  Bgp4IgphUpdateRouteInFIB    from Func[%s] Line[%d]",
                                   __func__, __LINE__);
                    Bgp4IgphUpdateRouteInFIB (pRtProfile);
                    Bgp4AddWithdRouteToIntPeerAdvtList (pRtProfile);
                }
            }

            BGP4_RT_GET_NEXT (pRtProfile, u4VrfId, pNextRt);
            i4DelRtRet = Bgp4RibhDelRtEntry (pRtProfile,
                                             pRibNode, u4VrfId, BGP4_TRUE);

            if (i4DelRtRet == BGP4_FAILURE)
            {
                BGP4_DBG (BGP4_DBG_ERROR,
                          "\tBgp4Vpn4ProcessVrfDown(): Route deletion"
                          "from RIB failed.\n");
            }
            if ((BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED) &&
                (BGP4_RT_CXT_ID (pRtProfile) == u4VrfId))

            {
                /* If route leaking is enabled ,Scan other VRFs for
                 * Leaked route and delete the leaked route also
                 * from the RIB */

                TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST
                              (pRtProfile), pTempRtVrfInfo, tRtInstallVrfInfo *)
                {
                    u4InstallVrfId =
                        BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pTempRtVrfInfo);

                    if (u4InstallVrfId == u4VrfId)
                    {
                        continue;
                    }
                    BGP4_VPN4_RT_VRF_SET_FLAG (pTempRtVrfInfo,
                                               BGP4_RT_VRF_WITHDRAWN);
                    i4DelRtRet = Bgp4RibhDelRtEntry (pRtProfile,
                                                     pRibNode, u4InstallVrfId,
                                                     BGP4_TRUE);
                    if (i4DelRtRet == BGP4_FAILURE)
                    {
                        BGP4_DBG (BGP4_DBG_ERROR,
                                  "\tBgp4Vpn4ProcessVrfDown(): Route deletion"
                                  "from RIB failed.\n");
                    }
                }
            }
            if (BGP4_RT_IS_LEAK_ROUTE (pRtProfile) == BGP4_TRUE)
            {
                BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);
                BGP4_VPN4_RT_VRF_SET_FLAG (pRtVrfInfo, BGP4_RT_VRF_WITHDRAWN);
                Bgp4IgphUpdateRouteInFIB (pRtProfile);
            }
            /*if ((BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID) &&
               (BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo) &
               BGP4_RT_BGP_VRF_BEST))
               {
               BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);
               Bgp4AddWithdRouteToIntPeerAdvtList (pRtProfile);
               } */
            TMO_SLL_Delete (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile),
                            (tTMO_SLL_NODE *) pRtVrfInfo);
            Bgp4MemReleaseVpnRtInstallVrfNode (pRtVrfInfo);
            pRtVrfInfo = NULL;

            /* Increment the processed route count. */
            u4RtCnt++;
            Bgp4DshReleaseRtInfo (pRtProfile);
        }

        if (i4Ret == BGP4_FAILURE)
        {
            break;
        }

        if (u4RtCnt > BGP4_VRF_CHG_MAX_RTS_CNT)
        {
            return BGP4_TRUE;
        }
        pRtProfile = pNextRoute;
        pNextRoute = NULL;
    }
    BGP4_L3VPN_ROUTE_COUNT (u4VrfId) = 0;
    return BGP4_FALSE;
}

/****************************************************************************/
/* Function Name : Bgp4Vpnv4UpdPeerInitRtInfo                               */
/* Description   : This function takes the route to be removed from the     */
/*               : LOCAL RIB as input and check whether any peer's going    */
/*               : throught initialisation stores this route in its peer    */
/*               : init route info. If so then updates the peer init route  */
/*               : info with the next best route in the RIB. This routine   */
/*               : must be called before a route is removed from LOCAL RIB  */
/* Inputs        : Pointer to the route to be removed (pRtProfile)          */
/*               : u4VrfId - VRF Identifier                                 */
/*               : i4Flag - Flag passed to RIB for semaphore lock decision  */
/* Output(s)       : None.                                                  */
/* Use of Recursion          : None.                                        */
/* Returns                   : BGP4_SUCCESS                                 */
/****************************************************************************/
INT4
Bgp4Vpnv4UpdPeerInitRtInfo (tRouteProfile * pRtProfile, UINT4 u4VrfId,
                            INT4 i4Flag)
{
    VOID               *pNode = NULL;
    tBgp4PeerEntry     *pPeerentry = NULL;
    tRouteProfile      *pNextBestRoute = NULL;
    INT4                i4Ret;

    pNode = BGP4_VPN4_RIB_TREE (u4VrfId);
    /* Scan all the peers in the peer list. */
    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4VrfId), pPeerentry, tBgp4PeerEntry *)
    {
        if ((NetAddrMatch (BGP4_RT_NET_ADDRESS_INFO (pRtProfile),
                           BGP4_PEER_INIT_NETADDR_INFO (pPeerentry)))
            == BGP4_TRUE)
        {
            /* This peer's init route info matches with the
             * route to be deleted. Update this init route
             * info with the next route. */
            i4Ret = Bgp4RibhGetNextEntry (pRtProfile, u4VrfId,
                                          &pNextBestRoute, &pNode, i4Flag);
            if (pNextBestRoute == NULL)
            {
                /* No more route is present in the tree. So reset the
                 * peer's init route info. The peer init routine will
                 * ensure to remove this peer from the peer init list. */
                Bgp4InitNetAddressStruct (&
                                          (BGP4_PEER_INIT_NETADDR_INFO
                                           (pPeerentry)), BGP4_INET_AFI_IPV4,
                                          BGP4_INET_SAFI_UNICAST);
                BGP4_SET_PEER_CURRENT_STATE (pPeerentry, BGP4_PEER_READY);
                if (BGP4_VPN4_VRF_SPEC_GET_FLAG (u4VrfId) &
                    BGP4_VRF_EXP_TGT_CHG_INPROGRESS)
                {
                    /* Update the vrf init prefix */
                    Bgp4InitNetAddressStruct (&
                                              (BGP4_VPN4_VRF_SPEC_INIT_NET_ADDR
                                               (u4VrfId)), BGP4_INET_AFI_IPV4,
                                              BGP4_INET_SAFI_VPNV4_UNICAST);
                }
            }
            else
            {
                Bgp4CopyNetAddressStruct (&
                                          (BGP4_PEER_INIT_NETADDR_INFO
                                           (pPeerentry)),
                                          BGP4_RT_NET_ADDRESS_INFO
                                          (pNextBestRoute));
                Bgp4CopyNetAddressStruct (&
                                          (BGP4_VPN4_VRF_SPEC_INIT_NET_ADDR
                                           (u4VrfId)),
                                          BGP4_RT_NET_ADDRESS_INFO
                                          (pNextBestRoute));
            }
        }
    }
    UNUSED_PARAM (i4Ret);
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name : Bgp4Vpnv4FillVpnExtComm                                  */
/* Description   : This function fills the extended community values from   */
/*                 configured export route targets. If Site-of-orign is to  */
/*                 be filled, this also will be taken care                  */
/* Inputs        : Pointer to the route to be removed (pRtProfile)          */
/*               : Pointer to the peer entry (pPeerEntry)                   */
/*               : Pointer to the advertisement BGP info (pAdvtBgpInfo)     */
/* Output(s)       : None.                                                  */
/* Use of Recursion          : None.                                        */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE                    */
/****************************************************************************/
INT4
Bgp4Vpnv4FillVpnExtComm (tRouteProfile * pRouteProfile,
                         tBgp4PeerEntry * pPeerEntry, tBgp4Info * pAdvtBgpInfo)
{
    tExtCommProfile    *pExportTarget = NULL;
    UINT1              *pu1VpnExtComm = NULL;
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
    UINT4               u4Count;
    UINT4               u4RcvdCount;
    UINT4               u4VrfId = 0;
    UINT1               u1RTRcvd = BGP4_FALSE;

    UNUSED_PARAM (pPeerEntry);
    pu1VpnExtComm = MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);
    if (pu1VpnExtComm == NULL)
    {
        return BGP4_FAILURE;
    }
    u4RcvdCount = BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo);

    if (u4RcvdCount > 0)
    {
        MEMCPY (pu1VpnExtComm, BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo),
                u4RcvdCount * EXT_COMM_VALUE_LEN);
    }
    /* The route is being advertised to PE peer.
     * If this route is received from CE peer and CE has sent some
     * route-targets then attach those route targets */
    if ((BGP4_RT_PROTOCOL (pRouteProfile) == BGP_ID) &&
        (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRouteProfile))
         == BGP4_VPN4_CE_PEER))
    {
        if ((BGP4_INFO_ECOMM_ATTR (BGP4_RT_BGP_INFO (pRouteProfile)) != NULL) &&
            (BGP4_VPN4_RCVD_RT_ECOMM_FLAG (pRouteProfile) == BGP4_TRUE))
        {
            /* The route Targets received from CE  will be sent to the PE 
             * peer*/
            u1RTRcvd = BGP4_TRUE;
        }
    }

    if (u1RTRcvd == BGP4_FALSE)
    {
        if (BGP4_RT_PROTOCOL (pRouteProfile) != BGP_ID)
        {
            pRtVrfInfo = (tRtInstallVrfInfo *)
                TMO_SLL_First (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRouteProfile));
            if (pRtVrfInfo != NULL)
            {
                u4VrfId = BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrfInfo);
            }
        }
        else
        {
            u4VrfId = BGP4_PEER_CXT_ID (BGP4_RT_PEER_ENTRY (pRouteProfile));
        }
        u4Count = TMO_SLL_Count (BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS (u4VrfId));
        if (u4Count > 0)
        {
            /*Some export targets are configured */
            TMO_SLL_Scan (BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS (u4VrfId),
                          pExportTarget, tExtCommProfile *)
            {
                if (BGP4_VPN4_EXT_COMM_ROW_STATUS (pExportTarget) != ACTIVE)
                {
                    continue;
                }
                MEMCPY ((pu1VpnExtComm + (BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo) *
                                          EXT_COMM_VALUE_LEN)),
                        (UINT1 *) pExportTarget->au1ExtComm,
                        EXT_COMM_VALUE_LEN);
                BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo)++;
            }

            if (BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo) == u4RcvdCount)
            {
                /* No export route-targets are active */
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                    pu1VpnExtComm);
                return EXT_COMM_FAILURE;
            }
        }
    }

    /*if ((BGP4_RT_PROTOCOL (pRouteProfile) == BGP_ID) &&
       (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRouteProfile)) ==
       BGP4_VPN4_CE_PEER))
       {
       Fill the site-of-origin value 
       MEMCPY ((pu1VpnExtComm +
       (BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo) * EXT_COMM_VALUE_LEN)),
       BGP4_VPN4_CE_PEER_SOO_EXT_COM_VALUE (BGP4_RT_PEER_ENTRY
       (pRouteProfile)),
       EXT_COMM_VALUE_LEN);
       BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo)++;
       } */
    if (BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo) > 0)
    {
        if (u4RcvdCount > 0)
        {
            ATTRIBUTE_NODE_FREE (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo));
        }
        ATTRIBUTE_NODE_CREATE (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo));
        if (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo) == NULL)
        {
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1VpnExtComm);
            return BGP4_FAILURE;
        }
        MEMCPY (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo), pu1VpnExtComm,
                BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo) * EXT_COMM_VALUE_LEN);
        BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_ECOMM_MASK;
    }
    MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1VpnExtComm);
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4GetRouteTargetFromVrf                 */
/* Description     : Gets route target pointer from a give vrf      */
/* Input(s)        : u4VrfId  - vrf id                              */
/*                   pu1ExtComm - route target value                */
/*                   u1RTType - route target type (import/export)   */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : Route Target pointer                 */
/********************************************************************/
tExtCommProfile    *
Bgp4Vpnv4GetRouteTargetFromVrf (UINT4 u4VrfId, UINT1 *pu1ExtComm,
                                UINT1 u1RTType)
{
    tTMO_SLL           *pRTComList = NULL;
    tExtCommProfile    *pExtRTCom = NULL;

    pRTComList = (u1RTType == L3VPN_BGP4_IMPORT_RT) ?
        BGP4_VPN4_VRF_SPEC_IMPORT_TARGETS (u4VrfId) :
        BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS (u4VrfId);

    TMO_SLL_Scan (pRTComList, pExtRTCom, tExtCommProfile *)
    {
        if ((MEMCMP (pExtRTCom->au1ExtComm,
                     pu1ExtComm, EXT_COMM_VALUE_LEN)) == 0)
        {
            return (pExtRTCom);
        }
    }
    return (NULL);
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4RouteTargetChgHandler                 */
/* Description     : If the import route target is added/deleted    */
/*                   route-refresh message will be sent to PE peers */
/*                   else if the export route target is             */
/*                   added/deleted then this policy is applied on   */
/*                   the routes store in the corresponding VRF      */
/*                   and the best routes will be advertised to PEs  */
/* Input(s)        : u4VrfId  - vrf info                            */
/*                   i4RTType - route target type                   */
/*                   pu1ExtComm - route target value                */
/*                   u1RTType - route target type (import/export)   */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : Route Target pointer                 */
/********************************************************************/
INT4
Bgp4Vpnv4RouteTargetChgHandler (UINT4 u4VrfId, INT4 i4RTType,
                                UINT1 *pu1RouteParam, UINT1 u1OperType)
{
    tTMO_SLL           *pRTComList = NULL;
    tExtCommProfile    *pExtRTCom = NULL;
    UINT1               au1RDtemp[BGP4_VPN4_ROUTE_DISTING_SIZE];
    UINT1              *pu1RDtemp = au1RDtemp;
    if (gBgpCxtNode[u4VrfId] == NULL)
    {
        return BGP4_SUCCESS;
    }

    if (i4RTType == L3VPN_BGP4_RD)
    {
        MEMSET (BGP4_VPN4_VRF_SPEC_ROUTE_DISTING (u4VrfId), 0,
                BGP4_VPN4_ROUTE_DISTING_SIZE);
        if (u1OperType == L3VPN_BGP4_ROUTE_PARAM_ADD)
        {
            MEMCPY (pu1RDtemp, pu1RouteParam, BGP4_VPN4_ROUTE_DISTING_SIZE);
            pu1RDtemp[1] = pu1RDtemp[0];
            pu1RDtemp[0] = 0;
            MEMCPY (BGP4_VPN4_VRF_SPEC_ROUTE_DISTING (u4VrfId), pu1RDtemp,
                    BGP4_VPN4_ROUTE_DISTING_SIZE);
        }
        return BGP4_SUCCESS;
    }
    else
    {
        if ((i4RTType == L3VPN_BGP4_BOTH_RT) &&
            (u1OperType == L3VPN_BGP4_ROUTE_PARAM_ADD))
        {
            pRTComList = BGP4_VPN4_VRF_SPEC_IMPORT_TARGETS (u4VrfId);
            if (Bgp4Vpn4AddRT (pRTComList, pu1RouteParam, L3VPN_BGP4_IMPORT_RT)
                == BGP4_FAILURE)
            {
                return BGP4_FAILURE;
            }

            pRTComList = BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS (u4VrfId);
            if (Bgp4Vpn4AddRT (pRTComList, pu1RouteParam, L3VPN_BGP4_EXPORT_RT)
                == BGP4_FAILURE)
            {
                return BGP4_FAILURE;
            }

        }
        else
        {
            pRTComList = (i4RTType == L3VPN_BGP4_IMPORT_RT) ?
                BGP4_VPN4_VRF_SPEC_IMPORT_TARGETS (u4VrfId) :
                BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS (u4VrfId);
            if (u1OperType == L3VPN_BGP4_ROUTE_PARAM_ADD)
            {
                if (Bgp4Vpn4AddRT (pRTComList, pu1RouteParam, i4RTType) ==
                    BGP4_FAILURE)
                {
                    return BGP4_FAILURE;
                }

            }
        }
        if ((i4RTType == L3VPN_BGP4_IMPORT_RT) ||
            (i4RTType == L3VPN_BGP4_BOTH_RT))
        {
            /* Operation is for import route targets.
             * If any import route target is added or deleted, we should send
             * route refresh message to PE peers.
             */
            if (u1OperType == L3VPN_BGP4_ROUTE_PARAM_DEL)
            {
                pExtRTCom =
                    Bgp4Vpnv4GetRouteTargetFromVrf (u4VrfId, pu1RouteParam,
                                                    (UINT1)
                                                    L3VPN_BGP4_IMPORT_RT);
                if (pExtRTCom != NULL)
                {
                    TMO_SLL_Delete (BGP4_VPN4_VRF_SPEC_IMPORT_TARGETS (u4VrfId),
                                    (tTMO_SLL_NODE *) pExtRTCom);
                    EXT_COMM_PROFILE_FREE (pExtRTCom);
                }
            }
            if (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) == BGP4_L3VPN_VRF_UP)
            {
                Bgp4Vpnv4SendRtRefToPEPeers ();
                if (BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED)
                {
                    if (u1OperType == L3VPN_BGP4_ROUTE_PARAM_ADD)
                    {
                        Bgp4Vpnv4GetLeakRoutesFromPeer (u4VrfId);
                    }
                    else if (u1OperType == L3VPN_BGP4_ROUTE_PARAM_DEL)
                    {
                        Bgp4Vpnv4DelLeakRoutesFromPeer (u4VrfId);
                    }

                }
            }
        }
        if ((i4RTType == L3VPN_BGP4_EXPORT_RT) ||
            (i4RTType == L3VPN_BGP4_BOTH_RT))
        {
            /* Operation is for export route targets. Apply this new policy
             * to the routes present in this vrf and advertise to PE peers.
             */
            if (u1OperType == L3VPN_BGP4_ROUTE_PARAM_DEL)
            {
                pExtRTCom =
                    Bgp4Vpnv4GetRouteTargetFromVrf (u4VrfId, pu1RouteParam,
                                                    (UINT1)
                                                    L3VPN_BGP4_EXPORT_RT);
                if (pExtRTCom != NULL)
                {
                    TMO_SLL_Delete (BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS (u4VrfId),
                                    (tTMO_SLL_NODE *) pExtRTCom);
                    EXT_COMM_PROFILE_FREE (pExtRTCom);
                }
            }
            /* If the VRF is operationally up, then process this change */
            if (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) == BGP4_L3VPN_VRF_UP)
            {
                Bgp4Vpnv4PostExpTgtChgEvent (u4VrfId);

            }
            /* indicate whether export targets are configured are not */
            if (TMO_SLL_Count (BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS (u4VrfId)) ==
                0)
            {
                BGP4_VPN4_VRF_SPEC_SET_FLAG (u4VrfId, BGP4_VRF_NO_EXP_TARGETS);
            }
            else
            {
                BGP4_VPN4_VRF_SPEC_RESET_FLAG (u4VrfId,
                                               (UINT4) BGP4_VRF_NO_EXP_TARGETS);
            }
        }
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpn4ConvertRtToVpn4Route                   */
/* Description     : This function converts the IPv4 route that is  */
/*                   received from a CE peer to a VPN route         */
/* Input(s)        : pRtProfile - route profile pointer             */
/*                   u4VrfId  - VRF information                     */
/* Output(s)       : None.                                          */
/* Use of Recursion: None.                                          */
/* Returns         : BGP4_SUCCESS or BGP4_FAILURE                   */
/********************************************************************/
INT4
Bgp4Vpn4ConvertRtToVpn4Route (tRouteProfile * pRtProfile, UINT4 u4VrfId)
{
    INT4                i4RetVal;

    /* Mark the route to be converted to VPNv4 route */
    BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_CONVERTED_TO_VPNV4);
    BGP4_RT_SAFI_INFO (pRtProfile) = BGP4_INET_SAFI_VPNV4_UNICAST;
    MEMCPY (BGP4_RT_ROUTE_DISTING (pRtProfile),
            BGP4_VPN4_VRF_SPEC_ROUTE_DISTING (u4VrfId),
            BGP4_VPN4_ROUTE_DISTING_SIZE);
    pRtProfile->pBgpCxtNode = Bgp4GetContextEntry (u4VrfId);
    i4RetVal = Bgp4Vpnv4LinkVrfIdToRouteProfile (pRtProfile, u4VrfId);
    return i4RetVal;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4IsRouteCanBeProcessed                 */
/* Description     : This function checks whether a route can be    */
/*                   processed based on the VRFs the route has been */
/*                   installed in.                                  */
/* Input(s)        : pRtProfile - route profile pointer             */
/* Output(s)       : None.                                          */
/* Use of Recursion: None.                                          */
/* Returns         : BGP4_SUCCESS or BGP4_FAILURE                   */
/********************************************************************/
INT4
Bgp4Vpnv4IsRouteCanBeProcessed (tRouteProfile * pRtProfile)
{
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
    INT4                i4Ret = BGP4_FAILURE;
    UINT4               u4VrfId;

    BGP4_VPN4_GET_PROCESS_VRFID (pRtProfile, u4VrfId);

    if (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) == 0)
    {
        /* If the route is already a filtered route, then allow the route for
         * processing.
         * e.g, some routes are installed already into one VRF and the import
         * targets are removed, and new routes are received and are filtered,
         * we should process these routes for the old routes to be removed.
         */
        if ((BGP4_RR_CLIENT_CNT (u4VrfId) > 0) ||
            (BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_FILTERED_INPUT))
        {
            /* the speaker is a route-reflector and some clients are 
             * configred, then also return success
             */
            return (BGP4_SUCCESS);
        }
        return (BGP4_FAILURE);
    }
    TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile), pRtVrfInfo,
                  tRtInstallVrfInfo *)
    {
        u4VrfId = BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrfInfo);
        i4Ret = (((BGP4_VPN4_RIB_TREE (u4VrfId) == NULL)
                  || (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) !=
                      BGP4_L3VPN_VRF_UP)
                  || ((BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
                      && (BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_BEST)
                      && (BGP4_VPN4_VRF_SPEC_GET_FLAG (u4VrfId) &
                          BGP4_VRF_NO_EXP_TARGETS)))) ? BGP4_FAILURE :
            BGP4_SUCCESS;
        if (i4Ret == BGP4_SUCCESS)
        {
            /* One of the VRFs that this route needs to be installed is active
             * so return success
             */
            break;
        }
    }
    return (i4Ret);
}

/****************************************************************************/
/* Function Name : Bgp4Vpnv4DelRouteFromVPNRIB                              */
/* Description   : This function is called to delete the route present in   */
/*                 VPN-RIB. When a VRF is down/deleted before some routes   */
/*                 that were supposed to be installed in that VRF MUST be   */
/*                 deleted from VPN-RIB.                                    */
/* Input(s)      : Pointer to the feasible route profile (pRtProfile)       */
/* Output(s)     : None                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                */
/****************************************************************************/
INT4
Bgp4Vpnv4DelRouteFromVPNRIB (tRouteProfile * pRtProfile)
{
    tTMO_SLL            TsTempFeasibleRouteList;
    tRouteProfile      *pFndRtProfileList = NULL;
    VOID               *pRibNode = NULL;
    tLinkNode          *pLinkNode = NULL;
    INT4                i4RtFound;
    INT4                i4Ret = BGP4_FAILURE;
    UINT1               u1IsRouteInvalid = BGP4_FALSE;

    TMO_SLL_Init (&TsTempFeasibleRouteList);

    i4RtFound = Bgp4RibhLookupRtEntry (pRtProfile, BGP4_DFLT_VRFID,
                                       BGP4_TREE_FIND_EXACT,
                                       &pFndRtProfileList, &pRibNode);
    if (i4RtFound == BGP4_FAILURE)
    {
        /* Route is not found in VPN-RIB */
        return BGP4_FAILURE;
    }

    /* Delete all the alternative routes for this destination */
    if (BGP4_RT_NEXT (pFndRtProfileList) == NULL)
    {
        if (BGP4_OVERLAP_ROUTE_POLICY (BGP4_DFLT_VRFID) != BGP4_INSTALL_BOTH_RT)
        {
            /* Overlapping route policy is not INSTALL_BOTH. So removing
             * a route can cause some other routes to be selected as best
             * route.  and delete these routes also
             */
            Bgp4DshGetDeniedRoutesFromOverlapList (&TsTempFeasibleRouteList,
                                                   pFndRtProfileList,
                                                   BGP4_OVERLAP_ROUTE_POLICY
                                                   (BGP4_DFLT_VRFID));
        }
    }

    u1IsRouteInvalid = (BGP4_IS_ROUTE_VALID (pFndRtProfileList)
                        == BGP4_FALSE) ? BGP4_TRUE : BGP4_FALSE;

    if (BGP4_RT_GET_FLAGS (pFndRtProfileList) & BGP4_RT_OVERLAP)
    {
        /* Route present in overlap list.Remove it from the Overlap 
         * List.*/
        BGP4_RT_REF_COUNT (pFndRtProfileList)++;
        i4Ret = Bgp4RibhDelRtEntry (pFndRtProfileList, pRibNode,
                                    BGP4_DFLT_VRFID, u1IsRouteInvalid);
        Bgp4DshReleaseRtInfo (pFndRtProfileList);
        BGP4_RT_RESET_FLAG (pFndRtProfileList, BGP4_RT_OVERLAP);
        Bgp4DshRemoveRouteFromList (BGP4_OVERLAP_ROUTE_LIST (BGP4_DFLT_VRFID),
                                    pFndRtProfileList);
    }
    else
    {
        BGP4_RT_REF_COUNT (pFndRtProfileList)++;
        i4Ret = Bgp4RibhDelRtEntry (pFndRtProfileList, pRibNode,
                                    BGP4_DFLT_VRFID, u1IsRouteInvalid);
        Bgp4DshReleaseRtInfo (pFndRtProfileList);
    }

    TMO_SLL_Scan (&TsTempFeasibleRouteList, pLinkNode, tLinkNode *)
    {
        pFndRtProfileList = pLinkNode->pRouteProfile;
        u1IsRouteInvalid = (BGP4_IS_ROUTE_VALID (pFndRtProfileList)
                            == BGP4_FALSE) ? BGP4_TRUE : BGP4_FALSE;
        BGP4_RT_REF_COUNT (pFndRtProfileList)++;
        i4Ret = Bgp4RibhDelRtEntry (pFndRtProfileList, pRibNode,
                                    BGP4_DFLT_VRFID, u1IsRouteInvalid);
        Bgp4DshReleaseRtInfo (pFndRtProfileList);
    }
    Bgp4DshReleaseList (&TsTempFeasibleRouteList, 0);
    return (i4Ret);
}

/*************************************************************************/
/*  Function Name   : Bgp4RegisterWithMpls                               */
/*  Input(s)        : None.                                              */
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS/FAILURE                                    */
/*  Description     : Registers with MPLS for                            */
/*                    1. RouteParams addition/deletion                   */
/*                    2. LSP status Handling                             */
/*                    3. VRF status Handling                             */
/*************************************************************************/
INT4
Bgp4RegisterWithMpls (VOID)
{
#ifdef MPLS_L3VPN_WANTED
    tMplsL3VpnRegInfo   MplsL3VpnRegInfo;

    MEMSET ((UINT1 *) &MplsL3VpnRegInfo, 0, sizeof (tMplsL3VpnRegInfo));
    MplsL3VpnRegInfo.pBgp4MplsL3VpnNotifyRouteParams
        = BgpMplsRouteParamsNotification;
    MplsL3VpnRegInfo.pBgp4MplsL3VpnNotifyLspStatusChange
        = BgpMplsLspStatusChangeNotification;
    MplsL3VpnRegInfo.pBgp4MplsL3vpnNotifyVrfAdminStatusChange
        = BgpMplsVrfAdminStatusChangeNotification;
    MplsL3VpnRegInfo.u2InfoMask |=
        (L3VPN_BGP4_ROUTE_PARAMS_REQ | L3VPN_BGP4_LSP_STATUS_REQ |
         L3VPN_BGP4_VRF_ADMIN_STATUS_REQ);
    if (BGP4_SUCCESS == MplsL3VpnRegisterCallback (&MplsL3VpnRegInfo))
    {
        return BGP4_SUCCESS;
    }
    return BGP4_FAILURE;
#else
    return BGP4_SUCCESS;
#endif
}

VOID
Bgp4UpdateRouteInMplsDb (tRouteProfile * pRtProfile, UINT4 u4VrfId,
                         UINT1 u1ILMAction, UINT1 u1LabelAction)
{
#ifdef MPLS_L3VPN_WANTED
    tMplsL3VpnBgp4RouteInfo MplsL3VpnBgp4RouteInfo;
    UINT4               u4DestAddr = 0;
    UINT4               u4NextHopAddr = 0;
    UINT4               u4IfIndex = 0;

    if ((BGP4_RT_HWSTATUS (pRtProfile) == TRUE
         && u1ILMAction == L3VPN_BGP4_ROUTE_ADD)
        || (BGP4_RT_HWSTATUS (pRtProfile) != TRUE
            && u1ILMAction == L3VPN_BGP4_ROUTE_DEL))
    {
        return;
    }

    BGP4_TRC_ARG5 (NULL, BGP4_TRC_FLAG, BGP4_FDB_TRC, BGP4_MOD_NAME,
                   "\t Received VPN route %s for updating in MPLS DB with nexthop %s "
                   "immediate nexthop %s flags 0x%x and extended flags 0x%x\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP
                                    (BGP4_RT_BGP_INFO (pRtProfile)),
                                    BGP4_INFO_AFI (BGP4_RT_BGP_INFO
                                                   (pRtProfile))),
                   Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pRtProfile),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                     (pRtProfile))), pRtProfile->u4Flags,
                   pRtProfile->u4ExtFlags);

    MEMSET ((UINT1 *) &MplsL3VpnBgp4RouteInfo, 0,
            sizeof (tMplsL3VpnBgp4RouteInfo));

    MplsL3VpnBgp4RouteInfo.IpPrefix.u2Afi = BGP4_RT_AFI_INFO (pRtProfile);

    MplsL3VpnBgp4RouteInfo.IpPrefix.u2AddressLen =
        BGP4_RT_IP_ADDRLEN (pRtProfile);

    PTR_FETCH_4 (u4DestAddr, BGP4_RT_IP_PREFIX (pRtProfile));

    u4DestAddr = OSIX_HTONL (u4DestAddr);

    MEMCPY (&MplsL3VpnBgp4RouteInfo.IpPrefix.au1Address, (UINT1 *) &u4DestAddr,
            4);

    MplsL3VpnBgp4RouteInfo.NextHop.u2Afi =
        BGP4_INFO_AFI (BGP4_RT_BGP_INFO (pRtProfile));

    MplsL3VpnBgp4RouteInfo.NextHop.u2AddressLen =
        BGP4_INFO_ADDR_LEN (BGP4_RT_BGP_INFO (pRtProfile));

    PTR_FETCH_4 (u4NextHopAddr,
                 BGP4_INFO_NEXTHOP (BGP4_RT_BGP_INFO (pRtProfile)));

    u4NextHopAddr = OSIX_HTONL (u4NextHopAddr);

    MEMCPY (&MplsL3VpnBgp4RouteInfo.NextHop.au1Address, &u4NextHopAddr, 4);

    MplsL3VpnBgp4RouteInfo.u4Label = BGP4_RT_LABEL_INFO (pRtProfile);

    MplsL3VpnBgp4RouteInfo.u4IfIndex = BGP4_RT_GET_RT_IF_INDEX (pRtProfile);

    NetIpv4GetCfaIfIndexFromPort ((UINT2)
                                  BGP4_RT_GET_RT_IF_INDEX (pRtProfile),
                                  &u4IfIndex);

    MplsL3VpnBgp4RouteInfo.u4IfIndex = u4IfIndex;

    if (BGP4_RT_PEER_ENTRY (pRtProfile) != NULL)
    {
        MplsL3VpnBgp4RouteInfo.u4NHAS =
            BGP4_PEER_ASNO (BGP4_RT_PEER_ENTRY (pRtProfile));
    }

    MplsL3VpnBgp4RouteInfo.u4VrfId = u4VrfId;

    MplsL3VpnBgp4RouteInfo.u4Metric = BGP4_RT_MED (pRtProfile);

    MplsL3VpnBgp4RouteInfo.u2PrefixLen = BGP4_RT_PREFIXLEN (pRtProfile);

    MplsL3VpnBgp4RouteInfo.u1ILMAction = u1ILMAction;

    MplsL3VpnBgp4RouteInfo.u1LabelAction = u1LabelAction;

    MplsL3VpnBgp4RouteInfo.u1LabelAllocPolicy =
        BGP4_LABEL_ALLOC_POLICY (BGP4_RT_CXT_ID (pRtProfile));

    if (u1ILMAction == L3VPN_BGP4_ROUTE_ADD)
    {
        BGP4_RT_SET_EXT_FLAG (pRtProfile, BGP4_RT_IN_RTM);
    }
    else
    {
        BGP4_RT_RESET_EXT_FLAG (pRtProfile, BGP4_RT_IN_RTM);
    }
    if ((BGP4_LABEL_ALLOC_POLICY (BGP4_RT_CXT_ID (pRtProfile)) ==
         BGP4_PERVRF_LABEL_ALLOC_POLICY)
        && (u1LabelAction == L3VPN_BGP4_LABEL_POP))
    {
        if (u1ILMAction == L3VPN_BGP4_ROUTE_ADD)
        {
            if (BGP4_L3VPN_ROUTE_COUNT (u4VrfId) == 0)
            {
                MplsL3VpnBgp4RouteUpdate (&MplsL3VpnBgp4RouteInfo);
            }
            BGP4_L3VPN_ROUTE_COUNT (u4VrfId)++;
        }
        else if (u1ILMAction == L3VPN_BGP4_ROUTE_DEL)
        {
            if (BGP4_L3VPN_ROUTE_COUNT (u4VrfId) > 0)
            {
                BGP4_L3VPN_ROUTE_COUNT (u4VrfId)--;
            }
            if (BGP4_L3VPN_ROUTE_COUNT (u4VrfId) == 0)
            {
                MplsL3VpnBgp4RouteUpdate (&MplsL3VpnBgp4RouteInfo);
            }
        }
    }
    else
    {
        MplsL3VpnBgp4RouteUpdate (&MplsL3VpnBgp4RouteInfo);
    }
    if (u1ILMAction == L3VPN_BGP4_ROUTE_ADD)
    {
        BGP4_RT_HWSTATUS (pRtProfile) = TRUE;
    }
    else
    {
        BGP4_RT_HWSTATUS (pRtProfile) = FALSE;
    }

#else
    UNUSED_PARAM (pRtProfile);
    UNUSED_PARAM (u4VrfId);
    UNUSED_PARAM (u1ILMAction);
    UNUSED_PARAM (u1LabelAction);
#endif
    return;

}

INT4
Bgp4DeRegisterWithMpls (VOID)
{
#ifdef MPLS_L3VPN_WANTED
    tMplsL3VpnRegInfo   MplsL3VpnRegInfo;

    MEMSET ((UINT1 *) &MplsL3VpnRegInfo, 0, sizeof (tMplsL3VpnRegInfo));
    MplsL3VpnRegInfo.u2InfoMask |=
        (L3VPN_BGP4_ROUTE_PARAMS_REQ | L3VPN_BGP4_LSP_STATUS_REQ |
         L3VPN_BGP4_VRF_ADMIN_STATUS_REQ);
    if (OSIX_SUCCESS == MplsL3VpnDeRegisterCallback (&MplsL3VpnRegInfo))
    {
        return BGP4_SUCCESS;
    }
    return BGP4_FAILURE;
#else
    return BGP4_SUCCESS;
#endif

}

INT4
Bgp4Vpn4AddRT (tTMO_SLL * pRTComList, UINT1 *pu1RouteParam, INT4 i4RTType)
{
    tExtCommProfile    *pExtRTCom = NULL;
    EXT_COMM_PROFILE_CREATE (pExtRTCom);
    if (pExtRTCom == NULL)
    {
        return BGP4_FAILURE;
    }
    TMO_SLL_Init_Node (&(pExtRTCom->ExtCommProfileNext));
    MEMCPY (pExtRTCom->au1ExtComm, pu1RouteParam, EXT_COMM_VALUE_LEN);
    pExtRTCom->au1ExtComm[1] = 2;
    if (i4RTType == L3VPN_BGP4_EXPORT_RT)
    {
        BGP4_VPN4_EXT_COMM_ROW_STATUS (pExtRTCom) = ACTIVE;
    }
    TMO_SLL_Add (pRTComList, (tTMO_SLL_NODE *) pExtRTCom);
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name : Bgp4Vpnv4SendVpnv4RtsToPeer                              */
/* Description   : This function is called to get the VPNV4 routes for the  */
/*                 VRF and send the routes to PE peers. If VPNV4 routes are */
/*                 not available, get the IPV4 routes for the VRF, convert  */
/*                 them into VPNV4 routes and send the routes to PE peers   */
/* Input(s)      : u4Context  - VRF number                                  */
/* Output(s)     : None                                                     */
/* Return(s)     : None                                                     */
/****************************************************************************/

VOID
Bgp4Vpnv4SendVpnv4RtsToPeer (UINT4 u4Context)
{
    INT4                i4RetStatus;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pVpnRtProfile = NULL;
    tTMO_SLL            TsRouteList;
    tLinkNode          *pLinkNode = NULL;
    UINT4               u4Label = 0;
    BOOL1               bRedistList = BGP4_FALSE;
    BOOL1               bIsVpnRtAvailable = BGP4_FALSE;

    TMO_SLL_Init (&TsRouteList);

    /* When VRF is made up, get the VPNV4 routes for the VRF and
     * send the routes to PE peers.
     * If VPNV4 routes are not available, get the IPV4 routes for the VRF, 
     * convert them into VPNV4 routes and send the routes to
     * PE peers
     * */
    Bgp4RcvdPADBGetFirstBestRoute (u4Context, &pVpnRtProfile,
                                   BGP4_INET_AFI_IPV4,
                                   BGP4_INET_SAFI_VPNV4_UNICAST);

    while (pVpnRtProfile != NULL)
    {
        Bgp4AddFeasRouteToIntPeerAdvtList (pVpnRtProfile, NULL);
        bIsVpnRtAvailable = BGP4_TRUE;
        if (Bgp4RcvdPADBGetNextBestRoute (pVpnRtProfile, &pVpnRtProfile)
            == BGP4_FAILURE)
        {
            break;
        }
    }
    pLinkNode = (tLinkNode *) TMO_SLL_First (BGP4_REDIST_LIST (u4Context));
    if (pLinkNode != NULL)
    {
        pRtProfile = pLinkNode->pRouteProfile;
    }

    while (pRtProfile != NULL)
    {
        if (BGP4_RT_SAFI_INFO (pRtProfile) == BGP4_INET_SAFI_VPNV4_UNICAST)
        {
            bIsVpnRtAvailable = BGP4_TRUE;
            break;
        }
        pLinkNode = (tLinkNode *) TMO_SLL_Next (BGP4_REDIST_LIST (u4Context),
                                                &pLinkNode->TSNext);
        pRtProfile = NULL;
        if (pLinkNode != NULL)
        {
            pRtProfile = pLinkNode->pRouteProfile;
        }
    }
    if (bIsVpnRtAvailable == BGP4_TRUE)
    {
        return;
    }

    Bgp4RcvdPADBGetFirstBestRoute (u4Context, &pRtProfile,
                                   BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);

    if (pRtProfile == NULL)
    {
        /*If routes are not available in the RIB, we need to take the 
         * routes from Redistribution list and convert them into VPNV4 route.
         * In this case, still routes are not taken from the Redistribution
         * list and added into the RIB. Hence it should be taken from
         * redistribution list and convert into VPNV4 route*/
        bRedistList = BGP4_TRUE;
        pLinkNode = (tLinkNode *) TMO_SLL_First (BGP4_REDIST_LIST (u4Context));
        if (pLinkNode != NULL)
        {
            pRtProfile = pLinkNode->pRouteProfile;
        }
    }

    while (pRtProfile != NULL)
    {
        pVpnRtProfile = Bgp4DupCompleteRtProfile (pRtProfile);

        if (pVpnRtProfile == NULL)
        {
            break;
        }

        if (Bgp4Vpn4ConvertRtToVpn4Route (pVpnRtProfile, u4Context)
            == BGP4_FAILURE)
        {
            Bgp4DshReleaseRtInfo (pVpnRtProfile);
            KW_FALSEPOSITIVE_FIX1 (pVpnRtProfile);
            break;
        }
        if ((BGP4_LABEL_ALLOC_POLICY (0) ==
             BGP4_PERVRF_LABEL_ALLOC_POLICY) &&
            (BGP4_VPNV4_LABEL (u4Context) != 0))
        {
            u4Label = BGP4_VPNV4_LABEL (u4Context);
        }
        else if (Bgp4GetBgp4Label (&u4Label) == BGP4_FAILURE)
        {
            Bgp4DshReleaseRtInfo (pVpnRtProfile);
            KW_FALSEPOSITIVE_FIX1 (pVpnRtProfile);
            break;
        }
        BGP4_RT_LABEL_CNT (pVpnRtProfile)++;
        BGP4_RT_LABEL_INFO (pVpnRtProfile) = u4Label;
        if (BGP4_LABEL_ALLOC_POLICY (0) == BGP4_PERVRF_LABEL_ALLOC_POLICY)
        {
            BGP4_VPNV4_LABEL (u4Context) = u4Label;
        }

        if (BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED)
        {
            /* IF VRF Route-Leaking is enabled ,then scan for other non-Default 
             * VRFs added and import its respective Route-target*/
            i4RetStatus = Bgp4Vpnv4VrftoVrfRouteLeak (pVpnRtProfile);

            if (i4RetStatus == BGP4_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tVPN:Bgp4Vpnv4VrftoVrfRouteLeak Failed \n");
            }
        }

        Bgp4DshAddRouteToList (&TsRouteList, pVpnRtProfile, 0);

        if (bRedistList == BGP4_TRUE)
        {
            pLinkNode =
                (tLinkNode *) TMO_SLL_Next (BGP4_REDIST_LIST (u4Context),
                                            &pLinkNode->TSNext);
            pRtProfile = NULL;
            if (pLinkNode != NULL)
            {
                pRtProfile = pLinkNode->pRouteProfile;
            }

        }
        else
        {
            if (Bgp4RcvdPADBGetNextBestRoute (pRtProfile, &pRtProfile) ==
                BGP4_FAILURE)
            {
                break;
            }
        }

    }
    TMO_SLL_Scan (&TsRouteList, pLinkNode, tLinkNode *)
    {
        pVpnRtProfile = pLinkNode->pRouteProfile;
        BGP4_RT_SET_FLAG (pVpnRtProfile, BGP4_RT_IN_REDIST_LIST);
        Bgp4DshAddRouteToList (BGP4_REDIST_LIST (u4Context), pVpnRtProfile, 0);
        KW_FALSEPOSITIVE_FIX1 (pVpnRtProfile);
    }

    Bgp4DshReleaseList (&TsRouteList, 0);

    return;
}

/****************************************************************************/
/* Function Name : Bgp4Vpnv4GetLeakRoutesFromPeer                           */
/* Description   : This Function is invoked during whenever address-family  */
/*                 IPv4 is configured for user VRFs.The routine gets the    */
/*                 routes based on the route targets(RT) imported by the    */
/*                 newly added VRF.The routes are fetched from other Active */
/*                 VRFS.                                                    */
/* Input(s)      : u4VrfId - Incoming VRF id                                */
/* Output(s)     : None                                                     */
/* Return(s)     : None                                                     */
/****************************************************************************/
INT4
Bgp4Vpnv4GetLeakRoutesFromPeer (UINT4 u4VrfId)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tRouteProfile      *pVpnRtProfile;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT4               u4Context = 0;
    INT4                i4RetVal;

    MEMSET (au1ExtComm, 0, EXT_COMM_VALUE_LEN);
    if (TMO_SLL_Count (BGP4_VPN4_VRF_SPEC_IMPORT_TARGETS (u4VrfId)) > 0)
    {
        /* New Customer VRF has been configured,If the route are already
         * available in the RIB, then the routes will not be again                    
         * programmed, if not, the new route will be added to the RIB */
        i4RetVal = BgpGetFirstActiveContextID (&u4Context);

        if (u4Context == BGP4_DFLT_VRFID)
        {
            i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
        }

        while (i4RetVal == SNMP_SUCCESS)
        {
            if (u4Context == u4VrfId)
            {
                /* If incoming vrf id ,continue */
                i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);

                continue;
            }
            if ((BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4Context) !=
                 BGP4_L3VPN_VRF_UP))
            {
                i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
                continue;
            }
            Bgp4RcvdPADBGetFirstBestRoute (u4Context, &pVpnRtProfile,
                                           BGP4_INET_AFI_IPV4,
                                           BGP4_INET_SAFI_VPNV4_UNICAST);

            while (pVpnRtProfile != NULL)
            {
                if (Bgp4Vpnv4VrftoVrfRouteLeak (pVpnRtProfile) == BGP4_FAILURE)
                {
                    /* Theres something wrong in linking VRF informations
                     * into route profile. hence return failure
                     */
                    if (Bgp4RcvdPADBGetNextBestRoute
                        (pVpnRtProfile, &pVpnRtProfile) == BGP4_FAILURE)
                    {
                        break;
                    }
                    continue;
                }
                else
                {
                    pPeerEntry = BGP4_RT_PEER_ENTRY (pVpnRtProfile);
                    /*New Snippet */
                    if (BGP4_RT_PROTOCOL (pVpnRtProfile) == BGP_ID)
                    {
                        Bgp4Vpnv4ProcessFeasibleRoute
                            (pVpnRtProfile, pPeerEntry, u4VrfId);
                    }
                    else
                    {
                        Bgp4Vpnv4ProcessNonBgpFeasRoute (pVpnRtProfile,
                                                         u4VrfId);
                    }
                }

                if (Bgp4RcvdPADBGetNextBestRoute (pVpnRtProfile, &pVpnRtProfile)
                    == BGP4_FAILURE)
                {
                    break;
                }

            }                    /* End of While loop */

            i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);

        }                        /* End of while loop */
    }
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name : Bgp4Vpnv4VrftoVrfRouteLeak                               */
/* Description   : This function would leak the route between the two       */
/*                 different VRFS.It would check whether the export route   */
/*                 targets of installed VRF match with import targets of    */
/*                 other VRFS. If so then attach those import route targets */
/*                 to the route so that the route will be installed in both */
/*                 VRFs.                                                    */
/* Input(s)      : pVpnRtProfile - route profile information                */
/* Output(s)     : None                                                     */
/* Return(s)     : BGP4_FAILURE/BGP4_SUCCESS                                */
/****************************************************************************/
INT4
Bgp4Vpnv4VrftoVrfRouteLeak (tRouteProfile * pVpnRtProfile)
{
    tExtCommProfile    *pExtCom = NULL;
    UINT2               u2EcommType;
    INT4                i4RetStatus = 0;

    /* Needs to add the route to REDISTRIBUTION update list. */

    /* If CE did not send any route-targets, then check whether export
     * targets of installed VRF match with import targets of other VRFs,
     *       CE1---\
     *              |---PE------
     *       CE2---/
     * If so, then attach those import route targets to the route so
     * that the route will be installed in both the VRFs
     */
    TMO_SLL_Scan (BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS
                  (BGP4_RT_CXT_ID (pVpnRtProfile)), pExtCom, tExtCommProfile *)
    {
        PTR_FETCH2 (u2EcommType, pExtCom->au1ExtComm);
        if (BGP4_IS_RT_EXT_COMMUNITY (u2EcommType) == BGP4_TRUE)
        {
            i4RetStatus =
                Bgp4Vpnv4LinkMatchingImportRTVrfs (pExtCom->au1ExtComm,
                                                   pVpnRtProfile);
            if (i4RetStatus == BGP4_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tVPN:Bgp4Vpnv4LinkMatchingImportRTVrfs Failed \n");
                return BGP4_FAILURE;
            }
        }
    }

    return i4RetStatus;
}

/****************************************************************************/
/* Function Name : Bgp4Vpnv4DelLeakRoutesFromPeer                           */
/* Description   : This Function is invoked whenever RT is altered in the   */
/*                 the Active VRFs.The routine gets the routes based on the */
/*                 route targets(RT) imported by the Active VRFs and deletes*/
/*                 the routes imported from other Active VRFs.              */
/* Input(s)      : u4VrfId - Incoming VRF id                                */
/* Output(s)     : None                                                     */
/* Return(s)     : None                                                     */
/****************************************************************************/
VOID
Bgp4Vpnv4DelLeakRoutesFromPeer (UINT4 u4VrfIdIn)
{
    UINT4               u4Context = 0;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pNextRt = NULL;
    tRouteProfile      *pNextRoute = NULL;
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4Ret;
    INT4                i4RetVal;

    i4RetVal = BgpGetFirstActiveContextID (&u4Context);
    if (u4Context == 0)
    {
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
    }
    while (i4RetVal == SNMP_SUCCESS)
    {
        if (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4Context) == BGP4_L3VPN_VRF_UP)
        {
            pRibNode = BGP4_VPN4_RIB_TREE (u4Context);

            if (Bgp4RibhGetFirstEntry (CAP_MP_VPN4_UNICAST, &pRtProfile,
                                       &pRibNode, TRUE) == BGP4_FAILURE)
            {
                return;
            }
            for (;;)
            {
                i4Ret = Bgp4RibhGetNextEntry (pRtProfile, u4Context,
                                              &pNextRoute, &pRibNode, TRUE);
                pRtVrfInfo = NULL;
                pNextRt = pRtProfile;
                while (pNextRt != NULL)
                {
                    pRtProfile = pNextRt;
                    Bgp4Vpnv4GetInstallVrfInfo (pRtProfile, u4Context,
                                                &pRtVrfInfo);

                    if (pRtVrfInfo == NULL)
                    {
                        BGP4_RT_GET_NEXT (pRtProfile, u4Context, pNextRt);
                        continue;
                    }

                    if (u4VrfIdIn == u4Context)
                    {
                        /*If incoming VRF id ,continue */
                        BGP4_RT_GET_NEXT (pRtProfile, u4Context, pNextRt);
                        continue;
                    }
                    if ((BGP4_RT_PEER_ENTRY (pRtProfile) != NULL) &&
                        (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRtProfile))
                         == BGP4_VPN4_PE_PEER))
                    {
                        BGP4_RT_GET_NEXT (pRtProfile, u4Context, pNextRt);
                        continue;
                    }
                    Bgp4Vpnv4ProcessNonBgpWithdRoute (pRtProfile, u4VrfIdIn);

                    BGP4_RT_GET_NEXT (pRtProfile, u4Context, pNextRt);
                }

                if (i4Ret == BGP4_FAILURE)
                {
                    break;
                }

                pRtProfile = pNextRoute;
                pNextRoute = NULL;
            }
        }
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
    }
}

#if 0
/****************************************************************************/
/* Function Name : Bgp4L3vpnProcessLabelAllocationPolicyChange              */
/* Description   : This function is will reprogram the VPN4 routes with     */
/*                 new labels based on the label allocation policy          */
/*                   configured                                             */
/*                 not available, get the IPV4 routes for the VRF, convert  */
/*                 them into VPNV4 routes and send the routes to PE peers   */
/* Input(s)      : u4Context  - VRF number                                  */
/* Output(s)     : None                                                     */
/* Return(s)     : None                                                     */
/****************************************************************************/
VOID
Bgp4Vpnv4ProcessLabelAllocationPolicyChange (VOID)
{
    UINT4               u4Context = 0;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pNextRt = NULL;
    tRouteProfile      *pNextRoute = NULL;
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4Ret;
    INT4                i4RetVal;

    i4RetVal = BgpGetFirstActiveContextID (&u4Context);
    if (u4Context == 0)
    {
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
    }
    while (i4RetVal == SNMP_SUCCESS)
    {
        /* Delete All Local VPNV4 routes from RIB and send withdraw message
           for those VPNV4 routes
         */
        if (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4Context) == BGP4_L3VPN_VRF_UP)
        {
            pRibNode = BGP4_VPN4_RIB_TREE (u4Context);

            if (Bgp4RibhGetFirstEntry (CAP_MP_VPN4_UNICAST, &pRtProfile,
                                       &pRibNode, TRUE) == BGP4_FAILURE)
            {
                return;
            }
            for (;;)
            {
                i4Ret = Bgp4RibhGetNextEntry (pRtProfile, u4Context,
                                              &pNextRoute, &pRibNode, TRUE);
                pRtVrfInfo = NULL;
                pNextRt = pRtProfile;
                while (pNextRt != NULL)
                {
                    pRtProfile = pNextRt;
                    Bgp4Vpnv4GetInstallVrfInfo (pRtProfile, u4Context,
                                                &pRtVrfInfo);

                    if (pRtVrfInfo == NULL)
                    {
                        BGP4_RT_GET_NEXT (pRtProfile, u4Context, pNextRt);
                        continue;
                    }

                    if ((BGP4_RT_PEER_ENTRY (pRtProfile) != NULL) &&
                        (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRtProfile))
                         == BGP4_VPN4_PE_PEER))
                    {
                        BGP4_RT_GET_NEXT (pRtProfile, u4Context, pNextRt);
                        continue;
                    }

                    BGP4_RT_REF_COUNT (pRtProfile)++;

                    /* Send Withdraw message to the peer */
                    if ((BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo) &
                         BGP4_RT_BGP_VRF_BEST))
                    {
                        BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);
                        Bgp4AddWithdRouteToIntPeerAdvtList (pRtProfile);
                    }
                    BGP4_RT_GET_NEXT (pRtProfile, u4Context, pNextRt);
                    /* Remove the routes from RIB */
                    Bgp4RibhDelRtEntry (pRtProfile,
                                        pRibNode, u4Context, BGP4_TRUE);
                    TMO_SLL_Delete (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile),
                                    (tTMO_SLL_NODE *) pRtVrfInfo);
                    Bgp4MemReleaseVpnRtInstallVrfNode (pRtVrfInfo);
                    pRtVrfInfo = NULL;

                    Bgp4DshReleaseRtInfo (pRtProfile);
                }

                if (i4Ret == BGP4_FAILURE)
                {
                    break;
                }

                pRtProfile = pNextRoute;
                pNextRoute = NULL;
            }

            /* Create VPNV4 route profile with new label and send update
               message for those routes
             */
            Bgp4Vpnv4SendVpnv4RtsToPeer (u4Context);
        }
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
    }
}

/****************************************************************************/
/* Function Name :  Bgp4Vpnv4GetRoutesFromPeer                              */
/* Description   : This Function is invoked during                          */
/*                 GET_VPNV4_ROUTE_FROM_PEER_EVENT, whenever address-family */
/*                 IPv4 is configured for user VRFs.The routine gets the    */
/*                 routes based on the route targets(RT) imported by the    */
/*                 newly added VRF.The routes are fetched from              */
/*                 (a) From the default context, if the same routes have    */
/*                     already been imported by any other user VRF.         */
/*                 (b) If not present in default context, then through      */
/*                     route refresh                                        */
/* Input(s)      : pu1Msg - Message from Set Routine                        */
/* Output(s)     : None                                                     */
/* Return(s)     : None                                                     */
/****************************************************************************/

INT4
Bgp4Vpnv4GetRoutesFromPeer (UINT4 u4VrfId)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tAddrPrefix         RemAddr;
    tRouteProfile      *pVpnRtProfile;
    tBgp4Info          *pBgp4Info;
    INT4                i4RetStatus;
    UINT4               u4Index = 0;
    UINT2               u2EcommType;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];

    MEMSET (au1ExtComm, 0, EXT_COMM_VALUE_LEN);
    if (TMO_SLL_Count (BGP4_VPN4_VRF_SPEC_IMPORT_TARGETS (u4VrfId)) > 0)
    {

        /* New Customer VRF has been configured, Trigger Route Refresh to get
         * routes from Peer. If the route are already available in the RIB, then
         * the routes will not be again programmed, if not, the new route will be
         * added to the RIB */
        Bgp4InitAddrPrefixStruct (&(RemAddr), BGP4_INET_AFI_IPV4);
        Bgp4RtRefRequestHandler (BGP4_DFLT_VRFID, RemAddr, BGP4_INET_AFI_IPV4,
                                 BGP4_INET_SAFI_VPNV4_UNICAST,
                                 BGP4_RR_ADVT_PE_PEERS);

        /* If the routes are correponding to the RT values imported by the
         * customer VRF, then routes are fetched directly from the Default
         * Context */
        Bgp4RcvdPADBGetFirstBestRoute (BGP4_DFLT_VRFID, &pVpnRtProfile,
                                       BGP4_INET_AFI_IPV4,
                                       BGP4_INET_SAFI_VPNV4_UNICAST);
        while (pVpnRtProfile != NULL)
        {
            pBgp4Info = BGP4_RT_BGP_INFO (pVpnRtProfile);
            if (BGP4_INFO_ECOMM_ATTR (pBgp4Info) != NULL)
            {
                for (u4Index = 0; u4Index < BGP4_INFO_ECOMM_COUNT (pBgp4Info);
                     u4Index++)
                {
                    MEMCPY ((UINT1 *) au1ExtComm,
                            (UINT1 *) (BGP4_INFO_ECOMM_ATTR_VAL (pBgp4Info) +
                                       +(u4Index * EXT_COMM_VALUE_LEN)),
                            EXT_COMM_VALUE_LEN);
                    PTR_FETCH2 (u2EcommType, au1ExtComm);
                    if (BGP4_IS_RT_EXT_COMMUNITY (u2EcommType) == BGP4_TRUE)
                    {
                        /* This function call will link the new Customer VRF
                         * in the Installed VRF list of the Routeprofile, though
                         * attempts will be made by this function to link all the
                         * VRFs to the profile, if the VRFs are already there in
                         * the installed list, then it will not be added again.*/
                        i4RetStatus =
                            Bgp4Vpnv4LinkMatchingImportRTVrfs (au1ExtComm,
                                                               pVpnRtProfile);
                        if (i4RetStatus == BGP4_FAILURE)
                        {
                            /* Theres something wrong in linking VRF informations
                             * into route profile. hence return failure
                             */
                            return BGP4_FAILURE;
                        }
                        else
                        {
                            pPeerEntry = BGP4_RT_PEER_ENTRY (pVpnRtProfile);
                            /*New Snippet */
                            Bgp4Vpnv4ProcessFeasibleRoute
                                (pVpnRtProfile, pPeerEntry, u4VrfId);
                        }
                    }
                }
            }
            if (Bgp4RcvdPADBGetNextBestRoute (pVpnRtProfile, &pVpnRtProfile)
                == BGP4_FAILURE)
            {
                break;
            }
        }
    }
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name : Bgp4ApplyVpnLabelAllocPolicyToVrfs                       */
/* Description   : This function is called to copy the label allocation     */
/*                 of Default Context into each of the VRFs in BGP whenever */
/*                 the Label Allocation policy is altered.                  */
/* Input(s)      : u1bgp4VpnLabelAllocPolicy - Label Allocation policy of   */
/*                 default VRF.                                             */
/* Output(s)     : None                                                     */
/* Return(s)     : None                                                     */
/****************************************************************************/
VOID
Bgp4ApplyVpnLabelAllocPolicyToVrfs (UINT1 u1bgp4VpnLabelAllocPolicy)
{
    UINT4               u4ActiveContext = 0;

    for (u4ActiveContext = 0;
         u4ActiveContext <
         FsBGPSizingParams[MAX_BGP_CXT_NODES_SIZING_ID].u4PreAllocatedUnits;
         u4ActiveContext++)
    {
        if (NULL == gBgpCxtNode[u4ActiveContext])
        {
            continue;
        }
        BGP4_LABEL_ALLOC_POLICY (u4ActiveContext) = u1bgp4VpnLabelAllocPolicy;
    }
}
#endif
#endif

#endif /* BGVPNFIL_C */
