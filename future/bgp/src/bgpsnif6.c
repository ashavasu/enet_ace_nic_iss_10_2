/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgpsnif6.c,v 1.5 2013/02/07 12:17:04 siva Exp $
 *
 * Description: Contains BGP ipv6 support routines
 *
 *******************************************************************/
#ifndef BGPSNIF6_C
#define BGPSNIF6_C

#ifdef BGP4_IPV6_WANTED
# include  "snmccons.h"
# include  "snmcdefn.h"
# include  "bgp4com.h"

INT4
Bgp4Ipv6GetFirstPeerPathAttrTableIndex (tNetAddress * pRoutePrefix,
                                        tAddrPrefix * pPeerAddress,
                                        tBgp4PeerEntry * pPeer)
{
    tRouteProfile      *pRtProfile = NULL;
    tBgp4Info          *pRtInfo = NULL;

    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
    {
        if (bgp4RibLock () != OSIX_SUCCESS)
        {
            return BGP4_FAILURE;
        }

        /* Search for the IPV6 routes in RIB */
        pRtProfile = Bgp4RibhGetFirstAsafiPeerRtEntry (pPeer,
                                                       CAP_MP_IPV6_UNICAST);
        for (;;)
        {
            if ((pRtProfile == NULL) ||
                ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_HISTORY) !=
                 BGP4_RT_HISTORY))
            {
                break;
            }

            /* Route is a History route. This can never be a Index. Get the
             * next route from the peer */
            pRtProfile = Bgp4RibhGetNextIpv6PeerRtEntry (pRtProfile, pPeer);
        }

        if (pRtProfile != NULL)
        {
            pRtInfo = BGP4_RT_BGP_INFO (pRtProfile);
            if (pRtInfo != NULL)
            {
                Bgp4CopyNetAddressStruct (pRoutePrefix,
                                          BGP4_RT_NET_ADDRESS_INFO
                                          (pRtProfile));
                Bgp4CopyAddrPrefixStruct (pPeerAddress,
                                          BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
                bgp4RibUnlock ();
                return BGP4_SUCCESS;
            }
        }
        bgp4RibUnlock ();
    }

    return BGP4_FAILURE;
}

INT4
Bgp4Ipv6GetNextIpv6PathAttrTableIndex (UINT4 u4Context,
                                       tNetAddress RoutePrefix,
                                       tAddrPrefix PeerAddress,
                                       tNetAddress * pNextRoutePrefix,
                                       tAddrPrefix * pNextPeerAddress)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tBgp4PeerEntry     *pTmpPeer = NULL;
    tRouteProfile      *pFndRtProfile = NULL;
    tRouteProfile       RtProfile;
    tBgp4Info           RtInfo;
    UINT1               u1PeerFound = BGP4_FALSE;
    UINT1               u1MatchFound = BGP4_FALSE;

    Bgp4InitBgp4info (&RtInfo);

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
    {
        if ((PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pPeer), PeerAddress))
            == BGP4_TRUE)
        {
            u1PeerFound = BGP4_TRUE;
            break;
        }
    }
    if (u1PeerFound == BGP4_FALSE)
    {
        return BGP4_FAILURE;
    }

    if (pPeer == NULL)
    {
        return BGP4_FAILURE;
    }
    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
    {
        Bgp4CopyNetAddressStruct (&(RtProfile.NetAddress), RoutePrefix);
        BGP4_RT_PEER_ENTRY ((&RtProfile)) = pPeer;
        BGP4_RT_BGP4_INF ((&RtProfile)) = &RtInfo;
        if (bgp4RibLock () != OSIX_SUCCESS)
        {
            return BGP4_FAILURE;
        }
        pFndRtProfile = Bgp4RibhGetNextIpv6PeerRtEntry (&RtProfile, pPeer);
        bgp4RibUnlock ();
        if (pFndRtProfile == NULL)
        {
            /* Get the next peer from peer list */
            pTmpPeer =
                (tBgp4PeerEntry *)
                TMO_SLL_Next (BGP4_PEERENTRY_HEAD (u4Context),
                              (tTMO_SLL_NODE *) pPeer);
            u1MatchFound =
                Bgp4GetNextIpv6RouteProfile (pNextRoutePrefix, pNextPeerAddress,
                                             pTmpPeer);
            if (u1MatchFound == BGP4_TRUE)
            {
                return BGP4_SUCCESS;
            }
            else
            {
                return BGP4_FAILURE;
            }
        }
        else
        {
            Bgp4CopyNetAddressStruct (pNextRoutePrefix,
                                      BGP4_RT_NET_ADDRESS_INFO (pFndRtProfile));
            Bgp4CopyAddrPrefixStruct (pNextPeerAddress,
                                      BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
            return BGP4_SUCCESS;
        }
    }
    return BGP4_FAILURE;
}

INT4
Bgp4Ipv6GetFirstFsbgp4ImportTableIndex (tNetAddress * pRoutePrefix,
                                        INT4 *pi4Protocol,
                                        tAddrPrefix * pNexthop,
                                        INT4 *pi4IfIndex, INT4 *pi4Metric)
{
    VOID               *pRibNode = NULL;
    tRouteProfile      *pFndRtProfile = NULL;
    tBgp4Info          *pBgp4Info = NULL;

    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return BGP4_FAILURE;
    }
    Bgp4GetRibNode (gpBgpCurrCxtNode->u4ContextId, CAP_MP_IPV6_UNICAST,
                    &pRibNode);
    if (pRibNode == NULL)
    {
        bgp4RibUnlock ();
        return BGP4_FAILURE;
    }
    pFndRtProfile = Bgp4RibhGetFirstImportAsafiRtEntry (CAP_MP_IPV6_UNICAST,
                                                        &pRibNode);
    if (pFndRtProfile == NULL)
    {
        bgp4RibUnlock ();
        return BGP4_FAILURE;
    }
    else
    {
        Bgp4CopyNetAddressStruct (pRoutePrefix,
                                  BGP4_RT_NET_ADDRESS_INFO (pFndRtProfile));
        *pi4Protocol = BGP4_RT_PROTOCOL (pFndRtProfile);
        pBgp4Info = BGP4_RT_BGP_INFO (pFndRtProfile);
        Bgp4CopyAddrPrefixStruct (pNexthop, BGP4_INFO_NEXTHOP_INFO (pBgp4Info));
        *pi4IfIndex = BGP4_RT_GET_RT_IF_INDEX (pFndRtProfile) +
            BGP4_INCREMENT_BY_ONE;
        *pi4Metric = (INT4) BGP4_INFO_RCVD_MED (pBgp4Info);
    }

    bgp4RibUnlock ();
    return BGP4_SUCCESS;
}

INT4
Bgp4Ipv6GetFirstIndexBgpPeerTable (UINT4 u4Context, tAddrPrefix * pPeerAddress)
{
    tBgp4PeerEntry     *pPeer = NULL;

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
    {
        if (((Bgp4IsValidAddress (BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                                  BGP4_FALSE)) == BGP4_TRUE) &&
            (BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_PEER_REMOTE_ADDR_INFO (pPeer)))
            == BGP4_INET_AFI_IPV6)
        {
            Bgp4CopyAddrPrefixStruct (pPeerAddress,
                                      BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
            return BGP4_SUCCESS;
        }
    }
    return BGP4_FAILURE;
}

INT4
Bgp4Ipv6GetNextIndexBgpPeerTable (UINT4 u4Context, tAddrPrefix PeerAddress,
                                  tAddrPrefix * pNextPeerAddress)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    UINT1               u1PeerFound = BGP4_FALSE;

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeerEntry, tBgp4PeerEntry *)
    {
        if (u1PeerFound == BGP4_TRUE)
        {
            if ((PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry),
                              PeerAddress) != BGP4_TRUE) &&
                (BGP4_AFI_IN_ADDR_PREFIX_INFO
                 (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)) ==
                 BGP4_INET_AFI_IPV6))
            {
                break;
            }
        }
        else if (PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry),
                              PeerAddress) == BGP4_TRUE)
        {
            u1PeerFound = BGP4_TRUE;
        }
    }

    if (pPeerEntry == NULL)
    {
        /* Next entry not present */
        return BGP4_FAILURE;
    }

    Bgp4CopyAddrPrefixStruct (pNextPeerAddress,
                              BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));
    return BGP4_SUCCESS;
}

#endif /* BGP4_IPV6_WANTED */
#endif /* BGPSNIF6_C */
