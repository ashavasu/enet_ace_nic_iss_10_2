/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgrfdfrt.c,v 1.16 2017/09/15 06:19:54 siva Exp $
 *
 * Description: This file contains Route Flap Dampening feature
 *              functions.
 *
 *******************************************************************/
#ifndef _RFDRTSHNDLR_C
#define _RFDRTSHNDLR_C
#endif
#ifdef RFD_WANTED
#include "bgp4com.h"
/*******************************************************************************
     Funtion Name     : RfdRoutesClassifier
     Description      : This module classifies routes into fresh, withdrawn, 
                        readvertised and replacement routes. This module 
                        fetches route damping history. If route doesn't have 
                        damping history, it is classified as fresh route. 
                        If received route is unreachable, it is classified 
                        as withdrawn route. If route is unfeasible in damping 
                        history and now received as feasible, then it is 
                        classified as readvertised route. If route doesn't 
                        come under above classes, it is replacement route.
     Input(s)         : pPeerInfo     - this is  a pointer to peer information
                                        from which the route have been received.
                        pRtInfo       - pointer to the route profile that needs
                                        to be processed.
                        pOldFeasRoute - pointer to the Current feasible route
                                        profile that is present in the RIB.
                                        If no route is present then this should
                                        be NULL. In case of Withdrawn route,
                                        this should be NULL always.
                        u4IsFeasRoute - Flag indicating whether the route is
                                        feasible route or withdrawn route.
                                        If BGP4_TRUE, then route is feasible,
                                        else if BGP4_FALSE, route is
                                        unfeasible route.
                        pRibNode      - Pointer to the Treenode in which the
                                        route is present. If this is NULL, then
                                        the route is no matching route is
                                        present in the RIB.
     Output(s)        : pRtInfo - Flag updated if the route is suppressed
     Global Variables
     Referred         : None                
     Global Variables
     Modified         : None
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None

     Return(s)        : RFD_SUCCESS/RFD_FAILURE
******************************************************************************/
INT4
RfdRoutesClassifier (tBgp4PeerEntry * pPeerInfo, tRouteProfile * pRtInfo,
                     tRouteProfile * pOldFeasRoute, UINT4 u4IsFeasRoute,
                     VOID *pRibNode)
{
    tRtDampHist        *pRtDampHist = NULL;
    INT4                i4RetSts;
    UINT1               u1PeerState = 0;
    UINT1               u1NewDamp = 0;

    if (BGP4_PEER_STATE (pPeerInfo) != BGP4_ESTABLISHED_STATE)
    {
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_DAMP_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Received Route %s for Route flap Damping "
                       "classification in Invalid Peer State\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtInfo),
                                        BGP4_RT_AFI_INFO (pRtInfo)));
        return BGP4_SUCCESS;
    }

    if (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerInfo), pPeerInfo) ==
        BGP4_INTERNAL_PEER)
    {
        /* Peer type is Internal. No need to apply Route flap Damping. */
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_DAMP_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Peer type is internal. Route flap Damping"
                       "is not applied for the route %s\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtInfo),
                                        BGP4_RT_AFI_INFO (pRtInfo)));
        return RFD_SUCCESS;
    }

    /* Get the peer status */
    i4RetSts = RfdGetPeerState (pPeerInfo, &u1PeerState);
    if (i4RetSts == RFD_FAILURE)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Getting Damping peer status failed.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))));
        return RFD_FAILURE;
    }

    if (u4IsFeasRoute == BGP4_TRUE)
    {
        /* Route is feasible route. Fetch route damping history */
        /* if peer is suppressed, store all fresh routes in SupPeerRtsList */
        if (u1PeerState == RFD_SUPPRESSED_STATE)
        {
            /* Peer is DAMPED. Route also will be damped. */
            BGP4_RT_SET_FLAG (pRtInfo, BGP4_RT_DAMPED);
            RfdWRHUpdateCreateRtDampHist (pRtInfo, BGP4_FALSE, &pRtDampHist,
                                          &u1NewDamp);
            BGP4_RT_DAMP_HIST (pRtInfo) = pRtDampHist;
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG,
                           BGP4_DAMP_TRC | BGP4_CONTROL_PATH_TRC |
                           BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                           "\tPEER %s - Peer is damped. So route %s also will be damped\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtInfo),
                                            BGP4_RT_AFI_INFO (pRtInfo)));
            return RFD_SUCCESS;
        }

        /* Fetch the Damping History for the route if it exist. */
        pRtDampHist = (pOldFeasRoute != NULL) ?
            BGP4_RT_DAMP_HIST (pOldFeasRoute) : NULL;

        /* If route doesn't have history, it is considered as Fresh route */
        if (pRtDampHist == NULL)
        {
            /* Check if the route already exists in RIB. If it exists, this
             * event should be processed for "route changes" - replacement
             * route processing
             */
            if (pOldFeasRoute == NULL)
            {
                /* Route is not present in BGP RIB. Route is fresh
                 * feasible route. */
                BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                               BGP4_TRC_FLAG,
                               BGP4_DAMP_TRC | BGP4_CONTROL_PATH_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s - Route %s is not already present in BGP RIB."
                               " It is a new feasible route. So no damping history\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerInfo))),
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtInfo),
                                                BGP4_RT_AFI_INFO (pRtInfo)));
                return RFD_SUCCESS;
            }
            else
            {
                if (RFD_IS_RTS_MATCH (pRtInfo, pOldFeasRoute) == BGP4_TRUE)
                {
                    /*Duplicate Routes */
                    return RFD_SUCCESS;
                }
                else
                {
                    /* Route is a replacement route and there is not
                     * Damping history existing earlier. So create
                     * a new damping history */
                    i4RetSts = RfdReplacementRoutesHandler (pRtInfo,
                                                            pRtDampHist);
                    if (i4RetSts == RFD_FAILURE)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                                  BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                                  "\tDAMPING : Processing REPLACEMENT "
                                  "ROUTES FAILED !!!\n");
                    }
                    return i4RetSts;
                }
            }
        }
        else
        {
            /* Check whether the route is HISTORY or not.
             * If HISTORY route, then this route should be treated as
             * RE-ADVERTISED route. If route is not HISTORY route then
             * this route should be processed for "route changes" -
             * replacement route processing.
             */
            if (pRtDampHist->u1RtFlag == RFD_UNFEASIBLE_ROUTE)
            {
                /* Previously route is withdrawn and now it is received
                 * as feasible, so it is considered as re-advertised route
                 */
                RfdReadvertisedRoutesHandler (pRtInfo, pPeerInfo, pRtDampHist);
            }
            else
            {
                /* Replacement route processing. Route is present in RIB and
                 * in Damp History. First Process the route for Replacement
                 * for updating the FOM value and then process the route
                 * as Re-advertised route. */

                /*if((BGP4_RT_GET_FLAGS (pRtInfo)& BGP4_RT_REPLACEMENT)== BGP4_RT_REPLACEMENT)
                   { */
                RfdReplacementRoutesHandler (pRtInfo, pRtDampHist);
                /* } */
                RfdReadvertisedRoutesHandler (pRtInfo, pPeerInfo, pRtDampHist);
            }
        }
    }
    else
    {
        /* Process withdrawn Routes */
        if (u1PeerState == RFD_SUPPRESSED_STATE)
        {
            /* Peer itself is suppresse. No route from this peer is present.
             * Set the incoming route's flag as BGP4_RT_DAMPED to
             * indiacate that this route was DAMPED already. */
            BGP4_RT_SET_FLAG (pRtInfo, BGP4_RT_DAMPED);
            return RFD_SUCCESS;
        }

        /* Peer is not suppressed. Handle the withdrawn route. */
        i4RetSts = RfdWithdrawnRoutesHandler (pRtInfo, pRibNode);
        if (i4RetSts == RFD_FAILURE)
        {
            BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                      BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                      BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tDAMPING : Processing WITHDRAWN ROUTES FAILED !!!\n");
            return RFD_FAILURE;
        }
    }
    return RFD_SUCCESS;
}

/****************************************************************************
     Function Name    : RfdReadvertisedRoutesHandler
     Description      : If there is no damping structure for the route, the
                        Damping structure won't be created. If the damping
                        structure is present, the damping structure will be
                        updated.  If the figure of merit goes above ceiling,
                        it is adjusted to ceiling value. If the route is not 
                        suppressed and the FOM crosses cutoff, the route will 
                        be suppressed.If the route is suppressed and the FOM 
                        goes below Reuse threshold, it will be reused and it 
                        should be deleted from reuse list.Else use this route.
     Input(s)         : pRtInfo - Pointer to the route profile
                        PeerInfo -Pointer to peer information from whom the 
                            readvertised routes are received.
                        pDampHist - Pointer to the corresponding Damping
                                    history structure.
     Output(s)        : None
     Global Variables
     Referred         : None 
     Global Variables
     Modified         : None  
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
****************************************************************************/
INT4
RfdReadvertisedRoutesHandler (tRouteProfile * pRtInfo,
                              tBgp4PeerEntry * pPeerInfo,
                              tRtDampHist * pDampHist)
{
    tRtDampHist        *pRtDampHist = NULL;
    RFDFLOAT            RtFom;
    UINT1               u1RtState = RFD_USABLE;
    UINT1               u1IsDampHistFreed = RFD_FALSE;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (pPeerInfo);

    /* Update the Local Damp History Pointer and the current 
     * FOM value. */
    pRtDampHist = pDampHist;
    RtFom = pRtDampHist->RtFom;

    /* Update route FOM */
    RfdRARDampHistHandler (pRtInfo, pRtDampHist, &u1RtState,
                           &u1IsDampHistFreed);
    if (u1RtState == RFD_SUPPRESS)
    {
        /* Route is suppressed. */
        BGP4_TRC_ARG4 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_DAMP_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tDAMPING : Route -- Dest = %s, Mask = %s, "
                       "from Peer = %s is DAMPED & FOM is %d.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtInfo),
                                        BGP4_RT_AFI_INFO (pRtInfo)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtInfo),
                                        BGP4_RT_AFI_INFO (pRtInfo)),
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))),
                       ((UINT4) BGP4_CEIL (RtFom)));
    }

    if (u1RtState == RFD_REUSE)
    {
        /* If route becomes reusable, remove this route from 
         * routes reuse list */
        RfdRemoveRtFromRtsReuseList (pRtInfo, pRtDampHist);
    }
    else if ((RtFom != pRtDampHist->RtFom) || (u1IsDampHistFreed == RFD_TRUE))
    {
        /* Remove route from routes reuse list */
        RfdRemoveRtFromRtsReuseList (pRtInfo, pRtDampHist);
        if (u1IsDampHistFreed == RFD_TRUE)
        {
            /*Damping history is disposed bcos the FOM becomes zero */
            BGP4_RT_DAMP_HIST (pRtDampHist->pRtProfile) = NULL;
            Bgp4DshReleaseRtInfo (pRtDampHist->pRtProfile);
            Bgp4MemReleaseRfdDampHist (pRtDampHist);
        }
        else
        {
            /* insert route in routes reuse list */
            RfdInsertRtInRtsReuseList (pRtDampHist);
        }
    }

    return RFD_SUCCESS;
}

/***************************************************************************
     Function Name    : RfdReplacementRoutesHandler
     Description      : This routine gives the routes to run the decision
                        process. It gets the new feasible routes and new
                        withdrawn routes after decsion process. This module
                        applies the damping for withdrawn routes.
     Input(s)         : pRtInfo - Pointer to a route profile 
                        pDampHist - Pointer to the corresponding Damping
                                    history structure.
     Output(s)        : None
     Global Variables
     Referred         : None  
     Global Variables
     Modified         : None 
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
******************************************************************************/
INT4
RfdReplacementRoutesHandler (tRouteProfile * pRtInfo, tRtDampHist * pDampHist)
{
    tRtDampHist        *pRtDampHist = NULL;
    INT4                i4RetSts;
    UINT1               u1IsNewDampHist;

    /* Update the Local Damp Hist pointer with the Input value. */
    pRtDampHist = pDampHist;

    /*Update the damping history for withdrawn routes */
    i4RetSts = RfdWRHDampHistHandler (pRtInfo, BGP4_FALSE, &pRtDampHist,
                                      &u1IsNewDampHist);
    if (i4RetSts == RFD_FAILURE)
    {
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pRtInfo->pPEPeer)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tDAMPING : UPDATING ROUTES [%s/%d] DAMPING HISTORY "
                       "FAILED !!!\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtInfo),
                                        BGP4_RT_AFI_INFO (pRtInfo)),
                       BGP4_RT_PREFIXLEN (pRtInfo));
        return RFD_FAILURE;
    }

    if (u1IsNewDampHist != RFD_TRUE)
    {
        /* If this route is not withdrawn first time, remove route from
         * routes reuse list */
        RfdRemoveRtFromRtsReuseList (pRtInfo, pRtDampHist);
    }

    /* Insert route in routes reuse list */
    RfdInsertRtInRtsReuseList (pRtDampHist);
    BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pRtInfo->pPEPeer)),
                   BGP4_TRC_FLAG, BGP4_DAMP_TRC |
                   BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                   "\tDAMPING : Replacement Route [%s/%d] is successfully handled."
                   " And Updation of damping history for withdrawn route is done.\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtInfo),
                                    BGP4_RT_AFI_INFO (pRtInfo)),
                   BGP4_RT_PREFIXLEN (pRtInfo));
    return RFD_SUCCESS;
}

/**************************************************************************
     Function Name    : RfdRARDampHistHandler
     Description      : This module handles damping history for re-advertised 
                        route
     Input(s)         : pRtInfo - Pointer to route information whose damping 
                           structure has to be updated.
                      : pRtDampHist - Pointer to route damping history
     Output(s)        : pu1RtState - Pointer to route state that gives whether 
                           the route is suppressed or not.
                        : pu1IsDampHistFreed -Pointer to status whether the 
                        damping history should be released or not.
     Global Variables
     Referred         :  None
     Global Variables
     Modified         :  None 
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
****************************************************************************/
INT4
RfdRARDampHistHandler (tRouteProfile * pRtInfo, tRtDampHist * pRtDampHist,
                       UINT1 *pu1RtState, UINT1 *pu1IsDampHistFreed)
{
    /* Update the Route profile info stored in the Damp History. */
    if (pRtInfo != pRtDampHist->pRtProfile)
    {
        BGP4_RT_DAMP_HIST (pRtDampHist->pRtProfile) = NULL;
        Bgp4DshReleaseRtInfo (pRtDampHist->pRtProfile);
        pRtDampHist->pRtProfile = pRtInfo;
        BGP4_RT_DAMP_HIST (pRtInfo) = pRtDampHist;
        BGP4_RT_REF_COUNT (pRtInfo)++;
    }

    /* Calaculate route FOM */
    RfdCalcRtNoPenaltyFOM (pRtDampHist);
    pRtDampHist->u1RtFlag = RFD_FEASIBLE_ROUTE;

    /* If route is not suppressed and FOM is below cutoff, the route
       can be used. If the route is suppressed and FOM comes below 
       reuse, it can be reused. Otherwise it should be suppressed */
    RfdRARProcessRouteAdvt (pRtDampHist, pu1RtState, pu1IsDampHistFreed);

    /* Depanding on the current status, update the routes Suppressed status */
    if (pRtDampHist->u1RtStatus == RFD_SUPPRESSED_STATE)
    {
        BGP4_RT_SET_FLAG (pRtInfo, BGP4_RT_DAMPED);
    }
    else
    {
        BGP4_RT_RESET_FLAG (pRtInfo, BGP4_RT_DAMPED);
    }
    return RFD_SUCCESS;
}

/****************************************************************************
     Function Name    : RfdCalcRtNoPenaltyFOM
     Description      : This module updates figure-of-merit value of 
                        re-advertised route.
     Input(s)         : pRtDampHist - Pointer to route damping history whose 
                            FOM has to be calculated.
     Output(s)        : pRtDampHist - Pointer to damping history whose damping 
                            history has been updated.
     Global Variables
     Referred         :  RFD_DECAY_ARRAY(BGP4_DFLT_VRFID) , RFD_DECAY_TIMER_GRANULARITY(BGP4_DFLT_VRFID), 
                         RFD_DECAY_ARRAY_SIZE (BGP4_DFLT_VRFID)
     Global Variables
     Modified         :  None
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
****************************************************************************/
INT4
RfdCalcRtNoPenaltyFOM (tRtDampHist * pRtDampHist)
{

    UINT4               u4CurrentTime;
    RFDFLOAT            FomTempVal;
    UINT2               u2TimeDiff;

    /*Get the system time */
    RFD_GET_SYS_TIME (&u4CurrentTime);
    /*Get the time difference between current time and last FOM updated time */
    u2TimeDiff =
        RFD_GET_TIME_DIFF_SEC (u4CurrentTime, pRtDampHist->u4RtLastTime);

    FomTempVal = ((RFDFLOAT) u2TimeDiff) /
        ((RFDFLOAT)
         RFD_DECAY_HALF_LIFE_TIME (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile)));
    FomTempVal = RFD_POW (0.5, FomTempVal);

    pRtDampHist->RtFom = ((pRtDampHist->RtFom) * FomTempVal);
    pRtDampHist->u4RtLastTime = u4CurrentTime;
    return RFD_SUCCESS;
}

/***************************************************************************
     Function Name    : RfdRARProcessRouteAdvt
     Description      : This module process damping history for readvertised 
                          routes
     Input(s)         : pRtDampHist - Pointer to route damping history whose 
                           damping structure has to be updated.
     Output(s)        : pu1RtState - Pointer to route status that gives whether 
                            the route is suppressed, usable or reusable.
                      : pu1IsDampHistFreed -Pointer to the status whether the
                          damping history should be released or not.        
     Global Variables
     Referred         : RFD_REUSE_THRESHOLD(BGP4_DFLT_VRFID)
     Global Variables
     Modified         : None
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
***************************************************************************/
INT4
RfdRARProcessRouteAdvt (tRtDampHist * pRtDampHist, UINT1 *pu1RtState,
                        UINT1 *pu1IsDampHistFreed)
{
    RFDFLOAT            ReuseTempVal;
    /* If route is not suppressed and FOM is below cutoff , the route 
     * can be used. Also reset the routes suppressed flag. */
    if ((pRtDampHist->u1RtStatus != RFD_SUPPRESSED_STATE) &&
        (pRtDampHist->RtFom <=
         RFD_CUTOFF_THRESHOLD (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile))))
    {
        *pu1RtState = RFD_USABLE;
    }
    /* If the route is suppressed and FOM goes below reuse threshold, 
     * the route can reused */
    else if ((pRtDampHist->u1RtStatus == RFD_SUPPRESSED_STATE) &&
             (pRtDampHist->RtFom <
              RFD_REUSE_THRESHOLD (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile))))
    {
        pRtDampHist->u1RtStatus = RFD_UNSUPPRESSED_STATE;
        *pu1RtState = RFD_REUSE;
        BGP4_RFD_FEAS_RTPROFILE_CNT (BGP4_RT_CXT_ID
                                     (pRtDampHist->pRtProfile))--;
    }
    /* Otherwise the route will be suppressed. Also set the 
     * route flag to reflect this state. */
    else
    {
        pRtDampHist->u1RtStatus = RFD_SUPPRESSED_STATE;
        *pu1RtState = RFD_SUPPRESS;
        /*Calculate reuse time */
        ReuseTempVal =
            RFD_NAT_LOG (RFD_REUSE_THRESHOLD
                         (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile)) /
                         pRtDampHist->RtFom);
        ReuseTempVal = (ReuseTempVal / RFD_NAT_LOG (1 / 2));
        ReuseTempVal =
            ReuseTempVal *
            RFD_DECAY_HALF_LIFE_TIME (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile));

        if (ReuseTempVal >
            RFD_MAX_HOLD_DOWN_TIME ((BGP4_RT_CXT_ID (pRtDampHist->pRtProfile))))
        {
            ReuseTempVal =
                RFD_MAX_HOLD_DOWN_TIME ((BGP4_RT_CXT_ID
                                         (pRtDampHist->pRtProfile)));
        }
        pRtDampHist->u4RtReuseTime = ReuseTempVal;

        Bgp4TmrhStopTimer (BGP4_ROUTE_REUSE_TIMER, (VOID *) pRtDampHist);
        MEMSET (&(pRtDampHist->RtReuseTmr), 0, sizeof (tBgp4AppTimer));
        pRtDampHist->RtReuseTmr.u4TimerId = BGP4_ROUTE_REUSE_TIMER;
        Bgp4TmrhStartTimer (BGP4_ROUTE_REUSE_TIMER,
                            (VOID *) pRtDampHist, ReuseTempVal);
        /*Start Reuse Timer for the route */
        BGP4_RFD_FEAS_RTPROFILE_CNT (BGP4_RT_CXT_ID
                                     (pRtDampHist->pRtProfile))++;
    }
    /* If FOM is greater than 0, Update the FOM updated time */
    if (pRtDampHist->RtFom > 0)
    {
        RFD_GET_SYS_TIME (&(pRtDampHist->u4RtLastTime));
        *pu1IsDampHistFreed = RFD_FALSE;
    }
    else
    {
        *pu1IsDampHistFreed = RFD_TRUE;
    }
    return RFD_SUCCESS;
}

/***************************** End of File ********************************/
#endif /* RFD_WANTED */
