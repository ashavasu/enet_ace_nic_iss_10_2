/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgrfdutl.c,v 1.12 2016/12/27 12:35:57 siva Exp $
 *
 * Description: This file contains Route Flap Dampening feature
 *              functions.
 *
 *******************************************************************/

#ifndef _RFDUTIL_C
#define _RFDUTIL_C
#endif
#ifdef RFD_WANTED
#include "bgp4com.h"
/*****************************************************************************
     Function Name    :  RfdFaultReportReceive
     Description      :  This module receives fault reports from all other 
                         modules in RFD and increments corresponding error 
                         statistics.
     Input(s)         :  i1FaultReport - This gives different types of errors.
     Output(s)        :  None
     Global Variables
     Referred         :  None 
     Global Variables
     Modified         : ((RFD_GLOBAL_PARAMS()).RfdErrStats)
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
*****************************************************************************/
INT4
RfdFaultReportReceive (UINT4 u4Context, INT1 i1FaultReport)
{
    switch (i1FaultReport)
    {
        case RFD_DECAY_ARRAY_MEM_ALLOC_FAIL:
            DECAY_ARRAY_MEM_ALLOC_FAIL_ERR (u4Context)++;
            break;
        case RFD_RINDEX_ARRAY_MEM_ALLOC_FAIL:
            RINDEX_ARRAY_MEM_ALLOC_FAIL_ERR (u4Context)++;
            break;
        case RFD_PEER_DAMP_HIST_DELETE_FAIL:
            PEER_DAMP_HIST_DELETE_FAIL_ERR (u4Context)++;
            break;
        case RFD_PEER_RLIST_ENTRY_NOT_FOUND:
            PEER_RLIST_ENTRY_NOT_FOUND_ERR (u4Context)++;
            break;
        case RFD_RT_RLIST_INSERT_FAIL:
            RT_RLIST_INSERT_FAIL_ERR (u4Context)++;
            break;
        case RFD_RT_DAMP_HIST_DELETE_FAIL:
            RT_DAMP_HIST_DELETE_FAIL_ERR (u4Context)++;
            break;
        case RFD_RT_RLIST_ENTRY_NOT_FOUND:
            RT_RLIST_ENTRY_NOT_FOUND_ERR (u4Context)++;
            break;
        case RFD_RT_DAMP_HIST_MEM_ALLOC_FAIL:
            RT_DAMP_HIST_MEM_ALLOC_FAIL_ERR (u4Context)++;
            break;
        case RFD_RT_NO_ALTERNATE_FOUND:
            RT_NO_ALTERNATE_FOUND_ERR (u4Context)++;
            break;
        case RFD_PEER_RLIST_INSERT_FAIL:
            PEER_RLIST_INSERT_FAIL_ERR (u4Context)++;
            break;
        case RFD_PEER_DAMP_HIST_MEM_ALLOC_FAIL:
            PEER_DAMP_HIST_MEM_ALLOC_FAIL_ERR (u4Context)++;
            break;
        default:
            return RFD_FAILURE;
    }
    return RFD_SUCCESS;
}

/****************************************************************************
     Function Name    : RfdGetPeerHashIndex
     Description      : This module calculates the hash index for the given 
                         hash keys.
     Input(s)         : u4HashKey - Hash key for which the hash index to be 
                          calculated.
     Output(s)        : pu1HashIndex -  Pointer to the Hash Index.
     Global Variables
     Referred         : None 
     Global Variables
     Modified         : None  
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
*****************************************************************************/
VOID
RfdGetPeerHashIndex (UINT4 u4HashKey, UINT1 *pu1HashIndex)
{
    UINT1               u1Count = 0;
    UINT1               u1Index;

    /* count number of 1's present in the Hashkey */

    for (u1Index = 0; u1Index < BGP4_FOUR_BYTE_BITS; u1Index++)
    {
        if ((u4HashKey & BGP4_SINGLE_BIT) == BGP4_SINGLE_BIT)
        {
            u1Count++;
        }
        u4HashKey = u4HashKey >> BGP4_SINGLE_BIT;
    }
    *pu1HashIndex = u1Count;
}
/****************************************************************************
     Function Name    : Bgp4RtReuseTimerExpiry
     Description      : This function reuses the route which is damped 
     Input(s)         : ID of the expired timer (u4TimerId),
                        Rout Damp History Information (u4Ref).
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
*****************************************************************************/
INT4 Bgp4RtReuseTimerExpiry (UINT4 u4TimerId , FS_ULONG u4Ref)
{
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tRtDampHist        *pRtDampHist = (tRtDampHist *)u4Ref;
    tRouteProfile      *pRtInfo = NULL;
    UINT1               u1IsHistFreed = BGP4_FALSE;

    UNUSED_PARAM (u4TimerId);
    pRtInfo = pRtDampHist->pRtProfile;
        Bgp4TmrhStopTimer(BGP4_ROUTE_REUSE_TIMER,(VOID *)pRtDampHist);
        if (pRtDampHist->u1RtStatus == RFD_SUPPRESSED_STATE)
        {
            BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                    BGP4_TRC_FLAG, BGP4_DAMP_TRC |
                    BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                    "\tDamped Route -- Dest = %s , Mask = %s "
                    "from Peer = %s -- becomes reusable.\n",
                    Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                        (pRtInfo),
                        BGP4_RT_AFI_INFO
                        (pRtInfo)),
                    Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                        (pRtInfo),
                        BGP4_RT_AFI_INFO
                        (pRtInfo)),
                    Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                        (BGP4_RT_PEER_ENTRY (pRtInfo)),
                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                        (BGP4_PEER_REMOTE_ADDR_INFO
                         (BGP4_RT_PEER_ENTRY
                          (pRtInfo)))));
            /* if feasible route becomes reusable, treat it as
               fresh route */
            if (pRtDampHist->u1RtFlag == RFD_FEASIBLE_ROUTE)
            {
                pPeerInfo = BGP4_RT_PEER_ENTRY (pRtInfo);

                /* Route needs to be advertised. Add the route to
                 * the peer's received route list. Alse Delete the
                 * existing route from the RIB. By Pass RFD for this
                 * process. */
                Bgp4DshDelinkRouteFromChgList (pRtInfo);
                Bgp4DshAddRouteToList (BGP4_PEER_RCVD_ROUTES (pPeerInfo),
                        pRtInfo, 0);
                BGP4_RT_SET_FLAG (pRtInfo, BGP4_RT_NO_RFD);
                Bgp4RibhDelRtEntry (pRtInfo, NULL, BGP4_RT_CXT_ID (pRtInfo),
                        BGP4_TRUE);
                /* Damping History has been cleared. */
                u1IsHistFreed = BGP4_TRUE;
                BGP4_RFD_FEAS_RTPROFILE_CNT (BGP4_PEER_CXT_ID
                        (pPeerInfo))--;

                /* Set NO_RFD flag to by-pass RFD when the route is
                 * processed as feasible route. */
                BGP4_RT_SET_FLAG (pRtInfo, BGP4_RT_NO_RFD);
                BGP4_RT_RESET_FLAG (pRtInfo, BGP4_RT_DAMPED);
            }
        }
        /* If route becomes reusable, dispose the route
         * damping history */
        if (pRtDampHist->u1RtFlag == RFD_UNFEASIBLE_ROUTE)
        {
            /* Route is Unfeasible. Unfeasible Damped routes are
             * present in RIB as HISTORY routes. Need to remove
             * this route from RIB. */
            BGP4_RT_SET_FLAG (pRtInfo, BGP4_RT_NO_RFD);
            BGP4_RT_RESET_FLAG (pRtInfo, (BGP4_RT_DAMPED |
                        BGP4_RT_HISTORY));
            Bgp4RibhDelRtEntry (pRtInfo, NULL, BGP4_RT_CXT_ID (pRtInfo),
                    BGP4_HISTORY);
        }
        else if (u1IsHistFreed == BGP4_FALSE)
        {
            BGP4_RT_DAMP_HIST (pRtDampHist->pRtProfile) = NULL;
            Bgp4DshReleaseRtInfo (pRtDampHist->pRtProfile);
            Bgp4MemReleaseRfdDampHist (pRtDampHist);
        }
    return RFD_SUCCESS;
}
/**************************** End of File *********************************/
#endif /* RFD_WANTED */
