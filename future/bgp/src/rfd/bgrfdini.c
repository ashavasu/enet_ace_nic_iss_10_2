/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgrfdini.c,v 1.23 2017/09/15 06:19:54 siva Exp $
 *
 * Description: This file contains Route Flap Dampening feature
 *              functions.
 *
 *******************************************************************/
#ifndef _RFDINIT_C
#define _RFDINIT_C
#endif
#ifdef RFD_WANTED
#include "bgp4com.h"
extern INT1         gi1RFDSyncStatus;
/******************************************************************************
     Function Name   :  RfdMemInit
     Description     :  This module allocates memory required for Decay 
                        Array, reuse Index Array, Routes Reuse list and 
                        Peers Reuse List.
     Input(s)        :  None
     Output(s)       :  None
     Global Variables
     Referred        : RFD_REUSE_INDEX_ARRAY_SIZE(BGP4_DFLT_VRFID)

    Global Variables
    Modified         : DECAY_ARRAY_MEM_ALLOC_FAIL_ERR(BGP4_DFLT_VRFID), 
                       PEER_DAMP_HIST_DELETE_FAIL_ERR(BGP4_DFLT_VRFID), 
                       RINDEX_ARRAY_MEM_ALLOC_FAIL_ERR(BGP4_DFLT_VRFID),
                       PEER_RLIST_ENTRY_NOT_FOUND_ERR(BGP4_DFLT_VRFID), 
                       RT_RLIST_INSERT_FAIL_ERR(BGP4_DFLT_VRFID), 
                       RT_DAMP_HIST_DELETE_FAIL_ERR(BGP4_DFLT_VRFID), 
                       RT_RLIST_ENTRY_NOT_FOUND_ERR(BGP4_DFLT_VRFID), 
                       RT_DAMP_HIST_MEM_ALLOC_FAIL_ERR(BGP4_DFLT_VRFID), 
                       RT_NO_ALTERNATE_FOUND_ERR(BGP4_DFLT_VRFID), 
                       PEER_RLIST_INSERT_FAIL_ERR(BGP4_DFLT_VRFID), 
                       PEER_DAMP_HIST_MEM_ALLOC_FAIL_ERR(BGP4_DFLT_VRFID), RFD_DECAY_ARRAY(BGP4_DFLT_VRFID),
                       RFD_REUSE_INDEX_ARRAY(BGP4_DFLT_VRFID), PEERS_DAMP_HIST_LIST(BGP4_DFLT_VRFID),
                       RFD_REUSE_LIST_OFFSET(BGP4_DFLT_VRFID), RFD_REUSE_THRESHOLD(BGP4_DFLT_VRFID),
                       RFD_CUTOFF_THRESHOLD(BGP4_DFLT_VRFID)
       
    Exceptions or OS
    Err Handling     :   None
    Use of Recursion :   None

    Return(s)        :  RFD_SUCCESS or RFD_FAILURE
******************************************************************************/
INT4
RfdMemInit (UINT4 u4Context)
{
    UINT2               u2ReuseIndex;

    /* Initialize all error counters */
    DECAY_ARRAY_MEM_ALLOC_FAIL_ERR (u4Context) = 0;
    PEER_DAMP_HIST_DELETE_FAIL_ERR (u4Context) = 0;
    RINDEX_ARRAY_MEM_ALLOC_FAIL_ERR (u4Context) = 0;
    PEER_RLIST_ENTRY_NOT_FOUND_ERR (u4Context) = 0;
    RT_RLIST_INSERT_FAIL_ERR (u4Context) = 0;
    RT_DAMP_HIST_DELETE_FAIL_ERR (u4Context) = 0;
    RT_RLIST_ENTRY_NOT_FOUND_ERR (u4Context) = 0;
    RT_DAMP_HIST_MEM_ALLOC_FAIL_ERR (u4Context) = 0;
    RT_NO_ALTERNATE_FOUND_ERR (u4Context) = 0;
    PEER_RLIST_INSERT_FAIL_ERR (u4Context) = 0;
    PEER_DAMP_HIST_MEM_ALLOC_FAIL_ERR (u4Context) = 0;
    /* Initialize routes reuse list offset */
    RFD_REUSE_LIST_OFFSET (u4Context) = 0;

    /* Read the Rfd configurable parameters from file */
    RfdReadSystemConfigInfo (u4Context);

    /* Allocate memory for Decay array */
    /* TODO this needs to be confirmed as max value is not dynamically changed */
    if (((RFD_MAX_HOLD_DOWN_TIME (u4Context) + 1) * sizeof (float)) >
        MAX_BGP_RFD_DECAY_ARRAY_SIZE)
        /*
           FsBGPSizingParams[MAX_BGP_RFD_DECAY_ARRAY_BLOCKS_SIZING_ID].
           u4PreAllocatedUnits)) */
    {
        RfdFaultReportReceive (u4Context, RFD_DECAY_ARRAY_MEM_ALLOC_FAIL);
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tRfdMemInit() : FAILED !!! RFD_MAX_HOLD_DOWN_TIME value is greater than"
                  " MAX_BGP_RFD_DECAY_ARRAY_SIZE,hence increase the size of"
                  " MAX_BGP_RFD_DECAY_ARRAY_SIZE in params.h same as RFD_MAX_HOLD_DOWN_TIME(BGP4_DFLT_VRFID)\n");
        return RFD_FAILURE;

    }
    if (((RFD_REUSE_INDEX_ARRAY_SIZE (u4Context) + 1) * 2) >
        MAX_BGP_RFD_REUSE_INDEX_ARRAY_SIZE)
    {
        RfdFaultReportReceive (u4Context, RFD_RINDEX_ARRAY_MEM_ALLOC_FAIL);
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tRfdMemInit() : FAILED!!!! RFD_REUSE_INDEX_ARRAY_SIZE() value is greater than"
                  "MAX_BGP_RFD_REUSE_INDEX_ARRAY_SIZE ,hence increase the size of"
                  "MAX_BGP_RFD_REUSE_INDEX_ARRAY_SIZE in params.h same as RFD_REUSE_INDEX_ARRAY_SIZE()\n");
        return RFD_FAILURE;
    }
    RFD_DECAY_ARRAY (u4Context) =
        (RFDFLOAT *) MemAllocMemBlk (gBgpNode.Bgp4RfdDecayPoolId);
    if (RFD_DECAY_ARRAY (u4Context) == NULL)
    {
        RfdFaultReportReceive (u4Context, RFD_DECAY_ARRAY_MEM_ALLOC_FAIL);
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tRfdMemInit() : CREATING MEMORY for DECAY ARRAY "
                  "FAILED !!!\n");
        gu4BgpDebugCnt[MAX_BGP_RFD_DECAY_ARRAY_BLOCKS_SIZING_ID]++;
        return RFD_FAILURE;
    }
    BGP4_MSG_CNT++;
    /*Allocate memory for Reuse Index Array */
    RFD_REUSE_INDEX_ARRAY (u4Context) = (UINT2 *)
        MemAllocMemBlk (gBgpNode.Bgp4RfdReuseIndexPoolId);
    if (RFD_REUSE_INDEX_ARRAY (u4Context) == NULL)
    {
        RfdFaultReportReceive (u4Context, RFD_RINDEX_ARRAY_MEM_ALLOC_FAIL);
        MemReleaseMemBlock (gBgpNode.Bgp4RfdDecayPoolId,
                            (UINT1 *) RFD_DECAY_ARRAY (u4Context));
        BGP4_MSG_CNT--;
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tRfdMemInit() : CREATING MEMORY for REUSE INDEX "
                  "ARRAY FAILED !!!\n");
        gu4BgpDebugCnt[MAX_BGP_RFD_REUSE_INDEX_ARRAY_BLOCKS_SIZING_ID]++;
        return RFD_FAILURE;
    }
    BGP4_MSG_CNT++;

    /* Allocate memory for Peers Damping History Hash Table */
    PEERS_DAMP_HIST_LIST (u4Context) =
        TMO_HASH_Create_Table (BGP4_MAX_PREFIX_HASH_TBL_SIZE, NULL, FALSE);
    if (PEERS_DAMP_HIST_LIST (u4Context) == NULL)
    {
        MemReleaseMemBlock (gBgpNode.Bgp4RfdDecayPoolId,
                            (UINT1 *) RFD_DECAY_ARRAY (u4Context));
        BGP4_MSG_CNT--;
        MemReleaseMemBlock (gBgpNode.Bgp4RfdReuseIndexPoolId,
                            (UINT1 *) RFD_REUSE_INDEX_ARRAY (u4Context));
        BGP4_MSG_CNT--;
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_INIT_SHUT_TRC |
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                  BGP4_MOD_NAME,
                  "\tRfdMemInit() : CREATING HASH TABLE for PEER DAMPING "
                  "HISTORY FAILED !!! \n");
        return RFD_FAILURE;
    }

    /*Initialize peers reuse list */
    TMO_SLL_Init (RFD_PEERS_REUSE_LIST (u4Context));
    u2ReuseIndex = 0;
    /* Initialize routes reuse list */
    while (u2ReuseIndex < RFD_NO_OF_REUSE_LISTS)
    {
        RFD_RTS_REUSE_LIST_FIRST (u4Context, u2ReuseIndex) = NULL;
        RFD_RTS_REUSE_LIST_LAST (u4Context, u2ReuseIndex) = NULL;
        RFD_RTS_REUSE_LIST_COUNT (u4Context, u2ReuseIndex) = 0;
        u2ReuseIndex++;
    }
    RFD_GLOBAL_PARAMS (u4Context).pLastReuseList = NULL;
    BGP4_RFD_FEAS_RTPROFILE_CNT (u4Context) = 0;
    /* Initialise the Reuse-Peer Handling List. */
    TMO_SLL_Init (BGP4_SUP_PEER_REUSE_LIST (u4Context));
    return RFD_SUCCESS;
}

/****************************************************************************
     Function Name    : RfdInitialise
     Description      : This invokes other modules to derive ceiling, build 
                        decay array and reuse index array. This module 
                        calculates Scale Factor Value using the following value
                        MaxRatio = min (ceiling/reuse, exp ((1/ 
                        (DecayHalfLifeTime /MaxHoldDownTime)) * ln (2)))
                        ScaleFactor = ReuseIndexArraySize/(MaxRatio-1)

     Input(s)         : None
     Output(s)        : None
     Global Variables
     Referred         :   RFD_CEILING_VALUE(BGP4_DFLT_VRFID), RFD_REUSE_THRESHOLD(BGP4_DFLT_VRFID), 
                          RFD_DECAY_HALF_LIFE_TIME(BGP4_DFLT_VRFID), RFD_MAX_HOLD_DOWN_TIME(BGP4_DFLT_VRFID),
                          RFD_REUSE_INDEX_ARRAY_SIZE(BGP4_DFLT_VRFID)
     Global Variables
     Modified         :   RFD_SCALE_FACTOR(BGP4_DFLT_VRFID)
     Exceptions or OS
     Err Handling     :   None
     Use of Recursion :   None

     Return(s)        :  None 
*******************************************************************************/
VOID
RfdInitialise (UINT4 u4Context)
{
    RFDFLOAT            RIndxTmp;
    RFDFLOAT            ReuseTmp;
    RFDFLOAT            MaxRatio;

    /* Calculate Ceiling Value */
    RfdCalcCeiling (u4Context);

    /* Precompute Scale Factor  */
    RIndxTmp =
        ((RFD_CEILING_VALUE (u4Context)) /
         ((RFDFLOAT) (RFD_REUSE_THRESHOLD (u4Context))));
    ReuseTmp =
        ((RFDFLOAT) ((RFD_REUSE_INDEX_ARRAY_SIZE (u4Context) *
                      RFD_REUSE_TIMER_GRANULARITY (u4Context))) /
         (RFDFLOAT) (RFD_DECAY_HALF_LIFE_TIME (u4Context)));

    ReuseTmp = ReuseTmp * RFD_NAT_LOG (2.0);
    ReuseTmp = RFD_EXP (ReuseTmp);
    MaxRatio = RFD_MIN (RIndxTmp, ReuseTmp);
    RFD_SCALE_FACTOR (u4Context) =
        (RFD_REUSE_INDEX_ARRAY_SIZE (u4Context) / (MaxRatio - 1.0));

    /* Build Decay Array */
    RfdBuildDecayArray (u4Context);
    /* Build Reuse Index Array */
    RfdBuildRIndxArray (u4Context);
    return;
}

/******************************************************************************
     Function Name    :  RfdCalcCeiling
     Description      :  This module calculates ceiling value from the 
                         following equation  
                         Ceiling = Reuse * 
                         (exp ((MaxHoldDownTime/ DecayHalfLifeTime)*ln (2)))
     Input(s)         :  None
     Output(s)        :  None
     Global Variables
     Referred         :   
     Global Variables
     Modified         :   
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : None
****************************************************************************/
VOID
RfdCalcCeiling (UINT4 u4Context)
{
    RFDFLOAT            CeilingTempVal;
    /* calculate the ceiling value using the formula */
    CeilingTempVal =
        ((RFDFLOAT) (RFD_MAX_HOLD_DOWN_TIME (u4Context)) /
         (RFDFLOAT) (RFD_DECAY_HALF_LIFE_TIME (u4Context)));
    CeilingTempVal = RFD_POW (2.0, CeilingTempVal);
    RFD_CEILING_VALUE (u4Context) =
        (CeilingTempVal * RFD_REUSE_THRESHOLD (u4Context));

    return;
}

/****************************************************************************
     Function Name    : RfdBuildDecayArray
     Description      : This module builds decay array from following 
                        equations
                        Decay [1] = exp ((1 / (DecayHalfLifeTime /
                        DecayTimerGranularity))*ln (1/2))Decay-array-size = 
                        (MaxHoldDownTime /DecayTimerGranularity)Decay [i] 
                        = decay [1] **i
     Input(s)         : None
     Output(s)        : None
     Global Variables
     Referred         :   
     Global Variables
     Modified         :   
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : None 
*****************************************************************************/
VOID
RfdBuildDecayArray (UINT4 u4Context)
{
    UINT2               u2DecayArraySize;
    UINT2               u2DecayIndex;
    RFDFLOAT            DecayTempVal;

    /* Calculate Decay[1] */
    DecayTempVal = ((RFDFLOAT) (RFD_DECAY_TIMER_GRANULARITY (u4Context)) /
                    (RFDFLOAT) (RFD_DECAY_HALF_LIFE_TIME (u4Context)));
    DecayTempVal = RFD_POW (2.0, DecayTempVal);
    DecayTempVal = 1.0 / DecayTempVal;
    RFD_DECAY_ARRAY (u4Context)[1] = DecayTempVal;
    /*When Decay Index becomes zero, no Decay */
    RFD_DECAY_ARRAY (u4Context)[0] = 1.0;
    u2DecayIndex = 2;
    u2DecayArraySize =
        (UINT2) (RFD_DECAY_ARRAY_SIZE (u4Context) + BGP4_INCREMENT_BY_ONE);

    /*  Calculate Decay[i] */
    while (u2DecayIndex < u2DecayArraySize)
    {
        RFD_DECAY_ARRAY (u4Context)[u2DecayIndex] =
            RFD_POW (DecayTempVal, u2DecayIndex);
        u2DecayIndex++;
    }
    return;
}

/*****************************************************************************
     Function Name    :  RfdBuildRIndxArray
     Description      : This module builds reuse index array from following 
                        equations
                        MaxRatio = min (ceiling/reuse, exp ((1/ 
                        (DecayHalfLifeTime /MaxHoldDownTime)) * ln (2)))
                        ScaleFactor = ReuseIndexArraySize/(MaxRatio-1)
                        ReuseIndexArray [j] = integer ((DecayHalfLifeTime / 
                        ReuseTimerGranularity) * 
                        ln (1/(reuse * (1 + (j / ScaleFactor)))) 
                        /ln (1/2))
     Input(s)         :  None
     Output(s)        :  None
     Global Variables
     Referred         :   
     Global Variables
     Modified         :   
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : None 
*****************************************************************************/
VOID
RfdBuildRIndxArray (UINT4 u4Context)
{
    UINT2               u2Index;
    RFDFLOAT            RIndxTempVal;
    RFDFLOAT            ReuseTempVal;
    UINT2               u2RIndxAraySize;

    /*Calculate reuse index value using the formula given in function header */

    u2Index = 0;
    u2RIndxAraySize = (UINT2) (RFD_REUSE_INDEX_ARRAY_SIZE (u4Context) +
                               BGP4_INCREMENT_BY_ONE);
    while (u2Index < u2RIndxAraySize)
    {
        RIndxTempVal = (u2Index / RFD_SCALE_FACTOR (u4Context));
        RIndxTempVal = (1.0 + RIndxTempVal);
        RIndxTempVal = ((RFD_REUSE_THRESHOLD (u4Context)) * RIndxTempVal);
        RIndxTempVal = 1.0 / RIndxTempVal;
        RIndxTempVal = RFD_NAT_LOG (RIndxTempVal);
        ReuseTempVal = ((RFDFLOAT) (RFD_DECAY_HALF_LIFE_TIME (u4Context)) /
                        (RFDFLOAT) (RFD_REUSE_TIMER_GRANULARITY (u4Context)));
        RIndxTempVal = ReuseTempVal * RIndxTempVal;
        RIndxTempVal = RIndxTempVal / RFD_NAT_LOG (1 / 2);
        RFD_REUSE_INDEX_ARRAY (u4Context)[u2Index] = (UINT2) RIndxTempVal;
        u2Index++;
    }
    return;
}

/******************************************************************************
     Function Name   :  RfdShutDown
     Description     :  This module releases the  memory allocated for Decay 
                        Array, reuse Index Array, Routes Reuse list and 
                        Peers Reuse List.
     Input(s)        :  None
     Output(s)       :  None
     Global Variables
     Referred        : RFD_DECAY_ARRAY(BGP4_DFLT_VRFID)
                       RFD_REUSE_INDEX_ARRAY_SIZE(BGP4_DFLT_VRFID)

    Global Variables
    Modified         : RFD_DECAY_ARRAY(BGP4_DFLT_VRFID),
                       RFD_REUSE_INDEX_ARRAY(BGP4_DFLT_VRFID)       
    Exceptions or OS
    Err Handling     :   None
    Use of Recursion :   None

    Return(s)        :  RFD_SUCCESS or RFD_FAILURE
******************************************************************************/
INT4
RfdShutDown (UINT4 u4Context)
{

    /*Release  memory allocated for Decay array */
    if (RFD_DECAY_ARRAY (u4Context) != NULL)
    {
        MemReleaseMemBlock (gBgpNode.Bgp4RfdDecayPoolId,
                            (UINT1 *) RFD_DECAY_ARRAY (u4Context));
        BGP4_MSG_CNT--;
        RFD_DECAY_ARRAY (u4Context) = NULL;
    }
    /*Release memory allocated for Reuse Index Array */
    if (RFD_REUSE_INDEX_ARRAY (u4Context) != NULL)
    {
        MemReleaseMemBlock (gBgpNode.Bgp4RfdReuseIndexPoolId,
                            (UINT1 *) RFD_REUSE_INDEX_ARRAY (u4Context));
        BGP4_MSG_CNT--;
        RFD_REUSE_INDEX_ARRAY (u4Context) = NULL;
    }

    /*Delete peers reuse list */
    RfdClearPeersReuseList (u4Context);
    /* Delete peers damping history */
    RfdDeletePeersDampHist (u4Context);

    /* Delete peers damping history */
    RfdClearRouteDampHist (u4Context);
    /* BGP4_SUP_PEER_REUSE_LIST will be cleared after 
     * advertising the routes learnt from the peers in the list */

    return RFD_SUCCESS;
}

/******************************************************************************
     Function Name   :  RfdReadSystemConfigInfo 
     Description     :  This module updates rfd configurable parameters with 
                        the values in the file.If file opening fails, the 
                        default values will be stored.
     Input(s)        :  None
     Output(s)       :  None
     Global Variables
     Referred        :  None

    Global Variables
    Modified         : RFD_CUTOFF_THRESHOLD(BGP4_DFLT_VRFID), RFD_REUSE_INDEX_ARRAY_SIZE(BGP4_DFLT_VRFID)
                       RFD_DECAY_ARRAY(BGP4_DFLT_VRFID),RFD_REUSE_THRESHOLD(BGP4_DFLT_VRFID),
                       RFD_MAX_HOLD_DOWN_TIME(BGP4_DFLT_VRFID), RFD_DECAY_HALF_LIFE_TIME(BGP4_DFLT_VRFID),
                       RFD_DECAY_TIMER_GRANULARITY(BGP4_DFLT_VRFID), RFD_REUSE_INDEX_ARRAY_SIZE(BGP4_DFLT_VRFID)
    Exceptions or OS
    Err Handling     : None
    Use of Recursion : None

    Return(s)        : None
******************************************************************************/
VOID
RfdReadSystemConfigInfo (UINT4 u4Context)
{
    tBgpSystemSize      BgpSystemSize;
#ifdef ISS_WANTED
    UNUSED_PARAM (BgpSystemSize);
    UNUSED_PARAM (u4Context);
    return;
#else
    GetBgpSizingParams (&BgpSystemSize);
    /* Cutoff */
    RFD_CUTOFF_THRESHOLD (u4Context) = BgpSystemSize.u4BgpRfdCutOffThreshold;
    /* Reuse */
    RFD_REUSE_THRESHOLD (u4Context) = BgpSystemSize.u4BgpRfdReuseThreshold;
    /* MaxHoldDownTime */
    RFD_MAX_HOLD_DOWN_TIME (u4Context) =
        (UINT2) BgpSystemSize.u4BgpRfdMaxHoldDownTime;
    /* DecayHalfLifeTime */
    RFD_DECAY_HALF_LIFE_TIME (u4Context) =
        (UINT2) BgpSystemSize.u4BgpRfdDecayHalfLifeTime;
    /* DecayTimerGranularity */
    RFD_DECAY_TIMER_GRANULARITY (u4Context) =
        (UINT2) BgpSystemSize.u4BgpRfdDecayTimerGranularity;
    /* ReuseTimerGranularity */
    RFD_REUSE_TIMER_GRANULARITY (u4Context) =
        (UINT2) BgpSystemSize.u4BgpRfdReuseTimerGranularity;
    /* ReuseIndexArraySize */
    RFD_REUSE_INDEX_ARRAY_SIZE (u4Context) =
        (UINT2) BgpSystemSize.u4BgpRfdReuseArraySize;
    return;
#endif /* ISS_WANTED */
}

/******************************************************************************/
/* Function Name : Bgp4InitRfdDampHist                                        */
/* Description   : This function intialises variables of type tRtDampHist to  */
/*                 their default values. This fn. is called whenever a memory */
/*                 is dynamically allocated for a tRtDampHist                 */
/* Input(s)      : pRtDampHist - pointer to Route Damp History                */
/* Output(s)     : None.                                                      */
/* Return(s)     : None                                                       */
/******************************************************************************/
VOID
Bgp4InitRfdDampHist (tRtDampHist * pRtDampHist)
{
    pRtDampHist->pRtProfile = NULL;
    pRtDampHist->pReusePrev = NULL;
    pRtDampHist->pReuseNext = NULL;
    pRtDampHist->RtFom = 0;
    pRtDampHist->u4RtLastTime = 0;
    pRtDampHist->u2ReuseIndex = RFD_INVALID_INDX;
    pRtDampHist->u1RtStatus = RFD_UNSUPPRESSED_STATE;
    pRtDampHist->u1RtFlag = RFD_UNFEASIBLE_ROUTE;
    pRtDampHist->u4RtFlapCount = 0;
    pRtDampHist->u4RtFlapTime = 0;
    pRtDampHist->u4RtReuseTime = 0;
}

/***************************************************************************
     Function Name    : RfdDeletePeersDampHist
     Description      : This module deletes the peers damping history and 
                        releases the memory allocated for peers damping 
                        history.
     Input(s)         : None
     Output(s)        : None
     Global Variables
     Referred         : PEERS_DAMP_HIST_LIST(BGP4_DFLT_VRFID)

     Global Variables
     Modified         : PEERS_DAMP_HIST_LIST(BGP4_DFLT_VRFID) 
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS RFD_FAILURE
*****************************************************************************/
INT4
RfdDeletePeersDampHist (UINT4 u4Context)
{
    tPeerDampHist      *pPeerDampHist = NULL;
    tPeerDampHist      *pTmpPeerDampHist = NULL;
    UINT4               u4HashIndex;

    if (PEERS_DAMP_HIST_LIST (u4Context) == NULL)
    {
        return RFD_SUCCESS;
    }
    TMO_HASH_Scan_Table (PEERS_DAMP_HIST_LIST (u4Context), u4HashIndex)
    {
        RFD_HASH_DYN_Scan_Bucket (PEERS_DAMP_HIST_LIST (u4Context), u4HashIndex,
                                  pPeerDampHist, pTmpPeerDampHist,
                                  tPeerDampHist *)
        {
            TMO_HASH_Delete_Node (PEERS_DAMP_HIST_LIST (u4Context),
                                  &(pPeerDampHist->NextPeerDampHist),
                                  u4HashIndex);
            PEER_DAMP_HIST_ENTRY_FREE (pPeerDampHist);
        }
    }
    TMO_HASH_Delete_Table (PEERS_DAMP_HIST_LIST (u4Context), NULL);
    PEERS_DAMP_HIST_LIST (u4Context) = NULL;
    return RFD_SUCCESS;
}

/***************************************************************************
     Function Name    : RfdClearRouteDampHist
     Description      : This module clears the routes damping history and
                         makes the routes as feasible routes.
     Input(s)         : None
     Output(s)        : None
     Return(s)        : RFD_SUCCESS RFD_FAILURE
*****************************************************************************/
INT4
RfdClearRouteDampHist (UINT4 u4Context)
{
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pRtProfileList = NULL;
    tRouteProfile      *pTmpRoute = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4Status = 0;
    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return BGP4_CLEAR_RIB_LOCK_FAILURE;
    }

    Bgp4GetRibNode (u4Context, CAP_MP_IPV4_UNICAST, &pRibNode);
    if (pRibNode != NULL)
    {

        i4Status = Bgp4RibhGetFirstEntry (CAP_MP_IPV4_UNICAST,
                                          &pRtProfileList, &pRibNode, TRUE);
        if (i4Status == BGP4_SUCCESS)
        {
            for (;;)
            {
                pRtProfile = pRtProfileList;
                u4Context = BGP4_RT_CXT_ID (pRtProfile);
                while (pRtProfile != NULL)
                {
                    if (BGP4_RT_NEXT (pRtProfile) != NULL)
                    {
                        pTmpRoute = BGP4_RT_NEXT (pRtProfile);
                    }
                    RfdUtlClearDampHist (pRtProfile);
                    if (pTmpRoute != NULL)
                    {
                        pRtProfile = pTmpRoute;
                        pTmpRoute = NULL;
                    }
                    else
                    {
                        pRtProfile = BGP4_RT_NEXT (pRtProfile);
                    }
                }
                i4Status = Bgp4RibhGetNextEntry (pRtProfileList,
                                                 u4Context,
                                                 &pRtProfileList,
                                                 &pRibNode, TRUE);
                if (i4Status == BGP4_FAILURE)
                {
                    break;
                }
            }
        }
    }
#ifdef BGP4_IPV6_WANTED
    Bgp4GetRibNode (u4Context, CAP_MP_IPV6_UNICAST, &pRibNode);
    if (pRibNode != NULL)
    {

        i4Status = Bgp4RibhGetFirstEntry (CAP_MP_IPV6_UNICAST,
                                          &pRtProfileList, &pRibNode, TRUE);
        if (i4Status == BGP4_SUCCESS)
        {
            for (;;)
            {
                pRtProfile = pRtProfileList;
                u4Context = BGP4_RT_CXT_ID (pRtProfile);
                while (pRtProfile != NULL)
                {
                    if (BGP4_RT_NEXT (pRtProfile) != NULL)
                    {
                        pTmpRoute = BGP4_RT_NEXT (pRtProfile);
                    }
                    RfdUtlClearDampHist (pRtProfile);
                    if (pTmpRoute != NULL)
                    {
                        pRtProfile = pTmpRoute;
                        pTmpRoute = NULL;
                    }
                    else
                    {
                        pRtProfile = BGP4_RT_NEXT (pRtProfile);
                    }
                }
                i4Status = Bgp4RibhGetNextEntry (pRtProfileList,
                                                 u4Context,
                                                 &pRtProfileList,
                                                 &pRibNode, TRUE);
                if (i4Status == BGP4_FAILURE)
                {
                    break;
                }
            }
        }
    }
#endif

    /* Unable to find the current route in the RIB. */
    bgp4RibUnlock ();
    return RFD_SUCCESS;
}

/****************************************************************************

     Function Name    :  RfdClearPeersReuseList 
     Description      :  This module removes  all the routes from  peers 
                         reuse list and advertises them as feasible routes.
     Input(s)         :  None
     Output(s)        :  None
     Return(s)        : RFD_SUCCESS RFD_FAILURE
****************************************************************************/
INT4
RfdClearPeersReuseList (UINT4 u4Context)
{
    tReusePeer         *pReusePeer = NULL;
    tReusePeer         *pTmpReusePeer = NULL;
    tPeerNode          *pPeerReuse = NULL;
    tBgp4PeerEntry     *pPeerEntry = NULL;

    BGP_SLL_DYN_Scan (RFD_PEERS_REUSE_LIST (u4Context), pReusePeer,
                      pTmpReusePeer, tReusePeer *)
    {
        pPeerEntry = pReusePeer->pPeerInfo;
        if (BGP4_LOCAL_ADMIN_STATUS (u4Context) != BGP4_ADMIN_DOWN)
        {
            BGP_PEER_NODE_CREATE (pPeerReuse);
            if (pPeerReuse == NULL)
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : Memory Allocation to "
                               "handle Supress-Peer-Reuse FAILED!!!\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerEntry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerEntry))));
                gu4BgpDebugCnt[MAX_BGP_PEER_NODE_SIZING_ID]++;
                return RFD_FAILURE;
            }

            pPeerReuse->pPeer = pPeerEntry;
            /* Always begin with IPV4_UNICAST. */
            BGP4_PEER_INIT_AFI (pPeerEntry) = BGP4_INET_AFI_IPV4;
            BGP4_PEER_INIT_SAFI (pPeerEntry) = BGP4_INET_SAFI_UNICAST;
            Bgp4InitNetAddressStruct (&
                                      (BGP4_PEER_INIT_NETADDR_INFO
                                       (pPeerEntry)), BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);
            TMO_SLL_Add (BGP4_SUP_PEER_REUSE_LIST
                         (BGP4_PEER_CXT_ID (pPeerEntry)), &pPeerReuse->TSNext);
            BGP4_SET_PEER_CURRENT_STATE (pPeerEntry,
                                         BGP4_PEER_REUSE_INPROGRESS);
            BGP4_RESET_PEER_PEND_FLAG (pPeerEntry, BGP4_PEER_REUSE_PEND);
        }
        else
        {
            /*  Reset the peer current status */
            BGP4_RESET_PEER_CURRENT_STATE (pPeerEntry);
            BGP4_SET_PEER_CURRENT_STATE (pPeerEntry, BGP4_PEER_READY);
        }

        TMO_SLL_Delete (RFD_PEERS_REUSE_LIST (u4Context),
                        &pReusePeer->NextReusePeer);
        REUSE_PEER_ENTRY_FREE (pReusePeer);
    }
    RFD_GLOBAL_PARAMS (u4Context).pLastReuseList = NULL;
    return RFD_SUCCESS;
}

/****************************************************************************
     Function Name    :  RfdEnable 
     Description      :  This function enables the RFD module by
                         allocating required memory and starting the
                         RFD reuse timer.
     Input(s)         :  None
     Output(s)        :  None
     Return(s)        :  RFD_SUCCESS RFD_FAILURE
****************************************************************************/
INT4
RfdEnable (UINT4 u4Context)
{
    INT4                i4RetValFsbgp4GlobalAdminStatus =
        BGP4_LOCAL_ADMIN_STATUS (u4Context);

    if (i4RetValFsbgp4GlobalAdminStatus != BGP4_ADMIN_UP)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tRfdEnable() : FAILED !!! BGP Admin is down\n");
        return RFD_FAILURE;
    }

    if (RfdInit (u4Context) != RFD_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tRfdEnable() : FAILED !!! Unable to allocate memory\n");
        return RFD_FAILURE;
    }
    RfdInitialise (u4Context);
    (RFD_REUSE_TIMER_ENTRY (u4Context))->u4Flag = BGP4_ACTIVE;
    (RFD_REUSE_TIMER_ENTRY (u4Context))->u4TimerId = BGP4_RFD_REUSE_TIMER;
    (RFD_REUSE_TIMER_ENTRY (u4Context))->Tmr.u4Data =
        (FS_ULONG) Bgp4GetContextEntry (u4Context);
    BGP4_TIMER_START (BGP4_TIMER_LISTID,
                      (tTmrAppTimer *) RFD_REUSE_TIMER_ENTRY (u4Context),
                      RFD_REUSE_TIMER_GRANULARITY (u4Context));
    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\t RFD enabled successfully\n");
    return RFD_SUCCESS;
}

/****************************************************************************
     Function Name    :  RfdDisable 
     Description      :  This function disables the RFD module by
                         releasing the RFD variables memory and stopping the
                         RFD reuse timer.
     Input(s)         :  None
     Output(s)        :  None
     Return(s)        :  RFD_SUCCESS RFD_FAILURE
****************************************************************************/
INT4
RfdDisable (UINT4 u4Context)
{
    INT4                i4RetValFsbgp4GlobalAdminStatus =
        BGP4_LOCAL_ADMIN_STATUS (u4Context);

    if (i4RetValFsbgp4GlobalAdminStatus != BGP4_ADMIN_UP)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tRfdDisable () : FAILED !!! BGP Admin is down\n");
    }

    if (RfdShutDown (u4Context) != RFD_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tRfdDisable () : FAILED !!! Unable to release memory\n");
        return RFD_FAILURE;
    }
    BGP4_TIMER_STOP (BGP4_TIMER_LISTID,
                     (tTmrAppTimer *) RFD_REUSE_TIMER_ENTRY (u4Context));
    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\t RFD disabled successfully\n");
    return RFD_SUCCESS;
}

/******************************************************************************
     Function Name   :  RfdInit
     Description     :  This module allocates memory required for Decay 
                        Array, reuse Index Array, Routes Reuse list and 
                        Peers Reuse List.
     Input(s)        :  None
     Output(s)       :  None
    Return(s)        :  RFD_SUCCESS or RFD_FAILURE
******************************************************************************/
INT4
RfdInit (UINT4 u4Context)
{
    UINT2               u2ReuseIndex;

    /* Initialize all error counters */
    DECAY_ARRAY_MEM_ALLOC_FAIL_ERR (u4Context) = 0;
    PEER_DAMP_HIST_DELETE_FAIL_ERR (u4Context) = 0;
    RINDEX_ARRAY_MEM_ALLOC_FAIL_ERR (u4Context) = 0;
    PEER_RLIST_ENTRY_NOT_FOUND_ERR (u4Context) = 0;
    RT_RLIST_INSERT_FAIL_ERR (u4Context) = 0;
    RT_DAMP_HIST_DELETE_FAIL_ERR (u4Context) = 0;
    RT_RLIST_ENTRY_NOT_FOUND_ERR (u4Context) = 0;
    RT_DAMP_HIST_MEM_ALLOC_FAIL_ERR (u4Context) = 0;
    RT_NO_ALTERNATE_FOUND_ERR (u4Context) = 0;
    PEER_RLIST_INSERT_FAIL_ERR (u4Context) = 0;
    PEER_DAMP_HIST_MEM_ALLOC_FAIL_ERR (u4Context) = 0;
    /* Initialize routes reuse list offset */
    RFD_REUSE_LIST_OFFSET (u4Context) = 0;

    /* Allocate memory for Decay array */
    if (((RFD_MAX_HOLD_DOWN_TIME (u4Context) + 1) * sizeof (float)) >
        ((MAX_BGP_RFD_DECAY_ARRAY_SIZE) *
         (FsBGPSizingParams[MAX_BGP_RFD_DECAY_ARRAY_BLOCKS_SIZING_ID].
          u4PreAllocatedUnits)))
    {
        RfdFaultReportReceive (u4Context, RFD_DECAY_ARRAY_MEM_ALLOC_FAIL);
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tRfdInit() : FAILED !!! RFD_MAX_HOLD_DOWN_TIME() value is greater than"
                  " MAX_BGP_RFD_DECAY_ARRAY_SIZE,hence increase the size of"
                  " MAX_BGP_RFD_DECAY_ARRAY_SIZE in params.h same as RFD_MAX_HOLD_DOWN_TIME()\n");
        return RFD_FAILURE;

    }
    if (((RFD_REUSE_INDEX_ARRAY_SIZE (u4Context) + 1) * 2) >
        MAX_BGP_RFD_REUSE_INDEX_ARRAY_SIZE)
    {
        RfdFaultReportReceive (u4Context, RFD_RINDEX_ARRAY_MEM_ALLOC_FAIL);
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tRfdInit() : FAILED!!!! RFD_REUSE_INDEX_ARRAY_SIZE() value is greater than"
                  "MAX_BGP_RFD_REUSE_INDEX_ARRAY_SIZE ,hence increase the size of"
                  "MAX_BGP_RFD_REUSE_INDEX_ARRAY_SIZE in params.h same as RFD_REUSE_INDEX_ARRAY_SIZE()\n");
        return RFD_FAILURE;
    }
    RFD_DECAY_ARRAY (u4Context) =
        (RFDFLOAT *) MemAllocMemBlk (gBgpNode.Bgp4RfdDecayPoolId);
    if (RFD_DECAY_ARRAY (u4Context) == NULL)
    {
        RfdFaultReportReceive (u4Context, RFD_DECAY_ARRAY_MEM_ALLOC_FAIL);
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tRfdInit() : CREATING MEMORY for DECAY ARRAY "
                  "FAILED !!!\n");
        return RFD_FAILURE;
    }
    BGP4_MSG_CNT++;
    /*Allocate memory for Reuse Index Array */
    RFD_REUSE_INDEX_ARRAY (u4Context) = (UINT2 *)
        MemAllocMemBlk (gBgpNode.Bgp4RfdReuseIndexPoolId);
    if (RFD_REUSE_INDEX_ARRAY (u4Context) == NULL)
    {
        RfdFaultReportReceive (u4Context, RFD_RINDEX_ARRAY_MEM_ALLOC_FAIL);
        MemReleaseMemBlock (gBgpNode.Bgp4RfdDecayPoolId,
                            (UINT1 *) RFD_DECAY_ARRAY (u4Context));
        BGP4_MSG_CNT--;
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tRfdInit() : CREATING MEMORY for REUSE INDEX "
                  "ARRAY FAILED !!!\n");
        gu4BgpDebugCnt[MAX_BGP_RFD_REUSE_INDEX_ARRAY_BLOCKS_SIZING_ID]++;
        return RFD_FAILURE;
    }
    BGP4_MSG_CNT++;

    /* Allocate memory for Peers Damping History Hash Table */
    PEERS_DAMP_HIST_LIST (u4Context) =
        TMO_HASH_Create_Table (BGP4_MAX_PREFIX_HASH_TBL_SIZE, NULL, FALSE);
    if (PEERS_DAMP_HIST_LIST (u4Context) == NULL)
    {
        MemReleaseMemBlock (gBgpNode.Bgp4RfdDecayPoolId,
                            (UINT1 *) RFD_DECAY_ARRAY (u4Context));
        BGP4_MSG_CNT--;
        MemReleaseMemBlock (gBgpNode.Bgp4RfdReuseIndexPoolId,
                            (UINT1 *) RFD_REUSE_INDEX_ARRAY (u4Context));
        BGP4_MSG_CNT--;
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_INIT_SHUT_TRC |
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                  BGP4_MOD_NAME,
                  "\tRfdInit() : CREATING HASH TABLE for PEER DAMPING "
                  "HISTORY FAILED !!! \n");
        return RFD_FAILURE;
    }

    /*Initialize peers reuse list */
    TMO_SLL_Init (RFD_PEERS_REUSE_LIST (u4Context));
    u2ReuseIndex = 0;
    /* Initialize routes reuse list */
    while (u2ReuseIndex < RFD_NO_OF_REUSE_LISTS)
    {
        RFD_RTS_REUSE_LIST_FIRST (u4Context, u2ReuseIndex) = NULL;
        RFD_RTS_REUSE_LIST_LAST (u4Context, u2ReuseIndex) = NULL;
        RFD_RTS_REUSE_LIST_COUNT (u4Context, u2ReuseIndex) = 0;
        u2ReuseIndex++;
    }
    RFD_GLOBAL_PARAMS (u4Context).pLastReuseList = NULL;
    BGP4_RFD_FEAS_RTPROFILE_CNT (u4Context) = 0;
    /* Initialise the Reuse-Peer Handling List. */
    TMO_SLL_Init (BGP4_SUP_PEER_REUSE_LIST (u4Context));
    return RFD_SUCCESS;
}

/***************************************************************************
     Function Name    : RfdUtlClearDampHist
     Description      : This module clears the routes damping history and
                         makes the routes as feasible routes.
     Input(s)         : Route Profile
     Output(s)        : None
     Return(s)        : RFD_SUCCESS RFD_FAILURE
*****************************************************************************/
INT4
RfdUtlClearDampHist (tRouteProfile * pRtProfile)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tRtDampHist        *pRtDampHist = NULL;

    if (pRtProfile->pRtDampHist != NULL)
    {
        Bgp4TmrhStopTimer (BGP4_ROUTE_REUSE_TIMER,
                           (VOID *) pRtProfile->pRtDampHist);
        pRtDampHist = pRtProfile->pRtDampHist;
        pRtDampHist->pRtProfile->pRtDampHist = NULL;

        if ((BGP4_RT_REF_COUNT (pRtDampHist->pRtProfile) > 1)
            && (pRtDampHist->u1RtFlag == RFD_FEASIBLE_ROUTE))
        {
            pRtProfile = pRtDampHist->pRtProfile;
            pPeerEntry = BGP4_RT_PEER_ENTRY (pRtProfile);

            /* Route needs to be advertised. Add the route to
             * the peer's received route list. Alse Delete the
             * existing route from the RIB. By Pass RFD for this
             * process. */
            Bgp4DshDelinkRouteFromChgList (pRtProfile);
            Bgp4DshAddRouteToList (BGP4_PEER_RCVD_ROUTES (pPeerEntry),
                                   pRtProfile, 0);
            Bgp4RibhDelRtEntry (pRtDampHist->pRtProfile, NULL,
                                BGP4_RT_CXT_ID (pRtDampHist->pRtProfile),
                                BGP4_TRUE);
            /* Set NO_RFD flag to by-pass RFD when the route is
             * processed as feasible route. */
            BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_NO_RFD);
            BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_DAMPED);
        }
        else
        {
            Bgp4DshReleaseRtInfo (pRtDampHist->pRtProfile);
            if (pRtDampHist->u1RtFlag != RFD_FEASIBLE_ROUTE)
            {
                BGP4_RT_RESET_FLAG (pRtDampHist->pRtProfile, BGP4_RT_HISTORY);
                Bgp4RibhDelRtEntry (pRtDampHist->pRtProfile, NULL,
                                    BGP4_RT_CXT_ID (pRtDampHist->pRtProfile),
                                    BGP4_HISTORY);
            }
        }
        Bgp4MemReleaseRfdDampHist (pRtDampHist);
    }
    return RFD_SUCCESS;

}

/**************************End of File ************************************/
#endif /* RFD_WANTED */
