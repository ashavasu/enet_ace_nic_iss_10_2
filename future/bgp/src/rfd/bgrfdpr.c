/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgrfdpr.c,v 1.13 2017/09/15 06:19:54 siva Exp $
 *
 * Description: This file contains Route Flap Dampening feature
 *              functions.
 *
 *******************************************************************/
#ifndef _RFDPEERHNDLR_C
#define _RFDPEERHNDLR_C
#endif
#ifdef RFD_WANTED
#include "bgp4com.h"
/****************************************************************************
     Function Name    : RfdPeersHandler
     Description      : This module handles damping history for peers. If 
                        peer changes its state from established to idle, this 
                        module invokes RfdPeerDownHandler (). If peer state 
                        changes to established, this module invokes 
                        RfdPeerUpHandler ()
     Input(s)         : pPeerInfo - This is a pointer to peer information 
                         whose damping structure has to be evaluated.
     Output(s)        : None
     Global Variables
     Referred         :  None 
     Global Variables
     Modified         :  None
     Exceptions or OS
     Err Handling     :  None
     Use of Recursion :  None

     Return(s)        :  RFD_SUCCESS or RFD_FAILURE
***************************************************************************/
INT4
RfdPeersHandler (tBgp4PeerEntry * pPeerInfo)
{
    INT4                i4RetSts;
    UINT4               u4Sts;
    /* Validate inputs */
    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tRfdPeersHandler() : INVALID PEER INFO\n");
        return RFD_FAILURE;
    }
    /* check for Valid peer remote address */
    u4Sts = Bgp4IsValidAddress (BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo),
                                BGP4_FALSE);
    if (u4Sts != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tRfdPeersHandler() : INVALID PEER REMOTE ADDRESS\n");
        return RFD_FAILURE;
    }

    /* Peer is up , peer up event should be processed */
    u4Sts = RFD_PEER_STATE (BGP4_PEER_CXT_ID (pPeerInfo), pPeerInfo);
    if (u4Sts == RFD_PEER_UP)
    {
        /* Process peer up event */
        i4RetSts = RfdPeerUpHandler (pPeerInfo);
        if (i4RetSts == RFD_FAILURE)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                           BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                           "\tDAMPING : Processing PEER UP Event for "
                           "Peer = %s FAILED!!!\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            return RFD_FAILURE;
        }
    }
    /* When peer goes down , peer down should be processed */
    else if (u4Sts == RFD_PEER_DOWN)
    {
        /* Process peer down event */
        i4RetSts = RfdPeerDownHandler (pPeerInfo);
        if (i4RetSts == RFD_FAILURE)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                           BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                           "\tDAMPING : Processing PEER DOWN Event for "
                           "Peer = %s FAILED!!!\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            return RFD_FAILURE;
        }
    }
    return RFD_SUCCESS;
}

/*****************************************************************************
     Function Name    : RfdPeerUpHandler
     Description      : This module updates damping history for peer when 
                        peer changes its state to established. This module 
                        advertises the routes that are present in the RIB, 
                        to the peer if it is not suppressed.
     Input(s)         : pPeerInfo -  Pointer to peer information whose 
                           damping structure has to be updated.
     Output(s)        : None
     Global Variables
     Referred         : None  
     Global Variables
     Modified         : None  
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
***************************************************************************/
INT4
RfdPeerUpHandler (tBgp4PeerEntry * pPeerInfo)
{
    tReusePeer         *pReusePeer = NULL;
    INT4                i4RetSts;
    UINT1               u1PeerState;
    UINT1               u1IsFreshPeer;
    UINT1               u1IsDampHistFreed = RFD_FALSE;

    /* Damping history should be updated */

    i4RetSts = RfdPUHDampHistHandler (pPeerInfo, &u1PeerState, &u1IsFreshPeer,
                                      &u1IsDampHistFreed);

    if (i4RetSts == RFD_FAILURE)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tDAMPING : UPDATING PEER - %s DAMPING "
                       "HISTORY FAILED !!!\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))));
        return RFD_FAILURE;
    }
    /* Peer is going to be suppressed  . If peer is suppressed, the routes 
     * in the RIB should not be advertised to this peer. It the peer be 
     * comes reusable, the normal peer up operation will be done */
    if (u1PeerState == RFD_SUPPRESS)
    {
        /* Peer is Suppressed. Insert peer into peers reuse list */
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_DAMP_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tDAMPING : Peer %s is DAMPED.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))));

        /* Insert peer into peers reuse list */
        i4RetSts = RfdInsertPeerReuseList (pPeerInfo);
        if (i4RetSts == RFD_FAILURE)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                           BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                           "\tDAMPING : Inserting PEER - %s "
                           "in PEER REUSE_LIST FAILED !!!\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            return RFD_FAILURE;
        }
    }

    /* Peer is going to be reused */
    else
    {
        if (u1PeerState == RFD_USABLE)
        {
            /* Peer is going to be reused */
            if ((u1IsFreshPeer != RFD_TRUE) && (u1IsDampHistFreed != RFD_TRUE))
            {
                /* Insert peer into peers reuse list */
                i4RetSts = RfdInsertPeerReuseList (pPeerInfo);
                if (i4RetSts == RFD_FAILURE)
                {
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                                   BGP4_TRC_FLAG,
                                   BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                                   BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                                   "\tDAMPING : Inserting PEER - %s "
                                   "in PEER REUSE_LIST FAILED !!!\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerInfo),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerInfo))));
                    return RFD_FAILURE;
                }
            }
        }

        if (u1IsFreshPeer != RFD_TRUE)
        {
            if ((u1PeerState == RFD_REUSE) || (u1IsDampHistFreed == RFD_TRUE))

            {
                /* Remove peer from peers reuse list */
                i4RetSts = RfdRemovePeerFromPeerReuseList (pPeerInfo,
                                                           &pReusePeer);
                if (i4RetSts == RFD_FAILURE)
                {
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                                   BGP4_TRC_FLAG,
                                   BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                                   BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                                   "\tDAMPING : Removing PEER - %s "
                                   "from PEER REUSE_LIST FAILED !!!\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerInfo),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerInfo))));
                    return RFD_FAILURE;
                }

                /* Peer becomes reusable, free the memory allocated for reuse */
                REUSE_PEER_ENTRY_FREE (pReusePeer);
            }
        }
    }
    return RFD_SUCCESS;
}

/****************************************************************************
     Function Name    : RfdRemovePeerFromPeerReuseList
     Description      : This module removes peer information from peers reuse 
                        list.
     Input(s)         : pPeerInfo - Pointer to peer information which is to 
                              be removed from peers reuse list.
     Output(s)        : ppReusePeer - Double pointer to reuse peer information 
                              that is removed from reuse list.
     Global Variables
     Referred         : RFD_PEERS_REUSE_LIST(BGP4_DFLT_VRFID) 
     Global Variables
     Modified         : RFD_PEERS_REUSE_LIST(BGP4_DFLT_VRFID)
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
****************************************************************************/
INT4
RfdRemovePeerFromPeerReuseList (tBgp4PeerEntry * pPeerInfo,
                                tReusePeer ** ppReusePeer)
{
    tReusePeer         *pPeerReuse = NULL;
    UINT4               u4Context = 0;

    /* Validate Inputs */
    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tRfdRemovePeerFromPeerReuseList() :INVALID PEER INFO\n");
        return RFD_FAILURE;
    }

    u4Context = BGP4_PEER_CXT_ID (pPeerInfo);

    RFD_GLOBAL_PARAMS (u4Context).pLastReuseList = NULL;

    *ppReusePeer = NULL;
    TMO_SLL_Scan (RFD_PEERS_REUSE_LIST (u4Context), pPeerReuse, tReusePeer *)
    {
        if ((PrefixMatch (RFD_PEER_INFO_IN_REUSE_PEER_STRUCT (pPeerReuse),
                          BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo))) == BGP4_TRUE)
        {
            *ppReusePeer = pPeerReuse;
            break;
        }
    }
    if (*ppReusePeer != NULL)
    {
        /*If entry found , Delete from peers reuse list */
        TMO_SLL_Delete (RFD_PEERS_REUSE_LIST (u4Context),
                        &pPeerReuse->NextReusePeer);
        return RFD_SUCCESS;
    }
    else
    {
        RfdFaultReportReceive (u4Context, RFD_PEER_RLIST_ENTRY_NOT_FOUND);
        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                  BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tDAMPING : PEER REUSE_LIST ENTRY is NOT PRESENT\n");
        return RFD_FAILURE;
    }
}

/****************************************************************************
     Function Name    : RfdInsertPeerReuseList
     Description      : This module inserts peer information into peers reuse 
                        list.
     Input(s)         : pPeerInfo - Pointer to peer information which is to 
                           be inserted into peers reuse list
     Output(s)        : None
     Global Variables
     Referred         : RFD_PEERS_REUSE_LIST(BGP4_DFLT_VRFID)
     Global Variables
     Modified         : RFD_PEERS_REUSE_LIST(BGP4_DFLT_VRFID)
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
*****************************************************************************/
INT4
RfdInsertPeerReuseList (tBgp4PeerEntry * pPeerInfo)
{
    tReusePeer         *pReusePeer = NULL;
    UINT4               u4Context = 0;

    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tRfdInsertPeerReuseList() : INVALID PEER INFO\n");
        return RFD_FAILURE;
    }

    u4Context = BGP4_PEER_CXT_ID (pPeerInfo);

    RFD_GLOBAL_PARAMS (u4Context).pLastReuseList = NULL;

    /*Validate input */
    /*If entry is already not present in peer reuse list the peer will 
     * be inserted in to this list */
    TMO_SLL_Scan (RFD_PEERS_REUSE_LIST (u4Context), pReusePeer, tReusePeer *)
    {
        if ((PrefixMatch (RFD_PEER_INFO_IN_REUSE_PEER_STRUCT (pReusePeer),
                          BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo))) == BGP4_TRUE)
        {
            pReusePeer->pPeerInfo = pPeerInfo;
            /* Peer Entry is already present in Peers Reuse List */
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                           BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                           "\tDAMPING : PEER - %s "
                           "ALREADY PRESENT in PEER REUSE_LIST\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            return RFD_SUCCESS;
        }
    }
    /* If entry is alredy not present in peers reuse list, the new entry 
     * will be created and added into peers reuse list */
    pReusePeer = (tReusePeer *) REUSE_PEER_ENTRY_CREATE ();
    if (pReusePeer == NULL)
    {
        RfdFaultReportReceive (u4Context, RFD_PEER_RLIST_INSERT_FAIL);
        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                  BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tDAMPING : CREATING REUSE PEER ENTRY FAILED !!!\n");
        return RFD_FAILURE;
    }
    TMO_SLL_Init_Node (&(pReusePeer->NextReusePeer));
    pReusePeer->pPeerInfo = pPeerInfo;
    Bgp4CopyAddrPrefixStruct (&
                              (RFD_PEER_INFO_IN_REUSE_PEER_STRUCT (pReusePeer)),
                              BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo));
    TMO_SLL_Add (RFD_PEERS_REUSE_LIST (u4Context), &pReusePeer->NextReusePeer);
    return RFD_SUCCESS;
}

/**************************************************************************
     Function Name    : RfdPUHDampHistHandler
     Description      : This module updates damping history for peer when 
                        peer goes to established state.
     Input(s)         : pPeerInfo - Pointer to peer information whose damping 
                            history has to be updated.
     Output(s)        : pu1PeerStatus - Pointer to peer status whether the peer 
                            is suppressed or not.
                      : pu1IsFreshPeer - Pointer to the status whether the peer
                            is fresh peer is or not      
              : pu1IsDampHistFreed -Pointer to the status whether 
                the peer damping history is freed or not.        
     Global Variables
     Referred         : PEERS_DAMP_HIST_LIST(BGP4_DFLT_VRFID)  
     Global Variables
     Modified         : None  
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
***************************************************************************/
INT4
RfdPUHDampHistHandler (tBgp4PeerEntry * pPeerInfo,
                       UINT1 *pu1PeerStatus, UINT1 *pu1IsFreshPeer,
                       UINT1 *pu1IsDampHistFreed)
{
    tPeerDampHist      *pPeerDampHist = NULL;
    INT4                i4RetSts;

    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tRfdPUHDampHistHandler() : INVALID PEER INFO\n");
        return RFD_FAILURE;
    }
    /* Fetch peer's damping history */
    i4RetSts = RfdFetchPeerDampHist (pPeerInfo, &pPeerDampHist);
    if (i4RetSts == RFD_FAILURE)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tDAMPING : Fetching PEER - %s "
                       "DAMPING HISTORY FAILED !!!\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))));
        return RFD_FAILURE;
    }
    /* If this is a first peer up, pPeerDampHist will be NULL. In this 
     * case no need to maintain damping history */
    if (pPeerDampHist == NULL)
    {
        *pu1PeerStatus = RFD_USABLE;
        *pu1IsFreshPeer = RFD_TRUE;

        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tDAMPING : PEER - %s HAS COME UP FIRST TIME\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))));
        return RFD_SUCCESS;
    }
    *pu1IsFreshPeer = RFD_FALSE;
    /* Update FOM */
    RfdPUHCalcPeerFOM (BGP4_PEER_CXT_ID (pPeerInfo), pPeerDampHist);

    /* Check FOM value. If FOM goes above cutoff and peer is not suppressed, 
     * suppress the peer. If peer is already suppressed, FOM goes below 
     * ireuse threshold, reuse the peer. Otherwise use the peer 
     */
    RfdPUHProcessPeerUp (BGP4_PEER_CXT_ID (pPeerInfo), pPeerDampHist,
                         pu1PeerStatus, pu1IsDampHistFreed);
    return RFD_SUCCESS;
}

/****************************************************************************
     Function Name    :  RfdPUHCalcPeerFOM
     Description      :  This module updates peer figure-of-merit value
     Input(s)         :  pPeerDampHist - Pointer to peer damping history whose 
                              FOM has to be updated.
     Output(s)        :  pPeerDampHist - Pointer to peer damping history 
                              whose FOM has been updated.
     Global Variables
     Referred         : RFD_DECAY_TIMER_GRANULARITY(BGP4_DFLT_VRFID), RFD_DECAY_ARRAY(BGP4_DFLT_VRFID)
     Global Variables
     Modified         : PEERS_DAMP_HIST_LIST(BGP4_DFLT_VRFID)   
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
***************************************************************************/
INT4
RfdPUHCalcPeerFOM (UINT4 u4CxtId, tPeerDampHist * pPeerDampHist)
{
    UINT4               u4CurrentTime;
    RFDFLOAT            DecayValue;
    UINT2               u2TimeDiff;
    UINT2               u2DecayIndx;

    if (pPeerDampHist == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tRfdPUHCalcPeerFOM() : INVALID PEER DAMPING HISTORY\n");
        return RFD_FAILURE;
    }
    /* Get the system time */
    RFD_GET_SYS_TIME (&u4CurrentTime);
    /* Find the time difference between current time and last FOM 
     * updated time */
    u2TimeDiff =
        RFD_GET_TIME_DIFF_SEC (u4CurrentTime, pPeerDampHist->u4PeerLastTime);
    u2DecayIndx = (UINT2) (u2TimeDiff / RFD_DECAY_TIMER_GRANULARITY (u4CxtId));
    /*If Decay index is greater than Decay Array Size, 
     * set the Fom as 0 */
    if (u2DecayIndx > RFD_DECAY_ARRAY_SIZE (u4CxtId))
    {
        pPeerDampHist->PeerFom = 0;
    }
    else
    {
        /*Otherwise FOM = FOM * Decay [u2DecayIndx] */
        DecayValue = RFD_DECAY_ARRAY (u4CxtId)[u2DecayIndx];
        pPeerDampHist->PeerFom = DecayValue * pPeerDampHist->PeerFom;
    }
    return RFD_SUCCESS;
}

/****************************************************************************
     Function Name    : RfdPUHProcessPeerUp
     Description      : This module processes peer damping history when peer 
                        goes to established state.
     Input(s)         : pPeerDampHist - Pointer to peer damping history
     Output(s)        : pu1PeerStatus -Pointer to the peer status which gives 
                        whether the peer is suppressed or not.
                      : pu1IsDampHistFreed -Pointer to the status which gives 
                        whether the damping history is freed or not.
     Global Variables
     Referred         : PEERS_DAMP_HIST_LIST(BGP4_DFLT_VRFID)
     Global Variables
     Modified         : PEERS_DAMP_HIST_LIST(BGP4_DFLT_VRFID)
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
****************************************************************************/
INT4
RfdPUHProcessPeerUp (UINT4 u4Context, tPeerDampHist * pPeerDampHist,
                     UINT1 *pu1PeerStatus, UINT1 *pu1IsDampHistFreed)
{
    UINT4               u4HashIndex;

    if (pPeerDampHist == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tRfdPUHProcessPeerUp() : INVALID PEER DAMPING "
                  "HISTORY\n");
        return RFD_FAILURE;
    }

    /* If peer is suppressed and the FOM is below reuse threshold, the peer 
     * can be reused. If the peer is not suppressed and the FOM goes above 
     * cutoff, the peer should be suppressed. Otherwise the peer can be 
     * used. 
     */
    if ((pPeerDampHist->u1PeerStatus != RFD_SUPPRESSED_STATE) &&
        (pPeerDampHist->PeerFom < RFD_CUTOFF_THRESHOLD (u4Context)))
    {
        *pu1PeerStatus = RFD_USABLE;
    }
    else if ((pPeerDampHist->u1PeerStatus == RFD_SUPPRESSED_STATE) &&
             (pPeerDampHist->PeerFom < RFD_REUSE_THRESHOLD (u4Context)))
    {
        pPeerDampHist->u1PeerStatus = RFD_UNSUPPRESSED_STATE;
        *pu1PeerStatus = RFD_REUSE;
        BGP4_TRC_ARG1 (&(pPeerDampHist->PeerRemoteAddr),
                       BGP4_TRC_FLAG, BGP4_DAMP_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tDAMPING : PEER - %s "
                       "PEER is UNSUPPRESSED \n",
                       Bgp4PrintIpAddr (pPeerDampHist->PeerRemoteAddr.
                                        au1Address,
                                        pPeerDampHist->PeerRemoteAddr.u2Afi));
    }
    else
    {
        pPeerDampHist->u1PeerStatus = RFD_SUPPRESSED_STATE;
        *pu1PeerStatus = RFD_SUPPRESS;
        BGP4_TRC_ARG1 (&(pPeerDampHist->PeerRemoteAddr),
                       BGP4_TRC_FLAG, BGP4_DAMP_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tDAMPING : PEER - %s "
                       "PEER is SUPPRESSED \n",
                       Bgp4PrintIpAddr (pPeerDampHist->PeerRemoteAddr.
                                        au1Address,
                                        pPeerDampHist->PeerRemoteAddr.u2Afi));
    }

    /* If peer FOM is greater than 0, update the time. otherwise 
     * dispose the peer damping history 
     */
    if (pPeerDampHist->PeerFom > 0)
    {
        RFD_GET_SYS_TIME (&(pPeerDampHist->u4PeerLastTime));
        *pu1IsDampHistFreed = RFD_FALSE;
    }
    else
    {
        *pu1IsDampHistFreed = RFD_TRUE;
        Bgp4GetPrefixHashIndex (RFD_PEER_INFO_IN_PEER_DAMPHIST_STRUCT
                                (pPeerDampHist), &u4HashIndex);
        TMO_HASH_Delete_Node (PEERS_DAMP_HIST_LIST (u4Context),
                              &(pPeerDampHist->NextPeerDampHist), u4HashIndex);
        if (PEER_DAMP_HIST_ENTRY_FREE (pPeerDampHist) == RFD_MEM_FAILURE)
        {
            RfdFaultReportReceive (u4Context, RFD_PEER_DAMP_HIST_DELETE_FAIL);
            BGP4_TRC (&(pPeerDampHist->PeerRemoteAddr),
                      BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                      BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tDAMPING : Deleting PEER DAMPING "
                      "HISTORY FAILED !!!\n");
            return RFD_FAILURE;
        }
    }
    return RFD_SUCCESS;
}

/***************************************************************************
     Function Name    :  RfdFetchPeerDampHist
     Description      :  This module fetches peer damping history from Peers 
                        damping history list.
     Input(s)         :  pPeerInfo - Pointer to peer information whose damping 
                          structure has to be fetched.
     Output(s         : ppPeerDampHist - Double Pointer to peer damping history
     Global Variables
     Referred         : PEERS_DAMP_HIST_LIST(BGP4_DFLT_VRFID)
     Global Variables
     Modified         : None  
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
******************************************************************************/
INT4
RfdFetchPeerDampHist (tBgp4PeerEntry * pPeerInfo,
                      tPeerDampHist ** ppPeerDampHist)
{
    tPeerDampHist      *pPeerHist = NULL;
    UINT4               u4HashIndex;
    UINT4               u4Context = 0;

    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tRfdFetchPeerDampHist() : INVALID PEER INFO\n");
        return RFD_FAILURE;
    }
    u4Context = BGP4_PEER_CXT_ID (pPeerInfo);
    if (PEERS_DAMP_HIST_LIST (u4Context) == NULL)
    {
        return RFD_FAILURE;
    }
    /* Get the HashIndex with the hash key as Peer Remote Address */
    Bgp4GetPrefixHashIndex (BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo),
                            &u4HashIndex);

    TMO_HASH_Scan_Bucket (PEERS_DAMP_HIST_LIST (u4Context), u4HashIndex,
                          pPeerHist, tPeerDampHist *)
    {
        if ((PrefixMatch (RFD_PEER_INFO_IN_PEER_DAMPHIST_STRUCT (pPeerHist),
                          BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo))) == BGP4_TRUE)
        {
            *ppPeerDampHist = pPeerHist;
            return RFD_SUCCESS;
        }
    }

    /* Peer Damping history is not present in Peer Damping list */
    *ppPeerDampHist = NULL;

    return RFD_SUCCESS;
}

/***************************************************************************
     Function Name    :  RfdPeerDownHandler
     Description      :  When Peer goes down, this module updates the damping
                          history for peer. If the peer is not suppressed, 
                          the routes learnt from this peer will be deleted 
                          from RIB and advertised as withdrawn routes to 
                          other peers.  The damping history of all the routes 
                          that are learnt from this peer will be deleted. 
                          These routes will be removed from routes reuse 
                          list and SupPeerRtsList
     Input(s)         :  pPeerInfo -  Pointer to peer information  whose 
                             damping structure has to be updated.
     Output(s         :  None
     Global Variables
     Referred         : PEERS_DAMP_HIST_LIST(BGP4_DFLT_VRFID) 
     Global Variables
     Modified         : None  
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
****************************************************************************/
INT4
RfdPeerDownHandler (tBgp4PeerEntry * pPeerInfo)
{
    tPeerDampHist      *pPeerDampHist = NULL;
    INT4                i4RetSts;
    UINT1               u1IsNewDampHist;

    /* Fetches the damping history if already present. Otherwise it 
     * creates new damping history for peer */

    i4RetSts = RfdPDHFetchCreatePeerDampHist (pPeerInfo, &pPeerDampHist,
                                              &u1IsNewDampHist);
    if (i4RetSts == RFD_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tDAMPING : FETCHING PEER DAMPING HISTORY " "FAILED !!!\n");
        return RFD_FAILURE;
    }
    /*If new Damp history, this is first time peer went down */
    if (u1IsNewDampHist != RFD_TRUE)
    {
        /* Updates figure-of-merit value */
        i4RetSts =
            RfdPDHCalcPeerFOM (BGP4_PEER_CXT_ID (pPeerInfo), pPeerDampHist);
        if (i4RetSts == RFD_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                      BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tDAMPING : CALCULATING PEER FOM " "FAILED !!!\n");
            return RFD_FAILURE;
        }
        /* Figure-of-merit value should be adjusted to ceiling */
        i4RetSts =
            RfdPDHPeerAdjustCeiling (BGP4_PEER_CXT_ID (pPeerInfo),
                                     pPeerDampHist);
    }
    /* Sync the peer damp FOM info to the Standby BGP instance */
    Bgp4RedSyncPeerDampMsg (BGP4_PEER_CXT_ID (pPeerInfo), pPeerDampHist);
    /* Insert the peer into reuse list. If the peer is not suppressed, the 
     * routes in the RIB that are learnt from this peer will be removed from 
     * RIB and advertised as withdrawn routes to other peers. Also the  
     * damping history of these routes will be disposed, these routes will 
     * be removed from routes reuse list.
     * If the peer is suppressed, the routes learnt from this peer that are 
     * stored in SupPeerRts list will be deleted */
    i4RetSts = RfdInsertPeerReuseList (pPeerInfo);
    if (i4RetSts == RFD_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tDAMPING : INSERTING PEER REUSE LIST FAILED !!!\n");
        return RFD_FAILURE;
    }
    if (pPeerDampHist->u1PeerStatus == RFD_SUPPRESSED_STATE)
    {
        /* Peer is in Suppressed State and no route from this peer
         * will be stored in RFD. So no need for any processing. */
    }
    else if (pPeerDampHist->u1PeerStatus == RFD_UNSUPPRESSED_STATE)
    {
        /* Need to dispose damping history of all the routes received
         * from this peer. However this will be taken care when the
         * peer routes are removed from the RIB. */
    }
    return RFD_SUCCESS;
}

/****************************************************************************
     Function Name    : RfdPDHFetchCreatePeerDampHist
     Description      : This module fetches the damping history if it presents 
                        in peer damping list. Otherwise it creates the new 
                        damping history for this peer.
     Input(s)         : pPeerInfo - Pointer to peer information
     Output(s)        : ppPeerDampHist - Double pointer to peer damping 
                            history  
                         pu1IsNewPeerDampHist - Pointer to the status whether 
                           the damping history is existing or newly created.
     Global Variables
     Referred         : PEERS_DAMP_HIST_LIST(BGP4_DFLT_VRFID) 
     Global Variables
     Modified         : None
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
*****************************************************************************/
INT4
RfdPDHFetchCreatePeerDampHist (tBgp4PeerEntry * pPeerInfo,
                               tPeerDampHist ** ppPeerDampHist,
                               UINT1 *pu1IsNewPeerDampHist)
{
    INT4                i4RetSts;
    UINT4               u4HashIndex;
    UINT4               u4Context;

    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tRfdPDHFetchCreatePeerDampHist() : INVALID PEER INFO\n");
        return RFD_FAILURE;
    }

    u4Context = BGP4_PEER_CXT_ID (pPeerInfo);
    /* Fetch peer damping history */
    i4RetSts = RfdFetchPeerDampHist (pPeerInfo, ppPeerDampHist);
    if (i4RetSts == RFD_FAILURE)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tDAMPING : FETCHING PEER %s "
                       "DAMPING HISTORY FAILED !!!\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))));
        return RFD_FAILURE;
    }
    /* If damping history is already not present, create new damping 
     * history */
    if (*ppPeerDampHist == NULL)
    {
        PEER_DAMP_HIST_ENTRY_CREATE (*ppPeerDampHist);
        if (*ppPeerDampHist == NULL)
        {
            RfdFaultReportReceive (u4Context,
                                   RFD_PEER_DAMP_HIST_MEM_ALLOC_FAIL);
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG,
                           BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                           BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                           "\tDAMPING : CREATING PEER %s "
                           "DAMPING HISTORY FAILED !!!\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            return RFD_FAILURE;
        }
        /* Initialize with the initial values */
        TMO_SLL_Init_Node (&((*ppPeerDampHist)->NextPeerDampHist));

        Bgp4CopyAddrPrefixStruct (&
                                  (RFD_PEER_INFO_IN_PEER_DAMPHIST_STRUCT
                                   ((*ppPeerDampHist))),
                                  BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo));

        (*ppPeerDampHist)->PeerFom = RFD_DEF_FOM;
        RFD_GET_SYS_TIME (&((*ppPeerDampHist)->u4PeerLastTime));
        (*ppPeerDampHist)->u1PeerStatus = RFD_UNSUPPRESSED_STATE;

        Bgp4GetPrefixHashIndex (BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo),
                                &u4HashIndex);

        /* Add damping history into Peer damping history hash table */
        TMO_HASH_Add_Node (PEERS_DAMP_HIST_LIST (u4Context),
                           &((*ppPeerDampHist)->NextPeerDampHist), u4HashIndex,
                           NULL);
        *pu1IsNewPeerDampHist = RFD_TRUE;
    }
    else
    {
        *pu1IsNewPeerDampHist = RFD_FALSE;
    }
    return RFD_SUCCESS;
}

/****************************************************************************
     Function Name    : RfdPDHCalcPeerFOM
     Description      : This module updates figure-of-merit value of peer, 
                        which is gone down.
     Input(s)         : pPeerDampHist - Pointer to peer damping history whose 
                              FOM has to be updated.
     Output(s)        : pPeerDampHist - Pointer to peer damping history whose 
                             FOM has been updated
     Global Variables
     Referred         : PEERS_DAMP_HIST_LIST(BGP4_DFLT_VRFID) 
     Global Variables
     Modified         : PEERS_DAMP_HIST_LIST(BGP4_DFLT_VRFID)
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
******************************************************************************/
INT4
RfdPDHCalcPeerFOM (UINT4 u4Context, tPeerDampHist * pPeerDampHist)
{
    RFDFLOAT            DecayValue;
    RFDFLOAT            PeerFOM;
    UINT4               u4CurrentTime;
    UINT2               u2TimeDiff;
    UINT2               u2DecayIndx;

    if (pPeerDampHist == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tRfdPDHCalcPeerFOM() : INVALID PEER DAMPING HISTORY\n");
        return RFD_FAILURE;
    }
    /* Get the system time */
    RFD_GET_SYS_TIME (&u4CurrentTime);
    /* Find the time difference between current time and last FOM 
     * updated time */
    u2TimeDiff =
        RFD_GET_TIME_DIFF_SEC (u4CurrentTime, pPeerDampHist->u4PeerLastTime);
    u2DecayIndx =
        (UINT2) (u2TimeDiff / RFD_DECAY_TIMER_GRANULARITY (u4Context));
    /*If Index goes beyond decay array size, the FOM will be set to 1000,
     * else calculate FOM */
    if (u2DecayIndx > RFD_DECAY_ARRAY_SIZE (u4Context))
    {
        pPeerDampHist->PeerFom = RFD_DEF_FOM;
        pPeerDampHist->u4PeerLastTime = u4CurrentTime;
    }
    else
    {
        DecayValue = RFD_DECAY_ARRAY (u4Context)[u2DecayIndx];
        PeerFOM = DecayValue * pPeerDampHist->PeerFom;
        pPeerDampHist->PeerFom = PeerFOM + RFD_DEF_FOM;
        pPeerDampHist->u4PeerLastTime = u4CurrentTime;
    }

    return RFD_SUCCESS;
}

/*****************************************************************************
     Function Name    : RfdPDHPeerAdjustCeiling
     Description      : This module adjusts figure-of-merit to ceiling if it 
                        crosses the ceiling value.
     Input(s)         : pPeerDampHist - Pointer to peer damping history whose 
                             FOM value has to be adjusted.
     Output(s)        : pPeerDampHist - Pointer to peer damping history whose 
                            FOM has been adjusted.
     Global Variables
     Referred         :  PEERS_DAMP_HIST_LIST(BGP4_DFLT_VRFID) 
     Global Variables
     Modified         :  PEERS_DAMP_HIST_LIST(BGP4_DFLT_VRFID)
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
*****************************************************************************/
INT4
RfdPDHPeerAdjustCeiling (UINT4 u4Context, tPeerDampHist * pPeerDampHist)
{

    if (pPeerDampHist == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tRfdPDHPeerAdjustCeiling() : INVALID PEER DAMPING "
                  "HISTORY\n");
        return RFD_FAILURE;
    }
    /*If FOM crosses ceiling value, adjust to cailing */
    if (pPeerDampHist->PeerFom > RFD_CEILING_VALUE (u4Context))
    {
        pPeerDampHist->PeerFom = RFD_CEILING_VALUE (u4Context);
    }
    return RFD_SUCCESS;
}

/**************************************************************************
     Function Name    : RfdGetPeerState
     Description      : This module gives the peer status. This status will 
                           be suppressed, reusable or usable.
     Input(s)         : pPeerInfo - Pointer to peer information whose peer 
                           status has to be given.
     Output(s)        : pu1PeerStatus - Pointer to peer status that gives the 
                          status whether the Peer is suppressed, usable or 
                           reusable.
     Global Variables
     Referred         :  PEER_DAMP_HIST_LIST 
     Global Variables
     Modified         : None  
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
***************************************************************************/
INT4
RfdGetPeerState (tBgp4PeerEntry * pPeerInfo, UINT1 *pu1PeerStatus)
{

    tPeerDampHist      *pPeerDampHist = NULL;
    INT4                i4RetSts;

    *pu1PeerStatus = RFD_UNSUPPRESSED_STATE;
    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tRfdGetPeerState() : INVALID PEER INFO\n");
        return RFD_FAILURE;
    }
    /* Fetch the Peer damping structure */
    i4RetSts = RfdFetchPeerDampHist (pPeerInfo, &pPeerDampHist);
    if (i4RetSts == RFD_FAILURE)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_DAMP_TRC | BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Fetching Peer Damping history failed.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))));
        return RFD_FAILURE;
    }
    /* pPeerDampHist will be NULL if there is no damping history for this 
     * peer.This is the first time the peer has come up */
    if (pPeerDampHist != NULL)
    {
        *pu1PeerStatus = pPeerDampHist->u1PeerStatus;
    }
    return RFD_SUCCESS;
}

/***************************************************************************
     Function Name    :  RfdClearPeerHistory
     Description      :  This function clears the complete information 
                         related to the given peer from RFD data structures.
     Input(s)         :  pPeerInfo -  Pointer to peer information  
     Output(s         :  None
     Global Variables
     Referred         :None 
     Global Variables
     Modified         : None  
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : BGP4_SUCCESS 
****************************************************************************/
INT4
RfdClearPeerHistory (tBgp4PeerEntry * pPeerInfo)
{
    tReusePeer         *pReusePeer = NULL;
    tPeerDampHist      *pPeerDampHist = NULL;
    UINT4               u4HashIndex = 0;
    INT4                i4Ret = RFD_FAILURE;

    /* Remove the Peer info from Peer Reuse List */
    RfdRemovePeerFromPeerReuseList (pPeerInfo, &pReusePeer);

    /* Free the memory allocated for reuse */
    if (pReusePeer != NULL)
    {
        REUSE_PEER_ENTRY_FREE (pReusePeer);
    }

    /* Remove the Peer info from Peer Damp Hist */
    i4Ret = RfdFetchPeerDampHist (pPeerInfo, &pPeerDampHist);
    if (pPeerDampHist != NULL)
    {
        Bgp4GetPrefixHashIndex (RFD_PEER_INFO_IN_PEER_DAMPHIST_STRUCT
                                (pPeerDampHist), &u4HashIndex);

        TMO_HASH_Delete_Node (PEERS_DAMP_HIST_LIST
                              (BGP4_PEER_CXT_ID (pPeerInfo)),
                              &(pPeerDampHist->NextPeerDampHist), u4HashIndex);
        PEER_DAMP_HIST_ENTRY_FREE (pPeerDampHist);
    }
    UNUSED_PARAM (i4Ret);        /* Fix for coverity warning. Since pPeerDampHist is 
                                   checked against NULL, i4Ret need not be checked. */
    return BGP4_SUCCESS;
}

/*************************** End of file ************************************/
#endif /* RFD_WANTED */
