/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgrfdwrt.c,v 1.14 2016/12/27 12:35:57 siva Exp $
 *
 * Description: This file contains Route Flap Dampening feature
 *              functions.
 *
 *******************************************************************/

#ifndef _RFDWDRTS_C
#define _RFDWDRTS_C
#endif
#ifdef RFD_WANTED
#include "bgp4com.h"
/****************************************************************************
     Function Name    :  RfdWithdrawnRoutesHandler
     Description      :  This module creates damping structure if 
                         the route is withdrawn first time. Otherwise it 
                         updates the existing routes damping history.
     Input(s)         :  pRtInfo - Pointer to withdrawn route profiles
                         pRouteRibNode  - Pointer to the Treenode in which the
                                          route is present. If this is NULL,
                                          then the route is no matching route
                                          is present in the RIB.
     Output(s)        : None
     Global Variables
     Referred         : None
     Global Variables
     Modified         : None   
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
*****************************************************************************/
INT4
RfdWithdrawnRoutesHandler (tRouteProfile * pRtInfo, VOID *pRouteRibNode)
{
    tRouteProfile      *pBestRtInfo = NULL;
    tRtDampHist        *pRtDampHist = NULL;
    tRouteProfile      *pFndRtProfileList = NULL;
    tRouteProfile      *pTempRoute = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4RetSts;
    INT4                i4Ret = BGP4_FAILURE;
    UINT1               u1AltRtState;
    UINT1               u1IsNewDampHist;

    pRibNode = pRouteRibNode;
    u1AltRtState = RFD_ALT_RT_NOT_EXIST;

    /* If damping history is already present, update route FOM.
     * Otherwise create new entry */
    pRtDampHist = BGP4_RT_DAMP_HIST (pRtInfo);

    i4RetSts = RfdWRHDampHistHandler (pRtInfo, BGP4_TRUE, &pRtDampHist,
                                      &u1IsNewDampHist);
    if (i4RetSts == RFD_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tDAMPING : UPDATING ROUTE DAMPING HISTORY FAILED !!!\n");
        return RFD_FAILURE;
    }
    if (u1IsNewDampHist != RFD_TRUE)
    {
        /* If this not first withdrawn, remove from routes 
         * reuse list */
        RfdRemoveRtFromRtsReuseList (pRtInfo, pRtDampHist);
    }

    /* Verify whether this route is the Best route or not. If Best route,
     * check whether an alternate route is present in the RIB for this
     * withdrawn route. If yes, then no need to send any alternate
     * route to this withdrawn route. Else if there is no alternate route
     * present, the route for the same destination having the 
     * minimum FOM should be used as alternate route. This alternate 
     * route should be removed from routes reuse list */

    /* Search the RIB tree */
    i4Ret = Bgp4RibhLookupRtEntry (pRtInfo, BGP4_RT_CXT_ID (pRtInfo),
                                   BGP4_TREE_FIND_EXACT, &pFndRtProfileList,
                                   &pRibNode);

    pBestRtInfo = Bgp4DshGetBestRouteFromProfileList (pFndRtProfileList,
                                                      BGP4_RT_CXT_ID (pRtInfo));
    if (pBestRtInfo != NULL)
    {
        if (pBestRtInfo == pRtInfo)
        {
            /* The withdrawn route was selected as the best route earlier
             * Check whether any alternative route is present or not.
             */
            pTempRoute = BGP4_RT_NEXT (pBestRtInfo);
            while (pTempRoute != NULL)
            {
                if (((BGP4_IS_ROUTE_VALID(pTempRoute) ==
                 BGP4_TRUE) &&
                            ((BGP4_RT_GET_FLAGS (pTempRoute) &
                              BGP4_RT_HISTORY) != BGP4_RT_HISTORY)) ||
                        ((pTempRoute != NULL) &&
                         (BGP4_RT_GET_FLAGS (pTempRoute) & BGP4_RT_DAMPED)))


            {
                /* Alternate route existes in RIB. */
                u1AltRtState = RFD_ALT_RT_EXIST;
            }
                pTempRoute = BGP4_RT_NEXT(pTempRoute);
            }
        }
        else
        {
            /* The withdrawn route is not the best route in RIB. */
            u1AltRtState = RFD_ALT_RT_EXIST;
        }
    }

    /*  insert route in routes reuse list */
    RfdInsertRtInRtsReuseList (pRtDampHist);
    UNUSED_PARAM (u1AltRtState);
    UNUSED_PARAM (i4Ret);        /* Fix for Coverity warning. Since pFndRtProfileList is
                                   checked against null, i4Ret need not be checked */
    return RFD_SUCCESS;
}

/****************************************************************************
     Function Name    : RfdWRHDampHistHandler
     Description      : This module updates damping history for withdrawn 
                        routes
     Input(s)         : pRtInfo - Pointer to route information whose damping 
                             structure has to be updated.
                        u1IsWithdRoute - Flag indicating whether the route is
                                         a withdrawn route or replacement
                                         route. If BGP4_TRUE, then the route
                                         is withdrawn route else the route
                                         is Replacment route.
                         ppRtDampHist  - Double pointer to the route damping 
                                         history. NULL if damping history
                                         does not exist earlier else points
                                         to the existing damping history.
     Output(s)        : ppRtDampHist  - Double pointer to the route damping 
                             history.
                        pu1IsNewDampHist -Pointer to the status whether route 
                             damping structure is newly created or already 
                             existing.
     Global Variables
     Referred         : RT_DAMP_HIST_LIST  
     Global Variables
     Modified         : None  
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
****************************************************************************/
INT4
RfdWRHDampHistHandler (tRouteProfile * pRtInfo, UINT1 u1IsWithdRoute,
                       tRtDampHist ** ppRtDampHist, UINT1 *pu1IsNewDampHist)
{
    INT4                i4RetSts = RFD_FAILURE;
    RFDFLOAT            ReuseTempVal =0;

    /* Update damping history if present already. Otherwise create 
     * new entry */
    i4RetSts = RfdWRHUpdateCreateRtDampHist (pRtInfo, u1IsWithdRoute,
                                             ppRtDampHist, pu1IsNewDampHist);
    if (i4RetSts == RFD_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tDAMPING : FETCHING ROUTE DAMPING HISTORY FAILED!!!\n");
        return RFD_FAILURE;
    }

    /*If it is new entry, no need to update again */
    if (*pu1IsNewDampHist == RFD_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tDAMPING : NEW DAMPING HISTORY CREATED\n");
        BGP4_RT_RESET_FLAG (pRtInfo, BGP4_RT_DAMPED);
    }
    else
    {

    /*Calculate route FOM */
    RfdWRHCalcRtFOM (*ppRtDampHist, u1IsWithdRoute);

    /* Adjust FOM to ceiling if it crosses ceiling */
    RfdWRHRtAdjustCeiling (*ppRtDampHist);
    }
    if((*ppRtDampHist)->u1RtStatus == RFD_SUPPRESSED_STATE)
    {
        ReuseTempVal = RFD_REUSE_THRESHOLD(BGP4_RT_CXT_ID((*ppRtDampHist)->pRtProfile));
    }
    else
    {
    /*Calculate reuse time*/
    ReuseTempVal = (RFDFLOAT)RFD_REUSE_THRESHOLD(BGP4_RT_CXT_ID((*ppRtDampHist)->pRtProfile)) / 2 ;
    }
    ReuseTempVal = RFD_NAT_LOG((RFDFLOAT)ReuseTempVal / (RFDFLOAT)((*ppRtDampHist))->RtFom );
    ReuseTempVal = ((RFDFLOAT)ReuseTempVal / RFD_NAT_LOG(0.5));
    ReuseTempVal =  ((RFDFLOAT)ReuseTempVal * 
                    (RFDFLOAT)RFD_DECAY_HALF_LIFE_TIME
                     (BGP4_RT_CXT_ID((*ppRtDampHist)->pRtProfile)));
    if (ReuseTempVal > RFD_MAX_HOLD_DOWN_TIME ((BGP4_RT_CXT_ID((*ppRtDampHist)->pRtProfile))))
    {
        ReuseTempVal = RFD_MAX_HOLD_DOWN_TIME ((BGP4_RT_CXT_ID((*ppRtDampHist)->pRtProfile)));
    }

    Bgp4TmrhStopTimer(BGP4_ROUTE_REUSE_TIMER,(VOID *)(*ppRtDampHist));
    MEMSET (&((*ppRtDampHist)->RtReuseTmr),0,sizeof (tBgp4AppTimer));
    (*ppRtDampHist)->RtReuseTmr.u4TimerId = BGP4_ROUTE_REUSE_TIMER;
    Bgp4TmrhStartTimer (BGP4_ROUTE_REUSE_TIMER ,
            (VOID *)(*ppRtDampHist), ReuseTempVal);

    if (u1IsWithdRoute == BGP4_TRUE)
    {
        /* Explicit withdrawn Route. */
        BGP4_RT_SET_FLAG (pRtInfo, BGP4_RT_HISTORY);
    }

    if ((*ppRtDampHist)->u1RtStatus == RFD_SUPPRESSED_STATE)
    {
        /* Since the route is suppressed, update the flag to indiacate this
         * behaviour. */
        BGP4_RT_SET_FLAG (pRtInfo, BGP4_RT_DAMPED);
        BGP4_RFD_FEAS_RTPROFILE_CNT (BGP4_RT_CXT_ID (pRtInfo))--;
    }
    else
    {
        BGP4_RT_RESET_FLAG (pRtInfo, BGP4_RT_DAMPED);
    }

    return RFD_SUCCESS;
}

/****************************************************************************
     Function Name    : RfdWRHUpdateCreateRtDampHist
     Description      : If the input Damping history is non-NULL, this
                        module updates the exising Damping history. Else
                        creates the new damping structure.
                        For the explicit withdrawn route the FOM is set as
                        RFD_DEF_FOM. But however Replacement route is not
                        damped aggresively and FOM is set as (RFD_DEF_FOM/2).
     Input(s)         : pRtInfo - Pointer to route information 
                        u1IsWithdRoute - Flag indicating whether the route is
                                         a withdrawn route or replacement
                                         route. If BGP4_TRUE, then the route
                                         is withdrawn route else the route
                                         is Replacment route.
                        ppRtDampHist - Double pointer to route damping 
                                       history
     Output(s)        : ppRtDampHist - Double pointer to route damping 
                                       history
                        pu1IsNewDampHist - Pointer to the status whether the 
                              damping structure is already present or newly 
                              created.
     Global Variables
     Referred         : None 
     Global Variables
     Modified         : None 
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
***************************************************************************/
INT4
RfdWRHUpdateCreateRtDampHist (tRouteProfile * pRtInfo, UINT1 u1IsWithdRoute,
                              tRtDampHist ** ppRtDampHist,
                              UINT1 *pu1IsNewDampHist)
{
    UINT4               u4RtTempFlapTime = 0;
    UINT4               u4Context = BGP4_RT_CXT_ID (pRtInfo);

    if (*ppRtDampHist == NULL)
    {
        /*if damping history is not present, create new entry */

        *(ppRtDampHist) = Bgp4MemAllocateRfdDampHist (sizeof (tRtDampHist));
        if (*(ppRtDampHist) == NULL)
        {
            RfdFaultReportReceive (u4Context, RFD_RT_DAMP_HIST_MEM_ALLOC_FAIL);
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tDAMPING : CREATING NEW RT DAMPING HISTORY "
                      "FAILED !!!\n");
            return RFD_FAILURE;
        }
        (*ppRtDampHist)->RtFom =
            (u1IsWithdRoute == BGP4_TRUE) ? RFD_DEF_FOM : (RFD_DEF_FOM / 2);
        RFD_GET_SYS_TIME (&((*ppRtDampHist)->u4RtLastTime));
        RFD_GET_SYS_TIME (&u4RtTempFlapTime);
        u4RtTempFlapTime = (u4RtTempFlapTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
        ((*ppRtDampHist)->u4RtFlapTime) = u4RtTempFlapTime;
        (*ppRtDampHist)->u4RtFlapCount = ((*ppRtDampHist)->u4RtFlapCount + 1);
        (*ppRtDampHist)->pRtProfile = pRtInfo;
        (*ppRtDampHist)->u1RtStatus = RFD_UNSUPPRESSED_STATE;
        (*ppRtDampHist)->u1RtFlag = (u1IsWithdRoute == BGP4_TRUE) ?
            RFD_UNFEASIBLE_ROUTE : RFD_FEASIBLE_ROUTE;
        (*ppRtDampHist)->u2ReuseIndex = RFD_INVALID_INDX;
        *pu1IsNewDampHist = RFD_TRUE;
        /* Update the route with the Damp History */
        BGP4_RT_DAMP_HIST (pRtInfo) = (*ppRtDampHist);
        BGP4_RT_REF_COUNT (pRtInfo)++;
        BGP4_TRC_ARG5 (&
                       (BGP4_PEER_REMOTE_ADDR_INFO
                        (BGP4_RT_PEER_ENTRY (pRtInfo))), BGP4_TRC_FLAG,
                       BGP4_DAMP_TRC | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tDAMPING : Created Damping History for "
                       "Route %s Mask %s from PEER %s,FOM is %d Flapped %d time(s) \n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtInfo),
                                        BGP4_RT_AFI_INFO (pRtInfo)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtInfo),
                                        BGP4_RT_AFI_INFO (pRtInfo)),
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                        (BGP4_RT_PEER_ENTRY (pRtInfo)),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (BGP4_RT_PEER_ENTRY (pRtInfo)))),
                       (UINT4) BGP4_CEIL ((*ppRtDampHist)->RtFom),
                       ((*ppRtDampHist)->u4RtFlapCount));

    }                            /* if (*ppRtDampHist == NULL) */
    else
    {
        /* Damping History Already Exists. In case of Feasible route
         * first Unlink the Old route info from the Damp History and
         * then link the new route info with the Damp History.
         */
        if (u1IsWithdRoute == BGP4_FALSE)
        {
            BGP4_RT_DAMP_HIST ((*ppRtDampHist)->pRtProfile) = NULL;
            Bgp4DshReleaseRtInfo ((*ppRtDampHist)->pRtProfile);
            (*ppRtDampHist)->pRtProfile = pRtInfo;
            BGP4_RT_DAMP_HIST (pRtInfo) = (*ppRtDampHist);
            BGP4_RT_REF_COUNT (pRtInfo)++;
        }
        (*ppRtDampHist)->u1RtFlag = (u1IsWithdRoute == BGP4_TRUE) ?
            RFD_UNFEASIBLE_ROUTE : RFD_FEASIBLE_ROUTE;
        (*ppRtDampHist)->u4RtFlapCount = ((*ppRtDampHist)->u4RtFlapCount + 1);
        *pu1IsNewDampHist = RFD_FALSE;
        BGP4_TRC_ARG3 (&
                       (BGP4_PEER_REMOTE_ADDR_INFO
                        (BGP4_RT_PEER_ENTRY (pRtInfo))), BGP4_TRC_FLAG,
                       BGP4_DAMP_TRC | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tDAMPING : Updated Damping History for "
                       "Route %s Mask %s from PEER %s\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtInfo),
                                        BGP4_RT_AFI_INFO (pRtInfo)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRtInfo),
                                        BGP4_RT_AFI_INFO (pRtInfo)),
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                        (BGP4_RT_PEER_ENTRY (pRtInfo)),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (BGP4_RT_PEER_ENTRY (pRtInfo)))));
    }

    return RFD_SUCCESS;
}                                /* End of RfdWRHUpdateCreateRtDampHist */

/***************************************************************************
     Function Name    : RfdWRHCalcRtFOM
     Description      : This module updates figure-of-merit value for 
                        withdrawn routes or replacement route. For the
                        explicit withdrawn route the FOM is increased by
                        RFD_DEF_FOM. But however Replacement route is not
                        damped aggresively and FOM is increased by
                        (RFD_DEF_FOM/2).
     Input(s)         : pRtDampHist - Pointer to route damping history whose 
                                      FOM has to be updated.
                        u1IsWithdRoute - Flag indicating whether the route is
                                         a withdrawn route or replacement
                                         route. If BGP4_TRUE, then the route
                                         is withdrawn route else the route
                                         is Replacment route.
     Output(s)        : pRtDampHist - Pointer to route damping history whose 
                            damping structure has been updated.
     Global Variables
     Referred         : RFD_DECAY_TIMER_GRANULARITY(BGP4_DFLT_VRFID),
                        RFD_DECAY_ARRAY(BGP4_DFLT_VRFID)
     Global Variables
     Modified         : None 
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
***************************************************************************/
INT4
RfdWRHCalcRtFOM (tRtDampHist * pRtDampHist, UINT1 u1IsWithdRoute)
{
    RFDFLOAT            FomTempVal;
    UINT4               u4FOM = 0;
    UINT4               u4CurrentTime;
    UINT2               u2TimeDiff;

    /*Get the system time */
    RFD_GET_SYS_TIME (&u4CurrentTime);

    /*find the time difference between current time and last FOM 
     * updated time */
    u2TimeDiff =
        RFD_GET_TIME_DIFF_SEC (u4CurrentTime, pRtDampHist->u4RtLastTime);
    /* Calculate the FOM to be added. */
    u4FOM = (u1IsWithdRoute == BGP4_TRUE) ? RFD_DEF_FOM : (RFD_DEF_FOM / 2);

    FomTempVal = (RFDFLOAT)u2TimeDiff/ 
                ((RFDFLOAT)RFD_DECAY_HALF_LIFE_TIME(BGP4_RT_CXT_ID(pRtDampHist->pRtProfile)));
    FomTempVal = RFD_POW (0.5, FomTempVal);

    pRtDampHist->RtFom = ((pRtDampHist->RtFom) * FomTempVal);
    pRtDampHist->RtFom =pRtDampHist->RtFom + u4FOM;
        pRtDampHist->u4RtLastTime = u4CurrentTime;

    if (pRtDampHist->u1RtStatus == RFD_SUPPRESSED_STATE)
    {
        BGP4_TRC_ARG2 (&
                       (BGP4_PEER_REMOTE_ADDR_INFO
                        (pRtDampHist->pRtProfile->pPEPeer)), BGP4_TRC_FLAG,
                       BGP4_DAMP_TRC | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       " DAMPING : Updated FOM is %d flapped %d time(s) route is SUPPRESSED \n",
                       ((UINT4) pRtDampHist->RtFom),
                       pRtDampHist->u4RtFlapCount);
    }
    else
    {
        BGP4_TRC_ARG2 (&
                       (BGP4_PEER_REMOTE_ADDR_INFO
                        (pRtDampHist->pRtProfile->pPEPeer)), BGP4_TRC_FLAG,
                       BGP4_DAMP_TRC | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       " DAMPING : Updated FOM is %d flapped %d time(s) route is UNSUPPRESSED \n",
                       ((UINT4) pRtDampHist->RtFom),
                       pRtDampHist->u4RtFlapCount);
    }
    /* if(u2DecayIndx >=0) */

    if (pRtDampHist->u1RtStatus == RFD_SUPPRESSED_STATE)
    {
        BGP4_TRC_ARG2 (&
                       (BGP4_PEER_REMOTE_ADDR_INFO
                        (pRtDampHist->pRtProfile->pPEPeer)), BGP4_TRC_FLAG,
                       BGP4_DAMP_TRC | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       " DAMPING : Updated FOM is %d flapped %d time(s) route is SUPPRESSED \n",
                       ((UINT4) pRtDampHist->RtFom),
                       pRtDampHist->u4RtFlapCount);
    }
    else
    {
        BGP4_TRC_ARG2 (&
                       (BGP4_PEER_REMOTE_ADDR_INFO
                        (pRtDampHist->pRtProfile->pPEPeer)), BGP4_TRC_FLAG,
                       BGP4_DAMP_TRC | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       " DAMPING : Updated FOM is %d flapped %d time(s) route is UNSUPPRESSED \n",
                       ((UINT4) pRtDampHist->RtFom),
                       pRtDampHist->u4RtFlapCount);
    }
    /* if(u2DecayIndx >=0) */
    return RFD_SUCCESS;
}                                /* End of RfdWRHCalcRtFOM */

/****************************************************************************
     Function Name    : RfdWRHRtAdjustCeiling
     Description      : This module adjusts figure-of-merit to ceiling value 
                        if it goes above the ceiling value.
     Input(s)         : pRtDampHist - Pointer to route damping history whose 
                            FOM has to be adjusted. 
     Output(s)        : pRtDampHist - Pointer to route damping history whose 
                            FOM has been adjusted if it crosses ceiling.
     Global Variables
     Referred         : RFD_CEILING_VALUE(BGP4_DFLT_VRFID)
     Global Variables
     Modified         : None 
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
****************************************************************************/
INT4
RfdWRHRtAdjustCeiling (tRtDampHist * pRtDampHist)
{
    /* If figure-of-merit exceeds ceiling value, it should be adjusted to 
     * ceiling value */
    if (pRtDampHist->RtFom >
        RFD_CEILING_VALUE (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile)))
    {
        pRtDampHist->RtFom =
            RFD_CEILING_VALUE (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile));
    }
    return RFD_SUCCESS;
}                                /* End of RfdWRHRtAdjustCeiling */

/***************************** End of File ********************************/
#endif /* RFD_WANTED */
