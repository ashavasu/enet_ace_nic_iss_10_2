/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgrfdru.c,v 1.16 2017/09/15 06:19:54 siva Exp $
 *
 * Description: This file contains Route Flap Dampening feature
 *              functions.
 *
 *******************************************************************/
#ifndef _BGRFDRU_C
#define _BGRFDRU_C
#endif
#ifdef RFD_WANTED
#include "bgp4com.h"
/*****************************************************************************
     Function Name    : RfdReuseListsHandler
     Description      : On Reuse Timer expiry, Bgp4TimerProcessTimeout invokes 
                        this module. This module invokes other modules to 
                        update the damping history for routes presents in 
                        routes reuse list and peers presents in peers reuse 
                        list.
     Input(s)         : None
     Output(s)        : None
     Global Variables
     Referred         : None 
     Global Variables
     Modified         : None  
     Exceptions or OS
     Err Handling     :  None
     Use of Recursion :  None

     Return(s)        :  RFD_SUCCESS or RFD_FAILURE

******************************************************************************/
INT4
RfdReuseListsHandler (UINT4 u4ContextId)
{
    INT4                i4RetSts;

    /* Peers present in peers reuse list should be processed */
    i4RetSts = RfdPeersReuseListHandler (u4ContextId);
    if (i4RetSts == RFD_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "DAMPING : Failure in processing peers reuse list for context %d\n",
                  u4ContextId);
    }

    return RFD_SUCCESS;
}                                /* End of RfdReuseListsHandler */

/****************************************************************************
     Function Name    : RfdPeersReuseListHandler
     Description      : This module updates damping history for peers present 
                        in peers reuse list.
     Input(s)         : None
     Output(s         : None
     Global Variables
     Referred         : RFD_PEERS_REUSE_LIST(BGP4_DFLT_VRFID) 
     Global Variables
     Modified         : None  
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
****************************************************************************/
INT4
RfdPeersReuseListHandler (UINT4 u4ContextId)
{
    tReusePeer         *pReusePeer = NULL;
    tReusePeer         *pNextReusePeer = NULL;
    tReusePeer         *pNextTmpReusePeer = NULL;
    tBgp4PeerEntry     *pNextPeerInfo = NULL;
    tPeerDampHist      *pPeerDampHist = NULL;
    UINT4               u4HashIndex;
    INT4                i4RetSts;
    UINT1               u1PeerState;

    /* Fetch peer information from peers reuse list */
    RfdPRLFetchPeerReuseList (u4ContextId, pReusePeer, &pNextReusePeer);
    while (pNextReusePeer != NULL)
    {
        pNextPeerInfo = pNextReusePeer->pPeerInfo;
        /* Updates damping history */
        i4RetSts = RfdPRLDampHistHandler (pNextPeerInfo, &pPeerDampHist,
                                          &u1PeerState);
        if (i4RetSts == RFD_FAILURE)
        {
            BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pNextPeerInfo)),
                      BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "DAMPING : Failure in updating peer damping history\n");
            return RFD_FAILURE;
        }

        /* Peer can be reused. If peer becomes reusable, the peer should be 
           removed from peers reuse list and the routes learnt from this peer 
           will be treated as fresh routes if the peer is up. Otherwise the 
           damping history of this peer will be disposed. */

        pReusePeer = pNextReusePeer;

        /* Fetches next peer information in peers reuse list */
        RfdPRLFetchPeerReuseList (u4ContextId, pReusePeer, &pNextTmpReusePeer);

        if (u1PeerState == RFD_REUSE)
        {
            /* Remove peer from peer's reuse list */
            i4RetSts = RfdRemovePeerFromPeerReuseList (pNextPeerInfo,
                                                       &pNextReusePeer);

            if (i4RetSts == RFD_FAILURE)
            {
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pNextPeerInfo)),
                          BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          " DAMPING : Failure in removing peer reuse list\n");
                return RFD_FAILURE;
            }
            REUSE_PEER_ENTRY_FREE (pNextReusePeer);
            if (pPeerDampHist->u1PeerStatus == RFD_SUPPRESSED_STATE)
            {

                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pNextPeerInfo)),
                               BGP4_TRC_FLAG, BGP4_DAMP_TRC |
                               BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                               "\tDAMPED Peer %s becomes reusable.\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pNextPeerInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pNextPeerInfo))));
                pPeerDampHist->u1PeerStatus = RFD_UNSUPPRESSED_STATE;
                /*If peer becomes reusable, process the routes learnt from 
                 * this peer */
                RfdReusePeerRtsHandler (pNextPeerInfo, pPeerDampHist);
            }
            else
            {
                Bgp4GetPrefixHashIndex (BGP4_PEER_REMOTE_ADDR_INFO
                                        (pNextPeerInfo), &u4HashIndex);
                TMO_HASH_Delete_Node (PEERS_DAMP_HIST_LIST
                                      (BGP4_PEER_CXT_ID (pNextPeerInfo)),
                                      &(pPeerDampHist->NextPeerDampHist),
                                      u4HashIndex);
                PEER_DAMP_HIST_ENTRY_FREE (pPeerDampHist);
            }

        }                        /* if (u1PeerState == RFD_REUSE) */
        pNextReusePeer = pNextTmpReusePeer;
    }                            /* End of while (pNextReusePeer != NULL)        */

    return RFD_SUCCESS;
}                                /* End of  RfdPeersReuseListHandler */

/****************************************************************************
     Function Name    : RfdPRLFetchPeerReuseList
     Description      : This module fetches reuse peer information next to 
                        pReusePeer from peers reuse list
     Input(s)         : pReusePeer -   Pointer to reuse peer information 
     Output(s)        : ppNextReusePeer - Double pointer to next reuse peer 
                         information
     Global Variables
     Referred         : RFD_PEERS_REUSE_LIST(BGP4_DFLT_VRFID) 
     Global Variables
     Modified         : None  
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
****************************************************************************/
INT4
RfdPRLFetchPeerReuseList (UINT4 u4ContextId, tReusePeer * pReusePeer,
                          tReusePeer ** ppNextReusePeer)
{
    /*If preusePeer is NULL, get the first entry in peers reuse list */
    if (pReusePeer == NULL)
    {
        *ppNextReusePeer =
            (tReusePeer *) (TMO_SLL_First (RFD_PEERS_REUSE_LIST (u4ContextId)));
    }
    /* otherwise get the next entry to pReusePeer */
    else
    {
        *ppNextReusePeer =
            (tReusePeer *) (TMO_SLL_Next (RFD_PEERS_REUSE_LIST (u4ContextId),
                                          &(pReusePeer->NextReusePeer)));
    }
    return RFD_SUCCESS;
}                                /* End of RfdPRLFetchPeerReuseList */

/***************************************************************************
     Function Name    : RfdPRLDampHistHandler
     Description      : This module handles damping history for peers that 
                        are in the peers reuse list.
     Input(s)         : pPeerInfo - Pointer to peer information  whose 
                             damping history has to updated.
     Output(s)        : ppPeerDampHist  -Double pointer to peer damping 
                              history.
                        pu1PeerState - Pointer to peer state that tells whether
                              the peer is suppressed or not.
     Global Variables
     Referred         : PEERS_DAMP_HIST_LIST(BGP4_DFLT_VRFID)
     Global Variables
     Modified         : None  
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
****************************************************************************/
INT4
RfdPRLDampHistHandler (tBgp4PeerEntry * pPeerInfo,
                       tPeerDampHist ** ppPeerDampHist, UINT1 *pu1PeerState)
{
    INT4                i4RetSts;

    /* Fetch the peer damping history */
    i4RetSts = RfdFetchPeerDampHist (pPeerInfo, ppPeerDampHist);
    if (i4RetSts == RFD_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "DAMPING : Failure in fetching peer damping history\n");
        return RFD_FAILURE;
    }
    /* Calculate peer FOM */
    i4RetSts =
        RfdPRLCalcPeerFOM (BGP4_PEER_CXT_ID (pPeerInfo), *ppPeerDampHist);
    if (i4RetSts == RFD_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "DAMPING : Failure in calculating peer's FOM\n");
        return RFD_FAILURE;
    }
    /* Check for reusability of peer */
    i4RetSts =
        RfdPRLReuseCheck (BGP4_PEER_CXT_ID (pPeerInfo), *ppPeerDampHist,
                          pu1PeerState);

    return RFD_SUCCESS;
}                                /* End of RfdPRLDampHistHandler */

/****************************************************************************
     Function Name    : RfdPRLCalcPeerFOM
     Description      : This module updates peer figure-of-merit from current 
                        figure-of-merit for peers that are in the peers reuse 
                        list.
     Input(s)         : pPeerDampHist -  Pointer to peer damping history 
                           whose FOM has to be updated.
     Output(s)        : pPeerDampHist -  Pointer to peer damping history whose 
                          FOM has been updated.
     Global Variables
     Referred         :  RFD_DECAY_TIMER_GRANULARITY(BGP4_DFLT_VRFID), RFD_DECAY_ARRAY(BGP4_DFLT_VRFID)
     Global Variables
     Modified         :  PEERS_DAMP_HIST_LIST(BGP4_DFLT_VRFID) 
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
*****************************************************************************/
INT4
RfdPRLCalcPeerFOM (UINT4 u4CxtId, tPeerDampHist * pPeerDampHist)
{

    RFDFLOAT            DecayValue;
    UINT4               u4CurrentTime;
    UINT2               u2TimeDiff;
    UINT2               u2DecayIndx;

    if (pPeerDampHist == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  " RfdPRLCalcPeerFOM: Invalid peer damping history \n");
        return RFD_FAILURE;
    }
    /*Get the current time */
    RFD_GET_SYS_TIME (&u4CurrentTime);
    /*Find the time difference between current time and last FOM 
       updated time */
    u2TimeDiff =
        RFD_GET_TIME_DIFF_SEC (u4CurrentTime, pPeerDampHist->u4PeerLastTime);
    u2DecayIndx = (UINT2) (u2TimeDiff / RFD_DECAY_TIMER_GRANULARITY (u4CxtId));
    /*u2DecayIndx is out off the end of decay array , set FOM as 0 */
    if (u2DecayIndx > RFD_DECAY_ARRAY_SIZE (u4CxtId))
    {
        pPeerDampHist->PeerFom = 0;
        pPeerDampHist->u4PeerLastTime = u4CurrentTime;
    }
    else
    {
        /* Otherwise FOM = FOM * Decay [u2TimeDiff] */
        DecayValue = RFD_DECAY_ARRAY (u4CxtId)[u2DecayIndx];
        pPeerDampHist->PeerFom = (pPeerDampHist->PeerFom) * DecayValue;
        pPeerDampHist->u4PeerLastTime = u4CurrentTime;
    }                            /* if(u2DecayIndx >=0) */

    return RFD_SUCCESS;
}                                /* End of RfdPRLCalcPeerFOM */

/****************************************************************************
     Function Name    :  RfdPRLReuseCheck
     Description      :  This module compares the figure of merit against Resue 
                        threshold. If figure of merit is below reuse threshold, 
                        this sets pu1PeerState as RFD_REUSE.
     Input(s)         :  pPeerDampHist -Pointer to peer damping history whose 
                          FOM has to be checked.
     Output(s)        :  pu1PeerState - Pointer to peer state that gives the 
                          status whether the peer is suppressed, usable or 
                          reusable.
     Global Variables
     Referred         : RFD_REUSE_THRESHOLD(BGP4_DFLT_VRFID) , PEERS_DAMP_HIST_LIST(BGP4_DFLT_VRFID)  
     Global Variables
     Modified         : None
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
*****************************************************************************/
INT4
RfdPRLReuseCheck (UINT4 u4CxtId, tPeerDampHist * pPeerDampHist,
                  UINT1 *pu1PeerState)
{

    if (pPeerDampHist == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "RfdPRLReuseCheck : Invalid peer damping history \n");
        return RFD_FAILURE;
    }
    /* If FOM is below reuse threshold, the peer can be reused */
    if (pPeerDampHist->PeerFom < (RFD_REUSE_THRESHOLD (u4CxtId)))
    {
        *pu1PeerState = RFD_REUSE;
    }
    else
    {
        *pu1PeerState = RFD_SUPPRESS;
    }
    return RFD_SUCCESS;
}                                /* End of RfdPRLReuseCheck */

/***************************************************************************
     Function Name    : RfdSupPeerReusePendHandler
     Description      : This module checks whether it is possible to initiate
                        the Reuse handler for the previously suppressed peer
                        or not.                                            
     Input(s)         : pPeerEntry - Pointer to the peer information which 
                           becomes reusable.
     Output(s)        : None
     Global Variables
     Referred         : BGP4_SUP_PEER_REUSE_LIST 
     Global Variables
     Modified         : None.                   
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
****************************************************************************/
INT4
RfdSupPeerReusePendHandler (tBgp4PeerEntry * pPeerEntry)
{
    tPeerNode          *pSupPeerReuse = NULL;

    switch (BGP4_GET_PEER_CURRENT_STATE (pPeerEntry))
    {
        case BGP4_PEER_READY:
            /* Add the peer to the suppressed-peer-reuse list */
            BGP_PEER_NODE_CREATE (pSupPeerReuse);
            if (pSupPeerReuse == NULL)
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : Memory Allocation to "
                               "handle Suppress-Peer-Reuse FAILED!!!\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerEntry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerEntry))));
                gu4BgpDebugCnt[MAX_BGP_PEER_NODE_SIZING_ID]++;
                return RFD_FAILURE;
            }
            pSupPeerReuse->pPeer = pPeerEntry;
            /* Always begin with IPV4_UNICAST. */
            BGP4_PEER_INIT_AFI (pPeerEntry) = BGP4_INET_AFI_IPV4;
            BGP4_PEER_INIT_SAFI (pPeerEntry) = BGP4_INET_SAFI_UNICAST;
            Bgp4InitNetAddressStruct (&
                                      (BGP4_PEER_INIT_NETADDR_INFO
                                       (pPeerEntry)), BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);
            TMO_SLL_Add (BGP4_SUP_PEER_REUSE_LIST
                         (BGP4_PEER_CXT_ID (pPeerEntry)),
                         &pSupPeerReuse->TSNext);
            BGP4_SET_PEER_CURRENT_STATE (pPeerEntry,
                                         BGP4_PEER_REUSE_INPROGRESS);
            BGP4_RESET_PEER_PEND_FLAG (pPeerEntry, BGP4_PEER_REUSE_PEND);
            break;

        case BGP4_PEER_REUSE_INPROGRESS:
            /* SUP-PEER-REUSE is already in-progress for this
             * Peer So the new request is rejected. */
            break;
        case BGP4_PEER_SOFTCONFIG_OUTBOUND_INPROGRESS:
        case BGP4_PEER_SOFTCONFIG_INBOUND_INPROGRESS:
        case BGP4_PEER_INIT_INPROGRESS:
            BGP4_SET_PEER_PEND_FLAG (pPeerEntry, BGP4_PEER_REUSE_PEND);
            break;
        case BGP4_PEER_DEINIT_INPROGRESS:
        default:
            /* Under these situation, PEER-REUSE Processing is
             * not accepted. */
            break;
    }
    return RFD_SUCCESS;
}

/***************************************************************************
     Function Name    : RfdSupPeerReuseHandler
     Description      : This module checks whether there is any damped route
                        is received from this peer or not. If there is any
                        damped route then it will be re-advertised as 
                        new feasible route.
     Input(s)         : pPeerInfo - Pointer to the peer information which 
                           becomes reusable.
     Output(s)        : None
     Global Variables
     Referred         : None.
     Global Variables
     Modified         : None.                   
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : BGP4_SUCCESS or BGP4_SUP_PEER_REUSE_COMPLETE
****************************************************************************/
INT4
RfdSupPeerReuseHandler (tBgp4PeerEntry * pPeerInfo)
{
    /* Variable Declarations */
    tAddrPrefix         InvPrefix;
    tRouteProfile       RtProfile;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pNextRtList = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4Status = 0;
    UINT4               u4AfiSafiIndex = 0;
    UINT4               u4TempAfiSafiIndex = 0;
    UINT4               u4RtCnt = 0;
    UINT2               u2TempAfi = 0;
    UINT2               u2TempSafi = 0;
    UINT1               u1IsInitAfiSafiFound = BGP4_FALSE;

    if (BGP4_LOCAL_ADMIN_STATUS (BGP4_PEER_CXT_ID (pPeerInfo)) ==
        BGP4_ADMIN_DOWN)
    {
        return BGP4_SUP_PEER_REUSE_COMPLETE;
    }

    Bgp4InitAddrPrefixStruct (&(InvPrefix), 0);

    Bgp4GetAfiSafiIndex (BGP4_PEER_INIT_AFI (pPeerInfo),
                         BGP4_PEER_INIT_SAFI (pPeerInfo), &u4TempAfiSafiIndex);

    for (u4AfiSafiIndex = 0; u4AfiSafiIndex < BGP4_MPE_ADDR_FAMILY_MAX_INDEX;
         u4AfiSafiIndex++)
    {
        if ((u1IsInitAfiSafiFound == BGP4_FALSE) &&
            (u4AfiSafiIndex != u4TempAfiSafiIndex))
        {
            /* Need not handle this <AFI,SAFI> */
            continue;
        }

        u1IsInitAfiSafiFound = BGP4_TRUE;

        if (BGP4_PEER_AFI_SAFI_INSTANCE (pPeerInfo, u4AfiSafiIndex) == NULL)
        {
            /* This <AFI,SAFI> is not negotiated. No need to handle this
             * <AFI,SAFI> */
            continue;
        }

        Bgp4GetAfiSafiFromIndex (u4AfiSafiIndex, &u2TempAfi, &u2TempSafi);

        if ((MEMCMP (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (InvPrefix),
                     BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                     (BGP4_PEER_INIT_PREFIX_INFO (pPeerInfo)),
                     BGP4_MAX_INET_ADDRESS_LEN)) == 0)
        {
            /* Start processing from the first route for this peer
             * present in the Peer-Route List. */
            pRtProfile =
                BGP4_DLL_FIRST_ROUTE (BGP4_PEER_ROUTE_LIST (pPeerInfo,
                                                            u4AfiSafiIndex));
            if (pRtProfile == NULL)
            {
                /* No route is present in the peer for this <AFI,SAFI>. */
                continue;
            }
        }
        else
        {
            /* Get the peer-route corresponding to the int 
             * rt-info from the RIB */
            RtProfile.u1Ref = 0;
            RtProfile.u1Protocol = 0;
            RtProfile.p_RPNext = 0;
            RtProfile.u4Flags = 0;
            RtProfile.pRtInfo = 0;
            Bgp4CopyNetAddressStruct (&(RtProfile.NetAddress),
                                      BGP4_PEER_INIT_NETADDR_INFO (pPeerInfo));
            /* Reset the peer-init address Info */
            Bgp4InitNetAddressStruct (&
                                      (BGP4_PEER_INIT_NETADDR_INFO (pPeerInfo)),
                                      u2TempAfi, (UINT1) u2TempSafi);
            i4Status =
                Bgp4RibhLookupRtEntry (&RtProfile, BGP4_PEER_CXT_ID (pPeerInfo),
                                       BGP4_TREE_FIND_EXACT, &pRtProfile,
                                       &pRibNode);
            if (i4Status == BGP4_FAILURE)
            {
                /* No more routes present in RIB for this <AFI,SAFI> */
                continue;
            }

            pRtProfile = Bgp4DshGetPeerRtFromProfileList (pRtProfile,
                                                          pPeerInfo);
            if (pRtProfile == NULL)
            {
                /* No Peer routes present in RIB for this <AFI,SAFI> */
                continue;
            }
        }

        for (;;)
        {
            /* Get the next-Route profile. */
            pNextRtList = BGP4_RT_PROTO_LINK_NEXT (pRtProfile);

            /* Process the current route */
            if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_DAMPED) ==
                BGP4_RT_DAMPED)
            {
                /* Route is damped earlier. Withdrawn the route from
                 * the RIB and Add the route to the Peer's RCVD-Route List. */
                Bgp4DshDelinkRouteFromChgList (pRtProfile);
                Bgp4DshAddRouteToList (BGP4_PEER_RCVD_ROUTES (pPeerInfo),
                                       pRtProfile, BGP4_PREPEND);
                BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_NO_RFD);
                Bgp4RibhProcessWithdRoute (pPeerInfo, pRtProfile, NULL);

                BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);
                /* Set NO_RFD flag, so that RFD is by-passed while procesing
                 * the feasible route. */
                BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_NO_RFD);
            }
            u4RtCnt++;

            if (pNextRtList == NULL)
            {
                /* No more route in this <AFI,SAFI> */
                break;
            }
            if (u4RtCnt >= BGP4_MAX_PEERRTS2PROCESS)
            {
                /* Store the next route to be processed in peer init route info.
                 * This route will be processed in the next TIC */
                BGP4_PEER_INIT_AFI (pPeerInfo) = u2TempAfi;
                BGP4_PEER_INIT_SAFI (pPeerInfo) = u2TempSafi;
                Bgp4CopyNetAddressStruct (&
                                          (BGP4_PEER_INIT_NETADDR_INFO
                                           (pPeerInfo)),
                                          BGP4_RT_NET_ADDRESS_INFO
                                          (pNextRtList));
                break;
            }
            pRtProfile = pNextRtList;
            pNextRtList = NULL;
        }

        if (pNextRtList == NULL)
        {
            /* No more route from Peer for this <AFI,SAFI> */
            continue;
        }

        if (u4RtCnt >= BGP4_MAX_PEERRTS2PROCESS)
        {
            /* Have processed more than max routes. */
            return BGP4_SUCCESS;
        }
    }
    return BGP4_SUP_PEER_REUSE_COMPLETE;
}

/***************************************************************************
     Function Name    : RfdSupPeerReuseListHandler
     Description      : This module process all the peers present in the
                        Sup Peer's Reuse list.
     Input(s)         : None.
     Output(s)        : None
     Global Variables
     Referred         : BGP4_SUP_PEER_REUSE_LIST 
     Global Variables
     Modified         : None.                   
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return           : BGP4_TRUE  - if more peers remains to be initialised.
                      : BGP4_FALSE - if no more peers needs to be initialised
                      : BGP4_RELINQUISH - if external events requires CPU
****************************************************************************/
INT4
RfdSupPeerReuseListHandler (UINT4 u4Context)
{
    /* Variable Declarations */
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tPeerNode          *pTmpPeerNode = NULL;
    tPeerNode          *pPeerNode = NULL;
    UINT4               u4PeerCnt = 0;
    INT4                i4RetVal = BGP4_FAILURE;

    /* Check if PeerInfo List is NULL */
    if (TMO_SLL_Count (BGP4_SUP_PEER_REUSE_LIST (u4Context)) == 0)
    {
        return BGP4_FALSE;
    }

    BGP_SLL_DYN_Scan (BGP4_SUP_PEER_REUSE_LIST (u4Context), pPeerNode,
                      pTmpPeerNode, tPeerNode *)
    {
        pPeerInfo = pPeerNode->pPeer;
        i4RetVal = RfdSupPeerReuseHandler (pPeerInfo);
        if (i4RetVal == BGP4_SUP_PEER_REUSE_COMPLETE)
        {
            /*  Reset the peer current status */
            BGP4_RESET_PEER_CURRENT_STATE (pPeerInfo);
            BGP4_SET_PEER_CURRENT_STATE (pPeerInfo, BGP4_PEER_READY);

            /* Reset the peer-init information */
            Bgp4InitNetAddressStruct (&
                                      (BGP4_PEER_INIT_NETADDR_INFO (pPeerInfo)),
                                      BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);
            BGP4_PEER_INIT_AFI (pPeerInfo) = 0;
            BGP4_PEER_INIT_SAFI (pPeerInfo) = 0;
            /*  Remove peer from suppressed-peer-init list */
            TMO_SLL_Delete (BGP4_SUP_PEER_REUSE_LIST (u4Context),
                            &pPeerNode->TSNext);

            BGP_PEER_NODE_FREE (pPeerNode);
        }
        u4PeerCnt++;
        if (u4PeerCnt > BGP4_MAX_PEERS2PROCESS)
        {
            break;
        }
    }
    if (Bgp4IsCpuNeeded () == BGP4_TRUE)
    {
        /* External events requires CPU. Relinquish CPU. */
        return (BGP4_RELINQUISH);
    }

    if (TMO_SLL_Count (BGP4_SUP_PEER_REUSE_LIST (u4Context)) > 0)
    {
        return (BGP4_TRUE);
    }
    else
    {
        TMO_SLL_Init (BGP4_SUP_PEER_REUSE_LIST (u4Context));
        return (BGP4_FALSE);
    }
}

/***************************************************************************
     Function Name    : RfdReusePeerRtsHandler
     Description      : This module adds all the routes from this peer to the
                        peer's received route list if the peer state is UP.
                        If the Peer state is down, it disposes the damping 
                        history of this peer.
     Input(s)         : pPeerInfo - Pointer to the peer information which 
                           becomes reusable.
                        pPeerDampHist -Pointer to the peer damping history.
     Output(s)        : None
     Global Variables
     Referred         : None 
     Global Variables
     Modified         : PEERS_DAMP_HIST_LIST(BGP4_DFLT_VRFID) 
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
****************************************************************************/
INT4
RfdReusePeerRtsHandler (tBgp4PeerEntry * pPeerInfo,
                        tPeerDampHist * pPeerDampHist)
{
    tTMO_SLL            TsFeasibleRts;
    UINT4               u4HashIndex;

    TMO_SLL_Init (&TsFeasibleRts);
    /* If the peer state is up, the routes learnt from this peer in 
       SupPeerRtsList will be treated as fresh routes */
    if (RFD_PEER_STATE (BGP4_PEER_CXT_ID (pPeerInfo), pPeerInfo) == RFD_PEER_UP)
    {
        /* Route are being received from this Peer. Since the peer itself
         * is damped till this moment, the route also should have been
         * damped. So need to treat all the routes received from this
         * peer as feasible route. */
        RfdSupPeerReusePendHandler (pPeerInfo);

    }                            /* if (RFD_PEER_STATE (BGP4_DFLT_VRFID,pPeerInfo) == RFD_PEER_UP) */

    /*If the peer is reusable, dispose the damping history */

    Bgp4GetPrefixHashIndex (BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo),
                            &u4HashIndex);

    TMO_HASH_Delete_Node (PEERS_DAMP_HIST_LIST (BGP4_PEER_CXT_ID (pPeerInfo)),
                          &(pPeerDampHist->NextPeerDampHist), u4HashIndex);
    PEER_DAMP_HIST_ENTRY_FREE (pPeerDampHist);
    return RFD_SUCCESS;

}                                /* End of RfdReusePeerRtsHandler */

/*****************************************************************************
     Function Name    :  RfdRoutesReuseListHandler
     Description      :  This module updates damping history for routes 
                         present in routes reuse list. If the route becomes
                         reusable then the route is added to the corresponding
                         peer's received route list and the damping history
                         is cleared.
     Input(s)         :  None
     Output(s)        :  None
     Global Variables
     Referred         :  None 
     Global Variables
     Modified         :  RFD_REUSE_LIST_OFFSET(BGP4_DFLT_VRFID)
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
******************************************************************************/
INT4
RfdRoutesReuseListHandler (UINT4 u4ContextId)
{

    tBgp4PeerEntry     *pPeerInfo = NULL;
    tRouteProfile      *pRtInfo = NULL;
    tRtDampHist        *pRtDampHist = NULL;
    tRtDampHist        *pNextDampHist = NULL;
    UINT1               u1IsHistFreed = BGP4_FALSE;
    UINT1               u1RtState;

    /* Fetches route information from routes reuse list . If the FOM goes 
       below reuse threshold, the routes will be reused if it is feasible
       route. If it is unfeasible route, the damping history of this reusable
       route will be disposed. These routes will be removed from routes reuse
       list. If the routes are not reusable,  these routes will be 
       removed from routes reuse list and reinserted in another list */
    RfdRRLFetchRtReuseList (u4ContextId, pRtDampHist, &pNextDampHist);

    while (pNextDampHist != NULL)
    {
        u1IsHistFreed = BGP4_FALSE;
        pRtDampHist = pNextDampHist;
        pRtInfo = pRtDampHist->pRtProfile;
        pNextDampHist = NULL;

        /* Updates damping history */
        RfdRRLDampHistHandler (pRtDampHist, &u1RtState);

        /* Fetch next route information from routes reuse list */
        RfdRRLFetchRtReuseList (u4ContextId, pRtDampHist, &pNextDampHist);

        /* Route is going to be reused */
        if (u1RtState == RFD_REUSE)
        {
            /* Remove the route from routes reuse list */
            RfdRemoveRtFromRtsReuseList (pRtInfo, pRtDampHist);

            if (pRtDampHist->u1RtStatus == RFD_SUPPRESSED_STATE)
            {
                BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                               BGP4_TRC_FLAG, BGP4_DAMP_TRC |
                               BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                               "\tDamped Route -- Dest = %s , Mask = %s "
                               "from Peer = %s -- becomes reusable.\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                (pRtInfo),
                                                BGP4_RT_AFI_INFO
                                                (pRtInfo)),
                               Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                                                (pRtInfo),
                                                BGP4_RT_AFI_INFO
                                                (pRtInfo)),
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (BGP4_RT_PEER_ENTRY (pRtInfo)),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (BGP4_RT_PEER_ENTRY
                                                  (pRtInfo)))));
                /* if feasible route becomes reusable, treat it as 
                   fresh route */
                if (pRtDampHist->u1RtFlag == RFD_FEASIBLE_ROUTE)
                {
                    pPeerInfo = BGP4_RT_PEER_ENTRY (pRtInfo);

                    /* Route needs to be advertised. Add the route to 
                     * the peer's received route list. Alse Delete the
                     * existing route from the RIB. By Pass RFD for this
                     * process. */
                    Bgp4DshDelinkRouteFromChgList (pRtInfo);
                    Bgp4DshAddRouteToList (BGP4_PEER_RCVD_ROUTES (pPeerInfo),
                                           pRtInfo, 0);
                    BGP4_RT_SET_FLAG (pRtInfo, BGP4_RT_NO_RFD);
                    Bgp4RibhDelRtEntry (pRtInfo, NULL, BGP4_RT_CXT_ID (pRtInfo),
                                        BGP4_TRUE);
                    /* Damping History has been cleared. */
                    u1IsHistFreed = BGP4_TRUE;
                    BGP4_RFD_FEAS_RTPROFILE_CNT (BGP4_PEER_CXT_ID
                                                 (pPeerInfo))--;

                    /* Set NO_RFD flag to by-pass RFD when the route is
                     * processed as feasible route. */
                    BGP4_RT_SET_FLAG (pRtInfo, BGP4_RT_NO_RFD);
                    BGP4_RT_RESET_FLAG (pRtInfo, BGP4_RT_DAMPED);
                }
            }

            /* If route becomes reusable, dispose the route
             * damping history */
            if (pRtDampHist->u1RtFlag == RFD_UNFEASIBLE_ROUTE)
            {
                /* Route is Unfeasible. Unfeasible Damped routes are
                 * present in RIB as HISTORY routes. Need to remove
                 * this route from RIB. */
                BGP4_RT_SET_FLAG (pRtInfo, BGP4_RT_NO_RFD);
                BGP4_RT_RESET_FLAG (pRtInfo, (BGP4_RT_DAMPED |
                                              BGP4_RT_HISTORY));
                Bgp4RibhDelRtEntry (pRtInfo, NULL, BGP4_RT_CXT_ID (pRtInfo),
                                    BGP4_HISTORY);
            }
            else if (u1IsHistFreed == BGP4_FALSE)
            {
                BGP4_RT_DAMP_HIST (pRtDampHist->pRtProfile) = NULL;
                Bgp4DshReleaseRtInfo (pRtDampHist->pRtProfile);
                Bgp4MemReleaseRfdDampHist (pRtDampHist);
            }
        }                        /* if (u1RtState <>RFD_REUSE) */
        else                    /* (if (u1RtState ==RFD_SUPPRESS)) */
        {
            /* Remove route from routes reuse list */
            RfdRemoveRtFromRtsReuseList (pRtInfo, pRtDampHist);

            /* Insert the route in routes reuse list */
            RfdInsertRtInRtsReuseList (pRtDampHist);
        }                        /* IF (u1RtState == RFD_REUSE) */
    }
    /*Increment the reuse list offset by 1 */
    RFD_REUSE_LIST_OFFSET (u4ContextId) = (UINT2)
        ((RFD_REUSE_LIST_OFFSET (u4ContextId) +
          RFD_SINGLE_INSTANCE) % RFD_NO_OF_REUSE_LISTS);

    return RFD_SUCCESS;
}                                /* End of RfdRoutesReuseListHandler */

/****************************************************************************
     Function Name    : RfdRRLFetchRtReuseList
     Description      : This module fetches reuse route information which is 
                        next to pRtDampHist from routes reuse list
     Input(s)         : pRtDampHist -  Pointer to reuse route information whose
                                       reuse list has to be fetched.
     Output(s)        : ppNextRtDampHist - Double Pointer to next reuse route 
                                           information
     Global Variables
     Referred         :  RFD_RTS_REUSE_LIST ,RFD_REUSE_LIST_OFFSET
     Global Variables
     Modified         :   
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
*****************************************************************************/
INT4
RfdRRLFetchRtReuseList (UINT4 u4Context, tRtDampHist * pRtDampHist,
                        tRtDampHist ** ppNextRtDampHist)
{
    /*If pRtDampHist is NULL, get the first entry in the list pointed by 
       reuse list offset , otherwise get the entry next to pRtDampHist */
    if (pRtDampHist == NULL)
    {
        if (RFD_REUSE_LIST_OFFSET (u4Context) < RFD_NO_OF_REUSE_LISTS)
        {
            *ppNextRtDampHist =
                (tRtDampHist *)
                RFD_RTS_REUSE_LIST_FIRST (u4Context,
                                          RFD_REUSE_LIST_OFFSET (u4Context));
        }
    }
    else
    {
        *ppNextRtDampHist = pRtDampHist->pReuseNext;
    }
    return RFD_SUCCESS;
}                                /*End of RfdRRLFetchRtReuseList */

/****************************************************************************
     Function Name    : RfdRRLDampHistHandler
     Description      : This module handles damping history for routes that 
                        are in the routes reuse list.
     Input(s)         : pRtDampHist - Pointer to the route damping 
                                      history.
     Output(s)        : pu1RtState - Pointer to route status that gives the 
                          status whether the route is suppressed, usable or 
                          reusable.
     Global Variables
     Referred         :  None.              
     Global Variables
     Modified         :  None
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
****************************************************************************/
INT4
RfdRRLDampHistHandler (tRtDampHist * pRtDampHist, UINT1 *pu1RtState)
{
    /* Calculate FOM */
    RfdRRLCalcRtFOM (pRtDampHist);

    /* Check whether the route becomes reusable */
    RfdRRLCheckReuse (pRtDampHist, pu1RtState);

    return RFD_SUCCESS;
}                                /* End of RfdRRLDampHistHandler */

/*****************************************************************************
     Function Name    : RfdRRLCalcRtFOM
     Description      : This module updates route figure-of-merit for the 
                        routes that are in routes reuse list.
     Input(s)         : pRtDampHist - Pointer to route damping history whose  
                             FOM has to be updated
     Output(s)        : pRtDampHist - Pointer to route damping history whose 
                             FOM has been updated.
     Global Variables
     Referred         : RFD_DECAY_TIMER_GRANULARITY(BGP4_DFLT_VRFID),RFD_DECAY_ARRAY(BGP4_DFLT_VRFID) 
     Global Variables
     Modified         : None
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
*****************************************************************************/
INT4
RfdRRLCalcRtFOM (tRtDampHist * pRtDampHist)
{

    RFDFLOAT            DecayValue;
    UINT4               u4CurrentTime;
    UINT2               u2TimeDiff;
    UINT2               u2DecayIndx;

    /*Get the system time */
    RFD_GET_SYS_TIME (&u4CurrentTime);
    /* Find the time difference between current time and last FOM 
       updated time */
    u2TimeDiff =
        RFD_GET_TIME_DIFF_SEC (u4CurrentTime, pRtDampHist->u4RtLastTime);
    u2DecayIndx = (UINT2) (u2TimeDiff /
                           RFD_DECAY_TIMER_GRANULARITY (BGP4_RT_CXT_ID
                                                        (pRtDampHist->
                                                         pRtProfile)));
    if (u2DecayIndx >
        RFD_DECAY_ARRAY_SIZE (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile)))
    {
        pRtDampHist->RtFom = 0;
    }
    else
    {
        DecayValue =
            RFD_DECAY_ARRAY (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile))
            [u2DecayIndx];
        pRtDampHist->RtFom = pRtDampHist->RtFom * DecayValue;
    }
    pRtDampHist->u4RtLastTime = u4CurrentTime;

    BGP4_TRC_ARG4 (&(BGP4_PEER_REMOTE_ADDR_INFO (BGP4_RT_PEER_ENTRY
                                                 (pRtDampHist->pRtProfile))),
                   BGP4_TRC_FLAG, BGP4_DAMP_TRC | BGP4_CONTROL_PATH_TRC,
                   BGP4_MOD_NAME,
                   "\tDAMPING :The FOM for the  "
                   "Route %s Mask %s from PEER %s, is %d\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtDampHist->pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtDampHist->pRtProfile)),
                   Bgp4PrintIpMask ((UINT1)
                                    BGP4_RT_PREFIXLEN (pRtDampHist->pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtDampHist->pRtProfile)),
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                    (BGP4_RT_PEER_ENTRY
                                     (pRtDampHist->pRtProfile)),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                     (BGP4_RT_PEER_ENTRY
                                      (pRtDampHist->pRtProfile)))),
                   (UINT4) BGP4_CEIL (pRtDampHist->RtFom));

    return RFD_SUCCESS;
}                                /* End of  RfdRRLCalcRtFOM */

/***************************************************************************
     Function Name    : RfdRRLCheckReuse
     Description      : This module compares the figure of merit against 
                        the reuse threshold. If figure of merit is below 
                        reuse threshold, this sets pu1RtState as RFD_REUSE
     Input(s)         : pRtDampHist -  Pointer to route damping history 
                           whose FOM value has to be checked.
     Output(s)        : pu1RtState -  Pointer to route state that holds the 
                          status whether the route is suppressed, usable 
                          or reusable.
     Global Variables
     Referred         : RFD_REUSE_THRESHOLD(BGP4_DFLT_VRFID) 
     Global Variables
     Modified         : None 
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
****************************************************************************/
INT4
RfdRRLCheckReuse (tRtDampHist * pRtDampHist, UINT1 *pu1RtState)
{

    /* If FOM is below Reuse, the route will be reused */
    if (pRtDampHist->RtFom <
        (RFD_REUSE_THRESHOLD (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile))))
    {
        *pu1RtState = RFD_REUSE;
    }
    else
    {
        *pu1RtState = RFD_SUPPRESS;
    }
    return RFD_SUCCESS;
}                                /* End of RfdRRLCheckReuse */

/***************************************************************************
     Function Name    : RfdInsertRtInRtsReuseList
     Description      : This module calculates reuse index value and finds 
                        the corresponding routes reuse list in the reuse 
                        array. Then this inserts route information into the 
                        routes reuse list
     Input(s)         : pRtDampHist - Pointer to route damp history which 
                                      is to be inserted.
     Output(s)        : None
     Global Variables
     Referred         : RFD_CUTOFF_THRESHOLD(BGP4_DFLT_VRFID) ,RFD_REUSE_INDEX_ARRAY(BGP4_DFLT_VRFID), 
                        RFD_REUSE_INDEX_ARRAY_SIZE(BGP4_DFLT_VRFID),RFD_SCALE_FACTOR(BGP4_DFLT_VRFID), 
                        RFD_REUSE_LIST_OFFSET(BGP4_DFLT_VRFID)
     Global Variables
     Modified         : None   
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
***************************************************************************/
INT4
RfdInsertRtInRtsReuseList (tRtDampHist * pRtDampHist)
{
    RFDFLOAT            RouteFOM;
    RFDFLOAT            TmpRIndx;
    UINT4               u4ReuseIndx;
    UINT4               u4RIndxArayIndx;

    RouteFOM = pRtDampHist->RtFom;
    TmpRIndx =
        RouteFOM /
        RFD_CUTOFF_THRESHOLD (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile));
    TmpRIndx = TmpRIndx - RFD_SINGLE_INSTANCE;
    TmpRIndx =
        TmpRIndx * RFD_SCALE_FACTOR (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile));

    if (TmpRIndx < 0)
    {
        TmpRIndx = 0;
    }
    u4RIndxArayIndx = (UINT4) TmpRIndx;

    u4ReuseIndx = RFD_REUSE_INDEX_ARRAY
        (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile))[u4RIndxArayIndx];
    if (u4ReuseIndx >
        RFD_REUSE_INDEX_ARRAY_SIZE (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile)))
    {
        u4ReuseIndx = (RFD_NO_OF_REUSE_LISTS - RFD_SINGLE_INSTANCE);
    }

    u4ReuseIndx =
        ((u4ReuseIndx +
          (RFD_REUSE_LIST_OFFSET
           (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile)))) %
         RFD_NO_OF_REUSE_LISTS);
    if (u4ReuseIndx <=
        RFD_REUSE_LIST_OFFSET (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile)))
    {
        u4ReuseIndx =
            (RFD_REUSE_LIST_OFFSET (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile)) +
             (RFD_MAX_HOLD_DOWN_TIME (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile))
              /
              RFD_REUSE_TIMER_GRANULARITY (BGP4_RT_CXT_ID
                                           (pRtDampHist->pRtProfile))));
    }
    pRtDampHist->u2ReuseIndex = (UINT2) u4ReuseIndx;

    /* Add the Routes with Damp History to the beginning of Reuse List. */
    pRtDampHist->pReusePrev = NULL;
    pRtDampHist->pReuseNext =
        RFD_RTS_REUSE_LIST_FIRST (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile),
                                  u4ReuseIndx);

    if (RFD_RTS_REUSE_LIST_FIRST
        (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile), u4ReuseIndx) == NULL)
    {
        /* List is Empty. */
        RFD_RTS_REUSE_LIST_FIRST (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile),
                                  u4ReuseIndx) = pRtDampHist;
        RFD_RTS_REUSE_LIST_LAST (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile),
                                 u4ReuseIndx) = pRtDampHist;
    }
    else
    {
        (RFD_RTS_REUSE_LIST_FIRST
         (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile), u4ReuseIndx))->pReusePrev =
pRtDampHist;
        RFD_RTS_REUSE_LIST_FIRST (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile),
                                  u4ReuseIndx) = pRtDampHist;
    }
    RFD_RTS_REUSE_LIST_COUNT (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile),
                              u4ReuseIndx)++;
    BGP4_TRC_ARG5 (&
                   (BGP4_PEER_REMOTE_ADDR_INFO
                    (pRtDampHist->pRtProfile->pPEPeer)), BGP4_TRC_FLAG,
                   BGP4_DAMP_TRC | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                   "\tDAMPING : Route -- Dest = %s, Mask = %s, "
                   "from Peer = %s will be available in reuse intervals of %d & FOM  %d.\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtDampHist->pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtDampHist->pRtProfile)),
                   Bgp4PrintIpMask ((UINT1)
                                    BGP4_RT_PREFIXLEN (pRtDampHist->pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtDampHist->pRtProfile)),
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                    (pRtDampHist->pRtProfile->pPEPeer),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                     (pRtDampHist->pRtProfile->pPEPeer))),
                   RFD_NO_OF_REUSE_LISTS - (pRtDampHist->u2ReuseIndex),
                   (UINT4) BGP4_CEIL ((pRtDampHist)->RtFom));
    return RFD_SUCCESS;
}                                /* End of RfdInsertRtInRtsReuseList */

/***************************************************************************
     Function Name    : RfdRemoveRtFromRtsReuseList
     Description      : This module removes route information from routes 
                        reuse list
     Input(s)         : pRtInfo -  a pointer to route information that is 
                           to be removed from routes reuse list.
              : pRtDampHist - pointer to the route's damping history
     Output(s)        : None. 
     Global Variables
     Referred         : RFD_RTS_REUSE_LIST(BGP4_DFLT_VRFID)
     Global Variables
     Modified         : RFD_RTS_REUSE_LIST(BGP4_DFLT_VRFID)
     Exceptions or OS
     Err Handling     : None
     Use of Recursion : None
     Return(s)        : RFD_SUCCESS or RFD_FAILURE
**************************************************************************/
INT4
RfdRemoveRtFromRtsReuseList (tRouteProfile * pRtInfo, tRtDampHist * pRtDampHist)
{
    UINT4               u4Context = 0;
    UINT2               u2ReuseIndex = 0;

    u2ReuseIndex = pRtDampHist->u2ReuseIndex;
    u4Context = BGP4_RT_CXT_ID (pRtInfo);

    if ((u2ReuseIndex == RFD_INVALID_INDX) ||
        (u2ReuseIndex >= RFD_NO_OF_REUSE_LISTS))
    {
        RfdFaultReportReceive (u4Context, RFD_RT_RLIST_ENTRY_NOT_FOUND);
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tRfdRemoveRtFromRtsReuseList : Route is not found "
                  "in routes reuse list\n");
        return RFD_SUCCESS;
    }

    if ((RFD_RTS_REUSE_LIST_FIRST
         (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile),
          u2ReuseIndex) != pRtDampHist)
        &&
        (RFD_RTS_REUSE_LIST_LAST
         (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile),
          u2ReuseIndex) != pRtDampHist) && (pRtDampHist->pReusePrev == NULL)
        && (pRtDampHist->pReuseNext == NULL))
    {
        RfdFaultReportReceive (u4Context, RFD_RT_RLIST_ENTRY_NOT_FOUND);
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "DAMPING : Route is not found in routes reuse list\n");
        return RFD_SUCCESS;
    }

    if ((pRtDampHist->pReusePrev == NULL) && (pRtDampHist->pReuseNext == NULL))
    {
        /* Only one entry in List. So clear the list. */
        RFD_RTS_REUSE_LIST_FIRST (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile),
                                  u2ReuseIndex) = NULL;
        RFD_RTS_REUSE_LIST_LAST (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile),
                                 u2ReuseIndex) = NULL;
        RFD_RTS_REUSE_LIST_COUNT (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile),
                                  u2ReuseIndex) = 0;
    }
    else
    {
        if (pRtDampHist->pReusePrev == NULL)
        {
            /* First Entry is removed. */
            RFD_RTS_REUSE_LIST_FIRST (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile),
                                      u2ReuseIndex) = pRtDampHist->pReuseNext;
        }
        else
        {
            (pRtDampHist->pReusePrev)->pReuseNext = pRtDampHist->pReuseNext;
        }
        if (pRtDampHist->pReuseNext == NULL)
        {
            /* Last Entry is removed. */
            RFD_RTS_REUSE_LIST_LAST (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile),
                                     u2ReuseIndex) = pRtDampHist->pReusePrev;
        }
        else
        {
            (pRtDampHist->pReuseNext)->pReusePrev = pRtDampHist->pReusePrev;
        }
        RFD_RTS_REUSE_LIST_COUNT (BGP4_RT_CXT_ID (pRtDampHist->pRtProfile),
                                  u2ReuseIndex)--;
    }

    pRtDampHist->pReusePrev = NULL;
    pRtDampHist->pReuseNext = NULL;
    pRtDampHist->u2ReuseIndex = RFD_INVALID_INDX;
    return RFD_SUCCESS;
}                                /*End of RfdRemoveRtFromRtsReuseList */

/************************** End of File **********************************/
#endif /* RFD_WANTED */
