/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgcomlow.c,v 1.25 2015/02/24 11:57:42 siva Exp $
 *
 * Description: Contains BGP Community feature low level routines
 *
 *******************************************************************/
#ifndef _BGCOMLOW_C
#define _BGCOMLOW_C

# include  "bgp4com.h"
# include  "include.h"
# include  "fsbgpcon.h"
# include  "fsbgpogi.h"
# include  "midconst.h"
# include  "fsmpbgcli.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4CommMaxInFTblEntries
 Input       :  The Indices

                The Object 
                retValFsbgp4CommMaxInFTblEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4CommMaxInFTblEntries (INT4 *pi4RetValFsbgp4CommMaxInFTblEntries)
{
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4CommMaxInFTblEntries =
        FsBGPSizingParams[MAX_BGP_COMM_IN_FILTER_INFOS_SIZING_ID].
        u4PreAllocatedUnits;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4CommMaxOutFTblEntries
 Input       :  The Indices

                The Object 
                retValFsbgp4CommMaxOutFTblEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4CommMaxOutFTblEntries (INT4 *pi4RetValFsbgp4CommMaxOutFTblEntries)
{

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4CommMaxOutFTblEntries =
        FsBGPSizingParams[MAX_BGP_COMM_OUT_FILTER_INFOS_SIZING_ID].
        u4PreAllocatedUnits;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4CommMaxInFTblEntries
 Input       :  The Indices

                The Object 
                setValFsbgp4CommMaxInFTblEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4CommMaxInFTblEntries (INT4 i4SetValFsbgp4CommMaxInFTblEntries)
{
    tBgpSystemSize      BgpSystemSize;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    GetBgpSizingParams (&BgpSystemSize);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4CommMaxInFTblEntries,
                          u4SeqNum, FALSE, BgpLock, BgpUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gpBgpCurrCxtNode->u4ContextId,
                      i4SetValFsbgp4CommMaxInFTblEntries));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsbgp4CommMaxOutFTblEntries
 Input       :  The Indices

                The Object 
                setValFsbgp4CommMaxOutFTblEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4CommMaxOutFTblEntries (INT4 i4SetValFsbgp4CommMaxOutFTblEntries)
{
    tBgpSystemSize      BgpSystemSize;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    GetBgpSizingParams (&BgpSystemSize);

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4CommMaxOutFTblEntries,
                          u4SeqNum, FALSE, BgpLock, BgpUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gpBgpCurrCxtNode->u4ContextId,
                      i4SetValFsbgp4CommMaxOutFTblEntries));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4CommMaxInFTblEntries
 Input       :  The Indices

                The Object 
                testValFsbgp4CommMaxInFTblEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4CommMaxInFTblEntries (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsbgp4CommMaxInFTblEntries)
{
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4CommMaxInFTblEntries > 0) &&
        (i4TestValFsbgp4CommMaxInFTblEntries <=
         MAX_INPUT_COMM_FILTER_TBL_ENTRIES))
    {
        return SNMP_SUCCESS;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - Invalid Max Comm Incoming Filter Table value.\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4CommMaxOutFTblEntries
 Input       :  The Indices

                The Object 
                testValFsbgp4CommMaxOutFTblEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4CommMaxOutFTblEntries (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFsbgp4CommMaxOutFTblEntries)
{
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4CommMaxOutFTblEntries > 0) &&
        (i4TestValFsbgp4CommMaxOutFTblEntries <=
         MAX_OUTPUT_COMM_FILTER_TBL_ENTRIES))
    {
        return SNMP_SUCCESS;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - Invalid Max Comm Output Filter Table Value.\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4CommMaxInFTblEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4CommMaxInFTblEntries (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4CommMaxOutFTblEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4CommMaxOutFTblEntries (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4MpeCommRouteAddCommTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4MpeCommRouteAddCommTable
 Input       :  The Indices
                Fsbgp4AddCommRtAfi
                Fsbgp4AddCommRtSafi
                Fsbgp4AddCommIpNetwork
                Fsbgp4AddCommIpPrefixLen
                Fsbgp4AddCommVal
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4MpeCommRouteAddCommTable (INT4
                                                        i4Fsbgp4AddCommRtAfi,
                                                        INT4
                                                        i4Fsbgp4AddCommRtSafi,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pFsbgp4AddCommIpNetwork,
                                                        INT4
                                                        i4Fsbgp4AddCommIpPrefixLen,
                                                        UINT4
                                                        u4Fsbgp4AddCommVal)
{
    tNetAddress         RtAddress;
    INT4                i4Sts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4AddCommRtAfi,
                                    i4Fsbgp4AddCommRtSafi,
                                    pFsbgp4AddCommIpNetwork, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4AddCommIpPrefixLen;
    i4Sts =
        Bgp4ValidateCommRouteAddCommTableIndex (RtAddress, u4Fsbgp4AddCommVal);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Prefix/Length/Community Value.\n");
        CLI_SET_ERR (CLI_BGP4_INVALID_PREFIX_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4MpeCommRouteAddCommTable
 Input       :  The Indices
                Fsbgp4AddCommRtAfi
                Fsbgp4AddCommRtSafi
                Fsbgp4AddCommIpNetwork
                Fsbgp4AddCommIpPrefixLen
                Fsbgp4AddCommVal
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4MpeCommRouteAddCommTable (INT4 *pi4Fsbgp4AddCommRtAfi,
                                                INT4 *pi4Fsbgp4AddCommRtSafi,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsbgp4AddCommIpNetwork,
                                                INT4
                                                *pi4Fsbgp4AddCommIpPrefixLen,
                                                UINT4 *pu4Fsbgp4AddCommVal)
{
    tNetAddress         RtAddress;
    INT4                i4Sts;
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4GetFirstIndexCommRouteAddCommTable (u4Context, &RtAddress,
                                                    pu4Fsbgp4AddCommVal);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No Community Value Configured.\n");
        return SNMP_FAILURE;
    }

    i4Sts =
        Bgp4LowSetRouteAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO
                                (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                 (RtAddress)),
                                BGP4_SAFI_IN_NET_ADDRESS_INFO (RtAddress),
                                pFsbgp4AddCommIpNetwork, RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4Fsbgp4AddCommRtAfi =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                      (RtAddress));
    *pi4Fsbgp4AddCommRtSafi = BGP4_SAFI_IN_NET_ADDRESS_INFO (RtAddress);
    *pi4Fsbgp4AddCommIpPrefixLen =
        BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4MpeCommRouteAddCommTable
 Input       :  The Indices
                Fsbgp4AddCommRtAfi
                nextFsbgp4AddCommRtAfi
                Fsbgp4AddCommRtSafi
                nextFsbgp4AddCommRtSafi
                Fsbgp4AddCommIpNetwork
                nextFsbgp4AddCommIpNetwork
                Fsbgp4AddCommIpPrefixLen
                nextFsbgp4AddCommIpPrefixLen
                Fsbgp4AddCommVal
                nextFsbgp4AddCommVal
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4MpeCommRouteAddCommTable (INT4 i4Fsbgp4AddCommRtAfi,
                                               INT4 *pi4NextFsbgp4AddCommRtAfi,
                                               INT4 i4Fsbgp4AddCommRtSafi,
                                               INT4 *pi4NextFsbgp4AddCommRtSafi,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsbgp4AddCommIpNetwork,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pNextFsbgp4AddCommIpNetwork,
                                               INT4 i4Fsbgp4AddCommIpPrefixLen,
                                               INT4
                                               *pi4NextFsbgp4AddCommIpPrefixLen,
                                               UINT4 u4Fsbgp4AddCommVal,
                                               UINT4 *pu4NextFsbgp4AddCommVal)
{
    tNetAddress         RtAddress;
    tNetAddress         NextRtAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4AddCommRtAfi,
                                    i4Fsbgp4AddCommRtSafi,
                                    pFsbgp4AddCommIpNetwork, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4AddCommIpPrefixLen;

    i4Sts =
        Bgp4GetNextIndexCommRouteAddCommTable (u4Context, RtAddress,
                                               u4Fsbgp4AddCommVal,
                                               &NextRtAddress,
                                               pu4NextFsbgp4AddCommVal);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                  "\tERROR - No more Community Value Configured.\n");
        return SNMP_FAILURE;
    }

    i4Sts =
        Bgp4LowSetRouteAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO
                                (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                 (NextRtAddress)),
                                BGP4_SAFI_IN_NET_ADDRESS_INFO (NextRtAddress),
                                pNextFsbgp4AddCommIpNetwork, NextRtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsbgp4AddCommRtAfi =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                      (NextRtAddress));
    *pi4NextFsbgp4AddCommRtSafi = BGP4_SAFI_IN_NET_ADDRESS_INFO (NextRtAddress);
    *pi4NextFsbgp4AddCommIpPrefixLen =
        BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NextRtAddress);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeAddCommRowStatus
 Input       :  The Indices
                Fsbgp4AddCommRtAfi
                Fsbgp4AddCommRtSafi
                Fsbgp4AddCommIpNetwork
                Fsbgp4AddCommIpPrefixLen
                Fsbgp4AddCommVal

                The Object 
                retValFsbgp4AddCommRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeAddCommRowStatus (INT4 i4Fsbgp4AddCommRtAfi,
                                 INT4 i4Fsbgp4AddCommRtSafi,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsbgp4AddCommIpNetwork,
                                 INT4 i4Fsbgp4AddCommIpPrefixLen,
                                 UINT4 u4Fsbgp4AddCommVal,
                                 INT4 *pi4RetValFsbgp4AddCommRowStatus)
{
    tCommProfile       *pCommProfile = NULL;
    tNetAddress         RtAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4AddCommRtAfi,
                                    i4Fsbgp4AddCommRtSafi,
                                    pFsbgp4AddCommIpNetwork, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4AddCommIpPrefixLen;

    i4Sts =
        CommGetAddCommEntry (u4Context, RtAddress, u4Fsbgp4AddCommVal,
                             &pCommProfile);
    if (pCommProfile == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No Matching Community is present.\n");
        return SNMP_FAILURE;
    }

    *pi4RetValFsbgp4AddCommRowStatus = pCommProfile->u1RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4mpeAddCommRowStatus
 Input       :  The Indices
                Fsbgp4AddCommRtAfi
                Fsbgp4AddCommRtSafi
                Fsbgp4AddCommIpNetwork
                Fsbgp4AddCommIpPrefixLen
                Fsbgp4AddCommVal

                The Object 
                setValFsbgp4AddCommRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4mpeAddCommRowStatus (INT4 i4Fsbgp4AddCommRtAfi,
                                 INT4 i4Fsbgp4AddCommRtSafi,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsbgp4AddCommIpNetwork,
                                 INT4 i4Fsbgp4AddCommIpPrefixLen,
                                 UINT4 u4Fsbgp4AddCommVal,
                                 INT4 i4SetValFsbgp4AddCommRowStatus)
{
    tCommProfile       *pCommProfile = NULL;
    tNetAddress         RtAddress;
    INT4                i4RetSts;

    UINT4               u4Context;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4RetSts = Bgp4LowGetRouteAddress (i4Fsbgp4AddCommRtAfi,
                                       i4Fsbgp4AddCommRtSafi,
                                       pFsbgp4AddCommIpNetwork, &RtAddress);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4AddCommIpPrefixLen;

    switch (i4SetValFsbgp4AddCommRowStatus)
    {
        case CREATE_AND_GO:
            i4RetSts =
                CommCreateAddCommEntry (u4Context, RtAddress,
                                        u4Fsbgp4AddCommVal, &pCommProfile);
            if (i4RetSts == COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create Community Entry.\n");
                return SNMP_FAILURE;
            }
            pCommProfile->u1RowStatus = ACTIVE;
            break;

        case CREATE_AND_WAIT:
            i4RetSts =
                CommCreateAddCommEntry (u4Context, RtAddress,
                                        u4Fsbgp4AddCommVal, &pCommProfile);
            if (i4RetSts == COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create Community Entry.\n");
                return SNMP_FAILURE;
            }
            pCommProfile->u1RowStatus = NOT_IN_SERVICE;
            break;
        case ACTIVE:
            i4RetSts =
                CommGetAddCommEntry (u4Context, RtAddress, u4Fsbgp4AddCommVal,
                                     &pCommProfile);
            if (pCommProfile == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Community is present.\n");
                return SNMP_FAILURE;
            }
            pCommProfile->u1RowStatus = ACTIVE;
            break;
        case NOT_IN_SERVICE:
            CommGetAddCommEntry (u4Context, RtAddress, u4Fsbgp4AddCommVal,
                                 &pCommProfile);
            if (pCommProfile == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Community is present.\n");
                return SNMP_FAILURE;
            }
            pCommProfile->u1RowStatus = NOT_IN_SERVICE;
            break;
        case DESTROY:
            i4RetSts =
                CommDeleteAddCommEntry (u4Context, RtAddress,
                                        u4Fsbgp4AddCommVal, &pCommProfile);
            break;
        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status value.\n");
            return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4mpeAddCommRowStatus, u4SeqNum,
                          TRUE, BgpLock, BgpUnLock, 6, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %s %i %u %i",
                      gpBgpCurrCxtNode->u4ContextId, i4Fsbgp4AddCommRtAfi,
                      i4Fsbgp4AddCommRtSafi, pFsbgp4AddCommIpNetwork,
                      i4Fsbgp4AddCommIpPrefixLen, u4Fsbgp4AddCommVal,
                      i4SetValFsbgp4AddCommRowStatus));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4mpeAddCommRowStatus
 Input       :  The Indices
                Fsbgp4AddCommRtAfi
                Fsbgp4AddCommRtSafi
                Fsbgp4AddCommIpNetwork
                Fsbgp4AddCommIpPrefixLen
                Fsbgp4AddCommVal

                The Object 
                testValFsbgp4AddCommRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4mpeAddCommRowStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4Fsbgp4AddCommRtAfi,
                                    INT4 i4Fsbgp4AddCommRtSafi,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsbgp4AddCommIpNetwork,
                                    INT4 i4Fsbgp4AddCommIpPrefixLen,
                                    UINT4 u4Fsbgp4AddCommVal,
                                    INT4 i4TestValFsbgp4AddCommRowStatus)
{
    tCommProfile       *pCommProfile = NULL;
    tNetAddress         RtAddress;
    INT4                i4RetSts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Local AS not configured..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }

    i4RetSts = CommValidateCommunity (u4Fsbgp4AddCommVal);
    if (i4RetSts == COMM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Community Value.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_COMM_VALUE_ERR);
        return SNMP_FAILURE;
    }

#ifdef L3VPN
    if ((i4Fsbgp4AddCommRtSafi == BGP4_INET_SAFI_LABEL) ||
        (i4Fsbgp4AddCommRtSafi == BGP4_INET_SAFI_VPNV4_UNICAST))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#endif
    i4RetSts = Bgp4LowGetRouteAddress (i4Fsbgp4AddCommRtAfi,
                                       i4Fsbgp4AddCommRtSafi,
                                       pFsbgp4AddCommIpNetwork, &RtAddress);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4AddCommIpPrefixLen;

    switch (i4TestValFsbgp4AddCommRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            i4RetSts = CommGetAddCommEntry (u4Context, RtAddress,
                                            u4Fsbgp4AddCommVal, &pCommProfile);
            if (i4RetSts != COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Matching Add Comm Entry is present.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
        case NOT_IN_SERVICE:
            i4RetSts = CommGetAddCommEntry (u4Context, RtAddress,
                                            u4Fsbgp4AddCommVal, &pCommProfile);
            if (i4RetSts != COMM_SUCCESS)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Add Comm Entry is present\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if ((pCommProfile->u1RowStatus == ACTIVE) ||
                (pCommProfile->u1RowStatus == NOT_IN_SERVICE) ||
                (pCommProfile->u1RowStatus == NOT_READY))
            {
                return SNMP_SUCCESS;
            }
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status value.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        case DESTROY:
            i4RetSts = CommGetAddCommEntry (u4Context, RtAddress,
                                            u4Fsbgp4AddCommVal, &pCommProfile);
            return SNMP_SUCCESS;
        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4MpeCommRouteAddCommTable
 Input       :  The Indices
                Fsbgp4AddCommRtAfi
                Fsbgp4AddCommRtSafi
                Fsbgp4AddCommIpNetwork
                Fsbgp4AddCommIpPrefixLen
                Fsbgp4AddCommVal
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4MpeCommRouteAddCommTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4MpeCommRouteDeleteCommTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4MpeCommRouteDeleteCommTable
 Input       :  The Indices
                Fsbgp4DeleteCommRtAfi
                Fsbgp4DeleteCommRtSafi
                Fsbgp4DeleteCommIpNetwork
                Fsbgp4DeleteCommIpPrefixLen
                Fsbgp4DeleteCommVal
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4MpeCommRouteDeleteCommTable (INT4
                                                           i4Fsbgp4DeleteCommRtAfi,
                                                           INT4
                                                           i4Fsbgp4DeleteCommRtSafi,
                                                           tSNMP_OCTET_STRING_TYPE
                                                           *
                                                           pFsbgp4DeleteCommIpNetwork,
                                                           INT4
                                                           i4Fsbgp4DeleteCommIpPrefixLen,
                                                           UINT4
                                                           u4Fsbgp4DeleteCommVal)
{
    tNetAddress         RtAddress;
    INT4                i4Sts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4DeleteCommRtAfi,
                                    i4Fsbgp4DeleteCommRtSafi,
                                    pFsbgp4DeleteCommIpNetwork, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4DeleteCommIpPrefixLen;
    i4Sts =
        Bgp4ValidateCommRouteDelCommTableIndex (RtAddress,
                                                u4Fsbgp4DeleteCommVal);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Prefix/Length/Community Value.\n");
        CLI_SET_ERR (CLI_BGP4_INVALID_PREFIX_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4MpeCommRouteDeleteCommTable
 Input       :  The Indices
                Fsbgp4DeleteCommRtAfi
                Fsbgp4DeleteCommRtSafi
                Fsbgp4DeleteCommIpNetwork
                Fsbgp4DeleteCommIpPrefixLen
                Fsbgp4DeleteCommVal
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4MpeCommRouteDeleteCommTable (INT4
                                                   *pi4Fsbgp4DeleteCommRtAfi,
                                                   INT4
                                                   *pi4Fsbgp4DeleteCommRtSafi,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pFsbgp4DeleteCommIpNetwork,
                                                   INT4
                                                   *pi4Fsbgp4DeleteCommIpPrefixLen,
                                                   UINT4
                                                   *pu4Fsbgp4DeleteCommVal)
{
    tNetAddress         RtAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4GetFirstIndexCommRouteDelCommTable (u4Context, &RtAddress,
                                                    pu4Fsbgp4DeleteCommVal);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No Community Entry is present.\n");
        return SNMP_FAILURE;
    }

    i4Sts =
        Bgp4LowSetRouteAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO
                                (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                 (RtAddress)),
                                BGP4_SAFI_IN_NET_ADDRESS_INFO (RtAddress),
                                pFsbgp4DeleteCommIpNetwork, RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4Fsbgp4DeleteCommRtAfi =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                      (RtAddress));
    *pi4Fsbgp4DeleteCommRtSafi = BGP4_SAFI_IN_NET_ADDRESS_INFO (RtAddress);
    *pi4Fsbgp4DeleteCommIpPrefixLen =
        BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4MpeCommRouteDeleteCommTable
 Input       :  The Indices
                Fsbgp4DeleteCommRtAfi
                nextFsbgp4DeleteCommRtAfi
                Fsbgp4DeleteCommRtSafi
                nextFsbgp4DeleteCommRtSafi
                Fsbgp4DeleteCommIpNetwork
                nextFsbgp4DeleteCommIpNetwork
                Fsbgp4DeleteCommIpPrefixLen
                nextFsbgp4DeleteCommIpPrefixLen
                Fsbgp4DeleteCommVal
                nextFsbgp4DeleteCommVal
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4MpeCommRouteDeleteCommTable (INT4 i4Fsbgp4DeleteCommRtAfi,
                                                  INT4
                                                  *pi4NextFsbgp4DeleteCommRtAfi,
                                                  INT4 i4Fsbgp4DeleteCommRtSafi,
                                                  INT4
                                                  *pi4NextFsbgp4DeleteCommRtSafi,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsbgp4DeleteCommIpNetwork,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pNextFsbgp4DeleteCommIpNetwork,
                                                  INT4
                                                  i4Fsbgp4DeleteCommIpPrefixLen,
                                                  INT4
                                                  *pi4NextFsbgp4DeleteCommIpPrefixLen,
                                                  UINT4 u4Fsbgp4DeleteCommVal,
                                                  UINT4
                                                  *pu4NextFsbgp4DeleteCommVal)
{
    tNetAddress         RtAddress;
    tNetAddress         NextRtAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4DeleteCommRtAfi,
                                    i4Fsbgp4DeleteCommRtSafi,
                                    pFsbgp4DeleteCommIpNetwork, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4DeleteCommIpPrefixLen;

    i4Sts =
        Bgp4GetNextIndexCommRouteDelCommTable (u4Context, RtAddress,
                                               u4Fsbgp4DeleteCommVal,
                                               &NextRtAddress,
                                               pu4NextFsbgp4DeleteCommVal);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                  "\tERROR - No more Community entry Configured.\n");
        return SNMP_FAILURE;
    }
    i4Sts =
        Bgp4LowSetRouteAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO
                                (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                 (NextRtAddress)),
                                BGP4_SAFI_IN_NET_ADDRESS_INFO (NextRtAddress),
                                pNextFsbgp4DeleteCommIpNetwork, NextRtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsbgp4DeleteCommRtAfi =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                      (NextRtAddress));
    *pi4NextFsbgp4DeleteCommRtSafi =
        BGP4_SAFI_IN_NET_ADDRESS_INFO (NextRtAddress);
    *pi4NextFsbgp4DeleteCommIpPrefixLen =
        BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NextRtAddress);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeDeleteCommRowStatus
 Input       :  The Indices
                Fsbgp4DeleteCommRtAfi
                Fsbgp4DeleteCommRtSafi
                Fsbgp4DeleteCommIpNetwork
                Fsbgp4DeleteCommIpPrefixLen
                Fsbgp4DeleteCommVal

                The Object 
                retValFsbgp4DeleteCommRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeDeleteCommRowStatus (INT4 i4Fsbgp4DeleteCommRtAfi,
                                    INT4 i4Fsbgp4DeleteCommRtSafi,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsbgp4DeleteCommIpNetwork,
                                    INT4 i4Fsbgp4DeleteCommIpPrefixLen,
                                    UINT4 u4Fsbgp4DeleteCommVal,
                                    INT4 *pi4RetValFsbgp4DeleteCommRowStatus)
{
    tRouteConfComm     *pRouteConfComm = NULL;
    tCommProfile       *pCommProfile = NULL;
    tRouteConfComm      RouteConfComm;
    tNetAddress         RtAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4DeleteCommRtAfi,
                                    i4Fsbgp4DeleteCommRtSafi,
                                    pFsbgp4DeleteCommIpNetwork, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4DeleteCommIpPrefixLen;

    RouteConfComm.u4Context = u4Context;
    Bgp4CopyNetAddressStruct (&(RouteConfComm.RtNetAddress), RtAddress);
    pRouteConfComm = BGP4_RB_TREE_GET (ROUTES_COMM_DELETE_TBL,
                                       (tBgp4RBElem *) (&RouteConfComm));
    if (pRouteConfComm != NULL)
    {
        TMO_SLL_Scan (&(pRouteConfComm->TSConfComms), pCommProfile,
                      tCommProfile *)
        {
            if (pCommProfile->u4Comm == u4Fsbgp4DeleteCommVal)
            {
                *pi4RetValFsbgp4DeleteCommRowStatus = pCommProfile->u1RowStatus;
                return SNMP_SUCCESS;
            }
        }
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - Input Community not present.\n");
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4mpeDeleteCommRowStatus
 Input       :  The Indices
                Fsbgp4DeleteCommRtAfi
                Fsbgp4DeleteCommRtSafi
                Fsbgp4DeleteCommIpNetwork
                Fsbgp4DeleteCommIpPrefixLen
                Fsbgp4DeleteCommVal

                The Object 
                setValFsbgp4DeleteCommRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4mpeDeleteCommRowStatus (INT4 i4Fsbgp4DeleteCommRtAfi,
                                    INT4 i4Fsbgp4DeleteCommRtSafi,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsbgp4DeleteCommIpNetwork,
                                    INT4 i4Fsbgp4DeleteCommIpPrefixLen,
                                    UINT4 u4Fsbgp4DeleteCommVal,
                                    INT4 i4SetValFsbgp4DeleteCommRowStatus)
{
    tCommProfile       *pCommProfile = NULL;
    tNetAddress         RtAddress;
    INT4                i4RetSts;

    UINT4               u4Context;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4RetSts = Bgp4LowGetRouteAddress (i4Fsbgp4DeleteCommRtAfi,
                                       i4Fsbgp4DeleteCommRtSafi,
                                       pFsbgp4DeleteCommIpNetwork, &RtAddress);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4DeleteCommIpPrefixLen;

    switch (i4SetValFsbgp4DeleteCommRowStatus)
    {
        case CREATE_AND_GO:
            i4RetSts = CommCreateDeleteCommEntry (u4Context, RtAddress,
                                                  u4Fsbgp4DeleteCommVal,
                                                  &pCommProfile);
            if (i4RetSts == COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create Delete Comm Entry.\n");
                return SNMP_FAILURE;
            }
            pCommProfile->u1RowStatus = ACTIVE;
            break;

        case CREATE_AND_WAIT:
            i4RetSts = CommCreateDeleteCommEntry (u4Context, RtAddress,
                                                  u4Fsbgp4DeleteCommVal,
                                                  &pCommProfile);
            if (i4RetSts == COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create Delete Comm Entry.\n");
                return SNMP_FAILURE;
            }
            pCommProfile->u1RowStatus = NOT_IN_SERVICE;
            break;

        case ACTIVE:
            i4RetSts = CommGetDeleteCommEntry (u4Context, RtAddress,
                                               u4Fsbgp4DeleteCommVal,
                                               &pCommProfile);
            if (pCommProfile == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Delete Comm Entry present\n");
                return SNMP_FAILURE;
            }
            pCommProfile->u1RowStatus = ACTIVE;
            break;

        case NOT_IN_SERVICE:
            CommGetDeleteCommEntry (u4Context, RtAddress,
                                    u4Fsbgp4DeleteCommVal, &pCommProfile);
            if (pCommProfile == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Delete Comm Entry present\n");
                return SNMP_FAILURE;
            }
            pCommProfile->u1RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:
            i4RetSts = CommDeleteDeleteCommEntry (u4Context, RtAddress,
                                                  u4Fsbgp4DeleteCommVal,
                                                  &pCommProfile);
            break;

        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4mpeDeleteCommRowStatus,
                          u4SeqNum, TRUE, BgpLock, BgpUnLock, 6, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %s %i %u %i",
                      gpBgpCurrCxtNode->u4ContextId, i4Fsbgp4DeleteCommRtAfi,
                      i4Fsbgp4DeleteCommRtSafi, pFsbgp4DeleteCommIpNetwork,
                      i4Fsbgp4DeleteCommIpPrefixLen, u4Fsbgp4DeleteCommVal,
                      i4SetValFsbgp4DeleteCommRowStatus));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4mpeDeleteCommRowStatus
 Input       :  The Indices
                Fsbgp4DeleteCommRtAfi
                Fsbgp4DeleteCommRtSafi
                Fsbgp4DeleteCommIpNetwork
                Fsbgp4DeleteCommIpPrefixLen
                Fsbgp4DeleteCommVal

                The Object 
                testValFsbgp4DeleteCommRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4mpeDeleteCommRowStatus (UINT4 *pu4ErrorCode,
                                       INT4 i4Fsbgp4DeleteCommRtAfi,
                                       INT4 i4Fsbgp4DeleteCommRtSafi,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsbgp4DeleteCommIpNetwork,
                                       INT4 i4Fsbgp4DeleteCommIpPrefixLen,
                                       UINT4 u4Fsbgp4DeleteCommVal,
                                       INT4 i4TestValFsbgp4DeleteCommRowStatus)
{
    tCommProfile       *pCommProfile = NULL;
    tNetAddress         RtAddress;
    INT4                i4RetSts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Local AS not configured..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }
#ifdef L3VPN
    if ((i4Fsbgp4DeleteCommRtSafi == BGP4_INET_SAFI_LABEL) ||
        (i4Fsbgp4DeleteCommRtSafi == BGP4_INET_SAFI_VPNV4_UNICAST))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#endif
    i4RetSts = Bgp4LowGetRouteAddress (i4Fsbgp4DeleteCommRtAfi,
                                       i4Fsbgp4DeleteCommRtSafi,
                                       pFsbgp4DeleteCommIpNetwork, &RtAddress);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4DeleteCommIpPrefixLen;

    i4RetSts = CommValidateCommunity (u4Fsbgp4DeleteCommVal);
    if (i4RetSts == COMM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Community Value.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_COMM_VALUE_ERR);
        return SNMP_FAILURE;
    }

    switch (i4TestValFsbgp4DeleteCommRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            i4RetSts = CommGetDeleteCommEntry (u4Context, RtAddress,
                                               u4Fsbgp4DeleteCommVal,
                                               &pCommProfile);
            if (i4RetSts != COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Matching Delete Comm Entry is present\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
        case NOT_IN_SERVICE:
            i4RetSts = CommGetDeleteCommEntry (u4Context, RtAddress,
                                               u4Fsbgp4DeleteCommVal,
                                               &pCommProfile);
            if (i4RetSts != COMM_SUCCESS)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No Matching Delete Comm Entry present.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if ((pCommProfile->u1RowStatus == ACTIVE) ||
                (pCommProfile->u1RowStatus == NOT_IN_SERVICE) ||
                (pCommProfile->u1RowStatus == NOT_READY))
            {
                return SNMP_SUCCESS;
            }
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        case DESTROY:
            i4RetSts = CommGetDeleteCommEntry (u4Context, RtAddress,
                                               u4Fsbgp4DeleteCommVal,
                                               &pCommProfile);
            return SNMP_SUCCESS;
        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4MpeCommRouteDeleteCommTable
 Input       :  The Indices
                Fsbgp4DeleteCommRtAfi
                Fsbgp4DeleteCommRtSafi
                Fsbgp4DeleteCommIpNetwork
                Fsbgp4DeleteCommIpPrefixLen
                Fsbgp4DeleteCommVal
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4MpeCommRouteDeleteCommTable (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4MpeCommRouteCommSetStatusTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4MpeCommRouteCommSetStatusTable
 Input       :  The Indices
                Fsbgp4CommSetStatusAfi
                Fsbgp4CommSetStatusSafi
                Fsbgp4CommSetStatusIpNetwork
                Fsbgp4CommSetStatusIpPrefixLen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4MpeCommRouteCommSetStatusTable (INT4
                                                              i4Fsbgp4CommSetStatusAfi,
                                                              INT4
                                                              i4Fsbgp4CommSetStatusSafi,
                                                              tSNMP_OCTET_STRING_TYPE
                                                              *
                                                              pFsbgp4CommSetStatusIpNetwork,
                                                              INT4
                                                              i4Fsbgp4CommSetStatusIpPrefixLen)
{
    tNetAddress         RtAddress;
    INT4                i4Sts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4CommSetStatusAfi,
                                    i4Fsbgp4CommSetStatusSafi,
                                    pFsbgp4CommSetStatusIpNetwork, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4CommSetStatusIpPrefixLen;
    i4Sts = Bgp4ValidateCommRouteCommSetStatusTableIndex (RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid input Prefix/Length.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4MpeCommRouteCommSetStatusTable
 Input       :  The Indices
                Fsbgp4CommSetStatusAfi
                Fsbgp4CommSetStatusSafi
                Fsbgp4CommSetStatusIpNetwork
                Fsbgp4CommSetStatusIpPrefixLen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4MpeCommRouteCommSetStatusTable (INT4
                                                      *pi4Fsbgp4CommSetStatusAfi,
                                                      INT4
                                                      *pi4Fsbgp4CommSetStatusSafi,
                                                      tSNMP_OCTET_STRING_TYPE *
                                                      pFsbgp4CommSetStatusIpNetwork,
                                                      INT4
                                                      *pi4Fsbgp4CommSetStatusIpPrefixLen)
{
    tNetAddress         RtAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts =
        Bgp4GetFirstIndexCommRouteCommSetStatusTable (u4Context, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i4Sts =
        Bgp4LowSetRouteAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO
                                (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                 (RtAddress)),
                                BGP4_SAFI_IN_NET_ADDRESS_INFO (RtAddress),
                                pFsbgp4CommSetStatusIpNetwork, RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4Fsbgp4CommSetStatusAfi =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                      (RtAddress));
    *pi4Fsbgp4CommSetStatusSafi = BGP4_SAFI_IN_NET_ADDRESS_INFO (RtAddress);
    *pi4Fsbgp4CommSetStatusIpPrefixLen =
        BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4MpeCommRouteCommSetStatusTable
 Input       :  The Indices
                Fsbgp4CommSetStatusAfi
                nextFsbgp4CommSetStatusAfi
                Fsbgp4CommSetStatusSafi
                nextFsbgp4CommSetStatusSafi
                Fsbgp4CommSetStatusIpNetwork
                nextFsbgp4CommSetStatusIpNetwork
                Fsbgp4CommSetStatusIpPrefixLen
                nextFsbgp4CommSetStatusIpPrefixLen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4MpeCommRouteCommSetStatusTable (INT4
                                                     i4Fsbgp4CommSetStatusAfi,
                                                     INT4
                                                     *pi4NextFsbgp4CommSetStatusAfi,
                                                     INT4
                                                     i4Fsbgp4CommSetStatusSafi,
                                                     INT4
                                                     *pi4NextFsbgp4CommSetStatusSafi,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pFsbgp4CommSetStatusIpNetwork,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pNextFsbgp4CommSetStatusIpNetwork,
                                                     INT4
                                                     i4Fsbgp4CommSetStatusIpPrefixLen,
                                                     INT4
                                                     *pi4NextFsbgp4CommSetStatusIpPrefixLen)
{
    tNetAddress         RtAddress;
    tNetAddress         NextRtAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4CommSetStatusAfi,
                                    i4Fsbgp4CommSetStatusSafi,
                                    pFsbgp4CommSetStatusIpNetwork, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4CommSetStatusIpPrefixLen;

    i4Sts = Bgp4GetNextIndexCommRouteCommSetStatusTable (u4Context, RtAddress,
                                                         &NextRtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts =
        Bgp4LowSetRouteAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO
                                (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                 (NextRtAddress)),
                                BGP4_SAFI_IN_NET_ADDRESS_INFO (NextRtAddress),
                                pNextFsbgp4CommSetStatusIpNetwork,
                                NextRtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4NextFsbgp4CommSetStatusAfi =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                      (NextRtAddress));
    *pi4NextFsbgp4CommSetStatusSafi =
        BGP4_SAFI_IN_NET_ADDRESS_INFO (NextRtAddress);
    *pi4NextFsbgp4CommSetStatusIpPrefixLen =
        BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NextRtAddress);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeCommSetStatus
 Input       :  The Indices
                Fsbgp4CommSetStatusAfi
                Fsbgp4CommSetStatusSafi
                Fsbgp4CommSetStatusIpNetwork
                Fsbgp4CommSetStatusIpPrefixLen

                The Object 
                retValFsbgp4CommSetStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeCommSetStatus (INT4 i4Fsbgp4CommSetStatusAfi,
                              INT4 i4Fsbgp4CommSetStatusSafi,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsbgp4CommSetStatusIpNetwork,
                              INT4 i4Fsbgp4CommSetStatusIpPrefixLen,
                              INT4 *pi4RetValFsbgp4CommSetStatus)
{
    tRouteCommSetStatus *pRouteCommSetStatus = NULL;
    tNetAddress         RtAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4CommSetStatusAfi,
                                    i4Fsbgp4CommSetStatusSafi,
                                    pFsbgp4CommSetStatusIpNetwork, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4CommSetStatusIpPrefixLen;

    CommGetRouteCommSetStatusEntry (u4Context, RtAddress, &pRouteCommSetStatus);
    if (pRouteCommSetStatus == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching entry is present.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4CommSetStatus = pRouteCommSetStatus->i1CommSetStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeCommSetStatusRowStatus
 Input       :  The Indices
                Fsbgp4CommSetStatusAfi
                Fsbgp4CommSetStatusSafi
                Fsbgp4CommSetStatusIpNetwork
                Fsbgp4CommSetStatusIpPrefixLen

                The Object 
                retValFsbgp4CommSetStatusRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeCommSetStatusRowStatus (INT4 i4Fsbgp4CommSetStatusAfi,
                                       INT4 i4Fsbgp4CommSetStatusSafi,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsbgp4CommSetStatusIpNetwork,
                                       INT4 i4Fsbgp4CommSetStatusIpPrefixLen,
                                       INT4
                                       *pi4RetValFsbgp4CommSetStatusRowStatus)
{
    tRouteCommSetStatus *pRouteCommSetStatus = NULL;
    tNetAddress         RtAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4CommSetStatusAfi,
                                    i4Fsbgp4CommSetStatusSafi,
                                    pFsbgp4CommSetStatusIpNetwork, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4CommSetStatusIpPrefixLen;

    CommGetRouteCommSetStatusEntry (u4Context, RtAddress, &pRouteCommSetStatus);
    if (pRouteCommSetStatus == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching entry is present.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4CommSetStatusRowStatus = pRouteCommSetStatus->u1RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4mpeCommSetStatus
 Input       :  The Indices
                Fsbgp4CommSetStatusAfi
                Fsbgp4CommSetStatusSafi
                Fsbgp4CommSetStatusIpNetwork
                Fsbgp4CommSetStatusIpPrefixLen

                The Object 
                setValFsbgp4CommSetStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4mpeCommSetStatus (INT4 i4Fsbgp4CommSetStatusAfi,
                              INT4 i4Fsbgp4CommSetStatusSafi,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsbgp4CommSetStatusIpNetwork,
                              INT4 i4Fsbgp4CommSetStatusIpPrefixLen,
                              INT4 i4SetValFsbgp4CommSetStatus)
{
    tRouteCommSetStatus *pRouteCommSetStatus = NULL;
    tNetAddress         RtAddress;
    INT4                i4Sts;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4CommSetStatusAfi,
                                    i4Fsbgp4CommSetStatusSafi,
                                    pFsbgp4CommSetStatusIpNetwork, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4CommSetStatusIpPrefixLen;

    CommGetRouteCommSetStatusEntry (u4Context, RtAddress, &pRouteCommSetStatus);
    if (pRouteCommSetStatus == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching entry is present.\n");
        return SNMP_FAILURE;
    }
    pRouteCommSetStatus->i1CommSetStatus = (INT1) i4SetValFsbgp4CommSetStatus;
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4mpeCommSetStatus, u4SeqNum,
                          FALSE, BgpLock, BgpUnLock, 5, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %s %i %i",
                      gpBgpCurrCxtNode->u4ContextId, i4Fsbgp4CommSetStatusAfi,
                      i4Fsbgp4CommSetStatusSafi, pFsbgp4CommSetStatusIpNetwork,
                      i4Fsbgp4CommSetStatusIpPrefixLen,
                      i4SetValFsbgp4CommSetStatus));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsbgp4mpeCommSetStatusRowStatus
 Input       :  The Indices
                Fsbgp4CommSetStatusAfi
                Fsbgp4CommSetStatusSafi
                Fsbgp4CommSetStatusIpNetwork
                Fsbgp4CommSetStatusIpPrefixLen

                The Object 
                setValFsbgp4CommSetStatusRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4mpeCommSetStatusRowStatus (INT4 i4Fsbgp4CommSetStatusAfi,
                                       INT4 i4Fsbgp4CommSetStatusSafi,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsbgp4CommSetStatusIpNetwork,
                                       INT4 i4Fsbgp4CommSetStatusIpPrefixLen,
                                       INT4
                                       i4SetValFsbgp4CommSetStatusRowStatus)
{
    tRouteCommSetStatus *pRouteCommSetStatus = NULL;
    tNetAddress         RtAddress;
    UINT4               u4RtMemSts;
    UINT4               u4Ret;
    INT4                i4RetSts;
    UINT4               u4Context;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4RetSts = Bgp4LowGetRouteAddress (i4Fsbgp4CommSetStatusAfi,
                                       i4Fsbgp4CommSetStatusSafi,
                                       pFsbgp4CommSetStatusIpNetwork,
                                       &RtAddress);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4CommSetStatusIpPrefixLen;

    switch (i4SetValFsbgp4CommSetStatusRowStatus)
    {
        case CREATE_AND_GO:
            i4RetSts =
                CommCreateRouteCommSetStatusEntry (u4Context, RtAddress,
                                                   &pRouteCommSetStatus);
            if (i4RetSts == COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create Comm "
                          "Set Status entry.\n");
                return SNMP_FAILURE;
            }
            pRouteCommSetStatus->i1CommSetStatus = COMM_MODIFY;
            pRouteCommSetStatus->u1RowStatus = ACTIVE;
            break;

        case CREATE_AND_WAIT:
            i4RetSts =
                CommCreateRouteCommSetStatusEntry (u4Context, RtAddress,
                                                   &pRouteCommSetStatus);
            if (i4RetSts == COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create Comm "
                          "Set Status entry.\n");
                return SNMP_FAILURE;
            }
            pRouteCommSetStatus->i1CommSetStatus = COMM_MODIFY;
            pRouteCommSetStatus->u1RowStatus = NOT_IN_SERVICE;
            break;

        case ACTIVE:
            CommGetRouteCommSetStatusEntry (u4Context, RtAddress,
                                            &pRouteCommSetStatus);
            if (pRouteCommSetStatus == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Comm Set Status "
                          "entry present.\n");
                return SNMP_FAILURE;
            }
            pRouteCommSetStatus->u1RowStatus = ACTIVE;
            break;

        case NOT_IN_SERVICE:
            CommGetRouteCommSetStatusEntry (u4Context, RtAddress,
                                            &pRouteCommSetStatus);
            if (pRouteCommSetStatus == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Comm Set Status "
                          "entry present.\n");
                return SNMP_FAILURE;
            }
            pRouteCommSetStatus->u1RowStatus = ACTIVE;
            break;

        case DESTROY:
            CommGetRouteCommSetStatusEntry (u4Context, RtAddress,
                                            &pRouteCommSetStatus);
            if (pRouteCommSetStatus == NULL)
            {
                return SNMP_SUCCESS;
            }
            u4Ret = BGP4_RB_TREE_REMOVE (ROUTES_COMM_SET_STATUS_TBL,
                                         (tBgp4RBElem *) pRouteCommSetStatus);
            if (u4Ret == RB_FAILURE)
            {
                return SNMP_FAILURE;
            }
            u4RtMemSts =
                COMM_ROUTES_COMM_SET_STATUS_ENTRY_FREE (pRouteCommSetStatus);
            if (u4RtMemSts == MEM_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4mpeCommSetStatusRowStatus,
                          u4SeqNum, TRUE, BgpLock, BgpUnLock, 5, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %s %i %i",
                      gpBgpCurrCxtNode->u4ContextId, i4Fsbgp4CommSetStatusAfi,
                      i4Fsbgp4CommSetStatusSafi, pFsbgp4CommSetStatusIpNetwork,
                      i4Fsbgp4CommSetStatusIpPrefixLen,
                      i4SetValFsbgp4CommSetStatusRowStatus));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4mpeCommSetStatus
 Input       :  The Indices
                Fsbgp4CommSetStatusAfi
                Fsbgp4CommSetStatusSafi
                Fsbgp4CommSetStatusIpNetwork
                Fsbgp4CommSetStatusIpPrefixLen

                The Object 
                testValFsbgp4CommSetStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4mpeCommSetStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4Fsbgp4CommSetStatusAfi,
                                 INT4 i4Fsbgp4CommSetStatusSafi,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsbgp4CommSetStatusIpNetwork,
                                 INT4 i4Fsbgp4CommSetStatusIpPrefixLen,
                                 INT4 i4TestValFsbgp4CommSetStatus)
{
    tNetAddress         RtAddress;
    INT4                i4RetSts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Local AS not configured..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }

#ifdef L3VPN
    if ((i4Fsbgp4CommSetStatusSafi == BGP4_INET_SAFI_LABEL) ||
        (i4Fsbgp4CommSetStatusSafi == BGP4_INET_SAFI_VPNV4_UNICAST))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#endif
    i4RetSts = Bgp4LowGetRouteAddress (i4Fsbgp4CommSetStatusAfi,
                                       i4Fsbgp4CommSetStatusSafi,
                                       pFsbgp4CommSetStatusIpNetwork,
                                       &RtAddress);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4CommSetStatusIpPrefixLen;
    i4RetSts = Bgp4ValidateCommRouteCommSetStatusTableIndex (RtAddress);
    if (i4RetSts == COMM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Input Prefix/Length Value.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PREFIX_ERR);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4CommSetStatus < COMM_SET) ||
        (i4TestValFsbgp4CommSetStatus > COMM_MODIFY))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Comm Set Status Value.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_COMM_SET_STATUS_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4mpeCommSetStatusRowStatus
 Input       :  The Indices
                Fsbgp4CommSetStatusAfi
                Fsbgp4CommSetStatusSafi
                Fsbgp4CommSetStatusIpNetwork
                Fsbgp4CommSetStatusIpPrefixLen

                The Object 
                testValFsbgp4CommSetStatusRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4mpeCommSetStatusRowStatus (UINT4 *pu4ErrorCode,
                                          INT4 i4Fsbgp4CommSetStatusAfi,
                                          INT4 i4Fsbgp4CommSetStatusSafi,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsbgp4CommSetStatusIpNetwork,
                                          INT4 i4Fsbgp4CommSetStatusIpPrefixLen,
                                          INT4
                                          i4TestValFsbgp4CommSetStatusRowStatus)
{
    tRouteCommSetStatus *pRouteCommSetStatus = NULL;
    tNetAddress         RtAddress;
    INT4                i4RetSts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Local AS not configured..\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }
#ifdef L3VPN
    if ((i4Fsbgp4CommSetStatusSafi == BGP4_INET_SAFI_LABEL) ||
        (i4Fsbgp4CommSetStatusSafi == BGP4_INET_SAFI_VPNV4_UNICAST))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#endif

    i4RetSts = Bgp4LowGetRouteAddress (i4Fsbgp4CommSetStatusAfi,
                                       i4Fsbgp4CommSetStatusSafi,
                                       pFsbgp4CommSetStatusIpNetwork,
                                       &RtAddress);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4CommSetStatusIpPrefixLen;
    i4RetSts = Bgp4ValidateCommRouteCommSetStatusTableIndex (RtAddress);
    if (i4RetSts == COMM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Local AS not configured..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_PREFIX_LEN_ERR);
        return SNMP_FAILURE;
    }

    switch (i4TestValFsbgp4CommSetStatusRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            i4RetSts =
                CommGetRouteCommSetStatusEntry (u4Context, RtAddress,
                                                &pRouteCommSetStatus);
            if (i4RetSts != COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Matching Comm Set Status Entry exist.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
        case NOT_IN_SERVICE:
            i4RetSts =
                CommGetRouteCommSetStatusEntry (u4Context, RtAddress,
                                                &pRouteCommSetStatus);
            if (i4RetSts != COMM_SUCCESS)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No Matching Comm Set Status "
                          "Entry exist.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if ((pRouteCommSetStatus->u1RowStatus == ACTIVE) ||
                (pRouteCommSetStatus->u1RowStatus == NOT_IN_SERVICE) ||
                (pRouteCommSetStatus->u1RowStatus == NOT_READY))
            {
                return SNMP_SUCCESS;
            }
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        case DESTROY:
            CommGetRouteCommSetStatusEntry (u4Context, RtAddress,
                                            &pRouteCommSetStatus);
            return SNMP_SUCCESS;
        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4MpeCommRouteCommSetStatusTable
 Input       :  The Indices
                Fsbgp4CommSetStatusAfi
                Fsbgp4CommSetStatusSafi
                Fsbgp4CommSetStatusIpNetwork
                Fsbgp4CommSetStatusIpPrefixLen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4MpeCommRouteCommSetStatusTable (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4CommInFilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4CommInFilterTable
 Input       :  The Indices
                Fsbgp4InFilterCommVal
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4CommInFilterTable (UINT4 u4Fsbgp4InFilterCommVal)
{
    INT4                i4RetSts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    i4RetSts = CommValidateCommunity (u4Fsbgp4InFilterCommVal);
    if (i4RetSts == COMM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Community Value.\n");
        CLI_SET_ERR (CLI_BGP4_COMM_VALUE_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4CommInFilterTable
 Input       :  The Indices
                Fsbgp4InFilterCommVal
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4CommInFilterTable (UINT4 *pu4Fsbgp4InFilterCommVal)
{
    tCommFilterInfo    *pCommFilterInfo = NULL;
    tCommFilterInfo     CommFilterInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    MEMSET (&CommFilterInfo, 0, sizeof (tCommFilterInfo));
    CommFilterInfo.u4Context = u4Context;
    pCommFilterInfo = (tCommFilterInfo *)
        BGP4_RB_TREE_GET_NEXT (COMM_INPUT_FILTER_TBL,
                               (tBgp4RBElem *) (&CommFilterInfo), NULL);
    if ((pCommFilterInfo != NULL) && (pCommFilterInfo->u4Context == u4Context))
    {
        *pu4Fsbgp4InFilterCommVal = pCommFilterInfo->u4Comm;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4CommInFilterTable
 Input       :  The Indices
                Fsbgp4InFilterCommVal
                nextFsbgp4InFilterCommVal
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4CommInFilterTable (UINT4 u4Fsbgp4InFilterCommVal,
                                        UINT4 *pu4NextFsbgp4InFilterCommVal)
{
    tCommFilterInfo    *pCommFilterInfo = NULL;
    tCommFilterInfo     CommFilterInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    CommFilterInfo.u4Comm = u4Fsbgp4InFilterCommVal;
    CommFilterInfo.u4Context = u4Context;

    pCommFilterInfo =
        BGP4_RB_TREE_GET_NEXT (COMM_INPUT_FILTER_TBL,
                               (tBgp4RBElem *) (&CommFilterInfo), NULL);
    if ((pCommFilterInfo != NULL) && (pCommFilterInfo->u4Context == u4Context))
    {

        *pu4NextFsbgp4InFilterCommVal = pCommFilterInfo->u4Comm;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4CommIncomingFilterStatus
 Input       :  The Indices
                Fsbgp4InFilterCommVal

                The Object 
                retValFsbgp4CommIncomingFilterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4CommIncomingFilterStatus (UINT4 u4Fsbgp4InFilterCommVal,
                                      INT4
                                      *pi4RetValFsbgp4CommIncomingFilterStatus)
{
    tCommFilterInfo    *pCommFilterInfo = NULL;
    INT4                i4GetInVal;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    CommGetInputFilterEntry (u4Context, u4Fsbgp4InFilterCommVal,
                             &pCommFilterInfo);
    if (pCommFilterInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Comm Incoming Filter entry exist.\n");
        return SNMP_FAILURE;
    }
    COMM_SNMP_GET_INPUT_FILTER_POLICY (i4GetInVal, pCommFilterInfo);
    *pi4RetValFsbgp4CommIncomingFilterStatus = i4GetInVal;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4InFilterRowStatus
 Input       :  The Indices
                Fsbgp4InFilterCommVal

                The Object 
                retValFsbgp4InFilterRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4InFilterRowStatus (UINT4 u4Fsbgp4InFilterCommVal,
                               INT4 *pi4RetValFsbgp4InFilterRowStatus)
{
    tCommFilterInfo    *pCommFilterInfo = NULL;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    CommGetInputFilterEntry (u4Context, u4Fsbgp4InFilterCommVal,
                             &pCommFilterInfo);
    if (pCommFilterInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Comm Incoming Filter Entry exist.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4InFilterRowStatus = pCommFilterInfo->u1RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4CommIncomingFilterStatus
 Input       :  The Indices
                Fsbgp4InFilterCommVal

                The Object 
                setValFsbgp4CommIncomingFilterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4CommIncomingFilterStatus (UINT4 u4Fsbgp4InFilterCommVal,
                                      INT4
                                      i4SetValFsbgp4CommIncomingFilterStatus)
{
    tCommFilterInfo    *pCommFilterInfo = NULL;

    UINT4               u4Context;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    CommGetInputFilterEntry (u4Context, u4Fsbgp4InFilterCommVal,
                             &pCommFilterInfo);
    if (pCommFilterInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Comm Incoming Filter Entry exist.\n");
        return SNMP_FAILURE;
    }
    COMM_SNMP_SET_INPUT_FILTER_POLICY (pCommFilterInfo,
                                       i4SetValFsbgp4CommIncomingFilterStatus);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4CommIncomingFilterStatus,
                          u4SeqNum, FALSE, BgpLock, BgpUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", gpBgpCurrCxtNode->u4ContextId,
                      u4Fsbgp4InFilterCommVal,
                      i4SetValFsbgp4CommIncomingFilterStatus));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsbgp4InFilterRowStatus
 Input       :  The Indices
                Fsbgp4InFilterCommVal

                The Object 
                setValFsbgp4InFilterRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4InFilterRowStatus (UINT4 u4Fsbgp4InFilterCommVal,
                               INT4 i4SetValFsbgp4InFilterRowStatus)
{
    tCommFilterInfo    *pCommFilterInfo = NULL;
    UINT4               u4RtMemSts;
    UINT4               u4Ret;
    INT4                i4RetSts;
    UINT4               u4Context;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    switch (i4SetValFsbgp4InFilterRowStatus)
    {
        case CREATE_AND_GO:
            i4RetSts =
                CommCreateInputFilterEntry (u4Context, u4Fsbgp4InFilterCommVal,
                                            &pCommFilterInfo);
            if (i4RetSts == COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create Comm Incoming "
                          "Filter Entry.\n");
                return SNMP_FAILURE;
            }
            pCommFilterInfo->i1FilterPolicy = COMM_ACCEPT;
            pCommFilterInfo->u1RowStatus = ACTIVE;
            break;
        case CREATE_AND_WAIT:
            i4RetSts =
                CommCreateInputFilterEntry (u4Context, u4Fsbgp4InFilterCommVal,
                                            &pCommFilterInfo);
            if (i4RetSts == COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create Comm Incoming "
                          "Filter Entry.\n");
                return SNMP_FAILURE;
            }
            pCommFilterInfo->i1FilterPolicy = COMM_ACCEPT;
            pCommFilterInfo->u1RowStatus = NOT_IN_SERVICE;
            break;
        case ACTIVE:
            CommGetInputFilterEntry (u4Context, u4Fsbgp4InFilterCommVal,
                                     &pCommFilterInfo);
            if (pCommFilterInfo == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Comm Incoming Filter "
                          "Entry exist.\n");
                return SNMP_FAILURE;
            }
            pCommFilterInfo->u1RowStatus = ACTIVE;
            break;
        case NOT_IN_SERVICE:
            CommGetInputFilterEntry (u4Context, u4Fsbgp4InFilterCommVal,
                                     &pCommFilterInfo);
            if (pCommFilterInfo == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Comm Incoming "
                          "Filter Entry exist.\n");
                return SNMP_FAILURE;
            }
            pCommFilterInfo->u1RowStatus = NOT_IN_SERVICE;
            break;
        case DESTROY:
            CommGetInputFilterEntry (u4Context, u4Fsbgp4InFilterCommVal,
                                     &pCommFilterInfo);
            if (pCommFilterInfo == NULL)
            {
                break;
            }
            u4Ret = BGP4_RB_TREE_REMOVE (COMM_INPUT_FILTER_TBL,
                                         (tBgp4RBElem *) pCommFilterInfo);
            if (u4Ret == RB_FAILURE)
            {
                return SNMP_FAILURE;
            }
            u4RtMemSts = COMM_INPUT_FILTER_ENTRY_FREE (pCommFilterInfo);
            if (u4RtMemSts == MEM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_BUFFER_TRC,BGP4_MOD_NAME,
                      "\tERROR - Memrelease failed.\n");
            }

            break;
        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4InFilterRowStatus, u4SeqNum,
                          TRUE, BgpLock, BgpUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", gpBgpCurrCxtNode->u4ContextId,
                      u4Fsbgp4InFilterCommVal,
                      i4SetValFsbgp4InFilterRowStatus));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4CommIncomingFilterStatus
 Input       :  The Indices
                Fsbgp4InFilterCommVal

                The Object 
                testValFsbgp4CommIncomingFilterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4CommIncomingFilterStatus (UINT4 *pu4ErrorCode,
                                         UINT4 u4Fsbgp4InFilterCommVal,
                                         INT4
                                         i4TestValFsbgp4CommIncomingFilterStatus)
{
    INT4                i4RetSts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    i4RetSts = CommValidateCommunity (u4Fsbgp4InFilterCommVal);
    if (i4RetSts == COMM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Community Value.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsbgp4CommIncomingFilterStatus == (COMM_ACCEPT +
                                                     BGP4_INCREMENT_BY_ONE)) ||
        (i4TestValFsbgp4CommIncomingFilterStatus == (COMM_DENY +
                                                     BGP4_INCREMENT_BY_ONE)))
    {
        return SNMP_SUCCESS;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - Invalid Comm Incoming Filter Entry Status.\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4InFilterRowStatus
 Input       :  The Indices
                Fsbgp4InFilterCommVal

                The Object 
                testValFsbgp4InFilterRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4InFilterRowStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4Fsbgp4InFilterCommVal,
                                  INT4 i4TestValFsbgp4InFilterRowStatus)
{
    tCommFilterInfo    *pCommFilterInfo = NULL;
    INT4                i4RetSts;
    UINT4               u4BgpMaxInFilterEntries = 0;
    UINT4               u4BgpCurInFilterEntries = 0;
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Local AS not configured..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Validation of comm val */
    i4RetSts = CommValidateCommunity (u4Fsbgp4InFilterCommVal);
    if (i4RetSts == COMM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Community Value.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_COMM_VALUE_ERR);
        return SNMP_FAILURE;
    }

    switch (i4TestValFsbgp4InFilterRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            i4RetSts =
                CommGetInputFilterEntry (u4Context, u4Fsbgp4InFilterCommVal,
                                         &pCommFilterInfo);

            if (i4RetSts != COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Matching Comm Incoming Filter "
                          "Table entry exists.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            u4BgpMaxInFilterEntries =
                FsBGPSizingParams[MAX_BGP_COMM_IN_FILTER_INFOS_SIZING_ID].
                u4PreAllocatedUnits;
            BGP4_RB_TREE_GET_COUNT (COMM_INPUT_FILTER_TBL,
                                    &u4BgpCurInFilterEntries);
            if (u4BgpCurInFilterEntries >= u4BgpMaxInFilterEntries)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Max entries created.\n");
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_BGP4_EXCEED_MAX_ENTRIES);
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
        case NOT_IN_SERVICE:
            i4RetSts =
                CommGetInputFilterEntry (u4Context, u4Fsbgp4InFilterCommVal,
                                         &pCommFilterInfo);

            if (i4RetSts != COMM_SUCCESS)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No Matching Comm Incoming "
                          "Filter Table entry exists.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if ((pCommFilterInfo->u1RowStatus == ACTIVE) ||
                (pCommFilterInfo->u1RowStatus == NOT_IN_SERVICE) ||
                (pCommFilterInfo->u1RowStatus == NOT_READY))
            {
                return SNMP_SUCCESS;
            }
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        case DESTROY:
            CommGetInputFilterEntry (u4Context, u4Fsbgp4InFilterCommVal,
                                     &pCommFilterInfo);
            return SNMP_SUCCESS;
        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4CommInFilterTable
 Input       :  The Indices
                Fsbgp4InFilterCommVal
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4CommInFilterTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4CommOutFilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4CommOutFilterTable
 Input       :  The Indices
                Fsbgp4OutFilterCommVal
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4CommOutFilterTable (UINT4
                                                  u4Fsbgp4OutFilterCommVal)
{
    INT4                i4RetSts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4RetSts = CommValidateCommunity (u4Fsbgp4OutFilterCommVal);
    if (i4RetSts == COMM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Community Value.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4CommOutFilterTable
 Input       :  The Indices
                Fsbgp4OutFilterCommVal
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4CommOutFilterTable (UINT4 *pu4Fsbgp4OutFilterCommVal)
{
    tCommFilterInfo    *pCommFilterInfo = NULL;
    tCommFilterInfo     CommFilterInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    MEMSET (&CommFilterInfo, 0, sizeof (tCommFilterInfo));
    CommFilterInfo.u4Context = gpBgpCurrCxtNode->u4ContextId;
    pCommFilterInfo = (tCommFilterInfo *)
        BGP4_RB_TREE_GET_NEXT (COMM_OUTPUT_FILTER_TBL,
                               (tBgp4RBElem *) (&CommFilterInfo), NULL);
    if ((pCommFilterInfo != NULL)
        && (pCommFilterInfo->u4Context == gpBgpCurrCxtNode->u4ContextId))
    {
        *pu4Fsbgp4OutFilterCommVal = pCommFilterInfo->u4Comm;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4CommOutFilterTable
 Input       :  The Indices
                Fsbgp4OutFilterCommVal
                nextFsbgp4OutFilterCommVal
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4CommOutFilterTable (UINT4 u4Fsbgp4OutFilterCommVal,
                                         UINT4 *pu4NextFsbgp4OutFilterCommVal)
{
    tCommFilterInfo    *pCommFilterInfo = NULL;
    tCommFilterInfo     CommFilterInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    CommFilterInfo.u4Comm = u4Fsbgp4OutFilterCommVal;
    CommFilterInfo.u4Context = u4Context;
    pCommFilterInfo =
        BGP4_RB_TREE_GET_NEXT (COMM_OUTPUT_FILTER_TBL,
                               (tBgp4RBElem *) (&CommFilterInfo), NULL);
    if ((pCommFilterInfo != NULL)
        && (pCommFilterInfo->u4Context == gpBgpCurrCxtNode->u4ContextId))
    {
        *pu4NextFsbgp4OutFilterCommVal = pCommFilterInfo->u4Comm;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4CommOutgoingFilterStatus
 Input       :  The Indices
                Fsbgp4OutFilterCommVal

                The Object 
                retValFsbgp4CommOutgoingFilterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4CommOutgoingFilterStatus (UINT4 u4Fsbgp4OutFilterCommVal,
                                      INT4
                                      *pi4RetValFsbgp4CommOutgoingFilterStatus)
{
    tCommFilterInfo    *pCommFilterInfo = NULL;
    INT4                i4GetOutVal;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    CommGetOutputFilterEntry (u4Context, u4Fsbgp4OutFilterCommVal,
                              &pCommFilterInfo);
    if (pCommFilterInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Comm Out Filter Entry exist.\n");
        return SNMP_FAILURE;
    }
    COMM_SNMP_GET_OUTPUT_FILTER_POLICY (i4GetOutVal, pCommFilterInfo);
    *pi4RetValFsbgp4CommOutgoingFilterStatus = i4GetOutVal;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4OutFilterRowStatus
 Input       :  The Indices
                Fsbgp4OutFilterCommVal

                The Object 
                retValFsbgp4OutFilterRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4OutFilterRowStatus (UINT4 u4Fsbgp4OutFilterCommVal,
                                INT4 *pi4RetValFsbgp4OutFilterRowStatus)
{
    tCommFilterInfo    *pCommFilterInfo = NULL;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    CommGetOutputFilterEntry (u4Context, u4Fsbgp4OutFilterCommVal,
                              &pCommFilterInfo);
    if (pCommFilterInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Comm Out Filter Entry exists.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4OutFilterRowStatus = pCommFilterInfo->u1RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4CommOutgoingFilterStatus
 Input       :  The Indices
                Fsbgp4OutFilterCommVal

                The Object 
                setValFsbgp4CommOutgoingFilterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4CommOutgoingFilterStatus (UINT4 u4Fsbgp4OutFilterCommVal,
                                      INT4
                                      i4SetValFsbgp4CommOutgoingFilterStatus)
{
    tCommFilterInfo    *pCommFilterInfo = NULL;

    UINT4               u4Context;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    CommGetOutputFilterEntry (u4Context, u4Fsbgp4OutFilterCommVal,
                              &pCommFilterInfo);
    if (pCommFilterInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No Comm Out Filter Entry exist.\n");
        return SNMP_FAILURE;
    }
    COMM_SNMP_SET_OUTPUT_FILTER_POLICY (pCommFilterInfo,
                                        i4SetValFsbgp4CommOutgoingFilterStatus);

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4CommOutgoingFilterStatus,
                          u4SeqNum, FALSE, BgpLock, BgpUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", gpBgpCurrCxtNode->u4ContextId,
                      u4Fsbgp4OutFilterCommVal,
                      i4SetValFsbgp4CommOutgoingFilterStatus));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsbgp4OutFilterRowStatus
 Input       :  The Indices
                Fsbgp4OutFilterCommVal

                The Object 
                setValFsbgp4OutFilterRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4OutFilterRowStatus (UINT4 u4Fsbgp4OutFilterCommVal,
                                INT4 i4SetValFsbgp4OutFilterRowStatus)
{
    tCommFilterInfo    *pCommFilterInfo = NULL;
    UINT4               u4RtMemSts;
    UINT4               u4Ret;
    INT4                i4RetSts;

    UINT4               u4Context;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    switch (i4SetValFsbgp4OutFilterRowStatus)
    {
        case CREATE_AND_GO:
            i4RetSts =
                CommCreateOutputFilterEntry (u4Context,
                                             u4Fsbgp4OutFilterCommVal,
                                             &pCommFilterInfo);
            if (i4RetSts == COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create Comm Out "
                          "Filter entry.\n");
                return SNMP_FAILURE;
            }
            pCommFilterInfo->i1FilterPolicy = COMM_ADVERTISE;
            pCommFilterInfo->u1RowStatus = ACTIVE;
            break;
        case CREATE_AND_WAIT:
            i4RetSts =
                CommCreateOutputFilterEntry (u4Context,
                                             u4Fsbgp4OutFilterCommVal,
                                             &pCommFilterInfo);
            if (i4RetSts == COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create Comm Out "
                          "Filter entry.\n");
                return SNMP_FAILURE;
            }
            pCommFilterInfo->i1FilterPolicy = COMM_ADVERTISE;
            pCommFilterInfo->u1RowStatus = NOT_IN_SERVICE;
            break;

        case ACTIVE:
            i4RetSts =
                CommGetOutputFilterEntry (u4Context, u4Fsbgp4OutFilterCommVal,
                                          &pCommFilterInfo);
            if (pCommFilterInfo == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Comm Out Filter "
                          "entry exist.\n");
                return SNMP_FAILURE;
            }
            pCommFilterInfo->u1RowStatus = ACTIVE;
            break;

        case NOT_IN_SERVICE:
            CommGetOutputFilterEntry (u4Context, u4Fsbgp4OutFilterCommVal,
                                      &pCommFilterInfo);
            if (pCommFilterInfo == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Comm Out Filter "
                          "entr exist.\n");
                return SNMP_FAILURE;
            }
            pCommFilterInfo->u1RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:
            CommGetOutputFilterEntry (u4Context, u4Fsbgp4OutFilterCommVal,
                                      &pCommFilterInfo);
            if (pCommFilterInfo == NULL)
            {
                break;
            }
            u4Ret = BGP4_RB_TREE_REMOVE (COMM_OUTPUT_FILTER_TBL,
                                         (tBgp4RBElem *) pCommFilterInfo);
            if (u4Ret == RB_FAILURE)
            {
                return SNMP_FAILURE;
            }
            u4RtMemSts = COMM_OUTPUT_FILTER_ENTRY_FREE (pCommFilterInfo);
            if (u4RtMemSts == MEM_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status.\n");
            return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4OutFilterRowStatus, u4SeqNum,
                          TRUE, BgpLock, BgpUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", gpBgpCurrCxtNode->u4ContextId,
                      u4Fsbgp4OutFilterCommVal,
                      i4SetValFsbgp4OutFilterRowStatus));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4CommOutgoingFilterStatus
 Input       :  The Indices
                Fsbgp4OutFilterCommVal

                The Object 
                testValFsbgp4CommOutgoingFilterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4CommOutgoingFilterStatus (UINT4 *pu4ErrorCode,
                                         UINT4 u4Fsbgp4OutFilterCommVal,
                                         INT4
                                         i4TestValFsbgp4CommOutgoingFilterStatus)
{
    INT4                i4RetSts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    i4RetSts = CommValidateCommunity (u4Fsbgp4OutFilterCommVal);
    if (i4RetSts == COMM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Community Value.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsbgp4CommOutgoingFilterStatus == (COMM_ADVERTISE +
                                                     BGP4_INCREMENT_BY_ONE)) ||
        (i4TestValFsbgp4CommOutgoingFilterStatus == (COMM_FILTER +
                                                     BGP4_INCREMENT_BY_ONE)))
    {
        return SNMP_SUCCESS;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - Invalid Comm Out Filter entry Status.\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4OutFilterRowStatus
 Input       :  The Indices
                Fsbgp4OutFilterCommVal

                The Object 
                testValFsbgp4OutFilterRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4OutFilterRowStatus (UINT4 *pu4ErrorCode,
                                   UINT4 u4Fsbgp4OutFilterCommVal,
                                   INT4 i4TestValFsbgp4OutFilterRowStatus)
{
    tCommFilterInfo    *pCommFilterInfo = NULL;
    INT4                i4RetSts;
    UINT4               u4BgpMaxOutFilterEntries = 0;
    UINT4               u4BgpCurOutFilterEntries = 0;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Local AS not configured..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    i4RetSts = CommValidateCommunity (u4Fsbgp4OutFilterCommVal);
    if (i4RetSts == COMM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Community Value.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_COMM_VALUE_ERR);
        return SNMP_FAILURE;
    }

    switch (i4TestValFsbgp4OutFilterRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            i4RetSts =
                CommGetOutputFilterEntry (u4Context, u4Fsbgp4OutFilterCommVal,
                                          &pCommFilterInfo);
            if (i4RetSts != COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Matching Comm Out Filter entry exist.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            u4BgpMaxOutFilterEntries =
                FsBGPSizingParams[MAX_BGP_COMM_OUT_FILTER_INFOS_SIZING_ID].
                u4PreAllocatedUnits;
            BGP4_RB_TREE_GET_COUNT (COMM_OUTPUT_FILTER_TBL,
                                    &u4BgpCurOutFilterEntries);
            if (u4BgpCurOutFilterEntries >= u4BgpMaxOutFilterEntries)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Max entries created.\n");
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_BGP4_EXCEED_MAX_ENTRIES);
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
        case NOT_IN_SERVICE:
            i4RetSts =
                CommGetOutputFilterEntry (u4Context, u4Fsbgp4OutFilterCommVal,
                                          &pCommFilterInfo);
            if (i4RetSts != COMM_SUCCESS)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Comm Out Filter "
                          "entry exist.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if ((pCommFilterInfo->u1RowStatus == ACTIVE) ||
                (pCommFilterInfo->u1RowStatus == NOT_IN_SERVICE) ||
                (pCommFilterInfo->u1RowStatus == NOT_READY))
            {
                return SNMP_SUCCESS;
            }
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        case DESTROY:
            CommGetOutputFilterEntry (u4Context, u4Fsbgp4OutFilterCommVal,
                                      &pCommFilterInfo);
            return SNMP_SUCCESS;
        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            break;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4CommOutFilterTable
 Input       :  The Indices
                Fsbgp4OutFilterCommVal
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4CommOutFilterTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
#endif /* _BGCOMLOW_C  */
