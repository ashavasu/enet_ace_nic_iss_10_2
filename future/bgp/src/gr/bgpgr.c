/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: bgpgr.c,v 1.22 2017/09/15 06:19:53 siva Exp $
 * *
 * * Description: BGP GR functionality routines
 * *********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2009 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : bgpgr.c                                        */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        :                                                */
/*    MODULE NAME           : BGP                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 22 July 2009                                   */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           :  This file contains functions related          */
/*                             to the core BGP features                      */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    22 July 2009           Initial Creation                        */
/*---------------------------------------------------------------------------*/

#ifndef BGPGR_C
#define BGPGR_C

#include "bgp4com.h"

/*****************************************************************************/
/*  Function Name   :   Bgp4GRPlannedShutdownProcess                         */
/*  Description     :   This module does the following -                     */
/*                1. Check the Ye/restart capability of this BGP node. If */
/*               it is GR-capable then proceed with the following  */
/*                         steps. Else Deinit BGP and kill the task.         */
/*                      2. Check whether planned restart is supported. If yes*/
/*                         then proceed with the following steps.            */
/*                      4. start restart timer with restart interval to      */
/*                         record remaining time left after start-up         */
/*                      5. Notify RTM about BGP Task going down and also     */
/*                         remaining time left for BGP to restart. RTM needs */
/*                         to mark all the  BGP routes for the supported     */
/*                         address families as stale                         */
/*                      6. Save all the GR related params in the bgpGr.txt   */
/*                         file                                              */
/*                      7. Deinit and kill BGP task.                         */
/*                                                                         */
/*  Input(s)        :   BGP4_SUCCESS on successful completion of planned     */
/*            restart                                              */
/*            BGP4_FAILURE on any failure.                         */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
PUBLIC VOID
Bgp4GRPlannedShutdownProcess (VOID)
{
    UINT4               u4CxtId = BGP4_DFLT_VRFID;
    UINT4               u4GrCxtExist = 0;
    INT4                i4RetStatus;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
              "\t", "GR Restart Timer Expiry Handling \r\n");

    i4RetStatus = BgpGetFirstActiveContextID (&u4CxtId);
    while (i4RetStatus == SNMP_SUCCESS)
    {
        /* Check for any context exists with GR capability, 
         *  if exist perform Gr operations */
        if (BGP4_RESTART_SUPPORT (u4CxtId) == BGP4_GR_SUPPORT_NONE)
        {
            i4RetStatus = BgpGetNextActiveContextID (u4CxtId, &u4CxtId);
            continue;
        }
        else if ((Bgp4GRCheckGRCapability (u4CxtId) == BGP4_FALSE))
        {
            i4RetStatus = BgpGetNextActiveContextID (u4CxtId, &u4CxtId);
            continue;
        }
        else
        {
            /* This is a consecutive restart. Hence, exit the restart
             * procedure and perform a shutdown */
            if (BGP4_RESTART_EXIT_REASON (u4CxtId) == BGP4_GR_EXIT_INPROGRESS)
            {
                BGP4_TIMER_STOP (BGP4_TIMER_LISTID, gBgpNode.pBgpRestartTmr);
                Bgp4GRExitRestart (u4CxtId);
            }
            gBgpCxtNode[u4CxtId]->u1RestartMode = BGP4_RESTARTING_MODE;
            /* Start the restart timer with restart timer interval */
            gBgpNode.pBgpRestartTmr->u4Data = BGP4_RESTART_TIMER;
            BGP4_TIMER_START (BGP4_TIMER_LISTID, gBgpNode.pBgpRestartTmr,
                              BGP4_RESTART_TIME_INTERVAL (u4CxtId));

            /* Notify RTM about BGP restart */
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                           "\t", "Notifying to RTM with restart interval %d "
                           "for planned shutdown process.\r\n",
                           gBgpCxtNode[u4CxtId]->u4RestartInterval);
            Bgp4GRNotifyRestartRTM (u4CxtId, BGP_ID,
                                    gBgpCxtNode[u4CxtId]->u4RestartInterval);

#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
#ifdef MPLS_WANTED
            /* Notify L2VPN about BGP restart */
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                      BGP4_MOD_NAME, "\t%s:%d Notify to MPLS-L2VPN about GR\n",
                      __func__, __LINE__);
            Bgp4VplsNotifyGRToL2VPN (u4CxtId);
#endif
#endif
#endif

            BGP4_RESTART_STATUS (u4CxtId) = BGP4_GR_STATUS_PLANNED;
            BGP4_GLB_RESTART_STATUS = BGP4_GR_STATUS_PLANNED;
            u4GrCxtExist = BGP4_GR_CXT_EXISTS;
        }
        Bgp4SnmpSendStatusChgTrap (u4CxtId, BGP4_RST_STATUS_CHANGE_TRAP);
        i4RetStatus = BgpGetNextActiveContextID (u4CxtId, &u4CxtId);
    }
    if (u4GrCxtExist == BGP4_GR_CXT_EXISTS)
    {
        /* Save BGP related configurations in bgpGr.txt */
        Bgp4GRStoreRestartConfig ();
    }
    if ((BGP4_TASK_INIT_STATUS != BGP4_FALSE) &&
        (Bgp4GRCheckGRCapability (BGP4_DFLT_VRFID) == BGP4_TRUE) &&
        (BGP4_RESTART_EXIT_REASON (BGP4_DFLT_VRFID) != BGP4_GR_EXIT_INPROGRESS))
    {
#ifdef CLI_WANTED
        IssCsrSaveCli ((UINT1 *) "BGP4");
#endif
    }
    /* DeInit the BGP Task */
    Bgp4GRKillProcess ();
    return;
}                                /* End of Bgp4GRPlannedShutdownProcess */

#ifdef BGP_DEBUG
/*****************************************************************************/
/*  Function Name   :   Bgp4GRUnPlannedShutdownProcess                       */
/*  Description     :   This function is used to shutdown BGP during         */
/*                      unplanned restart. This function is for testing      */
/*                      purpose only. This function will simply de-init BGP  */
/*                      and kill the BGP task.                               */
/*                         Deinit and kill BGP task.                         */
/*                                                                         */
/*  Input(s)        :   None                                                 */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
PUBLIC VOID
Bgp4GRUnPlannedShutdownProcess (VOID)
{
    UINT4               u4ContextId;
    INT4                i4RetStatus;

    BGP4_GLB_RESTART_MODE = BGP4_RESTARTING_MODE;
    i4RetStatus = BgpGetFirstActiveContextID (&u4ContextId);
    while (i4RetStatus == SNMP_SUCCESS)
    {
        /*If The context is GR capablale modifiy the restart mode */
        if (BGP4_GR_ADMIN_STATUS (u4ContextId) == BGP4_ENABLE_GR)
        {
            BGP4_RESTART_MODE (u4ContextId) = BGP4_RESTARTING_MODE;
        }
        i4RetStatus = BgpGetNextActiveContextID (u4ContextId, &u4ContextId);
    }
    /* Kill the BGP process */
    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
              "\tUnplanned restart has been performed \n");
    Bgp4GRKillProcess ();
    return;
}                                /* End of Bgp4GRUnPlannedShutdownProcess */
#endif /* BGP_DEBUG */

/*****************************************************************************/
/*  Function Name   :   Bgp4GRRestartProcess                                 */
/*  Description     :   This module does the following -                     */
/*                        1. Check the restart capability of this BGP node. If */
/*                           it is GR-capable then proceed with the following  */
/*                      2. Check if bgpgr.txt file is preserved before       */
/*                         restart.                                          */
/*                      3. If file present, then confirm that it is planned  */
/*                         restart by  reading the restart  mode and restart */
/*                         status reason from bgpGr.txt                      */
/*                      4. If it is a planned restart, Query RTM to obtain   */
/*                         the address families for which forwarding states  */
/*                         are preserved during restart                      */
/*                         i.Set the address families maintained by RTM      */
/*                         ii. Mark the restart mode of router as restarting */
/*                         iii. Start the selection deferral timer           */
/*                      5. If it is a unplanned restart                      */
/*                         i. Inform RTM of the unplanned outage with        */
/*                            remaining time for removal of stale BGP routes */
/*                            for each supported address family.             */
/*                         ii.RTM on reception of this notification, needs to*/
/*                            mark the BGP routes as stale for the address   */
/*                            families for which forwarding plane was        */
/*                            preserved across restart.                      */
/*                            Call Bgp4GRNotifyRestartRTM to notify RTM.     */
/*                         iii.Query RTM to obtain the address families for  */
/*                             which forwarding states are preserved during  */
/*                             restart.                                      */
/*                       6. Mark the restart mode of router as as            */
/*                             restarting                                    */
/*                       7. Start the selection deferral timer               */
/*                       8. Start the restart timer with restart interval    */
/*                       9. Sends the Restart Status change trap             */
/*                                                                           */
/*  Input(s)        :   None                                                 */
/*  Output(s)       :   BGP4_SUCCESS on successful completion of planned     */
/*                    restart                                              */
/*                    BGP4_FAILURE on any failure.                         */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
PUBLIC VOID
Bgp4GRRestartProcess (UINT4 u4CxtId)
{
    UINT4               u4CtxId = 0;    /* Context id */
    INT1                i1RetVal = BGP4_FAILURE;    /* Temp variable for return values */
    UINT1               u1FwdPath = 0;    /* Forwarding path flag for RTM querying */

    /* Mark the route selection flag as false */
    BGP4_ROUTE_SELECTION_FLAG (u4CxtId) = BGP4_FALSE;
    BGP4_GLB_ROUTE_SELECTION_FLAG = BGP4_FALSE;

#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
    BGP4_EOVPLS_FLAG (u4CxtId) = BGP4_FALSE;
#endif
#endif

    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                   "\tRestart process has been started.\n");
    i1RetVal = Bgp4GRReStoreRestartConfig (u4CxtId);
    /* bgpgr.txt file is found but a late restart */
    if ((i1RetVal == BGP4_SUCCESS) &&
        (BGP4_RESTART_STATUS (u4CxtId) == BGP4_GR_STATUS_NONE))
    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\tRestart has been performed. But BGP speaker has "
                       "taken more than the configured restart time"
                       "to come up\n");
        BGP4_ROUTE_SELECTION_FLAG (u4CxtId) = BGP4_TRUE;
        BGP4_RESTART_EXIT_REASON (u4CxtId) = BGP4_GR_EXIT_FAILURE;
        BGP4_GLB_ROUTE_SELECTION_FLAG = BGP4_TRUE;
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
        BGP4_EOVPLS_FLAG (u4CxtId) = BGP4_TRUE;
#endif
#endif
        BGP4_GLB_RESTART_EXIT_REASON = BGP4_GR_EXIT_FAILURE;
        Bgp4GRExitRestart (u4CxtId);
        return;
    }

    /* Configuration file is missing. Hence it is an unplanned restart */
    if (i1RetVal == BGP4_FAILURE)
    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "Unplanned Restart has been performed \n");
        BGP4_RESTART_STATUS (u4CxtId) = BGP4_GR_STATUS_UNPLANNED;
        BGP4_GLB_RESTART_STATUS = BGP4_GR_STATUS_UNPLANNED;
        BGP4_RESTART_REASON (u4CxtId) = BGP4_GR_REASON_UNKNOWN;
        BGP4_GLB_RESTART_REASON = BGP4_GR_REASON_UNKNOWN;
    }

    /* Restart support is not there for this speaker */
    if (((Bgp4GRCheckGRCapability (u4CxtId)) == BGP4_FALSE) ||
        ((BGP4_RESTART_SUPPORT (u4CxtId) == BGP4_GR_SUPPORT_NONE) &&
         ((BGP4_RESTART_STATUS (u4CxtId) == BGP4_GR_STATUS_UNPLANNED) ||
          (BGP4_RESTART_STATUS (u4CxtId) == BGP4_GR_STATUS_PLANNED))) ||
        ((BGP4_RESTART_SUPPORT (u4CxtId) == BGP4_GR_SUPPORT_PLANNED) &&
         (BGP4_RESTART_STATUS (u4CxtId) == BGP4_GR_STATUS_UNPLANNED)))
    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\tRequired restart support is not there\n");
        BGP4_ROUTE_SELECTION_FLAG (u4CxtId) = BGP4_TRUE;
        BGP4_RESTART_EXIT_REASON (u4CxtId) = BGP4_GR_EXIT_FAILURE;
        BGP4_GLB_RESTART_EXIT_REASON = BGP4_GR_EXIT_FAILURE;
        BGP4_RESTART_MODE (u4CxtId) = BGP4_RECEIVING_MODE;
        BGP4_RESTART_STATUS (u4CxtId) = BGP4_GR_STATUS_NONE;
        BGP4_GLB_RESTART_MODE = BGP4_RECEIVING_MODE;
        BGP4_GLB_ROUTE_SELECTION_FLAG = BGP4_TRUE;
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
        BGP4_EOVPLS_FLAG (u4CxtId) = BGP4_TRUE;
#endif
#endif
        BGP4_GLB_RESTART_STATUS = BGP4_GR_STATUS_NONE;
        return;
    }

    if ((BGP4_RESTART_SUPPORT (u4CxtId) == BGP4_GR_SUPPORT_BOTH) &&
        (BGP4_RESTART_STATUS (u4CxtId) == BGP4_GR_STATUS_UNPLANNED))
    {
        /* Unplanned restart. Notify this to RTM */
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "Notifying Unplanned Restart to RTM\n");
        Bgp4GRNotifyRestartRTM (u4CtxId, BGP_ID,
                                gBgpCxtNode[u4CxtId]->u4RestartInterval);
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
        /* 
         * In case of Unplanned shutdown, notify L2VPN about module restart. 
         * Because it is a unplanned restart, it will not go in GR*/
        Bgp4VplsNotifyRestartToL2VPN (u4CxtId);
#endif
#endif
        /* Start the restart timer since it is a unplanned restart */
        gBgpNode.pBgpRestartTmr->u4Data = BGP4_RESTART_TIMER;
        BGP4_TIMER_START (BGP4_TIMER_LISTID,
                          gBgpNode.pBgpRestartTmr,
                          BGP4_RESTART_TIME_INTERVAL (u4CxtId));
    }

    /* Query RTM to get the address families preserved  across restart */
    i1RetVal =
        (INT1) Bgp4GRQueryRTMForwarding (u4CxtId, BGP4_INET_AFI_IPV4,
                                         &u1FwdPath);
    if (i1RetVal == BGP4_SUCCESS)
    {
        BGP4_GR_AFI_SUPPORT (u4CxtId) |= BGP4_INET_AFI_IPV4;
    }
#ifdef BGP4_IPV6_WANTED
    i1RetVal =
        (INT1) Bgp4GRQueryRTMForwarding (u4CxtId, BGP4_INET_AFI_IPV6,
                                         &u1FwdPath);
    if (i1RetVal == BGP4_SUCCESS)
    {
        BGP4_GR_AFI_SUPPORT (u4CxtId) |= BGP4_INET_AFI_IPV6;
    }
#endif /* BGP4_IPV6_WANTED */
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
#ifdef MPLS_WANTED
    /* It means BGP module comes up after Graceful Restart.
     * Notify MPLS-L2VPN that BGP module comes up*/
    Bgp4VplsGRInProgress (u4CxtId);
#endif
    BGP4_GR_AFI_SUPPORT (u4CxtId) |= BGP4_INET_AFI_L2VPN;
#endif
#endif

    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                   "\tSelection Defferral timer started \n");

    gBgpNode.pBgpSelectionDeferTmr->u4Data = BGP4_SELECTION_DEFERRAL_TIMER;
    BGP4_TIMER_START (BGP4_TIMER_LISTID, gBgpNode.pBgpSelectionDeferTmr,
                      BGP4_SELECTION_DEFERAL_INTERVAL (u4CxtId));
    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                   "\tSpeaker is in restarting mode\n");
    BGP4_RESTART_MODE (u4CxtId) = BGP4_RESTARTING_MODE;
    BGP4_GLB_RESTART_MODE = BGP4_RESTARTING_MODE;

    gBgpCxtNode[u4CxtId]->u1AllowPeerInit = BGP4_FALSE;
    /* Generate trap message since the restart status of speaker has changed */
    Bgp4SnmpSendStatusChgTrap (u4CxtId, BGP4_RST_STATUS_CHANGE_TRAP);
    return;
}                                /* End of Bgp4GRRestartProcess */

/*****************************************************************************/
/*  Function Name   :    Bgp4GRProcessRestartCap                             */
/*                                                                           */
/*  Description     :   This function will process the restart capability in */
/*                      the  open message received from peer                 */
/*                      1. While receiving the open message from the peer it */
/*                         will check whether restart capability is supported*/
/*                      2. If it is supported it will check whether the      */
/*                         speaker is in restarting mode or  receiving mode  */
/*                      3. If the speaker is in restarting mode              */
/*                         i. Rcvd open message without restart capability   */
/*                            a. If it received the OPEN message from peer   */
/*                               without restart capability, then it will    */
/*                               make the peer's restart mode as none and    */
/*                               the session will be established             */
/*                         ii. Rcvd open message without restart bit set     */
/*                            a. If it received the OPEN message from peer   */
/*                               without restart bit set, then it will make  */
/*                               the peer'ss restart mode as receiving and   */
/*                               the session will be established             */
/*                         iii. Received open message with restart bit set   */
/*                            a. It will mark the peer.s restart mode as     */
/*                               restarting and the session will be          */
/*                               established.                                */
/*                      4. If the speaker is in receiving mode :             */
/*                         i. Received open message without restart bit set  */
/*                             a.It will check the peer.s restart mode, if   */
/*                               peer is already in restart mode, delete     */
/*                               stale routes  learned from the peer in local*/
/*                               RIB and also from forwarding table and make */
/*                               the peer.s restart mode as receiving        */
/*                             b.If peer is in receiving mode, then it will  */
/*                                do normal processing.                      */
/*                         ii. Received open message with restart bit set    */
/*                             a.It will check the forwarding flag of the    */
/*                               address families in   the open message.     */
/*                               It will delete the routes from the local    */
/*                               RIB and also from  thefrom the forwarding   */
/*                               table for which the restarting speaker's    */
/*                               forwarding table was not maintained.        */
/*                               It will set the peer's restart mode as      */
/*                               restarting.                                 */
/*                                                                           */
/*  Input(s)        :  pPeerEntry - peer information from whom the open      */
/*                     is received.                                          */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  BGP4_SUCCESS/ BGP4_FAILURE                */
/*****************************************************************************/
PUBLIC VOID
Bgp4GRProcessRestartCap (tBgp4PeerEntry * pPeerEntry)    /* Pointer to peer  
                                                         * whose restart 
                                                         * capability
                                                         * is to be prcsd */
{
    INT4                i4RestartCapFlag = BGP4_FALSE;    /* Flag to indicate 
                                                           GR cap of peer */
    tTMO_SLL           *pCapsList = NULL;    /* Pointer to 
                                               capability SLL */
    tSupCapsInfo       *pTmpSupCap = NULL;

    pCapsList = BGP4_PEER_RCVD_CAPS_LIST (pPeerEntry);

    if (Bgp4GRIsPeerGRCapable (pPeerEntry) == BGP4_SUCCESS)
    {
        if (pCapsList != NULL)
        {
            TMO_SLL_Scan (pCapsList, pTmpSupCap, tSupCapsInfo *)
            {
                if (pTmpSupCap->SupCapability.u1CapCode ==
                    CAP_CODE_GRACEFUL_RESTART)
                {
                    i4RestartCapFlag = BGP4_TRUE;

                    /* Fetch the restart interval of the peer */
                    PTR_FETCH2 (BGP4_PEER_RESTART_INTERVAL (pPeerEntry),
                                (pTmpSupCap->SupCapability.au1CapValue));
                    BGP4_PEER_RESTART_INTERVAL (pPeerEntry) &=
                        BGP4_RST_BIT_MASK;
                    /* The BGP speaker is in restarting mode */
                    if (Bgp4GRCheckRestartMode (BGP4_PEER_CXT_ID (pPeerEntry))
                        == BGP4_RESTARTING_MODE)
                    {
                        /* Received Restart capability with restart bit set, 
                         * then mark the
                         * peer as restarting */
                        if (((*(pTmpSupCap->SupCapability.au1CapValue)) &
                             BGP4_GR_BIT_MASK) == BGP4_GR_BIT_MASK)
                        {
                            pPeerEntry->peerStatus.u1RestartMode =
                                BGP4_RESTARTING_MODE;
                            /* Send trap to inform the mode 
                             * change of the peer */
                            BGP4_TRC_ARG1 (&
                                           (BGP4_PEER_REMOTE_ADDR_INFO
                                            (pPeerEntry)), BGP4_TRC_FLAG,
                                           BGP4_GR_TRC, BGP4_MOD_NAME,
                                           "\tPEER %s : Peer is moving to restarting mode .\n",
                                           Bgp4PrintIpAddr
                                           (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerEntry))));
                            Bgp4SnmpSendPeerStatusChgTrap (pPeerEntry,
                                                           BGP4_PEER_RST_STATUS_CHANGE_TRAP);
                        }
                        else
                        {
                            BGP4_TRC_ARG1 (&
                                           (BGP4_PEER_REMOTE_ADDR_INFO
                                            (pPeerEntry)), BGP4_TRC_FLAG,
                                           BGP4_GR_TRC, BGP4_MOD_NAME,
                                           "\tPEER %s : Peer is moving to receiving mode .\n",
                                           Bgp4PrintIpAddr
                                           (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerEntry))));
                            /* Received restart capability with restart 
                             * bit not set, then mark 
                             * the peer's mode as receiving */
                            pPeerEntry->peerStatus.u1RestartMode =
                                BGP4_RECEIVING_MODE;

                            /* Send trap to inform the mode 
                             * change of the peer */
                            Bgp4SnmpSendPeerStatusChgTrap (pPeerEntry,
                                                           BGP4_PEER_RST_STATUS_CHANGE_TRAP);
                        }
                    }            /* When the speaker is in restarting mode */
                    else if (Bgp4GRCheckRestartMode
                             (BGP4_PEER_CXT_ID (pPeerEntry)) ==
                             BGP4_RECEIVING_MODE)
                    {
                        /* If the speaker is in receiving mode 
                         * and the peer has come up
                         * with restart bit set, If the address family 
                         * forwarding bit is 
                         * not set then delete the routes belonging 
                         * to that particular peer                  
                         * * from RTM and RIB*/
                        if (((*(pTmpSupCap->SupCapability.au1CapValue)) &
                             BGP4_GR_BIT_MASK) == BGP4_GR_BIT_MASK)
                        {
                            if (pPeerEntry->peerStatus.u1RestartMode ==
                                BGP4_RESTARTING_MODE)
                            {
                                /* The peer is consecutively restarting, 
                                 * hence deleting
                                 * the stale routes */
                                BGP4_TRC_ARG1 (&
                                               (BGP4_PEER_REMOTE_ADDR_INFO
                                                (pPeerEntry)), BGP4_TRC_FLAG,
                                               BGP4_GR_TRC, BGP4_MOD_NAME,
                                               "\tPEER %s : This is a consecutive restart. All "
                                               "the stale routes are deleted \r\n",
                                               Bgp4PrintIpAddr
                                               (BGP4_PEER_REMOTE_ADDR
                                                (pPeerEntry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerEntry))));
                                Bgp4GRDeleteAllStaleRoutes (pPeerEntry);
                                return;
                            }
                            /* Mark the peer's mode as restarting */
                            pPeerEntry->peerStatus.u1RestartMode =
                                BGP4_RESTARTING_MODE;

                            /* If the forwarding bit is not set and 
                             * routes are preserved for
                             * that address family, then delete the routes */
                            if (((pTmpSupCap->SupCapability.
                                  au1CapValue[BGP4_GR_AFI_IPV4] &
                                  BGP4_GR_BIT_MASK) != BGP4_GR_BIT_MASK)
                                &&
                                ((BGP4_GR_AFI_SUPPORT
                                  (BGP4_PEER_CXT_ID (pPeerEntry)) &
                                  BGP4_INET_AFI_IPV4) == BGP4_INET_AFI_IPV4) &&
                                ((BGP4_PEER_SUP_ASAFI_MASK (pPeerEntry) &
                                  CAP_NEG_IPV4_UNI_MASK) == BGP4_INET_AFI_IPV4))
                            {
                                BGP4_TRC_ARG1 (&
                                               (BGP4_PEER_REMOTE_ADDR_INFO
                                                (pPeerEntry)), BGP4_TRC_FLAG,
                                               BGP4_GR_TRC, BGP4_MOD_NAME,
                                               "\tPEER %s : Forwarding bit "
                                               "not set for "
                                               "the IPv4 family.All the "
                                               "stale routes are "
                                               "deleted for IPv4 family \r\n",
                                               Bgp4PrintIpAddr
                                               (BGP4_PEER_REMOTE_ADDR
                                                (pPeerEntry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerEntry))));
                                Bgp4GRDeleteAfiSafiStaleRoutes (pPeerEntry,
                                                                BGP4_INET_AFI_IPV4);
                            }
#ifdef BGP4_IPV6_WANTED
                            if (((pTmpSupCap->SupCapability.
                                  au1CapValue[BGP4_GR_AFI_IPV6] &
                                  BGP4_GR_BIT_MASK) != BGP4_GR_BIT_MASK)
                                &&
                                ((BGP4_GR_AFI_SUPPORT
                                  (BGP4_PEER_CXT_ID (pPeerEntry)) &
                                  BGP4_INET_AFI_IPV6) == BGP4_INET_AFI_IPV6) &&
                                ((BGP4_PEER_SUP_ASAFI_MASK (pPeerEntry) &
                                  CAP_NEG_IPV6_UNI_MASK) == BGP4_INET_AFI_IPV6))
                            {
                                BGP4_TRC_ARG1 (&
                                               (BGP4_PEER_REMOTE_ADDR_INFO
                                                (pPeerEntry)), BGP4_TRC_FLAG,
                                               BGP4_GR_TRC, BGP4_MOD_NAME,
                                               "\t PEER %s : Forwarding bit "
                                               "not set for" "the IPv6 family."
                                               "All the stale routes are "
                                               "deleted for IPv6 "
                                               "family \r\n",
                                               Bgp4PrintIpAddr
                                               (BGP4_PEER_REMOTE_ADDR
                                                (pPeerEntry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerEntry))));
                                Bgp4GRDeleteAfiSafiStaleRoutes (pPeerEntry,
                                                                BGP4_INET_AFI_IPV6);
                            }
#endif /* BGP4_IPV6_WANTED */
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
                            if (((pTmpSupCap->SupCapability.
                                  au1CapValue[BGP4_GR_AFI_L2VPN] &
                                  BGP4_GR_BIT_MASK) != BGP4_GR_BIT_MASK)
                                &&
                                ((BGP4_GR_AFI_SUPPORT
                                  (BGP4_PEER_CXT_ID (pPeerEntry)) &
                                  BGP4_INET_AFI_L2VPN) == BGP4_INET_AFI_L2VPN))
                            {
                                BGP4_TRC_ARG1 (&
                                               (BGP4_PEER_REMOTE_ADDR_INFO
                                                (pPeerEntry)), BGP4_TRC_FLAG,
                                               BGP4_GR_TRC, BGP4_MOD_NAME,
                                               "\t PEER %s : Forwarding bit "
                                               "not set for" "the vpls family."
                                               "All the stale routes are "
                                               "deleted for vpls "
                                               "family \r\n",
                                               Bgp4PrintIpAddr
                                               (BGP4_PEER_REMOTE_ADDR
                                                (pPeerEntry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerEntry))));
                                /*Need to remove stale route of L2VPN-VPLS */
                                /*Bgp4GRDeleteAfiSafiStaleRoutes (pPeerEntry,
                                   BGP4_INET_AFI_L2VPN); */
                            }
#endif
#endif /* VPLSADS_WANTED */

                            BGP4_TRC_ARG1 (&
                                           (BGP4_PEER_REMOTE_ADDR_INFO
                                            (pPeerEntry)), BGP4_TRC_FLAG,
                                           BGP4_GR_TRC, BGP4_MOD_NAME,
                                           "\tPEER %s : Moving to "
                                           "restarting mode .\n",
                                           Bgp4PrintIpAddr
                                           (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerEntry))));
                            /* Send trap to inform the mode 
                             * change of the peer */
                            Bgp4SnmpSendPeerStatusChgTrap (pPeerEntry,
                                                           BGP4_PEER_RST_STATUS_CHANGE_TRAP);
                        }
                        else    /*  without restart bit set */
                        {
                            if (pPeerEntry->peerStatus.u1RestartMode ==
                                BGP4_RESTARTING_MODE)
                            {
                                /* Received restart capability with restart 
                                 * bit not set, then mark 
                                 * the peer's mode as receiving */
                                Bgp4TmrhStopTimer (BGP4_STALE_TIMER,
                                                   (VOID *) pPeerEntry);
                                Bgp4TmrhStopTimer (BGP4_PEER_RESTART_TIMER,
                                                   (VOID *) pPeerEntry);
                                pPeerEntry->peerStatus.u1RestartMode =
                                    BGP4_RECEIVING_MODE;
                                /* The peer is consecutively restarting, 
                                 * hence deleting 
                                 * the stale routes */
                                Bgp4SnmpSendPeerStatusChgTrap (pPeerEntry,
                                                               BGP4_PEER_RST_STATUS_CHANGE_TRAP);
                                BGP4_TRC_ARG1 (&
                                               (BGP4_PEER_REMOTE_ADDR_INFO
                                                (pPeerEntry)), BGP4_TRC_FLAG,
                                               BGP4_GR_TRC, BGP4_MOD_NAME,
                                               "\tPEER %s : This is a "
                                               "consecutive restart. "
                                               "All the stale routes"
                                               "are deleted \r\n",
                                               Bgp4PrintIpAddr
                                               (BGP4_PEER_REMOTE_ADDR
                                                (pPeerEntry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerEntry))));

                                Bgp4GRPutPeerInDeInitList (pPeerEntry);
                            }
                            else if (pPeerEntry->peerStatus.u1RestartMode ==
                                     BGP4_RECEIVING_MODE)
                            {
                                pPeerEntry->peerStatus.u1RestartMode =
                                    BGP4_RECEIVING_MODE;
                                Bgp4TmrhStopTimer (BGP4_STALE_TIMER,
                                                   (VOID *) pPeerEntry);
                                Bgp4TmrhStopTimer (BGP4_PEER_RESTART_TIMER,
                                                   (VOID *) pPeerEntry);
                                Bgp4SnmpSendPeerStatusChgTrap (pPeerEntry,
                                                               BGP4_PEER_RST_STATUS_CHANGE_TRAP);
                            }
                            else
                            {
                                /* Received restart capability with restart 
                                 * bit not set, then mark 
                                 * the peer's mode as receiving */
                                pPeerEntry->peerStatus.u1RestartMode =
                                    BGP4_RECEIVING_MODE;
                                Bgp4SnmpSendPeerStatusChgTrap (pPeerEntry,
                                                               BGP4_PEER_RST_STATUS_CHANGE_TRAP);
                            }
                        }
                    }            /* If the speaker is in receiving mode */
                }                /* For the Graceful restart capability */
            }                    /* Scanning the supported capabilites received for this peer */
        }                        /* If the BGP4_PEER_RCVD_CAPS_LIST list is not null */
    }                            /* If the peer is GR capable */
    if (i4RestartCapFlag != BGP4_TRUE)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                       BGP4_TRC_FLAG, BGP4_GR_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s : Not a restart capable peer  \r\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerEntry))));

        pPeerEntry->peerStatus.u1RestartMode = BGP4_RESTART_MODE_NONE;
    }
    return;
}                                /* End of Bgp4GRProcessRestartCap */

/*****************************************************************************/
/*                                                                           */
/* Function     : Bgp4GRProcessIdleState                                     */
/*                                                                           */
/* Description  : This routine is used to process peer idle state due to TCP */
/*                close event when GR is enabled                             */
/*                . This function will do the following                      */
/*                1.    it will check the speaker.s restart mode, if it is   */
/*                    receiving it will do the following steps 2 to 7        */
/*                2.    It will not clear the routes learned from the peer if*/
/*                    the peer is GR capable peer                            */
/*                3.    it will mark the routes BGP4_PEER_ROUTE_LIST as stale*/
/*                    routes.                                                */
/*                4.    it will start the stale timer                        */
/*                5.    it will start the hold timer with restart interval   */
/*                6.    if the peer is not restart capable peer it will call */
/*                7   Bgp4RibhEndConnection to clear all the routes learned  */
/*                    from peer and release the resources.                   */
/*                8.    if the speaker's restart mode is restarting it willdo*/
/*                    the following steps                                    */
/*                9.    it will call Bgp4RibhEndConnection to clear all the  */
/*                    routes learned from peer and release the resources.    */
/*                                                                           */
/* Input        : pPeerEntry                                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Bgp4GRProcessIdleState (tBgp4PeerEntry * pPeerentry)    /* Pointer to peer 
                                                         * whose idle state 
                                                         * is to be prcsd */
{
    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                   BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                   "\tPEER %s : TCP connection lost. Marking routes as stale\r\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry))));
    Bgp4GRMarkPeerRoutesAsStale (pPeerentry);

    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                   BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                   "Stale Timer started for the Peer : %s\r\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry))));
    if (Bgp4GRCheckRestartMode (BGP4_PEER_CXT_ID (pPeerentry)) ==
        BGP4_RECEIVING_MODE)
    {
        /* Start the stale timer with stale path interval */
        Bgp4TmrhStopTimer (BGP4_STALE_TIMER, (VOID *) pPeerentry);
        Bgp4TmrhStartTimer (BGP4_STALE_TIMER, (VOID *) pPeerentry,
                            BGP4_STALE_PATH_INTERVAL (BGP4_PEER_CXT_ID
                                                      (pPeerentry)));
    }
    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                   BGP4_TRC_FLAG, BGP4_GR_TRC,
                   BGP4_MOD_NAME,
                   "Restart Timer started for the Peer : %s\r\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry))));

    /* Start the hold timer with restart time  interval */
    Bgp4TmrhStopTimer (BGP4_PEER_RESTART_TIMER, (VOID *) pPeerentry);
    if (Bgp4GRIsPeerGRCapable (pPeerentry) == BGP4_SUCCESS)
    {
        Bgp4TmrhStartTimer (BGP4_PEER_RESTART_TIMER, (VOID *) pPeerentry,
                            BGP4_PEER_RESTART_INTERVAL (pPeerentry));
    }
    BGP4_SET_PEER_CURRENT_STATE (pPeerentry, BGP4_PEER_RST_CLOSE_IDENTIFIED);
    return;
}                                /* End of Bgp4GRProcessIdleState */

/****************************************************************************/
/*  Function Name   :   Bgp4GRProcessEndOfRIBRcvd                           */
/*                                                                          */
/*  Description     : This function will be called from Bgp4MhProcessUpdate */
/*                    when it gets End_Of_RIB marker  and also GR is enabled*/
/*                    in the speaker. This function will do the following   */
/*                    1.    It will call Bgp4GRMarkEndOfRIBMarker  to mark  */
/*                         End_Of_RIB marker is received.                   */
/*                    2.    If the speaker.s restart mode is receiving itwil*/
/*                        do the following stpes                            */
/*                    3.    It will check the peer.s restart mode if peer.s */
/*                        restart mode is restarting it will do the followin*/
/*                        steps                                             */
/*                    4.     it mark the peer.s restart mode as receiving   */
/*                    5.    it will delete the stale routes present in      */
/*                        BGP4_PEER_ROUTE_LIST                              */
/*                    6.    it will stop the stale timer                    */
/*                    7.    it will restart the hold timer with hold interva*/
/*                    8   if peer.s restart mode is receiving it will return*/
/*                        from this function                                */
/*  Input(s)        :  pPeerEntry - peer information                        */
/*  Output(s)       :   None                                                */
/*                  :                                                       */
/*  <OPTIONAL Fields>           :  None                                     */
/*  Global Variables Referred   :  None                                     */
/*  Global variables Modified   :  None                                     */
/*  Exceptions or Operating System Error Handling :                         */
/*  Use of Recursion            :  None                                     */
/*  Returns                     :  NONE                                     */
/****************************************************************************/
PUBLIC VOID
Bgp4GRProcessEndOfRIBRcvd (tBgp4PeerEntry * pPeerentry)    /* Peer Info  
                                                         * EOR marker 
                                                         * to be 
                                                         * procesed*/
{

    if (Bgp4GRCheckRestartMode (BGP4_PEER_CXT_ID (pPeerentry)) ==
        BGP4_RECEIVING_MODE)
    {
        /* The speaker is in receiving mode and the peer 
         * is in restarting mode */
        if (pPeerentry->peerStatus.u1RestartMode == BGP4_RESTARTING_MODE)
        {
            /* Since End of RIB Marker is received, 
             * the peer is moved to receiving mode
             * and trap is sent to inform the peer state change */
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_GR_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : End of RIB marker received."
                           "Moving to receiving mode \r\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));

            pPeerentry->peerStatus.u1RestartMode = BGP4_RECEIVING_MODE;
            Bgp4SnmpSendPeerStatusChgTrap (pPeerentry,
                                           BGP4_PEER_RST_STATUS_CHANGE_TRAP);

            BGP4_SET_PEER_CURRENT_STATE (pPeerentry,
                                         BGP4_PEER_STALE_DEL_INPROGRESS);
            /* Stop the stale timer and restart the hold timer 
             * with hold timer interval */
            Bgp4TmrhStopTimer (BGP4_STALE_TIMER, (VOID *) pPeerentry);
            Bgp4TmrhStopTimer (BGP4_PEER_RESTART_TIMER, (VOID *) pPeerentry);
        }                        /* The peer is in restart mode */
        else if (pPeerentry->peerStatus.u1RestartMode == BGP4_RECEIVING_MODE)
        {
            return;
        }                        /* The peer is in receiving mode */
    }                            /*End - speaker is in receiving mode */
    else if ((Bgp4GRCheckRestartMode (BGP4_PEER_CXT_ID (pPeerentry)) ==
              BGP4_RESTARTING_MODE)
             && (BGP4_RESTART_REASON (BGP4_PEER_CXT_ID (pPeerentry)) ==
                 BGP4_GR_REASON_UPGRADE))
    {
        /* In case of context is in restarting mode and the restart reason
         * is software upgrade due to switchover/failover, delete the Stale
         * routes present for this peer as EOR marker is received now.
         * Also stop the stale route expiry timer */
        Bgp4TmrhStopTimer (BGP4_PEER_RESTART_TIMER, (VOID *) pPeerentry);
    }

    return;
}                                /* End of Bgp4GRProcessEndOfRIBRcvd */

/*****************************************************************************/
/*  Function Name   :   Bgp4SendEORForMPNLRI                                 */
/*  Description     :   This function will do the following                  */
/*                      1.It will check the GR capability of the speaker,    */
/*                        if it is enabled it will do the following otherwise*/
/*                        it will return                                     */
/*                      2.It will send the END_OF_RIB marker to specified    */
/*                        peer.                                              */
/*                      3.It will Mark the peerStatus.u1EndOfRIBMarkerSent   */
/*                        as true                                            */
/*                      4. It will check whether end of rib marker is        */
/*                         received from all the peers                       */
/*                         If it is received and the speaker is in restarting*/
/*                         mode, then call Bgp4GRExitRestart to exit the     */
/*                         restart procedure successfully                    */
/*  Input(s)        :   pPeerEntry                                           */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
INT4
Bgp4SendEORForMPNLRI (tBgp4PeerEntry * pPeerentry, UINT4 u4afi, UINT4 u4safi)
{
    INT4                i4RetVal = BGP4_FAILURE;
    INT4                i4BytesFilled = BGP_ZERO;
    UINT1              *pu1UpdMsg = NULL;

    /* Allocate memory for the message */
    pu1UpdMsg = MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);
    if (pu1UpdMsg == NULL)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\t %s\r\n", "memory unavaible for "
                       "End of Rib marker \n");
        gu4BgpDebugCnt[MAX_BGP_BUF_NODES_SIZING_ID]++;
        return BGP4_FAILURE;
    }

    MEMSET (pu1UpdMsg, 0, BGP4_UPDATE_MSG_MIN_LEN);

    /* Form the header for update messgae */
    MEMSET (pu1UpdMsg, BGP4_MARKER_BYTE_VAL, BGP4_MARKER_LEN);
    *(UINT1 *) (pu1UpdMsg + BGP4_MARKER_LEN) = 0;
    *(UINT1 *) (pu1UpdMsg + BGP4_MARKER_LEN + sizeof (UINT1)) =
        BGP4_UPDATE_MSG_MIN_LEN;
    *(UINT1 *) (pu1UpdMsg + BGP4_MARKER_LEN + sizeof (UINT2)) = BGP4_UPDATE_MSG;

    /* Fill in the Unfeasible routes length */
    PTR_ASSIGN_2 ((pu1UpdMsg + BGP4_MSG_COMMON_HDR_LEN), 0);

    /* Fill in the Path Attributes length */
    PTR_ASSIGN_2 ((pu1UpdMsg + BGP4_MSG_COMMON_HDR_LEN + sizeof (UINT2)), 0);
    /* Allocate memory for the message */
    *(UINT1 *) (pu1UpdMsg + BGP4_MARKER_LEN + sizeof (UINT1)) =
        BGP4_MSG_COMMON_HDR_LEN +
        BGP4_ATTR_LEN_FIELD_SIZE +
        BGP4_WITHDRAWN_LEN_FIELD_SIZE +
        BGP4_ATYPE_FLAGS_LEN +
        BGP4_ATYPE_CODE_LEN +
        BGP4_NORMAL_ATTR_LEN_SIZE +
        BGP4_MPE_ADDRESS_FAMILY_LEN + BGP4_MPE_SUB_ADDRESS_FAMILY_LEN;

    /* For IPV6 address family frame EOR marker with 
     * MPE_UNREACH_NLRI attribute alone*/
    i4BytesFilled = (INT4) Bgp4AhAppendMpeAttrBuffToUpdMsg ((pu1UpdMsg +
                                                             BGP4_MSG_COMMON_HDR_LEN
                                                             +
                                                             BGP4_ATTR_LEN_FIELD_SIZE
                                                             +
                                                             BGP4_WITHDRAWN_LEN_FIELD_SIZE),
                                                            NULL, 0,
                                                            BGP4_ATTR_MP_UNREACH_NLRI,
                                                            u4afi, u4safi);

    PTR_ASSIGN2 ((pu1UpdMsg + BGP4_MSG_COMMON_HDR_LEN +
                  BGP4_WITHDRAWN_LEN_FIELD_SIZE), i4BytesFilled)
        if ((BGP4_TRC_FLAG & BGP4_DUMP_HEX_TRC) == BGP4_DUMP_HEX_TRC)
    {
        Bgp4DumpBuf (pu1UpdMsg, BGP4_EOR_MARKER_MAX_LEN, BGP4_OUTGOING);
    }

    i4RetVal =
        Bgp4PeerAddMsgToTxQ (pPeerentry, pu1UpdMsg, BGP4_EOR_MARKER_MAX_LEN, 0);

/* If Bgp4PeerAddMsgToTxQ fails due to memory allocation failure
 * for Tx node, release pu1UpdMsg and return */

    if (i4RetVal != BGP4_SUCCESS)
    {
        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, (UINT1 *) pu1UpdMsg);
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Failed to add EOR Update message to peer Tx list\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        return BGP4_FAILURE;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   :   Bgp4SendEORForNLRI                                 */
/*  Description     :   This function will do the following                  */
/*                      1.It will check the GR capability of the speaker,    */
/*                        if it is enabled it will do the following otherwise*/
/*                        it will return                                     */
/*                      2.It will send the END_OF_RIB marker to specified    */
/*                        peer.                                              */
/*                      3.It will Mark the peerStatus.u1EndOfRIBMarkerSent   */
/*                        as true                                            */
/*                      4. It will check whether end of rib marker is        */
/*                         received from all the peers                       */
/*                         If it is received and the speaker is in restarting*/
/*                         mode, then call Bgp4GRExitRestart to exit the     */
/*                         restart procedure successfully                    */
/*  Input(s)        :   pPeerEntry                                           */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
INT4
Bgp4SendEORForNLRI (tBgp4PeerEntry * pPeerentry)
{
    INT4                i4RetVal = BGP4_FAILURE;
    UINT1              *pu1UpdMsg = NULL;

    pu1UpdMsg = MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);

    if (pu1UpdMsg == NULL)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Memory unavaible for End of Rib marker \n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        return BGP4_FAILURE;
    }

    MEMSET (pu1UpdMsg, 0, BGP4_EOR_MARKER_MAX_LEN);

    /* Form the header for update messgae */
    MEMSET (pu1UpdMsg, BGP4_MARKER_BYTE_VAL, BGP4_MARKER_LEN);
    *(UINT1 *) (pu1UpdMsg + BGP4_MARKER_LEN) = 0;

    *(UINT1 *) (pu1UpdMsg + BGP4_MARKER_LEN + sizeof (UINT1)) =
        BGP4_UPDATE_MSG_MIN_LEN;
    *(UINT1 *) (pu1UpdMsg + BGP4_MARKER_LEN + sizeof (UINT2)) = BGP4_UPDATE_MSG;

    /* Fill in the Unfeasible routes length */
    PTR_ASSIGN_2 ((pu1UpdMsg + BGP4_MSG_COMMON_HDR_LEN), 0);

    /* Fill in the Path Attributes length */
    PTR_ASSIGN_2 ((pu1UpdMsg + BGP4_MSG_COMMON_HDR_LEN + sizeof (UINT2)), 0);

    if ((BGP4_TRC_FLAG & BGP4_DUMP_HEX_TRC) == BGP4_DUMP_HEX_TRC)
    {
        Bgp4DumpBuf (pu1UpdMsg, BGP4_EOR_MARKER_MAX_LEN, BGP4_OUTGOING);
    }

    i4RetVal =
        Bgp4PeerAddMsgToTxQ (pPeerentry, pu1UpdMsg, BGP4_UPDATE_MSG_MIN_LEN, 0);

    /* If Bgp4PeerAddMsgToTxQ fails due to memory allocation failure
     * for Tx node, release pu1UpdMsg and return */

    if (i4RetVal != BGP4_SUCCESS)
    {
        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, (UINT1 *) pu1UpdMsg);
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Failed to add EOR Update message to peer Tx list\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        return BGP4_FAILURE;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   :   Bgp4EORForNegotiatedCapabilities                     */
/*  Description     :   This function will do the following                  */
/*                      1.It will check the GR capability of the speaker,    */
/*                        if it is enabled it will do the following otherwise*/
/*                        it will return                                     */
/*                      2.It will send the END_OF_RIB marker to specified    */
/*                        peer.                                              */
/*                      3.It will Mark the peerStatus.u1EndOfRIBMarkerSent   */
/*                        as true                                            */
/*                      4. It will check whether end of rib marker is        */
/*                         received from all the peers                       */
/*                         If it is received and the speaker is in restarting*/
/*                         mode, then call Bgp4GRExitRestart to exit the     */
/*                         restart procedure successfully                    */
/*  Input(s)        :   pPeerEntry                                           */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/

INT4
Bgp4EORForNegotiatedCapabilities (tBgp4PeerEntry * pPeerInfo)
{

    INT1                i1RetVal = BGP4_FAILURE;

    if (pPeerInfo == NULL)
    {
        return BGP4_FALSE;
    }

#ifdef BGP4_IPV6_WANTED
    if ((pPeerInfo->peerConfig).RemoteAddrInfo.u2Afi != BGP4_INET_AFI_IPV6 &&
        (BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) & CAP_NEG_IPV6_UNI_MASK) ==
        CAP_NEG_IPV6_UNI_MASK)
    {
        i1RetVal =
            Bgp4SendEORForMPNLRI (pPeerInfo, BGP4_INET_AFI_IPV6,
                                  BGP4_INET_SAFI_UNICAST);
        if (i1RetVal == BGP4_FAILURE)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Failed to add send EOR for ipv6 capability\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            return BGP4_FAILURE;
        }
    }
#endif

#ifdef VPLSADS_WANTED
    if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) & CAP_NEG_L2VPN_VPLS_MASK) ==
        CAP_NEG_L2VPN_VPLS_MASK)
    {
        i1RetVal =
            (INT1) Bgp4SendEORForMPNLRI (pPeerInfo, BGP4_INET_AFI_L2VPN,
                                         BGP4_INET_SAFI_VPLS);
        if (i1RetVal == BGP4_FAILURE)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                           "%s\r\n", "Bgp4GREORForSupportedCapabilities:"
                           "Failed to add send EOR for VPLS capability");
            return BGP4_FAILURE;
        }
    }
#endif
#ifdef L3VPN
    if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) & CAP_MP_VPN4_UNICAST) ==
        CAP_MP_VPN4_UNICAST)
    {

        i1RetVal =
            (INT1) Bgp4SendEORForMPNLRI (pPeerInfo, BGP4_INET_AFI_IPV4,
                                         BGP4_INET_SAFI_VPNV4_UNICAST);
        if (i1RetVal == BGP4_FAILURE)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Failed to send EOR for L3VPN capability\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            return BGP4_FAILURE;
        }
    }
#endif
    if ((pPeerInfo->peerConfig).RemoteAddrInfo.u2Afi != BGP4_INET_AFI_IPV4
        && ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) & CAP_NEG_IPV4_UNI_MASK) ==
            CAP_NEG_IPV4_UNI_MASK))
    {
        i1RetVal = (INT1) Bgp4SendEORForNLRI (pPeerInfo);
        if (i1RetVal == BGP4_FAILURE)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Failed to send EOR for ipv4 capability\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            return BGP4_FAILURE;
        }
    }

    return BGP4_SUCCESS;

}

/*****************************************************************************/
/*  Function Name   :   Bgp4GRSendEndOfRIBMarker                             */
/*  Description     :   This function will do the following                  */
/*                      1.It will check the GR capability of the speaker,    */
/*                        if it is enabled it will do the following otherwise*/
/*                        it will return                                     */
/*                      2.It will send the END_OF_RIB marker to specified    */
/*                        peer.                                              */
/*                      3.It will Mark the peerStatus.u1EndOfRIBMarkerSent   */
/*                        as true                                            */
/*                      4. It will check whether end of rib marker is        */
/*                         received from all the peers                       */
/*                         If it is received and the speaker is in restarting*/
/*                         mode, then call Bgp4GRExitRestart to exit the     */
/*                         restart procedure successfully                    */
/*  Input(s)        :   pPeerEntry                                           */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
PUBLIC VOID
Bgp4GRSendEndOfRIBMarker (tBgp4PeerEntry * pPeerentry)    /* Peerentry to  
                                                         * whom the 
                                                         * End of RIB marker 
                                                         * need to sent */
{
    UINT2               u2TotSize = BGP4_UPDATE_MSG_MIN_LEN;
    UINT1              *pu1UpdMsg = NULL;
#ifdef BGP4_IPV6_WANTED
    INT4                i4BytesFilled = 0;
#endif
    INT4                i4RetVal = BGP4_FAILURE;

    /* Allocate memory for the message */
    pu1UpdMsg = MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);
    if (pu1UpdMsg == NULL)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Memory unavaible for End of Rib marker \n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        return;
    }

    MEMSET (pu1UpdMsg, 0, BGP4_EOR_MARKER_MAX_LEN);
    /* Form the header for update messgae */
    MEMSET (pu1UpdMsg, BGP4_MARKER_BYTE_VAL, BGP4_MARKER_LEN);
    *(UINT1 *) (pu1UpdMsg + BGP4_MARKER_LEN) = 0;
    *(UINT1 *) (pu1UpdMsg + BGP4_MARKER_LEN + sizeof (UINT1)) =
        BGP4_UPDATE_MSG_MIN_LEN;
    *(UINT1 *) (pu1UpdMsg + BGP4_MARKER_LEN + sizeof (UINT2)) = BGP4_UPDATE_MSG;

    /* Fill in the Unfeasible routes length */
    PTR_ASSIGN_2 ((pu1UpdMsg + BGP4_MSG_COMMON_HDR_LEN), 0);

    /* Fill in the Path Attributes length */
    PTR_ASSIGN_2 ((pu1UpdMsg + BGP4_MSG_COMMON_HDR_LEN + sizeof (UINT2)), 0);

#ifdef BGP4_IPV6_WANTED
    if ((pPeerentry->peerConfig).RemoteAddrInfo.u2Afi == BGP4_INET_AFI_IPV6)
    {
        *(UINT1 *) (pu1UpdMsg + BGP4_MARKER_LEN + sizeof (UINT1)) =
            BGP4_MSG_COMMON_HDR_LEN +
            BGP4_ATTR_LEN_FIELD_SIZE +
            BGP4_WITHDRAWN_LEN_FIELD_SIZE +
            BGP4_ATYPE_FLAGS_LEN +
            BGP4_ATYPE_CODE_LEN +
            BGP4_NORMAL_ATTR_LEN_SIZE +
            BGP4_MPE_ADDRESS_FAMILY_LEN + BGP4_MPE_SUB_ADDRESS_FAMILY_LEN;

        /* For IPV6 address family frame EOR marker with 
         * MPE_UNREACH_NLRI attribute alone*/
        i4BytesFilled = Bgp4AhAppendMpeAttrBuffToUpdMsg ((pu1UpdMsg +
                                                          BGP4_MSG_COMMON_HDR_LEN
                                                          +
                                                          BGP4_ATTR_LEN_FIELD_SIZE
                                                          +
                                                          BGP4_WITHDRAWN_LEN_FIELD_SIZE),
                                                         NULL, 0,
                                                         BGP4_ATTR_MP_UNREACH_NLRI,
                                                         BGP4_INET_AFI_IPV6,
                                                         BGP4_INET_SAFI_UNICAST);

        u2TotSize = BGP4_EOR_MARKER_MAX_LEN;

        PTR_ASSIGN2 ((pu1UpdMsg + BGP4_MSG_COMMON_HDR_LEN +
                      BGP4_WITHDRAWN_LEN_FIELD_SIZE), i4BytesFilled);

    }
#endif /* BGP4_IPV6_WANTED */

    if ((BGP4_TRC_FLAG & BGP4_DUMP_HEX_TRC) == BGP4_DUMP_HEX_TRC)
    {
        Bgp4DumpBuf (pu1UpdMsg, u2TotSize, BGP4_OUTGOING);
    }

    i4RetVal = Bgp4PeerAddMsgToTxQ (pPeerentry, pu1UpdMsg, u2TotSize, 0);
    /* If Bgp4PeerAddMsgToTxQ fails due to memory allocation failure
     * for Tx node, release pu1UpdMsg and return */
    if (i4RetVal != BGP4_SUCCESS)
    {
        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, (UINT1 *) pu1UpdMsg);
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Failed to add EOR Update message to peer Tx list\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        return;
    }

    /* Send EOR for All Supported capabilities */
    if (Bgp4EORForNegotiatedCapabilities (pPeerentry) != BGP4_FAILURE)
    {
        /* Mark the End of rib marker sent status of the peer as true */
        pPeerentry->peerStatus.u1EndOfRIBMarkerSent = BGP4_TRUE;
    }

    /* If End of rib marker is sent to all the peers,then call
     * Bgp4GRExitRestart to exit the restart procedure 
     * successfully */
    if ((Bgp4GRCheckEORMarkerSent (BGP4_PEER_CXT_ID (pPeerentry)) == BGP4_TRUE)
        && (BGP4_RESTART_MODE (BGP4_PEER_CXT_ID (pPeerentry)) ==
            BGP4_RESTARTING_MODE))
    {
        BGP4_RESTART_EXIT_REASON (BGP4_PEER_CXT_ID (pPeerentry)) =
            BGP4_GR_EXIT_SUCCESS;
        BGP4_GLB_RESTART_EXIT_REASON = BGP4_GR_EXIT_SUCCESS;
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Exiting the restart procedure successfully\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        Bgp4GRExitRestart (BGP4_PEER_CXT_ID (pPeerentry));
    }
    KW_FALSEPOSITIVE_FIX (pu1UpdMsg);
    return;
}                                /* End of Bgp4GRSendEndOfRIBMarker */

/*****************************************************************************/
/*  Function Name   :   Bgp4GRExitRestart                                    */
/*  Description     :   This function is used to exit BGP restart. If the    */
/*                      restart exit reason is failure then it will          */
/*                      re-initiate the connection with all the peers.       */
/*                      And it sends the trap                                */
/*  Input(s)        :   None                                                 */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
PUBLIC VOID
Bgp4GRExitRestart (UINT4 u4CxtId)
{
    tRtmRegnId          RegnId;
    INT4                i4RRDProtoMask = 0;
#ifdef BGP4_IPV6_WANTED
    tRtm6RegnId         Regn6Id;
    MEMSET (&Regn6Id, 0, sizeof (Regn6Id));
    Regn6Id.u2ProtoId = BGP6_ID;
    Regn6Id.u4ContextId = u4CxtId;
#endif /* BGP4_IPV6_WANTED */
    tBgp4PeerEntry     *pPeer = NULL;

    MEMSET (&RegnId, 0, sizeof (RegnId));
    RegnId.u2ProtoId = BGP_ID;
    RegnId.u4ContextId = u4CxtId;

    /* When BGP GR is disabled or when BGP GR is enabled and there is 
     * no configuration file to restore set restart mode as none and 
     * remove the gr conf file */
    if ((Bgp4GRCheckGRCapability (u4CxtId) == BGP4_FALSE)
        || (BGP4_RESTART_EXIT_REASON (u4CxtId) == BGP4_GR_EXIT_NONE))
    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\tExiting Restart Process since the speaker is not GR capable\n");
        Bgp4GRProcessRtmRouteCleanUp (&RegnId, IP_FAILURE);
#ifdef BGP4_IPV6_WANTED
        Bgp4GRProcessRtm6RouteCleanUp (&Regn6Id, IP6_FAILURE);
#endif /* BGP4_IPV6_WANTED */
        BGP4_RESTART_MODE (u4CxtId) = BGP4_RESTART_MODE_NONE;
        BGP4_GLB_RESTART_MODE = BGP4_RESTART_MODE_NONE;
        i4RRDProtoMask = (INT4) BGP4_RRD_PROTO_MASK (u4CxtId);
        Bgp4EnableRRDProtoMask (u4CxtId, (UINT4) i4RRDProtoMask);
#ifdef MSR_WANTED
        FlashRemoveFile (BGP4_GR_CONF);
#endif
        /* Delete all stale routes */
        TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4CxtId), pPeer, tBgp4PeerEntry *)
        {
            Bgp4GRDeleteAllStaleRoutes (pPeer);
        }
        if (BGP4_SELECTION_DEFERAL_FLAG (u4CxtId) != BGP4_DEFERAL_TMR_SET)
        {
            BGP4_SELECTION_DEFERAL_FLAG (u4CxtId) = BGP4_FALSE;
        }
        return;
    }

    /* Means it is a late restart which in RTM has deleted all the 
     * routes and deregistered. Since BGP has skipped RtM registration 
     * during task initialisation,registration with RTM/RTM6 
     * is made to happen again */
    if (BGP4_RESTART_EXIT_REASON (u4CxtId) == BGP4_GR_EXIT_FAILURE)
    {
        Bgp4GRProcessRtmRouteCleanUp (&RegnId, IP_FAILURE);
        RtmRegister (&RegnId, SET_MASK, Bgp4RecvMsgFromRtm);
#ifdef BGP4_IPV6_WANTED
        Bgp4GRProcessRtm6RouteCleanUp (&Regn6Id, IP6_FAILURE);
        Rtm6Register (&Regn6Id, RTM6_ACK_REQUIRED, Bgp4RecvMsgFromRtm6);
#endif /* BGP4_IPV6_WANTED */

#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
#ifdef MPLS_WANTED
        /* It means BGP module comes up after late  Graceful Restart.
         * Notify MPLS-L2VPN that BGP module comes up and clean-up of stale*/
        Bgp4VplsGRInProgress (u4CxtId);
#endif
#endif
#endif
    }
    else if (BGP4_RESTART_EXIT_REASON (u4CxtId) == BGP4_GR_EXIT_SUCCESS)
    {
        /* Timer need to be stopped only in the case of successful exit */
        /* Stop the restart timer started during successful restart */
        BGP4_TIMER_STOP (BGP4_TIMER_LISTID, gBgpNode.pBgpRestartTmr);
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\tStopping the restart timer\r\n");
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\tSending Notification to RTM to cleanup all the "
                       "stale routes belonging to BGP with successful "
                       "restart indication \r\n");
        Bgp4GRProcessRtmRouteCleanUp (&RegnId, IP_SUCCESS);
#ifdef BGP4_IPV6_WANTED
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\tSending Notification to RTM to cleanup all the "
                       "stale routes belonging to BGP with successful "
                       "restart indication \r\n");
        Bgp4GRProcessRtm6RouteCleanUp (&Regn6Id, IP6_SUCCESS);
#endif /* BGP4_IPV6_WANTED */
    }
    else if (BGP4_RESTART_EXIT_REASON (u4CxtId) == BGP4_GR_EXIT_INPROGRESS)
    {
        /* Notifies RTM that BGP has restarted so that it stops the GR timer */
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\tSending Notification to RTM to cleanup all the "
                       "stale routes belonging to BGP with failure "
                       "restart indication \r\n");
        Bgp4GRProcessRtmRouteCleanUp (&RegnId, IP_FAILURE);
#ifdef BGP4_IPV6_WANTED
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\tSending Notification to RTM to cleanup all the "
                       "stale routes belonging to BGP with failure "
                       "restart indication \r\n");
        Bgp4GRProcessRtm6RouteCleanUp (&Regn6Id, IP6_FAILURE);
#endif /* BGP4_IPV6_WANTED */

        /* If the restart exit reason is failure, 
         * reinitiate the connection for
         * all the exisiting peers */
        if (BGP4_RESTART_STATUS (u4CxtId) != BGP4_GR_STATUS_NONE)
        {
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC,
                           BGP4_MOD_NAME,
                           "\tRestart exiting with failure. "
                           "Resetting the connection for all the peers\r\n");
            Bgp4PeerSetAllActivePeerAdminStatus (u4CxtId, BGP4_ADMIN_UP);
        }
        BGP4_RESTART_EXIT_REASON (u4CxtId) = BGP4_GR_EXIT_FAILURE;
        BGP4_GLB_RESTART_EXIT_REASON = BGP4_GR_EXIT_FAILURE;
    }

    BGP4_RESTART_REASON (u4CxtId) = BGP4_GR_REASON_RESTART;
    BGP4_RESTART_MODE (u4CxtId) = BGP4_RECEIVING_MODE;
    BGP4_GLB_RESTART_MODE = BGP4_RECEIVING_MODE;
    if (BGP4_SELECTION_DEFERAL_FLAG (u4CxtId) != BGP4_DEFERAL_TMR_SET)
    {
        BGP4_SELECTION_DEFERAL_FLAG (u4CxtId) = BGP4_FALSE;
    }

    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC,
                   BGP4_MOD_NAME, "\tMoving to receiving mode\n");
    Bgp4SnmpSendStatusChgTrap (u4CxtId, BGP4_RST_STATUS_CHANGE_TRAP);

    /* Reset the restart status to none */
    BGP4_RESTART_STATUS (u4CxtId) = BGP4_GR_STATUS_NONE;
    BGP4_GLB_RESTART_STATUS = BGP4_GR_STATUS_NONE;
    i4RRDProtoMask = (INT4) BGP4_RRD_PROTO_MASK (u4CxtId);
    Bgp4EnableRRDProtoMask (u4CxtId, (UINT4) i4RRDProtoMask);

#ifdef MSR_WANTED
    /*Remove the GR conf file */
    FlashRemoveFile (BGP4_GR_CONF);
#endif
    /* Delete all stale routes */
    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4CxtId), pPeer, tBgp4PeerEntry *)
    {
        Bgp4GRDeleteAllStaleRoutes (pPeer);
    }

    return;
}                                /* End of Bgp4GRExitRestart */

#endif /*_BGP4GR_C_ */
