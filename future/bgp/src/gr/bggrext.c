/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bggrext.c,v 1.9 2014/03/18 12:01:42 siva Exp $
 *
 *******************************************************************/
/*****************************************************************************/
/* Copyright (C) 2009 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : bggrext.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        :                                                */
/*    MODULE NAME           : BGP                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 22 July 2009                                   */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           :  This file contains functions for related      */
/*                             to the BGP interactions with the RTM for      */
/*                             GR feature.                                   */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    22 July 2009           Initial Creation                        */
/*---------------------------------------------------------------------------*/

#ifndef BGPGREXT_C_
#define BGPGREXT_C_

#include "bgp4com.h"

/*****************************************************************************/
/*  Function Name   :   Bgp4GRQueryRTMForwarding                             */
/*  Description     : This function is used to query RTM/RTM6 to get the     */
/*                    forwarding plane preservation status for a particular  */
/*                    address familylies maintained during theacross last    */
/*                    restart                                                */
/*  Input(s)        :   u2Afi -Address Family                                */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  Returns the address families supported    */
/*****************************************************************************/
UINT4
Bgp4GRQueryRTMForwarding (UINT4 u4ContextId, UINT2 u2Afi,    /* Address Family */
                          UINT1 *pu1FwdPath)    /* SOFT_PATH/FAST_PATH */
{
    INT4                i4RetVal = BGP4_FAILURE;

    UNUSED_PARAM (pu1FwdPath);

    if (u2Afi == BGP4_INET_AFI_IPV4)
    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\t %s\r\n", "Querying RTM for ipv4 "
                       "forwarding state \n");
        i4RetVal = RTMGetForwardingStateInCxt (u4ContextId);
    }
#ifdef BGP4_IPV6_WANTED
    else
    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\t %s\r\n", "Querying RTM6 for ipv6 "
                       "forwarding state \n");
        i4RetVal = RTM6GetForwardingStateInCxt (u4ContextId);
    }
#endif /* BGP4_IPV6_WANTED */

    return i4RetVal;
}                                /* End of Bgp4GRQueryRTMForwarding */

/***************************************************************************/
/*  Function Name   :   Bgp4GRQueryRTMIgpConvergence                       */
/*                                                                         */
/*  Description     :   This function will lookup RTM for IGP convergence  */
/*                                                                         */
/*  Input(s)        :                                                      */
/*  Output(s)       :   None                                               */
/*                  :                                                      */
/*  <OPTIONAL Fields>           :  None                                    */
/*  Global Variables Referred   :  None                                    */
/*  Global variables Modified   :  None                                    */
/*  Exceptions or Operating System Error Handling :                        */
/*  Use of Recursion            :  None                                    */
/*  Returns                     :  BGP4_SUCCESS/ BGP4_FAILURE              */
/***************************************************************************/
INT1
Bgp4GRQueryRTMIgpConvergence (UINT4 u4ContextId, UINT2 u2Afi)
{
    INT1                i1RetVal = BGP4_FAILURE;

    if (u2Afi == BGP4_INET_AFI_IPV4)
    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\t %s\r\n",
                       "Querying RTM for IGP convergence state \n");
        i1RetVal = (INT1) RtmGetIgpConvergenceStateInCxt (u4ContextId);
    }
#ifdef BGP4_IPV6_WANTED
    else
    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\t %s\r\n",
                       "Querying RTM6 for IGP convergence state \n");
        i1RetVal = (INT1) Rtm6GetIgpConvergenceStateInCxt (u4ContextId);
    }
#endif /* BGP4_IPV6_WANTED */
    return i1RetVal;
}                                /* End of Bgp4GRQueryRTMIgpConvergence */

/***************************************************************************/
/*                                                                         */
/* Function     : Bgp4GRNotifyRestartRTM                                   */
/*                                                                         */
/* Description  : This routine notifies to rtm the restart interval        */
/*                                                                         */
/* Input        : u4CtxId - Context Id                                     */
/*                u4ProtId - which  is BGP_ID                              */
/*                u4RestartTime - The time that need to be notified to RTM */
/*                                                                         */
/* Output       : None                                                     */
/*                                                                         */
/* Returns      : NONE                                                     */
/*                                                                         */
/***************************************************************************/
PUBLIC VOID
Bgp4GRNotifyRestartRTM (UINT4 u4CtxId, UINT4 u4ProtId, UINT4 u4RestartTime)
{
    tOsixMsg           *pRtmMsg = NULL;
    tRtmMsgHdr         *pRtmMsgHdr = NULL;
#ifdef BGP4_IPV6_WANTED
    tOsixMsg           *pRtm6Msg = NULL;
    tRtm6MsgHdr        *pRtm6MsgHdr = NULL;
#endif /* BGP4_IPV6_WANTED */

    /* Notifies to RTMv4 about BGP restart with restart interval */
    if ((pRtmMsg = CRU_BUF_Allocate_MsgBufChain
         ((sizeof (tRtmMsgHdr) + sizeof (UINT4)), 0)) == NULL)

    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\t %s\r\n", "Bgp4GRNotifyRestartRTM - "
                       "Memory allocation" "failure for the RTM message \r\n");
        return;
    }

    pRtmMsgHdr = (tRtmMsgHdr *) IP_GET_MODULE_DATA_PTR (pRtmMsg);

    /* fill the message header */
    pRtmMsgHdr->u1MessageType = RTM_GR_NOTIFY_MSG;
    pRtmMsgHdr->RegnId.u2ProtoId = (UINT2) u4ProtId;
    pRtmMsgHdr->RegnId.u4ContextId = u4CtxId;
    pRtmMsgHdr->u2MsgLen = sizeof (UINT2) + sizeof (UINT4);

    if (IP_COPY_TO_BUF (pRtmMsg, &u4RestartTime,
                        0, sizeof (UINT4)) == IP_BUF_FAILURE)
    {
        IP_RELEASE_BUF (pRtmMsg, FALSE);
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\t %s\r\n", "Bgp4GRNotifyRestartRTM - "
                       "Copy to buffer failure for the RTM message \r\n");
        return;
    }

    /* Send the buffer to RTM */
    if (RpsEnqueuePktToRtm (pRtmMsg) != IP_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pRtmMsg, 0);
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\t %s\r\n", "Bgp4GRNotifyRestartRTM -"
                       "Packet Enqueue to RTM failure  \r\n");
        return;
    }
#ifdef BGP4_IPV6_WANTED
    /* Notifies to RTMv4 about BGP restart with restart interval */
    if ((pRtm6Msg = CRU_BUF_Allocate_MsgBufChain
         ((sizeof (tRtm6MsgHdr) + sizeof (UINT4)), 0)) == NULL)

    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\t %s\r\n", " Bgp4GRNotifyRestartRTM -"
                       "Memory allocation failure for the "
                       "RTM6 message \r\n");
        return;
    }

    pRtm6MsgHdr = (tRtm6MsgHdr *) IP6_GET_MODULE_DATA_PTR (pRtm6Msg);

    /* fill the message header */
    pRtm6MsgHdr->u1MessageType = RTM6_GR_NOTIFY_MSG;
    pRtm6MsgHdr->RegnId.u2ProtoId = BGP6_ID;
    pRtm6MsgHdr->RegnId.u4ContextId = u4CtxId;
    pRtm6MsgHdr->u2MsgLen = sizeof (UINT2) + sizeof (UINT4);

    if (IP6_COPY_TO_BUF (pRtm6Msg, &u4RestartTime,
                         0, sizeof (UINT4)) == CRU_FAILURE)
    {
        IP6_RELEASE_BUF (pRtm6Msg, FALSE);
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\t %s\r\n", "Bgp4GRNotifyRestartRTM - Copy to"
                       "buffer failure for the RTM6 message \r\n");
        return;
    }

    /* Send the buffer to RTM6 */
    if (RpsEnqueuePktToRtm6 (pRtm6Msg) != IP6_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pRtm6Msg, 0);
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                       "\t %s\r\n", "Bgp4GRNotifyRestartRTM -"
                       "Packet Enqueue to RTM6 failure  \r\n");
        return;
    }
    return;

#endif /* BGP4_IPV6_WANTED */
}                                /* End of Bgp4GRNotifyRestartRTM */

/*****************************************************************************/
/*                                                                           */
/* Function     : Bgp4GRGetSecondsSinceBase                                  */
/*                                                                           */
/* Description  : This routine calculates the seconds from the obtained      */
/*                time. The seconds is calculated from the BASE year (2000)  */
/*                                                                           */
/* Input        : tUtlTm   -   Time format structure                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : The calculated time in seconds                             */
/*                                                                           */
/*****************************************************************************/

UINT4
Bgp4GRGetSecondsSinceBase (tUtlTm utlTm)
{
    UINT4               u4year = utlTm.tm_year;
    UINT4               u4Secs = 0;

    --u4year;
    while (u4year >= TM_BASE_YEAR)
    {
        u4Secs += SECS_IN_YEAR (u4year);
        --u4year;
    }                            /* End of while */

    u4Secs += (utlTm.tm_yday * BGP4_SEC_IN_YEAR);
    u4Secs += (utlTm.tm_hour * BGP4_SEC_IN_HOUR);
    u4Secs += (utlTm.tm_min * BGP4_SEC_IN_MIN);
    u4Secs += (utlTm.tm_sec);

    return u4Secs;
}                                /*End of Bgp4GRGetSecondsSinceBase */

/*****************************************************************************/
/*                                                                           */
/* Function     : Bgp4GRStoreRestartConfig                                   */
/*                                                                           */
/* Description  : This routine save the BGP restart configurations in a file */
/*                bgpGr.txt file in the following way                        */
/*                1.Saves the remaining time in restart timer                */
/*                2.Write into file whether the restart support is planned or*/
/*                    none                                                     */
/*                2. Saves the restart reason whether it is software reload, */
/*                   upgrade,  or unknown                                    */
/*                5. Saves whether the restart status                        */
/*                6. Get the system time now and save it in file             */
/*                                                                           */
/* Input        : NONE                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : NONE                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
Bgp4GRStoreRestartConfig (VOID)
{
    UINT4               u4RemGrPeriod = 0;
    tUtlTm              utlTm;

    tBgpGrSaveConfig    GrSaveConfig;
    MEMSET (&GrSaveConfig, 0, sizeof (tBgpGrSaveConfig));
    MEMSET (&utlTm, 0, sizeof (utlTm));

    /* Store the remaining time of the restart timer already started */
    if (TmrGetRemainingTime
        (BGP4_TIMER_LISTID,
         (gBgpNode.pBgpRestartTmr), &u4RemGrPeriod) != TMR_SUCCESS)
    {
        u4RemGrPeriod = 0;
    }
    else
    {
        u4RemGrPeriod = u4RemGrPeriod / SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    }
    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC,
                   BGP4_MOD_NAME,
                   "\t %s\r\n", "Bgp4GRStoreRestartConfig - "
                   "Stopping the restart timer");
    BGP4_TIMER_STOP (BGP4_TIMER_LISTID, gBgpNode.pBgpRestartTmr);

    GrSaveConfig.u4RestartTime = u4RemGrPeriod;

    UtlGetTime (&utlTm);

    /* Get the seconds since the base year (2000) */
    GrSaveConfig.u4ShutTime = Bgp4GRGetSecondsSinceBase (utlTm);
    IssStoreGraceContent (&GrSaveConfig, BGP_ID);
    return;
}                                /* Bgp4GRStoreRestartConfig */

/*****************************************************************************/
/*                                                                           */
/* Function     : Bgp4GRReStoreRestartConfig                                 */
/*                                                                           */
/* Description  : This routine restores the configuration stored in bgpgr.txt*/
/*                It will start the restart timer with the remaining time as */
/*                restart interval                                           */
/*                                                                           */
/* Input        : NONE                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
INT1
Bgp4GRReStoreRestartConfig (UINT4 u4CxtId)
{
    INT4                i4GraceRemTime = 0;    /* store grace remaining time */
    UINT4               u4SysTime = 0;    /* store restart system time */
    UINT4               u4GrShutTime = 0;    /* restore the shut down of BGP */
    tUtlTm              utlTm;    /* Structure to fetch the system time */
    tBgpGrSaveConfig    GrSaveConfig;

    /* Initialising structure values */

    MEMSET (&utlTm, 0, sizeof (tUtlTm));
    MEMSET (&GrSaveConfig, 0, sizeof (tBgpGrSaveConfig));

    IssRestoreGraceContent (&GrSaveConfig, BGP_ID);
#ifdef MSR_WANTED
    if (FlashFileExists (BGP4_GR_CONF) != SNMP_SUCCESS)
    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC,
                       BGP4_MOD_NAME,
                       "\t %s\r\n", "Bgp4GRReStoreRestartConfig - "
                       "bgpgr.txt file does'nt exists. Hence an "
                       "unplanned restart\r\n");
        return BGP4_FAILURE;
    }
#endif
    /* Restore the configurations */
    i4GraceRemTime = (INT4) GrSaveConfig.u4RestartTime;
    u4GrShutTime = GrSaveConfig.u4ShutTime;
    UtlGetTime (&utlTm);

    /* Get the seconds since the base year (2000) */
    u4SysTime = Bgp4GRGetSecondsSinceBase (utlTm);

    i4GraceRemTime = i4GraceRemTime - (u4SysTime - u4GrShutTime);

    /* Means the speaker has come up after the restart time has expired */
    if (i4GraceRemTime <= 0)
    {
        BGP4_RESTART_STATUS (u4CxtId) = BGP4_GR_STATUS_NONE;
        BGP4_GLB_RESTART_STATUS = BGP4_GR_STATUS_NONE;
        return BGP4_SUCCESS;
    }
    else                        /* Speaker has successfully come up within "
                                 * the specified interval */
    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC,
                       BGP4_MOD_NAME,
                       "\t %s\r\n", "Bgp4GRReStoreRestartConfig - "
                       "Starting"
                       "the restart timer with" "remaining grace period\n");
        BGP4_TIMER_START (BGP4_TIMER_LISTID,
                          gBgpNode.pBgpRestartTmr, i4GraceRemTime);
        BGP4_RESTART_EXIT_REASON (u4CxtId) = BGP4_GR_EXIT_INPROGRESS;
        BGP4_GLB_RESTART_EXIT_REASON = BGP4_GR_EXIT_INPROGRESS;

    }
    BGP4_RESTART_STATUS (u4CxtId) = BGP4_GR_STATUS_PLANNED;
    BGP4_GLB_RESTART_STATUS = BGP4_GR_STATUS_PLANNED;
    return BGP4_SUCCESS;
}                                /* Bgp4GRReStoreRestartConfig */

#ifdef BGP4_IPV6_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : Bgp4GRProcessRtm6RouteCleanUp                              */
/*                                                                           */
/* Description  : This routine interacts with RTM6 to cleanup the stale      */
/*                routes                                                     */
/*                                                                           */
/* Input        : NONE                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
Bgp4GRProcessRtm6RouteCleanUp (tRtm6RegnId * pRtm6RegnId, INT1 i1Status)
{
    Rtm6ProcessGRRouteCleanUp (pRtm6RegnId, i1Status);
    return;
}
#endif /* BGP4_IPV6_WANTED */

/*****************************************************************************/
/*                                                                           */
/* Function     : Bgp4GRProcessRtmRouteCleanUp                               */
/*                                                                           */
/* Description  : This routine interacts with RTM to cleanup the stale       */
/*                routes                                                     */
/*                                                                           */
/* Input        : NONE                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
Bgp4GRProcessRtmRouteCleanUp (tRtmRegnId * pRtmRegnId, INT1 i1Status)
{
    RtmProcessGRRouteCleanUp (pRtmRegnId, i1Status);
    return;
}

#endif /* _BGPGREXT_C_ */
