/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bggrtmr.c,v 1.12 2016/12/27 12:35:57 siva Exp $
 *
 *******************************************************************/
/*****************************************************************************/
/*****************************************************************************/
/*    FILE  NAME            : bggrtmr.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        :                                                */
/*    MODULE NAME           : BGP                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 22 July 2009                                   */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           :  This file contains functions for related to   */
/*                             timers related to GR functionality            */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    22 July 2009           Initial Creation                        */
/*---------------------------------------------------------------------------*/
#ifndef BGPGRTMR_C
#define BGPGRTMR_C

#include "bgp4com.h"

/*****************************************************************************/
/*  Function Name   :   Bgp4GRProcessSelDefTimerExpiry                       */
/*  Description     :   This function will mark the route selection flag as  */
/*                      true and stop the selection deferral timer           */
/*  Input(s)        :   None                                                 */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  BGP4_SUCCESS/ BGP4_FAILURE                */
/*****************************************************************************/
PUBLIC INT4
Bgp4GRProcessSelDefTimerExpiry (VOID)
{
    UINT4               u4ContextId;
    INT4                i4RetStatus;

    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                   "\t %s\r\n", "Bgp4GRProcessSelectionDefTimerExpiry - Setting"
                   "the route selection flag as true \r\n");
    i4RetStatus = BgpGetFirstActiveContextID (&u4ContextId);
    while (i4RetStatus == SNMP_SUCCESS)
    {
        BGP4_ROUTE_SELECTION_FLAG (u4ContextId) = BGP4_TRUE;
        BGP4_RESTART_REASON (u4ContextId) = BGP4_GR_REASON_RESTART;
        BGP4_ROUTE_SELECTION_FLAG (u4ContextId) = BGP4_TRUE;
        BGP4_SELECTION_DEFERAL_FLAG(u4ContextId) = BGP4_FALSE;
        i4RetStatus = BgpGetNextActiveContextID (u4ContextId, &u4ContextId);
    }
    return BGP4_SUCCESS;

}                                /* End of Bgp4GRProcessSelDefTimerExpiry */
/*****************************************************************************/
/*  Function Name   :   Bgp4GRProcessNetworkMsrTempTimerExpiry               */
/*  Description     :                                                        */
/*  Input(s)        :   None                                                 */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  BGP4_SUCCESS/ BGP4_FAILURE                */
/*****************************************************************************/
PUBLIC INT4
Bgp4GRProcessNetworkMsrTempTimerExpiry (VOID)
{
    tNetworkAddr       *pNetworkRt = NULL;
    UINT4               u4Context = 0;
    INT4                i4Sts = 0;
    tAddrPrefix         NetworkAddr;
   
    pNetworkRt = (tNetworkAddr *)
        RBTreeGetFirst (BGP4_NETWORK_ROUTE_DATABASE(u4Context));
    while (pNetworkRt != NULL)
    {
        MEMCPY(&NetworkAddr, &(pNetworkRt->NetworkAddr), sizeof (tAddrPrefix));
        i4Sts = BgpNetworkIpRtLookup (u4Context, &(NetworkAddr),
                                      pNetworkRt->u2PrefixLen,
                                      BGP4_NETWORK_ROUTE_ADD,
                                      BGP4_ALL_APPIDS,0,NULL);
        if (i4Sts == BGP4_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                    BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                    "\tERROR - Unable to find matching Network Route Entry.\n");
            return SNMP_FAILURE;
        }
        pNetworkRt = (tNetworkAddr *)
            RBTreeGetNext (BGP4_NETWORK_ROUTE_DATABASE(u4Context),
                        (tRBElem *) pNetworkRt, NULL);

    }

    gBgpNode.u4BgpNetworkTempTmrRunning = OSIX_FALSE;
    return BGP4_SUCCESS;
}                                /* End of Bgp4GRProcessNetworkMsrTempTimerExpiry */

/*****************************************************************************/
/*  Function Name   :   Bgp4GRProcessPeerRstTimerExpiry                      */
/*  Description     :   This will put the peer in de-init list so that routes*/
/*                      learned from peer will be released from local rib and*/
/*                      FIB. In addition these routes will be advertised as  */
/*                      withdrawn routes                                     */
/*                      to this peer                                         */
/*  Input(s)        :   pPeerEntry                                           */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  BGP4_SUCCESS/ BGP4_FAILURE                */
/*****************************************************************************/
PUBLIC INT4
Bgp4GRProcessPeerRstTimerExpiry (tBgp4PeerEntry * pPeerEntry)    /* Peerentry 
                                                                 * whose
                                                                 * restart timer exp
                                                                 * to be processed */
{
    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)), BGP4_TRC_FLAG,
                   BGP4_GR_TRC, BGP4_MOD_NAME,
                   "\t %s\r\n", "Bgp4GRProcessPeerRstTimerExpiry - Putting "
                   "peer in De-Init list \r\n");
    BGP4_PEER_RESTART_MODE (pPeerEntry) = BGP4_RECEIVING_MODE;
    Bgp4GRPutPeerInDeInitList (pPeerEntry);
    return BGP4_SUCCESS;
}                                /* End of Bgp4GRProcessPeerRstTimerExpiry */

/*****************************************************************************/
/*  Function Name   :   Bgp4GRProcessRestartTimerExpiry                      */
/*  Description     :   This function will clean up all the resources related*/
/*                      to this peer                                         */
/*  Input(s)        :   pPeerEntry                                           */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  BGP4_SUCCESS/ BGP4_FAILURE                */
/*****************************************************************************/
PUBLIC INT4
Bgp4GRProcessRestartTimerExpiry ()
{
    tBgp4PeerEntry      *pPeer = NULL;
    UINT4               u4ContextId;
    INT4                i4RetStatus;

    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                   "\t %s\r\n", "BGP4-GR : Restart Timer Expiry Handling \r\n");
    i4RetStatus = BgpGetFirstActiveContextID (&u4ContextId);
    while (i4RetStatus == SNMP_SUCCESS)
    {
        BGP4_RESTART_EXIT_REASON (u4ContextId) = BGP4_GR_EXIT_FAILURE;
        Bgp4GRExitRestart (u4ContextId);
        TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4ContextId), pPeer, tBgp4PeerEntry *)
        {
            if (Bgp4GRIsPeerGRCapable (pPeer) != BGP4_SUCCESS)
            {
                Bgp4GRDeleteAllStaleRoutes (pPeer);
            }

        }
        i4RetStatus = BgpGetNextActiveContextID (u4ContextId, &u4ContextId);
    }
    BGP4_GLB_RESTART_EXIT_REASON = BGP4_GR_EXIT_FAILURE;
    return BGP4_SUCCESS;
}                                /* End of Bgp4GRProcessRestartTimerExpiry */

/*****************************************************************************/
/*  Function Name   :   Bgp4GRProcessStaleTimerExpiry                        */
/*  Description     :   This function will remove all the stale routes       */
/*                      for the given peer                                   */
/*  Input(s)        :   pPeerEntry                                           */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  BGP4_SUCCESS/ BGP4_FAILURE                */
/*****************************************************************************/
PUBLIC INT4
Bgp4GRProcessStaleTimerExpiry (tBgp4PeerEntry * pPeerEntry)    /* PeerEntry whose 
                                                             * stale timer expiry
                                                             * need to be processed */
{
    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                   "\t %s\r\n", "Bgp4GRProcessStaleTimerExpiry : "
                   "Deleting all the " "stale routes");
    if (BGP4_GET_NODE_STATUS != RM_STANDBY)
    {
    Bgp4GRDeleteAllStaleRoutes (pPeerEntry);
    }
    return BGP4_SUCCESS;
}                                /* End of Bgp4GRProcessStaleTimerExpiry */

#endif /* _BGPGRTMR_C */
