/*****************************************************************************/
/* Copyright (C) 2009 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/* $Id: bggrutil.c,v 1.28 2017/09/15 06:19:53 siva Exp $                   */
/*****************************************************************************/
/*    FILE  NAME            : bggrutil.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        :                                                */
/*    MODULE NAME           : BGP                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 22 July 2009                                   */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           :  This file contains functions  related to      */
/*                             utilities used for GR feature                 */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    22 July 2009           Initial Creation                        */
/*---------------------------------------------------------------------------*/
#ifndef BGGRUTIL_C
#define BGGRUTIL_C

/* Header file inclusion */
#include "bgp4com.h"
#include "fsbgp4wr.h"
#include "stdbgpwr.h"

/*****************************************************************************/
/*  Function Name   :   Bgp4GRCheckGRCapability                              */
/*  Description     :   This function is used to check the GR capability of  */
/*                      the router. If the bgp global flag                   */
/*                      u1GrAdminStatus is set then this API returns         */
/*                      BGP4_TRUE; else BGP4_FALSE                           */
/*  Input(s)        :   None                                                 */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  BGP4_TRUE/BGP4_FALSE                      */
/*****************************************************************************/
PUBLIC INT1
Bgp4GRCheckGRCapability (UINT4 u4Context)
{
    return (gBgpCxtNode[u4Context]->u1GrAdminStatus);    /* BGP4_TRUE or BGP4_FALSE */
}                                /* End of Bgp4GRCheckGRCapability */

/*****************************************************************************/
/*  Function Name   :   Bgp4GRIsPeerGRCapable                                */
/*  Description     :   This function will check if the peer is GR capable   */
/*  Input(s)        :   pPeerEntry                                           */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  BGP4_SUCCESS/BGP4_FAILURE                 */
/*****************************************************************************/
PUBLIC INT4
Bgp4GRIsPeerGRCapable (tBgp4PeerEntry * pPeerEntry)    /* PeerEntry whose GR 
                                                     * capability
                                                     * to be checked for */
{
    tTMO_SLL           *pCapsList = NULL;
    tSupCapsInfo       *pTmpSupCap = NULL;

    pCapsList = BGP4_PEER_RCVD_CAPS_LIST (pPeerEntry);
    TMO_SLL_Scan (pCapsList, pTmpSupCap, tSupCapsInfo *)
    {
        if ((pTmpSupCap->SupCapability.u1CapCode == CAP_CODE_GRACEFUL_RESTART))
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                           "\tPEER %s is GR capable\r\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerEntry))));
            return BGP4_SUCCESS;
        }
    }                            /* End of TMO_SLL_Scan */
    return BGP4_FAILURE;
}                                /* End of Bgp4GRIsPeerGRCapable */

/*****************************************************************************/
/*  Function Name   :   Bgp4GRPutPeerInDeInitList                            */
/*  Description     :   This function will put the peer in the Deinit list   */
/*  Input(s)        :   pPeerEntry                                           */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  BGP4_SUCCESS/BGP4_FAILURE                 */
/*****************************************************************************/
PUBLIC INT4
Bgp4GRPutPeerInDeInitList (tBgp4PeerEntry * pPeerentry)    /* Pointer to 
                                                         *  peerentry 
                                                         * which need to be put into
                                                         * BGP4_PEER_DEINIT_LIST */
{
    tPeerNode          *pPeerNode = NULL;

    BGP_PEER_NODE_CREATE (pPeerNode);
    if (pPeerNode == NULL)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_GR_TRC |
                       BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Memory Allocation to handle "
                       "Peer Shutdown FAILED!!!\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        gu4BgpDebugCnt[MAX_BGP_PEER_NODE_SIZING_ID]++;
        return BGP4_FAILURE;
    }
    pPeerNode->pPeer = pPeerentry;
    TMO_SLL_Add (BGP4_PEER_DEINIT_LIST (BGP4_PEER_CXT_ID (pPeerentry)),
                 &pPeerNode->TSNext);
    BGP4_SET_PEER_CURRENT_STATE (pPeerentry, BGP4_PEER_DEINIT_INPROGRESS);
    return BGP4_SUCCESS;

}                                /* End of Bgp4GRPutPeerInDeInitList */

/*****************************************************************************/
/*  Function Name   :   Bgp4GRMarkPeerRoutesAsStale                          */
/*  Description     :   This function will mark the peer routes as stale     */
/*  Input(s)        :   pPeerEntry                                           */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
PUBLIC VOID
Bgp4GRMarkPeerRoutesAsStale (tBgp4PeerEntry * pPeerentry)    /* Peerentry
                                                             * whose routes 
                                                             * need to be 
                                                             * marked stale */
{
    UINT4               u4AFIndex = BGP4_IPV4_UNI_INDEX;
    tRouteProfile      *pPeerRt = NULL;

#if ((defined VPLSADS_WANTED) && (defined VPLS_GR_WANTED))
    for (u4AFIndex = BGP4_IPV4_UNI_INDEX;
         u4AFIndex <= (BGP4_MPE_ADDR_FAMILY_MAX_INDEX); u4AFIndex++)
#else
    for (u4AFIndex = BGP4_IPV4_UNI_INDEX;
         u4AFIndex <= (BGP4_MPE_ADDR_FAMILY_MAX_INDEX -
                       BGP4_DECREMENT_BY_ONE); u4AFIndex++)
#endif
    {
        if (BGP4_PEER_AFI_SAFI_INSTANCE (pPeerentry, u4AFIndex) == NULL)
        {
            /* AFI-SAFI not negotiated for this peer. */
            continue;
        }
        pPeerRt = BGP4_DLL_FIRST_ROUTE (BGP4_PEER_ROUTE_LIST (pPeerentry,
                                                              u4AFIndex));
        if (pPeerRt == NULL)
        {
            /* No more route from this peer is present in RIB's for
             * this <AFI> <SAFI>. Try and process the next 
             * <AFI> <SAFI>*/
            continue;
        }
        for (;;)
        {
            /* Mark the route as stale */
            BGP4_RT_SET_FLAG (pPeerRt, BGP4_RT_STALE);
            if (pPeerRt->pRtDampHist != NULL)
            {
                Bgp4TmrhStopTimer (BGP4_ROUTE_REUSE_TIMER,
                                   (VOID *) pPeerRt->pRtDampHist);
            }
            pPeerRt = BGP4_RT_PROTO_LINK_NEXT (pPeerRt);
            if (pPeerRt == NULL)
            {
                /* No more routes present for this peer */
                break;
            }                    /* End of if */
        }                        /* End of infinite for */

    }                            /* End of For all address families */
    return;
}                                /* End of Bgp4GRMarkPeerRoutesAsStale */

/*****************************************************************************/
/*  Function Name   :    Bgp4GRCheckRouteSelection                           */
/*                                                                           */
/*  Description     : This function will do the following                    */
/*                    1.  it will check the speaker is GR capable if it is it*/
/*                         will do the following steps otherwise it will     */
/*                         return BGP4_TRUE                                  */
/*                    2.  it will check the speaker is in restarting mode if */
/*                        it is it will do the following steps otherwise it  */
/*                        will return BGP4_TRUE                              */
/*                    3.  it will check whether it received End_Of_RIB marker*/
/*                        from all GR capable peers if it is it will do the  */
/*                        following otherwise it will return BGP4_FALSE      */
/*                    4.  it will query IGP convergence with RTM by calling  */
/*                        Bgp4GRQueryRTMIgpConvergence if this returns true  */
/*                        then it will return BGP4_TRUE otherwise it will    */
/*                        return BGP4_FALSE                                  */
/*  Input(s)        :  NONE                                                  */
/*  Output(s)       :  None                                                  */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  BGP4_TRUE/ BGP4_FALSE                     */
/*****************************************************************************/
PUBLIC UINT1
Bgp4GRCheckRouteSelection (UINT4 u4CxtId)
{
    /* check the deferal timer flag */
    if ((BGP4_SELECTION_DEFERAL_FLAG (u4CxtId) == BGP4_TRUE) ||
        (BGP4_SELECTION_DEFERAL_FLAG (u4CxtId) == BGP4_DEFERAL_TMR_SET))
    {
        return BGP4_FALSE;
    }

    /* Route Selection is already true */
    if ((BGP4_ROUTE_SELECTION_FLAG (u4CxtId) == BGP4_TRUE))
    {
        return BGP4_TRUE;
    }
    /* The speaker is not GR capable. Hence route 
     * selection is true */
    if (Bgp4GRCheckGRCapability (u4CxtId) == BGP4_FALSE)
    {
        BGP4_ROUTE_SELECTION_FLAG (u4CxtId) = BGP4_TRUE;
        return BGP4_TRUE;
    }

    if (BGP4_RESTART_MODE (u4CxtId) == BGP4_RECEIVING_MODE)
    {
        BGP4_ROUTE_SELECTION_FLAG (u4CxtId) = BGP4_TRUE;
        return BGP4_TRUE;
    }

    if (Bgp4GRCheckEORMarkerRecvd (u4CxtId) == BGP4_TRUE)
    {
        if ((Bgp4GRQueryRTMIgpConvergence (u4CxtId, BGP4_INET_AFI_IPV4)) ==
            BGP4_SUCCESS)
        {
#ifdef BGP4_IPV6_WANTED
            if ((Bgp4GRQueryRTMIgpConvergence (u4CxtId, BGP4_INET_AFI_IPV6)) ==
                BGP4_SUCCESS)
            {
#endif /* BGP4_IPV6_WANTED */
                BGP4_TIMER_STOP (BGP4_TIMER_LISTID,
                                 gBgpNode.pBgpSelectionDeferTmr);
                BGP4_ROUTE_SELECTION_FLAG (u4CxtId) = BGP4_TRUE;
                return BGP4_TRUE;
#ifdef BGP4_IPV6_WANTED
            }

#endif /* BGP4_IPV6_WANTED */
        }
    }
    return BGP4_FALSE;
}                                /* End of Bgp4GRCheckRouteSelection */

/*****************************************************************************/
/*  Function Name   :   Bgp4GRCheckRestartMode                               */
/*  Description     :   This function will return the value of               */
/*                      gBgpNode. u1RestartMode                              */
/*  Input(s)        :   None                                                 */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  BGP4_RESTARTING_MODE/BGP4_RECEIVING_MODE  */
/*                                 BGP4_RESTART_MODE_NONE                    */
/*****************************************************************************/
PUBLIC UINT1
Bgp4GRCheckRestartMode (UINT4 u4Context)
{
    return gBgpCxtNode[u4Context]->u1RestartMode;
}                                /* End of Bgp4GRCheckRestartMode */

/*****************************************************************************/
/*  Function Name   :   Bgp4GRDeleteAllStaleRoutes                           */
/*  Description     :   This function is used to delete all the stale routes */
/*                      in BGP4_PEER_ROUTE_LIST for the given peer from Local*/
/*                      RIB. And the stale routes are advertised as withdrawn*/
/*                      to all the existing peers                            */
/*  Input(s)        :   pPeerEntry                                           */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
PUBLIC VOID
Bgp4GRDeleteAllStaleRoutes (tBgp4PeerEntry * pPeerentry)    /* Pointer to peer  
                                                             * whose stale routesto
                                                             * be deleted */
{
    UINT4               u4AFIndex = BGP4_IPV4_UNI_INDEX;
    tRouteProfile      *pPeerRt = NULL;

    for (u4AFIndex = BGP4_IPV4_UNI_INDEX;
         u4AFIndex <= (BGP4_MPE_ADDR_FAMILY_MAX_INDEX - BGP4_DECREMENT_BY_ONE);
         u4AFIndex++)
    {
        if (BGP4_PEER_AFI_SAFI_INSTANCE (pPeerentry, u4AFIndex) == NULL)
        {
            /* AFI-SAFI not negotiated for this peer. */
            continue;
        }
        pPeerRt = BGP4_DLL_FIRST_ROUTE (BGP4_PEER_ROUTE_LIST (pPeerentry,
                                                              u4AFIndex));
        if (pPeerRt == NULL)
        {
            /* No more route from this peer is present in RIB's for
             * this <AFI> <SAFI>. Try and process the next 
             * <AFI> <SAFI>*/
            continue;
        }
        BGP4_SET_PEER_CURRENT_STATE (pPeerentry,
                                     BGP4_PEER_STALE_DEL_INPROGRESS);

        for (;;)
        {
            /* Mark the route stale routes with stale */
            if (((BGP4_RT_GET_FLAGS (pPeerRt) & BGP4_RT_STALE) == BGP4_RT_STALE)
                && ((BGP4_RT_GET_FLAGS (pPeerRt) & BGP4_RT_WITHDRAWN) !=
                    BGP4_RT_WITHDRAWN))
            {
                BGP4_RT_SET_FLAG (pPeerRt, BGP4_RT_WITHDRAWN);
                if (pPeerRt->pRtDampHist != NULL)
                {
                    Bgp4TmrhStopTimer (BGP4_ROUTE_REUSE_TIMER,
                                       (VOID *) pPeerRt->pRtDampHist);
                }
                /* Disable RFD feature for the route as Stale route must 
                 * be immediately removed from RIB */
                BGP4_RT_SET_FLAG (pPeerRt, BGP4_RT_NO_RFD);
                Bgp4DshAddRouteToList (BGP4_PEER_RCVD_ROUTES (pPeerentry),
                                       pPeerRt, BGP4_PREPEND);
            }
            pPeerRt = BGP4_RT_PROTO_LINK_NEXT (pPeerRt);
            if (pPeerRt == NULL)
            {
                /* No more routes present for this peer */
                break;
            }                    /* End of if */
        }                        /* End of infinite for */
    }                            /* End of For all address families */

    return;
}                                /* End of Bgp4GRDeleteAllStaleRoutes */

/*****************************************************************************/
/*  Function Name   :   Bgp4GRDeleteAfiSafiStaleRoutes                       */
/*  Description     :   This function is used to delete all the stale routes */
/*                      for all the address families that are maintained by  */
/*                      the peer. It will send withdrawn message to all the  */
/*                      existing peers, the deleted stale routes             */
/*  Input(s)        :   pPeerEntry                                           */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
PUBLIC VOID
Bgp4GRDeleteAfiSafiStaleRoutes (tBgp4PeerEntry * pPeerEntry,    /* Pointer to 
                                                                 * peerentry 
                                                                 * whose routes 
                                                                 * to be deleted
                                                                 */
                                UINT4 u4AFIndex)    /* Address family index */
{
    tRouteProfile      *pPeerRt = NULL;
    if (BGP4_PEER_AFI_SAFI_INSTANCE
        (pPeerEntry, (u4AFIndex - BGP4_DECREMENT_BY_ONE)) == NULL)
    {
        return;
    }

    pPeerRt = BGP4_DLL_FIRST_ROUTE (BGP4_PEER_ROUTE_LIST (pPeerEntry,
                                                          u4AFIndex -
                                                          BGP4_DECREMENT_BY_ONE));
    if (pPeerRt == NULL)
    {
        /* No more route from this peer is present in RIB's for
         * this <AFI> <SAFI>. Try and process the next <AFI> <SAFI>*/
        return;
    }

    BGP4_SET_PEER_CURRENT_STATE (pPeerEntry, BGP4_PEER_STALE_DEL_INPROGRESS);
    for (;;)
    {
        /* Mark the route stale routes with stale */
        if ((BGP4_RT_GET_FLAGS (pPeerRt) & BGP4_RT_STALE) == BGP4_RT_STALE)
        {
            BGP4_RT_SET_FLAG (pPeerRt, BGP4_RT_WITHDRAWN);
            Bgp4DshAddRouteToList (BGP4_PEER_RCVD_ROUTES (pPeerEntry),
                                   pPeerRt, BGP4_TIMESTAMP);
        }
        pPeerRt = BGP4_RT_PROTO_LINK_NEXT (pPeerRt);
        if (pPeerRt == NULL)
        {
            /* No more routes present for this peer */
            break;
        }                        /* End of if */
    }                            /* End of For */
    return;
}                                /* End of Bgp4GRDeleteAfiSafiStaleRoutes */

/*****************************************************************************/
/*  Function Name   :   Bgp4GRCheckEORMarkerSent                             */
/*  Description     :   This function is used to check whether End of RIB    */
/*                      marker is sent to all the GR capable peers           */
/*  Input(s)        :   None                                                 */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
PUBLIC INT4
Bgp4GRCheckEORMarkerSent (UINT4 u4CxtId)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tTMO_SLL           *pCapsList = NULL;
    tSupCapsInfo       *pTmpSupCap = NULL;

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4CxtId), pPeer, tBgp4PeerEntry *)
    {

        if (BGP4_PEER_STATE (pPeer) != BGP4_ESTABLISHED_STATE
            && (BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_START))
        {
            return BGP4_FALSE;
        }
        pCapsList = BGP4_PEER_RCVD_CAPS_LIST (pPeer);
        TMO_SLL_Scan (pCapsList, pTmpSupCap, tSupCapsInfo *)
        {
            if ((pTmpSupCap->SupCapability.u1CapCode ==
                 CAP_CODE_GRACEFUL_RESTART) &&
                (BGP4_PEER_SENT_EOR_MARKER (pPeer) != BGP4_TRUE))
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                               BGP4_TRC_FLAG, BGP4_GR_TRC,
                               BGP4_MOD_NAME,
                               "\t End of rib marker is not "
                               "sent to the peer %s\r\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeer),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeer))));
                return BGP4_FALSE;
            }
            else
            {
                continue;
            }

        }                        /* End of scanning the capabability list of per peer */
    }                            /* End of TMO_SLL_Scan of BGP4_PEERENTRY_HEAD(u4CxtId) */
    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                   "\tEnd of rib marker is sent to  all "
                   "GR capable peers\r\n");
    return BGP4_TRUE;
}                                /* End of Bgp4GRCheckEORMarkerSent */

/*****************************************************************************/
/*  Function Name   :   Bgp4GRCheckEORMarkerRecvd                            */
/*  Description     :   This function is used to check whether End of RIB    */
/*                      marker is received from all the GR capable peers     */
/*  Input(s)        :   None                                                 */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
PUBLIC INT4
Bgp4GRCheckEORMarkerRecvd (UINT4 u4CxtId)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tTMO_SLL           *pCapsList = NULL;
    tSupCapsInfo       *pTmpSupCap = NULL;

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4CxtId), pPeer, tBgp4PeerEntry *)
    {
        pCapsList = BGP4_PEER_RCVD_CAPS_LIST (pPeer);
        TMO_SLL_Scan (pCapsList, pTmpSupCap, tSupCapsInfo *)
        {
            if ((pTmpSupCap->SupCapability.u1CapCode ==
                 CAP_CODE_GRACEFUL_RESTART) &&
                (BGP4_PEER_RESTART_MODE (pPeer) != BGP4_RESTARTING_MODE) &&
                (BGP4_PEER_RECVD_EOR_MARKER (pPeer) != BGP4_TRUE))
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                               BGP4_TRC_FLAG, BGP4_GR_TRC,
                               BGP4_MOD_NAME,
                               "\t End of rib marker is not "
                               "received for this peer %s\r\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeer),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeer))));
                return BGP4_FALSE;
            }
            else
            {
                continue;
            }

        }                        /* End of scanning the capabability list of per peer */
    }                            /* End of TMO_SLL_Scan of BGP4_PEERENTRY_HEAD(u4CxtId) */
    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                   "\tEnd of rib marker is received from all "
                   "GR capable peers\r\n");
    return BGP4_TRUE;

}                                /* End of Bgp4GRCheckEORMarkerRecvd */

/*****************************************************************************/
/*  Function Name   :   Bgp4GRCheckEORMarkerRecvdAtEstb                      */
/*  Description     :   This function is used to check whether End of RIB    */
/*                      marker is received from all the GR capable peers     */
/*                      at sessin Establishment                              */
/*  Input(s)        :   None                                                 */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
PUBLIC INT4
Bgp4GRCheckEORMarkerRecvdAtEstb (UINT4 u4CxtId)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tTMO_SLL           *pCapsList = NULL;
    tSupCapsInfo       *pTmpSupCap = NULL;

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4CxtId), pPeer, tBgp4PeerEntry *)
    {
        if (BGP4_PEER_STATE (pPeer) != BGP4_ESTABLISHED_STATE)
        {
            return BGP4_FALSE;
        }
        pCapsList = BGP4_PEER_RCVD_CAPS_LIST (pPeer);
        TMO_SLL_Scan (pCapsList, pTmpSupCap, tSupCapsInfo *)
        {
            if ((pTmpSupCap->SupCapability.u1CapCode ==
                 CAP_CODE_GRACEFUL_RESTART) &&
                (BGP4_PEER_RESTART_MODE (pPeer) != BGP4_RESTARTING_MODE) &&
                (BGP4_PEER_RECVD_EOR_MARKER (pPeer) != BGP4_TRUE))
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                               BGP4_TRC_FLAG, BGP4_GR_TRC,
                               BGP4_MOD_NAME,
                               "\t End of rib marker is not "
                               "received for this peer %s\r\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeer),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeer))));
                return BGP4_FALSE;
            }
            else
            {
                continue;
            }

        }                        /* End of scanning the capabability list of per peer */
    }                            /* End of TMO_SLL_Scan of BGP4_PEERENTRY_HEAD(u4CxtId) */
    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                   "\tEnd of rib marker is received from all "
                   "GR capable peers\r\n");
    return BGP4_TRUE;

}                                /* End of Bgp4GRCheckEORMarkerRecvd */

/*****************************************************************************/
/*                                                                           */
/* Function     : Bgp4GRKillProcess                                          */
/*                                                                           */
/* Description  : This function will simply de-init BGP, UnRegister BGP mibs */
/*                and kill the BGP task. This routine de-init BGP and kills  */
/*                the process                                                */
/*                                                                           */
/* Input        : NONE                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : NONE                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Bgp4GRKillProcess (VOID)
{
#if  (defined (SNMP_2_WANTED)) || (defined (SNMPV3_WANTED))
    UnRegisterFSBGP4 ();
    UnRegisterSTDBGP ();
    UnRegisterFSMPBG ();
#endif
    /* Deinit all memory allocations */
    Bgp4DeInit ();

    BGP4_TASK_INIT_STATUS = BGP4_FALSE;
    /* Indicate ISS module regarding module start */
    IssSetModuleSystemControl (BGP_MODULE_ID, MODULE_SHUTDOWN);

    OsixDeleteTask (SELF, (const UINT1 *) "Bgp");
    BGP4_TASK_ID = BGP4_INACTIVE;
}                                /* End of Bgp4GRKillProcess */

/*****************************************************************************/
/*  Function Name   :   Bgp4GRMarkEndOfRIBMarker                             */
/*  Description     :   This function will Mark the                          */
/*                      peerStatus.u1EndOfRIBMarkerReceived as true          */
/*  Input(s)        :   pPeerEntry                                           */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
PUBLIC VOID
Bgp4GRMarkEndOfRIBMarker (tBgp4PeerEntry * pPeerentry)    /* PeerEntry EOR 
                                                         * Marker rcvdstate  
                                                         * modified */
{
    /* Mark the End of rib marker received state as true for this peer */
    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                   BGP4_TRC_FLAG, BGP4_GR_TRC,
                   BGP4_MOD_NAME,
                   "\tPEER %s : End of RIB marker received.\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry))));
    BGP4_PEER_RECVD_EOR_MARKER (pPeerentry) = BGP4_TRUE;
    return;
}                                /* End of Bgp4GRMarkEndOfRIBMarker */

/*****************************************************************************/
/*  Function Name   :   Bgp4GRInit                                           */
/*  Description     :   This function will initialise GR related resources   */
/*                      for the given speaker                                */
/*  Input(s)        :                                                        */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
PUBLIC VOID
Bgp4GRInit (UINT4 u4CxtId)
{
    UINT1               u1FwdPath = 0;
    INT1                i1RetVal = 0;
    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                   "\t Initializing GR related params\n");
    /* Setting the AFI/SAFI support flag */
    BGP4_GR_AFI_SUPPORT (u4CxtId) = 0;

    /* Query RTM to get the address families preserved  across restart */
    i1RetVal =
        (INT1) Bgp4GRQueryRTMForwarding (u4CxtId, BGP4_INET_AFI_IPV4,
                                         &u1FwdPath);
    if (i1RetVal == BGP4_SUCCESS)
    {
        BGP4_GR_AFI_SUPPORT (u4CxtId) |= BGP4_INET_AFI_IPV4;
    }
#ifdef BGP4_IPV6_WANTED
    i1RetVal =
        (INT1) Bgp4GRQueryRTMForwarding (u4CxtId, BGP4_INET_AFI_IPV6,
                                         &u1FwdPath);
    if (i1RetVal == BGP4_SUCCESS)
    {
        BGP4_GR_AFI_SUPPORT (u4CxtId) |= BGP4_INET_AFI_IPV6;
    }
#endif /* BGP4_IPV6_WANTED */
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
    BGP4_GR_AFI_SUPPORT (u4CxtId) |= BGP4_INET_AFI_L2VPN;
#endif
#endif

    /* Setting the restart status of the speaker */
    BGP4_RESTART_STATUS (u4CxtId) = BGP4_GLB_RESTART_STATUS;

    /* Setting the restart time interval of the speaker */
    BGP4_RESTART_TIME_INTERVAL (u4CxtId) = BGP4_GLB_RESTART_TIME_INTERVAL;

    /* Setting the selection deferral time interval of the speaker */
    BGP4_SELECTION_DEFERAL_INTERVAL (u4CxtId) =
        BGP4_GLB_SELECTION_DEFERAL_INTERVAL;

    /* Setting the stale timer interval of the speaker */
    BGP4_STALE_PATH_INTERVAL (u4CxtId) = BGP4_GLB_STALE_PATH_INTERVAL;

    /* Setting the restart support of the speaker */
    BGP4_RESTART_SUPPORT (u4CxtId) = BGP4_GLB_RESTART_SUPPORT;

    /* Setting the restart mode of the speaker */
    BGP4_RESTART_MODE (u4CxtId) = BGP4_GLB_RESTART_MODE;

    BGP4_RESTART_REASON (u4CxtId) = BGP4_GLB_RESTART_REASON;

    /* Setting the GR admin status of the speaker */
    BGP4_GR_ADMIN_STATUS (u4CxtId) = BGP4_GLB_GR_ADMIN_STATUS;

    /* Setting the route selection flag */
    BGP4_ROUTE_SELECTION_FLAG (u4CxtId) = BGP4_TRUE;

    /* Initialize the selection defferal flag */
    BGP4_SELECTION_DEFERAL_FLAG (u4CxtId) = BGP4_FALSE;

#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
    /* Setting the EOVPLS flag */
    BGP4_EOVPLS_FLAG (u4CxtId) = BGP4_TRUE;
#endif
#endif

#ifdef MSR_WANTED
    /* Setting the restart reason of the speaker */
    if ((FlashFileExists (BGP4_CSR_CONFIG_FILE) == ISS_SUCCESS) &&
        (IssGetCsrRestoresFlag (BGP_MODULE_ID) == BGP4_TRUE))
    {
        BGP4_RESTART_EXIT_REASON (u4CxtId) = BGP4_GR_EXIT_INPROGRESS;
    }
    else
    {
#endif
        /* Setting the restart exit reason of the speaker */
        BGP4_RESTART_EXIT_REASON (u4CxtId) = BGP4_GR_EXIT_NONE;
        if (IssGetCsrRestoreFlag () == BGP4_TRUE)
        {
            Bgp4GRExitRestart (u4CxtId);
        }
#ifdef MSR_WANTED
    }
#endif

    return;
}                                /* End of Bgp4GRInit */

/****************************************************************************/
/*  Function Name   :    Bgp4GRFillRestartCap                               */
/*                                                                          */
/*  Description     :   This function will fill in open message thefollowig */
/*                      If the speaker is in restarting mode,               */
/*                      Fills the restart bit in the capability value       */
/*                      Set the forwarding bit of the address families      */
/*                      that has been preserved  across restart             */
/*                      If the speaker is in receiving mode,                */
/*                      Fills the restart value without setting the restart */
/*                      bit                                                 */
/*                      Set the forwarding bit of the address families whos */
/*                      forwarding plane can be preserved across restart.   */
/*  Input(s)        : pu1OpnMsgToFillCaps                                   */
/*  Output(s)       :   None                                                */
/*                  :                                                       */
/*  <OPTIONAL Fields>           :  None                                     */
/*  Global Variables Referred   :  None                                     */
/*  Global variables Modified   :  None                                     */
/*  Exceptions or Operating System Error Handling :                         */
/*  Use of Recursion            :  None                                     */
/*  Returns                     :                                           */
/****************************************************************************/
PUBLIC VOID
Bgp4GRFillRestartCap (UINT4 u4CxtId, UINT1 *pu1OpnMsgToFillCaps, tBgp4PeerEntry * pPeerEntry)    /* GR Capability 
                                                                                                 * message to be 
                                                                                                 * processed */
{
    UINT2               u2TmpMask = 0;

    MEMSET (pu1OpnMsgToFillCaps, 0, CAP_GR_CAP_LENGTH);

    if (Bgp4GRCheckGRCapability (u4CxtId) != BGP4_FALSE)
    {
        u2TmpMask = (UINT2) gBgpCxtNode[u4CxtId]->u4RestartInterval;
        PTR_ASSIGN2 (pu1OpnMsgToFillCaps, u2TmpMask);

        /* Set the restart bit if the peer is in restarting mode */
        if (Bgp4GRCheckRestartMode (u4CxtId) == BGP4_RESTARTING_MODE)
        {
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                           "\tIn restarting mode, framing open message "
                           "with restart bit set\n");
            *(pu1OpnMsgToFillCaps) = ((*pu1OpnMsgToFillCaps) |
                                      BGP4_GR_BIT_MASK);
        }
        if ((BGP4_PEER_SUP_ASAFI_MASK (pPeerEntry) &
             ((UINT4) CAP_NEG_IPV4_UNI_MASK)) == BGP4_INET_AFI_IPV4)
        {
            PTR_ASSIGN2 ((pu1OpnMsgToFillCaps + sizeof (UINT2)),
                         BGP4_INET_AFI_IPV4);
            *(pu1OpnMsgToFillCaps + sizeof (UINT4)) = BGP4_INET_SAFI_UNICAST;

            /* Fill the address family IPv4 */
            if ((BGP4_GR_AFI_SUPPORT (u4CxtId) & BGP4_INET_AFI_IPV4) ==
                BGP4_INET_AFI_IPV4)
            {
                /* Irrespective of the mode of the speaker, 
                 * fill the address families 
                 * supported */
                BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                               "\tSetting the Forwarding"
                               " bit for IPv4 address family\n");
                *(pu1OpnMsgToFillCaps + sizeof (UINT4) + sizeof (UINT1)) |=
                    BGP4_GR_BIT_MASK;
            }
        }

#ifdef BGP4_IPV6_WANTED
        if (BGP4_IPV6_AFI_FLAG (u4CxtId) == BGP4_CLI_AFI_ENABLED)
        {
            /* Fill the address family IPv6 */
            if (((BGP4_PEER_SUP_ASAFI_MASK (pPeerEntry) &
                  ((UINT4) CAP_NEG_IPV6_UNI_MASK)) == BGP4_INET_AFI_IPV6)
                &&
                ((BGP4_PEER_SUP_ASAFI_MASK (pPeerEntry) &
                  ((UINT4) CAP_NEG_IPV4_UNI_MASK)) == BGP4_INET_AFI_IPV4))

            {
                PTR_ASSIGN2 ((pu1OpnMsgToFillCaps + sizeof (UINT2) +
                              sizeof (UINT4)), BGP4_INET_AFI_IPV6);
                *(pu1OpnMsgToFillCaps + sizeof (UINT4) + sizeof (UINT4)) =
                    BGP4_INET_SAFI_UNICAST;

                if ((BGP4_GR_AFI_SUPPORT (u4CxtId) & BGP4_INET_AFI_IPV6) ==
                    BGP4_INET_AFI_IPV6)
                {
                    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC,
                                   BGP4_MOD_NAME,
                                   "\tSetting the Forwarding"
                                   " bit for IPv6 address family  \n");
                    *(pu1OpnMsgToFillCaps + sizeof (UINT4) + sizeof (UINT4) +
                      sizeof (UINT1)) |= BGP4_GR_BIT_MASK;
                }
            }
            else if ((BGP4_PEER_SUP_ASAFI_MASK (pPeerEntry) &
                      ((UINT4) CAP_NEG_IPV6_UNI_MASK)) == BGP4_INET_AFI_IPV6)
            {
                PTR_ASSIGN2 ((pu1OpnMsgToFillCaps +
                              sizeof (UINT2)), BGP4_INET_AFI_IPV6);
                *(pu1OpnMsgToFillCaps + sizeof (UINT4)) =
                    BGP4_INET_SAFI_UNICAST;

                if ((BGP4_GR_AFI_SUPPORT (u4CxtId) & BGP4_INET_AFI_IPV6) ==
                    BGP4_INET_AFI_IPV6)
                {
                    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC,
                                   BGP4_MOD_NAME,
                                   "\tSetting the Forwarding"
                                   " bit for IPv6 address family  \n");
                    *(pu1OpnMsgToFillCaps + sizeof (UINT4) + sizeof (UINT1)) |=
                        BGP4_GR_BIT_MASK;
                }

            }

        }

#endif /* BGP4_IPV6_WANTED */
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
        /* Fill the address family L2VPN-VPLS */
        PTR_ASSIGN2 ((pu1OpnMsgToFillCaps + sizeof (UINT2) +
                      sizeof (UINT4) + sizeof (UINT4)), BGP4_INET_AFI_L2VPN);
        *(pu1OpnMsgToFillCaps + sizeof (UINT4) + sizeof (UINT4) +
          sizeof (UINT4)) = BGP4_INET_SAFI_VPLS;

        if ((BGP4_GR_AFI_SUPPORT (u4CxtId) & BGP4_INET_AFI_L2VPN) ==
            BGP4_INET_AFI_L2VPN)
        {
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                           "\t %s\r\n", "Bgp4GRFillRestartCap : "
                           "Setting the Forwarding"
                           "bit for VPLS address family  \n");
            *(pu1OpnMsgToFillCaps + sizeof (UINT4) + sizeof (UINT4) +
              sizeof (UINT4) + sizeof (UINT1)) |= BGP4_GR_BIT_MASK;
        }
#endif
#endif

        /* Since only IPv4, IPv6, and VPLS address families support is currently 
         * provided, their 
         * attributes are filled. For further 
         * address families support, their 
         * attribute needs to be filled in the message. */
    }
    return;

}                                /* End of Bgp4GRFillRestartCap */

/*****************************************************************************/
/*  Function Name   :   Bgp4GlbGRInit                                        */
/*  Description     :   This function will initialise GR related global      */
/*                      resources for the given speaker                      */
/*  Input(s)        :                                                        */
/*  Output(s)       :   None                                                 */
/*  Returns         :   None                                                 */
/*****************************************************************************/
PUBLIC VOID
Bgp4GlbGRInit (VOID)
{
    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_GR_TRC, BGP4_MOD_NAME,
                   "\tInitializing GLB GR related params\n");
    /* Setting the AFI/SAFI support flag */
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
    BGP4_GLB_GR_AFI_SUPPORT |=
        (BGP4_INET_AFI_IPV4 | BGP4_INET_AFI_IPV6 | BGP4_INET_AFI_L2VPN);
#else
    BGP4_GLB_GR_AFI_SUPPORT |= (BGP4_INET_AFI_IPV4 | BGP4_INET_AFI_IPV6);
#endif
#else
    BGP4_GLB_GR_AFI_SUPPORT |= (BGP4_INET_AFI_IPV4 | BGP4_INET_AFI_IPV6);
#endif

    /* Setting the restart status of the speaker */
    BGP4_GLB_RESTART_STATUS = BGP4_GR_STATUS_NONE;

    /* Setting the restart time interval of the speaker */
    BGP4_GLB_RESTART_TIME_INTERVAL = BGP4_DEF_RESTART_TIME;

    /* Setting the selection deferral time interval of the speaker */
    BGP4_GLB_SELECTION_DEFERAL_INTERVAL = BGP4_DEF_SEL_DEF_TIME;

    /* Setting the stale timer interval of the speaker */
    BGP4_GLB_STALE_PATH_INTERVAL = BGP4_DEF_STALE_TIME;

    /* Setting the restart support of the speaker */
    BGP4_GLB_RESTART_SUPPORT = BGP4_GR_SUPPORT_PLANNED;

    /* Setting the restart mode of the speaker */
    BGP4_GLB_RESTART_MODE = BGP4_RECEIVING_MODE;

    BGP4_GLB_RESTART_REASON = BGP4_GR_REASON_RESTART;

    /* Setting the GR admin status of the speaker */
    BGP4_GLB_GR_ADMIN_STATUS = BGP4_ENABLE_GR;

    /* Setting the route selection flag */
    BGP4_GLB_ROUTE_SELECTION_FLAG = BGP4_TRUE;

    /* Setting the restart reason of the speaker */
#ifdef MSR_WANTED
    if ((FlashFileExists (BGP4_CSR_CONFIG_FILE) == ISS_SUCCESS) &&
        (IssGetCsrRestoreFlag () == BGP4_TRUE))
    {
        BGP4_GLB_RESTART_EXIT_REASON = BGP4_GR_EXIT_INPROGRESS;
    }
    else
    {
#endif
        /* Setting the restart exit reason of the speaker */
        BGP4_GLB_RESTART_EXIT_REASON = BGP4_GR_EXIT_NONE;
#ifdef MSR_WANTED
    }
#endif

    return;
}

/*****************************************************************************/
/*  Function Name   :   Bgp4GRMarkAllRoutesAsStale                           */
/*  Description     :   This function will mark the all the routes as stale  */
/*  Input(s)        :   u4Context                                            */
/*  Output(s)       :   None                                                 */
/*  Returns         :   None                                                 */
/*****************************************************************************/
PUBLIC VOID
Bgp4GRMarkAllRoutesAsStale (UINT4 u4Context)
{
    tBgp4PeerEntry     *pPeer = NULL;

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
    {
        /* When peer is moving from Standby to Active, peer restart interval
         * would not have been filled as the peer session was not established
         * in the standby state. Hence, set the default value of restart interval */
        BGP4_PEER_RESTART_INTERVAL (pPeer) =
            gBgpCxtNode[u4Context]->u4RestartInterval;
        Bgp4GRProcessIdleState (pPeer);
        BGP4_SET_PEER_CURRENT_STATE (pPeer, BGP4_PEER_READY);
    }
    return;
}

#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
/*****************************************************************************/
/*  Function Name   :   Bgp4GRSendOpenToRFLPeer                              */
/*  Description     :   This function will check that After GR, when BGP node*/
/*                      comes up, it will initiate session with CLIENT peers */
/*                      and will not initiate session towards NON-CLIENT     */
/*                      until EOR is received from all CLIENT.               */
/*  Input(s)        :   pPeerentry                                           */
/*  Output(s)       :   BGP4_TRUE, BGP4_FALSE                                */
/*  Returns         :   BGP4_TRUE, BGP4_FALSE                                */
/*****************************************************************************/
UINT1
Bgp4GRSendOpenToRFLPeer (tBgp4PeerEntry * pPeerentry)
{

    tBgp4PeerEntry     *pPeerEnt = NULL;
    UINT4               u4Context;

    u4Context = BGP4_PEER_CXT_ID (pPeerentry);

    if (BGP4_PEER_RFL_CLIENT (pPeerentry) == CLIENT)
    {
        /*If peer is CLIENT, Send open message to the peer */
        return BGP4_TRUE;
    }
    else
    {
        /* If peer is NON-CLIENT, check if the EOR is receieved from all the CLIENT
         * and then send open message to the NON-CLIENT peer*/

        TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeerEnt,
                      tBgp4PeerEntry *)
        {
            /*if Peer is non-client, no need to check EOR from the peer */
            if (BGP4_PEER_RFL_CLIENT (pPeerEnt) == NON_CLIENT)
            {
                continue;
            }
            else
            {
                /* If peer is CLIENT, check the EOR received from the peer.
                 * If EOR is not received, return false*/
                if (BGP4_PEER_RECVD_EOR_MARKER (pPeerEnt) == BGP4_TRUE)
                {
                    continue;
                }
                else
                {
                    return BGP4_FALSE;
                }
            }
        }
    }

    return BGP4_TRUE;
}
#endif
#endif /*End of VPLSADS_WANTED */
#endif /* _BGGRUTIL_C */
