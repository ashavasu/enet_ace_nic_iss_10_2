/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgcmfilt.c,v 1.22 2017/09/15 06:19:53 siva Exp $
 *
 * Description: This file contains Community Attribute processing
 *              functions.
 *
 *******************************************************************/
#ifndef _BGCMFILT_C
#define _BGCMFILT_C

#include "bgp4com.h"
#define CONF_COMM_STD 0
/*****************************************************************************/
/*    Function Name   : CommApplyRoutesInboundFilterPolicy                   */
/*                                                                           */
/*    Description     : Validates the input for community path attribute     */
/*                      present in the given route profile. Applies the      */
/*                      inbound filter policy if any configured and returns  */
/*                      the filtered results.                                */
/*                                                                           */
/*    Input(s)        : pFeasibleRoutes - pointer to the feasible            */
/*                      route profile, that is to be processed for           */
/*                      community path attribute.                            */
/*                                                                           */
/*    Output(s)       : None.                                                */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*  Returns         :  COMM_SUCCESS - If the route is not filtered           */
/*                     COMM_FAILURE - If the route is filtered               */
/*****************************************************************************/
INT4
CommApplyRoutesInboundFilterPolicy (tRouteProfile * pFeasibleRoute)
{
    tBgp4Info          *pRtInfo = NULL;
    UINT2               u2CommPathStartOffSet;
    INT1                i1CommInputFilterStatus;

    u2CommPathStartOffSet = 0;
    i1CommInputFilterStatus = ACCEPT_INCOMING_COMM;

    if ((BGP4_RT_PROTOCOL (pFeasibleRoute)) == BGP_ID)
    {
        pRtInfo = BGP4_RT_BGP_INFO (pFeasibleRoute);
        /* Check the presence of Community attribute. */
        if (BGP4_INFO_COMM_ATTR (pRtInfo) == NULL)
        {
            /* No community path attribute. Route not filtered */
            return COMM_SUCCESS;
        }
        else
        {
            /* Community attribute is present. */
            u2CommPathStartOffSet = 0;
        }
    }
    else
    {
        /* Protocol is not BGP_ID. Route not filtered */
        return COMM_SUCCESS;
    }

    /* Community path exists so start processing */
    i1CommInputFilterStatus =
        CommGetInputFilterStatus (pFeasibleRoute, u2CommPathStartOffSet);
    if (i1CommInputFilterStatus == ACCEPT_INCOMING_COMM)
    {
        /* Route not filtered. */
        return COMM_SUCCESS;
    }
    else
    {
        /* Route filtered out. */
        return COMM_FAILURE;
    }
}

/*****************************************************************************/
/*    Function Name   : CommFormListForInst                                  */
/*                                                                           */
/*    Description     : Validates the input for community path attribute.    */
/*                      Applies input community filtering and gives          */
/*                      the accepted list of route profiles.                 */
/*                                                                           */
/*    Input(s)        : pFeasibleRoutesList - pointer to the list of feasible*/
/*                      route profiles, that are to be processed for         */
/*                      community path attribute. It is an SLL of type       */
/*                      tLinkNode.                                           */
/*                                                                           */
/*                      pPeerInfo - Pointer to peer Information              */
/*                                                                           */
/*    Output(s)       : None.                                                */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*  Returns         :  COMM_SUCCESS - if the routes are not filtered.        */
/*                     COMM_FAILURE - if the routes are filtered out.        */
/*****************************************************************************/
INT4
CommFormListForInst (tTMO_SLL * pFeasibleRoutesList, tBgp4PeerEntry * pPeerInfo)
{
    tRouteProfile      *pFeasibleRoute = NULL;
    tLinkNode          *pLinkNode = NULL;
    INT4                i4RetVal;

    UNUSED_PARAM (pPeerInfo);
    /* It is necessary that the Feasible Route List contains list of routes
     * that have identical path attributes. Hence it is sufficient to
     * process one route and decide whether to filter the entire list
     * or not. */
    pLinkNode = ((tLinkNode *) TMO_SLL_First (pFeasibleRoutesList));
    if (pLinkNode == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                  BGP4_MOD_NAME,
                  "\tCommFormListForInst() : RECEIVED ZERO FEASIBLE "
                  "ROUTES\n");
        return COMM_FAILURE;
    }

    pFeasibleRoute = pLinkNode->pRouteProfile;
    i4RetVal = CommApplyRoutesInboundFilterPolicy (pFeasibleRoute);
    if (i4RetVal == COMM_SUCCESS)
    {
        /* Route is not filtered out. All routes in the list also shall
         * not be filtered since all are received in the same update
         * message. */
        return COMM_SUCCESS;
    }
    else
    {
        /* Route is filtered out. Set RT_FILTERED flag and continue.
         * The handling of the filtered route is taken care in the
         * Ribin Processing. */
        TMO_SLL_Scan (pFeasibleRoutesList, pLinkNode, tLinkNode *)
        {
            pFeasibleRoute = pLinkNode->pRouteProfile;
            BGP4_RT_SET_FLAG (pFeasibleRoute, BGP4_RT_FILTERED_INPUT);
        }

        return COMM_FAILURE;
    }
}

/*****************************************************************************/
/*    Function Name   : CommApplyOutboundFilterPolicy                        */
/*    Description     : Applies output community filtering policy over the   */
/*                      given input route.                                   */
/*                                                                           */
/*    Input(s)        : pRtProfile - Pointer to the route profile            */
/*                                                                           */
/*                      pPeerInfo - Pointer to peer Information              */
/*                                                                           */
/*    Output(s)       : pRtProfile - Updated for whether the route is        */
/*                                   filtered or not. If the route is        */
/*                                   filtered then BGP4_RT_FILTERED_OUTPUT   */
/*                                   flag is set. If the route is not        */
/*                                   filtered but not selected for advt,     */
/*                                   then BGP4_RT_DONT_ADVT flag is set.     */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*  Returns         :  COMM_SUCCESS or COMM_FAILURE                          */
/*****************************************************************************/
INT4
CommApplyOutboundFilterPolicy (tRouteProfile * pRtProfile,
                               tBgp4PeerEntry * pPeerInfo)
{
    tBgp4Info          *pRtInfo = NULL;
    UINT2               u2CommPathStartOffSet = 0;
    INT1                i1CommOutputFilterStatus;

    UNUSED_PARAM (pPeerInfo);

    if ((BGP4_RT_PROTOCOL (pRtProfile)) == BGP_ID)
    {
        pRtInfo = BGP4_RT_BGP_INFO (pRtProfile);

        if (BGP4_INFO_COMM_ATTR (pRtInfo) != NULL)
        {
            /* Attribute is present, so apply comm outgoing  filter */
            i1CommOutputFilterStatus =
                CommGetOutputFilterStatus (pRtProfile, u2CommPathStartOffSet);
            switch (i1CommOutputFilterStatus)
            {
                case ADVERTISE_OUTGOING_COMM:
                    /* The routes can be advertised. */
                    break;

                case FILTER_OUTGOING_COMM:
                    /* Route should be filtered */
                    BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_FILTERED_OUTPUT);
                    break;

                case IGNORE_OUTGOING_COMM:
                    /* Route is found to be ignored. Set the flags
                     * so that route is not advertised. */
                    BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_FILTERED_OUTPUT);
                    BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_DONT_ADVT);
                    break;

                default:
                    break;
            }
        }
        else
        {
            /* Community path attribute is NOT present. Route is not
             * filtered out by Community outgoing filter policy. */
        }
    }

    return COMM_SUCCESS;
}

/*****************************************************************************/
/*    Function Name   : CommGetInputFilterStatus                             */
/*                                                                           */
/*    Description     : Gets the Input Filter Status of the route            */
/*                                                                           */
/*    Input(s)        : pFeasibleRoute - pointer to the route profile that is*/
/*                      to be processed.                                     */
/*                                                                           */
/*    Output(s)       : u2CommPathStartOffSet - Holds the offset,            */
/*                      from where  the community path attribute starts.     */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*  Returns         :                                                        */
/*       ACCEPT_INCOMING_COMM /  DENY_INCOMING_COMM /IGNORE_INCOMING_COMM    */
/*****************************************************************************/
INT1
CommGetInputFilterStatus (tRouteProfile * pFeasibleRoute,
                          UINT2 u2CommPathStartOffSet)
{
    return (ApplyIncomingCommFilter (pFeasibleRoute, u2CommPathStartOffSet));
}

/*****************************************************************************/
/*    Function Name   : ApplyIncomingCommFilter                              */
/*                                                                           */
/*    Description     : Gets the Input Filter Status of the route            */
/*                      after applying community(s) input filter policy.     */
/*    Input(s)        : pFeasibleRoute - pointer to the route profile that is*/
/*                      to be processed.                                     */
/*                                                                           */
/*    Output(s)       : u2CommPathStartOffSet - Holds the offset,            */
/*                      from where  the community path attribute starts.     */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred : COMM_INPUT_FILTER_TBL                        */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*  Returns         :                                                        */
/*     ACCEPT_INCOMING_COMM /  DENY_INCOMING_COMM                            */
/*****************************************************************************/
INT1
ApplyIncomingCommFilter (const tRouteProfile * pFeasibleRoute,
                         UINT2 u2CommPathStartOffSet)
{
    tBgp4Info          *pRouteInfo = NULL;
    tCommFilterInfo    *pCommFilterInfo = NULL;
    UINT4               u4TempComm = 0;
    UINT4               u4Index = 0;
    tCommFilterInfo     CommFilterInfo;
    UINT1               u1CommInputFilterStatus;
    UINT1               u1TempInputFilterStatus;

    UNUSED_PARAM (u2CommPathStartOffSet);
    u1CommInputFilterStatus = COMM_INVALID_POLICY;
    u1TempInputFilterStatus = COMM_INVALID_POLICY;

    pRouteInfo = BGP4_RT_BGP_INFO (pFeasibleRoute);

    for (u4Index = 0; u4Index < BGP4_INFO_COMM_COUNT (pRouteInfo); u4Index++)
    {
        PTR_FETCH4 (u4TempComm, (BGP4_INFO_COMM_ATTR_VAL (pRouteInfo) +
                                 (u4Index * COMM_VALUE_LEN)));
        /* No Filter Policies are applied for Well-Known Communities. */
        if ((u4TempComm == NO_EXPORT) || (u4TempComm == NO_EXPORT_SUBCONFED) ||
            (u4TempComm == NO_ADVERTISE))
        {
            /* WELL-Known Comminity. */
            continue;
        }
        u1TempInputFilterStatus = COMM_INVALID_POLICY;
        CommFilterInfo.u4Comm = u4TempComm;
        CommFilterInfo.u4Context = BGP4_RT_CXT_ID (pFeasibleRoute);
        pCommFilterInfo = BGP4_RB_TREE_GET (COMM_INPUT_FILTER_TBL,
                                            (tBgp4RBElem *) (&CommFilterInfo));
        if (pCommFilterInfo != NULL)
        {
            u1TempInputFilterStatus = (UINT1) pCommFilterInfo->i1FilterPolicy;
        }

        if (u1TempInputFilterStatus == DENY_INCOMING_COMM)
        {
            return ((INT1) u1TempInputFilterStatus);
        }
        u1CommInputFilterStatus =
            u1CommInputFilterStatus & u1TempInputFilterStatus;
    }

    if (u1CommInputFilterStatus == COMM_INVALID_POLICY)
    {
        return ACCEPT_INCOMING_COMM;
    }

    return ((INT1) u1CommInputFilterStatus);
}

/*****************************************************************************/
/*    Function Name   : CommGetOutputFilterStatus                            */
/*                                                                           */
/*    Description     : Gets the Output Filter Status of the route           */
/*                                                                           */
/*    Input(s)        : pBestRoute - pointer to the route profile that is    */
/*                      to be processed.                                     */
/*                                                                           */
/*    Output(s)       : u2CommPathStartOffSet - Holds the offset,            */
/*                      from where  the community path attribute starts.     */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*  Returns         :                                                        */
/*   ADVERTISE_OUTGOING_COMM /  FILTER_OUTGOING_COMM /IGNORE_OUTGOING_COMM   */
/*****************************************************************************/
INT1
CommGetOutputFilterStatus (tRouteProfile * pBestRoute,
                           UINT2 u2CommPathStartOffSet)
{
    return (ApplyOutgoingCommFilter (pBestRoute, u2CommPathStartOffSet));
}

/*****************************************************************************/
/*    Function Name   : ApplyOutgoingCommFilter                              */
/*                                                                           */
/*    Description     : Gets the Output Filter Status of the route           */
/*                      after applying community(s) input filter policy.     */
/*                                                                           */
/*    Input(s)        : pBestRoute - pointer to the route profile that is    */
/*                      to be processed.                                     */
/*                                                                           */
/*    Output(s)       : u2CommPathStartOffSet - Holds the offset,            */
/*                      from where  the community path attribute starts.     */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred : COMM_OUTPUT_FILTER_TBL                       */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*  Returns         :                                                        */
/*     ADVERTISE_OUTGOING_COMM /  FILTER_OUTGOING_COMM                       */
/*****************************************************************************/
INT1
ApplyOutgoingCommFilter (const tRouteProfile * pBestRoute,
                         UINT2 u2CommPathStartOffSet)
{
    tBgp4Info          *pRouteInfo = NULL;
    tCommFilterInfo    *pCommFilterInfo = NULL;
    UINT4               u4TempComm = 0;
    UINT4               u4Index = 0;
    tCommFilterInfo     CommFilterInfo;
    UINT1               u1TempOutputFilterStatus;
    UINT1               u1CommOutputFilterStatus;

    UNUSED_PARAM (u2CommPathStartOffSet);
    u1CommOutputFilterStatus = COMM_INVALID_POLICY;
    u1TempOutputFilterStatus = COMM_INVALID_POLICY;

    pRouteInfo = BGP4_RT_BGP_INFO (pBestRoute);

    for (u4Index = 0; u4Index < BGP4_INFO_COMM_COUNT (pRouteInfo); u4Index++)
    {
        PTR_FETCH4 (u4TempComm,
                    (BGP4_INFO_COMM_ATTR_VAL (pRouteInfo) +
                     (u4Index * COMM_VALUE_LEN)));

        /* No Filter Policies are applied for Well-Known Communities. */
        if ((u4TempComm == NO_EXPORT) || (u4TempComm == NO_EXPORT_SUBCONFED) ||
            (u4TempComm == NO_ADVERTISE))
        {
            /* WELL-Known Comminity. */
            continue;
        }
        u1TempOutputFilterStatus = COMM_INVALID_POLICY;
        CommFilterInfo.u4Comm = u4TempComm;
        CommFilterInfo.u4Context = BGP4_RT_CXT_ID (pBestRoute);
        pCommFilterInfo = BGP4_RB_TREE_GET (COMM_OUTPUT_FILTER_TBL,
                                            (tBgp4RBElem *) (&CommFilterInfo));
        if (pCommFilterInfo != NULL)
        {
            u1TempOutputFilterStatus = (UINT1) pCommFilterInfo->i1FilterPolicy;
        }
        if (u1TempOutputFilterStatus == FILTER_OUTGOING_COMM)
        {
            return ((INT1) u1TempOutputFilterStatus);
        }
        u1CommOutputFilterStatus =
            u1CommOutputFilterStatus & u1TempOutputFilterStatus;

    }
    if (u1CommOutputFilterStatus == COMM_INVALID_POLICY)
    {
        return ADVERTISE_OUTGOING_COMM;
    }

    return ((INT1) u1CommOutputFilterStatus);
}

/*****************************************************************************/
/*    Function Name   : IsCommWellKnown                                      */
/*                                                                           */
/*    Description     : Checks if well known community is associated with    */
/*                      the route.If only one well known community is present*/
/*                      returns TRUE and outputs the value in u4WellKnownComm*/
/*                      If multiple community is present returns TRUE        */
/*                      and outputs the value in u4WellKnownComm as below.   */
/*                      NO_ADVERTISE in the presence of any other comms,     */
/*                      NO_EXPORT in the presence of any other communities   */
/*                      except NO_ADVERTISE                                  */
/*                      NO_EXPORT_SUBCONFED in the presence of any other     */
/*                      communities except NO_ADVERTISE and NO_EXPORT.       */
/*                                                                           */
/*    Input(s)        : pRoute - pointer to the route profile that is to be  */
/*                      processed.                                           */
/*                      u2CommPathStartOffSet - the offset at which  the     */
/*                      Community path attribute starts.                     */
/*                                                                           */
/*    Output(s)       : pu4WellKnownComm - pointer to well known community   */
/*                      as described.                                        */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*                                                                           */
/*  Returns         :  COMM_TRUE / COMM_FALSE / COMM_IGNORE                  */
/*****************************************************************************/
INT1
IsCommWellKnown (const tRouteProfile * pRoute,
                 UINT2 u2CommPathStartOffSet, UINT4 *pu4WellKnownComm)
{
    tBgp4Info          *pRouteInfo = NULL;
    UINT4               u4TempComm = 0;
    UINT4               u4Index = 0;
    INT2                i2NoAdvertiseCommCt = 0;
    INT2                i2NoExportCommCt = 0;
    INT2                i2NoExportSubCommCt = 0;
    INT1                i1IsWellKnownCommExists = COMM_FALSE;

    UNUSED_PARAM (u2CommPathStartOffSet);

    *pu4WellKnownComm = 0;
    pRouteInfo = BGP4_RT_BGP_INFO (pRoute);

    for (u4Index = 0; u4Index < BGP4_INFO_COMM_COUNT (pRouteInfo); u4Index++)
    {
        PTR_FETCH4 (u4TempComm, (BGP4_INFO_COMM_ATTR_VAL (pRouteInfo) +
                                 (u4Index * COMM_VALUE_LEN)));

        switch (u4TempComm)
        {
            case (NO_EXPORT):
                if ((*pu4WellKnownComm != NO_ADVERTISE) &&
                    (*pu4WellKnownComm != NO_EXPORT_SUBCONFED))
                {
                    *pu4WellKnownComm = u4TempComm;
                    i1IsWellKnownCommExists = COMM_TRUE;
                }
                i2NoExportCommCt++;
                break;
            case (NO_ADVERTISE):
                *pu4WellKnownComm = u4TempComm;
                i1IsWellKnownCommExists = COMM_TRUE;
                i2NoAdvertiseCommCt++;
                break;
            case (NO_EXPORT_SUBCONFED):
                if (*pu4WellKnownComm != NO_ADVERTISE)
                {
                    i1IsWellKnownCommExists = COMM_TRUE;
                    *pu4WellKnownComm = u4TempComm;
                }
                i2NoExportSubCommCt++;
                break;
            default:
                break;
        }
    }

    return i1IsWellKnownCommExists;
}

#ifdef ROUTEMAP_WANTED
/**************************************************************************/
/*   Function Name   : Bgp4UpdateRouteInfo                                */
/*   Description     : This function will update the routeinfo            */
/*                      if the Set applied.                               */
/*   Input(s)        : pInfo         - ptr to RouteMapinfo after applying 
 *                                        Set rules                       */
/*                     pRtProfile    - ptr to the route need to update
 *                     u4Dir         - Direction (in / out)               */
/*                                                                        */
/*   Output(s)       : None.                                              */
/*                                                                        */
/*   Return Value    : BGP4_FAILURE / BGP4_SUCCESS                        */
/**************************************************************************/
INT1
Bgp4UpdateRouteInfo (tRtMapInfo * pInfo,
                     tRouteProfile * pRtProfile, UINT4 u4Dir)
{
    tCommunity         *pComm = NULL;
    tExtCommunity      *pEcomm = NULL;
    UINT1              *pu1CommVal = NULL;
    tBgp4Info          *pDestBgp4Info = NULL;
    tBgp4Info          *pDupBgp4Info = NULL;
    tBgp4Info          *pOldBgp4Info = NULL;
    tAsPath            *pSrcAspath = NULL;
    UINT1              *pu1AsNos = NULL;
    UINT4               u4LocalAs = 0;
    UINT2               u2Count = 0;
    UINT1               u1Ext = 0;
    UINT1               u1Overwrite = 0;
    UINT1               u1AsCnt = 0;
    UINT1               u1CommCount = 0;
    UINT2               u2CommunityCnt = 0;
    UINT4               au4Community[2 * RMAP_BGP_MAX_COMM];
    UINT4               u4Comm = 0;
    UINT4               u4Offset = 0;
    MEMSET (au4Community, 0, RMAP_BGP_MAX_COMM);
    for (u1CommCount = 0; u1CommCount < pInfo->u1CommunityCnt; u1CommCount++)
    {
        if (pInfo->au4Community[u1CommCount] == RMAP_COMM_INTERNET)
        {
            pInfo->au4Community[u1CommCount] = 0;
        }
        pInfo->au4Community[u1CommCount] =
            OSIX_HTONL (pInfo->au4Community[u1CommCount]);
        au4Community[u1CommCount] = pInfo->au4Community[u1CommCount];
    }
    u2CommunityCnt = pInfo->u1CommunityCnt;
    if ((pInfo->u1Addflag) == RMAP_BGP_COMMUNITY_ADDITIVE)
    {
        if ((BGP4_INFO_COMM_ATTR (BGP4_RT_BGP_INFO (pRtProfile))) != NULL)
        {
            u2Count = BGP4_INFO_COMM_COUNT (BGP4_RT_BGP_INFO (pRtProfile));
            while (u2Count != 0)
            {
                PTR_FETCH_4 (u4Comm,
                             (BGP4_INFO_COMM_ATTR_VAL
                              (BGP4_RT_BGP_INFO (pRtProfile)) + u4Offset));
                if (u2CommunityCnt <= (2 * RMAP_BGP_MAX_COMM))
                {
                    au4Community[u2CommunityCnt++] = u4Comm;
                }
                u4Offset += COMM_VALUE_LEN;
                u2Count--;
            }
        }
    }
    Bgp4FindDupCommunity (au4Community, &u2CommunityCnt);
    if (u4Dir == BGP4_INCOMING)
    {
        if (pInfo->u4LocalPref != 0)
        {
            BGP4_RT_LOCAL_PREF (pRtProfile) = pInfo->u4LocalPref;
            BGP4_RT_BGP_INFO (pRtProfile)->u4RecdLocalPref = pInfo->u4LocalPref;
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tAfter applying routemap local preference value %d is updated "
                           "for the incoming route %s.\n",
                           pInfo->u4LocalPref,
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
        }
        if (pInfo->i4Metric != 0)
        {
            if (((BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pRtProfile)))
                 & BGP4_ATTR_MED_MASK) != BGP4_ATTR_MED_MASK)
            {
                pOldBgp4Info = BGP4_RT_BGP_INFO (pRtProfile);
                pDupBgp4Info = Bgp4DuplicateBgp4Info (pRtProfile->pRtInfo);
                if (pDupBgp4Info != NULL)
                {
                    BGP4_LINK_INFO_TO_PROFILE (pDupBgp4Info, pRtProfile);
                    Bgp4DshReleaseBgpInfo (pOldBgp4Info);
                }
                BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pRtProfile))
                    |= BGP4_ATTR_MED_MASK;
            }
            BGP4_RT_MED (pRtProfile) = (UINT4) pInfo->i4Metric;
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tAfter applying routemap metric value %d is updated "
                           "for the incoming route %s.\n",
                           pInfo->i4Metric,
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
        }
        if (pInfo->i4Origin != 0)
        {
            BGP4_RT_BGP_INFO (pRtProfile)->u1Origin =
                (UINT1) ((pInfo->i4Origin) - 1);
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tAfter applying routemap origin value %s is updated "
                           "for the incoming route %s.\n",
                           Bgp4PrintCodeName ((BGP4_RT_BGP_INFO (pRtProfile)->
                                               u1Origin),
                                              BGP4_ORIGIN_CODE_NAME),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
        }
        if (pInfo->u1CommunityCnt != 0)
        {
            if (BGP4_INFO_REF_COUNT (pRtProfile->pRtInfo))

            {
                pOldBgp4Info = BGP4_RT_BGP_INFO (pRtProfile);
                pDupBgp4Info = Bgp4DuplicateBgp4Info (pRtProfile->pRtInfo);
                if (pDupBgp4Info != NULL)
                {
                    BGP4_LINK_INFO_TO_PROFILE (pDupBgp4Info, pRtProfile);
                }
                Bgp4DshReleaseBgpInfo (pOldBgp4Info);
            }

            if (pInfo->au4Community[CONF_COMM_STD] == RMAP_COMM_NONE)
            {
                if (BGP4_INFO_COMM_ATTR (BGP4_RT_BGP_INFO (pRtProfile)) != NULL)
                {
                    if (BGP4_INFO_COMM_ATTR_VAL (BGP4_RT_BGP_INFO (pRtProfile))
                        != NULL)
                    {
                        ATTRIBUTE_NODE_FREE (BGP4_INFO_COMM_ATTR_VAL
                                             (BGP4_RT_BGP_INFO (pRtProfile)));
                    }
                    if (BGP4_INFO_COMM_ATTR (BGP4_RT_BGP_INFO (pRtProfile))
                        != NULL)
                    {
                        COMMUNITY_NODE_FREE (BGP4_INFO_COMM_ATTR
                                             (BGP4_RT_BGP_INFO (pRtProfile)));
                    }
                    if (pRtProfile->pRtInfo)
                    {
                        (BGP4_INFO_ATTR_FLAG (pRtProfile->pRtInfo) &=
                         (~BGP4_ATTR_COMM_MASK));
                    }
                }
            }
            else
            {
                if (pInfo->u1Addflag == RMAP_BGP_COMMUNITY_ADDITIVE)
                {
                    if (BGP4_RT_BGP_INFO (pRtProfile)->pCommunity != NULL)
                    {
                        pu1CommVal =
                            BGP4_INFO_COMM_ATTR_VAL (BGP4_RT_BGP_INFO
                                                     (pRtProfile));
                        u2Count =
                            BGP4_INFO_COMM_COUNT (BGP4_RT_BGP_INFO
                                                  (pRtProfile));
                        if (u2Count + pInfo->u1CommunityCnt >
                            MAX_BGP_ATTR_LEN / COMM_VALUE_LEN)
                        {
                            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                           BGP4_CONTROL_PATH_TRC | BGP4_FDB_TRC
                                           | BGP4_ALL_FAILURE_TRC,
                                           BGP4_MOD_NAME,
                                           "\tCommunity value count %d is exceeds the attribute values %d.\n",
                                           (u2Count + pInfo->u1CommunityCnt),
                                           (MAX_BGP_ATTR_LEN / COMM_VALUE_LEN));
                            return (BGP4_FAILURE);
                        }
                        MEMCPY (pu1CommVal,
                                &(au4Community),
                                (u2CommunityCnt * COMM_VALUE_LEN));
                        BGP4_INFO_COMM_COUNT (BGP4_RT_BGP_INFO (pRtProfile)) =
                            u2CommunityCnt;
                        BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pRtProfile)) |=
                            BGP4_ATTR_COMM_MASK;
                    }
                    else
                    {
                        u1Overwrite = 1;
                    }
                }
                else
                {
                    u1Overwrite = 1;
                }
                if (u1Overwrite == 1)
                {
                    if (BGP4_INFO_COMM_ATTR (BGP4_RT_BGP_INFO (pRtProfile)) !=
                        NULL)
                    {
                        if (BGP4_INFO_COMM_ATTR_VAL
                            (BGP4_RT_BGP_INFO (pRtProfile)) != NULL)
                        {
                            ATTRIBUTE_NODE_FREE (BGP4_INFO_COMM_ATTR_VAL
                                                 (BGP4_RT_BGP_INFO
                                                  (pRtProfile)));
                        }
                        COMMUNITY_NODE_FREE (BGP4_INFO_COMM_ATTR
                                             (BGP4_RT_BGP_INFO (pRtProfile)));
                    }
                    COMMUNITY_NODE_CREATE (BGP4_INFO_COMM_ATTR
                                           (BGP4_RT_BGP_INFO (pRtProfile)));
                    pComm = BGP4_INFO_COMM_ATTR (BGP4_RT_BGP_INFO (pRtProfile));
                    if (pComm == NULL)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tMemory Allocation for Community Attribute "
                                  "FAILED!!!\n");
                        gu4BgpDebugCnt[MAX_BGP_COMMUNITY_NODE_SIZING_ID]++;
                        return (BGP4_FAILURE);
                    }
                    ATTRIBUTE_NODE_CREATE (pu1CommVal);
                    if (pu1CommVal == NULL)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tMemory Allocation for Community Attribute "
                                  "FAILED!!!\n");
                        gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
                        return (BGP4_FAILURE);
                    }
                    MEMCPY (pu1CommVal, &(au4Community),
                            (u2CommunityCnt * COMM_VALUE_LEN));
                    pComm->pu1CommVal = pu1CommVal;
                    pComm->u2CommCnt = u2CommunityCnt;
                    pComm->u1Flag = 0;
                    BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pRtProfile)) |=
                        BGP4_ATTR_COMM_MASK;
                }
            }
        }
        if (pInfo->u2Weight != 0)
        {
            BGP4_INFO_WEIGHT (BGP4_RT_BGP_INFO (pRtProfile)) = pInfo->u2Weight;
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tAfter applying routemap weight %d is updated "
                           "for the incoming route %s.\n",
                           pInfo->u2Weight,
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
        }
        if (pInfo->u2ExtCommId != 0)
        {
            if ((BGP4_INFO_ECOMM_ATTR (BGP4_RT_BGP_INFO (pRtProfile))) == NULL)
            {
                EXT_COMMUNITY_NODE_CREATE ((BGP4_INFO_ECOMM_ATTR
                                            (BGP4_RT_BGP_INFO (pRtProfile))));
                pEcomm = (BGP4_INFO_ECOMM_ATTR (BGP4_RT_BGP_INFO (pRtProfile)));
                if (pEcomm == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG,
                              BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                              BGP4_MOD_NAME,
                              "\tMemory Allocation for Extended Community Attribute "
                              "FAILED!!!\n");
                    gu4BgpDebugCnt[MAX_BGP_EXT_COMM_NODE_SIZING_ID]++;
                    return (BGP4_FAILURE);
                }
                ATTRIBUTE_NODE_CREATE (pEcomm->pu1EcommVal);
                if (pEcomm->pu1EcommVal == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG,
                              BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                              BGP4_MOD_NAME,
                              "\tMemory Allocation for Community Attribute "
                              "FAILED!!!\n");
                    gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
                    return (BGP4_FAILURE);
                }
            }
            BGP4_INFO_ECOMM_ATTR_COST_VAL (BGP4_RT_BGP_INFO (pRtProfile)) =
                pInfo->u4ExtCommCost;
            BGP4_INFO_ECOMM_ATTR_COST_COMM (BGP4_RT_BGP_INFO (pRtProfile)) =
                pInfo->u2ExtCommId;
            BGP4_INFO_ECOMM_ATTR_COST_POI (BGP4_RT_BGP_INFO (pRtProfile)) =
                pInfo->u2ExtCommPOI;
            BGP4_INFO_ECOMM_COUNT (BGP4_RT_BGP_INFO (pRtProfile)) = 1;
            BGP4_INFO_ECOMM_ATTR_COST_FLAG (BGP4_RT_BGP_INFO (pRtProfile)) =
                BGP4_TRUE;
        }
        if (pInfo->u1AsCnt != 0)
        {
            pDestBgp4Info = BGP4_RT_BGP_INFO (pRtProfile);
            TMO_SLL_Scan (BGP4_INFO_ASPATH (pDestBgp4Info), pSrcAspath,
                          tAsPath *)
            {
                u1AsCnt = BGP4_ASPATH_LEN (pSrcAspath);
                pu1AsNos = BGP4_ASPATH_NOS (pSrcAspath);
                while (u1AsCnt > 0)
                {
                    u4LocalAs = OSIX_NTOHS (*(UINT2 *) (VOID *) pu1AsNos);
                    if (u4LocalAs == pInfo->u4MatchAs)
                    {
                        u4LocalAs = pInfo->u4NextHopAS;
                        PTR_ASSIGN2 (pu1AsNos, u4LocalAs);
                        u1Ext = 1;
                        break;
                    }
                    u1AsCnt--;
                    pu1AsNos += 2;
                }
                if (u1Ext == 1)
                    break;
            }

        }
    }
    else
    {
        BGP4_RT_OUT_BGP4_INFO (pRtProfile) = Bgp4MemAllocateBgp4info (0);
        /*   This BgpInfo Node will be used for advertising to the perticular peer .  */
        if (pRtProfile->pOutRtInfo == NULL)
        {
            return BGP4_FAILURE;
        }
        Bgp4InitBgp4info (BGP4_RT_OUT_BGP4_INFO (pRtProfile));
        BGP4_INFO_REF_COUNT (BGP4_RT_OUT_BGP4_INFO (pRtProfile))++;
        if (pInfo->u4LocalPref != 0)
        {
            BGP4_RT_OUT_BGP4_INFO (pRtProfile)->u4RecdLocalPref =
                pInfo->u4LocalPref;
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tAfter applying routemap local preference value %d is updated "
                           "for the outgoing route %s.\n",
                           pInfo->u4LocalPref,
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
        }
        if (pInfo->i4Metric != 0)
        {
            BGP4_RT_OUT_BGP4_INFO (pRtProfile)->u4SendMED =
                (UINT4) pInfo->i4Metric;
            BGP4_INFO_ATTR_FLAG (BGP4_RT_OUT_BGP4_INFO (pRtProfile)) |=
                BGP4_ATTR_SEND_MED_MASK;
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tAfter applying routemap metric value %d is updated "
                           "for the outgoing route %s.\n",
                           pInfo->i4Metric,
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
        }
        if (pInfo->i4Origin != 0)
        {
            BGP4_RT_OUT_BGP4_INFO (pRtProfile)->u1Origin =
                (UINT1) pInfo->i4Origin;
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tAfter applying routemap origin %s is updated "
                           "for the outgoing route %s.\n",
                           Bgp4PrintCodeName ((UINT1) (pInfo->i4Origin),
                                              BGP4_ORIGIN_CODE_NAME),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
        }
        if (pInfo->u1CommunityCnt != 0)
        {
            if (pInfo->au4Community[CONF_COMM_STD] == RMAP_COMM_NONE)
            {
                COMMUNITY_NODE_CREATE (BGP4_INFO_COMM_ATTR
                                       (BGP4_RT_OUT_BGP4_INFO (pRtProfile)));
                pComm =
                    BGP4_INFO_COMM_ATTR (BGP4_RT_OUT_BGP4_INFO (pRtProfile));
                if (pComm == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG,
                              BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                              BGP4_MOD_NAME,
                              "\tMemory Allocation for Community Attribute "
                              "FAILED!!!\n");
                    gu4BgpDebugCnt[MAX_BGP_COMMUNITY_NODE_SIZING_ID]++;
                    return (BGP4_FAILURE);
                }
                BGP4_INFO_COMM_ATTR_VAL (BGP4_RT_OUT_BGP4_INFO (pRtProfile)) =
                    NULL;
                BGP4_INFO_COMM_COUNT (BGP4_RT_OUT_BGP4_INFO (pRtProfile)) = 0;
            }
            else
            {
                COMMUNITY_NODE_CREATE (BGP4_INFO_COMM_ATTR
                                       (BGP4_RT_OUT_BGP4_INFO (pRtProfile)));
                pComm =
                    BGP4_INFO_COMM_ATTR (BGP4_RT_OUT_BGP4_INFO (pRtProfile));
                if (pComm == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG,
                              BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                              BGP4_MOD_NAME,
                              "\tMemory Allocation for Community Attribute "
                              "FAILED!!!\n");
                    gu4BgpDebugCnt[MAX_BGP_COMMUNITY_NODE_SIZING_ID]++;
                    return (BGP4_FAILURE);
                }
                ATTRIBUTE_NODE_CREATE (pu1CommVal);
                if (pu1CommVal == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG,
                              BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                              BGP4_MOD_NAME,
                              "\tMemory Allocation for Community Attribute "
                              "FAILED!!!\n");
                    gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
                    return (BGP4_FAILURE);
                }
                pComm->pu1CommVal = pu1CommVal;
                pComm->u1Flag = 0;
                if (BGP4_INFO_COMM_ATTR (BGP4_RT_BGP_INFO (pRtProfile)) != NULL)
                {
                    pu1CommVal =
                        BGP4_INFO_COMM_ATTR_VAL (BGP4_RT_BGP_INFO (pRtProfile));
                    u2Count =
                        BGP4_INFO_COMM_COUNT (BGP4_RT_BGP_INFO (pRtProfile));
                }
                else
                {
                    pu1CommVal = NULL;
                }
                if (pInfo->u1Addflag == RMAP_BGP_COMMUNITY_ADDITIVE
                    && pu1CommVal != NULL)
                {
                    pu1CommVal =
                        BGP4_INFO_COMM_ATTR_VAL (BGP4_RT_OUT_BGP4_INFO
                                                 (pRtProfile));
                    if (u2CommunityCnt > MAX_BGP_ATTR_LEN / COMM_VALUE_LEN)
                    {
                        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC | BGP4_FDB_TRC |
                                       BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                                       "\tCommunity value count %d is exceeds the attribute values %d.\n",
                                       u2CommunityCnt,
                                       (MAX_BGP_ATTR_LEN / COMM_VALUE_LEN));
                        return (BGP4_FAILURE);
                    }
                    MEMCPY (pu1CommVal,
                            (au4Community), (u2CommunityCnt * COMM_VALUE_LEN));
                    BGP4_INFO_COMM_COUNT (BGP4_RT_OUT_BGP4_INFO (pRtProfile)) =
                        u2CommunityCnt;
                    BGP4_INFO_ATTR_FLAG (BGP4_RT_OUT_BGP4_INFO (pRtProfile)) |=
                        BGP4_ATTR_COMM_MASK;
                }
                else
                {
                    MEMCPY (pComm->pu1CommVal, au4Community,
                            (u2CommunityCnt * COMM_VALUE_LEN));
                    pComm->u2CommCnt = u2CommunityCnt;
                    BGP4_INFO_ATTR_FLAG (BGP4_RT_OUT_BGP4_INFO (pRtProfile)) |=
                        BGP4_ATTR_COMM_MASK;
                }
            }
        }
        if (pInfo->u2ExtCommId != 0)
        {
            EXT_COMMUNITY_NODE_CREATE ((BGP4_INFO_ECOMM_ATTR
                                        (BGP4_RT_OUT_BGP4_INFO (pRtProfile))));
            pEcomm =
                (BGP4_INFO_ECOMM_ATTR (BGP4_RT_OUT_BGP4_INFO (pRtProfile)));
            if (pEcomm == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                          BGP4_MOD_NAME,
                          "\tMemory Allocation for Extended Community Attribute "
                          "FAILED!!!\n");
                gu4BgpDebugCnt[MAX_BGP_EXT_COMM_NODE_SIZING_ID]++;
                return (BGP4_FAILURE);
            }
            ATTRIBUTE_NODE_CREATE (pu1CommVal);
            if (pu1CommVal == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                          BGP4_MOD_NAME,
                          "\tMemory Allocation for Community Attribute "
                          "FAILED!!!\n");
                gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
                return (BGP4_FAILURE);
            }

            pEcomm->pu1EcommVal = pu1CommVal;

            BGP4_INFO_ECOMM_ATTR_COST_VAL (BGP4_RT_OUT_BGP4_INFO (pRtProfile)) =
                pInfo->u4ExtCommCost;
            BGP4_INFO_ECOMM_ATTR_COST_COMM (BGP4_RT_OUT_BGP4_INFO (pRtProfile))
                = pInfo->u2ExtCommId;
            BGP4_INFO_ECOMM_ATTR_COST_POI (BGP4_RT_OUT_BGP4_INFO (pRtProfile)) =
                pInfo->u2ExtCommPOI;
            BGP4_INFO_ECOMM_COUNT (BGP4_RT_OUT_BGP4_INFO (pRtProfile)) = 1;
            BGP4_INFO_ECOMM_ATTR_COST_FLAG (BGP4_RT_OUT_BGP4_INFO (pRtProfile))
                = BGP4_TRUE;
        }

        if (pInfo->u1AsCnt != 0)
        {
            pDestBgp4Info = BGP4_RT_OUT_BGP4_INFO (pRtProfile);
            TMO_SLL_Scan (BGP4_INFO_ASPATH (pDestBgp4Info), pSrcAspath,
                          tAsPath *)
            {
                u1AsCnt = BGP4_ASPATH_LEN (pSrcAspath);
                pu1AsNos = BGP4_ASPATH_NOS (pSrcAspath);
                while (u1AsCnt > 0)
                {
                    u4LocalAs = OSIX_NTOHS (*(UINT2 *) (VOID *) pu1AsNos);
                    if (u4LocalAs == pInfo->u4MatchAs)
                    {
                        u4LocalAs = pInfo->u4NextHopAS;
                        PTR_ASSIGN2 (pu1AsNos, u4LocalAs);
                        u1Ext = 1;
                        break;
                    }
                    u1AsCnt--;
                    pu1AsNos += 2;
                }
                if (u1Ext == 1)
                    break;
            }

        }
    }
    return BGP4_TRUE;
}

/**************************************************************************/
/*   Function Name   : Bgp4ApplyPrefixFilter                              */
/*   Description     : This function will check whether the route         */
/*                     can be added or dropped.                           */
/*                     If route is permitted then returned                */
/*                     BGP4_SUCCESS else return BGP4_FAILURE.             */
/*                                                                        */
/*   Input(s)        : pFilterRMap      - route map data                  */
/*                     pRtProfile       - ptr to the routing update       */
/*                                                                        */
/*   Output(s)       : None.                                              */
/*                                                                        */
/*   Return Value    : BGP4_FAILURE / BGP4_SUCCESS                        */
/**************************************************************************/
INT1
Bgp4ApplyPrefixFilter (tFilteringRMap * pFilterRMap, tRouteProfile * pRtProfile)
{
    tRtMapInfo          RtInfoIn;
    tRtMapInfo          RtInfoOut;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];

    if ((pFilterRMap == NULL) || (pRtProfile == NULL))
    {
        return BGP4_SUCCESS;
    }

/*  If status of route map is disable then filtering is not invoked */
    if (pFilterRMap->u1Status == FILTERNIG_STAT_DISABLE)
    {
        return BGP4_SUCCESS;
    }

    MEMSET (&RtInfoIn, 0, sizeof (tRtMapInfo));
    MEMSET (&RtInfoOut, 0, sizeof (tRtMapInfo));
    MEMSET (&au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
    if (MEMCMP
        (pFilterRMap->au1DistInOutFilterRMapName, au1RMapName,
         RMAP_MAX_NAME_LEN + 4) == 0)
    {
        return BGP4_SUCCESS;
    }

    if (pRtProfile->pRtInfo != NULL)
    {
#ifdef BGP4_IPV6_WANTED
        if (pRtProfile->NetAddress.NetAddr.u2Afi == BGP4_INET_AFI_IPV6)
        {
            IPVX_ADDR_INIT_IPV6 (RtInfoIn.DstXAddr, IPVX_ADDR_FMLY_IPV6,
                                 pRtProfile->NetAddress.NetAddr.au1Address);

            IPVX_ADDR_INIT_IPV6 (RtInfoIn.NextHopXAddr, IPVX_ADDR_FMLY_IPV6,
                                 pRtProfile->pRtInfo->NextHopInfo.au1Address);

            if (pRtProfile->pPEPeer != NULL)
            {
                IPVX_ADDR_INIT_IPV6 (RtInfoIn.SrcXAddr, IPVX_ADDR_FMLY_IPV6,
                                     pRtProfile->pPEPeer->peerConfig.
                                     RemoteAddrInfo.au1Address);

            }
        }
        else
#endif /* BGP4_IPV6_WANTED */
        {
            IPVX_ADDR_INIT_IPV4 (RtInfoIn.DstXAddr, IPVX_ADDR_FMLY_IPV4,
                                 pRtProfile->NetAddress.NetAddr.au1Address);

            IPVX_ADDR_INIT_IPV4 (RtInfoIn.NextHopXAddr, IPVX_ADDR_FMLY_IPV4,
                                 pRtProfile->pRtInfo->NextHopInfo.au1Address);

            if (pRtProfile->pPEPeer != NULL)
            {
                IPVX_ADDR_INIT_IPV4 (RtInfoIn.SrcXAddr, IPVX_ADDR_FMLY_IPV4,
                                     pRtProfile->pPEPeer->peerConfig.
                                     RemoteAddrInfo.au1Address);
            }
        }
        RtInfoIn.u2DstPrefixLen = pRtProfile->NetAddress.u2PrefixLen;
/*  Ignore modification of route attributes by route map */

        if (RMAP_ROUTE_DENY != RMapApplyPrefixRule (&RtInfoIn, &RtInfoOut,
                                                    pFilterRMap->
                                                    au1DistInOutFilterRMapName))
        {
            return BGP4_SUCCESS;
        }
        else
        {
            return BGP4_FAILURE;
        }
    }
    return BGP4_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : Bgp4ApplyInOutFilter                               */
/*   Description     : This function will check whether the route         */
/*                     can be added or dropped.                           */
/*                     If route is permitted then returned                */
/*                     BGP4_SUCCESS else return BGP4_FAILURE.             */
/*                                                                        */
/*   Input(s)        : pFilterRMap      - route map data                  */
/*                     pRtProfile       - ptr to the routing update       */
/*                     u4Dir            - Direction in/out                */
/*                                                                        */
/*   Output(s)       : None.                                              */
/*                                                                        */
/*   Return Value    : BGP4_FAILURE / BGP4_SUCCESS                        */
/**************************************************************************/
INT1
Bgp4ApplyInOutFilter (tFilteringRMap * pFilterRMap, tRouteProfile * pRtProfile,
                      UINT4 u4Dir)
{
    tRtMapInfo          RtInfoIn;
    tRtMapInfo          RtInfoOut;
    tBgp4Info          *pDestBgpInfo;
    tAsPath            *pDestAspath = NULL;
    tAsPath            *pSrcAspath = NULL;
    tAsPath            *pAspath = NULL;
    UINT1              *pu1CommVal = NULL;

    UINT2               u2Count = 0;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];

    if ((pFilterRMap == NULL) || (pRtProfile == NULL))
    {
        return BGP4_SUCCESS;
    }

/*  If status of route map is disable then filtering is not invoked */
    if (pFilterRMap->u1Status == FILTERNIG_STAT_DISABLE)
    {
        return BGP4_SUCCESS;
    }

    MEMSET (&RtInfoIn, 0, sizeof (tRtMapInfo));
    MEMSET (&RtInfoOut, 0, sizeof (tRtMapInfo));
    MEMSET (&au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
    if (MEMCMP
        (pFilterRMap->au1DistInOutFilterRMapName, au1RMapName,
         RMAP_MAX_NAME_LEN + 4) == 0)
    {
        return BGP4_SUCCESS;
    }
    TMO_SLL_Init (&RtInfoIn.TSASPath);

    if (pRtProfile->pRtInfo != NULL)
    {
#ifdef BGP4_IPV6_WANTED
        if (pRtProfile->NetAddress.NetAddr.u2Afi == BGP4_INET_AFI_IPV6)
        {
            IPVX_ADDR_INIT_IPV6 (RtInfoIn.DstXAddr, IPVX_ADDR_FMLY_IPV6,
                                 pRtProfile->NetAddress.NetAddr.au1Address);

            IPVX_ADDR_INIT_IPV6 (RtInfoIn.NextHopXAddr, IPVX_ADDR_FMLY_IPV6,
                                 pRtProfile->pRtInfo->NextHopInfo.au1Address);

            if (pRtProfile->pPEPeer != NULL)
            {
                IPVX_ADDR_INIT_IPV6 (RtInfoIn.SrcXAddr, IPVX_ADDR_FMLY_IPV6,
                                     pRtProfile->pPEPeer->peerConfig.
                                     RemoteAddrInfo.au1Address);

            }
        }
        else
#endif /* BGP4_IPV6_WANTED */
        {
            IPVX_ADDR_INIT_IPV4 (RtInfoIn.DstXAddr, IPVX_ADDR_FMLY_IPV4,
                                 pRtProfile->NetAddress.NetAddr.au1Address);

            IPVX_ADDR_INIT_IPV4 (RtInfoIn.NextHopXAddr, IPVX_ADDR_FMLY_IPV4,
                                 pRtProfile->pRtInfo->NextHopInfo.au1Address);

            if (pRtProfile->pPEPeer != NULL)
            {
                IPVX_ADDR_INIT_IPV4 (RtInfoIn.SrcXAddr, IPVX_ADDR_FMLY_IPV4,
                                     pRtProfile->pPEPeer->peerConfig.
                                     RemoteAddrInfo.au1Address);
            }
        }

        if ((pRtProfile->u1Protocol != BGP_ID) &&
            (pRtProfile->pRtInfo->TSASPath.u4_Count == 0))
        {
            RtInfoIn.i2RouteType = FSIP_LOCAL;
        }
        else
        {
            RtInfoIn.i2RouteType = REMOTE;
        }

        pDestBgpInfo = pRtProfile->pRtInfo;

        TMO_SLL_Scan (BGP4_INFO_ASPATH (pDestBgpInfo), pSrcAspath, tAsPath *)
        {
            pDestAspath = Bgp4MemGetASNode (sizeof (tAsPath));
            if (pDestAspath == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tBgp4ApplyInOutFilter() : GET_ASNode failed !!! \n");
                return BGP4_FAILURE;
            }
            BGP4_ASPATH_TYPE (pDestAspath) = BGP4_ASPATH_TYPE (pSrcAspath);
            BGP4_ASPATH_LEN (pDestAspath) = BGP4_ASPATH_LEN (pSrcAspath);
            MEMSET (BGP4_ASPATH_NOS (pDestAspath), 0, BGP4_AS4_SEG_LEN);
            MEMCPY (BGP4_ASPATH_NOS (pDestAspath), BGP4_ASPATH_NOS (pSrcAspath),
                    (BGP4_ASPATH_LEN (pSrcAspath) * BGP4_AS4_LENGTH));

            TMO_SLL_Add (&(RtInfoIn.TSASPath), &pDestAspath->sllNode);
        }
    }

    RtInfoIn.u2DstPrefixLen = pRtProfile->NetAddress.u2PrefixLen;
    if (pRtProfile->pRtInfo != NULL)
    {
        RtInfoIn.u4LocalPref = BGP4_INFO_RCVD_LOCAL_PREF (pRtProfile->pRtInfo);
        RtInfoIn.i4Origin = pRtProfile->pRtInfo->u1Origin + 1;
        RtInfoIn.u2RtProto = pRtProfile->u1Protocol;

        if (((BGP4_INFO_ATTR_FLAG (pRtProfile->pRtInfo)
              & BGP4_ATTR_COMM_MASK) == BGP4_ATTR_COMM_MASK)
            && (BGP4_RT_BGP_INFO (pRtProfile)->pCommunity != NULL))
        {
            pu1CommVal =
                BGP4_INFO_COMM_ATTR_VAL (BGP4_RT_BGP_INFO (pRtProfile));
            u2Count = BGP4_INFO_COMM_COUNT (BGP4_RT_BGP_INFO (pRtProfile));
            if (u2Count + 1 > MAX_BGP_ATTR_LEN / COMM_VALUE_LEN)
            {
                BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_FDB_TRC | BGP4_ALL_FAILURE_TRC,
                               BGP4_MOD_NAME,
                               "\tCommunity value count %d is exceeds the attribute values %d.\n",
                               (u2Count + 1),
                               (MAX_BGP_ATTR_LEN / COMM_VALUE_LEN));
                return (BGP4_FAILURE);

            }
            MEMCPY (&(RtInfoIn.au4Community),
                    pu1CommVal, (u2Count * COMM_VALUE_LEN));
            RtInfoIn.u1CommunityCnt = (UINT1) u2Count;
        }
        if (((BGP4_INFO_ATTR_FLAG (pRtProfile->pRtInfo)
              & BGP4_ATTR_MED_MASK) == BGP4_ATTR_MED_MASK))
        {
            MEMCPY (&(RtInfoIn.i4Metric),
                    &BGP4_INFO_RCVD_MED (pRtProfile->pRtInfo), 4);
        }
    }

/*  Ignore modification of route attributes by route map */

    if (RMAP_ROUTE_DENY != RMapApplyRule (&RtInfoIn, &RtInfoOut,
                                          pFilterRMap->
                                          au1DistInOutFilterRMapName))
    {
        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tRoutemap rule is applied for the route %s mask %s and "
                       "it is not denied.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                                        (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        Bgp4UpdateRouteInfo (&RtInfoOut, pRtProfile, u4Dir);
        for (;;)
        {
            pAspath = (tAsPath *) TMO_SLL_Last (&(RtInfoIn.TSASPath));
            if (pAspath == NULL)
            {
                break;
            }
            TMO_SLL_Delete (&(RtInfoIn.TSASPath), &pAspath->sllNode);
            Bgp4MemReleaseASNode (pAspath);
        }

        return BGP4_SUCCESS;
    }
    else
    {
        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tRoutemap rule is applied for the route %s mask %s and "
                       "it is denied.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                                        (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        for (;;)
        {
            pAspath = (tAsPath *) TMO_SLL_Last (&(RtInfoIn.TSASPath));
            if (pAspath == NULL)
            {
                break;
            }
            TMO_SLL_Delete (&(RtInfoIn.TSASPath), &pAspath->sllNode);
            Bgp4MemReleaseASNode (pAspath);
        }
        return BGP4_FAILURE;
    }
}
#endif /* ROUTEMAP_WANTED */

#endif /* _BGCMFILT_C */
