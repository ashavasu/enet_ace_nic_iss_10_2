/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgcmattr.c,v 1.21 2017/09/15 06:19:53 siva Exp $
 *
 * Description: This file contains Community Attribute processing
 *              functions.
 *
 *******************************************************************/
#ifndef _BGCMATTR_C
#define _BGCMATTR_C

#include "bgp4com.h"

/*****************************************************************************/
/*    Function Name   : GetAdvtStatus                                        */
/*                                                                           */
/*    Description     :                                                      */
/*                      Gets the advertisement status by applying the scope  */
/*                      of well known communities against the peer.          */
/*                      The value of COMM_TRUE/COMM_FALSE indicates          */
/*                      whether route can be advertised to this peer or not. */
/*                      For other than well known communities , advertisement */
/*                      status takes the value COMM_TRUE.                    */
/*                                                                           */
/*    Input(s)        : pRoute - pointer to the route profile that is to be  */
/*                      processed.                                           */
/*                      pPeerInfo - pointer to peer entry which holds        */
/*                      peer information.                                    */
/*                                                                           */
/*    Output(s)       : pi1AdvtStatus - Holds the advertisement status for   */
/*                      the route                                            */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*                                                                           */
/*  Returns         :   COMM_SUCCESS / COMM_FAILURE                          */
/*****************************************************************************/
INT4
GetAdvtStatus (tRouteProfile * pRoute,
               tBgp4PeerEntry * pPeerInfo, INT1 *pi1AdvtStatus)
{
    UINT2               u2CommPathStartOffSet;
    UINT4               u4WellKnownComm;
    INT1                i1IsWellKnownCommExists;
    INT1                i1CommSetStatus;
    INT1                i1NahCanRouteBeAdvt;
    INT1                i1NehCanRouteBeAdvt;
    INT1                i1NeshCanRouteBeAdvt;

    u2CommPathStartOffSet = 0;
    u4WellKnownComm = 0;

    if (BGP4_INFO_COMM_ATTR (BGP4_RT_BGP_INFO (pRoute)) != NULL)
    {
        /* Check for the presence of well known community */

        i1CommSetStatus = CommGetCommSetStatus (pRoute);

        if (i1CommSetStatus == COMM_SET_NONE)
        {
            /* Community Set None is TRUE hence route should be considered */
            /* for advertising. while advertising it is taken care that    */
            /* existing comm or new configured communities is not sent     */
            *pi1AdvtStatus = COMM_TRUE;
            return COMM_SUCCESS;
        }

        i1IsWellKnownCommExists =
            IsCommWellKnown (pRoute, u2CommPathStartOffSet, &u4WellKnownComm);

        if (i1IsWellKnownCommExists == COMM_TRUE)
        {
            switch (u4WellKnownComm)
            {
                case (NO_ADVERTISE):
                    i1NahCanRouteBeAdvt = COMM_FALSE;
                    *pi1AdvtStatus = i1NahCanRouteBeAdvt;
                    return COMM_SUCCESS;

                case (NO_EXPORT):
                    if (pPeerInfo != NULL)
                    {
                        i1NehCanRouteBeAdvt = NoExportHandler (pPeerInfo);
                        *pi1AdvtStatus = i1NehCanRouteBeAdvt;
                    }
                    else
                    {
                        *pi1AdvtStatus = COMM_FALSE;
                    }
                    return COMM_SUCCESS;

                case (NO_EXPORT_SUBCONFED):
                    if (pPeerInfo != NULL)
                    {
                        i1NeshCanRouteBeAdvt =
                            NoExportSubConfHandler (pPeerInfo);
                        *pi1AdvtStatus = i1NeshCanRouteBeAdvt;
                    }
                    else
                    {
                        *pi1AdvtStatus = COMM_FALSE;
                    }
                    return COMM_SUCCESS;
                default:
                    *pi1AdvtStatus = COMM_TRUE;
                    return COMM_FAILURE;
            }
        }

        if (i1IsWellKnownCommExists == COMM_FALSE)
        {
            /* For other than well known communities , */
            /* status takes the value COMM_TRUE       */
            *pi1AdvtStatus = COMM_TRUE;
        }

        if (i1IsWellKnownCommExists == COMM_IGNORE)
        {
            *pi1AdvtStatus = COMM_FALSE;
        }

        return COMM_SUCCESS;

    }
    else
    {
        /* Community path attribute NOT present */
        *pi1AdvtStatus = COMM_TRUE;
        return COMM_SUCCESS;
    }
}

/*****************************************************************************/
/*    Function Name   : CommGetUpdCommAttrib                                 */
/*                                                                           */
/*    Description     : Gets the community path attribute updated for the    */
/*                      route in the pSelRouteProfile                        */
/*                                                                           */
/*    Input(s)        : pSelRouteProfile - pointer to route profile of NLRI  */
/*                                                                           */
/*    Output(s)       : pAdvtBgpInfo - Pointer to updated BGP4-INFO to be    */
/*                                     advertised.                           */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*                                                                           */
/*  Returns         :   COMM_SUCCESS / COMM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
CommGetUpdCommAttrib (tRouteProfile * pSelRouteProfile,
                      tBgp4Info * pAdvtBgpInfo, tBgp4Info * pRtInfo)
{
    tCommunity         *pComm = NULL;
    INT4                i4RouteConfCommStatus;
    INT4                i4CommFormStatus;
    INT1                i1CommSetStatus;
#ifdef VPLSADS_WANTED
    UINT4               u4AsafiMask;
#endif

    i1CommSetStatus = CommGetCommSetStatus (pSelRouteProfile);
    if (i1CommSetStatus == COMM_SET_NONE)
    {
        /* Community Set None is TRUE hence route should be considered for  */
        /* advertising , existing comm or new config communities is not sent */
        /* Success is returned */

        BGP4_TRC_ARG1 (NULL,
                       BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                       "\tCommunity None is set. Hence the existing community or "
                       "new configured communities are not sent for the route %s.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pSelRouteProfile),
                                        BGP4_RT_AFI_INFO (pSelRouteProfile)));
        return COMM_SUCCESS;
    }

    COMMUNITY_NODE_CREATE (pComm);
    if (pComm == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for Community Attribute " "FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_COMMUNITY_NODE_SIZING_ID]++;
        return (COMM_FAILURE);
    }
    pComm->pu1CommVal = NULL;
    pComm->u2CommCnt = 0;
    pComm->u1Flag = 0;
    BGP4_INFO_COMM_ATTR (pAdvtBgpInfo) = pComm;

    /* If i1CommSetStatus is COMM_MODIFY means whatever comms         */
    /* configured as delete will be deleted in the received comm info */

    if (i1CommSetStatus == COMM_MODIFY)
    {
        i4CommFormStatus = CommFormRouteCommPath (pSelRouteProfile,
                                                  pAdvtBgpInfo, pRtInfo);
        if (i4CommFormStatus == COMM_FAILURE)
        {
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC |
                           BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                           BGP4_MOD_NAME,
                           "\tUnable to form community path attribute for route %s/%d "
                           "when community modify is set.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                            (pSelRouteProfile),
                                            BGP4_RT_AFI_INFO
                                            (pSelRouteProfile)),
                           BGP4_RT_PREFIXLEN (pSelRouteProfile));
            if (BGP4_INFO_COMM_ATTR_VAL (pAdvtBgpInfo) != NULL)
            {
                ATTRIBUTE_NODE_FREE (BGP4_INFO_COMM_ATTR_VAL (pAdvtBgpInfo));
            }
            COMMUNITY_NODE_FREE (BGP4_INFO_COMM_ATTR (pAdvtBgpInfo));
            BGP4_INFO_COMM_ATTR (pAdvtBgpInfo) = NULL;
            return COMM_FAILURE;
        }
    }

#ifdef VPLSADS_WANTED
    /*ADS-VPLS related processing */
    /*Skip in l2vpn,vpls case, since there will not be any addition of 
       community for prefix */
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pSelRouteProfile),
                           BGP4_RT_SAFI_INFO (pSelRouteProfile), u4AsafiMask);
    if (CAP_MP_L2VPN_VPLS != u4AsafiMask)
    {

#endif
        i4RouteConfCommStatus = CommUpdateRouteCommAdd (pSelRouteProfile,
                                                        pAdvtBgpInfo);
        if (i4RouteConfCommStatus == COMM_FAILURE)
        {
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC |
                           BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                           BGP4_MOD_NAME,
                           "\tUnable to form community path attribute for route %s/%d \n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                            (pSelRouteProfile),
                                            BGP4_RT_AFI_INFO
                                            (pSelRouteProfile)),
                           BGP4_RT_PREFIXLEN (pSelRouteProfile));
            if (BGP4_INFO_COMM_ATTR_VAL (pAdvtBgpInfo) != NULL)
            {
                ATTRIBUTE_NODE_FREE (BGP4_INFO_COMM_ATTR_VAL (pAdvtBgpInfo));
            }
            COMMUNITY_NODE_FREE (BGP4_INFO_COMM_ATTR (pAdvtBgpInfo));
            BGP4_INFO_COMM_ATTR (pAdvtBgpInfo) = NULL;
            return COMM_FAILURE;
        }
#ifdef VPLSADS_WANTED
    }
#endif

    /* Check if any community exist. If no community exist, then unlink the
     * community pointer from Advertised BGP4-INFO and free it. */
    if (BGP4_INFO_COMM_COUNT (pAdvtBgpInfo) == 0)
    {
        COMMUNITY_NODE_FREE (BGP4_INFO_COMM_ATTR (pAdvtBgpInfo));
        BGP4_INFO_COMM_ATTR (pAdvtBgpInfo) = NULL;
    }
    else
    {
        BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_COMM_MASK;
    }
    return COMM_SUCCESS;
}

/*****************************************************************************/
/*    Function Name   :  FillCalcCommInUpdMesg                               */
/*                                                                           */
/*    Description     : Incoming update message is updated with              */
/*                      community path attribute                             */
/*                                                                           */
/*    Input(s)        : pu1UpdateMesg - Incoming update message              */
/*                                                                           */
/*                      pAdvtBgpInfo - Path attribute info containing the    */
/*                                     the communities to be advertised.     */
/*                                                                           */
/*    Output(s)       : pu2NumBytesFilled - Number of bytes filled with      */
/*                      community path attribute                             */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*                                                                           */
/*  Returns         :   Number of bytes filled with community path attribute.*/
/*                                                                           */
/*****************************************************************************/
UINT2
FillCalcCommInUpdMesg (UINT1 *pu1UpdateMesg, tBgp4Info * pAdvtBgpInfo)
{
    UINT2               u2ByteWriteOffset;
    UINT2               u2CommLen;
    UINT1               u1FlagVal;
    UINT1               u1IsExtLenBitSet = BGP4_FALSE;

    u2ByteWriteOffset = 0;
    u1FlagVal = 0;

    if ((BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) & BGP4_ATTR_COMM_MASK) !=
        BGP4_ATTR_COMM_MASK)
    {
        return 0;
    }

    u2CommLen = (UINT2) (BGP4_INFO_COMM_COUNT (pAdvtBgpInfo) * COMM_VALUE_LEN);
    u1IsExtLenBitSet = (u2CommLen > BGP4_ONE_BYTE_MAX_VAL) ?
        BGP4_TRUE : BGP4_FALSE;

    u1FlagVal = (UINT1) (BGP4_OPTIONAL_FLAG_MASK | BGP4_TRANSITIVE_FLAG_MASK);
    if ((BGP4_INFO_COMM_ATTR_FLAG (pAdvtBgpInfo) & BGP4_PARTIAL_FLAG_MASK) ==
        BGP4_PARTIAL_FLAG_MASK)
    {
        u1FlagVal |= (UINT1) BGP4_PARTIAL_FLAG_MASK;
    }
    if (u1IsExtLenBitSet == BGP4_TRUE)
    {
        u1FlagVal |= (UINT1) BGP4_EXT_LEN_FLAG_MASK;
    }

    /* Comm Attribute Type Flag */
    *(pu1UpdateMesg + u2ByteWriteOffset) = u1FlagVal;
    u2ByteWriteOffset += BGP4_ATYPE_FLAGS_LEN;

    /* Comm Attribute Type */
    *(pu1UpdateMesg + u2ByteWriteOffset) = COMM_TYPE_CODE;
    u2ByteWriteOffset += BGP4_ATYPE_CODE_LEN;

    /* Comm Attribute Len */
    if (u1IsExtLenBitSet == BGP4_FALSE)
    {
        *(pu1UpdateMesg + u2ByteWriteOffset) = (UINT1) u2CommLen;
        u2ByteWriteOffset += BGP4_NORMAL_ATTR_LEN_SIZE;
    }
    else
    {
        PTR_ASSIGN2 ((pu1UpdateMesg + u2ByteWriteOffset), u2CommLen);
        u2ByteWriteOffset += BGP4_EXTENDED_ATTR_LEN_SIZE;
    }

    /* Fill the community value. */
    MEMCPY ((pu1UpdateMesg + u2ByteWriteOffset),
            BGP4_INFO_COMM_ATTR_VAL (pAdvtBgpInfo), u2CommLen);
    u2ByteWriteOffset += u2CommLen;

    return u2ByteWriteOffset;
}

/*****************************************************************************/
/*    Function Name   : CommFormRouteCommPath                                */
/*                                                                           */
/*    Description     : Copies the received community path attribute to the  */
/*                      Route community path after checking each community   */
/*                      in route community delete table                      */
/*                                                                           */
/*    Input(s)        : pSelRouteProfile - pointer to route profile of NLRI  */
/*                                                                           */
/*    Output(s)       : pAdvtBgpInfo - Pointer to updated BGP4-INFO to be    */
/*                                     advertised.                           */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*                                                                           */
/*  Returns         :   COMM_SUCCESS/COMM_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CommFormRouteCommPath (tRouteProfile * pSelRouteProfile,
                       tBgp4Info * pAdvtBgpInfo, tBgp4Info * pRtInfo)
{
    tBgp4Info          *pRouteInfo = NULL;
    UINT1              *pu1CommBuf = NULL;
    UINT4               u4TempComm;
    INT1                i1IsRouteCommDeleteStatus;
    UINT2               u2WriteCommCount = 0;
    UINT4               u4Index;
    UINT1               u1IsPartialSet = BGP4_FALSE;

    if (pRtInfo != NULL && BGP4_INFO_COMM_ATTR (pRtInfo) != NULL)
    {
        pRouteInfo = pRtInfo;
    }
    else
    {
        pRouteInfo = BGP4_RT_BGP_INFO (pSelRouteProfile);
    }
    if (BGP4_INFO_COMM_ATTR (pRouteInfo) == NULL)
    {
        /* Community path attribute NOT present */
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                       "\tCommunity path attribute is not present for the route %s\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pSelRouteProfile),
                                        BGP4_RT_AFI_INFO (pSelRouteProfile)));
        return COMM_SUCCESS;
    }

    /* Process all the communities */
    for (u4Index = 0; u4Index < BGP4_INFO_COMM_COUNT (pRouteInfo); u4Index++)
    {
        if ((u2WriteCommCount * COMM_VALUE_LEN) >= BGP4_MAX_MSG_LEN)
        {
            /* Set the partial bit. */
            u1IsPartialSet = BGP4_TRUE;
            break;
        }
        PTR_FETCH4 (u4TempComm,
                    (BGP4_INFO_COMM_ATTR_VAL (pRouteInfo) +
                     (u4Index * COMM_VALUE_LEN)));
        i1IsRouteCommDeleteStatus = CommIsRouteCommDelete (pSelRouteProfile,
                                                           u4TempComm);
        if (i1IsRouteCommDeleteStatus == COMM_FALSE)
        {
            /* Community NOT to be deleted                          */
            /* To begin with Allocate a Buffer of size BGP4_MAX_MSG_LEN
             * and fill in the community Attributes. If the length of this
             * attribute goes greater that this then set the Partial Flag
             * Bit. Else if the length of the community attribute is less,
             * then release this and reallocate buffer equal to the correct
             * size. */
            if (pu1CommBuf == NULL)
            {
                pu1CommBuf = MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);
                if (pu1CommBuf == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                              BGP4_MOD_NAME,
                              "\tMemory Allocation for Filling Community Attribute"
                              " FAILED!!!\n");
                    gu4BgpDebugCnt[MAX_BGP_BUFNODE_MSGS_SIZING_ID]++;
                    return COMM_FAILURE;
                }
            }

            PTR_ASSIGN4 ((pu1CommBuf + (u2WriteCommCount * COMM_VALUE_LEN)),
                         u4TempComm);
            u2WriteCommCount++;
        }
        else
        {
            /* The received community is deleted for the route  */
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tCOMMUNITY ATTRIBUTE - (%d) - Present"
                           "in Received FEASIBLE ROUTE %s is deleted.\n",
                           u4TempComm,
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                            (pSelRouteProfile),
                                            BGP4_RT_AFI_INFO
                                            (pSelRouteProfile)));
        }
    }
    /* Now Reallocate the Buffer for storing the Community Attribute. */
    if (u2WriteCommCount > 0)
    {
        ATTRIBUTE_NODE_CREATE (BGP4_INFO_COMM_ATTR_VAL (pAdvtBgpInfo));
        if (BGP4_INFO_COMM_ATTR_VAL (pAdvtBgpInfo) == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for Filling Community Attribute"
                      " FAILED!!!\n");
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1CommBuf);
            gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
            return COMM_FAILURE;
        }
        BGP4_INFO_COMM_COUNT (pAdvtBgpInfo) = u2WriteCommCount;
        MEMCPY (BGP4_INFO_COMM_ATTR_VAL (pAdvtBgpInfo), pu1CommBuf,
                u2WriteCommCount * COMM_VALUE_LEN);
        BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_COMM_MASK;
        if (((BGP4_INFO_COMM_ATTR_FLAG (pRouteInfo) &
              BGP4_PARTIAL_FLAG_MASK) == BGP4_PARTIAL_FLAG_MASK) ||
            (u1IsPartialSet == BGP4_TRUE))
        {
            /* Partial Bit should be set. */
            BGP4_INFO_COMM_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_PARTIAL_FLAG_MASK;
        }

        /* Release the Old Buffer. */
        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1CommBuf);
    }

    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                   BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                   "\tThe Received Community path attribute is successfully filled "
                   "for the route %s\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pSelRouteProfile),
                                    BGP4_RT_AFI_INFO (pSelRouteProfile)));
    return COMM_SUCCESS;
}

/*****************************************************************************/
/*    Function Name   : CommUpdateAggrComm                                   */
/*                                                                           */
/*    Description     : Aggregated route should have a COMMUNITIES           */
/*                      path attribute which contains all communities from   */
/*                      all of the aggregated routes.                        */
/*                                                                           */
/*    Input(s)        : pAggrRtProfile - Aggregated route.                   */
/*                      pNewRtProfile  - Route to be aggregated.             */
/*                                                                           */
/*    Output(s)       : None.                                                */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*                                                                           */
/*  Returns         :   COMM_SUCCESS/COMM_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CommUpdateAggrComm (tRouteProfile * pAggrRtProfile,
                    tRouteProfile * pNewRtProfile)
{
    tBgp4Info          *pAggrBgpInfo = NULL;
    tBgp4Info          *pNewBgpInfo = NULL;
    UINT1              *pu1Str = NULL;
    UINT1              *pu1TempStr = NULL;
    UINT4               u4AggrComm = 0;
    UINT4               u4NewComm = 0;
    UINT4               u4Index = 0;
    UINT2               u2TotalCommLen;
    UINT2               u2AggrCommPathStartOffSet;
    UINT2               u2AggrCommPathLen;
    UINT2               u2NewCommPathLen;
    UINT1               u1IsCommMatch = BGP4_FALSE;
    INT1                i1IsAggrCommPathExists = BGP4_FALSE;

    pNewBgpInfo = BGP4_RT_BGP_INFO (pNewRtProfile);
    pAggrBgpInfo = BGP4_RT_BGP_INFO (pAggrRtProfile);

    /* Check for the Community Attribute in New Route. */
    if (BGP4_INFO_COMM_ATTR (pNewBgpInfo) == NULL)
    {
        /* New route does not contain Community attribute. No need for
         * any aggregation. */
        return COMM_SUCCESS;
    }

    u2NewCommPathLen = (UINT2) (BGP4_INFO_COMM_COUNT (pNewBgpInfo) *
                                COMM_VALUE_LEN);
    if (BGP4_INFO_COMM_ATTR (pAggrBgpInfo) == NULL)
    {
        /* Community not present in Aggregated route. */
        i1IsAggrCommPathExists = BGP4_FALSE;
        u2AggrCommPathLen = 0;
    }
    else
    {
        /* Community attribute is present. */
        i1IsAggrCommPathExists = BGP4_TRUE;
        u2AggrCommPathLen = (UINT2) (BGP4_INFO_COMM_COUNT (pAggrBgpInfo) *
                                     COMM_VALUE_LEN);
    }

    ATTRIBUTE_NODE_CREATE (pu1Str);
    if (pu1Str == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for Aggr-Community Attribute "
                  "FAILED!!!.\n");
        /* Set the partial bit to indicate that the attribute
         * info is not complete. */
        if (i1IsAggrCommPathExists == BGP4_TRUE)
        {
            BGP4_INFO_COMM_ATTR_FLAG (pAggrBgpInfo) |= BGP4_PARTIAL_FLAG_MASK;
        }
        gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
        return (COMM_FAILURE);
    }

    /* Add the community attribute from new route */
    MEMCPY (pu1Str, BGP4_INFO_COMM_ATTR_VAL (pNewBgpInfo), u2NewCommPathLen);
    u2TotalCommLen = u2NewCommPathLen;

    if (i1IsAggrCommPathExists == BGP4_TRUE)
    {
        /* Verify whether the same attribute is present in the new route
         * and aggregate route. If yes, then that comm is added only once.
         */
        u2AggrCommPathStartOffSet = 0;
        for (;;)
        {
            PTR_FETCH_4 (u4AggrComm, (BGP4_INFO_COMM_ATTR_VAL (pAggrBgpInfo) +
                                      u2AggrCommPathStartOffSet));
            u1IsCommMatch = BGP4_FALSE;
            for (u4Index = 0; u4Index < BGP4_INFO_COMM_COUNT (pNewBgpInfo);
                 u4Index++)
            {
                PTR_FETCH_4 (u4NewComm, (BGP4_INFO_COMM_ATTR_VAL (pNewBgpInfo) +
                                         (u4Index * COMM_VALUE_LEN)));
                if (u4NewComm == u4AggrComm)
                {
                    u1IsCommMatch = BGP4_TRUE;
                    break;
                }
            }
            if (u1IsCommMatch == BGP4_FALSE)
            {
                /* Add the community attribute to new comm list. */
                MEMCPY ((pu1Str + u2TotalCommLen), &u4AggrComm, COMM_VALUE_LEN);
                u2TotalCommLen += COMM_VALUE_LEN;
            }
            u2AggrCommPathStartOffSet += COMM_VALUE_LEN;
            if (BGP4_INFO_COMM_COUNT (pAggrBgpInfo) ==
                (UINT2) (u2AggrCommPathStartOffSet / COMM_VALUE_LEN))
            {
                break;
            }
        }

        if ((UINT2) (u2NewCommPathLen + u2AggrCommPathLen) != u2TotalCommLen)
        {
            /* Few overlapping communities exists in both routes. */
            ATTRIBUTE_NODE_CREATE (pu1TempStr);
            if (pu1TempStr == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                          "\tMemory Allocation for Aggr-Community Attribute "
                          "FAILED!!!.\n");
                /* Set the partial bit to indicate that the attribute
                 * info is not complete. */
                if (i1IsAggrCommPathExists == BGP4_TRUE)
                {
                    BGP4_INFO_COMM_ATTR_FLAG (pAggrBgpInfo) |=
                        BGP4_PARTIAL_FLAG_MASK;
                }
                ATTRIBUTE_NODE_FREE (pu1Str);
                gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
                return (COMM_FAILURE);
            }

            MEMCPY (pu1TempStr, pu1Str, u2TotalCommLen);
            ATTRIBUTE_NODE_FREE (pu1Str);
            pu1Str = pu1TempStr;
        }
        /* Free old Comm attribute from the Aggregate route. */
        ATTRIBUTE_NODE_FREE (BGP4_INFO_COMM_ATTR_VAL (pAggrBgpInfo));
        BGP4_INFO_COMM_ATTR_VAL (pAggrBgpInfo) = NULL;
    }
    else
    {
        /* Allocate memory for Community Attribute. */
        COMMUNITY_NODE_CREATE (BGP4_INFO_COMM_ATTR (pAggrBgpInfo));
        if (BGP4_INFO_COMM_ATTR (pAggrBgpInfo) == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                      BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for Aggregate Community "
                      "Attribute FAILED!!!\n");
            ATTRIBUTE_NODE_FREE (pu1Str);
            gu4BgpDebugCnt[MAX_BGP_COMMUNITY_NODE_SIZING_ID]++;
            return (COMM_FAILURE);
        }

        /* Initialise the community attribute. */
        BGP4_INFO_COMM_ATTR_FLAG (pAggrBgpInfo) =
            BGP4_INFO_COMM_ATTR_FLAG (pNewBgpInfo);
        BGP4_INFO_COMM_COUNT (pAggrBgpInfo) = 0;
        BGP4_INFO_COMM_ATTR_VAL (pAggrBgpInfo) = NULL;
    }

    /* Update the Community Attribute pointer */
    BGP4_INFO_COMM_ATTR_VAL (pAggrBgpInfo) = pu1Str;
    BGP4_INFO_COMM_COUNT (pAggrBgpInfo) =
        (UINT2) (u2TotalCommLen / COMM_VALUE_LEN);
    if ((BGP4_INFO_COMM_ATTR_FLAG (pNewBgpInfo) & BGP4_PARTIAL_FLAG_MASK) ==
        BGP4_PARTIAL_FLAG_MASK)
    {
        /* Set the partial bit in the Aggregated comm attribute. */
        BGP4_INFO_COMM_ATTR_FLAG (pAggrBgpInfo) |= BGP4_PARTIAL_FLAG_MASK;
    }
    BGP4_INFO_ATTR_FLAG (pAggrBgpInfo) |= (BGP4_ATTR_COMM_MASK);
    return COMM_SUCCESS;
}

/*****************************************************************************/
/*    Function Name   : CommUpdateRouteCommAdd                               */
/*                                                                           */
/*    Description     : Update the communities as per policy.                */
/*                                                                           */
/*    Input(s)        : pSelRouteProfile - pointer to route profile of NLRI  */
/*                                                                           */
/*    Output(s)       : pAdvtBgpInfo - Pointer to updated BGP4-INFO to be    */
/*                                     advertised.                           */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred : ROUTES_COMM_ADD_TBL                          */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*                                                                           */
/*  Returns         :   COMM_SUCCESS                                         */
/*                                                                           */
/*****************************************************************************/
INT4
CommUpdateRouteCommAdd (tRouteProfile * pSelRouteProfile,
                        tBgp4Info * pAdvtBgpInfo)
{
    tRouteConfComm     *pRouteConfComm = NULL;
    UINT1              *pu1CommBuf = NULL;
    UINT1              *pu1ReallocCommBuf = NULL;
    tCommProfile       *pCommProfile = NULL;
    UINT4               u4ReadComm;
    tRouteConfComm      RouteConfComm;
    UINT2               u2WriteCommCount = 0;
    UINT1               u1IsPartialSet = BGP4_FALSE;

    if (pSelRouteProfile == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC |
                  BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                  BGP4_MOD_NAME,
                  "\tReceived Route profile is NULL while updating communities "
                  "as per policy\n");
        return COMM_FAILURE;
    }
    Bgp4CopyNetAddressStruct (&(RouteConfComm.RtNetAddress),
                              BGP4_RT_NET_ADDRESS_INFO (pSelRouteProfile));
    RouteConfComm.u4Context = BGP4_RT_CXT_ID (pSelRouteProfile);
    pRouteConfComm = BGP4_RB_TREE_GET (ROUTES_COMM_ADD_TBL,
                                       (tBgp4RBElem *) (&RouteConfComm));
    if (pRouteConfComm != NULL)
    {
        TMO_SLL_Scan (&(pRouteConfComm->TSConfComms), pCommProfile,
                      tCommProfile *)
        {
            u4ReadComm = pCommProfile->u4Comm;
            if (pu1CommBuf == NULL)
            {
                /* Allocate a Buffer of size BGP4_MAX_MSG_LEN and keep
                 * storing the communities. If the total comm size
                 * exceeds this limit, then set the partial bit. */
                pu1CommBuf = MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);
                if (pu1CommBuf == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                              BGP4_MOD_NAME,
                              "\tMemory Allocation for Filling Community"
                              " Attribute FAILED!!!\n");
                    gu4BgpDebugCnt[MAX_BGP_BUFNODE_MSGS_SIZING_ID]++;
                    return COMM_FAILURE;
                }
            }

            if ((u2WriteCommCount * COMM_VALUE_LEN) < BGP4_MAX_MSG_LEN)
            {
                PTR_ASSIGN4 ((pu1CommBuf + (u2WriteCommCount * COMM_VALUE_LEN)),
                             u4ReadComm);
                u2WriteCommCount++;
            }
            else
            {
                /* The community buffer is full , we can fill only */
                /* partial of the community configured for this route */
                u1IsPartialSet = BGP4_TRUE;
                break;
            }
        }
    }

    if (u2WriteCommCount > 0)
    {
        /* Reallocate the memory for the community attribute. */
        ATTRIBUTE_NODE_CREATE (pu1ReallocCommBuf);
        if (pu1ReallocCommBuf == NULL)
        {
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1CommBuf);
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for Filling Community"
                      " Attribute FAILED!!!\n");
            gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
            return COMM_FAILURE;
        }
        if (BGP4_INFO_COMM_ATTR_VAL (pAdvtBgpInfo) != NULL)
        {
            /* Fill Comm Attribute present in the BGP4-INFO */
            MEMCPY (pu1ReallocCommBuf, BGP4_INFO_COMM_ATTR_VAL (pAdvtBgpInfo),
                    BGP4_INFO_COMM_COUNT (pAdvtBgpInfo) * COMM_VALUE_LEN);

            /* Fill Comm Attribute Configured. */
            if ((u2WriteCommCount * COMM_VALUE_LEN) < BGP4_MAX_MSG_LEN)
            {
                MEMCPY (pu1ReallocCommBuf +
                        (BGP4_INFO_COMM_COUNT (pAdvtBgpInfo) * COMM_VALUE_LEN),
                        pu1CommBuf, u2WriteCommCount * COMM_VALUE_LEN);
            }
            /* Release the old comm buffer from BGP4-INFO */
            ATTRIBUTE_NODE_FREE (BGP4_INFO_COMM_ATTR_VAL (pAdvtBgpInfo));
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1CommBuf);
        }
        else
        {
            MEMCPY (pu1ReallocCommBuf, pu1CommBuf,
                    u2WriteCommCount * COMM_VALUE_LEN);
        }
        /* Update the Community attribute in BGP4-INFO */
        BGP4_INFO_COMM_ATTR_VAL (pAdvtBgpInfo) = pu1ReallocCommBuf;
        BGP4_INFO_COMM_COUNT (pAdvtBgpInfo) += u2WriteCommCount;
        BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_COMM_MASK;
        if (u1IsPartialSet == BGP4_TRUE)
        {
            BGP4_INFO_COMM_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_PARTIAL_FLAG_MASK;
        }
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                       "\tThe Community path attribute is successfully updated "
                       "for the route %s\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pSelRouteProfile),
                                        BGP4_RT_AFI_INFO (pSelRouteProfile)));
    }
    else
    {
        if (pRouteConfComm != NULL)
        {
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1CommBuf);
        }
    }
    return COMM_SUCCESS;
}

/*****************************************************************************/
/*    Function Name   : CommIsRouteCommDelete                                */
/*                                                                           */
/*    Description     : To Check whether this community is in  delete list   */
/*                                                                           */
/*    Input(s)        : pSelRouteProfile - pointer to route profile of NLRI  */
/*                      u4CommVal - The community which is to checked in the */
/*                      ROUTES_COMM_DELETE_TBL , whether this community is to */
/*                      be deleted or not                                    */
/*                                                                           */
/*    Output(s)       : None                                                 */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred : ROUTES_COMM_DELETE_TBL                       */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*                                                                           */
/*  Returns         :     COMM_TRUE / COMM_FALSE                             */
/*                                                                           */
/*****************************************************************************/
INT1
CommIsRouteCommDelete (tRouteProfile * pSelRouteProfile, UINT4 u4CommVal)
{
    tRouteConfComm     *pRouteConfComm = NULL;
    tCommProfile       *pCommProfile = NULL;
    tRouteConfComm      RouteConfComm;
    UINT4               u4ReadComm;
    INT1                i1CommDeleteStatus;
#ifdef VPLSADS_WANTED
    UINT4               u4AsafiMask;
#endif

    i1CommDeleteStatus = COMM_FALSE;

    if (pSelRouteProfile == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC |
                  BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                  BGP4_MOD_NAME,
                  "\tReceived Route profile is NULL while checking whether "
                  "community is in delete list.\n");
        return COMM_FAILURE;
    }

#ifdef VPLSADS_WANTED
    /*ADS-VPLS related processing */
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pSelRouteProfile),
                           BGP4_RT_SAFI_INFO (pSelRouteProfile), u4AsafiMask);
    if (CAP_MP_L2VPN_VPLS == u4AsafiMask)
    {
        return COMM_FALSE;
    }

#endif

    /* To Get all the communities configured for this route         */
    /* The key is the number of ones in u4IpNetwork and u1PrefixLen */

    Bgp4CopyNetAddressStruct (&(RouteConfComm.RtNetAddress),
                              BGP4_RT_NET_ADDRESS_INFO (pSelRouteProfile));
    RouteConfComm.u4Context = BGP4_RT_CXT_ID (pSelRouteProfile);
    pRouteConfComm = BGP4_RB_TREE_GET (ROUTES_COMM_DELETE_TBL,
                                       (tBgp4RBElem *) (&RouteConfComm));
    if (pRouteConfComm != NULL)
    {
        TMO_SLL_Scan (&(pRouteConfComm->TSConfComms), pCommProfile,
                      tCommProfile *)
        {
            u4ReadComm = pCommProfile->u4Comm;
            if (u4ReadComm == u4CommVal)
            {
                /* The community has to deleted since it is  */
                /* present in the delete list                */
                return COMM_TRUE;
            }
        }
    }

    return i1CommDeleteStatus;
}

/*****************************************************************************/
/*    Function Name   : CommGetCommSetStatus                             */
/*                                                                           */
/*    Description     : Gets the Community Set None Status , which means the  */
/*                      route has to be sent without community attribute     */
/*                                                                           */
/*    Input(s)        : pSelRouteProfile - pointer to route profile of NLRI  */
/*                                                                           */
/*    Output(s)       : None                                                 */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred : ROUTES_COMM_ADD_TBL                          */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*                                                                           */
/*  Returns         :     COMM_TRUE/COMM_FALSE                               */
/*                                                                           */
/*****************************************************************************/
INT1
CommGetCommSetStatus (tRouteProfile * pSelRouteProfile)
{
    tRouteCommSetStatus *pRouteCommSetStatus = NULL;
    tRouteCommSetStatus RouteCommSetStatus;
    INT1                i1CommSetStatus;
#ifdef VPLSADS_WANTED
    UINT4               u4AsafiMask;
#endif

    i1CommSetStatus = COMM_MODIFY;

#ifdef VPLSADS_WANTED
    /*ADS-VPLS related processing */
    /* ROUTES_COMM_SET_STATUS_TBL has prefixes for IPv4/Ipv6 family 
       and does not contain l2vpn,vpls prefixes */
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pSelRouteProfile),
                           BGP4_RT_SAFI_INFO (pSelRouteProfile), u4AsafiMask);
    if (CAP_MP_L2VPN_VPLS == u4AsafiMask)
    {
        if (BGP4_RT_PROTOCOL (pSelRouteProfile) != BGP_ID)
        {
            /* local-route */
            return COMM_SET_NONE;
        }
        else
        {
            /* RR case fwd the received comm ,if any */
            return COMM_MODIFY;
        }
    }
#endif

    /* To Get all the communities configured for this route         */
    /* The key is the number of ones in u4IpNetwork and u1PrefixLen */
    Bgp4CopyNetAddressStruct (&((RouteCommSetStatus.RtNetAddress)),
                              BGP4_RT_NET_ADDRESS_INFO (pSelRouteProfile));
    RouteCommSetStatus.u4Context = BGP4_RT_CXT_ID (pSelRouteProfile);
    pRouteCommSetStatus = BGP4_RB_TREE_GET (ROUTES_COMM_SET_STATUS_TBL,
                                            (tBgp4RBElem
                                             *) (&RouteCommSetStatus));
    if (pRouteCommSetStatus != NULL)
    {
        i1CommSetStatus = pRouteCommSetStatus->i1CommSetStatus;
    }

    return i1CommSetStatus;
}

/*****************************************************************************/
/*    Function Name   : NoExportHandler                                      */
/*                                                                           */
/*    Description     : Gets the peer classification whether it is an        */
/*                      external peer                                        */
/*                                                                           */
/*    Input(s)        : pPeerInfo - pointer to peer information.             */
/*                                                                           */
/*    Output(s)       : None                                                 */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*                                                                           */
/*  Returns         :     COMM_TRUE/COMM_FALSE                               */
/*                                                                           */
/*****************************************************************************/
INT1
NoExportHandler (const tBgp4PeerEntry * pPeerInfo)
{
    INT1                i1PeerType;

    /* This should also check for SubConfedaration boundary.            */
    /* So this will undergo change when AS CONFEDARATION is implemented */

    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                  BGP4_MOD_NAME,
                  "\tNoExportHandler : Received NULL Peer Info\n");
        return COMM_FAILURE;
    }

    i1PeerType = BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerInfo), pPeerInfo);

    if ((i1PeerType == BGP4_EXTERNAL_PEER) &&
        (BGP4_CONFED_PEER_STATUS (pPeerInfo) == BGP4_FALSE))
    {
        return COMM_FALSE;
    }
    return COMM_TRUE;
}

/*****************************************************************************/
/*    Function Name   : NoExportSubConfHandler                               */
/*                                                                           */
/*    Description     : Gets the peer classification whether it is an        */
/*                      external peer                                        */
/*                                                                           */
/*    Input(s)        : pPeerInfo - pointer to peer information.             */
/*                                                                           */
/*    Output(s)       : None                                                 */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*                                                                           */
/*  Returns         :     COMM_TRUE/COMM_FALSE                               */
/*                                                                           */
/*****************************************************************************/
INT1
NoExportSubConfHandler (const tBgp4PeerEntry * pPeerInfo)
{
    INT1                i1PeerType;

    /* This should also check for SubConfedaration boundary.            */
    /* So this will undergo change when AS CONFEDARATION is implemented */

    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                  BGP4_MOD_NAME,
                  "\tNoExportSubConfHandler : Received NULL Peer Info\n");
        return COMM_FAILURE;
    }

    i1PeerType = BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerInfo), pPeerInfo);

    if (i1PeerType == BGP4_EXTERNAL_PEER)
    {
        return COMM_FALSE;
    }

    return COMM_TRUE;
}

/*****************************************************************************/
/*    Function Name   : CommGetHashIndex                                     */
/*                                                                           */
/*    Description     : calculates the hash index for the given hash key.    */
/*                                                                           */
/*    Input(s)        : u4HashKey1 - one of the Hash key for which           */
/*                      the hash index to be calculated of size 4 bytes      */
/*                                                                           */
/*                      u1HashKey2 - one of the Hash key for which           */
/*                      the hash index to be calculated of size 1 byte       */
/*                                                                           */
/*    Output(s)       : pu4HashIndex -  Pointer to the Hash Index.           */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*                                                                           */
/*  Returns         :   None                                                 */
/*****************************************************************************/
VOID
CommGetHashIndex (UINT4 u4HashKey1, UINT1 u1HashKey2, UINT4 *pu4HashIndex)
{
    UINT4               u4HashKey;
    UINT1               u1Count;
    UINT1               u1Index;

    u1Count = 0;

    u4HashKey = (((u4HashKey1 & 0xff000000) >> BGP4_THREE_BYTE_BITS) ^
                 u1HashKey2);
    u4HashKey = (((u4HashKey1 & 0x00ff0000) >> BGP4_TWO_BYTE_BITS) ^
                 u1HashKey2) + u4HashKey;
    u4HashKey = (((u4HashKey1 & 0x0000ff00) >> BGP4_ONE_BYTE_BITS) ^ u1HashKey2)
        + u4HashKey;
    u4HashKey = (((u4HashKey1 & 0x000000ff)) ^ u1HashKey2) + u4HashKey;

    for (u1Index = 0; u1Index < BGP4_FOUR_BYTE_BITS; u1Index++)
    {
        if ((u4HashKey & BGP4_MSB) == BGP4_MSB)
        {
            u1Count++;
        }
        u4HashKey = u4HashKey << BGP4_SINGLE_BIT;
    }
    *pu4HashIndex = u1Count;
}
#endif /* _BGCMATTR_C */
