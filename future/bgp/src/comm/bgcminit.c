/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgcminit.c,v 1.13 2013/02/16 11:41:23 siva Exp $
 *
 * Description: This file contains Community Attribute processing
 *              functions.
 *
 *******************************************************************/
#ifndef _BGCMINIT_C
#define _BGCMINIT_C

#define COMM_GLB_VAR

#include "bgp4com.h"

/*****************************************************************************/
/*    Function Name   : CommInit                                             */
/*    Description     : This module does                                     */
/*                      Allocates memory for                                 */
/*                        - Incoming community filter table entries          */
/*                        - Outgoing community filter table entries          */
/*                        - Routes community configuration table entries     */
/*                        - Route Community Path memory units.               */
/*                        - Community profiles                               */
/*                        - Peer Community Send Status memory units          */
/*                      Creates Hash Table for                               */
/*                        - incoming community filter table- outgoing        */
/*                          community filter table                           */
/*                        - routes community additive configuration table    */
/*                        - routes community delete configuration table      */
/*                        - Peer Community Send Status configuration         */
/*                          table                                            */
/*  If initialisation is successful this returns success to FutureBGP4.      */
/*  Else returns failure to FutureBGP4.                                      */
/*                                                                           */
/*  Input(s)        : MaxRoutes indicates the maximum number of routes the   */
/*                    BGP Routing table can store.                           */
/*                                                                           */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*  INPUT_COMM_FILTER_ENTRY_POOL_ID, OUTPUT_COMM_FILTER_ENTRY_POOL_ID        */
/*  ROUTE_CONF_COMM_POOL_ID, COMM_PROFILE_POOL_ID,                           */
/*  ROUTES_COMM_SET_STATUS_POOL_ID                                           */
/*                                                                           */
/*  COMM_INPUT_FILTER_TBL, COMM_OUTPUT_FILTER_TBL,                           */
/*  ROUTES_COMM_ADD_TBL, ROUTES_COMM_DELETE_TBL,                             */
/*  ROUTES_COMM_SET_STATUS_TBL                                               */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*  Returns         :  COMM_SUCCESS or COMM_FAILURE                          */
/*****************************************************************************/
INT4
CommInit (INT4 i4MaxRoutes)
{

    UNUSED_PARAM (i4MaxRoutes);
    ROUTES_COMM_ADD_TBL = 0;
    ROUTES_COMM_DELETE_TBL = 0;
    COMM_INPUT_FILTER_TBL = 0;
    COMM_OUTPUT_FILTER_TBL = 0;
    ROUTES_COMM_SET_STATUS_TBL = 0;

    /*  HASH TBL creation for the Tables --
     *  COMM_INPUT_FILTER_TBL, COMM_OUTPUT_FILTER_TBL,
     *  ROUTES_COMM_ADD_TBL, ROUTES_COMM_DELETE_TBL,
     *  ROUTES_COMM_SET_STATUS_TBL
     */

    COMM_INPUT_FILTER_TBL =
        BGP4_RB_TREE_CREATE (FsBGPSizingParams
                             [MAX_BGP_COMM_IN_FILTER_INFOS_SIZING_ID].
                             u4PreAllocatedUnits, CommFiltTblCmpFunc);

    if (COMM_INPUT_FILTER_TBL == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tCommInit() : Creation of Input Comm Filter Hash Tbl "
                  "FAILED !!!\n");
        CommShut ();
        return COMM_FAILURE;
    }

    COMM_OUTPUT_FILTER_TBL =
        BGP4_RB_TREE_CREATE (FsBGPSizingParams
                             [MAX_BGP_COMM_OUT_FILTER_INFOS_SIZING_ID].
                             u4PreAllocatedUnits, CommFiltTblCmpFunc);

    if (COMM_OUTPUT_FILTER_TBL == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tCommInit() : Creation of Output Comm Filter Hash Tbl "
                  "FAILED !!!\n");
        CommShut ();
        return COMM_FAILURE;
    }

    ROUTES_COMM_ADD_TBL =
        BGP4_RB_TREE_CREATE (FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                             u4PreAllocatedUnits, CommRtTblCmpFunc);

    if (ROUTES_COMM_ADD_TBL == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tCommInit() : Creation of Comm Add_Conf Hash Tbl "
                  "FAILED !!!\n");
        CommShut ();
        return COMM_FAILURE;
    }

    ROUTES_COMM_DELETE_TBL =
        BGP4_RB_TREE_CREATE (FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                             u4PreAllocatedUnits, CommRtTblCmpFunc);

    if (ROUTES_COMM_DELETE_TBL == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tCommInit() : Creation of Comm Del_Conf Hash Tbl "
                  "FAILED !!!\n");
        CommShut ();
        return COMM_FAILURE;
    }

    ROUTES_COMM_SET_STATUS_TBL =
        BGP4_RB_TREE_CREATE (FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                             u4PreAllocatedUnits, CommRtStatTblCmpFunc);

    if (ROUTES_COMM_SET_STATUS_TBL == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tCommInit() : Creation of Routes Set Status Hash Tbl "
                  "FAILED !!!\n");
        CommShut ();
        return COMM_FAILURE;
    }
    return COMM_SUCCESS;
}

/*****************************************************************************/
/*    Function Name   : CommShut                                             */
/*    Description     : This module does                                     */
/*                      Free memory of                                       */
/*                        - Incoming community filter table entries          */
/*                        - Outgoing community filter table entries          */
/*                        - Routes community configuration table entries     */
/*                        - Route Community Path memory units.               */
/*                        - Community profiles                               */
/*                        - Peer Community Send Status memory units          */
/*                      Free Hash Table of                                   */
/*                        - incoming community filter table                  */
/*                        - outgoing community filter table                  */
/*                        - routes community additive configuration table    */
/*                        - routes community delete configuration table      */
/*                        - Peer Community Send Status configuration         */
/*                          table                                            */
/*  If shut down is successful this returns success                          */
/*  Else returns failure                                                     */
/*                                                                           */
/*  Input(s)       : None                                                    */
/*                                                                           */
/*  Output(s)       : None                                                   */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*  INPUT_COMM_FILTER_ENTRY_POOL_ID, OUTPUT_COMM_FILTER_ENTRY_POOL_ID        */
/*  ROUTE_CONF_COMM_POOL_ID, COMM_PROFILE_POOL_ID,                           */
/*  ROUTES_COMM_SET_STATUS_POOL_ID                                           */
/*                                                                           */
/*  COMM_INPUT_FILTER_TBL, COMM_OUTPUT_FILTER_TBL,                           */
/*  ROUTES_COMM_ADD_TBL, ROUTES_COMM_DELETE_TBL,                             */
/*  ROUTES_COMM_SET_STATUS_TBL                                               */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*  Returns         :  COMM_SUCCESS or COMM_FAILURE                          */
/*****************************************************************************/
INT4
CommShut (VOID)
{
    INT4                i4CommShutDownStatus = COMM_SUCCESS;

    /*  HASH TBL Free for the Tables --
     *  COMM_INPUT_FILTER_TBL, COMM_OUTPUT_FILTER_TBL,
     *  ROUTES_COMM_ADD_TBL, ROUTES_COMM_DELETE_TBL,
     *  ROUTES_COMM_SET_STATUS_TBL
     */

    if (COMM_INPUT_FILTER_TBL != NULL)
    {
        BGP4_RB_TREE_DELETE (COMM_INPUT_FILTER_TBL);
        COMM_INPUT_FILTER_TBL = NULL;
    }

    if (COMM_OUTPUT_FILTER_TBL != NULL)
    {
        BGP4_RB_TREE_DELETE (COMM_OUTPUT_FILTER_TBL);
        COMM_OUTPUT_FILTER_TBL = NULL;
    }

    if (ROUTES_COMM_ADD_TBL != NULL)
    {
        BGP4_RB_TREE_DELETE (ROUTES_COMM_ADD_TBL);
        ROUTES_COMM_ADD_TBL = NULL;
    }

    if (ROUTES_COMM_DELETE_TBL != NULL)
    {
        BGP4_RB_TREE_DELETE (ROUTES_COMM_DELETE_TBL);
        ROUTES_COMM_DELETE_TBL = NULL;
    }

    if (ROUTES_COMM_SET_STATUS_TBL != NULL)
    {
        BGP4_RB_TREE_DELETE (ROUTES_COMM_SET_STATUS_TBL);
        ROUTES_COMM_SET_STATUS_TBL = NULL;
    }

    /*  Freeing the Created MemPools
     *  INPUT_COMM_FILTER_ENTRY_POOL_ID,
     *  OUTPUT_COMM_FILTER_ENTRY_POOL_ID,
     *  ROUTE_CONF_COMM_POOL_ID, EXT_COMM_PROFILE_POOL_ID,
     *  ROUTES_COMM_SET_STATUS_POOL_ID
     */

    INPUT_COMM_FILTER_ENTRY_POOL_ID = 0;

    OUTPUT_COMM_FILTER_ENTRY_POOL_ID = 0;

    ROUTE_CONF_COMM_POOL_ID = 0;

    COMM_PROFILE_POOL_ID = 0;

    ROUTES_COMM_SET_STATUS_POOL_ID = 0;

    return i4CommShutDownStatus;
}

/****************************************************************************/
/* Function Name   : CommRtTblCmpFunc                                       */
/* Description     : user compare function for  COM ADD & DEL TBL RBTrees   */
/* Input(s)        : Two RBTree Nodes be compared                           */
/* Output(s)       : None                                                   */
/* Returns         : 1/(-1)/0                                               */
/****************************************************************************/
INT4
CommRtTblCmpFunc (tBgp4RBElem * pRBElem1, tBgp4RBElem * pRBElem2)
{
    tNetAddress         NetAddress1 =
        ((tRouteConfComm *) pRBElem1)->RtNetAddress;
    tNetAddress         NetAddress2 =
        ((tRouteConfComm *) pRBElem2)->RtNetAddress;
    UINT4               u4Context1 = ((tRouteConfComm *) pRBElem1)->u4Context;
    UINT4               u4Context2 = ((tRouteConfComm *) pRBElem2)->u4Context;

    if (u4Context1 > u4Context2)
    {
        return 1;
    }
    else if (u4Context1 < u4Context2)
    {
        return -1;
    }

    if (BGP4_AFI_IN_ADDR_PREFIX_INFO (NetAddress1.NetAddr) >
        BGP4_AFI_IN_ADDR_PREFIX_INFO (NetAddress2.NetAddr))
    {
        return 1;
    }
    else if (BGP4_AFI_IN_ADDR_PREFIX_INFO (NetAddress1.NetAddr) <
             BGP4_AFI_IN_ADDR_PREFIX_INFO (NetAddress2.NetAddr))
    {
        return -1;
    }
    else
    {

        if ((PrefixGreaterThan
             (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress1),
              BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress2))) ==
            BGP4_TRUE)
        {
            return 1;
        }
        else if ((PrefixLessThan
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress1),
                   BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress2))) ==
                 BGP4_TRUE)

        {
            return -1;
        }
        else
        {
            if (BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress1) >
                BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress2))
            {
                return 1;
            }
            else if (BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress1) <
                     BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress2))
            {
                return -1;
            }
            return 0;
        }

    }
}

/****************************************************************************/
/* Function Name   : CommRtStatTblCmpFunc                                   */
/* Description     : user compare function for  COM RT STATUS TBL RBTrees   */
/* Input(s)        : Two RBTree Nodes be compared                           */
/* Output(s)       : None                                                   */
/* Returns         : 1/(-1)/0                                               */
/****************************************************************************/
INT4
CommRtStatTblCmpFunc (tBgp4RBElem * pRBElem1, tBgp4RBElem * pRBElem2)
{

    tNetAddress         NetAddress1 =
        ((tRouteCommSetStatus *) pRBElem1)->RtNetAddress;
    tNetAddress         NetAddress2 =
        ((tRouteCommSetStatus *) pRBElem2)->RtNetAddress;
    UINT4               u4Context1 =
        ((tRouteCommSetStatus *) pRBElem1)->u4Context;
    UINT4               u4Context2 =
        ((tRouteCommSetStatus *) pRBElem2)->u4Context;

    if (u4Context1 > u4Context2)
    {
        return 1;
    }
    else if (u4Context1 < u4Context2)
    {
        return -1;
    }

    if (BGP4_AFI_IN_ADDR_PREFIX_INFO (NetAddress1.NetAddr) >
        BGP4_AFI_IN_ADDR_PREFIX_INFO (NetAddress2.NetAddr))
    {
        return 1;
    }
    else if (BGP4_AFI_IN_ADDR_PREFIX_INFO (NetAddress1.NetAddr) <
             BGP4_AFI_IN_ADDR_PREFIX_INFO (NetAddress2.NetAddr))
    {
        return -1;
    }
    else
    {

        if ((PrefixGreaterThan
             (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress1),
              BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress2))) ==
            BGP4_TRUE)
        {
            return 1;
        }
        else if ((PrefixLessThan
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress1),
                   BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress2))) ==
                 BGP4_TRUE)
        {
            return -1;
        }
        else
        {
            if (BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress1) >
                BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress2))
            {
                return 1;
            }
            else if (BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress1) <
                     BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress2))
            {
                return -1;
            }
            return 0;
        }
    }
}

/****************************************************************************/
/* Function Name   : CommFiltTblCmpFunc                                     */
/* Description     : user compare function for  COM IN&OUT FILT TBL RBTrees */
/* Input(s)        : Two RBTree Nodes be compared                           */
/* Output(s)       : None                                                   */
/* Returns         : 1/(-1)/0                                               */
/****************************************************************************/
INT4
CommFiltTblCmpFunc (tBgp4RBElem * pRBElem1, tBgp4RBElem * pRBElem2)
{
    UINT4               u4Comm1 = ((tCommFilterInfo *) pRBElem1)->u4Comm;
    UINT4               u4Comm2 = ((tCommFilterInfo *) pRBElem2)->u4Comm;
    UINT4               u4Context1 = ((tCommFilterInfo *) pRBElem1)->u4Context;
    UINT4               u4Context2 = ((tCommFilterInfo *) pRBElem2)->u4Context;

    if (u4Context1 > u4Context2)
    {
        return 1;
    }
    else if (u4Context1 < u4Context2)
    {
        return -1;
    }

    if (u4Comm1 < u4Comm2)
    {
        return -1;
    }
    if (u4Comm1 > u4Comm2)
    {
        return 1;
    }
    return 0;
}

/*****************************************************************************/
/*    Function Name   :  Bgp4GetMaxPeers                                     */
/*    Description     :  This module returns the maximum number of peers     */
/*                                                                           */
/*  Input(s)       : None                                                    */
/*                                                                           */
/*  Output(s)      : Maximum Peers                                           */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*  Returns         :  None                                                  */
/*****************************************************************************/
INT4
Bgp4GetMaxPeers (VOID)
{
    return (FsBGPSizingParams[MAX_BGP_PEERS_SIZING_ID].u4PreAllocatedUnits);
}

#endif /* ifndef _BGCMINIT_C */
