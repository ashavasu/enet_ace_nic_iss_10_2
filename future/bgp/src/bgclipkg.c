/****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgclipkg.c,v 1.50 2017/09/15 06:19:52 siva Exp $
 *
 * Description: 
 * Future BGP Command Mappings
 *
 * This document captures the Cisco CLI commands that need to be supported in
 * BGP in the form of Cisco-like-CLI plug-ins. For details on the command,
 * DEFAULT values and parameter usages, please refer to the Cisco 
 * Configuration document attach with this document in the appendix.
 *
 * This document presents the plug-ins that would be implemented in the
 * FutureBGP code and the function prototypes for the plug-ins.
 *
 * A few notes on the document are below.
 *
 * 1. Most functions will have a parameter named u4Set. This parameter takes
 *    values:
 *
 * #define    VAL_NO         (0)
 * #define    VAL_SET        (1)
 * 
 * VAL_SET would instantiate the values to the data structures and VAL_NO
 * would represent the no form of the command (like Cisco CLI).
 * 
 * 2. Some SET/RESET kind of configuration values get mapped to the
 * following parameters values in the functions. These values are:
 *
 * #define    VAL_SET        (1)
 * #define    VAL_INVALID    (2)
 *
 * VAL_SET means that the particular parameter is SET (or enabled).
 * VAL_INVALID means that the parameter was not specified in the CLI at all
 * (i.e. the parameter might have been optional and need not always be given
 * as part of the CLI command. This will be clear as we start looking at the
 * commands below.
 *
 * 3. The items under Change Notes indicate the items that will require
 * changes in the current FSBGP implementation. The items in RED color
 * indicate that the item would be taken up for implementation at a later
 * phase.
 *
 * 4. The command under the heading Current Command Support will indicate the
 * implementation that would be taken up immediately. This will usually be a
 * subset of the Cisco command excluding the items specified in RED color in
 * Change Notes head.
 *
 ****************************************************************************/

#ifndef BGCLIPKG_C
#define BGCLIPKG_C

#ifdef SNMP_WANTED
#include "snmcmacs.h"
#include "snmcdefn.h"
#include "snmccons.h"
#endif
#include "bgp4com.h"

#ifdef CLI_WANTED
#include "cli.h"
#endif
/* CLI utility function declarations */
PRIVATE INT4        bgpCliPkgGetRouteAttributes (tRouteProfile *,
                                                 tShowIpBgpRoute *, UINT1);
INT4                bgpGetIpv4NeighborAttributes (UINT4, tIpBgpNeighbor *);
#ifdef L3VPN
PRIVATE INT4
 
 
 
 bgpGetRouteLabels (tRouteProfile * pMatchRoute,
                    tShowIpBgpRoute * pShowIpBgpRoute);
#endif
#ifdef BGP4_IPV6_WANTED
PRIVATE INT4        bgpGetIpv6NeighborAttributes (tAddrPrefix *,
                                                  tIpBgpNeighbor *);
#endif
/*****************************************************************************/
/* Function Name : bgpShowBgpVersion                                         */
/* CLI Command   : show bgp-version                                          */
/* Description   : Returns the BGP Version                                   */
/* Input(s)      : None                                                      */
/* Output(s)     : Future BGP Version (pu4Version)                           */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/

INT4
bgpShowBgpVersion (UINT4 *pu4Version)
{
    BGP4_ASSERT (pu4Version);

    *pu4Version = BGP4_VERSION_4;
    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : bgpConfigRouterBgp                                        */
/* CLI Command   : (a) router bgp {autonomous-system}                        */
/*               : (b) no router bgp {autonomous-system}                     */
/* Description   : Command (a), while called for the first time sets the     */
/*               : Local AS to the given value. "no bgp shutdown" command    */
/*               : needs to be called explicitly inorder to make global      */
/*               : admin UP. If command (a) is called with different AS no   */
/*               : before command (b) is called, then operation fails.       */
/*               : If Command (b) is called with the same AS no as that of   */
/*               : Command (a), then BGP Global Admin Status is made DOWN    */
/*               : and the Local AS is resetted.                             */
/* Input(s)      : Bgp Speaker's AS number (pu4AsNo)                         */
/*               : Operation to be performed (u1Set)                         */
/*               : u1Set = VAL_SET for command (a) and                       */
/*               : u1Set = VAL_NO for command (b)                            */
/* Output(s)     : Current AS number if (a) is called with different AS      */
/*               : number, when the BGP Session is already UP and running    */
/*               : (pu4AsNo)                                                 */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigRouterBgp (UINT4 u4AsNo,    /* AS number */
                    UINT1 u1Set    /* VAL_SET/VAL_NO */
    )
{
    tBgp4QMsg          *pQMsg = NULL;
    UINT4               u4ErrorCode = 0;
    UINT4               u4LocalAS = BGP4_INV_AS;
    INT4                i4GlobalAdmin = 0;
    INT1                i1RetVal;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    nmhGetBgpLocalAs ((INT4 *) &u4LocalAS);
    nmhGetFsbgp4GlobalAdminStatus (&i4GlobalAdmin);

    if ((u4LocalAS != BGP4_INV_AS) &&
        (u1Set == VAL_SET) && (u4AsNo == u4LocalAS))
    {                            /* Local AS already configured and equal to input AS */
        if (i4GlobalAdmin == BGP4_ADMIN_UP)
        {
            return BGP4_SUCCESS;
        }
    }

    if ((u1Set == VAL_NO) && (u4LocalAS == BGP4_INV_AS))
    {                            /* BGP already not active */
        return BGP4_CLI_ERR_BGP_INACTIVE;
    }

    if ((u4LocalAS != BGP4_INV_AS) && (u4AsNo != BGP4_INV_AS) &&
        (u4AsNo != u4LocalAS) && (i4GlobalAdmin == BGP4_ADMIN_UP))
    {                            /* Mismatch between Local AS and input AS */
        u4AsNo = u4LocalAS;
        return BGP4_CLI_ERR_BGP_AS_MISMATCH;
    }

    switch (u1Set)
    {
        case VAL_SET:
            i1RetVal = nmhTestv2Fsbgp4LocalAs (&u4ErrorCode, u4AsNo);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigureRouterBgp() : Invalid AS\n");
                return BGP4_CLI_ERR_INVALID_AS_NO;
            }

            i1RetVal = nmhSetFsbgp4LocalAs (u4AsNo);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                          BGP4_MOD_NAME,
                          "\tbgpConfigureRouterBgp() : Unable to configure AS\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal =
                nmhTestv2Fsbgp4GlobalAdminStatus (&u4ErrorCode, BGP4_ADMIN_UP);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                          BGP4_MOD_NAME,
                          "\tbgpConfigureRouterBgp() : Invalid Input value\n");
                nmhSetFsbgp4LocalAs (BGP4_INV_AS);
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhSetFsbgp4GlobalAdminStatus (BGP4_ADMIN_UP);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                          BGP4_MOD_NAME,
                          "\tbgpConfigureRouterBgp() : Unable to Start BGP\n");
                nmhSetFsbgp4LocalAs (BGP4_INV_AS);
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            break;

        case VAL_NO:

            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                          BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                          "\tMemory Allocation for posting BGP Shutdown Msg "
                          "to BGP Queue FAILED!!!\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_CLI_NO_ROUTER_BGP_EVENT;
            BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
            BGP4_INPUTQ_CXT (pQMsg) = u4Context;
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                          BGP4_MOD_NAME,
                          "\tbgpConfigureRouterBgp() : Unable to Reset AS\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            break;
        default:
            break;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpShutdown                                               */
/* CLI Command   : (a) shutdown                                              */
/*               : (b) no shutdown                                           */
/* Description   : Command (a), sets the global admin status of the BGP      */
/*               : Speaker DOWN. Local AS no is not affected.                */
/*               : Command (b), sets the global admin status of the BGP      */
/*               : Speaker UP. This command needs to be called after         */
/*               : "router bgp {AS no}" command inorder to make BGP Speaker  */
/*               : functionally active.                                      */
/* Input(s)      : Operation to be performed (u1Set)                         */
/*               : u1Set = VAL_SET for command (a) and                       */
/*               : u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpShutdown (UINT1 u1Set)        /* VAL_SET/VAL_NO */
{
    UINT4               u4ErrorCode = 0;
    INT4                i4GlobalAdmin;
    INT1                i1RetVal;

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    nmhGetFsbgp4GlobalAdminStatus (&i4GlobalAdmin);

    if (((u1Set == VAL_SET) && (i4GlobalAdmin == BGP4_ADMIN_DOWN)) ||
        ((u1Set == VAL_NO) && (i4GlobalAdmin == BGP4_ADMIN_UP)))
    {
        return BGP4_SUCCESS;
    }

    switch (u1Set)
    {
        case VAL_SET:
            /* Global Admin needs to be set DOWN */
            i1RetVal =
                nmhTestv2Fsbgp4GlobalAdminStatus (&u4ErrorCode,
                                                  BGP4_ADMIN_DOWN);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                          BGP4_MOD_NAME,
                          "\tbgpShutdown() : Invalid Input value\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhSetFsbgp4GlobalAdminStatus (BGP4_ADMIN_DOWN);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                          BGP4_MOD_NAME,
                          "\tbgpShutdown() : Unable make Global Admin DOWN\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            break;

        case VAL_NO:
            /* Global Admin needs to bg set UP */
            i1RetVal =
                nmhTestv2Fsbgp4GlobalAdminStatus (&u4ErrorCode, BGP4_ADMIN_UP);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                          BGP4_MOD_NAME,
                          "\tbgpShutdown() : Invalid Input value\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhSetFsbgp4GlobalAdminStatus (BGP4_ADMIN_UP);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                          BGP4_MOD_NAME,
                          "\tbgpShutdown() : Unable make Global Admin UP\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            break;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigRouterId                                         */
/* CLI Command   : (a) bgp router-id {ip-address}                            */
/*               : (b) no bgp router-id {ip-address}                         */
/* Description   : Sets Bgp Identifier. By default, BGP Identifier will be   */
/*               : highest interface address. Command (a) configures the     */
/*               : BGP Identifier to the given address. Command (b) resets   */
/*               : the BGP Identifier to the default value. If u1Set is      */
/*               : VAL_NO, irrespective of the u4RouterId (i.e., u4RouterId  */
/*               : can be 0 or any value), the BGP Identifier will be set to */
/*               : the default value. If the BGP Identifier changes, the     */
/*               : Peer Sessions will be restarted.                          */
/* Input(s)      : Bgp Router Identifier value to be set (u4RouterId)        */
/*               : Operation to be performed (u1Set)                         */
/*               : u1Set = VAL_SET for command (a) and                       */
/*               : u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigRouterId (UINT4 u4RouterId,    /* Bgp Identifier */
                   UINT1 u1Set    /* VAL_SET/VAL_NO */
    )
{
    UINT4               u4ErrorCode = 0;
    INT1                i1RetVal;

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    i1RetVal = nmhTestv2Fsbgp4Identifier (&u4ErrorCode, u4RouterId);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigureRouterBgp() : Invalid Bgp Identifier\n");
        return BGP4_CLI_ERR_INVALID_BGP_IDENTIFIER;
    }

    switch (u1Set)
    {
        case VAL_SET:
            i1RetVal = nmhSetFsbgp4Identifier (u4RouterId);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                          BGP4_MOD_NAME,
                          "\tbgpConfigureRouterBgp() : Unable to configure "
                          "Bgp Identifier\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            break;

        case VAL_NO:
            i1RetVal = nmhSetFsbgp4Identifier (BGP4_INV_IPADDRESS);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                          BGP4_MOD_NAME,
                          "\tbgpConfigureRouterBgp() : Unable to configure "
                          "Bgp Identifier\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            break;
        default:
            break;
    }
    return BGP4_SUCCESS;

}

/*****************************************************************************/
/* Function Name : bgpConfigDefaultLocalPreference                           */
/* CLI Command   : (a) bgp default local-preference {value}                  */
/*               : (b) no bgp default local-preference {value}               */
/* Description   : Sets the Default local preference value. If no Local      */
/*               : preference is configured for the route advertised to the  */
/*               : internal peer, then the route will be advertised with     */
/*               : this default Local Preference value. Command (a)          */
/*               : configures the Default Local Preference to the given      */
/*               : input value. Command (b) will reset the Default Local     */
/*               : preference to the default value                           */
/* Input(s)      : Bgp Local Prefernce value to be set (u4LocalPreference)   */
/*               : Operation to be performed (u1Set)                         */
/*               : u1Set = VAL_SET for command (a) and                       */
/*               : u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigDefaultLocalPreference (UINT4 u4LocalPreference,    /*Local Pref */
                                 UINT1 u1Set    /* VAL_SET/VAL_NO */
    )
{
    UINT4               u4ErrorCode = 0;
    INT1                i1RetVal;

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    i1RetVal = nmhTestv2Fsbgp4DefaultLocalPref (&u4ErrorCode,
                                                u4LocalPreference);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigDefaultLocalPreference() : Invalid Local Pref\n");
        return BGP4_CLI_ERR_INVALID_LOCAL_PREF;
    }

    switch (u1Set)
    {
        case VAL_SET:
            i1RetVal = nmhSetFsbgp4DefaultLocalPref (u4LocalPreference);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                          BGP4_MOD_NAME,
                          "\tbgpConfigDefaultLocalPreference() : Unable to "
                          "configure Local Preference Value\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            break;

        case VAL_NO:
            i1RetVal = nmhSetFsbgp4DefaultLocalPref (BGP4_DEF_LP);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                          BGP4_MOD_NAME,
                          "\tbgpConfigDefaultLocalPreference() : Unable to "
                          "configure Local Preference Value\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            break;
        default:
            break;
    }
    return BGP4_SUCCESS;

}

/*****************************************************************************/
/* Function Name : bgpConfigNeighborRemoteAs                                 */
/* CLI Command   : (a) neighbor {ip-address|peer-group-name}                 */
/*                          remote-as <AS no>                                */
/*               : (b) no neighbor {ip-address|peer-group-name}              */
/*                          remote-as <AS no>                                */
/* Description   : This command creates/deletes a Peer. Command (a), will    */
/*               : look-up for the Peer matching the given IP address. If no */
/*               : match is found, then a new entry for the Peer is created  */
/*               : with the Default configuration values, the Peer AS is set */
/*               : with the given Peer AS and the Peer Session is initiated. */
/*               : If an existing peer is found for the given IP address and */
/*               : if the Peer AS does not match, then the current session   */
/*               : is disabled, the peer AS set for the new AS value, and    */
/*               : peer session is re-initiated. Command (b), will delete    */
/*               : the peer matching the given IP address.                   */
/* Input(s)      : Remote AS of the Peer (u4RemoteAs)                        */
/*               : IP address of the Peer (pu4IpAddress)                     */
/*               : Group Name of the Peer (pu1PeerGroupName)                 */
/*               : Operation to be performed (u1Set)                         */
/*               : u1Set = VAL_SET for command (a) and                       */
/*               : u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/* Note          : This release the Peer is created or deleted based on the  */
/*               : the IpAddress and the PeerGroupName is always NULL. This  */
/*               : feauture will be take up in later release.                */
/*****************************************************************************/
INT4
bgpConfigNeighborRemoteAs (UINT4 u4RemoteAs,    /* Peer's AS number */
                           tAddrPrefix * pIpAddress,    /* Peer's remote addr */
                           UINT1 *pu1PeerGroupName,    /* Peer's Group name */
                           UINT1 u1Set    /* VAL_SET/VAL_NO */
    )
{
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tAddrPrefix         PeerRemAddr;
    tBgp4PeerEntry     *pPeer = NULL;
    UINT4               u4Context;
#ifdef L3VPN
    UINT1               au1CliPrompt[MAX_PROMPT_LEN];
#endif
    UINT4               u4ErrorCode = 0;
    UINT4               u4CurRemoteAs = BGP4_INV_AS;
    UINT4               u4RemAddr = 0;
    INT4                i4PeerAdminState = BGP4_PEER_STOP;
    INT4                i4Err = 0;
#ifdef L3VPN
    INT4                i4RetSts;
#endif
    INT1                i1RetVal = 0;
    UINT1               au1PeerOctetList[BGP4_PEER_OCTET_LIST_LEN];

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));
    BGP4_ASSERT (pu1PeerGroupName == NULL);

#ifdef L3VPN
    i4RetSts = Bgp4CliGetCurPrompt (au1CliPrompt);
    if (i4RetSts == BGP4_FAILURE)
    {
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }
    if (STRCMP (au1CliPrompt, BGP4_IPV4_VPNV4_FAMLY_MODE) == 0)
    {
        /*Nbr should be configued in general mode and should be activated in            Vpnv4 Address Family mode */
        return BGP4_CLI_ERR_VPN4_FAMLY_NBR_CONFIG_FAIL;
    }
#endif
    MEMSET (au1PeerOctetList, 0, BGP4_PEER_OCTET_LIST_LEN);
    PeerAddr.pu1_OctetList = au1PeerOctetList;
    MEMCPY (PeerAddr.pu1_OctetList, pIpAddress->au1Address,
            pIpAddress->u2AddressLen);
    PeerAddr.i4_Length = pIpAddress->u2AddressLen;

    i1RetVal = nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable (pIpAddress->u2Afi,
                                                              &PeerAddr);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigNeighborRemoteAs() : Invalid Peer ADDR\n");
        return BGP4_CLI_ERR_INV_PEER_ADDR;
    }

    switch (pIpAddress->u2Afi)
    {
        case BGP4_INET_AFI_IPV4:
            PTR_FETCH4 (u4RemAddr, pIpAddress->au1Address);
            Bgp4InitAddrPrefixStruct (&PeerRemAddr, BGP4_INET_AFI_IPV4);
            MEMCPY (PeerRemAddr.au1Address, &u4RemAddr, sizeof (UINT4));
            break;
        case BGP4_INET_AFI_IPV6:
            PeerRemAddr = *pIpAddress;
            break;
        default:
            break;
    }

    switch (u1Set)
    {
        case VAL_SET:
            i1RetVal = nmhTestv2Fsbgp4mpePeerExtPeerRemoteAs (&u4ErrorCode,
                                                              pIpAddress->u2Afi,
                                                              &PeerAddr,
                                                              u4RemoteAs);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigNeighborRemoteAs() : Invalid Peer AS\n");
                return (BGP4_CLI_ERR_PEER_INVALID_AS_NO);
            }
            pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerRemAddr);
            if (pPeer != NULL)
            {
                u4CurRemoteAs = (UINT4) (BGP4_PEER_ASNO (pPeer));
                i4PeerAdminState = (INT4) (BGP4_PEER_ADMIN_STATUS (pPeer));
            }

            if ((u4CurRemoteAs != BGP4_INV_AS) &&
                (i4PeerAdminState == BGP4_PEER_START))
                /* || (i4PeerAdminState == BGP4_PEER_AUTO_START))) */
            {
                if (u4CurRemoteAs == u4RemoteAs)
                {
                    return BGP4_SUCCESS;
                }
                else
                {
                    /* Disable the peer session and restart the peer session
                     * after setting the new AS number. */
                    i1RetVal =
                        nmhSetFsbgp4mpebgpPeerAdminStatus (pIpAddress->u2Afi,
                                                           &PeerAddr,
                                                           BGP4_PEER_STOP);
                    if (i1RetVal == BGP4_CLI_FAILURE)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                                  "\tbgpConfigNeighborRemoteAs() : Unable to"
                                  " disable the Peer Admin State\n");
                        return (BGP4_CLI_ERR_PEER_DELETE_FAILS);
                    }

                    /* Configure remote AS for the peer */
                    BGP4_PEER_ASNO (pPeer) = (UINT2) u4RemoteAs;

                    /* check if Confed is configured  and mark, 
                     * if peer is confed peer */
                    if (BGP4_CONFED_ID (u4Context) != BGP4_INV_AS)
                    {
                        UINT4               u4Indx = 0;

                        for (; u4Indx < BGP4_CONFED_MAX_MEMBER_AS; u4Indx++)
                        {
                            if (BGP4_PEER_ASNO (pPeer) ==
                                BGP4_CONFED_PEER (u4Context, u4Indx))
                            {
                                BGP4_CONFED_PEER_STATUS (pPeer) = BGP4_TRUE;
                                break;
                            }
                        }
                    }
                    /* Create the peer */
                    i1RetVal =
                        nmhSetFsbgp4mpebgpPeerAdminStatus (pIpAddress->u2Afi,
                                                           &PeerAddr,
                                                           BGP4_PEER_START);
                    if (i1RetVal == BGP4_CLI_FAILURE)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                                  "\tbgpConfigNeighborRemoteAs() : Unable to "
                                  "Configure the Peer Admin State\n");
                        return (BGP4_CLI_ERR_PEER_CREATE_FAIL);
                    }
                    break;
                }
            }

            if ((u4CurRemoteAs == u4RemoteAs) && (pPeer != NULL) &&
                ((BGP4_GET_PEER_PEND_FLAG (pPeer) & BGP4_PEER_DELETE_PENDING)
                 == BGP4_PEER_DELETE_PENDING))
            {
                return (BGP4_CLI_ERR_PEER_CREATE_FAIL);
            }

            if (u4CurRemoteAs == BGP4_INV_AS)
            {
                /* Peer Entry may not be existing or existing with a
                 * INVALID AS no. Try creating a peer entry. */
                i1RetVal =
                    nmhSetFsbgp4mpePeerExtConfigurePeer (pIpAddress->u2Afi,
                                                         &PeerAddr,
                                                         BGP4_PEER_CREATE);
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigureRouterBgp() : Unable to Create Peer\n");
                    return (BGP4_CLI_ERR_PEER_CREATE_FAIL);
                }
                i1RetVal =
                    nmhSetFsbgp4mpePeerExtPeerRemoteAs (pIpAddress->u2Afi,
                                                        &PeerAddr, u4RemoteAs);
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigNeighborRemoteAs() : Unable to "
                              "Configure the Peer Remote AS\n");
                    return (BGP4_CLI_ERR_GENERAL_ERROR);
                }
            }
            i1RetVal = nmhSetFsbgp4mpebgpPeerAdminStatus (pIpAddress->u2Afi,
                                                          &PeerAddr,
                                                          BGP4_PEER_START);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigNeighborRemoteAs() : Unable to "
                          "Configure the Peer Admin State\n");
                return (BGP4_CLI_ERR_GENERAL_ERROR);
            }
            break;

        case VAL_NO:
            i1RetVal = nmhGetFsbgp4mpePeerExtConfigurePeer (pIpAddress->u2Afi,
                                                            &PeerAddr, &i4Err);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                /* Peer entry for given IP addres not exising. Return */
                return BGP4_SUCCESS;
            }
            i1RetVal = nmhSetFsbgp4mpePeerExtConfigurePeer (pIpAddress->u2Afi,
                                                            &PeerAddr,
                                                            BGP4_PEER_DELETE);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigNeighborRemoteAs() : Unable to "
                          "Configure the Peer Admin State\n");
                return (BGP4_CLI_ERR_PEER_DELETE_FAILS);
            }
            break;
        default:
            break;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigNonBgpRtAdvt                                     */
/* CLI Command   : (a) bgp nonbgprt-advt {external | both}                   */
/*               : (b) no bgp nonbgprt-advt {external | both}                */
/* Description   : This configuration controls the advertisement of non-BGP  */
/*               : routes. The non-BGP routes can be configured to be        */
/*               : advertised only to the external peer or can be advertised */
/*               : to internal and external peers. By default the non-BGP    */
/*               : routes are advertiesd to  both internal & external peers. */
/*               : Command (a) will set the policy as either external or     */
/*               : both. Command (b) will set the policy to its default      */
/*               : value.                                                    */
/* Input(s)      : Non BGP route Advt Policy (i4NonBgpAdvtPolicy) : Value    */
/*                 - BGP4_EXTERNAL_PEER - For advt only to external peers.   */
/*                 - BGP4_EXT_OR_INT_PEER - For advt to all peers.            */
/*               : u1Set = VAL_SET for command (a) and                       */
/*                 u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigNonBgpRtAdvt (INT4 i4NonBgpAdvtPolicy,    /* Non Bgp Rts Advt policy */
                       UINT1 u1Set /* VAL_SET/VAL_NO */ )
{
    UINT4               u4ErrorCode = 0;
    INT1                i1RetVal = 0;

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    if (u1Set == VAL_NO)
    {
        i4NonBgpAdvtPolicy = BGP4_EXT_OR_INT_PEER;
    }

    i1RetVal = nmhTestv2Fsbgp4AdvtNonBgpRt (&u4ErrorCode, i4NonBgpAdvtPolicy);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigNonBgpRtAdvt: Invalid Non Bgp route Advt Policy\n");
        return BGP4_CLI_ERR_INV_NON_BGP_ADVT_POLICY;
    }

    switch (u1Set)
    {
        case VAL_SET:
            i1RetVal = nmhSetFsbgp4AdvtNonBgpRt (i4NonBgpAdvtPolicy);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigNonBgpRtAdvt : Unable to set "
                          "the non BGP Route Advt Policy\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            break;

        case VAL_NO:
            i1RetVal = nmhSetFsbgp4AdvtNonBgpRt (i4NonBgpAdvtPolicy);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigNonBgpRtAdvt : Unable to re-set "
                          "the non BGP Route Advt Policy\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            break;
        default:
            break;

    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigOverlapPolicy                                    */
/* CLI Command   : (a) bgp overlap-policy {policy}                           */
/*               : (b) no bgp overlap-policy {policy}                        */
/* Description   : Depanding on the policy, either the more-specific routes  */
/*               : alone can be installed, or the less-specific routes can   */
/*               : be installed or both less & more specific routes can be   */
/*               : installed. By default, both less & more specific routes   */
/*               : will be installed. Command (a) will set the policy to     */
/*               : desired value. Command (b) will reset the policy to       */
/*               : default value.                                            */
/* Input(s)      : Overlap Policy (i4OverlapPolicy)                          */
/*               : i4OverlapPolicy can take following values:                */
/*               : BGP4_INSTALL_BOTH_RT    (3) - For installing all routes.  */
/*               : BGP4_INSTALL_LESSSPEC_RT(2) - For installing less specific*/
/*               :                               routes.                     */
/*               : BGP4_INSTALL_MORESPEC_RT(1) - For installing more specific*/
/*               :                               routes.                     */
/*               : u1Set = VAL_SET for command (a) and                       */
/*                 u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigOverlapPolicy (INT4 i4OverlapPolicy,    /* Overlap Routes Policy */
                        UINT1 u1Set /* VAL_SET/VAL_NO */ )
{
    UINT4               u4ErrorCode = 0;
    INT1                i1RetVal = 0;

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    if (u1Set == VAL_NO)
    {
        i4OverlapPolicy = BGP4_INSTALL_BOTH_RT;
    }

    i1RetVal = nmhTestv2Fsbgp4OverlappingRoute (&u4ErrorCode, i4OverlapPolicy);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigOverlapPolicy : Invalid Overlap Policy.\n");
        return BGP4_CLI_ERR_INV_OVERLAP_POLICY;
    }

    switch (u1Set)
    {
        case VAL_SET:
            i1RetVal = nmhSetFsbgp4OverlappingRoute (i4OverlapPolicy);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigOverlapPolicy : Unable to set "
                          "the Overlap Policy\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            break;

        case VAL_NO:
            i1RetVal = nmhSetFsbgp4OverlappingRoute (i4OverlapPolicy);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigNonBgpRtAdvt : Unable to re-set "
                          "the Overlap Policy\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            break;
        default:
            break;

    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigDefRouteOriginate                                */
/* CLI Command   : (a) default-information origination                       */
/*               : (b) no default-information origination                    */
/* Description   : This command controls the redistribution/advertisement    */
/*               : of default route. Command (a) specifies that the default  */
/*               : route can be redistributed. It also enables the           */
/*               : advertisement of default route to other peers.            */
/*               : Command (b) will disable the advertisement and            */
/*               : redistribution of default route.                          */
/* Input(s)      : u1Set = VAL_SET for command (a) and                       */
/*                 u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigDefRouteOriginate (UINT1 u1Set /* VAL_SET/VAL_NO */ )
{
    INT4                i4DefRouteStatus;
    INT1                i1Status;

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    if (u1Set == VAL_SET)
    {
        i4DefRouteStatus = BGP4_DEF_ROUTE_ORIG_ENABLE;
    }
    else
    {
        i4DefRouteStatus = BGP4_DEF_ROUTE_ORIG_DISABLE;
    }

    i1Status = nmhSetFsbgp4DefaultOriginate (i4DefRouteStatus);
    if (i1Status == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigDefRouteOriginate : Unable to Configure Default "
                  "Route Originate Option.\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigRedistribute                                     */
/* CLI Command   : (a) redistribute <static|connected|rip|ospf>              */
/*               : (b) no redistribute <static|connected|rip|ospf>           */
/* Description   : This command controls the redistribution of routes to     */
/*               : BGP from various protocols. Command (a) specifies the     */
/*               : the protocols from which the routes can be redistributed  */
/*               : into BGP. Command (b) disables redistribution of the      */
/*               : specified protocols routes into BGP.                      */
/* Input(s)      : Protocol from which routes are redistributed into BGP     */
/*               : (i4RedisProto). i4RedisProto can take the following value.*/
/*               : BGP4_IMPORT_DIRECT (0x0002) - For redistributing Local    */
/*               :                               routes.                     */
/*               : BGP4_IMPORT_STATIC (0x0004) - For redistributing static   */
/*               :                               routes.                     */
/*               : BGP4_IMPORT_RIP    (0x0080) - For redistributing RIP      */
/*               :                               routes.                     */
/*               : BGP4_IMPORT_OSPF   (0x1000) - For redistributing OSPF     */
/*               :                               routes.                     */
/*               : u1Set = VAL_SET for command (a) and                       */
/*                 u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigRedistribute (INT4 i4RedisProto,    /* Protocol to be redistributed */
                       UINT1 u1Set /* VAL_SET/VAL_NO */ )
{
    UINT4               u4ErrorCode = 0;
    INT1                i1RetVal = 0;

    i1RetVal = nmhTestv2Fsbgp4RRDProtoMaskForEnable (&u4ErrorCode,
                                                     i4RedisProto);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigRedistribute: Redistribution not supported for "
                  "given protocol.\n");
        return BGP4_CLI_ERR_PROTO_REDIS_NOT_SUPPORTED;
    }

    switch (u1Set)
    {
        case VAL_SET:
            i1RetVal = nmhSetFsbgp4RRDProtoMaskForEnable (i4RedisProto);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigRedistribute : Unable to set "
                          "Redistribution for the protocol\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            break;

        case VAL_NO:
            i1RetVal = nmhSetFsbgp4RRDSrcProtoMaskForDisable (i4RedisProto);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigRedistribute : Unable to reset "
                          "Redistribution for the protocol\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            break;
        default:
            break;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigSynchronization                                  */
/* CLI Command   : (a) synchronization                                       */
/*               : (b) no synchronization                                    */
/* Description   : This command enables the synchronization between BGP and  */
/*               : IGP. Command (a) enables the synchronization feauture.    */
/*               : Command (b) disables synchronization.                     */
/* Input(s)      : u1Set = VAL_SET for command (a) and                       */
/*                 u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigSynchronization (UINT1 u1Set /* VAL_SET/VAL_NO */ )
{
    UINT4               u4ErrorCode = 0;
    INT4                i4Synchronisation = 0;
    INT4                i4LocalAS = BGP4_INV_AS;
    INT1                i1RetVal = 0;

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    if (u1Set == VAL_SET)
    {
        i4Synchronisation = BGP4_ENABLE_SYNC;
    }
    else
    {
        i4Synchronisation = BGP4_DISABLE_SYNC;
    }

    i1RetVal = nmhTestv2Fsbgp4Synchronization (&u4ErrorCode, i4Synchronisation);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        nmhGetBgpLocalAs (&i4LocalAS);
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigSynchronization : Invalid Input value.\n");
        if (i4LocalAS == BGP4_INV_AS)
        {
            return BGP4_CLI_ERR_INVALID_AS_NO;
        }
        else
        {
            return BGP4_CLI_ERR_BGP_ADMIN_UP;
        }
    }

    i1RetVal = nmhSetFsbgp4Synchronization (i4Synchronisation);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigSynchronization : Unable to set/reset"
                  "synchronization\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigAlwaysCompareMed                                 */
/* CLI Command   : (a) bgp always-compare-med                                */
/*               : (b) no bgp always-compare-med                             */
/* Description   : This command allows the comparision of MED for paths from */
/*               : neighbors in different AS. By default, MEDs are compared  */
/*               : only for paths received from same neighbor AS.            */
/*               : Command (a) - enables the comparision of MED from         */
/*               : different neighbor AS.                                    */
/*               : Command (b) - disables the MED comparision for paths from */
/*               : different AS and set it to the default mode.              */
/* Input(s)      : u1Set = VAL_SET for command (a) and                       */
/*                 u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigAlwaysCompareMed (UINT1 u1Set /* VAL_SET/VAL_NO */ )
{
    UINT4               u4ErrorCode = 0;
    INT4                i4CompareMed = 0;
    INT4                i4LocalAS = BGP4_INV_AS;
    INT1                i1RetVal = 0;

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    if (u1Set == VAL_SET)
    {
        i4CompareMed = BGP4_MED_COMPARE_ENABLE;
    }
    else
    {
        i4CompareMed = BGP4_MED_COMPARE_DISABLE;
    }

    i1RetVal = nmhTestv2Fsbgp4AlwaysCompareMED (&u4ErrorCode, i4CompareMed);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        nmhGetBgpLocalAs (&i4LocalAS);
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigCompareMed : Invalid Input value.\n");
        if (i4LocalAS == BGP4_INV_AS)
        {
            return BGP4_CLI_ERR_INVALID_AS_NO;
        }
        else
        {
            return BGP4_CLI_ERR_INV_COMP_MED_VALUE;
        }
    }

    i1RetVal = nmhSetFsbgp4AlwaysCompareMED (i4CompareMed);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigCompareMed : Unable to set/reset"
                  "Compare MED value\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigDefaultMetric                                    */
/* CLI Command   : (a) default-metric                                        */
/*               : (b) no default-metric                                     */
/* Description   : This command allows to set the Default metric value that  */
/*               : needs to be applied to the IGP routes. If set the MED of  */
/*               : IGP learned routes will take this value.                  */
/*               : Command (a) - Configures the Default metric value         */
/*               : Command (b) - Resets the Default metric value to 0        */
/* Input(s)      : Default metric value (u4DfltMetric)                       */
/*               : u1Set = VAL_SET for command (a) and                       */
/*                 u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigDefaultMetric (UINT4 u4DfltMetric,    /* Default Metric value */
                        UINT1 u1Set /* VAL_SET/VAL_NO */ )
{
    UINT4               u4Metric = 0;
    UINT4               u4ErrorCode = 0;
    INT1                i1RetVal = 0;

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    if (u1Set == VAL_SET)
    {
        u4Metric = u4DfltMetric;
    }
    else
    {
        u4Metric = BGP4_DEF_IGP_METRIC_VAL;
    }

    i1RetVal = nmhTestv2Fsbgp4RRDDefaultMetric (&u4ErrorCode, u4Metric);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigDefaultMetric : Invalid Input value.\n");
        return BGP4_CLI_ERR_INV_DEF_METRIC_VALUE;
    }

    i1RetVal = nmhSetFsbgp4RRDDefaultMetric (u4Metric);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigDefaultMetric : Unable to set/reset"
                  "Default Metric value\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigDefaultAfiSafi                                   */
/* CLI Command   : (a) bgp default ipv4-unicast                              */
/*               : (b) no bgp default ipv4-unicast                           */
/* Description   : This command allows to set the Default <AFI,SAFI> as      */
/*               : <IPv4, unicast>.                                          */
/*               : Command (a) - Configures the Default afi, safi.           */
/*               : Command (b) - Resets the default afi, safi support        */
/* Input(s)      : u1Set = VAL_SET for command (a) and                       */
/*                 u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigDefaultAfiSafi (UINT1 u1Set /* VAL_SET/VAL_NO */ )
{
    UINT4               u4ErrorCode = 0;
    INT4                i4Status = BGP4_FALSE;
    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    if (u1Set == VAL_SET)
    {
        i4Status = BGP4_TRUE;
    }
    else
    {
        i4Status = BGP4_FALSE;
    }

    if (nmhTestv2Fsbgp4DefaultIpv4UniCast (&u4ErrorCode, i4Status) !=
        SNMP_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigDefaultAfiSafi : Unable to set/reset"
                  "Default <AFI,SAFI> value\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    if (nmhSetFsbgp4DefaultIpv4UniCast (i4Status) != SNMP_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigDefaultAfiSafi : Unable to set/reset"
                  "Default <AFI,SAFI> value\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigMEDTable                                         */
/* CLI Command   : (a) bgp med-table index {index} remote-as {as-no}         */
/*                         {ip-address} {ip-mask}                            */
/*                         [intermediate-as {as-num1, as-num2...as-numN}]    */
/*                         med {med-value} direction {in/out} [override]     */
/*               : (b) no bgp med-table index {index} remote-as {as-no}      */
/*                         {ip-address} {ip-mask}                            */
/*                         [intermediate-as {as-num1, as-num2...as-numN}]    */
/*                         med {med-value} direction {in/out} [override]     */
/* Description   : This command configures/removes an entry in/from the      */
/*               : MED table. When as route is received or send out, the     */
/*               : entries configured in the MED table are applied, if       */
/*               : necessary, to derive the MED to be associated will the    */
/*               : route. Command (a) configures a MED entry and             */
/*               : Command (b) removes the entry from MED table.             */
/* Input(s)      : Index of the MED table entry (u4Index)                    */
/*               :                            - mandatory parameter          */
/*               : Remote AS value (u4Remote AS) - mandatory parameter       */
/*               : IP address of the route (u4IpAddres)-mandatory parameter  */
/*               : MED value that will be set if the route matches the       */
/*               : parameters in the entry.(u4MedValue)-mandatory parameter  */
/*               : Direction in which this entry is applied (i4Direction)    */
/*               :                            - mandatory parameter          */
/*               : -value is BGP4_INCOMING (0x01), for applying in incoming  */
/*               :  direction                                                */
/*               : -value is BGP4_OUTGOING (0x02), for applying in outgoing  */
/*               :  direction                                                */
/*               : Override paramater based on which the decision to either  */
/*               : overwrite the already existing value is taken.            */
/*               : (u4Override). This is an optional parameter. The          */
/*               : -value is VAL_INVALID, if the command is issued without   */
/*               :  this parameter                                           */
/*               : -value is VAL_SET, if the command is issued with          */
/*               :  this parameter                                           */
/*               : Intermediate AS number (pu1IntermediateAsNums). This is   */
/*               : an optional parameter. If non-NULL, this contains a comma */
/*               : separated string of AS-numbers.                           */
/*               : u1Set = VAL_SET for command (a) and                       */
/*               : u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigMEDTable (UINT4 u4Index,    /* Arbitary Index */
#ifdef L3VPN
                   UINT1 *pu1VrfName,    /* Vrf Name */
                   INT4 i4VrfNameLen,    /* Vrf Name Length */
#endif
                   UINT4 u4RemoteAS,    /* Remote AS value to be matched */
                   tNetAddress * pIpAddress,    /* Ip Address to be matched */
                   UINT4 u4MedValue,    /* MED value for this entry */
                   INT4 i4Direction,    /* Direction - in/out */
                   UINT4 u4Override,    /* Override value */
                   UINT1 *pu1IntermediateAsNums,    /* Inter AS Number */
                   UINT1 u1Set    /* VAL_SET/VAL_NO */
    )
{
    tSNMP_OCTET_STRING_TYPE IpAddr;
#ifdef L3VPN
    tSNMP_OCTET_STRING_TYPE VrfName;
    UINT1               au1Vrf[BGP4_VPN4_MAX_VRF_NAME_SIZE];
#endif
    UINT4               u4ErrorCode = 0;
    INT4                i4Override = 0;
    INT1                i1RetVal;
    UINT1               au1IpAddrOctetList[BGP4_PEER_OCTET_LIST_LEN];

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    /* INPUT VALIDATION */
    i1RetVal = nmhValidateIndexInstanceFsbgp4MEDTable ((INT4) u4Index);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigMEDTable: INVALID MED Table Index\n");
        return BGP4_CLI_ERR_MED_TBL_INVALID_INDEX;
    }

    if (u1Set == VAL_SET)
    {
        MEMSET (au1IpAddrOctetList, 0, BGP4_PEER_OCTET_LIST_LEN);
        IpAddr.pu1_OctetList = au1IpAddrOctetList;

        MEMCPY (IpAddr.pu1_OctetList, pIpAddress->NetAddr.au1Address,
                pIpAddress->NetAddr.u2AddressLen);
        IpAddr.i4_Length = pIpAddress->NetAddr.u2AddressLen;

        i1RetVal =
            nmhTestv2Fsbgp4MEDAdminStatus (&u4ErrorCode, (INT4) u4Index,
                                           BGP4_ADMIN_DOWN);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigMEDTable: Unable to set the Med Entry.\n");
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        i1RetVal = nmhSetFsbgp4MEDAdminStatus ((INT4) u4Index, BGP4_ADMIN_DOWN);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigMEDTable: Unable to set the Med Entry.\n");
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }
#ifdef L3VPN
        if (i4VrfNameLen != 0)
        {
            VrfName.pu1_OctetList = au1Vrf;
            MEMCPY (VrfName.pu1_OctetList, pu1VrfName, i4VrfNameLen);
            VrfName.i4_Length = i4VrfNameLen;

            i1RetVal =
                nmhTestv2Fsbgp4mpeMEDVrfName (&u4ErrorCode, (INT4) u4Index,
                                              &(VrfName));
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigMEDTable: Unable to set the Med Entry.\n");
                Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                     BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhSetFsbgp4mpeMEDVrfName ((INT4) u4Index, &(VrfName));
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigMEDTable: Unable to set the Med Entry.\n");
                Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                     BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
        }
#endif

        i1RetVal = nmhTestv2Fsbgp4mpeMEDIPAddrAfi (&u4ErrorCode, (INT4) u4Index,
                                                   pIpAddress->NetAddr.u2Afi);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigMEDTable: Unable to set the Med Entry.\n");
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        i1RetVal =
            nmhTestv2Fsbgp4mpeMEDIPAddrSafi (&u4ErrorCode, (INT4) u4Index,
                                             pIpAddress->u2Safi);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigMEDTable: Unable to set the Med Entry.\n");
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        i1RetVal = nmhSetFsbgp4mpeMEDIPAddrAfi ((INT4) u4Index,
                                                pIpAddress->NetAddr.u2Afi);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigMEDTable: Unable to set the Med Entry.\n");
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        i1RetVal = nmhSetFsbgp4mpeMEDIPAddrSafi ((INT4) u4Index,
                                                 pIpAddress->u2Safi);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigMEDTable: Unable to set the Med Entry.\n");
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        i1RetVal = nmhTestv2Fsbgp4mpeMEDIPAddrPrefix (&u4ErrorCode,
                                                      (INT4) u4Index, &IpAddr);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigMEDTable: INVALID MED IP Address.\n");
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_MED_TBL_INVALID_IP_PREFIX;
        }

        i1RetVal = nmhTestv2Fsbgp4mpeMEDIPAddrPrefixLen (&u4ErrorCode,
                                                         (INT4) u4Index,
                                                         pIpAddress->
                                                         u2PrefixLen);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigMEDTable: INVALID MED IP Address Mask.\n");
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_MED_TBL_INVALID_IP_PREFIX_LEN;
        }

        i1RetVal = nmhTestv2Fsbgp4mpeMEDRemoteAS (&u4ErrorCode, (INT4) u4Index,
                                                  u4RemoteAS);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigMEDTable: INVALID MED Remote AS.\n");
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_MED_TBL_INVALID_REMOTE_AS;
        }

        i1RetVal = nmhTestv2Fsbgp4mpeMEDValue (&u4ErrorCode, (INT4) u4Index,
                                               u4MedValue);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigMEDTable: INVALID MED VALUE.\n");
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_MED_TBL_INVALID_MED_VALUE;
        }

        i1RetVal = nmhTestv2Fsbgp4mpeMEDDirection (&u4ErrorCode, (INT4) u4Index,
                                                   i4Direction);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                      BGP4_MOD_NAME,
                      "\tbgpConfigMEDTable: INVALID MED Direction.\n");
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_MED_TBL_INVALID_MED_DIRECTION;
        }

        if (u4Override == VAL_SET)
        {
            i4Override = BGP4_OVERRIDING;
        }
        else if (u4Override == VAL_INVALID)
        {
            i4Override = BGP4_NORMAL;
        }
        else
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigMEDTable: INVALID MED Overriding value.\n");
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_MED_TBL_INVALID_MED_OVERRIDING_VALUE;
        }

        if (pu1IntermediateAsNums != NULL)
        {
            i1RetVal = Bgp4Testv2Fsbgp4MEDIntermediateAS (&u4ErrorCode,
                                                          (INT4) u4Index,
                                                          pu1IntermediateAsNums,
                                                          STRLEN
                                                          (pu1IntermediateAsNums));
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigMEDTable: INVALID MED Intermediate AS.\n");
                Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                     BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_MED_TBL_INVALID_MED_INTERMEDIATE_AS;
            }
        }
    }

    /* INPUT PROCESSING */
    switch (u1Set)
    {
        case VAL_SET:
            i1RetVal = nmhSetFsbgp4mpeMEDIPAddrPrefix ((INT4) u4Index, &IpAddr);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigMEDTable: Unable to set the Med Ip Addr.\n");
                Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                     BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhSetFsbgp4mpeMEDIPAddrPrefixLen ((INT4) u4Index,
                                                          pIpAddress->
                                                          u2PrefixLen);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigMEDTable: Unable to set the Med Ip Addr Mask.\n");

                Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                     BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhSetFsbgp4mpeMEDRemoteAS ((INT4) u4Index, u4RemoteAS);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigMEDTable: Unable to set the Remote AS.\n");
                Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                     BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhSetFsbgp4mpeMEDValue ((INT4) u4Index, u4MedValue);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigMEDTable: Unable to set the Med Value.\n");
                Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                     BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal =
                nmhSetFsbgp4mpeMEDDirection ((INT4) u4Index, i4Direction);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigMEDTable: Unable to set the Med Direction.\n");
                Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                     BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal =
                nmhSetFsbgp4mpeMEDPreference ((INT4) u4Index, i4Override);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigMEDTable: Unable to set the Med Override.\n");
                Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                     BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            if (pu1IntermediateAsNums != NULL)
            {
                i1RetVal = Bgp4SetFsbgp4MEDIntermediateAS ((INT4) u4Index,
                                                           pu1IntermediateAsNums);
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigMEDTable: Unable to set the Med Inter AS.\n");
                    Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                         BGP4_DECREMENT_BY_ONE]));
                    return BGP4_CLI_ERR_MED_TBL_INVALID_MED_INTERMEDIATE_AS;
                }
            }

            i1RetVal = nmhSetFsbgp4mpeMEDAdminStatus ((INT4) u4Index,
                                                      BGP4_ADMIN_UP);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigMEDTable: Unable to set the Med Entry.\n");
                Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                     BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            break;

        case VAL_NO:

            i1RetVal = nmhSetFsbgp4mpeMEDAdminStatus ((INT4) u4Index,
                                                      BGP4_ADMIN_DOWN);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigMEDTable: Unable to set the Med Entry.\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            /* Re-Setting the MED table entry */
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            break;
        default:
            break;

    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigLocalPrefTable                                   */
/* CLI Command   : (a) bgp local-preference-table index {index}              */
/*                         remote-as {as-no}{ip-address} {ip-mask}           */
/*                         [intermediate-as {as-num1, as-num2...as-numN}]    */
/*                         local-pref {local-pref-value} direction {in/out}  */
/*                         [override]                                        */
/*               : (b) no bgp local-preference-table index {index}           */
/*                         remote-as {as-no}{ip-address} {ip-mask}           */
/*                         [intermediate-as {as-num1, as-num2...as-numN}]    */
/*                         local-pref {local-pref-value} direction {in/out}  */
/*                         [override]                                        */
/* Description   : This command configures/removes an entry in/from the      */
/*               : Local Preference table. When as route is received or send */
/*               : out, the entries configured in the Local Preference table */
/*               : are applied, if necessary, to derive the Local preference */
/*               : to be associated will the route.                          */
/*               : Command (a) configures a entry in Local Preference Table. */
/*               : Command (b) removes the entry from Local Pref table.      */
/* Input(s)      : Index of the Local Preference table entry (u4Index)       */
/*                                            - mandatory parameter          */
/*               : Remote AS value (u4Remote AS) - mandatory parameter       */
/*               : IP address of the route (u4IpAddres)-mandatory parameter  */
/*               : Local Pref value that will be set if the route matches the*/
/*                 parameters in the entry.(u4LocalPrefValue) - mandatory    */
/*                 parameter                                                 */
/*               : Direction in which this entry is applied (i4Direction)    */
/*                                            - mandatory parameter          */
/*                 -value is BGP4_INCOMING (0x01), for applying in incoming  */
/*                  direction                                                */
/*                 -value is BGP4_OUTGOING (0x02), for applying in outgoing  */
/*                  direction                                                */
/*               : Override paramater based on which the decision to either  */
/*                 overwrite the already existing value is taken.            */
/*                 (u4Override). This is an optional parameter. The          */
/*                 -value is VAL_INVALID, if the command is issued without   */
/*                  this parameter                                           */
/*                 -value is VAL_SET, if the command is issued with          */
/*                  this parameter                                           */
/*               : Intermediate AS number (pu1IntermediateAsNums). This is   */
/*                 an optional parameter. If non-NULL, this contains a comma */
/*                 separated string of AS-numbers.                           */
/*               : u1Set = VAL_SET for command (a) and                       */
/*                 u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigLocalPrefTable (UINT4 u4Index,    /* Arbitary Index                */
#ifdef L3VPN
                         UINT1 *pu1VrfName,    /* Vrf Name                      */
                         INT4 i4VrfNameLen,    /* Vrf Name Length              */
#endif
                         UINT4 u4RemoteAS,    /* Remote AS value to be matched */
                         tNetAddress * pIpAddress,    /* Ip Address to be matched      */
                         UINT4 u4LocalPrefValue,    /* LP value for this entry       */
                         INT4 i4Direction,    /* Direction - in/out            */
                         UINT4 u4Override,    /* Override value                */
                         UINT1 *pu1IntermediateAsNums,    /* Inter AS Number       */
                         UINT1 u1Set    /* VAL_SET/VAL_NO                */
    )
{
    tSNMP_OCTET_STRING_TYPE IpAddr;
#ifdef L3VPN
    tSNMP_OCTET_STRING_TYPE VrfName;
    UINT1               au1Vrf[BGP4_VPN4_MAX_VRF_NAME_SIZE];
#endif
    UINT4               u4ErrorCode = 0;
    INT4                i4Override = 0;
    INT1                i1RetVal;
    UINT1               au1IpAddrOctetList[BGP4_PEER_OCTET_LIST_LEN];
    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    /* INPUT VALIDATION */
    i1RetVal = nmhValidateIndexInstanceFsbgp4MpeLocalPrefTable ((INT4) u4Index);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigLocalPrefTable: INVALID Local Pref Table Index\n");
        return BGP4_CLI_ERR_LP_TBL_INVALID_INDEX;
    }

    if (u1Set == VAL_SET)
    {
        MEMSET (au1IpAddrOctetList, 0, BGP4_PEER_OCTET_LIST_LEN);
        IpAddr.pu1_OctetList = au1IpAddrOctetList;

        MEMCPY (IpAddr.pu1_OctetList, pIpAddress->NetAddr.au1Address,
                pIpAddress->NetAddr.u2AddressLen);
        IpAddr.i4_Length = pIpAddress->NetAddr.u2AddressLen;

        i1RetVal =
            nmhTestv2Fsbgp4LocalPrefAdminStatus (&u4ErrorCode, (INT4) u4Index,
                                                 BGP4_ADMIN_DOWN);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigLocalPrefTable: Unable to set the LP Entry.\n");
            Bgp4InitMedlpEntry (&(BGP4_LP_ENTRY[u4Index -
                                                BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        i1RetVal = nmhSetFsbgp4LocalPrefAdminStatus ((INT4) u4Index,
                                                     BGP4_ADMIN_DOWN);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigLocalPrefTable: Unable to set the LP Entry.\n");
            Bgp4InitMedlpEntry (&(BGP4_LP_ENTRY[u4Index -
                                                BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }
#ifdef L3VPN
        if (i4VrfNameLen != 0)
        {
            VrfName.pu1_OctetList = au1Vrf;
            MEMCPY (VrfName.pu1_OctetList, pu1VrfName, i4VrfNameLen);
            VrfName.i4_Length = i4VrfNameLen;
            i1RetVal = nmhTestv2Fsbgp4mpeLocalPrefVrfName (&u4ErrorCode,
                                                           (INT4) u4Index,
                                                           &(VrfName));
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigMEDTable: Unable to set the Med Entry.\n");
                Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                     BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal =
                nmhSetFsbgp4mpeLocalPrefVrfName ((INT4) u4Index, &(VrfName));
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigMEDTable: Unable to set the Med Entry.\n");
                Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                     BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
        }
#endif

        i1RetVal = nmhTestv2Fsbgp4mpeLocalPrefIPAddrAfi (&u4ErrorCode,
                                                         (INT4) u4Index,
                                                         pIpAddress->NetAddr.
                                                         u2Afi);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigMEDTable: Unable to set the Med Entry.\n");
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        i1RetVal = nmhTestv2Fsbgp4mpeLocalPrefIPAddrSafi (&u4ErrorCode,
                                                          (INT4) u4Index,
                                                          pIpAddress->u2Safi);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigMEDTable: Unable to set the Med Entry.\n");
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        i1RetVal = nmhSetFsbgp4mpeLocalPrefIPAddrAfi ((INT4) u4Index,
                                                      pIpAddress->NetAddr.
                                                      u2Afi);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigMEDTable: Unable to set the Med Entry.\n");
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        i1RetVal = nmhSetFsbgp4mpeLocalPrefIPAddrSafi ((INT4) u4Index,
                                                       pIpAddress->u2Safi);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigMEDTable: Unable to set the Med Entry.\n");
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        i1RetVal = nmhTestv2Fsbgp4mpeLocalPrefIPAddrPrefix (&u4ErrorCode,
                                                            (INT4) u4Index,
                                                            &IpAddr);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigLocalPrefTable: INVALID LP IP Address.\n");
            Bgp4InitMedlpEntry (&(BGP4_LP_ENTRY[u4Index -
                                                BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_LP_TBL_INVALID_IP_PREFIX;
        }

        i1RetVal = nmhTestv2Fsbgp4mpeLocalPrefIPAddrPrefixLen (&u4ErrorCode,
                                                               (INT4) u4Index,
                                                               pIpAddress->
                                                               u2PrefixLen);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigLocalPrefTable: INVALID LP IP Address Mask.\n");
            Bgp4InitMedlpEntry (&(BGP4_LP_ENTRY[u4Index -
                                                BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_LP_TBL_INVALID_IP_PREFIX_LEN;
        }

        i1RetVal = nmhTestv2Fsbgp4mpeLocalPrefRemoteAS (&u4ErrorCode,
                                                        (INT4) u4Index,
                                                        u4RemoteAS);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigLocalPrefTable: INVALID LP Remote AS.\n");
            Bgp4InitMedlpEntry (&(BGP4_LP_ENTRY[u4Index -
                                                BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_LP_TBL_INVALID_REMOTE_AS;
        }

        i1RetVal =
            nmhTestv2Fsbgp4mpeLocalPrefValue (&u4ErrorCode, (INT4) u4Index,
                                              u4LocalPrefValue);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigLocalPrefTable: INVALID LP VALUE.\n");
            Bgp4InitMedlpEntry (&(BGP4_LP_ENTRY[u4Index -
                                                BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_LP_TBL_INVALID_LP_VALUE;
        }

        i1RetVal = nmhTestv2Fsbgp4mpeLocalPrefDirection (&u4ErrorCode,
                                                         (INT4) u4Index,
                                                         i4Direction);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                      BGP4_MOD_NAME,
                      "\tbgpConfigLocalPrefTable: INVALID LP Direction.\n");
            Bgp4InitMedlpEntry (&(BGP4_LP_ENTRY[u4Index -
                                                BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_LP_TBL_INVALID_LP_DIRECTION;
        }

        if (u4Override == VAL_SET)
        {
            i4Override = BGP4_OVERRIDING;
        }
        else if (u4Override == VAL_INVALID)
        {
            i4Override = BGP4_NORMAL;
        }
        else
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigLocalPrefTable: INVALID LP Overriding value.\n");
            Bgp4InitMedlpEntry (&(BGP4_LP_ENTRY[u4Index -
                                                BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_LP_TBL_INVALID_LP_OVERRIDING_VALUE;
        }

        if (pu1IntermediateAsNums != NULL)
        {
            i1RetVal = Bgp4Testv2Fsbgp4LocalPrefIntermediateAS (&u4ErrorCode,
                                                                (INT4) u4Index,
                                                                pu1IntermediateAsNums,
                                                                STRLEN
                                                                (pu1IntermediateAsNums));
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigLocalPrefTable: INVALID LP Intermediate AS.\n");
                Bgp4InitMedlpEntry (&(BGP4_LP_ENTRY[u4Index -
                                                    BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_LP_TBL_INVALID_LP_INTERMEDIATE_AS;
            }
        }
    }

    /* INPUT PROCESSING */
    switch (u1Set)
    {
        case VAL_SET:
            i1RetVal = nmhSetFsbgp4mpeLocalPrefIPAddrPrefix ((INT4) u4Index,
                                                             &IpAddr);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigLocalPrefTable: Unable to set LP Ip Addr.\n");
                Bgp4InitMedlpEntry (&(BGP4_LP_ENTRY[u4Index -
                                                    BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhSetFsbgp4mpeLocalPrefIPAddrPrefixLen ((INT4) u4Index,
                                                                pIpAddress->
                                                                u2PrefixLen);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigLocalPrefTable: Unable to set LP Ip Addr Mask.\n");
                Bgp4InitMedlpEntry (&(BGP4_LP_ENTRY[u4Index -
                                                    BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhSetFsbgp4mpeLocalPrefRemoteAS ((INT4) u4Index,
                                                         u4RemoteAS);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigLocalPrefTable: Unable to set Remote AS.\n");
                Bgp4InitMedlpEntry (&(BGP4_LP_ENTRY[u4Index -
                                                    BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhSetFsbgp4mpeLocalPrefValue ((INT4) u4Index,
                                                      u4LocalPrefValue);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigLocalPrefTable: Unable to set LP Value.\n");
                Bgp4InitMedlpEntry (&(BGP4_LP_ENTRY[u4Index -
                                                    BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhSetFsbgp4mpeLocalPrefDirection ((INT4) u4Index,
                                                          i4Direction);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigLocalPrefTable: Unable to set LP Direction.\n");
                Bgp4InitMedlpEntry (&(BGP4_LP_ENTRY[u4Index -
                                                    BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhSetFsbgp4mpeLocalPrefPreference ((INT4) u4Index,
                                                           i4Override);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigLocalPrefTable: Unable to set LP Override.\n");
                Bgp4InitMedlpEntry (&(BGP4_LP_ENTRY[u4Index -
                                                    BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            if (pu1IntermediateAsNums != NULL)
            {
                i1RetVal = Bgp4SetFsbgp4LocalPrefIntermediateAS ((INT4) u4Index,
                                                                 pu1IntermediateAsNums);
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigLocalPrefTable: Unable to set LP Inter AS.\n");
                    Bgp4InitMedlpEntry (&(BGP4_LP_ENTRY[u4Index -
                                                        BGP4_DECREMENT_BY_ONE]));
                    return BGP4_CLI_ERR_LP_TBL_INVALID_LP_INTERMEDIATE_AS;
                }
            }

            i1RetVal = nmhSetFsbgp4mpeLocalPrefAdminStatus ((INT4) u4Index,
                                                            BGP4_ADMIN_UP);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigLocalPrefTable: Unable to set LP Entry.\n");
                Bgp4InitMedlpEntry (&(BGP4_LP_ENTRY[u4Index -
                                                    BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            break;

        case VAL_NO:

            i1RetVal = nmhSetFsbgp4mpeLocalPrefAdminStatus ((INT4) u4Index,
                                                            BGP4_ADMIN_DOWN);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigLocalPrefTable: Unable to set LP Entry.\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            /* Re-Setting the LP table entry */
            Bgp4InitMedlpEntry (&(BGP4_LP_ENTRY[u4Index -
                                                BGP4_DECREMENT_BY_ONE]));
            break;
        default:
            break;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigUpdateFilterTable                                */
/* CLI Command   : (a) bgp update-filter-table index {index} {permit|deny}   */
/*                         remote-as {as-no} {ip-address} {ip-mask}          */
/*                         [intermediate-as {as-num1, as-num2...as-numN}]    */
/*                         direction {in/out}                                */
/*               : (b) no bgp update-filter-table index {index} {permit|deny}*/
/*                         remote-as {as-no} {ip-address} {ip-mask}          */
/*                         [intermediate-as {as-num1, as-num2...as-numN}]    */
/*                         direction {in/out}                                */
/* Description   : This command configures/removes an entry in/from the      */
/*               : Update Filter table. When as route is received or send    */
/*               : out, the entries configured in the Update Filter table    */
/*               : are applied, if necessary, to filter the matching route.  */
/*               : Command (a) configures a entry in Update Filter table and */
/*               : Command (b) removes the entry from the Filter table.      */
/* Input(s)      : Index of the Update Filter table entry (u4Index)          */
/*                                            - mandatory parameter          */
/*               : Action to be performed on the matching route (i4Action)   */
/*                                            - mandatory parameter          */
/*                 -value is BGP4_ALLOW (0x01), for not filtering the route  */
/*                 -value is BGP4_DENY (0x02), for filtering the route.      */
/*               : Remote AS value (u4Remote AS) - mandatory parameter       */
/*               : IP address of the route (u4IpAddres)-mandatory parameter  */
/*               : Direction in which this entry is applied (i4Direction)    */
/*                                            - mandatory parameter          */
/*                 -value is BGP4_INCOMING (0x01), for applying in incoming  */
/*                  direction                                                */
/*                 -value is BGP4_OUTGOING (0x02), for applying in outgoing  */
/*                  direction                                                */
/*               : Intermediate AS number (pu1IntermediateAsNums). This is   */
/*                 an optional parameter. If non-NULL, this contains a comma */
/*                 separated string of AS-numbers.                           */
/*               : u1Set = VAL_SET for command (a) and                       */
/*                 u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigUpdateFilterTable (UINT4 u4Index,    /* Arbitary Index */
#ifdef L3VPN
                            UINT1 *pu1VrfName,    /* Vrf Name       */
                            INT4 i4VrfNameLen,    /* Vrf Name  Length  */
#endif
                            UINT4 u4RemoteAS,    /* Remote AS value to be matched */
                            tNetAddress * pIpAddress,    /* Ip Address to be matched */
                            INT4 i4Direction,    /* Direction - in/out */
                            INT4 i4Action,    /* Action to be performed */
                            UINT1 *pu1IntermediateAsNums,    /* Inter AS number */
                            UINT1 u1Set    /* VAL_SET/VAL_NO */
    )
{
    tSNMP_OCTET_STRING_TYPE IpAddr;
#ifdef L3VPN
    tSNMP_OCTET_STRING_TYPE VrfName;
    UINT1               au1Vrf[BGP4_VPN4_MAX_VRF_NAME_SIZE];
#endif
    UINT4               u4ErrorCode = 0;
    INT1                i1RetVal;
    UINT1               au1IpAddrOctetList[BGP4_PEER_OCTET_LIST_LEN];

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    /* INPUT VALIDATION */
    i1RetVal =
        nmhValidateIndexInstanceFsbgp4MpeUpdateFilterTable ((INT4) u4Index);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigUpdateFilterTable: INVALID Filter Table Index\n");
        return BGP4_CLI_ERR_FILTER_TBL_INVALID_INDEX;
    }

    if (u1Set == VAL_SET)
    {
        MEMSET (au1IpAddrOctetList, 0, BGP4_PEER_OCTET_LIST_LEN);
        IpAddr.pu1_OctetList = au1IpAddrOctetList;
        MEMCPY (IpAddr.pu1_OctetList, pIpAddress->NetAddr.au1Address,
                pIpAddress->NetAddr.u2AddressLen);
        IpAddr.i4_Length = pIpAddress->NetAddr.u2AddressLen;

        i1RetVal =
            nmhTestv2Fsbgp4mpeUpdateFilterAdminStatus (&u4ErrorCode,
                                                       (INT4) u4Index,
                                                       BGP4_ADMIN_DOWN);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigUpdateFilterTable: Unable to set the "
                      "Filter Entry.\n");
            Bgp4InitFilterEntry (&(BGP4_FILTER_ENTRY[u4Index - 1]));
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        i1RetVal = nmhSetFsbgp4mpeUpdateFilterAdminStatus ((INT4) u4Index,
                                                           BGP4_ADMIN_DOWN);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigUpdateFilterTable: Unable to set the "
                      "Filter Entry.\n");
            Bgp4InitFilterEntry (&(BGP4_FILTER_ENTRY[u4Index - 1]));
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }
#ifdef L3VPN
        if (i4VrfNameLen != 0)
        {
            VrfName.pu1_OctetList = au1Vrf;
            MEMCPY (VrfName.pu1_OctetList, pu1VrfName, i4VrfNameLen);
            VrfName.i4_Length = i4VrfNameLen;

            i1RetVal = nmhTestv2Fsbgp4mpeUpdateFilterVrfName (&u4ErrorCode,
                                                              (INT4) u4Index,
                                                              &(VrfName));
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigUpdateFilterTable: Unable to set the "
                          "Filter Entry.\n");
                Bgp4InitFilterEntry (&(BGP4_FILTER_ENTRY[u4Index - 1]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal =
                nmhSetFsbgp4mpeUpdateFilterVrfName ((INT4) u4Index, &(VrfName));
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigUpdateFilterTable: Unable to set the "
                          "Filter Entry.\n");
                Bgp4InitFilterEntry (&(BGP4_FILTER_ENTRY[u4Index - 1]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
        }
#endif

        i1RetVal = nmhTestv2Fsbgp4mpeUpdateFilterAction (&u4ErrorCode,
                                                         (INT4) u4Index,
                                                         i4Action);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigUpdateFilterTable: INVALID Filter Action.\n");
            Bgp4InitFilterEntry (&(BGP4_FILTER_ENTRY[u4Index -
                                                     BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_FILTER_TBL_INVALID_ACTION;
        }

        i1RetVal = nmhTestv2Fsbgp4mpeUpdateFilterIPAddrAfi (&u4ErrorCode,
                                                            (INT4) u4Index,
                                                            pIpAddress->NetAddr.
                                                            u2Afi);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigMEDTable: Unable to set the Med Entry.\n");
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        i1RetVal = nmhTestv2Fsbgp4mpeUpdateFilterIPAddrSafi (&u4ErrorCode,
                                                             (INT4) u4Index,
                                                             pIpAddress->
                                                             u2Safi);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigMEDTable: Unable to set the Med Entry.\n");
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        i1RetVal = nmhSetFsbgp4mpeUpdateFilterIPAddrAfi ((INT4) u4Index,
                                                         pIpAddress->NetAddr.
                                                         u2Afi);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigMEDTable: Unable to set the Med Entry.\n");
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        i1RetVal = nmhSetFsbgp4mpeUpdateFilterIPAddrSafi ((INT4) u4Index,
                                                          pIpAddress->u2Safi);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigMEDTable: Unable to set the Med Entry.\n");
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[u4Index -
                                                 BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        i1RetVal = nmhTestv2Fsbgp4mpeUpdateFilterIPAddrPrefix (&u4ErrorCode,
                                                               (INT4) u4Index,
                                                               &IpAddr);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigUpdateFilterTable: INVALID Filter IP Address.\n");
            Bgp4InitFilterEntry (&(BGP4_FILTER_ENTRY[u4Index -
                                                     BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_FILTER_TBL_INVALID_IP_PREFIX;
        }

        i1RetVal = nmhTestv2Fsbgp4mpeUpdateFilterIPAddrPrefixLen (&u4ErrorCode,
                                                                  (INT4)
                                                                  u4Index,
                                                                  pIpAddress->
                                                                  u2PrefixLen);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigUpdateFilterTable: INVALID Filter IP Address Mask.\n");
            Bgp4InitFilterEntry (&(BGP4_FILTER_ENTRY[u4Index -
                                                     BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_FILTER_TBL_INVALID_IP_PREFIX_LEN;
        }

        i1RetVal = nmhTestv2Fsbgp4mpeUpdateFilterRemoteAS (&u4ErrorCode,
                                                           (INT4) u4Index,
                                                           u4RemoteAS);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigUpdateFilterTable: INVALID Filter Remote AS.\n");
            Bgp4InitFilterEntry (&(BGP4_FILTER_ENTRY[u4Index -
                                                     BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_FILTER_TBL_INVALID_REMOTE_AS;
        }

        i1RetVal = nmhTestv2Fsbgp4mpeUpdateFilterDirection (&u4ErrorCode,
                                                            (INT4) u4Index,
                                                            i4Direction);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                      BGP4_MOD_NAME,
                      "\tbgpConfigUpdateFilterTable: INVALID Filter Direction.\n");
            Bgp4InitFilterEntry (&(BGP4_FILTER_ENTRY[u4Index -
                                                     BGP4_DECREMENT_BY_ONE]));
            return BGP4_CLI_ERR_FILTER_TBL_INVALID_DIRECTION;
        }

        if (pu1IntermediateAsNums != NULL)
        {
            i1RetVal = Bgp4Testv2Fsbgp4UpdateFilterIntermediateAS (&u4ErrorCode,
                                                                   (INT4)
                                                                   u4Index,
                                                                   pu1IntermediateAsNums,
                                                                   STRLEN
                                                                   (pu1IntermediateAsNums));
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigUpdateFilterTable: INVALID Filter Inter AS.\n");
                Bgp4InitFilterEntry (&(BGP4_FILTER_ENTRY[u4Index -
                                                         BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_FILTER_TBL_INVALID_INTERMEDIATE_AS;
            }
        }
    }

    /* INPUT PROCESSING */
    switch (u1Set)
    {
        case VAL_SET:
            i1RetVal = nmhSetFsbgp4mpeUpdateFilterAction ((INT4) u4Index,
                                                          i4Action);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigUpdateFilterTable: Unable to set Action.\n");
                Bgp4InitFilterEntry (&(BGP4_FILTER_ENTRY[u4Index -
                                                         BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhSetFsbgp4mpeUpdateFilterIPAddrPrefix ((INT4) u4Index,
                                                                &IpAddr);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigUpdateFilterTable: Unable to set Ip Addr.\n");
                Bgp4InitFilterEntry (&(BGP4_FILTER_ENTRY[u4Index -
                                                         BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal =
                nmhSetFsbgp4mpeUpdateFilterIPAddrPrefixLen ((INT4) u4Index,
                                                            pIpAddress->
                                                            u2PrefixLen);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigUpdateFilterTable: Unable to set Filter Mask.\n");
                Bgp4InitFilterEntry (&(BGP4_FILTER_ENTRY[u4Index -
                                                         BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhSetFsbgp4mpeUpdateFilterRemoteAS ((INT4) u4Index,
                                                            u4RemoteAS);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigUpdateFilterTable: Unable to set Remote AS.\n");
                Bgp4InitFilterEntry (&(BGP4_FILTER_ENTRY[u4Index -
                                                         BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhSetFsbgp4mpeUpdateFilterDirection ((INT4) u4Index,
                                                             i4Direction);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigUpdateFilterTable: Unable to set Direction.\n");
                Bgp4InitFilterEntry (&(BGP4_FILTER_ENTRY[u4Index -
                                                         BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            if (pu1IntermediateAsNums != NULL)
            {
                i1RetVal = Bgp4SetFsbgp4UpdateFilterIntermediateAS ((INT4)
                                                                    u4Index,
                                                                    pu1IntermediateAsNums);
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigUpdateFilterTable: Unable to set Inter AS.\n");
                    Bgp4InitFilterEntry (&(BGP4_FILTER_ENTRY[u4Index -
                                                             BGP4_DECREMENT_BY_ONE]));
                    return BGP4_CLI_ERR_FILTER_TBL_INVALID_INTERMEDIATE_AS;
                }
            }

            i1RetVal = nmhSetFsbgp4mpeUpdateFilterAdminStatus ((INT4) u4Index,
                                                               BGP4_ADMIN_UP);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigUpdateFilterTable: Unable to set Admin Status.\n");
                Bgp4InitFilterEntry (&(BGP4_FILTER_ENTRY[u4Index -
                                                         BGP4_DECREMENT_BY_ONE]));
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            break;

        case VAL_NO:
            i1RetVal = nmhSetFsbgp4mpeUpdateFilterAdminStatus ((INT4) u4Index,
                                                               BGP4_ADMIN_DOWN);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigUpdateFilterTable: Unable to set Entry.\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            /* Re-Setting the Filter table entry */
            Bgp4InitFilterEntry (&(BGP4_FILTER_ENTRY[u4Index -
                                                     BGP4_DECREMENT_BY_ONE]));
            break;
        default:
            break;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigAggregateAddress                                 */
/* CLI Command   : (a) aggregate-address index {index} {ip-address}          */
/*                     {ip-mask} [summary-only]                              */
/*               : (b) no aggregate-address index {index} {ip-address}       */
/*                     {ip-mask} [summary-only]                              */
/* Description   : This command configures/removes an entry in/from the      */
/*               : Aggregate table.                                          */
/*               : Command (a) configures a entry in Aggregate table and     */
/*               : Command (b) removes the entry from Aggregate table.       */
/* Input(s)      : Index of the Aggregate table entry (u4Index)              */
/*                                            - mandatory parameter          */
/*               : IP address of the route (u4IpAddres)-mandatory parameter  */
/*               : Aggregation policy (u4SummaryOnly)                        */
/*                 This is an optional parameter. The                        */
/*                 -value is VAL_INVALID, if the command is issued without   */
/*                  this parameter                                           */
/*                 -value is VAL_SET, if the command is issued with          */
/*                  this parameter                                           */
/*               : u1Set = VAL_SET for command (a) and                       */
/*                 u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigAggregateAddress (UINT4 u4Index,    /* Arbitary Index */
#ifdef L3VPN
                           UINT1 *pu1VrfName,    /* Vrf Name       */
                           INT4 i4VrfNameLen,    /* Vrf Name  Length  */
#endif
                           tNetAddress * pIpAddress,    /* Ip Address value */
                           UINT4 u4SummaryOnly,    /* Aggregation policy */
                           UINT1 u1Set)    /* VAL_SET/VAL_NO */
{
    tSNMP_OCTET_STRING_TYPE IpAddr;
#ifdef L3VPN
    tSNMP_OCTET_STRING_TYPE VrfName;
    UINT1               au1Vrf[BGP4_VPN4_MAX_VRF_NAME_SIZE];
#endif
    tNetAddress         IpAddr1;
    UINT4               u4IpAddr;
    UINT4               u4ErrorCode = 0;
    UINT4               u4Cntr;
    INT4                i4Action = 0;
    UINT1               u1IsPrefixChg = BGP4_FALSE;
    UINT1               u1IsPolicyChg = BGP4_FALSE;
    UINT1               u1IsAdminChg = BGP4_FALSE;
    INT1                i1RetVal;
    UINT1               au1IpAddrOctetList[BGP4_PEER_OCTET_LIST_LEN];

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    /* INPUT VALIDATION */
    i1RetVal = nmhValidateIndexInstanceFsbgp4MpeAggregateTable ((INT4) u4Index);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigAggregateAddress: INVALID Aggrgate Table Index\n");
        return BGP4_CLI_ERR_AGGR_TBL_INVALID_INDEX;
    }

    if (u1Set == VAL_SET)
    {
        MEMSET (au1IpAddrOctetList, 0, BGP4_PEER_OCTET_LIST_LEN);
        IpAddr.pu1_OctetList = au1IpAddrOctetList;
        MEMCPY (IpAddr.pu1_OctetList, pIpAddress->NetAddr.au1Address,
                pIpAddress->NetAddr.u2AddressLen);
        IpAddr.i4_Length = pIpAddress->NetAddr.u2AddressLen;

        if (u4SummaryOnly == VAL_SET)
        {
            i4Action = BGP4_SUMMARY;
        }
        else
        {
            i4Action = BGP4_ALL;
        }

        i1RetVal = nmhTestv2Fsbgp4mpeAggregateAdvertise (&u4ErrorCode,
                                                         (INT4) u4Index,
                                                         i4Action);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                      BGP4_MOD_NAME,
                      "\tbgpConfigAggregateAddress: INVALID Aggregate Policy.\n");
            return BGP4_CLI_ERR_AGGR_TBL_INVALID_POLICY;
        }
    }

    /* INPUT PROCESSING */
    switch (u1Set)
    {
        case VAL_SET:

            switch (pIpAddress->NetAddr.u2Afi)
            {
                case BGP4_INET_AFI_IPV4:
                    Bgp4InitNetAddressStruct (&IpAddr1, BGP4_INET_AFI_IPV4,
                                              BGP4_INET_SAFI_UNICAST);
                    PTR_FETCH4 (u4IpAddr, pIpAddress->NetAddr.au1Address);
                    PTR_ASSIGN_4 (IpAddr1.NetAddr.au1Address, u4IpAddr);
                    IpAddr1.u2PrefixLen = pIpAddress->u2PrefixLen;
                    break;
#ifdef BGP4_IPV6_WANTED
                case BGP4_INET_AFI_IPV6:
                    Bgp4InitNetAddressStruct (&IpAddr1, BGP4_INET_AFI_IPV6,
                                              BGP4_INET_SAFI_UNICAST);
                    MEMCPY (IpAddr1.NetAddr.au1Address,
                            pIpAddress->NetAddr.au1Address,
                            BGP4_IPV6_PREFIX_LEN);
                    IpAddr1.u2PrefixLen = pIpAddress->u2PrefixLen;
                    break;
#endif
                default:
                    return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhTestv2Fsbgp4mpeAggregateIPAddrPrefix (&u4ErrorCode,
                                                                (INT4) u4Index,
                                                                &IpAddr);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigAggregateAddress: INVALID Aggr IP Address.\n");
                return BGP4_CLI_ERR_AGGR_TBL_INVALID_IP_PREFIX;
            }

            i1RetVal = nmhTestv2Fsbgp4mpeAggregateIPAddrPrefixLen (&u4ErrorCode,
                                                                   (INT4)
                                                                   u4Index,
                                                                   pIpAddress->
                                                                   u2PrefixLen);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigAggregateAddress: INVALID Aggr IP Address Mask.\n");
                return BGP4_CLI_ERR_AGGR_TBL_INVALID_IP_PREFIX_LEN;
            }

            if (BGP4_AGGRENTRY_ADMIN (u4Index - BGP4_DECREMENT_BY_ONE) !=
                BGP4_ADMIN_UP)
            {
                u1IsAdminChg = BGP4_TRUE;
            }

            if (NetAddrMatch ((BGP4_AGGRENTRY_NET_ADDR_INFO (u4Index - 1)),
                              IpAddr1) == BGP4_FALSE)
            {
                u1IsPrefixChg = BGP4_TRUE;
            }

            if ((BGP4_AGGRENTRY_ADV_TYPE (u4Index - BGP4_DECREMENT_BY_ONE) !=
                 i4Action) ||
                ((BGP4_AGGRENTRY_AGGR_STATUS (u4Index - BGP4_DECREMENT_BY_ONE)
                  & BGP4_AGGR_PENDING_ADVT_CHG) == BGP4_AGGR_PENDING_ADVT_CHG))
            {
                u1IsPolicyChg = BGP4_TRUE;
            }

            if (BGP4_AGGRENTRY_ADMIN (u4Index - BGP4_DECREMENT_BY_ONE) ==
                BGP4_INVALID)
            {
                i1RetVal = nmhSetFsbgp4mpeAggregateAdminStatus ((INT4) u4Index,
                                                                BGP4_ADMIN_DOWN);
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigAggregateAddress: Unable to set "
                              "Aggregate Entry.\n");
                    return BGP4_CLI_ERR_SET_AGGR_ADMIN_FAIL;
                }
            }

#ifdef L3VPN
            if (i4VrfNameLen != 0)
            {
                VrfName.pu1_OctetList = au1Vrf;
                MEMCPY (VrfName.pu1_OctetList, pu1VrfName, i4VrfNameLen);
                VrfName.i4_Length = i4VrfNameLen;

                i1RetVal = nmhTestv2Fsbgp4mpeAggregateVrfName (&u4ErrorCode,
                                                               (INT4) u4Index,
                                                               &(VrfName));
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigAggregateAddress: Invalid VRF Name.\n");
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }

                i1RetVal = nmhSetFsbgp4mpeAggregateVrfName ((INT4) u4Index,
                                                            &(VrfName));
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigAggregateAddress: Unable to set "
                              "VRF Name. for aggregation entry\n");
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }
            }
#endif

            if (u1IsPrefixChg == BGP4_TRUE)
            {
                i1RetVal = nmhSetFsbgp4mpeAggregateIPAddrAfi ((INT4) u4Index,
                                                              pIpAddress->
                                                              NetAddr.u2Afi);
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    return BGP4_CLI_ERR_AGGR_TBL_INVALID_IP_PREFIX;
                }

                i1RetVal = nmhSetFsbgp4mpeAggregateIPAddrSafi ((INT4) u4Index,
                                                               pIpAddress->
                                                               u2Safi);
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    return BGP4_CLI_ERR_AGGR_TBL_INVALID_IP_PREFIX;
                }

                i1RetVal = nmhSetFsbgp4mpeAggregateIPAddrPrefix ((INT4) u4Index,
                                                                 &IpAddr);
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigAggregateAddress: Unable to set Ip Addr.\n");
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }

                i1RetVal =
                    nmhSetFsbgp4mpeAggregateIPAddrPrefixLen ((INT4) u4Index,
                                                             pIpAddress->
                                                             u2PrefixLen);
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigAggregateAddress: Unable to set Aggregate Mask.\n");
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }
            }

            if (u1IsPolicyChg == BGP4_TRUE)
            {
                i1RetVal = nmhSetFsbgp4mpeAggregateAdvertise ((INT4) u4Index,
                                                              i4Action);
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigAggregateAddress: Unable to set Aggr Action.\n");
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }
            }

            if (u1IsAdminChg == BGP4_TRUE)
            {
                i1RetVal = nmhSetFsbgp4mpeAggregateAdminStatus ((INT4) u4Index,
                                                                BGP4_ADMIN_UP);
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigAggregateAddress: Unable to set Admin Status.\n");
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }
            }

            break;

        case VAL_NO:

            i1RetVal = nmhSetFsbgp4mpeAggregateAdminStatus ((INT4) u4Index,
                                                            BGP4_ADMIN_DOWN);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigAggregateAddress: Unable to set Entry.\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            BGP4_AGGRENTRY_VRFID (u4Index - 1) = BGP4_DFLT_VRFID;
            BGP4_AGGRENTRY_VRFNAME_SIZE (u4Index - 1) = 0;
            for (u4Cntr = 0; u4Cntr < BGP4_VPN4_MAX_VRF_NAME_SIZE; u4Cntr++)
            {
                BGP4_AGGRENTRY_VRFNAME_VALUE (u4Index - 1)[u4Cntr] = 0;
            }
            break;
        default:
            break;

    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigBgpDampening                                     */
/* CLI Command   : (a) bgp dampening  [{half-life} {reuse} {suppress}        */
/*                     {max-suppress-time}] [{decay-granularity}             */
/*                     {reuse-granularity} {reuse-arrary-size}]              */
/*               : (b) no bgp dampening  [{half-life} {reuse} {suppress}     */
/*                     {max-suppress-time}] [{decay-granularity}             */
/*                     {reuse-granularity} {reuse-arrary-size}]              */
/* Description   : This command configures the route dampening feature       */
/*               : related parameters. Bgp Dampening related parameters can  */
/*                 be set only when BGP4 global admin state is down.         */
/*               : Command (a) Set the route dampening parameter to the      */
/*                 given value.                                              */
/*               : Command (b) Resets the route dampening parameter to       */
/*                 the default value.                                        */
/* Input(s)      : Half Life Time - (pu4HalfLife)                            */
/*               : FOM value for route reuse - (pu4Reuse)                    */
/*               : FOM value for suppressing route - (pu4Suppress)           */
/*               : Max Route suppress time - (pu4MaxSuppressTime)            */
/*               : Timer granularity for all decay calculation in Seconds    */
/*                                                 - (pu4DecayGranularity)   */
/*               : Reuse lists evaluation Time - (pu4ReuseGranularity)       */
/*               : Reuse index array size - (pu4ReuseArraySize)              */
/*               : u1Set = VAL_SET for command (a) and                       */
/*                 u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigBgpDampening (UINT4 *pu4HalfLife,    /* Half Life Time */
                       UINT4 *pu4Reuse,    /* Route Reuse Value */
                       UINT4 *pu4Suppress,    /* Route Suppress Value */
                       UINT4 *pu4MaxSuppressTime,    /* Max Route Suppress 
                                                       Time */
                       UINT4 *pu4DecayGranularity,    /*Time Granularity for
                                                       Decay calculation */
                       UINT4 *pu4ReuseGranularity,    /*Time Granularity for
                                                       Reuse list evaluation */
                       UINT4 *pu4ReuseArraySize,    /* Reuse index Array 
                                                       size */
                       UINT1 u1Set)    /* VAL_SET/VAL_NO */
{
    UINT4               u4ErrorCode = 0;
    INT4                i4LocalAS = 0;
    INT4                i4GlobalAdmin = 0;
    INT1                i1RetVal;

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    nmhGetFsbgp4LocalAs ((UINT4 *) &i4LocalAS);
    if ((UINT4) i4LocalAS == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpconfigbgpdamepening: BGP Local AS not configured.\n");
        return BGP4_CLI_ERR_INVALID_AS_NO;
    }

    nmhGetFsbgp4GlobalAdminStatus (&i4GlobalAdmin);
    if (i4GlobalAdmin == BGP4_ADMIN_UP)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpconfigbgpdamepening: global admin up.\n");
        return BGP4_CLI_ERR_BGP_ADMIN_UP;
    }

    if (u1Set == VAL_SET)
    {
        if (pu4HalfLife != NULL)
        {
            i1RetVal = nmhTestv2Fsbgp4RfdDecayHalfLifeTime (&u4ErrorCode,
                                                            (INT4)
                                                            *pu4HalfLife);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigBgpDamepening: Invalid Decay Half Time value.\n");
                return BGP4_CLI_ERR_DAMP_INV_DECAY_HALFLIFE_TIME;
            }
        }

        if (pu4Reuse != NULL)
        {
            i1RetVal = nmhTestv2Fsbgp4RfdReuse (&u4ErrorCode, (INT4) *pu4Reuse);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigBgpDamepening: Invalid Reuse value.\n");
                return BGP4_CLI_ERR_DAMP_INV_REUSE_VAL;
            }
        }

        if (pu4Suppress != NULL)
        {
            i1RetVal = nmhTestv2Fsbgp4RfdCutOff (&u4ErrorCode,
                                                 (INT4) *pu4Suppress);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigBgpDamepening: Invalid CutOff value.\n");
                return BGP4_CLI_ERR_DAMP_INV_SUPPRESS_VAL;
            }
        }

        if (pu4MaxSuppressTime != NULL)
        {
            i1RetVal =
                nmhTestv2Fsbgp4RfdMaxHoldDownTime (&u4ErrorCode,
                                                   (INT4) *pu4MaxSuppressTime);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigBgpDamepening: Invalid Max Hold Down Time.\n");
                return BGP4_CLI_ERR_DAMP_INV_MAX_SUPPRESS_TIME;
            }
        }

        if (pu4DecayGranularity != NULL)
        {
            i1RetVal =
                nmhTestv2Fsbgp4RfdDecayTimerGranularity (&u4ErrorCode,
                                                         (INT4)
                                                         *pu4DecayGranularity);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigBgpDamepening: Invalid Decay Granularity value\n");
                return BGP4_CLI_ERR_DAMP_INV_DECAY_GRANULARITY;
            }
        }

        if (pu4ReuseGranularity != NULL)
        {
            i1RetVal =
                nmhTestv2Fsbgp4RfdReuseTimerGranularity (&u4ErrorCode,
                                                         (INT4)
                                                         *pu4ReuseGranularity);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigBgpDamepening: Invalid Reuse Granularity value\n");
                return BGP4_CLI_ERR_DAMP_INV_REUSE_GRANULARITY;
            }
        }

        if (pu4ReuseArraySize != NULL)
        {
            i1RetVal =
                nmhTestv2Fsbgp4RfdReuseIndxArraySize (&u4ErrorCode,
                                                      (INT4)
                                                      *pu4ReuseArraySize);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigBgpDamepening: Invalid Reuse Index Array Size\n");
                return BGP4_CLI_ERR_DAMP_INV_REUSE_INDEX_ARRAY_SIZE;
            }
        }
    }

    switch (u1Set)
    {
        case VAL_SET:

            if (pu4HalfLife != NULL)
            {
                i1RetVal = nmhSetFsbgp4RfdDecayHalfLifeTime ((INT4)
                                                             *pu4HalfLife);
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigBgpDamepening: Unable to set Decay"
                              "Half Time.\n");
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }
            }

            if (pu4Reuse != NULL)
            {
                i1RetVal = nmhSetFsbgp4RfdReuse ((INT4) *pu4Reuse);
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigBgpDamepening: Unable to set Reuse value.\n");
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }
            }

            if (pu4Suppress != NULL)
            {
                i1RetVal = nmhSetFsbgp4RfdCutOff ((INT4) *pu4Suppress);
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigBgpDamepening: Unable to set CutOff value.\n");
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }
            }

            if (pu4MaxSuppressTime != NULL)
            {
                i1RetVal = nmhSetFsbgp4RfdMaxHoldDownTime ((INT4)
                                                           *pu4MaxSuppressTime);
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigBgpDamepening: Unable to set Max"
                              "Hold Down Time.\n");
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }
            }

            if (pu4DecayGranularity != NULL)
            {
                i1RetVal = nmhSetFsbgp4RfdDecayTimerGranularity ((INT4)
                                                                 *pu4DecayGranularity);
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigBgpDamepening: Unable to set  Decay"
                              "Granularity value\n");
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }
            }

            if (pu4ReuseGranularity != NULL)
            {
                i1RetVal = nmhSetFsbgp4RfdReuseTimerGranularity ((INT4)
                                                                 *pu4ReuseGranularity);
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigBgpDamepening: Unable to set Reuse"
                              "Granularity value\n");
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }
            }

            if (pu4ReuseArraySize != NULL)
            {
                i1RetVal = nmhSetFsbgp4RfdReuseIndxArraySize ((INT4)
                                                              *pu4ReuseArraySize);
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigBgpDamepening: Unable to set"
                              "Reuse Index Array Size\n");
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }
            }

            break;

        case VAL_NO:
            /* RESET the Dampening parameters to Default values */
            i1RetVal =
                nmhSetFsbgp4RfdDecayHalfLifeTime (RFD_DEF_DECAY_HALF_LIFE_TIME);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigBgpDamepening: Unable to reset Decay"
                          "Half Time.\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhSetFsbgp4RfdReuse (RFD_DEF_REUSE_THRESHOLD);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigBgpDamepening: Unable to reset Reuse "
                          "value.\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhSetFsbgp4RfdCutOff (RFD_DEF_CUTOFF_THRESHOLD);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigBgpDamepening: Unable to reset "
                          "CutOff value.\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal =
                nmhSetFsbgp4RfdMaxHoldDownTime (RFD_DEF_MAX_HOLD_DOWN_TIME);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigBgpDamepening: Unable to reset Max"
                          "Hold Down Time.\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhSetFsbgp4RfdDecayTimerGranularity
                (RFD_DEF_DECAY_TIMER_GRANULARITY);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigBgpDamepening: Unable to reset  Decay"
                          "Granularity value\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhSetFsbgp4RfdReuseTimerGranularity
                (RFD_DEF_REUSE_TIMER_GRANULARITY);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigBgpDamepening: Unable to reset Reuse"
                          "Granularity value\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            i1RetVal = nmhSetFsbgp4RfdReuseIndxArraySize
                (RFD_DEF_REUSE_INDEX_ARRAY_SIZE);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigBgpDamepening: Unable to reset"
                          "Reuse Index Array Size\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            break;
        default:
            break;

    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigClusterId                                        */
/* CLI Command   : (a) bgp cluster-id {cluster-id}                           */
/*               : (b) no bgp cluster-id {cluster-id}                        */
/* Description   : This command configures, the cluster-id for the FutureBGP4*/
/*               : speaker. If the cluster-id is set, then the BGP speaker   */
/*               : will act as Route Reflector. By default, the cluster-id   */
/*               : is not set and the BGP Speaker does not act as Route      */
/*               : reflector. This object can be set only if the global      */
/*               : admin status is down.                                     */
/* Input(s)      : Bgp Speaker's Cluster Id (u4ClusterId)                    */
/*               : Operation to be performed (u1Set)                         */
/*               : u1Set = VAL_SET for command (a) and                       */
/*               : u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigClusterId (UINT4 u4ClusterId,    /* BGP Speaker's Cluster Id  */
                    UINT1 u1Set    /* VAL_SET/VAL_NO */
    )
{
    UINT4               u4ErrorCode = 0;
    INT4                i4GlobalAdmin = 0;
    INT1                i1RetVal;

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    switch (u1Set)
    {
        case VAL_SET:
            i1RetVal =
                RflTestv2Fsbgp4RflbgpClusterId (&u4ErrorCode, u4ClusterId);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigClusterId: Invalid Bgp Cluster ID\n");
                return BGP4_CLI_ERR_RFL_INV_CLUSTER_ID;
            }

            i1RetVal = RflSetFsbgp4RflbgpClusterId (u4ClusterId);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigClusterId: Unable to set Bgp Cluster ID\n");
                nmhGetFsbgp4GlobalAdminStatus (&i4GlobalAdmin);
                if (i4GlobalAdmin == BGP4_ADMIN_DOWN)
                {
                    return BGP4_CLI_ERR_INVALID_AS_NO;
                }
                else
                {
                    return BGP4_CLI_ERR_BGP_ADMIN_UP;
                }
            }
            break;

        case VAL_NO:
            i1RetVal = RflSetFsbgp4RflbgpClusterId (NO_CLUSTER_ID);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigClusterId: Unable to reset Bgp Cluster ID\n");
                return BGP4_CLI_ERR_BGP_ADMIN_UP;
            }
            break;
        default:
            break;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigClientToClientReflection                         */
/* CLI Command   : (a) bgp client-to-client reflection                       */
/*               : (b) no bgp client-to-client reflection                    */
/* Description   : This command configures, the route reflector operation    */
/*               : policy for handling routes from the client peers.         */
/*               : Command (a), configures the route reflector to advertise  */
/*               : the routes received from a client peer to other client    */
/*               : peer. By default, this operation will be done if the      */
/*               : a peer is configured as route reflector client.           */
/*               : Command (b), disable the advertisement of routes from     */
/*               : client peers to other client peers in the Route Reflector */
/* Input(s)      : Operation to be performed (u1Set)                         */
/*               : u1Set = VAL_SET for command (a) and                       */
/*               : u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigClientToClientReflection (UINT1 u1Set)    /* VAL_SET/VAL_NO */
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RflSupport = 0;
    INT4                i4GlobalAdmin = 0;
    INT1                i1RetVal;

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    if (u1Set == VAL_SET)
    {
        i4RflSupport = CLIENT_SUPPORT;
    }
    else
    {
        i4RflSupport = NO_CLIENT_SUPPORT;
    }
    i1RetVal = nmhTestv2Fsbgp4RflRflSupport (&u4ErrorCode, i4RflSupport);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigClusterId: Invalid RR Support\n");
        return BGP4_CLI_ERR_RFL_INV_RFL_SUPPORT;
    }

    i1RetVal = nmhSetFsbgp4RflRflSupport (i4RflSupport);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigClientToClientReflection: Unable to set"
                  "RFl support\n");
        nmhGetFsbgp4GlobalAdminStatus (&i4GlobalAdmin);
        if (i4GlobalAdmin == BGP4_ADMIN_DOWN)
        {
            return BGP4_CLI_ERR_RFL_CLUSTER_ID_NOT_SET;
        }
        else
        {
            return BGP4_CLI_ERR_BGP_ADMIN_UP;
        }
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigCommFilterTable                                  */
/* CLI Command   : (a) bgp comm-filter-table {comm-value} {permit|deny}      */
/*                     {in|out}                                              */
/*               : (b) no bgp comm-filter-table {comm-value} {permit|deny}   */
/*                     {in|out}                                              */
/* Description   : This command enables BGP to either accept/deny the        */
/*               : community attribute in the incoming direction or          */
/*               : advertise/filter the community attribute in the outgoing  */
/*               : direction.                                                */
/*               : Command (a) configures a entry in IN/OUT Community filter */
/*               : table.                                                    */
/*               : Command (b) removes the entry from corresponding table.   */
/* Input(s)      : Community value (u4CommValue)                             */
/*               : Action to be performed (i4Action)                         */
/*                 - value is BGP4_ALLOW (1), if community can be advertised.*/
/*                 - value is BGP4_DENY (2), if community can be filtered.   */
/*               : Direction in which the action needs to be applied         */
/*                 (i4Direction)                                             */
/*               : -value is BGP4_INCOMING (0x01), for applying in incoming  */
/*               :  direction                                                */
/*               : -value is BGP4_OUTGOING (0x02), for applying in outgoing  */
/*               :  direction                                                */
/*               : u1Set = VAL_SET for command (a) and                       */
/*                 u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigCommFilterTable (UINT4 u4CommValue,    /* Community value */
                          INT4 i4DoAction,    /* "permit" or "deny" */
                          INT4 i4Direction,    /* "in" or "out" */
                          UINT1 u1Set)    /* VAL_SET/VAL_NO */
{
    UINT4               u4ErrorCode = 0;
    INT4                i4Action = 0;
    INT4                i4RowStatus = 0;
    UINT1               u1CommTable = 0;
    INT1                i1RetVal = 0;

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    if (i4Direction == BGP4_INCOMING)
    {
        u1CommTable = BGP4_COMM_IN_FLT_TBL;
    }
    else if (i4Direction == BGP4_OUTGOING)
    {
        u1CommTable = BGP4_COMM_OUT_FLT_TBL;
    }
    else
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigCommFilterTable: Invalid Direction.");
        return BGP4_CLI_ERR_COMM_INV_DIRECTION;
    }

    if (i4DoAction == BGP4_ALLOW)
    {
        if (u1CommTable == BGP4_COMM_IN_FLT_TBL)
        {
            i4Action = (COMM_ACCEPT + 1);
        }
        else
        {
            i4Action = (COMM_ADVERTISE + 1);
        }
    }
    else if (i4DoAction == BGP4_DENY)
    {
        if (u1CommTable == BGP4_COMM_IN_FLT_TBL)
        {
            i4Action = (COMM_DENY + 1);
        }
        else
        {
            i4Action = (COMM_FILTER + 1);
        }
    }
    else
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigCommFilterTable: Invalid Action.");
        return BGP4_CLI_ERR_COMM_INV_ACTION;
    }

    if (u1CommTable == BGP4_COMM_IN_FLT_TBL)
    {
        i1RetVal =
            nmhValidateIndexInstanceFsbgp4CommInFilterTable (u4CommValue);
    }
    else
    {
        i1RetVal =
            nmhValidateIndexInstanceFsbgp4CommOutFilterTable (u4CommValue);
    }

    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigCommFilterTable: Invalid Comm Value\n");
        return BGP4_CLI_ERR_COMM_INV_COMM_VALUE;
    }

    if (u1CommTable == BGP4_COMM_IN_FLT_TBL)
    {
        i1RetVal = nmhGetFsbgp4InFilterRowStatus (u4CommValue, &i4RowStatus);
    }
    else
    {
        i1RetVal = nmhGetFsbgp4OutFilterRowStatus (u4CommValue, &i4RowStatus);
    }

    if ((i1RetVal == BGP4_CLI_FAILURE) && (u1Set == VAL_NO))
    {
        /* Trying to delete a non-existing Entry */
        return BGP4_SUCCESS;
    }

    switch (u1Set)
    {
        case VAL_SET:
            /* i1RetVal derived from return value of
             * nmhGetFsbgp4InFilterRowStatus/nmhGetFsbgp4OutFilterRowStatus */
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                /* Entry not present in Comm IN/OUT filter Table.
                 * Need to create a entry, set the input action and
                 * activate the entry. */
                if (u1CommTable == BGP4_COMM_IN_FLT_TBL)
                {
                    i1RetVal =
                        nmhTestv2Fsbgp4InFilterRowStatus (&u4ErrorCode,
                                                          u4CommValue,
                                                          CREATE_AND_WAIT);
                }
                else
                {
                    i1RetVal =
                        nmhTestv2Fsbgp4OutFilterRowStatus (&u4ErrorCode,
                                                           u4CommValue,
                                                           CREATE_AND_WAIT);
                }
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigCommFilterTable: Unable to create entry\n");
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }

                if (u1CommTable == BGP4_COMM_IN_FLT_TBL)
                {
                    i1RetVal = nmhSetFsbgp4InFilterRowStatus (u4CommValue,
                                                              CREATE_AND_WAIT);
                }
                else
                {
                    i1RetVal = nmhSetFsbgp4OutFilterRowStatus (u4CommValue,
                                                               CREATE_AND_WAIT);
                }
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigCommFilterTable: Unable to create entry\n");
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }

            }
            else
            {
                /* Already Entry is present. First make the entry row status
                 * not in service, set the input action and activate the
                 * entry */
                if (u1CommTable == BGP4_COMM_IN_FLT_TBL)
                {
                    i1RetVal =
                        nmhTestv2Fsbgp4InFilterRowStatus (&u4ErrorCode,
                                                          u4CommValue,
                                                          NOT_IN_SERVICE);
                }
                else
                {
                    i1RetVal =
                        nmhTestv2Fsbgp4OutFilterRowStatus (&u4ErrorCode,
                                                           u4CommValue,
                                                           NOT_IN_SERVICE);
                }
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigCommFilterTable: Unable to activate entry\n");
                    if (u1CommTable == BGP4_COMM_IN_FLT_TBL)
                    {
                        nmhSetFsbgp4InFilterRowStatus (u4CommValue, DESTROY);
                    }
                    else
                    {
                        nmhSetFsbgp4OutFilterRowStatus (u4CommValue, DESTROY);
                    }
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }

                if (u1CommTable == BGP4_COMM_IN_FLT_TBL)
                {
                    i1RetVal =
                        nmhSetFsbgp4InFilterRowStatus (u4CommValue,
                                                       NOT_IN_SERVICE);
                }
                else
                {
                    i1RetVal =
                        nmhSetFsbgp4OutFilterRowStatus (u4CommValue,
                                                        NOT_IN_SERVICE);
                }
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigCommFilterTable: Unable to activate entry\n");
                    if (u1CommTable == BGP4_COMM_IN_FLT_TBL)
                    {
                        nmhSetFsbgp4InFilterRowStatus (u4CommValue, DESTROY);
                    }
                    else
                    {
                        nmhSetFsbgp4OutFilterRowStatus (u4CommValue, DESTROY);
                    }
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }

            }

            /* Entry present and not active. Set the input action and 
             * make the entry status as active */
            if (u1CommTable == BGP4_COMM_IN_FLT_TBL)
            {
                i1RetVal =
                    nmhTestv2Fsbgp4CommIncomingFilterStatus (&u4ErrorCode,
                                                             u4CommValue,
                                                             i4Action);
            }
            else
            {
                i1RetVal =
                    nmhTestv2Fsbgp4CommOutgoingFilterStatus (&u4ErrorCode,
                                                             u4CommValue,
                                                             i4Action);
            }
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigCommRouteTable: Unable to activate entry\n");
                if (u1CommTable == BGP4_COMM_IN_FLT_TBL)
                {
                    nmhSetFsbgp4InFilterRowStatus (u4CommValue, DESTROY);
                }
                else
                {
                    nmhSetFsbgp4OutFilterRowStatus (u4CommValue, DESTROY);
                }
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            if (u1CommTable == BGP4_COMM_IN_FLT_TBL)
            {
                i1RetVal =
                    nmhSetFsbgp4CommIncomingFilterStatus (u4CommValue,
                                                          i4Action);
            }
            else
            {
                i1RetVal =
                    nmhSetFsbgp4CommOutgoingFilterStatus (u4CommValue,
                                                          i4Action);
            }
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigCommRouteTable: Unable to activate entry\n");

                if (u1CommTable == BGP4_COMM_IN_FLT_TBL)
                {
                    nmhSetFsbgp4InFilterRowStatus (u4CommValue, DESTROY);
                }
                else
                {
                    nmhSetFsbgp4OutFilterRowStatus (u4CommValue, DESTROY);
                }
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            if (u1CommTable == BGP4_COMM_IN_FLT_TBL)
            {
                i1RetVal =
                    nmhTestv2Fsbgp4InFilterRowStatus (&u4ErrorCode, u4CommValue,
                                                      ACTIVE);
            }
            else
            {
                i1RetVal =
                    nmhTestv2Fsbgp4OutFilterRowStatus (&u4ErrorCode,
                                                       u4CommValue, ACTIVE);
            }
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigCommFilterTable: Unable to activate entry\n");
                if (u1CommTable == BGP4_COMM_IN_FLT_TBL)
                {
                    nmhSetFsbgp4InFilterRowStatus (u4CommValue, DESTROY);
                }
                else
                {
                    nmhSetFsbgp4OutFilterRowStatus (u4CommValue, DESTROY);
                }
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            if (u1CommTable == BGP4_COMM_IN_FLT_TBL)
            {
                i1RetVal = nmhSetFsbgp4InFilterRowStatus (u4CommValue, ACTIVE);
            }
            else
            {
                i1RetVal = nmhSetFsbgp4OutFilterRowStatus (u4CommValue, ACTIVE);
            }
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigCommFilterTable: Unable to activate entry\n");
                if (u1CommTable == BGP4_COMM_IN_FLT_TBL)
                {
                    nmhSetFsbgp4InFilterRowStatus (u4CommValue, DESTROY);
                }
                else
                {
                    nmhSetFsbgp4OutFilterRowStatus (u4CommValue, DESTROY);
                }
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            break;

        case VAL_NO:
            /* Entry present in Comm IN/OUT filter  Table. Need to Delete */
            if (u1CommTable == BGP4_COMM_IN_FLT_TBL)
            {
                i1RetVal =
                    nmhTestv2Fsbgp4InFilterRowStatus (&u4ErrorCode, u4CommValue,
                                                      DESTROY);
            }
            else
            {
                i1RetVal =
                    nmhTestv2Fsbgp4OutFilterRowStatus (&u4ErrorCode,
                                                       u4CommValue, DESTROY);
            }
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigCommFilterTable: Unable to delete entry\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            if (u1CommTable == BGP4_COMM_IN_FLT_TBL)
            {
                i1RetVal = nmhSetFsbgp4InFilterRowStatus (u4CommValue, DESTROY);
            }
            else
            {
                i1RetVal =
                    nmhSetFsbgp4OutFilterRowStatus (u4CommValue, DESTROY);
            }
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigCommFilterTable: Unable to delete entry\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            break;
        default:
            break;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigEcommFilterTable                                 */
/* CLI Command   : (a) bgp ecomm-filter-table {ecomm-value} {permit|deny}    */
/*                     {in|out}                                              */
/*               : (b) no bgp ecomm-filter-table {ecomm-value} {permit|deny} */
/*                     {in|out}                                              */
/* Description   : This command enables BGP to either accept/deny the        */
/*               : extended community attribute in the incoming direction or */
/*               : advertise/filter the extended  community attribute in the */
/*               : outgoing direction.                                       */
/*               : Command (a) configures a entry in IN/OUT extended         */
/*               " community filter table.                                   */
/*               : Command (b) removes the entry from corresponding table.   */
/* Input(s)      : Extended Community value (pu1EcommValue)                  */
/*               : Action to be performed (i4DoAction)                       */
/*                 - value is BGP4_ALLOW (1), if community can be advertised.*/
/*                 - value is BGP4_DENY (2), if community can be filtered.   */
/*               : Direction in which the action needs to be applied         */
/*                 (i4Direction)                                             */
/*               : -value is BGP4_INCOMING (0x01), for applying in incoming  */
/*               :  direction                                                */
/*               : -value is BGP4_OUTGOING (0x02), for applying in outgoing  */
/*               :  direction                                                */
/*               : u1Set = VAL_SET for command (a) and                       */
/*                 u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigEcommFilterTable (UINT1 *pu1EcommValue,    /* Ext Community value */
                           INT4 i4DoAction,    /* "permit" or "deny" */
                           INT4 i4Direction,    /* "in" or "out" */
                           UINT1 u1Set)    /* VAL_SET/VAL_NO */
{
    tSNMP_OCTET_STRING_TYPE EcommValue;
    UINT4               u4ErrorCode = 0;
    INT4                i4Action = 0;
    INT4                i4RowStatus = 0;
    UINT1               u1EcommTable = 0;
    INT1                i1RetVal = 0;

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));
    BGP4_ASSERT (pu1EcommValue);

    if (i4Direction == BGP4_INCOMING)
    {
        u1EcommTable = BGP4_ECOMM_IN_FLT_TBL;
    }
    else if (i4Direction == BGP4_OUTGOING)
    {
        u1EcommTable = BGP4_ECOMM_OUT_FLT_TBL;
    }
    else
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigEcommFilterTable: Invalid Direction.");
        return BGP4_CLI_ERR_ECOMM_INV_DIRECTION;
    }

    if (i4DoAction == BGP4_ALLOW)
    {
        if (u1EcommTable == BGP4_ECOMM_IN_FLT_TBL)
        {
            i4Action = (EXT_COMM_ACCEPT + BGP4_INCREMENT_BY_ONE);
        }
        else
        {
            i4Action = (EXT_COMM_ADVERTISE + BGP4_INCREMENT_BY_ONE);
        }
    }
    else if (i4DoAction == BGP4_DENY)
    {
        if (u1EcommTable == BGP4_ECOMM_IN_FLT_TBL)
        {
            i4Action = (EXT_COMM_DENY + BGP4_INCREMENT_BY_ONE);
        }
        else
        {
            i4Action = (EXT_COMM_FILTER + BGP4_INCREMENT_BY_ONE);
        }
    }
    else
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigEcommFilterTable: Invalid Action.");
        return BGP4_CLI_ERR_ECOMM_INV_ACTION;
    }

    /* Extract Ecomm Value */
    EcommValue.pu1_OctetList = pu1EcommValue;
    EcommValue.i4_Length = EXT_COMM_VALUE_LEN;

    if (u1EcommTable == BGP4_ECOMM_IN_FLT_TBL)
    {
        i1RetVal =
            nmhValidateIndexInstanceFsbgp4ExtCommInFilterTable (&EcommValue);
    }
    else
    {
        i1RetVal =
            nmhValidateIndexInstanceFsbgp4ExtCommOutFilterTable (&EcommValue);
    }

    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigEcommFilterTable: Invalid Ext Comm Value\n");
        return BGP4_CLI_ERR_ECOMM_INV_ECOMM_VALUE;
    }

    if (u1EcommTable == BGP4_ECOMM_IN_FLT_TBL)
    {
        i1RetVal =
            nmhGetFsbgp4ExtCommInFilterRowStatus (&EcommValue, &i4RowStatus);
    }
    else
    {
        i1RetVal =
            nmhGetFsbgp4ExtCommOutFilterRowStatus (&EcommValue, &i4RowStatus);
    }

    if ((i1RetVal == BGP4_CLI_FAILURE) && (u1Set == VAL_NO))
    {
        /* Trying to delete a non-existing Entry */
        return BGP4_SUCCESS;
    }

    switch (u1Set)
    {
        case VAL_SET:
            /* i1RetVal derived from return value of
             * nmhGetFsbgp4ExtCommInFilterRowStatus or 
             * nmhGetFsbgp4ExtCommOutFilterRowStatus */
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                /* Entry not present in Ext Comm IN/OUT filter Table.
                 * Need to create a entry, set the input action and
                 * activate the entry. */
                if (u1EcommTable == BGP4_ECOMM_IN_FLT_TBL)
                {
                    i1RetVal =
                        nmhTestv2Fsbgp4ExtCommInFilterRowStatus (&u4ErrorCode,
                                                                 &EcommValue,
                                                                 CREATE_AND_WAIT);
                }
                else
                {
                    i1RetVal =
                        nmhTestv2Fsbgp4ExtCommOutFilterRowStatus (&u4ErrorCode,
                                                                  &EcommValue,
                                                                  CREATE_AND_WAIT);
                }
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigEcommFilterTable: Unable to create entry\n");
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }

                if (u1EcommTable == BGP4_ECOMM_IN_FLT_TBL)
                {
                    i1RetVal =
                        nmhSetFsbgp4ExtCommInFilterRowStatus (&EcommValue,
                                                              CREATE_AND_WAIT);
                }
                else
                {
                    i1RetVal =
                        nmhSetFsbgp4ExtCommOutFilterRowStatus (&EcommValue,
                                                               CREATE_AND_WAIT);
                }
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigEcommFilterTable: Unable to create entry\n");
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }

            }
            else
            {
                /* Already Entry is present. First make the entry row status
                 * not in service, set the input action and activate the
                 * entry */
                if (u1EcommTable == BGP4_ECOMM_IN_FLT_TBL)
                {
                    i1RetVal =
                        nmhTestv2Fsbgp4ExtCommInFilterRowStatus (&u4ErrorCode,
                                                                 &EcommValue,
                                                                 NOT_IN_SERVICE);
                }
                else
                {
                    i1RetVal =
                        nmhTestv2Fsbgp4ExtCommOutFilterRowStatus (&u4ErrorCode,
                                                                  &EcommValue,
                                                                  NOT_IN_SERVICE);
                }
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigEcommFilterTable: Unable to activate entry\n");
                    if (u1EcommTable == BGP4_ECOMM_IN_FLT_TBL)
                    {
                        nmhSetFsbgp4ExtCommInFilterRowStatus (&EcommValue,
                                                              DESTROY);
                    }
                    else
                    {
                        nmhSetFsbgp4ExtCommOutFilterRowStatus (&EcommValue,
                                                               DESTROY);
                    }
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }

                if (u1EcommTable == BGP4_ECOMM_IN_FLT_TBL)
                {
                    i1RetVal =
                        nmhSetFsbgp4ExtCommInFilterRowStatus (&EcommValue,
                                                              NOT_IN_SERVICE);
                }
                else
                {
                    i1RetVal =
                        nmhSetFsbgp4ExtCommOutFilterRowStatus (&EcommValue,
                                                               NOT_IN_SERVICE);
                }
                if (i1RetVal == BGP4_CLI_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigEcommFilterTable: Unable to activate entry\n");
                    if (u1EcommTable == BGP4_ECOMM_IN_FLT_TBL)
                    {
                        nmhSetFsbgp4ExtCommInFilterRowStatus (&EcommValue,
                                                              DESTROY);
                    }
                    else
                    {
                        nmhSetFsbgp4ExtCommOutFilterRowStatus (&EcommValue,
                                                               DESTROY);
                    }
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }

            }

            /* Entry present and not active. Set the input action and 
             * make the entry status as active */
            if (u1EcommTable == BGP4_ECOMM_IN_FLT_TBL)
            {
                i1RetVal =
                    nmhTestv2Fsbgp4ExtCommIncomingFilterStatus (&u4ErrorCode,
                                                                &EcommValue,
                                                                i4Action);
            }
            else
            {
                i1RetVal =
                    nmhTestv2Fsbgp4ExtCommOutgoingFilterStatus (&u4ErrorCode,
                                                                &EcommValue,
                                                                i4Action);
            }
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigEcommRouteTable: Unable to activate entry\n");
                if (u1EcommTable == BGP4_ECOMM_IN_FLT_TBL)
                {
                    nmhSetFsbgp4ExtCommInFilterRowStatus (&EcommValue, DESTROY);
                }
                else
                {
                    nmhSetFsbgp4ExtCommOutFilterRowStatus (&EcommValue,
                                                           DESTROY);
                }
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            if (u1EcommTable == BGP4_ECOMM_IN_FLT_TBL)
            {
                i1RetVal =
                    nmhSetFsbgp4ExtCommIncomingFilterStatus (&EcommValue,
                                                             i4Action);
            }
            else
            {
                i1RetVal =
                    nmhSetFsbgp4ExtCommOutgoingFilterStatus (&EcommValue,
                                                             i4Action);
            }
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigEcommRouteTable: Unable to activate entry\n");
                if (u1EcommTable == BGP4_ECOMM_IN_FLT_TBL)
                {
                    nmhSetFsbgp4ExtCommInFilterRowStatus (&EcommValue, DESTROY);
                }
                else
                {
                    nmhSetFsbgp4ExtCommOutFilterRowStatus (&EcommValue,
                                                           DESTROY);
                }
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            if (u1EcommTable == BGP4_ECOMM_IN_FLT_TBL)
            {
                i1RetVal =
                    nmhTestv2Fsbgp4ExtCommInFilterRowStatus (&u4ErrorCode,
                                                             &EcommValue,
                                                             ACTIVE);
            }
            else
            {
                i1RetVal =
                    nmhTestv2Fsbgp4ExtCommOutFilterRowStatus (&u4ErrorCode,
                                                              &EcommValue,
                                                              ACTIVE);
            }
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigEcommFilterTable: Unable to activate entry\n");
                if (u1EcommTable == BGP4_ECOMM_IN_FLT_TBL)
                {
                    nmhSetFsbgp4ExtCommInFilterRowStatus (&EcommValue, DESTROY);
                }
                else
                {
                    nmhSetFsbgp4ExtCommOutFilterRowStatus (&EcommValue,
                                                           DESTROY);
                }
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            if (u1EcommTable == BGP4_ECOMM_IN_FLT_TBL)
            {
                i1RetVal =
                    nmhSetFsbgp4ExtCommInFilterRowStatus (&EcommValue, ACTIVE);
            }
            else
            {
                i1RetVal =
                    nmhSetFsbgp4ExtCommOutFilterRowStatus (&EcommValue, ACTIVE);
            }
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigEcommFilterTable: Unable to activate entry\n");
                if (u1EcommTable == BGP4_ECOMM_IN_FLT_TBL)
                {
                    nmhSetFsbgp4ExtCommInFilterRowStatus (&EcommValue, DESTROY);
                }
                else
                {
                    nmhSetFsbgp4ExtCommOutFilterRowStatus (&EcommValue,
                                                           DESTROY);
                }
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            break;

        case VAL_NO:
            /* Entry present in Ext Comm IN/OUT filter Table. Need to Delete */
            if (u1EcommTable == BGP4_ECOMM_IN_FLT_TBL)
            {
                i1RetVal =
                    nmhTestv2Fsbgp4ExtCommInFilterRowStatus (&u4ErrorCode,
                                                             &EcommValue,
                                                             DESTROY);
            }
            else
            {
                i1RetVal =
                    nmhTestv2Fsbgp4ExtCommOutFilterRowStatus (&u4ErrorCode,
                                                              &EcommValue,
                                                              DESTROY);
            }
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigEcommFilterTable: Unable to delete entry\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            if (u1EcommTable == BGP4_ECOMM_IN_FLT_TBL)
            {
                i1RetVal =
                    nmhSetFsbgp4ExtCommInFilterRowStatus (&EcommValue, DESTROY);
            }
            else
            {
                i1RetVal =
                    nmhSetFsbgp4ExtCommOutFilterRowStatus (&EcommValue,
                                                           DESTROY);
            }
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigEcommFilterTable: Unable to delete entry\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            break;
        default:
            break;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigTrace                                            */
/* CLI Command   : (a) debug bgp {peer | update | fdb | keep | in | out |    */
/*                                damp | events | all}                       */
/*               : (b) no debug bgp {peer | update | fdb | keep | in | out | */
/*                                   damp | events | all}                    */
/* Description   : This command enables the appropriate trace in BGP.        */
/*               : Command (a) specifies the trace to be enabled.            */
/*               : Command (b) specifies the trace to be disabled.           */
/* Input(s)      : Trace level value (u4Trace)                               */
/*               : u4Trace can take a value of:                              */
/*               : For All Failure Trace         -  0x00000001               */
/*               : For Resource Alloc Fail Trace -  0x00000002               */
/*               : For Init & Shutdown Trace     -  0x00000004               */
/*               : For Management Trace          -  0x00000008               */
/*               : For Control Path Trace        -  0x00001000               */
/*               : For Data Path Trace           -  0x00002000               */
/*               : For PEER Conn Trace           -  0x00004000               */
/*               : For Update Message Trace      -  0x00008000               */
/*               : For FDB Update Trace          -  0x00010000               */
/*               : For Keep-Alive Trace          -  0x00020000               */
/*               : For All Transmission Trace    -  0x00040000               */
/*               : For All Receive Trace         -  0x00080000               */
/*               : For Damping Trace             -  0x00100000               */
/*               : For All Events Trace          -  0x00200000               */
/*               : For Dump - High level         -  0x10000000               */
/*               : For Dump - Low level          -  0x20000000               */
/*               : For Dump - Hex Dump           -  0x30000000               */
/*               : u1Set = VAL_SET for command (a) and                       */
/*                 u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigTrace (UINT4 u4Trace,    /* Value of Trace */
                UINT1 u1Set /* VAL_SET/VAL_NO */ )
{
    UINT4               u4SetTrace = 0;
    UINT4               u4ErrorCode = 0;
    INT1                i1RetVal = 0;

    i1RetVal = nmhTestv2Fsbgp4TraceEnable (&u4ErrorCode, u4Trace);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigTrace : Unsupported Trace option.\n");
        return BGP4_CLI_ERR_UNSUPPORTED_TRACE;
    }

    i1RetVal = nmhGetFsbgp4TraceEnable (&u4SetTrace);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigTrace : Unable to set Trace option\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    switch (u1Set)
    {
        case VAL_SET:
            if (u4Trace != 0)
            {
                /* Need to enable the specific trace message. */
                u4SetTrace |= u4Trace;
            }
            else
            {
                u4SetTrace = 0;
            }

            i1RetVal = nmhSetFsbgp4TraceEnable (u4SetTrace);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigTrace : Unable to set " "Trace option\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            break;

        case VAL_NO:
            if (u4Trace != 0)
            {
                /* Need to disable only the specific trace message. */
                u4SetTrace &= ~u4Trace;
            }
            else
            {
                u4SetTrace = 0;
            }
            i1RetVal = nmhSetFsbgp4TraceEnable (u4SetTrace);
            if (i1RetVal == BGP4_CLI_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigTrace : Unable to reset "
                          "Trace option\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            break;
        default:
            break;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4ShowAggregateRoute                                     */
/* CLI Command   : show ip bgp                                               */
/* Description   : Display the Aggregated Route.                             */
/* Input(s)      : pCookie - holds information about the route returned      */
/*               : in previous call.                                         */
/*               : u4BufLen - lenght of the buffer for storing the route.    */
/* Output(s)     : pu1Buffer - Stores the route informations.                */
/* Return(s)     : BGP4_SUCCESS    - if all no more route is left            */
/*               : BGP4_CLI_MORE   - if more routes still exists             */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
Bgp4ShowAggregateRoute (UINT1 *pu1Buffer, UINT4 u4BufLen,
                        tShowIpBgpCookie * pCookie, UINT1 u1IpDispFlag,
                        INT4 *pi4AggrIndex)
{
    tShowIpBgp         *pShowIpBgp = NULL;
    tShowIpBgpRoute    *pShowIpBgpRoute = NULL;
    INT4                i4AggrAdminStatus = 0;
    INT4                i4PrevAggrIndex = *pi4AggrIndex;
    tBgp4AggrDetails    aBgp4Aggrdetails;
    INT4                i4Retval = BGP4_SUCCESS;
    UINT4               u4Context = 0;
    UINT4               u4NoOfRoutes = 0;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    MEMSET ((VOID *) pu1Buffer, 0, u4BufLen);

    pShowIpBgp = (tShowIpBgp *) (VOID *) pu1Buffer;

    /* get and fill the local bgp router id */
    nmhGetFsbgp4Identifier (&pShowIpBgp->u4LocalRouterID);
    pShowIpBgp->u4TableVersion = BGP4_TABLE_VERSION (u4Context);
    pShowIpBgp->u4NoOfRoutes = 0;

    /* Initialize the Route pointer Structure */
    pShowIpBgpRoute = &(pShowIpBgp->aRoutes[0]);

    /* calculate the number of routes that could be accomodated into this
     * buffer
     */
    u4NoOfRoutes = (u4BufLen -
                    ((sizeof (tShowIpBgp)) - (sizeof (tShowIpBgpRoute)))) /
        sizeof (tShowIpBgpRoute);
    if (*pi4AggrIndex == 0)
    {
        if (nmhGetFirstIndexFsbgp4MpeAggregateTable (pi4AggrIndex) ==
            BGP4_CLI_FAILURE)
        {
            return BGP4_SUCCESS;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeAggregateTable (i4PrevAggrIndex,
                                                    pi4AggrIndex) ==
            BGP4_CLI_FAILURE)
        {
            return BGP4_SUCCESS;
        }
    }
    do
    {
        nmhGetFsbgp4mpeAggregateAdminStatus (*pi4AggrIndex, &i4AggrAdminStatus);
        if (i4AggrAdminStatus == BGP4_ADMIN_UP)
        {
            i4Retval =
                Bgp4GetAggregateDetails (&aBgp4Aggrdetails, *pi4AggrIndex);
            if ((i4Retval == BGP4_SUCCESS)
                && (aBgp4Aggrdetails.pAggregatedRoute != NULL)
                && (PrefixMatch
                    (aBgp4Aggrdetails.pAggregatedRoute->NetAddress.NetAddr,
                     pCookie->IpAddress.NetAddr) == BGP4_TRUE))
            {
                if (u1IpDispFlag == BGP_CLI_IP_SHOW_IP_PREF)
                {
                    if (pCookie->IpAddress.u2PrefixLen !=
                        BGP4_RT_PREFIXLEN (aBgp4Aggrdetails.pAggregatedRoute))
                    {
                        i4PrevAggrIndex = *pi4AggrIndex;
                        continue;
                    }
                }
                bgpCliPkgGetRouteAttributes (aBgp4Aggrdetails.pAggregatedRoute,
                                             pShowIpBgpRoute,
                                             BGP4_CLI_RIB_SPECIFIED);
                if (pShowIpBgpRoute->i4RouteStatus & BGP_CLI_ROUTE_SUPPRESSED
                    && ((BGP4_RT_GET_FLAGS (aBgp4Aggrdetails.pAggregatedRoute)
                         & BGP4_RT_BEST) == BGP4_RT_BEST))
                {
                    pShowIpBgpRoute->i4RouteStatus &=
                        ~(BGP_CLI_ROUTE_SUPPRESSED);
                    pShowIpBgpRoute->i4RouteStatus |= BGP_CLI_ROUTE_BEST;
                }
                if (BGP4_AGGRENTRY_AS_ATOMIC_AGGR
                    (*pi4AggrIndex - (BGP4_DECREMENT_BY_ONE)) == BGP4_TRUE)
                {
                    pShowIpBgpRoute->i4RouteStatus |= BGP_CLI_ROUTE_AGGR;
                    pShowIpBgpRoute->i4RouteStatus |= BGP_CLI_ROUTE_ATOMIC_AGGR;
                }
                pShowIpBgp->u4NoOfRoutes++;
                u4NoOfRoutes--;
                if (u4NoOfRoutes)
                {
                    /* Increment the pShowIpBgpRoute pointer to store the next
                     * route information. */
                    pShowIpBgpRoute++;
                }
                else
                {
                    break;
                }
            }
        }
        i4PrevAggrIndex = *pi4AggrIndex;
    }
    while (nmhGetNextIndexFsbgp4MpeAggregateTable (i4PrevAggrIndex,
                                                   pi4AggrIndex) !=
           BGP4_CLI_FAILURE);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpShowIpv4Bgp                                            */
/* CLI Command   : show ip bgp                                               */
/* Description   : Display the IPv4 routes received from all the Peers.      */
/* Input(s)      : pCookie - holds information about the route returned      */
/*               : in previous call.                                         */
/*               : u4BufLen - lenght of the buffer for storing the route.    */
/* Output(s)     : pu1Buffer - Stores the route informations.                */
/* Return(s)     : BGP4_SUCCESS    - if all no more route is left            */
/*               : BGP4_CLI_MORE   - if more routes still exists             */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpShowIpv4Bgp (UINT1 *pu1Buffer,    /* non-NULL buffer, where the 
                                     * table is returned. this
                                     * buffer must be big enough
                                     * to hold atleast one route 
                                     * (i.e. sizeof(tShowIpBgp)).
                                     */
                UINT4 u4BufLen,    /* length of pu1Buffer          */
                tShowIpBgpCookie * pCookie    /* Cookie, used to fetch tables
                                             * that are greater than length
                                             * u4BufLen */ ,
                UINT4 u4AsafiMask, UINT1 u1IpPrefSpec    /* Flag for indicating the prefix 
                                                           specified in CLI */ )
{
    return (bgpShowIpv4BgpRIB
            (pu1Buffer, u4BufLen, u4AsafiMask, pCookie, u1IpPrefSpec));
}

/*****************************************************************************/
/* Function Name : bgpShowIpv6Bgp                                            */
/* CLI Command   : show bgp ipv6                                             */
/* Description   : Display the ipv6 routes received from all the Peers.      */
/* Input(s)      : pCookie - holds information about the route returned      */
/*               : in previous call.                                         */
/*               : u4BufLen - lenght of the buffer for storing the route.    */
/* Output(s)     : pu1Buffer - Stores the route informations.                */
/* Return(s)     : BGP4_SUCCESS    - if all no more route is left            */
/*               : BGP4_CLI_MORE   - if more routes still exists             */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpShowIpv6Bgp (UINT1 *pu1Buffer,
                /* non-NULL buffer, where the 
                 * table is returned. this
                 * buffer must be big enough
                 * to hold atleast one route 
                 * (i.e. sizeof(tShowIpBgp)).
                 */
                UINT4 u4BufLen,    /* length of pu1Buffer          */
                tShowIpBgpCookie * pCookie,    /* Cookie, used to fetch tables
                                             * that are greater than length
                                             * u4BufLen
                                             */
                UINT1 u1IpPrefSpec    /* Flag for indicating the prefix
                                       specified in CLI */
    )
{
#ifdef BGP4_IPV6_WANTED
    return (bgpShowIpv6BgpRIB (pu1Buffer, u4BufLen, pCookie, u1IpPrefSpec));
#else
    UNUSED_PARAM (pu1Buffer);
    UNUSED_PARAM (u4BufLen);
    UNUSED_PARAM (pCookie);
    UNUSED_PARAM (u1IpPrefSpec);
    return BGP4_FAILURE;
#endif
}

#ifdef L3VPN
/*****************************************************************************/
/* Function Name : bgpShowIpBgpLabel                                         */
/* CLI Command   : show ip bgp label                                         */
/* Description   : Display the FutureBGP Local RIB Database with Label.      */
/* Input(s)      : pCookie - holds information about the route returned      */
/*               : in previous call.                                         */
/*               : u4BufLen - lenght of the buffer for storing the route.    */
/* Output(s)     : pu1Buffer - Stores the route informations.                */
/* Return(s)     : BGP4_SUCCESS    - if all no more route is left            */
/*               : BGP4_CLI_MORE   - if more routes still exists             */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpShowIpBgpLabel (UINT1 *pu1Buffer,    /* non-NULL buffer, where the 
                                         * table is returned. this
                                         * buffer must be big enough
                                         * to hold atleast one route 
                                         * (i.e. sizeof(tShowIpBgp)).
                                         */
                   UINT4 u4BufLen,    /* length of pu1Buffer          */
                   UINT4 u4AsafiMask, tShowIpBgpCookie * pCookie    /* Cookie, used to fetch tables
                                                                     * that are greater than length
                                                                     * u4BufLen
                                                                     */
    )
{
    tNetAddress         InAddr;
    tShowIpBgp         *pShowIpBgp = NULL;
    tShowIpBgpRoute    *pShowIpBgpRoute = NULL;
    tRouteProfile      *pRoute = NULL;
    tRouteProfile      *pCurRoute = NULL;
    tRouteProfile      *pPrevRoute = NULL;
    tRouteProfile      *pFndRtProfileList = NULL;
    VOID               *pRibNode = NULL;
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tRouteProfile       Route;
    UINT4               u4NoOfRoutes;
    UINT4               u4RoutesExist = 0;
    UINT4               u4RtAsafiMask;
    INT4                i4Return = BGP4_SUCCESS;
    INT4                i4Found = BGP4_FAILURE;
    INT4                i4Match = BGP4_FALSE;

    UINT4               u4Context = 0;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    /* check for NULL buffer or too small buffer */
    BGP4_ASSERT ((pu1Buffer) && (u4BufLen >= (sizeof (tShowIpBgp))));

    /* cookie cannot be NULL */
    BGP4_ASSERT (pCookie);

    /* check for valid u4Cookie; if cookie is invalid, then this is a fresh
     * call to bgpShowIpBgp - start from the begining. If cookie is valid, then
     * use to cookie to continue from where we left last time
     */
    switch (u4AsafiMask)
    {
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
            Bgp4InitNetAddressStruct (&InAddr, BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_LABEL);
            break;
        case CAP_MP_VPN4_UNICAST:
            Bgp4InitNetAddressStruct (&InAddr, BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_VPNV4_UNICAST);
            pRibNode = BGP4_VPN4_RIB_TREE (u4Context);
            break;
#endif
        default:
            Bgp4InitNetAddressStruct (&InAddr, BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);
            break;
    }
    if (MEMCMP (&(pCookie->IpAddress), &InAddr, sizeof (tNetAddress)) != 0)
    {
        /* valid cookie */
        /* Since cookie is valid, routes do exist in the database. Try and get
         * the current route from RIB. */
        MEMSET (&Route, 0, sizeof (tRouteProfile));
#ifdef L3VPN
        if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
        {
            Bgp4CopyVpn4NetAddressToRt (&Route, pCookie->IpAddress);
        }
        else
        {
            Bgp4CopyNetAddressStruct (&(BGP4_RT_NET_ADDRESS_INFO ((&Route))),
                                      pCookie->IpAddress);
        }
#endif
        BGP4_RT_PROTOCOL ((&Route)) = pCookie->u1Protocol;

        if (bgp4RibLock () != OSIX_SUCCESS)
        {
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }
        i4Found = Bgp4RibhLookupRtEntry (&Route, u4Context,
                                         BGP4_TREE_FIND_EXACT,
                                         &pFndRtProfileList, &pRibNode);
        if (i4Found == BGP4_FAILURE)
        {
            /* Unable to find the current route in the RIB. */
            bgp4RibUnlock ();
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }
        pRoute = pFndRtProfileList;

        while (pRoute != NULL)
        {
            /* Check if the route matches the current route
             * or not. */
            BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRoute),
                                   BGP4_RT_SAFI_INFO (pRoute), u4RtAsafiMask);
            if (u4AsafiMask != u4RtAsafiMask)
            {
                pRoute = BGP4_RT_NEXT (pRoute);
                continue;
            }
#ifdef L3VPN
            if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
            {
                if ((BGP4_RT_PROTOCOL (pRoute) != BGP_ID) ||
                    ((BGP4_RT_PROTOCOL (pRoute) == BGP_ID) &&
                     (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRoute))
                      == BGP4_VPN4_CE_PEER)))
                {
                    pRoute = BGP4_RT_NEXT (pRoute);
                    continue;
                }
                i4Match = Vpnv4AddrMatch (pCookie->IpAddress.NetAddr, pRoute,
                                          BGP4_RT_PREFIXLEN (pRoute));
            }
            else
            {
#endif
                i4Match = PrefixMatch (pRoute->NetAddress.NetAddr,
                                       pCookie->IpAddress.NetAddr);
#ifdef L3VPN
            }
#endif
            if ((i4Match == BGP4_TRUE) &&
                (BGP4_RT_PREFIXLEN (pRoute) == pCookie->IpAddress.u2PrefixLen)
                && (BGP4_RT_PROTOCOL (pRoute) == pCookie->u1Protocol))
            {
                if (BGP4_RT_PROTOCOL (pRoute) == BGP_ID)
                {
                    pPeerEntry = BGP4_RT_PEER_ENTRY (pRoute);
                    if (MEMCMP (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                                &(pCookie->PeerAddr), sizeof (tAddrPrefix))
                        == 0)
                    {
                        /* Match route found. */
                        break;
                    }
                }
                else
                {
                    /* Match route found. */
                    break;
                }
            }
            pRoute = BGP4_RT_NEXT (pRoute);
        }

        if (pRoute == NULL)
        {
            /* Unable to find the current route in the RIB. */
            bgp4RibUnlock ();
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }
        /* Current route exists. */
        u4RoutesExist = 1;
    }
    else
    {
        /* invalid cookie; get the first index */
        if (bgp4RibLock () != OSIX_SUCCESS)
        {
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        Bgp4GetRibNode (u4Context, u4AsafiMask, &pRibNode);
        if (pRibNode == NULL)
        {
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        i4Found = Bgp4RibhGetFirstEntry (u4AsafiMask, &pRoute, &pRibNode, TRUE);
        if ((i4Found == BGP4_SUCCESS) && (pRoute != NULL))
        {
            /* GetFirst has succeeded, hence, the routes exist in the database;
             * set the u4RoutesExist flag to 1, to indicate that the routes
             * exist. If u4RoutesExist flag is 0, then no routes exist in the
             * database.
             */
            bgp4RibUnlock ();
            u4RoutesExist = 1;
        }
        else
        {
            /* No routes present in the BGP */
            bgp4RibUnlock ();
            return i4Return;
        }
    }

    MEMSET ((VOID *) pu1Buffer, 0, u4BufLen);

    pShowIpBgp = (tShowIpBgp *) (VOID *) pu1Buffer;

    pShowIpBgp->u4NoOfRoutes = 0;

    /* Initialize the Route pointer Structure */
    pShowIpBgpRoute = &(pShowIpBgp->aRoutes[0]);

    /* if u4RoutesExist flag is set to 1 then routes exist in the database;
     * else no routes exist in the database - just return.
     */
    if (u4RoutesExist)
    {
        /* calculate the number of routes that could be accomodated into this
         * buffer
         */
        u4NoOfRoutes = (u4BufLen -
                        ((sizeof (tShowIpBgp)) - (sizeof (tShowIpBgpRoute)))) /
            sizeof (tShowIpBgpRoute);

        do
        {
            if (!u4NoOfRoutes)
            {
                /* we have run out of buffer; but we have more routes in the
                 * database to be returned. Hence set the cookie in the return
                 * buffer
                 */
#ifdef L3VPN
                if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
                {
                    Bgp4CopyVpn4NetAddressFromRt (&(pCookie->IpAddress),
                                                  pRoute);
                }
                else
                {
#endif
                    Bgp4CopyNetAddressStruct (&pCookie->IpAddress,
                                              pRoute->NetAddress);
#ifdef L3VPN
                }
#endif
                pCookie->u1Protocol = BGP4_RT_PROTOCOL (pRoute);
                if (BGP4_RT_PROTOCOL (pRoute) == BGP_ID)
                {
                    pPeerEntry = BGP4_RT_PEER_ENTRY (pRoute);
                    Bgp4CopyAddrPrefixStruct (&pCookie->PeerAddr,
                                              BGP4_PEER_REMOTE_ADDR_INFO
                                              (pPeerEntry));
                }
                else
                {
                    MEMSET (&pCookie->PeerAddr, 0, sizeof (tAddrPrefix));
                }

                /* set return value to indicate that we ran out of buffer
                 * space and more data is present in the database to be
                 * sent.
                 */
                i4Return = BGP4_CLI_MORE;
                break;

            }

            BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRoute),
                                   BGP4_RT_SAFI_INFO (pRoute), u4RtAsafiMask);
#ifdef L3VPN
            if ((u4RtAsafiMask == CAP_MP_LABELLED_IPV4) ||
                ((u4AsafiMask == CAP_MP_VPN4_UNICAST) &&
                 ((BGP4_RT_PROTOCOL (pRoute) == BGP_ID) &&
                  (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRoute))
                   != BGP4_VPN4_CE_PEER))))
            {
                if (bgp4RibLock () != OSIX_SUCCESS)
                {
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }

                /* fill the fields of this entry */
                if (bgpGetRouteLabels (pRoute, pShowIpBgpRoute) == BGP4_FAILURE)
                {
                    bgp4RibUnlock ();
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tbgpShowIpBgp:bgpGetRouteLabels  failed\n");
                    return BGP4_CLI_ERR_GENERAL_ERROR;
                }
                bgp4RibUnlock ();
                /* Increment the stored route count and decrement the max 
                 * possible route count */
                pShowIpBgp->u4NoOfRoutes++;
                u4NoOfRoutes--;

                if (u4NoOfRoutes)
                {
                    /* Increment the pShowIpBgpRoute pointer to store the next
                     * route information. */
                    pShowIpBgpRoute++;
                }
            }
#endif
            pCurRoute = BGP4_RT_NEXT (pRoute);
            if (pCurRoute != NULL)
            {
                pRoute = pCurRoute;
                continue;
            }
            /* prepare to get the next indices */
            if (bgp4RibLock () != OSIX_SUCCESS)
            {
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            /* get the next indecies to the table */
            pPrevRoute = pRoute;
            pRoute = NULL;
            i4Found = Bgp4RibhGetNextEntry (pPrevRoute, u4Context,
                                            &pRoute, &pRibNode, TRUE);
            if (i4Found != BGP4_SUCCESS)
            {
                bgp4RibUnlock ();
                break;
            }
            bgp4RibUnlock ();
        }
        while ((pRoute != NULL));
    }
    return (i4Return);
}
#endif /* L3VPN */

/*****************************************************************************/
/* Function Name : bgpShowIpv4BgpRIB                                         */
/* CLI Command   : show ip bgp rib                                           */
/* Description   : Display the FutureBGP Local RIB Database.                 */
/* Input(s)      : pCookie - holds information about the route returned      */
/*               : in previous call.                                         */
/*               : u4BufLen - lenght of the buffer for storing the route.    */
/* Output(s)     : pu1Buffer - Stores the route informations.                */
/* Return(s)     : BGP4_SUCCESS    - if no more route is left                */
/*               : BGP4_CLI_MORE   - if more routes still exists             */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpShowIpv4BgpRIB (UINT1 *pu1Buffer,    /* non-NULL buffer, where the 
                                         * table is returned. this
                                         * buffer must be big enough
                                         * to hold atleast one route 
                                         * (i.e. sizeof(tShowIpBgp)).
                                         */
                   UINT4 u4BufLen,    /* length of pu1Buffer          */
                   UINT4 u4AsafiMask, tShowIpBgpCookie * pCookie,    /* Cookie, used to fetch tables
                                                                     * that are greater than length
                                                                     * u4BufLen
                                                                     */
                   UINT1 u1IpPrefSpec    /* Flag for indicating the prefix
                                           specified in CLI */
    )
{
    tNetAddress         InAddr;
    tAddrPrefix         PeerAddr;
    tShowIpBgp         *pShowIpBgp = NULL;
    tShowIpBgpRoute    *pShowIpBgpRoute = NULL;
    tRouteProfile      *pRoute = NULL;
    tRouteProfile      *pCurRoute = NULL;
    tRouteProfile      *pPrevRoute = NULL;
    tRouteProfile      *pFndRtProfileList = NULL;
    VOID               *pRibNode = NULL;
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tRouteProfile       Route;
    UINT4               u4NoOfRoutes;
    UINT4               u4RoutesExist = 0;
    INT4                i4Return = BGP4_SUCCESS;
    INT4                i4Found = BGP4_FAILURE;
    INT4                i4IpPrefTempFlag = 0;
    UINT2               u2PrefixLen = 0;

    UINT4               u4Context = 0;
    UINT4               u4RouteAddr = 0;
    UINT4               u4BufAddr = 0;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    /* check for NULL buffer or too small buffer */
    BGP4_ASSERT ((pu1Buffer) && (u4BufLen >= (sizeof (tShowIpBgp))));

    /* cookie cannot be NULL */
    BGP4_ASSERT (pCookie);

    /* check for valid u4Cookie; if cookie is invalid, then this is a fresh
     * call to bgpShowIpBgp - start from the begining. If cookie is valid, then
     * use to cookie to continue from where we left last time
     */
    Bgp4InitAddrPrefixStruct (&PeerAddr, BGP4_INET_AFI_IPV4);

    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
            Bgp4InitNetAddressStruct (&InAddr, BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);
            break;
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
            Bgp4InitNetAddressStruct (&InAddr, BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_LABEL);
            break;
        case CAP_MP_VPN4_UNICAST:
            Bgp4InitNetAddressStruct (&InAddr, BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_VPNV4_UNICAST);
            break;
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
            Bgp4InitNetAddressStruct (&InAddr, BGP4_INET_AFI_L2VPN,
                                      BGP4_INET_SAFI_VPLS);
            break;
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
            Bgp4InitNetAddressStruct (&InAddr, BGP4_INET_AFI_L2VPN,
                                      BGP4_INET_SAFI_EVPN);
            break;
#endif

        default:
            Bgp4InitNetAddressStruct (&InAddr, BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);
            break;
    }

    if ((pCookie->u1Protocol != 0) ||
        ((MEMCMP (&(pCookie->IpAddress), &InAddr, sizeof (tNetAddress)) != 0) &&
         (MEMCMP (&(pCookie->PeerAddr), &PeerAddr, sizeof (tAddrPrefix)) != 0))
        || (u1IpPrefSpec == BGP_CLI_IP_SHOW_IP_PREF))
    {
        /* valid cookie */
        /* Since cookie is valid, routes do exist in the database. Try and get
         * the current route from RIB. */
        MEMSET (&Route, 0, sizeof (tRouteProfile));
        Bgp4CopyNetAddressStruct (&(BGP4_RT_NET_ADDRESS_INFO ((&Route))),
                                  pCookie->IpAddress);
        BGP4_RT_PROTOCOL ((&Route)) = pCookie->u1Protocol;
#ifdef L3VPN
        if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
        {
            MEMCPY (Route.au1RouteDisting, pCookie->au1RouteDisting, 8);
        }
#endif
#ifdef EVPN_WANTED
        if (u4AsafiMask == CAP_MP_L2VPN_EVPN)
        {
            MEMCPY (Route.EvpnNlriInfo.au1RouteDistinguisher,
                    pCookie->au1RouteDisting, MAX_LEN_RD_VALUE);

            Route.EvpnNlriInfo.u4VnId = pCookie->u4VnId;
            MEMCPY (Route.EvpnNlriInfo.MacAddress, pCookie->MacAddress,
                    sizeof (tMacAddr));
            MEMCPY (Route.EvpnNlriInfo.au1EthernetSegmentID,
                    pCookie->au1EthSegId, MAX_LEN_ETH_SEG_ID);
            Route.EvpnNlriInfo.u1RouteType = pCookie->u1RouteType;
        }
#endif

        i4Found = Bgp4RibhLookupRtEntry (&Route, u4Context,
                                         BGP4_TREE_FIND_EXACT,
                                         &pFndRtProfileList, &pRibNode);
        if (i4Found == BGP4_FAILURE)
        {
            /* Unable to find the current route in the RIB. */
            bgp4RibUnlock ();
            CLI_SET_ERR (CLI_BGP4_INVALID_RIB_ROUTE);
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }
        pRoute = pFndRtProfileList;

        while (pRoute != NULL)
        {
            /* Check if the route matches the current route
             * or not. */

            if ((PrefixMatch (pRoute->NetAddress.NetAddr,
                              pCookie->IpAddress.NetAddr) == BGP4_TRUE) &&
                (BGP4_RT_PREFIXLEN (pRoute) == pCookie->IpAddress.u2PrefixLen)
                && ((pCookie->u1Protocol == 0) ||
                    (BGP4_RT_PROTOCOL (pRoute) == pCookie->u1Protocol)))
            {
                if ((BGP4_RT_PROTOCOL (pRoute) == BGP_ID)
                    &&
                    ((u1IpPrefSpec == 0 || u1IpPrefSpec == BGP4_RFD_DAMPED_PATH
                      || u1IpPrefSpec == BGP4_RFD_FLAP_STAT)
                     ||
                     ((u1IpPrefSpec == BGP_CLI_IP_SHOW_IP
                       || u1IpPrefSpec == BGP_CLI_IP_SHOW_IP_PREF)
                      &&
                      (MEMCMP
                       (&(pCookie->PeerAddr), &PeerAddr,
                        sizeof (tAddrPrefix)) != 0))))
                {
                    pPeerEntry = BGP4_RT_PEER_ENTRY (pRoute);
                    if (MEMCMP (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                                &(pCookie->PeerAddr), sizeof (tAddrPrefix))
                        == 0)
                    {
                        if (pCookie->u1AdvFlag == BGP4_RT_GET_ADV_FLAG (pRoute))
                        {
                            /* Match route found. */
                            break;
                        }
                    }
                }
                else if (BGP4_RT_PROTOCOL (pRoute) != BGP_ID)
                {
                    if (MEMCMP (&(BGP4_RT_IMMEDIATE_NEXTHOP_INFO (pRoute)),
                                &(pCookie->PeerAddr),
                                sizeof (tAddrPrefix)) == 0)
                    {
                        if (pCookie->u1AdvFlag == BGP4_RT_GET_ADV_FLAG (pRoute))
                        {
                            /* Match route found. */
                            break;
                        }
                    }
                }
                else
                {
                    /* Match route found. */
                    break;
                }
            }
            pRoute = BGP4_RT_NEXT (pRoute);
        }

        if (pRoute == NULL)
        {
            /* Unable to find the current route in the RIB. */
            bgp4RibUnlock ();
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        /* Current route exists. */
        u4RoutesExist = 1;
    }
    else
    {

        Bgp4GetRibNode (u4Context, u4AsafiMask, &pRibNode);
        if (pRibNode == NULL)
        {
            bgp4RibUnlock ();
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        /* invalid cookie; get the first index */
        i4Found = Bgp4RibhGetFirstEntry (u4AsafiMask, &pRoute, &pRibNode, TRUE);
        if ((i4Found == BGP4_SUCCESS) && (pRoute != NULL))
        {
            /* GetFirst has succeeded, hence, the routes exist in the database;
             * set the u4RoutesExist flag to 1, to indicate that the routes
             * exist. If u4RoutesExist flag is 0, then no routes exist in the
             * database.
             */
            u4RoutesExist = 1;
        }
        else
        {
            /* No routes present in the BGP */
            bgp4RibUnlock ();
            return i4Return;
        }
    }

    MEMSET ((VOID *) pu1Buffer, 0, u4BufLen);

    pShowIpBgp = (tShowIpBgp *) (VOID *) pu1Buffer;

    /* get and fill the local bgp router id */
    nmhGetFsbgp4Identifier (&pShowIpBgp->u4LocalRouterID);
    pShowIpBgp->u4TableVersion = BGP4_TABLE_VERSION (u4Context);
    pShowIpBgp->u4NoOfRoutes = 0;

    /* Initialize the Route pointer Structure */
    pShowIpBgpRoute = &(pShowIpBgp->aRoutes[0]);

    /* if u4RoutesExist flag is set to 1 then routes exist in the database;
     * else no routes exist in the database - just return.
     */
    if (u4RoutesExist)
    {
        /* calculate the number of routes that could be accomodated into this
         * buffer
         */
        u4NoOfRoutes = (u4BufLen -
                        ((sizeof (tShowIpBgp)) - (sizeof (tShowIpBgpRoute)))) /
            sizeof (tShowIpBgpRoute);

        do
        {
            i4IpPrefTempFlag = 0;
            if (u1IpPrefSpec == BGP_CLI_IP_SHOW_IP_PREF)
            {
                u2PrefixLen = (pRoute->NetAddress.u2PrefixLen / MASK_SHIFT_LEN);
                MEMCPY (&u4RouteAddr, &(pRoute->NetAddress.NetAddr.au1Address),
                        BGP4_IPV4_PREFIX_LEN);
                MEMCPY (&u4BufAddr, &(pCookie->IpAddress.NetAddr.au1Address),
                        BGP4_IPV4_PREFIX_LEN);
                u4RouteAddr = OSIX_HTONL (u4RouteAddr);
                u4BufAddr = OSIX_HTONL (u4BufAddr);
                if (u2PrefixLen == 0)
                {
                    u2PrefixLen = BGP4_IPV4_PREFIX_LEN;
                }
                if (MEMCMP (&u4RouteAddr, &u4BufAddr, u2PrefixLen) == 0)
                {
                    i4IpPrefTempFlag = 1;
                    if ((u1IpPrefSpec == BGP_CLI_IP_SHOW_IP_PREF) &&
                        (pRoute->NetAddress.u2PrefixLen !=
                         pCookie->IpAddress.u2PrefixLen))
                    {
                        i4IpPrefTempFlag = 0;
                    }
                }
            }
            else if (u1IpPrefSpec == BGP_CLI_IP_SHOW_IP)
            {
                if (MEMCMP (&(pRoute->NetAddress.NetAddr.au1Address),
                            &(pCookie->IpAddress.NetAddr.au1Address),
                            sizeof (pRoute->NetAddress.NetAddr.au1Address)) ==
                    0)
                {
                    i4IpPrefTempFlag = 1;
                }
            }
            else
            {
                i4IpPrefTempFlag = 1;
            }

            if (i4IpPrefTempFlag == 1)
            {
                if (!u4NoOfRoutes)
                {
                    /* we have run out of buffer; but we have more routes in the
                     * database to be returned. Hence set the cookie in the return
                     * buffer */
                    Bgp4CopyNetAddressStruct (&pCookie->IpAddress,
                                              pRoute->NetAddress);
                    pCookie->u1Protocol = BGP4_RT_PROTOCOL (pRoute);
                    pCookie->u1AdvFlag = BGP4_RT_GET_ADV_FLAG (pRoute);
#ifdef L3VPN
                    if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
                    {
                        MEMCPY (pCookie->au1RouteDisting,
                                pRoute->au1RouteDisting, 8);
                    }
#endif
#ifdef EVPN_WANTED
                    if (u4AsafiMask == CAP_MP_L2VPN_EVPN)
                    {
                        MEMCPY (pCookie->au1RouteDisting,
                                pRoute->EvpnNlriInfo.au1RouteDistinguisher,
                                MAX_LEN_RD_VALUE);
                        pCookie->u4VnId = pRoute->EvpnNlriInfo.u4VnId;
                        MEMCPY (pCookie->MacAddress,
                                pRoute->EvpnNlriInfo.MacAddress,
                                sizeof (tMacAddr));
                        MEMCPY (pCookie->au1EthSegId,
                                pRoute->EvpnNlriInfo.au1EthernetSegmentID,
                                MAX_LEN_ETH_SEG_ID);
                        pCookie->u1RouteType = pRoute->EvpnNlriInfo.u1RouteType;
                    }
#endif
                    if (BGP4_RT_PROTOCOL (pRoute) == BGP_ID)
                    {
                        pPeerEntry = BGP4_RT_PEER_ENTRY (pRoute);
                        Bgp4CopyAddrPrefixStruct (&pCookie->PeerAddr,
                                                  BGP4_PEER_REMOTE_ADDR_INFO
                                                  (pPeerEntry));
                    }
                    else
                    {
                        /* To handle ECMP routes from other protocols */
                        MEMSET (&pCookie->PeerAddr, 0, sizeof (tAddrPrefix));
                        Bgp4CopyAddrPrefixStruct (&pCookie->PeerAddr,
                                                  BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                                  (pRoute));
                    }

                    /* set return value to indicate that we ran out of buffer
                     * space and more data is present in the database to be
                     * sent. */
                    i4Return = BGP4_CLI_MORE;
                    break;
                }

                /* fill the fields of this entry */

                bgpCliPkgGetRouteAttributes (pRoute, pShowIpBgpRoute,
                                             BGP4_CLI_RIB_SPECIFIED);
#ifdef L3VPN
                if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
                {
                    /* fill the fields of this entry */
                    if (bgpGetRouteLabels (pRoute, pShowIpBgpRoute) ==
                        BGP4_FAILURE)
                    {
                        bgp4RibUnlock ();
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                                  "\tbgpShowIpv4BgpRIB:bgpGetRouteLabels  failed\n");
                        return BGP4_CLI_ERR_GENERAL_ERROR;
                    }
                }
#endif /* L3VPN */

                /* Increment the stored route count and decrement the max possible
                 * route count */
                pShowIpBgp->u4NoOfRoutes++;
                u4NoOfRoutes--;

                if (u4NoOfRoutes)
                {
                    /* Increment the pShowIpBgpRoute pointer to store the next
                     * route information. */
                    pShowIpBgpRoute++;
                }
            }
            pCurRoute = BGP4_RT_NEXT (pRoute);
            if (pCurRoute != NULL)
            {
                pRoute = pCurRoute;
                continue;
            }

            /* get the next indecies to the table */
            pPrevRoute = pRoute;
            pRoute = NULL;
            i4Found = Bgp4RibhGetNextEntry (pPrevRoute, u4Context,
                                            &pRoute, &pRibNode, TRUE);
            if (i4Found != BGP4_SUCCESS)
            {
                break;
            }
        }
        while ((pRoute != NULL));
    }
    bgp4RibUnlock ();
    return (i4Return);
}

/*****************************************************************************/
/* Function Name : bgpShowIpv6BgpRIB                                         */
/* CLI Command   : show bgp ipv6 rib                                         */
/* Description   : Display the FutureBGP v6 Local RIB Database.              */
/* Input(s)      : pCookie - holds information about the route returned      */
/*               : in previous call.                                         */
/*               : u4BufLen - lenght of the buffer for storing the route.    */
/* Output(s)     : pu1Buffer - Stores the route informations.                */
/* Return(s)     : BGP4_SUCCESS    - if all no more route is left            */
/*               : BGP4_CLI_MORE   - if more routes still exists             */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpShowIpv6BgpRIB (UINT1 *pu1Buffer,    /* non-NULL buffer, where the 
                                         * table is returned. this
                                         * buffer must be big enough
                                         * to hold atleast one route 
                                         * (i.e. sizeof(tShowIpBgp)).
                                         */
                   UINT4 u4BufLen,    /* length of pu1Buffer          */
                   tShowIpBgpCookie * pCookie,    /* Cookie, used to fetch tables
                                                 * that are greater than length
                                                 * u4BufLen
                                                 */
                   UINT1 u1IpPrefSpec    /* Flag for indicating the prefix
                                           specified in CLI */
    )
{
#ifdef BGP4_IPV6_WANTED
    tNetAddress         InAddr;
    tAddrPrefix         PeerAddr;
    tShowIpBgp         *pShowIpBgp = NULL;
    tShowIpBgpRoute    *pShowIpBgpRoute = NULL;
    tRouteProfile      *pRoute = NULL;
    tRouteProfile      *pCurRoute = NULL;
    tRouteProfile      *pPrevRoute = NULL;
    tRouteProfile      *pFndRtProfileList = NULL;
    VOID               *pRibNode = NULL;
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tRouteProfile       Route;
    UINT4               u4NoOfRoutes;
    UINT4               u4RoutesExist = 0;
    INT4                i4Return = BGP4_SUCCESS;
    INT4                i4Found = BGP4_FAILURE;
    INT4                i4IpPrefTempFlag = 0;
    UINT2               u2PrefixLen = 0;

    UINT4               u4Context = 0;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    /* check for NULL buffer or too small buffer */
    BGP4_ASSERT ((pu1Buffer) && (u4BufLen >= (sizeof (tShowIpBgp))));

    /* cookie cannot be NULL */
    BGP4_ASSERT (pCookie);

    /* check for valid u4Cookie; if cookie is invalid, then this is a fresh
     * call to bgpShowIpBgp - start from the begining. If cookie is valid, then
     * use to cookie to continue from where we left last time
     */
    Bgp4InitNetAddressStruct (&InAddr, BGP4_INET_AFI_IPV6,
                              BGP4_INET_SAFI_UNICAST);
    Bgp4InitAddrPrefixStruct (&PeerAddr, BGP4_INET_AFI_IPV6);

    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    if ((pCookie->u1Protocol != 0) ||
        ((MEMCMP (&(pCookie->IpAddress), &InAddr, sizeof (tNetAddress)) != 0) &&
         (MEMCMP (&(pCookie->PeerAddr), &PeerAddr, sizeof (tAddrPrefix)) != 0))
        || (u1IpPrefSpec == BGP_CLI_IP_SHOW_IP_PREF))
    {
        /* valid cookie */
        /* Since cookie is valid, routes do exist in the database. Try and get
         * the current route from RIB. */
        MEMSET (&Route, 0, sizeof (tRouteProfile));
        Bgp4CopyNetAddressStruct (&(BGP4_RT_NET_ADDRESS_INFO ((&Route))),
                                  pCookie->IpAddress);
        BGP4_RT_PROTOCOL ((&Route)) = pCookie->u1Protocol;

        i4Found = Bgp4RibhLookupRtEntry (&Route, u4Context,
                                         BGP4_TREE_FIND_EXACT,
                                         &pFndRtProfileList, &pRibNode);
        if (i4Found == BGP4_FAILURE)
        {
            /* Unable to find the current route in the RIB. */
            bgp4RibUnlock ();
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }
        pRoute = pFndRtProfileList;

        while (pRoute != NULL)
        {
            /* Check if the route matches the current route
             * or not. */

            if ((PrefixMatch (pRoute->NetAddress.NetAddr,
                              pCookie->IpAddress.NetAddr) == BGP4_TRUE) &&
                (BGP4_RT_PREFIXLEN (pRoute) == pCookie->IpAddress.u2PrefixLen)
                && ((pCookie->u1Protocol == 0) ||
                    (BGP4_RT_PROTOCOL (pRoute) == pCookie->u1Protocol)))
            {
                if ((BGP4_RT_PROTOCOL (pRoute) == BGP_ID)
                    &&
                    ((u1IpPrefSpec == 0 || u1IpPrefSpec == BGP4_RFD_DAMPED_PATH
                      || u1IpPrefSpec == BGP4_RFD_FLAP_STAT)
                     ||
                     ((u1IpPrefSpec == BGP_CLI_IP_SHOW_IP
                       || u1IpPrefSpec == BGP_CLI_IP_SHOW_IP_PREF)
                      &&
                      (MEMCMP
                       (&(pCookie->PeerAddr), &PeerAddr,
                        sizeof (tAddrPrefix)) != 0))))
                {
                    pPeerEntry = BGP4_RT_PEER_ENTRY (pRoute);
                    if (MEMCMP (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                                &(pCookie->PeerAddr), sizeof (tAddrPrefix))
                        == 0)
                    {
                        /* Match route found. */
                        break;
                    }
                }
                else
                {
                    if ((BGP4_RT_PROTOCOL (pRoute) != BGP_ID) &&
                        (pCookie->u1AdvFlag == BGP4_RT_GET_ADV_FLAG (pRoute)))
                    {
                        /* Match route found. */
                        break;
                    }
                }
            }
            pRoute = BGP4_RT_NEXT (pRoute);
        }

        if (pRoute == NULL)
        {
            /* Unable to find the current route in the RIB. */
            bgp4RibUnlock ();
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        /* Current route exists. */
        u4RoutesExist = 1;
    }
    else
    {
        Bgp4GetRibNode (u4Context, CAP_MP_IPV6_UNICAST, &pRibNode);
        if (pRibNode == NULL)
        {
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }

        /* invalid cookie; get the first index */
        i4Found = Bgp4RibhGetFirstEntry (CAP_MP_IPV6_UNICAST, &pRoute,
                                         &pRibNode, TRUE);
        if ((i4Found == BGP4_SUCCESS) && (pRoute != NULL))
        {
            /* GetFirst has succeeded, hence, the routes exist in the database;
             * set the u4RoutesExist flag to 1, to indicate that the routes
             * exist. If u4RoutesExist flag is 0, then no routes exist in the
             * database.
             */
            u4RoutesExist = 1;
        }
        else
        {
            /* No routes present in the BGP */
            bgp4RibUnlock ();
            return i4Return;
        }
    }

    MEMSET ((VOID *) pu1Buffer, 0, u4BufLen);

    pShowIpBgp = (tShowIpBgp *) (VOID *) pu1Buffer;

    /* get and fill the local bgp router id */
    nmhGetFsbgp4Identifier (&pShowIpBgp->u4LocalRouterID);
    pShowIpBgp->u4TableVersion = BGP4_TABLE_VERSION (u4Context);
    pShowIpBgp->u4NoOfRoutes = 0;

    /* Initialize the Route pointer Structure */
    pShowIpBgpRoute = &(pShowIpBgp->aRoutes[0]);

    /* if u4RoutesExist flag is set to 1 then routes exist in the database;
     * else no routes exist in the database - just return.
     */
    if (u4RoutesExist)
    {
        /* calculate the number of routes that could be accomodated into this
         * buffer
         */
        u4NoOfRoutes = (u4BufLen -
                        ((sizeof (tShowIpBgp)) - (sizeof (tShowIpBgpRoute)))) /
            sizeof (tShowIpBgpRoute);

        do
        {
            i4IpPrefTempFlag = 0;
            if ((u1IpPrefSpec == BGP_CLI_IP_SHOW_IP) ||
                (u1IpPrefSpec == BGP_CLI_IP_SHOW_IP_PREF))
            {
                u2PrefixLen = (pRoute->NetAddress.u2PrefixLen / MASK_SHIFT_LEN);
                if (u2PrefixLen == 0)
                {
                    u2PrefixLen = BGP4_IPV6_PREFIX_LEN;
                }
                if (MEMCMP ((pRoute->NetAddress.NetAddr.au1Address),
                            (pCookie->IpAddress.NetAddr.au1Address),
                            u2PrefixLen) == 0)
                {
                    i4IpPrefTempFlag = 1;
                    if ((u1IpPrefSpec == BGP_CLI_IP_SHOW_IP_PREF) &&
                        (pRoute->NetAddress.u2PrefixLen !=
                         pCookie->IpAddress.u2PrefixLen))
                    {
                        i4IpPrefTempFlag = 0;
                    }
                }
            }
            else
            {
                i4IpPrefTempFlag = 1;
            }

            if (i4IpPrefTempFlag == 1)
            {
                if (!u4NoOfRoutes)
                {
                    /* we have run out of buffer; but we have more routes in the
                     * database to be returned. Hence set the cookie in the return
                     * buffer
                     */
                    Bgp4CopyNetAddressStruct (&pCookie->IpAddress,
                                              pRoute->NetAddress);
                    pCookie->u1Protocol = BGP4_RT_PROTOCOL (pRoute);
                    pCookie->u1AdvFlag = BGP4_RT_GET_ADV_FLAG (pRoute);
                    if (BGP4_RT_PROTOCOL (pRoute) == BGP_ID)
                    {
                        pPeerEntry = BGP4_RT_PEER_ENTRY (pRoute);
                        Bgp4CopyAddrPrefixStruct (&pCookie->PeerAddr,
                                                  BGP4_PEER_REMOTE_ADDR_INFO
                                                  (pPeerEntry));
                    }
                    else
                    {
                        MEMSET (&pCookie->PeerAddr, 0, sizeof (tAddrPrefix));
                    }

                    /* set return value to indicate that we ran out of buffer
                     * space and more data is present in the database to be
                     * sent.
                     */
                    i4Return = BGP4_CLI_MORE;
                    break;
                }

                bgpCliPkgGetRouteAttributes (pRoute, pShowIpBgpRoute,
                                             BGP4_CLI_RIB_SPECIFIED);
                /* Increment the stored route count and decrement the max possible
                 * route count */
                pShowIpBgp->u4NoOfRoutes++;
                u4NoOfRoutes--;

                if (u4NoOfRoutes)
                {
                    /* Increment the pShowIpBgpRoute pointer to store the next
                     * route information. */
                    pShowIpBgpRoute++;
                }
            }

            /* prepare to get the next indices */
            pCurRoute = BGP4_RT_NEXT (pRoute);
            if (pCurRoute != NULL)
            {
                pRoute = pCurRoute;
                continue;
            }

            /* get the next indecies to the table */
            pPrevRoute = pRoute;
            pRoute = NULL;
            i4Found = Bgp4RibhGetNextEntry (pPrevRoute, u4Context,
                                            &pRoute, &pRibNode, TRUE);
            if (i4Found != BGP4_SUCCESS)
            {
                break;
            }
        }
        while ((pRoute != NULL));
    }
    bgp4RibUnlock ();
    return (i4Return);
#else
    UNUSED_PARAM (pu1Buffer);
    UNUSED_PARAM (u4BufLen);
    UNUSED_PARAM (pCookie);
    UNUSED_PARAM (u1IpPrefSpec);
    return BGP4_FAILURE;
#endif
}

/*****************************************************************************/
/* Function Name : bgpShowIpv4BgpNeighbor                                    */
/* CLI Command   : show ip bgp neighbors {peer-address}                      */
/* Description   : Display the information about the peers. If the peer      */
/*               : address is given then the information about that peer     */
/*               : will be displayed. Else if no peer address is specified,  */
/*               : then the information about all peers will be displayed.   */
/* Input(s)      : pCookie - holds information about the peers returned      */
/*               : in previous call.                                         */
/*               : u4BufLen - lenght of the buffer for storing the route.    */
/*               : pu4Neighbor - Peer address whose information needs to be  */
/*               : displayed.                                                */
/* Output(s)     : pu1Buffer - Stores the route informations.                */
/* Return(s)     : BGP4_SUCCESS    - if all no more route is left            */
/*               : BGP4_CLI_MORE   - if more routes still exists             */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4                bgpShowIpv4BgpNeighbor (UINT4 *pu4Neighbor,
                                            /* if non-NULL, contains the neighbor
                                             * ip address; if NULL then the all
                                             * neighbor entries are returned.  */
                                            UINT1 *pu1Buffer,
                                            /* non-NULL buffer, where the 
                                             * table is returned. this
                                             * buffer must be big enough
                                             * to hold atleast one route (i.e.
                                             * sizeof(tShowIpBgpNeibhbor)).  */
                                            UINT4 u4BufLen,    /* length of pu1Buffer */
                                            tAddrPrefix * pCookie
                                            /* Cookie, used to fetch tables
                                             * that are greater than length
                                             * u4BufLen */
    )
{
    tAddrPrefix         InAddr;
    tShowIpBgpNeighbor *pShowIpBgpNeig = NULL;
    tIpBgpNeighbor     *pIpBgpNeig = NULL;
    UINT4               u4NeigAddr, u4PrevNeigAddr;
    UINT4               u4NoOfNeig = 0;
    UINT4               u4NeigExist = 0;
    INT4                i4RetVal = 0;
    INT4                i4Return = BGP4_SUCCESS;

    /* check for NULL buffer or too small buffer */
    BGP4_ASSERT ((pu1Buffer) && (u4BufLen >= (sizeof (tShowIpBgpNeighbor))));

    /* cookie cannot be NULL */
    BGP4_ASSERT (pCookie);

    pShowIpBgpNeig = (tShowIpBgpNeighbor *) (VOID *) pu1Buffer;
    pIpBgpNeig = &pShowIpBgpNeig->aNeighbors[0];

    MEMSET (pu1Buffer, 0, u4BufLen);

    /* get local-as number */
    nmhGetBgpLocalAs ((INT4 *) &pShowIpBgpNeig->u4LocalAs);

    /* check if ip address was specified on the command-line */
    if (pu4Neighbor)
    {
        /* ip address was specified on the command line. hence, fill the
         * details of the specified neighbor and return.
         */

        i4RetVal = bgpGetIpv4NeighborAttributes (*pu4Neighbor, pIpBgpNeig);

        if (i4RetVal == BGP4_SUCCESS)
        {
            pShowIpBgpNeig->u4NoOfNeig = 1;
        }
        return (i4RetVal);
    }

    /* check for valid u4Cookie; if cookie is invalid, then this is a fresh
     * call to bgpShowIpBgpNeighbor - start from the begining. If cookie is
     * valid, then use to cookie to continue from where we left last time
     */
    Bgp4InitAddrPrefixStruct (&InAddr, BGP4_INET_AFI_IPV4);
    if (MEMCMP (pCookie, &InAddr, sizeof (tAddrPrefix)) != 0)
    {
        /* valid cookie */
        MEMCPY (&u4NeigAddr, pCookie->au1Address, sizeof (UINT4));
        /* Neighbor does exist */
        u4NeigExist = 1;
    }
    else
    {
        /* invalid cookie; get the first index */
        if (nmhGetFirstIndexBgpPeerTable (&u4NeigAddr) == SNMP_SUCCESS)
        {
            /* GetFirst has succeeded, hence, the neighbors exist in the
             * database; set the u4NeigExist flag to 1, to indicate that the
             * neighbors exist. If u4NeigExist flag is 0, then no neighbors
             * exist in the database.
             */
            u4NeigExist = 1;
        }
    }

    /* if u4NeigExist flag is set to 1 then neighbors exist in the database;
     * else no neighbors exist in the database - just return.
     */
    if (u4NeigExist)
    {
        /* calculate the number of neighbors that could be accomodated into this
         * buffer
         */
        u4NoOfNeig = (u4BufLen - ((sizeof (tShowIpBgpNeighbor)) -
                                  (sizeof (tIpBgpNeighbor)))) /
            (sizeof (tIpBgpNeighbor));

        do
        {
            if (!u4NoOfNeig)
            {
                /* we have run out of buffer; but we have more neighbors in the
                 * database to be returned. Hence set the cookie.
                 */
                MEMCPY (pCookie->au1Address, &u4NeigAddr, sizeof (UINT4));

                /* set return value to indicate that we ran out of buffer
                 * space and more data is present in the database to be
                 * sent.
                 */
                i4Return = BGP4_CLI_MORE;
                break;
            }

            /* fill the fields of this neighbor */
            if (bgpGetIpv4NeighborAttributes (u4NeigAddr, pIpBgpNeig) !=
                BGP4_SUCCESS)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpShowIpBgpNeighbor: bgpGetIpv4NeighborAttributes failed\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            pShowIpBgpNeig->u4NoOfNeig++;
            u4NoOfNeig--;

            /* prepare to get the next indices */
            u4PrevNeigAddr = u4NeigAddr;

            if (u4NoOfNeig)
            {
                /* Increment the pIpBgpNeig pointer to store the next
                 * Neighbor information. */
                pIpBgpNeig++;
            }

            /* get the next indecies to the table */
        }
        while (nmhGetNextIndexBgpPeerTable (u4PrevNeigAddr, &u4NeigAddr)
               != SNMP_FAILURE);
    }
    return (i4Return);
}

/*****************************************************************************/
/* Function Name : bgpShowIpv6BgpNeighbor                                    */
/* CLI Command   : show ip bgp neighbors {peer-address}                      */
/* Description   : Display the information about the peers. If the peer      */
/*               : address is given then the information about that peer     */
/*               : will be displayed. Else if no peer address is specified,  */
/*               : then the information about all peers will be displayed.   */
/* Input(s)      : pCookie - holds information about the peers returned      */
/*               : in previous call.                                         */
/*               : u4BufLen - lenght of the buffer for storing the route.    */
/*               : pu4Neighbor - Peer address whose information needs to be  */
/*               : displayed.                                                */
/* Output(s)     : pu1Buffer - Stores the route informations.                */
/* Return(s)     : BGP4_SUCCESS    - if all no more route is left            */
/*               : BGP4_CLI_MORE   - if more routes still exists             */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4                bgpShowIpv6BgpNeighbor (tAddrPrefix * pNeighbor,
                                            /* if non-NULL, contains the neighbor
                                             * ip address; if NULL then the all
                                             * neighbor entries are returned.  */
                                            UINT1 *pu1Buffer,
                                            /* non-NULL buffer, where the 
                                             * table is returned. this
                                             * buffer must be big enough
                                             * to hold atleast one route (i.e.
                                             * sizeof(tShowIpBgpNeibhbor)).  */
                                            UINT4 u4BufLen,    /* length of pu1Buffer */
                                            tAddrPrefix * pCookie
                                            /* Cookie, used to fetch tables
                                             * that are greater than length
                                             * u4BufLen */
    )
{
#ifdef BGP4_IPV6_WANTED
    tShowIpBgpNeighbor *pShowIpBgpNeig = NULL;
    tIpBgpNeighbor     *pIpBgpNeig = NULL;
    tAddrPrefix         NeigAddr, PrevNeigAddr, InAddr;
    UINT4               u4NoOfNeig = 0;
    UINT4               u4NeigExist = 0;
    INT4                i4RetVal = 0;
    INT4                i4Return = BGP4_SUCCESS;
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    /* check for NULL buffer or too small buffer */
    BGP4_ASSERT ((pu1Buffer) && (u4BufLen >= (sizeof (tShowIpBgpNeighbor))));

    /* cookie cannot be NULL */
    BGP4_ASSERT (pCookie);

    pShowIpBgpNeig = (tShowIpBgpNeighbor *) (VOID *) pu1Buffer;
    pIpBgpNeig = &pShowIpBgpNeig->aNeighbors[0];

    MEMSET (pu1Buffer, 0, u4BufLen);

    /* get local-as number */
    nmhGetBgpLocalAs ((INT4 *) &pShowIpBgpNeig->u4LocalAs);

    /* check if ip address was specified on the command-line */
    if (pNeighbor)
    {
        /* ip address was specified on the command line. hence, fill the
         * details of the specified neighbor and return.
         */
        i4RetVal = bgpGetIpv6NeighborAttributes (pNeighbor, pIpBgpNeig);
        if (i4RetVal == BGP4_SUCCESS)
        {
            pShowIpBgpNeig->u4NoOfNeig = 1;
        }
        return (i4RetVal);
    }

    /* check for valid u4Cookie; if cookie is invalid, then this is a fresh
     * call to bgpShowIpBgpNeighbor - start from the begining. If cookie is
     * valid, then use to cookie to continue from where we left last time
     */
    Bgp4InitAddrPrefixStruct (&InAddr, BGP4_INET_AFI_IPV6);
    Bgp4InitAddrPrefixStruct (&NeigAddr, BGP4_INET_AFI_IPV6);
    if (MEMCMP (pCookie, &InAddr, sizeof (tAddrPrefix)) != 0)
    {
        /* valid cookie */
        MEMCPY (NeigAddr.au1Address, pCookie->au1Address, BGP4_IPV6_PREFIX_LEN);

        /* Neighbor does exist */
        u4NeigExist = 1;
    }
    else
    {
        /* invalid cookie; get the first index */
        if (Bgp4Ipv6GetFirstIndexBgpPeerTable (u4Context, &NeigAddr) ==
            BGP4_SUCCESS)
        {
            /* GetFirst has succeeded, hence, the neighbors exist in the
             * database; set the u4NeigExist flag to 1, to indicate that the
             * neighbors exist. If u4NeigExist flag is 0, then no neighbors
             * exist in the database.
             */
            u4NeigExist = 1;
        }
    }

    /* if u4NeigExist flag is set to 1 then neighbors exist in the database;
     * else no neighbors exist in the database - just return.
     */
    if (u4NeigExist)
    {
        /* calculate the number of neighbors that could be accomodated into this
         * buffer
         */
        u4NoOfNeig = (u4BufLen - ((sizeof (tShowIpBgpNeighbor)) -
                                  (sizeof (tIpBgpNeighbor)))) /
            (sizeof (tIpBgpNeighbor));
        do
        {
            if (!u4NoOfNeig)
            {
                /* we have run out of buffer; but we have more neighbors in the
                 * database to be returned. Hence set the cookie.
                 */
                pCookie->u2Afi = NeigAddr.u2Afi;
                pCookie->u2AddressLen = NeigAddr.u2AddressLen;
                MEMCPY (pCookie->au1Address, NeigAddr.au1Address,
                        BGP4_IPV6_PREFIX_LEN);

                /* set return value to indicate that we ran out of buffer
                 * space and more data is present in the database to be
                 * sent.
                 */
                i4Return = BGP4_CLI_MORE;
                break;
            }

            /* fill the fields of this neighbor */
            if (bgpGetIpv6NeighborAttributes (&NeigAddr, pIpBgpNeig) !=
                BGP4_SUCCESS)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tbgpShowIpv6BgpNeighbor: bgpGetIpv6NeighborAttributes failed\n");
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }

            pShowIpBgpNeig->u4NoOfNeig++;
            u4NoOfNeig--;

            /* prepare to get the next indices */
            Bgp4CopyAddrPrefixStruct (&PrevNeigAddr, NeigAddr);

            if (u4NoOfNeig)
            {
                /* Increment the pIpBgpNeig pointer to store the next
                 * Neighbor information. */
                pIpBgpNeig++;
            }

            /* get the next indecies to the table */
        }
        while (Bgp4Ipv6GetNextIndexBgpPeerTable
               (u4Context, PrevNeigAddr, &NeigAddr) != BGP4_FAILURE);
    }
    return (i4Return);
#else
    UNUSED_PARAM (pNeighbor);
    UNUSED_PARAM (pu1Buffer);
    UNUSED_PARAM (u4BufLen);
    UNUSED_PARAM (pCookie);
    return BGP4_FAILURE;
#endif
}

/* cli utility functions */
/*****************************************************************************/
/* Function Name : bgpGetRouteAttributes                                     */
/* Description   : This is a CLI utility function to fetch the BGP routes    */
/* Input(s)      : See parameters below                                      */
/* Output(s)     : Fills pShowIpBgpRoute with route attributes.              */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_xx - if operation fails                      */
/*****************************************************************************/
INT4
bgpCliPkgGetRouteAttributes (tRouteProfile * pMatchRoute,    /* Route whose info needs
                                                             * to be displayed */
                             tShowIpBgpRoute * pShowIpBgpRoute,    /* return structure */
                             UINT1 u1RibFlag)
{
    tTMO_SLL           *pAsPathList = NULL;
    tAsPath            *pAsPath = NULL;
    tBgp4Info          *pBgp4Info = NULL;
    tBgp4PeerEntry     *pPeer = NULL;
    UINT1              *pu1AsNos = NULL;
    INT4                i4BytesFilled;
    UINT4               u4LocalAs;
    UINT4               u4AsafiMask;
#ifdef RFD_WANTED
    UINT4               u4CurrTime = 0;
#endif
    UINT1               u1AsCnt;
    UINT4               u4Context;
    UINT4               u4Index = 0;
    UINT4               u4CurrentTime = 0;
    RFDFLOAT            FomTempVal;
    RFDFLOAT            ReuseTempVal;
    UINT2               u2TimeDiff;
    if (pMatchRoute == NULL)
    {
        return BGP4_FAILURE;
    }
    u4Context = gpBgpCurrCxtNode->u4ContextId;
    /* get local-as number */
    nmhGetFsbgp4LocalAs (&u4LocalAs);

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pMatchRoute),
                           BGP4_RT_SAFI_INFO (pMatchRoute), u4AsafiMask);
    /* fill network prefix */
#ifdef L3VPN
    if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
    {
        Bgp4CopyVpn4NetAddressFromRt (&(pShowIpBgpRoute->NetAddress),
                                      pMatchRoute);
    }
    else
#endif
    {
        Bgp4CopyNetAddressStruct (&(pShowIpBgpRoute->NetAddress),
                                  pMatchRoute->NetAddress);
    }
    /*This check added for aggregated route display. RtInfo is Null for the 
     *Aggregated routes, not having any specific routes */
    if (BGP4_RT_BGP_INFO (pMatchRoute) == NULL)
    {
        return BGP4_SUCCESS;
    }
    /* fill nexthop */
    Bgp4CopyAddrPrefixStruct (&(pShowIpBgpRoute->NextHop),
                              BGP4_INFO_NEXTHOP_INFO (BGP4_RT_BGP_INFO
                                                      (pMatchRoute)));

    /* fill metric (MED) */
    pBgp4Info = BGP4_RT_BGP_INFO (pMatchRoute);
    pShowIpBgpRoute->u4AttrFlag = BGP4_INFO_ATTR_FLAG (pBgp4Info);
    if (BGP4_RT_PROTOCOL (pMatchRoute) == BGP_ID)
    {
        if (BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_MED_MASK)
        {
            pShowIpBgpRoute->u4Metric = BGP4_RT_MED (pMatchRoute);
        }
        else
        {
            if (u1RibFlag == BGP4_CLI_RIB_SPECIFIED)
            {
                pShowIpBgpRoute->u4Metric = BGP4_RT_MED (pMatchRoute);
            }
            else
            {
                pShowIpBgpRoute->u4Metric = 0;
            }
        }
    }
    else
    {
        pShowIpBgpRoute->u4Metric = BGP4_RT_MED (pMatchRoute);
    }
    /* fill local-preference */
    pBgp4Info = BGP4_RT_BGP_INFO (pMatchRoute);
    if (BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_LOCAL_PREF_MASK)
    {
        pShowIpBgpRoute->u4LocPref = BGP4_RT_LOCAL_PREF (pMatchRoute);
    }
    else
    {
        if (u1RibFlag == BGP4_CLI_RIB_SPECIFIED)
        {
            pShowIpBgpRoute->u4LocPref = BGP4_RT_LOCAL_PREF (pMatchRoute);
        }
        else
        {
            pShowIpBgpRoute->u4LocPref = 0;
        }
    }

    /* fill weight  */
    pShowIpBgpRoute->u4Weight =
        BGP4_INFO_WEIGHT (BGP4_RT_BGP_INFO (pMatchRoute));

    /* fill AS Path */
    i4BytesFilled = 0;
    pAsPathList = BGP4_INFO_ASPATH (BGP4_RT_BGP_INFO (pMatchRoute));
    TMO_SLL_Scan (pAsPathList, pAsPath, tAsPath *)
    {
        u1AsCnt = BGP4_ASPATH_LEN (pAsPath);
        pu1AsNos = BGP4_ASPATH_NOS (pAsPath);
        while (u1AsCnt > 0)
        {
            if (i4BytesFilled >= BGP_CLI_MAX_AS_PATHS)
            {
                /* ran out of space in the AS Path array */
                break;
            }
            pShowIpBgpRoute->ASPath.au4Path[i4BytesFilled] =
                OSIX_NTOHL (*(UINT4 *) (VOID *) pu1AsNos);
            pShowIpBgpRoute->ASPath.au1AsType[i4BytesFilled] =
                BGP4_ASPATH_TYPE (pAsPath);
            pu1AsNos += 4;
            u1AsCnt--;
            i4BytesFilled++;
        }
    }
    pShowIpBgpRoute->ASPath.u4NoOfAsPaths = i4BytesFilled;

    /* fill route status */
    if ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_STALE) == BGP4_RT_STALE)
    {
        pShowIpBgpRoute->i4RouteStatus |= BGP_CLI_ROUTE_STALE;
    }
    else if ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_BEST) == BGP4_RT_BEST)
    {
        pShowIpBgpRoute->i4RouteStatus |= BGP_CLI_ROUTE_BEST;
    }
    else if ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_DAMPED) ==
             BGP4_RT_DAMPED)
    {
        /* Check for Feasible / Unfeasible Route in damped state */
        if ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_HISTORY) ==
            BGP4_RT_HISTORY)
        {
            pShowIpBgpRoute->i4RouteStatus |= BGP_CLI_ROUTE_HISTORY;
            pShowIpBgpRoute->i4RouteStatus |= BGP_CLI_ROUTE_DAMPED;

        }
        else
        {
            pShowIpBgpRoute->i4RouteStatus |= BGP_CLI_ROUTE_VALID;
            pShowIpBgpRoute->i4RouteStatus |= BGP_CLI_ROUTE_DAMPED;
        }
    }
    else if ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_HISTORY) ==
             BGP4_RT_HISTORY)
    {
        pShowIpBgpRoute->i4RouteStatus |= BGP_CLI_ROUTE_HISTORY;
    }
    else if ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_OVERLAP) ||
             (BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_FILTERED_INPUT))
    {
        pShowIpBgpRoute->i4RouteStatus |= BGP_CLI_ROUTE_SUPPRESSED;
    }
    else if ((BGP4_RT_GET_EXT_FLAGS (pMatchRoute) & BGP4_RT_MULTIPATH) ==
             BGP4_RT_MULTIPATH)
    {
        pShowIpBgpRoute->i4RouteStatus |= BGP_CLI_ROUTE_MPATH;
    }

    if ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_AGGREGATED) ==
        BGP4_RT_AGGREGATED)
    {
        u4Index = Bgp4AggrFindAggrEntry (pMatchRoute, u4Index);
        if (u4Index != 0)
        {
            if (BGP4_AGGRENTRY_ADV_TYPE (u4Index - BGP4_DECREMENT_BY_ONE) ==
                BGP4_SUMMARY)
            {
                pShowIpBgpRoute->i4RouteStatus |= BGP_CLI_ROUTE_SUPPRESSED;
            }
        }
    }
    if (BGP4_RT_PROTOCOL (pMatchRoute) == BGP_ID)
    {
        pPeer = BGP4_RT_PEER_ENTRY (pMatchRoute);
        if (u4LocalAs == BGP4_PEER_ASNO (pPeer))
        {                        /* route learnt from internal peer. */
            pShowIpBgpRoute->i4RouteStatus |= BGP_CLI_ROUTE_INTERNAL;
        }
    }
    if ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_STALE) != BGP4_RT_STALE)
    {
        if ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_HISTORY) !=
            BGP4_RT_HISTORY)
        {
            pShowIpBgpRoute->i4RouteStatus |= BGP_CLI_ROUTE_VALID;
        }
    }

    /*fill Route flags */
    pShowIpBgpRoute->u4RouteFlags = BGP4_RT_GET_FLAGS (pMatchRoute);

    /*fill Route Ext flags */
    pShowIpBgpRoute->u4RouteExtFlags = BGP4_RT_GET_EXT_FLAGS (pMatchRoute);

    /* fill origin */
    pShowIpBgpRoute->i4Origin =
        (INT4) (BGP4_INFO_ORIGIN (BGP4_RT_BGP_INFO (pMatchRoute))) + 1;

    /* fill Community Attributes */
    pBgp4Info = BGP4_RT_BGP_INFO (pMatchRoute);
    if (BGP4_INFO_COMM_ATTR (pBgp4Info) != NULL)
    {
        pShowIpBgpRoute->Community.u2CommCnt = BGP4_INFO_COMM_COUNT (pBgp4Info);
        pShowIpBgpRoute->Community.pu1CommVal =
            BGP4_INFO_COMM_ATTR_VAL (pBgp4Info);
        pShowIpBgpRoute->Community.u1Flag =
            BGP4_INFO_COMM_ATTR_FLAG (pBgp4Info);
    }

#ifdef VPLSADS_WANTED
    /* fill Ext Community Attributes */
    pBgp4Info = BGP4_RT_BGP_INFO (pMatchRoute);
    if (BGP4_INFO_ECOMM_ATTR (pBgp4Info) != NULL)
    {
        pShowIpBgpRoute->ExtCommunity.u2CommCnt =
            BGP4_INFO_ECOMM_COUNT (pBgp4Info);
        pShowIpBgpRoute->ExtCommunity.pu1CommVal =
            BGP4_INFO_ECOMM_ATTR_VAL (pBgp4Info);
        pShowIpBgpRoute->ExtCommunity.u1Flag =
            BGP4_INFO_ECOMM_ATTR_FLAG (pBgp4Info);
    }

#endif

#if defined (VPLSADS_WANTED) || defined (EVPN_WANTED)
    pShowIpBgpRoute->u4Protocol = (UINT4) pMatchRoute->u1Protocol;
#endif
    /* Fill Orgin ID */
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_ORIG_ID_MASK) ==
        BGP4_ATTR_ORIG_ID_MASK)
    {
        pShowIpBgpRoute->u4OrigId = BGP4_INFO_ORIG_ID (pBgp4Info);
    }

    /* Fill Cluster list */
    pBgp4Info = BGP4_RT_BGP_INFO (pMatchRoute);
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_CLUS_LIST_MASK)
        == BGP4_ATTR_CLUS_LIST_MASK)
    {
        pShowIpBgpRoute->Cluster.u2ClusCnt =
            BGP4_INFO_CLUS_LIST_COUNT (pBgp4Info);
        pShowIpBgpRoute->Cluster.pu1ClusId =
            BGP4_INFO_CLUS_LIST_ATTR_VAL (pBgp4Info);
        pShowIpBgpRoute->Cluster.u1Flag =
            BGP4_INFO_CLUS_LIST_ATTR_FLAG (pBgp4Info);
    }

    /* Fill neighbor router-id */
    if (pMatchRoute->pPEPeer != NULL)
    {
        pPeer = Bgp4SnmphGetPeerEntry (u4Context,
                                       BGP4_PEER_REMOTE_ADDR_INFO (pMatchRoute->
                                                                   pPEPeer));
        if (pPeer == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - No matching Peer exist.\n");
            return SNMP_FAILURE;
        }
        pShowIpBgpRoute->u4NeigRouterId = BGP4_PEER_BGP_ID (pPeer);
    }
#ifdef RFD_WANTED
    /* Fill Dampening details */
    if (BGP4_RT_DAMP_HIST (pMatchRoute) != NULL)
    {
        RFD_GET_SYS_TIME (&u4CurrentTime);
        u2TimeDiff =
            RFD_GET_TIME_DIFF_SEC (u4CurrentTime,
                                   pMatchRoute->pRtDampHist->u4RtLastTime);
        FomTempVal =
            ((RFDFLOAT) u2TimeDiff) /
            ((RFDFLOAT)
             RFD_DECAY_HALF_LIFE_TIME (BGP4_RT_CXT_ID
                                       (pMatchRoute->pRtDampHist->pRtProfile)));
        FomTempVal = (RFDFLOAT) RFD_POW (0.5, FomTempVal);

        pShowIpBgpRoute->i4DampFom =
            ((pMatchRoute->pRtDampHist->RtFom) * FomTempVal);
        pShowIpBgpRoute->u1DampRtStat = pMatchRoute->pRtDampHist->u1RtStatus;
        pShowIpBgpRoute->u4DampFlapCnt =
            pMatchRoute->pRtDampHist->u4RtFlapCount;
        RFD_GET_SYS_TIME (&u4CurrTime);
        u4CurrTime = (u4CurrTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
        pShowIpBgpRoute->u4DampFlapTime = (u4CurrTime -
                                           pMatchRoute->pRtDampHist->
                                           u4RtFlapTime);

        ReuseTempVal = RFD_NAT_LOG (RFD_REUSE_THRESHOLD
                                    (BGP4_RT_CXT_ID
                                     (pMatchRoute->pRtDampHist->pRtProfile)) /
                                    pShowIpBgpRoute->i4DampFom);
        ReuseTempVal = (ReuseTempVal / RFD_NAT_LOG (1 / 2));
        ReuseTempVal = ReuseTempVal * RFD_DECAY_HALF_LIFE_TIME
            (BGP4_RT_CXT_ID (pMatchRoute->pRtDampHist->pRtProfile));
        if (ReuseTempVal >
            RFD_MAX_HOLD_DOWN_TIME ((BGP4_RT_CXT_ID
                                     (pMatchRoute->pRtDampHist->pRtProfile))))
        {
            ReuseTempVal =
                RFD_MAX_HOLD_DOWN_TIME ((BGP4_RT_CXT_ID
                                         (pMatchRoute->pRtDampHist->
                                          pRtProfile)));
        }

        if (ReuseTempVal >= 0)
        {
            pShowIpBgpRoute->u4DampReuseTime = ReuseTempVal;
        }
        else
        {
            pShowIpBgpRoute->u4DampReuseTime = 0;
        }
        pShowIpBgpRoute->u2DampReuseInd =
            pMatchRoute->pRtDampHist->u2ReuseIndex;
        pShowIpBgpRoute->u1RtFlag = pMatchRoute->pRtDampHist->u1RtFlag;
    }
#endif
#ifdef EVPN_WANTED
    MEMCPY (pShowIpBgpRoute->au1RD,
            pMatchRoute->EvpnNlriInfo.au1RouteDistinguisher, MAX_LEN_RD_VALUE);
    pShowIpBgpRoute->u4VnId = pMatchRoute->EvpnNlriInfo.u4VnId;
    MEMCPY (pShowIpBgpRoute->MacAddress, pMatchRoute->EvpnNlriInfo.MacAddress,
            sizeof (tMacAddr));
    MEMCPY (pShowIpBgpRoute->au1EthSegId,
            pMatchRoute->EvpnNlriInfo.au1EthernetSegmentID, MAX_LEN_ETH_SEG_ID);
    pShowIpBgpRoute->u1RouteType = pMatchRoute->EvpnNlriInfo.u1RouteType;
#endif
    return BGP4_SUCCESS;
}

#ifdef L3VPN
/*****************************************************************************/
/* Function Name : bgpGetRouteLabels                                        */
/* Description   : This is a CLI utility function to fetch the BGP routes    */
/* Input(s)      : See parameters below                                      */
/* Output(s)     : Fills pShowIpBgpRoute with route attributes.              */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_xx - if operation fails                      */
/*****************************************************************************/
INT4
bgpGetRouteLabels (tRouteProfile * pMatchRoute,    /* Route whose info needs
                                                 * to be displayed */
                   tShowIpBgpRoute * pShowIpBgpRoute    /* return structure */
    )
{
    UINT4               u4AsafiMask;

    if (pMatchRoute == NULL)
    {
        return BGP4_FAILURE;
    }

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pMatchRoute),
                           BGP4_RT_SAFI_INFO (pMatchRoute), u4AsafiMask);
    /* fill network prefix */
#ifdef L3VPN
    if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
    {
        Bgp4CopyVpn4NetAddressFromRt (&(pShowIpBgpRoute->NetAddress),
                                      pMatchRoute);
    }
    else
#endif
    {
        Bgp4CopyNetAddressStruct (&(pShowIpBgpRoute->NetAddress),
                                  pMatchRoute->NetAddress);
    }
    /* fill nexthop */
    Bgp4CopyAddrPrefixStruct (&(pShowIpBgpRoute->NextHop),
                              BGP4_INFO_NEXTHOP_INFO (BGP4_RT_BGP_INFO
                                                      (pMatchRoute)));

#ifdef L3VPN
    if ((u4AsafiMask == CAP_MP_LABELLED_IPV4) ||
        (u4AsafiMask == CAP_MP_VPN4_UNICAST))
    {
        if (BGP4_RT_LABEL_CNT (pMatchRoute))
        {
            /*Assumed that BGP will take only one label with the route.
             * Thsi has to be modified other wise*/
            pShowIpBgpRoute->u4BgpLabel = BGP4_RT_LABEL_INFO (pMatchRoute);
        }
    }
#endif
    return BGP4_SUCCESS;
}
#endif
/*****************************************************************************/
/* Function Name : bgpGetIpv4NeighborAttributes                              */
/* Description   : This is a CLI utility function used for fetching the      */
/*               : attributes of a neighbor.                                 */
/* Input(s)      : See parameters below.                                     */
/* Output(s)     : Fills the attributes in the pIpBgpNeig buffer.            */
/* Return(s)     : BGP4_SUCCESS - if operation is success                    */
/*               : BGP4_CLI_ERR_xx - if operation fails                      */
/*****************************************************************************/
INT4
bgpGetIpv4NeighborAttributes (UINT4 u4Neighbor,    /* index: Neighbor address */
                              tIpBgpNeighbor * pIpBgpNeig    /* return structure  */
    )
{
    tSNMP_OCTET_STRING_TYPE CapVal;
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE PeerNetAddr;
    tSNMP_OCTET_STRING_TYPE UpdateSource;
#ifdef ROUTEMAP_WANTED
    tSNMP_OCTET_STRING_TYPE IpPrefixName;
#endif
    tAddrPrefix         PeerRemAddr;
    tOrfCapsValue       OrfCapsValue;
    tBgp4PeerEntry     *pPeer = NULL;
    INT4                i4RetVal;
    INT4                i4CapSts;
    INT4                i4BfdStatus = 0;
    UINT4               u4LocalAddr;
    UINT1               au1CapVal[CAP_MIN_CAP_MAX_DATA_SIZE];
    UINT4               u4PeerOctetList;
    UINT1               au1IpAddrOctetList[BGP4_MAX_INET_ADDRESS_LEN];
    UINT1               au1UpdateSrcOctetList[BGP4_IPV4_PREFIX_LEN];
    UINT4               u4Context;

    MEMSET (&OrfCapsValue, 0, sizeof (tOrfCapsValue));
    u4Context = gpBgpCurrCxtNode->u4ContextId;
    /* fill neighbor ip address */
    pIpBgpNeig->NeigIp.u2Afi = BGP4_INET_AFI_IPV4;
    MEMCPY (pIpBgpNeig->NeigIp.au1Address, &u4Neighbor, sizeof (UINT4));

    PeerAddr.pu1_OctetList = (UINT1 *) (VOID *) &u4PeerOctetList;
    PTR_ASSIGN4 ((PeerAddr.pu1_OctetList), (u4Neighbor));
    PeerAddr.i4_Length = sizeof (UINT4);

    Bgp4InitAddrPrefixStruct (&PeerRemAddr, BGP4_INET_AFI_IPV4);
    MEMCPY (PeerRemAddr.au1Address, &(u4Neighbor), sizeof (UINT4));
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerRemAddr);
    if (pPeer == NULL)
    {
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }
    /* fill remote-as */
    i4RetVal = nmhGetBgpPeerRemoteAs (u4Neighbor,
                                      (INT4 *) &pIpBgpNeig->u4RemoteAs);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : nmhGetBgpPeerRemoteAs "
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* Fill Peer Admin State */
    i4RetVal = nmhGetBgpPeerAdminStatus (u4Neighbor,
                                         (INT4 *) &pIpBgpNeig->
                                         u4PeerAdminState);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : nmhGetBgpPeerAdminStatus"
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* Fill Peer Operational Status */
    if (BGP4_GET_PEER_PEND_FLAG (pPeer) & BGP4_PEER_MULTIHOP_PEND_START)
    {
        pIpBgpNeig->u4PeerOperStatus |= BGP_CLI_PEND_MULTIHOP;
    }

    if (BGP4_GET_PEER_PEND_FLAG (pPeer) & BGP4_PEER_MP_CAP_CONFIG_PEND_START)
    {
        pIpBgpNeig->u4PeerOperStatus |= BGP_CLI_PEND_MP_CONFIG_CAPS;
    }
    if (BGP4_GET_PEER_PEND_FLAG (pPeer) & BGP4_PEER_MP_CAP_RECV_PEND_START)
    {
        pIpBgpNeig->u4PeerOperStatus |= BGP_CLI_PEND_MP_RECV_CAPS;
    }

    /* fill remote tcp port */
    i4RetVal = nmhGetBgpPeerRemotePort (u4Neighbor,
                                        (INT4 *) &pIpBgpNeig->u4NeigPort);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : nmhGetBgpPeerRemotePort"
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill local ip address */
    i4RetVal = nmhGetBgpPeerLocalAddr (u4Neighbor, &(u4LocalAddr));
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : nmhGetBgpPeerLocalAddr"
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    MEMCPY (pIpBgpNeig->LocalIp.au1Address, &u4LocalAddr, sizeof (UINT4));
    pIpBgpNeig->LocalIp.u2Afi = BGP4_INET_AFI_IPV4;

    /* fill local tcp port */
    i4RetVal = nmhGetBgpPeerLocalPort (u4Neighbor,
                                       (INT4 *) &pIpBgpNeig->u4LocalPort);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : nmhGetBgpPeerLocalPort"
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill negotiated BGP version */
    i4RetVal = nmhGetBgpPeerNegotiatedVersion (u4Neighbor,
                                               (INT4 *) &pIpBgpNeig->
                                               u4NegBgpVersion);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : nmhGetBgpPeerNegotiatedVersion "
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill neighbor router-id */
    i4RetVal = nmhGetBgpPeerIdentifier (u4Neighbor,
                                        &pIpBgpNeig->u4NeigRouterId);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : nmhGetBgpPeerIdentifier "
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill bgp session state */
    i4RetVal =
        nmhGetBgpPeerState (u4Neighbor, (INT4 *) &pIpBgpNeig->u4BgpState);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : nmhGetBgpPeerState "
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill elapsed time */
    i4RetVal = nmhGetBgpPeerFsmEstablishedTime (u4Neighbor,
                                                &pIpBgpNeig->u4ElapsedTime);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : "
                  "nmhGetBgpPeerFsmEstablishedTime failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill elapsed time since last update */
    i4RetVal = nmhGetBgpPeerInUpdateElapsedTime (u4Neighbor,
                                                 &pIpBgpNeig->u4LastUpdateTime);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : "
                  "nmhGetBgpPeerInUpdateElapsedTime failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }
    /* fill hold time */
    if (pIpBgpNeig->u4BgpState < BGP4_OPENCONFIRM_STATE)
    {
        i4RetVal = nmhGetBgpPeerHoldTimeConfigured (u4Neighbor,
                                                    (INT4 *) &pIpBgpNeig->
                                                    u4HoldTime);
    }
    else
    {
        i4RetVal = nmhGetBgpPeerHoldTime (u4Neighbor,
                                          (INT4 *) &pIpBgpNeig->u4HoldTime);
    }
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : nmhGetBgpPeerHoldTime "
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill keepalive time */
    if (pIpBgpNeig->u4BgpState < BGP4_OPENCONFIRM_STATE)
    {
        i4RetVal = nmhGetBgpPeerKeepAliveConfigured (u4Neighbor,
                                                     (INT4 *) &pIpBgpNeig->
                                                     u4KeepAlive);
    }
    else
    {
        i4RetVal = nmhGetBgpPeerKeepAlive (u4Neighbor,
                                           (INT4 *) &pIpBgpNeig->u4KeepAlive);
    }
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : nmhGetBgpPeerKeepAlive "
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill minimum advertisement time */
    i4RetVal = nmhGetBgpPeerMinRouteAdvertisementInterval (u4Neighbor,
                                                           (INT4 *)
                                                           &pIpBgpNeig->
                                                           u4MinAdvtInt);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : "
                  "nmhGetBgpPeerMinRouteAdvertisementInterval failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill minimum AS origination interval */
    i4RetVal = nmhGetBgpPeerMinASOriginationInterval (u4Neighbor,
                                                      (INT4 *) &pIpBgpNeig->
                                                      u4MinAsOrigInt);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : "
                  "nmhGetBgpPeerMinASOriginationInterval failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill connect retry timer */
    i4RetVal = nmhGetBgpPeerConnectRetryInterval (u4Neighbor,
                                                  (INT4 *) &pIpBgpNeig->
                                                  u4ConnectRetry);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : "
                  "nmhGetBgpPeerConnectRetryInterval failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

#ifdef ROUTEMAP_WANTED
    MEMSET (&IpPrefixName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    IpPrefixName.pu1_OctetList = pIpBgpNeig->au1IpPrefixFilterIn;

    nmhGetFsbgp4mpePeerIpPrefixNameIn (BGP4_INET_AFI_IPV4, &PeerAddr,
                                       &IpPrefixName);

    MEMSET (&IpPrefixName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    IpPrefixName.pu1_OctetList = pIpBgpNeig->au1IpPrefixFilterOut;

    nmhGetFsbgp4mpePeerIpPrefixNameOut (BGP4_INET_AFI_IPV4, &PeerAddr,
                                        &IpPrefixName);
#endif

    /* fill send stats */
    /* fill announced capabilities */
    CapVal.pu1_OctetList = au1CapVal;
    CapVal.i4_Length = 0;

    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapAnnouncedStatus (BGP4_INET_AFI_IPV4,
                                                  &PeerAddr,
                                                  CAP_CODE_ROUTE_REFRESH,
                                                  0, &CapVal, &i4CapSts);

    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->SendStats.u4Cap |= BGP_CLI_CAP_ROUTE_REFRESH;
        /* fill route-refresh msg sentto the peer  */
        if (BGP4_PEER_IPV4_AFISAFI_INSTANCE (pPeer) != NULL)
        {
            pIpBgpNeig->SendStats.u4Ipv4RtRefreshCnt =
                BGP4_RTREF_MSG_SENT_CTR (pPeer, BGP4_IPV4_UNI_INDEX);
        }
    }

    /* to fill the announced status for 4-byte ASN */
    i4CapSts = 0;
    CapVal.pu1_OctetList = au1CapVal;
    CapVal.i4_Length = CAP_4_BYTE_ASN_LENGTH;
    MEMSET (CapVal.pu1_OctetList, 0, CAP_4_BYTE_ASN_LENGTH);
    if ((BGP4_CONFED_ID (BGP4_PEER_CXT_ID (pPeer)) != BGP4_INV_AS) &&
        (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeer), pPeer) ==
         BGP4_EXTERNAL_PEER) && (BGP4_CONFED_PEER_STATUS (pPeer) == BGP4_FALSE))
    {
        PTR_ASSIGN4 (CapVal.pu1_OctetList,
                     BGP4_CONFED_ID (BGP4_PEER_CXT_ID (pPeer)));
    }
    else
    {
        PTR_ASSIGN4 (CapVal.pu1_OctetList, BGP4_PEER_LOCAL_AS (pPeer));
    }

    i4RetVal = nmhGetFsbgp4mpeCapAnnouncedStatus (BGP4_INET_AFI_IPV4,
                                                  &PeerAddr,
                                                  CAP_CODE_4_OCTET_ASNO,
                                                  CAP_4_BYTE_ASN_LENGTH,
                                                  &CapVal, &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->SendStats.u4Cap |= BGP_CLI_CAP_4_OCTET_ASNO;
    }

    /* to avoid memory allcation */
    CapVal.i4_Length = CAP_MP_CAP_LENGTH;
    PTR_ASSIGN4 (CapVal.pu1_OctetList, (UINT4) CAP_MP_IPV4_UNICAST);
    i4RetVal = nmhGetFsbgp4mpeCapAnnouncedStatus (BGP4_INET_AFI_IPV4,
                                                  &PeerAddr, CAP_CODE_MP_EXTN,
                                                  CAP_MP_CAP_LENGTH, &CapVal,
                                                  &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {

        pIpBgpNeig->SendStats.u4Cap |= BGP_CLI_CAP_IPV4_UNICAST;
    }

    OrfCapsValue.u2Afi = OSIX_HTONS (BGP4_INET_AFI_IPV4);
    OrfCapsValue.u1Safi = BGP4_INET_SAFI_UNICAST;
    OrfCapsValue.u1OrfCount = 1;
    OrfCapsValue.u1OrfType = BGP_ORF_TYPE_ADDRESS_PREFIX;
    OrfCapsValue.u1OrfMode = BGP4_CLI_ORF_MODE_SEND;

    CapVal.pu1_OctetList = (UINT1 *) &OrfCapsValue;
    CapVal.i4_Length = BGP_ORF_CAPS_LEN;

    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapAnnouncedStatus (BGP4_INET_AFI_IPV4,
                                                  &PeerAddr, CAP_CODE_ORF,
                                                  BGP_ORF_CAPS_LEN, &CapVal,
                                                  &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->SendStats.u4Cap |= BGP_CLI_CAP_IPV4_ORF_SEND;
    }

    OrfCapsValue.u1OrfMode = BGP4_CLI_ORF_MODE_RECEIVE;

    CapVal.pu1_OctetList = (UINT1 *) &OrfCapsValue;
    CapVal.i4_Length = BGP_ORF_CAPS_LEN;

    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapAnnouncedStatus (BGP4_INET_AFI_IPV4,
                                                  &PeerAddr, CAP_CODE_ORF,
                                                  BGP_ORF_CAPS_LEN, &CapVal,
                                                  &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->SendStats.u4Cap |= BGP_CLI_CAP_IPV4_ORF_RECEIVE;
    }

#ifdef BGP4_IPV6_WANTED
    CapVal.i4_Length = CAP_MP_CAP_LENGTH;
    PTR_ASSIGN4 (CapVal.pu1_OctetList, (UINT4) CAP_MP_IPV6_UNICAST);
    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapAnnouncedStatus (BGP4_INET_AFI_IPV4,
                                                  &PeerAddr, CAP_CODE_MP_EXTN,
                                                  CAP_MP_CAP_LENGTH, &CapVal,
                                                  &i4CapSts);

    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->SendStats.u4Cap |= BGP_CLI_CAP_IPV6_UNICAST;
    }
#endif
#ifdef L3VPN
    CapVal.i4_Length = CAP_MP_CAP_LENGTH;
    PTR_ASSIGN4 (CapVal.pu1_OctetList, (UINT4) CAP_MP_LABELLED_IPV4);
    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapAnnouncedStatus (BGP4_INET_AFI_IPV4,
                                                  &PeerAddr, CAP_CODE_MP_EXTN,
                                                  CAP_MP_CAP_LENGTH, &CapVal,
                                                  &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->SendStats.u4Cap |= BGP_CLI_CAP_LBLD_IPV4_UNICAST;
    }

    CapVal.i4_Length = CAP_MP_CAP_LENGTH;
    PTR_ASSIGN4 (CapVal.pu1_OctetList, (UINT4) CAP_MP_VPN4_UNICAST);
    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapAnnouncedStatus (BGP4_INET_AFI_IPV4,
                                                  &PeerAddr, CAP_CODE_MP_EXTN,
                                                  CAP_MP_CAP_LENGTH, &CapVal,
                                                  &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->SendStats.u4Cap |= BGP_CLI_CAP_VPN4_UNICAST;
    }
#endif

#ifdef VPLSADS_WANTED
    CapVal.i4_Length = CAP_MP_CAP_LENGTH;
    PTR_ASSIGN4 (CapVal.pu1_OctetList, (UINT4) CAP_MP_L2VPN_VPLS);
    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapAnnouncedStatus (BGP4_INET_AFI_IPV4,
                                                  &PeerAddr, CAP_CODE_MP_EXTN,
                                                  CAP_MP_CAP_LENGTH, &CapVal,
                                                  &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->SendStats.u4Cap |= BGP_CLI_CAP_L2VPN_VPLS;
    }
#endif

    /* total updates sent */
    i4RetVal = nmhGetBgpPeerOutUpdates (u4Neighbor,
                                        &pIpBgpNeig->SendStats.u4Updates);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : nmhGetBgpPeerOutUpdates "
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* total msgs sent to the peer */
    i4RetVal = nmhGetBgpPeerOutTotalMessages (u4Neighbor,
                                              &pIpBgpNeig->SendStats.
                                              u4TotalMessages);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : "
                  "nmhGetBgpPeerOutTotalMessages failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }
    /* Route refresh statistics */
    i4RetVal = nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgRcvdCntr
        (BGP4_INET_AFI_IPV4, &PeerAddr, BGP4_INET_AFI_IPV4,
         BGP4_INET_SAFI_UNICAST, &pIpBgpNeig->RcvdStats.u4RouteRefresh);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : "
                  "nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgRcvdCntr failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }
    i4RetVal = nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgSentCntr
        (BGP4_INET_AFI_IPV4, &PeerAddr, BGP4_INET_AFI_IPV4,
         BGP4_INET_SAFI_UNICAST, &pIpBgpNeig->SendStats.u4RouteRefresh);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : "
                  "nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgSentCntr failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill receive stats */

    /* fill received capabilities */
    CapVal.i4_Length = 0;
    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapReceivedStatus (BGP4_INET_AFI_IPV4,
                                                 &PeerAddr,
                                                 CAP_CODE_ROUTE_REFRESH, 0,
                                                 &CapVal, &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->RcvdStats.u4Cap |= BGP_CLI_CAP_ROUTE_REFRESH;
        /* fill route-refresh msg received from the peer */
        if (BGP4_PEER_IPV4_AFISAFI_INSTANCE (pPeer) != NULL)
        {
            pIpBgpNeig->RcvdStats.u4Ipv4RtRefreshCnt =
                BGP4_RTREF_MSG_RCVD_CTR (pPeer, BGP4_IPV4_UNI_INDEX);
        }
    }

    /* to fill the received status for 4-byte ASN */
    CapVal.i4_Length = CAP_4_BYTE_ASN_LENGTH;
    MEMSET (CapVal.pu1_OctetList, 0, CAP_4_BYTE_ASN_LENGTH);

    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapReceivedStatus (BGP4_INET_AFI_IPV4,
                                                 &PeerAddr,
                                                 CAP_CODE_4_OCTET_ASNO,
                                                 CAP_4_BYTE_ASN_LENGTH,
                                                 &CapVal, &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->RcvdStats.u4Cap |= BGP_CLI_CAP_4_OCTET_ASNO;
    }

    CapVal.i4_Length = CAP_MP_CAP_LENGTH;
    PTR_ASSIGN4 (CapVal.pu1_OctetList, (UINT4) CAP_MP_IPV4_UNICAST);
    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapReceivedStatus (BGP4_INET_AFI_IPV4,
                                                 &PeerAddr, CAP_CODE_MP_EXTN,
                                                 CAP_MP_CAP_LENGTH, &CapVal,
                                                 &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {

        pIpBgpNeig->RcvdStats.u4Cap |= BGP_CLI_CAP_IPV4_UNICAST;
    }

    OrfCapsValue.u1OrfMode = BGP4_CLI_ORF_MODE_RECEIVE;

    CapVal.pu1_OctetList = (UINT1 *) &OrfCapsValue;
    CapVal.i4_Length = BGP_ORF_CAPS_LEN;

    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapReceivedStatus (BGP4_INET_AFI_IPV4,
                                                 &PeerAddr, CAP_CODE_ORF,
                                                 BGP_ORF_CAPS_LEN, &CapVal,
                                                 &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->RcvdStats.u4Cap |= BGP_CLI_CAP_IPV4_ORF_RECEIVE;
    }

    OrfCapsValue.u1OrfMode = BGP4_CLI_ORF_MODE_SEND;

    CapVal.pu1_OctetList = (UINT1 *) &OrfCapsValue;
    CapVal.i4_Length = BGP_ORF_CAPS_LEN;

    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapReceivedStatus (BGP4_INET_AFI_IPV4,
                                                 &PeerAddr, CAP_CODE_ORF,
                                                 BGP_ORF_CAPS_LEN, &CapVal,
                                                 &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->RcvdStats.u4Cap |= BGP_CLI_CAP_IPV4_ORF_SEND;
    }

#ifdef BGP4_IPV6_WANTED
    CapVal.i4_Length = CAP_MP_CAP_LENGTH;
    PTR_ASSIGN4 (CapVal.pu1_OctetList, (UINT4) CAP_MP_IPV6_UNICAST);

    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapReceivedStatus (BGP4_INET_AFI_IPV4,
                                                 &PeerAddr, CAP_CODE_MP_EXTN,
                                                 CAP_MP_CAP_LENGTH, &CapVal,
                                                 &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {

        pIpBgpNeig->RcvdStats.u4Cap |= BGP_CLI_CAP_IPV6_UNICAST;
    }
#endif
#ifdef L3VPN
    CapVal.i4_Length = CAP_MP_CAP_LENGTH;
    PTR_ASSIGN4 (CapVal.pu1_OctetList, (UINT4) CAP_MP_LABELLED_IPV4);

    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapReceivedStatus (BGP4_INET_AFI_IPV4,
                                                 &PeerAddr, CAP_CODE_MP_EXTN,
                                                 CAP_MP_CAP_LENGTH, &CapVal,
                                                 &i4CapSts);

    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->RcvdStats.u4Cap |= BGP_CLI_CAP_LBLD_IPV4_UNICAST;
    }

    CapVal.i4_Length = CAP_MP_CAP_LENGTH;
    PTR_ASSIGN4 (CapVal.pu1_OctetList, (UINT4) CAP_MP_VPN4_UNICAST);

    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapReceivedStatus (BGP4_INET_AFI_IPV4,
                                                 &PeerAddr, CAP_CODE_MP_EXTN,
                                                 CAP_MP_CAP_LENGTH, &CapVal,
                                                 &i4CapSts);

    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->RcvdStats.u4Cap |= BGP_CLI_CAP_VPN4_UNICAST;
    }
#endif

#ifdef VPLSADS_WANTED
    CapVal.i4_Length = CAP_MP_CAP_LENGTH;
    PTR_ASSIGN4 (CapVal.pu1_OctetList, (UINT4) CAP_MP_L2VPN_VPLS);
    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapReceivedStatus (BGP4_INET_AFI_IPV4,
                                                 &PeerAddr, CAP_CODE_MP_EXTN,
                                                 CAP_MP_CAP_LENGTH, &CapVal,
                                                 &i4CapSts);

    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->RcvdStats.u4Cap |= BGP_CLI_CAP_L2VPN_VPLS;
    }
#endif

#ifdef BGP4_IPV6_WANTED
    if ((pIpBgpNeig->RcvdStats.u4Cap & BGP_CLI_CAP_IPV6_UNICAST) ==
        BGP_CLI_CAP_IPV6_UNICAST)
    {
        if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
        {
            pIpBgpNeig->SendStats.u4Ipv6RtRefreshCnt =
                BGP4_RTREF_MSG_SENT_CTR (pPeer, BGP4_IPV6_UNI_INDEX);

            pIpBgpNeig->RcvdStats.u4Ipv6RtRefreshCnt =
                BGP4_RTREF_MSG_RCVD_CTR (pPeer, BGP4_IPV6_UNI_INDEX);
        }
    }
#endif
#ifdef L3VPN
    if ((pIpBgpNeig->RcvdStats.u4Cap & BGP_CLI_CAP_LBLD_IPV4_UNICAST) ==
        BGP_CLI_CAP_LBLD_IPV4_UNICAST)
    {
        if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
        {
            pIpBgpNeig->SendStats.u4LbldIpv4RtRefreshCnt =
                BGP4_RTREF_MSG_SENT_CTR (pPeer, BGP4_IPV4_LBLD_INDEX);

            pIpBgpNeig->RcvdStats.u4LbldIpv4RtRefreshCnt =
                BGP4_RTREF_MSG_RCVD_CTR (pPeer, BGP4_IPV4_LBLD_INDEX);
        }
    }

    if ((pIpBgpNeig->RcvdStats.u4Cap & BGP_CLI_CAP_VPN4_UNICAST) ==
        BGP_CLI_CAP_VPN4_UNICAST)
    {
        if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
        {
            pIpBgpNeig->SendStats.u4Vpnv4RtRefreshCnt =
                BGP4_RTREF_MSG_SENT_CTR (pPeer, BGP4_VPN4_UNI_INDEX);

            pIpBgpNeig->RcvdStats.u4Vpnv4RtRefreshCnt =
                BGP4_RTREF_MSG_RCVD_CTR (pPeer, BGP4_VPN4_UNI_INDEX);
        }
    }
#endif
    /* updates received */
    i4RetVal = nmhGetBgpPeerInUpdates (u4Neighbor,
                                       &pIpBgpNeig->RcvdStats.u4Updates);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : nmhGetBgpPeerInUpdates "
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* total msgs received */
    i4RetVal = nmhGetBgpPeerInTotalMessages (u4Neighbor,
                                             &pIpBgpNeig->RcvdStats.
                                             u4TotalMessages);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : "
                  "nmhGetBgpPeerInTotalMessages failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill no of time connections was established */
    i4RetVal = nmhGetBgpPeerFsmEstablishedTransitions (u4Neighbor,
                                                       &pIpBgpNeig->
                                                       u4ConnEstab);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : "
                  "nmhGetBgpPeerFsmEstablishedTransitions failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill reason why last session was tore-down */
    pIpBgpNeig->u1LastErrorCode = BGP4_PEER_LAST_ERROR (pPeer);
    pIpBgpNeig->u1LastErrorSubCode = BGP4_PEER_LAST_ERROR_SUB_CODE (pPeer);

    /* fill peer network address */
#ifdef BGP4_IPV6_WANTED
    Bgp4InitAddrPrefixStruct (&(pIpBgpNeig->NeighNetIp), BGP4_INET_AFI_IPV6);
#else
    Bgp4InitAddrPrefixStruct (&(pIpBgpNeig->NeighNetIp), 0);
#endif
    MEMSET (au1IpAddrOctetList, 0, BGP4_MAX_INET_ADDRESS_LEN);
    PeerNetAddr.pu1_OctetList = au1IpAddrOctetList;
    i4RetVal = nmhGetFsbgp4mpePeerExtNetworkAddress (BGP4_INET_AFI_IPV4,
                                                     &PeerAddr, &PeerNetAddr);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() : nmhGetBgpPeerNetworkAddr"
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }
    MEMCPY (pIpBgpNeig->NeighNetIp.au1Address, PeerNetAddr.pu1_OctetList,
            BGP4_MAX_INET_ADDRESS_LEN);
    /* Counters  - accepted prefixes */
    pIpBgpNeig->RcvdStats.u4Ipv4UniRtsCnt = 0;
    i4RetVal =
        nmhGetFsbgp4MpePrefixCountersInPrefixesAccepted (BGP4_INET_AFI_IPV4,
                                                         &PeerAddr,
                                                         BGP4_INET_AFI_IPV4,
                                                         BGP4_INET_SAFI_UNICAST,
                                                         &(pIpBgpNeig->
                                                           RcvdStats.
                                                           u4Ipv4UniRtsCnt));
#ifdef BGP4_IPV6_WANTED
    pIpBgpNeig->RcvdStats.u4Ipv6UniRtsCnt = 0;
    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
    {
        i4RetVal =
            nmhGetFsbgp4MpePrefixCountersInPrefixesAccepted (BGP4_INET_AFI_IPV4,
                                                             &PeerAddr,
                                                             BGP4_INET_AFI_IPV6,
                                                             BGP4_INET_SAFI_UNICAST,
                                                             &(pIpBgpNeig->
                                                               RcvdStats.
                                                               u4Ipv6UniRtsCnt));
    }
#endif
#ifdef L3VPN
    pIpBgpNeig->RcvdStats.u4Ipv4LblRtsCnt = 0;
    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
    {
        i4RetVal =
            nmhGetFsbgp4MpePrefixCountersInPrefixesAccepted (BGP4_INET_AFI_IPV4,
                                                             &PeerAddr,
                                                             BGP4_INET_AFI_IPV4,
                                                             BGP4_INET_SAFI_LABEL,
                                                             &(pIpBgpNeig->
                                                               RcvdStats.
                                                               u4Ipv4LblRtsCnt));
    }
    pIpBgpNeig->RcvdStats.u4Vpn4UniRtsCnt = 0;
    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
    {
        i4RetVal =
            nmhGetFsbgp4MpePrefixCountersInPrefixesAccepted (BGP4_INET_AFI_IPV4,
                                                             &PeerAddr,
                                                             BGP4_INET_AFI_IPV4,
                                                             BGP4_INET_SAFI_VPNV4_UNICAST,
                                                             &(pIpBgpNeig->
                                                               RcvdStats.
                                                               u4Vpn4UniRtsCnt));
    }
#endif
    i4RetVal = nmhGetFsbgp4mpePeerSessionAuthStatus (BGP4_INET_AFI_IPV4,
                                                     &PeerAddr,
                                                     &(pIpBgpNeig->
                                                       i4ConnAuthStatus));

    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() :"
                  " nmhGetFsbgp4mpePeerSessionAuthStatus failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }
    /* fill the local ip address only if its configured by user explicitly */

    if (BGP4_PEER_LOCAL_ADDR_CONFIGURED (pPeer) == BGP4_TRUE)
    {
        /* fill Update source addr */
        MEMSET (au1UpdateSrcOctetList, 0, BGP4_IPV4_PREFIX_LEN);
        UpdateSource.pu1_OctetList = au1UpdateSrcOctetList;
        i4RetVal =
            nmhGetFsbgp4mpePeerExtLclAddress (BGP4_INET_AFI_IPV4, &PeerAddr,
                                              &UpdateSource);
        if (i4RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\nbgpGetIpv4NeighborAttributes() :"
                      " nmhGetFsbgp4mpePeerExtLclAddress failed\n");
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }
        pIpBgpNeig->UpdateSource.u2Afi = BGP4_INET_AFI_IPV4;
        MEMCPY (pIpBgpNeig->UpdateSource.au1Address, UpdateSource.pu1_OctetList,
                UpdateSource.i4_Length);
        pIpBgpNeig->UpdateSource.u2AddressLen = (UINT2) UpdateSource.i4_Length;
    }
    /* Fill BFD monitoring status */
    i4RetVal =
        nmhGetFsbgp4mpePeerBfdStatus (BGP4_INET_AFI_IPV4, &PeerAddr,
                                      &i4BfdStatus);
    pIpBgpNeig->u1BfdStatus = (UINT1) i4BfdStatus;
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv4NeighborAttributes() :"
                  " nmhGetFsbgp4mpePeerBfdStatus failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    i4RetVal = Bgp4SnmphGetGrCapSupportStatus (PeerRemAddr);
    if (i4RetVal == BGP4_TRUE)
    {
        pIpBgpNeig->u1GRCapability = BGP4_GR_ADVT;
    }
    i4RetVal = Bgp4GRIsPeerGRCapable (pPeer);
    if (i4RetVal == BGP4_SUCCESS)
    {
        if (pIpBgpNeig->u1GRCapability == BGP4_GR_ADVT)
        {
            pIpBgpNeig->u1GRCapability = BGP4_GR_ADVT_RCVD;
        }
        else
        {
            pIpBgpNeig->u1GRCapability = BGP4_GR_RCVD;
        }
    }
    pIpBgpNeig->u4RestartInterval = BGP4_PEER_RESTART_INTERVAL (pPeer);
    return BGP4_SUCCESS;
}

#ifdef BGP4_IPV6_WANTED
/*****************************************************************************/
/* Function Name : bgpGetIpv6NeighborAttributes                              */
/* Description   : This is a CLI utility function used for fetching the      */
/*               : attributes of a neighbor.                                 */
/* Input(s)      : See parameters below.                                     */
/* Output(s)     : Fills the attributes in the pIpBgpNeig buffer.            */
/* Return(s)     : BGP4_SUCCESS - if operation is success                    */
/*               : BGP4_CLI_ERR_xx - if operation fails                      */
/*****************************************************************************/
INT4
bgpGetIpv6NeighborAttributes (tAddrPrefix * pNeighbor,    /* index: Neighbor address */
                              tIpBgpNeighbor * pIpBgpNeig    /* return structure  */
    )
{
    tSNMP_OCTET_STRING_TYPE CapVal;
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE LocalAddr;
    tSNMP_OCTET_STRING_TYPE PeerNetAddr;
    tSNMP_OCTET_STRING_TYPE UpdateSource;
#ifdef ROUTEMAP_WANTED
    tSNMP_OCTET_STRING_TYPE IpPrefixName;
#endif
    tAddrPrefix         PeerRemAddr;
    tOrfCapsValue       OrfCapsValue;
    tBgp4PeerEntry     *pPeer = NULL;
    INT4                i4RetVal;
    INT4                i4CapSts;
    UINT4               u4LocalAddr;
    UINT4               u4RemAddr;
    UINT1               au1CapVal[CAP_MIN_CAP_MAX_DATA_SIZE];
    UINT1               au1PeerOctetList[BGP4_IPV6_PREFIX_LEN];
    UINT1               au1LocalOctetList[BGP4_IPV6_PREFIX_LEN];
    UINT1               au1UpdateSrcOctetList[BGP4_IPV6_PREFIX_LEN];
    UINT4               u4PeerNetOctetList;
    UINT4               u4Context;
    INT4                i4BfdStatus = 0;

    MEMSET (&OrfCapsValue, 0, sizeof (tOrfCapsValue));
    u4Context = gpBgpCurrCxtNode->u4ContextId;
    /* fill neighbor ip address */
    pIpBgpNeig->NeigIp.u2Afi = BGP4_INET_AFI_IPV6;
    MEMCPY (pIpBgpNeig->NeigIp.au1Address, pNeighbor->au1Address,
            BGP4_IPV6_PREFIX_LEN);
    pIpBgpNeig->NeigIp.u2AddressLen = BGP4_IPV6_PREFIX_LEN;

    MEMSET (au1PeerOctetList, 0, BGP4_IPV6_PREFIX_LEN);
    PeerAddr.pu1_OctetList = au1PeerOctetList;
    MEMCPY ((PeerAddr.pu1_OctetList), pNeighbor->au1Address,
            BGP4_IPV6_PREFIX_LEN);
    PeerAddr.i4_Length = BGP4_IPV6_PREFIX_LEN;

    Bgp4InitAddrPrefixStruct (&PeerRemAddr, BGP4_INET_AFI_IPV6);
    MEMCPY (PeerRemAddr.au1Address, (PeerAddr.pu1_OctetList),
            BGP4_IPV6_PREFIX_LEN);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerRemAddr);
    if (pPeer == NULL)
    {
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill remote-as */
    i4RetVal = nmhGetFsbgp4mpebgpPeerRemoteAs (pNeighbor->u2Afi,
                                               &PeerAddr,
                                               &(pIpBgpNeig->u4RemoteAs));
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : nmhGetBgpPeerRemoteAs "
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* Fill Peer Admin State */
    i4RetVal = nmhGetFsbgp4mpebgpPeerAdminStatus (pNeighbor->u2Afi,
                                                  &PeerAddr,
                                                  (INT4 *) &pIpBgpNeig->
                                                  u4PeerAdminState);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : nmhGetBgpPeerAdminStatus"
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* Fill Peer Operational Status */
    if (BGP4_GET_PEER_PEND_FLAG (pPeer) & BGP4_PEER_MULTIHOP_PEND_START)
    {
        pIpBgpNeig->u4PeerOperStatus |= BGP_CLI_PEND_MULTIHOP;
    }

    if (BGP4_GET_PEER_PEND_FLAG (pPeer) & BGP4_PEER_MP_CAP_CONFIG_PEND_START)
    {
        pIpBgpNeig->u4PeerOperStatus |= BGP_CLI_PEND_MP_CONFIG_CAPS;
    }
    if (BGP4_GET_PEER_PEND_FLAG (pPeer) & BGP4_PEER_MP_CAP_RECV_PEND_START)
    {
        pIpBgpNeig->u4PeerOperStatus |= BGP_CLI_PEND_MP_RECV_CAPS;
    }

    /* fill remote tcp port */
    i4RetVal = nmhGetFsbgp4mpebgpPeerRemotePort (pNeighbor->u2Afi,
                                                 &PeerAddr,
                                                 (INT4 *) &pIpBgpNeig->
                                                 u4NeigPort);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : nmhGetBgpPeerRemotePort"
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill local ip address */
    MEMSET (au1LocalOctetList, 0, BGP4_IPV6_PREFIX_LEN);
    LocalAddr.pu1_OctetList = au1LocalOctetList;
    i4RetVal = nmhGetFsbgp4mpebgpPeerLocalAddr (pNeighbor->u2Afi,
                                                &PeerAddr, &LocalAddr);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : nmhGetBgpPeerLocalAddr"
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    pIpBgpNeig->LocalIp.u2Afi = BGP4_INET_AFI_IPV6;
    MEMCPY (pIpBgpNeig->LocalIp.au1Address, LocalAddr.pu1_OctetList,
            LocalAddr.i4_Length);
    pIpBgpNeig->LocalIp.u2AddressLen = (UINT2) LocalAddr.i4_Length;

    /* fill local tcp port */
    i4RetVal = nmhGetFsbgp4mpebgpPeerLocalPort (pNeighbor->u2Afi,
                                                &PeerAddr,
                                                (INT4 *) &pIpBgpNeig->
                                                u4LocalPort);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : nmhGetBgpPeerLocalPort"
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill negotiated BGP version */
    i4RetVal = nmhGetFsbgp4mpebgpPeerNegotiatedVersion (pNeighbor->u2Afi,
                                                        &PeerAddr,
                                                        (INT4 *) &(pIpBgpNeig->
                                                                   u4NegBgpVersion));
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : nmhGetBgpPeerNegotiatedVersion "
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill neighbor router-id */
    i4RetVal = nmhGetFsbgp4mpebgpPeerIdentifier (pNeighbor->u2Afi,
                                                 &PeerAddr, &LocalAddr);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : nmhGetBgpPeerIdentifier "
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }
    PTR_FETCH4 (u4LocalAddr, LocalAddr.pu1_OctetList);
    MEMCPY (&(pIpBgpNeig->u4NeigRouterId), &u4LocalAddr, sizeof (UINT4));

    /* fill bgp session state */
    i4RetVal =
        nmhGetFsbgp4mpebgpPeerState (pNeighbor->u2Afi,
                                     &PeerAddr,
                                     (INT4 *) &pIpBgpNeig->u4BgpState);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : nmhGetBgpPeerState "
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill elapsed time */
    i4RetVal = nmhGetFsbgp4mpebgpPeerFsmEstablishedTime (pNeighbor->u2Afi,
                                                         &PeerAddr,
                                                         &(pIpBgpNeig->
                                                           u4ElapsedTime));
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : "
                  "nmhGetBgpPeerFsmEstablishedTime failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill elapsed time since last update */
    i4RetVal = nmhGetFsbgp4mpebgpPeerInUpdateElapsedTime (pNeighbor->u2Afi,
                                                          &PeerAddr,
                                                          &(pIpBgpNeig->
                                                            u4LastUpdateTime));
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : "
                  "nmhGetBgpPeerInUpdateElapsedTime failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill hold time */
    if (pIpBgpNeig->u4BgpState < BGP4_OPENCONFIRM_STATE)
    {
        i4RetVal = nmhGetFsbgp4mpebgpPeerHoldTimeConfigured (pNeighbor->u2Afi,
                                                             &PeerAddr,
                                                             (INT4 *)
                                                             &(pIpBgpNeig->
                                                               u4HoldTime));
    }
    else
    {
        i4RetVal = nmhGetFsbgp4mpebgpPeerHoldTime (pNeighbor->u2Afi,
                                                   &PeerAddr,
                                                   (INT4 *) &(pIpBgpNeig->
                                                              u4HoldTime));
    }

    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : nmhGetBgpPeerHoldTime "
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill keepalive time */
    if (pIpBgpNeig->u4BgpState < BGP4_OPENCONFIRM_STATE)
    {
        i4RetVal = nmhGetFsbgp4mpebgpPeerKeepAliveConfigured (pNeighbor->u2Afi,
                                                              &PeerAddr,
                                                              (INT4 *)
                                                              &pIpBgpNeig->
                                                              u4KeepAlive);
    }
    else
    {
        i4RetVal = nmhGetFsbgp4mpebgpPeerKeepAlive (pNeighbor->u2Afi,
                                                    &PeerAddr,
                                                    (INT4 *) &pIpBgpNeig->
                                                    u4KeepAlive);
    }

    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : nmhGetBgpPeerKeepAlive "
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill minimum advertisement time */
    i4RetVal =
        nmhGetFsbgp4mpebgpPeerMinRouteAdvertisementInterval (pNeighbor->u2Afi,
                                                             &PeerAddr,
                                                             (INT4 *)
                                                             &(pIpBgpNeig->
                                                               u4MinAdvtInt));
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : "
                  "nmhGetBgpPeerMinRouteAdvertisementInterval failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill minimum AS origination interval */
    i4RetVal = nmhGetFsbgp4mpebgpPeerMinASOriginationInterval (pNeighbor->u2Afi,
                                                               &PeerAddr,
                                                               (INT4 *)
                                                               &(pIpBgpNeig->
                                                                 u4MinAsOrigInt));
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : "
                  "nmhGetBgpPeerMinASOriginationInterval failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill connect retry timer */
    i4RetVal = nmhGetFsbgp4mpebgpPeerConnectRetryInterval (pNeighbor->u2Afi,
                                                           &PeerAddr,
                                                           (INT4 *)
                                                           &(pIpBgpNeig->
                                                             u4ConnectRetry));
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : "
                  "nmhGetBgpPeerConnectRetryInterval failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

#ifdef ROUTEMAP_WANTED
    MEMSET (&IpPrefixName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    IpPrefixName.pu1_OctetList = pIpBgpNeig->au1IpPrefixFilterIn;

    nmhGetFsbgp4mpePeerIpPrefixNameIn (BGP4_INET_AFI_IPV6, &PeerAddr,
                                       &IpPrefixName);

    MEMSET (&IpPrefixName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    IpPrefixName.pu1_OctetList = pIpBgpNeig->au1IpPrefixFilterOut;

    nmhGetFsbgp4mpePeerIpPrefixNameOut (BGP4_INET_AFI_IPV6, &PeerAddr,
                                        &IpPrefixName);
#endif

    /* fill send stats */
    /* fill announced capabilities */
    CapVal.pu1_OctetList = au1CapVal;
    CapVal.i4_Length = 0;

    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapAnnouncedStatus (BGP4_INET_AFI_IPV6,
                                                  &PeerAddr,
                                                  CAP_CODE_ROUTE_REFRESH,
                                                  0, &CapVal, &i4CapSts);

    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->SendStats.u4Cap |= BGP_CLI_CAP_ROUTE_REFRESH;
        /* fill route-refresh msg sentto the peer  */
        if (BGP4_PEER_IPV4_AFISAFI_INSTANCE (pPeer) != NULL)
        {
            pIpBgpNeig->SendStats.u4Ipv4RtRefreshCnt =
                BGP4_RTREF_MSG_SENT_CTR (pPeer, BGP4_IPV4_UNI_INDEX);
        }
    }

    /* to fill the announced status for 4-byte ASN */
    i4CapSts = 0;
    CapVal.pu1_OctetList = au1CapVal;
    CapVal.i4_Length = CAP_4_BYTE_ASN_LENGTH;
    MEMSET (CapVal.pu1_OctetList, 0, CAP_4_BYTE_ASN_LENGTH);
    if ((BGP4_CONFED_ID (BGP4_PEER_CXT_ID (pPeer)) != BGP4_INV_AS) &&
        (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeer), pPeer) ==
         BGP4_EXTERNAL_PEER) && (BGP4_CONFED_PEER_STATUS (pPeer) == BGP4_FALSE))
    {
        PTR_ASSIGN4 (CapVal.pu1_OctetList,
                     BGP4_CONFED_ID (BGP4_PEER_CXT_ID (pPeer)));
    }
    else
    {
        PTR_ASSIGN4 (CapVal.pu1_OctetList, BGP4_PEER_LOCAL_AS (pPeer));
    }

    i4RetVal = nmhGetFsbgp4mpeCapAnnouncedStatus (BGP4_INET_AFI_IPV6,
                                                  &PeerAddr,
                                                  CAP_CODE_4_OCTET_ASNO,
                                                  CAP_4_BYTE_ASN_LENGTH,
                                                  &CapVal, &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->SendStats.u4Cap |= BGP_CLI_CAP_4_OCTET_ASNO;
    }

    /* to avoid memory allcation */
    CapVal.i4_Length = CAP_MP_CAP_LENGTH;
    PTR_ASSIGN4 (CapVal.pu1_OctetList, (UINT4) CAP_MP_IPV4_UNICAST);
    i4RetVal = nmhGetFsbgp4mpeCapAnnouncedStatus (BGP4_INET_AFI_IPV6,
                                                  &PeerAddr, CAP_CODE_MP_EXTN,
                                                  CAP_MP_CAP_LENGTH, &CapVal,
                                                  &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {

        pIpBgpNeig->SendStats.u4Cap |= BGP_CLI_CAP_IPV4_UNICAST;
    }

    /* To fetch the ORF send mode advertised status */
    OrfCapsValue.u2Afi = OSIX_HTONS (BGP4_INET_AFI_IPV6);
    OrfCapsValue.u1Safi = BGP4_INET_SAFI_UNICAST;
    OrfCapsValue.u1OrfCount = 1;
    OrfCapsValue.u1OrfType = BGP_ORF_TYPE_ADDRESS_PREFIX;
    OrfCapsValue.u1OrfMode = BGP4_CLI_ORF_MODE_SEND;

    CapVal.pu1_OctetList = (UINT1 *) &OrfCapsValue;
    CapVal.i4_Length = BGP_ORF_CAPS_LEN;

    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapAnnouncedStatus (BGP4_INET_AFI_IPV6,
                                                  &PeerAddr, CAP_CODE_ORF,
                                                  BGP_ORF_CAPS_LEN, &CapVal,
                                                  &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->SendStats.u4Cap |= BGP_CLI_CAP_IPV6_ORF_SEND;
    }

    /* To fetch the ORF receive mode advertised status */
    OrfCapsValue.u1OrfMode = BGP4_CLI_ORF_MODE_RECEIVE;

    CapVal.pu1_OctetList = (UINT1 *) &OrfCapsValue;
    CapVal.i4_Length = BGP_ORF_CAPS_LEN;

    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapAnnouncedStatus (BGP4_INET_AFI_IPV6,
                                                  &PeerAddr, CAP_CODE_ORF,
                                                  BGP_ORF_CAPS_LEN, &CapVal,
                                                  &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->SendStats.u4Cap |= BGP_CLI_CAP_IPV6_ORF_RECEIVE;
    }

    CapVal.pu1_OctetList = au1CapVal;

#ifdef BGP4_IPV6_WANTED
    CapVal.i4_Length = CAP_MP_CAP_LENGTH;
    PTR_ASSIGN4 (CapVal.pu1_OctetList, (UINT4) CAP_MP_IPV6_UNICAST);
    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapAnnouncedStatus (BGP4_INET_AFI_IPV6,
                                                  &PeerAddr, CAP_CODE_MP_EXTN,
                                                  CAP_MP_CAP_LENGTH, &CapVal,
                                                  &i4CapSts);

    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->SendStats.u4Cap |= BGP_CLI_CAP_IPV6_UNICAST;
    }
#endif
#ifdef L3VPN
    CapVal.i4_Length = CAP_MP_CAP_LENGTH;
    PTR_ASSIGN4 (CapVal.pu1_OctetList, (UINT4) CAP_MP_LABELLED_IPV4);
    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapAnnouncedStatus (BGP4_INET_AFI_IPV6,
                                                  &PeerAddr, CAP_CODE_MP_EXTN,
                                                  CAP_MP_CAP_LENGTH, &CapVal,
                                                  &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->SendStats.u4Cap |= BGP_CLI_CAP_LBLD_IPV4_UNICAST;
    }

    CapVal.i4_Length = CAP_MP_CAP_LENGTH;
    PTR_ASSIGN4 (CapVal.pu1_OctetList, (UINT4) CAP_MP_VPN4_UNICAST);
    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapAnnouncedStatus (BGP4_INET_AFI_IPV6,
                                                  &PeerAddr, CAP_CODE_MP_EXTN,
                                                  CAP_MP_CAP_LENGTH, &CapVal,
                                                  &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->SendStats.u4Cap |= BGP_CLI_CAP_VPN4_UNICAST;
    }
#endif

    /* total updates sent */
    i4RetVal = nmhGetFsbgp4mpebgpPeerOutUpdates (pNeighbor->u2Afi,
                                                 &PeerAddr,
                                                 &(pIpBgpNeig->SendStats.
                                                   u4Updates));
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : nmhGetBgpPeerOutUpdates "
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* total msgs sent to the peer */
    i4RetVal = nmhGetFsbgp4mpebgpPeerOutTotalMessages (pNeighbor->u2Afi,
                                                       &PeerAddr,
                                                       &(pIpBgpNeig->SendStats.
                                                         u4TotalMessages));
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : "
                  "nmhGetBgpPeerOutTotalMessages failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* Route refresh statistics */
    i4RetVal = nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgRcvdCntr
        (BGP4_INET_AFI_IPV6, &PeerAddr, BGP4_INET_AFI_IPV6,
         BGP4_INET_SAFI_UNICAST, &pIpBgpNeig->RcvdStats.u4RouteRefresh);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : "
                  "nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgRcvdCntr failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }
    i4RetVal = nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgSentCntr
        (BGP4_INET_AFI_IPV6, &PeerAddr, BGP4_INET_AFI_IPV6,
         BGP4_INET_SAFI_UNICAST, &pIpBgpNeig->SendStats.u4RouteRefresh);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : "
                  "nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgSentCntr failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill receive stats */
    /* fill received capabilities */
    CapVal.i4_Length = 0;
    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapReceivedStatus (BGP4_INET_AFI_IPV6,
                                                 &PeerAddr,
                                                 CAP_CODE_ROUTE_REFRESH, 0,
                                                 &CapVal, &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->RcvdStats.u4Cap |= BGP_CLI_CAP_ROUTE_REFRESH;
        /* fill route-refresh msg received from the peer */
        if (BGP4_PEER_IPV4_AFISAFI_INSTANCE (pPeer) != NULL)
        {
            pIpBgpNeig->RcvdStats.u4Ipv4RtRefreshCnt =
                BGP4_RTREF_MSG_RCVD_CTR (pPeer, BGP4_IPV4_UNI_INDEX);
        }
    }

    CapVal.i4_Length = CAP_4_BYTE_ASN_LENGTH;
    MEMSET (CapVal.pu1_OctetList, 0, CAP_4_BYTE_ASN_LENGTH);

    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapReceivedStatus (BGP4_INET_AFI_IPV6,
                                                 &PeerAddr,
                                                 CAP_CODE_4_OCTET_ASNO,
                                                 CAP_4_BYTE_ASN_LENGTH,
                                                 &CapVal, &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->RcvdStats.u4Cap |= BGP_CLI_CAP_4_OCTET_ASNO;
    }

    CapVal.i4_Length = CAP_MP_CAP_LENGTH;
    PTR_ASSIGN4 (CapVal.pu1_OctetList, (UINT4) CAP_MP_IPV4_UNICAST);
    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapReceivedStatus (BGP4_INET_AFI_IPV6,
                                                 &PeerAddr, CAP_CODE_MP_EXTN,
                                                 CAP_MP_CAP_LENGTH, &CapVal,
                                                 &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {

        pIpBgpNeig->RcvdStats.u4Cap |= BGP_CLI_CAP_IPV4_UNICAST;
    }

    /* To fetch the ORF send mode received status */
    OrfCapsValue.u1OrfMode = BGP4_CLI_ORF_MODE_SEND;

    CapVal.pu1_OctetList = (UINT1 *) &OrfCapsValue;
    CapVal.i4_Length = BGP_ORF_CAPS_LEN;

    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapReceivedStatus (BGP4_INET_AFI_IPV6,
                                                 &PeerAddr, CAP_CODE_ORF,
                                                 BGP_ORF_CAPS_LEN, &CapVal,
                                                 &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->RcvdStats.u4Cap |= BGP_CLI_CAP_IPV6_ORF_SEND;
    }

    /* To fetch the ORF receive mode received status */
    OrfCapsValue.u1OrfMode = BGP4_CLI_ORF_MODE_RECEIVE;

    CapVal.pu1_OctetList = (UINT1 *) &OrfCapsValue;
    CapVal.i4_Length = BGP_ORF_CAPS_LEN;

    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapReceivedStatus (BGP4_INET_AFI_IPV6,
                                                 &PeerAddr, CAP_CODE_ORF,
                                                 BGP_ORF_CAPS_LEN, &CapVal,
                                                 &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->RcvdStats.u4Cap |= BGP_CLI_CAP_IPV6_ORF_RECEIVE;
    }

    CapVal.pu1_OctetList = au1CapVal;

    /* To fetch the ORF receive mode advertised status */
#ifdef BGP4_IPV6_WANTED
    CapVal.i4_Length = CAP_MP_CAP_LENGTH;
    PTR_ASSIGN4 (CapVal.pu1_OctetList, (UINT4) CAP_MP_IPV6_UNICAST);

    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapReceivedStatus (BGP4_INET_AFI_IPV6,
                                                 &PeerAddr, CAP_CODE_MP_EXTN,
                                                 CAP_MP_CAP_LENGTH, &CapVal,
                                                 &i4CapSts);
    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {

        pIpBgpNeig->RcvdStats.u4Cap |= BGP_CLI_CAP_IPV6_UNICAST;
    }
#endif
#ifdef L3VPN
    CapVal.i4_Length = CAP_MP_CAP_LENGTH;
    PTR_ASSIGN4 (CapVal.pu1_OctetList, (UINT4) CAP_MP_LABELLED_IPV4);

    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapReceivedStatus (BGP4_INET_AFI_IPV6,
                                                 &PeerAddr, CAP_CODE_MP_EXTN,
                                                 CAP_MP_CAP_LENGTH, &CapVal,
                                                 &i4CapSts);

    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->RcvdStats.u4Cap |= BGP_CLI_CAP_LBLD_IPV4_UNICAST;
    }

    CapVal.i4_Length = CAP_MP_CAP_LENGTH;
    PTR_ASSIGN4 (CapVal.pu1_OctetList, (UINT4) CAP_MP_VPN4_UNICAST);

    i4CapSts = 0;
    i4RetVal = nmhGetFsbgp4mpeCapReceivedStatus (BGP4_INET_AFI_IPV6,
                                                 &PeerAddr, CAP_CODE_MP_EXTN,
                                                 CAP_MP_CAP_LENGTH, &CapVal,
                                                 &i4CapSts);

    if ((i4RetVal == BGP4_CLI_SUCCESS) && (i4CapSts == CAPS_TRUE))
    {
        pIpBgpNeig->RcvdStats.u4Cap |= BGP_CLI_CAP_VPN4_UNICAST;
    }
#endif

#ifdef BGP4_IPV6_WANTED
    if ((pIpBgpNeig->RcvdStats.u4Cap & BGP_CLI_CAP_IPV6_UNICAST) ==
        BGP_CLI_CAP_IPV6_UNICAST)
    {
        if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
        {
            pIpBgpNeig->SendStats.u4Ipv6RtRefreshCnt =
                BGP4_RTREF_MSG_SENT_CTR (pPeer, BGP4_IPV6_UNI_INDEX);

            pIpBgpNeig->RcvdStats.u4Ipv6RtRefreshCnt =
                BGP4_RTREF_MSG_RCVD_CTR (pPeer, BGP4_IPV6_UNI_INDEX);
        }
    }
#endif
#ifdef L3VPN
    if ((pIpBgpNeig->RcvdStats.u4Cap & BGP_CLI_CAP_LBLD_IPV4_UNICAST) ==
        BGP_CLI_CAP_LBLD_IPV4_UNICAST)
    {
        if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
        {
            pIpBgpNeig->SendStats.u4LbldIpv4RtRefreshCnt =
                BGP4_RTREF_MSG_SENT_CTR (pPeer, BGP4_IPV4_LBLD_INDEX);

            pIpBgpNeig->RcvdStats.u4LbldIpv4RtRefreshCnt =
                BGP4_RTREF_MSG_RCVD_CTR (pPeer, BGP4_IPV4_LBLD_INDEX);
        }
    }

    if ((pIpBgpNeig->RcvdStats.u4Cap & BGP_CLI_CAP_VPN4_UNICAST) ==
        BGP_CLI_CAP_VPN4_UNICAST)
    {
        if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
        {
            pIpBgpNeig->SendStats.u4Vpnv4RtRefreshCnt =
                BGP4_RTREF_MSG_SENT_CTR (pPeer, BGP4_VPN4_UNI_INDEX);

            pIpBgpNeig->RcvdStats.u4Vpnv4RtRefreshCnt =
                BGP4_RTREF_MSG_RCVD_CTR (pPeer, BGP4_VPN4_UNI_INDEX);
        }
    }
#endif

    /* updates received */
    i4RetVal = nmhGetFsbgp4mpebgpPeerInUpdates (pNeighbor->u2Afi,
                                                &PeerAddr,
                                                &(pIpBgpNeig->RcvdStats.
                                                  u4Updates));
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : nmhGetBgpPeerInUpdates "
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* total msgs received */
    i4RetVal = nmhGetFsbgp4mpebgpPeerInTotalMessages (pNeighbor->u2Afi,
                                                      &PeerAddr,
                                                      &(pIpBgpNeig->RcvdStats.
                                                        u4TotalMessages));
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : "
                  "nmhGetBgpPeerInTotalMessages failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill no of time connections was established */
    i4RetVal =
        nmhGetFsbgp4mpebgpPeerFsmEstablishedTransitions (pNeighbor->u2Afi,
                                                         &PeerAddr,
                                                         &(pIpBgpNeig->
                                                           u4ConnEstab));
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : "
                  "nmhGetBgpPeerFsmEstablishedTransitions failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* fill reason why last session was tore-down */
    pIpBgpNeig->u1LastErrorCode = BGP4_PEER_LAST_ERROR (pPeer);
    pIpBgpNeig->u1LastErrorSubCode = BGP4_PEER_LAST_ERROR_SUB_CODE (pPeer);

    /* fill peer network address */
    Bgp4InitAddrPrefixStruct (&(pIpBgpNeig->NeighNetIp), BGP4_INET_AFI_IPV4);
    PeerNetAddr.pu1_OctetList = (UINT1 *) (VOID *) &u4PeerNetOctetList;
    if (PeerNetAddr.pu1_OctetList == NULL)
    {
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }
    i4RetVal = nmhGetFsbgp4mpePeerExtNetworkAddress (BGP4_INET_AFI_IPV6,
                                                     &PeerAddr, &PeerNetAddr);
    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() : nmhGetBgpPeerNetworkAddr"
                  "failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* Counters  - accepted prefixes */
    pIpBgpNeig->RcvdStats.u4Ipv4UniRtsCnt = 0;
    i4RetVal =
        nmhGetFsbgp4MpePrefixCountersInPrefixesAccepted (BGP4_INET_AFI_IPV6,
                                                         &PeerAddr,
                                                         BGP4_INET_AFI_IPV4,
                                                         BGP4_INET_SAFI_UNICAST,
                                                         &(pIpBgpNeig->
                                                           RcvdStats.
                                                           u4Ipv4UniRtsCnt));
#ifdef BGP4_IPV6_WANTED
    pIpBgpNeig->RcvdStats.u4Ipv6UniRtsCnt = 0;
    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
    {
        i4RetVal =
            nmhGetFsbgp4MpePrefixCountersInPrefixesAccepted (BGP4_INET_AFI_IPV6,
                                                             &PeerAddr,
                                                             BGP4_INET_AFI_IPV6,
                                                             BGP4_INET_SAFI_UNICAST,
                                                             &(pIpBgpNeig->
                                                               RcvdStats.
                                                               u4Ipv6UniRtsCnt));
    }
#endif
#ifdef L3VPN
    pIpBgpNeig->RcvdStats.u4Ipv4LblRtsCnt = 0;
    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
    {
        i4RetVal =
            nmhGetFsbgp4MpePrefixCountersInPrefixesAccepted (BGP4_INET_AFI_IPV6,
                                                             &PeerAddr,
                                                             BGP4_INET_AFI_IPV4,
                                                             BGP4_INET_SAFI_LABEL,
                                                             &(pIpBgpNeig->
                                                               RcvdStats.
                                                               u4Ipv4LblRtsCnt));
    }
    pIpBgpNeig->RcvdStats.u4Vpn4UniRtsCnt = 0;
    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
    {
        i4RetVal =
            nmhGetFsbgp4MpePrefixCountersInPrefixesAccepted (BGP4_INET_AFI_IPV6,
                                                             &PeerAddr,
                                                             BGP4_INET_AFI_IPV4,
                                                             BGP4_INET_SAFI_VPNV4_UNICAST,
                                                             &(pIpBgpNeig->
                                                               RcvdStats.
                                                               u4Vpn4UniRtsCnt));
    }
#endif

    i4RetVal = nmhGetFsbgp4mpePeerSessionAuthStatus (BGP4_INET_AFI_IPV6,
                                                     &PeerAddr,
                                                     &(pIpBgpNeig->
                                                       i4ConnAuthStatus));

    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() :"
                  " nmhGetFsbgp4mpePeerSessionAuthStatus failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    PTR_FETCH4 (u4RemAddr, PeerNetAddr.pu1_OctetList);
    MEMCPY (pIpBgpNeig->NeighNetIp.au1Address, &u4RemAddr, sizeof (UINT4));
    /* fill the local ip address only if its configured by user explicitly */

    if (BGP4_PEER_LOCAL_ADDR_CONFIGURED (pPeer) == BGP4_TRUE)
    {
        /* fill Update source addr */
        MEMSET (au1UpdateSrcOctetList, 0, BGP4_IPV6_PREFIX_LEN);
        UpdateSource.pu1_OctetList = au1UpdateSrcOctetList;
        i4RetVal =
            nmhGetFsbgp4mpePeerExtLclAddress (BGP4_INET_AFI_IPV6, &PeerAddr,
                                              &UpdateSource);
        if (i4RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\nbgpGetIpv6NeighborAttributes() :"
                      " nmhGetFsbgp4mpePeerExtLclAddress failed\n");
            return BGP4_CLI_ERR_GENERAL_ERROR;
        }
        pIpBgpNeig->UpdateSource.u2Afi = BGP4_INET_AFI_IPV6;
        MEMCPY (pIpBgpNeig->UpdateSource.au1Address, UpdateSource.pu1_OctetList,
                UpdateSource.i4_Length);
        pIpBgpNeig->UpdateSource.u2AddressLen = (UINT2) UpdateSource.i4_Length;
    }
    /* fill BFD monitoring status */
    i4RetVal =
        nmhGetFsbgp4mpePeerBfdStatus (BGP4_INET_AFI_IPV6, &PeerAddr,
                                      &i4BfdStatus);

    if (i4RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\nbgpGetIpv6NeighborAttributes() :"
                  " nmhGetFsbgp4mpePeerBfdStatus failed\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }
    pIpBgpNeig->u1BfdStatus = (UINT1) i4BfdStatus;

    i4RetVal = Bgp4SnmphGetGrCapSupportStatus (PeerRemAddr);
    if (i4RetVal == BGP4_TRUE)
    {
        pIpBgpNeig->u1GRCapability = BGP4_GR_ADVT;
    }
    i4RetVal = Bgp4GRIsPeerGRCapable (pPeer);
    if (i4RetVal == BGP4_SUCCESS)
    {
        if (pIpBgpNeig->u1GRCapability == BGP4_GR_ADVT)
        {
            pIpBgpNeig->u1GRCapability = BGP4_GR_ADVT_RCVD;
        }
        else
        {
            pIpBgpNeig->u1GRCapability = BGP4_GR_RCVD;
        }
    }
    pIpBgpNeig->u4RestartInterval = BGP4_PEER_RESTART_INTERVAL (pPeer);
    return BGP4_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name : bgpConfigConfedId                                         */
/* CLI Command   : (a) bgp confederation identifier {autonomous-system}      */
/*               : (b) no bgp confederation identifier {autonomous-system    */
/* Description   : Command (a), while called for the first time sets the     */
/*               : ConfedId to the given value.                              */
/*               : If command (a) is called with different AS no before      */
/*               : command (b) is called, then operation fails.              */
/*               : If Command (b) is called with the same AS no as that of   */
/*               : Command (a), then Confed Id is cleared.                   */
/* Input(s)      : Bgp confed Id (pu4AsNo)                                   */
/*               : Operation to be performed (u1Set)                         */
/*               : u1Set = VAL_SET for command (a) and                       */
/*               : u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigConfedId (UINT4 u4AsNo,    /* AS number,Confed Id */
                   UINT1 u1Set /* VAL_SET/VAL_NO */ )
{
    UINT4               u4ConfedId = BGP4_INV_AS;
    UINT4               u4ErrorCode = 0;
    INT1                i1RetVal = BGP4_CLI_FAILURE;

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    nmhGetFsbgpAscConfedId (&u4ConfedId);

    /* CONFED ID already configured and same as input AS no */
    if ((u4ConfedId != BGP4_INV_AS) &&
        (u1Set == VAL_SET) && (u4AsNo == u4ConfedId))
    {
        return BGP4_SUCCESS;
    }

    /* Confederation is already not active */
    if ((u1Set == VAL_NO) && (u4ConfedId == BGP4_INV_AS))
    {
        return BGP4_CLI_ERR_CONFED_INACTIVE;
    }

    /* Mismatch between Confed id and input AS */
    if ((u4ConfedId != BGP4_INV_AS) && (u4AsNo != u4ConfedId) &&
        (u1Set == VAL_SET))
    {
        return BGP4_CLI_ERR_CONFED_ID_MISMATCH;
    }

    if (u1Set == VAL_SET)
    {
        i1RetVal = nmhTestv2FsbgpAscConfedId (&u4ErrorCode, u4AsNo);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tbgpConfigureConfedId() : Invalid Confed Id\n");
            return BGP4_CLI_ERR_INVALID_CONFED_ID;
        }
    }

    if (u1Set == VAL_SET)
    {
        i1RetVal = nmhSetFsbgpAscConfedId (u4AsNo);
    }
    else if (u1Set == VAL_NO)
    {
        i1RetVal = nmhSetFsbgpAscConfedId (0);
    }
    /* if operation is not successfull */
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME,
                  "\tbgpConfigureConfedId() : Unable to configure\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigConfedPeers                                      */
/* CLI Command   : (a) bgp confederation peers {AS} [{AS}]                   */
/*               : (b) no bgp confederation peers {AS} [{AS}]                */
/* Description   : Command (a), while called, sets the Peer AS no as Confed  */
/*               : Peer to the given value in confed Peer array.             */
/*               : If command (a) is called with same AS no before           */
/*               : command (b) is called, then such operation is ignored.    */
/*               : If Command (b) is called with the same AS no as that of   */
/*               : Command (a), then Confed Peer is removed from the array.  */
/* Input(s)      : Bgp confed Peers (pu4AsNo)                                */
/*               : Operation to be performed (u1Set)                         */
/*               : u1Set = VAL_SET for command (a) and                       */
/*               : u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigConfedPeers (UINT4 u4ConfedPeer,    /* Confed peer AS no */
                      UINT1 u1Set /* VAL_SET/VAL_NO */ )
{
    UINT4               u4ConfedPeerStatus;
    UINT4               u4ErrorCode = 0;
    INT1                i1RetVal;

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    i1RetVal = nmhValidateIndexInstanceFsbgpAscConfedPeerTable (u4ConfedPeer);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigureConfedPeers() : Invalid Confed Peer no\n");
        return BGP4_CLI_ERR_INVALID_CONFED_PEER;
    }

    nmhGetFsbgpAscConfedPeerStatus (u4ConfedPeer, (INT4 *) &u4ConfedPeerStatus);

    /* Confederation Peer is already enabled */
    if ((u4ConfedPeerStatus == BGP4_CONFED_PEER_AS_ENABLE) &&
        (u1Set == VAL_SET))
    {
        return BGP4_SUCCESS;
    }

    /* Confederation Peer is already Disabled */
    if ((u4ConfedPeerStatus == BGP4_CONFED_PEER_AS_DISABLE) &&
        (u1Set == VAL_NO))
    {
        return BGP4_SUCCESS;
    }

    if (u1Set == VAL_SET)
    {
        i1RetVal = nmhTestv2FsbgpAscConfedPeerStatus (&u4ErrorCode,
                                                      u4ConfedPeer,
                                                      BGP4_CONFED_PEER_AS_ENABLE);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                      BGP4_MOD_NAME,
                      "\tbgpConfigureConfedPeerIndex() : Invalid Confed Index\n");
            return BGP4_CLI_ERR_INVALID_CONFED_INDEX;
        }
        i1RetVal = nmhSetFsbgpAscConfedPeerStatus (u4ConfedPeer,
                                                   BGP4_CONFED_PEER_AS_ENABLE);
    }
    else if (u1Set == VAL_NO)
    {
        i1RetVal = nmhTestv2FsbgpAscConfedPeerStatus (&u4ErrorCode,
                                                      u4ConfedPeer,
                                                      BGP4_CONFED_PEER_AS_DISABLE);
        if (i1RetVal == BGP4_CLI_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                      BGP4_MOD_NAME,
                      "\tbgpConfigureConfedPeerIndex() : Invalid Confed Index\n");
            return BGP4_CLI_ERR_INVALID_CONFED_INDEX;
        }
        i1RetVal = nmhSetFsbgpAscConfedPeerStatus (u4ConfedPeer,
                                                   BGP4_CONFED_PEER_AS_DISABLE);
    }
    /* if operation is not successfull */
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME,
                  "\tbgpConfigureConfedPeer() : Unable to configure\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigBestPathMedConfed                                */
/* CLI Command   : (a) bgp bestpath med confed                               */
/*               : (b) no bgp bestpath med confed                            */
/* Description   : Command (a), while called for the first time Enables MED  */
/*               : comparison among paths learned from confed peers.         */
/*               : Command (b), while called Disables MED comparison among   */
/*               : paths learned from confed peers.                          */
/*               : BY DEFAULT disabled                                       */
/* Input(s)      : u1Set = VAL_SET for command (a) and                       */
/*               : u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigBestPathMedConfed (UINT1 u1Set /* VAL_SET/VAL_NO */ )
{
    UINT4               u4ErrorCode = 0;
    INT4                i4InputVal = 0;
    INT1                i1RetVal = 0;

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));
    if (u1Set == VAL_SET)
    {
        i4InputVal = BGP4_CONFED_COMPARE_MED_SET;
    }
    else if (u1Set == VAL_NO)
    {
        i4InputVal = BGP4_CONFED_COMPARE_MED_CLEAR;
    }

    i1RetVal = nmhTestv2FsbgpAscConfedBestPathCompareMED (&u4ErrorCode,
                                                          i4InputVal);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigBestPathMedConfed() : Invalid Input value\n");
        return BGP4_CLI_ERR_INVALID_CONFED_MED_INPUT;
    }

    i1RetVal = nmhSetFsbgpAscConfedBestPathCompareMED (i4InputVal);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME,
                  "\tbgpConfigBestPathMedConfed() : Unable to configure\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigNeighborSoftReconfigClearIpBgpAll                */
/* CLI Command   : clear ip bgp {*|ip-address |peer-group-name}              */
/*                             [soft[in|out]]                                */
/* Description   : This function is used to reset the peer session and bring */
/*                 it back dynamically. This function needs to be called     */
/*                 when (clear ip bgp {*|ip-address|peer-group-name}.        */
/*                 If peer address is not mentioned then all the peers will  */
/*                 be restarted depanding upon the peer start status.        */
/* Input(s)      : u1AllPeers - Indicates whether soft-reset needs to be     */
/*                 done for all peers or not.                                */
/*               : pu4IpAddress - Pointer to the BGP peer's IP Address for   */
/*                 which soft reset needs to be done.                        */
/*               : pu1PeerGroupName - If non NULL, pointer to the peer-grp.  */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_ALLPEER_SOFTRECONFIG_FAILURE -               */
/*                      if route-refresh configuration for all peers fails   */
/*               : BGP4_CLI_ERR_RTREF_INVALID_INPUT                          */
/*                      if input values supplied are invalid                 */
/*               : BGP4_CLI_ERR_RTREF_INVALID_INDEX                          */
/*                      if input index/keywords are invalid                  */
/*               : BGP4_CLI_ERR_RTREF_COMMIT_FAILED                          */
/*                      if sending of route-refresh message results in failure*/
/*****************************************************************************/
INT4
bgpConfigNeighborSoftReconfigClearIpBgp (UINT1 u1AllPeers,    /* AllPeers_SET or AllPeers_NO */
                                         tAddrPrefix * pIpAddress,    /* if non-NULL points to the IpAddress 
                                                                     * specified on the CLI */
                                         UINT1 *pu1PeerGroupName    /* if non-NULL points to the Peer 
                                                                     * Group name specified on the CLI */
    )
{
    tBgp4QMsg          *pQMsg = NULL;
    tAddrPrefix         InvPrefix;
    UINT1               au1TempAddress[BGP4_MAX_INET_ADDRESS_LEN];

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    /* These variable will be used once peer group is implemented. */
    UNUSED_PARAM (pu1PeerGroupName);

    BGP4_ASSERT (pIpAddress);

    Bgp4InitAddrPrefixStruct (&InvPrefix, pIpAddress->u2Afi);

    MEMSET (au1TempAddress, 0, BGP4_MAX_INET_ADDRESS_LEN);

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME,
                  "\tbgpConfigNeighborSoftReconfigClearIpBgpAll() : Unable to"
                  "trigger Clear ip bgp command\n");
        return BGP4_CLI_ERR_ALLPEER_SOFTRECONFIG_FAILURE;
    }

    if ((MEMCMP (pIpAddress->au1Address, au1TempAddress,
                 MEM_MAX_BYTES (pIpAddress->u2Afi,
                                sizeof (pIpAddress->au1Address)))) == 0)
    {
        /* do nothing as the input given by user is '*' */
    }
    else if (Bgp4SnmphGetPeerEntry (u4Context, *pIpAddress) == NULL)
    {
        CLI_SET_ERR (CLI_BGP4_PEER_NOT_ENABLED);
        BGP4_QMSG_FREE (pQMsg);
        return BGP4_CLI_ERR_ALLPEER_SOFTRECONFIG_FAILURE;
    }

    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_CLI_CLEAR_IP_BGP_EVENT;

    if (u1AllPeers == ALL_PEERS_SET)
    {
        MEMCPY (&BGP4_INPUTQ_PEER_ADDR_INFO (pQMsg),
                &InvPrefix, sizeof (tAddrPrefix));
    }
    else
    {
        MEMCPY (&BGP4_INPUTQ_PEER_ADDR_INFO (pQMsg),
                pIpAddress, sizeof (tAddrPrefix));
    }

    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
    BGP4_INPUTQ_CXT (pQMsg) = u4Context;
    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME,
                  "\tbgpConfigNeighborSoftReconfigClearIpBgpAll() : Unable to"
                  "trigger Clear ip bgp command\n");
        return BGP4_CLI_ERR_ALLPEER_SOFTRECONFIG_FAILURE;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigNeighborSoftReconfigClearIpBgpSoftIn             */
/* CLI Command   : clear ip bgp {*|ip-address |peer-group-name}              */
/*                             [soft[in|out]]                                */
/* Description   : This function is used to trigger inbound routing table    */
/*                 updates dynamically.                                      */
/* Input(s)      : u1AllPeers - Indicates whether soft-reconfig needs to be  */
/*                 done for all peers or not.                                */
/*               : pu4IpAddress - Pointer to the BGP peer's IP Address to    */
/*                 whom the route-refresh message needs to be sent           */
/*               : pu1PeerGroupName - If non NULL, pointer to the peer-grp.  */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_ALLPEER_SOFTRECONFIG_FAILURE -               */
/*                      if route-refresh configuration for all peers fails   */
/*               : BGP4_CLI_ERR_RTREF_INVALID_INPUT                          */
/*                      if input values supplied are invalid                 */
/*               : BGP4_CLI_ERR_RTREF_INVALID_INDEX                          */
/*                      if input index/keywords are invalid                  */
/*               : BGP4_CLI_ERR_RTREF_COMMIT_FAILED                          */
/*                      if sending of route-refresh message results in failure*/
/*****************************************************************************/
INT4
bgpConfigNeighborSoftReconfigClearIpBgpSoftIn (UINT1 u1AllPeers,    /* AllPeers_SET or AllPeers_NO */
                                               UINT4 u4AsafiMask,    /* to identify ipv4/vpnv4 */
                                               tAddrPrefix * pIpAddress,    /* if non-NULL points to the 
                                                                             * IpAddress specified on the CLI*/
                                               UINT1 *pu1PeerGroupName,    /* if non-NULL points to the Peer 
                                                                         * Group name specified on the CLI */
                                               UINT1 u1OrfFlag)
{
    tSNMP_OCTET_STRING_TYPE InboundAddr;
    UINT4               u4ErrorCode;
    UINT2               u2Afi = 0;
    UINT2               u2Safi = 0;
    INT1                i1Status;
    UINT1               au1IpAddrOctetList[BGP4_PEER_OCTET_LIST_LEN];

    UNUSED_PARAM (pu1PeerGroupName);

    MEMSET (&InboundAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (pIpAddress->u2Afi == BGP4_INET_AFI_IPV4)
    {
        InboundAddr.i4_Length = sizeof (UINT4);
    }
#ifdef BGP4_IPV6_WANTED
    else
    {
        InboundAddr.i4_Length = BGP4_IPV6_PREFIX_LEN;
    }
#endif
    Bgp4GetAfiSafiFromMask (u4AsafiMask, &u2Afi, &u2Safi);
    MEMSET (au1IpAddrOctetList, 0, BGP4_PEER_OCTET_LIST_LEN);
    InboundAddr.pu1_OctetList = au1IpAddrOctetList;

    if (InboundAddr.pu1_OctetList == NULL)
    {
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }
    if (u1AllPeers == ALL_PEERS_SET)
    {
        MEMSET ((InboundAddr.pu1_OctetList), 0, InboundAddr.i4_Length);
        if (u1OrfFlag == BGP4_TRUE)
        {
            i1Status =
                nmhSetFsbgp4mpeRtRefreshInboundPrefixFilter (pIpAddress->u2Afi,
                                                             &InboundAddr, 0, 0,
                                                             BGP4_ORF_REQ_SET);
        }
        else
        {
            /* Apply Soft re-config for all peers */
            i1Status =
                nmhSetFsbgp4mpeRtRefreshInboundRequest (pIpAddress->u2Afi,
                                                        &InboundAddr, 0, 0,
                                                        BGP4_RTREF_REQ_SET);
        }

        if (i1Status == SNMP_FAILURE)
        {
            return BGP4_CLI_ERR_ALLPEER_SOFTRECONFIG_FAILURE;
        }
        else
        {
            return BGP4_SUCCESS;
        }
    }
    else if (u1AllPeers == ALL_PEERS_NO)
    {
        switch (pIpAddress->u2Afi)
        {
            case BGP4_INET_AFI_IPV4:
            case BGP4_INET_AFI_IPV6:
                MEMCPY (InboundAddr.pu1_OctetList,
                        pIpAddress->au1Address, InboundAddr.i4_Length);
                if (u1OrfFlag == BGP4_TRUE)
                {
                    i1Status =
                        nmhTestv2Fsbgp4mpeRtRefreshInboundPrefixFilter
                        (&u4ErrorCode, pIpAddress->u2Afi, &InboundAddr,
                         (INT4) u2Afi, (INT4) u2Safi, BGP4_ORF_REQ_SET);
                    if (i1Status == SNMP_FAILURE)
                    {
                        return BGP4_CLI_ERR_RTREF_INVALID_INPUT;
                    }

                    i1Status =
                        nmhSetFsbgp4mpeRtRefreshInboundPrefixFilter
                        (pIpAddress->u2Afi, &InboundAddr, (INT4) u2Afi,
                         (INT4) u2Safi, BGP4_ORF_REQ_SET);
                    if (i1Status == SNMP_FAILURE)
                    {
                        return BGP4_CLI_ERR_RTREF_COMMIT_FAILED;
                    }

                }
                else
                {
                    i1Status =
                        nmhTestv2Fsbgp4mpeRtRefreshInboundRequest (&u4ErrorCode,
                                                                   pIpAddress->
                                                                   u2Afi,
                                                                   &InboundAddr,
                                                                   (INT4) u2Afi,
                                                                   (INT4)
                                                                   u2Safi,
                                                                   BGP4_RTREF_REQ_SET);
                    if (i1Status == SNMP_FAILURE)
                    {
                        return BGP4_CLI_ERR_RTREF_INVALID_INPUT;
                    }

                    i1Status =
                        nmhSetFsbgp4mpeRtRefreshInboundRequest (pIpAddress->
                                                                u2Afi,
                                                                &InboundAddr,
                                                                (INT4) u2Afi,
                                                                (INT4) u2Safi,
                                                                BGP4_RTREF_REQ_SET);
                    if (i1Status == SNMP_FAILURE)
                    {
                        return BGP4_CLI_ERR_RTREF_COMMIT_FAILED;
                    }

                }
                break;

            default:
                return BGP4_CLI_ERR_RTREF_INVALID_INDEX;
        }
    }
    else
    {
        return BGP4_CLI_ERR_ALLPEER_SOFTRECONFIG_FAILURE;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigNeighborSoftReconfigClearIpBgpSoftOut            */
/* CLI Command   : clear ip bgp {*|ip-address |peer-group-name}              */
/*                             [soft[in|out]]                                */
/* Description   : This function is used to trigger outbound routing table   */
/*                 updates dynamically.                                      */
/* Input(s)      : u1AllPeers - Indicates whether outbound soft-reconfig     */
/*                 needs to be done for all peers or not.                    */
/*               : pu4IpAddress - Pointer to the BGP peer's IP Address for   */
/*                 which outbound soft-reconfig needs to be performed        */
/*               : pu1PeerGroupName - If non NULL, pointer to the peer-grp.  */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_ALLPEER_SOFTRECONFIG_FAILURE -               */
/*                      if route-refresh configuration for all peers fails   */
/*                 BGP4_CLI_ERR_SOFTRECONFIG_OUTBOUND_INVALID_INPUT;         */
/*                      if input values supplied are invalid                 */
/*               : BGP4_CLI_ERR_SOFTRECONFIG_OUTBOUND_INVALID_INDEX          */
/*                      if input index/keywords are invalid                  */
/*               : BGP4_CLI_ERR_SOFTRECONFIG_OUTBOUND_COMMIT_FAILED          */
/*                      if soft-reconfig outbound process results in failure */
/*****************************************************************************/
INT4
bgpConfigNeighborSoftReconfigClearIpBgpSoftOut (UINT1 u1AllPeers,    /* AllPeers_SET or AllPeers_NO */
                                                UINT4 u4AsafiMask,    /* Afisafi mask to indicate the family */
                                                tAddrPrefix * pIpAddress,    /* if non-NULL points to the 
                                                                             * IpAddress specified on 
                                                                             * the CLI */
                                                UINT1 *pu1PeerGroupName    /* if non-NULL points to the 
                                                                         * Peer Group name 
                                                                         * specified on the CLI */
    )
{
    tSNMP_OCTET_STRING_TYPE OutboundAddr;
    UINT4               u4ErrorCode;
    UINT2               u2Afi = 0;
    UINT2               u2Safi = 0;
    INT1                i1Status;
    UINT1               au1IpAddrOctetList[BGP4_PEER_OCTET_LIST_LEN];

    UNUSED_PARAM (pu1PeerGroupName);

    MEMSET (&OutboundAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (pIpAddress->u2Afi == BGP4_INET_AFI_IPV4)
    {
        OutboundAddr.i4_Length = sizeof (UINT4);
    }
#ifdef BGP4_IPV6_WANTED
    else
    {
        OutboundAddr.i4_Length = BGP4_IPV6_PREFIX_LEN;
    }
#endif
    Bgp4GetAfiSafiFromMask (u4AsafiMask, &u2Afi, &u2Safi);
    MEMSET (au1IpAddrOctetList, 0, BGP4_PEER_OCTET_LIST_LEN);
    OutboundAddr.pu1_OctetList = au1IpAddrOctetList;
    if (u1AllPeers == ALL_PEERS_SET)
    {
        /* Apply Soft re-config for all peers */
        MEMSET ((OutboundAddr.pu1_OctetList), 0, OutboundAddr.i4_Length);

        i1Status =
            nmhTestv2Fsbgp4mpeSoftReconfigOutboundRequest (&u4ErrorCode,
                                                           pIpAddress->u2Afi,
                                                           &OutboundAddr,
                                                           0, 0,
                                                           BGP4_RTREF_REQ_SET);
        if (i1Status == SNMP_FAILURE)
        {
            return BGP4_CLI_ERR_ALLPEER_SOFTRECONFIG_FAILURE;
        }

        i1Status =
            nmhSetFsbgp4mpeSoftReconfigOutboundRequest (pIpAddress->u2Afi,
                                                        &OutboundAddr,
                                                        pIpAddress->u2Afi,
                                                        BGP4_INET_SAFI_UNICAST,
                                                        BGP4_RTREF_REQ_SET);

        if (i1Status == SNMP_FAILURE)
        {
            return BGP4_CLI_ERR_ALLPEER_SOFTRECONFIG_FAILURE;
        }
        else
        {
            return BGP4_SUCCESS;
        }
    }
    else if (u1AllPeers == ALL_PEERS_NO)
    {
        /*  
         * currently only IPv4 is supported
         */
        switch (pIpAddress->u2Afi)
        {
            case BGP4_INET_AFI_IPV4:
            case BGP4_INET_AFI_IPV6:
                MEMCPY ((OutboundAddr.pu1_OctetList),
                        pIpAddress->au1Address, OutboundAddr.i4_Length);
                i1Status =
                    nmhTestv2Fsbgp4mpeSoftReconfigOutboundRequest (&u4ErrorCode,
                                                                   pIpAddress->
                                                                   u2Afi,
                                                                   &OutboundAddr,
                                                                   (INT4) u2Afi,
                                                                   (INT4)
                                                                   u2Safi,
                                                                   BGP4_RTREF_REQ_SET);
                if (i1Status == SNMP_FAILURE)
                {
                    return BGP4_CLI_ERR_SOFTRECONFIG_OUTBOUND_INVALID_INPUT;
                }

                i1Status =
                    nmhSetFsbgp4mpeSoftReconfigOutboundRequest (pIpAddress->
                                                                u2Afi,
                                                                &OutboundAddr,
                                                                (INT4) u2Afi,
                                                                (INT4) u2Safi,
                                                                BGP4_RTREF_REQ_SET);
                if (i1Status == SNMP_FAILURE)
                {
                    return BGP4_CLI_ERR_SOFTRECONFIG_OUTBOUND_COMMIT_FAILED;
                }
                break;
            default:
                return BGP4_CLI_ERR_SOFTRECONFIG_OUTBOUND_INVALID_INDEX;
        }
    }
    else
    {
        return BGP4_CLI_ERR_ALLPEER_SOFTRECONFIG_FAILURE;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigNeighborPassword                                 */
/* CLI Command   : (a) neighbor {ip-address|peer-group-name}                 */
/*                          password string                                  */
/*               : (b) no neighbor {ip-address|peer-group-name}              */
/*                          password                                         */
/* Description   : Command (a) enables the FutureBGP4 Speaker to configure a */
/* Input(s)      : IP address of the Peer (pu1IpAddress)                     */
/*               : Group Name of the Peer (pu1PeerGroupName)                 */
/*               : Password to be configured for the peer                    */
/*               : Operation to be performed (u1Set)                         */
/*               : u1Set = VAL_SET for command (a) and                       */
/*               : u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_INVALID_PASSWORD_OR_PEERADDRESS -            */
/*                      if operation fails, where XX represents              */
/*               : the appropriate reason for the failure                    */
/* Note          : In this release the Multihop support is provided only per */
/*               : peer basic. This feauture will be extended for Peer Group */
/*               : in subsequent releases. For this release pu1PeerGroupName */
/*               : will be NULL.                                             */
/*****************************************************************************/
INT4
bgpConfigNeighborPassword (tAddrPrefix * pIpAddress,    /* if non-NULL points to the 
                                                         * IpAddress specified on the CLI 
                                                         */
                           UINT1 *pu1PeerGroupName,    /* if non-NULL points to the Peer Group 
                                                     * name specified on the CLI */
                           UINT1 *pu1Password,    /* pointer to password string */
                           UINT1 u1PwdLen, UINT1 u1Set    /* VAL_SET or VAL_NO */
    )
{
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE Passwd;
    tSNMP_OCTET_STRING_TYPE *TcpPwd;
    UINT4               u4ErrorCode;
    INT1                i1Status;
    UINT1               au1PeerOctetList[BGP4_PEER_OCTET_LIST_LEN];

    UNUSED_PARAM (pu1PeerGroupName);

    MEMSET (&PeerAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (pIpAddress->u2Afi == BGP4_INET_AFI_IPV4)
    {
        PeerAddr.i4_Length = sizeof (UINT4);
    }
#ifdef BGP4_IPV6_WANTED
    else
    {
        PeerAddr.i4_Length = BGP4_IPV6_PREFIX_LEN;
    }
#endif
    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));
    MEMSET (au1PeerOctetList, 0, BGP4_PEER_OCTET_LIST_LEN);
    PeerAddr.pu1_OctetList = au1PeerOctetList;
    switch (pIpAddress->u2Afi)
    {
        case BGP4_INET_AFI_IPV4:
        case BGP4_INET_AFI_IPV6:
            MEMCPY ((PeerAddr.pu1_OctetList),
                    pIpAddress->au1Address, PeerAddr.i4_Length);
            Passwd.pu1_OctetList = pu1Password;
            Passwd.i4_Length = u1PwdLen;
            TcpPwd = allocmem_octetstring (BGP4_TCPMD5_PWD_SIZE);
            if (TcpPwd == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                          "\tbgpConfigNeighborPassword() : Unable to allocate the"
                          "Buffer for PASSWORD. !!!\n");
                return BGP4_CLI_FAILURE;
            }

            if (u1Set == VAL_SET)
            {
                i1Status = nmhGetFsbgp4TCPMD5AuthPassword (pIpAddress->u2Afi,
                                                           &PeerAddr, TcpPwd);
                if ((i1Status == SNMP_SUCCESS) && (TcpPwd->i4_Length != 0))
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                              "\tbgpConfigNeighborPassword() : TCP MD5 "
                              "PASSWORD already Set. !!!\n");
                    free_octetstring (TcpPwd);
                    return BGP4_CLI_ERR_TCPMD5_PASSWORD_ALREADY_EXISTS;
                }

                i1Status =
                    nmhTestv2Fsbgp4TCPMD5AuthPwdSet (&u4ErrorCode,
                                                     pIpAddress->u2Afi,
                                                     &PeerAddr,
                                                     BGP4_TCPMD5_PWD_SET);
                if (i1Status == SNMP_FAILURE)
                {
                    free_octetstring (TcpPwd);
                    return BGP4_CLI_ERR_TCPMD5_INVALID_INDEX;
                }
                i1Status =
                    nmhSetFsbgp4TCPMD5AuthPwdSet (pIpAddress->u2Afi,
                                                  &PeerAddr,
                                                  BGP4_TCPMD5_PWD_SET);
                if (i1Status == SNMP_FAILURE)
                {
                    free_octetstring (TcpPwd);
                    return BGP4_CLI_ERR_TCPMD5_COMMIT_FAILED;
                }
                i1Status =
                    nmhTestv2Fsbgp4TCPMD5AuthPassword (&u4ErrorCode,
                                                       pIpAddress->u2Afi,
                                                       &PeerAddr, &Passwd);

                if (i1Status == SNMP_FAILURE)
                {
                    free_octetstring (TcpPwd);
                    return BGP4_CLI_ERR_INVALID_PASSWORD_OR_PEERADDRESS;
                }

                i1Status =
                    nmhSetFsbgp4TCPMD5AuthPassword (pIpAddress->u2Afi,
                                                    &PeerAddr, &Passwd);
                if (i1Status == SNMP_FAILURE)
                {
                    free_octetstring (TcpPwd);
                    return BGP4_CLI_ERR_TCPMD5_COMMIT_FAILED;
                }
            }
            else
            {
                i1Status =
                    nmhTestv2Fsbgp4TCPMD5AuthPwdSet (&u4ErrorCode,
                                                     pIpAddress->u2Afi,
                                                     &PeerAddr,
                                                     BGP4_TCPMD5_PWD_RESET);
                if (i1Status == SNMP_FAILURE)
                {
                    free_octetstring (TcpPwd);
                    return BGP4_CLI_ERR_TCPMD5_INVALID_INDEX;
                }
                i1Status =
                    nmhSetFsbgp4TCPMD5AuthPwdSet (pIpAddress->u2Afi,
                                                  &PeerAddr,
                                                  BGP4_TCPMD5_PWD_RESET);
                if (i1Status == SNMP_FAILURE)
                {
                    free_octetstring (TcpPwd);
                    return BGP4_CLI_ERR_TCPMD5_COMMIT_FAILED;
                }
            }
            free_octetstring (TcpPwd);
            break;
        default:
            return BGP4_CLI_ERR_INVALID_PASSWORD_OR_PEERADDRESS;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigAddressFamily                                    */
/* CLI Command   : (a) address-family [ipv4|ipv6]                            */
/*               : (b) no address-family {ipv4|ipv6}                         */
/* Description   : Command (a) - enables address-family                      */
/*               : Command (b) - disables specified address-family           */
/* Input(s)      : Address family to be enabled                              */
/*               : Operation to be performed (u1Set)                         */
/*               : u1Set = VAL_SET for command (a) and                       */
/*               : u1Set = VAL_NO for command (b)                            */
/*               : VRF name and length                                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/* Note          : In this release the configuration support is provided only*/
/*               : for peers. This feauture will be extended for Peer Group  */
/*               : in subsequent releases. For this release pu1PeerGroupName */
/*               : will be NULL.                                             */
/*****************************************************************************/
INT4
bgpConfigAddressFamily (UINT1 *pu1VrfName, INT4 i4VrfNameLen, UINT2 u2Afi,    /* Address Family */
                        UINT1 u1Set    /* VAL_SET/VAL_NO */
    )
{
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    UNUSED_PARAM (pu1VrfName);
    UNUSED_PARAM (i4VrfNameLen);
    if (u1Set == VAL_SET)
    {
        switch (u2Afi)
        {
            case BGP4_CLI_AFI_IPV4:
                BGP4_IPV4_AFI_FLAG (u4Context) = BGP4_TRUE;
                break;
            case BGP4_CLI_AFI_IPV6:
                BGP4_IPV6_AFI_FLAG (u4Context) = BGP4_TRUE;
                break;
#ifdef L3VPN
            case BGP4_CLI_AFI_VPNV4:
                break;
#endif
            default:
                break;
        }
    }
    else if (u1Set == VAL_NO)
    {
        switch (u2Afi)
        {
            case BGP4_CLI_AFI_IPV4:
                BGP4_IPV4_AFI_FLAG (u4Context) = BGP4_FALSE;
                /* Delete all v4 neighbors */
                Bgp4SnmphDeletePeers (BGP4_INET_AFI_IPV4);
                break;
            case BGP4_CLI_AFI_IPV6:
                BGP4_IPV6_AFI_FLAG (u4Context) = BGP4_FALSE;
                /* Delete all v6 neighbors */
                Bgp4SnmphDeletePeers (BGP4_INET_AFI_IPV6);
                break;
#ifdef L3VPN
            case BGP4_CLI_AFI_VPNV4:
                Bgp4SnmphDeactivateCapabilyForPeers (BGP4_INET_AFI_IPV4,
                                                     BGP4_INET_SAFI_VPNV4_UNICAST);
                break;
#endif
            default:
                break;
        }
    }
    return BGP4_SUCCESS;
}

#ifdef L3VPN_TEST_WANTED
/*****************************************************************************/
/* Function Name : bgpConfigVrfRouteTarget                                   */
/* CLI Command   : (a) vrf <vrf-name> route-target {import|export|both| 
 *                      <route-target-value>                                 */
/*               : (b) no vrf <vrf-name> route-target {import|export|both}
 *                      <route-target-value>                                 */
/* Description   : Command (a) -configures route target                      */
/*               : Command (b) - disables route target                       */
/* Input(s)      : Vrf Name, RouteTarget type, Route Target                  */
/*               : Operation to be performed (u1Set)                         */
/*               : u1Set = VAL_SET for command (a) and                       */
/*               : u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigVrfRouteTarget (INT4 i4RouteTargetType,
                         tSNMP_OCTET_STRING_TYPE * pRouteTargetVal, UINT1 u1Set)
{
    UINT1               au1CliPrompt[MAX_PROMPT_LEN];
    tSNMP_OCTET_STRING_TYPE *VrfName;
    tBgp4VrfName        VpnVrfName;
    tVrfSpecInfo       *pVrfInfo = NULL;
    UINT4               u4ErrorCode;
    INT4                i4RetVal;
    INT1                i1RetSts;
    UINT1               u1VrfOctetList;
    i4RetVal = Bgp4CliGetCurPrompt (au1CliPrompt);
    if (i4RetVal == BGP4_FAILURE)
    {
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    if (STRNCMP (au1CliPrompt, BGP4_IPV4_VRF_FAMLY_MODE,
                 BGP4_IPV4_VRF_FAMLY_MODE_LEN) != 0)
    {
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }
    u1VrfOctetList = (STRLEN (au1CliPrompt)) - BGP4_IPV4_VRF_FAMLY_MODE_LEN;
    VrfName = allocmem_octetstring (u1VrfOctetList);
    VrfName->i4_Length = u1VrfOctetList;
    if (VrfName == NULL)
    {
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    MEMCPY (VrfName->pu1_OctetList,
            (au1CliPrompt + BGP4_IPV4_VRF_FAMLY_MODE_LEN), VrfName->i4_Length);
    VpnVrfName.i4Length = VrfName->i4_Length;
    MEMCPY (VpnVrfName.au1VrfName, VrfName->pu1_OctetList, VrfName->i4_Length);
    Bgp4Vpn4GetVrfInfoFromName (&VpnVrfName, &pVrfInfo);
    if (pVrfInfo == NULL)
    {
        free_octetstring (VrfName);
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    switch (u1Set)
    {
        case VAL_SET:
            i1RetSts =
                nmhTestv2Fsbgp4MplsVpnVrfRouteTargetRowStatus (&u4ErrorCode,
                                                               VrfName,
                                                               i4RouteTargetType,
                                                               pRouteTargetVal,
                                                               CREATE_AND_GO);
            if (i1RetSts != SNMP_SUCCESS)
            {
                free_octetstring (VrfName);
                return BGP4_CLI_ERR_VRF_INVALID_INPUT;
            }
            i1RetSts =
                nmhSetFsbgp4MplsVpnVrfRouteTargetRowStatus (VrfName,
                                                            i4RouteTargetType,
                                                            pRouteTargetVal,
                                                            CREATE_AND_GO);
            if (i1RetSts != SNMP_SUCCESS)
            {
                free_octetstring (VrfName);
                return BGP4_CLI_ERR_VRF_ROUTE_TARGET_CREATE_FAIL;
            }
            break;
        case VAL_NO:
            i1RetSts =
                nmhTestv2Fsbgp4MplsVpnVrfRouteTargetRowStatus (&u4ErrorCode,
                                                               VrfName,
                                                               i4RouteTargetType,
                                                               pRouteTargetVal,
                                                               DESTROY);
            if (i1RetSts != SNMP_SUCCESS)
            {
                free_octetstring (VrfName);
                return BGP4_CLI_ERR_GENERAL_ERROR;
            }
            i1RetSts =
                nmhSetFsbgp4MplsVpnVrfRouteTargetRowStatus (VrfName,
                                                            i4RouteTargetType,
                                                            pRouteTargetVal,
                                                            DESTROY);
            if (i1RetSts != SNMP_SUCCESS)
            {
                free_octetstring (VrfName);
                return BGP4_CLI_ERR_VRF_ROUTE_TARGET_DELETE_FAIL;
            }

            break;
    }
    free_octetstring (VrfName);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigNeighborCERouteTargetAdvt*/
/* CLI Command   : (a) neighbor <ip-address | peergroup> route-target       */
/*                      <send|recv|both>                                    */
/*               : (b) no neighbor <ip-address | peergroup> route-target    */
/*                      <send|recv|both>                                    */
/* Description   : Command (a) -configures  CE Peer's rotue Target Status   */
/*               : Command (b) - disables route target status               */
/* Input(s)      :Peer address, Route Target Advertisement Status            */
/*               : Operation to be performed (u1Set)                         */
/*               : u1Set = VAL_SET for command (a) and                       */
/*               : u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigNeighborCERouteTargetAdvt (tAddrPrefix * pIpAddress,
                                    UINT1 *pu1PeerGroupName, UINT1 u1Set)
{
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    UINT4               u4ErrorCode = 0;
    INT4                i4CEPeerRTAdvtSts;
    INT1                i1RetVal;
    UINT1               au1PeerOctetList[BGP4_PEER_OCTET_LIST_LEN];

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));
    BGP4_ASSERT ((pIpAddress == NULL) || (pu1PeerGroupName == NULL));

    MEMSET (au1PeerOctetList, 0, BGP4_PEER_OCTET_LIST_LEN);
    PeerAddr.pu1_OctetList = au1PeerOctetList;
    if (PeerAddr.pu1_OctetList == NULL)
    {
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }
    MEMCPY (PeerAddr.pu1_OctetList, pIpAddress->au1Address,
            pIpAddress->u2AddressLen);
    PeerAddr.i4_Length = pIpAddress->u2AddressLen;

    i1RetVal = nmhValidateIndexInstanceFsbgp4MpePeerExtTable (pIpAddress->u2Afi,
                                                              &PeerAddr);
    if (i1RetVal == SNMP_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigNeighborRemoteAs() : Invalid Peer ADDR\n");
        return BGP4_CLI_ERR_INV_PEER_ADDR;
    }
    if (u1Set == VAL_SET)
    {
        i4CEPeerRTAdvtSts = BGP4_VPN4_CE_RT_SEND;
    }
    else
    {
        i4CEPeerRTAdvtSts = BGP4_VPN4_CE_RT_DONOTSEND;
    }

    i1RetVal = nmhTestv2Fsbgp4mpePeerExtMplsVpnCERouteTargetAdvt (&u4ErrorCode,
                                                                  pIpAddress->
                                                                  u2Afi,
                                                                  &PeerAddr,
                                                                  i4CEPeerRTAdvtSts);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigNeighborCERouteTargetAdvt: Invalid Input\n");
        return BGP4_CLI_ERR_PEER_INVALID_CE_RT_ADVT;
    }

    i1RetVal =
        nmhSetFsbgp4mpePeerExtMplsVpnCERouteTargetAdvt (pIpAddress->u2Afi,
                                                        &PeerAddr,
                                                        i4CEPeerRTAdvtSts);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigNeighborCERouteTargetAdvt: Unable to set "
                  "Route Target Advt for the Peer\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : bgpConfigNeighborSiteOfOrigin                            */
/* CLI Command   : (a) neighbor <ip-address > site-of-origin                */
/*             <random_str>                         */
/*               : (b) no neighbor <ip-address > site-of-origin             */
/*                       <random_str>                                        */
/* Description   : Command (a) -configures  Site of Origin for a CE Peer     */
/*               : Command (b) - Resets Site Of Origin                       */
/* Input(s)      :Peer Address                             */
/*               : Operation to be performed (u1Set)                         */
/*               : u1Set = VAL_SET for command (a) and                       */
/*               : u1Set = VAL_NO for command (b)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLI_ERR_XX - if operation fails, where XX represents */
/*               : the appropriate reason for the failure                    */
/*****************************************************************************/
INT4
bgpConfigNeighborSiteOfOrigin (tAddrPrefix * pIpAddress,
                               UINT1 *pu1SiteOfOrigin, UINT1 u1Set)
{
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE SOOExtCom;
    UINT4               u4ErrorCode = 0;
    INT1                i1RetVal;
    INT4                i4Len;
    UINT1               au1PeerOctetList[BGP4_PEER_OCTET_LIST_LEN];
    UINT1               au1ExtComOctetList[BGP4_EXT_COM_OCTET_LIST_LEN];

    BGP4_ASSERT ((u1Set == VAL_SET) || (u1Set == VAL_NO));

    if (pIpAddress->u2AddressLen > BGP4_IPV4_PREFIX_LEN)
    {
        return BGP4_CLI_ERR_INVALID_ADDR_FAMILY;
    }
    MEMSET (au1PeerOctetList, 0, BGP4_PEER_OCTET_LIST_LEN);
    PeerAddr.pu1_OctetList = au1PeerOctetList;;
    MEMCPY (PeerAddr.pu1_OctetList, pIpAddress->au1Address,
            pIpAddress->u2AddressLen);
    PeerAddr.i4_Length = pIpAddress->u2AddressLen;

    i4Len = STRLEN (pu1SiteOfOrigin);

    MEMSET (au1ExtComOctetList, 0, BGP4_EXT_COM_OCTET_LIST_LEN);
    SOOExtCom.pu1_OctetList = au1ExtComOctetList;
    if (u1Set == VAL_SET)
    {
        MEMCPY (SOOExtCom.pu1_OctetList, pu1SiteOfOrigin, i4Len);
    }
    else
    {
        MEMSET (SOOExtCom.pu1_OctetList, 0, i4Len);
    }
    SOOExtCom.i4_Length = i4Len;

    i1RetVal = nmhValidateIndexInstanceFsbgp4MpePeerExtTable (pIpAddress->u2Afi,
                                                              &PeerAddr);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigNeighborSiteOfOrigin() : Invalid Peer ADDR\n");
        return BGP4_CLI_ERR_INV_PEER_ADDR;
    }
    i1RetVal = nmhTestv2Fsbgp4mpePeerExtMplsVpnCESiteOfOrigin (&u4ErrorCode,
                                                               pIpAddress->
                                                               u2Afi, &PeerAddr,
                                                               &SOOExtCom);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigNeighborSiteOfOrigin: Invalid Input\n");
        return BGP4_CLI_ERR_PEER_INVALID_SOO;
    }

    i1RetVal =
        nmhSetFsbgp4mpePeerExtMplsVpnCESiteOfOrigin (pIpAddress->u2Afi,
                                                     &PeerAddr, &SOOExtCom);
    if (i1RetVal == BGP4_CLI_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tbgpConfigNeighborSiteOfOrigin : Unable to set "
                  "Peer Role\n");
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }
    nmhGetFsbgp4mpePeerExtMplsVpnCESiteOfOrigin (pIpAddress->u2Afi,
                                                 &PeerAddr, &SOOExtCom);

    return BGP4_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name : Bgp4GetAggregateDetails                                   */
/* CLI Command   : (a) sh ip bgp                                             */
/* Description   : Command (a) -To show bgp routes                           */
/* Input(s)      :index of the aggregate table                               */
/* Output(s)     : Structure where aggregate details are to be stored        */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_FAILURE - if operation fails                         */
/*****************************************************************************/

INT4
Bgp4GetAggregateDetails (tBgp4AggrDetails * pBgp4Aggrdetails, INT4 i4AggrIndex)
{
    tBgp4AggrEntry     *pAggrEntry[BGP4_MAX_AGGR_ENTRIES];
    *pAggrEntry = BGP4_AGGRENTRY;
    if (pBgp4Aggrdetails != NULL)
    {
        /* to get the aggregated route */
        pBgp4Aggrdetails->pAggregatedRoute =
            BGP4_AGGRENTRY_AGGR_ROUTE (i4AggrIndex - 1);
        /* to get the pBgpInfo */
        if (BGP4_AGGRENTRY_AGGR_ROUTE (i4AggrIndex - 1) != NULL)
        {
            pBgp4Aggrdetails->pBgpInfo =
                BGP4_RT_BGP_INFO (pBgp4Aggrdetails->pAggregatedRoute);
        }
        else
        {
            return BGP4_FAILURE;
        }
    }

    return BGP4_SUCCESS;
}

#endif /* BGCLIPKG_C */
