/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgporf.c,v 1.7 2017/09/15 06:19:55 siva Exp $
 *
 * Description: This file contains functions related to BGP-ORF feature
 *
 *******************************************************************/
#ifndef _BGPORF_C
#define _BGPORF_C

#include "bgp4com.h"

/*****************************************************************************/
/*  Function Name   :   Bgp4OrfMsgRcvdHandler                                */
/*                                                                           */
/*  Description     :   This function process the received ORF message and   */
/*                      updates the ORF filter data base accordingly.        */
/*                      If when-To-Refrsh field is set to IMMEDIATE,         */
/*                      the it will send the RIB out to the peer             */
/*                                                                           */
/*  Input(s)        :   pPeerInfo -                                          */
/*                         Pointer to the peer information from which the    */
/*                         route refresh message is received.                */
/*                      pu1OrfMsgBuf -                                       */
/*                         Pointer to the ORF message received from peer     */
/*                      i4BufLen - ORF message length                        */
/*                      u2Afi    - Address family identifier                 */
/*                      u1Safi   - Subsequent Address family identifier      */
/*                                                                           */
/*  Output(s)       :   None                                                 */
/*                                                                           */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*                                                                           */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  BGP4_SUCCESS, when ORF msg processing is  */
/*                                 successful.                               */
/*                              :  BGP4_FAILURE, when there is any failure   */
/*****************************************************************************/
INT4
Bgp4OrfMsgRcvdHandler (tBgp4PeerEntry * pPeerInfo, UINT1 *pu1OrfMsgBuf,
                       INT4 i4BufLen, UINT2 u2Afi, UINT1 u1Safi)
{
    tBgp4OrfEntry       OrfEntry;
    UINT4               u4TmpAddr = 0;
    UINT4               u4Addr = 0;
    INT4                i4OrfLen = 0;
    INT4                i4TmpLen = 0;
    UINT1               u1OrfType = 0;
    UINT1               u1WhenToRefresh = 0;
    UINT1               u1ActionMatch = 0;
    UINT1               u1Action = 0;
    UINT1               u1PrefixLen = 0;

    if (i4BufLen <=
        BGP4_ORF_WHEN_TO_REF_LEN + BGP4_ORF_TYPE_LEN + BGP4_ORF_LEN_FIELD)
    {
        return BGP4_FAILURE;
    }

    /* Extract When-To-refresh field from the rcvd ORF message */
    u1WhenToRefresh = *pu1OrfMsgBuf;
    pu1OrfMsgBuf += BGP4_ORF_WHEN_TO_REF_LEN;
    i4BufLen -= BGP4_ORF_WHEN_TO_REF_LEN;

    if ((u1WhenToRefresh != BGP4_ORF_IMMEDIATE) &&
        (u1WhenToRefresh != BGP4_ORF_DEFER))
    {
        return BGP4_FAILURE;
    }

    while (i4BufLen > 0)
    {
        /* Extract ORF type from the rcvd ORF message */
        u1OrfType = *pu1OrfMsgBuf;
        pu1OrfMsgBuf += BGP4_ORF_TYPE_LEN;
        i4BufLen -= BGP4_ORF_TYPE_LEN;

        /* Extract ORF length from the rcvd ORF message */
        PTR_FETCH2 (i4OrfLen, pu1OrfMsgBuf);
        pu1OrfMsgBuf += BGP4_ORF_LEN_FIELD;
        i4BufLen -= BGP4_ORF_LEN_FIELD;

        if (i4OrfLen == 0)
        {
            return BGP4_FAILURE;
        }
        if (u1OrfType == BGP_ORF_TYPE_ADDRESS_PREFIX)
        {
            i4TmpLen = i4OrfLen;
            while (i4TmpLen > 0)
            {
                MEMSET (&OrfEntry, 0, sizeof (tBgp4OrfEntry));

                MEMCPY (&OrfEntry.PeerAddr,
                        &(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                        sizeof (tAddrPrefix));
                OrfEntry.u4CxtId = BGP4_PEER_CXT_ID (pPeerInfo);
                OrfEntry.u1OrfType = u1OrfType;
                OrfEntry.u2Afi = u2Afi;
                OrfEntry.u1Safi = u1Safi;

                /* Extract the byte containing ACTION and MATCH field */
                u1ActionMatch = *pu1OrfMsgBuf;
                pu1OrfMsgBuf += BGP4_ORF_ACTION_MATCH_LEN;

                u1Action = (u1ActionMatch & BGP4_ORF_ACTION_MASK) >> 6;
                OrfEntry.u1Match =
                    (UINT1) ((u1ActionMatch & BGP4_ORF_MATCH_MASK) >> 5);

                /* If the ACTION is not valid return failure */
                if ((u1Action != BGP4_ORF_ADD) && (u1Action != BGP4_ORF_REMOVE)
                    && (u1Action != BGP4_ORF_REMOVE_ALL))
                {
                    BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                              BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                              "\tInvalid action received in the ORF message\n");
                    return BGP4_FAILURE;
                }

                i4TmpLen -= BGP4_ORF_ACTION_MATCH_LEN;

                /* If the action is REMOVE_ALL, then remove all the ORF entries received from 
                 * the neighbor with specific AFI, SAFI, ORF-Type
                 */
                if (u1Action == BGP4_ORF_REMOVE_ALL)
                {
                    if (BgpOrfProcessRcvdOrfEntry (&OrfEntry, u1Action) ==
                        BGP4_FAILURE)
                    {
                        return BGP4_FAILURE;
                    }

                    /* If there is no more entry, then break the loop and return
                     * Else, continue and process the other ORF entries*/
                    if (i4TmpLen == 0)
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                /* Extract the Sequence number from the rcvd ORF message */
                PTR_FETCH4 (OrfEntry.u4SeqNo, pu1OrfMsgBuf);
                pu1OrfMsgBuf += BGP4_ORF_SEQUENCE_LEN;
                i4TmpLen -= BGP4_ORF_SEQUENCE_LEN;

                /* Extract the Minimum prefix length number from the rcvd ORF message */
                OrfEntry.u1MinPrefixLen = *pu1OrfMsgBuf;
                pu1OrfMsgBuf += BGP4_ORF_MIN_PREFIX_LEN;
                i4TmpLen -= BGP4_ORF_MIN_PREFIX_LEN;

                /* Extract the Maximum prefix length number from the rcvd ORF message */
                OrfEntry.u1MaxPrefixLen = *pu1OrfMsgBuf;
                pu1OrfMsgBuf += BGP4_ORF_MAX_PREFIX_LEN;
                i4TmpLen -= BGP4_ORF_MAX_PREFIX_LEN;

                /* Extract the Prefix length number from the rcvd ORF message */
                OrfEntry.u1PrefixLen = *pu1OrfMsgBuf;
                pu1OrfMsgBuf += BGP4_ORF_PREFIX_LEN;
                i4TmpLen -= BGP4_ORF_PREFIX_LEN;

                /* Minimum prefix length should not be less than or equal to 
                 * Prefix length, if both are specified*/
                if ((OrfEntry.u1MinPrefixLen != 0)
                    && (OrfEntry.u1PrefixLen != 0))
                {
                    if (OrfEntry.u1MinPrefixLen <= OrfEntry.u1PrefixLen)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                                  BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                  "\tMin prefix length is less than the prefix length "
                                  "in the received ORF message\n");
                        return BGP4_FAILURE;
                    }
                }
                /* Maximum prefix length should not be less than or equal to 
                 * Prefix length, if both are specified*/
                if ((OrfEntry.u1MaxPrefixLen != 0)
                    && (OrfEntry.u1PrefixLen != 0))
                {
                    if (OrfEntry.u1MaxPrefixLen <= OrfEntry.u1PrefixLen)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                                  BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                  "\tMax prefix length is less than the prefix length "
                                  "in the received ORF message\n");
                        return BGP4_FAILURE;
                    }
                }
                /* Maximum prefix length should not be less than 
                 * Minimum Prefix length, if both are specified*/
                if ((OrfEntry.u1MaxPrefixLen != 0)
                    && (OrfEntry.u1MinPrefixLen != 0))
                {
                    if (OrfEntry.u1MaxPrefixLen < OrfEntry.u1MinPrefixLen)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                                  BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                  "\tMin prefix length is greater than Max prefix length "
                                  "in the received ORF message\n");
                        return BGP4_FAILURE;
                    }
                }

                if ((OrfEntry.u1PrefixLen % BITS_PER_BYTE) == 0)
                {
                    u1PrefixLen =
                        (UINT1) (OrfEntry.u1PrefixLen / BITS_PER_BYTE);
                }
                else
                {
                    u1PrefixLen =
                        (UINT1) ((OrfEntry.u1PrefixLen / BITS_PER_BYTE) + 1);
                }

                if (u2Afi == BGP4_INET_AFI_IPV4)
                {
                    if (OrfEntry.u1PrefixLen > BGP4_MAX_PREFIXLEN)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                                  BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                  "\tprefix length is invalid in the received ORF message\n");
                        return BGP4_FAILURE;
                    }

                    /* Extract the Address Prefix from the rcvd ORF message */
                    MEMCPY (&OrfEntry.AddrPrefix.au1Address, pu1OrfMsgBuf,
                            u1PrefixLen);
                    MEMCPY (&u4TmpAddr, &OrfEntry.AddrPrefix.au1Address,
                            BGP4_IPV4_PREFIX_LEN);
                    u4Addr = OSIX_NTOHL (u4TmpAddr);
                    MEMCPY (&OrfEntry.AddrPrefix.au1Address, &u4Addr,
                            BGP4_IPV4_PREFIX_LEN);

                    OrfEntry.AddrPrefix.u2Afi = BGP4_INET_AFI_IPV4;
                    OrfEntry.AddrPrefix.u2AddressLen = BGP4_IPV4_PREFIX_LEN;

                    pu1OrfMsgBuf += u1PrefixLen;
                    i4TmpLen -= u1PrefixLen;

                    /* Maximum prefix length or Minimum prefix length should not be 
                     * greater than the maximum value allowed for the Prefix length 
                     * of specific address family
                     */
                    if ((OrfEntry.u1MaxPrefixLen > BGP4_MAX_PREFIXLEN) ||
                        (OrfEntry.u1MinPrefixLen > BGP4_MAX_PREFIXLEN))
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                                  BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                  "\tMax/Min prefix length is invalid in the received ORF message\n");
                        return BGP4_FAILURE;
                    }
                }
#ifdef BGP4_IPV6_WANTED
                else
                {
                    if (OrfEntry.u1PrefixLen > BGP4_MAX_IPV6_PREFIXLEN)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                                  BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                  "\tprefix length is invalid in the received ORF message\n");
                        return BGP4_FAILURE;
                    }

                    /* Extract the Address Prefix from the rcvd ORF message */
                    MEMCPY (&OrfEntry.AddrPrefix.au1Address, pu1OrfMsgBuf,
                            u1PrefixLen);
                    OrfEntry.AddrPrefix.u2Afi = BGP4_INET_AFI_IPV6;
                    OrfEntry.AddrPrefix.u2AddressLen = BGP4_IPV6_PREFIX_LEN;

                    pu1OrfMsgBuf += u1PrefixLen;
                    i4TmpLen -= u1PrefixLen;

                    /* Maximum prefix length or Minimum prefix length should not be 
                     * greater than the maximum value allowed for the Prefix length 
                     * of specific address family
                     */
                    if ((OrfEntry.u1MaxPrefixLen > BGP4_MAX_IPV6_PREFIXLEN) ||
                        (OrfEntry.u1MinPrefixLen > BGP4_MAX_IPV6_PREFIXLEN))
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                                  BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                  "\tMax/Min prefix length is invalid in the received ORF message\n");
                        return BGP4_FAILURE;
                    }
                }
#endif
                if (BgpOrfProcessRcvdOrfEntry (&OrfEntry, u1Action) ==
                    BGP4_FAILURE)
                {
                    return BGP4_FAILURE;
                }
            }
            i4BufLen -= i4OrfLen;
        }
        /* Other ORF types are not supported */
        else
        {
            return BGP4_FAILURE;
        }
    }

    if (u1WhenToRefresh == BGP4_ORF_IMMEDIATE)
    {
        /* Send the Out-RIB to the Peer */
        if (Bgp4FiltOutBoundSoftConfigHandler (BGP4_PEER_CXT_ID (pPeerInfo),
                                               BGP4_PEER_REMOTE_ADDR_INFO
                                               (pPeerInfo), u2Afi, u1Safi)
            != BGP4_SUCCESS)
        {
            return BGP4_FAILURE;
        }
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   :   Bgp4OrfRequestHandler                                */
/*  Description     :   This function handles the ORF Request                */
/*                      issued by the administrator. This function verifies  */
/*                      whether the request is for specific <AFI,SAFI> or    */
/*                      for all the available <AFI> <SAFI> combinations.     */
/*                      If the <AFI,SAFI> is <0,0>, then ORF                 */
/*                      operation will be performed for all <AFI,SAFI>.      */
/*                      Also based on the Input the ORF message will be send */
/*                      either to all the peers or to specific Peer.         */
/*  Input(s)        :   u4Context - Context Identifier                       */
/*                      PeerAddr - Address of the peer to be handled.        */
/*                      u2Afi - address family                               */
/*                      u2Safi - Subsequent address family                   */
/*  Output(s)       :   None                                                 */
/*                                                                           */
/*  Returns         :   BGP4_SUCCESS/BGP4_FAILURE                            */
/*****************************************************************************/
INT4
Bgp4OrfRequestHandler (UINT4 u4Context, tAddrPrefix PeerAddr,
                       UINT2 u2Afi, UINT2 u2Safi)
{
    /* Variable Declarations */
    tAddrPrefix         AddrPrefix;
    tBgp4PeerEntry     *pPeerEntry = NULL;
    UINT4               u4NegMask = 0;
    UINT4               u4AsafiIndex = 0;
    UINT4               u4AfiSafiMask = 0;
    UINT4               u4TempAsafiIndex = 0;
    UINT4               u4NegOrfCapMask = 0;
    UINT1               u1IsRefReqForAllPeer = BGP4_FALSE;
    UINT2               u2TempAfi = 0;
    UINT2               u2TempSafi = 0;
    INT4                i4RetStatus = BGP4_FAILURE;

    Bgp4InitAddrPrefixStruct (&(AddrPrefix),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddr));
    if ((PrefixMatch (PeerAddr, AddrPrefix)) == BGP4_TRUE)
    {
        /* ORF Request for all peers */
        u1IsRefReqForAllPeer = BGP4_TRUE;
    }
    else
    {
        /* ORF Request for Specific peers */
        u1IsRefReqForAllPeer = BGP4_FALSE;
    }

    if ((u2Afi != 0) && (u2Safi != 0))
    {
        /* Request is for Specific <AFI,SAFI>. */
        u2TempAfi = u2Afi;
        u2TempSafi = u2Safi;
        Bgp4GetAfiSafiIndex (u2TempAfi, u2TempSafi, &u4TempAsafiIndex);
    }

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeerEntry, tBgp4PeerEntry *)
    {
        if ((PrefixMatch (AddrPrefix,
                          BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)))
            == BGP4_TRUE)
        {
            /* Current Peer Prefix matches witht the Previous Peer's
             * Prefix. So this is the duplicate entry. */
            continue;
        }
        else
        {
            Bgp4CopyAddrPrefixStruct (&(AddrPrefix),
                                      BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));
        }

        if (u1IsRefReqForAllPeer == BGP4_FALSE)
        {
            /* Request for Specific Peer. */
            if (PrefixMatch (AddrPrefix, PeerAddr) == BGP4_FALSE)
            {
                /* Peer does not match the input. */
                continue;
            }
        }
        else
        {
            /* Request for all Peers. */
            if (BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix) !=
                BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddr))
            {
                /* Peer Address Family not matching. */
                continue;
            }
        }

        if (BGP4_PEER_STATE (pPeerEntry) != BGP4_ESTABLISHED_STATE)
        {
            /* Peer is not in Established State. No need for any
             * processing. */
            if (u1IsRefReqForAllPeer == BGP4_FALSE)
            {
                if ((u2Afi != 0) && (u2Safi != 0))
                {
                    if (BGP4_PEER_AFI_SAFI_INSTANCE (pPeerEntry,
                                                     u4TempAsafiIndex) != NULL)
                    {
                        BGP4_ORF_MSG_REQ_PEER (pPeerEntry,
                                               u4TempAsafiIndex) = 0;
                    }
                }
                break;
            }
            else
            {
                continue;
            }
        }

        for (u4AsafiIndex = 0; u4AsafiIndex < BGP4_MPE_ADDR_FAMILY_MAX_INDEX;
             u4AsafiIndex++)
        {
            /* Check whether the ORF request if for specific
             * <AFI,SAFI> pair or for all the supported <AFI,SAFI>.
             * If <AFI,SAFI> is <0,0>, then the request is for all
             * the supported <AFI,SAFI>. */
            if ((u2Afi != 0) && (u2Safi != 0))
            {
                /* Request is for Specific <AFI,SAFI>. */
                if (u4TempAsafiIndex != u4AsafiIndex)
                {
                    continue;
                }
            }
            else
            {
                /* Get the <AFI,SAFI> index and mask */
                i4RetStatus =
                    Bgp4GetAfiSafiFromIndex (u4AsafiIndex, &u2TempAfi,
                                             &u2TempSafi);
                if (i4RetStatus == BGP4_FAILURE)
                {
                    continue;
                }

            }

            /* Check whether the Peer support this <AFI,SAFI> instance. */
            switch (u4AsafiIndex)
            {
                case BGP4_IPV4_UNI_INDEX:
                    u4NegMask = CAP_NEG_IPV4_UNI_MASK;
                    u4AfiSafiMask = CAP_MP_IPV4_UNICAST;
                    break;
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    u4NegMask = CAP_NEG_IPV6_UNI_MASK;
                    u4AfiSafiMask = CAP_MP_IPV6_UNICAST;
                    break;
#endif
                default:
                    break;
            }

            if (((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) != 0) &&
                 ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) & u4NegMask) !=
                  u4NegMask)) ||
                ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) == 0) &&
                 (u4NegMask != CAP_NEG_IPV4_UNI_MASK)))
            {
                /* The <AFI,SAFI> instance is not negotiated for this
                 * peer. */
                if ((u2Afi != 0) && (u2Safi != 0))
                {
                    if (BGP4_PEER_AFI_SAFI_INSTANCE (pPeerEntry,
                                                     u4TempAsafiIndex) != NULL)
                    {
                        BGP4_ORF_MSG_REQ_PEER (pPeerEntry,
                                               u4TempAsafiIndex) = 0;
                    }
                    break;
                }
                else
                {
                    /* Look for next <AFI,SAFI> */
                    continue;
                }
            }

            /* Fetch the ORF Capability negotiated mask for the given AFI,SAFI,ORF mode */
            if (Bgp4OrfGetOrfCapMask (u4AfiSafiMask, BGP4_CLI_ORF_MODE_SEND,
                                      &u4NegOrfCapMask) != BGP4_SUCCESS)
            {
                continue;
            }
            /* Check whether ORF send Capability is negotiated with
             * the peer for this AFI,SAFI or not. If negotiated, then
             * send the ORF message for this <AFI,SAFI>. */
            if ((BGP4_PEER_NEG_CAP_MASK (pPeerEntry) &
                 u4NegOrfCapMask) == u4NegOrfCapMask)
            {
                /* ORF send Capability is negotiated. */
                switch (BGP4_GET_PEER_CURRENT_STATE (pPeerEntry))
                {
                    case BGP4_PEER_READY:
                    case BGP4_PEER_SOFTCONFIG_INBOUND_INPROGRESS:
                    case BGP4_PEER_SOFTCONFIG_OUTBOUND_INPROGRESS:
                    case BGP4_PEER_REUSE_INPROGRESS:

                        Bgp4RtRefMsgFormHandler (pPeerEntry, u2TempAfi,
                                                 (UINT1) u2TempSafi, BGP4_TRUE);
                        BGP4_ORF_MSG_REQ_PEER (pPeerEntry, u4AsafiIndex) = 0;
                        break;

                    case BGP4_PEER_INIT_INPROGRESS:
                    case BGP4_PEER_DEINIT_INPROGRESS:
                    default:
                        BGP4_ORF_MSG_REQ_PEER (pPeerEntry, u4AsafiIndex) = 0;
                        break;
                }
            }

            if ((u2Afi != 0) && (u2Safi != 0))
            {
                /* Request is for specific <AFI,SAFI> */
                break;
            }
        }
        if (u1IsRefReqForAllPeer == BGP4_FALSE)
        {
            /* Request is for single peer only and it has been processed.
             * So break out of the loop. */
            break;
        }
    }                            /* TMO_SLL_Scan */

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   :   Bgp4OrfGetOrfCapMask                                 */
/*  Description     :   This function is to process the update the negotiated*/
/*                      capabilities information                             */
/*  Input(s)        :   u4AsafiMask - AFi & Safi mask                        */
/*                      u1OrfMode - SEND/RECEIVE                             */
/*  Output(s)       :   pu4OrfCapNegMask - pointer upated with orf cap mask  */
/*                                                                           */
/*  Returns         :   BGP4_SUCCESS/BGP4_FAILURE                            */
/*****************************************************************************/
INT4
Bgp4OrfGetOrfCapMask (UINT4 u4AsafiMask, UINT1 u1OrfMode, UINT4 *pu4OrfCapMask)
{
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
        {
            if (u1OrfMode & BGP4_CLI_ORF_MODE_RECEIVE)
            {
                *pu4OrfCapMask |= CAP_NEG_ORF_IPV4_UNICAST_RCV;
            }
            if (u1OrfMode & BGP4_CLI_ORF_MODE_SEND)
            {
                *pu4OrfCapMask |= CAP_NEG_ORF_IPV4_UNICAST_SEND;
            }
            break;
        }
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
        {
            if (u1OrfMode & BGP4_CLI_ORF_MODE_RECEIVE)
            {
                *pu4OrfCapMask |= CAP_NEG_ORF_IPV6_UNICAST_RCV;
            }
            if (u1OrfMode & BGP4_CLI_ORF_MODE_SEND)
            {
                *pu4OrfCapMask |= CAP_NEG_ORF_IPV6_UNICAST_SEND;
            }
            break;
        }
#endif

        default:
            return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/****************************************************************************
 Function    :  BgpOrfListTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparison
                pRBElem2 - Pointer to the node2 for comparison
 Description :  RBTree Compare function for
                fsBgp4ORFListTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/
INT4
BgpOrfListTableRBCmp (tBgp4RBElem * pRBElem1, tBgp4RBElem * pRBElem2)
{
    tBgp4OrfEntry      *pOrfEntry1 = (tBgp4OrfEntry *) pRBElem1;
    tBgp4OrfEntry      *pOrfEntry2 = (tBgp4OrfEntry *) pRBElem2;

    if (BGP4_AFI_IN_ADDR_PREFIX_INFO (pOrfEntry1->PeerAddr) >
        BGP4_AFI_IN_ADDR_PREFIX_INFO (pOrfEntry2->PeerAddr))
    {
        return 1;
    }
    else if (BGP4_AFI_IN_ADDR_PREFIX_INFO (pOrfEntry1->PeerAddr) <
             BGP4_AFI_IN_ADDR_PREFIX_INFO (pOrfEntry2->PeerAddr))
    {
        return -1;
    }

    if ((PrefixGreaterThan (pOrfEntry1->PeerAddr, pOrfEntry2->PeerAddr))
        == BGP4_TRUE)
    {
        return 1;
    }
    else if ((PrefixLessThan (pOrfEntry1->PeerAddr, pOrfEntry2->PeerAddr))
             == BGP4_TRUE)
    {
        return -1;
    }

    if (pOrfEntry1->u2Afi > pOrfEntry2->u2Afi)
    {
        return 1;
    }
    else if (pOrfEntry1->u2Afi < pOrfEntry2->u2Afi)
    {
        return -1;
    }

    if (pOrfEntry1->u1Safi > pOrfEntry2->u1Safi)
    {
        return 1;
    }
    else if (pOrfEntry1->u1Safi < pOrfEntry2->u1Safi)
    {
        return -1;
    }

    if (pOrfEntry1->u1OrfType > pOrfEntry2->u1OrfType)
    {
        return 1;
    }
    else if (pOrfEntry1->u1OrfType < pOrfEntry2->u1OrfType)
    {
        return -1;
    }

    if (pOrfEntry1->u4SeqNo > pOrfEntry2->u4SeqNo)
    {
        return 1;
    }
    else if (pOrfEntry1->u4SeqNo < pOrfEntry2->u4SeqNo)
    {
        return -1;
    }

    if ((PrefixGreaterThan (pOrfEntry1->AddrPrefix, pOrfEntry2->AddrPrefix))
        == BGP4_TRUE)
    {
        return 1;
    }
    else if ((PrefixLessThan (pOrfEntry1->AddrPrefix, pOrfEntry2->AddrPrefix))
             == BGP4_TRUE)
    {
        return -1;
    }

    if (pOrfEntry1->u1PrefixLen > pOrfEntry2->u1PrefixLen)
    {
        return 1;
    }
    else if (pOrfEntry1->u1PrefixLen < pOrfEntry2->u1PrefixLen)
    {
        return -1;
    }

    if (pOrfEntry1->u1MinPrefixLen > pOrfEntry2->u1MinPrefixLen)
    {
        return 1;
    }
    else if (pOrfEntry1->u1MinPrefixLen < pOrfEntry2->u1MinPrefixLen)
    {
        return -1;
    }

    if (pOrfEntry1->u1MaxPrefixLen > pOrfEntry2->u1MaxPrefixLen)
    {
        return 1;
    }
    else if (pOrfEntry1->u1MaxPrefixLen < pOrfEntry2->u1MaxPrefixLen)
    {
        return -1;
    }

    if (pOrfEntry1->u1Match > pOrfEntry2->u1Match)
    {
        return 1;
    }
    else if (pOrfEntry1->u1Match < pOrfEntry2->u1Match)
    {
        return -1;
    }

    return 0;
}

/****************************************************************************
 Function    :  BgpOrfProcessRcvdOrfEntry 
 Input       :  pOrfEntry - Pointer to the received ORF entry
                u1Action  - ADD/REMOVE/REMOVE_ALL
 Description :  This function is to process the received ORF entry and update
                ORF database
 Output      :  None
 Returns     :  BGP4_SUCCESS/BGP4_FAILURE
****************************************************************************/
INT4
BgpOrfProcessRcvdOrfEntry (tBgp4OrfEntry * pRcvdOrfEntry, UINT1 u1Action)
{
    tBgp4OrfEntry      *pOrfEntry = NULL;

    /* IF the action is remove, then delete the matching ORF entry */
    if (u1Action == BGP4_ORF_REMOVE)
    {
        pOrfEntry = RBTreeGet (BGP4_ORF_ENTRY_TABLE (pRcvdOrfEntry->u4CxtId),
                               (tRBElem *) pRcvdOrfEntry);

        if (pOrfEntry == NULL)
        {
            return BGP4_FAILURE;
        }
        /* IF the matching ORF entry is statically configured, then do not delete it */
        else if (pOrfEntry->u1IsStatic != BGP4_TRUE)
        {
            RBTreeRem (BGP4_ORF_ENTRY_TABLE (pRcvdOrfEntry->u4CxtId),
                       (tRBElem *) pOrfEntry);
            MemReleaseMemBlock (BGP4_ORF_MEM_POOL_ID, (UINT1 *) pOrfEntry);
        }
    }
    /* IF the action is ADD, then add the new ORF entry, if it does not exist */
    else if (u1Action == BGP4_ORF_ADD)
    {
        pOrfEntry = RBTreeGet (BGP4_ORF_ENTRY_TABLE (pRcvdOrfEntry->u4CxtId),
                               (tRBElem *) pRcvdOrfEntry);

        if (pOrfEntry != NULL)
        {
            /* In case of BGP session after GR/switchover, the ORF entries might
             * have been previously added. So accept the route refresh message */
            return BGP4_SUCCESS;
        }

        pOrfEntry = (tBgp4OrfEntry *) MemAllocMemBlk (BGP4_ORF_MEM_POOL_ID);
        if (pOrfEntry == NULL)
        {
            BGP4_TRC (&pRcvdOrfEntry->PeerAddr, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                      BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory allocation for ORF node FAILED!!!\n");
            gu4BgpDebugCnt[MAX_BGP_ORF_ENTRIES_SIZING_ID]++;
            return BGP4_FAILURE;
        }
        MEMCPY (pOrfEntry, pRcvdOrfEntry, sizeof (tBgp4OrfEntry));
        if (RBTreeAdd (BGP4_ORF_ENTRY_TABLE (pRcvdOrfEntry->u4CxtId),
                       (tRBElem *) pOrfEntry) != RB_SUCCESS)
        {
            BGP4_TRC (&pRcvdOrfEntry->PeerAddr, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                      BGP4_MOD_NAME, "\tRBTree Add for ORF entry FAILED!!!\n");
            MemReleaseMemBlock (BGP4_ORF_MEM_POOL_ID, (UINT1 *) pOrfEntry);
            return BGP4_FAILURE;
        }
    }
    /* If the action is REMOVE_ALL, delete all the ORF entries for the
     * specific neighbor with the received AFI,SAFI, ORF type*/
    else
    {
        pOrfEntry =
            RBTreeGetNext (BGP4_ORF_ENTRY_TABLE (pRcvdOrfEntry->u4CxtId),
                           (tRBElem *) pRcvdOrfEntry, NULL);

        while (pOrfEntry != NULL)
        {
            if ((pOrfEntry->u1OrfType != pRcvdOrfEntry->u1OrfType) ||
                (MEMCMP (&(pOrfEntry->PeerAddr), &(pRcvdOrfEntry->PeerAddr),
                         (sizeof (tAddrPrefix))) != 0) ||
                (pOrfEntry->u2Afi != pRcvdOrfEntry->u2Afi) ||
                (pOrfEntry->u1Safi != pRcvdOrfEntry->u1Safi))
            {
                break;
            }
            /* IF the ORF entry is statically configured, then do not delete it */
            else if (pOrfEntry->u1IsStatic != BGP4_TRUE)
            {
                RBTreeRem (BGP4_ORF_ENTRY_TABLE (pRcvdOrfEntry->u4CxtId),
                           (tRBElem *) pOrfEntry);
                MemReleaseMemBlock (BGP4_ORF_MEM_POOL_ID, (UINT1 *) pOrfEntry);
            }

            pOrfEntry =
                RBTreeGetNext (BGP4_ORF_ENTRY_TABLE (pRcvdOrfEntry->u4CxtId),
                               (tRBElem *) pOrfEntry, NULL);
        }
    }
    return BGP4_SUCCESS;
}

/****************************************************************************
 Function    :  BgpOrfDelRcvdOrfEntries 
 Input       :  pPeerEntry - Pointer to the peer entry
 Description :  This function is to delete the ORF entries received from the peer
 Output      :  None
 Returns     :  None
****************************************************************************/
VOID
BgpOrfDelRcvdOrfEntries (tBgp4PeerEntry * pPeerEntry)
{
    tBgp4OrfEntry       OrfEntry;
    tBgp4OrfEntry      *pOrfEntry = NULL;

    MEMSET (&OrfEntry, 0, sizeof (tBgp4OrfEntry));

    MEMCPY (&OrfEntry.PeerAddr, &(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
            sizeof (tAddrPrefix));

    do
    {
        pOrfEntry =
            RBTreeGetNext (BGP4_ORF_ENTRY_TABLE (BGP4_PEER_CXT_ID (pPeerEntry)),
                           (tRBElem *) & OrfEntry, NULL);

        if (pOrfEntry == NULL)
        {
            break;
        }
        if (MEMCMP
            (&pOrfEntry->PeerAddr, &(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
             sizeof (tAddrPrefix)) != 0)
        {
            break;
        }
        MEMSET (&OrfEntry, 0, sizeof (tBgp4OrfEntry));
        MEMCPY (&OrfEntry, pOrfEntry, sizeof (tBgp4OrfEntry));

        RBTreeRem (BGP4_ORF_ENTRY_TABLE (BGP4_PEER_CXT_ID (pPeerEntry)),
                   (tRBElem *) pOrfEntry);
        MemReleaseMemBlock (BGP4_ORF_MEM_POOL_ID, (UINT1 *) pOrfEntry);

    }
    while (1);
}

/****************************************************************************
 Function    :  BgpOrfFillOrfEntries 
 Input       :  pPeerInfo - Peer information
                pu1OrfMsgBuf - Pointer to the buffer
                u2Afi - Address family identifier
 Description :  This function is to fetch the route map entries from the 
                route map and fill the ORF message
 Output      :  u4Length - Length of the ORF entries
 Returns     :  u4Length - Length of the ORF entries
****************************************************************************/
UINT4
BgpOrfFillOrfEntries (tBgp4PeerEntry * pPeerInfo, UINT1 *pu1OrfMsgBuf,
                      UINT2 u2Afi)
{
#ifdef ROUTEMAP_WANTED
    tBgp4OrfEntry       OrfEntry;
    UINT1              *pu1OrfMsg1 = NULL;
    UINT1              *pu1OrfMsg2 = NULL;
    UINT4               u4SeqNo = 0;
    INT4                i4RetVal = RMAP_FAILURE;
    UINT2               u2Msg1Len = BGP4_RTREF_MSG_SIZE;
    UINT2               u2Msg2Len = BGP4_RTREF_MSG_SIZE;
    UINT2               u2OrfLength = 0;
    UINT1               u1PrefixLen = 0;
    UINT1               u1Bits = 0;
    UINT1               u1Mask = 0;

    pu1OrfMsg1 = pu1OrfMsgBuf;
    pu1OrfMsg2 = pu1OrfMsgBuf;
    /* If none of the IP-Prefix list is associated with the neighbor, 
     * (i.e) IpPrefixFilterIn.u1Status is FILTERNIG_STAT_DISABLE,
     * then no ORF data has to be carried in the ORF message.
     * But if any ORF entries has been sent previously for the peer
     * that should be removed.
     */
    if (pPeerInfo->IpPrefixFilterIn.u1Status != FILTERNIG_STAT_ENABLE)
    {
        if (pPeerInfo->u1IsOrfSent != 0)
        {
            /* ORF entries have been sent previously for this neighbor.
             * So removing those entries, with when-to-refresh field set as IMMEDIATE
             */

            /* Fill the when-to-refresh field as DEFER */
            pu1OrfMsg1 += BGP4_RTREF_MSG_SIZE;
            *pu1OrfMsg1 = BGP4_ORF_IMMEDIATE;
            u2Msg1Len = (UINT2) (u2Msg1Len + BGP4_ORF_WHEN_TO_REF_LEN);

            /* Fill the ORF type (Address Prefix based ORF) */
            pu1OrfMsg1 += BGP4_ORF_WHEN_TO_REF_LEN;
            *pu1OrfMsg1 = BGP_ORF_TYPE_ADDRESS_PREFIX;
            u2Msg1Len = (UINT2) (u2Msg1Len + BGP4_ORF_TYPE_LEN);

            /* Fill the length of ORF entries.
             * In this case it will be 1. Because we are carrying only REMOVE_ALL*/
            pu1OrfMsg1 += BGP4_ORF_TYPE_LEN;
            PTR_ASSIGN2 (pu1OrfMsg1, 1);
            u2Msg1Len = (UINT2) (u2Msg1Len + BGP4_ORF_LEN_FIELD);

            /* Fill the action field as REMOVE_ALL */
            pu1OrfMsg1 += BGP4_ORF_LEN_FIELD;
            *pu1OrfMsg1 = (BGP4_ORF_REMOVE_ALL << 6);
            u2Msg1Len = (UINT2) (u2Msg1Len + BGP4_ORF_ACTION_MATCH_LEN);

            /* Assigning the length for the ORF (Route Refresh )message */
            pu1OrfMsgBuf += BGP4_MARKER_LEN;
            PTR_ASSIGN2 (pu1OrfMsgBuf, u2Msg1Len);

            pPeerInfo->u1IsOrfSent = 0;
        }
        return u2Msg1Len;
    }
    else
    {
        /* If the IP prefix list is associated with the neighbor, then send the 
         * newly configured IP prefix entries to the neighbor.
         *
         * But if the ORF message with previously configured IP prefix entries was 
         * already sent to the peer, i.e (pPeerInfo->u1IsOrfSent != 0),
         * then remove all those old entries. Because the old entries might have been 
         * removed or modified now by the administrator. So one or more ORF entries 
         * previously sent may be invalid now.
         *
         * To achieve this, we have to send two ORF messages (Route Refresh message)
         * in a single packet.
         * 1. One is to REMOVE all the previously sent entries
         * 2. Another one contains the new set of IP Prefix entries
         */
        if (pPeerInfo->u1IsOrfSent != 0)
        {
            /* ORF message with old IP prefix entries has been sent.
             * So remove the old entries first*/

            /* Fill the when-to-refresh field as DEFER */
            pu1OrfMsg1 += BGP4_RTREF_MSG_SIZE;
            *pu1OrfMsg1 = BGP4_ORF_DEFER;
            u2Msg1Len = (UINT2) (u2Msg1Len + BGP4_ORF_WHEN_TO_REF_LEN);

            /* Fill the ORF type (address prefix based ORF) */
            pu1OrfMsg1 += BGP4_ORF_WHEN_TO_REF_LEN;
            *pu1OrfMsg1 = BGP_ORF_TYPE_ADDRESS_PREFIX;
            u2Msg1Len = (UINT2) (u2Msg1Len + BGP4_ORF_TYPE_LEN);

            /* Fill the ORF length (it will be one, since we have only REMOVE_ALL) */
            pu1OrfMsg1 += BGP4_ORF_TYPE_LEN;
            PTR_ASSIGN2 (pu1OrfMsg1, 1);
            u2Msg1Len = (UINT2) (u2Msg1Len + BGP4_ORF_LEN_FIELD);

            /* Fill the action as REMOVE_ALL */
            pu1OrfMsg1 += BGP4_ORF_LEN_FIELD;
            *pu1OrfMsg1 = (BGP4_ORF_REMOVE_ALL << 6);
            u2Msg1Len = (UINT2) (u2Msg1Len + BGP4_ORF_ACTION_MATCH_LEN);

            pu1OrfMsg1 += BGP4_ORF_ACTION_MATCH_LEN;

            /* Assigning the length for the first ORF (Route Refresh )message */
            pu1OrfMsgBuf += BGP4_MARKER_LEN;
            PTR_ASSIGN2 (pu1OrfMsgBuf, u2Msg1Len);
            pu1OrfMsgBuf -= BGP4_MARKER_LEN;

            /* Assigning the start of second message to pu1OrfMsg2 */
            pu1OrfMsg2 = pu1OrfMsg1;

            /* Route Refresh message header will be same for the both messages
             * So simply doing memcpy for the first 23 (BGP4_RTREF_MSG_SIZE)
             * bytes (marker, length, msg type, afi, res, safi).
             * Length will change, we can update it later after filling the 
             * ORF entries
             */

            MEMCPY (pu1OrfMsg2, pu1OrfMsgBuf, BGP4_RTREF_MSG_SIZE);
            /* So now pu1OrfMsgBuf points to the start of second ORF message */
            pu1OrfMsgBuf = pu1OrfMsg1;
        }
        else
        {
            /* ORF messagea are not previously sent. So only one ORF message
             * with new entries will be sent */
            pPeerInfo->u1IsOrfSent = BGP4_TRUE;
            u2Msg1Len = 0;
        }

        /* Fill the when-to-refresh field as IMMEDIATE */
        pu1OrfMsg2 += BGP4_RTREF_MSG_SIZE;
        *pu1OrfMsg2 = BGP4_ORF_IMMEDIATE;
        u2Msg2Len = (UINT2) (u2Msg2Len + BGP4_ORF_WHEN_TO_REF_LEN);

        /* Fill the ORF type (address prefix based ORF) */
        pu1OrfMsg2 += BGP4_ORF_WHEN_TO_REF_LEN;
        *pu1OrfMsg2 = BGP_ORF_TYPE_ADDRESS_PREFIX;
        u2Msg2Len = (UINT2) (u2Msg2Len + BGP4_ORF_TYPE_LEN);

        pu1OrfMsg2 += BGP4_ORF_TYPE_LEN;
        /* ORF length will be filled below */
        u2Msg2Len = (UINT2) (u2Msg2Len + BGP4_ORF_LEN_FIELD);

        pu1OrfMsg2 += BGP4_ORF_LEN_FIELD;

        /*Start filling the ORF entries */

        do
        {
            MEMSET (&OrfEntry, 0, sizeof (tBgp4OrfEntry));
            i4RetVal =
                RMapGetNextValidIpPrefixEntry (pPeerInfo->IpPrefixFilterIn.
                                               au1DistInOutFilterRMapName,
                                               u4SeqNo, &OrfEntry);

            if (i4RetVal == RMAP_FAILURE)
            {
                break;
            }
            u4SeqNo = OrfEntry.u4SeqNo;

            if (OrfEntry.AddrPrefix.u2Afi != u2Afi)
            {
                continue;
            }

            /* Fill the action as ADD and match condition (permit/deny) */
            *pu1OrfMsg2 =
                (UINT1) ((BGP4_ORF_ADD << 6) | (OrfEntry.u1Match << 5));
            u2OrfLength = (UINT2) (u2OrfLength + BGP4_ORF_ACTION_MATCH_LEN);

            /* Fill the sequence number for the entry */
            pu1OrfMsg2 += BGP4_ORF_ACTION_MATCH_LEN;
            PTR_ASSIGN4 (pu1OrfMsg2, OrfEntry.u4SeqNo);
            u2OrfLength = (UINT2) (u2OrfLength + BGP4_ORF_SEQUENCE_LEN);

            /* Fill the minimum prefix length */
            pu1OrfMsg2 += BGP4_ORF_SEQUENCE_LEN;
            *pu1OrfMsg2 = OrfEntry.u1MinPrefixLen;
            u2OrfLength = (UINT2) (u2OrfLength + BGP4_ORF_MIN_PREFIX_LEN);

            /* Fill the maximum prefix length */
            pu1OrfMsg2 += BGP4_ORF_MIN_PREFIX_LEN;
            *pu1OrfMsg2 = OrfEntry.u1MaxPrefixLen;
            u2OrfLength = (UINT2) (u2OrfLength + BGP4_ORF_MAX_PREFIX_LEN);

            /* Fill the prefix length */
            pu1OrfMsg2 += BGP4_ORF_MAX_PREFIX_LEN;
            *pu1OrfMsg2 = OrfEntry.u1PrefixLen;
            u2OrfLength = (UINT2) (u2OrfLength + BGP4_ORF_PREFIX_LEN);

            /* Fill the Address prefix */
            pu1OrfMsg2 += BGP4_ORF_PREFIX_LEN;

            if ((OrfEntry.u1PrefixLen % BITS_PER_BYTE) == 0)
            {
                u1PrefixLen = (OrfEntry.u1PrefixLen / BITS_PER_BYTE);
            }
            else
            {
                u1PrefixLen =
                    (UINT1) ((OrfEntry.u1PrefixLen / BITS_PER_BYTE) + 1);
                u1Bits = (OrfEntry.u1PrefixLen % BITS_PER_BYTE);
                u1Mask = (UINT1) (0xff << (BITS_PER_BYTE - u1Bits));
                OrfEntry.AddrPrefix.au1Address[u1PrefixLen - 1] =
                    (OrfEntry.AddrPrefix.au1Address[u1PrefixLen - 1] & u1Mask);
            }
            MEMCPY (pu1OrfMsg2, &OrfEntry.AddrPrefix.au1Address, u1PrefixLen);
            u2OrfLength = (UINT2) (u2OrfLength + u1PrefixLen);
            pu1OrfMsg2 += u1PrefixLen;

        }
        while (1);

        /* Moving the pointer to the Length field in the Route Refresh message */
        pu1OrfMsgBuf += BGP4_MARKER_LEN;
        /* Fill the second route refresh message length */
        u2Msg2Len = (UINT2) (u2Msg2Len + u2OrfLength);
        PTR_ASSIGN2 (pu1OrfMsgBuf, u2Msg2Len);

        /* Moving the pointer to the ORF Length field in the Route Refresh message */
        pu1OrfMsgBuf +=
            (BGP4_RTREF_LEN_FIELD + BGP4_MSG_TYPE_LEN + BGP4_RTREF_AFI_LEN +
             BGP4_RTREF_RES_LEN + BGP4_RTREF_SAFI_LEN +
             BGP4_ORF_WHEN_TO_REF_LEN + BGP4_ORF_TYPE_LEN);
        /* Fill the ORF length in the second route refrsh messsage */
        PTR_ASSIGN2 (pu1OrfMsgBuf, u2OrfLength);

        return (u2Msg1Len + u2Msg2Len);
    }
#else
    UNUSED_PARAM (pPeerInfo);
    UNUSED_PARAM (pu1OrfMsgBuf);
    UNUSED_PARAM (u2Afi);
    return BGP4_RTREF_MSG_SIZE;
#endif
}

/****************************************************************************
 Function    :  BgpOrfIsAddrPrefixMatching 
 Input       :  pNetAddress - network information
                pOrfEntry - pointer to the ORF entry
 Description :  This function is to check whether the Route information matches
                with the ORF entry
 Output      :  
 Returns     :  BGP4_TRUE/BGP4_FALSE
****************************************************************************/
INT4
BgpOrfIsAddrPrefixMatching (tNetAddress * pNetAddress,
                            tBgp4OrfEntry * pOrfEntry)
{
    if ((AddrMatch (pNetAddress->NetAddr, pOrfEntry->AddrPrefix,
                    (UINT4) pOrfEntry->u1PrefixLen)) != BGP4_TRUE)
    {
        return BGP4_FALSE;
    }
    if (pOrfEntry->u1PrefixLen != (UINT1) pNetAddress->u2PrefixLen)
    {
        return BGP4_FALSE;
    }

    /* If both min length and max length are unspecified in ORF,
     * then the route's prefix length should match with ORF prefix length*/
    if ((pOrfEntry->u1MinPrefixLen == 0) && (pOrfEntry->u1MaxPrefixLen == 0))
    {
        if (pNetAddress->u2PrefixLen != pOrfEntry->u1PrefixLen)
        {
            return BGP4_FALSE;
        }
    }
    /* If Min length is specified in ORF, then the route's prefix length
     * should be greater than or equal to Min prefix length in the ORF*/
    if (pOrfEntry->u1MinPrefixLen != 0)
    {
        if (pNetAddress->u2PrefixLen > pOrfEntry->u1MinPrefixLen)
        {
            return BGP4_FALSE;
        }
    }
    /* If Max length is specified in ORF, then the route's prefix length
     * should be less than or equal to Max prefix length in the ORF*/
    if (pOrfEntry->u1MaxPrefixLen != 0)
    {
        if (pNetAddress->u2PrefixLen > pOrfEntry->u1MaxPrefixLen)
        {
            return BGP4_FALSE;
        }
    }
    if (pNetAddress->u2PrefixLen < pOrfEntry->u1PrefixLen)
    {
        return BGP4_FALSE;
    }

    return BGP4_TRUE;
}

/****************************************************************************
 Function    :  Bgp4OrfReleaseOrfEntries 
 Input       :  u4ContextId - context identifier
 Description :  This function is to check whether the Route information matches
                with the ORF entry
 Output      :  None
 Returns     :  None
****************************************************************************/
VOID
Bgp4OrfReleaseOrfEntries (UINT4 u4ContextId)
{
    tBgp4OrfEntry       OrfEntry;
    tBgp4OrfEntry      *pOrfEntry = NULL;

    MEMSET (&OrfEntry, 0, sizeof (tBgp4OrfEntry));

    do
    {
        pOrfEntry = RBTreeGetNext (BGP4_ORF_ENTRY_TABLE (u4ContextId),
                                   (tRBElem *) & OrfEntry, NULL);
        if (pOrfEntry == NULL)
        {
            break;
        }
        MEMSET (&OrfEntry, 0, sizeof (tBgp4OrfEntry));
        MEMCPY (&OrfEntry, pOrfEntry, sizeof (tBgp4OrfEntry));

        RBTreeRem (BGP4_ORF_ENTRY_TABLE (u4ContextId), (tRBElem *) pOrfEntry);
        MemReleaseMemBlock (BGP4_ORF_MEM_POOL_ID, (UINT1 *) pOrfEntry);
    }
    while (1);
}

/****************************************************************************
 Function    :  Bgp4RRDNetworkTableCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparison
                pRBElem2 - Pointer to the node2 for comparison
 Description :  RBTree Compare function for fsMIBgp4RRDNetworkTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  BGP4_RB_GREATER or BGP4_RB_LESSER or BGP4_RB_EQUAL
****************************************************************************/
INT4
Bgp4RRDNetworkTableCmp (tBgp4RBElem * pRBElem1, tBgp4RBElem * pRBElem2)
{
    tNetworkAddr       *pNetAddrEntry1 = (tNetworkAddr *) pRBElem1;
    tNetworkAddr       *pNetAddrEntry2 = (tNetworkAddr *) pRBElem2;

    if (BGP4_AFI_IN_ADDR_PREFIX_INFO (pNetAddrEntry1->NetworkAddr) >
        BGP4_AFI_IN_ADDR_PREFIX_INFO (pNetAddrEntry2->NetworkAddr))
    {
        return BGP4_RB_GREATER;
    }
    else if (BGP4_AFI_IN_ADDR_PREFIX_INFO (pNetAddrEntry1->NetworkAddr) <
             BGP4_AFI_IN_ADDR_PREFIX_INFO (pNetAddrEntry2->NetworkAddr))
    {
        return BGP4_RB_LESSER;
    }

    if (MEMCMP (pNetAddrEntry1->NetworkAddr.au1Address,
                pNetAddrEntry2->NetworkAddr.au1Address,
                BGP4_MAX_INET_ADDRESS_LEN) == 0)
    {
        return BGP4_RB_EQUAL;
    }
    else if (MEMCMP (pNetAddrEntry1->NetworkAddr.au1Address,
                     pNetAddrEntry2->NetworkAddr.au1Address,
                     BGP4_MAX_INET_ADDRESS_LEN) < 0)
    {
        return BGP4_RB_LESSER;
    }
    else
    {
        return BGP4_RB_GREATER;
    }
}

#endif /* _BGPORF_C */
