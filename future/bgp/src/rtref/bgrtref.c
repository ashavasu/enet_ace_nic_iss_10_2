/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgrtref.c,v 1.17 2017/09/15 06:19:55 siva Exp $
 *
 * Description: This file contains functions for related to
 *              Route Refresh feature.
 *
 *******************************************************************/
#ifndef _BGRTREF_C
#define _BGRTREF_C

#include "bgp4com.h"

/*****************************************************************************/
/*  Function Name   :   Bgp4RtRefMsgFormHandler                              */
/*  Description     :   This module does the following -                     */
/*                  :   * Checks for route-refresh capability negotiation for*/
/*                  :     for given <AFI,SAFI> pair.                         */
/*                  :   * Calls Bgp4RtRefConstructRoutetRefMsg to form the   */
/*                  :     route refresh message                              */
/*                  :   * Sends the route refresh message to the peer.       */
/*                                                                           */
/*  Input(s)        :   pPeerInfo -                                          */
/*                         Pointer to the peer information from which the    */
/*                         route refresh message is received.                */
/*                      u2Afi - AFI value for which the OUT-RIB has to be    */
/*                              selected.                                    */
/*                      u1Safi - SAFI value for which the OUT-RIB has to be  */
/*                              selected.                                    */
/*                      u1IsOrf - Flag to identify whether ORF message needs */
/*                                to be sent or plain Rouite Refresh message */
/*                                                                           */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  BGP4_SUCCESS, if the route-refresh message*/
/*                              :  construction and sending to peer is       */
/*                              :  successful  OR                            */
/*                              :  BGP4_FAILURE, if route-ref msg constructn */
/*                              :  and sending results in failure            */
/*****************************************************************************/
INT4
Bgp4RtRefMsgFormHandler (tBgp4PeerEntry * pPeerInfo, UINT2 u2Afi, UINT1 u1Safi,
                         UINT1 u1IsOrf)
{
    /* Variable declarations */

    UINT1              *pu1RtRefMsgBuf = NULL;
    INT4                i4RetVal = BGP4_FAILURE;
    UINT4               u4Index;
    UINT4               u4Length = BGP4_RTREF_MSG_SIZE;

    /* Construct the route-refresh message for the peer */
    pu1RtRefMsgBuf = Bgp4RtRefConstructRouteRefMsg (u2Afi, u1Safi);

    if (Bgp4GetAfiSafiIndex (u2Afi, u1Safi, &u4Index) == BGP4_FAILURE)
    {
        if (pu1RtRefMsgBuf != NULL)
        {
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1RtRefMsgBuf);
        }
        return BGP4_FAILURE;
    }
    /* Handle failure in message construction */
    if (pu1RtRefMsgBuf == NULL)
    {
        (BGP4_RTREF_MSG_TXERR_CTR (pPeerInfo, u4Index))
            += BGP4_INCREMENT_BY_ONE;
        return BGP4_FAILURE;
    }

    /* If u1IsOrf is TRUE, ORF message message should be sent
     * i.e. Route Refresh messages with ORF entries if present*/
    if (u1IsOrf == BGP4_TRUE)
    {
        u4Length = BgpOrfFillOrfEntries (pPeerInfo, pu1RtRefMsgBuf, u2Afi);
    }

    /* Send the route-refresh message to the peer */
    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                   BGP4_TRC_FLAG, BGP4_TXQ_TRC, BGP4_MOD_NAME,
                   "\tPEER %s : Sending Route Refresh Message,\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo))));

    i4RetVal = Bgp4TcphCommand (pPeerInfo, BGP4_TCP_SEND,
                                pu1RtRefMsgBuf, u4Length);

    /* Handle failure in sending the route-refresh message to the peer */
    if (i4RetVal == BGP4_SUCCESS)
    {
        (BGP4_RTREF_MSG_SENT_CTR (pPeerInfo, u4Index)) += BGP4_INCREMENT_BY_ONE;
    }
    else
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Sending Route Refresh Message FAILS!!!,\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))));
        (BGP4_RTREF_MSG_TXERR_CTR (pPeerInfo, u4Index)) +=
            BGP4_INCREMENT_BY_ONE;
    }

    return BGP4_SUCCESS;

}                                /* END of Bgp4RtRefMsgFormHandler */

/*****************************************************************************/
/*  Function Name   :   Bgp4RtRefConstructRouteRefMsg                        */
/*                                                                           */
/*  Description     :   This module does the following -                     */
/*                  :   * Allocates buffer for the route-refresh message     */
/*                  :   * Fills the Bgp message header and the afi,safi      */
/*                  :     values in the message                              */
/*                                                                           */
/*  Input(s)        :   u2Afi  -  Address family identifier for which the    */
/*                                route Refresh message has to be sent.      */
/*                  :   u1Safi -  Subsequent Address family identifier for   */
/*                                which the route refresh msg. has to be sent*/
/*                                                                           */
/*  Output(s)       :   None                                                 */
/*                                                                           */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*                                                                           */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  pu1RtRefMsg, if the route-refresh message */
/*                              :  construction is successful       OR       */
/*                              :  NULL, if route-ref msg constructn         */
/*                              :  results in failure                        */
/*****************************************************************************/

UINT1              *
Bgp4RtRefConstructRouteRefMsg (UINT2 u2Afi, UINT1 u1Safi)
{

    /* Variable declarations */
    UINT1              *pu1RtRefMsgBuf = NULL;
    UINT1              *pu1RouteRefreshMsg = NULL;

    /* Allocate buffer for the route-refresh message */
    pu1RtRefMsgBuf = MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);

    /* Handle failure case in message allocation */
    if (pu1RtRefMsgBuf == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation to Form Route Refresh Msg FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_BUFNODE_MSGS_SIZING_ID]++;
        return (NULL);
    }

    /* Assign start of the buffer to pu1RouteRefreshMsg */
    pu1RouteRefreshMsg = pu1RtRefMsgBuf;

    /* Fill the marker values to form the BGP message header */
    MEMSET ((VOID *) pu1RtRefMsgBuf, BGP4_MARKER_BYTE_VAL, BGP4_MARKER_LEN);

    /* Move buffer offset by 16 bytes */
    pu1RtRefMsgBuf += BGP4_MARKER_LEN;

    /* Fill message length in header */
    PTR_ASSIGN2 (pu1RtRefMsgBuf, ((BGP4_RTREF_MSG_SIZE)));
    pu1RtRefMsgBuf += sizeof (UINT2);

    /* Fill message type in header */
    *pu1RtRefMsgBuf = BGP4_ROUTE_REFRESH_MSG;
    pu1RtRefMsgBuf++;

    /* Copy AFI value in the message */
    PTR_ASSIGN2 (pu1RtRefMsgBuf, ((u2Afi)));
    pu1RtRefMsgBuf += BGP4_RTREF_AFI_LEN;

    /* Increment the buffer offset for the reserved field */
    *pu1RtRefMsgBuf = 0;
    pu1RtRefMsgBuf += BGP4_RTREF_RES_LEN;

    /* Copy SAFI value in the message */
    *pu1RtRefMsgBuf = u1Safi;

    return (pu1RouteRefreshMsg);
}

/*****************************************************************************/
/*  Function Name   :   Bgp4RtRefMsgRcvdHandler                              */
/*                                                                           */
/*  Description     :   This module checks whether the Route Refresh         */
/*                      capability is negotiated. If negotiated, then this   */
/*                      module gets the best routes from RIB and advertises  */
/*                      to peer.                                             */
/*                                                                           */
/*  Input(s)        :   pPeerInfo -                                          */
/*                         Pointer to the peer information from which the    */
/*                         route refresh message is received.                */
/*                      pu1RrMsgBuf -                                        */
/*                         Pointer to the RR message received from peer      */
/*                      u4BufLen - Buffer Length                             */
/*                                                                           */
/*  Output(s)       :   None                                                 */
/*                                                                           */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*                                                                           */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  BGP4_SUCCESS, when RR msg processing is   */
/*                                 successful.                               */
/*                              :  BGP4_FAILURE, when there is any failure   */
/*****************************************************************************/

INT4
Bgp4RtRefMsgRcvdHandler (tBgp4PeerEntry * pPeerInfo, UINT1 *pu1RrMsgBuf,
                         UINT4 u4BufLen)
{
    /* Variable Declarations */
    UINT4               u4AfiSafi = 0;
    UINT4               u4Index;
    UINT4               u4NegOrfCapMask = 0;
    INT4                i4RetVal = 0;
    UINT2               u2Afi = 0;
    UINT1               u1Safi = 0;

    /* Extract AFI value from received route-refresh message  */
    PTR_FETCH2 (u2Afi, pu1RrMsgBuf);
    pu1RrMsgBuf += BGP4_RTREF_AFI_LEN + BGP4_RTREF_RES_LEN;

    /* Extract SAFI value from rcvd. route-refresh message */
    u1Safi = *pu1RrMsgBuf;

    pu1RrMsgBuf += BGP4_RTREF_SAFI_LEN;
    u4AfiSafi = ((UINT4) (u2Afi << BGP4_TWO_BYTE_BITS) | (u1Safi));

    if (Bgp4GetAfiSafiIndex (u2Afi, u1Safi, &u4Index) == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }

    /* 
     * Check if Route-refresh capability/ORF receive Capability in the received message is negotiated 
     * with  the peer 
     */
    Bgp4OrfGetOrfCapMask (u4AfiSafi, BGP4_CLI_ORF_MODE_RECEIVE,
                          &u4NegOrfCapMask);

    if (((BGP4_PEER_NEG_CAP_MASK (pPeerInfo) &
          CAP_NEG_ROUTE_REFRESH_MASK) != CAP_NEG_ROUTE_REFRESH_MASK) &&
        ((BGP4_PEER_NEG_CAP_MASK (pPeerInfo) & u4NegOrfCapMask) !=
         u4NegOrfCapMask))
    {
        /* Handle failure condition */
        (BGP4_RTREF_MSG_RCVD_INVALID_CTR (pPeerInfo, u4Index)) +=
            BGP4_INCREMENT_BY_ONE;
        return BGP4_FAILURE;
    }

    /* 
     * Check if <AFI,SAFI> in the received message is negotiated 
     * with  the peer 
     */
    switch (u4AfiSafi)
    {
        case CAP_MP_IPV4_UNICAST:
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) & CAP_NEG_IPV4_UNI_MASK)
                != CAP_NEG_IPV4_UNI_MASK)
            {
                (BGP4_RTREF_MSG_RCVD_INVALID_CTR (pPeerInfo, u4Index))
                    += BGP4_INCREMENT_BY_ONE;
                return BGP4_FAILURE;
            }
            break;

#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) & CAP_NEG_IPV6_UNI_MASK)
                != CAP_NEG_IPV6_UNI_MASK)
            {
                (BGP4_RTREF_MSG_RCVD_INVALID_CTR (pPeerInfo, u4Index))
                    += BGP4_INCREMENT_BY_ONE;
                return BGP4_FAILURE;
            }
            break;
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) & CAP_NEG_L2VPN_VPLS_MASK)
                != CAP_NEG_L2VPN_VPLS_MASK)
            {
                (BGP4_RTREF_MSG_RCVD_INVALID_CTR (pPeerInfo, u4Index))
                    += BGP4_INCREMENT_BY_ONE;
                return BGP4_FAILURE;
            }
            break;
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) & CAP_NEG_L2VPN_EVPN_MASK)
                != CAP_NEG_L2VPN_EVPN_MASK)
            {
                (BGP4_RTREF_MSG_RCVD_INVALID_CTR (pPeerInfo, u4Index))
                    += BGP4_INCREMENT_BY_ONE;
                return BGP4_FAILURE;
            }
            break;
#endif
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) & CAP_NEG_LBL_IPV4_MASK)
                != CAP_NEG_LBL_IPV4_MASK)
            {
                (BGP4_RTREF_MSG_RCVD_INVALID_CTR (pPeerInfo, u4Index))
                    += BGP4_INCREMENT_BY_ONE;
                return BGP4_FAILURE;
            }
            break;

        case CAP_MP_VPN4_UNICAST:
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) & CAP_NEG_IPV4_UNI_MASK)
                != CAP_NEG_IPV4_UNI_MASK)
            {
                (BGP4_RTREF_MSG_RCVD_INVALID_CTR (pPeerInfo, u4Index))
                    += BGP4_INCREMENT_BY_ONE;
                return BGP4_FAILURE;
            }
            break;
#endif
        default:
            (BGP4_RTREF_MSG_RCVD_INVALID_CTR (pPeerInfo, u4Index))
                += BGP4_INCREMENT_BY_ONE;
            return BGP4_FAILURE;
    }

    /* IF the Route Refresh message length is greater than 4 
     * (BGP4_RTREF_AFI_LEN + BGP4_RTREF_RES_LEN + BGP4_RTREF_SAFI_LEN), then it carries 
     * ORF message */
    if (u4BufLen >
        (BGP4_RTREF_AFI_LEN + BGP4_RTREF_RES_LEN + BGP4_RTREF_SAFI_LEN))
    {
        /* Check whether ORF receive Capability is negotiated for the received AFI,SAFI */
        if ((BGP4_PEER_NEG_CAP_MASK (pPeerInfo) & u4NegOrfCapMask) !=
            u4NegOrfCapMask)
        {
            (BGP4_RTREF_MSG_RCVD_INVALID_CTR (pPeerInfo, u4Index)) +=
                BGP4_INCREMENT_BY_ONE;
            return BGP4_FAILURE;
        }
        u4BufLen -=
            (BGP4_RTREF_AFI_LEN + BGP4_RTREF_RES_LEN + BGP4_RTREF_SAFI_LEN);
        /* Process the received ORF message */
        if ((i4RetVal =
             Bgp4OrfMsgRcvdHandler (pPeerInfo, pu1RrMsgBuf, (INT4) u4BufLen,
                                    u2Afi, u1Safi)) != BGP4_SUCCESS)
        {
            (BGP4_RTREF_MSG_RCVD_INVALID_CTR (pPeerInfo, u4Index))
                += BGP4_INCREMENT_BY_ONE;
            return BGP4_FAILURE;
        }
    }
    else
    {
        /* Send the Out-RIB to the Peer */
        i4RetVal =
            Bgp4FiltOutBoundSoftConfigHandler (BGP4_PEER_CXT_ID (pPeerInfo),
                                               BGP4_PEER_REMOTE_ADDR_INFO
                                               (pPeerInfo), u2Afi, u1Safi);
    }
    if (i4RetVal == BGP4_SUCCESS)
    {
        if (BGP4_PEER_AFI_SAFI_INSTANCE (pPeerInfo, u4Index) != NULL)
        {
            (BGP4_RTREF_MSG_RCVD_CTR (pPeerInfo, u4Index))++;
        }
    }
    return (i4RetVal);
}

/*****************************************************************************/
/*  Function Name   :   Bgp4RtRefRequestHandler                              */
/*  Description     :   This function handles the Soft-Inbound Request       */
/*                      issued by the administrator. This function verifies  */
/*                      whether the request is for specific <AFI,SAFI> or    */
/*                      for all the available <AFI> <SAFI> combinations.     */
/*                      If the <AFI,SAFI> is <0,0>, then SOFT-Inbound        */
/*                      operation will be performed for all <AFI,SAFI>.      */
/*                      Also based on the Input the Soft-In will be applied  */
/*                      either to all the peers or to specific Peer.         */
/*  Input(s)        :   PeerAddr - Address of the peer to be handled.        */
/*                      u2Afi - address family                               */
/*                      u2Safi - Subsequent address family                   */
/*                      u1OperType - BGP4_RR_ADVT_PE_PEERS                   */
/*                                   BGP4_RR_ADVT_NON_CLIENTS                */
/*                                   BGP4_RR_ADVT_DEFAULT                    */
/*                                   BGP4_RR_ADVT_VPLS_PE_PEERS                    */
/*  Output(s)       :   None                                                 */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  BGP4_PEERENTRY_HEAD(BGP4_DFLT_VRFID)                       */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*                                                                           */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :                                            */
/*****************************************************************************/
INT4
Bgp4RtRefRequestHandler (UINT4 u4Context, tAddrPrefix PeerAddr,
                         UINT2 u2Afi, UINT2 u2Safi, UINT1 u1OperType)
{
    /* Variable Declarations */
    tAddrPrefix         AddrPrefix;
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tPeerNode          *pRtRefPeerNode = NULL;
    tAfiSafiNode       *pPendReq = NULL;
    INT4                i4Status = BGP4_FALSE;
    UINT4               u4NegMask = 0;
    UINT4               u4AsafiMask = 0;
    UINT4               u4AsafiIndex = 0;
    UINT4               u4TempAsafiIndex = 0;
    UINT1               u1IsRefReqForAllPeer = BGP4_FALSE;
    UINT2               u2TempAfi = 0;
    UINT2               u2TempSafi = 0;
    INT4                i4RetStatus = BGP4_FAILURE;

    Bgp4InitAddrPrefixStruct (&(AddrPrefix),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddr));
    if ((PrefixMatch (PeerAddr, AddrPrefix)) == BGP4_TRUE)
    {
        /* Inbound Soft Request for all peers */
        u1IsRefReqForAllPeer = BGP4_TRUE;
    }
    else
    {
        /* Inbound Soft Request for Specific peers */
        u1IsRefReqForAllPeer = BGP4_FALSE;
    }

    if ((u2Afi != 0) && (u2Safi != 0))
    {
        /* Request is for Specific <AFI,SAFI>. */
        u2TempAfi = u2Afi;
        u2TempSafi = u2Safi;
        Bgp4GetAfiSafiIndex (u2TempAfi, u2TempSafi, &u4TempAsafiIndex);
    }

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeerEntry, tBgp4PeerEntry *)
    {
        if ((PrefixMatch (AddrPrefix,
                          BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)))
            == BGP4_TRUE)
        {
            /* Current Peer Prefix matches witht the Previous Peer's
             * Prefix. So this is the duplicate entry. */
            continue;
        }
        else
        {
            Bgp4CopyAddrPrefixStruct (&(AddrPrefix),
                                      BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));
        }

        if (u1IsRefReqForAllPeer == BGP4_FALSE)
        {
            /* Request for Specific Peer. */
            if (PrefixMatch (AddrPrefix, PeerAddr) == BGP4_FALSE)
            {
                /* Peer does not match the input. */
                continue;
            }
        }
        else
        {
            /* Request for all Peers. */
            if (BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix) !=
                BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddr))
            {
                /* Peer Address Family not matching. */
                continue;
            }
        }

        if (BGP4_PEER_STATE (pPeerEntry) != BGP4_ESTABLISHED_STATE)
        {
            /* Peer is not in Established State. No need for any
             * processing. */
            if (u1IsRefReqForAllPeer == BGP4_FALSE)
            {
                if ((u2Afi != 0) && (u2Safi != 0))
                {
                    BGP4_GET_AFISAFI_MASK (u2Afi, u2Safi, u4AsafiMask);
                    if (BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) & u4AsafiMask)
                    {
                        BGP4_RTREF_MSG_REQ_PEER (pPeerEntry,
                                                 u4TempAsafiIndex) = 0;
                    }
                }
                break;
            }
            else
            {
                continue;
            }
        }

        for (u4AsafiIndex = 0; u4AsafiIndex < BGP4_MPE_ADDR_FAMILY_MAX_INDEX;
             u4AsafiIndex++)
        {
            /* Check whether the Soft-Inbound request if for specific
             * <AFI,SAFI> pair or for all the supported <AFI,SAFI>.
             * If <AFI,SAFI> is <0,0>, then the request is for all
             * the supported <AFI,SAFI>. */
            if ((u2Afi != 0) && (u2Safi != 0))
            {
                /* Request is for Specific <AFI,SAFI>. */
                if (u4TempAsafiIndex != u4AsafiIndex)
                {
                    continue;
                }
            }
            else
            {
                /* Get the <AFI,SAFI> index and mask */
                i4RetStatus =
                    Bgp4GetAfiSafiFromIndex (u4AsafiIndex, &u2TempAfi,
                                             &u2TempSafi);
                if (i4RetStatus == BGP4_FAILURE)
                {
                    continue;
                }

            }

            switch (u1OperType)
            {
#ifdef L3VPN
                case BGP4_RR_ADVT_PE_PEERS:
                    /* Request is to advertise route-refresh message only
                     * to PE peers */
                    if (BGP4_VPN4_PEER_ROLE (pPeerEntry) != BGP4_VPN4_PE_PEER)
                    {
                        continue;
                    }
                    break;
#endif
#if defined(L3VPN)|| defined(VPLSADS_WANTED) || defined (EVPN_WANTED)
                case BGP4_RR_ADVT_NON_CLIENTS:
                    /* Request is to advertise route-refresh message only
                     * to NON_CLIENT peers */
                    if (BGP4_PEER_RFL_CLIENT (pPeerEntry) != NON_CLIENT)
                    {
                        continue;
                    }
                    break;
#endif
#ifdef VPLSADS_WANTED
                case BGP4_RR_ADVT_VPLS_PE_PEERS:
                    /* Request is to advertise route-refresh message only
                     * to Internal PE peers */
                    if (BGP4_GET_PEER_TYPE
                        (BGP4_PEER_CXT_ID (pPeerEntry),
                         pPeerEntry) == BGP4_EXTERNAL_PEER)
                    {
                        continue;
                    }
#endif

#ifdef EVPN_WANTED
                case BGP4_RR_ADVT_EVPN_PEERS:
                    break;
#endif
                case BGP4_RR_ADVT_DEFAULT:
                    /* default */
                default:
                    break;
            }

            BGP4_GET_AFISAFI_MASK (u2TempAfi, u2TempSafi, u4AsafiMask);

            /* Check whether the Peer support this <AFI,SAFI> instance. */
            switch (u4AsafiIndex)
            {
                case BGP4_IPV4_UNI_INDEX:
                    u4NegMask = CAP_NEG_IPV4_UNI_MASK;
                    break;
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    u4NegMask = CAP_NEG_IPV6_UNI_MASK;
                    break;
#endif
#ifdef L3VPN
                case BGP4_IPV4_LBLD_INDEX:
                    u4NegMask = CAP_NEG_LBL_IPV4_MASK;
                    break;
                case BGP4_VPN4_UNI_INDEX:
                    u4NegMask = CAP_NEG_VPN4_UNI_MASK;
                    break;
#endif
#ifdef VPLSADS_WANTED
                case BGP4_L2VPN_VPLS_INDEX:
                    u4NegMask = CAP_NEG_L2VPN_VPLS_MASK;
                    break;
#endif
#ifdef EVPN_WANTED
                case BGP4_L2VPN_EVPN_INDEX:
                    u4NegMask = CAP_NEG_L2VPN_EVPN_MASK;
                    break;
#endif
            }

            if (((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) != 0) &&
                 ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) & u4NegMask) !=
                  u4NegMask)) ||
                ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) == 0) &&
                 (u4NegMask != CAP_NEG_IPV4_UNI_MASK)))
            {
                /* The <AFI,SAFI> instance is not negotiated for this
                 * peer. */
                if ((u2Afi != 0) && (u2Safi != 0))
                {
                    if (BGP4_PEER_AFI_SAFI_INSTANCE (pPeerEntry,
                                                     u4TempAsafiIndex) != NULL)
                    {
                        BGP4_RTREF_MSG_REQ_PEER (pPeerEntry,
                                                 u4TempAsafiIndex) = 0;
                    }
                    break;
                }
                else
                {
                    /* Look for next <AFI,SAFI> */
                    continue;
                }
            }

            /* Check whether Route-Refresh Capability is negotiated with
             * the peer for this instance or not. If negotiated, then
             * send the route refresh message for this <AFI,SAFI>. If 
             * route refresh not negotiated, then enable SOFT-IN operation
             * for that <AFI,SAFI> */

            if ((BGP4_PEER_NEG_CAP_MASK (pPeerEntry) &
                 CAP_NEG_ROUTE_REFRESH_MASK) == CAP_NEG_ROUTE_REFRESH_MASK)
            {
                /* Route Refresh Capability is negotiated. */
                switch (BGP4_GET_PEER_CURRENT_STATE (pPeerEntry))
                {
                    case BGP4_PEER_READY:
                    case BGP4_PEER_SOFTCONFIG_INBOUND_INPROGRESS:
                    case BGP4_PEER_SOFTCONFIG_OUTBOUND_INPROGRESS:
                    case BGP4_PEER_REUSE_INPROGRESS:
                        /* Route Refresh Capability needs to be
                         * advertised for this <AFI,SAFI>. */
                        Bgp4RtRefMsgFormHandler (pPeerEntry, u2TempAfi,
                                                 (UINT1) u2TempSafi,
                                                 BGP4_FALSE);
                        BGP4_RTREF_MSG_REQ_PEER (pPeerEntry, u4AsafiIndex) = 0;
                        break;
                    case BGP4_PEER_INIT_INPROGRESS:
                    case BGP4_PEER_DEINIT_INPROGRESS:
                    default:
                        /* Route Refresh Capability need not be
                         * advertised. */
                        BGP4_RTREF_MSG_REQ_PEER (pPeerEntry, u4AsafiIndex) = 0;
                        break;
                }
            }
            else
            {
                /* Route Refresh Capability is not negotiated. */
#if defined(L3VPN) || defined(VPLSADS_WANTED)
                if ((u1OperType == BGP4_RR_ADVT_NON_CLIENTS)
#ifdef L3VPN
                    || (u1OperType == BGP4_RR_ADVT_PE_PEERS)
#endif
#ifdef VPLSADS_WANTED
                    || (u1OperType == BGP4_RR_ADVT_VPLS_PE_PEERS)
#endif
                    )
                {
                    /* Route-refresh capability is not negotiated, so, reset
                     * the session with the peer by send CEASE notification
                     */
                    Bgp4EhSendError (pPeerEntry, BGP4_CEASE, 0, NULL, 0);
                    BGP4_PEER_ESTAB_TIME (pPeerEntry) = Bgp4ElapTime ();
                    if (CAPS_ADVT_SUP (u4Context) == BGP4_TRUE)
                    {
                        CapsDeletePeerCapsInfo (pPeerEntry);
                        CapsClearSpkrAncdSts (pPeerEntry);
                    }
#ifdef RFD_WANTED
                    if ((BGP4_GET_PEER_TYPE (u4Context, pPeerEntry) ==
                         BGP4_EXTERNAL_PEER) &&
                        (BGP4_CONFED_PEER_STATUS (pPeerEntry) == BGP4_FALSE))
                    {
                        BGP4_CHANGE_STATE (pPeerEntry, BGP4_IDLE_STATE);
                        Bgp4AddTransitionToFsmHist (pPeerEntry,
                                                    BGP4_IDLE_STATE);
                        RfdPeersHandler (pPeerEntry);
                    }
#endif
                    Bgp4RibhEndConnection (pPeerEntry);
                    Bgp4SemhProcessInvalidEvt (pPeerEntry);
                    continue;
                }
#endif
                /* Check the current status of the peer */
                switch (BGP4_GET_PEER_CURRENT_STATE (pPeerEntry))
                {
                    case BGP4_PEER_READY:
                        /* Add the peer to the soft-reconfig list */
                        BGP_PEER_NODE_CREATE (pRtRefPeerNode);
                        if (pRtRefPeerNode == NULL)
                        {
                            BGP4_TRC_ARG1 (&
                                           (BGP4_PEER_REMOTE_ADDR_INFO
                                            (pPeerEntry)), BGP4_TRC_FLAG,
                                           BGP4_ALL_FAILURE_TRC |
                                           BGP4_EVENTS_TRC |
                                           BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                           "\tPEER %s : Memory Allocation to "
                                           "handle Route Refresh Request "
                                           "FAILED!!!\n",
                                           Bgp4PrintIpAddr
                                           (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerEntry))));
                            gu4BgpDebugCnt[MAX_BGP_PEER_NODE_SIZING_ID]++;
                            return BGP4_FAILURE;
                        }
                        pRtRefPeerNode->pPeer = pPeerEntry;
                        BGP4_PEER_SOFTCONFIG_INITIAL_AFISAFI (pPeerEntry) =
                            u4AsafiMask;
                        BGP4_PEER_SOFTCONFIG_INBOUND_AFI (pPeerEntry) =
                            u2TempAfi;
                        BGP4_PEER_SOFTCONFIG_INBOUND_SAFI (pPeerEntry) =
                            u2TempSafi;
                        Bgp4InitNetAddressStruct (&
                                                  (BGP4_PEER_INIT_NETADDR_INFO
                                                   (pPeerEntry)), u2TempAfi,
                                                  (UINT1) u2TempSafi);
                        TMO_SLL_Add (BGP4_RTREF_PEER_LIST (u4Context),
                                     &pRtRefPeerNode->TSNext);
                        BGP4_SET_PEER_CURRENT_STATE (pPeerEntry,
                                                     BGP4_PEER_SOFTCONFIG_INBOUND_INPROGRESS);
                        break;

                    case BGP4_PEER_SOFTCONFIG_INBOUND_INPROGRESS:
                        if ((BGP4_PEER_SOFTCONFIG_INBOUND_AFI (pPeerEntry) ==
                             u2TempAfi) &&
                            (BGP4_PEER_SOFTCONFIG_INBOUND_SAFI (pPeerEntry) ==
                             u2TempSafi))
                        {
                            /* Soft-In is already in-progress for this
                             * <AFI,SAFI> So the new request is rejected. */
                            break;
                        }
                        /* For other <AFI,SAFI>, need to add the request in Pending
                         * list and process it later. For this reason, allow the
                         * code to FALL-THROUGH and no break needed. */
                    case BGP4_PEER_SOFTCONFIG_OUTBOUND_INPROGRESS:
                    case BGP4_PEER_REUSE_INPROGRESS:
                        i4Status = Bgp4IsAfiSafiReqAlreadyPending (pPeerEntry,
                                                                   u4AsafiMask);
                        if (i4Status == BGP4_TRUE)
                        {
                            break;
                        }
                        else
                        {
                            AFI_SAFI_NODE_CREATE (pPendReq);
                            if (pPendReq == NULL)
                            {
                                return BGP4_FAILURE;
                            }
                            TMO_SLL_Init_Node (&pPendReq->TsAfiSafiNext);
                            pPendReq->u4AfiSafiMask = u4AsafiMask;
                            TMO_SLL_Add (BGP4_PEER_SOFTCONFIG_IN_PEND_LIST
                                         (pPeerEntry),
                                         &pPendReq->TsAfiSafiNext);
                        }
                        BGP4_SET_PEER_PEND_FLAG (pPeerEntry,
                                                 BGP4_PEER_SOFTCONFIG_INBOUND_PEND);
                        break;
                    case BGP4_PEER_INIT_INPROGRESS:
                    case BGP4_PEER_DEINIT_INPROGRESS:
                    default:
                        /* Under these situation, SOFT-INBOUND configuration is
                         * not accepted. */
                        break;
                }
            }

            if ((u2Afi != 0) && (u2Safi != 0))
            {
                /* Request is for specific <AFI,SAFI> */
                break;
            }
        }
        if (u1IsRefReqForAllPeer == BGP4_FALSE)
        {
            /* Request is for single peer only and it has been processed.
             * So break out of the loop. */
            break;
        }
    }                            /* TMO_SLL_Scan */

    if (u1IsRefReqForAllPeer == BGP4_TRUE)
    {
        /* Inbound policy may have changed for imported routes */
        Bgp4ProcessImportList (BGP4_IGP_METRIC_SET_EVENT, u4Context);
    }

    BGPUpdateDistanceChangeForExistRoutes (u4Context, CAP_MP_IPV4_UNICAST);
#ifdef BGP4_IPV6_WANTED
    BGPUpdateDistanceChangeForExistRoutes (u4Context, CAP_MP_IPV6_UNICAST);
#endif

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   :   Bgp4RtRefPendHandler                                 */
/*                                                                           */
/*  Description     :   Handles the pending route-request requests           */
/*                                                                           */
/*  Input(s)        :   pPeerInfo - pointer to the peer information          */
/*                                                                           */
/*  Output(s)       :   None                                                 */
/*                                                                           */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  BGP4_PEERENTRY_HEAD(BGP4_DFLT_VRFID)                       */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*                                                                           */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :                                            */
/*****************************************************************************/
INT4
Bgp4RtRefPendHandler (tBgp4PeerEntry * pPeerInfo)
{
    /* Variable Declarations */
    tPeerNode          *pRtRefPeerNode = NULL;
    tAfiSafiNode       *pAfiSafiNode = NULL;
    UINT4               u4AsafiMask = 0;
    UINT2               u2Afi = 0;
    UINT2               u2Safi = 0;

    if (BGP4_PEER_STATE (pPeerInfo) != BGP4_ESTABLISHED_STATE)
    {
        return BGP4_FAILURE;
    }

    pAfiSafiNode = (tAfiSafiNode *)
        TMO_SLL_First (BGP4_PEER_SOFTCONFIG_IN_PEND_LIST (pPeerInfo));

    if (pAfiSafiNode == NULL)
    {
        /* List is empty. Clear the pend flag. */
        BGP4_RESET_PEER_PEND_FLAG (pPeerInfo,
                                   BGP4_PEER_SOFTCONFIG_INBOUND_PEND);
        return BGP4_SUCCESS;
    }
    u4AsafiMask = pAfiSafiNode->u4AfiSafiMask;

    Bgp4GetAfiSafiFromMask (u4AsafiMask, &u2Afi, &u2Safi);

    /* Check the current status of the peer */
    switch (BGP4_GET_PEER_CURRENT_STATE (pPeerInfo))
    {
        case BGP4_PEER_READY:
            BGP_PEER_NODE_CREATE (pRtRefPeerNode);
            if (pRtRefPeerNode == NULL)
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : Memory Allocation to handle "
                               "Route Refresh Request FAILED!!!\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerInfo))));
                gu4BgpDebugCnt[MAX_BGP_PEER_NODE_SIZING_ID]++;
                return BGP4_FAILURE;
            }
            pRtRefPeerNode->pPeer = pPeerInfo;
            BGP4_PEER_SOFTCONFIG_INITIAL_AFISAFI (pPeerInfo) = u4AsafiMask;
            BGP4_PEER_SOFTCONFIG_INBOUND_AFI (pPeerInfo) = u2Afi;
            BGP4_PEER_SOFTCONFIG_INBOUND_SAFI (pPeerInfo) = u2Safi;
            Bgp4InitAddrPrefixStruct (&(BGP4_PEER_INIT_PREFIX_INFO (pPeerInfo)),
                                      u2Afi);
            TMO_SLL_Add (BGP4_RTREF_PEER_LIST (BGP4_PEER_CXT_ID (pPeerInfo)),
                         &pRtRefPeerNode->TSNext);

            BGP4_SET_PEER_CURRENT_STATE (pPeerInfo,
                                         BGP4_PEER_SOFTCONFIG_INBOUND_INPROGRESS);
            TMO_SLL_Delete (BGP4_PEER_SOFTCONFIG_IN_PEND_LIST (pPeerInfo),
                            &pAfiSafiNode->TsAfiSafiNext);
            if (TMO_SLL_Count (BGP4_PEER_SOFTCONFIG_IN_PEND_LIST (pPeerInfo))
                == 0)
            {
                BGP4_RESET_PEER_PEND_FLAG (pPeerInfo,
                                           BGP4_PEER_SOFTCONFIG_INBOUND_PEND);
            }
            AFI_SAFI_NODE_FREE (pAfiSafiNode);
            break;

        case BGP4_PEER_SOFTCONFIG_INBOUND_INPROGRESS:
        case BGP4_PEER_SOFTCONFIG_OUTBOUND_INPROGRESS:
        case BGP4_PEER_REUSE_INPROGRESS:
        case BGP4_PEER_INIT_INPROGRESS:
        case BGP4_PEER_DEINIT_INPROGRESS:
        default:
            return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   :   Bgp4IsAfiSafiReqAlreadyPending                       */
/*  Description     :   Checks if afi-safi request is already queued up      */
/*                      for Soft-In process.                                 */
/*  Input(s)        :   pPeerInfo - pointer to the peer information          */
/*  Output(s)       :   None                                                 */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  BGP4_PEERENTRY_HEAD(BGP4_DFLT_VRFID)                       */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  BGP4_TRUE/BGP4_FALSE                      */
/*****************************************************************************/
INT4
Bgp4IsAfiSafiReqAlreadyPending (tBgp4PeerEntry * pPeerInfo, UINT4 u4AfiSafi)
{
    /* Variable Declarations */
    tAfiSafiNode       *pAfiSafiNode = NULL;
    INT4                i4Status = BGP4_FALSE;

    TMO_SLL_Scan (BGP4_PEER_SOFTCONFIG_IN_PEND_LIST (pPeerInfo),
                  pAfiSafiNode, tAfiSafiNode *)
    {
        if (u4AfiSafi == pAfiSafiNode->u4AfiSafiMask)
        {
            i4Status = BGP4_TRUE;
            break;
        }
    }
    return i4Status;
}

/*****************************************************************************/
/*  Function Name   :   Bgp4IsAfiSafiOutboundReqPending                      */
/*  Description     :   Checks if afi-safi request is already queued up      */
/*                      for Soft-Out Processing.                             */
/*  Input(s)        :   pPeerInfo - pointer to the peer information          */
/*  Output(s)       :   None                                                 */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  BGP4_PEERENTRY_HEAD(BGP4_DFLT_VRFID)                       */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  BGP4_TRUE/BGP4_FALSE                      */
/*****************************************************************************/
INT4
Bgp4IsAfiSafiOutboundReqPending (tBgp4PeerEntry * pPeerInfo, UINT4 u4AfiSafi)
{
    /* Variable Declarations */
    tAfiSafiNode       *pAfiSafiNode = NULL;
    INT4                i4Status = BGP4_FALSE;

    TMO_SLL_Scan (BGP4_PEER_SOFTCONFIG_OUT_PEND_LIST (pPeerInfo),
                  pAfiSafiNode, tAfiSafiNode *)
    {
        if (u4AfiSafi == pAfiSafiNode->u4AfiSafiMask)
        {
            i4Status = BGP4_TRUE;
            break;
        }
    }
    return i4Status;
}

#endif /* _BGRTREF_C */
