/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: fsbgp4wr.c,v 1.32 2016/12/27 12:35:55 siva Exp $
 *
 * Description:
 ********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsbgp4wr.h"
# include  "fsbgp4db.h"
# include  "bgp4com.h"

VOID
RegisterFSBGP4 ()
{
    SNMPRegisterMibWithContextIdAndLock (&fsbgp4OID, &fsbgp4Entry,
                                         BgpLock, BgpUnLock,
                                         (INT4 (*)(UINT4)) BgpSetContext,
                                         BgpReleaseContext, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsbgp4OID, (const UINT1 *) "fsbgp4");
}

VOID
UnRegisterFSBGP4 ()
{
    SNMPUnRegisterMib (&fsbgp4OID, &fsbgp4Entry);
    SNMPDelSysorEntry (&fsbgp4OID, (const UINT1 *) "fsbgp4");
}

INT4
Fsbgp4GlobalAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4GlobalAdminStatus (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4LocalAsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4LocalAs (&(pMultiData->u4_ULongValue)));
}

INT4
Fsbgp4IdentifierGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4Identifier (&(pMultiData->u4_ULongValue)));
}

INT4
Fsbgp4SynchronizationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4Synchronization (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4DefaultLocalPrefGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4DefaultLocalPref (&(pMultiData->u4_ULongValue)));
}

INT4
Fsbgp4AdvtNonBgpRtGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4AdvtNonBgpRt (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4TraceEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4TraceEnable (&(pMultiData->u4_ULongValue)));
}

INT4
Fsbgp4DebugEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4DebugEnable (&(pMultiData->u4_ULongValue)));
}

INT4
Fsbgp4OverlappingRouteGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4OverlappingRoute (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4MaxPeerEntryGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4MaxPeerEntry (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4MaxNoofRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4MaxNoofRoutes (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4AlwaysCompareMEDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4AlwaysCompareMED (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4DefaultOriginateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4DefaultOriginate (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4DefaultIpv4UniCastGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4DefaultIpv4UniCast (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4GRAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4GRAdminStatus (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4GRRestartTimeIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4GRRestartTimeInterval (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4GRSelectionDeferralTimeIntervalGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4GRSelectionDeferralTimeInterval
            (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4GRStaleTimeIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4GRStaleTimeInterval (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4GRModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4GRMode (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4RestartSupportGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RestartSupport (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4RestartStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RestartStatus (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4RestartExitReasonGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RestartExitReason (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4RestartReasonGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RestartReason (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4ForwardingPreservationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4ForwardingPreservation (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4IsTrapEnabledGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4IsTrapEnabled (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4NextHopProcessingIntervalGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4NextHopProcessingInterval
            (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4IBGPRedistributionStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4IBGPRedistributionStatus
            (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4IBGPMaxPathsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4IBGPMaxPaths (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4EBGPMaxPathsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4EBGPMaxPaths (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4EIBGPMaxPathsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4EIBGPMaxPaths (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4OperIBGPMaxPathsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4OperIBGPMaxPaths (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4OperEBGPMaxPathsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4OperEBGPMaxPaths (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4OperEIBGPMaxPathsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4OperEIBGPMaxPaths (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4FourByteASNSupportStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4FourByteASNSupportStatus
            (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4FourByteASNotationTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4FourByteASNotationType (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4VpnLabelAllocPolicyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4VpnLabelAllocPolicy (&(pMultiData->i4_SLongValue)));
}

INT4 Fsbgp4MacMobDuplicationTimeIntervalGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
#ifdef EVPN_WANTED
        return(nmhGetFsbgp4MacMobDuplicationTimeInterval(&(pMultiData->i4_SLongValue)));
#else
        UNUSED_PARAM(pMultiData);
        return SNMP_SUCCESS;
#endif

}
INT4 Fsbgp4MaxMacMovesGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
#ifdef EVPN_WANTED
        return(nmhGetFsbgp4MaxMacMoves(&(pMultiData->i4_SLongValue)));
#else
        UNUSED_PARAM(pMultiData);
        return SNMP_SUCCESS;
#endif
}

INT4 Fsbgp4VpnRouteLeakStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsbgp4VpnRouteLeakStatus(&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4GlobalAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4GlobalAdminStatus (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4LocalAsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4LocalAs (pMultiData->u4_ULongValue));
}

INT4
Fsbgp4IdentifierSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4Identifier (pMultiData->u4_ULongValue));
}

INT4
Fsbgp4SynchronizationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4Synchronization (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4DefaultLocalPrefSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4DefaultLocalPref (pMultiData->u4_ULongValue));
}

INT4
Fsbgp4AdvtNonBgpRtSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4AdvtNonBgpRt (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4TraceEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4TraceEnable (pMultiData->u4_ULongValue));
}

INT4
Fsbgp4DebugEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4DebugEnable (pMultiData->u4_ULongValue));
}

INT4
Fsbgp4OverlappingRouteSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4OverlappingRoute (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4MaxPeerEntrySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4MaxPeerEntry (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4MaxNoofRoutesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4MaxNoofRoutes (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4AlwaysCompareMEDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4AlwaysCompareMED (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4DefaultOriginateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4DefaultOriginate (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4DefaultIpv4UniCastSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4DefaultIpv4UniCast (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4GRAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4GRAdminStatus (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4GRRestartTimeIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4GRRestartTimeInterval (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4GRSelectionDeferralTimeIntervalSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4GRSelectionDeferralTimeInterval
            (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4GRStaleTimeIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4GRStaleTimeInterval (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RestartSupportSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4RestartSupport (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RestartReasonSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4RestartReason (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4IsTrapEnabledSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4IsTrapEnabled (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4NextHopProcessingIntervalSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4NextHopProcessingInterval (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4IBGPRedistributionStatusSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4IBGPRedistributionStatus (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4IBGPMaxPathsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4IBGPMaxPaths (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4EBGPMaxPathsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4EBGPMaxPaths (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4EIBGPMaxPathsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4EIBGPMaxPaths (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4FourByteASNSupportStatusSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4FourByteASNSupportStatus (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4FourByteASNotationTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4FourByteASNotationType (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4VpnLabelAllocPolicySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4VpnLabelAllocPolicy (pMultiData->i4_SLongValue));
}

INT4 Fsbgp4MacMobDuplicationTimeIntervalSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
#ifdef EVPN_WANTED
        return(nmhSetFsbgp4MacMobDuplicationTimeInterval(pMultiData->i4_SLongValue));
#else
        UNUSED_PARAM(pMultiData);
        return SNMP_SUCCESS;
#endif
}


INT4 Fsbgp4MaxMacMovesSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
#ifdef EVPN_WANTED
        return(nmhSetFsbgp4MaxMacMoves(pMultiData->i4_SLongValue));
#else
        UNUSED_PARAM(pMultiData);
        return SNMP_SUCCESS;
#endif
}

INT4 Fsbgp4VpnRouteLeakStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhSetFsbgp4VpnRouteLeakStatus(pMultiData->i4_SLongValue));
}


INT4
Fsbgp4GlobalAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4GlobalAdminStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4LocalAsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4LocalAs (pu4Error, pMultiData->u4_ULongValue));
}

INT4
Fsbgp4IdentifierTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4Identifier (pu4Error, pMultiData->u4_ULongValue));
}

INT4
Fsbgp4SynchronizationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4Synchronization
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4DefaultLocalPrefTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4DefaultLocalPref
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
Fsbgp4AdvtNonBgpRtTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4AdvtNonBgpRt (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4TraceEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4TraceEnable (pu4Error, pMultiData->u4_ULongValue));
}

INT4
Fsbgp4DebugEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4DebugEnable (pu4Error, pMultiData->u4_ULongValue));
}

INT4
Fsbgp4OverlappingRouteTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4OverlappingRoute
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4MaxPeerEntryTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4MaxPeerEntry (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4MaxNoofRoutesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4MaxNoofRoutes (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4AlwaysCompareMEDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4AlwaysCompareMED
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4DefaultOriginateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4DefaultOriginate
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4DefaultIpv4UniCastTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4DefaultIpv4UniCast
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4GRAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4GRAdminStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4GRRestartTimeIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4GRRestartTimeInterval
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4GRSelectionDeferralTimeIntervalTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4GRSelectionDeferralTimeInterval
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4GRStaleTimeIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4GRStaleTimeInterval
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RestartSupportTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4RestartSupport
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RestartReasonTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4RestartReason (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4IsTrapEnabledTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4IsTrapEnabled (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4NextHopProcessingIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4NextHopProcessingInterval
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4IBGPRedistributionStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4IBGPRedistributionStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4IBGPMaxPathsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4IBGPMaxPaths (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4EBGPMaxPathsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4EBGPMaxPaths (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4EIBGPMaxPathsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4EIBGPMaxPaths (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4FourByteASNSupportStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4FourByteASNSupportStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4FourByteASNotationTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4FourByteASNotationType
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4VpnLabelAllocPolicyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4VpnLabelAllocPolicy
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4 Fsbgp4MacMobDuplicationTimeIntervalTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
#ifdef EVPN_WANTED
        return(nmhTestv2Fsbgp4MacMobDuplicationTimeInterval(pu4Error, pMultiData->i4_SLongValue));
#else
        UNUSED_PARAM(pu4Error);
        UNUSED_PARAM(pMultiData);
        return SNMP_SUCCESS;
#endif
}


INT4 Fsbgp4MaxMacMovesTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
#ifdef EVPN_WANTED
        return(nmhTestv2Fsbgp4MaxMacMoves(pu4Error, pMultiData->i4_SLongValue));
#else
        UNUSED_PARAM(pu4Error);
        UNUSED_PARAM(pMultiData);
        return SNMP_SUCCESS;
#endif
}

INT4 Fsbgp4VpnRouteLeakStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhTestv2Fsbgp4VpnRouteLeakStatus(pu4Error, pMultiData->i4_SLongValue));
}


INT4
Fsbgp4GlobalAdminStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4GlobalAdminStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4LocalAsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4LocalAs (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4IdentifierDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4Identifier (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4SynchronizationDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4Synchronization
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4DefaultLocalPrefDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4DefaultLocalPref
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4AdvtNonBgpRtDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4AdvtNonBgpRt
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4TraceEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4TraceEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4DebugEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4DebugEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4OverlappingRouteDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4OverlappingRoute
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4MaxPeerEntryDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MaxPeerEntry
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4MaxNoofRoutesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MaxNoofRoutes
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4AlwaysCompareMEDDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4AlwaysCompareMED
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4DefaultOriginateDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4DefaultOriginate
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4DefaultIpv4UniCastDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4DefaultIpv4UniCast
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4GRAdminStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4GRAdminStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4GRRestartTimeIntervalDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4GRRestartTimeInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4GRSelectionDeferralTimeIntervalDep (UINT4 *pu4Error,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4GRSelectionDeferralTimeInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4GRStaleTimeIntervalDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4GRStaleTimeInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4RestartSupportDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RestartSupport
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4RestartReasonDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RestartReason
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4IsTrapEnabledDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4IsTrapEnabled
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4NextHopProcessingIntervalDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4NextHopProcessingInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4IBGPRedistributionStatusDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4IBGPRedistributionStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4IBGPMaxPathsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4IBGPMaxPaths
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4EBGPMaxPathsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4EBGPMaxPaths
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4EIBGPMaxPathsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4EIBGPMaxPaths
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4FourByteASNSupportStatusDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4FourByteASNSupportStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4FourByteASNotationTypeDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4FourByteASNotationType
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4VpnLabelAllocPolicyDep (UINT4 *pu4Error,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4VpnLabelAllocPolicy
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 Fsbgp4MacMobDuplicationTimeIntervalDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
#ifdef EVPN_WANTED
        return(nmhDepv2Fsbgp4MacMobDuplicationTimeInterval(pu4Error, pSnmpIndexList, pSnmpvarbinds));
#else
        UNUSED_PARAM(pu4Error);
        UNUSED_PARAM(pSnmpIndexList);
        UNUSED_PARAM(pSnmpvarbinds);
        return SNMP_SUCCESS;
#endif
}

INT4 Fsbgp4MaxMacMovesDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
#ifdef EVPN_WANTED
        return(nmhDepv2Fsbgp4MaxMacMoves(pu4Error, pSnmpIndexList, pSnmpvarbinds));
#else
        UNUSED_PARAM(pu4Error);
        UNUSED_PARAM(pSnmpIndexList);
        UNUSED_PARAM(pSnmpvarbinds);
        return SNMP_SUCCESS;
#endif
}


INT4 Fsbgp4VpnRouteLeakStatusDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2Fsbgp4VpnRouteLeakStatus(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4
GetNextIndexFsbgp4PeerExtTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4PeerExtTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4PeerExtTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4PeerExtConfigurePeerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4PeerExtTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4PeerExtConfigurePeer
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4PeerExtPeerRemoteAsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4PeerExtTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4PeerExtPeerRemoteAs
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4PeerExtEBGPMultiHopGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4PeerExtTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4PeerExtEBGPMultiHop
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4PeerExtNextHopSelfGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4PeerExtTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4PeerExtNextHopSelf
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4PeerExtConnSrcIfIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4PeerExtTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4PeerExtConnSrcIfId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4PeerExtRflClientGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4PeerExtTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4PeerExtRflClient (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4PeerExtConfigurePeerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4PeerExtConfigurePeer
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4PeerExtPeerRemoteAsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4PeerExtPeerRemoteAs
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
Fsbgp4PeerExtEBGPMultiHopSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4PeerExtEBGPMultiHop
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4PeerExtNextHopSelfSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4PeerExtNextHopSelf
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4PeerExtRflClientSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4PeerExtRflClient (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsbgp4PeerExtConfigurePeerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4PeerExtConfigurePeer (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
Fsbgp4PeerExtPeerRemoteAsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4PeerExtPeerRemoteAs (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
Fsbgp4PeerExtEBGPMultiHopTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4PeerExtEBGPMultiHop (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
Fsbgp4PeerExtNextHopSelfTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4PeerExtNextHopSelf (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
Fsbgp4PeerExtRflClientTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4PeerExtRflClient (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Fsbgp4PeerExtTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4PeerExtTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MEDTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MEDTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MEDTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4MEDAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MEDTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4MEDAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4MEDRemoteASGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MEDTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4MEDRemoteAS (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4MEDIPAddrPrefixGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MEDTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4MEDIPAddrPrefix (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4MEDIPAddrPrefixLenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MEDTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4MEDIPAddrPrefixLen
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4MEDIntermediateASGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MEDTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4MEDIntermediateAS (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
Fsbgp4MEDDirectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MEDTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4MEDDirection (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4MEDValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MEDTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4MEDValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4MEDPreferenceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MEDTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4MEDPreference (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4MEDAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4MEDAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
Fsbgp4MEDRemoteASSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4MEDRemoteAS (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->u4_ULongValue));

}

INT4
Fsbgp4MEDIPAddrPrefixSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4MEDIPAddrPrefix (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
Fsbgp4MEDIPAddrPrefixLenSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4MEDIPAddrPrefixLen
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4MEDIntermediateASSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4MEDIntermediateAS (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
Fsbgp4MEDDirectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4MEDDirection (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
Fsbgp4MEDValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4MEDValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->u4_ULongValue));

}

INT4
Fsbgp4MEDPreferenceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4MEDPreference (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
Fsbgp4MEDAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4MEDAdminStatus (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Fsbgp4MEDRemoteASTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4MEDRemoteAS (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Fsbgp4MEDIPAddrPrefixTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4MEDIPAddrPrefix (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->u4_ULongValue));

}

INT4
Fsbgp4MEDIPAddrPrefixLenTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4MEDIPAddrPrefixLen (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
Fsbgp4MEDIntermediateASTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4MEDIntermediateAS (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->pOctetStrValue));

}

INT4
Fsbgp4MEDDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4MEDDirection (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
Fsbgp4MEDValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4MEDValue (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->u4_ULongValue));

}

INT4
Fsbgp4MEDPreferenceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4MEDPreference (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsbgp4MEDTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MEDTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4LocalPrefTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4LocalPrefTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4LocalPrefTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4LocalPrefAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4LocalPrefTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4LocalPrefAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4LocalPrefRemoteASGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4LocalPrefTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4LocalPrefRemoteAS (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4LocalPrefIPAddrPrefixGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4LocalPrefTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4LocalPrefIPAddrPrefix
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4LocalPrefIPAddrPrefixLenGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4LocalPrefTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4LocalPrefIPAddrPrefixLen
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4LocalPrefIntermediateASGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4LocalPrefTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4LocalPrefIntermediateAS
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4LocalPrefDirectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4LocalPrefTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4LocalPrefDirection
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4LocalPrefValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4LocalPrefTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4LocalPrefValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4LocalPrefPreferenceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4LocalPrefTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4LocalPrefPreference
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4LocalPrefAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4LocalPrefAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4LocalPrefRemoteASSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4LocalPrefRemoteAS (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
Fsbgp4LocalPrefIPAddrPrefixSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4LocalPrefIPAddrPrefix
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
Fsbgp4LocalPrefIPAddrPrefixLenSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsbgp4LocalPrefIPAddrPrefixLen
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4LocalPrefIntermediateASSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsbgp4LocalPrefIntermediateAS
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4LocalPrefDirectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4LocalPrefDirection
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4LocalPrefValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4LocalPrefValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Fsbgp4LocalPrefPreferenceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4LocalPrefPreference
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4LocalPrefAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4LocalPrefAdminStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
Fsbgp4LocalPrefRemoteASTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4LocalPrefRemoteAS (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
Fsbgp4LocalPrefIPAddrPrefixTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4LocalPrefIPAddrPrefix (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
Fsbgp4LocalPrefIPAddrPrefixLenTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4LocalPrefIPAddrPrefixLen (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
Fsbgp4LocalPrefIntermediateASTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4LocalPrefIntermediateAS (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->
                                                    pOctetStrValue));

}

INT4
Fsbgp4LocalPrefDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4LocalPrefDirection (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
Fsbgp4LocalPrefValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4LocalPrefValue (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
Fsbgp4LocalPrefPreferenceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4LocalPrefPreference (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
Fsbgp4LocalPrefTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4LocalPrefTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4UpdateFilterTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4UpdateFilterTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4UpdateFilterTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4UpdateFilterAdminStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4UpdateFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4UpdateFilterAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4UpdateFilterRemoteASGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4UpdateFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4UpdateFilterRemoteAS
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4UpdateFilterIPAddrPrefixGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4UpdateFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4UpdateFilterIPAddrPrefix
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4UpdateFilterIPAddrPrefixLenGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4UpdateFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4UpdateFilterIPAddrPrefixLen
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4UpdateFilterIntermediateASGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4UpdateFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4UpdateFilterIntermediateAS
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4UpdateFilterDirectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4UpdateFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4UpdateFilterDirection
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4UpdateFilterActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4UpdateFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4UpdateFilterAction
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4UpdateFilterAdminStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsbgp4UpdateFilterAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4UpdateFilterRemoteASSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4UpdateFilterRemoteAS
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
Fsbgp4UpdateFilterIPAddrPrefixSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsbgp4UpdateFilterIPAddrPrefix
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
Fsbgp4UpdateFilterIPAddrPrefixLenSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsbgp4UpdateFilterIPAddrPrefixLen
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4UpdateFilterIntermediateASSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsbgp4UpdateFilterIntermediateAS
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4UpdateFilterDirectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4UpdateFilterDirection
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4UpdateFilterActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4UpdateFilterAction
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4UpdateFilterAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4UpdateFilterAdminStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
Fsbgp4UpdateFilterRemoteASTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4UpdateFilterRemoteAS (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
Fsbgp4UpdateFilterIPAddrPrefixTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4UpdateFilterIPAddrPrefix (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     u4_ULongValue));

}

INT4
Fsbgp4UpdateFilterIPAddrPrefixLenTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4UpdateFilterIPAddrPrefixLen (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
Fsbgp4UpdateFilterIntermediateASTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4UpdateFilterIntermediateAS (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       pOctetStrValue));

}

INT4
Fsbgp4UpdateFilterDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4UpdateFilterDirection (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Fsbgp4UpdateFilterActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4UpdateFilterAction (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
Fsbgp4UpdateFilterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4UpdateFilterTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4AggregateTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4AggregateTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4AggregateTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4AggregateAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4AggregateTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4AggregateAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4AggregateIPAddrPrefixGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4AggregateTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4AggregateIPAddrPrefix
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4AggregateIPAddrPrefixLenGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4AggregateTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4AggregateIPAddrPrefixLen
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4AggregateAdvertiseGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4AggregateTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4AggregateAdvertise
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4AggregateAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4AggregateAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4AggregateIPAddrPrefixSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4AggregateIPAddrPrefix
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
Fsbgp4AggregateIPAddrPrefixLenSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsbgp4AggregateIPAddrPrefixLen
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4AggregateAdvertiseSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4AggregateAdvertise
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4AggregateAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4AggregateAdminStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
Fsbgp4AggregateIPAddrPrefixTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4AggregateIPAddrPrefix (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
Fsbgp4AggregateIPAddrPrefixLenTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4AggregateIPAddrPrefixLen (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
Fsbgp4AggregateAdvertiseTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4AggregateAdvertise (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
Fsbgp4AggregateTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4AggregateTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4RRDAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RRDAdminStatus (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4RRDProtoMaskForEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RRDProtoMaskForEnable (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4RRDSrcProtoMaskForDisableGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RRDSrcProtoMaskForDisable
            (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4RRDDefaultMetricGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RRDDefaultMetric (&(pMultiData->u4_ULongValue)));
}

INT4
Fsbgp4RRDRouteMapNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RRDRouteMapName (pMultiData->pOctetStrValue));
}

INT4
Fsbgp4RRDMatchTypeEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RRDMatchTypeEnable (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4RRDMatchTypeDisableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RRDMatchTypeDisable (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4RRDAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4RRDAdminStatus (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RRDProtoMaskForEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4RRDProtoMaskForEnable (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RRDSrcProtoMaskForDisableSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4RRDSrcProtoMaskForDisable (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RRDDefaultMetricSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4RRDDefaultMetric (pMultiData->u4_ULongValue));
}

INT4
Fsbgp4RRDRouteMapNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4RRDRouteMapName (pMultiData->pOctetStrValue));
}

INT4
Fsbgp4RRDMatchTypeEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4RRDMatchTypeEnable (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RRDMatchTypeDisableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4RRDMatchTypeDisable (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RRDAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4RRDAdminStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RRDProtoMaskForEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4RRDProtoMaskForEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RRDSrcProtoMaskForDisableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4RRDSrcProtoMaskForDisable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RRDDefaultMetricTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4RRDDefaultMetric
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
Fsbgp4RRDRouteMapNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4RRDRouteMapName
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
Fsbgp4RRDMatchTypeEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4RRDMatchTypeEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RRDMatchTypeDisableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4RRDMatchTypeDisable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RRDAdminStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RRDAdminStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4RRDProtoMaskForEnableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RRDProtoMaskForEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4RRDSrcProtoMaskForDisableDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RRDSrcProtoMaskForDisable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4RRDDefaultMetricDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RRDDefaultMetric
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4RRDRouteMapNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RRDRouteMapName
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4RRDMatchTypeEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RRDMatchTypeEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4RRDMatchTypeDisableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RRDMatchTypeDisable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4RRDMetricTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4RRDMetricTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4RRDMetricTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsBgp4RRDMetricValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RRDMetricTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4RRDMetricValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsBgp4RRDMetricValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsBgp4RRDMetricValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
FsBgp4RRDMetricValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4RRDMetricValue (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
Fsbgp4RRDMetricTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RRDMetricTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4ImportRouteTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4ImportRouteTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             &(pNextMultiIndex->pIndex[5].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4ImportRouteTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pFirstMultiIndex->pIndex[5].i4_SLongValue,
             &(pNextMultiIndex->pIndex[5].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4ImportRouteActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4ImportRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4ImportRouteAction (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiIndex->pIndex[4].i4_SLongValue,
                                           pMultiIndex->pIndex[5].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4ImportRouteActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4ImportRouteAction (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiIndex->pIndex[4].i4_SLongValue,
                                           pMultiIndex->pIndex[5].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Fsbgp4ImportRouteActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4ImportRouteAction (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[1].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[2].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[3].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[4].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[5].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
Fsbgp4ImportRouteTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4ImportRouteTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4FsmTransitionHistTable (tSnmpIndex * pFirstMultiIndex,
                                          tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4FsmTransitionHistTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4FsmTransitionHistTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4FsmTransitionHistGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4FsmTransitionHistTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4FsmTransitionHist (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
Fsbgp4RflbgpClusterIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RflbgpClusterId (pMultiData->pOctetStrValue));
}

INT4
Fsbgp4RflRflSupportGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RflRflSupport (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4RflbgpClusterIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4RflbgpClusterId (pMultiData->pOctetStrValue));
}

INT4
Fsbgp4RflRflSupportSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4RflRflSupport (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RflbgpClusterIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4RflbgpClusterId
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
Fsbgp4RflRflSupportTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4RflRflSupport (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RflbgpClusterIdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RflbgpClusterId
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4RflRflSupportDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RflRflSupport
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4RflRouteReflectorTable (tSnmpIndex * pFirstMultiIndex,
                                          tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4RflRouteReflectorTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4RflRouteReflectorTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4RflPathAttrOriginatorIdGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RflRouteReflectorTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RflPathAttrOriginatorId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4RflPathAttrClusterListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RflRouteReflectorTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RflPathAttrClusterList
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4RfdCutOffGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RfdCutOff (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4RfdReuseGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RfdReuse (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4RfdCeilingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RfdCeiling (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4RfdMaxHoldDownTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RfdMaxHoldDownTime (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4RfdDecayHalfLifeTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RfdDecayHalfLifeTime (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4RfdDecayTimerGranularityGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RfdDecayTimerGranularity
            (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4RfdReuseTimerGranularityGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RfdReuseTimerGranularity
            (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4RfdReuseIndxArraySizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RfdReuseIndxArraySize (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4RfdAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RfdAdminStatus (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4RfdCutOffSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4RfdCutOff (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RfdReuseSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4RfdReuse (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RfdMaxHoldDownTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4RfdMaxHoldDownTime (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RfdDecayHalfLifeTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4RfdDecayHalfLifeTime (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RfdDecayTimerGranularitySet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4RfdDecayTimerGranularity (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RfdReuseTimerGranularitySet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4RfdReuseTimerGranularity (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RfdReuseIndxArraySizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4RfdReuseIndxArraySize (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RfdAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4RfdAdminStatus (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RfdCutOffTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4RfdCutOff (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RfdReuseTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4RfdReuse (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RfdMaxHoldDownTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4RfdMaxHoldDownTime
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RfdDecayHalfLifeTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4RfdDecayHalfLifeTime
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RfdDecayTimerGranularityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4RfdDecayTimerGranularity
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RfdReuseTimerGranularityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4RfdReuseTimerGranularity
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RfdReuseIndxArraySizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4RfdReuseIndxArraySize
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RfdAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4RfdAdminStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RfdCutOffDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RfdCutOff (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4RfdReuseDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RfdReuse (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4RfdMaxHoldDownTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RfdMaxHoldDownTime
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4RfdDecayHalfLifeTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RfdDecayHalfLifeTime
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4RfdDecayTimerGranularityDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RfdDecayTimerGranularity
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4RfdReuseTimerGranularityDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RfdReuseTimerGranularity
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4RfdReuseIndxArraySizeDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RfdReuseIndxArraySize
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4RfdAdminStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RfdAdminStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4RfdRtDampHistTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4RfdRtDampHistTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4RfdRtDampHistTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4RfdRtFomGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RfdRtDampHistTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RfdRtFom (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].i4_SLongValue,
                                  pMultiIndex->pIndex[2].u4_ULongValue,
                                  pMultiIndex->pIndex[3].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4RfdRtLastUpdtTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RfdRtDampHistTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RfdRtLastUpdtTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4RfdRtStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RfdRtDampHistTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RfdRtState (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiIndex->pIndex[2].u4_ULongValue,
                                    pMultiIndex->pIndex[3].i4_SLongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4RfdRtStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RfdRtDampHistTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RfdRtStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFsbgp4RfdPeerDampHistTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4RfdPeerDampHistTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4RfdPeerDampHistTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4RfdPeerFomGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RfdPeerDampHistTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RfdPeerFom (pMultiIndex->pIndex[0].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4RfdPeerLastUpdtTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RfdPeerDampHistTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RfdPeerLastUpdtTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4RfdPeerStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RfdPeerDampHistTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RfdPeerState (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4RfdPeerStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RfdPeerDampHistTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RfdPeerStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFsbgp4RfdRtsReuseListTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4RfdRtsReuseListTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4RfdRtsReuseListTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4RfdRtReuseListRtFomGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RfdRtsReuseListTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RfdRtReuseListRtFom
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4RfdRtReuseListRtLastUpdtTimeGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RfdRtsReuseListTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RfdRtReuseListRtLastUpdtTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4RfdRtReuseListRtStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RfdRtsReuseListTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RfdRtReuseListRtState
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4RfdRtReuseListRtStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RfdRtsReuseListTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RfdRtReuseListRtStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFsbgp4RfdPeerReuseListTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4RfdPeerReuseListTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4RfdPeerReuseListTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4RfdPeerReuseListPeerFomGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RfdPeerReuseListTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RfdPeerReuseListPeerFom
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4RfdPeerReuseListLastUpdtTimeGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RfdPeerReuseListTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RfdPeerReuseListLastUpdtTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4RfdPeerReuseListPeerStateGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RfdPeerReuseListTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RfdPeerReuseListPeerState
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4RfdPeerReuseListPeerStatusGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RfdPeerReuseListTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RfdPeerReuseListPeerStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4CommMaxInFTblEntriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4CommMaxInFTblEntries (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4CommMaxOutFTblEntriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4CommMaxOutFTblEntries (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4CommMaxInFTblEntriesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4CommMaxInFTblEntries (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4CommMaxOutFTblEntriesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4CommMaxOutFTblEntries (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4CommMaxInFTblEntriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4CommMaxInFTblEntries
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4CommMaxOutFTblEntriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4CommMaxOutFTblEntries
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4CommMaxInFTblEntriesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4CommMaxInFTblEntries
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4CommMaxOutFTblEntriesDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4CommMaxOutFTblEntries
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4CommRouteAddCommTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4CommRouteAddCommTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4CommRouteAddCommTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4AddCommRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4CommRouteAddCommTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4AddCommRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4AddCommRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4AddCommRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsbgp4AddCommRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4AddCommRowStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[2].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Fsbgp4CommRouteAddCommTableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4CommRouteAddCommTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4CommRouteDeleteCommTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4CommRouteDeleteCommTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4CommRouteDeleteCommTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4DeleteCommRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4CommRouteDeleteCommTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4DeleteCommRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4DeleteCommRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4DeleteCommRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4DeleteCommRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4DeleteCommRowStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[1].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[2].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
Fsbgp4CommRouteDeleteCommTableDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4CommRouteDeleteCommTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4CommRouteCommSetStatusTable (tSnmpIndex * pFirstMultiIndex,
                                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4CommRouteCommSetStatusTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4CommRouteCommSetStatusTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4CommSetStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4CommRouteCommSetStatusTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4CommSetStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4CommSetStatusRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4CommRouteCommSetStatusTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4CommSetStatusRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4CommSetStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4CommSetStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
Fsbgp4CommSetStatusRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4CommSetStatusRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4CommSetStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4CommSetStatus (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsbgp4CommSetStatusRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4CommSetStatusRowStatus (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
Fsbgp4CommRouteCommSetStatusTableDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4CommRouteCommSetStatusTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4CommPeerSendStatusTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4CommPeerSendStatusTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4CommPeerSendStatusTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4CommSendStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4CommPeerSendStatusTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4CommSendStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4CommPeerSendRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4CommPeerSendStatusTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4CommPeerSendRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4CommSendStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4CommSendStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
Fsbgp4CommPeerSendRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4CommPeerSendRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4CommSendStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4CommSendStatus (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Fsbgp4CommPeerSendRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4CommPeerSendRowStatus (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Fsbgp4CommPeerSendStatusTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4CommPeerSendStatusTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4CommInFilterTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4CommInFilterTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4CommInFilterTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4CommIncomingFilterStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4CommInFilterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4CommIncomingFilterStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4InFilterRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4CommInFilterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4InFilterRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4CommIncomingFilterStatusSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsbgp4CommIncomingFilterStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4InFilterRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4InFilterRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Fsbgp4CommIncomingFilterStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4CommIncomingFilterStatus (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
Fsbgp4InFilterRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4InFilterRowStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
Fsbgp4CommInFilterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4CommInFilterTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4CommOutFilterTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4CommOutFilterTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4CommOutFilterTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4CommOutgoingFilterStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4CommOutFilterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4CommOutgoingFilterStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4OutFilterRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4CommOutFilterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4OutFilterRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4CommOutgoingFilterStatusSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsbgp4CommOutgoingFilterStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4OutFilterRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4OutFilterRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4CommOutgoingFilterStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4CommOutgoingFilterStatus (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
Fsbgp4OutFilterRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4OutFilterRowStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
Fsbgp4CommOutFilterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4CommOutFilterTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4CommReceivedRouteCommTable (tSnmpIndex * pFirstMultiIndex,
                                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4CommReceivedRouteCommTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4CommReceivedRouteCommTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4ReceivedRouteCommPathGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4CommReceivedRouteCommTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4ReceivedRouteCommPath
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4ExtCommMaxInFTblEntriesGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4ExtCommMaxInFTblEntries (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4ExtCommMaxOutFTblEntriesGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4ExtCommMaxOutFTblEntries
            (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4ExtCommMaxInFTblEntriesSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4ExtCommMaxInFTblEntries (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4ExtCommMaxOutFTblEntriesSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4ExtCommMaxOutFTblEntries (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4ExtCommMaxInFTblEntriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4ExtCommMaxInFTblEntries
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4ExtCommMaxOutFTblEntriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4ExtCommMaxOutFTblEntries
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4ExtCommMaxInFTblEntriesDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4ExtCommMaxInFTblEntries
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4ExtCommMaxOutFTblEntriesDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4ExtCommMaxOutFTblEntries
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4ExtCommRouteAddExtCommTable (tSnmpIndex * pFirstMultiIndex,
                                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4ExtCommRouteAddExtCommTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4ExtCommRouteAddExtCommTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4AddExtCommRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4ExtCommRouteAddExtCommTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4AddExtCommRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4AddExtCommRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4AddExtCommRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4AddExtCommRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4AddExtCommRowStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[1].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[2].
                                                pOctetStrValue,
                                                pMultiData->i4_SLongValue));

}

INT4
Fsbgp4ExtCommRouteAddExtCommTableDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4ExtCommRouteAddExtCommTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4ExtCommRouteDeleteExtCommTable (tSnmpIndex * pFirstMultiIndex,
                                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4ExtCommRouteDeleteExtCommTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4ExtCommRouteDeleteExtCommTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4DeleteExtCommRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4ExtCommRouteDeleteExtCommTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4DeleteExtCommRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4DeleteExtCommRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4DeleteExtCommRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4DeleteExtCommRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4DeleteExtCommRowStatus (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[2].
                                                   pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
Fsbgp4ExtCommRouteDeleteExtCommTableDep (UINT4 *pu4Error,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4ExtCommRouteDeleteExtCommTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4ExtCommRouteExtCommSetStatusTable (tSnmpIndex *
                                                     pFirstMultiIndex,
                                                     tSnmpIndex *
                                                     pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4ExtCommRouteExtCommSetStatusTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4ExtCommRouteExtCommSetStatusTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4ExtCommSetStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4ExtCommRouteExtCommSetStatusTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4ExtCommSetStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4ExtCommSetStatusRowStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4ExtCommRouteExtCommSetStatusTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4ExtCommSetStatusRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4ExtCommSetStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4ExtCommSetStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsbgp4ExtCommSetStatusRowStatusSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsbgp4ExtCommSetStatusRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4ExtCommSetStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4ExtCommSetStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Fsbgp4ExtCommSetStatusRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4ExtCommSetStatusRowStatus (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[1].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
Fsbgp4ExtCommRouteExtCommSetStatusTableDep (UINT4 *pu4Error,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4ExtCommRouteExtCommSetStatusTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4ExtCommPeerSendStatusTable (tSnmpIndex * pFirstMultiIndex,
                                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4ExtCommPeerSendStatusTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4ExtCommPeerSendStatusTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4ExtCommPeerSendStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4ExtCommPeerSendStatusTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4ExtCommPeerSendStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4ExtCommPeerSendStatusRowStatusGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4ExtCommPeerSendStatusTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4ExtCommPeerSendStatusRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4ExtCommPeerSendStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4ExtCommPeerSendStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4ExtCommPeerSendStatusRowStatusSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsbgp4ExtCommPeerSendStatusRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4ExtCommPeerSendStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4ExtCommPeerSendStatus (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Fsbgp4ExtCommPeerSendStatusRowStatusTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4ExtCommPeerSendStatusRowStatus (pu4Error,
                                                           pMultiIndex->pIndex
                                                           [0].u4_ULongValue,
                                                           pMultiData->
                                                           i4_SLongValue));

}

INT4
Fsbgp4ExtCommPeerSendStatusTableDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4ExtCommPeerSendStatusTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4ExtCommInFilterTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4ExtCommInFilterTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4ExtCommInFilterTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4ExtCommIncomingFilterStatusGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4ExtCommInFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4ExtCommIncomingFilterStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4ExtCommInFilterRowStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4ExtCommInFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4ExtCommInFilterRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4ExtCommIncomingFilterStatusSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsbgp4ExtCommIncomingFilterStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4ExtCommInFilterRowStatusSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsbgp4ExtCommInFilterRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4ExtCommIncomingFilterStatusTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4ExtCommIncomingFilterStatus (pu4Error,
                                                        pMultiIndex->pIndex
                                                        [0].pOctetStrValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
Fsbgp4ExtCommInFilterRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4ExtCommInFilterRowStatus (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     pOctetStrValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
Fsbgp4ExtCommInFilterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4ExtCommInFilterTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4ExtCommOutFilterTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4ExtCommOutFilterTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4ExtCommOutFilterTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4ExtCommOutgoingFilterStatusGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4ExtCommOutFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4ExtCommOutgoingFilterStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4ExtCommOutFilterRowStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4ExtCommOutFilterTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4ExtCommOutFilterRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4ExtCommOutgoingFilterStatusSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsbgp4ExtCommOutgoingFilterStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4ExtCommOutFilterRowStatusSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsbgp4ExtCommOutFilterRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4ExtCommOutgoingFilterStatusTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4ExtCommOutgoingFilterStatus (pu4Error,
                                                        pMultiIndex->pIndex
                                                        [0].pOctetStrValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
Fsbgp4ExtCommOutFilterRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4ExtCommOutFilterRowStatus (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      pOctetStrValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
Fsbgp4ExtCommOutFilterTableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4ExtCommOutFilterTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4PeerLinkBwTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4PeerLinkBwTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4PeerLinkBwTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4LinkBandWidthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4PeerLinkBwTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4LinkBandWidth (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4PeerLinkBwRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4PeerLinkBwTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4PeerLinkBwRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4LinkBandWidthSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4LinkBandWidth (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
Fsbgp4PeerLinkBwRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4PeerLinkBwRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4LinkBandWidthTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4LinkBandWidth (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
Fsbgp4PeerLinkBwRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4PeerLinkBwRowStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
Fsbgp4PeerLinkBwTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4PeerLinkBwTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4ExtCommReceivedRouteExtCommTable (tSnmpIndex *
                                                    pFirstMultiIndex,
                                                    tSnmpIndex *
                                                    pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4ExtCommReceivedRouteExtCommTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4ExtCommReceivedRouteExtCommTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4ReceivedRouteExtCommPathGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4ExtCommReceivedRouteExtCommTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4ReceivedRouteExtCommPath
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4CapabilitySupportAvailableGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4CapabilitySupportAvailable
            (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4MaxCapsPerPeerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4MaxCapsPerPeer (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4MaxInstancesPerCapGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4MaxInstancesPerCap (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4MaxCapDataSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4MaxCapDataSize (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4CapabilitySupportAvailableSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4CapabilitySupportAvailable (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4MaxCapsPerPeerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4MaxCapsPerPeer (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4MaxInstancesPerCapSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4MaxInstancesPerCap (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4MaxCapDataSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4MaxCapDataSize (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4CapabilitySupportAvailableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4CapabilitySupportAvailable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4MaxCapsPerPeerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4MaxCapsPerPeer
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4MaxInstancesPerCapTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4MaxInstancesPerCap
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4MaxCapDataSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4MaxCapDataSize
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4CapabilitySupportAvailableDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4CapabilitySupportAvailable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4MaxCapsPerPeerDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MaxCapsPerPeer
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4MaxInstancesPerCapDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MaxInstancesPerCap
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4MaxCapDataSizeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MaxCapDataSize
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4CapSupportedCapsTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4CapSupportedCapsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4CapSupportedCapsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4SupportedCapabilityLengthGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4CapSupportedCapsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4SupportedCapabilityLength
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4SupportedCapabilityValueGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4CapSupportedCapsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4SupportedCapabilityValue
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4CapSupportedCapsRowStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4CapSupportedCapsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4CapSupportedCapsRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4SupportedCapabilityLengthSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsbgp4SupportedCapabilityLength
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4SupportedCapabilityValueSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsbgp4SupportedCapabilityValue
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4CapSupportedCapsRowStatusSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsbgp4CapSupportedCapsRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4SupportedCapabilityLengthTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4SupportedCapabilityLength (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[1].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[2].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
Fsbgp4SupportedCapabilityValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4SupportedCapabilityValue (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[2].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     pOctetStrValue));

}

INT4
Fsbgp4CapSupportedCapsRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4CapSupportedCapsRowStatus (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[1].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[2].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
Fsbgp4CapSupportedCapsTableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4CapSupportedCapsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4StrictCapabilityMatchTable (tSnmpIndex * pFirstMultiIndex,
                                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4StrictCapabilityMatchTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4StrictCapabilityMatchTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4StrictCapabilityMatchGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4StrictCapabilityMatchTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4StrictCapabilityMatch
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4StrictCapabilityMatchSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4StrictCapabilityMatch
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4StrictCapabilityMatchTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4StrictCapabilityMatch (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Fsbgp4StrictCapabilityMatchTableDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4StrictCapabilityMatchTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4CapsAnnouncedTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4CapsAnnouncedTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4CapsAnnouncedTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4PeerCapAnnouncedLengthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4CapsAnnouncedTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4PeerCapAnnouncedLength
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4PeerCapAnnouncedValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4CapsAnnouncedTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4PeerCapAnnouncedValue
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
GetNextIndexFsbgp4CapReceivedCapsTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4CapReceivedCapsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4CapReceivedCapsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4PeerCapReceivedLengthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4CapReceivedCapsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4PeerCapReceivedLength
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4PeerCapReceivedValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4CapReceivedCapsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4PeerCapReceivedValue
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
GetNextIndexFsbgp4CapAcceptedCapsTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4CapAcceptedCapsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4CapAcceptedCapsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4PeerCapAcceptedLengthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4CapAcceptedCapsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4PeerCapAcceptedLength
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4PeerCapAcceptedValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4CapAcceptedCapsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4PeerCapAcceptedValue
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsbgpAscConfedIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgpAscConfedId (&(pMultiData->u4_ULongValue)));
}

INT4
FsbgpAscConfedBestPathCompareMEDGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgpAscConfedBestPathCompareMED
            (&(pMultiData->i4_SLongValue)));
}

INT4
FsbgpAscConfedIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgpAscConfedId (pMultiData->u4_ULongValue));
}

INT4
FsbgpAscConfedBestPathCompareMEDSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgpAscConfedBestPathCompareMED (pMultiData->i4_SLongValue));
}

INT4
FsbgpAscConfedIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsbgpAscConfedId (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsbgpAscConfedBestPathCompareMEDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsbgpAscConfedBestPathCompareMED
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsbgpAscConfedIdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsbgpAscConfedId (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsbgpAscConfedBestPathCompareMEDDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsbgpAscConfedBestPathCompareMED
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgpAscConfedPeerTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgpAscConfedPeerTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgpAscConfedPeerTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsbgpAscConfedPeerStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgpAscConfedPeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgpAscConfedPeerStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsbgpAscConfedPeerStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgpAscConfedPeerStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsbgpAscConfedPeerStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsbgpAscConfedPeerStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsbgpAscConfedPeerTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsbgpAscConfedPeerTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4RtRefreshAllPeerInboundRequestGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4RtRefreshAllPeerInboundRequest
            (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4RtRefreshAllPeerInboundRequestSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4RtRefreshAllPeerInboundRequest
            (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RtRefreshAllPeerInboundRequestTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4RtRefreshAllPeerInboundRequest
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4RtRefreshAllPeerInboundRequestDep (UINT4 *pu4Error,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RtRefreshAllPeerInboundRequest
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4RtRefreshInboundTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4RtRefreshInboundTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4RtRefreshInboundTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4RtRefreshInboundRequestGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RtRefreshInboundTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RtRefreshInboundRequest
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4RtRefreshInboundRequestSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsbgp4RtRefreshInboundRequest
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4RtRefreshInboundRequestTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4RtRefreshInboundRequest (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[1].
                                                    pOctetStrValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
Fsbgp4RtRefreshInboundTableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4RtRefreshInboundTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4RtRefreshStatisticsTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4RtRefreshStatisticsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4RtRefreshStatisticsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4RtRefreshStatisticsRtRefMsgSentCntrGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RtRefreshStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RtRefreshStatisticsRtRefMsgSentCntr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4RtRefreshStatisticsRtRefMsgTxErrCntrGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RtRefreshStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RtRefreshStatisticsRtRefMsgTxErrCntr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4RtRefreshStatisticsRtRefMsgRcvdCntrGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RtRefreshStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RtRefreshStatisticsRtRefMsgRcvdCntr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4RtRefreshStatisticsRtRefMsgInvalidCntrGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4RtRefreshStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4RtRefreshStatisticsRtRefMsgInvalidCntr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexFsbgp4TCPMD5AuthTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4TCPMD5AuthTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4TCPMD5AuthTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4TCPMD5AuthPasswordGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4TCPMD5AuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4TCPMD5AuthPassword
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4TCPMD5AuthPwdSetGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4TCPMD5AuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4TCPMD5AuthPwdSet (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4TCPMD5AuthPasswordSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4TCPMD5AuthPassword
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4TCPMD5AuthPwdSetSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4TCPMD5AuthPwdSet (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsbgp4TCPMD5AuthPasswordTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4TCPMD5AuthPassword (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               pOctetStrValue,
                                               pMultiData->pOctetStrValue));

}

INT4
Fsbgp4TCPMD5AuthPwdSetTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4TCPMD5AuthPwdSet (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             pOctetStrValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Fsbgp4TCPMD5AuthTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4TCPMD5AuthTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Fsbgp4SoftReconfigAllPeerOutboundRequestGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsbgp4SoftReconfigAllPeerOutboundRequest
            (&(pMultiData->i4_SLongValue)));
}

INT4
Fsbgp4SoftReconfigAllPeerOutboundRequestSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsbgp4SoftReconfigAllPeerOutboundRequest
            (pMultiData->i4_SLongValue));
}

INT4
Fsbgp4SoftReconfigAllPeerOutboundRequestTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Fsbgp4SoftReconfigAllPeerOutboundRequest
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Fsbgp4SoftReconfigAllPeerOutboundRequestDep (UINT4 *pu4Error,
                                             tSnmpIndexList * pSnmpIndexList,
                                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4SoftReconfigAllPeerOutboundRequest
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4SoftReconfigOutboundTable (tSnmpIndex * pFirstMultiIndex,
                                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4SoftReconfigOutboundTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4SoftReconfigOutboundTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4SoftReconfigOutboundRequestGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4SoftReconfigOutboundTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4SoftReconfigOutboundRequest
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4SoftReconfigOutboundRequestSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsbgp4SoftReconfigOutboundRequest
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4SoftReconfigOutboundRequestTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4SoftReconfigOutboundRequest (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiIndex->pIndex[1].
                                                        pOctetStrValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
Fsbgp4SoftReconfigOutboundTableDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4SoftReconfigOutboundTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MpeBgpPeerTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeBgpPeerTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeBgpPeerTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpebgpPeerIdentifierGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerIdentifier
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpebgpPeerStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerState (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgpPeerAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgpPeerNegotiatedVersionGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerNegotiatedVersion
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgpPeerLocalAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerLocalAddr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpebgpPeerLocalPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerLocalPort
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgpPeerRemotePortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerRemotePort
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgpPeerRemoteAsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerRemoteAs
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4mpebgpPeerInUpdatesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerInUpdates
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4mpebgpPeerOutUpdatesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerOutUpdates
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4mpebgpPeerInTotalMessagesGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerInTotalMessages
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4mpebgpPeerOutTotalMessagesGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerOutTotalMessages
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4mpebgpPeerLastErrorGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerLastError
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpebgpPeerFsmEstablishedTransitionsGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerFsmEstablishedTransitions
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4mpebgpPeerFsmEstablishedTimeGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerFsmEstablishedTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4mpebgpPeerConnectRetryIntervalGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerConnectRetryInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgpPeerHoldTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerHoldTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgpPeerKeepAliveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerKeepAlive
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgpPeerHoldTimeConfiguredGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerHoldTimeConfigured
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgpPeerKeepAliveConfiguredGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerKeepAliveConfigured
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgpPeerMinASOriginationIntervalGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerMinASOriginationInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgpPeerMinRouteAdvertisementIntervalGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerMinRouteAdvertisementInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgpPeerInUpdateElapsedTimeGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerInUpdateElapsedTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4mpebgpPeerEndOfRIBMarkerSentStatusGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerEndOfRIBMarkerSentStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgpPeerEndOfRIBMarkerReceivedStatusGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerEndOfRIBMarkerReceivedStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgpPeerRestartModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerRestartMode
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerRestartTimeIntervalGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerRestartTimeInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerAllowAutomaticStartGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerAllowAutomaticStart
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerAllowAutomaticStopGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerAllowAutomaticStop
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgpPeerIdleHoldTimeConfiguredGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerIdleHoldTimeConfigured
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeDampPeerOscillationsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeDampPeerOscillations
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerDelayOpenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerDelayOpen (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgpPeerDelayOpenTimeConfiguredGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerDelayOpenTimeConfigured
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerPrefixUpperLimitGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerPrefixUpperLimit
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerTcpConnectRetryCntGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerTcpConnectRetryCnt
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerTcpCurrentConnectRetryCntGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerTcpCurrentConnectRetryCnt
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeIsPeerDampedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeIsPeerDamped (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerSessionAuthStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerSessionAuthStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerTCPAOKeyIdInUseGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerTCPAOKeyIdInUse
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerTCPAOAuthNoMKTDiscardGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerTCPAOAuthNoMKTDiscard
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerTCPAOAuthICMPAcceptGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerTCPAOAuthICMPAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerIpPrefixNameInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerIpPrefixNameIn
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpePeerIpPrefixNameOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerIpPrefixNameOut
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpePeerBfdStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerBfdStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgpPeerHoldAdvtRoutesGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgpPeerHoldAdvtRoutes
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgpPeerAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpebgpPeerAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpebgpPeerConnectRetryIntervalSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpebgpPeerConnectRetryInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpebgpPeerHoldTimeConfiguredSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpebgpPeerHoldTimeConfigured
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpebgpPeerKeepAliveConfiguredSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpebgpPeerKeepAliveConfigured
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpebgpPeerMinASOriginationIntervalSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpebgpPeerMinASOriginationInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpebgpPeerMinRouteAdvertisementIntervalSet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpebgpPeerMinRouteAdvertisementInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerAllowAutomaticStartSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerAllowAutomaticStart
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerAllowAutomaticStopSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerAllowAutomaticStop
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpebgpPeerIdleHoldTimeConfiguredSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpebgpPeerIdleHoldTimeConfigured
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeDampPeerOscillationsSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeDampPeerOscillations
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerDelayOpenSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerDelayOpen (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpebgpPeerDelayOpenTimeConfiguredSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpebgpPeerDelayOpenTimeConfigured
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerPrefixUpperLimitSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerPrefixUpperLimit
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerTcpConnectRetryCntSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerTcpConnectRetryCnt
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerTCPAOAuthNoMKTDiscardSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerTCPAOAuthNoMKTDiscard
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerTCPAOAuthICMPAcceptSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerTCPAOAuthICMPAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerIpPrefixNameInSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerIpPrefixNameIn
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpePeerIpPrefixNameOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerIpPrefixNameOut
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpePeerBfdStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerBfdStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpebgpPeerHoldAdvtRoutesSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpebgpPeerHoldAdvtRoutes
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpebgpPeerAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpebgpPeerAdminStatus (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[1].
                                                  pOctetStrValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpebgpPeerConnectRetryIntervalTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpebgpPeerConnectRetryInterval (pu4Error,
                                                           pMultiIndex->pIndex
                                                           [0].i4_SLongValue,
                                                           pMultiIndex->pIndex
                                                           [1].pOctetStrValue,
                                                           pMultiData->
                                                           i4_SLongValue));

}

INT4
Fsbgp4mpebgpPeerHoldTimeConfiguredTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpebgpPeerHoldTimeConfigured (pu4Error,
                                                         pMultiIndex->pIndex
                                                         [0].i4_SLongValue,
                                                         pMultiIndex->pIndex
                                                         [1].pOctetStrValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
Fsbgp4mpebgpPeerKeepAliveConfiguredTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpebgpPeerKeepAliveConfigured (pu4Error,
                                                          pMultiIndex->pIndex
                                                          [0].i4_SLongValue,
                                                          pMultiIndex->pIndex
                                                          [1].pOctetStrValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
Fsbgp4mpebgpPeerMinASOriginationIntervalTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpebgpPeerMinASOriginationInterval (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               i4_SLongValue,
                                                               pMultiIndex->
                                                               pIndex[1].
                                                               pOctetStrValue,
                                                               pMultiData->
                                                               i4_SLongValue));

}

INT4
Fsbgp4mpebgpPeerMinRouteAdvertisementIntervalTest (UINT4 *pu4Error,
                                                   tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpebgpPeerMinRouteAdvertisementInterval (pu4Error,
                                                                    pMultiIndex->
                                                                    pIndex[0].
                                                                    i4_SLongValue,
                                                                    pMultiIndex->
                                                                    pIndex[1].
                                                                    pOctetStrValue,
                                                                    pMultiData->
                                                                    i4_SLongValue));

}

INT4
Fsbgp4mpePeerAllowAutomaticStartTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerAllowAutomaticStart (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiIndex->pIndex[1].
                                                       pOctetStrValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
Fsbgp4mpePeerAllowAutomaticStopTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerAllowAutomaticStop (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      pOctetStrValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
Fsbgp4mpebgpPeerIdleHoldTimeConfiguredTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpebgpPeerIdleHoldTimeConfigured (pu4Error,
                                                             pMultiIndex->pIndex
                                                             [0].i4_SLongValue,
                                                             pMultiIndex->pIndex
                                                             [1].pOctetStrValue,
                                                             pMultiData->
                                                             i4_SLongValue));

}

INT4
Fsbgp4mpeDampPeerOscillationsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeDampPeerOscillations (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[1].
                                                    pOctetStrValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerDelayOpenTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerDelayOpen (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             pOctetStrValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpebgpPeerDelayOpenTimeConfiguredTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpebgpPeerDelayOpenTimeConfigured (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              i4_SLongValue,
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              pOctetStrValue,
                                                              pMultiData->
                                                              i4_SLongValue));

}

INT4
Fsbgp4mpePeerPrefixUpperLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerPrefixUpperLimit (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[1].
                                                    pOctetStrValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerTcpConnectRetryCntTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerTcpConnectRetryCnt (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      pOctetStrValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
Fsbgp4mpePeerTCPAOAuthNoMKTDiscardTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerTCPAOAuthNoMKTDiscard (pu4Error,
                                                         pMultiIndex->pIndex
                                                         [0].i4_SLongValue,
                                                         pMultiIndex->pIndex
                                                         [1].pOctetStrValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
Fsbgp4mpePeerTCPAOAuthICMPAcceptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerTCPAOAuthICMPAccept (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiIndex->pIndex[1].
                                                       pOctetStrValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
Fsbgp4mpePeerIpPrefixNameInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerIpPrefixNameIn (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[1].
                                                  pOctetStrValue,
                                                  pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpePeerIpPrefixNameOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerIpPrefixNameOut (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   pOctetStrValue,
                                                   pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpePeerBfdStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerBfdStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             pOctetStrValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpebgpPeerHoldAdvtRoutesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpebgpPeerHoldAdvtRoutes (pu4Error,
                                                     pMultiIndex->
                                                     pIndex[0].i4_SLongValue,
                                                     pMultiIndex->
                                                     pIndex[1].pOctetStrValue,
                                                     pMultiData->i4_SLongValue));

}

INT4
Fsbgp4MpeBgpPeerTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MpeBgpPeerTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MpeBgp4PathAttrTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeBgp4PathAttrTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pNextMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeBgp4PathAttrTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pFirstMultiIndex->pIndex[5].pOctetStrValue,
             pNextMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpebgp4PathAttrOriginGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgp4PathAttrOrigin
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgp4PathAttrASPathSegmentGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgp4PathAttrASPathSegment
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpebgp4PathAttrNextHopGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgp4PathAttrNextHop
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpebgp4PathAttrMultiExitDiscGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgp4PathAttrMultiExitDisc
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgp4PathAttrLocalPrefGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgp4PathAttrLocalPref
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgp4PathAttrAtomicAggregateGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgp4PathAttrAtomicAggregate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgp4PathAttrAggregatorASGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgp4PathAttrAggregatorAS
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4mpebgp4PathAttrAggregatorAddrGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgp4PathAttrAggregatorAddr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4mpebgp4PathAttrCalcLocalPrefGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgp4PathAttrCalcLocalPref
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgp4PathAttrBestGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgp4PathAttrBest
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpebgp4PathAttrCommunityGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgp4PathAttrCommunity
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpebgp4PathAttrOriginatorIdGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgp4PathAttrOriginatorId
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpebgp4PathAttrClusterListGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgp4PathAttrClusterList
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpebgp4PathAttrExtCommunityGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgp4PathAttrExtCommunity
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpebgp4PathAttrUnknownGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgp4PathAttrUnknown
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpebgp4PathAttrLabelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgp4PathAttrLabel
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpebgp4PathAttrAS4PathSegmentGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgp4PathAttrAS4PathSegment
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpebgp4PathAttrAggregatorAS4Get (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpebgp4PathAttrAggregatorAS4
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexFsbgp4MpePeerExtTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpePeerExtTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpePeerExtTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpePeerExtConfigurePeerGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtConfigurePeer
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerExtPeerRemoteAsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtPeerRemoteAs
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4mpePeerExtEBGPMultiHopGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtEBGPMultiHop
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerExtEBGPHopLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtEBGPHopLimit
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerExtNextHopSelfGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtNextHopSelf
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerExtRflClientGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtRflClient
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerExtTcpSendBufSizeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtTcpSendBufSize
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerExtTcpRcvBufSizeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtTcpRcvBufSize
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerExtLclAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtLclAddress
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpePeerExtNetworkAddressGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtNetworkAddress
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpePeerExtGatewayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtGateway (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpePeerExtCommSendStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtCommSendStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerExtECommSendStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtECommSendStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerExtPassiveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtPassive (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerExtDefaultOriginateGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtDefaultOriginate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerExtActivateMPCapabilityGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtActivateMPCapability
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerExtDeactivateMPCapabilityGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtDeactivateMPCapability
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerExtMplsVpnVrfAssociatedGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtMplsVpnVrfAssociated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpePeerExtMplsVpnCERouteTargetAdvtGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtMplsVpnCERouteTargetAdvt
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerExtMplsVpnCESiteOfOriginGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtMplsVpnCESiteOfOrigin
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpePeerExtOverrideCapabilityGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerExtOverrideCapability
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpePeerExtConfigurePeerSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtConfigurePeer
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtPeerRemoteAsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtPeerRemoteAs
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
Fsbgp4mpePeerExtEBGPMultiHopSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtEBGPMultiHop
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtEBGPHopLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtEBGPHopLimit
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtNextHopSelfSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtNextHopSelf
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtRflClientSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtRflClient
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtTcpSendBufSizeSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtTcpSendBufSize
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtTcpRcvBufSizeSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtTcpRcvBufSize
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtLclAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtLclAddress
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpePeerExtNetworkAddressSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtNetworkAddress
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpePeerExtGatewaySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtGateway (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpePeerExtCommSendStatusSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtCommSendStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtECommSendStatusSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtECommSendStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtPassiveSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtPassive (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtDefaultOriginateSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtDefaultOriginate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtActivateMPCapabilitySet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtActivateMPCapability
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtDeactivateMPCapabilitySet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtDeactivateMPCapability
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtMplsVpnVrfAssociatedSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtMplsVpnVrfAssociated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpePeerExtMplsVpnCERouteTargetAdvtSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtMplsVpnCERouteTargetAdvt
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtMplsVpnCESiteOfOriginSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtMplsVpnCESiteOfOrigin
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpePeerExtOverrideCapabilitySet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerExtOverrideCapability
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtConfigurePeerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtConfigurePeer (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[1].
                                                    pOctetStrValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtPeerRemoteAsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtPeerRemoteAs (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   pOctetStrValue,
                                                   pMultiData->u4_ULongValue));

}

INT4
Fsbgp4mpePeerExtEBGPMultiHopTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtEBGPMultiHop (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtEBGPHopLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtEBGPHopLimit (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtNextHopSelfTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtNextHopSelf (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[1].
                                                  pOctetStrValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtRflClientTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtRflClient (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[1].
                                                pOctetStrValue,
                                                pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtTcpSendBufSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtTcpSendBufSize (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     pOctetStrValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtTcpRcvBufSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtTcpRcvBufSize (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[1].
                                                    pOctetStrValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtLclAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtLclAddress (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 pOctetStrValue,
                                                 pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpePeerExtNetworkAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtNetworkAddress (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     pOctetStrValue,
                                                     pMultiData->
                                                     pOctetStrValue));

}

INT4
Fsbgp4mpePeerExtGatewayTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtGateway (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[1].
                                              pOctetStrValue,
                                              pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpePeerExtCommSendStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtCommSendStatus (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     pOctetStrValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtECommSendStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtECommSendStatus (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      pOctetStrValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtPassiveTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtPassive (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[1].
                                              pOctetStrValue,
                                              pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtDefaultOriginateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtDefaultOriginate (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiIndex->pIndex[1].
                                                       pOctetStrValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtActivateMPCapabilityTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtActivateMPCapability (pu4Error,
                                                           pMultiIndex->pIndex
                                                           [0].i4_SLongValue,
                                                           pMultiIndex->pIndex
                                                           [1].pOctetStrValue,
                                                           pMultiData->
                                                           i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtDeactivateMPCapabilityTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtDeactivateMPCapability (pu4Error,
                                                             pMultiIndex->pIndex
                                                             [0].i4_SLongValue,
                                                             pMultiIndex->pIndex
                                                             [1].pOctetStrValue,
                                                             pMultiData->
                                                             i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtMplsVpnVrfAssociatedTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtMplsVpnVrfAssociated (pu4Error,
                                                           pMultiIndex->pIndex
                                                           [0].i4_SLongValue,
                                                           pMultiIndex->pIndex
                                                           [1].pOctetStrValue,
                                                           pMultiData->
                                                           pOctetStrValue));

}

INT4
Fsbgp4mpePeerExtMplsVpnCERouteTargetAdvtTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtMplsVpnCERouteTargetAdvt (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               i4_SLongValue,
                                                               pMultiIndex->
                                                               pIndex[1].
                                                               pOctetStrValue,
                                                               pMultiData->
                                                               i4_SLongValue));

}

INT4
Fsbgp4mpePeerExtMplsVpnCESiteOfOriginTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtMplsVpnCESiteOfOrigin (pu4Error,
                                                            pMultiIndex->pIndex
                                                            [0].i4_SLongValue,
                                                            pMultiIndex->pIndex
                                                            [1].pOctetStrValue,
                                                            pMultiData->
                                                            pOctetStrValue));

}

INT4
Fsbgp4mpePeerExtOverrideCapabilityTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerExtOverrideCapability (pu4Error,
                                                         pMultiIndex->pIndex
                                                         [0].i4_SLongValue,
                                                         pMultiIndex->pIndex
                                                         [1].pOctetStrValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
Fsbgp4MpePeerExtTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MpePeerExtTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MpeMEDTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeMEDTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeMEDTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeMEDAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeMEDTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeMEDAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeMEDRemoteASGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeMEDTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeMEDRemoteAS (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4mpeMEDIPAddrAfiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeMEDTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeMEDIPAddrAfi (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeMEDIPAddrSafiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeMEDTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeMEDIPAddrSafi (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeMEDIPAddrPrefixGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeMEDTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeMEDIPAddrPrefix
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeMEDIPAddrPrefixLenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeMEDTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeMEDIPAddrPrefixLen
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeMEDIntermediateASGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeMEDTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeMEDIntermediateAS
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeMEDDirectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeMEDTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeMEDDirection (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeMEDValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeMEDTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeMEDValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4mpeMEDPreferenceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeMEDTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeMEDPreference (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeMEDVrfNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeMEDTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeMEDVrfName (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeMEDAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeMEDAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeMEDRemoteASSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeMEDRemoteAS (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Fsbgp4mpeMEDIPAddrAfiSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeMEDIPAddrAfi (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeMEDIPAddrSafiSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeMEDIPAddrSafi (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeMEDIPAddrPrefixSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeMEDIPAddrPrefix
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeMEDIPAddrPrefixLenSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeMEDIPAddrPrefixLen
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeMEDIntermediateASSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeMEDIntermediateAS
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeMEDDirectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeMEDDirection (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeMEDValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeMEDValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->u4_ULongValue));

}

INT4
Fsbgp4mpeMEDPreferenceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeMEDPreference (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeMEDVrfNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeMEDVrfName (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeMEDAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeMEDAdminStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeMEDRemoteASTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeMEDRemoteAS (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
Fsbgp4mpeMEDIPAddrAfiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeMEDIPAddrAfi (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeMEDIPAddrSafiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeMEDIPAddrSafi (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeMEDIPAddrPrefixTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeMEDIPAddrPrefix (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeMEDIPAddrPrefixLenTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeMEDIPAddrPrefixLen (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeMEDIntermediateASTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeMEDIntermediateAS (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeMEDDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeMEDDirection (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeMEDValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeMEDValue (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Fsbgp4mpeMEDPreferenceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeMEDPreference (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeMEDVrfNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeMEDVrfName (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
Fsbgp4MpeMEDTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MpeMEDTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MpeLocalPrefTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeLocalPrefTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeLocalPrefTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeLocalPrefAdminStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeLocalPrefTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeLocalPrefAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeLocalPrefRemoteASGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeLocalPrefTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeLocalPrefRemoteAS
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4mpeLocalPrefIPAddrAfiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeLocalPrefTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeLocalPrefIPAddrAfi
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeLocalPrefIPAddrSafiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeLocalPrefTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeLocalPrefIPAddrSafi
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeLocalPrefIPAddrPrefixGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeLocalPrefTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeLocalPrefIPAddrPrefix
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeLocalPrefIPAddrPrefixLenGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeLocalPrefTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeLocalPrefIPAddrPrefixLen
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeLocalPrefIntermediateASGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeLocalPrefTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeLocalPrefIntermediateAS
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeLocalPrefDirectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeLocalPrefTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeLocalPrefDirection
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeLocalPrefValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeLocalPrefTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeLocalPrefValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4mpeLocalPrefPreferenceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeLocalPrefTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeLocalPrefPreference
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeLocalPrefVrfNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeLocalPrefTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeLocalPrefVrfName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeLocalPrefAdminStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeLocalPrefAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeLocalPrefRemoteASSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeLocalPrefRemoteAS
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
Fsbgp4mpeLocalPrefIPAddrAfiSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeLocalPrefIPAddrAfi
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeLocalPrefIPAddrSafiSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeLocalPrefIPAddrSafi
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeLocalPrefIPAddrPrefixSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeLocalPrefIPAddrPrefix
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeLocalPrefIPAddrPrefixLenSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeLocalPrefIPAddrPrefixLen
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeLocalPrefIntermediateASSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeLocalPrefIntermediateAS
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeLocalPrefDirectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeLocalPrefDirection
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeLocalPrefValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeLocalPrefValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
Fsbgp4mpeLocalPrefPreferenceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeLocalPrefPreference
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeLocalPrefVrfNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeLocalPrefVrfName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeLocalPrefAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeLocalPrefAdminStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeLocalPrefRemoteASTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeLocalPrefRemoteAS (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
Fsbgp4mpeLocalPrefIPAddrAfiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeLocalPrefIPAddrAfi (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeLocalPrefIPAddrSafiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeLocalPrefIPAddrSafi (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeLocalPrefIPAddrPrefixTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeLocalPrefIPAddrPrefix (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     pOctetStrValue));

}

INT4
Fsbgp4mpeLocalPrefIPAddrPrefixLenTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeLocalPrefIPAddrPrefixLen (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
Fsbgp4mpeLocalPrefIntermediateASTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeLocalPrefIntermediateAS (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       pOctetStrValue));

}

INT4
Fsbgp4mpeLocalPrefDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeLocalPrefDirection (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeLocalPrefValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeLocalPrefValue (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
Fsbgp4mpeLocalPrefPreferenceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeLocalPrefPreference (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeLocalPrefVrfNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeLocalPrefVrfName (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->pOctetStrValue));

}

INT4
Fsbgp4MpeLocalPrefTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MpeLocalPrefTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MpeUpdateFilterTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeUpdateFilterTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeUpdateFilterTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeUpdateFilterAdminStatusGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeUpdateFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeUpdateFilterAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeUpdateFilterRemoteASGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeUpdateFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeUpdateFilterRemoteAS
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4mpeUpdateFilterIPAddrAfiGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeUpdateFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeUpdateFilterIPAddrAfi
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeUpdateFilterIPAddrSafiGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeUpdateFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeUpdateFilterIPAddrSafi
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeUpdateFilterIPAddrPrefixGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeUpdateFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeUpdateFilterIPAddrPrefix
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeUpdateFilterIPAddrPrefixLenGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeUpdateFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeUpdateFilterIPAddrPrefixLen
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeUpdateFilterIntermediateASGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeUpdateFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeUpdateFilterIntermediateAS
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeUpdateFilterDirectionGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeUpdateFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeUpdateFilterDirection
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeUpdateFilterActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeUpdateFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeUpdateFilterAction
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeUpdateFilterVrfNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeUpdateFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeUpdateFilterVrfName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeUpdateFilterAdminStatusSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeUpdateFilterAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeUpdateFilterRemoteASSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeUpdateFilterRemoteAS
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
Fsbgp4mpeUpdateFilterIPAddrAfiSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeUpdateFilterIPAddrAfi
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeUpdateFilterIPAddrSafiSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeUpdateFilterIPAddrSafi
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeUpdateFilterIPAddrPrefixSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeUpdateFilterIPAddrPrefix
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeUpdateFilterIPAddrPrefixLenSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeUpdateFilterIPAddrPrefixLen
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeUpdateFilterIntermediateASSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeUpdateFilterIntermediateAS
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeUpdateFilterDirectionSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeUpdateFilterDirection
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeUpdateFilterActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeUpdateFilterAction
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeUpdateFilterVrfNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeUpdateFilterVrfName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeUpdateFilterAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeUpdateFilterAdminStatus (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
Fsbgp4mpeUpdateFilterRemoteASTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeUpdateFilterRemoteAS (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->u4_ULongValue));

}

INT4
Fsbgp4mpeUpdateFilterIPAddrAfiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeUpdateFilterIPAddrAfi (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
Fsbgp4mpeUpdateFilterIPAddrSafiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeUpdateFilterIPAddrSafi (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
Fsbgp4mpeUpdateFilterIPAddrPrefixTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeUpdateFilterIPAddrPrefix (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        pOctetStrValue));

}

INT4
Fsbgp4mpeUpdateFilterIPAddrPrefixLenTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeUpdateFilterIPAddrPrefixLen (pu4Error,
                                                           pMultiIndex->pIndex
                                                           [0].i4_SLongValue,
                                                           pMultiData->
                                                           i4_SLongValue));

}

INT4
Fsbgp4mpeUpdateFilterIntermediateASTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeUpdateFilterIntermediateAS (pu4Error,
                                                          pMultiIndex->pIndex
                                                          [0].i4_SLongValue,
                                                          pMultiData->
                                                          pOctetStrValue));

}

INT4
Fsbgp4mpeUpdateFilterDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeUpdateFilterDirection (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
Fsbgp4mpeUpdateFilterActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeUpdateFilterAction (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeUpdateFilterVrfNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeUpdateFilterVrfName (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->pOctetStrValue));

}

INT4
Fsbgp4MpeUpdateFilterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MpeUpdateFilterTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MpeAggregateTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeAggregateTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeAggregateTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeAggregateAdminStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeAggregateTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeAggregateAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeAggregateIPAddrAfiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeAggregateTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeAggregateIPAddrAfi
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeAggregateIPAddrSafiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeAggregateTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeAggregateIPAddrSafi
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeAggregateIPAddrPrefixGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeAggregateTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeAggregateIPAddrPrefix
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeAggregateIPAddrPrefixLenGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeAggregateTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeAggregateIPAddrPrefixLen
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeAggregateAdvertiseGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeAggregateTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeAggregateAdvertise
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeAggregateVrfNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeAggregateTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeAggregateVrfName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeAggregateAsSetGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeAggregateTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeAggregateAsSet (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeAggregateAdvertiseRouteMapNameGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeAggregateTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeAggregateAdvertiseRouteMapName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeAggregateSuppressRouteMapNameGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeAggregateTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeAggregateSuppressRouteMapName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeAggregateAttributeRouteMapNameGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeAggregateTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeAggregateAttributeRouteMapName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeAggregateAdminStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeAggregateAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeAggregateIPAddrAfiSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeAggregateIPAddrAfi
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeAggregateIPAddrSafiSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeAggregateIPAddrSafi
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeAggregateIPAddrPrefixSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeAggregateIPAddrPrefix
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeAggregateIPAddrPrefixLenSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeAggregateIPAddrPrefixLen
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeAggregateAdvertiseSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeAggregateAdvertise
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeAggregateVrfNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeAggregateVrfName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeAggregateAsSetSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeAggregateAsSet (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeAggregateAdvertiseRouteMapNameSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeAggregateAdvertiseRouteMapName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeAggregateSuppressRouteMapNameSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeAggregateSuppressRouteMapName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeAggregateAttributeRouteMapNameSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeAggregateAttributeRouteMapName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeAggregateAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeAggregateAdminStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeAggregateIPAddrAfiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeAggregateIPAddrAfi (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeAggregateIPAddrSafiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeAggregateIPAddrSafi (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeAggregateIPAddrPrefixTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeAggregateIPAddrPrefix (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     pOctetStrValue));

}

INT4
Fsbgp4mpeAggregateIPAddrPrefixLenTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeAggregateIPAddrPrefixLen (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
Fsbgp4mpeAggregateAdvertiseTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeAggregateAdvertise (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeAggregateVrfNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeAggregateVrfName (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->pOctetStrValue));

}

INT4
Fsbgp4mpeAggregateAsSetTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeAggregateAsSet (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeAggregateAdvertiseRouteMapNameTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeAggregateAdvertiseRouteMapName (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              i4_SLongValue,
                                                              pMultiData->
                                                              pOctetStrValue));

}

INT4
Fsbgp4mpeAggregateSuppressRouteMapNameTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeAggregateSuppressRouteMapName (pu4Error,
                                                             pMultiIndex->pIndex
                                                             [0].i4_SLongValue,
                                                             pMultiData->
                                                             pOctetStrValue));

}

INT4
Fsbgp4mpeAggregateAttributeRouteMapNameTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeAggregateAttributeRouteMapName (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              i4_SLongValue,
                                                              pMultiData->
                                                              pOctetStrValue));

}

INT4
Fsbgp4MpeAggregateTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MpeAggregateTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MpeImportRouteTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeImportRouteTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pNextMultiIndex->pIndex[5].pOctetStrValue,
             &(pNextMultiIndex->pIndex[6].i4_SLongValue),
             &(pNextMultiIndex->pIndex[7].i4_SLongValue),
             pNextMultiIndex->pIndex[8].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeImportRouteTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pFirstMultiIndex->pIndex[5].pOctetStrValue,
             pNextMultiIndex->pIndex[5].pOctetStrValue,
             pFirstMultiIndex->pIndex[6].i4_SLongValue,
             &(pNextMultiIndex->pIndex[6].i4_SLongValue),
             pFirstMultiIndex->pIndex[7].i4_SLongValue,
             &(pNextMultiIndex->pIndex[7].i4_SLongValue),
             pFirstMultiIndex->pIndex[8].pOctetStrValue,
             pNextMultiIndex->pIndex[8].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeImportRouteActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeImportRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue,
         pMultiIndex->pIndex[6].i4_SLongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeImportRouteAction
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].i4_SLongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeImportRouteActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeImportRouteAction
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].i4_SLongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeImportRouteActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeImportRouteAction (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[2].
                                                 pOctetStrValue,
                                                 pMultiIndex->pIndex[3].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[4].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[5].
                                                 pOctetStrValue,
                                                 pMultiIndex->pIndex[6].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[7].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[8].
                                                 pOctetStrValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
Fsbgp4MpeImportRouteTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MpeImportRouteTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MpeFsmTransitionHistTable (tSnmpIndex * pFirstMultiIndex,
                                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeFsmTransitionHistTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeFsmTransitionHistTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeFsmTransitionHistGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeFsmTransitionHistTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeFsmTransitionHist
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
GetNextIndexFsbgp4MpeRfdRtDampHistTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeRfdRtDampHistTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pNextMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeRfdRtDampHistTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pFirstMultiIndex->pIndex[5].pOctetStrValue,
             pNextMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeRfdRtFomGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRfdRtDampHistTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRfdRtFom (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiIndex->pIndex[2].pOctetStrValue,
                                     pMultiIndex->pIndex[3].i4_SLongValue,
                                     pMultiIndex->pIndex[4].i4_SLongValue,
                                     pMultiIndex->pIndex[5].pOctetStrValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeRfdRtLastUpdtTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRfdRtDampHistTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRfdRtLastUpdtTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeRfdRtStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRfdRtDampHistTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRfdRtState (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       pMultiIndex->pIndex[3].i4_SLongValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       pMultiIndex->pIndex[5].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeRfdRtStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRfdRtDampHistTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRfdRtStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiIndex->pIndex[2].pOctetStrValue,
                                        pMultiIndex->pIndex[3].i4_SLongValue,
                                        pMultiIndex->pIndex[4].i4_SLongValue,
                                        pMultiIndex->pIndex[5].pOctetStrValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeRfdRtFlapCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRfdRtDampHistTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRfdRtFlapCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiIndex->pIndex[2].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[3].i4_SLongValue,
                                           pMultiIndex->pIndex[4].i4_SLongValue,
                                           pMultiIndex->pIndex[5].
                                           pOctetStrValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeRfdRtFlapTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRfdRtDampHistTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRfdRtFlapTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].pOctetStrValue,
                                          pMultiIndex->pIndex[3].i4_SLongValue,
                                          pMultiIndex->pIndex[4].i4_SLongValue,
                                          pMultiIndex->pIndex[5].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeRfdRtReuseTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRfdRtDampHistTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRfdRtReuseTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiIndex->pIndex[2].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[3].i4_SLongValue,
                                           pMultiIndex->pIndex[4].i4_SLongValue,
                                           pMultiIndex->pIndex[5].
                                           pOctetStrValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFsbgp4MpeRfdPeerDampHistTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeRfdPeerDampHistTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeRfdPeerDampHistTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeRfdPeerFomGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRfdPeerDampHistTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRfdPeerFom (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeRfdPeerLastUpdtTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRfdPeerDampHistTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRfdPeerLastUpdtTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeRfdPeerStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRfdPeerDampHistTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRfdPeerState (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeRfdPeerStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRfdPeerDampHistTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRfdPeerStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFsbgp4MpeRfdRtsReuseListTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeRfdRtsReuseListTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pNextMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeRfdRtsReuseListTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pFirstMultiIndex->pIndex[5].pOctetStrValue,
             pNextMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeRfdRtReuseListRtFomGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRfdRtsReuseListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRfdRtReuseListRtFom
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeRfdRtReuseListRtLastUpdtTimeGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRfdRtsReuseListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRfdRtReuseListRtLastUpdtTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeRfdRtReuseListRtStateGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRfdRtsReuseListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRfdRtReuseListRtState
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeRfdRtReuseListRtStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRfdRtsReuseListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRfdRtReuseListRtStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFsbgp4MpeRfdPeerReuseListTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeRfdPeerReuseListTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeRfdPeerReuseListTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeRfdPeerReuseListPeerFomGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRfdPeerReuseListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRfdPeerReuseListPeerFom
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeRfdPeerReuseListLastUpdtTimeGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRfdPeerReuseListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRfdPeerReuseListLastUpdtTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeRfdPeerReuseListPeerStateGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRfdPeerReuseListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRfdPeerReuseListPeerState
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeRfdPeerReuseListPeerStatusGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRfdPeerReuseListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRfdPeerReuseListPeerStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFsbgp4MpeCommRouteAddCommTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeCommRouteAddCommTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeCommRouteAddCommTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeAddCommRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeCommRouteAddCommTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeAddCommRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeAddCommRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeAddCommRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeAddCommRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeAddCommRowStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[1].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[2].
                                                pOctetStrValue,
                                                pMultiIndex->pIndex[3].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[4].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
Fsbgp4MpeCommRouteAddCommTableDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MpeCommRouteAddCommTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MpeCommRouteDeleteCommTable (tSnmpIndex * pFirstMultiIndex,
                                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeCommRouteDeleteCommTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeCommRouteDeleteCommTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeDeleteCommRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeCommRouteDeleteCommTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeDeleteCommRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeDeleteCommRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeDeleteCommRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeDeleteCommRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeDeleteCommRowStatus (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[2].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[3].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[4].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
Fsbgp4MpeCommRouteDeleteCommTableDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MpeCommRouteDeleteCommTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MpeCommRouteCommSetStatusTable (tSnmpIndex * pFirstMultiIndex,
                                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeCommRouteCommSetStatusTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeCommRouteCommSetStatusTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeCommSetStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeCommRouteCommSetStatusTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeCommSetStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].pOctetStrValue,
                                          pMultiIndex->pIndex[3].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeCommSetStatusRowStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeCommRouteCommSetStatusTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeCommSetStatusRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeCommSetStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeCommSetStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].pOctetStrValue,
                                          pMultiIndex->pIndex[3].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeCommSetStatusRowStatusSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeCommSetStatusRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeCommSetStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeCommSetStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[2].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[3].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeCommSetStatusRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeCommSetStatusRowStatus (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[2].
                                                      pOctetStrValue,
                                                      pMultiIndex->pIndex[3].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
Fsbgp4MpeCommRouteCommSetStatusTableDep (UINT4 *pu4Error,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MpeCommRouteCommSetStatusTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MpeExtCommRouteAddExtCommTable (tSnmpIndex * pFirstMultiIndex,
                                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeExtCommRouteAddExtCommTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pNextMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeExtCommRouteAddExtCommTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].pOctetStrValue,
             pNextMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeAddExtCommRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeExtCommRouteAddExtCommTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeAddExtCommRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeAddExtCommRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeAddExtCommRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeAddExtCommRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeAddExtCommRowStatus (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[2].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[3].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[4].
                                                   pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
Fsbgp4MpeExtCommRouteAddExtCommTableDep (UINT4 *pu4Error,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MpeExtCommRouteAddExtCommTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MpeExtCommRouteDeleteExtCommTable (tSnmpIndex *
                                                     pFirstMultiIndex,
                                                     tSnmpIndex *
                                                     pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeExtCommRouteDeleteExtCommTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pNextMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeExtCommRouteDeleteExtCommTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].pOctetStrValue,
             pNextMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeDeleteExtCommRowStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeExtCommRouteDeleteExtCommTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeDeleteExtCommRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeDeleteExtCommRowStatusSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeDeleteExtCommRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeDeleteExtCommRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeDeleteExtCommRowStatus (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[2].
                                                      pOctetStrValue,
                                                      pMultiIndex->pIndex[3].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[4].
                                                      pOctetStrValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
Fsbgp4MpeExtCommRouteDeleteExtCommTableDep (UINT4 *pu4Error,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MpeExtCommRouteDeleteExtCommTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable (tSnmpIndex *
                                                        pFirstMultiIndex,
                                                        tSnmpIndex *
                                                        pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeExtCommSetStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeExtCommRouteExtCommSetStatusTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeExtCommSetStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeExtCommSetStatusRowStatusGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeExtCommRouteExtCommSetStatusTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeExtCommSetStatusRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeExtCommSetStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeExtCommSetStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeExtCommSetStatusRowStatusSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeExtCommSetStatusRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeExtCommSetStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeExtCommSetStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[1].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[2].
                                                pOctetStrValue,
                                                pMultiIndex->pIndex[3].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeExtCommSetStatusRowStatusTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeExtCommSetStatusRowStatus (pu4Error,
                                                         pMultiIndex->pIndex
                                                         [0].i4_SLongValue,
                                                         pMultiIndex->pIndex
                                                         [1].i4_SLongValue,
                                                         pMultiIndex->pIndex
                                                         [2].pOctetStrValue,
                                                         pMultiIndex->pIndex
                                                         [3].i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
Fsbgp4MpeExtCommRouteExtCommSetStatusTableDep (UINT4 *pu4Error,
                                               tSnmpIndexList * pSnmpIndexList,
                                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MpeExtCommRouteExtCommSetStatusTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MpePeerLinkBwTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpePeerLinkBwTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpePeerLinkBwTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeLinkBandWidthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerLinkBwTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeLinkBandWidth (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4mpePeerLinkBwRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePeerLinkBwTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpePeerLinkBwRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeLinkBandWidthSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeLinkBandWidth (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->u4_ULongValue));

}

INT4
Fsbgp4mpePeerLinkBwRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpePeerLinkBwRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeLinkBandWidthTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeLinkBandWidth (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             pOctetStrValue,
                                             pMultiData->u4_ULongValue));

}

INT4
Fsbgp4mpePeerLinkBwRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpePeerLinkBwRowStatus (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
Fsbgp4MpePeerLinkBwTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MpePeerLinkBwTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MpeCapSupportedCapsTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeCapSupportedCapsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pNextMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeCapSupportedCapsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].pOctetStrValue,
             pNextMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeCapSupportedCapsRowStatusGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeCapSupportedCapsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeCapSupportedCapsRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeCapAnnouncedStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeCapSupportedCapsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeCapAnnouncedStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeCapReceivedStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeCapSupportedCapsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeCapReceivedStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeCapNegotiatedStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeCapSupportedCapsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeCapNegotiatedStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeCapConfiguredStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeCapSupportedCapsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeCapConfiguredStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeCapSupportedCapsRowStatusSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeCapSupportedCapsRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeCapSupportedCapsRowStatusTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeCapSupportedCapsRowStatus (pu4Error,
                                                         pMultiIndex->pIndex
                                                         [0].i4_SLongValue,
                                                         pMultiIndex->pIndex
                                                         [1].pOctetStrValue,
                                                         pMultiIndex->pIndex
                                                         [2].i4_SLongValue,
                                                         pMultiIndex->pIndex
                                                         [3].i4_SLongValue,
                                                         pMultiIndex->pIndex
                                                         [4].pOctetStrValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
Fsbgp4MpeCapSupportedCapsTableDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MpeCapSupportedCapsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MpeRtRefreshInboundTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeRtRefreshInboundTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeRtRefreshInboundTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeRtRefreshInboundRequestGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRtRefreshInboundTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRtRefreshInboundRequest
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeRtRefreshInboundPrefixFilterGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRtRefreshInboundTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRtRefreshInboundPrefixFilter
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeRtRefreshInboundRequestSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeRtRefreshInboundRequest
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeRtRefreshInboundPrefixFilterSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeRtRefreshInboundPrefixFilter
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeRtRefreshInboundRequestTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeRtRefreshInboundRequest (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiIndex->pIndex[1].
                                                       pOctetStrValue,
                                                       pMultiIndex->pIndex[2].
                                                       i4_SLongValue,
                                                       pMultiIndex->pIndex[3].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
Fsbgp4mpeRtRefreshInboundPrefixFilterTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeRtRefreshInboundPrefixFilter (pu4Error,
                                                            pMultiIndex->pIndex
                                                            [0].i4_SLongValue,
                                                            pMultiIndex->pIndex
                                                            [1].pOctetStrValue,
                                                            pMultiIndex->pIndex
                                                            [2].i4_SLongValue,
                                                            pMultiIndex->pIndex
                                                            [3].i4_SLongValue,
                                                            pMultiData->
                                                            i4_SLongValue));

}

INT4
Fsbgp4MpeRtRefreshInboundTableDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MpeRtRefreshInboundTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MpeRtRefreshStatisticsTable (tSnmpIndex * pFirstMultiIndex,
                                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeRtRefreshStatisticsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeRtRefreshStatisticsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeRtRefreshStatisticsRtRefMsgSentCntrGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRtRefreshStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgSentCntr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4mpeRtRefreshStatisticsRtRefMsgTxErrCntrGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRtRefreshStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgTxErrCntr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4mpeRtRefreshStatisticsRtRefMsgRcvdCntrGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRtRefreshStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgRcvdCntr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4mpeRtRefreshStatisticsRtRefMsgInvalidCntrGet (tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRtRefreshStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgInvalidCntr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexFsbgp4MpeSoftReconfigOutboundTable (tSnmpIndex * pFirstMultiIndex,
                                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpeSoftReconfigOutboundTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpeSoftReconfigOutboundTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4mpeSoftReconfigOutboundRequestGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpeSoftReconfigOutboundTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4mpeSoftReconfigOutboundRequest
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4mpeSoftReconfigOutboundRequestSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsbgp4mpeSoftReconfigOutboundRequest
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4mpeSoftReconfigOutboundRequestTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4mpeSoftReconfigOutboundRequest (pu4Error,
                                                           pMultiIndex->pIndex
                                                           [0].i4_SLongValue,
                                                           pMultiIndex->pIndex
                                                           [1].pOctetStrValue,
                                                           pMultiIndex->pIndex
                                                           [2].i4_SLongValue,
                                                           pMultiIndex->pIndex
                                                           [3].i4_SLongValue,
                                                           pMultiData->
                                                           i4_SLongValue));

}

INT4
Fsbgp4MpeSoftReconfigOutboundTableDep (UINT4 *pu4Error,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MpeSoftReconfigOutboundTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MpePrefixCountersTable (tSnmpIndex * pFirstMultiIndex,
                                          tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MpePrefixCountersTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MpePrefixCountersTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4MpePrefixCountersPrefixesReceivedGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePrefixCountersTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4MpePrefixCountersPrefixesReceived
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4MpePrefixCountersPrefixesSentGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePrefixCountersTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4MpePrefixCountersPrefixesSent
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4MpePrefixCountersWithdrawsReceivedGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePrefixCountersTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4MpePrefixCountersWithdrawsReceived
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4MpePrefixCountersWithdrawsSentGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePrefixCountersTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4MpePrefixCountersWithdrawsSent
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4MpePrefixCountersInPrefixesGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePrefixCountersTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4MpePrefixCountersInPrefixes
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4MpePrefixCountersInPrefixesAcceptedGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePrefixCountersTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4MpePrefixCountersInPrefixesAccepted
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4MpePrefixCountersInPrefixesRejectedGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePrefixCountersTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4MpePrefixCountersInPrefixesRejected
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Fsbgp4MpePrefixCountersOutPrefixesGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MpePrefixCountersTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4MpePrefixCountersOutPrefixes
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexFsbgp4MplsVpnVrfRouteTargetTable (tSnmpIndex * pFirstMultiIndex,
                                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MplsVpnVrfRouteTargetTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MplsVpnVrfRouteTargetTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4MplsVpnVrfRouteTargetRowStatusGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MplsVpnVrfRouteTargetTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4MplsVpnVrfRouteTargetRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4MplsVpnVrfRouteTargetRowStatusSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsbgp4MplsVpnVrfRouteTargetRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4MplsVpnVrfRouteTargetRowStatusTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4MplsVpnVrfRouteTargetRowStatus (pu4Error,
                                                           pMultiIndex->pIndex
                                                           [0].pOctetStrValue,
                                                           pMultiIndex->pIndex
                                                           [1].i4_SLongValue,
                                                           pMultiIndex->pIndex
                                                           [2].pOctetStrValue,
                                                           pMultiData->
                                                           i4_SLongValue));

}

INT4
Fsbgp4MplsVpnVrfRouteTargetTableDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MplsVpnVrfRouteTargetTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MplsVpnVrfRedistributeTable (tSnmpIndex * pFirstMultiIndex,
                                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MplsVpnVrfRedistributeTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MplsVpnVrfRedistributeTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4MplsVpnVrfRedisProtoMaskGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MplsVpnVrfRedistributeTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4MplsVpnVrfRedisProtoMask
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4MplsVpnVrfRedisProtoMaskSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsbgp4MplsVpnVrfRedisProtoMask
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4MplsVpnVrfRedisProtoMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4MplsVpnVrfRedisProtoMask (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[2].
                                                     pOctetStrValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
Fsbgp4MplsVpnVrfRedistributeTableDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4MplsVpnVrfRedistributeTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4MplsVpnRRRouteTargetTable (tSnmpIndex * pFirstMultiIndex,
                                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4MplsVpnRRRouteTargetTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4MplsVpnRRRouteTargetTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4MplsVpnRRRouteTargetRtCntGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MplsVpnRRRouteTargetTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4MplsVpnRRRouteTargetRtCnt
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4MplsVpnRRRouteTargetTimeStampGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4MplsVpnRRRouteTargetTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4MplsVpnRRRouteTargetTimeStamp
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFsBgp4DistInOutRouteMapTable (tSnmpIndex * pFirstMultiIndex,
                                          tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsBgp4DistInOutRouteMapTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsBgp4DistInOutRouteMapTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsBgp4DistInOutRouteMapValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4DistInOutRouteMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4DistInOutRouteMapValue
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4DistInOutRouteMapRowStatusGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4DistInOutRouteMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4DistInOutRouteMapRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4DistInOutRouteMapValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsBgp4DistInOutRouteMapValue
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4DistInOutRouteMapRowStatusSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsBgp4DistInOutRouteMapRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4DistInOutRouteMapValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4DistInOutRouteMapValue (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsBgp4DistInOutRouteMapRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4DistInOutRouteMapRowStatus (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       pOctetStrValue,
                                                       pMultiIndex->pIndex[1].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
FsBgp4DistInOutRouteMapTableDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsBgp4DistInOutRouteMapTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsBgp4PreferenceValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsBgp4PreferenceValue (&(pMultiData->i4_SLongValue)));
}

INT4
FsBgp4PreferenceValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsBgp4PreferenceValue (pMultiData->i4_SLongValue));
}

INT4
FsBgp4PreferenceValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsBgp4PreferenceValue
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsBgp4PreferenceValueDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsBgp4PreferenceValue
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsBgp4PeerGroupTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsBgp4PeerGroupTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsBgp4PeerGroupTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsBgp4PeerGroupAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupAddrType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupRemoteAsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupRemoteAs
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsBgp4PeerGroupHoldTimeConfiguredGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupHoldTimeConfigured
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupKeepAliveConfiguredGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupKeepAliveConfigured
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupConnectRetryIntervalGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupConnectRetryInterval
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupMinASOriginIntervalGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupMinASOriginInterval
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupMinRouteAdvIntervalGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupMinRouteAdvInterval
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupAllowAutomaticStartGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupAllowAutomaticStart
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupAllowAutomaticStopGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupAllowAutomaticStop
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupIdleHoldTimeConfiguredGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupIdleHoldTimeConfigured
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupDampPeerOscillationsGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupDampPeerOscillations
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupDelayOpenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupDelayOpen
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupDelayOpenTimeConfiguredGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupDelayOpenTimeConfigured
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupPrefixUpperLimitGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupPrefixUpperLimit
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupTcpConnectRetryCntGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupTcpConnectRetryCnt
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupEBGPMultiHopGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupEBGPMultiHop
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupEBGPHopLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupEBGPHopLimit
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupNextHopSelfGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupNextHopSelf
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupRflClientGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupRflClient
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupTcpSendBufSizeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupTcpSendBufSize
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupTcpRcvBufSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupTcpRcvBufSize
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupCommSendStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupCommSendStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupECommSendStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupECommSendStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupPassiveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupPassive (pMultiIndex->pIndex[0].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupDefaultOriginateGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupDefaultOriginate
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupActivateMPCapabilityGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupActivateMPCapability
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupDeactivateMPCapabilityGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupDeactivateMPCapability
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupRouteMapNameInGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupRouteMapNameIn
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsBgp4PeerGroupRouteMapNameOutGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupRouteMapNameOut
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsBgp4PeerGroupStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupIpPrefixNameInGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupIpPrefixNameIn
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsBgp4PeerGroupIpPrefixNameOutGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupIpPrefixNameOut
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsBgp4PeerGroupOrfTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupOrfType (pMultiIndex->pIndex[0].pOctetStrValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsBgp4PeerGroupOrfCapModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupOrfCapMode
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupOrfRequestGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupOrfRequest
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupBfdStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupBfdStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerGroupOverrideCapabilityGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerGroupOverrideCapability
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4 FsBgp4PeerGroupLocalAsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        if (nmhValidateIndexInstanceFsBgp4PeerGroupTable(
                            pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
                {
                            return SNMP_FAILURE;
                                }
            return(nmhGetFsBgp4PeerGroupLocalAs(
                                pMultiIndex->pIndex[0].pOctetStrValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsBgp4PeerGroupRemoteAsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupRemoteAs
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4 FsBgp4PeerGroupLocalAsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhSetFsBgp4PeerGroupLocalAs(
                            pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiData->u4_ULongValue));

}

INT4
FsBgp4PeerGroupHoldTimeConfiguredSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupHoldTimeConfigured
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupKeepAliveConfiguredSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupKeepAliveConfigured
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupConnectRetryIntervalSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupConnectRetryInterval
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupMinASOriginIntervalSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupMinASOriginInterval
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupMinRouteAdvIntervalSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupMinRouteAdvInterval
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupAllowAutomaticStartSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupAllowAutomaticStart
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupAllowAutomaticStopSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupAllowAutomaticStop
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupIdleHoldTimeConfiguredSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupIdleHoldTimeConfigured
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupDampPeerOscillationsSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupDampPeerOscillations
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupDelayOpenSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupDelayOpen
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupDelayOpenTimeConfiguredSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupDelayOpenTimeConfigured
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupPrefixUpperLimitSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupPrefixUpperLimit
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupTcpConnectRetryCntSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupTcpConnectRetryCnt
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupEBGPMultiHopSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupEBGPMultiHop
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupEBGPHopLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupEBGPHopLimit
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupNextHopSelfSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupNextHopSelf
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupRflClientSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupRflClient
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupTcpSendBufSizeSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupTcpSendBufSize
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupTcpRcvBufSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupTcpRcvBufSize
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupCommSendStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupCommSendStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupECommSendStatusSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupECommSendStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupPassiveSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupPassive (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupDefaultOriginateSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupDefaultOriginate
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupActivateMPCapabilitySet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupActivateMPCapability
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupDeactivateMPCapabilitySet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupDeactivateMPCapability
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupRouteMapNameInSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupRouteMapNameIn
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsBgp4PeerGroupRouteMapNameOutSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupRouteMapNameOut
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsBgp4PeerGroupStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupIpPrefixNameInSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupIpPrefixNameIn
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsBgp4PeerGroupIpPrefixNameOutSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupIpPrefixNameOut
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsBgp4PeerGroupOrfTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupOrfType (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsBgp4PeerGroupOrfCapModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupOrfCapMode
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupOrfRequestSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupOrfRequest
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupBfdStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupBfdStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupOverrideCapabilitySet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerGroupOverrideCapability
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupRemoteAsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupRemoteAs (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              pOctetStrValue,
                                              pMultiData->u4_ULongValue));

}

INT4
FsBgp4PeerGroupHoldTimeConfiguredTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupHoldTimeConfigured (pu4Error,
                                                        pMultiIndex->pIndex
                                                        [0].pOctetStrValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
FsBgp4PeerGroupKeepAliveConfiguredTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupKeepAliveConfigured (pu4Error,
                                                         pMultiIndex->pIndex
                                                         [0].pOctetStrValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
FsBgp4PeerGroupConnectRetryIntervalTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupConnectRetryInterval (pu4Error,
                                                          pMultiIndex->pIndex
                                                          [0].pOctetStrValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
FsBgp4PeerGroupMinASOriginIntervalTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupMinASOriginInterval (pu4Error,
                                                         pMultiIndex->pIndex
                                                         [0].pOctetStrValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
FsBgp4PeerGroupMinRouteAdvIntervalTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupMinRouteAdvInterval (pu4Error,
                                                         pMultiIndex->pIndex
                                                         [0].pOctetStrValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
FsBgp4PeerGroupAllowAutomaticStartTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupAllowAutomaticStart (pu4Error,
                                                         pMultiIndex->pIndex
                                                         [0].pOctetStrValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
FsBgp4PeerGroupAllowAutomaticStopTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupAllowAutomaticStop (pu4Error,
                                                        pMultiIndex->pIndex
                                                        [0].pOctetStrValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
FsBgp4PeerGroupIdleHoldTimeConfiguredTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupIdleHoldTimeConfigured (pu4Error,
                                                            pMultiIndex->pIndex
                                                            [0].pOctetStrValue,
                                                            pMultiData->
                                                            i4_SLongValue));

}

INT4
FsBgp4PeerGroupDampPeerOscillationsTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupDampPeerOscillations (pu4Error,
                                                          pMultiIndex->pIndex
                                                          [0].pOctetStrValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
FsBgp4PeerGroupDelayOpenTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupDelayOpen (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupDelayOpenTimeConfiguredTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupDelayOpenTimeConfigured (pu4Error,
                                                             pMultiIndex->pIndex
                                                             [0].pOctetStrValue,
                                                             pMultiData->
                                                             i4_SLongValue));

}

INT4
FsBgp4PeerGroupPrefixUpperLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupPrefixUpperLimit (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      pOctetStrValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FsBgp4PeerGroupTcpConnectRetryCntTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupTcpConnectRetryCnt (pu4Error,
                                                        pMultiIndex->pIndex
                                                        [0].pOctetStrValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
FsBgp4PeerGroupEBGPMultiHopTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupEBGPMultiHop (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupEBGPHopLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupEBGPHopLimit (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupNextHopSelfTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupNextHopSelf (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 pOctetStrValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupRflClientTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupRflClient (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupTcpSendBufSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupTcpSendBufSize (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    pOctetStrValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupTcpRcvBufSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupTcpRcvBufSize (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupCommSendStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupCommSendStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    pOctetStrValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupECommSendStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupECommSendStatus (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     pOctetStrValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsBgp4PeerGroupPassiveTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupPassive (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupDefaultOriginateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupDefaultOriginate (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      pOctetStrValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FsBgp4PeerGroupActivateMPCapabilityTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupActivateMPCapability (pu4Error,
                                                          pMultiIndex->pIndex
                                                          [0].pOctetStrValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
FsBgp4PeerGroupDeactivateMPCapabilityTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupDeactivateMPCapability (pu4Error,
                                                            pMultiIndex->pIndex
                                                            [0].pOctetStrValue,
                                                            pMultiData->
                                                            i4_SLongValue));

}

INT4
FsBgp4PeerGroupRouteMapNameInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupRouteMapNameIn (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    pOctetStrValue,
                                                    pMultiData->
                                                    pOctetStrValue));

}

INT4
FsBgp4PeerGroupRouteMapNameOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupRouteMapNameOut (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     pOctetStrValue,
                                                     pMultiData->
                                                     pOctetStrValue));

}

INT4
FsBgp4PeerGroupStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupIpPrefixNameInTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupIpPrefixNameIn (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    pOctetStrValue,
                                                    pMultiData->
                                                    pOctetStrValue));

}

INT4
FsBgp4PeerGroupIpPrefixNameOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupIpPrefixNameOut (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     pOctetStrValue,
                                                     pMultiData->
                                                     pOctetStrValue));

}

INT4
FsBgp4PeerGroupOrfTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupOrfType (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiData->u4_ULongValue));

}

INT4
FsBgp4PeerGroupOrfCapModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupOrfCapMode (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupOrfRequestTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupOrfRequest (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupBfdStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupBfdStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupOverrideCapabilityTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerGroupOverrideCapability (pu4Error,
                                                        pMultiIndex->pIndex
                                                        [0].pOctetStrValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4 FsBgp4PeerGroupLocalAsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhTestv2FsBgp4PeerGroupLocalAs(pu4Error,
                            pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiData->u4_ULongValue));

}

INT4
FsBgp4PeerGroupTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsBgp4PeerGroupTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsBgp4PeerGroupListTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsBgp4PeerGroupListTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsBgp4PeerGroupListTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsBgp4PeerAddStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4PeerGroupListTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4PeerAddStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4PeerAddStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsBgp4PeerAddStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerAddStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4PeerAddStatus (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsBgp4PeerGroupListTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsBgp4PeerGroupListTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsBgp4NeighborRouteMapTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsBgp4NeighborRouteMapTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsBgp4NeighborRouteMapTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsBgp4NeighborRouteMapNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4NeighborRouteMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4NeighborRouteMapName
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsBgp4NeighborRouteMapRowStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsBgp4NeighborRouteMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBgp4NeighborRouteMapRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBgp4NeighborRouteMapNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsBgp4NeighborRouteMapName
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsBgp4NeighborRouteMapRowStatusSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsBgp4NeighborRouteMapRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsBgp4NeighborRouteMapNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4NeighborRouteMapName (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 pOctetStrValue,
                                                 pMultiIndex->pIndex[2].
                                                 i4_SLongValue,
                                                 pMultiData->pOctetStrValue));

}

INT4
FsBgp4NeighborRouteMapRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsBgp4NeighborRouteMapRowStatus (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      pOctetStrValue,
                                                      pMultiIndex->pIndex[2].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FsBgp4NeighborRouteMapTableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsBgp4NeighborRouteMapTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4TCPMKTAuthTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4TCPMKTAuthTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4TCPMKTAuthTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4TCPMKTAuthRecvKeyIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4TCPMKTAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4TCPMKTAuthRecvKeyId
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4TCPMKTAuthMasterKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4TCPMKTAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4TCPMKTAuthMasterKey
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4TCPMKTAuthAlgoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4TCPMKTAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4TCPMKTAuthAlgo (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4TCPMKTAuthTcpOptExcGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4TCPMKTAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4TCPMKTAuthTcpOptExc
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4TCPMKTAuthRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4TCPMKTAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4TCPMKTAuthRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4TCPMKTAuthRecvKeyIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4TCPMKTAuthRecvKeyId
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4TCPMKTAuthMasterKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4TCPMKTAuthMasterKey
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4TCPMKTAuthAlgoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4TCPMKTAuthAlgo (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
Fsbgp4TCPMKTAuthTcpOptExcSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4TCPMKTAuthTcpOptExc
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4TCPMKTAuthRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4TCPMKTAuthRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4TCPMKTAuthRecvKeyIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4TCPMKTAuthRecvKeyId (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
Fsbgp4TCPMKTAuthMasterKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4TCPMKTAuthMasterKey (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->pOctetStrValue));

}

INT4
Fsbgp4TCPMKTAuthAlgoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4TCPMKTAuthAlgo (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
Fsbgp4TCPMKTAuthTcpOptExcTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4TCPMKTAuthTcpOptExc (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
Fsbgp4TCPMKTAuthRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4TCPMKTAuthRowStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
Fsbgp4TCPMKTAuthTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4TCPMKTAuthTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsbgp4TCPAOAuthPeerTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsbgp4TCPAOAuthPeerTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsbgp4TCPAOAuthPeerTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Fsbgp4TCPAOAuthKeyStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4TCPAOAuthPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4TCPAOAuthKeyStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Fsbgp4TCPAOAuthKeyStartAcceptGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4TCPAOAuthPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4TCPAOAuthKeyStartAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4TCPAOAuthKeyStartGenerateGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4TCPAOAuthPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4TCPAOAuthKeyStartGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4TCPAOAuthKeyStopGenerateGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4TCPAOAuthPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4TCPAOAuthKeyStopGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4TCPAOAuthKeyStopAcceptGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsbgp4TCPAOAuthPeerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsbgp4TCPAOAuthKeyStopAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4TCPAOAuthKeyStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4TCPAOAuthKeyStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Fsbgp4TCPAOAuthKeyStartAcceptSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsbgp4TCPAOAuthKeyStartAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4TCPAOAuthKeyStartGenerateSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsbgp4TCPAOAuthKeyStartGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4TCPAOAuthKeyStopGenerateSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsbgp4TCPAOAuthKeyStopGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4TCPAOAuthKeyStopAcceptSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsbgp4TCPAOAuthKeyStopAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
Fsbgp4TCPAOAuthKeyStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4TCPAOAuthKeyStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[2].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
Fsbgp4TCPAOAuthKeyStartAcceptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4TCPAOAuthKeyStartAccept (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[1].
                                                    pOctetStrValue,
                                                    pMultiIndex->pIndex[2].
                                                    i4_SLongValue,
                                                    pMultiData->
                                                    pOctetStrValue));

}

INT4
Fsbgp4TCPAOAuthKeyStartGenerateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4TCPAOAuthKeyStartGenerate (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      pOctetStrValue,
                                                      pMultiIndex->pIndex[2].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      pOctetStrValue));

}

INT4
Fsbgp4TCPAOAuthKeyStopGenerateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4TCPAOAuthKeyStopGenerate (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     pOctetStrValue,
                                                     pMultiIndex->pIndex[2].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     pOctetStrValue));

}

INT4
Fsbgp4TCPAOAuthKeyStopAcceptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2Fsbgp4TCPAOAuthKeyStopAccept (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[2].
                                                   i4_SLongValue,
                                                   pMultiData->pOctetStrValue));

}

INT4
Fsbgp4TCPAOAuthPeerTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Fsbgp4TCPAOAuthPeerTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsBgp4ORFListTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsBgp4ORFListTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue),
             &(pNextMultiIndex->pIndex[5].u4_ULongValue),
             pNextMultiIndex->pIndex[6].pOctetStrValue,
             &(pNextMultiIndex->pIndex[7].u4_ULongValue),
             &(pNextMultiIndex->pIndex[8].u4_ULongValue),
             &(pNextMultiIndex->pIndex[9].u4_ULongValue),
             &(pNextMultiIndex->pIndex[10].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsBgp4ORFListTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue),
             pFirstMultiIndex->pIndex[5].u4_ULongValue,
             &(pNextMultiIndex->pIndex[5].u4_ULongValue),
             pFirstMultiIndex->pIndex[6].pOctetStrValue,
             pNextMultiIndex->pIndex[6].pOctetStrValue,
             pFirstMultiIndex->pIndex[7].u4_ULongValue,
             &(pNextMultiIndex->pIndex[7].u4_ULongValue),
             pFirstMultiIndex->pIndex[8].u4_ULongValue,
             &(pNextMultiIndex->pIndex[8].u4_ULongValue),
             pFirstMultiIndex->pIndex[9].u4_ULongValue,
             &(pNextMultiIndex->pIndex[9].u4_ULongValue),
             pFirstMultiIndex->pIndex[10].i4_SLongValue,
             &(pNextMultiIndex->pIndex[10].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsBgp4RmTestObjectGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsBgp4RmTestObject (&(pMultiData->i4_SLongValue)));
}

INT4
FsBgp4RmTestObjectSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsBgp4RmTestObject (pMultiData->i4_SLongValue));
}

INT4
FsBgp4RmTestObjectTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsBgp4RmTestObject (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsBgp4RmTestObjectDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsBgp4RmTestObject
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
INT4 GetNextIndexFsBgp4RRDNetworkTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsBgp4RRDNetworkTable(
			pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsBgp4RRDNetworkTable(
			pFirstMultiIndex->pIndex[0].pOctetStrValue,
			pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsBgp4RRDNetworkAddrTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsBgp4RRDNetworkTable(
		pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsBgp4RRDNetworkAddrType(
		pMultiIndex->pIndex[0].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsBgp4RRDNetworkPrefixLenGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsBgp4RRDNetworkTable(
		pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsBgp4RRDNetworkPrefixLen(
		pMultiIndex->pIndex[0].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsBgp4RRDNetworkRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsBgp4RRDNetworkTable(
		pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsBgp4RRDNetworkRowStatus(
		pMultiIndex->pIndex[0].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsBgp4RRDNetworkAddrTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsBgp4RRDNetworkAddrType(
		pMultiIndex->pIndex[0].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsBgp4RRDNetworkPrefixLenSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsBgp4RRDNetworkPrefixLen(
		pMultiIndex->pIndex[0].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsBgp4RRDNetworkRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsBgp4RRDNetworkRowStatus(
		pMultiIndex->pIndex[0].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsBgp4RRDNetworkAddrTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsBgp4RRDNetworkAddrType(pu4Error,
		pMultiIndex->pIndex[0].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsBgp4RRDNetworkPrefixLenTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsBgp4RRDNetworkPrefixLen(pu4Error,
		pMultiIndex->pIndex[0].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsBgp4RRDNetworkRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsBgp4RRDNetworkRowStatus(pu4Error,
		pMultiIndex->pIndex[0].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsBgp4RRDNetworkTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsBgp4RRDNetworkTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 Fsbgp4DebugTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsbgp4DebugType(&(pMultiData->u4_ULongValue)));
}
INT4 Fsbgp4DebugTypeIPAddrGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsbgp4DebugTypeIPAddr(pMultiData->pOctetStrValue));
}
INT4 Fsbgp4DebugTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhSetFsbgp4DebugType(pMultiData->u4_ULongValue));
}
INT4 Fsbgp4DebugTypeIPAddrSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhSetFsbgp4DebugTypeIPAddr(pMultiData->pOctetStrValue));
}
INT4 Fsbgp4DebugTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhTestv2Fsbgp4DebugType(pu4Error, pMultiData->u4_ULongValue));
}

INT4 Fsbgp4DebugTypeIPAddrTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhTestv2Fsbgp4DebugTypeIPAddr(pu4Error, pMultiData->pOctetStrValue));
}

INT4 Fsbgp4DebugTypeDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2Fsbgp4DebugType(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 Fsbgp4DebugTypeIPAddrDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2Fsbgp4DebugTypeIPAddr(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
