/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4prcs.c,v 1.46 2017/09/15 06:19:53 siva Exp $
 *
 * Description: This file contains core BGP processing routines.
 *
 *******************************************************************/
#ifndef _BGP4PRCS_C
#define _BGP4PRCS_C

#include "bgp4com.h"

/****************************************************************************/
/* FUNCTION NAME : Bgp4Dispatcher                                           */
/* DESCRIPTION   : This is the Bgp main processing function. This function  */
/*               : should be called from the environment once every second. */
/*               : This function takes care of scheduling various phases of */
/*               : BGP in each instance and performs the task associated    */
/*                  with each phase.                                        */
/* INPUTS        : None                                                     */
/* OUTPUTS       : None                                                     */
/* RETURNS       : BGP4_SUCCESS                                             */
/****************************************************************************/
INT4
Bgp4Dispatcher (VOID)
{
    UINT4               u4Context;
    INT4                i4RetVal = 0;
    tBgpCxtNode        *pBgpCxtNode = NULL;

    for (u4Context = 0;
         u4Context <
         FsBGPSizingParams[MAX_BGP_CXT_NODES_SIZING_ID].u4PreAllocatedUnits;
         u4Context++)
    {
        /* if context is active */
        pBgpCxtNode = Bgp4GetContextEntry (u4Context);
        if ((pBgpCxtNode == NULL) || (BGP4_STATUS (u4Context) == BGP4_FALSE))
        {
            continue;
        }

        BgpSetContextId (u4Context);
        i4RetVal = (INT4) Bgp4DispatcherInCxt (u4Context);
    }
    return (i4RetVal);
}

/****************************************************************************/
/* FUNCTION NAME : Bgp4DispatcherInCxt                                      */
/* DESCRIPTION   : This function takes care of scheduling various phases of */
/*               : BGP in the given instance and performs the task          */
/*                  associated with each phase.                             */
/* INPUTS        : None                                                     */
/* OUTPUTS       : None                                                     */
/* RETURNS       : BGP4_SUCCESS                                             */
/****************************************************************************/
INT4
Bgp4DispatcherInCxt (UINT4 u4Context)
{
    PRIVATE UINT4       u4Event = BGP4_RIBIN_PRCS_EVENT;
    PRIVATE UINT4       u4Event1 = BGP4_PEER_INIT_EVENT;
    INT4                i4RetVal = 0;
    INT4                i4RelinquishFlag = BGP4_FALSE;
    UINT4               u4ElapTime = 0;
    UINT1               u1IsEventComplete = BGP4_FALSE;
    UINT1               u1IsEvent1Complete = BGP4_FALSE;

    BGP4_DBG1 (BGP4_DBG_ENTRY, "\tEntering BGP4 Dispatcher - %d\n",
               Bgp4ElapTime ());

    /* Update Prev entry and exit time. */
    BGP4_DISP_TIME (u4Context)[BGP4_PREV_ENTRY_TIME_INDEX] =
        BGP4_DISP_TIME (u4Context)[BGP4_CUR_ENTRY_TIME_INDEX];
    BGP4_DISP_TIME (u4Context)[BGP4_PREV_EXIT_TIME_INDEX] =
        BGP4_DISP_TIME (u4Context)[BGP4_CUR_EXIT_TIME_INDEX];

    /* recompute all throttle values */
    BgpRecomputeThrottle (u4Context);

    /* Update the current entry time. */
    BGP4_GET_SYS_TIME (&u4ElapTime);
    BGP4_DISP_TIME (u4Context)[BGP4_CUR_ENTRY_TIME_INDEX] = u4ElapTime;

    /* Process Global Admin Operations */
    if (BGP4_GLOBAL_STATE (u4Context) == BGP4_GLOBAL_ADMIN_DOWN_INPROGRESS)
    {
        if (bgp4RibLock () == OSIX_SUCCESS)
        {
            i4RetVal = Bgp4RibhFreeRibtree (u4Context);
            bgp4RibUnlock ();
        }
        else
        {
            i4RetVal = 0;
        }

        if (i4RetVal == BGP4_RELINQUISH)
        {
            /* External tasks requires CPU. */
            i4RelinquishFlag = BGP4_TRUE;
        }
    }

    /* Process Transmission queue for each peer. This needs to be done
     * initially inorder to free the resources allocated by BGP module.
     */
    BGP4_DBG1 (BGP4_DBG_ALL,
               "\tEntering BGP4 Peer Transmission - %d\n", Bgp4ElapTime ());
    i4RetVal = Bgp4ProcessPeerTransmissions (u4Context);
    if (i4RetVal == BGP4_RELINQUISH)
    {
        /* External tasks requires CPU. */
        i4RelinquishFlag = BGP4_TRUE;
    }

    /* Process peer reception */
    BGP4_DBG1 (BGP4_DBG_ALL,
               "\tEntering BGP4 Peer Reception - %d\n", Bgp4ElapTime ());
#ifdef BGP_TCP4_WANTED
    Bgp4TcphWaitOnSelect (u4Context, BGP4_INET_AFI_IPV4);
#endif /* BGP_TCP4_WANTED */
#ifdef BGP_TCP6_WANTED
    Bgp4TcphWaitOnSelect (u4Context, BGP4_INET_AFI_IPV6);
#endif /* BGP_TCP6_WANTED */

    /* Update the FIB update time. */
    if (BGP4_FIB_UPD_HOLD_TIME (u4Context) < BGP4_FIB_UPD_MAX_HOLD_TIME)
    {
        BGP4_FIB_UPD_HOLD_TIME (u4Context)++;
    }

    /* Update the synchronisation Process Interval */
    if ((BGP4_LOCAL_SYNC_STATUS (u4Context) == BGP4_ENABLE_SYNC) &&
        (BGP4_SYNC_INTERVAL (u4Context) < BGP4_SYNC_PRCS_INTERVAL))
    {
        BGP4_SYNC_INTERVAL (u4Context)++;
    }

    /* Update the Next Hop Metric Change process time. */
    if (BGP4_NH_CHG_INTERVAL (u4Context) <
        BGP4_NH_CHG_PRCS_INTERVAL (u4Context))
    {
        BGP4_NH_CHG_INTERVAL (u4Context)++;
    }

#ifdef L3VPN
    if ((BGP4_VPN4_RR_TARGETS_INTERVAL > 0) &&
        (BGP4_VPN4_RR_TARGETS_INTERVAL <= BGP4_VPN4_RR_TARGETS_DEL_INTERVAL))
    {
        BGP4_VPN4_RR_TARGETS_INTERVAL++;
    }
    Bgp4Vpnv4ProcessVrfChngList ();
#endif
#ifdef VPLSADS_WANTED
    if ((BGP4_VPLS_RR_TARGETS_INTERVAL > 0) &&
        (BGP4_VPLS_RR_TARGETS_INTERVAL <= BGP4_VPLS_RR_TARGETS_DEL_INTERVAL))
    {
        BGP4_VPLS_RR_TARGETS_INTERVAL++;
    }
#endif

    for (;;)
    {
        if ((u1IsEventComplete == BGP4_TRUE) &&
            (u1IsEvent1Complete == BGP4_TRUE))
        {
            /* No more processing needs to be done for this tic. */
            break;
        }

        if (u1IsEventComplete == BGP4_FALSE)
        {
            /* BGP processing is divided into four main events. 
             * 1) Updating the Forwarding Information Base
             * 2) Sending the updates to the External/Internal Peer 
             * 3) Processing the Peer initialisation/deinitialisation
             * 4) Processing the Input RIB. 
             * All the above phase are executed in such a way that the same
             * event is not executed for consecutive tics, so that all events
             * shares CPU evenly. */

            switch (u4Event)
            {
                case BGP4_RIBIN_PRCS_EVENT:
                    /* Process Input RIB for all peers  */
                    BGP4_DBG1 (BGP4_DBG_ALL,
                               "\tEntering BGP4 Peer Input RIB - %d\n",
                               Bgp4ElapTime ());
                    i4RetVal = Bgp4ProcessInputRIB (u4Context);
                    break;

#ifdef L3VPN
                case BGP4_RIBIN_VPN4_ROUTE_PRCS_EVENT:
                    /* Process Input RIB for all peers  */
                    BGP4_DBG1 (BGP4_DBG_ALL,
                               "\tEntering BGP4 Vpnv4 RIB- %d\n",
                               Bgp4ElapTime ());
                    /* Process Vpnv4 route processing */
                    i4RetVal = Bgp4Vpnv4ProcessFeasWdrawRts ();
                    break;
#endif

                case BGP4_FIB_UPD_EVENT:
                    if (BGP4_FIB_UPD_HOLD_TIME (u4Context) >=
                        BGP4_FIB_UPD_MAX_HOLD_TIME)
                    {
                        BGP4_DBG1 (BGP4_DBG_ALL, "\tEntering BGP4 FIB- %d\n",
                                   Bgp4ElapTime ());
                        i4RetVal = Bgp4ProcessFIBUpdates (u4Context);
                    }
                    break;

                case BGP4_EXT_PEER_ADVT_EVENT:
                    BGP4_DBG1 (BGP4_DBG_ALL,
                               "\tEntering BGP4 External Peer Advt- %d\n",
                               Bgp4ElapTime ());
                    i4RetVal = Bgp4ProcessExternalPeerAdvt (u4Context);
                    break;

                case BGP4_INT_PEER_ADVT_EVENT:
                    BGP4_DBG1 (BGP4_DBG_ALL,
                               "\tEntering BGP4 Internal Peer Advt- %d\n",
                               Bgp4ElapTime ());
                    i4RetVal = Bgp4ProcessInternalPeerAdvt (u4Context);
                    break;
                case BGP4_RM_BULK_PRCS_EVENT:
                    if (BGP4_RED_BULK_UPD_STATUS == BGP4_HA_UPD_IN_PROGRESS)
                    {
                        BGP4_DBG1 (BGP4_DBG_ALL,
                                   "Entering BGP4 RM BULK UPDATE - %d\n",
                                   Bgp4ElapTime ());
                        i4RetVal = Bgp4RedSendBulkUpdMsg ();
                    }
                    else
                    {
                        i4RetVal = BGP4_SUCCESS;
                    }
                    break;
                default:
                    break;
            }

            /* Update the event to be done during next iteration. This
             * ensures that the same event does not take away the complete
             * CPU utilisation. */
            if (u4Event >= BGP4_RM_BULK_PRCS_EVENT)
            {
                /* Reset the event */
                u4Event = BGP4_RIBIN_PRCS_EVENT;
                u1IsEventComplete = BGP4_TRUE;
            }
            else
            {
                u4Event++;
            }

            /* Need to process for each event. */
            if (i4RetVal == BGP4_RELINQUISH)
            {
                /* External tasks requires CPU. */
                i4RelinquishFlag = BGP4_TRUE;
            }
        }

        if (u1IsEvent1Complete == BGP4_FALSE)
        {
            /* Process all peer related operations. */
            switch (u4Event1)
            {
                case BGP4_PEER_INIT_EVENT:
                    BGP4_DBG1 (BGP4_DBG_ALL, "\tEntering BGP4 Peer Init- %d\n",
                               Bgp4ElapTime ());
                    i4RetVal = Bgp4ProcessPeerInit (u4Context);
                    if ((i4RetVal == BGP4_FALSE) &&
                        (BGP4_GLOBAL_STATE (u4Context) ==
                         BGP4_GLOBAL_ADMIN_UP_INPROGRESS))
                    {
                        BGP4_GLOBAL_STATE (u4Context) = BGP4_GLOBAL_READY;
                    }
                    break;

                case BGP4_PEER_DEINIT_EVENT:
                    BGP4_DBG1 (BGP4_DBG_ALL,
                               "\tEntering BGP4 Peer Deinit- %d\n",
                               Bgp4ElapTime ());
                    i4RetVal = Bgp4ProcessPeerDeInit (u4Context);
                    break;

                case BGP4_PEER_SOFT_IN_EVENT:
                    BGP4_DBG1 (BGP4_DBG_ALL,
                               "\tEntering BGP4 Peer SoftIn %d\n",
                               Bgp4ElapTime ());
                    i4RetVal = Bgp4FiltApplyInboundSoftCfgSendRtRef (u4Context);
                    break;

                case BGP4_PEER_SOFT_OUT_EVENT:
                    BGP4_DBG1 (BGP4_DBG_ALL,
                               "\tEntering BGP4 Peer SoftOut %d\n",
                               Bgp4ElapTime ());
                    i4RetVal = Bgp4FiltApplyOutBoundSoftCfg (u4Context);
                    break;

#ifdef RFD_WANTED
                case BGP4_PEER_REUSE_EVENT:
                    BGP4_DBG1 (BGP4_DBG_ALL,
                               "\tEntering BGP4 Peer Reuse List %d\n",
                               Bgp4ElapTime ());
                    i4RetVal = RfdSupPeerReuseListHandler (u4Context);
                    break;
#endif
#ifdef L3VPN
                case BGP4_EXPTGT_EVENT:
                    BGP4_DBG1 (BGP4_DBG_ALL,
                               "\tEntering BGP4 VRF export Out - %d\n",
                               Bgp4ElapTime ());
                    i4RetVal = Bgp4Vpnv4ProcessVrfChngList ();
                    break;

                case BGP4_LSP_CHNG_EVENT:
                    BGP4_DBG1 (BGP4_DBG_ALL,
                               "\tEntering BGP4 Lsp Chng List - %d\n",
                               Bgp4ElapTime ());
                    i4RetVal = Bgp4ProcessLspChngList ();
                    break;
#endif
                default:
                    break;
            }

            /* Update the event to be done during next iteration. This
             * ensures that the same event does not take away the complete
             * CPU utilisation. */
#ifdef RFD_WANTED
            if (u4Event1 >= BGP4_PEER_REUSE_EVENT)
#else
            if (u4Event1 >= BGP4_PEER_SOFT_OUT_EVENT)
#endif
            {
                /* Reset the event1 to the first peer event */
                u4Event1 = BGP4_PEER_INIT_EVENT;
                u1IsEvent1Complete = BGP4_TRUE;
            }
            else
            {
                u4Event1++;
            }

            if (i4RetVal == BGP4_RELINQUISH)
            {
                /* External tasks requires CPU. */
                i4RelinquishFlag = BGP4_TRUE;
            }
        }

        /* Process the Pend Job activity for all iterations. */
        Bgp4ProcessPendJobs (u4Context);

        if (i4RelinquishFlag == BGP4_TRUE)
        {
            break;
        }
    }

    /* Process Non BGP Routes for re-distribution */
    BGP4_DBG1 (BGP4_DBG_ALL, "\tEntering BGP4 Redist %d\n", Bgp4ElapTime ());
    Bgp4ProcessRedisUpdates (u4Context);

    /* Process Next hop resolution */
    if (BGP4_NH_CHG_INTERVAL (u4Context) >=
        BGP4_NH_CHG_PRCS_INTERVAL (u4Context))
    {
        if (bgp4RibLock () == OSIX_SUCCESS)
        {
            BGP4_DBG1 (BGP4_DBG_ALL,
                       "\tEntering BGP4 Next-Hop %d\n", Bgp4ElapTime ());
            Bgp4ProcessNextHopResolution (u4Context);
            bgp4RibUnlock ();
        }
    }

    /* Process Synchronisation event.  */
    if ((BGP4_LOCAL_SYNC_STATUS (u4Context) == BGP4_ENABLE_SYNC) &&
        (BGP4_SYNC_INTERVAL (u4Context) >= BGP4_SYNC_PRCS_INTERVAL))
    {
        if (bgp4RibLock () == OSIX_SUCCESS)
        {
            Bgp4SyncProcessSynchronisation (u4Context);
            bgp4RibUnlock ();
        }
    }

#ifdef L3VPN
    /* Delete import route target from the RR targets set */
    if (BGP4_VPN4_RR_TARGETS_INTERVAL > BGP4_VPN4_RR_TARGETS_DEL_INTERVAL)
    {
        Bgp4Vpnv4RrDeleteTargetFromSet ();
    }
#endif
#ifdef VPLSADS_WANTED
    if (BGP4_VPLS_RR_TARGETS_INTERVAL > BGP4_VPLS_RR_TARGETS_DEL_INTERVAL)
    {
        Bgp4VplsRrDeleteTargetFromSet ();
    }
#endif
#ifdef EVPN_WANTED
    if (BGP4_EVPN_RR_TARGETS_INTERVAL > BGP4_EVPN_RR_TARGETS_DEL_INTERVAL)
    {
        Bgp4EvpnRrDeleteTargetFromSet ();
    }
#endif
    /* Process Aggregation */
    Bgp4AggregationHandler (u4Context);

    /* Update the current exit time. */
    BGP4_GET_SYS_TIME (&u4ElapTime);
    BGP4_DISP_TIME (u4Context)[BGP4_CUR_EXIT_TIME_INDEX] = u4ElapTime;

    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Bgp4ProcessInputRIB                                       */
/* Description   : This function process the Input RIB. All the peer entries */
/*                 have received route list holding the routes received from */
/*                 that peer. If any route is present in this list, then     */
/*                 this acts as a trigger to process the INPUT RIB.          */
/*                 The maximum number of peers(BGP4_MAX_PEERRTS2PROCESS) and */
/*                 routes(BGP4_MAX_PKTS2PROCESS) to be processed can be      */
/*                 controlled by configuring the default value.              */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return        : BGP4_TRUE  - if more routes or peers remains to be        */
/*                 processed.                                                */
/*               : BGP4_FALSE - if no more routes or peers available for     */
/*                 processing                                                */
/*                 BGP4_RELINQUISH - if external events requires CPU         */
/*****************************************************************************/
INT4
Bgp4ProcessInputRIB (UINT4 u4Context)
{

    tLinkNode          *pLinkNode = NULL;
    tBgp4PeerEntry     *pPeerentry = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pFndRtProfileList = NULL;
    tRouteProfile      *pRtList = NULL;
    UINT4               u4RtCnt = 0;
    PRIVATE UINT4       u4Cnt = 0;
    UINT4               u4PeerRtCnt = 0;
    UINT4               u4Iterate = BGP4_FALSE;
    UINT4               u4ProcessRoutes = BGP4_TRUE;
    INT4                i4RetVal = BGP4_SUCCESS;
    INT4                i4RtFound = BGP4_FAILURE;
    VOID               *pRibNode = NULL;

    if (gBgpCxtNode[u4Context] == NULL)
    {
        return (BGP4_FALSE);
    }
    if (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_DOWN)
    {
        /* No need to process */
        return (BGP4_FALSE);
    }

    if (u4Cnt != 0)
    {
        /* Since we are not processing from the first peer in the
         * peer list, we need atleast one iterations */
        u4Iterate = BGP4_TRUE;
    }

    while (u4Cnt <= BGP4_PEER_DATA_MAX_PEER (u4Context))
    {
        /* Reset the u4PeerRtCnt */
        u4PeerRtCnt = 0;

        if (BGP4_PEER_DATA_PEER_PTR (u4Context)[u4Cnt] == NULL)
        {
            u4Cnt++;
            if ((u4Cnt > BGP4_PEER_DATA_MAX_PEER (u4Context))
                && (u4Iterate == BGP4_TRUE))
            {
                u4Iterate = BGP4_FALSE;
                u4Cnt = 0;
            }
            continue;
        }
        pPeerentry = BGP4_PEER_DATA_PEER_PTR (u4Context)[u4Cnt];

        if (BGP4_GET_NODE_STATUS != RM_STANDBY)
        {
            if ((BGP4_PEER_STATE (pPeerentry) != BGP4_ESTABLISHED_STATE) &&
                (BGP4_GET_PEER_CURRENT_STATE (pPeerentry) !=
                 BGP4_PEER_STALE_DEL_INPROGRESS))
            {
                u4Cnt++;
                if ((u4Cnt > BGP4_PEER_DATA_MAX_PEER (u4Context))
                    && (u4Iterate == BGP4_TRUE))
                {
                    u4Iterate = BGP4_FALSE;
                    u4Cnt = 0;
                }
                continue;
            }
        }
        if (TMO_SLL_Count (BGP4_PEER_RCVD_ROUTES (pPeerentry)) == 0)
        {
            u4Cnt++;
            if ((u4Cnt > BGP4_PEER_DATA_MAX_PEER (u4Context))
                && (u4Iterate == BGP4_TRUE))
            {
                u4Iterate = BGP4_FALSE;
                u4Cnt = 0;
            }
            if (BGP4_GET_PEER_CURRENT_STATE (pPeerentry) ==
                BGP4_PEER_STALE_DEL_INPROGRESS)
            {
                Bgp4GRDeleteAllStaleRoutes (pPeerentry);
            }
            continue;
        }

        for (;;)
        {
            if (u4RtCnt >= BGP4_MAX_PKTS2PROCESS)
            {
                /* Has processed MAX updates from the RIB IN */
                break;
            }

            pLinkNode =
                (tLinkNode *)
                TMO_SLL_First (BGP4_PEER_RCVD_ROUTES (pPeerentry));

            if ((u4PeerRtCnt >= BGP4_MAX_PEERRTS2PROCESS) ||
                (pLinkNode == NULL))
            {
                if (pLinkNode != NULL)
                {
                    /* Set the flag u4Iterate to indicate that this peer
                     *
                     * has some more routes to be process. */
                    u4Iterate = BGP4_TRUE;
                }
                if (pLinkNode == NULL)
                {
                    if (BGP4_GET_PEER_CURRENT_STATE (pPeerentry) ==
                        BGP4_PEER_STALE_DEL_INPROGRESS)
                    {
                        Bgp4GRDeleteAllStaleRoutes (pPeerentry);
                    }
                }
                break;
            }

            i4RetVal = BGP4_SUCCESS;
            pRtProfile = pLinkNode->pRouteProfile;
            /* Try and get the route from the RIB. If the route is present as stale  
               in the  RIB also, then process the route. Else do not process it. */
            i4RtFound =
                Bgp4RibhLookupRtEntry (pRtProfile,
                                       BGP4_PEER_CXT_ID (pPeerentry),
                                       BGP4_TREE_FIND_EXACT, &pFndRtProfileList,
                                       &pRibNode);
            if (i4RtFound == BGP4_SUCCESS)
            {
                pRtList = pFndRtProfileList;
                while (pRtList != NULL)
                {
                    if (((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_STALE) ==
                         BGP4_RT_STALE)
                        && ((BGP4_RT_GET_FLAGS (pRtList) & BGP4_RT_STALE) !=
                            BGP4_RT_STALE)
                        &&
                        (Bgp4AttrIsBgpinfosIdentical
                         (BGP4_RT_BGP_INFO (pRtProfile),
                          BGP4_RT_BGP_INFO (pRtList))))
                    {
                        u4ProcessRoutes = BGP4_FALSE;
                        break;
                    }
                    pRtList = BGP4_RT_NEXT (pRtList);
                }

            }

            if (u4ProcessRoutes != BGP4_FALSE)
            {
                if (BGP4_LOCAL_SYNC_STATUS (u4Context) == BGP4_ENABLE_SYNC)
                {
                    /* If the route has been received from an external peer then
                     * no need to apply synchronisation. Else apply 
                     * synchronisation */
                    if ((BGP4_GET_PEER_TYPE (u4Context, pPeerentry) ==
                         BGP4_EXTERNAL_PEER)
                        && (BGP4_CONFED_PEER_STATUS (pPeerentry) == BGP4_FALSE))
                    {
                        /* Route leanrt from external peer. */
                    }
                    else
                    {
                        /* Apply synchronisation. */
                        i4RetVal =
                            Bgp4SyncCheckForSync (pRtProfile, pPeerentry);
                        if (i4RetVal == BGP4_FAILURE)
                        {
                            /* If the route is a feasible route, now send this as
                             * withdrawn route. */
                            if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                                 BGP4_RT_WITHDRAWN) != BGP4_RT_WITHDRAWN)
                            {
                                BGP4_RT_SET_FLAG (pRtProfile,
                                                  BGP4_RT_WITHDRAWN);
                                i4RetVal = BGP4_SUCCESS;
                            }
                        }
                    }
                }

                if (i4RetVal == BGP4_SUCCESS)
                {
                    if (bgp4RibLock () != OSIX_SUCCESS)
                    {
                        return BGP4_TRUE;
                    }

                    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_WITHDRAWN) ==
                        BGP4_RT_WITHDRAWN)
                    {
                        /* Route is a withdrawn route from the peer pPeerentry. */
                        Bgp4RibhProcessWithdRoute (pPeerentry, pRtProfile,
                                                   NULL);

                        /* Remove the route from the FIB */
                        if ((BGP4_GET_PEER_CURRENT_STATE (pPeerentry) ==
                             BGP4_PEER_STALE_DEL_INPROGRESS))
                        {
                            if (((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_BEST)
                                 == BGP4_RT_BEST)
                                &&
                                ((BGP4_RT_GET_FLAGS (pRtProfile) &
                                  BGP4_RT_IN_FIB_UPD_LIST) !=
                                 BGP4_RT_IN_FIB_UPD_LIST))
                            {
                                Bgp4IgphUpdateRouteInFIB (pRtProfile);
                            }
                        }
                    }
                    else
                    {
                        /* Route is a feasible route from the peer pPeerentry. */
                        BGP4_RT_RESET_FLAG (pRtProfile,
                                            (BGP4_RT_WITHDRAWN |
                                             BGP4_RT_REPLACEMENT));
                        Bgp4RibhProcessFeasibleRoute (pPeerentry, pRtProfile);
                    }
                    bgp4RibUnlock ();
                }
            }
            u4ProcessRoutes = BGP4_TRUE;
            /* Route processing complete. Remove it from the peer's Received
             * route list */
            Bgp4DshRemoveNodeFromList (BGP4_PEER_RCVD_ROUTES (pPeerentry),
                                       pLinkNode);

            /* Increment the total route count and peer route count. */
            u4RtCnt++;
            u4PeerRtCnt++;

        }

        if (u4RtCnt >= BGP4_MAX_PKTS2PROCESS)
        {
            /* Increment the u4Cnt, so that next time we start processing from
             * the next peer. */
            u4Cnt++;
            if (u4Cnt > BGP4_PEER_DATA_MAX_PEER (u4Context))
            {
                u4Cnt = 0;
            }
            /* Has reached the MAX RIBIN updates to be processed. Check for
             * Tic Break event. If return value is BGP4_TURE, then the
             * environment may require the CPU. So BGP should relinquish
             * the CPU. */
            if (Bgp4IsCpuNeeded () == BGP4_TRUE)
            {
                /* External events requires CPU. Relinquish CPU. */
                return (BGP4_RELINQUISH);
            }
            else
            {
                if (u4Iterate == BGP4_TRUE)
                {
                    /* Still routes are pending in RIB IN for processing */
                    return BGP4_TRUE;
                }
                else
                {
                    /* No more routes left to be processed in RIB IN */
                    if ((BGP4_GET_PEER_CURRENT_STATE (pPeerentry) ==
                         BGP4_PEER_STALE_DEL_INPROGRESS))
                    {
                        BGP4_SET_PEER_CURRENT_STATE (pPeerentry,
                                                     BGP4_PEER_READY);
                    }
                    return BGP4_FALSE;
                }
            }
        }

        if (u4Cnt == BGP4_PEER_DATA_MAX_PEER (u4Context))
        {
            /* Has not processed the MAX updates. Check whether still updates
             * are present in RIB IN to be processed. If yes, then keep
             * processing. */
            if (u4Iterate == BGP4_TRUE)
            {
                u4Cnt = 0;
                u4Iterate = BGP4_FALSE;
                continue;
            }
        }
        u4Cnt++;
    }

    /* No more updates present in RIB in to be processed. */
    u4Cnt = 0;
    return BGP4_FALSE;
}

/****************************************************************************/
/* FUNCTION NAME : Bgp4ProcessFIBUpdates                                    */
/* DESCRIPTION   : This function handles the updation of the Forward        */
/*               : Information Base table. The FIB update list contains     */
/*               : routes (both new feasible and withdrawn) that needs to   */
/*               : be updated in the FIB.                                   */
/* INPUTS        : None                                                     */
/* OUTPUTS       : None                                                     */
/* Return        : BGP4_TRUE  - if more routes remains to be processed.     */
/*               : BGP4_FALSE - if no more routes available for processing  */
/*                 BGP4_RELINQUISH - if external events requires CPU        */
/****************************************************************************/
INT4
Bgp4ProcessFIBUpdates (UINT4 u4Context)
{
    tRouteProfile      *pRtProfile = NULL;
    UINT4               u4ProcUptCount = 0;

    /* FIB can be updated. Process the FIB update list. */

    if ((BGP4_ROUTE_SELECTION_FLAG (u4Context) == BGP4_TRUE)
        || (BGP4_RESTART_REASON (u4Context) == BGP4_GR_REASON_UPGRADE))
    {
        for (;;)
        {
            if (u4ProcUptCount == BGP4_MAX_FIB_UPD2PROCESS)
            {
                /* Has reached the MAX FIB update to be processed. Check for the
                 * Tic Break event. If return value is BGP4_TURE, then the
                 * environment may require the CPU. So BGP should relinquish
                 * the CPU. */
                if (Bgp4IsCpuNeeded () == BGP4_TRUE)
                {
                    /* External events requires CPU. Relinquish CPU. */
                    return (BGP4_RELINQUISH);
                }
                else
                {
                    /* Maximum routes processed. Can return. */
                    break;
                }
            }

            /* Get the first entry from the list. */
            pRtProfile = BGP4_FIB_LIST_FIRST_ROUTE (u4Context);
            if (pRtProfile == NULL)
            {
                /* The peer list is completely processed. Hence allow
                 * the peer initialisation by setting the flag true */
                if (Bgp4GRCheckRestartMode (u4Context) == BGP4_RESTARTING_MODE)
                {
                    gBgpCxtNode[u4Context]->u1AllowPeerInit = BGP4_TRUE;
                }
                break;
            }
            /* Increment the processed route count. */
            u4ProcUptCount++;

            /* Update route to FIB */
            Bgp4IgphUpdateRouteInFIB (pRtProfile);

            /* Remove the route from the FIB update list. */
            BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_IN_FIB_UPD_LIST);
            Bgp4DshDelinkRouteFromList (BGP4_FIB_UPD_LIST (u4Context),
                                        pRtProfile, BGP4_FIB_UPD_LIST_INDEX);
        }

        if (BGP4_FIB_LIST_COUNT (u4Context) > 0)
        {
            return (BGP4_TRUE);
        }

        /* Re-set the FIB Update Interval */
        BGP4_FIB_UPD_HOLD_TIME (u4Context) = 0;
    }
    return (BGP4_FALSE);
}

/****************************************************************************/
/* FUNCTION NAME : Bgp4ProcessRedisUpdates                                  */
/* DESCRIPTION   : This function handles the routes learnt by redistribution*/
/*               : The Redistribution update list holds the routes that are */
/*               : learnt via redistribution (both feasible and withdrawn)  */
/* INPUTS        : None                                                     */
/* OUTPUTS       : None                                                     */
/* Return        : BGP4_TRUE  - if more routes remains to be processed.     */
/*               : BGP4_FALSE - if no more routes available for processing  */
/*                 BGP4_RELINQUISH - if external events requires CPU        */
/****************************************************************************/
INT4
Bgp4ProcessRedisUpdates (UINT4 u4Context)
{

    tLinkNode          *pLinkNode = NULL;
    tRouteProfile      *pRtProfile = NULL;
    UINT4               u4ProcUptCount = 0;

    if (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_DOWN)
    {
        return BGP4_FALSE;
    }

    /* Process the RRD Pending Event. */
    if (BGP4_NON_BGP_EVENT_RCVD (u4Context) != 0)
    {
        /* Process the non-bgp routes for change in policy */
        Bgp4AhHandleNonBgpRtAdvtPolicyChange (u4Context,
                                              BGP4_NON_BGP_EVENT_RCVD
                                              (u4Context));
    }

    for (;;)
    {
        if (u4ProcUptCount == BGP4_MAX_REDIS_UPD2PROCESS)
        {
            /* Has reached the MAX Redistribution updates to be processed.
             * Check for the Tic Break event. If return value is BGP4_TURE,
             * then the environment may require the CPU. So BGP should
             * relinquish the CPU. */
            if (Bgp4IsCpuNeeded () == BGP4_TRUE)
            {
                /* External events requires CPU. Relinquish CPU. */
                return (BGP4_RELINQUISH);
            }
            else
            {
                /* Maximum routes processed. Can return. */
                break;
            }
        }

        Bgp4RedistLock ();
        /* Get the first entry from the list. */
        pLinkNode = (tLinkNode *) TMO_SLL_First (BGP4_REDIST_LIST (u4Context));

        if (pLinkNode == NULL)
        {
            /* List is empty. No need of any processing to be done. */
            Bgp4RedistUnLock ();
            break;
        }

        pRtProfile = pLinkNode->pRouteProfile;
        /* Increment the processed route count. */
        u4ProcUptCount++;

        /* Check whether the route is feasible or withdrawn. */

        if (bgp4RibLock () != OSIX_SUCCESS)
        {
            Bgp4RedistUnLock ();
            return BGP4_TRUE;
        }
        if (BGP4_GET_NODE_STATUS != RM_STANDBY)
        {
            if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_WITHDRAWN) ==
                BGP4_RT_WITHDRAWN)
            {
                if ((BGP4_RT_GET_ADV_FLAG (pRtProfile) & BGP4_REDIST_ADV_ROUTE)
                    == BGP4_REDIST_ADV_ROUTE)
                {
                    /* Route is a withdrawn route. */
                    Bgp4IgphProcessWithdrawnImportRoute (pRtProfile);

                }
            }
            else
            {
                /* Route is a feasible route. */
                Bgp4IgphProcessFeasibleImportRoute (pRtProfile);

            }
        }
        bgp4RibUnlock ();
        Bgp4RedistUnLock ();

        /* Remove the route from the Redistribution update list. */
        BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_IN_REDIST_LIST);
        Bgp4DshRemoveNodeFromList (BGP4_REDIST_LIST (u4Context), pLinkNode);

    }

    if (TMO_SLL_Count (BGP4_REDIST_LIST (u4Context)) > 0)
    {
        return (BGP4_TRUE);
    }
    else
    {
        return (BGP4_FALSE);
    }
}

/****************************************************************************/
/* FUNCTION NAME : Bgp4ProcessExternalPeerAdvt                              */
/* DESCRIPTION   : This function handles the advertisement of routes to the */
/*               : external peers. The route to be advertised to external   */
/*               : peers are present in the external peer advertisement list*/
/*               : This list contains both the feasible and withdrawn routes*/
/*               : that are to be advertised to the external peers.         */
/* INPUTS        : None                                                     */
/* OUTPUTS       : None                                                     */
/* Return        : BGP4_TRUE  - if more routes remains to be processed.     */
/*               : BGP4_FALSE - if no more routes available for processing  */
/*                 BGP4_RELINQUISH - if external events requires CPU        */
/****************************************************************************/
INT4
Bgp4ProcessExternalPeerAdvt (UINT4 u4Context)
{
    tTMO_SLL            TsFeasibleRtList;
    tTMO_SLL            TsWithdrawnRtList;
    tLinkNode          *pLinkNode = NULL;
    tLinkNode          *pTmpLinkNode = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tBgp4PeerEntry     *pPeerentry = NULL;
    UINT4               u4ProcUptCount = 0;
#ifdef  L3VPN
    UINT4               u4AsafiMask = 0;
#endif

    TMO_SLL_Init (&TsFeasibleRtList);
    TMO_SLL_Init (&TsWithdrawnRtList);

    if (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_DOWN)
    {
        return BGP4_FALSE;
    }

    if ((BGP4_ROUTE_SELECTION_FLAG (u4Context) == BGP4_TRUE)
        || (BGP4_RESTART_REASON (u4Context) == BGP4_GR_REASON_UPGRADE))
    {
        /* Process the External peers advt list. */
        for (;;)
        {
            /* Collect the routes from the External peer Advt list and form the
             * list of feasible and withdrawn routes. */
            pRtProfile = BGP4_EXT_PEER_LIST_FIRST_ROUTE (u4Context);
            if (pRtProfile == NULL)
            {
                /* List is empty. No need of any processing to be done. */
                break;
            }
            /* if <afi, safi> differs, then send the routes collected for this
             * <afi, safi> and process the remaining routes in the next
             * iteration
             */
#ifdef  L3VPN
            /*It is assumed that one route will be unfeasible in one vrf and 
             * feasible in another Vrf. If this condition occurs, this has to 
             * be modified*/
#endif
            if (((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_WITHDRAWN) ==
                 BGP4_RT_WITHDRAWN) ||
                ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGR_WITHDRAWN) ==
                 BGP4_RT_AGGR_WITHDRAWN))
            {
                /* Route is a withdrawn route. Add to withdrawn route list. */
                Bgp4DshAddRouteToList (&TsWithdrawnRtList, pRtProfile, 0);
            }
            else
            {
                /* Route is a feasible route. Add to feasible route list. */
                Bgp4DshAddRouteToList (&TsFeasibleRtList, pRtProfile,
                                       BGP4_TRUE);
            }

            /* Remove the route from the External peer advertise list. */
            BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_IN_EXT_PEER_ADVT_LIST);
#ifdef  L3VPN
            pRtProfile->u4ExtListCxtId = u4Context;
#endif
            Bgp4DshDelinkRouteFromList (BGP4_EXT_PEERS_ADVT_LIST (u4Context),
                                        pRtProfile, BGP4_EXT_PEER_LIST_INDEX);

            /* Increment the processed route count. */
            u4ProcUptCount++;
            if (u4ProcUptCount >= BGP4_MAX_EXT_PEER_UPD2PROCESS)
            {
                break;
            }

        }
        /* Process the Routes in the withdrawn and feasible route list. */
        if ((TMO_SLL_Count (&TsWithdrawnRtList) > 0) ||
            (TMO_SLL_Count (&TsFeasibleRtList) > 0))
        {
            /* Route needs to be sent to all external peers */
            TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeerentry,
                          tBgp4PeerEntry *)
            {
                if ((BGP4_GET_PEER_TYPE (u4Context, pPeerentry) !=
                     BGP4_EXTERNAL_PEER)
                    ||
                    ((BGP4_GET_PEER_TYPE (u4Context, pPeerentry) ==
                      BGP4_EXTERNAL_PEER)
                     && (BGP4_CONFED_PEER_STATUS (pPeerentry) == BGP4_TRUE)))
                {
                    /* Peer is internal/confed peer. No need to advertise to
                     * this peer. */
                    continue;
                }

                if (BGP4_PEER_STATE (pPeerentry) != BGP4_ESTABLISHED_STATE)
                {
                    /* Connection not in established state. So no need to
                     * advertise to this peer. */
                    continue;
                }

                /* Route can be advertised to this peer. */
                Bgp4PeerSendRoutesToPeerTxQ (pPeerentry, &TsFeasibleRtList,
                                             &TsWithdrawnRtList, BGP4_TRUE);
            }

            BGP_SLL_DYN_Scan (&TsFeasibleRtList, pLinkNode, pTmpLinkNode,
                              tLinkNode *)
            {
                pRtProfile = pLinkNode->pRouteProfile;
                /* Reset the flag for all the routes in the list. */
                if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                     BGP4_RT_IN_INT_PEER_ADVT_LIST) == 0)
                {
                    BGP4_RT_RESET_FLAG (pRtProfile, (BGP4_RT_WITHDRAWN |
                                                     BGP4_RT_AGGR_WITHDRAWN |
                                                     BGP4_RT_ADVT_NOTTO_INTERNAL
                                                     | BGP4_RT_REPLACEMENT));
                }
                BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_ADVT_NOTTO_EXTERNAL);
#ifdef L3VPN

                BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                                       BGP4_RT_SAFI_INFO (pRtProfile),
                                       u4AsafiMask);
                if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
                {
                    Bgp4Vpnv4ResetFlagsForAllVrf (pRtProfile,
                                                  BGP4_RT_VRF_WITHDRAWN |
                                                  BGP4_RT_VRF_ADVT_NOTTO_EXTERNAL
                                                  | BGP4_RT_VRF_REPLACEMENT);
                }
#endif
                Bgp4DshRemoveNodeFromList (&TsFeasibleRtList, pLinkNode);
            }

            BGP_SLL_DYN_Scan (&TsWithdrawnRtList, pLinkNode, pTmpLinkNode,
                              tLinkNode *)
            {
                pRtProfile = pLinkNode->pRouteProfile;
                /* Reset the flag for all the routes in the list. */
                if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                     BGP4_RT_IN_INT_PEER_ADVT_LIST) == 0)
                {
                    BGP4_RT_RESET_FLAG (pRtProfile, (BGP4_RT_WITHDRAWN |
                                                     BGP4_RT_AGGR_WITHDRAWN |
                                                     BGP4_RT_ADVT_NOTTO_INTERNAL
                                                     | BGP4_RT_REPLACEMENT));
                }
                BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_ADVT_NOTTO_EXTERNAL);
#ifdef L3VPN

                BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                                       BGP4_RT_SAFI_INFO (pRtProfile),
                                       u4AsafiMask);
                if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
                {
                    Bgp4Vpnv4ResetFlagsForAllVrf (pRtProfile,
                                                  BGP4_RT_VRF_WITHDRAWN |
                                                  BGP4_RT_VRF_ADVT_NOTTO_EXTERNAL
                                                  | BGP4_RT_VRF_REPLACEMENT);
                }
#endif
                Bgp4DshRemoveNodeFromList (&TsWithdrawnRtList, pLinkNode);
            }
        }

        Bgp4DshReleaseList (&TsFeasibleRtList, 0);
        Bgp4DshReleaseList (&TsWithdrawnRtList, 0);

        if (u4ProcUptCount >= BGP4_MAX_EXT_PEER_UPD2PROCESS)
        {
            /* Has reached the MAX External peer updates to be processed.
             * Check for the Tic Break event. If return value is BGP4_TURE,
             * then the environment may require the CPU. So BGP should
             * relinquish the CPU. */
            if (Bgp4IsCpuNeeded () == BGP4_TRUE)
            {
                /* External events requires CPU. Relinquish CPU. */
                return (BGP4_RELINQUISH);
            }
        }

        if (BGP4_EXT_PEER_LIST_COUNT (u4Context) > 0)
        {
            return (BGP4_TRUE);
        }
        else
        {
            return (BGP4_FALSE);
        }
    }
    return BGP4_FALSE;
}

/****************************************************************************/
/* FUNCTION NAME : Bgp4ProcessInternalPeerAdvt                              */
/* DESCRIPTION   : This function handles the advertisement of routes to the */
/*               : internal peers. The route to be advertised to internal   */
/*               : peers are present in the internal peer advertisement list*/
/*               : This list contains both the feasible and withdrawn routes*/
/*               : that are to be advertised to the internal peers.         */
/* INPUTS        : None                                                     */
/* OUTPUTS       : None                                                     */
/* Return        : BGP4_TRUE  - if more routes remains to be processed.     */
/*               : BGP4_FALSE - if no more routes available for processing  */
/*                 BGP4_RELINQUISH - if external events requires CPU        */
/****************************************************************************/
INT4
Bgp4ProcessInternalPeerAdvt (UINT4 u4Context)
{
    tTMO_SLL            TsFeasibleRtList;
    tTMO_SLL            TsWithdrawnRtList;
    tLinkNode          *pLinkNode = NULL;
    tLinkNode          *pTmpLinkNode = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tBgp4PeerEntry     *pPeerentry = NULL;
    UINT4               u4ProcUptCount = 0;
    UINT4               u4AsafiMask = 0;

    TMO_SLL_Init (&TsFeasibleRtList);
    TMO_SLL_Init (&TsWithdrawnRtList);

    if (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_DOWN)
    {
        return BGP4_FALSE;
    }

    if ((BGP4_ROUTE_SELECTION_FLAG (u4Context) == BGP4_TRUE)
        || (BGP4_RESTART_REASON (u4Context) == BGP4_GR_REASON_UPGRADE))
    {
        /* Process the Internal peers advt list. */
        for (;;)
        {
            /* Collect the routes from the Internal peer Advt list and form the
             * list of feasible and withdrawn routes. */
            pRtProfile = BGP4_INT_PEER_LIST_FIRST_ROUTE (u4Context);
            if (pRtProfile == NULL)
            {
                /* List is empty. No need of any processing to be done. */
                break;
            }
            /* if <afi, safi> differs, then send the routes collected for this
             * <afi, safi> and process the remaining routes in the next
             * iteration
             */
            BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                                   BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);

            if (((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_WITHDRAWN) ==
                 BGP4_RT_WITHDRAWN) ||
                ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGR_WITHDRAWN) ==
                 BGP4_RT_AGGR_WITHDRAWN))
            {
                /* Route is a withdrawn route. Add to withdrawn route list. */
                Bgp4DshAddRouteToList (&TsWithdrawnRtList, pRtProfile, 0);
            }
            else
            {
                /* Route is a feasible route. Add to feasible route list. */
                Bgp4DshAddRouteToList (&TsFeasibleRtList, pRtProfile,
                                       BGP4_TRUE);
            }

            /* Remove the route from the External peer advertise list. */
            BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_IN_INT_PEER_ADVT_LIST);
            Bgp4DshDelinkRouteFromList (BGP4_INT_PEERS_ADVT_LIST (u4Context),
                                        pRtProfile, BGP4_INT_PEER_LIST_INDEX);

            /* Increment the processed route count. */
            u4ProcUptCount++;
            if (u4ProcUptCount >= BGP4_MAX_INT_PEER_UPD2PROCESS)
            {
                break;
            }
        }
        /* Process the Routes in the withdrawn and feasible route list. */
        if ((TMO_SLL_Count (&TsWithdrawnRtList) > 0) ||
            (TMO_SLL_Count (&TsFeasibleRtList) > 0))
        {
            /* Route needs to be sent to all internal peers */
            TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeerentry,
                          tBgp4PeerEntry *)
            {
                if ((BGP4_GET_PEER_TYPE (u4Context, pPeerentry) ==
                     BGP4_EXTERNAL_PEER)
                    && (BGP4_CONFED_PEER_STATUS (pPeerentry) == BGP4_FALSE))
                {
                    /* Peer is not internal/confed peer. No need to advertise
                     * to this peer. */
                    continue;
                }
                if (BGP4_PEER_STATE (pPeerentry) != BGP4_ESTABLISHED_STATE)
                {
                    /* Connection not in established state. So no need to
                     * advertise to this peer. */
                    continue;
                }
                /* Route can be advertised to this peer. */
                Bgp4PeerSendRoutesToPeerTxQ (pPeerentry, &TsFeasibleRtList,
                                             &TsWithdrawnRtList, BGP4_TRUE);
            }
#ifdef L3VPN
            if (u4Context != BGP4_DFLT_VRFID)
            {
                TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (BGP4_DFLT_VRFID), pPeerentry,
                              tBgp4PeerEntry *)
                {
                    if (BGP4_PEER_STATE (pPeerentry) != BGP4_ESTABLISHED_STATE)
                    {
                        /* Connection not in established state. So no need to
                         * advertise to this peer. */
                        continue;
                    }
                    if (BGP4_VPN4_PEER_ROLE (pPeerentry) != BGP4_VPN4_PE_PEER)
                    {
                        continue;
                    }

                    /* Route can be advertised to this peer. */
                    Bgp4PeerSendRoutesToPeerTxQ (pPeerentry, &TsFeasibleRtList,
                                                 &TsWithdrawnRtList, BGP4_TRUE);
                }
            }
#endif
            BGP_SLL_DYN_Scan (&TsFeasibleRtList, pLinkNode, pTmpLinkNode,
                              tLinkNode *)
            {
                pRtProfile = pLinkNode->pRouteProfile;
                /* Reset the flag for all the routes in the list. */
                if (((BGP4_RT_GET_FLAGS (pRtProfile) &
                      BGP4_RT_IN_EXT_PEER_ADVT_LIST) == 0) &&
                    ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_IN_FIB_UPD_LIST)
                     == 0))
                {
                    BGP4_RT_RESET_FLAG (pRtProfile, (BGP4_RT_WITHDRAWN |
                                                     BGP4_RT_AGGR_WITHDRAWN |
                                                     BGP4_RT_ADVT_NOTTO_EXTERNAL
                                                     | BGP4_RT_REPLACEMENT));
#ifdef L3VPN
                    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                                           BGP4_RT_SAFI_INFO (pRtProfile),
                                           u4AsafiMask);
                    if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
                    {
                        Bgp4Vpnv4ResetFlagsForAllVrf (pRtProfile,
                                                      BGP4_RT_VRF_WITHDRAWN |
                                                      BGP4_RT_VRF_ADVT_NOTTO_EXTERNAL
                                                      |
                                                      BGP4_RT_VRF_REPLACEMENT);
                    }
#endif
                }
                BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_ADVT_NOTTO_INTERNAL);
                Bgp4DshRemoveNodeFromList (&TsFeasibleRtList, pLinkNode);
            }

            BGP_SLL_DYN_Scan (&TsWithdrawnRtList, pLinkNode, pTmpLinkNode,
                              tLinkNode *)
            {
                pRtProfile = pLinkNode->pRouteProfile;
                /* Reset the flag for all the routes in the list. */
                if (((BGP4_RT_GET_FLAGS (pRtProfile) &
                      BGP4_RT_IN_EXT_PEER_ADVT_LIST) == 0) &&
                    ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_IN_FIB_UPD_LIST)
                     == 0))
                {
                    BGP4_RT_RESET_FLAG (pRtProfile, (BGP4_RT_WITHDRAWN |
                                                     BGP4_RT_AGGR_WITHDRAWN |
                                                     BGP4_RT_ADVT_NOTTO_EXTERNAL
                                                     | BGP4_RT_REPLACEMENT));
#ifdef L3VPN
                    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                                           BGP4_RT_SAFI_INFO (pRtProfile),
                                           u4AsafiMask);
                    if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
                    {
                        Bgp4Vpnv4ResetFlagsForAllVrf (pRtProfile,
                                                      BGP4_RT_VRF_WITHDRAWN |
                                                      BGP4_RT_VRF_ADVT_NOTTO_EXTERNAL
                                                      |
                                                      BGP4_RT_VRF_REPLACEMENT);
                    }
#endif
                }
                BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_ADVT_NOTTO_INTERNAL);
                Bgp4DshRemoveNodeFromList (&TsWithdrawnRtList, pLinkNode);
            }
        }
        Bgp4DshReleaseList (&TsFeasibleRtList, 0);
        Bgp4DshReleaseList (&TsWithdrawnRtList, 0);

        if (u4ProcUptCount >= BGP4_MAX_INT_PEER_UPD2PROCESS)
        {
            /* Has reached the MAX Internal peer updates to be processed.
             * Check for the Tic Break event. If return value is BGP4_TURE,
             * then the environment may require the CPU. So BGP should
             * relinquish the CPU. */
            if (Bgp4IsCpuNeeded () == BGP4_TRUE)
            {
                /* External events requires CPU. Relinquish CPU. */
                return (BGP4_RELINQUISH);
            }
        }

        if (BGP4_INT_PEER_LIST_COUNT (u4Context) > 0)
        {
            return (BGP4_TRUE);
        }
        else
        {
            return (BGP4_FALSE);
        }
    }
    return BGP4_FALSE;
}

/****************************************************************************/
/* FUNCTION NAME : Bgp4ProcessPeerInit                                      */
/* DESCRIPTION   : This functions advertises the routes present in the local*/
/*                 RIB to all peers present in the peer init list. These    */
/*                 peers have established a BGP session with the router     */
/*                 newly and need to have xchange of the initial-route set. */
/*               :                                                          */
/* INPUTS        : None                                                     */
/* OUTPUTS       : None                                                     */
/* Return        : BGP4_TRUE  - if more peers remains to be initialised.    */
/*               : BGP4_FALSE - if no more peers needs to be initialised    */
/*                 BGP4_RELINQUISH - if external events requires CPU        */
/****************************************************************************/
INT4
Bgp4ProcessPeerInit (UINT4 u4Context)
{
    /* Variable Declarations */
    tPeerNode          *pPeerNode = NULL;
    tPeerNode          *pTmpPeerNode = NULL;
    tBgp4PeerEntry     *pPeerInfo = NULL;
    UINT4               u4MaxRtCnt = 0;
    INT4                i4Status = BGP4_TRUE;
    UINT4               u4AfiSafiMask = 0;
    UINT4               u4NegOrfCapMask = 0;

    if (TMO_SLL_Count (BGP4_PEER_INIT_LIST (u4Context)) == 0)
    {
        return BGP4_FALSE;
    }

/* while switch over the below routine checks all the needed condition*/
    if (Bgp4GRCheckRouteSelection (u4Context) == BGP4_FALSE)
    {
        return BGP4_FALSE;
    }

    /* In Restarting mode, u1AllowPeerInit flag should be set 
     * for FIB list needs to be processed completed before sending 
     * initial updates to the peer */
    if ((gBgpCxtNode[u4Context]->u1AllowPeerInit != BGP4_TRUE) &&
        (BGP4_RESTART_MODE (u4Context) == BGP4_RESTARTING_MODE))
    {
        return BGP4_FALSE;
    }

    u4MaxRtCnt =
        BGP4_MAX_PEERINIT_RTS /
        (TMO_SLL_Count (BGP4_PEER_INIT_LIST (u4Context)));

    BGP_SLL_DYN_Scan (BGP4_PEER_INIT_LIST (u4Context), pPeerNode, pTmpPeerNode,
                      tPeerNode *)
    {
        pPeerInfo = pPeerNode->pPeer;

        /* Check for the peer is negotiated with ORF capability and if so skip peer
         * process init routes till the RouteRefresh message received Inorder to apply
         * filters properly.*/
        if ((Bgp4GRCheckGRCapability (u4Context) != BGP4_TRUE)
            || (BGP4_PEER_SENT_EOR_MARKER (pPeerInfo) == BGP4_INVALID_STATUS))
        {
            BGP4_GET_AFISAFI_MASK (BGP4_PEER_REMOTE_ADDR_TYPE (pPeerInfo),
                                   (UINT2) BGP4_INET_SAFI_UNICAST,
                                   u4AfiSafiMask);
            if (Bgp4OrfGetOrfCapMask
                (u4AfiSafiMask, BGP4_CLI_ORF_MODE_RECEIVE,
                 &u4NegOrfCapMask) == BGP4_SUCCESS)
            {
                /* Check whether ORF receive Capability is negotiated with
                 * the peer for this AFI,SAFI or not. */
                if ((BGP4_PEER_NEG_CAP_MASK (pPeerInfo) & u4NegOrfCapMask) ==
                    u4NegOrfCapMask)
                {
                    if (BGP4_PEER_REMOTE_ADDR_TYPE (pPeerInfo) ==
                        BGP4_INET_AFI_IPV4)
                    {
                        if (BGP4_RTREF_MSG_RCVD_CTR
                            (pPeerInfo, BGP4_IPV4_UNI_INDEX) == 0)
                        {
                            continue;
                        }

                    }
#ifdef BGP4_IPV6_WANTED
                    else if (BGP4_PEER_REMOTE_ADDR_TYPE (pPeerInfo) ==
                             BGP4_INET_AFI_IPV6)
                    {
                        if (BGP4_RTREF_MSG_RCVD_CTR
                            (pPeerInfo, BGP4_IPV6_UNI_INDEX) == 0)
                        {
                            continue;;
                        }
                    }
#endif
                }
            }
            if (bgp4RibLock () != OSIX_SUCCESS)
            {
                return BGP4_TRUE;
            }

            /* Add init routes to peer Tx Q */
            i4Status = Bgp4PeerProcessInitRoutes (pPeerInfo, u4MaxRtCnt);

            bgp4RibUnlock ();

            if (i4Status == BGP4_INIT_COMPLETE)
            {
                /* Reset the peer init route info. */
                Bgp4InitNetAddressStruct (&
                                          (BGP4_PEER_INIT_NETADDR_INFO
                                           (pPeerInfo)), BGP4_INET_AFI_IPV4,
                                          BGP4_INET_SAFI_UNICAST);
                BGP4_PEER_INIT_PA_ENTRY (pPeerInfo) = NULL;
                BGP4_PEER_INIT_PA_HASHKEY (pPeerInfo) = 0;
                BGP4_PEER_INIT_AFI (pPeerInfo) = 0;
                BGP4_PEER_INIT_SAFI (pPeerInfo) = 0;
#ifdef L3VPN
                BGP4_PEER_INIT_PE_CONTEXT (pPeerInfo) = 0;
#endif

                if (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_UP)
                {
                    /* Need to advertise the aggregated routes to the peer. */
                    Bgp4AggrAdvtAggrRouteToPeer (pPeerInfo);

                    if (BGP4_PEER_DEF_ROUTE_ORIG_STATUS (pPeerInfo) ==
                        BGP4_DEF_ROUTE_ORIG_ENABLE)
                    {
                        /* Need to advertise default-route. */
                        Bgp4PeerAdvtDftRoute (pPeerInfo,
                                              BGP4_DEF_ROUTE_ORIG_ENABLE,
                                              BGP4_FALSE);
                    }

                    /* Start the Peer's MinAS-Origination Timer. */
                    Bgp4TmrhStartTimer (BGP4_MINASORIG_TIMER,
                                        (VOID *) pPeerInfo,
                                        BGP4_PEER_MIN_AS_ORIG_TIME (pPeerInfo));
                    /* Start the Peer's MinRt-Advertisement Timer. */
                    Bgp4TmrhStartTimer (BGP4_MINROUTEADV_TIMER,
                                        (VOID *) pPeerInfo,
                                        BGP4_PEER_MIN_ADV_TIME (pPeerInfo));
                    /* DORMANT status would make the Replacement route to
                     * get advertised after Min Rt Adv time after the 
                     * Peer initialization */
                    pPeerInfo->peerLocal.tRouteAdvTmr.u4Flag = BGP4_DORMANT;
                }

                /* Update the peer's current status. */
                BGP4_SET_PEER_CURRENT_STATE (pPeerInfo, BGP4_PEER_READY);
            }
        }
        else
        {
            if (BGP4_PEER_SENT_EOR_MARKER (pPeerInfo) == BGP4_FALSE)
            {
                i4Status = BGP4_INIT_COMPLETE;
            }
        }

        if (i4Status == BGP4_INIT_COMPLETE)
        {
            /* Send End of RIB Marker to the peer */
            if (Bgp4GRCheckGRCapability (u4Context) == BGP4_TRUE)
            {
                if (Bgp4GRCheckRouteSelection (u4Context) == BGP4_TRUE)
                {
                    Bgp4GRSendEndOfRIBMarker (pPeerInfo);
                    BGP4_SET_PEER_CURRENT_STATE (pPeerInfo, BGP4_PEER_READY);
                    /* Remove the peer from the peer init list. */
                    TMO_SLL_Delete (BGP4_PEER_INIT_LIST (u4Context),
                                    &pPeerNode->TSNext);
                    BGP_PEER_NODE_FREE (pPeerNode);
                }
                else
                {
                    /* Else Retain the node in peer init list 
                     * as EOR needs to be sent to it*/
                    BGP4_PEER_SENT_EOR_MARKER (pPeerInfo) = BGP4_FALSE;
                }
            }
            else
            {
                /* Remove the peer from the peer init list. */
                TMO_SLL_Delete (BGP4_PEER_INIT_LIST (u4Context),
                                &pPeerNode->TSNext);
                BGP_PEER_NODE_FREE (pPeerNode);
            }
        }
    }

    if (Bgp4IsCpuNeeded () == BGP4_TRUE)
    {
        /* External events requires CPU. Relinquish CPU. */
        return (BGP4_RELINQUISH);
    }

    if (TMO_SLL_Count (BGP4_PEER_INIT_LIST (u4Context)) > 0)
    {
        return (BGP4_TRUE);
    }
    else
    {
        return (BGP4_FALSE);
    }
}

/****************************************************************************/
/* FUNCTION NAME : Bgp4ProcessPeerDeInit                                    */
/* DESCRIPTION   : This routine removes all routes from the RIB for those   */
/*                 peers which are present in the peer de-init list. These  */
/*                 peers are undergoing de-initialization, and routes learnt*/
/*                 from them need to be advertised to other peers as        */
/*                 withdrawn. If the peer routes are also present in the FIB*/
/*                 they are removed from the forwarding table also.         */
/* INPUTS        : None                                                     */
/* OUTPUTS       : None                                                     */
/* Return        : BGP4_TRUE  - if more peers remains to be initialised.    */
/*               : BGP4_FALSE - if no more peers needs to be initialised    */
/*                 BGP4_RELINQUISH - if external events requires CPU        */
/****************************************************************************/
INT4
Bgp4ProcessPeerDeInit (UINT4 u4Context)
{
    /* Variable Declarations */
    tPeerNode          *pPeerNode = NULL;
    tPeerNode          *pTmpPeerNode = NULL;
    tBgp4PeerEntry     *pPeerInfo = NULL;
    UINT4               u4MaxRtCnt = 0;
    INT4                i4Status = BGP4_FAILURE;

    if (TMO_SLL_Count (BGP4_PEER_DEINIT_LIST (u4Context)) == 0)
    {
        return BGP4_FALSE;
    }
    u4MaxRtCnt =
        BGP4_MAX_PKTS2PROCESS /
        (TMO_SLL_Count (BGP4_PEER_DEINIT_LIST (u4Context)));

    BGP_SLL_DYN_Scan (BGP4_PEER_DEINIT_LIST (u4Context), pPeerNode,
                      pTmpPeerNode, tPeerNode *)
    {
        pPeerInfo = pPeerNode->pPeer;

        if (bgp4RibLock () != OSIX_SUCCESS)
        {
            return BGP4_TRUE;
        }

        /* Add route list to peer Tx Q */
        /* If the GR capable peer session is identified to be closed,
         * then do not Deinit the routes */
        if ((BGP4_GET_PEER_CURRENT_STATE (pPeerInfo) !=
             BGP4_PEER_RST_CLOSE_IDENTIFIED))
        {
            i4Status =
                Bgp4PeerProcessDeInitRoutes (pPeerInfo, u4MaxRtCnt, u4Context);
        }
        else
        {
            i4Status = BGP4_DEINIT_COMPLETE;
        }

        bgp4RibUnlock ();

        if (i4Status == BGP4_DEINIT_COMPLETE)
        {
            /* No more routes present in RIB for de-init */
            /* Reset the peer deinit route info. */
            Bgp4InitNetAddressStruct (&
                                      (BGP4_PEER_INIT_NETADDR_INFO (pPeerInfo)),
                                      BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);

            /* If the peer is not in restarting mode, then proceed the 
             * following */
            if (BGP4_PEER_RESTART_MODE (pPeerInfo) != BGP4_RESTARTING_MODE)
            {
                /* Release the lists,
                 * (input list, output list and the pending list)
                 * allocated for this peer. 
                 */
                if (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_UP)
                {
                    i4Status = Bgp4DshReleasePeerRtLists (pPeerInfo, BGP4_TRUE);
                    if (i4Status == BGP4_FALSE)
                    {
                        BGP4_SET_PEER_PEND_FLAG (pPeerInfo,
                                                 BGP4_DEINIT_PEERLISTS_INPROGRESS);
                    }
                    else
                    {
                        BGP4_RESET_PEER_PEND_FLAG (pPeerInfo,
                                                   BGP4_DEINIT_PEERLISTS_INPROGRESS);
                        /* Update the peer's current status. */
                        BGP4_SET_PEER_CURRENT_STATE (pPeerInfo,
                                                     BGP4_PEER_READY);
                        /* Remove the peer from the peer De-Init list. */
                        Bgp4DshRemovePeerFromList (BGP4_PEER_DEINIT_LIST
                                                   (u4Context), pPeerInfo);
                    }
                }
                else
                {
                    Bgp4DshReleasePeerRtLists (pPeerInfo, BGP4_FALSE);
                    BGP4_RESET_PEER_PEND_FLAG (pPeerInfo,
                                               BGP4_DEINIT_PEERLISTS_INPROGRESS);
                    /* Update the peer's current status. */
                    BGP4_SET_PEER_CURRENT_STATE (pPeerInfo, BGP4_PEER_READY);

                    /* Remove the peer from the peer De-Init list. */
                    Bgp4DshRemovePeerFromList (BGP4_PEER_DEINIT_LIST
                                               (u4Context), pPeerInfo);
                }
            }
            else
            {
                BGP4_RESET_PEER_CURRENT_STATE (pPeerInfo);
                BGP4_RESET_PEER_PEND_FLAG (pPeerInfo,
                                           BGP4_DEINIT_PEERLISTS_INPROGRESS);
                /* Remove the peer from the peer De-Init list. */
                Bgp4DshRemovePeerFromList (BGP4_PEER_DEINIT_LIST (u4Context),
                                           pPeerInfo);
            }
        }
    }

    if (Bgp4IsCpuNeeded () == BGP4_TRUE)
    {
        /* External events requires CPU. Relinquish CPU. */
        return (BGP4_RELINQUISH);
    }

    if (TMO_SLL_Count (BGP4_PEER_DEINIT_LIST (u4Context)) > 0)
    {
        return (BGP4_TRUE);
    }
    else
    {
        return (BGP4_FALSE);
    }
}

/****************************************************************************/
/* FUNCTION NAME : Bgp4AddFeasRouteToFIBUpdList                             */
/* DESCRIPTION   : This function adds the given new route to the FIB        */
/*               : Update list. This addition is controlled by the old      */
/*               : route's current advertisement status.                    */
/* INPUTS        : pointer to the new route (pNewRoute)                     */
/*               : pointer to the old route (pOldRoute) - if null then      */
/*               : pNewRoute will be treated as new feasible route else     */
/*               : pNewRoute will be treated as replacement to pOldRoute.   */
/* OUTPUTS       : None                                                     */
/* RETURNS       : BGP4_SUCCESS/BGP4_FAILURE                                */
/****************************************************************************/
INT4
Bgp4AddFeasRouteToFIBUpdList (tRouteProfile * pNewRoute,
                              tRouteProfile * pOldRoute)
{
    UINT4               u4Context = 0;
    /* Need to add the pNewRoute to the FIB update List. */

    u4Context = BGP4_RT_CXT_ID (pNewRoute);
    if (pOldRoute != NULL)
    {
        /* pNewRoute route is the replacement route to the pOldRoute 
         * Check the status of the old route and then process
         * the new route accordingly */
        if ((BGP4_RT_GET_FLAGS (pOldRoute) &
             BGP4_RT_IN_FIB_UPD_LIST) == BGP4_RT_IN_FIB_UPD_LIST)
        {
            /* Old best route was not updated in the FIB. So
             * the new best route needed to be added in the
             * FIB update list and old route needs to be removed
             * from the FIB Update list. */
            BGP4_RT_RESET_FLAG (pOldRoute, BGP4_RT_IN_FIB_UPD_LIST);
            Bgp4DshDelinkRouteFromList (BGP4_FIB_UPD_LIST (u4Context),
                                        pOldRoute, BGP4_FIB_UPD_LIST_INDEX);
            BGP4_RT_SET_FLAG (pNewRoute, BGP4_RT_NEWUPDATE_TOIP);
        }
        else if ((BGP4_RT_GET_FLAGS (pOldRoute) &
                  BGP4_RT_IN_EXT_PEER_ADVT_LIST) ==
                 BGP4_RT_IN_EXT_PEER_ADVT_LIST)
        {
            /* Route is present in the External peers advertisement list. This
             * means old route has been update in RIB but pending external peer
             * advertisement. So remove old route from the external peer advt
             * list. */
            BGP4_RT_RESET_FLAG (pOldRoute, (BGP4_RT_IN_EXT_PEER_ADVT_LIST |
                                            BGP4_RT_AGGR_WITHDRAWN));
            Bgp4DshDelinkRouteFromList (BGP4_EXT_PEERS_ADVT_LIST (u4Context),
                                        pOldRoute, BGP4_EXT_PEER_LIST_INDEX);

            /* Add the new route to FIB update list. */
        }
        else
        {
            /* Old route has been updated in FIB and advertised to external
             * peers. Add the new route to FIB update list. */
        }
    }
    else
    {
        /* New route to be updated in FIB */
        BGP4_RT_SET_FLAG (pNewRoute, BGP4_RT_NEWUPDATE_TOIP);
    }

    if ((BGP4_RT_GET_FLAGS (pNewRoute) & BGP4_RT_IN_FIB_UPD_LIST) !=
        BGP4_RT_IN_FIB_UPD_LIST)
    {
        /* Add the new route to the FIB update list. */
        Bgp4DshLinkRouteToList (BGP4_FIB_UPD_LIST (u4Context), pNewRoute, 0,
                                BGP4_FIB_UPD_LIST_INDEX);
        BGP4_RT_SET_FLAG (pNewRoute, BGP4_RT_IN_FIB_UPD_LIST);
    }

    return (BGP4_SUCCESS);
}

/****************************************************************************/
/* FUNCTION NAME : Bgp4AddWithdRouteToFIBUpdList                            */
/* DESCRIPTION   : This function adds the given withdrawn route to the FIB  */
/*               : Update list. This addition is controlled by the          */
/*               : route's current advertisement status.                    */
/* INPUTS        : pointer to the withdrawn route (pRtProfile)              */
/* OUTPUTS       : None                                                     */
/* RETURNS       : BGP4_SUCCESS/BGP4_FAILURE                                */
/****************************************************************************/
INT4
Bgp4AddWithdRouteToFIBUpdList (tRouteProfile * pRtProfile)
{
    /* Variable Declarations */
    tBgp4PeerEntry     *pPeerInfo = NULL;
    UINT4               u4PeerState = 0;
    UINT4               u4IsRouteToBeProc = BGP4_FALSE;
    UINT4               u4AsafiMask;

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
    if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
    {
        pPeerInfo = BGP4_RT_PEER_ENTRY (pRtProfile);
        u4PeerState = BGP4_GET_PEER_CURRENT_STATE (pPeerInfo);
    }

    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_IN_FIB_UPD_LIST)
        == BGP4_RT_IN_FIB_UPD_LIST)
    {
        /* Route is present in the FIB Update list and not yet advt
         * to external Peer. So just remove the route from FIB
         * Update List. No need to add the route to the peer deinit
         * list/FIB update list */
        BGP4_RT_RESET_FLAG (pRtProfile, (BGP4_RT_IN_FIB_UPD_LIST |
                                         BGP4_RT_NEWUPDATE_TOIP |
                                         BGP4_RT_REPLACEMENT |
                                         BGP4_RT_ADVT_NOTTO_EXTERNAL));
        Bgp4DshDelinkRouteFromList (BGP4_FIB_UPD_LIST
                                    (BGP4_RT_CXT_ID (pRtProfile)), pRtProfile,
                                    BGP4_FIB_UPD_LIST_INDEX);

#ifdef L3VPN
        if ((u4AsafiMask == CAP_MP_VPN4_UNICAST) &&
            (pPeerInfo != NULL) && (BGP4_VPN4_PEER_ROLE (pPeerInfo)
                                    == BGP4_VPN4_PE_PEER))
        {
            /* If the peer is not going de-init, then add this route into FIB */
            /* This happens when, the route has become best in one VRF and
             * is withdrawn in another VRF
             */
            if (u4PeerState != BGP4_PEER_DEINIT_INPROGRESS)
            {
                Bgp4DshLinkRouteToList (BGP4_FIB_UPD_LIST (BGP4_DFLT_VRFID),
                                        pRtProfile, 0, BGP4_FIB_UPD_LIST_INDEX);
                BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_IN_FIB_UPD_LIST);
            }
        }
#endif
        if (((BGP4_RT_GET_EXT_FLAGS (pRtProfile) & BGP4_RT_IN_RTM)
             == BGP4_RT_IN_RTM)
            || ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_NEWUPDATE_TOIP)
                != BGP4_RT_NEWUPDATE_TOIP))
        {
            u4IsRouteToBeProc = BGP4_TRUE;
        }
    }
    else if ((BGP4_RT_GET_FLAGS (pRtProfile) &
              BGP4_RT_IN_EXT_PEER_ADVT_LIST) == BGP4_RT_IN_EXT_PEER_ADVT_LIST)
    {
        /* Route is present in the External Peers update send list.
         * If the flag BGP4_RT_AGGR_WITHDRAWN is set then the route
         * should be advertised as withdrawn to external peers.
         * Else this route has been updated in FIB but pending
         * external peer update. Remove the route from this list
         * and add the route to the FIB update list/deinit list */
        BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_IN_EXT_PEER_ADVT_LIST);
        Bgp4DshDelinkRouteFromList (BGP4_EXT_PEERS_ADVT_LIST
                                    (BGP4_RT_CXT_ID (pRtProfile)), pRtProfile,
                                    BGP4_EXT_PEER_LIST_INDEX);
        if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGR_WITHDRAWN) ==
            BGP4_RT_AGGR_WITHDRAWN)
        {
            /* Just reset the BGP4_RT_ADVT_NOTTO_EXTERNAL flag so that
             * the route will be sent to external peers as withdrawn
             * routes. */
            BGP4_RT_RESET_FLAG (pRtProfile, (BGP4_RT_ADVT_NOTTO_EXTERNAL |
                                             BGP4_RT_AGGR_WITHDRAWN));
        }
        else
        {
            /* Set the BGP4_RT_ADVT_NOTTO_EXTERNAL flag, so route is
             * not advt to the external peers. */
            BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_ADVT_NOTTO_EXTERNAL);
        }
        u4IsRouteToBeProc = BGP4_TRUE;
    }
    else
    {
        /* Route was already installed in FIB and advertised to external
         * peer. So add this route to FIB update list. */
        u4IsRouteToBeProc = BGP4_TRUE;
    }

    if (u4IsRouteToBeProc == BGP4_TRUE)
    {
        if (u4PeerState == BGP4_PEER_DEINIT_INPROGRESS)
        {
            if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_IN_FIB_UPD_LIST) !=
                BGP4_RT_IN_FIB_UPD_LIST)
            {
                BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_IN_PEER_DEINIT_LIST);
                switch (u4AsafiMask)
                {
                    case CAP_MP_IPV4_UNICAST:
                        Bgp4DshAddRouteToList (BGP4_PEER_IPV4_DEINIT_RT_LIST
                                               (pPeerInfo), pRtProfile, 0);
                        break;
#ifdef BGP4_IPV6_WANTED
                    case CAP_MP_IPV6_UNICAST:
                        if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeerInfo) != NULL)
                        {
                            Bgp4DshAddRouteToList (BGP4_PEER_IPV6_DEINIT_RT_LIST
                                                   (pPeerInfo), pRtProfile, 0);
                        }
                        break;
#endif
#ifdef VPLSADS_WANTED
                    case CAP_MP_L2VPN_VPLS:
                        if (BGP4_PEER_VPLS_AFISAFI_INSTANCE (pPeerInfo) != NULL)
                        {
                            Bgp4DshAddRouteToList (BGP4_PEER_VPLS_DEINIT_RT_LIST
                                                   (pPeerInfo), pRtProfile, 0);
                        }
                        break;
#endif
#ifdef L3VPN
                    case CAP_MP_LABELLED_IPV4:
                        /* Carrying Label Information - RFC 3107 */
                        if (BGP4_RT_SAFI_INFO (pRtProfile) ==
                            BGP4_INET_SAFI_LABEL)
                        {
                            Bgp4DshAddRouteToList
                                (BGP4_PEER_IPV4_LBLD_DEINIT_RT_LIST (pPeerInfo),
                                 pRtProfile, 0);
                        }
                        break;
                    case CAP_MP_VPN4_UNICAST:
                        if (BGP4_RT_GET_FLAGS (pRtProfile) &
                            BGP4_RT_CONVERTED_TO_VPNV4)
                        {
                            /* The route is received from a CE peer, hence 
                             * add this route to IPV4 deinit list
                             */
                            Bgp4DshAddRouteToList (BGP4_PEER_IPV4_DEINIT_RT_LIST
                                                   (pPeerInfo), pRtProfile, 0);
                            break;
                        }
                        if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeerInfo) != NULL)
                        {
                            Bgp4DshAddRouteToList (BGP4_PEER_VPN4_DEINIT_RT_LIST
                                                   (pPeerInfo), pRtProfile, 0);
                        }
                        break;
#endif
#ifdef EVPN_WANTED
                    case CAP_MP_L2VPN_EVPN:
                        if (BGP4_PEER_EVPN_AFISAFI_INSTANCE (pPeerInfo) != NULL)
                        {
                            Bgp4DshAddRouteToList (BGP4_PEER_EVPN_DEINIT_RT_LIST
                                                   (pPeerInfo), pRtProfile, 0);
                        }
                        break;
#endif

                    default:
                        return BGP4_SUCCESS;
                }
            }
        }
        else
        {
            Bgp4DshLinkRouteToList (BGP4_FIB_UPD_LIST
                                    (BGP4_RT_CXT_ID (pRtProfile)), pRtProfile,
                                    0, BGP4_FIB_UPD_LIST_INDEX);
            BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_IN_FIB_UPD_LIST);
        }
    }
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* FUNCTION NAME : Bgp4AddFeasRouteToIntPeerAdvtList                        */
/* DESCRIPTION   : This function adds the given new route to the internal   */
/*               : peer's advertisement list. This addiion is controlled    */
/*               : by the old routes current advertisement status.          */
/* INPUTS        : pointer to the new route (pNewRoute)                     */
/*               : pointer to the old route (pOldRoute) - if null then      */
/*               : pNewRoute will be treated as new feasible route else     */
/*               : pNewRoute will be treated as replacement to pOldRoute.   */
/* OUTPUTS       : None                                                     */
/* RETURNS       : BGP4_SUCCESS/BGP4_FAILURE                                */
/* NOTE: This function should called only after updating FIB list/Ext Peer  */
/*       Advt list (i.e)after calling Bgp4AddFeasRouteToFIBUpdList/         */
/*       Bgp4AddFeasRouteToExtPeerAdvtList                                  */
/****************************************************************************/
INT4
Bgp4AddFeasRouteToIntPeerAdvtList (tRouteProfile * pNewRoute,
                                   tRouteProfile * pOldRoute)
{
    tBgp4PeerEntry     *pNewRtPeer = NULL;
    tBgp4PeerEntry     *pOldRtPeer = NULL;
    UINT4               u4IsOldRtBeAdvt = BGP4_FALSE;
    UINT4               u4Index;
    UINT4               u4Context;

    if ((Bgp4GetAfiSafiIndex (BGP4_RT_AFI_INFO (pNewRoute),
                              BGP4_RT_SAFI_INFO (pNewRoute), &u4Index))
        == BGP4_FAILURE)
    {
        /* Address family support is not there */
    }

    u4Context = BGP4_RT_CXT_ID (pNewRoute);
    if ((Bgp4GRCheckRestartMode (u4Context) != BGP4_RESTARTING_MODE)
        || (BGP4_RESTART_REASON (u4Context) == BGP4_GR_REASON_UPGRADE))
    {
        if (pOldRoute != NULL)
        {
            /* pNewRoute route is the replacement route to the pOldRoute.
             * If the pOldRoute was advt by an external peer or non-BGP route and
             * pNewRoute is received from an internal peer, then the pNewRoute will
             * not be advt to internal peers as feasible and the pOldRoute should
             * be advt as withdrawn to all the internal peers. The internal
             * peer which advt this route should ensure that these routes are
             * advt to all other internal peers. */
            if (BGP4_RT_PROTOCOL (pNewRoute) == BGP_ID)
            {
                pNewRtPeer = BGP4_RT_PEER_ENTRY (pNewRoute);
            }
            if (BGP4_RT_PROTOCOL (pOldRoute) == BGP_ID)
            {
                pOldRtPeer = BGP4_RT_PEER_ENTRY (pOldRoute);
            }
            if ((pNewRtPeer != NULL) &&
                (BGP4_GET_PEER_TYPE (BGP4_RT_CXT_ID (pNewRoute), pNewRtPeer) ==
                 BGP4_INTERNAL_PEER))
            {
                /* Route learnt from internal peer. */
                if ((BGP4_RT_PROTOCOL (pOldRoute) != BGP_ID) ||
                    (BGP4_GET_PEER_TYPE (BGP4_RT_CXT_ID (pNewRoute), pOldRtPeer)
                     == BGP4_EXTERNAL_PEER))
                {
                    /* Need to advt the pOldRoute as withdrawn. */
                    u4IsOldRtBeAdvt = BGP4_TRUE;
                }
            }
            if ((BGP4_RT_GET_FLAGS (pOldRoute) & BGP4_RT_IN_INT_PEER_ADVT_LIST)
                == BGP4_RT_IN_INT_PEER_ADVT_LIST)
            {
                /* Old route is present in the Internal peers advertisement list
                 * So remove the old route from the list */
                BGP4_RT_RESET_FLAG (pOldRoute, (BGP4_RT_IN_INT_PEER_ADVT_LIST |
                                                BGP4_RT_AGGR_WITHDRAWN));
                Bgp4DshDelinkRouteFromList (BGP4_INT_PEERS_ADVT_LIST
                                            (BGP4_RT_CXT_ID (pNewRoute)),
                                            pOldRoute,
                                            BGP4_INT_PEER_LIST_INDEX);

                if (((BGP4_RT_GET_FLAGS (pOldRoute) & BGP4_RT_IN_FIB_UPD_LIST)
                     != BGP4_RT_IN_FIB_UPD_LIST) &&
                    ((BGP4_RT_GET_FLAGS (pOldRoute) &
                      BGP4_RT_IN_EXT_PEER_ADVT_LIST) !=
                     BGP4_RT_IN_EXT_PEER_ADVT_LIST))
                {
                    /* Old Route is not in FIB list or Int/Ext Peer Advt List. */
                    BGP4_RT_RESET_FLAG (pOldRoute, BGP4_RT_WITHDRAWN);
                }
                /* Add the new route to the Internal peers update list. */
            }
            else
            {
                /* Old route has already been advertised to Internal peers.
                 * May be the Old route needs to be sent as withdrawn. */
                if (u4IsOldRtBeAdvt == BGP4_TRUE)
                {
                    if ((pOldRtPeer != NULL) &&
                        (BGP4_GET_PEER_CURRENT_STATE (pOldRtPeer) ==
                         BGP4_PEER_DEINIT_INPROGRESS))
                    {
                        /* If route not present in peer de-init list, then
                         * add it and set the withdrawn flag. */
                        BGP4_RT_SET_FLAG (pOldRoute, BGP4_RT_WITHDRAWN);
                        if ((BGP4_RT_GET_FLAGS (pOldRoute) &
                             BGP4_RT_IN_PEER_DEINIT_LIST) !=
                            BGP4_RT_IN_PEER_DEINIT_LIST)
                        {
                            BGP4_RT_SET_FLAG (pOldRoute, BGP4_RT_WITHDRAWN);
                            if (u4Index == BGP4_IPV4_UNI_INDEX)
                            {
                                Bgp4DshAddRouteToList
                                    (BGP4_PEER_IPV4_DEINIT_RT_LIST (pOldRtPeer),
                                     pOldRoute, 0);
                            }
#ifdef BGP4_IPV6_WANTED
                            else if ((u4Index == BGP4_IPV6_UNI_INDEX) &&
                                     (BGP4_PEER_IPV6_AFISAFI_INSTANCE
                                      (pOldRtPeer) != NULL))
                            {
                                Bgp4DshAddRouteToList
                                    (BGP4_PEER_IPV6_DEINIT_RT_LIST (pOldRtPeer),
                                     pOldRoute, 0);
                            }
#endif
#ifdef L3VPN
                            else if ((u4Index == BGP4_IPV4_LBLD_INDEX) &&
                                     (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE
                                      (pOldRtPeer) != NULL))
                            {
                                Bgp4DshAddRouteToList
                                    (BGP4_PEER_IPV4_LBLD_DEINIT_RT_LIST
                                     (pOldRtPeer), pOldRoute, 0);
                            }
                            else if (u4Index == BGP4_VPN4_UNI_INDEX)
                            {
                                if (BGP4_RT_GET_FLAGS (pOldRoute) &
                                    BGP4_RT_CONVERTED_TO_VPNV4)
                                {
                                    /* This route is received from a CE peer */
                                    Bgp4DshAddRouteToList
                                        (BGP4_PEER_IPV4_DEINIT_RT_LIST
                                         (pOldRtPeer), pOldRoute, 0);
                                }
                                else if (BGP4_PEER_VPN4_AFISAFI_INSTANCE
                                         (pOldRtPeer) != NULL)
                                {
                                    Bgp4DshAddRouteToList
                                        (BGP4_PEER_VPN4_DEINIT_RT_LIST
                                         (pOldRtPeer), pOldRoute, 0);
                                }
                            }
#endif
                            BGP4_RT_SET_FLAG (pOldRoute,
                                              (BGP4_RT_IN_PEER_DEINIT_LIST |
                                               BGP4_RT_ADVT_NOTTO_EXTERNAL));
                        }
                    }
                    else
                    {
                        /* Add the route to the internal peer advt list. */
                        Bgp4DshLinkRouteToList (BGP4_INT_PEERS_ADVT_LIST
                                                (BGP4_RT_CXT_ID (pNewRoute)),
                                                pOldRoute, 0,
                                                BGP4_INT_PEER_LIST_INDEX);
                        BGP4_RT_SET_FLAG (pOldRoute,
                                          (BGP4_RT_IN_INT_PEER_ADVT_LIST |
                                           BGP4_RT_WITHDRAWN));
                    }
                }
                else
                {
                    if (((BGP4_RT_GET_FLAGS (pOldRoute) &
                          BGP4_RT_IN_FIB_UPD_LIST) != BGP4_RT_IN_FIB_UPD_LIST)
                        &&
                        ((BGP4_RT_GET_FLAGS (pOldRoute) &
                          BGP4_RT_IN_EXT_PEER_ADVT_LIST) !=
                         BGP4_RT_IN_EXT_PEER_ADVT_LIST) &&
                        /* Peer not going thro de-init */
                        ((pOldRtPeer != NULL) &&
                         (BGP4_GET_PEER_CURRENT_STATE (pOldRtPeer) !=
                          BGP4_PEER_DEINIT_INPROGRESS)))
                    {
                        /* Old Route is not in FIB list or Int/Ext Peer Advt List */
                        BGP4_RT_RESET_FLAG (pOldRoute, BGP4_RT_WITHDRAWN);
                    }
                }

                /* New route has to be sent as replacement route. So add the
                 * new route to the Internal peers update list */
            }
        }

        if ((BGP4_RT_GET_FLAGS (pNewRoute) & BGP4_RT_ADVT_NOTTO_INTERNAL) ==
            BGP4_RT_ADVT_NOTTO_INTERNAL)
        {
            /* No need to advt the route to internal peers. */
            BGP4_RT_RESET_FLAG (pNewRoute, BGP4_RT_ADVT_NOTTO_INTERNAL);
        }
        else
        {
            if ((BGP4_RT_GET_FLAGS (pNewRoute) & BGP4_RT_IN_INT_PEER_ADVT_LIST)
                != BGP4_RT_IN_INT_PEER_ADVT_LIST)
            {
                /* Adding the new route to the Internal peers update list. */
                Bgp4DshLinkRouteToList (BGP4_INT_PEERS_ADVT_LIST
                                        (BGP4_RT_CXT_ID (pNewRoute)), pNewRoute,
                                        0, BGP4_INT_PEER_LIST_INDEX);
                BGP4_RT_SET_FLAG (pNewRoute, BGP4_RT_IN_INT_PEER_ADVT_LIST);
            }
        }
    }
    return (BGP4_SUCCESS);
}

/****************************************************************************/
/* FUNCTION NAME : Bgp4AddWithdRouteToIntPeerAdvtList                       */
/* DESCRIPTION   : This function adds the given withdrawn route to the      */
/*               : internal peer's advertisement list. This addition is     */
/*               : controlled by the routes current advertisement status.   */
/* INPUTS        : pointer to the withdrawn route (pRtProfile)              */
/* OUTPUTS       : None                                                     */
/* RETURNS       : BGP4_SUCCESS/BGP4_FAILURE                                */
/* NOTE: This function should called only after updating FIB list/Ext Peer  */
/*       Advt list (i.e)after calling Bgp4AddWithdRouteToFIBUpdList/        */
/*       Bgp4AddWithdRouteToExtPeerAdvtList                                 */
/****************************************************************************/
INT4
Bgp4AddWithdRouteToIntPeerAdvtList (tRouteProfile * pRtProfile)
{
    /* Variable Declarations */
    tBgp4PeerEntry     *pPeerInfo = NULL;
    UINT4               u4PeerState = 0;
    UINT4               u4Index;

    if ((Bgp4GetAfiSafiIndex (BGP4_RT_AFI_INFO (pRtProfile),
                              BGP4_RT_SAFI_INFO (pRtProfile), &u4Index))
        == BGP4_FAILURE)
    {
        /* Address family support is not there */
    }

    if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
    {
        pPeerInfo = BGP4_RT_PEER_ENTRY (pRtProfile);
        u4PeerState = BGP4_GET_PEER_CURRENT_STATE (pPeerInfo);
    }

    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_IN_INT_PEER_ADVT_LIST)
        == BGP4_RT_IN_INT_PEER_ADVT_LIST)
    {
        /* Route is present in the Internal Peers Advt list and not yet advt
         * to internal Peer. If BGP4_RT_AGGR_WITHDRAWN is set then the route
         * needs to be sent out as withdrawn. Else just remove the route
         * from internal peers Advertise List. */

        if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGR_WITHDRAWN) ==
            BGP4_RT_AGGR_WITHDRAWN)
        {
            /* The route was added to the internal peer update list
             * as part of the new aggregation policy. So this route
             * needs to be send out as withdrawn to the internal peers. */
            BGP4_RT_RESET_FLAG (pRtProfile, (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                             BGP4_RT_AGGR_WITHDRAWN));
        }
        else
        {
            BGP4_RT_RESET_FLAG (pRtProfile, (BGP4_RT_IN_INT_PEER_ADVT_LIST |
                                             BGP4_RT_ADVT_NOTTO_INTERNAL));
            Bgp4DshDelinkRouteFromList (BGP4_INT_PEERS_ADVT_LIST
                                        (BGP4_RT_CXT_ID (pRtProfile)),
                                        pRtProfile, BGP4_INT_PEER_LIST_INDEX);

            /* If peer is going through de-init, then this route need not be
             * advertised to all internal peers as withdrawn. So set the flag
             * BGP4_RT_ADVT_NOTTO_INTERNAL. */
            if ((u4PeerState == BGP4_PEER_DEINIT_INPROGRESS) &&
                ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_IN_PEER_DEINIT_LIST)
                 == BGP4_RT_IN_PEER_DEINIT_LIST))
            {
                BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_ADVT_NOTTO_INTERNAL);
            }
        }
    }
    else
    {
        /* Route has already been advertised to internal peers. So need to
         * send this route as withdrawn. */
        if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_ADVT_NOTTO_INTERNAL)
            == BGP4_RT_ADVT_NOTTO_INTERNAL)
        {
            /* No need to advertise this route if its aggregated. */
            if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGREGATED) !=
                BGP4_RT_AGGREGATED)
            {
                /* No need to advt this route to internal peers. */
                BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_ADVT_NOTTO_INTERNAL);
            }
        }
        else
        {
            if (u4PeerState == BGP4_PEER_DEINIT_INPROGRESS)
            {
                /* Peer is going through de-init. Check whether the route is
                 * already added to peer's deinit list. If added, then no
                 * operation required. If the route is not in deint list,
                 * this is because the route was not added to FIB & not advt to
                 * external peer earlier. So add this route to the peer deinit
                 * list and set the flag as BGP4_RT_ADVT_NOTTO_EXTERNAL */
                if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                     BGP4_RT_IN_PEER_DEINIT_LIST)
                    != BGP4_RT_IN_PEER_DEINIT_LIST)
                {
                    if (u4Index == BGP4_IPV4_UNI_INDEX)
                    {
                        Bgp4DshAddRouteToList
                            (BGP4_PEER_IPV4_DEINIT_RT_LIST (pPeerInfo),
                             pRtProfile, 0);
                    }
#ifdef BGP4_IPV6_WANTED
                    else if ((u4Index == BGP4_IPV6_UNI_INDEX) &&
                             (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeerInfo)
                              != NULL))
                    {
                        Bgp4DshAddRouteToList
                            (BGP4_PEER_IPV6_DEINIT_RT_LIST (pPeerInfo),
                             pRtProfile, 0);
                    }
#endif
#ifdef VPLSADS_WANTED
                    else if ((u4Index == BGP4_L2VPN_VPLS_INDEX) &&
                             (BGP4_PEER_VPLS_AFISAFI_INSTANCE (pPeerInfo)
                              != NULL))
                    {
                        Bgp4DshAddRouteToList
                            (BGP4_PEER_VPLS_DEINIT_RT_LIST (pPeerInfo),
                             pRtProfile, 0);
                    }
#endif
#ifdef L3VPN
                    /* Carrying Label Information - RFC 3107 */
                    else if ((u4Index == BGP4_IPV4_LBLD_INDEX) &&
                             (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeerInfo)
                              != NULL))
                    {
                        Bgp4DshAddRouteToList
                            (BGP4_PEER_IPV4_LBLD_DEINIT_RT_LIST (pPeerInfo),
                             pRtProfile, 0);
                    }
                    else if (u4Index == BGP4_VPN4_UNI_INDEX)
                    {
                        if (BGP4_RT_GET_FLAGS (pRtProfile) &
                            BGP4_RT_CONVERTED_TO_VPNV4)
                        {
                            /* This route is received from a CE peer */
                            Bgp4DshAddRouteToList
                                (BGP4_PEER_IPV4_DEINIT_RT_LIST (pPeerInfo),
                                 pRtProfile, 0);
                        }
                        else if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeerInfo)
                                 != NULL)
                        {
                            Bgp4DshAddRouteToList
                                (BGP4_PEER_VPN4_DEINIT_RT_LIST (pPeerInfo),
                                 pRtProfile, 0);
                        }
                    }
#endif
#ifdef EVPN_WANTED
                    else if ((u4Index == BGP4_L2VPN_EVPN_INDEX) &&
                             (BGP4_PEER_EVPN_AFISAFI_INSTANCE (pPeerInfo)
                              != NULL))
                    {
                        Bgp4DshAddRouteToList
                            (BGP4_PEER_EVPN_DEINIT_RT_LIST (pPeerInfo),
                             pRtProfile, 0);
                    }
#endif

                    BGP4_RT_SET_FLAG (pRtProfile,
                                      (BGP4_RT_IN_PEER_DEINIT_LIST |
                                       BGP4_RT_ADVT_NOTTO_EXTERNAL |
                                       BGP4_RT_ONLY_ADVT));
                }
            }
            else
            {
                Bgp4DshLinkRouteToList (BGP4_INT_PEERS_ADVT_LIST
                                        (BGP4_RT_CXT_ID (pRtProfile)),
                                        pRtProfile, 0,
                                        BGP4_INT_PEER_LIST_INDEX);
                BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_IN_INT_PEER_ADVT_LIST);
            }
        }
    }
    return (BGP4_SUCCESS);
}

/****************************************************************************/
/* FUNCTION NAME : Bgp4AddFeasRouteToExtPeerAdvtList                        */
/* DESCRIPTION   : This function adds the given new route to the external   */
/*               : peer's advertisement list. This addiion is controlled    */
/*               : by the old routes current advertisement status.          */
/* INPUTS        : pointer to the new route (pNewRoute)                     */
/*               : pointer to the old route (pOldRoute) - if null then      */
/*               : pNewRoute will be treated as new feasible route else     */
/*               : pNewRoute will be treated as replacement to pOldRoute.   */
/* OUTPUTS       : None                                                     */
/* RETURNS       : BGP4_SUCCESS/BGP4_FAILURE                                */
/****************************************************************************/
INT4
Bgp4AddFeasRouteToExtPeerAdvtList (tRouteProfile * pNewRoute,
                                   tRouteProfile * pOldRoute)
{
    UINT4               u4Context;

    u4Context = BGP4_RT_CXT_ID (pNewRoute);
    if ((Bgp4GRCheckRestartMode (u4Context) != BGP4_RESTARTING_MODE)
        || (BGP4_RESTART_REASON (u4Context) == BGP4_GR_REASON_UPGRADE))
    {
        if (pOldRoute != NULL)
        {
            /* pNewRoute route is the replacement route to the pOldRoute 
             * Check the status of the old route and then process
             * the new route accordingly */
            if ((BGP4_RT_GET_FLAGS (pOldRoute) & BGP4_RT_IN_EXT_PEER_ADVT_LIST)
                == BGP4_RT_IN_EXT_PEER_ADVT_LIST)
            {
                /* Old route is present in the External peers advertisement list
                 * So remove the old route from the list */
                BGP4_RT_RESET_FLAG (pOldRoute, (BGP4_RT_IN_EXT_PEER_ADVT_LIST |
                                                BGP4_RT_AGGR_WITHDRAWN));
                Bgp4DshDelinkRouteFromList (BGP4_EXT_PEERS_ADVT_LIST
                                            (BGP4_RT_CXT_ID (pNewRoute)),
                                            pOldRoute,
                                            BGP4_EXT_PEER_LIST_INDEX);

                /* Add the new route to the External peers update list. */
            }
            else
            {
                /* Old route has already been advertised to External peers.
                 * New route has to be sent as replacement route. So add the
                 * new route to the External peers update list */
            }
        }

        if ((BGP4_RT_GET_FLAGS (pNewRoute) & BGP4_RT_IN_EXT_PEER_ADVT_LIST)
            != BGP4_RT_IN_EXT_PEER_ADVT_LIST)
        {
            /* Adding the new route to the External peers update list. */
            Bgp4DshLinkRouteToList (BGP4_EXT_PEERS_ADVT_LIST
                                    (BGP4_RT_CXT_ID (pNewRoute)), pNewRoute, 0,
                                    BGP4_EXT_PEER_LIST_INDEX);
            BGP4_RT_SET_FLAG (pNewRoute, BGP4_RT_IN_EXT_PEER_ADVT_LIST);
        }
    }
    return (BGP4_SUCCESS);
}

/****************************************************************************/
/* FUNCTION NAME : Bgp4AddWithdRouteToExtPeerAdvtList                       */
/* DESCRIPTION   : This function adds the given withdrawn route to the      */
/*               : external peer's advertisement list. This addition is     */
/*               : controlled by the routes current advertisement status.   */
/* INPUTS        : pointer to the withdrawn route (pRtProfile)              */
/* OUTPUTS       : None                                                     */
/* RETURNS       : BGP4_SUCCESS/BGP4_FAILURE                                */
/****************************************************************************/
INT4
Bgp4AddWithdRouteToExtPeerAdvtList (tRouteProfile * pRtProfile)
{
    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_IN_EXT_PEER_ADVT_LIST)
        == BGP4_RT_IN_EXT_PEER_ADVT_LIST)
    {
        /* Route is present in the External Peers Advt list and not yet advt
         * to external Peer. If BGP4_RT_AGGR_WITHDRAWN is set then the route
         * needs to be sent out as withdrawn. Else just remove the route
         * from external peers Advertise List. */

        if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGR_WITHDRAWN) ==
            BGP4_RT_AGGR_WITHDRAWN)
        {
            /* The route was added to the external peer update list
             * as part of the new aggregation policy. So this route
             * needs to be send out as withdrawn to the external peers. */
            BGP4_RT_RESET_FLAG (pRtProfile, (BGP4_RT_ADVT_NOTTO_EXTERNAL |
                                             BGP4_RT_AGGR_WITHDRAWN));
        }
        else
        {
            BGP4_RT_RESET_FLAG (pRtProfile, (BGP4_RT_IN_EXT_PEER_ADVT_LIST |
                                             BGP4_RT_ADVT_NOTTO_EXTERNAL));
            Bgp4DshDelinkRouteFromList (BGP4_EXT_PEERS_ADVT_LIST
                                        (BGP4_RT_CXT_ID (pRtProfile)),
                                        pRtProfile, BGP4_EXT_PEER_LIST_INDEX);
        }
    }
    else
    {
        /* Route has already been advertised to external peers. So need to
         * send this route as withdrawn. */
        if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_ADVT_NOTTO_EXTERNAL)
            == BGP4_RT_ADVT_NOTTO_EXTERNAL)
        {
            /* No need to advt this route to external peers. */
            BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_ADVT_NOTTO_EXTERNAL);
        }
        else
        {
            Bgp4DshLinkRouteToList (BGP4_EXT_PEERS_ADVT_LIST
                                    (BGP4_RT_CXT_ID (pRtProfile)), pRtProfile,
                                    0, BGP4_EXT_PEER_LIST_INDEX);
            BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_IN_EXT_PEER_ADVT_LIST);
        }
    }
    return (BGP4_SUCCESS);
}

/****************************************************************************/
/* FUNCTION NAME : Bgp4ProcessPendJobs                                      */
/* DESCRIPTION   : This function processes all pending tasks on a priority  */
/*                 basis.                                                   */
/* INPUTS        : None                                                     */
/* OUTPUTS       : None                                                     */
/* RETURNS       : BGP4_SUCCESS/BGP4_FAILURE                                */
/****************************************************************************/
INT4
Bgp4ProcessPendJobs (UINT4 u4Context)
{
    /* Variable Declarations */
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tBgp4PeerEntry     *pTmpPeerInfo = NULL;
    tRouteProfile      *pRtProfile = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4RetVal = BGP4_FAILURE;

    if (BGP4_GLOBAL_STATE (u4Context) == BGP4_GLOBAL_ADMIN_DOWN_INPROGRESS)
    {
        /* Check whether LOCAL RIB and FIB list are free. If yes,
         * then update the Global state as ready. */
        if (BGP4_FIB_LIST_COUNT (u4Context) == 0)
        {
            Bgp4GetRibNode (u4Context, CAP_MP_IPV4_UNICAST, &pRibNode);
            if (pRibNode != NULL)
            {
                i4RetVal = Bgp4RibhGetFirstEntry (CAP_MP_IPV4_UNICAST,
                                                  &pRtProfile, &pRibNode, TRUE);
            }
#ifdef BGP4_IPV6_WANTED
            if (i4RetVal == BGP4_FAILURE)
            {
                Bgp4GetRibNode (u4Context, CAP_MP_IPV6_UNICAST, &pRibNode);
                if (pRibNode != NULL)
                {
                    i4RetVal = Bgp4RibhGetFirstEntry (CAP_MP_IPV6_UNICAST,
                                                      &pRtProfile, &pRibNode,
                                                      TRUE);
                }
            }
#endif
#ifdef L3VPN
            if (i4RetVal == BGP4_FAILURE)
            {
                i4RetVal = Bgp4RibhGetFirstEntry (CAP_MP_LABELLED_IPV4,
                                                  &pRtProfile, &pRibNode, TRUE);
            }
            if (i4RetVal == BGP4_FAILURE)
            {
                pRibNode = BGP4_VPN4_RIB_TREE (u4Context);
                i4RetVal = Bgp4RibhGetFirstEntry (CAP_MP_VPN4_UNICAST,
                                                  &pRtProfile, &pRibNode, TRUE);
            }
#endif
            if (i4RetVal == BGP4_FAILURE)
            {
                /* Delete the Peer which are pending for deletion. */
                BGP_SLL_DYN_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeerInfo,
                                  pTmpPeerInfo, tBgp4PeerEntry *)
                {
                    if ((BGP4_GET_PEER_PEND_FLAG (pPeerInfo) &
                         BGP4_PEER_DELETE_PENDING) == BGP4_PEER_DELETE_PENDING)
                    {
                        BGP4_RESET_PEER_PEND_FLAG (pPeerInfo,
                                                   BGP4_PEER_DELETE_PENDING);
                        Bgp4ClearPeer (pPeerInfo);
                    }
                }
                /* Clear the Global Data Structure. */
                Bgp4GlobalMemoryRelease (u4Context);
                if ((BGP4_GET_GLOBAL_FLAG (u4Context) &
                     BGP4_GLOBAL_CLEAR_PENDING) == BGP4_GLOBAL_CLEAR_PENDING)
                {
                    Bgp4ClearAndReInit (u4Context);
                    BGP4_RESET_GLOBAL_FLAG (u4Context,
                                            BGP4_GLOBAL_CLEAR_PENDING);
                }
                else
                {
                    /* Reset the Default route origination status. */
                    BGP4_DEF_ROUTE_ORIG_STATUS (u4Context) =
                        BGP4_DEF_ROUTE_ORIG_NEW_STATUS (u4Context);
                }
                BGP4_GLOBAL_STATE (u4Context) = BGP4_GLOBAL_READY;
            }
        }
    }

    /* Check Global Pending Flags */
    if ((BGP4_GET_GLOBAL_FLAG (u4Context) & BGP4_GLOBAL_ADMIN_UP_PENDING) ==
        BGP4_GLOBAL_ADMIN_UP_PENDING)
    {
        if (BGP4_GLOBAL_STATE (u4Context) == BGP4_GLOBAL_READY)
        {
            BGP4_RESET_GLOBAL_FLAG (u4Context, BGP4_GLOBAL_ADMIN_UP_PENDING);
            Bgp4GlobalAdminStatusHandler (u4Context, BGP4_ADMIN_UP);
        }
    }

    if (BGP4_DEF_ROUTE_ORIG_STATUS (u4Context) !=
        BGP4_DEF_ROUTE_ORIG_NEW_STATUS (u4Context))
    {
        /* The default route origination status has been changed. 
         * Now need to advertised/withdraw the default route if present
         * as the new policy. */
        Bgp4ProcessDflRouteOrigPolicyChange (u4Context,
                                             BGP4_DEF_ROUTE_ORIG_NEW_STATUS
                                             (u4Context),
                                             BGP4_DEF_ROUTE_ORIG_STATUS
                                             (u4Context));

        /* Update the Default route origination status */
        BGP4_DEF_ROUTE_ORIG_STATUS (u4Context) =
            BGP4_DEF_ROUTE_ORIG_NEW_STATUS (u4Context);
    }

    BGP_SLL_DYN_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeerInfo,
                      pTmpPeerInfo, tBgp4PeerEntry *)
    {
        /* Check if the peer has any routes to be handled for MIN-AS
         * Origination Timer Expiry. */
        if ((pPeerInfo->peerLocal.tOrigTmr.u4Flag == BGP4_INACTIVE) &&
            (BGP4_PEER_STATE (pPeerInfo) == BGP4_ESTABLISHED_STATE) &&
            (BGP4_GET_PEER_CURRENT_STATE (pPeerInfo) !=
             BGP4_PEER_INIT_INPROGRESS))
        {
            /* This peer has got some routes left for processing. */
            Bgp4AhProcessMinASTimerExpiryEvent (pPeerInfo);
        }

        if ((BGP4_GET_PEER_CURRENT_STATE (pPeerInfo) & BGP4_PEER_READY) !=
            BGP4_PEER_READY)
        {
            continue;
        }

        /* Check for peer delete pend-task */
        if ((BGP4_GLOBAL_STATE (u4Context) != BGP4_GLOBAL_ADMIN_DOWN_INPROGRESS)
            && ((BGP4_GET_PEER_PEND_FLAG (pPeerInfo) & BGP4_PEER_DELETE_PENDING)
                == BGP4_PEER_DELETE_PENDING))
        {
            BGP4_RESET_PEER_PEND_FLAG (pPeerInfo, BGP4_PEER_DELETE_PENDING);

            /* Peer De-Init is already over. Free the peer resources.  */
            Bgp4ClearPeer (pPeerInfo);
            continue;
        }

        /* Check if the reflector client event is pending or not. */
        if ((BGP4_GET_PEER_PEND_FLAG (pPeerInfo) & BGP4_PEER_RFL_CLIENT_PEND)
            == BGP4_PEER_RFL_CLIENT_PEND)
        {
            /* Update the peer rfl client status */
            if (BGP4_PEER_RFL_CLIENT (pPeerInfo) == CLIENT)
            {
                BGP4_PEER_RFL_CLIENT (pPeerInfo) = NON_CLIENT;
            }
            else
            {
                BGP4_PEER_RFL_CLIENT (pPeerInfo) = CLIENT;
            }
            BGP4_RESET_PEER_PEND_FLAG (pPeerInfo, BGP4_PEER_RFL_CLIENT_PEND);
        }

        /* Check for peer init pend-task */
        if ((BGP4_GET_PEER_PEND_FLAG (pPeerInfo) & BGP4_PEER_INIT_PENDING) ==
            BGP4_PEER_INIT_PENDING)
        {
            BGP4_RESET_PEER_PEND_FLAG (pPeerInfo, BGP4_PEER_INIT_PENDING);

            Bgp4InitNetAddressStruct (&
                                      (BGP4_PEER_INIT_NETADDR_INFO (pPeerInfo)),
                                      BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);

            /* Peer is pending for initialisation.Enable the peer connection. */
            Bgp4EnablePeer (pPeerInfo);
            continue;
        }

#ifdef RFD_WANTED
        if ((BGP4_GET_PEER_PEND_FLAG (pPeerInfo) & BGP4_PEER_REUSE_PEND) ==
            BGP4_PEER_REUSE_PEND)
        {
            /* Peer is Pending for Reuse of Suppressed routes. */
            RfdSupPeerReusePendHandler (pPeerInfo);
            continue;
        }
#endif
        /* Check for peer soft-config inbound pending */
        if (((BGP4_GET_PEER_PEND_FLAG (pPeerInfo) &
              BGP4_PEER_SOFTCONFIG_INBOUND_PEND) ==
             BGP4_PEER_SOFTCONFIG_INBOUND_PEND) ||
            (TMO_SLL_Count (BGP4_PEER_SOFTCONFIG_IN_PEND_LIST (pPeerInfo)) > 0))
        {
            Bgp4RtRefPendHandler (pPeerInfo);
        }

        /* Check for peer soft-config outbound pending */
        if (((BGP4_GET_PEER_PEND_FLAG (pPeerInfo) &
              BGP4_PEER_SOFTCONFIG_OUTBOUND_PEND) ==
             BGP4_PEER_SOFTCONFIG_OUTBOUND_PEND) ||
            (TMO_SLL_Count (BGP4_PEER_SOFTCONFIG_OUT_PEND_LIST (pPeerInfo))
             > 0))
        {
            Bgp4SoftOutPendHandler (pPeerInfo);
        }
    }
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function    :  Bgp4CheckNextHopResolution                                */
/* Description :  This function check whether the given next hop entry is   */
/*                resolvable or not and updates the entry.                  */
/* Input       :  pIgpMetricEntry - Pointer to the next hop metric entry    */
/* Output      :  pIgpMetricEntry - Pointer to the updated next hop metric  */
/*                                  entry.                                  */
/* Returns     :  BGP4_SUCCESS - if the next hop is resolvable.             */
/*                BGP4_FAILURE - if the next hop is not resolvable.         */
/****************************************************************************/
INT4
Bgp4CheckNextHopResolution (tBgp4IgpMetricEntry * pIgpMetricEntry)
{
    tAddrPrefix         NextHop;
    tAddrPrefix         ImmediateNextHop;
    tAddrPrefix         InvPrefix;
    INT4                i4LkupStatus = BGP4_FAILURE;
    INT4                i4Metric = BGP4_INVALID_NH_METRIC;
    UINT4               u4IfIndex = BGP4_INVALID_IFINDX;
    UINT2               u2PrefixLen = 0;
    UINT1               u1PrevRouteResolve = BGP4_FALSE;
    UINT1               u1IsRouteResolve = BGP4_FALSE;
    UINT1               u1IsNextHopResolve = BGP4_TRUE;
    UINT1               u1IsNHMetricChanged = BGP4_NEXTHOP_METRIC_STABLE;
    UINT4               u4VrfId;
#ifdef L3VPN
    INT4                i4RetVal;
#endif

    /* Look up in FIB to check whether the next hop is resolvable or not. */
    Bgp4CopyAddrPrefixStruct (&(NextHop),
                              BGP4_NH_METRIC_NEXTHOP_INFO (pIgpMetricEntry));
    Bgp4InitAddrPrefixStruct (&(ImmediateNextHop),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO (NextHop));
    Bgp4InitAddrPrefixStruct (&(InvPrefix),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO (NextHop));
    u2PrefixLen = Bgp4GetMaxPrefixLength (NextHop);
    u4VrfId = BGP4_NH_METRIC_VRFID (pIgpMetricEntry);

    u1PrevRouteResolve = BGP4_NH_METRIC_RESOLVE_STATUS (pIgpMetricEntry);
#ifdef L3VPN
    if (BGP4_NH_METRIC_LSP_CHECK (pIgpMetricEntry) == BGP4_TRUE)
    {
        u1PrevRouteResolve = BGP4_GET_NH_RESOLVE_STATUS (u1PrevRouteResolve,
                                                         BGP4_NH_METRIC_LSP_STATUS
                                                         (pIgpMetricEntry));
    }
#endif

    i4LkupStatus = BgpIpRtLookup (u4VrfId, &NextHop, u2PrefixLen,
                                  &ImmediateNextHop, &i4Metric, &u4IfIndex,
                                  BGP4_ALL_APPIDS);
    if (i4LkupStatus == BGP4_FAILURE)
    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tRoute Look UP FAILURE. NextHop %s NOT RESOLVABLE.\n",
                       Bgp4PrintIpAddr (NextHop.au1Address, NextHop.u2Afi));
        u1IsRouteResolve = BGP4_FALSE;
        u1IsNextHopResolve = BGP4_FALSE;
    }
    else
    {
        u1IsRouteResolve = BGP4_TRUE;
#ifdef L3VPN
        if ((BGP4_NH_METRIC_LSP_CHECK (pIgpMetricEntry) == BGP4_TRUE) &&
            (BGP4_NH_METRIC_LSPID (pIgpMetricEntry) == BGP4_INVALID_IGP_LABEL))
        {
            i4RetVal = Bgp4UpdateLspStatusforNextHop (pIgpMetricEntry);
            if (i4RetVal == BGP4_FAILURE)
            {
                BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                               "\tLSP Not Present. NextHop %s NOT RESOLVABLE.\n",
                               Bgp4PrintIpAddr (NextHop.au1Address,
                                                NextHop.u2Afi));
                u1IsRouteResolve = BGP4_FALSE;
            }
        }

#endif
        /* If the entry obtained by lookup specifies an attached subnet
         * and not a next-hop address, then the bgp next hop address
         * should be used as the immediate next-hop address. Else if the
         * entry specifies the next-hop address then it should be used
         * as the next hop address. */
        if (((PrefixMatch (ImmediateNextHop, InvPrefix)) == BGP4_TRUE) &&
            (u4IfIndex != BGP4_INVALID_IFINDX))
        {
            /* Look up has resolved an attached subnet. */
            Bgp4CopyAddrPrefixStruct (&(ImmediateNextHop), NextHop);
        }
        else
        {
            /* The obtained entry will be used as immediate next hop. */
        }
    }

    /* Update the next hop metric table with this info. */
    if (u1IsRouteResolve == BGP4_TRUE)
    {
        if (u1PrevRouteResolve == BGP4_FALSE)
        {
            /* Previously not resolved. Now resolvable. */
            if (BGP4_NH_METRIC_RT_CNT (pIgpMetricEntry) > 0)
            {
                /* Some routes are pending for process. So set the
                 * action to process. */
                u1IsNHMetricChanged = BGP4_NEXTHOP_METRIC_CHANGED;
            }
        }
        else
        {
            if (BGP4_NH_METRIC_RT_CNT (pIgpMetricEntry) > 0)
            {
                /* Some routes are present in LOCAL RIB with this next 
                 * hop. Check whether they needs to be processed again */
                if ((PrefixMatch
                     (BGP4_NH_METRIC_IMME_NEXTHOP_INFO (pIgpMetricEntry),
                      ImmediateNextHop)) != BGP4_TRUE)
                {
                    /* Change in the first hop address. */
                    u1IsNHMetricChanged = BGP4_NEXTHOP_METRIC_CHANGED;
                }
                else if (BGP4_NH_METRIC_METRIC (pIgpMetricEntry) != i4Metric)
                {
                    /* Change in the metric */
                    u1IsNHMetricChanged = BGP4_NEXTHOP_METRIC_CHANGED;
                }
            }
        }
    }
    else
    {
        /* Route is not resolvable. */
        if (u1PrevRouteResolve == BGP4_TRUE)
        {
            /* Previously resolved. Now not resolvable. */
            if (BGP4_NH_METRIC_RT_CNT (pIgpMetricEntry) > 0)
            {
                /* Some routes are present in the LOCAL RIB. 
                 * Need to remove them. */
                u1IsNHMetricChanged = BGP4_NEXTHOP_METRIC_CHANGED;
            }
        }
    }
    /* Update the Next hop IGP table with this entry. */
    BGP4_NH_METRIC_ACTION (pIgpMetricEntry) = u1IsNHMetricChanged;
    BGP4_NH_METRIC_RESOLVE_STATUS (pIgpMetricEntry) = u1IsNextHopResolve;
    Bgp4CopyAddrPrefixStruct (&
                              (BGP4_NH_METRIC_IMME_NEXTHOP_INFO
                               (pIgpMetricEntry)), ImmediateNextHop);
    BGP4_NH_METRIC_METRIC (pIgpMetricEntry) = i4Metric;
    BGP4_NH_METRIC_IFINDEX (pIgpMetricEntry) = u4IfIndex;

    return (u1IsRouteResolve);
}

#ifdef L3VPN
/****************************************************************************/
/* Function Name : Bgp4UpdateVrfChgInitInfo                                 */
/* Description   : This routine is called whenever a route is removed from  */
/*                 the LOCAL RIB. This function checks whether the removed  */
/*                 route and the route stored in the vrf init address       */
/*                 are identical. If yes, then the next route               */
/*                 from the LOCAL RIB is obtained and stored in the vrf init*/
/*                 address. If next route is not present,                   */
/*                 then the vrf init route is set as 0.                     */
/* Input(s)      : Pointer to the current route profile (pRtProfile)        */
/*                 Pointer to the treenode in which the route is present    */
/*                          - pTreeNode                                     */
/*               : i4Flag - Flag passed to RIB for semaphore lock decision  */
/* Output(s)     : None.                                                    */
/* Return(s)     : BGP4_SUCCESS.                                            */
/****************************************************************************/
INT4
Bgp4UpdateVrfChgInitInfo (tRouteProfile * pRtProfile)
{
    tRouteProfile      *pNextRoute = NULL;
    tRtInstallVrfInfo  *pInstVrfInfo = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4Found = BGP4_FAILURE;
    UINT1               u1Lookup = BGP4_FALSE;
    UINT4               u4VrfId;

    BGP4_VPN4_GET_PROCESS_VRFID (pRtProfile, u4VrfId);

    if (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) == 0)
    {
        return (BGP4_SUCCESS);
    }

    pRibNode = BGP4_VPN4_RIB_TREE (u4VrfId);

    TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile),
                  pInstVrfInfo, tRtInstallVrfInfo *)
    {
        u4VrfId = BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pInstVrfInfo);
        if (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) == BGP4_L3VPN_VRF_DELETE)
        {
            continue;
        }
        if (NetAddrMatch (BGP4_VPN4_VRF_SPEC_INIT_NET_ADDR (u4VrfId),
                          BGP4_RT_NET_ADDRESS_INFO (pRtProfile)) == BGP4_TRUE)
        {
            if (u1Lookup == BGP4_FALSE)
            {
                /* To prevent calling getnext each time. This getnext must
                 * be called only once.
                 */
                i4Found =
                    Bgp4RibhGetNextEntry (pRtProfile, u4VrfId,
                                          &pNextRoute, &pRibNode, FALSE);
                u1Lookup = BGP4_TRUE;
            }
            if (i4Found == BGP4_FAILURE)
            {
                Bgp4InitNetAddressStruct (&
                                          (BGP4_VPN4_VRF_SPEC_INIT_NET_ADDR
                                           (u4VrfId)), BGP4_INET_AFI_IPV4,
                                          BGP4_INET_SAFI_VPNV4_UNICAST);
                continue;
            }
            /* Update the VRF-Init Info */
            Bgp4CopyNetAddressStruct (&
                                      (BGP4_VPN4_VRF_SPEC_INIT_NET_ADDR
                                       (u4VrfId)),
                                      BGP4_RT_NET_ADDRESS_INFO (pNextRoute));
        }
    }
    return (BGP4_SUCCESS);
}
#endif

/*****************************************************************************/
/* Function Name : Bgp4UpdateNHChgInitRoute                                  */
/* Description   : This routine is called whenever a route is removed from   */
/*                 the LOCAL RIB. This function checks whether the removed   */
/*                 route and the route stored in the next hop change init    */
/*                 route variable are identical. If yes, then the next route */
/*                 from the LOCAL RIB is obtained and stored in the Next hop */
/*                 change init route variable. If next route is not present, */
/*                 then the next hop change init route is set as 0.          */
/* Input(s)      : Pointer to the current route profile (pRtProfile)         */
/*                 Pointer to the treenode in which the route is present     */
/*                          - pTreeNode                                      */
/*               : i4Flag - Flag passed to RIB for semaphore lock decision   */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS.                                             */
/*****************************************************************************/
INT4
Bgp4UpdateNHChgInitRoute (tRouteProfile * pRtProfile, UINT4 u4VrfId,
                          VOID *pTreeNode, INT4 i4Flag)
{
    tRouteProfile      *pNextRoute = NULL;
    tBgp4IgpMetricEntry *pIgpMetricEntry = NULL;
    tBgp4IgpMetricEntry *pNextIgpMetricEntry = NULL;
    INT4                i4RetVal = BGP4_FALSE;

    UNUSED_PARAM (pTreeNode);
    UNUSED_PARAM (i4Flag);

    if ((BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID) &&
        ((NetAddrMatch (BGP4_RT_NET_ADDRESS_INFO (pRtProfile),
                        BGP4_NH_CHG_INIT_ROUTE_NETADDR_INFO (BGP4_RT_CXT_ID
                                                             (pRtProfile)))) ==
         BGP4_TRUE)
        &&
        ((PrefixMatch
          (BGP4_NH_CHG_INIT_NEXTHOP_PEER_INFO (BGP4_RT_CXT_ID (pRtProfile)),
           BGP4_PEER_REMOTE_ADDR_INFO (BGP4_RT_PEER_ENTRY (pRtProfile)))) ==
         BGP4_TRUE) && (BGP4_NH_CHG_INIT_VRFID (u4VrfId) == u4VrfId))
    {
        /* Get the next route from the next-hop list. */
        pNextRoute = BGP4_RT_NEXTHOP_LINK_NEXT (pRtProfile);

        if (pNextRoute != NULL)
        {
            /* More route present in the next-hop route list. */
            Bgp4CopyNetAddressStruct (&
                                      (BGP4_NH_CHG_INIT_ROUTE_NETADDR_INFO
                                       (BGP4_RT_CXT_ID (pRtProfile))),
                                      BGP4_RT_NET_ADDRESS_INFO (pNextRoute));
            Bgp4CopyAddrPrefixStruct (&
                                      (BGP4_NH_CHG_INIT_NEXTHOP_PEER_INFO
                                       (BGP4_RT_CXT_ID (pRtProfile))),
                                      BGP4_PEER_REMOTE_ADDR_INFO
                                      (BGP4_RT_PEER_ENTRY ((pNextRoute))));
        }
        else
        {
            /* No more route is present in the list. Try and get
             * the next entry in IgpMetricTable */
            Bgp4IgpMetricTblGetEntry (u4VrfId,
                                      BGP4_INFO_NEXTHOP_INFO (BGP4_RT_BGP_INFO
                                                              (pRtProfile)),
                                      &pIgpMetricEntry);
            if (NULL != pIgpMetricEntry)
            {
                i4RetVal =
                    Bgp4IgpMetricTblGetNextEntry (pIgpMetricEntry,
                                                  &pNextIgpMetricEntry);
            }
            if (i4RetVal == BGP4_FALSE)
            {
                /* No more entry is present in the next-hop metric table
                 * to process.*/
                Bgp4InitAddrPrefixStruct (&
                                          (BGP4_NH_CHG_INIT_NEXTHOP_INFO
                                           (BGP4_RT_CXT_ID (pRtProfile))),
                                          BGP4_INET_AFI_IPV4);
                Bgp4InitNetAddressStruct (&
                                          (BGP4_NH_CHG_INIT_ROUTE_NETADDR_INFO
                                           (BGP4_RT_CXT_ID (pRtProfile))),
                                          BGP4_INET_AFI_IPV4,
                                          BGP4_INET_SAFI_UNICAST);
                Bgp4InitAddrPrefixStruct (&
                                          (BGP4_NH_CHG_INIT_NEXTHOP_PEER_INFO
                                           (BGP4_RT_CXT_ID (pRtProfile))),
                                          BGP4_INET_AFI_IPV4);
                BGP4_NH_CHG_INTERVAL (BGP4_RT_CXT_ID (pRtProfile)) = 0;
            }
            else
            {
                /* Just set the next-hop value, so that the next-hop
                 * resolution function start processing this new entry. */
                Bgp4InitNetAddressStruct
                    (&
                     (BGP4_NH_CHG_INIT_ROUTE_NETADDR_INFO
                      (BGP4_RT_CXT_ID (pRtProfile))), BGP4_INET_AFI_IPV4,
                     BGP4_INET_SAFI_UNICAST);
                Bgp4InitAddrPrefixStruct (&
                                          (BGP4_NH_CHG_INIT_NEXTHOP_PEER_INFO
                                           (BGP4_RT_CXT_ID (pRtProfile))),
                                          BGP4_INET_AFI_IPV4);
                Bgp4CopyAddrPrefixStruct (&
                                          (BGP4_NH_CHG_INIT_NEXTHOP_INFO
                                           (BGP4_RT_CXT_ID (pRtProfile))),
                                          BGP4_NH_METRIC_NEXTHOP_INFO
                                          (pNextIgpMetricEntry));
                BGP4_NH_CHG_INIT_VRFID (BGP4_RT_CXT_ID (pRtProfile)) =
                    BGP4_NH_METRIC_VRFID (pNextIgpMetricEntry);
            }
        }
    }
    return (BGP4_SUCCESS);
}

/****************************************************************************/
/* Function    :  Bgp4ProcessNextHopForResolution                           */
/* Description :  This function takes the next hop address as the input and */
/*                checks a matching entry presents in the next hop metrics  */
/*                table or not. If not present create one and does a route  */
/*                look up for this next hop. Then the output parameter is   */
/*                updated with the status of this entry.                    */
/* Input       :  u4Nexthop - Nexthop Address received in the update message*/
/* Output      :  pu4ImmNextHop - Immediate next hop address                */
/*                pi4Metric - Next hop metric value for the route.          */
/*                pu4IfIndex - Interface index via which the next hop is    */
/*                             reachable.                                   */
/* Returns     :  BGP4_TRUE - if the next hop is resolvable.                */
/*                BGP4_FALSE - if the next hop is not resolvable.           */
/*                BGP4_ENTRYCREATE_FAILURE - if nexthop entry could not be  */
/*                created in the igp metric table for the nexthop           */
/****************************************************************************/
INT4
Bgp4ProcessNextHopForResolution (tRouteProfile * pRtProfile,
                                 tAddrPrefix * pImmNextHop,
                                 INT4 *pi4Metric, UINT4 *pu4IfIndex)
{
    tBgp4IgpMetricEntry *pIgpMetricEntry = NULL;
#ifdef L3VPN
    tBgp4PeerEntry     *pPeer = NULL;
#endif
    tAddrPrefix         ImmediateNextHop;
    tAddrPrefix         InvPrefix;
    tBgp4Info          *pBgp4Info = NULL;
    INT4                i4LkupStatus = BGP4_FAILURE;
    INT4                i4Metric = BGP4_INVALID_NH_METRIC;
    UINT4               u4IfIndex = BGP4_INVALID_IFINDX;
    UINT4               u4VrfId;
#ifdef L3VPN
    UINT4               u4AsafiMask;
#endif
    UINT1               u1IsEntryPresent = BGP4_FALSE;
    UINT1               u1IsRouteResolve = BGP4_FALSE;
    UINT2               u2PrefixLen;
#ifdef L3VPN
    UINT1               u1LspResolution = BGP4_FALSE;
#endif

    u4VrfId = BGP4_RT_CXT_ID (pRtProfile);
#ifdef L3VPN
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
    if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
    {
        if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
        {
            pPeer = BGP4_RT_PEER_ENTRY (pRtProfile);
            if (BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_CE_PEER)
            {
                /* The route is received from CE peer. Hence check the nexthop
                 * reachability in the VRF that is associated with CE
                 */
                u4VrfId = BGP4_PEER_CXT_ID (pPeer);
            }
            else if (BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) == 0)
            {
                u1LspResolution = BGP4_TRUE;
            }
        }
    }

    if (u4AsafiMask == CAP_MP_LABELLED_IPV4)
    {
        u1LspResolution = BGP4_TRUE;
    }
#endif

    pBgp4Info = BGP4_RT_BGP_INFO (pRtProfile);
    if (pBgp4Info == NULL)
    {
        *pi4Metric = BGP4_INVALID_NH_METRIC;
        *pu4IfIndex = BGP4_INVALID_IFINDX;
        return BGP4_FALSE;
    }
    Bgp4InitAddrPrefixStruct (&(InvPrefix),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO
                              (BGP4_INFO_NEXTHOP_INFO (pBgp4Info)));

    u1IsEntryPresent =
        Bgp4IgpMetricTblGetEntry (u4VrfId, BGP4_INFO_NEXTHOP_INFO (pBgp4Info),
                                  &pIgpMetricEntry);
    if (u1IsEntryPresent == BGP4_FALSE)
    {
        /* Next hop entry does not exist. Create a new entry. */
        pIgpMetricEntry =
            Bgp4IgpMetricTblAddEntry (u4VrfId,
                                      BGP4_INFO_NEXTHOP_INFO (pBgp4Info));
        if (pIgpMetricEntry == NULL)
        {
            /* Entry creation fails. */
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC | BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                           "\tCreation of new nexthop entry failed for the route %s."
                           " with nexthop %s.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP (pBgp4Info),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_INFO_NEXTHOP_INFO
                                             (pBgp4Info))));

            Bgp4InitAddrPrefixStruct (pImmNextHop,
                                      BGP4_AFI_IN_ADDR_PREFIX_INFO
                                      (BGP4_INFO_NEXTHOP_INFO (pBgp4Info)));
            *pi4Metric = BGP4_INVALID_NH_METRIC;
            *pu4IfIndex = BGP4_INVALID_IFINDX;
            return BGP4_ENTRYCREATE_FAILURE;
        }
        /* Look up IN FIB to check whether the next hop is resolvable or 
         * not */
        u2PrefixLen =
            Bgp4GetMaxPrefixLength (BGP4_INFO_NEXTHOP_INFO (pBgp4Info));
        i4LkupStatus =
            BgpIpRtLookup (u4VrfId, &BGP4_INFO_NEXTHOP_INFO (pBgp4Info),
                           u2PrefixLen, &ImmediateNextHop, &i4Metric,
                           &u4IfIndex, BGP4_ALL_APPIDS);
        if (i4LkupStatus == BGP4_FAILURE)
        {
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC | BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                           "\tRoute Lookup in FIB table for the nexthop %s failed "
                           " for the route %s. Nexthop is not resolvable\n",
                           Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP (pBgp4Info),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_INFO_NEXTHOP_INFO
                                             (pBgp4Info))),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            BGP4_NH_METRIC_RESOLVE_STATUS (pIgpMetricEntry) = BGP4_FALSE;
        }
        else
        {
            /* If the entry obtained by lookup specifies an attached 
             * subnet  and not a next-hop address, then the bgp 
             * next hop address  should be used as the immediate 
             * next-hop address. Else if the entry specifies the 
             * next-hop address then it should be used
             * as the next hop address. */
            if (((PrefixMatch (ImmediateNextHop, InvPrefix)) == BGP4_TRUE)
                && (u4IfIndex != BGP4_INVALID_IFINDX))
            {
                /* Look up has resolved an attached subnet. */
                Bgp4CopyAddrPrefixStruct (&
                                          (BGP4_NH_METRIC_IMME_NEXTHOP_INFO
                                           (pIgpMetricEntry)),
                                          BGP4_INFO_NEXTHOP_INFO (pBgp4Info));
            }
            else
            {
                /* The obtained entry will be used as immediate 
                 * next hop.  * */
                Bgp4CopyAddrPrefixStruct (&
                                          (BGP4_NH_METRIC_IMME_NEXTHOP_INFO
                                           (pIgpMetricEntry)),
                                          ImmediateNextHop);
            }
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tNexthop resolved for the route %s. Immediate nexthop "
                           " for the route is %s\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpAddr (BGP4_NH_METRIC_IMME_NEXTHOP
                                            (pIgpMetricEntry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_NH_METRIC_IMME_NEXTHOP_INFO
                                             (pIgpMetricEntry))));
            BGP4_NH_METRIC_RESOLVE_STATUS (pIgpMetricEntry) = BGP4_TRUE;
            BGP4_NH_METRIC_METRIC (pIgpMetricEntry) = i4Metric;
            BGP4_NH_METRIC_IFINDEX (pIgpMetricEntry) = u4IfIndex;
        }
    }

#ifdef L3VPN
    if ((u1LspResolution == BGP4_TRUE) &&
        (BGP4_NH_METRIC_LSPID (pIgpMetricEntry) == BGP4_INVALID_IGP_LABEL) &&
        (BGP4_NH_METRIC_RESOLVE_STATUS (pIgpMetricEntry) == BGP4_TRUE))
    {
        Bgp4UpdateLspStatusforNextHop (pIgpMetricEntry);
    }
#endif

    /* Update the output parameters with this entry. */
    u1IsRouteResolve = BGP4_NH_METRIC_RESOLVE_STATUS (pIgpMetricEntry);
#ifdef L3VPN
    if (u1LspResolution == BGP4_TRUE)
    {
        if (BGP4_NH_METRIC_LSP_CHECK (pIgpMetricEntry) == BGP4_FALSE)
        {
            BGP4_NH_METRIC_LSP_CHECK (pIgpMetricEntry) = BGP4_TRUE;
        }
        u1IsRouteResolve = BGP4_GET_NH_RESOLVE_STATUS (u1IsRouteResolve,
                                                       BGP4_NH_METRIC_LSP_STATUS
                                                       (pIgpMetricEntry));
    }
    BGP4_RT_IGP_LSP_ID (pRtProfile) = BGP4_NH_METRIC_LSPID (pIgpMetricEntry);
#endif
    Bgp4CopyAddrPrefixStruct (pImmNextHop,
                              BGP4_NH_METRIC_IMME_NEXTHOP_INFO
                              (pIgpMetricEntry));
    *pi4Metric = BGP4_NH_METRIC_METRIC (pIgpMetricEntry);
    *pu4IfIndex = BGP4_NH_METRIC_IFINDEX (pIgpMetricEntry);

    return (u1IsRouteResolve);
}

/****************************************************************************/
/* Function    :  Bgp4ProcessNextHopResolution                              */
/* Description :  This function checks whether the next hop entry in the    */
/*                next hop metric table has gone through any change and     */
/*                pending action. If yes, then the routes having that next  */
/*                hop are processed accordingly. This routine is called     */
/*                periodically, and it ensures that route whose immediate   */
/*                next hop or igp metrics have changed are processed again  */
/*                for route selection.                                      */
/* Input       :  None.                                                     */
/* Output      :  None.                                                     */
/* Return      :  BGP4_TRUE  - if more routes available for processing.     */
/*             :  BGP4_FALSE - if no more routes available for processing.  */
/*                BGP4_RELINQUISH - if external events requires CPU         */
/****************************************************************************/
INT4
Bgp4ProcessNextHopResolution (UINT4 u4Context)
{
    tBgp4IgpMetricEntry *pIgpMetricEntry = NULL;
    tBgp4IgpMetricEntry *pTempIgpMetricEntry = NULL;
    tRouteProfile       RtProfile;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pAddRtProfile = NULL;
    tRouteProfile      *pNextRtProfile = NULL;
    VOID               *pRibNode = NULL;
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         InvPrefix;
    UINT4               u4StartHashKey = 0;
    UINT4               u4HashKey = 0;
    UINT4               u4RtCnt = 0;
    UINT1               u1PrevState = 0;
    INT4                i4RetVal = 0;
    UINT4               u4VrfId = u4Context;
#ifdef L3VPN
    UINT1               u1NoChange = BGP4_FALSE;
    UINT1               u1PrevLspState = 0;
#endif

    if ((BGP4_NH_CHG_INTERVAL (u4Context) <
         BGP4_NH_CHG_PRCS_INTERVAL (u4Context))
        || (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_DOWN))
    {
        return (BGP4_FALSE);
    }

    Bgp4InitAddrPrefixStruct (&(InvPrefix),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO
                              (BGP4_NH_CHG_INIT_NEXTHOP_INFO (u4Context)));

    /* Need to process the entry in next-hop metric and to update the
     * current status of the entry. */
    if ((PrefixMatch (BGP4_NH_CHG_INIT_NEXTHOP_INFO (u4Context), InvPrefix)) !=
        BGP4_TRUE)
    {
        /* Need to start processing from the pending next hop. */
        BGP4_GET_NH_HASH_KEY (BGP4_NH_CHG_INIT_VRFID (u4Context),
                              BGP4_NH_CHG_INIT_NEXTHOP_INFO (u4Context),
                              u4StartHashKey);
    }
    else
    {
        u4StartHashKey = 0;
    }
    BGP4_HASH_Scan_Table (BGP4_NEXTHOP_METRIC_TBL (u4Context), u4HashKey,
                          u4StartHashKey)
    {
        if (((PrefixMatch (BGP4_NH_CHG_INIT_NEXTHOP_INFO (u4Context),
                           InvPrefix)) != BGP4_TRUE) &&
            (u4HashKey != u4StartHashKey))
        {
            /* This means we have started processing from where we left
             * last time. But that next hop is not found. So start
             * processing the next hop in the next hash index.
             */
            Bgp4InitAddrPrefixStruct (&
                                      (BGP4_NH_CHG_INIT_NEXTHOP_INFO
                                       (u4Context)), BGP4_INET_AFI_IPV4);
            Bgp4InitNetAddressStruct (&
                                      (BGP4_NH_CHG_INIT_ROUTE_NETADDR_INFO
                                       (u4Context)), BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);
            Bgp4InitAddrPrefixStruct (&
                                      (BGP4_NH_CHG_INIT_NEXTHOP_PEER_INFO
                                       (u4Context)), BGP4_INET_AFI_IPV4);
            BGP4_NH_CHG_INIT_VRFID (u4Context) = u4Context;
        }

        BGP4_HASH_DYN_Scan_Bucket (BGP4_NEXTHOP_METRIC_TBL (u4Context),
                                   u4HashKey, pIgpMetricEntry,
                                   pTempIgpMetricEntry, tBgp4IgpMetricEntry *)
        {
#ifdef L3VPN
            u1NoChange = BGP4_FALSE;
#endif
            if ((PrefixMatch (BGP4_NH_CHG_INIT_NEXTHOP_INFO (u4Context),
                              InvPrefix)) != BGP4_TRUE)
            {
                if (((PrefixMatch
                      (BGP4_NH_METRIC_NEXTHOP_INFO (pIgpMetricEntry),
                       BGP4_NH_CHG_INIT_NEXTHOP_INFO (u4Context))) != BGP4_TRUE)
                    || (BGP4_NH_CHG_INIT_VRFID (u4Context) !=
                        BGP4_NH_METRIC_VRFID (pIgpMetricEntry)))
                {
                    continue;
                }
                else
                {
                    /* Matching entry is found. Just clear the Nexthop init
                     * info and continue processing the routes */
                    Bgp4InitAddrPrefixStruct (&
                                              (BGP4_NH_CHG_INIT_NEXTHOP_INFO
                                               (u4Context)),
                                              BGP4_INET_AFI_IPV4);
                }
            }

            if (BGP4_NH_METRIC_RT_CNT (pIgpMetricEntry) == 0)
            {
                /* No route is available with this next hop. So just
                 * remove this entry from the next hop metric table. */
                TMO_HASH_Delete_Node (BGP4_NEXTHOP_METRIC_TBL (u4Context),
                                      &pIgpMetricEntry->NextIgpMetricEntry,
                                      u4HashKey);
                Bgp4ReleaseIgpMetricEntry (pIgpMetricEntry);
                Bgp4InitNetAddressStruct (&
                                          (BGP4_NH_CHG_INIT_ROUTE_NETADDR_INFO
                                           (u4Context)), BGP4_INET_AFI_IPV4,
                                          BGP4_INET_SAFI_UNICAST);
                Bgp4InitAddrPrefixStruct (&
                                          (BGP4_NH_CHG_INIT_NEXTHOP_PEER_INFO
                                           (u4Context)), BGP4_INET_AFI_IPV4);
                continue;
            }

            if (u4RtCnt >= BGP4_MAX_NH_MET_RTS2PRCS)
            {
                /* Has reached the MAX entries to be processed.
                 * Check for the Tic Break event. If return value is
                 * BGP4_TURE, then the environment may require the CPU.
                 * So BGP should relinquish the CPU. Before that store
                 * the next hop so that processing begins at this next-hop
                 * next time. */
                Bgp4CopyAddrPrefixStruct (&
                                          (BGP4_NH_CHG_INIT_NEXTHOP_INFO
                                           (u4Context)),
                                          BGP4_NH_METRIC_NEXTHOP_INFO
                                          (pIgpMetricEntry));
                BGP4_NH_CHG_INIT_VRFID (u4Context) =
                    BGP4_NH_METRIC_VRFID (pIgpMetricEntry);
                Bgp4InitNetAddressStruct (&
                                          (BGP4_NH_CHG_INIT_ROUTE_NETADDR_INFO
                                           (u4Context)), BGP4_INET_AFI_IPV4,
                                          BGP4_INET_SAFI_UNICAST);
                Bgp4InitAddrPrefixStruct (&
                                          (BGP4_NH_CHG_INIT_NEXTHOP_PEER_INFO
                                           (u4Context)), BGP4_INET_AFI_IPV4);
                if (Bgp4IsCpuNeeded () == BGP4_TRUE)
                {
                    /* External events requires CPU. Relinquish CPU. */
                    return (BGP4_RELINQUISH);
                }
                else
                {
                    /* Maximum entries processed. Can return. */
                    return (BGP4_TRUE);
                }
            }

            if ((PrefixMatch (BGP4_NH_CHG_INIT_ROUTE_PREFIX_INFO (u4Context),
                              InvPrefix)) != BGP4_TRUE)
            {
                /* Need to process the route whose next-hop has undergone
                 * some change during the lookup done in the previous
                 * call. */
                pRtProfile = NULL;
                pRibNode = NULL;
                Bgp4CopyNetAddressStruct (&(RtProfile.NetAddress),
                                          BGP4_NH_CHG_INIT_ROUTE_NETADDR_INFO
                                          (u4Context));
                RtProfile.u1Ref = 0;
                RtProfile.u1Protocol = 0;
                RtProfile.p_RPNext = 0;
                RtProfile.u4Flags = 0;
                RtProfile.pRtInfo = 0;
                u4VrfId = BGP4_NH_CHG_INIT_VRFID (u4Context);
                i4RetVal = Bgp4RibhLookupRtEntry (&RtProfile,
                                                  u4VrfId,
                                                  BGP4_TREE_FIND_EXACT,
                                                  &pRtProfile, &pRibNode);
                /* Reset the Next-HOP Init route information. */
                Bgp4InitNetAddressStruct (&
                                          (BGP4_NH_CHG_INIT_ROUTE_NETADDR_INFO
                                           (u4Context)), BGP4_INET_AFI_IPV4,
                                          BGP4_INET_SAFI_UNICAST);
                pPeer =
                    Bgp4SnmphGetPeerEntry (u4Context,
                                           BGP4_NH_CHG_INIT_NEXTHOP_PEER_INFO
                                           (u4Context));
                Bgp4InitAddrPrefixStruct (&
                                          (BGP4_NH_CHG_INIT_NEXTHOP_PEER_INFO
                                           (u4Context)), BGP4_INET_AFI_IPV4);

                if ((pPeer == NULL) || (i4RetVal == BGP4_FAILURE))
                {
                    /* Matching Route is not present. Start
                     * processing the next IgpMetricEntry. */
                    continue;
                }

                /* Get the route with the matching Peer. */
                pRtProfile = Bgp4DshGetPeerRtFromProfileList (pRtProfile,
                                                              pPeer);
                if (pRtProfile == NULL)
                {
                    /* Matching Route is not present in the RIB. Start
                     * processing the next IgpMetricEntry. */
                    continue;
                }
            }
            else
            {
                /* Check the Resolution status for this next-hop. If
                 * there is any change in the next-hop status then
                 * process the routes accordingly. */
                pRtProfile = NULL;
                pRibNode = NULL;
                u1PrevState = BGP4_NH_METRIC_RESOLVE_STATUS (pIgpMetricEntry);
#ifdef L3VPN
                u1PrevLspState = BGP4_NH_METRIC_LSP_STATUS (pIgpMetricEntry);
#endif
                Bgp4CheckNextHopResolution (pIgpMetricEntry);

                if ((u1PrevState ==
                     BGP4_NH_METRIC_RESOLVE_STATUS (pIgpMetricEntry)) &&
                    (BGP4_NH_METRIC_ACTION (pIgpMetricEntry) ==
                     BGP4_NEXTHOP_METRIC_STABLE))
                {
#ifdef L3VPN
                    u1NoChange = BGP4_TRUE;
#else
                    continue;
#endif
                }
                pRtProfile = BGP4_DLL_FIRST_ROUTE (BGP4_NH_METRIC_ROUTES
                                                   (pIgpMetricEntry));
            }

            /* Current status of the NEXT-HOP has changed. Need to process
             * all the routes with this NEXT-HOP. */
            while (pRtProfile)
            {
#ifdef L3VPN
                if ((u1NoChange == BGP4_TRUE) &&
                    (!((BGP4_RT_AFI_INFO (pRtProfile) == BGP4_INET_AFI_IPV4) &&
                       (BGP4_RT_SAFI_INFO (pRtProfile) ==
                        BGP4_INET_SAFI_VPNV4_UNICAST))))
                {
                    /* No change in the Next-Hop status. */
                    pRtProfile = BGP4_RT_NEXTHOP_LINK_NEXT (pRtProfile);
                    continue;
                }
                if ((u1NoChange == BGP4_TRUE) &&
                    ((BGP4_RT_AFI_INFO (pRtProfile) == BGP4_INET_AFI_IPV4) &&
                     (BGP4_RT_SAFI_INFO (pRtProfile) ==
                      BGP4_INET_SAFI_VPNV4_UNICAST)))
                {
                    pNextRtProfile = BGP4_RT_NEXTHOP_LINK_NEXT (pRtProfile);
                    /* No change in the Next-Hop status.Route is a VPN route */
                    if ((Bgp4Vpnv4IsRouteCanBeProcessed (pRtProfile))
                        == BGP4_FAILURE)
                    {
                        BGP4_RT_REF_COUNT (pRtProfile)++;
                        /* route can be withdrawn */
                        Bgp4Vpnv4DelRouteFromVPNRIB (pRtProfile);
                        Bgp4DshReleaseRtInfo (pRtProfile);
                    }
                    pRtProfile = pNextRtProfile;
                    continue;
                }
                if ((BGP4_RT_AFI_INFO (pRtProfile) == BGP4_INET_AFI_IPV4) &&
                    (BGP4_RT_SAFI_INFO (pRtProfile) ==
                     BGP4_INET_SAFI_VPNV4_UNICAST) &&
                    (u1PrevLspState ==
                     BGP4_NH_METRIC_LSP_STATUS (pIgpMetricEntry)))
                {
                    pRtProfile = pNextRtProfile;
                    continue;
                }
#endif
                /* Increment the route count. Because this route is to
                 * be processed. */
                u4RtCnt++;
                BGP4_RT_REF_COUNT (pRtProfile)++;
                pNextRtProfile = BGP4_RT_NEXTHOP_LINK_NEXT (pRtProfile);

                /* There is a change in the next-hop status for this route.
                 * This situation is quiet tricky. If the route's next-hop
                 * is unresolvable earlier, just withdraw the route from RIB
                 * and allow the same route profile to be added back to the
                 * Peer's RCVD_LIST. Else if the route is resolvable earlier,
                 * then need to allocate a new route profile and add the new
                 * route profile with the updated values to the peer's
                 * RCVD_LIST.
                 */
                pPeer = BGP4_RT_PEER_ENTRY (pRtProfile);
                if (((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_NEXT_HOP_UNKNOWN)
                     == BGP4_RT_NEXT_HOP_UNKNOWN) &&
                    (BGP4_NH_METRIC_RESOLVE_STATUS (pIgpMetricEntry) ==
                     BGP4_TRUE))
                {
                    /* The route is previously unresolvable but now resolvable.
                     * So just withdraw the route */
#ifdef RFD_WANTED
                    BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_NO_RFD);
#endif
                    Bgp4RibhProcessWithdRoute (pPeer, pRtProfile, NULL);
                    BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);
                    /* Set the NO_RFD flag to BY-PASS RFD while processing
                     * the route as feasible. */
#ifdef RFD_WANTED
                    BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_NO_RFD);
#endif
                    pAddRtProfile = pRtProfile;
                }
                else
                {
                    /* The route is previously resolvable. Either the route has
                     * become unresolvable or some metric have changed.
                     * Allocate a new route profile and update it. */

                    pAddRtProfile = Bgp4DuplicateRouteProfile (pRtProfile);

                    if ((BGP4_NH_METRIC_ACTION (pIgpMetricEntry) ==
                         BGP4_NEXTHOP_METRIC_CHANGED)
                        && (pAddRtProfile != NULL))
                    {
                        BGP4_RT_SET_FLAG (pAddRtProfile,
                                          BGP4_RT_NH_METRIC_CHANGE);
                    }

                    if (pAddRtProfile == NULL)
                    {
                        /* Allocation FAILS. We are not able to handle the
                         * status change for this route.
                         */
                        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                       BGP4_ALL_FAILURE_TRC |
                                       BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                       "\tPEER %s : Allocate Route Profile for "
                                       "NEXT-HOP Status Change FAILS!!!\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeer),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeer))));
                        Bgp4DshReleaseRtInfo (pRtProfile);
                        pRtProfile = pNextRtProfile;
                        pNextRtProfile = NULL;
                        continue;
                    }
                    BGP4_RT_RESET_FLAG (pAddRtProfile, BGP4_RT_BEST);
#ifdef L3VPN
                    if ((BGP4_VPN4_PEER_ROLE
                         (BGP4_RT_PEER_ENTRY (pAddRtProfile)) ==
                         BGP4_VPN4_CE_PEER)
                        && (BGP4_RT_AFI_INFO (pAddRtProfile) ==
                            BGP4_INET_AFI_IPV4)
                        && (BGP4_RT_SAFI_INFO (pAddRtProfile) ==
                            BGP4_INET_SAFI_VPNV4_UNICAST))
                    {
                        BGP4_RT_SET_FLAG (pAddRtProfile,
                                          BGP4_RT_CONVERTED_TO_VPNV4);
                    }
#endif
                }

                /* Add the route to the PEER's RCVD_ROUTE List */
                Bgp4DshDelinkRouteFromChgList (pRtProfile);

                if ((BGP4_RT_GET_FLAGS (pAddRtProfile) & BGP4_RT_STALE) !=
                    BGP4_RT_STALE)
                {
                    Bgp4DshAddRouteToList (BGP4_PEER_RCVD_ROUTES (pPeer),
                                           pAddRtProfile, BGP4_PREPEND);
                }
                Bgp4DshReleaseRtInfo (pRtProfile);

                /* Check whether more route are available in this list
                 * to be processed. If no, then go to next IgpMetricEntry */
                if (pNextRtProfile == NULL)
                {
                    break;
                }

                /* Check whether we have processed expected MAX entires */
                if (u4RtCnt >= BGP4_MAX_NH_MET_RTS2PRCS)
                {
                    /* Has reached the MAX entries to be processed.
                     * Check for the Tic Break event. If return value is
                     * BGP4_TURE, then the environment may require the CPU.
                     * So BGP should relinquish the CPU. Before that store
                     * the next hop so that processing begins at this hop
                     * next time. */
                    Bgp4CopyAddrPrefixStruct (&
                                              (BGP4_NH_CHG_INIT_NEXTHOP_INFO
                                               (u4Context)),
                                              BGP4_NH_METRIC_NEXTHOP_INFO
                                              (pIgpMetricEntry));
                    BGP4_NH_CHG_INIT_VRFID (u4Context) =
                        BGP4_NH_METRIC_VRFID (pIgpMetricEntry);
                    Bgp4CopyNetAddressStruct (&
                                              (BGP4_NH_CHG_INIT_ROUTE_NETADDR_INFO
                                               (u4Context)),
                                              BGP4_RT_NET_ADDRESS_INFO
                                              (pNextRtProfile));
                    Bgp4CopyAddrPrefixStruct (&
                                              (BGP4_NH_CHG_INIT_NEXTHOP_PEER_INFO
                                               (u4Context)),
                                              BGP4_PEER_REMOTE_ADDR_INFO
                                              (BGP4_RT_PEER_ENTRY
                                               (pNextRtProfile)));
                    if (Bgp4IsCpuNeeded () == BGP4_TRUE)
                    {
                        /* External events requires CPU. Relinquish CPU. */
                        return (BGP4_RELINQUISH);
                    }
                    else
                    {
                        /* Maximum entries processed. Can return. */
                        return (BGP4_TRUE);
                    }
                }
                pRtProfile = pNextRtProfile;
                pNextRtProfile = NULL;
            }
            /* Have finished processing for all the routes with this next-hop */
            BGP4_NH_METRIC_ACTION (pIgpMetricEntry) =
                BGP4_NEXTHOP_METRIC_STABLE;
        }
    }

    Bgp4InitAddrPrefixStruct (&(BGP4_NH_CHG_INIT_NEXTHOP_INFO (u4Context)),
                              BGP4_INET_AFI_IPV4);
    Bgp4InitNetAddressStruct (&
                              (BGP4_NH_CHG_INIT_ROUTE_NETADDR_INFO (u4Context)),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    Bgp4InitAddrPrefixStruct (&(BGP4_NH_CHG_INIT_NEXTHOP_PEER_INFO (u4Context)),
                              BGP4_INET_AFI_IPV4);
    Bgp4InitNetAddressStruct (&
                              (BGP4_NH_CHG_INIT_ROUTE_NETADDR_INFO
                               (BGP4_DFLT_VRFID)), BGP4_INET_AFI_IPV4,
                              BGP4_INET_SAFI_UNICAST);
    Bgp4InitAddrPrefixStruct (&
                              (BGP4_NH_CHG_INIT_NEXTHOP_PEER_INFO
                               (BGP4_DFLT_VRFID)), BGP4_INET_AFI_IPV4);
    BGP4_NH_CHG_INIT_VRFID (u4Context) = u4Context;
    BGP4_NH_CHG_INTERVAL (u4Context) = 0;
    return (BGP4_FALSE);
}

/****************************************************************************/
/* Function    :  Bgp4ProcessDflRouteOrigPolicyChange                       */
/* Description :  This function process the change in the default route     */
/*                origination policy. If the new policy is enable, then this*/
/*                function will try and get the default route from FDB or   */
/*                get the best default route learnt from other Peers. If    */
/*                a default route exists, then this default route is        */
/*                advertised to peers. If the new policy is disable,        */
/*                if already any default route has been advertised to peers */
/*                as feasible, then the route will withdrawn from the peer  */
/*                provided the peer based default-origination is not        */
/*                configured.                                               */
/* Input       :  None.                                                     */
/* Output      :  None.                                                     */
/* Return      :  BGP4_TRUE  - if more routes available for processing.     */
/*             :  BGP4_FALSE - if no more routes available for processing.  */
/*                BGP4_RELINQUISH - if external events requires CPU         */
/****************************************************************************/
INT4
Bgp4ProcessDflRouteOrigPolicyChange (UINT4 u4ContextId, UINT1 u1NewDftRtPolicy,
                                     UINT1 u1OldDftRtPolicy)
{
    VOID               *pRibNode = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tBgp4PeerEntry     *pPeerentry = NULL;
    tTMO_SLL            TsFeasList;
    tTMO_SLL            TsWithList;
    tRouteProfile       RtProfile;
    UINT4               u4AsafiIndex = 0;
    INT4                i4Status = BGP4_SUCCESS;
    UINT2               u2Afi = 0;
    UINT2               u2Safi = 0;

    TMO_SLL_Init (&TsFeasList);
    TMO_SLL_Init (&TsWithList);

    if (u1NewDftRtPolicy == u1OldDftRtPolicy)
    {
        /* No change in policy. Just return. */
        return BGP4_SUCCESS;
    }

    for (u4AsafiIndex = 0; u4AsafiIndex < BGP4_MPE_ADDR_FAMILY_MAX_INDEX;
         u4AsafiIndex++)
    {
        if (Bgp4GetAfiSafiFromIndex (u4AsafiIndex, &u2Afi, &u2Safi)
            == BGP4_FAILURE)
        {
            /* Address family not supported. */
            continue;
        }

        if (u1NewDftRtPolicy == BGP4_DEF_ROUTE_ORIG_ENABLE)
        {
            /* Need to get the Default route from FDB, if ever it is present
             * and add it to the BGP RIB. */
            i4Status = Bgp4GetDefaultRouteFromFDB (u4ContextId, u2Afi);

            if (i4Status == BGP4_SUCCESS)
            {
                /* Default route is present in FDB and it is added to BGP RIB.
                 * This default route will be selected as the best route and
                 * will be advertised to peers. */
                continue;
            }

            /* Default route is not present in FDB. Try and get the default
             * route if present in BGP RIB. If present, then advertise this
             * route to the peers. */
            RtProfile.u1Ref = 0;
            RtProfile.u1Protocol = 0;
            RtProfile.p_RPNext = NULL;
            RtProfile.u4Flags = 0;
            RtProfile.pRtInfo = 0;

            Bgp4InitNetAddressStruct (&(RtProfile.NetAddress), u2Afi,
                                      (UINT1) u2Safi);
            i4Status = Bgp4RibhLookupRtEntry (&RtProfile, u4ContextId,
                                              BGP4_TREE_FIND_EXACT,
                                              &pRtProfile, &pRibNode);
            if (i4Status == BGP4_FAILURE)
            {
                /* No default routes present in RIB */
                continue;
            }

            pRtProfile = Bgp4DshGetBestRouteFromProfileList (pRtProfile,
                                                             u4ContextId);
            if (pRtProfile == NULL)
            {
                /* No valid default route present in RIB. */
                continue;
            }

            /* Default route is present in RIB. Need to advertise this route
             * to the peers. Just add the route to the Peer's Advt List and
             * the route will be advertised as per the new policy. */
            if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                 BGP4_RT_IN_INT_PEER_ADVT_LIST) !=
                BGP4_RT_IN_INT_PEER_ADVT_LIST)
            {
                /* Add the route to internal peer advertise list. */
                Bgp4AddFeasRouteToIntPeerAdvtList (pRtProfile, NULL);
            }

            if (((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_IN_FIB_UPD_LIST) !=
                 BGP4_RT_IN_FIB_UPD_LIST) &&
                ((BGP4_RT_GET_FLAGS (pRtProfile) &
                  BGP4_RT_IN_EXT_PEER_ADVT_LIST) !=
                 BGP4_RT_IN_EXT_PEER_ADVT_LIST))
            {
                /* Add the route to external peer advertise list. */
                Bgp4AddFeasRouteToExtPeerAdvtList (pRtProfile, NULL);
            }
        }
        else
        {
            /* Remove the redistributed default route in Tree and advertise it
             * as withdrawn to the peers. */
            RtProfile.u1Ref = 0;
            RtProfile.u1Protocol = 0;
            RtProfile.p_RPNext = NULL;
            RtProfile.u4Flags = 0;
            RtProfile.pRtInfo = NULL;
            Bgp4InitNetAddressStruct (&(RtProfile.NetAddress), u2Afi,
                                      (UINT1) u2Safi);

            i4Status = Bgp4RibhLookupRtEntry (&RtProfile, u4ContextId,
                                              BGP4_TREE_FIND_EXACT,
                                              &pRtProfile, &pRibNode);
            if (i4Status == BGP4_FAILURE)
            {
                /* No default routes present in RIB */
                continue;
            }

            pRtProfile = Bgp4DshGetBestRouteFromProfileList (pRtProfile,
                                                             u4ContextId);
            if (pRtProfile == NULL)
            {
                /* No valid default route present in RIB. */
                return BGP4_SUCCESS;
            }

            /* Default route is present in the RIB. Need to advertise this
             * route as withdrawn to all the peers. */
            if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                 BGP4_RT_IN_INT_PEER_ADVT_LIST) ==
                BGP4_RT_IN_INT_PEER_ADVT_LIST)
            {
                BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_IN_INT_PEER_ADVT_LIST);
                Bgp4DshDelinkRouteFromList (BGP4_INT_PEERS_ADVT_LIST
                                            (u4ContextId), pRtProfile,
                                            BGP4_INT_PEER_LIST_INDEX);
                /* Set flag not to internal so that this route is not
                 * advertised to internal peers. */
                BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_ADVT_NOTTO_INTERNAL);
            }

            if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                 BGP4_RT_IN_EXT_PEER_ADVT_LIST) ==
                BGP4_RT_IN_EXT_PEER_ADVT_LIST)
            {
                BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_IN_EXT_PEER_ADVT_LIST);
                Bgp4DshDelinkRouteFromList (BGP4_EXT_PEERS_ADVT_LIST
                                            (u4ContextId), pRtProfile,
                                            BGP4_EXT_PEER_LIST_INDEX);
                /* Set flag not to external so that this route is not
                 * advertised to external peers. */
                BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_ADVT_NOTTO_EXTERNAL);
            }

            if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_IN_FIB_UPD_LIST) ==
                BGP4_RT_IN_FIB_UPD_LIST)
            {
                /* Default route is present in FIB update list. If the route is
                 * BGP route then the route should be installed in FIB but not
                 * advertised to external peers. However if the route is
                 * non-BGP route then this route need not be added to the FIB.
                 */
                if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
                {
                    /* Set flag not to external so that this route will be
                     * installed in the FIB and will not be advertised to
                     * external peers. */
                    BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_ADVT_NOTTO_EXTERNAL);
                }
                else
                {
                    BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_IN_FIB_UPD_LIST);
                    Bgp4DshDelinkRouteFromList (BGP4_FIB_UPD_LIST (u4ContextId),
                                                pRtProfile,
                                                BGP4_FIB_UPD_LIST_INDEX);
                }
            }

            /* Advertise the default route as withdrawn to all peers. */
            BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_AGGR_WITHDRAWN);
            Bgp4DshAddRouteToList (&TsWithList, pRtProfile, 0);

            TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4ContextId), pPeerentry,
                          tBgp4PeerEntry *)
            {
                if (BGP4_PEER_STATE (pPeerentry) != BGP4_ESTABLISHED_STATE)
                {
                    /* Connection not in established state. So no need to
                     * advertise to this peer. */
                    continue;
                }

                /* Route can be advertised to this peer. */
                Bgp4PeerSendRoutesToPeerTxQ (pPeerentry, &TsFeasList,
                                             &TsWithList, BGP4_FALSE);
            }

            /* Clear the route from the withdrawn list. */
            Bgp4DshReleaseList (&TsWithList, 0);
            if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_IN_FIB_UPD_LIST) !=
                BGP4_RT_IN_FIB_UPD_LIST)
            {
                /* Clear not to extern flag */
                BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_ADVT_NOTTO_EXTERNAL);
            }

            BGP4_RT_RESET_FLAG (pRtProfile, (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                             BGP4_RT_AGGR_WITHDRAWN));

            /* If the route is non-BGP route, then remove it from the RIB. */
            if (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID)
            {
                BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);
                BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_IN_REDIST_LIST);
                Bgp4DshAddRouteToList (BGP4_REDIST_LIST (u4ContextId),
                                       pRtProfile, BGP4_PREPEND);
            }
        }
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function        : BGPUpdateDistanceChangeForExistRoutes                   */
/* Description     : Bgp update configured distance value for best routes    */
/*                   in RIB                                                  */
/* Input           : u4Context - BGP context id                              */
/*                   u4AsafiMask - u4AsafiMask - <AFI, SAFI> mask which      */
/*                   uniquely identifies a pair of <AFI, SAFI>               */
/* Output          : None                                                    */
/* Returns         : None                                                    */
/*****************************************************************************/
INT4
BGPUpdateDistanceChangeForExistRoutes (UINT4 u4Context, UINT4 u4AsafiMask)
{
    UINT4               i4Return = 0;
    INT4                i4Found = 0;
    VOID               *pRibNode = NULL;
    tRouteProfile      *pRoute = NULL;
    tRouteProfile      *pCurRoute = NULL;
    tRouteProfile      *pPrevRoute = NULL;

    Bgp4GetRibNode (u4Context, u4AsafiMask, &pRibNode);
    if (pRibNode == NULL)
    {
        return BGP4_CLI_ERR_GENERAL_ERROR;
    }

    /* invalid cookie; get the first index */
    i4Found = Bgp4RibhGetFirstEntry (u4AsafiMask, &pRoute, &pRibNode, TRUE);
    if ((i4Found != BGP4_SUCCESS))
    {
        /* No routes present in the BGP */
        return i4Return;
    }
    do
    {
        if ((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_BEST) == BGP4_RT_BEST)
        {
            BGP4RTModifyRouteInCommIpRtTbl (pRoute);
        }
        pCurRoute = BGP4_RT_NEXT (pRoute);
        if (pCurRoute != NULL)
        {
            pRoute = pCurRoute;
            continue;
        }

        /* get the next indecies to the table */
        pPrevRoute = pRoute;
        pRoute = NULL;
        i4Found = Bgp4RibhGetNextEntry (pPrevRoute, u4Context,
                                        &pRoute, &pRibNode, TRUE);
        if (i4Found != BGP4_SUCCESS)
        {
            break;
        }
    }
    while ((pRoute != NULL));
    return BGP4_SUCCESS;
}
#endif /* _BGP4PRCS_C */
