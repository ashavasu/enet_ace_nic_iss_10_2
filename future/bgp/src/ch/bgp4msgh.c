/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4msgh.c,v 1.51 2017/09/15 06:19:52 siva Exp $
 *
 * Description: These routines are part of the Message Handler 
 *              SubModule which process the message received from 
 *              the peer
 *
 *******************************************************************/
#ifndef BGP4MSGH_C
#define BGP4MSGH_C

#include "bgp4com.h"

PRIVATE UINT4       au4Bgp4AttrMask[] = {
    0,
    BGP4_ATTR_ORIGIN_MASK,
    BGP4_ATTR_PATH_MASK,
    BGP4_ATTR_NEXT_HOP_MASK,
    BGP4_ATTR_MED_MASK,
    BGP4_ATTR_LOCAL_PREF_MASK,
    BGP4_ATTR_ATOMIC_AGGR_MASK,
    BGP4_ATTR_AGGREGATOR_MASK,
    BGP4_ATTR_COMM_MASK,
    BGP4_ATTR_ORIG_ID_MASK,
    BGP4_ATTR_CLUS_LIST_MASK,
    0,
    0,
    0,
    BGP4_ATTR_MP_REACH_NLRI_MASK,
    BGP4_ATTR_MP_UNREACH_NLRI_MASK,
    BGP4_ATTR_ECOMM_MASK,
    BGP4_ATTR_PATH_FOUR_MASK,
    BGP4_ATTR_AGGREGATOR_FOUR_MASK,
    BGP4_ATTR_UNKNOWN_MASK
};

/******************************************************************************/
/* Function Name : Bgp4MhProcessOpen                                          */
/* Description   : This function process the receivied BGP OPEN message. If   */
/*                 some of the fields in the message are invalid then         */
/*                 NOTIFICATION is send to the corresponding peer.            */
/* Input(s)      : Peer which send us the OPEN message (pPeerentry),          */
/*                 Buffer containing the OPEN message (pu1Buf),               */
/*                 Size of the OPEN message (u4Msglen).                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the processing was successful,             */
/*                 BGP4_FAILURE otherwise.                                    */
/******************************************************************************/
INT4
Bgp4MhProcessOpen (tBgp4PeerEntry * pPeerentry, UINT1 *pu1Buf, UINT4 u4Msglen)
{
    UINT1              *pu1OptParamBuf = NULL;
    UINT1               au1OptErrData[MAX_CAP_OPTPARAM_LEN];
    UINT1               u1InvalidPeerAs = BGP4_FALSE;
    UINT4               u4BGPId;
    UINT4               u4Len;
    INT4                i4RetSts;
    UINT2               u2ASNo;
    UINT2               u2HoldTime;
    UINT2               u2Data = 0;
    UINT2               u2Holdtmr = 0;
    UINT2               u2Keepalivetmr = 0;
    UINT2               u2PrcsdOptParamLen = 0;
    UINT1               u1Version;
    UINT1               u1OptParmLen = 0;
    UINT1               u1OptParamType;
    UINT1               u1OptParamDataLen;
    UINT1               u1CapOptParamPrcsd = BGP4_FALSE;

    UNUSED_PARAM (u4Msglen);

    u4Len = 0;

    u1Version = *pu1Buf;
    u4Len += sizeof (u1Version);

    PTR_FETCH2 (u2ASNo, (pu1Buf + u4Len));
    u4Len += sizeof (u2ASNo);

    PTR_FETCH2 (u2HoldTime, (pu1Buf + u4Len));
    u4Len += sizeof (u2HoldTime);

    PTR_FETCH4 (u4BGPId, (pu1Buf + u4Len));
    u4Len += sizeof (u4BGPId);

    u1OptParmLen = (UINT1) *(pu1Buf + u4Len);
    u4Len += sizeof (u1OptParmLen);

    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                   BGP4_RX_TRC | BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                   "\tPEER %s : Received OPEN Message - Version: %d, "
                   "AS no: %d \n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry))),
                   u1Version, u2ASNo);

    if (u1Version != BGP4_VERSION_4)
    {
        u2Data = BGP4_VERSION_4;
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_PEER_CON_TRC
                       | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tPEER %s - OPEN MSG ERR - Unsupported Version (%d)\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))), u1Version);
        Bgp4EhSendError (pPeerentry, BGP4_OPEN_MSG_ERR, BGP4_UNSUPPORTED_VER_NO,
                         (UINT1 *) &u2Data, sizeof (UINT2));
        return (BGP4_FAILURE);
    }

    /* The AS number received in the open msg is validated as follows:
     * 1. If u2ASNo is 0, it is invalid
     * 2. If u2ASNo is AS_TRANS, check for the 4-byte ASN support 
     *                           for this peer.
     *       ---> If 4-byte ASN is not supported, Compare u2ASNo with configured PEER_AS.
     *       ---> If 4-byte ASN is supported, proceed to compare the capability value.
     *                                        The capability value will be compared with
     *                                        the PEER_AS.
     * 3. If u2ASNo != AS_TRANS, Compare u2ASNo with configured PEER_AS. */

    if (u2ASNo == BGP4_INV_AS)
    {
        u1InvalidPeerAs = BGP4_TRUE;
    }

    if ((u2ASNo == BGP4_AS_TRANS) &&
        (BGP4_FOUR_BYTE_ASN_SUPPORT (BGP4_PEER_CXT_ID (pPeerentry)) ==
         BGP4_DISABLE_4BYTE_ASN_SUPPORT))
    {
        if (BGP4_PEER_ASNO (pPeerentry) != u2ASNo)
        {
            u1InvalidPeerAs = BGP4_TRUE;
        }
    }
    if ((u2ASNo != BGP4_AS_TRANS) && (BGP4_PEER_ASNO (pPeerentry) != u2ASNo))
    {
        u1InvalidPeerAs = BGP4_TRUE;
    }

    if (u1InvalidPeerAs == BGP4_TRUE)
    {
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_PEER_CON_TRC
                       | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tPEER %s - OPEN MSG ERR - Invalid AS No (%d)\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))), u2ASNo);
        Bgp4EhSendError (pPeerentry, BGP4_OPEN_MSG_ERR, BGP4_AS_UNACCEPTABLE,
                         NULL, 0);
        return (BGP4_FAILURE);
    }

    /* CHECK FOR THE VALIDITY OF THE BGP IDENTIFIER */
    if ((BGP4_IS_VALID_ADDRESS (u4BGPId) == 0) ||
        (u4BGPId == BGP4_LOCAL_BGP_ID (BGP4_PEER_CXT_ID (pPeerentry))))
    {
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                       "\tPEER %s - OPEN MSG ERR - Invalid BGP ID (%d)\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))), u4BGPId);
        Bgp4EhSendError (pPeerentry, BGP4_OPEN_MSG_ERR, BGP4_BGPID_INCORRECT,
                         NULL, 0);
        return (BGP4_FAILURE);
    }

    if (u1OptParmLen > 0)
    {
        UINT2               u2RcvdCapsLen = 0;    /* received capabilities value length */
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_RX_TRC | BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Received OPEN with Option Parameter "
                       "length: %d, \n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))), u1OptParmLen);
        pu1OptParamBuf = pu1Buf + OPTIONAL_PARAMETER_OFFSET;
        while (pu1OptParamBuf < (pu1Buf + OPTIONAL_PARAMETER_OFFSET +
                                 u1OptParmLen))
        {
            /* Check for the Capabilities Optional Parameter type */
            u1OptParamType = *(pu1OptParamBuf);
            if (u1OptParamType == CAPABILITIES_OPTIONAL_PARAMETER)
            {
                if (u1CapOptParamPrcsd != BGP4_TRUE)
                {
                    if (CAPS_ADVT_SUP (BGP4_PEER_CXT_ID (pPeerentry)) ==
                        CAPS_TRUE)
                    {
                        u1CapOptParamPrcsd = BGP4_TRUE;
                        i4RetSts = CapsRcvdHandler (pu1Buf, pPeerentry,
                                                    &u2RcvdCapsLen);
                        if (i4RetSts != BGP4_SUCCESS)
                        {
                            return (BGP4_FAILURE);
                        }
                        /* Processing capabilities optional parameter success.
                         * Update the processed optional parameter length
                         */
                        u2PrcsdOptParamLen += u2RcvdCapsLen;
                    }
                    else
                    {
                        /* Error Handling */
                        BGP4_TRC_ARG1 (&
                                       (BGP4_PEER_REMOTE_ADDR_INFO
                                        (pPeerentry)), BGP4_TRC_FLAG,
                                       BGP4_ALL_FAILURE_TRC | BGP4_PEER_CON_TRC
                                       | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                                       "\tPEER %s - OPEN MSG ERR - "
                                       "Unrecognized Optional Parameter\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeerentry),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeerentry))));
                        u1OptParamDataLen =
                            *(pu1Buf + OPTIONAL_PARAMETER_OFFSET +
                              OPTPARAM_TYPE_SIZE + u2PrcsdOptParamLen);
                        if ((OPTPARAM_TYPE_LENGTH_SIZE + u1OptParamDataLen) <
                            BGP4_MAX_BUFFER_LENGTH)
                        {
                            MEMCPY (au1OptErrData,
                                    (pu1Buf + OPTIONAL_PARAMETER_OFFSET +
                                     u2PrcsdOptParamLen),
                                    OPTPARAM_TYPE_LENGTH_SIZE +
                                    u1OptParamDataLen);
                        }
                        Bgp4EhSendError (pPeerentry, BGP4_OPEN_MSG_ERR,
                                         BGP4_OPT_PARM_UNRECOGNIZED,
                                         au1OptErrData,
                                         OPTPARAM_TYPE_LENGTH_SIZE +
                                         u1OptParamDataLen);
                        return (BGP4_FAILURE);
                    }
                }
            }
            else
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s - OPEN MSG ERR - "
                               "Unrecognized Optional Parameter\n",
                               Bgp4PrintIpAddr
                               (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry))));
                u1OptParamDataLen =
                    *(pu1Buf + OPTIONAL_PARAMETER_OFFSET + OPTPARAM_TYPE_SIZE);
                if ((OPTPARAM_TYPE_LENGTH_SIZE + u1OptParamDataLen) <
                    BGP4_MAX_BUFFER_LENGTH)
                {
                    MEMCPY (au1OptErrData, (pu1Buf + OPTIONAL_PARAMETER_OFFSET),
                            OPTPARAM_TYPE_LENGTH_SIZE + u1OptParamDataLen);
                }
                Bgp4EhSendError (pPeerentry, BGP4_OPEN_MSG_ERR,
                                 BGP4_OPT_PARM_UNRECOGNIZED, au1OptErrData,
                                 OPTPARAM_TYPE_LENGTH_SIZE + u1OptParamDataLen);
                return (BGP4_FAILURE);
            }
            /* 
             * Reach the position of next Optional parameter in 
             * the OPEN message 
             */
            pu1OptParamBuf = pu1OptParamBuf +
                (*(pu1OptParamBuf + OPTPARAM_TYPE_SIZE)) +
                OPTPARAM_TYPE_LENGTH_SIZE;
        }
    }

    if (u2HoldTime < BGP4_PEER_HOLD_TIME (pPeerentry))
    {
        u2Holdtmr = u2HoldTime;
        if (u2Holdtmr > 0)
        {
            /* u2Keepalivetmr = (u2Keepalivetmr) ? u2Keepalivetmr : 1; */

            if (u2Holdtmr < BGP4_MIN_HOLD_INTERVAL)
            {
                BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_PEER_CON_TRC | BGP4_CONTROL_PATH_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s - OPEN MSG ERR - Invalid "
                               "Hold Timer Value (%d)\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))), u2Holdtmr);
                Bgp4EhSendError (pPeerentry, BGP4_OPEN_MSG_ERR,
                                 BGP4_HOLD_TMR_UNACCEPTABLE, NULL, 0);
                return (BGP4_FAILURE);
            }
            u2Keepalivetmr = (UINT2) BGP4_PEER_KEEP_ALIVE_TIME (pPeerentry);
            if (u2Keepalivetmr > (u2Holdtmr / 3))
            {
                u2Keepalivetmr = (u2Holdtmr / 3);
            }
        }
    }
    else
    {
        u2Holdtmr = (UINT2) BGP4_PEER_HOLD_TIME (pPeerentry);
        if (u2Holdtmr > 0)
        {
            u2Keepalivetmr = (UINT2) BGP4_PEER_KEEP_ALIVE_TIME (pPeerentry);
            if (u2Keepalivetmr > (u2Holdtmr / 3))
            {
                u2Keepalivetmr = (u2Holdtmr / 3);
            }
        }
        else
        {
            u2Keepalivetmr = 0;
        }
    }

    BGP4_PEER_BGP_ID (pPeerentry) = u4BGPId;
    BGP4_PEER_NEG_HOLD_INT (pPeerentry) = u2Holdtmr;
    BGP4_PEER_NEG_KEEP_ALIVE (pPeerentry) = u2Keepalivetmr;
    BGP4_PEER_NEG_VER (pPeerentry) = u1Version;
    Bgp4RedSyncBgpPeerInfo (BGP4_PEER_CXT_ID (pPeerentry), pPeerentry);
    return (BGP4_SUCCESS);
}

/******************************************************************************/
/* Function Name : Bgp4MhProcessNotification                                  */
/* Description   : This function processes the received BGP NOTIFICATION      */
/*                 message.                                                   */
    /* Input(s)      : Peer which send us the NOTIFICATION  message (pPeerentry)  */
/*                 Buffer containing the NOTIFICATION  message (pu1Buf),      */
/*                 Size of the NOTIFICATION  message (u4Msglen).              */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS always                                        */
/******************************************************************************/
INT4
Bgp4MhProcessNotification (tBgp4PeerEntry * pPeerentry,
                           UINT1 *pu1Buf, UINT4 u4Msglen)
{
    BGP4_PEER_LAST_ERROR (pPeerentry) = *pu1Buf;
    BGP4_PEER_LAST_ERROR_SUB_CODE (pPeerentry) =
        *(pu1Buf + BGP_ERROR_CODE_LENGTH);

    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                   BGP4_RX_TRC | BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                   "\tPEER %s : Received Notification with ERR CODE. =%d,"
                   "SUBCODE = %d\n ",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry))),
                   BGP4_PEER_LAST_ERROR (pPeerentry),
                   BGP4_PEER_LAST_ERROR_SUB_CODE (pPeerentry));

    /* RFC 4271 Update */
    if (BGP4_PEER_LAST_ERROR (pPeerentry) == BGP4_CEASE)
    {
        Bgp4SEMHProcessEvent (pPeerentry, BGP4_AUTOMATIC_STOP_EVT, NULL, 0);
    }
    if ((BGP4_TRC_FLAG & BGP4_DUMP_HEX_TRC) == BGP4_DUMP_HEX_TRC)
    {
        Bgp4DumpBuf (pu1Buf, u4Msglen, BGP4_INCOMING);
    }
    /* DATA FIELD IS NOT PROCESSED */
    return (BGP4_SUCCESS);
}

/******************************************************************************/
/* Function Name : Bgp4MhProcessEndOfRIBMarker                                */
/* Description   : This function process the receivied BGP EOR message.       */
/* Input(s)      : Peer which send us the UPDATE message (pPeerentry),        */
/*                 Buffer containing the UPDATE message (pu1Buf),             */
/*                 Size of the UPDATE message (u4Msglen).                     */
/* Output(s)     : -                                                          */
/* Return(s)     : BGP4_SUCCESS if the processing was successful,             */
/*                 BGP4_FAILURE otherwise                                     */
/******************************************************************************/
INT4
Bgp4MhProcessEndOfRIBMarker (tBgp4PeerEntry * pPeerentry, UINT1 *pu1Buf,
                             UINT4 u4Msglen)
{

    UINT1              *pu1RcvdBuf = NULL;
    UINT4               u4AsafiMask = 0;
    UINT2               u2MpeAttribLen = BGP4_FALSE;
    UINT2               u2RcvdAfi = 0;
    UINT1               u1Flag = BGP4_FALSE;
    UINT1               u1RcvdSafi = 0;
    UINT1               u1Attr = 0;
#ifdef BGP4_IPV6_WANTED
    UINT1               u1SupCapIpv6 = ZERO;
#endif
#ifdef VPLSADS_WANTED
    UINT1               u1SupCapVpls = ZERO;
#endif
#ifdef L3VPN
    UINT1               u1SupCapL3vpn = ZERO;
#endif

    if (u4Msglen == sizeof (UINT4))
    {
        if ((pPeerentry->peerConfig).RemoteAddrInfo.u2Afi == BGP4_INET_AFI_IPV4)
        {
            BGP4_PEER_EOR_MARKER (pPeerentry) = BGP4_TRUE;
            Bgp4GRProcessEndOfRIBRcvd (pPeerentry);
        }
        else
        {
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerentry) & CAP_NEG_IPV4_UNI_MASK)
                == CAP_NEG_IPV4_UNI_MASK)
            {
                BGP4_EOR_RCVD (pPeerentry) =
                    BGP4_EOR_RCVD (pPeerentry) | BGP4_CAP_BITMASK_IPV4;
            }

        }
    }
    else
    {
        pu1RcvdBuf = pu1Buf;

        /*Extract Path attribute and attribute length from recived update message */
        u1Attr =
            *(pu1Buf + BGP4_WITHDRAWN_LEN_FIELD_SIZE +
              BGP4_ATTR_LEN_FIELD_SIZE + BGP4_ATYPE_FLAGS_LEN);
        u2MpeAttribLen =
            *(pu1RcvdBuf + BGP4_WITHDRAWN_LEN_FIELD_SIZE +
              BGP4_ATTR_LEN_FIELD_SIZE + BGP4_ATYPE_FLAGS_LEN +
              BGP4_ATYPE_CODE_LEN + BGP4_NORMAL_ATTR_LEN_SIZE);

        if (!(u1Attr == BGP4_ATTR_MP_UNREACH_NLRI && u2MpeAttribLen == 0))
        {
            /*Packet received is not End-of-Rib Marker */
            return BGP4_FAILURE;
        }

        pu1RcvdBuf +=
            BGP4_WITHDRAWN_LEN_FIELD_SIZE + BGP4_ATTR_LEN_FIELD_SIZE +
            BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_NORMAL_ATTR_LEN_SIZE;

        /*Extract AFI and SAFI Information from recived update message */
        PTR_FETCH2 (u2RcvdAfi, pu1RcvdBuf);
        u1RcvdSafi = *(pu1RcvdBuf + BGP4_MPE_ADDRESS_FAMILY_LEN);
        u4AsafiMask = ((UINT4) u2RcvdAfi << BGP4_TWO_BYTE_BITS) | u1RcvdSafi;

        switch (u4AsafiMask)
        {
#ifdef BGP4_IPV6_WANTED
            case CAP_MP_IPV6_UNICAST:
                if ((pPeerentry->peerConfig).RemoteAddrInfo.u2Afi ==
                    BGP4_INET_AFI_IPV6)
                {
                    BGP4_PEER_EOR_MARKER (pPeerentry) = BGP4_TRUE;
                    Bgp4GRProcessEndOfRIBRcvd (pPeerentry);
                    break;
                }
                else
                {
                    if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerentry) &
                         CAP_NEG_IPV6_UNI_MASK) == CAP_NEG_IPV6_UNI_MASK)

                    {
                        BGP4_EOR_RCVD (pPeerentry) =
                            BGP4_EOR_RCVD (pPeerentry) | BGP4_CAP_BITMASK_IPV6;
                    }

                }
                break;
#endif

#ifdef L3VPN
            case CAP_MP_VPN4_UNICAST:

                if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerentry) &
                     CAP_MP_VPN4_UNICAST) == CAP_MP_VPN4_UNICAST)
                {
                    BGP4_EOR_RCVD (pPeerentry) =
                        BGP4_EOR_RCVD (pPeerentry) | BGP4_CAP_BITMASK_L3VPN;

                }

                break;
#endif
#ifdef VPLSADS_WANTED
            case CAP_MP_L2VPN_VPLS:
#ifdef VPLS_GR_WANTED
                if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerentry) &
                     CAP_NEG_L2VPN_VPLS_MASK) == CAP_NEG_L2VPN_VPLS_MASK)
                {
                    BGP4_EOR_RCVD (pPeerentry) =
                        BGP4_EOR_RCVD (pPeerentry) | BGP4_CAP_BITMASK_VPLS;
                }

#endif /*BGP4_IPV6_WANTED */
#endif /*VPLS_GR_WANTED */
                break;
            default:
                BGP4_DBG3 (BGP4_DBG_ALL,
                           "\tBgp4MhProcessUpdate() : Update with Unknown <%d, %d> "
                           "is received from peer %s, Update IS Discarded !!!\n",
                           u2RcvdAfi, u1RcvdSafi,
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));

        }
    }

    /* Set BGP4_PEER_RECVD_EOR_MARKER(pPeerEntry) only if EOR for all supported capabilities are received */
    /* FOR IPv4 */
    u1Flag =
        (((BGP4_PEER_NEG_ASAFI_MASK (pPeerentry) & CAP_NEG_IPV4_UNI_MASK) ==
          CAP_NEG_IPV4_UNI_MASK)
         && (pPeerentry->peerConfig).RemoteAddrInfo.u2Afi !=
         BGP4_INET_AFI_IPV4) ? BGP4_CAP_BITMASK_IPV4 : ZERO;

#ifdef BGP4_IPV6_WANTED
    /* FOR IPv6 */
    u1SupCapIpv6 =
        (((BGP4_PEER_NEG_ASAFI_MASK (pPeerentry) & CAP_NEG_IPV6_UNI_MASK) ==
          CAP_NEG_IPV6_UNI_MASK)
         && (pPeerentry->peerConfig).RemoteAddrInfo.u2Afi !=
         BGP4_INET_AFI_IPV6) ? BGP4_CAP_BITMASK_IPV6 : ZERO;

#endif
#ifdef VPLSADS_WANTED
    /* FOR VPLS */
    u1SupCapVpls =
        (((BGP4_PEER_NEG_ASAFI_MASK (pPeerentry) & CAP_NEG_L2VPN_VPLS_MASK) ==
          CAP_NEG_L2VPN_VPLS_MASK)) ? BGP4_CAP_BITMASK_VPLS : ZERO;

#endif
#ifdef L3VPN
    /* FOR L3VPN */
#ifdef VPLSADS_WANTED
    u1SupCapL3vpn =
        ((BGP4_PEER_NEG_ASAFI_MASK (pPeerentry) & CAP_NEG_L2VPN_VPLS_MASK) ==
         CAP_NEG_L2VPN_VPLS_MASK) ? BGP4_CAP_BITMASK_L3VPN : ZERO;
#endif
#endif

    /* Perform EX-NOR opertion to verify if EOR is rcvd for all supported capabilities */
    if (BGP4_CHECK_RCVD_EOR
        (u1Flag, (BGP4_EOR_RCVD (pPeerentry) & BGP4_CAP_BITMASK_IPV4))
#ifdef BGP4_IPV6_WANTED
        &&
        (BGP4_CHECK_RCVD_EOR
         (u1SupCapIpv6, (BGP4_EOR_RCVD (pPeerentry) & BGP4_CAP_BITMASK_IPV6)))
#endif
#ifdef VPLSADS_WANTED
        &&
        (BGP4_CHECK_RCVD_EOR
         (u1SupCapVpls, (BGP4_EOR_RCVD (pPeerentry) & BGP4_CAP_BITMASK_VPLS)))
#endif
#ifdef L3VPN
        &&
        (BGP4_CHECK_RCVD_EOR
         (u1SupCapL3vpn, (BGP4_EOR_RCVD (pPeerentry) & BGP4_CAP_BITMASK_L3VPN)))
#endif
        )
    {
        /*EOR for all supported capabilities rcvd. Set EOR MARK as BGP4_TRUE */
        if (BGP4_PEER_EOR_MARKER (pPeerentry) == BGP4_TRUE)
        {
            Bgp4GRMarkEndOfRIBMarker (pPeerentry);
        }
    }

    return BGP4_SUCCESS;
}

/******************************************************************************/

/******************************************************************************/
/* Function Name : Bgp4MhProcessUpdate                                        */
/* Description   : This function process the receivied BGP UPDATE message. If */
/*                 some of the fields in the message are invalid then         */
/*                 NOTIFICATION is send to the corresponding peer. It forms   */
/*                 the list of new routes and withdrawn routes advertised     */
/*                 in the update message which will be given to the RIB       */
/*                 handler for the updation of the RIB tree.                  */
/* Input(s)      : Peer which send us the UPDATE message (pPeerentry),        */
/*                 Buffer containing the UPDATE message (pu1Buf),             */
/*                 Size of the UPDATE message (u4Msglen).                     */
/* Output(s)     : List of New routes from the buffer (pTsNewroutes),         */
/*                 List of withdrawn routes (pTsWithdrawnroutes).             */
/* Return(s)     : BGP4_SUCCESS if the processing was successful,             */
/*                 BGP4_FAILURE otherwise                                     */
/* Note          : It also checks for the presence of any Routing Loop and    */
/*                 also processes the same.                                   */
/******************************************************************************/
INT4
Bgp4MhProcessUpdate (tBgp4PeerEntry * pPeerentry,
                     UINT1 *pu1Buf,
                     UINT4 u4Msglen,
                     tTMO_SLL * pTsNewroutes, tTMO_SLL * pTsWithdrawnroutes)
{
    tLinkNode          *pLinknode = NULL;
    INT4                i4NLRILen;
    INT4                i4RetVal;
    UINT2               u2Withdrawnlen;
    UINT2               u2Pathattrlen;

    BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                   BGP4_RX_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                   "\tPEER %s : Received Update Message of length %d"
                   " excluding header\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry))),
                   u4Msglen);

    if ((BGP4_TRC_FLAG & BGP4_DUMP_HEX_TRC) == BGP4_DUMP_HEX_TRC)
    {
        Bgp4DumpBuf (pu1Buf, u4Msglen, BGP4_INCOMING);
    }
    /* End-Of-RIB marker received */
    if (u4Msglen == sizeof (UINT4)
        || (u4Msglen == (BGP4_EOR_MARKER_MAX_LEN - BGP4_MSG_COMMON_HDR_LEN)))
    {
        if (Bgp4GRIsPeerGRCapable (pPeerentry) == BGP4_SUCCESS)
        {
            if (Bgp4MhProcessEndOfRIBMarker (pPeerentry, pu1Buf, u4Msglen) ==
                BGP4_SUCCESS)
            {
                /*Start the Selection deferal timer here and set the deferal flag to Timer set */
                if ((Bgp4GRCheckEORMarkerRecvdAtEstb
                     (BGP4_PEER_CXT_ID (pPeerentry)) == BGP4_TRUE)
                    && (Bgp4GRCheckRestartMode (BGP4_PEER_CXT_ID (pPeerentry))
                        == BGP4_RESTARTING_MODE))
                {
                    gBgpNode.pBgpSelectionDeferTmr->u4Data =
                        BGP4_SELECTION_DEFERRAL_TIMER;
                    BGP4_TIMER_START (BGP4_TIMER_LISTID,
                                      gBgpNode.pBgpSelectionDeferTmr,
                                      BGP4_SELECTION_DEFERAL_INTERVAL
                                      (BGP4_DFLT_VRFID));
                    BGP4_SELECTION_DEFERAL_FLAG (BGP4_PEER_CXT_ID (pPeerentry))
                        = BGP4_DEFERAL_TMR_SET;
                }
                return BGP4_SUCCESS;
            }
        }

    }

    PTR_FETCH2 (u2Withdrawnlen, (pu1Buf));

    if ((u2Withdrawnlen + BGP4_WITHDRAWN_LEN_FIELD_SIZE +
         BGP4_ATTR_LEN_FIELD_SIZE) > (UINT2) u4Msglen)
    {
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Withdrawn length %d is"
                       " greater than the Update Message length\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))), u2Withdrawnlen);
        Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR,
                         BGP4_MALFORMED_ATTR_LIST, NULL, 0);
        return (BGP4_FAILURE);
    }

    PTR_FETCH2 (u2Pathattrlen,
                (pu1Buf + u2Withdrawnlen + BGP4_WITHDRAWN_LEN_FIELD_SIZE));

    if ((u2Withdrawnlen + u2Pathattrlen + BGP4_WITHDRAWN_LEN_FIELD_SIZE +
         BGP4_ATTR_LEN_FIELD_SIZE) > (UINT2) u4Msglen)
    {
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Path attribute length %d is"
                       " greater than the Update message length\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))), u2Pathattrlen);
        Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR,
                         BGP4_MALFORMED_ATTR_LIST, NULL, 0);
        return (BGP4_FAILURE);
    }

    i4NLRILen = u4Msglen - (u2Withdrawnlen + u2Pathattrlen +
                            BGP4_WITHDRAWN_LEN_FIELD_SIZE +
                            BGP4_ATTR_LEN_FIELD_SIZE);
    i4RetVal = Bgp4MhGetWithdrawnRoutes (pPeerentry, u2Withdrawnlen,
                                         (pu1Buf +
                                          BGP4_WITHDRAWN_LEN_FIELD_SIZE),
                                         pTsWithdrawnroutes);
    if (i4RetVal == BGP4_FAILURE)
    {
        Bgp4DshReleaseList (pTsWithdrawnroutes, 0);
        return (BGP4_FAILURE);
    }

    if (u2Pathattrlen > 0)
    {
        i4RetVal = Bgp4MhGetNewRoutes (pPeerentry, u2Pathattrlen,
                                       i4NLRILen,
                                       (pu1Buf + u2Withdrawnlen +
                                        BGP4_WITHDRAWN_LEN_FIELD_SIZE +
                                        BGP4_ATTR_LEN_FIELD_SIZE),
                                       pTsNewroutes, pTsWithdrawnroutes);
        if (i4RetVal == BGP4_FAILURE)
        {
            Bgp4DshReleaseList (pTsNewroutes, 0);
            Bgp4DshReleaseList (pTsWithdrawnroutes, 0);
            return (BGP4_FAILURE);
        }
        else if (i4RetVal == BGP4_IGNORE)
        {
            return BGP4_IGNORE;
        }

        if (Bgp4MhCheckASPathRoutingLoop (pTsNewroutes) == BGP4_FAILURE)
        {
            /* if routing loop is detected, the feasible routes are not to
             * be processed. But these route shall be a replacement route
             * to the existing routes. So process these routes as withdrawn.
             */
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - Routing loop is detected for the routes received."
                           " Hence the routes are processed as withdrawn.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            TMO_SLL_Scan (pTsNewroutes, pLinknode, tLinkNode *)
            {
                BGP4_RT_SET_FLAG (pLinknode->pRouteProfile, BGP4_RT_WITHDRAWN);
            }
            TMO_SLL_Concat (pTsWithdrawnroutes, pTsNewroutes);
        }
    }
    return (BGP4_SUCCESS);
}

/******************************************************************************/
/* Function Name : Bgp4MhFormOpen                                          */
/* Description   : This function forms an BGP OPEN message using the relevant */
/*                 information from the BGP Peer information.                 */
/* Input(s)      : Peer to which the OPEN msg is going to be sent             */
/*                 (pPeerentry),                                             */
/* Output(s)     : Size of the OPEN message (pu4Msglen).                     */
/* Return(s)     : Buffer containing the OPEN message                         */
/******************************************************************************/
UINT1              *
Bgp4MhFormOpen (tBgp4PeerEntry * pPeerentry, UINT4 *pu4Msglen)
{
    UINT1              *pu1Msghdr = NULL;
    UINT1              *pu1Openmsg = NULL;
    INT4                i4RetSts;
    UINT2               u2ASNo;
    UINT1               u1OptParmLen = 0;

    pu1Msghdr = MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);

    if (pu1Msghdr == NULL)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG,
                       BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                       BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tPEER %s: Memory Allocation for OPEN message FAILED!!!\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        gu4BgpDebugCnt[MAX_BGP_BUFNODE_MSGS_SIZING_ID]++;
        return (NULL);
    }

    MEMSET (pu1Msghdr, 0, BGP4_MAX_MSG_LEN);
    pu1Openmsg = pu1Msghdr;

    /* set marker field */
    MEMSET ((VOID *) pu1Openmsg, BGP4_MARKER_BYTE_VAL, BGP4_MARKER_LEN);
    pu1Openmsg += BGP4_MARKER_LEN;

    /* message length */
    PTR_ASSIGN2 ((pu1Openmsg), (BGP4_MSG_COMMON_HDR_LEN));
    pu1Openmsg += sizeof (UINT2);

    /* message type */
    *pu1Openmsg = BGP4_OPEN_MSG;
    pu1Openmsg++;
    if (CAPS_ADVT_SUP (BGP4_PEER_CXT_ID (pPeerentry)) == CAPS_TRUE)
    {
        i4RetSts = CapsFormHandler (pu1Openmsg, pPeerentry);
        u1OptParmLen = *(pu1Openmsg + OPTPARAM_LENGTH_OFFSET);
    }

    /* version */
    *pu1Openmsg = BGP4_VERSION_4;
    pu1Openmsg++;

    /* AS number, Get the data to fill in "my as" field */
    u2ASNo = Bgp4MhGetMyASField (pPeerentry);
    PTR_ASSIGN2 ((pu1Openmsg), (u2ASNo));

    pu1Openmsg += sizeof (UINT2);

    /* hold time */
    PTR_ASSIGN2 ((pu1Openmsg), ((UINT2) BGP4_PEER_HOLD_TIME (pPeerentry)));
    pu1Openmsg += sizeof (UINT2);

    /* bgp identifier */
    PTR_ASSIGN4 ((pu1Openmsg),
                 (BGP4_LOCAL_BGP_ID (BGP4_PEER_CXT_ID (pPeerentry))));
    pu1Openmsg += sizeof (UINT4);

    /* optional parameter len */
    *pu1Openmsg = u1OptParmLen;

    pu1Openmsg += sizeof (UINT1);
    *pu4Msglen = BGP4_OPEN_MSG_MIN_LEN + u1OptParmLen;
    PTR_ASSIGN2 ((pu1Msghdr + BGP4_MARKER_LEN), ((UINT2) *pu4Msglen));

    /* if optional parameters exist,the msg-length needs to be updated 
     * appropriately */
    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                   BGP4_TRC_FLAG | BGP4_PEER_CON_TRC, BGP4_TXQ_TRC,
                   BGP4_MOD_NAME,
                   "\tPEER %s: Sending Open Message - Version %d, "
                   "Local AS %d.\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry))),
                   BGP4_VERSION_4, u2ASNo);
    UNUSED_PARAM (i4RetSts);
    return (pu1Msghdr);
}

/******************************************************************************/
/* Function Name : Bgp4MhFormKeepalive                                     */
/* Description   : This function forms BGP KEEPALIVE msg using the relevant   */
/*                 information from the BGP Peer information.                 */
/* Input(s)      : Peer to which the KEEPALIVE msg is going to be             */
/*                 send(pPeerentry),                                         */
/* Output(s)     : Size of the KEEPALIVE message (pu4Msglen).                */
/* Return(s)     : Buffer containing the KEEPALIVE message                    */
/******************************************************************************/
UINT1              *
Bgp4MhFormKeepalive (tBgp4PeerEntry * pPeerentry, UINT4 *pu4Msglen)
{
    UINT1              *pu1Msghdr = NULL;
    UINT1              *pu1KALmsg = NULL;

    UNUSED_PARAM (pPeerentry);

    *pu4Msglen = BGP4_MSG_COMMON_HDR_LEN;
    pu1Msghdr = MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);

    if (pu1Msghdr == NULL)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_EVENTS_TRC |
                       BGP4_OS_RESOURCE_TRC | BGP4_KEEP_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Memory Allocation for KEEPALIVE "
                       "message FAILED!!!.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        gu4BgpDebugCnt[MAX_BGP_BUFNODE_MSGS_SIZING_ID]++;
        return (NULL);
    }
    MEMSET (pu1Msghdr, 0, BGP4_MAX_MSG_LEN);

    /* WHEN AUTHENTICATION IS ADDED CALL A ROUTINE FROM THIS POINT TO FILL THE 
       PROPER VALUES IN THE MSGs MARKER FIELD */

    pu1KALmsg = pu1Msghdr;
    /* set marker field */
    MEMSET ((VOID *) pu1KALmsg, BGP4_MARKER_BYTE_VAL, BGP4_MARKER_LEN);
    pu1KALmsg += BGP4_MARKER_LEN;

    /* message length */
    PTR_ASSIGN2 ((pu1KALmsg), (BGP4_MSG_COMMON_HDR_LEN));
    pu1KALmsg += sizeof (UINT2);

    /* message type */
    *pu1KALmsg = BGP4_KEEPALIVE_MSG;
    pu1KALmsg++;

    return (pu1Msghdr);
}

/******************************************************************************/
/* Function Name : Bgp4MhFormNotification                                     */
/* Description   : This function forms an BGP NOTIFICATION message            */
/* Input(s)      : Peer to which the NOTIFICATION msg is going to be sent     */
/*                (pPeerentry),                                               */
/*                 Error Code (u1Code),                                       */
/*                 Error SubCode (u1Subcode),                                 */
/*                 Error Data (pu1Data),                                      */
/*                 Length of the Error Data (pu4Msglen).                      */
/* Output(s)     : Size of the NOTIFICATION message (pu4Msglen).              */
/* Return(s)     : Buffer containing the NOTIFICATION message                 */
/******************************************************************************/
UINT1              *
Bgp4MhFormNotification (tBgp4PeerEntry * pPeerentry,
                        UINT1 u1Code,
                        UINT1 u1Subcode, UINT1 *pu1Data, UINT4 *pu4Msglen)
{
    UINT1              *pu1Msghdr = NULL;
    UINT1              *pu1Notifymsg = NULL;
    UINT4               u4Datalen = 0;

    UNUSED_PARAM (pPeerentry);

    u4Datalen = *pu4Msglen;
    *pu4Msglen += BGP4_MSG_COMMON_HDR_LEN + BGP_ERROR_CODE_LENGTH
        + BGP_ERROR_SUBCODE_LENGTH;

    pu1Msghdr = MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);

    if (pu1Msghdr == NULL)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Memory Allocation for NOTIFICATION message "
                       "FAILED!!!.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        (BGP4_AFI_IN_ADDR_PREFIX_INFO
                                         (BGP4_PEER_REMOTE_ADDR_INFO
                                          (pPeerentry)))));
        gu4BgpDebugCnt[MAX_BGP_BUFNODE_MSGS_SIZING_ID]++;
        return (NULL);
    }
    MEMSET (pu1Msghdr, 0, BGP4_MAX_MSG_LEN);
    pu1Notifymsg = pu1Msghdr;

    /* WHEN AUTHENTICATION IS ADDED CALL A ROUTINE FROM THIS POINT TO FILL THE 
       PROPER VALUES IN THE MSGs MARKER FIELD */
    /* set marker field */
    MEMSET ((VOID *) pu1Notifymsg, BGP4_MARKER_BYTE_VAL, BGP4_MARKER_LEN);
    pu1Notifymsg += BGP4_MARKER_LEN;

    /* message length */
    PTR_ASSIGN2 ((pu1Notifymsg), ((UINT2) *pu4Msglen));
    pu1Notifymsg += sizeof (UINT2);

    /* message type */
    *pu1Notifymsg = BGP4_NOTIFICATION_MSG;
    pu1Notifymsg++;

    *pu1Notifymsg = u1Code;
    pu1Notifymsg++;

    *pu1Notifymsg = u1Subcode;
    pu1Notifymsg++;

    if (u4Datalen > 0)
    {
        MEMCPY ((VOID *) pu1Notifymsg, pu1Data, u4Datalen);
        pu1Notifymsg += u4Datalen;
    }
    BGP4_PEER_LAST_ERROR (pPeerentry) = u1Code;
    BGP4_PEER_LAST_ERROR_SUB_CODE (pPeerentry) = u1Subcode;
    return (pu1Msghdr);
}

/******************************************************************************/
/* Function Name : Bgp4MhGetWithdrawnRoutes                                   */
/* Description   : This function checks for the validity of NLRIs and         */
/*                 extracts the Withdrawn routes information from the UPDATE  */
/*                 message receivied.                                         */
/* Input(s)      : Peer which has sent us the UPDATE message (pPeerentry),    */
/*                 Buffer containing the UPDATE message (pu1Buf),             */
/*                 Length of the withdrawn length field (u2Withdrawnlen)      */
/* Output(s)     : List of withdrawn routes (pTsWithdrawnroutes).             */
/* Return(s)     : BGP4_SUCCESS if the operation is successful,               */
/*                 BGP4_FAILURE if it fails.                                  */
/*                 BGP4_RSRC_ALLOC_FAIL, if resource allocation fails.        */
/******************************************************************************/
INT4
Bgp4MhGetWithdrawnRoutes (tBgp4PeerEntry * pPeerentry,
                          UINT2 u2Withdrawnlen,
                          UINT1 *pu1Buf, tTMO_SLL * pTsWithdrawnroutes)
{
    tRouteProfile      *pRtprofile = NULL;
#ifdef L3VPN
    tRouteProfile      *pVpnRtProfile = NULL;
    tLinkNode          *pLknode = NULL;
#endif
    static tRouteProfile TmpRtprofile;
    UINT4               u4Prefix = 0;
    UINT4               u4WithdrawsCnt = 0;
    UINT4               u4Context = 0;
    INT4                i4Bytes;
    INT4                i4ReturnVal = 0;

    u4Context = BGP4_PEER_CXT_ID (pPeerentry);
    while (u2Withdrawnlen > 0)
    {
        MEMSET (&TmpRtprofile, 0, sizeof (tRouteProfile));
        pRtprofile = &TmpRtprofile;
        BGP4_RT_PROTOCOL (pRtprofile) = BGP_ID;
        BGP4_RT_PREFIXLEN (pRtprofile) = *pu1Buf;
        pRtprofile->pBgpCxtNode = Bgp4GetContextEntry (u4Context);
        i4ReturnVal = BGP4_RSRC_ALLOC_FAIL;
        i4Bytes = BGP4_PREFIX_BYTES ((*pu1Buf));
        pu1Buf++;
        u2Withdrawnlen--;
        if (i4Bytes > u2Withdrawnlen)
        {
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Update MSG ERR - The prefix bytes %d is"
                           " greater than the withdrawn length field size\n",
                           Bgp4PrintIpAddr
                           (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                            (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry))),
                           i4Bytes);
            Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR,
                             BGP4_MALFORMED_ATTR_LIST, NULL, 0);
            Bgp4DshRemoveRouteFromList (pTsWithdrawnroutes, pRtprofile);
            return (BGP4_FAILURE);
        }
        BGP4_GET_PREFIX (pu1Buf, &u4Prefix, i4Bytes);

        /* Put the AFI */
        BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                      (BGP4_RT_NET_ADDRESS_INFO (pRtprofile))) =
            BGP4_INET_AFI_IPV4;
        /* Put the Address Length = 4 */
        BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO
            (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
             (BGP4_RT_NET_ADDRESS_INFO (pRtprofile))) = BGP4_IPV4_PREFIX_LEN;
        /* Put the SAFI */
        BGP4_SAFI_IN_NET_ADDRESS_INFO (BGP4_RT_NET_ADDRESS_INFO (pRtprofile))
            = BGP4_INET_SAFI_UNICAST;
        /* Assign the prefix */
        PTR_ASSIGN4 (BGP4_RT_IP_PREFIX (pRtprofile), u4Prefix);

        if ((Bgp4IsValidAddress
             (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
              (BGP4_RT_NET_ADDRESS_INFO (pRtprofile)), BGP4_TRUE)) != BGP4_TRUE)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Update MSG ERR - Invalid NLRI",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR, BGP4_INVALID_NLRI,
                             NULL, 0);
            Bgp4DshRemoveRouteFromList (pTsWithdrawnroutes, pRtprofile);
            return (BGP4_FAILURE);
        }
        BGP4_RT_SET_FLAG (pRtprofile, BGP4_RT_WITHDRAWN);
        u2Withdrawnlen -= (UINT2) i4Bytes;
        u4WithdrawsCnt++;

        pRtprofile =
            Bgp4GetRoute (u4Context, BGP4_RT_NET_ADDRESS_INFO (pRtprofile),
                          BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry), NULL);
        if (pRtprofile != NULL)
        {
            if (BGP4_PEER_CURRENT_PREFIX_LIMIT (pPeerentry) > 0)
            {
                BGP4_PEER_CURRENT_PREFIX_LIMIT (pPeerentry) -= 1;
            }
            if (i4ReturnVal == BGP4_RSRC_ALLOC_FAIL)
            {
                Bgp4RibhProcessWithdRoute (pPeerentry, pRtprofile, NULL);
            }
        }
#ifdef L3VPN
        if ((BGP4_VPN4_PEER_ROLE (pPeerentry) == BGP4_VPN4_CE_PEER) &&
            (pRtprofile != NULL))
        {
            INT4                i4RetVal;

            pLknode = Bgp4MemAllocateLinkNode (sizeof (tLinkNode));

            if (pLknode == NULL)
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Allocate Link Node for Withdrawn list "
                               "FAILS!!!\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))));
                return (BGP4_RSRC_ALLOC_FAIL);
            }
            pVpnRtProfile = Bgp4DuplicateRouteProfile (pRtprofile);

            if (pVpnRtProfile == NULL)
            {
                Bgp4DshReleaseLinkNode (pLknode);
                return (BGP4_RSRC_ALLOC_FAIL);
            }

            i4RetVal = Bgp4Vpn4ConvertRtToVpn4Route (pVpnRtProfile,
                                                     BGP4_PEER_CXT_ID
                                                     (pPeerentry));

            BGP4_RT_SET_FLAG (pVpnRtProfile, BGP4_RT_WITHDRAWN);

            BGP4_LINK_PROFILE_TO_NODE (pVpnRtProfile, pLknode);

            TMO_SLL_Add (pTsWithdrawnroutes, &pLknode->TSNext);

            if (i4RetVal == BGP4_FAILURE)
            {
                Bgp4DshRemoveRouteFromList (pTsWithdrawnroutes, pVpnRtProfile);
                return (BGP4_RSRC_ALLOC_FAIL);
            }
            u4WithdrawsCnt++;
        }
#endif

        if (pRtprofile != NULL)
        {
            BGP4_TRC_ARG4 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_UPD_MSG_TRC | BGP4_RX_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Received Withdrawn Route - "
                           "Prefix: %s, PrefixLen: %s, Nexthop : %s\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtprofile),
                                            BGP4_RT_AFI_INFO (pRtprofile)),
                           Bgp4PrintIpMask ((UINT1)
                                            BGP4_RT_PREFIXLEN (pRtprofile),
                                            BGP4_RT_AFI_INFO (pRtprofile)),
                           Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP
                                            (pRtprofile),
                                            BGP4_RT_IMMEDIATE_NEXTHOP_AFI_INFO
                                            (pRtprofile)));
        }

    }
    BGP4_PEER_IPV4_WITHDRAWS_RCVD_CNT (pPeerentry) += u4WithdrawsCnt;
    return (BGP4_SUCCESS);
}

/******************************************************************************/
/* Function Name : Bgp4MhGetNewRoutes                                         */
/* Description   : This function checks for the validity of NLRIs,attributes  */
/*                 and extracts the Feasible routes info from the UPDATE      */
/*                 message receivied.                                         */
/* Input(s)      : Peer which has sent us the UPDATE message (pPeerentry),    */
/*                 Buffer containing the UPDATE message (pu1Buf),             */
/*                 Length of the Path Attribute field (i4Pathattrlen),        */
/*                 Length of the NLRI field (i4NLRIlen)                       */
/* Output(s)     : List of feasible routes (pTsNewroutes).                    */
/* Return(s)     : BGP4_SUCCESS if the operation is successful,               */
/*                 BGP4_FAILURE if it fails.                                  */
/*                 BGP4_RSRC_ALLOC_FAIL, if resource allocation fails.        */
/******************************************************************************/
INT4
Bgp4MhGetNewRoutes (tBgp4PeerEntry * pPeerentry,
                    INT4 i4Pathattrlen,
                    INT4 i4NLRIlen, UINT1 *pu1Buf, tTMO_SLL * pTsNewroutes,
                    tTMO_SLL * pTsWithdrawnroutes)
{
    tBgp4Info          *pBGP4Info = NULL;
    tBgp4Info          *pMpeBgpInfo = NULL;
    tAsPath            *pTsAspath = NULL;
    tRouteProfile      *pRtprofile = NULL;
    tLinkNode          *pLinkNode = NULL;
    tLinkNode          *pLknode = NULL;
    tRouteProfile      *pAltRtProfile = NULL;
    UINT4               u4PrefixesCnt = 0;
    UINT4               u4Prefix = 0;
    UINT4               u4RtProfileCnt = 0;
    UINT4               u4MaxRoutes;
    UINT4               u4Context = 0;
    INT4                i4Retval = 0;
    INT4                i4Bytes;
    UINT4               u4ExtPeerAs = 0;
    UINT4               u4Flag = 0;
    UINT1               u1NlriField = BGP4_FALSE;
    UINT1               u1MpNlriField = BGP4_FALSE;
    UINT1               u1Flag;
    UINT1               u1Attr;
    UINT1               u1PrefixLen;
    UINT1               u1SupportSafi = 0;
    UINT2               u2SupportAfi = 0;
    INT4                i4Supported = BGP4_FALSE;
    tSupCapsInfo       *pSpkrSupCap = NULL;

    pBGP4Info = Bgp4MemAllocateBgp4info (sizeof (tBgp4Info));
    if (pBGP4Info == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "Memory Allocation for Route's BGP-Info FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_ROUTE_INFO_ENTRIES_SIZING_ID]++;
        return (BGP4_RSRC_ALLOC_FAIL);
    }
    BGP4_INFO_REF_COUNT (pBGP4Info)++;

    u4Context = BGP4_PEER_CXT_ID (pPeerentry);
    while (i4Pathattrlen > 0)
    {
        u1Flag = *pu1Buf;
        u1Attr = *(pu1Buf + BGP4_ATYPE_FLAGS_LEN);
        /* RFC states that - "The same attribute cannot appear more
         * than once in the path attribute field of a particular
         * Update message". Check for this and if so then
         * reject those packets. */
        if ((u1Attr > 0) && (u1Attr <= BGP4_ATTR_AGGREGATOR_FOUR))
        {

            if ((BGP4_INFO_ATTR_FLAG (pBGP4Info) & au4Bgp4AttrMask[u1Attr]) > 0)
            {
                BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s - Update MSG ERR - The attribute %s"
                               " is repeated more than once.\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))),
                               Bgp4PrintCodeName (u1Attr,
                                                  BGP4_PATH_ATTR_CODE_NAME));
                Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR,
                                 BGP4_MALFORMED_ATTR_LIST, NULL, 0);
                Bgp4DshReleaseBgpInfo (pBGP4Info);
                return (BGP4_FAILURE);
            }
        }

        i4Retval = Bgp4MsghCheckFlag (u1Flag, u1Attr, pu1Buf);
        if (i4Retval != BGP4_FAILURE)
        {
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - Update MSG ERR - Attribute %s Flag "
                           "error\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))),
                           Bgp4PrintCodeName (u1Attr,
                                              BGP4_PATH_ATTR_CODE_NAME));
            Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR,
                             BGP4_ATTR_FLAG_ERR, pu1Buf, (UINT4) i4Retval);
            Bgp4DshReleaseBgpInfo (pBGP4Info);
            return (BGP4_FAILURE);
        }

        switch (u1Attr)
        {
            case BGP4_ATTR_ORIGIN:
                i4Retval = Bgp4MsghProcessOrigin (pPeerentry,
                                                  pBGP4Info, pu1Buf,
                                                  i4Pathattrlen);
                break;

            case BGP4_ATTR_PATH:
                i4Retval = Bgp4MsghProcessPath (pPeerentry, pBGP4Info, pu1Buf,
                                                i4Pathattrlen);
                break;

            case BGP4_ATTR_PATH_FOUR:
                i4Retval = Bgp4MsghProcessPathFour (pPeerentry, pBGP4Info,
                                                    pu1Buf, i4Pathattrlen);
                break;

            case BGP4_ATTR_NEXT_HOP:
                i4Retval = Bgp4MsghProcessNexthop (pPeerentry,
                                                   pBGP4Info, pu1Buf,
                                                   i4Pathattrlen);
                break;

            case BGP4_ATTR_MED:
                i4Retval = Bgp4MsghProcessMed (pPeerentry, pBGP4Info, pu1Buf,
                                               i4Pathattrlen);
                break;

            case BGP4_ATTR_LOCAL_PREF:
                i4Retval =
                    Bgp4MsghProcessLocalpref (pPeerentry, pBGP4Info, pu1Buf,
                                              i4Pathattrlen);
                break;

            case BGP4_ATTR_ATOMIC_AGGR:
                i4Retval = Bgp4MsghProcessAtomicAggr (pPeerentry,
                                                      pBGP4Info, pu1Buf,
                                                      i4Pathattrlen);
                break;

            case BGP4_ATTR_AGGREGATOR:
                i4Retval = Bgp4MsghProcessAggregator (pPeerentry,
                                                      pBGP4Info, pu1Buf,
                                                      i4Pathattrlen);
                break;

            case BGP4_ATTR_AGGREGATOR_FOUR:
                i4Retval = Bgp4MsghProcessAggregatorFour (pPeerentry,
                                                          pBGP4Info,
                                                          pu1Buf,
                                                          i4Pathattrlen);
                break;

            case BGP4_ATTR_COMM:
                i4Retval = Bgp4MsghProcessCommunity (pPeerentry,
                                                     pBGP4Info, pu1Buf,
                                                     i4Pathattrlen);
                break;

            case BGP4_ATTR_ECOMM:
                i4Retval = Bgp4MsghProcessExtComm (pPeerentry,
                                                   pBGP4Info, pu1Buf,
                                                   i4Pathattrlen);
                break;

            case BGP4_ATTR_ORIG_ID:
                i4Retval = Bgp4MsghProcessOrigId (pPeerentry,
                                                  pBGP4Info, pu1Buf,
                                                  i4Pathattrlen);
                break;

            case BGP4_ATTR_CLUS_LIST:
                i4Retval = Bgp4MsghProcessClusterList (pPeerentry,
                                                       pBGP4Info, pu1Buf,
                                                       i4Pathattrlen);
                break;

            case BGP4_ATTR_MP_REACH_NLRI:
                i4Retval = Bgp4MpeProcessAttribute (pPeerentry,
                                                    pu1Buf, i4Pathattrlen,
                                                    pTsNewroutes, &u4Flag);
                break;
            case BGP4_ATTR_MP_UNREACH_NLRI:
                i4Retval = Bgp4MpeProcessAttribute (pPeerentry,
                                                    pu1Buf, i4Pathattrlen,
                                                    pTsWithdrawnroutes, NULL);
                break;

            default:
                BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG,
                               BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC |
                               BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Unknown Attribute Code = %d in update msg\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))), u1Attr);
                i4Retval =
                    Bgp4MsghProcessUnknown (pPeerentry, pBGP4Info, pu1Buf,
                                            i4Pathattrlen);
                break;
        }
        if (i4Retval <= 0)
        {
            /* Some error detected while processing the update message.
             * Process the errors as required. If BGP4_FAILURE is returned
             * then the connection with the peer will be resetted. */
            switch (i4Retval)
            {
                case BGP4_RSRC_ALLOC_FAIL:
                    /* MEM-ALLOC Failed. Release the New Feasible route
                     * list because this list might have Partially filled
                     * feasible route. However process any route if present
                     * in withdrawn route list. */
                    Bgp4DshReleaseList (pTsNewroutes, 0);
                    Bgp4DshReleaseBgpInfo (pBGP4Info);
                    return BGP4_SUCCESS;
                case BGP4_RECEIVED_CONFED_SEGMENT_FROM_EXT_PEER:
                case BGP4_SEMANTIC_INVALID_NEXTHOP:
                case BGP4_INVALID_CLUS_LIST:
                case BGP4_INVALID_ORIG_ID:
                case BGP4_PEER_NETWORK_ADDR_NOT_CONF:
                case BGP4_PEER_LCL_ADDR_NOT_AVAILABLE:
                    Bgp4DshReleaseBgpInfo (pBGP4Info);
                    return BGP4_SUCCESS;
                default:
                    Bgp4DshReleaseBgpInfo (pBGP4Info);
                    return (i4Retval);
            }
        }
        else
        {
            i4Pathattrlen -= i4Retval;
            pu1Buf += i4Retval;
        }
    }
    /* Weight of the recvd routes should always be zero */
    BGP4_INFO_WEIGHT (pBGP4Info) = 0;
    if (TMO_SLL_Count (pTsNewroutes) > 0)
    {
        /* Some <AFI, SAFI> routes received through MP_REACH_NLRI 
         * path attribute
         */
        u1MpNlriField = BGP4_TRUE;
        pLinkNode = (tLinkNode *) TMO_SLL_First (pTsNewroutes);
        pMpeBgpInfo = BGP4_RT_BGP_INFO (pLinkNode->pRouteProfile);
        Bgp4CopyInfoToMpeInfo (pMpeBgpInfo, pBGP4Info);
    }

    u4MaxRoutes =
        FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].u4PreAllocatedUnits;
    u4RtProfileCnt =
        BGP4_RIB_ROUTE_CNT (BGP4_PEER_CXT_ID (pPeerentry)) +
        (BGP4_NONSYNC_LIST (BGP4_PEER_CXT_ID (pPeerentry)))->u4_Count +
        TMO_SLL_Count (pTsNewroutes);

    while (i4NLRIlen > 0)
    {
        /*Get the supported capability for the peer. If v4 capability is disabled, silenltly ignore the update */

        TMO_SLL_Scan (BGP4_PEER_SUP_CAPS_LIST (pPeerentry), pSpkrSupCap,
                      tSupCapsInfo *)
        {
            if (pSpkrSupCap->SupCapability.u1CapCode != CAP_CODE_MP_EXTN)
            {
                continue;
            }
            if (pSpkrSupCap->SupCapability.u1CapLength != CAP_MP_CAP_LENGTH)
            {
                continue;
            }

            /* Compare the <AFI, SAFI> */
            PTR_FETCH2 (u2SupportAfi, pSpkrSupCap->SupCapability.au1CapValue);
            u1SupportSafi = *(pSpkrSupCap->SupCapability.au1CapValue +
                              BGP4_MPE_ADDRESS_FAMILY_LEN +
                              BGP4_MPE_MP_CAP_RESERVED_FIELD_LEN);

            if ((u2SupportAfi != BGP4_INET_AFI_IPV4)
                || (u1SupportSafi != BGP4_INET_SAFI_UNICAST))
            {
                continue;
            }
            i4Supported = BGP4_TRUE;

        }
        if (i4Supported != BGP4_TRUE)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tPEER %s - IPv4 Capability is not supported by the peer."
                           " Hence silently ignore the update message\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            /* Neighbor is not activated. Just return */
            Bgp4DshReleaseBgpInfo (pBGP4Info);
            return (BGP4_IGNORE);
        }

        u1NlriField = BGP4_TRUE;
        u1PrefixLen = (*pu1Buf);
        i4Bytes = BGP4_PREFIX_BYTES (*pu1Buf);
        pu1Buf++;
        i4NLRIlen--;
        if ((i4Bytes > i4NLRIlen) || (i4Bytes > BGP4_MAX_PREFIX_BYTE_LEN))
        {
            BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - Update MSG ERR - The prefix byte length %d "
                           "is greater than NLRI length %d or maximum prefix byte length\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))), i4Bytes,
                           i4NLRIlen);
            Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR, BGP4_INVALID_NLRI,
                             NULL, 0);
            Bgp4DshReleaseBgpInfo (pBGP4Info);
            return (BGP4_FAILURE);
        }
        BGP4_GET_PREFIX (pu1Buf, &u4Prefix, i4Bytes);

        /* INVESTIGATE THE ORDER */
        if (u4RtProfileCnt >= u4MaxRoutes)
        {
            tNetAddress         IpAddress;

            Bgp4InitNetAddressStruct (&(IpAddress),
                                      BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);
            BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (IpAddress) = u1PrefixLen;
            PTR_ASSIGN4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                         (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                          (IpAddress)), (u4Prefix));
            pAltRtProfile =
                Bgp4GetRoute (u4Context, IpAddress,
                              BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry), NULL);
        }
        if ((u4RtProfileCnt >= u4MaxRoutes) && (pAltRtProfile == NULL))
        {
            i4NLRIlen -= i4Bytes;
            BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - No of Routes %d received exceeding maximum routes %d."
                           " Discarding the route received in update message\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))), u4RtProfileCnt,
                           u4MaxRoutes);
            continue;
        }
        /* RFC 4271 Update */
        if (BGP4_PEER_CURRENT_PREFIX_LIMIT (pPeerentry) >=
            BGP4_PEER_PREFIX_UPPER_LIMIT (pPeerentry))
        {
            BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - No of Routes %d received for the peer exceeding the "
                           "maximum prefix limit of the peer %d. Discarding the route received\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))),
                           BGP4_PEER_CURRENT_PREFIX_LIMIT (pPeerentry),
                           BGP4_PEER_PREFIX_UPPER_LIMIT (pPeerentry));
            break;
        }

        pLknode = Bgp4MemAllocateLinkNode (sizeof (tLinkNode));
        if (pLknode == NULL)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Allocate Link Node for Feasible "
                           "Route FAILS!!!\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4DshReleaseBgpInfo (pBGP4Info);
            return (BGP4_RSRC_ALLOC_FAIL);
        }
        pRtprofile = Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
        if (pRtprofile == NULL)
        {
            Bgp4DshReleaseLinkNode (pLknode);
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Allocate Route Profile for Feasible "
                           "route FAILS!!!\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4DshReleaseBgpInfo (pBGP4Info);
            return (BGP4_RSRC_ALLOC_FAIL);
        }
        BGP4_LINK_INFO_TO_PROFILE (pBGP4Info, pRtprofile);
        BGP4_LINK_PROFILE_TO_NODE (pRtprofile, pLknode);
        u4RtProfileCnt++;
        TMO_SLL_Add (pTsNewroutes, &pLknode->TSNext);
        BGP4_RT_PROTOCOL (pRtprofile) = BGP_ID;
        BGP4_RT_PREFIXLEN (pRtprofile) = u1PrefixLen;
        BGP4_RT_PEER_ENTRY (pRtprofile) = pPeerentry;

        /* Put the AFI */
        BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                      (BGP4_RT_NET_ADDRESS_INFO (pRtprofile))) =
            BGP4_INET_AFI_IPV4;
        /* Put the Address Length = 4 */
        BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO
            (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
             (BGP4_RT_NET_ADDRESS_INFO (pRtprofile))) = BGP4_IPV4_PREFIX_LEN;
        /* Put the SAFI */
        BGP4_SAFI_IN_NET_ADDRESS_INFO (BGP4_RT_NET_ADDRESS_INFO (pRtprofile))
            = BGP4_INET_SAFI_UNICAST;
        PTR_ASSIGN4 (BGP4_RT_IP_PREFIX (pRtprofile), (u4Prefix));

        if ((Bgp4IsValidAddress
             (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
              (BGP4_RT_NET_ADDRESS_INFO (pRtprofile)), BGP4_TRUE)) != BGP4_TRUE)
        {
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - Update MSG ERR - Invalid Prefix address %s\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtprofile),
                                            BGP4_RT_AFI_INFO (pRtprofile)));
            Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR, BGP4_INVALID_NLRI,
                             NULL, 0);
            Bgp4DshReleaseBgpInfo (pBGP4Info);
            return (BGP4_FAILURE);
        }
        i4NLRIlen -= i4Bytes;
        u4PrefixesCnt++;
        /* RFC 4271 Update */
        BGP4_PEER_CURRENT_PREFIX_LIMIT (pPeerentry) += 1;
    }

    u1Attr = 0;

    /* If an UPDATE message carrying only MP_UNREACH_NLRI attribute need
     * not carry any other path attributes.
     * So all the mandatory and any other path attributes presence should
     * be checked either UPDATE contains MP_REACH_NLRI or normal NLRI field.
     */

    while ((u1Attr == 0) && ((u1NlriField == BGP4_TRUE) ||
                             (u4Flag & BGP4_ATTR_MP_REACH_NLRI_MASK)))
    {

        if ((BGP4_INFO_ATTR_FLAG (pBGP4Info) & BGP4_ATTR_ORIGIN_MASK) !=
            BGP4_ATTR_ORIGIN_MASK)
        {
            u1Attr = BGP4_ATTR_ORIGIN;
            break;
        }

        if ((BGP4_INFO_ATTR_FLAG (pBGP4Info) & BGP4_ATTR_PATH_MASK) !=
            BGP4_ATTR_PATH_MASK)
        {
            u1Attr = BGP4_ATTR_PATH;
            break;
        }

        if ((u1NlriField == BGP4_TRUE) &&
            ((BGP4_INFO_ATTR_FLAG (pBGP4Info) & BGP4_ATTR_NEXT_HOP_MASK) !=
             BGP4_ATTR_NEXT_HOP_MASK))
        {
            u1Attr = BGP4_ATTR_NEXT_HOP;
            break;
        }

        /* If an UPDATE message containing MP_REACH_NLRI attribute is 
         * received over an IBGP exchanges,it MUST contain LOCAL_PREF attribute
         */
        if ((u1MpNlriField == BGP4_TRUE) &&
            (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerentry), pPeerentry) ==
             BGP4_INTERNAL_PEER)
            && ((BGP4_INFO_ATTR_FLAG (pBGP4Info) & BGP4_ATTR_LOCAL_PREF_MASK) !=
                BGP4_ATTR_LOCAL_PREF_MASK))
        {
            u1Attr = BGP4_ATTR_LOCAL_PREF;
            break;
        }

        /* No further check */
        break;
    }

    if (u1Attr != 0)
    {
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Mandatory attribute %s is "
                       "missing in update message\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))),
                       Bgp4PrintCodeName (u1Attr, BGP4_PATH_ATTR_CODE_NAME));
        Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR,
                         BGP4_MISSING_WELLKNOWN_ATTR, (UINT1 *) &u1Attr,
                         BGP4_ONE_BYTE);
        Bgp4DshReleaseBgpInfo (pBGP4Info);
        return (BGP4_FAILURE);
    }

    /* If an UPDATE message containing MP_REACH_NLRI attribute is received
     * over an EBGP exchanges, the left most AS_PATH attribute should be equal
     * to the AS number of the peer that has sent this message.
     */
    if ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerentry), pPeerentry) ==
         BGP4_EXTERNAL_PEER) && (u1MpNlriField == BGP4_TRUE))
    {
        pTsAspath = (tAsPath *) TMO_SLL_First (BGP4_INFO_ASPATH (pMpeBgpInfo));
        if (pTsAspath == NULL)
        {
            /* Error handling. We have received an empty AS-PATH from
             * external peer.
             */
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - Update MSG ERR - Received an empty AS path "
                           "from external peer\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR,
                             BGP4_MALFORMED_AS_PATH, NULL, 0);
            Bgp4DshReleaseBgpInfo (pBGP4Info);
            return BGP4_FAILURE;
        }
        PTR_FETCH4 (u4ExtPeerAs, (BGP4_ASPATH_NOS (pTsAspath)));
        /* since AS number is stored as it comes in N/W order, we have to
         * convert it to Host order before checking against the peer AS
         * number
         */

        /* Before checking against the peer AS number, check for the 
         * 4-byte ASN support capability. If the capability is not supported
         * and the first AS number in AS path is greater than 65535, convert it 
         * to AS_TRANS */

        if ((BGP4_FOUR_BYTE_ASN_SUPPORT (u4Context) ==
             BGP4_DISABLE_4BYTE_ASN_SUPPORT) &&
            (u4ExtPeerAs > BGP4_MAX_TWO_BYTE_AS))
        {
            u4ExtPeerAs = BGP4_AS_TRANS;
        }

        if (u4ExtPeerAs != BGP4_PEER_ASNO (pPeerentry))
        {
            BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - Update MSG ERR - The external AS number %d "
                           "is not same as peer AS number %d\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))),
                           u4ExtPeerAs, BGP4_PEER_ASNO (pPeerentry));
            Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR,
                             BGP4_MALFORMED_AS_PATH, NULL, 0);
            Bgp4DshReleaseBgpInfo (pBGP4Info);
            return BGP4_FAILURE;
        }
    }
    if (u1MpNlriField == BGP4_TRUE)
    {
        BGP4_TRC_ARG5 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_RX_TRC |
                       BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                       "\tPEER %s - Received Update message with attributes : "
                       "nexthop %s metric %d local preference %d origin %s\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))),
                       Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP (pBGP4Info),
                                        BGP4_INFO_AFI (pBGP4Info)),
                       BGP4_INFO_RCVD_MED (pBGP4Info),
                       BGP4_INFO_RCVD_LOCAL_PREF (pBGP4Info),
                       Bgp4PrintCodeName (BGP4_INFO_ORIGIN (pBGP4Info),
                                          BGP4_ORIGIN_CODE_NAME));
    }

    BGP4_PEER_IPV4_PREFIX_RCVD_CNT (pPeerentry) += u4PrefixesCnt;
    Bgp4DshReleaseBgpInfo (pBGP4Info);
    return (BGP4_SUCCESS);
}

/******************************************************************************/
/* Function Name : Bgp4MsghProcessOrigin                                      */
/* Description   : This function checks the validity of the Origin attribute  */
/*                 received in the UPDATE message and stores the same in the  */
/*                 given BGP4_INFO structure.                                 */
/* Input(s)      : Peer which has sent us the UPDATE message (pPeerentry),    */
/*                 Buffer containing the UPDATE message (pu1Buf),             */
/* Output(s)     : Updated BGP4 Information structure (pBGP4Info)             */
/* Return(s)     : Number of bytes processed in the buffer.                   */
/******************************************************************************/
INT4
Bgp4MsghProcessOrigin (tBgp4PeerEntry * pPeerentry,
                       tBgp4Info * pBGP4Info, UINT1 *pu1Buf, INT4 i4RemPattrLen)
{
    INT4                i4Len;
    INT4                i4AttrFieldLen;

    i4AttrFieldLen = BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN;

    i4Len = *(UINT1 *) (pu1Buf + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN);

    if ((i4Len != BGP4_ATTR_ORIGIN_LEN) ||
        ((i4Len + i4AttrFieldLen + BGP4_ATTR_ORIGIN_LEN) > i4RemPattrLen))
    {
        i4Len += i4AttrFieldLen + BGP4_ATTR_ORIGIN_LEN;
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Origin Attr Length "
                       "Error\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR, BGP4_ATTR_LEN_ERR,
                         pu1Buf, i4Len);
        return (BGP4_FAILURE);
    }

    BGP4_INFO_ORIGIN (pBGP4Info) = *(pu1Buf + i4AttrFieldLen +
                                     BGP4_ATTR_ORIGIN_LEN);

    if ((BGP4_INFO_ORIGIN (pBGP4Info) != BGP4_ATTR_ORIGIN_IGP)
        && (BGP4_INFO_ORIGIN (pBGP4Info) != BGP4_ATTR_ORIGIN_EGP)
        && (BGP4_INFO_ORIGIN (pBGP4Info) != BGP4_ATTR_ORIGIN_INCOMPLETE))
    {
        i4Len += i4AttrFieldLen + BGP4_ATTR_ORIGIN_LEN;
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Invalid ORIGIN info %d\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))),
                       BGP4_INFO_ORIGIN (pBGP4Info));
        Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR, BGP4_INVALID_ORIGIN,
                         pu1Buf, i4Len);
        return (BGP4_FAILURE);
    }

    BGP4_INFO_ATTR_FLAG (pBGP4Info) |= BGP4_ATTR_ORIGIN_MASK;
    return (i4Len + i4AttrFieldLen + BGP4_ATTR_ORIGIN_LEN);
}

/******************************************************************************/
/* Function Name : Bgp4MsghProcessPath                                        */
/* Description   : This function checks the validity of the AS_PATH attribute */
/*                 receivied in the UPDATE message and stores the same in the */
/*                 given BGP4_INFO structure.                                 */
/* Input(s)      : Peer which has sent us the UPDATE message (pPeerentry),    */
/*                 Buffer containing the UPDATE message (pu1Buf),             */
/* Output(s)     : Updated BGP4 Information structure (pBGP4Info)             */
/* Return(s)     : Number of bytes processed in the buffer.                   */
/******************************************************************************/
INT4
Bgp4MsghProcessPath (tBgp4PeerEntry * pPeerentry,
                     tBgp4Info * pBGP4Info, UINT1 *pu1Buf, INT4 i4RemPattrLen)
{
    tAsPath            *pASPath = NULL;
    tAsPath            *pLocalASPath = NULL;
    UINT4              *pu4ASNo = NULL;
    UINT4               u4PeerType = 0;
    UINT4               u4ASPathFlag = FALSE;
    INT4                i4Attrlen;
    INT4                i4AttrValFieldLen;
    INT4                i4SpeakerPeerCap;
    INT2                i2SegLen;
    INT2                i2AsLen;
    INT2                i2Len;
    INT2                i2Len1 = 0;
    INT2                i2NumAs;
    INT2                i2AsOffset = BGP4_AS_LENGTH;
    INT2                i2TempNumAs;
    UINT4               u4AsnNo = 0;
    UINT4               u4LocalAs = 0;
    UINT1               u1LocalAsPeer = 0;
    u4PeerType = BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerentry), pPeerentry);

    if (BGP4_PEER_LOCAL_AS_CFG (pPeerentry) == BGP4_TRUE)
    {
        u1LocalAsPeer = BGP4_PEER_LCL_AS;
    }
    if (((*pu1Buf) & BGP4_EXT_LEN_FLAG_MASK) == BGP4_EXT_LEN_FLAG_MASK)
    {
        PTR_FETCH2 (i2Len,
                    (pu1Buf + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN));
        i4AttrValFieldLen = BGP4_EXTENDED_ATTR_LEN_SIZE;

        pu1Buf +=
            BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN + i4AttrValFieldLen;
        i4Attrlen =
            i2Len + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            i4AttrValFieldLen;
    }
    else
    {
        i4AttrValFieldLen = sizeof (UINT1);
        i2Len =
            *(UINT1 *) (pu1Buf + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN);
        pu1Buf +=
            BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN + i4AttrValFieldLen;
        i4Attrlen =
            i2Len + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            i4AttrValFieldLen;
    }

    if (i4Attrlen > i4RemPattrLen)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - AS Path Attr Length "
                       "Error\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR, BGP4_ATTR_LEN_ERR,
                         pu1Buf, i4Attrlen);
        return (BGP4_FAILURE);
    }

    /* If the Speaker & the peer are 
       New BGP Speakers i.e.NEW->NEW -----> AS_PATH contains all 4 byte ASNs 
       For all other cases, i.e NEW->OLD, OLD->OLD, OLD->NEW 
       -----> AS_PATH contains all 2 byte ASNs */
    i4SpeakerPeerCap = Bgp4GetSpeakerPeer4ByteAsnCapability (pPeerentry);
    if (i4SpeakerPeerCap == BGP4_4BYTE_ASN_SPEAKER_AND_PEER)
    {
        i2AsLen = BGP4_AS4_LENGTH;    /* AS Length = 4 */
        i2SegLen = BGP4_AS4_SEG_LEN;    /* Max Seg Length = 4*10 = 40 */
    }
    else
    {
        i2AsLen = BGP4_AS_LENGTH;    /* AS Length = 2 */
        i2SegLen = BGP4_AS_SEG_LEN;    /*Max Seg Length = 2*10 = 20 */
    }

    while (i2Len > 0)
    {
        pASPath = Bgp4MemGetASNode (sizeof (tAsPath));
        if (pASPath == NULL)
        {
            BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                      BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for Feasible Route's AS Node "
                      "FAILED!!!\n");
            gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
            return (BGP4_RSRC_ALLOC_FAIL);
        }
        TMO_SLL_Add (BGP4_INFO_ASPATH (pBGP4Info), &pASPath->sllNode);

        /* accept confed_set/sequence also */
        if ((*pu1Buf != BGP4_ATTR_PATH_SET) &&
            (*pu1Buf != BGP4_ATTR_PATH_SEQUENCE) &&
            (*pu1Buf != BGP4_ATTR_PATH_CONFED_SET) &&
            (*pu1Buf != BGP4_ATTR_PATH_CONFED_SEQUENCE))
        {
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - Update MSG ERR - Malformed AS Path %d\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))), *pu1Buf);
            Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR,
                             BGP4_MALFORMED_AS_PATH, NULL, 0);
            TMO_SLL_Delete (BGP4_INFO_ASPATH (pBGP4Info), &pASPath->sllNode);
            Bgp4MemReleaseASNode (pASPath);
            return (BGP4_FAILURE);
        }
        /* ignore the route if received from extenal peer (located 
         * outside the confed), and contains confed path segment 
         */
        if ((*pu1Buf == BGP4_ATTR_PATH_CONFED_SET) ||
            (*pu1Buf == BGP4_ATTR_PATH_CONFED_SEQUENCE))
        {
            if ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerentry), pPeerentry)
                 == BGP4_EXTERNAL_PEER)
                && (BGP4_CONFED_PEER_STATUS (pPeerentry) == BGP4_FALSE))
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s - Update MSG ERR - The external peer contains"
                               " confed path segment\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))));
                TMO_SLL_Delete (BGP4_INFO_ASPATH (pBGP4Info),
                                &pASPath->sllNode);
                Bgp4MemReleaseASNode (pASPath);
                return (BGP4_RECEIVED_CONFED_SEGMENT_FROM_EXT_PEER);
            }
        }

        BGP4_ASPATH_TYPE (pASPath) = *pu1Buf;
        pu1Buf++;
        if (u1LocalAsPeer == BGP4_PEER_LCL_AS)
        {
            BGP4_ASPATH_LEN (pASPath) = *pu1Buf + BGP4_MIN_AS_IN_SEG;
        }
        else
        {
            BGP4_ASPATH_LEN (pASPath) = *pu1Buf;
        }
        pu1Buf++;
        if (u1LocalAsPeer == BGP4_PEER_LCL_AS)
        {
            i2Len1 =
                BGP4_TWO_BYTE +
                ((BGP4_ASPATH_LEN (pASPath) -
                  BGP4_MIN_AS_IN_SEG) * BGP4_TWO_BYTE);
        }
        else
        {
            i2Len1 = BGP4_TWO_BYTE + (BGP4_ASPATH_LEN (pASPath) * i2AsLen);
        }
        if (i2Len < i2Len1)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG,
                           BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - Zero As number recieved in AS Path attribute!!!\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            TMO_SLL_Delete (BGP4_INFO_ASPATH (pBGP4Info), &pASPath->sllNode);
            Bgp4MemReleaseASNode (pASPath);
            Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR,
                             BGP4_MALFORMED_AS_PATH, pu1Buf, i4Attrlen);
            return BGP4_FAILURE;
        }

        if (i2SegLen < (BGP4_ASPATH_LEN (pASPath) * i2AsLen))
        {
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Update MSG ERR - Size of au1ASSegs is less %d\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))), i2SegLen);
            TMO_SLL_Delete (BGP4_INFO_ASPATH (pBGP4Info), &pASPath->sllNode);
            Bgp4MemReleaseASNode (pASPath);
            return BGP4_FAILURE;

        }
        if (i4SpeakerPeerCap == BGP4_4BYTE_ASN_SPEAKER_AND_PEER)
        {
            /* If speaker & peer are NEW->NEW, memcopy all the 4 byte asns 
               into Path List */
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : 4 Byte ASN Capability is supported by peer and speaker\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            MEMSET (BGP4_ASPATH_NOS (pASPath), 0, i2SegLen);
/* when local-as is configured for that neighbour append both remote-as and local-as*/
            if (u1LocalAsPeer == BGP4_PEER_LCL_AS)
            {
                u4LocalAs = BGP4_PEER_LOCAL_AS (pPeerentry);
                pLocalASPath = Bgp4MemGetASNode (sizeof (tAsPath));
                if (pLocalASPath == NULL)
                {
                    BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                              BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                              BGP4_MOD_NAME,
                              "\tMemory Allocation for Feasible Route's AS Node "
                              "FAILED!!!\n");
                    gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
                    return (BGP4_RSRC_ALLOC_FAIL);
                }
                BGP4_ASPATH_LEN (pLocalASPath) = BGP4_MIN_AS_IN_SEG;
                PTR_ASSIGN4 (BGP4_ASPATH_NOS (pLocalASPath), u4LocalAs);
                MEMCPY (BGP4_ASPATH_NOS (pASPath),
                        BGP4_ASPATH_NOS (pLocalASPath),
                        BGP4_ASPATH_LEN (pLocalASPath) * i2AsLen);
                MEMCPY ((BGP4_ASPATH_NOS (pASPath) + i2AsLen), pu1Buf,
                        BGP4_ASPATH_LEN (pASPath) * i2AsLen);

                pu1Buf +=
                    ((BGP4_ASPATH_LEN (pASPath) * i2AsLen) -
                     BGP4_ASPATH_LEN (pLocalASPath) * i2AsLen);
            }
            else
            {

                MEMCPY (BGP4_ASPATH_NOS (pASPath), pu1Buf,
                        BGP4_ASPATH_LEN (pASPath) * i2AsLen);
                pu1Buf += (BGP4_ASPATH_LEN (pASPath) * i2AsLen);
            }
            for (i2NumAs = 0; i2NumAs < BGP4_ASPATH_LEN (pASPath); i2NumAs++)
            {
                if (MEMCMP
                    (&u4AsnNo,
                     (BGP4_ASPATH_NOS (pASPath) + (i2NumAs * i2AsLen)),
                     i2AsLen) == 0)
                {
                    /* zero AS number recieved in Update message */
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG,
                                   BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : Zero As number recieved in AS Path attribute!!!\n");
                    TMO_SLL_Delete (BGP4_INFO_ASPATH (pBGP4Info),
                                    &pASPath->sllNode);
                    Bgp4MemReleaseASNode (pASPath);
                    Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR,
                                     BGP4_MALFORMED_AS_PATH, pu1Buf, i4Attrlen);
                    return BGP4_FAILURE;
                }
            }
        }
        else
        {
            /* For other cases, NEW->OLD, OLD->NEW, OLD->OLD, memcopy each
               2 byte asn as a 4 byte asn (higher order byte set to 0)
               into the Path List */
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : 4 Byte ASN Capability is not supported by peer."
                           " The 2 byte ASN is converted to 4 byte ASN and copied into the path list\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));

            MEMSET (BGP4_ASPATH_NOS (pASPath), 0, BGP4_AS4_SEG_LEN);

/* when local-as and remote-as is configured for that peer append both AS'es*/
            if (u1LocalAsPeer == BGP4_PEER_LCL_AS)
            {
                u4LocalAs = BGP4_PEER_LOCAL_AS (pPeerentry);
                pLocalASPath = Bgp4MemGetASNode (sizeof (tAsPath));
                if (pLocalASPath == NULL)
                {
                    BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                              BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                              BGP4_MOD_NAME,
                              "\tMemory Allocation for Feasible Route's AS Node "
                              "FAILED!!!\n");
                    gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
                    return (BGP4_RSRC_ALLOC_FAIL);
                }
                BGP4_ASPATH_LEN (pLocalASPath) = BGP4_MIN_AS_IN_SEG;
                PTR_ASSIGN4 (BGP4_ASPATH_NOS (pLocalASPath), u4LocalAs);
                MEMCPY (BGP4_ASPATH_NOS (pASPath),
                        BGP4_ASPATH_NOS (pLocalASPath),
                        BGP4_ASPATH_LEN (pLocalASPath) * BGP4_AS4_LENGTH);
                i2TempNumAs = BGP4_ASPATH_LEN (pASPath) - BGP4_MIN_AS_IN_SEG;
            }
            else
            {
                i2TempNumAs = BGP4_ASPATH_LEN (pASPath);
            }
            for (i2NumAs = 0;
                 ((i2NumAs < i2TempNumAs) &&
                  (i2AsOffset < BGP4_AS4_SEG_LEN)); i2NumAs++)
            {

                if ((i2NumAs == 0) && (u1LocalAsPeer == BGP4_PEER_LCL_AS))
                {
                    MEMCPY ((BGP4_ASPATH_NOS (pASPath) +
                             (BGP4_ASPATH_LEN (pLocalASPath) *
                              BGP4_AS4_LENGTH) + i2AsOffset), pu1Buf, i2AsLen);

                }
                else
                {
                    MEMCPY (BGP4_ASPATH_NOS (pASPath) + i2AsOffset,
                            pu1Buf, i2AsLen);
                }
                if (MEMCMP
                    (&u4AsnNo,
                     (BGP4_ASPATH_NOS (pASPath) + (i2NumAs * BGP4_AS4_LENGTH)),
                     BGP4_AS4_LENGTH) == 0)
                {
                    /* zero AS number recieved in Update message */
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG,
                                   BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : Zero As number recieved in AS Path attribute!!!\n");
                    TMO_SLL_Delete (BGP4_INFO_ASPATH (pBGP4Info),
                                    &pASPath->sllNode);
                    Bgp4MemReleaseASNode (pASPath);
                    Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR,
                                     BGP4_MALFORMED_AS_PATH, pu1Buf, i4Attrlen);
                    return BGP4_FAILURE;
                }
                pu1Buf += i2AsLen;
                i2AsOffset += BGP4_AS4_LENGTH;
            }
        }
        if ((u4PeerType == BGP4_EXTERNAL_PEER && u4ASPathFlag == FALSE)
            && (u1LocalAsPeer != BGP4_PEER_LCL_AS))
        {
            pu4ASNo = (UINT4 *) (VOID *) BGP4_ASPATH_NOS (pASPath);
            if (BGP4_PEER_ASNO (pPeerentry) != OSIX_NTOHL (*(pu4ASNo)))
            {
                BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Update MSG ERR - Peer AS NO %d does not match with AS path"
                               " numbers.\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))),
                               BGP4_PEER_ASNO (pPeerentry));
                Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR,
                                 BGP4_MALFORMED_AS_PATH, NULL, 0);
                TMO_SLL_Delete (BGP4_INFO_ASPATH (pBGP4Info),
                                &pASPath->sllNode);
                Bgp4MemReleaseASNode (pASPath);
                return (BGP4_FAILURE);
            }
            u4ASPathFlag = TRUE;
        }
        i2Len = (INT2) (i2Len - ((BGP4_ASPATH_LEN (pASPath) * i2AsLen) +
                                 BGP4_ATTR_PATH_TYPE_LEN +
                                 BGP4_ATTR_PATH_LEN_LEN));
        i2AsOffset = BGP4_AS_LENGTH;
    }

    BGP4_INFO_ATTR_FLAG (pBGP4Info) |= BGP4_ATTR_PATH_MASK;
    return (i4Attrlen);
}

/******************************************************************************/
/* Function Name : Bgp4MsghProcessPathFour                                    */
/* Description   : This function checks the validity of the AS4_PATH attribute*/
/*                 receivied in the UPDATE message and stores the same in the */
/*                 given BGP4_INFO structure.                                 */
/* Input(s)      : Peer which has sent us the UPDATE message (pPeerentry),    */
/*                 Buffer containing the UPDATE message (pu1Buf),             */
/* Output(s)     : Updated BGP4 Information structure (pBGP4Info)             */
/* Return(s)     : Number of bytes processed in the buffer.                   */
/******************************************************************************/
INT4
Bgp4MsghProcessPathFour (tBgp4PeerEntry * pPeerentry,
                         tBgp4Info * pBGP4Info, UINT1 *pu1Buf,
                         INT4 i4RemPattrLen)
{
    tAsPath            *pTsAspath = NULL;
    UINT4              *pu4As4List;
    UINT4               u4AsNo, u4AsNoInUpd;
    INT4                i4Attrlen;
    INT4                i4AttrValFieldLen;
    INT4                i4SpeakerPeerCap;
    INT4                i4NumAs;
    INT4                i4FirstAsn = -1;
    INT2                i2Len;
    INT2                i2NumASFour;
    UINT1               u1Type;
    UINT1               u1Length;

    if (((*pu1Buf) & BGP4_EXT_LEN_FLAG_MASK) == BGP4_EXT_LEN_FLAG_MASK)
    {
        PTR_FETCH2 (*(UINT2 *) &i2Len,
                    (pu1Buf + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN));
        i4AttrValFieldLen = BGP4_EXTENDED_ATTR_LEN_SIZE;

        pu1Buf +=
            BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN + i4AttrValFieldLen;
        i4Attrlen =
            i2Len + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            i4AttrValFieldLen;
    }
    else
    {
        i4AttrValFieldLen = sizeof (UINT1);
        i2Len =
            *(UINT1 *) (pu1Buf + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN);
        pu1Buf +=
            BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN + i4AttrValFieldLen;
        i4Attrlen =
            i2Len + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            i4AttrValFieldLen;
    }
    if (i4Attrlen > i4RemPattrLen)    /* Discard the attribute and continue processing UPDATE message */
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - AS4 Path Attr Length "
                       "Error\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        return (i4Attrlen);
    }

    /* If the Speaker & the peer are
       New BGP Speakers i.e.NEW->NEW -----> This attribute should not be present. 
       If AS4_PATH is present, then it  should be 
       ignored and the UPDATE message has to be processed further.
       For all other cases, i.e NEW->OLD, OLD->OLD, OLD->NEW
       -----> AS4_PATH contains all 4 byte ASNs */
    i4SpeakerPeerCap = Bgp4GetSpeakerPeer4ByteAsnCapability (pPeerentry);
    if (i4SpeakerPeerCap == BGP4_4BYTE_ASN_SPEAKER_AND_PEER)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - AS4 Path Attr Shouldn't be there\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        return (i4Attrlen);
    }

    while (i2Len > 0)
    {
        /* BGP4_ATTR_PATH_CONFED_SET, BGP4_ATTR_PATH_CONFED_SEQUENCE => Should not be there in
           AS4_PATH attribute, hence ignored */
        if ((*pu1Buf != BGP4_ATTR_PATH_SET) &&
            (*pu1Buf != BGP4_ATTR_PATH_SEQUENCE))
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG,
                           BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC |
                           BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tPEER %s - Update MSG ERR - Confed segment path and"
                           " Confed sequence path should not be there in AS4 path attribute.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            return (i4Attrlen);
        }

        u1Type = *pu1Buf;
        pu1Buf++;
        u1Length = *pu1Buf;
        pu1Buf++;

        if (BGP4_AS4_SEG_LEN < (u1Length * BGP4_AS4_LENGTH))
        {
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Update MSG ERR - Size of au1ASSegs is less %d\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))), BGP4_AS4_SEG_LEN);
            return BGP4_FAILURE;

        }

        for (i2NumASFour = 0; i2NumASFour < u1Length; i2NumASFour++)
        {
            PTR_FETCH4 (u4AsNoInUpd, pu1Buf);
            if (u4AsNoInUpd > BGP4_MAX_TWO_BYTE_AS)
            {
                TMO_SLL_Scan (BGP4_INFO_ASPATH (pBGP4Info),
                              pTsAspath, tAsPath *)
                {
                    if (BGP4_ASPATH_TYPE (pTsAspath) == u1Type)
                    {
                        pu4As4List = (UINT4 *) (VOID *)
                            BGP4_ASPATH_NOS (pTsAspath);
                        for (i4NumAs = i4FirstAsn + 1;
                             i4NumAs < BGP4_ASPATH_LEN (pTsAspath); i4NumAs++)
                        {
                            PTR_FETCH4 (u4AsNo,
                                        pu4As4List +
                                        (i4NumAs * BGP4_AS4_LENGTH));
                            if (u4AsNo == BGP4_AS_TRANS)
                            {
                                PTR_ASSIGN4 ((BGP4_ASPATH_NOS (pTsAspath) +
                                              (i4NumAs * BGP4_AS4_LENGTH)),
                                             u4AsNoInUpd);
                                i4FirstAsn = i4NumAs;
                                break;
                            }
                        }
                    }
                }
            }
            pu1Buf += BGP4_AS4_LENGTH;
            i2Len = (INT2) (i2Len - BGP4_AS4_LENGTH);
        }
        i4FirstAsn = -1;

        i2Len =
            (INT2) (i2Len - (BGP4_ATTR_PATH_TYPE_LEN + BGP4_ATTR_PATH_LEN_LEN));
    }

    BGP4_INFO_ATTR_FLAG (pBGP4Info) |= BGP4_ATTR_PATH_FOUR_MASK;
    return (i4Attrlen);
}

/******************************************************************************/
/* Function Name : Bgp4MsghProcessNexthop                                     */
/* Description   : This function checks the validity of the Next Hop          */
/*                 attribute receivied in the UPDATE message and stores the   */
/*                 same in the given BGP4_INFO structure.                     */
/* Input(s)      : Peer which has sent us the UPDATE message (pPeerentry),    */
/*                 Buffer containing the UPDATE message (pu1Buf),             */
/* Output(s)     : Updated BGP4 Information structure (pBGP4Info)             */
/* Return(s)     : Number of bytes processed in the buffer.                   */
/******************************************************************************/
INT4
Bgp4MsghProcessNexthop (tBgp4PeerEntry * pPeerentry,
                        tBgp4Info * pBGP4Info, UINT1 *pu1Buf,
                        INT4 i4RemPattrLen)
{
    INT4                i4Len;
    INT4                i4Ret;

    i4Len = *(UINT1 *) (pu1Buf + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN);

    if ((i4Len != BGP4_ATTR_NEXTHOP_LEN) ||
        ((i4Len + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
          BGP4_AL_NEXTHOP_SIZE) > i4RemPattrLen))
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Invalid Next Hop "
                       "Attr Length\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        i4Len +=
            (BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN + BGP4_AL_NEXTHOP_SIZE);
        Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR, BGP4_ATTR_LEN_ERR,
                         pu1Buf, i4Len);
        return (BGP4_FAILURE);
    }

    i4Ret = Bgp4MsghValidateIpv4Nexthop (pPeerentry, pBGP4Info,
                                         (pu1Buf + BGP4_ATYPE_FLAGS_LEN +
                                          BGP4_ATYPE_CODE_LEN +
                                          BGP4_AL_NEXTHOP_SIZE));
    if (i4Ret == BGP4_FAILURE)
    {
        i4Len +=
            (BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN + BGP4_AL_NEXTHOP_SIZE);
        Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR,
                         BGP4_INVALID_NEXTHOP, pu1Buf, i4Len);
        return BGP4_FAILURE;
    }
    else if (i4Ret == BGP4_SUCCESS)
    {

        BGP4_INFO_ATTR_FLAG (pBGP4Info) |= BGP4_ATTR_NEXT_HOP_MASK;
        return (i4Len + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                BGP4_AL_NEXTHOP_SIZE);
    }

    return i4Ret;
}

/******************************************************************************/
/* Function Name : Bgp4MsghValidateIpv4Nexthop                                */
/* Description   : This function checks the validity of the Next Hop          */
/*                 attribute receivied in the UPDATE message and stores the   */
/*                 same in the given BGP4_INFO structure.                     */
/* Input(s)      : Peer which has sent us the UPDATE message (pPeerentry),    */
/*                 Buffer containing the UPDATE message (pu1Buf),             */
/* Output(s)     : Updated BGP4 Information structure (pBGP4Info)             */
/* Return(s)     : Number of bytes processed in the buffer.                   */
/******************************************************************************/
INT4
Bgp4MsghValidateIpv4Nexthop (tBgp4PeerEntry * pPeerentry,
                             tBgp4Info * pBGP4Info, UINT1 *pu1Buf)
{
    tAddrPrefix         LocalBgpIdAddrPrefix;
    tAddrPrefix         RemAddr;
    tNetAddress         LocalAddr;
    UINT4               u4Nexthop;
    INT4                i4LocalAddrPresent = BGP4_TRUE;
    INT4                i4Ret;
#ifdef L3VPN
    UINT1               u1MultihopChk = BGP4_TRUE;
#endif

    /* retrieve nexthop */
    PTR_FETCH_4 (u4Nexthop, pu1Buf);
    PTR_ASSIGN4 (BGP4_INFO_NEXTHOP (pBGP4Info), u4Nexthop);

    if ((Bgp4IsValidNextHopAddress
         (BGP4_INFO_NEXTHOP_INFO (pBGP4Info), BGP4_FALSE)) != BGP4_TRUE)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Invalid NEXTHOP "
                       "Address\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        return (BGP4_FAILURE);
    }
#ifdef L3VPN
    /* If the rotue is received from CE peer, the nexthop should be its
     * own local address
     */
    if (BGP4_VPN4_PEER_ROLE (pPeerentry) == BGP4_VPN4_CE_PEER)
    {
        u1MultihopChk = BGP4_FALSE;
        if ((PrefixMatch (BGP4_INFO_NEXTHOP_INFO (pBGP4Info),
                          BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry))) ==
            BGP4_FALSE)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - Update MSG ERR - Semantically "
                           "Invalid NEXTHOP\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            return (BGP4_SEMANTIC_INVALID_NEXTHOP);
        }
    }
    if (u1MultihopChk == BGP4_TRUE)
#endif
    {
        /** Check for Semantic correctness - Sharing the same subnet */
        if (BGP4_PEER_EBGP_MULTIHOP (pPeerentry) == BGP4_EBGP_MULTI_HOP_DISABLE)
        {
            if ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerentry), pPeerentry)
                 == BGP4_EXTERNAL_PEER)
                && (BGP4_CONFED_PEER_STATUS (pPeerentry) == BGP4_FALSE))
            {
                if ((Bgp4IsOnSameSubnet (BGP4_INFO_NEXTHOP_INFO (pBGP4Info),
                                         pPeerentry) == FALSE))
                {
                    BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                                   BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s - Update MSG ERR - Nexthop %s is not on"
                                   " the same subnet of the peer entry.\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))),
                                   Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP
                                                    (pBGP4Info),
                                                    BGP4_INFO_AFI (pBGP4Info)));
                    SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, BGP4_SYSLOG_ID,
                                  "\tPEER %s - Update MSG ERR - Semantically Invalid"
                                  "NEXTHOP\n",
                                  Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                   (pPeerentry),
                                                   BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                   (BGP4_PEER_REMOTE_ADDR_INFO
                                                    (pPeerentry)))));
                    return (BGP4_SEMANTIC_INVALID_NEXTHOP);
                }
            }
        }
    }

    /* This value will remain for all address families, router ID is a 
     * 32 bit value */
    Bgp4InitAddrPrefixStruct (&LocalBgpIdAddrPrefix, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalBgpIdAddrPrefix),
                  BGP4_LOCAL_BGP_ID (BGP4_PEER_CXT_ID (pPeerentry)));
    /* Check for semantic correctness - Same as LOCAL address - */
    i4Ret =
        Bgp4GetPeerNetworkAddress (pPeerentry, BGP4_INET_AFI_IPV4, &RemAddr);
    if (i4Ret == BGP4_FAILURE)
    {
        /* Network address (v4) is not configured for peer. */
        Bgp4CopyAddrPrefixStruct (&RemAddr, BGP4_INFO_NEXTHOP_INFO (pBGP4Info));
    }

    i4Ret = Bgp4GetLocalAddrForPeer (pPeerentry, RemAddr, &LocalAddr,
                                     BGP4_TRUE);
    if (i4Ret == BGP4_FAILURE)
    {
        i4LocalAddrPresent = BGP4_FALSE;
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Failed in getting local interface address "
                       "for peer\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                        (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, BGP4_SYSLOG_ID,
                      "\tPEER %s - Update MSG ERR - Semantically Invalid"
                      "NEXTHOP\n",
                      Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                       BGP4_AFI_IN_ADDR_PREFIX_INFO
                                       (BGP4_PEER_REMOTE_ADDR_INFO
                                        (pPeerentry)))));
        return (BGP4_SEMANTIC_INVALID_NEXTHOP);

    }

    if (((i4LocalAddrPresent == BGP4_TRUE) &&
         ((PrefixMatch (BGP4_INFO_NEXTHOP_INFO (pBGP4Info),
                        LocalAddr.NetAddr)) == BGP4_TRUE)) ||
        ((PrefixMatch (BGP4_INFO_NEXTHOP_INFO (pBGP4Info),
                       LocalBgpIdAddrPrefix)) == BGP4_TRUE))
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Nexthop and local BGP"
                       " Identifier are same\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, BGP4_SYSLOG_ID,
                      "\tPEER %s - Update MSG ERR - Semantically Invalid"
                      "NEXTHOP\n",
                      Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                       BGP4_AFI_IN_ADDR_PREFIX_INFO
                                       (BGP4_PEER_REMOTE_ADDR_INFO
                                        (pPeerentry)))));

        return (BGP4_SEMANTIC_INVALID_NEXTHOP);
    }

    BGP4_INFO_ATTR_FLAG (pBGP4Info) |= BGP4_ATTR_NEXT_HOP_MASK;
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4MsghProcessMed                                         */
/* Description   : This function stores the MED attribute in the given        */
/*                 BGP4_INFO structure from the receivied UPDATE message.     */
/* Input(s)      : Peer which has sent us the UPDATE message (pPeerentry),    */
/*                 Buffer containing the UPDATE message (pu1Buf),             */
/* Output(s)     : Updated BGP4 Information structure (pBGP4Info)             */
/* Return(s)     : Number of bytes processed in the buffer.                   */
/******************************************************************************/
INT4
Bgp4MsghProcessMed (tBgp4PeerEntry * pPeerentry, tBgp4Info * pBGP4Info,
                    UINT1 *pu1Buf, INT4 i4RemPattrLen)
{
    INT4                i4Len;

    i4Len = *(UINT1 *) (pu1Buf + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN);

    if ((i4Len != BGP4_ATTR_MED_LEN) ||
        ((i4Len + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
          BGP4_AL_MED_SIZE) > i4RemPattrLen))
    {
        i4Len += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN + BGP4_AL_MED_SIZE;
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Invalid MED Length\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR, BGP4_ATTR_LEN_ERR,
                         pu1Buf, i4Len);
        return (BGP4_FAILURE);
    }

    /* retrive MED */
    PTR_FETCH4 (BGP4_INFO_RCVD_MED (pBGP4Info),
                (pu1Buf + BGP4_ATYPE_FLAGS_LEN
                 + BGP4_ATYPE_CODE_LEN + BGP4_AL_MED_SIZE));

    BGP4_INFO_ATTR_FLAG (pBGP4Info) |= BGP4_ATTR_MED_MASK;
    return (i4Len + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_AL_MED_SIZE);
}

/******************************************************************************/
/* Function Name : Bgp4MsghProcessLocalpref                                   */
/* Description   : This function stores the LP attribute in the given         */
/*                 BGP_INFO structure from the receivied UPDATE message.      */
/* Input(s)      : Peer which has sent us the UPDATE message (pPeerentry),    */
/*                 Buffer containing the UPDATE message (pu1Buf),             */
/* Output(s)     : Updated BGP4 Information structure (pBGP4Info)             */
/* Return(s)     : Number of bytes processed in the buffer.                   */
/******************************************************************************/
INT4
Bgp4MsghProcessLocalpref (tBgp4PeerEntry * pPeerentry,
                          tBgp4Info * pBGP4Info, UINT1 *pu1Buf,
                          INT4 i4RemPattrLen)
{
    INT4                i4Len;

    i4Len = *(UINT1 *) (pu1Buf + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN);

    if ((i4Len != BGP4_ATTR_LOCAL_PREF_LEN) ||
        ((i4Len + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
          BGP4_AL_LP_SIZE) > i4RemPattrLen))
    {
        i4Len += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN + BGP4_AL_LP_SIZE;
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Invalid Local "
                       "Preference Attribute Length\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR, BGP4_ATTR_LEN_ERR,
                         pu1Buf, i4Len);
        return (BGP4_FAILURE);
    }

    if ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerentry), pPeerentry) ==
         BGP4_EXTERNAL_PEER)
        && (BGP4_CONFED_PEER_STATUS (pPeerentry) == BGP4_FALSE))
    {
        BGP4_INFO_RCVD_LOCAL_PREF (pBGP4Info) = BGP4_INV_MEDLP;
    }
    else
    {
        PTR_FETCH4 (BGP4_INFO_RCVD_LOCAL_PREF (pBGP4Info),
                    (pu1Buf + BGP4_ATYPE_FLAGS_LEN
                     + BGP4_ATYPE_CODE_LEN + BGP4_AL_LP_SIZE));
        BGP4_INFO_ATTR_FLAG (pBGP4Info) |= BGP4_ATTR_LOCAL_PREF_MASK;
    }
    return (i4Len + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_AL_LP_SIZE);
}

/******************************************************************************/
/* Function Name : Bgp4MsghProcessAtomicAggr                                  */
/* Description   : This function stores the Atomic Aggregator attribute in    */
/*                 the given BGP4_INFO structure from the receivied UPDATE    */
/*                 message.                                                   */
/* Input(s)      : Peer which has sent us the UPDATE message (pPeerentry),    */
/*                 Buffer containing the UPDATE message (pu1Buf),             */
/* Output(s)     : Updated BGP4 Information structure (pBGP4Info)             */
/* Return(s)     : Number of bytes processed in the buffer.                   */
/******************************************************************************/
INT4
Bgp4MsghProcessAtomicAggr (tBgp4PeerEntry * pPeerentry,
                           tBgp4Info * pBGP4Info, UINT1 *pu1Buf,
                           INT4 i4RemPattrLen)
{
    INT4                i4Len;

    i4Len = *(UINT1 *) (pu1Buf + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN);

    if ((i4Len != BGP4_ATTR_ATOMIC_AGGR_LEN) ||
        ((i4Len + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
          BGP4_AL_ATOMIC_AGGR_SIZE) > i4RemPattrLen))
    {
        i4Len += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_AL_ATOMIC_AGGR_SIZE;
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Invalid Atomic "
                       "Aggregator Attribute Length\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR, BGP4_ATTR_LEN_ERR,
                         pu1Buf, i4Len);
        return (BGP4_FAILURE);
    }

    BGP4_INFO_ATTR_FLAG (pBGP4Info) |= BGP4_ATTR_ATOMIC_AGGR_MASK;
    return (i4Len + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_AL_ATOMIC_AGGR_SIZE);
}

/******************************************************************************/
/* Function Name : Bgp4MsghProcessAggregator                                  */
/* Description   : This function validates & stores the Aggregator attr in    */
/*                 the given BGP4_INFO structure from the receivied UPDATE    */
/*                 message.                                                   */
/* Input(s)      : Peer which has sent us the UPDATE message (pPeerentry),    */
/*                 Buffer containing the UPDATE message (pu1Buf),             */
/* Output(s)     : Updated BGP4 Information structure (pBGP4Info)             */
/* Return(s)     : Number of bytes processed in the buffer.                   */
/******************************************************************************/
INT4
Bgp4MsghProcessAggregator (tBgp4PeerEntry * pPeerentry,
                           tBgp4Info * pBGP4Info, UINT1 *pu1Buf,
                           INT4 i4RemPattrLen)
{
    UINT4               u4AggrAddr;
    INT4                i4Len;
    INT4                i4AggrLen;
    INT4                i4AggrAsLen;
    INT4                i4SpeakerPeerCap;
    UINT4               u4AggrAs;
    UINT2               u2AsNoInUpd;
    UINT1               u1Flag;

    /* If the Speaker & the peer are
       New BGP Speakers i.e.NEW->NEW -----> AS_AGGR contains 4 byte ASN
       For all other cases, i.e NEW->OLD, OLD->OLD, OLD->NEW
       -----> AS_AGGR contains 2 byte ASN */
    i4SpeakerPeerCap = Bgp4GetSpeakerPeer4ByteAsnCapability (pPeerentry);
    if (i4SpeakerPeerCap == BGP4_4BYTE_ASN_SPEAKER_AND_PEER)
    {
        i4AggrLen = BGP4_ATTR_AGGREGATOR_FOUR_LEN;    /* Aggregator length = 8 */
        i4AggrAsLen = BGP4_ATTR_AGGR_AS4LEN;
    }
    else
    {
        i4AggrLen = BGP4_ATTR_AGGREGATOR_LEN;    /* Aggregator length = 6 */
        i4AggrAsLen = BGP4_ATTR_AGGR_ASLEN;
    }

    i4Len = *(UINT1 *) (pu1Buf + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN);
    u1Flag = *pu1Buf;

    if ((i4Len != i4AggrLen) ||
        ((i4Len + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
          BGP4_AL_AGGREGATOR_SIZE) > i4RemPattrLen))
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Invalid Aggregator "
                       "Attribute Length\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        i4Len +=
            BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_AL_AGGREGATOR_SIZE;
        Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR, BGP4_ATTR_LEN_ERR,
                         pu1Buf, i4Len);
        return (BGP4_FAILURE);
    }

    AGGR_NODE_CREATE (BGP4_INFO_AGGREGATOR (pBGP4Info));
    if (BGP4_INFO_AGGREGATOR (pBGP4Info) == NULL)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG,
                       BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                       BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Memory Allocation for Aggregator "
                       "Attribute FAILED!!!\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        gu4BgpDebugCnt[MAX_BGP_AGGR_NODE_SIZING_ID]++;
        return (BGP4_RSRC_ALLOC_FAIL);
    }

    BGP4_INFO_AGGREGATOR_FLAG (pBGP4Info) = u1Flag;

    if (i4SpeakerPeerCap == BGP4_4BYTE_ASN_SPEAKER_AND_PEER)
    {

        PTR_FETCH4 (BGP4_INFO_AGGREGATOR_AS (pBGP4Info),
                    (pu1Buf + BGP4_ATYPE_FLAGS_LEN
                     + BGP4_ATYPE_CODE_LEN + BGP4_AL_AGGREGATOR_SIZE));
    }
    else
    {
        PTR_FETCH2 (u2AsNoInUpd, (pu1Buf + BGP4_ATYPE_FLAGS_LEN
                                  + BGP4_ATYPE_CODE_LEN +
                                  BGP4_AL_AGGREGATOR_SIZE));
        BGP4_INFO_AGGREGATOR_AS (pBGP4Info) = (UINT4) u2AsNoInUpd;
    }

    u4AggrAs = BGP4_INFO_AGGREGATOR_AS (pBGP4Info);

    if (u4AggrAs == 0)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Aggregator As is zero\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        i4Len +=
            BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_AL_AGGREGATOR_SIZE;
        Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR,
                         BGP4_OPTIONAL_ATTR_ERR, pu1Buf, i4Len);
        AGGR_NODE_FREE (BGP4_INFO_AGGREGATOR (pBGP4Info));
        BGP4_INFO_AGGREGATOR (pBGP4Info) = NULL;
        return (BGP4_FAILURE);
    }

    PTR_FETCH4 (BGP4_INFO_AGGREGATOR_NODE (pBGP4Info),
                (pu1Buf + BGP4_ATYPE_FLAGS_LEN
                 + BGP4_ATYPE_CODE_LEN
                 + BGP4_AL_AGGREGATOR_SIZE + i4AggrAsLen));

    u4AggrAddr = BGP4_INFO_AGGREGATOR_NODE (pBGP4Info);

    if (BGP4_IS_VALID_ADDRESS (u4AggrAddr) == 0)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Invalid Aggregator "
                       "Address\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        i4Len +=
            BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_AL_AGGREGATOR_SIZE;
        Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR,
                         BGP4_OPTIONAL_ATTR_ERR, pu1Buf, i4Len);
        AGGR_NODE_FREE (BGP4_INFO_AGGREGATOR (pBGP4Info));
        BGP4_INFO_AGGREGATOR (pBGP4Info) = NULL;
        return (BGP4_FAILURE);
    }

    BGP4_INFO_ATTR_FLAG (pBGP4Info) |= BGP4_ATTR_AGGREGATOR_MASK;
    return (i4Len + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_AL_AGGREGATOR_SIZE);
}

/******************************************************************************/
/* Function Name : Bgp4MsghProcessAggregatorFour                              */
/* Description   : This function validates & stores the Aggregator attr in    */
/*                 the given BGP4_INFO structure from the receivied UPDATE    */
/*                 message.                                                   */
/* Input(s)      : Peer which has sent us the UPDATE message (pPeerentry),    */
/*                 Buffer containing the UPDATE message (pu1Buf),             */
/* Output(s)     : Updated BGP4 Information structure (pBGP4Info)             */
/* Return(s)     : Number of bytes processed in the buffer.                   */
/******************************************************************************/
INT4
Bgp4MsghProcessAggregatorFour (tBgp4PeerEntry * pPeerentry,
                               tBgp4Info * pBGP4Info, UINT1 *pu1Buf,
                               INT4 i4RemPattrLen)
{
    UINT4               u4AggrAddr;
    UINT4               u4AsNoInUpd;
    INT4                i4Len;
    INT4                i4SpeakerPeerCap;
    UINT1               u1Flag;

    i4Len = *(UINT1 *) (pu1Buf + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN);
    u1Flag = *pu1Buf;

    i4SpeakerPeerCap = Bgp4GetSpeakerPeer4ByteAsnCapability (pPeerentry);
    if ((i4SpeakerPeerCap == BGP4_4BYTE_ASN_SPEAKER_AND_PEER) || (BGP4_INFO_AGGREGATOR (pBGP4Info) == NULL))    /*Discard the attribute and continue processing
                                                                                                                   the update message */
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - AS4 Aggregator "
                       "should not be sent\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        i4Len +=
            BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_AL_AGGREGATOR_SIZE;
        return (i4Len);
    }

    if ((i4Len != BGP4_ATTR_AGGREGATOR_FOUR_LEN) ||
        ((i4Len + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
          BGP4_AL_AGGREGATOR_SIZE) > i4RemPattrLen))
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Invalid Aggregator "
                       "Attribute Length\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        i4Len +=
            BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_AL_AGGREGATOR_SIZE;
        return (i4Len);
    }

    BGP4_INFO_AGGREGATOR_FLAG (pBGP4Info) = u1Flag;

    PTR_FETCH4 (u4AsNoInUpd,
                (pu1Buf + BGP4_ATYPE_FLAGS_LEN
                 + BGP4_ATYPE_CODE_LEN + BGP4_AL_AGGREGATOR_SIZE));

    if (u4AsNoInUpd == 0)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Aggregator AS is zero\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        i4Len +=
            BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_AL_AGGREGATOR_SIZE;
        return (i4Len);
    }

    if ((BGP4_INFO_AGGREGATOR_AS (pBGP4Info) == BGP4_AS_TRANS) &&
        (u4AsNoInUpd > BGP4_MAX_TWO_BYTE_AS))
    {
        BGP4_INFO_AGGREGATOR_AS (pBGP4Info) = u4AsNoInUpd;
    }

    PTR_FETCH4 (u4AggrAddr,
                (pu1Buf + BGP4_ATYPE_FLAGS_LEN
                 + BGP4_ATYPE_CODE_LEN
                 + BGP4_AL_AGGREGATOR_SIZE + BGP4_ATTR_AGGR_AS4LEN));

    if (u4AggrAddr != BGP4_INFO_AGGREGATOR_NODE (pBGP4Info))
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Aggregator address is "
                       "invalid\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        i4Len +=
            BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_AL_AGGREGATOR_SIZE;
        return (i4Len);
    }

    BGP4_INFO_ATTR_FLAG (pBGP4Info) |= BGP4_ATTR_AGGREGATOR_FOUR_MASK;
    return (i4Len + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_AL_AGGREGATOR_SIZE);
}

/******************************************************************************/
/* Function Name : Bgp4MsghProcessCommunity                                   */
/* Description   : This function validates & stores the Community attr in     */
/*                 the given BGP4_INFO structure from the received UPDATE     */
/*                 message.                                                   */
/* Input(s)      : Peer which has sent us the UPDATE message (pPeerentry),    */
/*                 Buffer containing the UPDATE message (pu1Buf),             */
/* Output(s)     : Updated BGP4 Information structure (pBGP4Info)             */
/* Return(s)     : Number of bytes processed in the buffer.                   */
/*               : or BGP4_FAILURE if any error is detecte                    */
/******************************************************************************/
INT4
Bgp4MsghProcessCommunity (tBgp4PeerEntry * pPeerentry,
                          tBgp4Info * pBGP4Info, UINT1 *pu1Buf,
                          INT4 i4RemPattrLen)
{
    tCommunity         *pComm = NULL;
    UINT1              *pu1Str = NULL;
    INT4                i4TotalAttrLen = 0;
    UINT2               u2AttribLen = 0;
    UINT1               u1Flag = 0;
    UINT1               u1AttribType = 0;
    UINT1               u1IsAttribLenValid;
    UINT4               au4Community[RMAP_BGP_MAX_COMM];
    UINT2               u2Count = 0;
    UINT2               u2CommCnt = 0;
    UINT4               u4Comm = 0;
    UINT4               u4Offset = 0;
    if (((*pu1Buf) & BGP4_EXT_LEN_FLAG_MASK) == BGP4_EXT_LEN_FLAG_MASK)
    {
        /* Extended Length flag bit is set. 2 & 3 byte holds the length
         * value. */
        PTR_FETCH2 (u2AttribLen, (pu1Buf + BGP4_ATYPE_FLAGS_LEN +
                                  BGP4_ATYPE_CODE_LEN));
        i4TotalAttrLen = u2AttribLen + BGP4_ATYPE_FLAGS_LEN +
            BGP4_ATYPE_CODE_LEN + BGP4_EXTENDED_ATTR_LEN_SIZE;
    }
    else
    {
        /* Extended length flag bit not set */
        u2AttribLen =
            *(UINT1 *) (pu1Buf + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN);
        i4TotalAttrLen = u2AttribLen + BGP4_ATYPE_FLAGS_LEN +
            BGP4_ATYPE_CODE_LEN + BGP4_AL_UNKNOWN_SIZE;
    }

    u1Flag = *pu1Buf;
    u1AttribType = *(pu1Buf + BGP4_ONE_BYTE);

    /* Validate the length of Community Attribute */
    u1IsAttribLenValid = BGP4_IS_COMM_PATH_LEN_VALID (u2AttribLen);
    if ((u1IsAttribLenValid != COMM_PATH_LEN_VALID) ||
        (i4TotalAttrLen > i4RemPattrLen))
    {
        BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Invalid Community "
                       "Attribute Length - Attr Code: %d, Length: %d\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))), u1AttribType,
                       u2AttribLen);
        Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR, BGP4_ATTR_LEN_ERR,
                         pu1Buf, i4TotalAttrLen);
        /* Community path Length is Invalid, so update statistics */
        COMM_LEN_ERROR_STATS (BGP4_PEER_CXT_ID (pPeerentry))++;
        return (BGP4_FAILURE);
    }

    /* Fill the BgpInfo with the received Community Attribute */
    COMMUNITY_NODE_CREATE (pComm);
    if (pComm == NULL)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG,
                       BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                       BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Memory Allocation for Community "
                       "Attribute FAILED!!!\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        gu4BgpDebugCnt[MAX_BGP_COMMUNITY_NODE_SIZING_ID]++;
        return (BGP4_RSRC_ALLOC_FAIL);
    }

    ATTRIBUTE_NODE_CREATE (pu1Str);
    if (pu1Str == NULL)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG,
                       BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                       BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Memory Allocation for Community "
                       "Attribute FAILED!!!\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        COMMUNITY_NODE_FREE (pComm);
        gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
        return (BGP4_RSRC_ALLOC_FAIL);
    }

    BGP4_INFO_COMM_ATTR (pBGP4Info) = pComm;
    BGP4_INFO_COMM_ATTR_VAL (pBGP4Info) = pu1Str;
    u2CommCnt = (UINT2) (u2AttribLen / COMM_VALUE_LEN);
    BGP4_INFO_COMM_ATTR_FLAG (pBGP4Info) = u1Flag;
    for (u2Count = 0; u2Count < u2CommCnt; u2Count++)
    {
        PTR_FETCH_4 (u4Comm,
                     ((pu1Buf + (i4TotalAttrLen - u2AttribLen)) + u4Offset));
        if (u2Count < RMAP_BGP_MAX_COMM)
        {
            au4Community[u2Count] = u4Comm;
        }
        else
        {
            u2CommCnt = RMAP_BGP_MAX_COMM;
            break;
        }
        u4Offset += COMM_VALUE_LEN;
    }
    Bgp4FindDupCommunity (au4Community, &u2CommCnt);
    BGP4_INFO_COMM_COUNT (pBGP4Info) = u2CommCnt;
    MEMCPY (BGP4_INFO_COMM_ATTR_VAL (pBGP4Info),
            au4Community, u2CommCnt * COMM_VALUE_LEN);

    /* Set the COMM_MASK indicating that the COMM attribute is
     * present in this route info. */
    BGP4_INFO_ATTR_FLAG (pBGP4Info) |= (BGP4_ATTR_COMM_MASK);
    return i4TotalAttrLen;
}

/******************************************************************************/
/* Function Name : Bgp4MsghProcessExtComm                                     */
/* Description   : This function validates & stores the Extended Community    */
/*                 attr int the given BGP4_INFO structure from the received   */
/*                 update message.                                            */
/* Input(s)      : Peer which has sent us the UPDATE message (pPeerentry),    */
/*                 Buffer containing the UPDATE message (pu1Buf),             */
/* Output(s)     : Updated BGP4 Information structure (pBGP4Info)             */
/* Return(s)     : Number of bytes processed in the buffer.                   */
/*               : or BGP4_FAILURE if any error is detecte                    */
/******************************************************************************/
INT4
Bgp4MsghProcessExtComm (tBgp4PeerEntry * pPeerentry,
                        tBgp4Info * pBGP4Info, UINT1 *pu1Buf,
                        INT4 i4RemPattrLen)
{
    tExtCommunity      *pEcomm;
    UINT1              *pu1Str = NULL;
    INT4                i4TotalAttrLen = 0;
    UINT2               u2AttribLen = 0;
    UINT2               u2TypeField = 0;
    UINT1               u1Flag = 0;
    UINT1               u1AttribType = 0;
    UINT1               u1IsAttribLenValid;

    if (((*pu1Buf) & BGP4_EXT_LEN_FLAG_MASK) == BGP4_EXT_LEN_FLAG_MASK)
    {
        /* Extended Length flag bit is set. 2 & 3 byte holds the length
         * value. */
        PTR_FETCH2 (u2AttribLen, (pu1Buf + BGP4_ATYPE_FLAGS_LEN +
                                  BGP4_ATYPE_CODE_LEN));
        i4TotalAttrLen = u2AttribLen + BGP4_ATYPE_FLAGS_LEN +
            BGP4_ATYPE_CODE_LEN + BGP4_EXTENDED_ATTR_LEN_SIZE;
    }
    else
    {
        /* Extended length flag bit not set */
        u2AttribLen =
            *(UINT1 *) (pu1Buf + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN);
        i4TotalAttrLen =
            u2AttribLen + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_AL_UNKNOWN_SIZE;
    }

    u1Flag = *pu1Buf;
    u1AttribType = *(pu1Buf + BGP4_ONE_BYTE);

    /* Validate the length of Extended Community Attribute */
    u1IsAttribLenValid = BGP4_IS_EXT_COMM_PATH_LEN_VALID (u2AttribLen);
    if ((u1IsAttribLenValid != EXT_COMM_PATH_LEN_VALID) ||
        (i4TotalAttrLen > i4RemPattrLen))
    {
        BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Invalid Ext-Comm "
                       "Attribute Length - Attr Code: %d, Length: %d\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))), u1AttribType,
                       u2AttribLen);
        Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR, BGP4_ATTR_LEN_ERR,
                         pu1Buf, i4TotalAttrLen);
        /* Ext-Comm  path Length is Invalid, so update statistics */
        EXT_COMM_LEN_ERROR_STATS (BGP4_PEER_CXT_ID (pPeerentry))++;
        return (BGP4_FAILURE);
    }

    /* Fill the BgpInfo with the received Community Attribute */
    EXT_COMMUNITY_NODE_CREATE (pEcomm);
    if (pEcomm == NULL)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG,
                       BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                       BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Memory Allocation for Ext-Community "
                       "Attribute FAILED!!!\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        gu4BgpDebugCnt[MAX_BGP_EXT_COMM_NODE_SIZING_ID]++;
        return (BGP4_RSRC_ALLOC_FAIL);
    }

    BGP4_INFO_ECOMM_ATTR (pBGP4Info) = pEcomm;

    PTR_FETCH2 (u2TypeField, (pu1Buf + (i4TotalAttrLen - u2AttribLen)));
    if (BGP_EXT_COMM_COST_TYPE_FIELD == u2TypeField)
    {
        BGP4_INFO_ECOMM_ATTR_COST_FLAG (pBGP4Info) = BGP4_TRUE;
        BGP4_INFO_ECOMM_ATTR_COST_POI (pBGP4Info) =
            *(UINT2 *) (VOID *) (pu1Buf + (i4TotalAttrLen - u2AttribLen) +
                                 BGP4_TWO_BYTE);
        BGP4_INFO_ECOMM_ATTR_COST_COMM (pBGP4Info) =
            *(UINT2 *) (VOID *) (pu1Buf + (i4TotalAttrLen - u2AttribLen) +
                                 BGP4_THREE_BYTE);
        PTR_FETCH4 (BGP4_INFO_ECOMM_ATTR_COST_VAL (pBGP4Info),
                    (pu1Buf + (i4TotalAttrLen - u2AttribLen) +
                     BGP4_FOUR_BYTES));
        BGP4_INFO_ECOMM_COUNT (pBGP4Info) =
            (UINT2) (u2AttribLen / EXT_COMM_VALUE_LEN);
        BGP4_INFO_ECOMM_ATTR_FLAG (pBGP4Info) = u1Flag;
    }
    else
    {
        ATTRIBUTE_NODE_CREATE (pu1Str);
        if (pu1Str == NULL)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG,
                           BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                           BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Memory Allocation for Ext-Community "
                           "Attribute FAILED!!!\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            EXT_COMMUNITY_NODE_FREE (pEcomm);
            gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
            return (BGP4_RSRC_ALLOC_FAIL);
        }
        BGP4_INFO_ECOMM_ATTR_VAL (pBGP4Info) = pu1Str;
        BGP4_INFO_ECOMM_COUNT (pBGP4Info) =
            (UINT2) (u2AttribLen / EXT_COMM_VALUE_LEN);
        BGP4_INFO_ECOMM_ATTR_FLAG (pBGP4Info) = u1Flag;
        BGP4_INFO_ECOMM_ATTR_COST_FLAG (pBGP4Info) = BGP4_FALSE;
        MEMCPY (BGP4_INFO_ECOMM_ATTR_VAL (pBGP4Info), pu1Buf +
                (i4TotalAttrLen - u2AttribLen), u2AttribLen);
    }

    /* Set the ECOMM_MASK indicating that the ECOMM attribute is
     * present in this route info. */
    BGP4_INFO_ATTR_FLAG (pBGP4Info) |= (BGP4_ATTR_ECOMM_MASK);
    return i4TotalAttrLen;
}

/******************************************************************************/
/* Function Name : Bgp4MsghProcessOrigId                                      */
/* Description   : This function validates and stores the                     */
/*                 originator identifier attribute in the given BGP4_INFO     */
/*                 structure from the received update message.                */
/* Input(s)      : Peer which has sent us the UPDATE message (pPeerentry),    */
/*                 Buffer containing the UPDATE message (pu1Buf),             */
/* Output(s)     : Updated BGP4 Information structure (pBGP4Info)             */
/* Return(s)     : Number of bytes processed in the buffer.                   */
/*               : or BGP4_FAILURE if any error is detecte                    */
/******************************************************************************/
INT4
Bgp4MsghProcessOrigId (tBgp4PeerEntry * pPeerentry,
                       tBgp4Info * pBGP4Info, UINT1 *pu1Buf, INT4 i4RemPattrLen)
{
    UINT4               u4RecvOrigId;
    INT4                i4TotalAttrLen = 0;
    UINT2               u2AttribLen = 0;
    UINT1               u1AttribType = 0;

    u2AttribLen =
        *(UINT1 *) (pu1Buf + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN);
    i4TotalAttrLen = u2AttribLen + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
        BGP4_AL_UNKNOWN_SIZE;

    u1AttribType = *(pu1Buf + BGP4_ONE_BYTE);

    /* Validate the length of Origination Identifier Attribute */
    if ((u2AttribLen != ORIGINATOR_ID_LENGTH) ||
        (i4TotalAttrLen > i4RemPattrLen))
    {
        BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Invalid Reflector "
                       "Originator Id Length - Attr Code: %d, Length: %d\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))), u1AttribType,
                       u2AttribLen);
        Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR, BGP4_ATTR_LEN_ERR,
                         pu1Buf, i4TotalAttrLen);
        return (BGP4_FAILURE);
    }

    /* Validate the originator id. A router should ignore a route received
     * with its router id as the originator id. */
    PTR_FETCH4 (u4RecvOrigId,
                (pu1Buf + BGP4_ATYPE_FLAGS_LEN +
                 BGP4_ATYPE_CODE_LEN + BGP4_AL_UNKNOWN_SIZE));

    if (u4RecvOrigId == BGP4_LOCAL_BGP_ID (BGP4_PEER_CXT_ID (pPeerentry)))
    {
        /* Received Originator Id is same as that of Local BGP Id. Hence
         * ignore this update message. No need to send any error message
         * to the peer. */
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Received Originator ID "
                       "is same as Local BGP ID\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        return (BGP4_INVALID_ORIG_ID);
    }

    BGP4_INFO_ORIG_ID (pBGP4Info) = u4RecvOrigId;
    /* Set the ORIG_ID_MASK indicating that the ORIG_ID attribute is
     * present in this route info. */
    BGP4_INFO_ATTR_FLAG (pBGP4Info) |= (BGP4_ATTR_ORIG_ID_MASK);
    return i4TotalAttrLen;
}

/******************************************************************************/
/* Function Name : Bgp4MsghProcessClusterList                                 */
/* Description   : This function validates & stores the Cluster List          */
/*                 attribute in the given BGP4_INFO structure from the        */
/*                 received update message.                                   */
/* Input(s)      : Peer which has sent us the UPDATE message (pPeerentry),    */
/*                 Buffer containing the UPDATE message (pu1Buf),             */
/* Output(s)     : Updated BGP4 Information structure (pBGP4Info)             */
/* Return(s)     : Number of bytes processed in the buffer.                   */
/*               : or BGP4_FAILURE if any error is detecte                    */
/******************************************************************************/
INT4
Bgp4MsghProcessClusterList (tBgp4PeerEntry * pPeerentry,
                            tBgp4Info * pBGP4Info, UINT1 *pu1Buf,
                            INT4 i4RemPattrLen)
{
    tClusterList       *pClusList = NULL;
    UINT1              *pu1ClusterList = NULL;
    UINT1              *pu1Str = NULL;
    INT4                i4TotalAttrLen = 0;
    UINT2               u2AttribLen = 0;
    UINT1               u1Flag = 0;
    UINT1               u1AttribType = 0;

    if (((*pu1Buf) & BGP4_EXT_LEN_FLAG_MASK) == BGP4_EXT_LEN_FLAG_MASK)
    {
        /* Extended Length flag bit is set. 2 & 3 byte holds the length
         * value. */
        PTR_FETCH2 (u2AttribLen, (pu1Buf + BGP4_ATYPE_FLAGS_LEN +
                                  BGP4_ATYPE_CODE_LEN));
        i4TotalAttrLen = u2AttribLen + BGP4_ATYPE_FLAGS_LEN +
            BGP4_ATYPE_CODE_LEN + BGP4_EXTENDED_ATTR_LEN_SIZE;
    }
    else
    {
        /* Extended length flag bit not set */
        u2AttribLen =
            *(UINT1 *) (pu1Buf + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN);
        i4TotalAttrLen = u2AttribLen + BGP4_ATYPE_FLAGS_LEN +
            BGP4_ATYPE_CODE_LEN + BGP4_AL_UNKNOWN_SIZE;
    }

    u1Flag = *pu1Buf;
    u1AttribType = *(pu1Buf + BGP4_ONE_BYTE);

    /* Validate the length of Cluster List Attribute */
    if (((u2AttribLen == 0) || ((u2AttribLen % CLUSTER_ID_LENGTH) != 0)) ||
        (i4TotalAttrLen > i4RemPattrLen))
    {
        BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Invalid Reflector "
                       "Cluster Id Length - Attr Code: %d, Length: %d\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))), u1AttribType,
                       u2AttribLen);
        Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR, BGP4_ATTR_LEN_ERR,
                         pu1Buf, i4TotalAttrLen);
        return (BGP4_FAILURE);
    }

    /* Validate the cluster list. A route reflector should ignore a route
     * if its cluster id is present in the cluster list. */
    pu1ClusterList = pu1Buf + BGP4_ATYPE_FLAGS_LEN +
        BGP4_ATYPE_CODE_LEN + BGP4_GET_ATTRIBUTE_LEN (u1Flag);

    if (RflClusterListValidation (BGP4_PEER_CXT_ID (pPeerentry), pu1ClusterList,
                                  (UINT2) (u2AttribLen / CLUSTER_ID_LENGTH)) !=
        BGP4_SUCCESS)
    {
        /* Received Cluster id contains the RR cluster id. Hence
         * ignore this update message. No need to send any error message
         * to the peer. */
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Received Cluster ID contains "
                       "route reflector Cluster ID.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        return (BGP4_INVALID_CLUS_LIST);
    }

    /* Fill the BgpInfo with the received Community Attribute */
    CLUSTER_LIST_CREATE (pClusList);
    if (pClusList == NULL)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG,
                       BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                       BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Memory Allocation for Ext-Community "
                       "Attribute FAILED!!!\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        gu4BgpDebugCnt[MAX_BGP_CLUSTER_LIST_SIZING_ID]++;
        return (BGP4_RSRC_ALLOC_FAIL);
    }

    ATTRIBUTE_NODE_CREATE (pu1Str);
    if (pu1Str == NULL)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG,
                       BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                       BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Memory Allocation for Ext-Community "
                       "Attribute FAILED!!!\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        CLUSTER_LIST_FREE (pClusList);
        gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
        return (BGP4_RSRC_ALLOC_FAIL);
    }
    BGP4_INFO_CLUS_LIST_ATTR (pBGP4Info) = pClusList;
    BGP4_INFO_CLUS_LIST_ATTR_VAL (pBGP4Info) = pu1Str;
    BGP4_INFO_CLUS_LIST_COUNT (pBGP4Info) =
        (UINT2) (u2AttribLen / CLUSTER_ID_LENGTH);
    BGP4_INFO_CLUS_LIST_ATTR_FLAG (pBGP4Info) = u1Flag;
    if (u2AttribLen > MAX_BGP_ATTR_LEN)
    {
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG,
                       BGP4_CONTROL_PATH_TRC | BGP4_OS_RESOURCE_TRC |
                       BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Cluster attribute length %d is greater than maximum"
                       " attribute length. Hence truncating the cluster list to maximum length\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))), u2AttribLen);
        MEMCPY (BGP4_INFO_CLUS_LIST_ATTR_VAL (pBGP4Info),
                pu1Buf + (i4TotalAttrLen - u2AttribLen), MAX_BGP_ATTR_LEN);
    }
    else
    {
        MEMCPY (BGP4_INFO_CLUS_LIST_ATTR_VAL (pBGP4Info), pu1Buf +
                (i4TotalAttrLen - u2AttribLen), u2AttribLen);
    }
    /* Set the CLUSLIST_MASK indicating that the CLUSTER-ID List attribute is
     * present in this route info. */
    BGP4_INFO_ATTR_FLAG (pBGP4Info) |= (BGP4_ATTR_CLUS_LIST_MASK);
    return i4TotalAttrLen;
}

/******************************************************************************/
/* Function Name : Bgp4MsghProcessUnknown                                     */
/* Description   : This function validates and stores the unknown attributes  */
/*                 in the given BGP4_INFO structure from the receivied        */
/*                 UPDATE message.                                            */
/* Input(s)      : Peer which has sent us the UPDATE message (pPeerentry),    */
/*                 Buffer containing the UPDATE message (pu1Buf),             */
/* Output(s)     : Updated BGP4 Information structure (pBGP4Info)             */
/* Return(s)     : Number of bytes processed in the buffer.                   */
/******************************************************************************/
INT4
Bgp4MsghProcessUnknown (tBgp4PeerEntry * pPeerentry,
                        tBgp4Info * pBGP4Info, UINT1 *pu1Buf,
                        INT4 i4RemPattrLen)
{
    UINT1              *pu1Str = NULL;
    INT4                i4Prevlen;
    UINT1               u1Flag;
    INT2                i2Len = 0;

    if (((*pu1Buf) & BGP4_EXT_LEN_FLAG_MASK) == BGP4_EXT_LEN_FLAG_MASK)
    {
        PTR_FETCH2 (i2Len, (pu1Buf + BGP4_ATYPE_FLAGS_LEN +
                            BGP4_ATYPE_CODE_LEN));
        i2Len += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            (BGP4_EXTENDED_ATTR_LEN_SIZE);
    }
    else
    {
        i2Len =
            *(UINT1 *) (pu1Buf + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN);
        i2Len +=
            BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN + BGP4_AL_UNKNOWN_SIZE;
    }

    u1Flag = *pu1Buf;

    if (i2Len > i4RemPattrLen)
    {
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Invalid unknown "
                       " Attribute Length - Length: %d\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))), i2Len);
        Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR, BGP4_ATTR_LEN_ERR,
                         pu1Buf, i2Len);
        return (BGP4_FAILURE);
    }

    if ((u1Flag & BGP4_OPTIONAL_FLAG_MASK) != BGP4_OPTIONAL_FLAG_MASK)
    {
        /* Optional flag not set. So it is a unrecognised well known
         * attribute. Send appropriate error message. */
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Unrecognized Well-Known "
                       "Attribute\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        Bgp4EhSendError (pPeerentry, BGP4_UPDATE_MSG_ERR,
                         BGP4_UNRECOGNISED_WELLKNOWN_ATTR, pu1Buf, i2Len);
        return (BGP4_FAILURE);
    }

    if ((u1Flag & BGP4_OPTIONAL_FLAG_MASK) == BGP4_OPTIONAL_FLAG_MASK)
    {

        /* If an optional non-transitive attribute is unrecognized, then it
         * should be ignored. If an optional transitive attribute is
         * unrecognized, then the partial bit in the attribute flag is set.*/
        if ((u1Flag & BGP4_TRANSITIVE_FLAG_MASK) != BGP4_TRANSITIVE_FLAG_MASK)
        {
            /* unrecognized Optional non-transitive attribute. */
            return i2Len;
        }
        else
        {
            /* unrecognized Optional transitive attribute */
            *(pu1Buf) |= BGP4_PARTIAL_FLAG_MASK;
        }
    }

    if (BGP4_INFO_UNKNOWN_ATTR (pBGP4Info) != NULL)
    {
        /* Update the previous unknown attribute length */
        i4Prevlen = BGP4_INFO_UNKNOWN_ATTR_LEN (pBGP4Info);
        ATTRIBUTE_NODE_CREATE (pu1Str);
        if (pu1Str == NULL)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG,
                           BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                           BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Memory Allocation for Unknown "
                           "Attribute FAILED!!!\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
            return (BGP4_RSRC_ALLOC_FAIL);
        }
        MEMCPY ((UINT1 *) pu1Str, BGP4_INFO_UNKNOWN_ATTR (pBGP4Info),
                i4Prevlen);

        /* Release the earlier allocated memory */
        ATTRIBUTE_NODE_FREE (BGP4_INFO_UNKNOWN_ATTR (pBGP4Info));
        BGP4_INFO_UNKNOWN_ATTR (pBGP4Info) = pu1Str;
    }
    else
    {
        i4Prevlen = 0;
        BGP4_INFO_UNKNOWN_ATTR_LEN (pBGP4Info) = 0;
        ATTRIBUTE_NODE_CREATE (BGP4_INFO_UNKNOWN_ATTR (pBGP4Info));
        if (BGP4_INFO_UNKNOWN_ATTR (pBGP4Info) == NULL)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG,
                           BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                           BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Memory Allocation for Unknown "
                           "Attribute FAILED!!!\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
            return (BGP4_RSRC_ALLOC_FAIL);
        }
    }
    MEMCPY ((BGP4_INFO_UNKNOWN_ATTR (pBGP4Info) + i4Prevlen), pu1Buf, i2Len);
    BGP4_INFO_UNKNOWN_ATTR_LEN (pBGP4Info) += i2Len;
    BGP4_INFO_ATTR_FLAG (pBGP4Info) |= BGP4_ATTR_UNKNOWN_MASK;
    return ((INT4) i2Len);
}

/******************************************************************************/
/* Function Name : Bgp4MsghCheckFlag                                          */
/* Description   : This function whether the flags set for the particular     */
/*                 attribute is proper or not. If the flags is improper       */
/*                 then NOTIFICATION message with ATTR_ERROR will be send.    */
/* Input(s)      : Buffer containing the UPDATE message (pu1Buf),             */
/*                 Flag which needs to be checked for (u1Flag),               */
/*                 Attribute value (u1Attr).                                  */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_FAILURE if the flag is a valid one,                   */
/*                 size of error data which needs to be send in the           */
/*                 notification message from the buffer(pu1Buf).              */
/******************************************************************************/
INT4
Bgp4MsghCheckFlag (UINT1 u1Flag, UINT1 u1Attr, UINT1 *pu1Buf)
{
    INT4                i4Retval;
    INT4                i4Len;
    INT2                i2Tmp;

    switch (u1Attr)
    {
        case BGP4_ATTR_ORIGIN:
            /* Well Known Mandotory attribute */
            if (((u1Flag) & BGP4_OPTIONAL_FLAG_MASK)
                || (((u1Flag) & BGP4_TRANSITIVE_FLAG_MASK) !=
                    BGP4_TRANSITIVE_FLAG_MASK)
                || (((u1Flag) & BGP4_PARTIAL_FLAG_MASK))
                || ((u1Flag) & BGP4_EXT_LEN_FLAG_MASK))
            {
                i4Retval = BGP4_FAILURE;
            }
            else
            {
                i4Retval = BGP4_SUCCESS;
            }
            break;

        case BGP4_ATTR_PATH:
            /* Well Known Mandotory attribute */
            if (((u1Flag) & BGP4_OPTIONAL_FLAG_MASK)
                || (((u1Flag) & BGP4_TRANSITIVE_FLAG_MASK) !=
                    BGP4_TRANSITIVE_FLAG_MASK)
                || (((u1Flag) & BGP4_PARTIAL_FLAG_MASK)))
            {
                i4Retval = BGP4_FAILURE;
            }
            else
            {
                i4Retval = BGP4_SUCCESS;
            }
            break;

        case BGP4_ATTR_NEXT_HOP:
            /* Well Known Mandotory attribute */
            if (((u1Flag) & BGP4_OPTIONAL_FLAG_MASK)
                || (((u1Flag) & BGP4_TRANSITIVE_FLAG_MASK) !=
                    BGP4_TRANSITIVE_FLAG_MASK)
                || (((u1Flag) & BGP4_PARTIAL_FLAG_MASK))
                || ((u1Flag) & BGP4_EXT_LEN_FLAG_MASK))
            {
                i4Retval = BGP4_FAILURE;
            }
            else
            {
                i4Retval = BGP4_SUCCESS;
            }
            break;

        case BGP4_ATTR_MED:
            /* Optional Non-Transitive Attribute */
            if ((((u1Flag) & BGP4_OPTIONAL_FLAG_MASK) !=
                 BGP4_OPTIONAL_FLAG_MASK)
                || (((u1Flag) & BGP4_TRANSITIVE_FLAG_MASK))
                || (((u1Flag) & BGP4_PARTIAL_FLAG_MASK))
                || ((u1Flag) & BGP4_EXT_LEN_FLAG_MASK))
            {
                i4Retval = BGP4_FAILURE;
            }
            else
            {
                i4Retval = BGP4_SUCCESS;
            }
            break;

        case BGP4_ATTR_LOCAL_PREF:
            /* Well known attribute */
            if (((u1Flag) & BGP4_OPTIONAL_FLAG_MASK)
                || (((u1Flag) & BGP4_TRANSITIVE_FLAG_MASK) !=
                    BGP4_TRANSITIVE_FLAG_MASK)
                || (((u1Flag) & BGP4_PARTIAL_FLAG_MASK))
                || ((u1Flag) & BGP4_EXT_LEN_FLAG_MASK))
            {
                i4Retval = BGP4_FAILURE;
            }
            else
            {
                i4Retval = BGP4_SUCCESS;
            }
            break;

        case BGP4_ATTR_ATOMIC_AGGR:
            /* Well known discretionary attribute */
            if (((u1Flag) & BGP4_OPTIONAL_FLAG_MASK)
                || (((u1Flag) & BGP4_TRANSITIVE_FLAG_MASK) !=
                    BGP4_TRANSITIVE_FLAG_MASK)
                || (((u1Flag) & BGP4_PARTIAL_FLAG_MASK))
                || ((u1Flag) & BGP4_EXT_LEN_FLAG_MASK))
            {
                i4Retval = BGP4_FAILURE;
            }
            else
            {
                i4Retval = BGP4_SUCCESS;
            }
            break;

        case BGP4_ATTR_AGGREGATOR:
            /* Optional Transitive attribute */
            if ((((u1Flag) & BGP4_OPTIONAL_FLAG_MASK) !=
                 BGP4_OPTIONAL_FLAG_MASK)
                || (((u1Flag) & BGP4_TRANSITIVE_FLAG_MASK) !=
                    BGP4_TRANSITIVE_FLAG_MASK)
                || ((u1Flag) & BGP4_EXT_LEN_FLAG_MASK))
            {
                i4Retval = BGP4_FAILURE;
            }
            else
            {
                i4Retval = BGP4_SUCCESS;
            }
            break;

        case BGP4_ATTR_COMM:
            /* Optional Transitive attribute */
            if ((((u1Flag) & BGP4_OPTIONAL_FLAG_MASK) !=
                 BGP4_OPTIONAL_FLAG_MASK)
                || (((u1Flag) & BGP4_TRANSITIVE_FLAG_MASK) !=
                    BGP4_TRANSITIVE_FLAG_MASK))
            {
                i4Retval = BGP4_FAILURE;
            }
            else
            {
                i4Retval = BGP4_SUCCESS;
            }
            break;

        case BGP4_ATTR_ORIG_ID:
            /* Optional Non-Transitive attribute */
            if ((((u1Flag) & BGP4_OPTIONAL_FLAG_MASK) !=
                 BGP4_OPTIONAL_FLAG_MASK)
                || ((u1Flag) & BGP4_TRANSITIVE_FLAG_MASK)
                || ((u1Flag) & BGP4_PARTIAL_FLAG_MASK)
                || ((u1Flag) & BGP4_EXT_LEN_FLAG_MASK))
            {
                i4Retval = BGP4_FAILURE;
            }
            else
            {
                i4Retval = BGP4_SUCCESS;
            }
            break;

        case BGP4_ATTR_CLUS_LIST:
            /* Optional Non-Transitive attribute */
            if ((((u1Flag) & BGP4_OPTIONAL_FLAG_MASK) !=
                 BGP4_OPTIONAL_FLAG_MASK)
                || ((u1Flag) & BGP4_TRANSITIVE_FLAG_MASK)
                || ((u1Flag) & BGP4_PARTIAL_FLAG_MASK))
            {
                i4Retval = BGP4_FAILURE;
            }
            else
            {
                i4Retval = BGP4_SUCCESS;
            }
            break;

        case BGP4_ATTR_MP_REACH_NLRI:
        case BGP4_ATTR_MP_UNREACH_NLRI:
            /* Optional Non-Transitive attribute */
            if ((u1Flag & BGP4_OPTIONAL_FLAG_MASK) &&
                ((u1Flag & BGP4_TRANSITIVE_FLAG_MASK) !=
                 BGP4_TRANSITIVE_FLAG_MASK))
            {
                i4Retval = BGP4_SUCCESS;
            }
            else
            {
                i4Retval = BGP4_FAILURE;
            }
            break;

        case BGP4_ATTR_ECOMM:
            /* Optional Transitive attribute */
            if ((((u1Flag) & BGP4_OPTIONAL_FLAG_MASK) !=
                 BGP4_OPTIONAL_FLAG_MASK)
                || (((u1Flag) & BGP4_TRANSITIVE_FLAG_MASK) !=
                    BGP4_TRANSITIVE_FLAG_MASK))
            {
                i4Retval = BGP4_FAILURE;
            }
            else
            {
                i4Retval = BGP4_SUCCESS;
            }
            break;

        case BGP4_ATTR_PATH_FOUR:
            /* Optional Transitive attribute */
            if ((((u1Flag) & BGP4_OPTIONAL_FLAG_MASK) !=
                 BGP4_OPTIONAL_FLAG_MASK)
                || (((u1Flag) & BGP4_TRANSITIVE_FLAG_MASK) !=
                    BGP4_TRANSITIVE_FLAG_MASK))
            {
                i4Retval = BGP4_FAILURE;
            }
            else
            {
                i4Retval = BGP4_SUCCESS;
            }
            break;

        case BGP4_ATTR_AGGREGATOR_FOUR:
            /* Optional Transitive attribute */
            if ((((u1Flag) & BGP4_OPTIONAL_FLAG_MASK) !=
                 BGP4_OPTIONAL_FLAG_MASK)
                || (((u1Flag) & BGP4_TRANSITIVE_FLAG_MASK) !=
                    BGP4_TRANSITIVE_FLAG_MASK)
                || ((u1Flag) & BGP4_EXT_LEN_FLAG_MASK))
            {
                i4Retval = BGP4_FAILURE;
            }
            else
            {
                i4Retval = BGP4_SUCCESS;
            }
            break;

        default:
            i4Retval = BGP4_SUCCESS;
            break;
    }
    if (i4Retval == BGP4_FAILURE)
    {
        if (((u1Flag) & BGP4_EXT_LEN_FLAG_MASK) == BGP4_EXT_LEN_FLAG_MASK)
        {
            PTR_FETCH2 (i2Tmp, (pu1Buf + BGP4_ATYPE_FLAGS_LEN +
                                BGP4_ATYPE_CODE_LEN));
            i4Len = (i2Tmp);
            i4Len +=
                BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN + sizeof (UINT2);
        }
        else
        {
            i4Len = *(UINT1 *) (pu1Buf + BGP4_ATYPE_FLAGS_LEN +
                                BGP4_ATYPE_CODE_LEN);

            i4Len +=
                BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN + sizeof (UINT1);
        }
        return (i4Len);
    }
    return (BGP4_FAILURE);
}

/******************************************************************************/
/* Function Name : Bgp4MhDisplayRouteInfo                                     */
/* Description   : This function prints routing information in the received   */
/* Input(s)      : Peer from which the route is learnt.                       */
/*                 Feasible Route list containing feasible routes             */
/*                 Withdrawn Route list containing withdrawn routes           */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS                                               */
/******************************************************************************/
INT4
Bgp4MhDisplayRouteInfo (tBgp4PeerEntry * pPeer, tTMO_SLL * pFeasibleroutes,
                        tTMO_SLL * pWithdrawnroutes)
{
    tLinkNode          *pLinknode = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tAsPath            *pASPath = NULL;

    if ((BGP4_TRC_FLAG & BGP4_DUMP_HGH_TRC) == 0)
    {
        /* High level Dump Trace not enabled. So return. */
        return BGP4_SUCCESS;
    }

    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                   BGP4_TRC_FLAG, BGP4_DUMP_HGH_TRC, BGP4_MOD_NAME,
                   "\tPEER %s : UPDATE Message Details.....\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO (pPeer->
                                                                  peerConfig.
                                                                  RemoteAddrInfo)));
    if (TMO_SLL_Count (pWithdrawnroutes) > 0)
    {
        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                  BGP4_TRC_FLAG, BGP4_DUMP_HGH_TRC, BGP4_MOD_NAME,
                  "\tUNFEASIBLE Routes Details.....\n");
    }

    TMO_SLL_Scan (pWithdrawnroutes, pLinknode, tLinkNode *)
    {
        pRtProfile = pLinknode->pRouteProfile;
        if ((BGP4_RT_GET_FLAGS (pRtProfile) &
             BGP4_MPE_RT_ADVT_MP_UNREACH) != BGP4_MPE_RT_ADVT_MP_UNREACH)
        {
            BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                           BGP4_TRC_FLAG, BGP4_DUMP_HGH_TRC, BGP4_MOD_NAME,
                           "\t<ipv4, unicast> NLRI - [%s/%d] - Withdrawn , Peer : %s\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           BGP4_RT_PREFIXLEN (pRtProfile),
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (pPeer->peerConfig.
                                             RemoteAddrInfo)));
        }
    }

    if (TMO_SLL_Count (pFeasibleroutes) == 0)
    {
        return BGP4_SUCCESS;
    }
    pRtProfile = ((tLinkNode *) TMO_SLL_First (pFeasibleroutes))->pRouteProfile;
    BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
              BGP4_TRC_FLAG, BGP4_DUMP_HGH_TRC, BGP4_MOD_NAME,
              "\tPath Attribute Details follows .....\n");
    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                   BGP4_TRC_FLAG, BGP4_DUMP_HGH_TRC, BGP4_MOD_NAME,
                   "\tOrigin : [%d]\n",
                   BGP4_INFO_ORIGIN (BGP4_RT_BGP_INFO (pRtProfile)));
    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                   BGP4_TRC_FLAG, BGP4_DUMP_HGH_TRC, BGP4_MOD_NAME,
                   "\tOrigin : [%d]\n",
                   BGP4_INFO_ORIGIN (BGP4_RT_BGP_INFO (pRtProfile)));
    BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
              BGP4_TRC_FLAG, BGP4_DUMP_HGH_TRC, BGP4_MOD_NAME,
              "\tAS_PATH : ...\n");
    TMO_SLL_Scan (&
                  (((tBgp4Info *) (pRtProfile->pRtInfo))->TSASPath),
                  pASPath, tAsPath *)
    {
        UINT4              *pu4ASNo = NULL;
        UINT4               u4Length = 0;
        UINT4               u4Index;
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                       BGP4_TRC_FLAG, BGP4_DUMP_HGH_TRC, BGP4_MOD_NAME,
                       "\t\tPath Segment - Type = [%d], Length = [%d]\n",
                       pASPath->u1Type, pASPath->u1Length);
        pu4ASNo = (UINT4 *) (VOID *) pASPath->au1ASSegs;

        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                  BGP4_TRC_FLAG, BGP4_DUMP_HGH_TRC, BGP4_MOD_NAME, "\t\t");
        for (u4Index = 0; u4Index < pASPath->u1Length; u4Index++)
        {
            if (u4Length == BGP4_AS_PATH_PRINT)
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                               BGP4_TRC_FLAG, BGP4_DUMP_HGH_TRC, BGP4_MOD_NAME,
                               "\n\t\t%u, ", OSIX_NTOHL (*(pu4ASNo + u4Index)));
                u4Length = 0;
            }
            else
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                               BGP4_TRC_FLAG, BGP4_DUMP_HGH_TRC, BGP4_MOD_NAME,
                               "%u, ", OSIX_NTOHL (*(pu4ASNo + u4Index)));
            }
            u4Length++;
        }
        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                  BGP4_TRC_FLAG, BGP4_DUMP_HGH_TRC, BGP4_MOD_NAME, "\n");
    }
    if (((BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pRtProfile)) &
          BGP4_ATTR_NEXT_HOP_MASK)) == BGP4_ATTR_NEXT_HOP_MASK)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                       BGP4_TRC_FLAG, BGP4_DUMP_HGH_TRC, BGP4_MOD_NAME,
                       "\tNextHOP : [%s]\n",
                       Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP
                                        (BGP4_RT_BGP_INFO (pRtProfile)),
                                        BGP4_INET_AFI_IPV4));
    }
    if (((BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pRtProfile)) &
          BGP4_ATTR_MED_MASK)) == BGP4_ATTR_MED_MASK)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                       BGP4_TRC_FLAG, BGP4_DUMP_HGH_TRC, BGP4_MOD_NAME,
                       "\tMED : [%d]\n",
                       BGP4_INFO_RCVD_MED (BGP4_RT_BGP_INFO (pRtProfile)));
    }
    if (((BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pRtProfile)) &
          BGP4_ATTR_LOCAL_PREF_MASK)) == BGP4_ATTR_LOCAL_PREF_MASK)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                       BGP4_TRC_FLAG, BGP4_DUMP_HGH_TRC, BGP4_MOD_NAME,
                       "\tLocal Preference : [%d]\n ",
                       BGP4_INFO_RCVD_LOCAL_PREF
                       (BGP4_RT_BGP_INFO (pRtProfile)));
    }
    if (((BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pRtProfile)) &
          BGP4_ATTR_ATOMIC_AGGR_MASK)) == BGP4_ATTR_ATOMIC_AGGR_MASK)
    {
        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                  BGP4_TRC_FLAG, BGP4_DUMP_HGH_TRC, BGP4_MOD_NAME,
                  "\tRoute received with Atomic Aggregator bit set\n");
    }
    if (((BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pRtProfile)) &
          BGP4_ATTR_AGGREGATOR_MASK)) == BGP4_ATTR_AGGREGATOR_MASK)
    {
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                       BGP4_TRC_FLAG, BGP4_DUMP_HGH_TRC, BGP4_MOD_NAME,
                       "\tAggregator - ASNo = [%d], IP Address = [%x]\n",
                       BGP4_INFO_AGGREGATOR_AS (BGP4_RT_BGP_INFO (pRtProfile)),
                       BGP4_INFO_AGGREGATOR_NODE (BGP4_RT_BGP_INFO
                                                  (pRtProfile)));
    }
    BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
              BGP4_TRC_FLAG, BGP4_DUMP_HGH_TRC, BGP4_MOD_NAME,
              "\tFEASIBLE Routes Details .....\n");
    TMO_SLL_Scan (pFeasibleroutes, pLinknode, tLinkNode *)
    {
        pRtProfile = pLinknode->pRouteProfile;
        if ((BGP4_RT_GET_FLAGS (pRtProfile) &
             BGP4_MPE_RT_ADVT_MP_REACH) != BGP4_MPE_RT_ADVT_MP_REACH)
        {
            BGP4_DBG3 (BGP4_DBG_RX,
                       "\t<ipv4, unicast> NLRI - [%s/%d] - Nlri, Peer %s\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       BGP4_RT_PREFIXLEN (pRtProfile),
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO (pPeer->
                                                                      peerConfig.
                                                                      RemoteAddrInfo)));
        }
    }
    return BGP4_SUCCESS;
}

#endif /* BGP4MSGH_C */
