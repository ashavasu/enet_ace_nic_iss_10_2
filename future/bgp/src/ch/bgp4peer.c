/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4peer.c,v 1.43 2017/09/15 06:19:52 siva Exp $
 *
 * Description: This file contains the peer related processing
 *              functions.
 *
 *******************************************************************/
#ifndef _BGP4PEER_C
#define _BGP4PEER_C

#include "bgp4com.h"

/*****************************************************************************/
/* FUNCTION NAME : Bgp4ProcessPeerTransmissions                              */
/* DESCRIPTION   : This function checks the Tx Queue of each peer for BGP    */
/*                 messages, tranmists them over the socket and clears the Tx*/
/*                 Queues. Also stores the message that needs to be re-trans-*/
/*                 -mitted, if the socket Tx queue overflows and retransmits */
/*                 it after some delay.                                      */
/* INPUTS        : None                                                      */
/* OUTPUTS       : None                                                      */
/* Return        : BGP4_TRUE  - if more routes or peers remains to be        */
/*                 processed.                                                */
/*               : BGP4_FALSE - if no more routes or peers available for     */
/*                 processing                                                */
/*                 BGP4_RELINQUISH - if external events requires CPU         */
/*****************************************************************************/
INT4
Bgp4ProcessPeerTransmissions (UINT4 u4Context)
{
    /* Variable declarations */
    tPeerNode          *pPeerNode = NULL;
    tPeerNode          *pNextPeerNode = NULL;
    tBgp4PeerEntry     *pPeerentry = NULL;
    tBufNode           *pBufNode = NULL;
    UINT1              *pu1CurBuf = NULL;
    UINT4               u4msgCnt = 0;
    UINT4               u4IsFirstIteration = BGP4_TRUE;
    UINT4               u4MaxMsgPerPeer = 0;
    UINT4               u4PeermsgCnt = 0;
    UINT4               u4ProcessPktSize = 0;
    UINT4               u4IsMorePeer = 0;
    INT4                i4UpdateMessageSentStatus = BGP4_FAILURE;
    INT4                i4CurBufSize = 0;

    if (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_DOWN)
    {
        return BGP4_FALSE;
    }

    u4IsMorePeer = 0;
    u4msgCnt = 0;

    for (;;)                    /* Loop for processing the Peer's from beginning of the
                                 * Transmission List */
    {

        /* Since each peer need to receive some data at specified time, in
         * this loop we need to provide equal preference to all peers. The
         * disadvantage of this will be, say some peer might have more msg
         * to be tx'ed while some have few or no message to be tx'ed.
         */
        pPeerNode =
            (tPeerNode *) TMO_SLL_First (BGP4_PEER_TRANS_LIST (u4Context));
        if (pPeerNode == NULL)
        {
            return BGP4_FALSE;
        }
        pPeerentry = pPeerNode->pPeer;
        pNextPeerNode = NULL;

        /* Set the MaxMsg to be processed per peer. This ensure that all peers
         * will be processed atleast in this loop. */
        u4MaxMsgPerPeer = (BGP4_MAX_PEER_PKTS2TX /
                           TMO_SLL_Count (BGP4_PEER_TRANS_LIST (u4Context)));

        for (;;)                /* Loop for processing all the Peers. */
        {
            u4PeermsgCnt = 0;
            u4ProcessPktSize = 0;

            pNextPeerNode =
                (tPeerNode *) TMO_SLL_Next (BGP4_PEER_TRANS_LIST (u4Context),
                                            &pPeerNode->TSNext);
            for (;;)            /* Loop for processing messages from each peer. */
            {
                if (u4IsFirstIteration == BGP4_FALSE)
                {
                    /* This peer has already been processed once. Check the
                     * peer status to find whether the peer can be processed
                     * further or not. */
                    if ((BGP4_GET_PEER_PEND_FLAG (pPeerentry) &
                         BGP4_PEER_ADVT_MSG) == BGP4_PEER_ADVT_MSG)
                    {
                        /* Need to process this peer for transmission. */
                        BGP4_RESET_PEER_PEND_FLAG (pPeerentry,
                                                   BGP4_PEER_ADVT_MSG);
                    }
                    else
                    {
                        /* No need to process this peer again. */
                        break;
                    }
                }
                else
                {
                    /* Just reset the peer advt pend status. May be this flag 
                     * set during previous call might not have been resetted.*/
                    BGP4_RESET_PEER_PEND_FLAG (pPeerentry, BGP4_PEER_ADVT_MSG);
                }

                if (BGP4_PEER_CONN_ID (pPeerentry) == BGP4_INV_CONN_ID)
                {
                    /* Connection is not established. So no need to
                     * advertise to this peer. */
                    break;
                }

                /* Check if there is any message needs to be re-transmitted. 
                 * If yes then wait for TCP Send buffer to become ready for 
                 * write operation.
                 */
                if (BGP4_PEER_READVT_BUFFER (pPeerentry) != NULL)
                {
                    break;
                }

                /* Check whether max allowed message size has been processed or
                 * not. If yes, then break. Remaining message will be processed
                 * in next cycle. */
                if (u4ProcessPktSize >= BGP4_MAX_TXBUF2PROCESS)
                {
                    /* Has processed MAX messages for Transmission */
                    break;
                }

                /* Check if any message is available for transmission. */
                pBufNode = (tBufNode *) TMO_SLL_First (BGP4_PEER_ADVT_MSG_LIST
                                                       (pPeerentry));
                if (pBufNode == NULL)
                {
                    /* No more message available for transmission. Remove
                     * the peer from the Peer Tranx List. */
                    TMO_SLL_Delete (BGP4_PEER_TRANS_LIST (u4Context),
                                    &pPeerNode->TSNext);
                    BGP4_PEER_IS_IN_TRANX_LIST (pPeerentry) = BGP4_FALSE;
                    BGP_PEER_NODE_FREE (pPeerNode);
                    pPeerNode = NULL;
                    break;
                }
                /* Check whether max number of packets has been processed for
                 * this peer.*/
                if (u4PeermsgCnt >= u4MaxMsgPerPeer)
                {
                    /* Set the Peer Advt pend status for this peer. */
                    BGP4_SET_PEER_PEND_FLAG (pPeerentry, BGP4_PEER_ADVT_MSG);
                    u4IsMorePeer++;
                    break;
                }

                i4CurBufSize =
                    (INT4) (BGP4_PEER_MSG_SIZE_ADVT_BUF_NODE (pBufNode) -
                            BGP4_PEER_MSG_OFFSET_ADVT_BUF_NODE (pBufNode));

                pu1CurBuf = BGP4_PEER_MSG_IN_ADVT_BUF_NODE (pBufNode) +
                    BGP4_PEER_MSG_OFFSET_ADVT_BUF_NODE (pBufNode);

                i4UpdateMessageSentStatus =
                    Bgp4TcphSendData (pPeerentry, pu1CurBuf,
                                      (UINT4) i4CurBufSize);

                if (i4UpdateMessageSentStatus != i4CurBufSize)
                {
                    if (i4UpdateMessageSentStatus == BGP4_FAILURE)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                  BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC,
                                  BGP4_MOD_NAME,
                                  "\t Bgp4ProcessPeerTransmissions:"
                                  " Failure in sending Update message\n");
                        TMO_SLL_Delete (BGP4_PEER_ADVT_MSG_LIST (pPeerentry),
                                        &pBufNode->TSNext);
                        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                            BGP4_PEER_MSG_IN_ADVT_BUF_NODE
                                            (pBufNode));
                        MemReleaseMemBlock (gBgpNode.Bgp4BufNodesPoolId,
                                            (UINT1 *) pBufNode);
                        Bgp4IphHandleControl (pPeerentry, BGP4_TCP_ERROR);
                    }
                    else if (i4UpdateMessageSentStatus == BGP_EWOULDBLOCK)
                    {
                        /* Send fails. Store update message in ReTxmit buffer. 
                         * Add the corresponding socket in write socket fd_set,
                         * to enable re-transmission of buffer when write 
                         * socket is ready. */
                        TMO_SLL_Delete (BGP4_PEER_ADVT_MSG_LIST (pPeerentry),
                                        &pBufNode->TSNext);
                        BGP4_PEER_READVT_BUFFER (pPeerentry) =
                            (UINT1 *) pBufNode;
                        BGP_FD_SET (BGP4_PEER_CONN_ID (pPeerentry),
                                    BGP4_WRITE_SOCK_FD_SET (u4Context));
                    }
                    else
                    {
                        /* Only partial msg has been sent. Update the offset
                         * field for next transmission */
                        u4ProcessPktSize += (UINT4) i4UpdateMessageSentStatus;
                        BGP4_PEER_MSG_OFFSET_ADVT_BUF_NODE (pBufNode) +=
                            (UINT4) i4UpdateMessageSentStatus;
                    }
                    break;
                }

                /* Increment the peer message count and processed msg size. */
                u4msgCnt++;
                u4PeermsgCnt++;
                u4ProcessPktSize += (UINT4) i4UpdateMessageSentStatus;

                /* Check for the peer's current state. If the current state is
                 * ESTABLISHED, since a message has been sent to the peer
                 * sending of keep alive message can be differed. */
                if (BGP4_PEER_STATE (pPeerentry) == BGP4_ESTABLISHED_STATE)
                {
                    Bgp4TmrhStopTimer (BGP4_KEEPALIVE_TIMER,
                                       (VOID *) pPeerentry);
                    Bgp4TmrhStartTimer (BGP4_KEEPALIVE_TIMER,
                                        (VOID *) pPeerentry,
                                        BGP4_PEER_NEG_KEEP_ALIVE (pPeerentry));
                }
                TMO_SLL_Delete (BGP4_PEER_ADVT_MSG_LIST (pPeerentry),
                                &pBufNode->TSNext);
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                    BGP4_PEER_MSG_IN_ADVT_BUF_NODE (pBufNode));
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodesPoolId,
                                    (UINT1 *) pBufNode);
                pBufNode = NULL;
            }

            /* Check whether we have processed the maximum permitable msgs. */
            if (u4msgCnt >= BGP4_MAX_PEER_PKTS2TX)
            {
                /* Has reached the MAX messages to be transmitted. Check for
                 * Tic Break event. If return value is BGP4_TURE, then the
                 * environment may require the CPU. So BGP should relinquish
                 * the CPU. */
                if (Bgp4IsCpuNeeded () == BGP4_TRUE)
                {
                    /* External events requires CPU. Relinquish CPU. */
                    return (BGP4_RELINQUISH);
                }
                else
                {
                    if (u4IsMorePeer > 0)
                    {
                        /* Still messages are pending for transmission. */
                        return (BGP4_TRUE);
                    }
                    else
                    {
                        /* No more message left to be transmitted. */
                        return (BGP4_FALSE);
                    }
                }
            }

            if (pNextPeerNode == NULL)
            {
                /* Has finished processing, one loop. Check whether it is
                 * possible to process more message. If yes, start again from
                 * the beginning. */
                if (u4IsMorePeer > 0)
                {
                    /* Start processing from the first peer in Tranx List. */
                    u4IsMorePeer = 0;
                    u4IsFirstIteration = BGP4_FALSE;
                    break;
                }
                else
                {
                    /* No more message left for transmission. */
                    return BGP4_FALSE;
                }
            }

            /* Process the next Peer. */
            pPeerNode = pNextPeerNode;
            pPeerentry = pPeerNode->pPeer;
            pNextPeerNode = NULL;
        }
    }
}

/****************************************************************************
 * FUNCTION NAME : Bgp4PeerCanInitRouteBeAdvt   
 * DESCRIPTION   : This function check whether the given route can be       
 *               : advertises to the peer which is going through
 *               : initialization.
 * INPUTS        : pPeerInfo - Peer to which this route is to be advertised.
 *               : pRtProfile - Pointer to Route that needs to be advertised.
 * OUTPUTS       : None
 * RETURNS       : BGP4_TRUE - if the route can be advertised.  
 *               : BGP4_FALSE - if route need not be advertised.
 ***************************************************************************/
UINT4
Bgp4PeerCanInitRouteBeAdvt (tBgp4PeerEntry * pPeerInfo,
                            tRouteProfile * pRtProfile)
{
    tAddrPrefix         InvPrefix;
    INT4                i4AggrStatus = BGP4_FAILURE;
    UINT4               u4Index = 0;
    UINT4               u4PeerType = 0;

    u4PeerType = BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerInfo), pPeerInfo);
    Bgp4InitAddrPrefixStruct (&(InvPrefix),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO
                              (BGP4_PEER_INIT_PREFIX_INFO (pPeerInfo)));

    /* Check for the aggregation policy. */
    i4AggrStatus = Bgp4AggrCanAggregationBeDone (pRtProfile, &u4Index, u4Index);
    if (i4AggrStatus == BGP4_SUCCESS)
    {
        if (BGP4_RT_PREFIXLEN (pRtProfile) ==
            BGP4_AGGRENTRY_PREFIXLEN (u4Index))
        {
            if ((BGP4_AGGR_GET_STATUS ((&(BGP4_AGGRENTRY[u4Index]))) &
                 BGP4_AGGR_CREATE_IN_PROGRESS) == BGP4_AGGR_CREATE_IN_PROGRESS)
            {
                /* Advertise this route. Aggregate route may be advertised
                 * once creation is complete. */
                i4AggrStatus = BGP4_FAILURE;
            }
            else if ((BGP4_AGGR_GET_STATUS ((&(BGP4_AGGRENTRY[u4Index]))) &
                      BGP4_AGGR_DELETE_IN_PROGRESS) ==
                     BGP4_AGGR_DELETE_IN_PROGRESS)
            {
                /* Need to advertise this route. */
                i4AggrStatus = BGP4_FAILURE;
            }
            else if ((((BGP4_RT_GET_FLAGS
                        (BGP4_AGGRENTRY_AGGR_ROUTE (u4Index))) &
                       (BGP4_RT_AGGREGATED)) == BGP4_RT_AGGREGATED) ||
                     ((BGP4_AGGRENTRY_AGGR_STATUS (u4Index) &
                       BGP4_AGGR_REAGGREGATE) == BGP4_AGGR_REAGGREGATE))
            {
                if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
                {
                    /* No need to advt this route. Aggrgated route
                     * should be advt for this peer. */
                }
                else
                {
                    /* Need to advt this route. */
                    i4AggrStatus = BGP4_FAILURE;
                }
            }
        }
        else
        {
            /* Route can be aggregated and u4Index points to the index of
             * corresponding configured aggregation policy. */
            if ((BGP4_AGGR_GET_STATUS ((&(BGP4_AGGRENTRY[u4Index]))) &
                 BGP4_AGGR_CREATE_IN_PROGRESS) == BGP4_AGGR_CREATE_IN_PROGRESS)
            {
                if (((PrefixMatch ((BGP4_AGGRENTRY_INIT_PREFIX_INFO (u4Index)),
                                   InvPrefix) == BGP4_TRUE) &&
                     (BGP4_AGGRENTRY_INIT_PREFIX_LEN (u4Index) ==
                      BGP4_MIN_PREFIXLEN)) ||
                    (AddrCompare
                     ((BGP4_RT_NET_ADDRESS_INFO (pRtProfile).NetAddr),
                      BGP4_RT_PREFIXLEN (pRtProfile),
                      (BGP4_AGGRENTRY_INIT_PREFIX_INFO (u4Index)),
                      BGP4_AGGRENTRY_INIT_PREFIX_LEN (u4Index)) > 0))
                {
                    /* Route is either more-specific to the stored init
                     * route or stored init route is 0. Advertise the
                     * route to peer. */
                    i4AggrStatus = BGP4_FAILURE;
                }
                else
                {
                    if (BGP4_AGGRENTRY_ADV_TYPE (u4Index) == BGP4_SUMMARY)
                    {
                        /* No need to advertise this route. Aggregate route
                         * will be advertised once creat is completed. */
                    }
                    else
                    {
                        /* Need to advt this route. */
                        i4AggrStatus = BGP4_FAILURE;
                    }
                }
            }
            else if ((BGP4_AGGR_GET_STATUS ((&(BGP4_AGGRENTRY[u4Index]))) &
                      BGP4_AGGR_DELETE_IN_PROGRESS) ==
                     BGP4_AGGR_DELETE_IN_PROGRESS)
            {
                /* Need to advt this route. */
                i4AggrStatus = BGP4_FAILURE;
            }
            else
            {
                if (BGP4_AGGRENTRY_ADV_TYPE (u4Index) == BGP4_SUMMARY)
                {
                    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGREGATED)
                        != BGP4_RT_AGGREGATED)
                    {
                        /* Need to advt this route. */
                        i4AggrStatus = BGP4_FAILURE;
                    }
                    /* Else No need to advertise this route. */
                }
                else
                {
                    /* Need to advt this route. */
                    i4AggrStatus = BGP4_FAILURE;
                }
            }
        }
    }
    /* if i4AggrStatus is BGP4_FAILURE, then either no aggregation entry
     * matches the route or the route can be advertised to the
     * peer. */
    if (i4AggrStatus == BGP4_FAILURE)
    {
        /* 1) If the initialising peer is external peer and the route is
         * present in the FIB/EXT_PEER_ADVT LIST, then the route will
         * be advertised via normal path and no need to add to
         * init advertisement list. Else need to add the route to the 
         * init advertisement list. */
        /* 2) Else if the initialising peer is internal peer and the route
         * is present in the INT_PEER_ADVT LIST, then the route will
         * be advertised via normal path and no need to add to
         * init advertisement list. Else need to add the route to the 
         * init advertisement list. */
        /* 3) Else check if the route is learn from a BGP peer and if that
         * peer is going through De-init then dont consider this route
         * for advertisement. */
        if (((u4PeerType == BGP4_EXTERNAL_PEER) &&
             (((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_IN_FIB_UPD_LIST)
               == BGP4_RT_IN_FIB_UPD_LIST) ||
              ((BGP4_RT_GET_FLAGS (pRtProfile) &
                BGP4_RT_IN_EXT_PEER_ADVT_LIST)
               == BGP4_RT_IN_EXT_PEER_ADVT_LIST))) ||
            ((u4PeerType == BGP4_INTERNAL_PEER) &&
             ((BGP4_RT_GET_FLAGS (pRtProfile) &
               BGP4_RT_IN_INT_PEER_ADVT_LIST) ==
              BGP4_RT_IN_INT_PEER_ADVT_LIST)) ||
            ((BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID) &&
             (BGP4_GET_PEER_CURRENT_STATE
              (BGP4_RT_PEER_ENTRY (pRtProfile)) ==
              BGP4_PEER_DEINIT_INPROGRESS)))
        {
            /* Under all these conditions no need to add the route to
             * the init advertisement list. */
        }
        else
        {
            return BGP4_TRUE;
        }
    }
    return BGP4_FALSE;
}

/****************************************************************************
 * FUNCTION NAME : Bgp4PeerProcessSoftInitRoutes
 * DESCRIPTION   : This function  obtains BGP routes from the Local RIB and 
 *               : advertises them to the BGP peer, with whom a session is 
 *               : newly established. Update messages are formed from the route
 *               : updates and put in the peer Tx queue. 
 * INPUTS        : pPeerInfo - Pointer to the BGP peer entry
 *               : u4MaxRtCnt - The maximum no. of routes that can be processed
 *               : in a given tic 
 * OUTPUTS       : None
 * RETURNS       : BGP4_SUCCESS/BGP4_FAILURE/BGP4_SOFTRECONFIG_COMPLETE
 ***************************************************************************/
INT4
Bgp4PeerProcessSoftInitRoutes (tBgp4PeerEntry * pPeerInfo, UINT4 u4MaxRtCnt)
{
    /* Variable Declarations */
    tTMO_SLL            PeerFeasRtList;
    tTMO_SLL            DummyWithRtList;
    tLinkNode          *pRtLink = NULL;
    tLinkNode          *pTempLinkNode = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pNextRt = NULL;
    UINT4               u4RtCnt = 0;
    INT4                i4Status = BGP4_FAILURE;
    INT4                i4InitStatus = BGP4_FAILURE;
#ifdef L3VPN
    UINT4               u4Context = 0;
    INT4                i4RetVal = 0;
#endif

    TMO_SLL_Init (&PeerFeasRtList);
    TMO_SLL_Init (&DummyWithRtList);

    /* Check whether the peer soft initialisation is in progress or completed.
     * If initialisation completed then return */
    if ((BGP4_GET_PEER_CURRENT_STATE (pPeerInfo) == BGP4_PEER_READY) ||
        ((BGP4_PEER_SOFTCONFIG_OUTBOUND_AFI (pPeerInfo) == 0) &&
         (BGP4_PEER_SOFTCONFIG_OUTBOUND_SAFI (pPeerInfo) == 0)) ||
        (BGP4_LOCAL_ADMIN_STATUS (BGP4_PEER_CXT_ID (pPeerInfo)) ==
         BGP4_ADMIN_DOWN))
    {
        /* Peer still present in Peer Init list. But opertion is set as
         * completed. Means peer initialisation is completed. Needs to
         * remove this peer from the peer init list. */
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s : Peer soft intialisation is already completed "
                       "but peer still present peer init list. Need to remove from it.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))));
        return BGP4_SOFTRECONFIG_COMPLETE;
    }

    if ((BGP4_PEER_INIT_PA_ENTRY (pPeerInfo) == NULL) &&
        (BGP4_PEER_INIT_PA_HASHKEY (pPeerInfo) == 0))
    {
#ifdef L3VPN
        /* Get the first best route from RCVD_PA_DB. */
        if (BGP4_PEER_SOFTCONFIG_OUTBOUND_SAFI (pPeerInfo)
            == BGP4_INET_SAFI_VPNV4_UNICAST)
        {
            i4RetVal = BgpGetFirstActiveContextID (&u4Context);

            while (i4RetVal == SNMP_SUCCESS)
            {
                i4Status = Bgp4RcvdPADBGetFirstBestRoute (u4Context,
                                                          &pRtProfile,
                                                          BGP4_PEER_SOFTCONFIG_OUTBOUND_AFI
                                                          (pPeerInfo),
                                                          BGP4_PEER_SOFTCONFIG_OUTBOUND_SAFI
                                                          (pPeerInfo));
                if (pRtProfile != NULL)
                {
                    break;
                }
                i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
            }
        }
        else
        {
#endif
            i4Status =
                Bgp4RcvdPADBGetFirstBestRoute (BGP4_PEER_CXT_ID (pPeerInfo),
                                               &pRtProfile,
                                               BGP4_PEER_SOFTCONFIG_OUTBOUND_AFI
                                               (pPeerInfo),
                                               BGP4_PEER_SOFTCONFIG_OUTBOUND_SAFI
                                               (pPeerInfo));
#ifdef L3VPN
        }
#endif
        if (i4Status == BGP4_FAILURE)
        {
            /* No route present for this <AFI,SAFI> */
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : No route is present in the peer for this "
                           "address family. Soft reconfiguration processing is not done\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            return BGP4_SOFTRECONFIG_COMPLETE;
        }
    }
    else
    {
        /* Get the first best route from the RCVD_PA_ENTRY stored in
         * Peer Init structure. */
        if ((BGP4_PEER_INIT_PA_HASHKEY (pPeerInfo) ==
             BGP4_MAX_RCVD_PA_HASH_INDEX) &&
            (BGP4_PEER_INIT_PA_ENTRY (pPeerInfo) == NULL))
        {
            /* This means during the interval between successive call to
             * this routine, the previously stored RCVD_PA_ENTRY is being
             * deleted and there is no more RCVD_PA_ENTRY exists.
             */
            return BGP4_SOFTRECONFIG_COMPLETE;
        }
        else
        {
#ifdef L3VPN
            u4Context = BGP4_PEER_INIT_PE_CONTEXT (pPeerInfo);
#endif
            pRtProfile = BGP4_PA_IDENT_BEST_ROUTE_FIRST
                (BGP4_PEER_INIT_PA_ENTRY (pPeerInfo));
            /* It is possible that in the mean time between storing this info
             * in the peer Init structure and this call, the best route in this
             * list might have been moved to Non-Best route list and no
             * more best route is present in this list. In such condition
             * try and get the next Best route and use it. */
            if (pRtProfile == NULL)
            {
                pRtProfile = BGP4_PA_IDENT_NON_BEST_ROUTE_LAST
                    (BGP4_PEER_INIT_PA_ENTRY (pPeerInfo));
                i4Status = Bgp4RcvdPADBGetNextBestRoute (pRtProfile, &pNextRt);
                if (i4Status == BGP4_SUCCESS)
                {
                    pRtProfile = pNextRt;
                    pNextRt = NULL;
                }
            }
        }
    }

    for (;;)
    {
        /* Check for the Route can be advertised. */
        if (Bgp4PeerCanInitRouteBeAdvt (pPeerInfo, pRtProfile) == BGP4_TRUE)
        {
            /* Route can be advertised. */
            Bgp4DshAddRouteToList (&PeerFeasRtList, pRtProfile, 0);
        }
        /* Increment the processed route count. */
        u4RtCnt++;

        i4Status = Bgp4RcvdPADBGetNextBestRoute (pRtProfile, &pNextRt);
#ifdef L3VPN
        if ((i4Status == BGP4_FAILURE) &&
            (BGP4_PEER_SOFTCONFIG_OUTBOUND_SAFI (pPeerInfo)
             == BGP4_INET_SAFI_VPNV4_UNICAST))
        {
            if (BgpGetNextActiveContextID (u4Context, &u4Context) !=
                SNMP_SUCCESS)
            {
                i4InitStatus = BGP4_SOFTRECONFIG_COMPLETE;
                break;
            }
            while (i4RetVal == SNMP_SUCCESS)
            {
                Bgp4RcvdPADBGetFirstBestRoute (u4Context, &pNextRt,
                                               BGP4_PEER_SOFTCONFIG_OUTBOUND_AFI
                                               (pPeerInfo),
                                               BGP4_PEER_SOFTCONFIG_OUTBOUND_SAFI
                                               (pPeerInfo));
                if (pNextRt != NULL)
                {
                    break;
                }

                i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
            }
            if (pNextRt == NULL)
            {
                i4InitStatus = BGP4_SOFTRECONFIG_COMPLETE;
                break;
            }

        }
        else if (i4Status == BGP4_FAILURE)
#else
        if (i4Status == BGP4_FAILURE)
#endif
        {
            /* No more routes present for processing. */
            i4InitStatus = BGP4_SOFTRECONFIG_COMPLETE;
            break;
        }

        /* Check whether route count has reached the max count value only if
         * the RCVD_PA_ENTRY for current route and next route are different.
         * This ensures that all the routes with the identical PA will be
         * packed in a single update message. */
        if ((BGP4_INFO_RCVD_PA_ENTRY (BGP4_RT_BGP_INFO (pRtProfile)) !=
             BGP4_INFO_RCVD_PA_ENTRY (BGP4_RT_BGP_INFO (pNextRt))) &&
            (u4RtCnt > u4MaxRtCnt))
        {
            /* Store the next RCVD_PA_ENTRY information in the peerentry info */
            BGP4_PEER_INIT_PA_ENTRY (pPeerInfo) =
                BGP4_INFO_RCVD_PA_ENTRY (BGP4_RT_BGP_INFO (pNextRt));
            Bgp4PAFormHashKey
                (BGP4_PA_BGP4INFO (BGP4_PEER_INIT_PA_ENTRY (pPeerInfo)),
                 &(BGP4_PEER_INIT_PA_HASHKEY (pPeerInfo)),
                 BGP4_MAX_RCVD_PA_HASH_INDEX);
#ifdef L3VPN
            BGP4_PEER_INIT_PE_CONTEXT (pPeerInfo) = u4Context;
#endif
            break;
        }
        pRtProfile = pNextRt;
        pNextRt = NULL;
    }

    /* Add route list to peer Tx Q */
    Bgp4PeerSendRoutesToPeerTxQ (pPeerInfo, &PeerFeasRtList, &DummyWithRtList,
                                 BGP4_FALSE);

    /*  The flags NOTTO_EXTERNAL and NOTTO_INTERNAL needs to be reset now. */
    BGP_SLL_DYN_Scan (&PeerFeasRtList, pRtLink, pTempLinkNode, tLinkNode *)
    {
        BGP4_RT_RESET_FLAG (pRtLink->pRouteProfile,
                            (BGP4_RT_ADVT_NOTTO_INTERNAL |
                             BGP4_RT_ADVT_NOTTO_EXTERNAL));
        Bgp4DshRemoveNodeFromList (&PeerFeasRtList, pRtLink);
    }

    Bgp4DshReleaseList (&DummyWithRtList, 0);

    if (i4InitStatus == BGP4_SOFTRECONFIG_COMPLETE)
    {
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s : Peer soft intialisation is completed for the peer.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))));
        return i4InitStatus;
    }
    else
    {
        return BGP4_SUCCESS;
    }
}

/****************************************************************************
 * FUNCTION NAME : Bgp4PeerProcessInitRoutes
 * DESCRIPTION   : This function  obtains BGP routes from the Local RIB and 
 *               : advertises them to the BGP peer, with whom a session is 
 *               : newly established. Update messages are formed from the route
 *               : updates and put in the peer Tx queue. 
 * INPUTS        : pPeerInfo - Pointer to the BGP peer entry
 *               : u4MaxRtCnt - The maximum no. of routes that can be processed
 *               : in a given tic 
 * OUTPUTS       : None
 * RETURNS       : BGP4_SUCCESS/BGP4_FAILURE/BGP4_INIT_COMPLETE 
 ***************************************************************************/
INT4
Bgp4PeerProcessInitRoutes (tBgp4PeerEntry * pPeerInfo, UINT4 u4MaxRtCnt)
{
    /* Variable Declarations */
    tTMO_SLL            PeerFeasRtList;
    tTMO_SLL            DummyWithRtList;
    tLinkNode          *pRtLink = NULL;
    tLinkNode          *pTempLinkNode = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pNextRt = NULL;
    UINT4               u4RtCnt = 0;
    UINT4               u4AsafiIndex = 0;
    UINT4               u4AsafiMask = 0;
    INT4                i4Status = BGP4_FAILURE;
    INT4                i4InitStatus = BGP4_FAILURE;
#ifdef L3VPN
    INT4                i4RetVal = BGP4_FAILURE;
    UINT4               u4Context = 0;
#endif
    UINT2               u2Afi = 0;
    UINT2               u2Safi = 0;

    TMO_SLL_Init (&PeerFeasRtList);
    TMO_SLL_Init (&DummyWithRtList);

    u2Afi = BGP4_PEER_INIT_AFI (pPeerInfo);
    u2Safi = BGP4_PEER_INIT_SAFI (pPeerInfo);
    Bgp4GetAfiSafiIndex (u2Afi, u2Safi, &u4AsafiIndex);

    /* Check whether the peer initialisation is in progress or completed. If
     * initialisation completed then return */
    if ((BGP4_GET_PEER_CURRENT_STATE (pPeerInfo) == BGP4_PEER_READY) ||
        ((u2Afi == 0) && (u2Safi == 0)) ||
        (BGP4_LOCAL_ADMIN_STATUS (BGP4_PEER_CXT_ID (pPeerInfo)) ==
         BGP4_ADMIN_DOWN))
    {
        /* Peer still present in Peer Init list. But opertion is set as
         * completed. Means peer initialisation is completed. Needs to
         * remove this peer from the peer init list. */
        return BGP4_INIT_COMPLETE;
    }

#ifdef L3VPN
    if ((u4AsafiIndex == BGP4_VPN4_UNI_INDEX) &&
        (BGP4_VPN4_PEER_ROLE (pPeerInfo) == BGP4_VPN4_CE_PEER))
    {
        /* The peer is a CE peer, then advertise the routes present in the
         * VRF correspond to this CE peer 
         */
        BGP4_PEER_INIT_AFI (pPeerInfo) = BGP4_INET_AFI_IPV4;
        BGP4_PEER_INIT_SAFI (pPeerInfo) = BGP4_INET_SAFI_VPNV4_UNICAST;
        i4Status = Bgp4Vpnv4CEPeerInit (pPeerInfo, u4MaxRtCnt);
        if (i4Status != BGP4_INIT_COMPLETE)
        {
            return (BGP4_SUCCESS);
        }
        /* Right now after vpnv4 family, there are no other supported families.
         * hence return complete, otherwise peer init afi and saif must be
         * updated accordingly inside the above called function
         */
        return (BGP4_INIT_COMPLETE);
    }
#endif

    if ((BGP4_PEER_INIT_PA_ENTRY (pPeerInfo) == NULL) &&
        (BGP4_PEER_INIT_PA_HASHKEY (pPeerInfo) == 0))
    {
#ifdef L3VPN
        if (u4AsafiIndex == BGP4_VPN4_UNI_INDEX)
        {
            i4RetVal = BgpGetFirstActiveContextID (&u4Context);

            while (i4RetVal == SNMP_SUCCESS)
            {
                Bgp4RcvdPADBGetFirstBestRoute (u4Context,
                                               &pRtProfile, u2Afi, u2Safi);
                if (pRtProfile != NULL)
                {
                    break;
                }

                i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
            }
        }
        else
        {
#endif
            /* Get the first best route from RCVD_PA_DB. */
            Bgp4RcvdPADBGetFirstBestRoute (BGP4_PEER_CXT_ID (pPeerInfo),
                                           &pRtProfile,
                                           BGP4_PEER_INIT_AFI (pPeerInfo),
                                           BGP4_PEER_INIT_SAFI (pPeerInfo));
#ifdef L3VPN
        }
#endif
    }
    else
    {
        /* Get the first best route from the RCVD_PA_ENTRY stored in
         * Peer Init structure. */
        if ((BGP4_PEER_INIT_PA_HASHKEY (pPeerInfo) ==
             BGP4_MAX_RCVD_PA_HASH_INDEX) &&
            (BGP4_PEER_INIT_PA_ENTRY (pPeerInfo) == NULL))
        {
            /* This means during the interval between successive call to
             * this routine, the previously stored RCVD_PA_ENTRY is being
             * deleted and there is no more RCVD_PA_ENTRY exists.
             * Now try and get the First Route in the next <AFI, SAFI>.*/
        }
        else
        {
            pRtProfile = BGP4_PA_IDENT_BEST_ROUTE_FIRST
                (BGP4_PEER_INIT_PA_ENTRY (pPeerInfo));
            /* It is possible that in the mean time between storing this info
             * in the peer Init structure and this call, the best route in this
             * list might have been moved to Non-Best route list and no
             * more best route is present in this list. In such condition
             * try and get the next Best route and use it. */
#ifdef L3VPN
            u4Context = BGP4_PEER_INIT_PE_CONTEXT (pPeerInfo);
#endif
            if (pRtProfile == NULL)
            {
                pRtProfile = BGP4_PA_IDENT_NON_BEST_ROUTE_LAST
                    (BGP4_PEER_INIT_PA_ENTRY (pPeerInfo));
                i4Status = Bgp4RcvdPADBGetNextBestRoute (pRtProfile, &pNextRt);
                if (i4Status == BGP4_SUCCESS)
                {
                    pRtProfile = pNextRt;
                    pNextRt = NULL;
                }
            }
        }
    }

    if (pRtProfile == NULL)
    {
        /* Route is not found for current <AFI,AFI>. Try and get the best route
         * for next <AFI,SAFI> */
        for (u4AsafiIndex++; u4AsafiIndex < BGP4_MPE_ADDR_FAMILY_MAX_INDEX;
             u4AsafiIndex++)
        {
            /* Check whether this <AFI,SAFI> is negotiated.
             * If negotiated try and get the first route. If first
             * route not present or if <AFI,SAFI> not negotiated
             * go for next <AFI,SAFI> */
            i4Status = Bgp4GetAfiSafiFromIndex (u4AsafiIndex, &u2Afi, &u2Safi);
            if (i4Status == BGP4_FAILURE)
            {
                continue;
            }
            u4AsafiMask = ((UINT4) u2Afi << BGP4_TWO_BYTE_BITS) | u2Safi;
            switch (u4AsafiMask)
            {
                case CAP_MP_IPV4_UNICAST:
                    /* This case cannot occur. Since we always start with
                     * IPV4_UNICAST. */
                    break;

#ifdef BGP4_IPV6_WANTED
                case CAP_MP_IPV6_UNICAST:
                    if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) &
                         CAP_NEG_IPV6_UNI_MASK) == CAP_NEG_IPV6_UNI_MASK)
                    {
                        Bgp4RcvdPADBGetFirstBestRoute (BGP4_PEER_CXT_ID
                                                       (pPeerInfo), &pRtProfile,
                                                       u2Afi, u2Safi);
                    }
                    break;
#endif
#ifdef L3VPN
                case CAP_MP_LABELLED_IPV4:
                    /*Since both Ipv4 Unicast and Labelled Ipv4 routes
                     * are in the same RIB, both are put in the same PADB.
                     * So Lablled routes are advertised in Ipv4 unicast 
                     * processing itself*/
                    break;

                case CAP_MP_VPN4_UNICAST:
                    if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) &
                         CAP_NEG_VPN4_UNI_MASK) == CAP_NEG_VPN4_UNI_MASK)
                    {
                        i4RetVal = BgpGetFirstActiveContextID (&u4Context);

                        while (i4RetVal == SNMP_SUCCESS)
                        {
                            Bgp4RcvdPADBGetFirstBestRoute (u4Context,
                                                           &pRtProfile, u2Afi,
                                                           u2Safi);
                            if (pRtProfile != NULL)
                            {
                                break;
                            }
                            i4RetVal = BgpGetNextActiveContextID (u4Context,
                                                                  &u4Context);
                        }
                        break;
                    }

                    if (BGP4_VPN4_PEER_ROLE (pPeerInfo) == BGP4_VPN4_CE_PEER)
                    {
                        /* The peer is a CE peer, then advertise the routes 
                         * present in the VRF correspond to this CE peer 
                         */
                        BGP4_PEER_INIT_AFI (pPeerInfo) = BGP4_INET_AFI_IPV4;
                        BGP4_PEER_INIT_SAFI (pPeerInfo) =
                            BGP4_INET_SAFI_VPNV4_UNICAST;
                        Bgp4InitNetAddressStruct (&
                                                  (BGP4_PEER_INIT_NETADDR_INFO
                                                   (pPeerInfo)),
                                                  BGP4_INET_AFI_IPV4,
                                                  BGP4_INET_SAFI_VPNV4_UNICAST);
                        i4Status = Bgp4Vpnv4CEPeerInit (pPeerInfo, u4MaxRtCnt);
                        if (i4Status != BGP4_INIT_COMPLETE)
                        {
                            return (BGP4_SUCCESS);
                        }
                        /* Right now after vpnv4 family, there are no other 
                         * supported families. hence return complete, 
                         * otherwise peer init afi and saif must be
                         * updated accordingly inside the above called function
                         */
                        return (BGP4_INIT_COMPLETE);
                    }
                    break;
#endif
#ifdef VPLSADS_WANTED
                case CAP_MP_L2VPN_VPLS:
                    if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) &
                         CAP_NEG_L2VPN_VPLS_MASK) == CAP_NEG_L2VPN_VPLS_MASK)
                    {
                        Bgp4RcvdPADBGetFirstBestRoute (BGP4_PEER_CXT_ID
                                                       (pPeerInfo), &pRtProfile,
                                                       u2Afi, u2Safi);
                    }
                    break;
#endif
#ifdef EVPN_WANTED
                case CAP_MP_L2VPN_EVPN:
                    if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) &
                         CAP_NEG_L2VPN_EVPN_MASK) == CAP_NEG_L2VPN_EVPN_MASK)
                    {
                        Bgp4RcvdPADBGetFirstBestRoute (BGP4_PEER_CXT_ID
                                                       (pPeerInfo), &pRtProfile,
                                                       u2Afi, u2Safi);
                    }
                    break;
#endif
                default:
                    break;
            }
#if defined (BGP4_IPV6_WANTED) || defined (L3VPN)
            if (pRtProfile != NULL)
            {
                /* First Route is found. */
                break;
            }
#endif
        }

        if (pRtProfile == NULL)
        {
            /* No more route is present. */
            return BGP4_INIT_COMPLETE;
        }
    }

    for (;;)
    {
        /* Check for the Route can be advertised. */
        if (Bgp4PeerCanInitRouteBeAdvt (pPeerInfo, pRtProfile) == BGP4_TRUE)
        {
            /* Route can be advertised. */
            Bgp4DshAddRouteToList (&PeerFeasRtList, pRtProfile, 0);
        }
        /* Increment the processed route count. */
        u4RtCnt++;

        i4Status = Bgp4RcvdPADBGetNextBestRoute (pRtProfile, &pNextRt);
#ifdef L3VPN
        if ((i4Status == BGP4_FAILURE) &&
            (u4AsafiMask == CAP_MP_VPN4_UNICAST) &&
            ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) &
              CAP_NEG_VPN4_UNI_MASK) == CAP_NEG_VPN4_UNI_MASK))
        {
            if (BgpGetNextActiveContextID (u4Context, &u4Context) !=
                SNMP_SUCCESS)
            {
                i4InitStatus = BGP4_INIT_COMPLETE;
                break;
            }
            while (i4RetVal == SNMP_SUCCESS)
            {
                Bgp4RcvdPADBGetFirstBestRoute (u4Context, &pNextRt, u2Afi,
                                               u2Safi);
                if (pNextRt != NULL)
                {
                    break;
                }

                i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
            }
            if (pNextRt == NULL)
            {
                i4InitStatus = BGP4_INIT_COMPLETE;
                break;
            }

        }
        else if (i4Status == BGP4_FAILURE)
#else
        if (i4Status == BGP4_FAILURE)
#endif
        {
            /* No more route present for this <AFI,SAFI>. Try to get the
             * first Best Route in other <AFI,SAFI> */
            for (u4AsafiIndex++; u4AsafiIndex < BGP4_MPE_ADDR_FAMILY_MAX_INDEX;
                 u4AsafiIndex++)
            {
                /* Check whether this <AFI,SAFI> is negotiated.
                 * If negotiated try and get the first route. If first
                 * route not present or if <AFI,SAFI> not negotiated
                 * go for next <AFI,SAFI> */
                i4Status =
                    Bgp4GetAfiSafiFromIndex (u4AsafiIndex, &u2Afi, &u2Safi);
                if (i4Status == BGP4_FAILURE)
                {
                    continue;
                }
                u4AsafiMask = ((UINT4) u2Afi << BGP4_TWO_BYTE_BITS) | u2Safi;
                switch (u4AsafiMask)
                {
                    case CAP_MP_IPV4_UNICAST:
                        /* This case cannot occur. Since we always start with
                         * IPV4_UNICAST. */
                        break;

#ifdef BGP4_IPV6_WANTED
                    case CAP_MP_IPV6_UNICAST:
                        if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) &
                             CAP_NEG_IPV6_UNI_MASK) == CAP_NEG_IPV6_UNI_MASK)
                        {
                            Bgp4RcvdPADBGetFirstBestRoute (BGP4_PEER_CXT_ID
                                                           (pPeerInfo),
                                                           &pNextRt, u2Afi,
                                                           u2Safi);
                        }
                        break;
#endif
#ifdef L3VPN
                    case CAP_MP_LABELLED_IPV4:
                        /*Since both Ipv4 Unicast and Labelled Ipv4 routes
                         * are in the same RIB, both are put in the same PADB.
                         * So Lablled routes are advertised in Ipv4 unicast 
                         * processing itself*/
                        break;
                    case CAP_MP_VPN4_UNICAST:
                        if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) &
                             CAP_NEG_VPN4_UNI_MASK) == CAP_NEG_VPN4_UNI_MASK)
                        {
                            i4RetVal = BgpGetFirstActiveContextID (&u4Context);

                            while (i4RetVal == SNMP_SUCCESS)
                            {
                                Bgp4RcvdPADBGetFirstBestRoute (u4Context,
                                                               &pNextRt, u2Afi,
                                                               u2Safi);
                                if (pNextRt != NULL)
                                {
                                    break;
                                }
                                i4RetVal = BgpGetNextActiveContextID (u4Context,
                                                                      &u4Context);
                            }
                            break;
                        }
                        if (BGP4_VPN4_PEER_ROLE (pPeerInfo) ==
                            BGP4_VPN4_CE_PEER)
                        {
                            /* The peer is a CE peer, then advertise the 
                             * routes present in the VRF correspond to 
                             * this CE peer 
                             */
                            BGP4_PEER_INIT_AFI (pPeerInfo) = BGP4_INET_AFI_IPV4;
                            BGP4_PEER_INIT_SAFI (pPeerInfo) =
                                BGP4_INET_SAFI_VPNV4_UNICAST;
                            Bgp4InitNetAddressStruct (&
                                                      (BGP4_PEER_INIT_NETADDR_INFO
                                                       (pPeerInfo)),
                                                      BGP4_INET_AFI_IPV4,
                                                      BGP4_INET_SAFI_VPNV4_UNICAST);
                            i4Status =
                                Bgp4Vpnv4CEPeerInit (pPeerInfo, u4MaxRtCnt);
                            if (i4Status != BGP4_INIT_COMPLETE)
                            {
                                return (BGP4_SUCCESS);
                            }
                            /* Right now after vpnv4 family, there are no 
                             * other * supported families. hence return 
                             * complete, otherwise peer init afi and saif 
                             * must be updated accordingly inside the 
                             * above called function
                             */
                            return (BGP4_INIT_COMPLETE);
                        }
                        break;
#endif
#ifdef VPLSADS_WANTED
                    case CAP_MP_L2VPN_VPLS:
                        if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) &
                             CAP_NEG_L2VPN_VPLS_MASK) ==
                            CAP_NEG_L2VPN_VPLS_MASK)
                        {
                            Bgp4RcvdPADBGetFirstBestRoute (BGP4_PEER_CXT_ID
                                                           (pPeerInfo),
                                                           &pNextRt, u2Afi,
                                                           u2Safi);
                        }
                        break;
#endif
#ifdef EVPN_WANTED
                    case CAP_MP_L2VPN_EVPN:
                        if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) &
                             CAP_NEG_L2VPN_EVPN_MASK) ==
                            CAP_NEG_L2VPN_EVPN_MASK)
                        {
                            Bgp4RcvdPADBGetFirstBestRoute (BGP4_PEER_CXT_ID
                                                           (pPeerInfo),
                                                           &pNextRt, u2Afi,
                                                           u2Safi);
                        }
                        break;
#endif
                    default:
                        break;
                }
                if (pNextRt != NULL)
                {
                    /* Next Route is found. */
                    break;
                }
            }

            if (pNextRt == NULL)
            {
                /* No more routes present for processing. */
                i4InitStatus = BGP4_INIT_COMPLETE;
                break;
            }
        }

        /* Check whether route count has reached the max count value only if
         * the RCVD_PA_ENTRY for current route and next route are different.
         * This ensures that all the routes with the identical PA will be
         * packed in a single update message. */
        if ((BGP4_INFO_RCVD_PA_ENTRY (BGP4_RT_BGP_INFO (pRtProfile)) !=
             BGP4_INFO_RCVD_PA_ENTRY (BGP4_RT_BGP_INFO (pNextRt))) &&
            (u4RtCnt > u4MaxRtCnt))
        {
            /* Store the next RCVD_PA_ENTRY information in the peerentry info */
            BGP4_PEER_INIT_PA_ENTRY (pPeerInfo) =
                BGP4_INFO_RCVD_PA_ENTRY (BGP4_RT_BGP_INFO (pNextRt));
            Bgp4PAFormHashKey
                (BGP4_PA_BGP4INFO (BGP4_PEER_INIT_PA_ENTRY (pPeerInfo)),
                 &(BGP4_PEER_INIT_PA_HASHKEY (pPeerInfo)),
                 BGP4_MAX_RCVD_PA_HASH_INDEX);
            BGP4_PEER_INIT_AFI (pPeerInfo) = u2Afi;
            BGP4_PEER_INIT_SAFI (pPeerInfo) = u2Safi;
#ifdef L3VPN
            BGP4_PEER_INIT_PE_CONTEXT (pPeerInfo) = u4Context;
#endif
            break;
        }
        pRtProfile = pNextRt;
        pNextRt = NULL;
    }

    /* Add route list to peer Tx Q */
    Bgp4PeerSendRoutesToPeerTxQ (pPeerInfo, &PeerFeasRtList, &DummyWithRtList,
                                 BGP4_FALSE);

    /*  The flags NOTTO_EXTERNAL and NOTTO_INTERNAL needs to be reset now. */
    BGP_SLL_DYN_Scan (&PeerFeasRtList, pRtLink, pTempLinkNode, tLinkNode *)
    {
        BGP4_RT_RESET_FLAG (pRtLink->pRouteProfile,
                            (BGP4_RT_ADVT_NOTTO_INTERNAL |
                             BGP4_RT_ADVT_NOTTO_EXTERNAL));
        Bgp4DshRemoveNodeFromList (&PeerFeasRtList, pRtLink);
    }

    Bgp4DshReleaseList (&DummyWithRtList, 0);

    if (i4InitStatus == BGP4_INIT_COMPLETE)
    {
        return i4InitStatus;
    }
    else
    {
        return BGP4_SUCCESS;
    }
}

/****************************************************************************
 * FUNCTION NAME : Bgp4PeerProcessDeInitRoutes
 * DESCRIPTION   : This function removes the routes advertised by the peer 
 *               : from the Local RIB, updates the FIB and advertises these
 *               : routes as withdrawn to other peers. For this purpose,
 *               : the update messages are formed and put in the peer Tx queue.
 * INPUTS        : pPeerInfo - Pointer to the BGP peer entry
 *               : u4MaxRtCnt - The maximum no. of routes that can be processed
 *               : in a given tic 
 * OUTPUTS       : None
 * RETURNS       : BGP4_SUCCESS/BGP4_FAILURE/BGP4_DEINIT_COMPLETE 
 ***************************************************************************/
INT4
Bgp4PeerProcessDeInitRoutes (tBgp4PeerEntry * pPeerInfo, UINT4 u4MaxRtCnt,
                             UINT4 u4Context)
{
    /* Variable Declarations */
    tRouteProfile      *pPeerRt = NULL;
    UINT4               u4AFIndex = BGP4_IPV4_UNI_INDEX;
    UINT4               u4RtCnt = 0;
    UINT1               u1CanFIBUpdated = BGP4_FALSE;

    /* Check whether the peer de-initialisation is in progress or completed.
     * If de-initialisation has completed then return */
    if ((BGP4_GET_PEER_CURRENT_STATE (pPeerInfo) == BGP4_PEER_READY) ||
        ((BGP4_GET_PEER_PEND_FLAG (pPeerInfo) &
          BGP4_DEINIT_PEERLISTS_INPROGRESS) ==
         BGP4_DEINIT_PEERLISTS_INPROGRESS) ||
        (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_DOWN))
    {
        /* Peer still present in Peer DeInit list. But opertion is set as 
         * completed. Means peer de-initialisation is completed. Needs to
         * remove this peer from the peer deinit list. */
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Peer de-initialisation is completed but it is in "
                       "peer de-init list. Need to remove it from the list.\r\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))));
        return BGP4_DEINIT_COMPLETE;
    }

    /* Check if there is any route received from this peer is present in RIB or
     * not. Query this using the Peer-Route List. */
    for (u4AFIndex = BGP4_IPV4_UNI_INDEX;
         u4AFIndex < BGP4_MPE_ADDR_FAMILY_MAX_INDEX; u4AFIndex++)
    {
        if (BGP4_PEER_AFI_SAFI_INSTANCE (pPeerInfo, u4AFIndex) == NULL)
        {
            /* AFI-SAFI not negotiated for this peer. */
            continue;
        }
        for (;;)
        {
            pPeerRt = BGP4_DLL_FIRST_ROUTE (BGP4_PEER_ROUTE_LIST (pPeerInfo,
                                                                  u4AFIndex));
            if (pPeerRt == NULL)
            {
                /* No more route from this peer is present in RIB's for
                 * this <AFI> <SAFI>. Try and process the next <AFI> <SAFI>*/
                break;
            }

            u1CanFIBUpdated = BGP4_FALSE;

            if ((((BGP4_RT_GET_FLAGS (pPeerRt) & BGP4_RT_BEST) ==
                  BGP4_RT_BEST) ||
                 (BGP4_RT_GET_EXT_FLAGS (pPeerRt) & BGP4_RT_MULTIPATH) ==
                 BGP4_RT_MULTIPATH)
                && ((BGP4_RT_GET_EXT_FLAGS (pPeerRt) & BGP4_RT_IN_RTM) ==
                    BGP4_RT_IN_RTM)
                && ((BGP4_RT_GET_FLAGS (pPeerRt) & BGP4_RT_IN_FIB_UPD_LIST) !=
                    BGP4_RT_IN_FIB_UPD_LIST))
            {
                /* Peer route is best. Need to remove it from FIB. */
                u1CanFIBUpdated = BGP4_TRUE;
            }

            BGP4_RT_REF_COUNT (pPeerRt)++;
            if ((BGP4_RT_GET_FLAGS (pPeerRt) & BGP4_RT_HISTORY) ==
                BGP4_RT_HISTORY)
            {
                BGP4_RT_RESET_FLAG (pPeerRt, BGP4_RT_HISTORY);
                Bgp4RibhDelRtEntry (pPeerRt, NULL, u4Context, BGP4_HISTORY);
            }
            else
            {
                BGP4_RT_SET_FLAG (pPeerRt, BGP4_RT_WITHDRAWN);
                Bgp4RibhProcessWithdRoute (pPeerInfo, pPeerRt, NULL);
            }

            if ((BGP4_RT_GET_FLAGS (pPeerRt) & BGP4_RT_ONLY_ADVT) ==
                BGP4_RT_ONLY_ADVT)
            {
                /* If this flag is set, then the route needs to be 
                 * advertised only to the peers and no need to update
                 * the FIB. */
                BGP4_RT_RESET_FLAG (pPeerRt, BGP4_RT_ONLY_ADVT);
            }
            else
            {
                /* Remove the route from the FIB */
                if (u1CanFIBUpdated == BGP4_TRUE)
                {
                    BGP4_TRC_ARG2 (NULL,
                                   BGP4_TRC_FLAG, BGP4_DETAIL_TRC,
                                   BGP4_MOD_NAME,
                                   "\tCalling  Bgp4IgphUpdateRouteInFIB  from Func[%s] Line[%d]\n",
                                   __func__, __LINE__);
                    Bgp4IgphUpdateRouteInFIB (pPeerRt);
                }
            }

            Bgp4DshReleaseRtInfo (pPeerRt);
            /* Increment the processed route count */
            u4RtCnt++;

            if (u4RtCnt >= u4MaxRtCnt)
            {
                /* Has Finished processing the MAX-DEINIT Routes. */
                break;
            }
        }
        if (u4RtCnt >= u4MaxRtCnt)
        {
            /* Has Finished processing the MAX-DEINIT Routes. */
            break;
        }
    }                            /* while-loop */
    /* Advertise the routes collected from the RIB to all peers as
     * withdrawn. */
    Bgp4PeerUpdateDeInitRoutesToPeerTxQ (pPeerInfo);
    if (u4RtCnt < u4MaxRtCnt)
    {
        /* Processed Route count < MAX Route count. This is possible
         * only if all the route from the PEER has been removed.
         * So effectivelyl DE-INIT is complete. */
        return BGP4_DEINIT_COMPLETE;
    }
    else
    {
        return BGP4_SUCCESS;
    }
}

/****************************************************************************
 * FUNCTION NAME : Bgp4PeerSendRoutesToPeerTxQ
  * DESCRIPTION   : This function takes routes from the input feasibe/withdrawn 
 *               : lists and form update messages from them. These are put into 
 *               : the peer Tx queue, for advertisement to the peer. 
 * INPUTS        : pPeerEntry - Pointer to the BGP peer entry
 *               : pFeasRtList - Pointer to the feasible route list
 *               : pWithRtList - Pointer to the withdrawn route list
 *               : u1Flag - Flag indicating whether it is necessary to check
 *               : the peer init status or not. If flag is BGP4_TRUE, then
 *               : check the pPeerEntry init status and process the routes.
 *               : Else no need to check the peer's init status.
 ** OUTPUTS       : None
 * RETURNS       : BGP4_SUCCESS/BGP4_FAILURE 
 ***************************************************************************/
INT4
Bgp4PeerSendRoutesToPeerTxQ (tBgp4PeerEntry * pPeerEntry,
                             tTMO_SLL * pFeasRtList, tTMO_SLL * pWithRtList,
                             UINT1 u1Flag)
{
    /* Variable Declarations */
    tTMO_SLL            SelectedBestRtList;
    tTMO_SLL            SelectedWithRtList;
    tLinkNode          *pRtLink = NULL;
    tRouteProfile      *pRtProfile = NULL;
    UINT4               u4HashKey = 0;
    UINT4               u4RtAsafiIndex = 0;
    UINT4               u4PeerInitAsafiIndex = 0;
    UINT4               u4Index = 0;
    INT4                i4RetVal = 0;

    if ((TMO_SLL_Count (pFeasRtList) == 0) &&
        (TMO_SLL_Count (pWithRtList) == 0))
    {
        return BGP4_SUCCESS;
    }

    TMO_SLL_Init (&SelectedBestRtList);
    TMO_SLL_Init (&SelectedWithRtList);

    TMO_SLL_Scan (pWithRtList, pRtLink, tLinkNode *)
    {
        pRtProfile = pRtLink->pRouteProfile;
        /* If the peer is going through initialisation and if the received
         * route's PA Hash Key is greater than that stored in the PeerInit
         * structure, then the route has not been advertised earlier and there
         * is no need to advertise that route. Else advertise the route.
         */
        if ((BGP4_GET_PEER_CURRENT_STATE (pPeerEntry) ==
             BGP4_PEER_INIT_INPROGRESS))
        {
            Bgp4GetAfiSafiIndex (BGP4_RT_AFI_INFO (pRtProfile),
                                 BGP4_RT_SAFI_INFO (pRtProfile),
                                 &u4RtAsafiIndex);
            Bgp4GetAfiSafiIndex (BGP4_PEER_INIT_AFI (pPeerEntry),
                                 BGP4_PEER_INIT_SAFI (pPeerEntry),
                                 &u4PeerInitAsafiIndex);
#ifdef L3VPN
            /* Carrying Label Information - RFC 3107 */
            if (u4RtAsafiIndex == BGP4_IPV4_LBLD_INDEX)
            {
                u4RtAsafiIndex = BGP4_IPV4_UNI_INDEX;
            }
#endif
            Bgp4PAFormHashKey (BGP4_RT_BGP_INFO (pRtProfile), &u4HashKey,
                               BGP4_MAX_RCVD_PA_HASH_INDEX);

            if ((u4RtAsafiIndex > u4PeerInitAsafiIndex) ||
                ((u4RtAsafiIndex == u4PeerInitAsafiIndex) &&
                 (u4HashKey > BGP4_PEER_INIT_PA_HASHKEY (pPeerEntry))))
            {
                continue;
            }
        }
        if (Bgp4AhIsRouteCanbeAdvt (pPeerEntry, pRtProfile, BGP4_FALSE) == TRUE)
        {
            Bgp4AhUpdateWithdrawnRouteList (pPeerEntry, pRtProfile,
                                            &SelectedWithRtList);
        }
        else
        {
            if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
            {
                BGP4_TRC_ARG4 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                               BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                               "\tRoute %s Mask %s from Peer %s not "
                               "selected for advertisement as "
                               "Unfeasible route to Peer %s\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               Bgp4PrintIpMask ((UINT1)
                                                BGP4_RT_PREFIXLEN (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (BGP4_RT_PEER_ENTRY
                                                 ((pRtProfile))),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (BGP4_RT_PEER_ENTRY
                                                  ((pRtProfile))))),
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerEntry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerEntry))));
            }
            else
            {
                BGP4_TRC_ARG4 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                               BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                               "\tRoute %s Mask %s from Protocol %d not "
                               "selected for advertisement as Unfeasible "
                               "route to Peer %s\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               Bgp4PrintIpMask ((UINT1)
                                                BGP4_RT_PREFIXLEN (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               BGP4_RT_PROTOCOL (pRtProfile),
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerEntry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerEntry))));
            }
        }
    }

    TMO_SLL_Scan (pFeasRtList, pRtLink, tLinkNode *)
    {
        pRtProfile = pRtLink->pRouteProfile;
        Bgp4GetAfiSafiIndex (BGP4_RT_AFI_INFO (pRtProfile),
                             BGP4_RT_SAFI_INFO (pRtProfile), &u4Index);

        /* If u1Flag is BGP4_TRUE, then check whether the peer to which the
         * routes are to be advertised is going through initialisation or not.
         * If the peer is going through initialisation, then dont advertise this
         * route to the peer as Feasible route if the route's PA Hash Key
         * is greater than the HashKey stored in the PeerInit structure.
         * Else advertise the route.
         */
        if (u1Flag == BGP4_TRUE)
        {
            if (BGP4_GET_PEER_CURRENT_STATE (pPeerEntry) ==
                BGP4_PEER_INIT_INPROGRESS)
            {
                Bgp4GetAfiSafiIndex (BGP4_RT_AFI_INFO (pRtProfile),
                                     BGP4_RT_SAFI_INFO (pRtProfile),
                                     &u4RtAsafiIndex);
                Bgp4GetAfiSafiIndex (BGP4_PEER_INIT_AFI (pPeerEntry),
                                     BGP4_PEER_INIT_SAFI (pPeerEntry),
                                     &u4PeerInitAsafiIndex);
#ifdef L3VPN
                /* Carrying Label Information - RFC 3107 */
                if (u4RtAsafiIndex == BGP4_IPV4_LBLD_INDEX)
                {
                    u4RtAsafiIndex = BGP4_IPV4_UNI_INDEX;
                }
#endif
                Bgp4PAFormHashKey (BGP4_RT_BGP_INFO (pRtProfile), &u4HashKey,
                                   BGP4_MAX_RCVD_PA_HASH_INDEX);
                if ((u4RtAsafiIndex > u4PeerInitAsafiIndex) ||
                    ((u4RtAsafiIndex == u4PeerInitAsafiIndex) &&
                     (u4HashKey > BGP4_PEER_INIT_PA_HASHKEY (pPeerEntry))))
                {
                    continue;
                }
            }
        }

        if (Bgp4AhIsRouteCanbeAdvt (pPeerEntry, pRtProfile, BGP4_TRUE) == FALSE)
        {
            if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
            {
                BGP4_TRC_ARG4 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                               BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                               "\tRoute %s Mask %s from Peer %s not selected for "
                               "advertisement as feasible route to Peer %s\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               Bgp4PrintIpMask ((UINT1)
                                                BGP4_RT_PREFIXLEN (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (BGP4_RT_PEER_ENTRY
                                                 ((pRtProfile))),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (BGP4_RT_PEER_ENTRY
                                                  ((pRtProfile))))),
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerEntry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerEntry))));
                /* Increment the out prefixes counter as this route
                 * can't be sent out for this peer. This will be in Adj-Rib-Out
                 * for this peer
                 */
                if ((BGP4_RT_PEER_ENTRY (pRtProfile) != pPeerEntry) &&
                    (pPeerEntry->apAsafiInstance[u4Index] != NULL))
                {
                    BGP4_PEER_OUT_PREFIXES_CNT (pPeerEntry, u4Index)++;
                }
            }
            else
            {
                BGP4_TRC_ARG4 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                               BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                               "\tRoute %s Mask %s from Protocol %d not selected for "
                               "advertisement as feasible route to Peer %s\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               Bgp4PrintIpMask ((UINT1)
                                                BGP4_RT_PREFIXLEN (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               BGP4_RT_PROTOCOL (pRtProfile),
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerEntry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerEntry))));
            }
            continue;
        }
        /* Apply OUTGOING filter policy on the route */
        i4RetVal = Bgp4FiltApplyOutgoingFilter (pPeerEntry, pRtProfile);
        if (i4RetVal != BGP4_SUCCESS)
        {
            /* Route is filtered out. If the returned value is
             * BGP4_RT_ADVT_WITHDRAWN, then this route needs to be advt to this
             * peer as withdrawn. If the peer is going through initialisation
             * no need to advt this route to those peer as withdrawn since
             * this is the first time the routes are sent to the peer. */
            BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                           BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tRoute %s Mask %s Filtered. Not Advt to PEER %s\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpMask ((UINT1)
                                            BGP4_RT_PREFIXLEN (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerEntry))));

#ifdef L3VPN
            if (((i4RetVal == BGP4_RT_ADVT_WITHDRAWN) &&
                 ((BGP4_GET_PEER_CURRENT_STATE (pPeerEntry) &
                   BGP4_PEER_INIT_INPROGRESS) == 0)) ||
                ((BGP4_GET_PEER_CURRENT_STATE (pPeerEntry) ==
                  BGP4_PEER_SOFTCONFIG_OUTBOUND_INPROGRESS) &&
                 (i4RetVal != BGP4_RT_DONT_ADVT)) ||
                (i4RetVal == BGP4_NO_EXPORT_TARGETS))
#else
            if (((i4RetVal == BGP4_RT_ADVT_WITHDRAWN) &&
                 ((BGP4_GET_PEER_CURRENT_STATE (pPeerEntry) &
                   BGP4_PEER_INIT_INPROGRESS) == 0)) ||
                ((BGP4_GET_PEER_CURRENT_STATE (pPeerEntry) ==
                  BGP4_PEER_SOFTCONFIG_OUTBOUND_INPROGRESS) &&
                 (i4RetVal != BGP4_RT_DONT_ADVT)))
#endif
            {
                /* Route needs to be advertised as withdrawn */
                if (Bgp4AhIsRouteCanbeAdvt (pPeerEntry, pRtProfile,
                                            BGP4_FALSE) == TRUE)
                {
                    Bgp4AhUpdateWithdrawnRouteList (pPeerEntry, pRtProfile,
                                                    &SelectedWithRtList);
                }
            }
            /* Add the route to the Peer output list. */
            Bgp4DshAddRouteToPeerList (pPeerEntry, BGP4_PEER_OUTPUT_LIST,
                                       pRtProfile, 0);
            if ((BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID) &&
                (pPeerEntry->apAsafiInstance[u4Index] != NULL))
            {
                /* Increment the out prefixes counter as this route
                 * can't be sent out for this peer. This will be in Adj-Rib-Out
                 * for this peer
                 */
                BGP4_PEER_OUT_PREFIXES_CNT (pPeerEntry, u4Index)++;
            }
            continue;
        }
        if ((BGP4_GET_PEER_CURRENT_STATE (pPeerEntry) &
             BGP4_PEER_INIT_INPROGRESS) == 0)
        {
            i4RetVal = Bgp4AhUpdateFeasibleRouteList (pPeerEntry,
                                                      pRtProfile,
                                                      &SelectedBestRtList,
                                                      &SelectedWithRtList);
            if (i4RetVal == BGP4_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                          "\tBgp4PeerSendRoutesToPeerTxQ () : Updt_Feasible_Route_List"
                          "returning Failure\n");

                break;
            }

        }
        else
        {
            Bgp4DshAddRouteToList (&SelectedBestRtList, pRtProfile, 0);
        }
    }

    i4RetVal = Bgp4AhSendUpdatesToPeer (pPeerEntry,
                                        &SelectedBestRtList,
                                        &SelectedWithRtList);

    Bgp4DshReleaseList (&SelectedBestRtList, 0);
    Bgp4DshReleaseList (&SelectedWithRtList, 0);
    return i4RetVal;
}

/****************************************************************************
 * FUNCTION NAME : Bgp4PeerAddMsgToTxQ
 * DESCRIPTION   : This function updates the BGP message to the peer Tx queue. 
 * INPUTS        : pPeerEntry - Pointer to the Peer information
 *               : pu1Buf - pointer  to the BGP message
 *               : u4Size - the size of the BGP message 
 * OUTPUTS       : None
 * RETURNS       : BGP4_SUCCESS/BGP4_FAILURE 
 ***************************************************************************/
INT4
Bgp4PeerAddMsgToTxQ (tBgp4PeerEntry * pPeerInfo, UINT1 *pu1Buf,
                     UINT4 u4Size, UINT1 u1Flag)
{
    tBufNode           *pBufNode = NULL;
    tPeerNode          *pPeerNode = NULL;

    /* Allocate BufNode and put the message in the peer tx queue */
    pBufNode = (tBufNode *) MemAllocMemBlk (gBgpNode.Bgp4BufNodesPoolId);
    if (pBufNode == NULL)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG,
                       BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s: Memory Allocation to add Msg to Tx Queue "
                       "FAILED!!!\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))));
        gu4BgpDebugCnt[MAX_BGP_BUFNODE_MSGS_SIZING_ID]++;
        return BGP4_FAILURE;
    }
    TMO_SLL_Init_Node (&pBufNode->TSNext);
    pBufNode->pu1Msg = pu1Buf;
    pBufNode->u4MsgSize = u4Size;
    pBufNode->u4MsgOffset = 0;
    /* Add message to peer Tx queue */
    if (u1Flag == BGP4_PREPEND)
    {
        TMO_SLL_Insert (BGP4_PEER_ADVT_MSG_LIST (pPeerInfo),
                        (tTMO_SLL_NODE *) NULL, &pBufNode->TSNext);
    }
    else                        /*  append */
    {
        TMO_SLL_Add (BGP4_PEER_ADVT_MSG_LIST (pPeerInfo), &pBufNode->TSNext);
    }

    if (BGP4_PEER_IS_IN_TRANX_LIST (pPeerInfo) == BGP4_FALSE)
    {
        /* Add the peer to the Peer Tranx List. */
        BGP_PEER_NODE_CREATE (pPeerNode);
        if (pPeerNode == NULL)
        {
            /* In this case, no need to clear the Peer entry List. The
             * Peer will be added back to the list, if another message
             * it to be tranx. So this failure is not to be treated as
             * fatal. */
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Memory Allocation to add "
                           "Peer to Tranx List FAILED!!!\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            gu4BgpDebugCnt[MAX_BGP_PEER_NODE_SIZING_ID]++;
            return BGP4_SUCCESS;
        }
        pPeerNode->pPeer = pPeerInfo;
        TMO_SLL_Add (BGP4_PEER_TRANS_LIST (BGP4_PEER_CXT_ID (pPeerInfo)),
                     &pPeerNode->TSNext);
        BGP4_PEER_IS_IN_TRANX_LIST (pPeerInfo) = BGP4_TRUE;
    }
    return BGP4_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : Bgp4PeerReTxmMsg
 * DESCRIPTION   : This function re-transmits a BGP message to the peer. 
 * INPUTS        : pPeerInfo - Pointer to the peer information 
 * OUTPUTS       : None
 * RETURNS       : BGP4_SUCCESS/BGP4_FAILURE 
 ***************************************************************************/
INT4
Bgp4PeerReTxmMsg (tBgp4PeerEntry * pPeerInfo)
{
    /* Variable Declarations */
    tBufNode           *pBufNode = NULL;
    UINT1              *pu1CurBuf = NULL;
    INT4                i4CurBufSize = 0;
    INT4                i4UpdateMessageSentStatus = BGP4_FAILURE;
    UINT4               u4Context = 0;

    u4Context = BGP4_PEER_CXT_ID (pPeerInfo);
    if (BGP4_PEER_READVT_BUFFER (pPeerInfo) != NULL)
    {
        /* Re-transmit this buffer to the peer */
        pBufNode = (tBufNode *) (VOID *) BGP4_PEER_READVT_BUFFER (pPeerInfo);
        i4CurBufSize = BGP4_PEER_MSG_SIZE_ADVT_BUF_NODE (pBufNode) -
            BGP4_PEER_MSG_OFFSET_ADVT_BUF_NODE (pBufNode);

        pu1CurBuf = BGP4_PEER_MSG_IN_ADVT_BUF_NODE (pBufNode) +
            BGP4_PEER_MSG_OFFSET_ADVT_BUF_NODE (pBufNode);

        i4UpdateMessageSentStatus =
            Bgp4TcphSendData (pPeerInfo, pu1CurBuf, i4CurBufSize);

        if (i4UpdateMessageSentStatus != i4CurBufSize)
        {
            if (i4UpdateMessageSentStatus == BGP_EWOULDBLOCK)
            {
                /* Send blocks. Do nothing */
            }
            else if (i4UpdateMessageSentStatus == BGP4_FAILURE)
            {
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                    BGP4_PEER_MSG_IN_ADVT_BUF_NODE (pBufNode));
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodesPoolId,
                                    BGP4_PEER_READVT_BUFFER (pPeerInfo));
                BGP4_PEER_READVT_BUFFER (pPeerInfo) = NULL;
                BGP_FD_CLR (BGP4_PEER_CONN_ID (pPeerInfo),
                            BGP4_WRITE_SOCK_FD_SET (u4Context));
                Bgp4IphHandleControl (pPeerInfo, BGP4_TCP_ERROR);
            }
            else                /* Fragmented message txmitted */
            {
                /* Only partial msg has been sent. Update the offset
                 * field for next transmission */
                BGP4_PEER_MSG_OFFSET_ADVT_BUF_NODE (pBufNode) +=
                    (UINT4) i4UpdateMessageSentStatus;
            }
            return BGP4_FAILURE;
        }
        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                            BGP4_PEER_MSG_IN_ADVT_BUF_NODE (pBufNode));
        MemReleaseMemBlock (gBgpNode.Bgp4BufNodesPoolId, (UINT1 *) pBufNode);
        BGP4_PEER_READVT_BUFFER (pPeerInfo) = NULL;
    }
    BGP_FD_CLR (BGP4_PEER_CONN_ID (pPeerInfo),
                BGP4_WRITE_SOCK_FD_SET (u4Context));
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* FUNCTION NAME : Bgp4PeerUpdPeerInitRtInfo                                */
/* DESCRIPTION   : This function takes the route to be removed from the     */
/*               : LOCAL RIB as input and check whether any peer's going    */
/*               : throught initialisation stores this route in its peer    */
/*               : init route info. If so then updates the peer init route  */
/*               : info with the next best route in the RIB. This routine   */
/*               : must be called before a route is removed from LOCAL RIB  */
/* INPUTS        : Pointer to the route to be removed (pRtProfile)          */
/*               : Pointer to the treenode in which this route is present   */
/*               :          - pTreeNode                                     */
/*               : i4Flag - Flag passed to RIB for semaphore lock decision  */
/* OUTPUTS       : None                                                     */
/* RETURNS       : BGP4_SUCCESS/BGP4_FAILURE                                */
/****************************************************************************/
INT4
Bgp4PeerUpdPeerInitRtInfo (tRouteProfile * pRtProfile, VOID *pTreeNode,
                           INT4 i4Flag)
{
    VOID               *pNode = NULL;
    tBgp4PeerEntry     *pPeerentry = NULL;
    tRouteProfile      *pNextBestRoute = NULL;
    INT4                i4RetVal = 0;
    INT4                i4NextRib = BGP4_FALSE;
    UINT4               u4IsNextRoutePresent = BGP4_TRUE;
    UINT4               u4AsafiMask;
    UINT4               u4AsafiMaskFound;

    pNode = (VOID *) pTreeNode;
    /* Scan all the peers in the peer list. */
    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (BGP4_RT_CXT_ID (pRtProfile)), pPeerentry,
                  tBgp4PeerEntry *)
    {
        u4AsafiMaskFound = 0;
        if ((NetAddrMatch (BGP4_RT_NET_ADDRESS_INFO (pRtProfile),
                           BGP4_PEER_INIT_NETADDR_INFO (pPeerentry)))
            == BGP4_TRUE)
        {
            /* This peer's init route info matches with the
             * route to be deleted. Update this init route
             * info with the next route. */
            if ((BGP4_GET_PEER_CURRENT_STATE (pPeerentry) ==
                 BGP4_PEER_SOFTCONFIG_INBOUND_INPROGRESS) ||
                (BGP4_GET_PEER_CURRENT_STATE (pPeerentry) ==
                 BGP4_PEER_REUSE_INPROGRESS))
            {
                /* Soft-Inbound/RFD Reuse is in progress for this Peer. */
                pNextBestRoute = BGP4_RT_PROTO_LINK_NEXT (pRtProfile);
                if (pNextBestRoute != NULL)
                {
                    /* Update the Peer-Init Info */
                    Bgp4CopyNetAddressStruct (&
                                              (BGP4_PEER_INIT_NETADDR_INFO
                                               (pPeerentry)),
                                              BGP4_RT_NET_ADDRESS_INFO
                                              (pNextBestRoute));
                }
                else
                {
                    /* There is no more route present in RIB from this peer.
                     * So just continue without resetting the Peer-Init status
                     * and the corresponding event handler will take care of
                     * remaining sequence.
                     */
                }
                continue;
            }
            if ((pNextBestRoute == NULL) && (u4IsNextRoutePresent == BGP4_TRUE))
            {
                /* Get the next best route to the current route. */
                i4RetVal =
                    Bgp4RibhGetNextEntry (pRtProfile,
                                          BGP4_RT_CXT_ID (pRtProfile),
                                          &pNextBestRoute, &pNode, i4Flag);
                if (i4RetVal == BGP4_FAILURE)
                {
                    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                                           BGP4_RT_SAFI_INFO (pRtProfile),
                                           u4AsafiMask);
                    switch (u4AsafiMask)
                    {
                        case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
                        case CAP_MP_LABELLED_IPV4:
#endif
                            /* Get the first entry from IPV6 RIB */
#ifdef BGP4_IPV6_WANTED
                            Bgp4GetRibNode (BGP4_RT_CXT_ID (pRtProfile),
                                            CAP_MP_IPV6_UNICAST, &pNode);
                            if (pNode != NULL)
                            {
                                i4RetVal =
                                    Bgp4RibhGetFirstEntry (CAP_MP_IPV6_UNICAST,
                                                           &pNextBestRoute,
                                                           &pNode, i4Flag);
                            }
                            if (i4RetVal != BGP4_FAILURE)
                            {
                                i4NextRib = BGP4_TRUE;
                                u4AsafiMaskFound = CAP_MP_IPV6_UNICAST;
                                break;
                            }
#endif
#ifdef L3VPN
                            pNode =
                                BGP4_VPN4_RIB_TREE (BGP4_RT_CXT_ID
                                                    (pRtProfile));
                            i4RetVal =
                                Bgp4RibhGetFirstEntry (CAP_MP_VPN4_UNICAST,
                                                       &pNextBestRoute, &pNode,
                                                       i4Flag);
                            if (i4RetVal != BGP4_FAILURE)
                            {
                                i4NextRib = BGP4_TRUE;
                                u4AsafiMaskFound = CAP_MP_VPN4_UNICAST;
                                break;
                            }
#endif
                            /* Set u4IsNextRoutePresent as BGP4_FALSE to
                             * indicate that there is no next best route 
                             * present in RIB. */
                            u4IsNextRoutePresent = BGP4_FALSE;
                            i4NextRib = BGP4_FALSE;
                            break;
#ifdef BGP4_IPV6_WANTED
                        case CAP_MP_IPV6_UNICAST:
                            /* Set u4IsNextRoutePresent as BGP4_FALSE to 
                             * indicate that there is no next best route 
                             * present in RIB. */
#ifdef L3VPN
                            pNode =
                                BGP4_VPN4_RIB_TREE (BGP4_RT_CXT_ID
                                                    (pRtProfile));
                            i4RetVal =
                                Bgp4RibhGetFirstEntry (CAP_MP_VPN4_UNICAST,
                                                       &pNextBestRoute, &pNode,
                                                       i4Flag);
                            if (i4RetVal != BGP4_FAILURE)
                            {
                                i4NextRib = BGP4_TRUE;
                                u4AsafiMaskFound = CAP_MP_VPN4_UNICAST;
                                break;
                            }
#endif
                            /* Set u4IsNextRoutePresent as BGP4_FALSE to
                             * indicate that there is no next best route 
                             * present in RIB. */
                            u4IsNextRoutePresent = BGP4_FALSE;
                            i4NextRib = BGP4_FALSE;
                            break;
#endif
#ifdef L3VPN
                        case CAP_MP_VPN4_UNICAST:
                            u4IsNextRoutePresent = BGP4_FALSE;
                            i4NextRib = BGP4_FALSE;
                            break;
#endif
                        default:
                            /* Set u4IsNextRoutePresent as BGP4_FALSE to 
                             * indicate that there is no next best route 
                             * present in RIB. */
                            u4IsNextRoutePresent = BGP4_FALSE;
                            i4NextRib = BGP4_FALSE;
                    }
                }
            }

            if (pNextBestRoute == NULL)
            {
                /* No more route is present in the tree. So reset the
                 * peer's init route info. The peer init routine will
                 * ensure to remove this peer from the peer init list. */
                Bgp4InitNetAddressStruct (&
                                          (BGP4_PEER_INIT_NETADDR_INFO
                                           (pPeerentry)), BGP4_INET_AFI_IPV4,
                                          BGP4_INET_SAFI_UNICAST);
                BGP4_SET_PEER_CURRENT_STATE (pPeerentry, BGP4_PEER_READY);
            }
            else
            {
                /* Store the next route to be processed in peer init route info.
                 * This route will be processed in the next TIC */
                if (BGP4_GET_PEER_CURRENT_STATE (pPeerentry) ==
                    BGP4_PEER_SOFTCONFIG_OUTBOUND_INPROGRESS)
                {
                    if (i4NextRib == BGP4_FALSE)
                    {
                        Bgp4CopyNetAddressStruct (&
                                                  (BGP4_PEER_INIT_NETADDR_INFO
                                                   (pPeerentry)),
                                                  BGP4_RT_NET_ADDRESS_INFO
                                                  (pNextBestRoute));
                    }
                    else
                    {
#ifdef BGP4_IPV6_WANTED
                        if (u4AsafiMaskFound == CAP_MP_IPV6_UNICAST)
                        {
                            Bgp4InitNetAddressStruct (&
                                                      (BGP4_PEER_INIT_NETADDR_INFO
                                                       (pPeerentry)),
                                                      BGP4_INET_AFI_IPV6,
                                                      BGP4_INET_SAFI_UNICAST);
                            BGP4_PEER_SOFTCONFIG_OUTBOUND_AFI (pPeerentry) =
                                BGP4_INET_AFI_IPV6;
                            BGP4_PEER_SOFTCONFIG_OUTBOUND_SAFI (pPeerentry) =
                                BGP4_INET_SAFI_UNICAST;
                        }
#endif
#ifdef L3VPN
                        if (u4AsafiMaskFound == CAP_MP_VPN4_UNICAST)
                        {
                            Bgp4InitNetAddressStruct (&
                                                      (BGP4_PEER_INIT_NETADDR_INFO
                                                       (pPeerentry)),
                                                      BGP4_INET_AFI_IPV6,
                                                      BGP4_INET_SAFI_UNICAST);
                            BGP4_PEER_SOFTCONFIG_OUTBOUND_AFI (pPeerentry) =
                                BGP4_INET_AFI_IPV6;
                            BGP4_PEER_SOFTCONFIG_OUTBOUND_SAFI (pPeerentry) =
                                BGP4_INET_SAFI_UNICAST;
                        }
#endif
                    }
                }
                else
                {
                    Bgp4CopyNetAddressStruct (&
                                              (BGP4_PEER_INIT_NETADDR_INFO
                                               (pPeerentry)),
                                              BGP4_RT_NET_ADDRESS_INFO
                                              (pNextBestRoute));
                }
            }
        }
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4PeerCreatePeerEntry                                   */
/* Description   : Creates a Peer entry for the BGP Peer.                    */
/* Input(s)      : u4PeerAddr - Peer's Remote IP Address                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4PeerCreatePeerEntry (UINT4 u4Context, tAddrPrefix PeerAddr)
{
    /* Variable Declarations */
    tBgp4PeerEntry     *pPeer = NULL;

    pPeer = Bgp4SnmphAddPeerEntry (u4Context, PeerAddr);
    if (pPeer == NULL)
    {
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4DeletePeer                                            */
/* Description   : This function handles the peer Delete event.              */
/* Input(s)      : Peer Entry which needs to be disabled (pPeer).            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4DeletePeer (tBgp4PeerEntry * pPeer)
{
    tBgp4PeerEntry     *pDupPeer = NULL;

    if (pPeer == NULL)
    {
        return BGP4_FAILURE;
    }
    if (((BGP4_GET_NODE_STATUS != RM_ACTIVE) ||
         (BGP4_PEER_RESTART_MODE (pPeer) == BGP4_RESTARTING_MODE) ||
         (BGP4_PEER_RESTART_MODE (pPeer) == BGP4_RECEIVING_MODE)) &&
        (BGP4_GET_PEER_CURRENT_STATE (pPeer) != BGP4_PEER_DEINIT_INPROGRESS))
    {
        /* If the GR re-start peer is getting deleted, the preserved
         * routes obtained from it before shutdown, need to be cleared off */
        Bgp4RibhInitPeerDeInitInfo (pPeer);
        Bgp4GRPutPeerInDeInitList (pPeer);
    }

    if ((BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_START) ||
        (BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_AUTO_START))
    {
        /* This clears all the data structures associated with 
         * the peer. */
        Bgp4SEMHProcessEvent (pPeer, BGP4_STOP_EVT, NULL, 0);
        BGP4_PEER_ADMIN_STATUS (pPeer) = BGP4_PEER_STOP;
    }

    if ((BGP4_GET_PEER_CURRENT_STATE (pPeer) == BGP4_PEER_DEINIT_INPROGRESS) ||
        (BGP4_GLOBAL_STATE (BGP4_PEER_CXT_ID (pPeer)) ==
         BGP4_GLOBAL_ADMIN_DOWN_INPROGRESS))
    {
        /* Peer undergoing de-initialisation/Global Admin Down is in progress.
         * Set the delete event as pending and reset all other pending events.
         */
        BGP4_RESET_PEER_PEND_FLAG (pPeer, (BGP4_PEER_INIT_PENDING |
                                           BGP4_PEER_MULTIHOP_PEND_START));
        BGP4_SET_PEER_PEND_FLAG (pPeer, BGP4_PEER_DELETE_PENDING);
    }

    /* Check if any Duplicate peer entry exists. If so, then
     * delete it. */
    pDupPeer = Bgp4SnmphGetDuplicatePeerEntry (pPeer);
    if (pDupPeer != NULL)
    {
        Bgp4SEMHProcessEvent (pDupPeer, BGP4_STOP_EVT, NULL, 0);
        Bgp4SnmphDeletePeerEntry (pDupPeer);
    }

    if ((BGP4_GET_PEER_PEND_FLAG (pPeer) & BGP4_PEER_DELETE_PENDING) !=
        BGP4_PEER_DELETE_PENDING)
    {
        /* Release the lists, (input list, output list and the pending list)
         * allocated for this peer. 
         */
        Bgp4DshReleasePeerRtLists (pPeer, BGP4_FALSE);

        /* Peer is not pending for delete event. So can free peer's
         * resources. */
        Bgp4ClearPeer (pPeer);
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4ClearPeer                                             */
/* Description   : This function clears the data structure and resources     */
/*               : associated/allocated with/to the peer.                    */
/* Input(s)      : Peer Entry whose resource needs to be freed (pPeer)       */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4ClearPeer (tBgp4PeerEntry * pPeer)
{
    INT4                i4RetVal = BGP4_SUCCESS;
    /* Ensure that all the peer timers are stopped before
     * clearing the peer. */
    Bgp4TmrhStopAllTimers (pPeer);
    /* These timers are not included as part of TmrhStopAllTimers since
     * during tcp close event this function may be called and flush 
     * out the stale and peer restart timer running during peer 
     * restart process */
    Bgp4TmrhStopTimer (BGP4_PEER_RESTART_TIMER, (VOID *) pPeer);
    Bgp4TmrhStopTimer (BGP4_STALE_TIMER, (VOID *) pPeer);

    if (CAPS_ADVT_SUP (BGP4_PEER_CXT_ID (pPeer)) == BGP4_TRUE)
    {
        CapsDeleteSpkrCapsInfo (pPeer);
    }

#ifdef RFD_WANTED
    /* Clear Peer related Data from RFD storage */
    if ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeer), pPeer) ==
         BGP4_EXTERNAL_PEER) && (BGP4_CONFED_PEER_STATUS (pPeer) == BGP4_FALSE))
    {
        RfdClearPeerHistory (pPeer);
    }
#endif

    /* If MD5 password was set on listen socket/password buffer allocated
     * remove password from listen socket then release md5 buffer */
    if (BGP4_PEER_TCPMD5_PTR (pPeer) != NULL)
    {
        if (BGP4_PEER_TCPMD5_PASSWD_LEN (pPeer) != ZERO)
        {
            i4RetVal = Bgp4TcphMD5AuthOptSet (pPeer, NULL, ZERO);
            if (i4RetVal != BGP4_SUCCESS)
            {
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                          BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                          BGP4_MOD_NAME,
                          "\tPeer delete - password remove failed\n");
            }
        }
        Bgp4MemReleaseTcpMd5Buf (BGP4_PEER_TCPMD5_PTR (pPeer),
                                 BGP4_PEER_CXT_ID (pPeer));
        BGP4_PEER_TCPMD5_PTR (pPeer) = NULL;
    }

    /* Delete the RBTree holding advertised routes to peer */
    if (BGP4_PEER_SENT_ROUTES (pPeer) != NULL)
    {
        BGP4_RB_TREE_DELETE (BGP4_PEER_SENT_ROUTES (pPeer));
        BGP4_PEER_SENT_ROUTES (pPeer) = NULL;
    }

    /* Peer is not pending for delete event. So Delete the Peer Entry */
    Bgp4SnmphDeletePeerEntry (pPeer);
    return BGP4_SUCCESS;

}

/*****************************************************************************/
/* Function Name : Bgp4PeerStartPeer                                         */
/* Description   : This routine will process the Peer Start event.           */
/* Input(s)      : u4PeerAddr - Peer's Remote IP Address                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4PeerStartPeer (UINT4 u4Context, tAddrPrefix PeerAddr)
{
    /* Variable Declarations */
    tBgp4PeerEntry     *pPeer = NULL;
    UINT2               u2Afi;
    INT1                i1Index = 0;

    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddr);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME,
                  "\tConfig Error - No matching peer exist!!!\n");
        return BGP4_FAILURE;
    }
    if ((BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_START) ||
        (BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_AUTO_START))
    {
        return BGP4_SUCCESS;
    }
    if (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_UP)
    {
        /* Peer is going to be enabled. Check whether peer delete
         * is pending or not. If yes, then delete peer has been
         * already issued and there is no point in doing a peer
         * initialisation.
         */
        if ((BGP4_GET_PEER_PEND_FLAG (pPeer) &
             BGP4_PEER_DELETE_PENDING) == BGP4_PEER_DELETE_PENDING)
        {
            return BGP4_FAILURE;
        }
        /* Reset the peer FSM history */
        BGP4_PEER_FSM_HIST_HEAD (pPeer) = BGP4_INVALID_STATE;
        BGP4_PEER_FSM_HIST_TAIL (pPeer) = BGP4_INVALID_STATE;
        for (i1Index = 0; i1Index < BGP4_FSM_HIST_MAX_SIZE; i1Index++)
        {
            (BGP4_PEER_FSM_TRANSITION_HIST (pPeer, i1Index)) = 0;
        }

        /* Fill the local address if its not configured */
        if (BGP4_PEER_LOCAL_ADDR_CONFIGURED (pPeer) != BGP4_TRUE)
        {
            u2Afi =
                BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_PEER_REMOTE_ADDR_INFO
                                              (pPeer));

            if (u2Afi == BGP4_INET_AFI_IPV4)
            {
                Bgp4InitNetAddressStruct (&
                                          (BGP4_PEER_LOCAL_NETADDR_INFO
                                           (pPeer)), BGP4_INET_AFI_IPV4,
                                          BGP4_INET_SAFI_UNICAST);
                PTR_ASSIGN_4 (BGP4_PEER_LOCAL_ADDR (pPeer),
                              BGP4_LOCAL_BGP_ID (u4Context));
            }
#ifdef BGP_TCP6_WANTED
            else
            {
                INT4                i4Ret;
                Bgp4InitNetAddressStruct (&
                                          (BGP4_PEER_LOCAL_NETADDR_INFO
                                           (pPeer)), BGP4_INET_AFI_IPV6,
                                          BGP4_INET_SAFI_UNICAST);
                i4Ret =
                    Bgp4GetLocalAddrForPeer (pPeer,
                                             BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                                             &(BGP4_PEER_LOCAL_NETADDR_INFO
                                               (pPeer)), BGP4_FALSE);
                if (i4Ret == BGP4_FAILURE)
                {
                    return BGP4_FAILURE;
                }
            }
#endif
        }
        /*  RFC 4271 Update */
        if (BGP4_PEER_ALLOW_AUTOMATIC_START (pPeer) ==
            BGP4_PEER_AUTOMATICSTART_ENABLE)
        {
            BGP4_PEER_ADMIN_STATUS (pPeer) = BGP4_PEER_AUTO_START;
        }
        else
        {
            BGP4_PEER_ADMIN_STATUS (pPeer) = BGP4_PEER_START;
        }
        BGP4_PEER_RECVD_EOR_MARKER (pPeer) = BGP4_INVALID_STATUS;
        BGP4_EOR_RCVD (pPeer) = BGP4_INVALID_STATUS;
        BGP4_PEER_EOR_MARKER (pPeer) = BGP4_INVALID_STATUS;
        BGP4_PEER_SENT_EOR_MARKER (pPeer) = BGP4_INVALID_STATUS;
        BGP4_PEER_RESTART_INTERVAL (pPeer) = ZERO;
        if (BGP4_PEER_RESTART_MODE (pPeer) != BGP4_RECEIVING_MODE)
        {
            BGP4_PEER_RESTART_MODE (pPeer) = BGP4_RESTART_MODE_NONE;
        }
        BGP4_CHANGE_STATE (pPeer, BGP4_IDLE_STATE);
        Bgp4AddTransitionToFsmHist (pPeer, BGP4_IDLE_STATE);
        Bgp4EnablePeer (pPeer);
    }
    else
    {
        BGP4_SET_PEER_PEND_FLAG (pPeer, BGP4_PEER_START_PEND);
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4PeerStopPeer                                          */
/* Description   : This routine will process the Peer Stop event.            */
/* Input(s)      : u4PeerAddr - Peer's Remote IP Address                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4PeerStopPeer (UINT4 u4Context, tAddrPrefix PeerAddr)
{
    /* Variable Declarations */
    tBgp4PeerEntry     *pPeer = NULL;

    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddr);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME,
                  "\tConfig Error - No matching peer exist!!!\n");
        return BGP4_FAILURE;
    }

    /* Reset the peer flags */
    BGP4_RESET_PEER_PEND_FLAG (pPeer, BGP4_PEER_MULTIHOP_PEND_START);

    if (BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_STOP)
    {
        return BGP4_SUCCESS;
    }

    BGP4_PEER_ADMIN_STATUS (pPeer) = BGP4_PEER_STOP;
    BGP4_PEER_START_TIME (pPeer) = BGP4_DEF_STARTINTERVAL;

    if (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_UP)
    {
        /* Disable the peer. */
        Bgp4DisablePeer (pPeer);
    }
    else
    {
        BGP4_RESET_PEER_PEND_FLAG (pPeer, BGP4_PEER_START_PEND);
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4EnablePeer                                            */
/* Description   : This function handles the peer Enable event.              */
/* Input(s)      : Peer Entry which needs to be enabled (pPeer).             */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4EnablePeer (tBgp4PeerEntry * pPeer)
{
    UINT4               u4Context = BGP4_PEER_CXT_ID (pPeer);
    tTcpAoMktAddr       TcpAoMktAdr;
    UINT1               u1ConfigCaps = BGP4_FALSE;

    if ((BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_STOP) ||
        (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_DOWN))
    {
        /* Cannot start the peer since the peer admin status is down. */
        return BGP4_FAILURE;
    }

    /* Peer can't be enabled before configuring the peer's AS number */
    if (BGP4_PEER_ASNO (pPeer) == BGP4_INV_AS)
    {
        return BGP4_FAILURE;
    }

    if (CAPS_ADVT_SUP (u4Context) == CAPS_TRUE)
    {
        if (Bgp4SnmphGetMPCapSupportStatus (BGP4_PEER_CXT_ID (pPeer),
                                            BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                                            BGP4_INET_AFI_IPV4,
                                            BGP4_INET_SAFI_UNICAST)
            == BGP4_TRUE)
        {
            u1ConfigCaps = BGP4_TRUE;
        }
#ifdef BGP4_IPV6_WANTED
        if (u1ConfigCaps == BGP4_FALSE)
        {
            if (Bgp4SnmphGetMPCapSupportStatus
                (BGP4_PEER_CXT_ID (pPeer),
                 BGP4_PEER_REMOTE_ADDR_INFO (pPeer), BGP4_INET_AFI_IPV6,
                 BGP4_INET_SAFI_UNICAST) == BGP4_TRUE)
            {
                u1ConfigCaps = BGP4_TRUE;
            }
        }
#endif
#ifdef VPLSADS_WANTED
        if (u1ConfigCaps == BGP4_FALSE)
        {
            if (Bgp4SnmphGetMPCapSupportStatus (BGP4_PEER_CXT_ID (pPeer),
                                                BGP4_PEER_REMOTE_ADDR_INFO
                                                (pPeer), BGP4_INET_AFI_L2VPN,
                                                BGP4_INET_SAFI_VPLS) ==
                BGP4_TRUE)
            {
                u1ConfigCaps = BGP4_TRUE;
            }
        }

#endif
#ifdef EVPN_WANTED
        if (u1ConfigCaps == BGP4_FALSE)
        {
            if (Bgp4SnmphGetMPCapSupportStatus (BGP4_PEER_CXT_ID (pPeer),
                                                BGP4_PEER_REMOTE_ADDR_INFO
                                                (pPeer), BGP4_INET_AFI_L2VPN,
                                                BGP4_INET_SAFI_EVPN) ==
                BGP4_TRUE)
            {
                u1ConfigCaps = BGP4_TRUE;
            }
        }

#endif

#ifdef L3VPN
        if (u1ConfigCaps == BGP4_FALSE)
        {
            if (Bgp4SnmphGetMPCapSupportStatus
                (BGP4_PEER_CXT_ID (pPeer),
                 BGP4_PEER_REMOTE_ADDR_INFO (pPeer), BGP4_INET_AFI_IPV4,
                 BGP4_INET_SAFI_LABEL) == BGP4_TRUE)
            {
                u1ConfigCaps = BGP4_TRUE;
            }
        }
        if (u1ConfigCaps == BGP4_FALSE)
        {
            if (Bgp4SnmphGetMPCapSupportStatus
                (BGP4_PEER_CXT_ID (pPeer),
                 BGP4_PEER_REMOTE_ADDR_INFO (pPeer), BGP4_INET_AFI_IPV4,
                 BGP4_INET_SAFI_VPNV4_UNICAST) == BGP4_TRUE)
            {
                u1ConfigCaps = BGP4_TRUE;
            }
        }
#endif
        if (u1ConfigCaps == BGP4_FALSE)
        {
            /* No MP Address Family Capability is configured for this peer.
             * Peer Start will be deferred till a MP capability is configured.
             */
            BGP4_SET_PEER_PEND_FLAG (pPeer, BGP4_PEER_MP_CAP_CONFIG_PEND_START);
            return BGP4_SUCCESS;
        }
    }

    /* There exist some MP Address Family Capability for this peer or Caps
     * Support is Disabled. Proceed with Peer connection establishment. */
    BGP4_RESET_PEER_PEND_FLAG (pPeer, (BGP4_PEER_MP_CAP_CONFIG_PEND_START |
                                       BGP4_PEER_MP_CAP_RECV_PEND_START |
                                       BGP4_PEER_MULTIHOP_PEND_START));
    BGP4_PEER_RECVD_EOR_MARKER (pPeer) = BGP4_INVALID_STATUS;
    BGP4_EOR_RCVD (pPeer) = BGP4_INVALID_STATUS;
    BGP4_PEER_EOR_MARKER (pPeer) = BGP4_INVALID_STATUS;

    BGP4_PEER_SENT_EOR_MARKER (pPeer) = BGP4_INVALID_STATUS;

    switch (BGP4_GET_PEER_CURRENT_STATE (pPeer))
    {
        case BGP4_PEER_DEINIT_INPROGRESS:
            BGP4_SET_PEER_PEND_FLAG (pPeer, BGP4_PEER_INIT_PENDING);
            break;

        case BGP4_PEER_READY:
        case BGP4_PEER_UNKNOWN_STATE:
        case BGP4_PEER_STALE_DEL_INPROGRESS:
            /* RFC 4271 Update */
            BGP4_PEER_CURRENT_PREFIX_LIMIT (pPeer) = 0;

            if (BGP4_PEER_CURRENT_IDLE_HOLD_TIME (pPeer) == 0)
            {
                BGP4_PEER_CURRENT_IDLE_HOLD_TIME (pPeer) =
                    BGP4_PEER_IDLE_HOLD_TIME (pPeer);
            }
            if (BGP4_GET_NODE_STATUS != RM_ACTIVE)
            {
                /* When BGP is not in active state, do not
                 * allow peer connection */
                break;
            }
            /* At start BGP4_PEER_TCP_CONFIGURED_CONNECT_COUNT of the
             * peer will be zero and hence the session need not be
             * damped for oscillations at first time */

            if (BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_AUTO_START &&
                BGP4_PEER_TCP_CONFIGURED_CONNECT_COUNT (pPeer) != 0)
            {
                Bgp4TmrhStartTimer (BGP4_IDLE_HOLD_TIMER,
                                    (VOID *) pPeer,
                                    BGP4_PEER_CURRENT_IDLE_HOLD_TIME (pPeer));
            }
            else
            {
                /* To check if the IdleHold Time reaches its
                 * maximum threshold. 
                 * If it does, then a manual stop is issued and 
                 * the time value is reset to 0. */

                if (BGP4_PEER_CURRENT_IDLE_HOLD_TIME (pPeer) >
                    (BGP4_MAX_IDLE_HOLD_INTERVAL / 2))
                {
                    BGP4_PEER_ADMIN_STATUS (pPeer) = BGP4_PEER_STOP;
                    BGP4_PEER_CURRENT_IDLE_HOLD_TIME (pPeer) = 0;
                    Bgp4SEMHProcessEvent (pPeer, BGP4_STOP_EVT, NULL, 0);
                }
                else
                {
                    Bgp4SEMHProcessEvent (pPeer, BGP4_START_EVT, NULL, 0);
                }
            }

            /*Set the TCP AO option on the listen socket if configured on a peer */
            if (BGP4_PEER_TCPAO_MKT (pPeer) != NULL)
            {
                MEMSET (&TcpAoMktAdr, ZERO, sizeof (tTcpAoMktAddr));
                TcpAoMktAdr.u1SndKeyId = pPeer->pTcpAOAuthMKT->u1SendKeyId;
                TcpAoMktAdr.u1RcvKeyId = pPeer->pTcpAOAuthMKT->u1ReceiveKeyId;
                TcpAoMktAdr.u1Algo = pPeer->pTcpAOAuthMKT->u1MACAlgo;
                TcpAoMktAdr.u1KeyLen =
                    pPeer->pTcpAOAuthMKT->u1TcpAOPasswdLength;
                TcpAoMktAdr.u1TcpOptIgn =
                    pPeer->pTcpAOAuthMKT->u1TcpOptionIgnore;
                MEMCPY (TcpAoMktAdr.au1Key,
                        pPeer->pTcpAOAuthMKT->au1TcpAOMasterKey,
                        TcpAoMktAdr.u1KeyLen);
                Bgp4TcphAuthOptionMktSet (pPeer, &TcpAoMktAdr);
            }

            else if (BGP4_PEER_TCPMD5_PASSWD (pPeer) != NULL)
            {
                Bgp4TcphMD5AuthOptSet (pPeer, BGP4_PEER_TCPMD5_PASSWD (pPeer),
                                       BGP4_PEER_TCPMD5_PASSWD_LEN (pPeer));
            }
            break;

        case BGP4_PEER_INIT_INPROGRESS:
        case BGP4_PEER_SOFTCONFIG_INBOUND_INPROGRESS:
        case BGP4_PEER_SOFTCONFIG_OUTBOUND_INPROGRESS:
        case BGP4_PEER_REUSE_INPROGRESS:
        default:
            return BGP4_FAILURE;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4DisablePeer                                           */
/* Description   : This function handles the peer Disable event.             */
/* Input(s)      : Peer Entry which needs to be disabled (pPeer).            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4DisablePeer (tBgp4PeerEntry * pPeer)
{
    tBgp4PeerEntry     *pDupPeer = NULL;

    if (pPeer == NULL)
    {
        return BGP4_FAILURE;
    }

    /* Sent the peer stop event to SEM handler. */
    Bgp4SEMHProcessEvent (pPeer, BGP4_STOP_EVT, NULL, 0);

    /* Check and delete duplicate entry if present. */
    pDupPeer = Bgp4SnmphGetDuplicatePeerEntry (pPeer);
    if (pDupPeer != NULL)
    {
        Bgp4SEMHProcessEvent (pDupPeer, BGP4_STOP_EVT, NULL, 0);
        Bgp4SnmphDeletePeerEntry (pDupPeer);
    }
    if (BGP4_GET_PEER_CURRENT_STATE (pPeer) ==
        BGP4_PEER_SOFTCONFIG_OUTBOUND_INPROGRESS)
    {
        BGP4_SET_PEER_CURRENT_STATE (pPeer, BGP4_PEER_READY);
    }

    if ((BGP4_GET_PEER_CURRENT_STATE (pPeer) & BGP4_PEER_DEINIT_INPROGRESS) !=
        BGP4_PEER_DEINIT_INPROGRESS)
    {
        /* Release the lists, (input list, output list and the pending list)
         * allocated for this peer. 
         */
        Bgp4DshReleasePeerRtLists (pPeer, BGP4_FALSE);
    }

    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name : Bgp4PeerEbgpMultihopHandler                              */
/* Description   : This route handles the change in the EBGP Multi hop      */
/*               : status of the peer                                       */
/* Input(s)      : Peer whose EBGP Multihop status is altered (pPeer)       */
/* Output(s)     : None.                                                    */
/* Return(s)     : BGP4_SUCCESS - if operation is successful                */
/*               : BGP4_FAILURE - otherwise                                 */
/****************************************************************************/
INT4
Bgp4PeerEbgpMultihopHandler (tBgp4PeerEntry * pPeer)
{
    UINT4               u4VrfId;
    UINT2               u2Afi;

    if ((BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_STOP) &&
        (!(BGP4_GET_PEER_PEND_FLAG (pPeer) & BGP4_PEER_MULTIHOP_PEND_START)))
    {
        /* Currently Peer Admin State is disable and Peer is not pending
         * for start with multihop enabled. */
        return BGP4_SUCCESS;
    }

    /* Check for the peer type. If IBGP or EBGP peer and directly connected
     * then this change has no effect */
    u4VrfId = BGP4_PEER_CXT_ID (pPeer);
    if ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeer), pPeer) ==
         BGP4_INTERNAL_PEER)
        ||
        ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeer), pPeer) ==
          BGP4_EXTERNAL_PEER)
         &&
         (Bgp4IsDirectlyConnected (BGP4_PEER_REMOTE_ADDR_INFO (pPeer), u4VrfId)
          == BGP4_TRUE)))
    {
        return BGP4_SUCCESS;
    }

    /* External Peer not directly connected */
    switch (BGP4_PEER_EBGP_MULTIHOP (pPeer))
    {
        case BGP4_EBGP_MULTI_HOP_ENABLE:
            /* If peer admin state is DOWN, then set peer admin status
             * to START */
            if (BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_STOP)
            {                    /* Initialising the peer structure */
                u2Afi =
                    BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_PEER_REMOTE_ADDR_INFO
                                                  (pPeer));
                if (u2Afi == BGP4_INET_AFI_IPV4)
                {
                    Bgp4InitNetAddressStruct (&
                                              (BGP4_PEER_LOCAL_NETADDR_INFO
                                               (pPeer)), BGP4_INET_AFI_IPV4,
                                              BGP4_INET_SAFI_UNICAST);
                    PTR_ASSIGN_4 (BGP4_PEER_LOCAL_ADDR (pPeer),
                                  BGP4_LOCAL_BGP_ID (BGP4_PEER_CXT_ID (pPeer)));
                }
#ifdef BGP_TCP6_WANTED
                else
                {
                    INT4                i4Ret;
                    Bgp4InitNetAddressStruct (&
                                              (BGP4_PEER_LOCAL_NETADDR_INFO
                                               (pPeer)), BGP4_INET_AFI_IPV6,
                                              BGP4_INET_SAFI_UNICAST);
                    i4Ret =
                        Bgp4GetLocalAddrForPeer (pPeer,
                                                 BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeer),
                                                 &(BGP4_PEER_LOCAL_NETADDR_INFO
                                                   (pPeer)), BGP4_FALSE);
                    if (i4Ret == BGP4_FAILURE)
                    {
                        return BGP4_FAILURE;
                    }
                }
#endif
                if (BGP4_PEER_ALLOW_AUTOMATIC_START (pPeer) ==
                    BGP4_PEER_AUTOMATICSTART_ENABLE)
                {
                    BGP4_PEER_ADMIN_STATUS (pPeer) = BGP4_PEER_AUTO_START;
                }
                else
                {
                    BGP4_PEER_ADMIN_STATUS (pPeer) = BGP4_PEER_START;
                }
                BGP4_CHANGE_STATE (pPeer, BGP4_IDLE_STATE);
                Bgp4AddTransitionToFsmHist (pPeer, BGP4_IDLE_STATE);
            }

            /* Reset the peer pend multihop flag */
            BGP4_RESET_PEER_PEND_FLAG (pPeer, BGP4_PEER_MULTIHOP_PEND_START);

            if (BGP4_PEER_STATE (pPeer) != BGP4_IDLE_STATE)
            {
                /* This should not happen at all. If happens then some
                 * problem. */
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                          BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC | BGP4_MGMT_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tBgp4EbgpMultihopHandler() : PEER STATE NOT IDLE!!\n");
                Bgp4DisablePeer (pPeer);
            }
            /* Enabling the peer connection. */
            if (Bgp4EnablePeer (pPeer) == BGP4_FAILURE)
            {
                return BGP4_FAILURE;
            }

            break;

        case BGP4_EBGP_MULTI_HOP_DISABLE:
            /* EBGP Multihop disabled for the non-directly connected
             * external peer. Peer connection needs to be disabled and
             * should be started only if EBGP Multihop status is set to
             * enable. */
            Bgp4DisablePeer (pPeer);
            if ((BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_START) ||
                (BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_AUTO_START))
            {
                BGP4_SET_PEER_PEND_FLAG (pPeer, BGP4_PEER_MULTIHOP_PEND_START);
                BGP4_PEER_ADMIN_STATUS (pPeer) = BGP4_PEER_STOP;
            }
            break;
        default:
            break;
    }
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name : Bgp4PeerSetAllActivePeerAdminStatus                      */
/* Description   : This function process all the peers, whose admin status  */
/*               : is UP according to the input status. If the input is     */
/*               : ADMIN DOWN, then the Peer Session is closed else if the  */
/*               : input status is ADMIN UP, then the Peer Session is       */
/*               : re-initiated.                                            */
/* Input(s)      : Admin Status (i4AdminStatus).                            */
/* Output(s)     : None.                                                    */
/* Return(s)     : BGP4_SUCCESS                                             */
/****************************************************************************/
INT4
Bgp4PeerSetAllActivePeerAdminStatus (UINT4 u4Context, INT4 i4AdminStatus)
{
    tBgp4PeerEntry     *pPeer = NULL;
    INT1                i1Index = 0;

    pPeer = NULL;
    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
    {
        switch (i4AdminStatus)
        {
            case BGP4_ADMIN_UP:
                BGP4_PEER_FSM_HIST_HEAD (pPeer) = BGP4_INVALID_STATE;
                BGP4_PEER_FSM_HIST_TAIL (pPeer) = BGP4_INVALID_STATE;
                for (i1Index = 0; i1Index < BGP4_FSM_HIST_MAX_SIZE; i1Index++)
                {
                    (BGP4_PEER_FSM_TRANSITION_HIST (pPeer, i1Index)) = 0;
                }
                BGP4_CHANGE_STATE (pPeer, BGP4_IDLE_STATE);
                Bgp4AddTransitionToFsmHist (pPeer, BGP4_IDLE_STATE);
                if ((BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_START) ||
                    (BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_AUTO_START))
                {
                    Bgp4EnablePeer (pPeer);
                }
                else if ((BGP4_GET_PEER_PEND_FLAG (pPeer) &
                          BGP4_PEER_START_PEND) == BGP4_PEER_START_PEND)
                {
                    BGP4_RESET_PEER_PEND_FLAG (pPeer, BGP4_PEER_START_PEND);
                    Bgp4PeerStartPeer (u4Context,
                                       BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
                }
                break;

            case BGP4_ADMIN_DOWN:
                if ((BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_START) ||
                    (BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_AUTO_START))
                {
                    BGP4_PEER_START_TIME (pPeer) = BGP4_DEF_STARTINTERVAL;
                    Bgp4DisablePeer (pPeer);
                }
                /* Check for the peer's current state and pending state
                 * and process the peer accordingly. */
                if ((BGP4_GET_PEER_CURRENT_STATE (pPeer) &
                     BGP4_PEER_DEINIT_INPROGRESS) ==
                    BGP4_PEER_DEINIT_INPROGRESS)
                {
                    /* Remove the peer from the peer De-Init list. */
                    Bgp4DshRemovePeerFromList (BGP4_PEER_DEINIT_LIST
                                               (BGP4_PEER_CXT_ID (pPeer)),
                                               pPeer);
                    Bgp4DshReleasePeerRtLists (pPeer, BGP4_FALSE);
                    Bgp4InitNetAddressStruct (&
                                              (BGP4_PEER_INIT_NETADDR_INFO
                                               (pPeer)), BGP4_INET_AFI_IPV4,
                                              BGP4_INET_SAFI_UNICAST);
                    BGP4_SET_PEER_CURRENT_STATE (pPeer, BGP4_PEER_READY);
                }
                break;

            default:
                break;
        }
    }
    return BGP4_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME : Bgp4PeerUpdateDeInitRoutesToPeerTxQ
 * DESCRIPTION   : This routines takes a route from the peer deinit route list
 *               : and advertises it to all peers. The peer  status is updated
 *               : after the deinit route list is  
 * INPUTS        : pPeerInfo - Pointer to the peer information 
 * OUTPUTS       : None
 * RETURNS       : BGP4_SUCCESS/BGP4_FAILURE
 ***************************************************************************/
INT4
Bgp4PeerUpdateDeInitRoutesToPeerTxQ (tBgp4PeerEntry * pPeerInfo)
{
    /* Variable Declarations */
    tTMO_SLL            TsDummyFeasibleRtList;
    tLinkNode          *pRtLink = NULL;
    tBgp4PeerEntry     *pPeerentry = NULL;

    TMO_SLL_Init (&(TsDummyFeasibleRtList));

    /* Peer DeInit routes need to be sent to all peers */
    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (BGP4_PEER_CXT_ID (pPeerInfo)),
                  pPeerentry, tBgp4PeerEntry *)
    {
        if (BGP4_PEER_STATE (pPeerentry) != BGP4_ESTABLISHED_STATE)
        {
            /* Connection not in established state. So no need to
             * advertise to this peer. */
            continue;
        }

        /* Process the route advt for this peer. */
        Bgp4PeerSendRoutesToPeerTxQ (pPeerentry,
                                     &TsDummyFeasibleRtList,
                                     BGP4_PEER_IPV4_DEINIT_RT_LIST (pPeerInfo),
                                     BGP4_TRUE);
#ifdef BGP4_IPV6_WANTED
        if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeerInfo) != NULL)
        {
            Bgp4PeerSendRoutesToPeerTxQ (pPeerentry,
                                         &TsDummyFeasibleRtList,
                                         BGP4_PEER_IPV6_DEINIT_RT_LIST
                                         (pPeerInfo), BGP4_TRUE);
        }
#endif
#ifdef L3VPN
        if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeerInfo) != NULL)
        {
            Bgp4PeerSendRoutesToPeerTxQ (pPeerentry,
                                         &TsDummyFeasibleRtList,
                                         BGP4_PEER_IPV4_LBLD_DEINIT_RT_LIST
                                         (pPeerInfo), BGP4_TRUE);
        }
        if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeerInfo) != NULL)
        {
            Bgp4PeerSendRoutesToPeerTxQ (pPeerentry,
                                         &TsDummyFeasibleRtList,
                                         BGP4_PEER_VPN4_DEINIT_RT_LIST
                                         (pPeerInfo), BGP4_TRUE);
        }
#endif
        /* Processing of route for this peer is completed.
         * Continue for the next peer. */
        Bgp4DshReleaseList (&TsDummyFeasibleRtList, 0);
    }
#ifdef L3VPN
    if (BGP4_VPN4_PEER_ROLE (pPeerInfo) == BGP4_VPN4_CE_PEER)
    {
        TMO_SLL_Scan ((BGP4_PEERENTRY_HEAD (0)), pPeerentry, tBgp4PeerEntry *)
        {
            if (BGP4_PEER_STATE (pPeerentry) != BGP4_ESTABLISHED_STATE)
            {
                /* Connection not in established state. So no need to
                 * advertise to this peer. */
                continue;
            }

            /* Process the route advt for this peer. */
            Bgp4PeerSendRoutesToPeerTxQ (pPeerentry,
                                         &TsDummyFeasibleRtList,
                                         BGP4_PEER_IPV4_DEINIT_RT_LIST
                                         (pPeerInfo), BGP4_TRUE);
            if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeerInfo) != NULL)
            {
                Bgp4PeerSendRoutesToPeerTxQ (pPeerentry,
                                             &TsDummyFeasibleRtList,
                                             BGP4_PEER_VPN4_DEINIT_RT_LIST
                                             (pPeerInfo), BGP4_TRUE);
            }
            /* Processing of route for this peer is completed.
             * Continue for the next peer. */
            Bgp4DshReleaseList (&TsDummyFeasibleRtList, 0);
        }
    }
#endif
    /*  The flags NOTTO_EXTERNAL and NOTTO_INTERNAL needs to be reset now. */
    TMO_SLL_Scan (BGP4_PEER_IPV4_DEINIT_RT_LIST (pPeerInfo), pRtLink,
                  tLinkNode *)
    {
        BGP4_RT_RESET_FLAG (pRtLink->pRouteProfile,
                            (BGP4_RT_ADVT_NOTTO_INTERNAL |
                             BGP4_RT_ADVT_NOTTO_EXTERNAL |
                             BGP4_RT_REPLACEMENT |
                             BGP4_RT_IN_PEER_DEINIT_LIST));
    }
    Bgp4DshReleaseList (BGP4_PEER_IPV4_DEINIT_RT_LIST (pPeerInfo), 0);
#ifdef BGP4_IPV6_WANTED
    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeerInfo) != NULL)
    {
        TMO_SLL_Scan (BGP4_PEER_IPV6_DEINIT_RT_LIST (pPeerInfo),
                      pRtLink, tLinkNode *)
        {
            BGP4_RT_RESET_FLAG (pRtLink->pRouteProfile,
                                (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                 BGP4_RT_ADVT_NOTTO_EXTERNAL |
                                 BGP4_RT_REPLACEMENT |
                                 BGP4_RT_WITHDRAWN |
                                 BGP4_RT_IN_PEER_DEINIT_LIST));
        }
        Bgp4DshReleaseList (BGP4_PEER_IPV6_DEINIT_RT_LIST (pPeerInfo), 0);
    }
#endif
#ifdef L3VPN
    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeerInfo) != NULL)
    {
        TMO_SLL_Scan (BGP4_PEER_IPV4_LBLD_DEINIT_RT_LIST (pPeerInfo),
                      pRtLink, tLinkNode *)
        {
            BGP4_RT_RESET_FLAG (pRtLink->pRouteProfile,
                                (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                 BGP4_RT_ADVT_NOTTO_EXTERNAL |
                                 BGP4_RT_REPLACEMENT |
                                 BGP4_RT_WITHDRAWN |
                                 BGP4_RT_IN_PEER_DEINIT_LIST));
        }
        Bgp4DshReleaseList (BGP4_PEER_IPV4_LBLD_DEINIT_RT_LIST (pPeerInfo), 0);
    }
    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeerInfo) != NULL)
    {
        TMO_SLL_Scan (BGP4_PEER_VPN4_DEINIT_RT_LIST (pPeerInfo),
                      pRtLink, tLinkNode *)
        {
            BGP4_RT_RESET_FLAG (pRtLink->pRouteProfile,
                                (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                 BGP4_RT_ADVT_NOTTO_EXTERNAL |
                                 BGP4_RT_REPLACEMENT |
                                 BGP4_RT_WITHDRAWN |
                                 BGP4_RT_IN_PEER_DEINIT_LIST));
        }
        Bgp4DshReleaseList (BGP4_PEER_VPN4_DEINIT_RT_LIST (pPeerInfo), 0);
    }
#endif
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4DshReleasePeerRtLists                                 */
/* Description   : Releases all the list of route profiles present in a      */
/*                 particular peer's entry and other peer related info.      */
/* Input(s)      : Peer Information which contains various list of route     */
/*                 profiles associated with it (pPeerEntry).                 */
/*                 u4Flag - Flag to indicate if throttling needs to be done  */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE/BGP4_FALSE.                                     */
/*                 BGP4_TRUE - if release activity complete                  */
/*                 BGP4_FALSE - if release of peer rt lists incomplete       */
/*****************************************************************************/
INT4
Bgp4DshReleasePeerRtLists (tBgp4PeerEntry * pPeerEntry, UINT4 u4Flag)
{
    tTMO_SLL           *pTsList = NULL;
    tBufNode           *pBufNode = NULL;
    tBufNode           *pTmpBufNode = NULL;
    tAfiSafiNode       *pAsafiPtr = NULL;
    tAfiSafiNode       *pTmpAfiPtr = NULL;
    UINT4               u4RtCnt = 0;
    UINT4               u4TotalRtCnt = 0;
    INT4                i4RetVal;

    /* Initialize the number of route to be processed. */
    u4RtCnt = (u4Flag == BGP4_TRUE) ? BGP4_MAX_DELRTS2PROCESS : 0;

    /* Free the input packet reception list */
    pTsList = BGP4_PEER_RCVD_ROUTES (pPeerEntry);
    u4TotalRtCnt = Bgp4DshReleaseList (pTsList, u4RtCnt);
    if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= BGP4_MAX_DELRTS2PROCESS))
    {
        return BGP4_FALSE;
    }

    /* Free the peer advt message list. */
    BGP_SLL_DYN_Scan (BGP4_PEER_ADVT_MSG_LIST (pPeerEntry), pBufNode,
                      pTmpBufNode, tBufNode *)
    {
        TMO_SLL_Delete (BGP4_PEER_ADVT_MSG_LIST (pPeerEntry),
                        &pBufNode->TSNext);
        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pBufNode->pu1Msg);
        MemReleaseMemBlock (gBgpNode.Bgp4BufNodesPoolId, (UINT1 *) pBufNode);
        u4TotalRtCnt++;
        if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= BGP4_MAX_DELRTS2PROCESS))
        {
            return BGP4_FALSE;
        }
    }

    /* Release IPV4_UNICAST specific lists */
    u4RtCnt = (u4Flag == BGP4_TRUE) ?
        (BGP4_MAX_DELRTS2PROCESS - u4TotalRtCnt) : 0;
    i4RetVal = Bgp4DshReleasePeerAsafiRtLists (pPeerEntry, BGP4_IPV4_UNI_INDEX,
                                               u4Flag, &u4RtCnt);
    u4TotalRtCnt += u4RtCnt;
    if ((i4RetVal == BGP4_FALSE) ||
        ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= BGP4_MAX_DELRTS2PROCESS)))
    {
        return BGP4_FALSE;
    }

#ifdef L3VPN
    /* Carrying Label Information - RFC 3107 */
    /* Release IPV4_LABEL specific lists */
    u4RtCnt = (u4Flag == BGP4_TRUE) ?
        (BGP4_MAX_DELRTS2PROCESS - u4TotalRtCnt) : 0;
    i4RetVal = Bgp4DshReleasePeerAsafiRtLists (pPeerEntry, BGP4_IPV4_LBLD_INDEX,
                                               u4Flag, &u4RtCnt);
    u4TotalRtCnt += u4RtCnt;
    if ((i4RetVal == BGP4_FALSE) ||
        ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= BGP4_MAX_DELRTS2PROCESS)))
    {
        return BGP4_FALSE;
    }

    /* Release VPN4_UNICAST specific lists */
    u4RtCnt = (u4Flag == BGP4_TRUE) ?
        (BGP4_MAX_DELRTS2PROCESS - u4TotalRtCnt) : 0;
    i4RetVal = Bgp4DshReleasePeerAsafiRtLists (pPeerEntry, BGP4_VPN4_UNI_INDEX,
                                               u4Flag, &u4RtCnt);
    u4TotalRtCnt += u4RtCnt;
    if ((i4RetVal == BGP4_FALSE) ||
        ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= BGP4_MAX_DELRTS2PROCESS)))
    {
        return BGP4_FALSE;
    }
#endif
#ifdef BGP4_IPV6_WANTED
    /* Release IPV6_UNICAST specific lists */
    u4RtCnt = (u4Flag == BGP4_TRUE) ?
        (BGP4_MAX_DELRTS2PROCESS - u4TotalRtCnt) : 0;
    i4RetVal = Bgp4DshReleasePeerAsafiRtLists (pPeerEntry, BGP4_IPV6_UNI_INDEX,
                                               u4Flag, &u4RtCnt);
    u4TotalRtCnt += u4RtCnt;
    if ((i4RetVal == BGP4_FALSE) ||
        ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= BGP4_MAX_DELRTS2PROCESS)))
    {
        return BGP4_FALSE;
    }
#endif

#ifdef VPLSADS_WANTED
    /* Release L2VPN,VPLS specific lists */
    u4RtCnt = (u4Flag == BGP4_TRUE) ?
        (BGP4_MAX_DELRTS2PROCESS - u4TotalRtCnt) : 0;
    i4RetVal =
        Bgp4DshReleasePeerAsafiRtLists (pPeerEntry, BGP4_L2VPN_VPLS_INDEX,
                                        u4Flag, &u4RtCnt);
    u4TotalRtCnt += u4RtCnt;
    if ((i4RetVal == BGP4_FALSE) ||
        ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= BGP4_MAX_DELRTS2PROCESS)))
    {
        return BGP4_FALSE;
    }

#endif
#ifdef EVPN_WANTED
    /* Release L2VPN,EVPN specific lists */
    u4RtCnt = (u4Flag == BGP4_TRUE) ?
        (BGP4_MAX_DELRTS2PROCESS - u4TotalRtCnt) : 0;
    i4RetVal =
        Bgp4DshReleasePeerAsafiRtLists (pPeerEntry, BGP4_L2VPN_EVPN_INDEX,
                                        u4Flag, &u4RtCnt);
    u4TotalRtCnt += u4RtCnt;
    if ((i4RetVal == BGP4_FALSE) ||
        ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= BGP4_MAX_DELRTS2PROCESS)))
    {
        return BGP4_FALSE;
    }

#endif

    /* Free the peer soft-config pend lists */
    BGP_SLL_DYN_Scan (BGP4_PEER_SOFTCONFIG_IN_PEND_LIST (pPeerEntry), pAsafiPtr,
                      pTmpAfiPtr, tAfiSafiNode *)
    {
        TMO_SLL_Delete (BGP4_PEER_SOFTCONFIG_IN_PEND_LIST (pPeerEntry),
                        &pAsafiPtr->TsAfiSafiNext);
        AFI_SAFI_NODE_FREE (pAsafiPtr);

    }

    BGP_SLL_DYN_Scan (BGP4_PEER_SOFTCONFIG_OUT_PEND_LIST (pPeerEntry),
                      pAsafiPtr, pTmpAfiPtr, tAfiSafiNode *)
    {
        TMO_SLL_Delete (BGP4_PEER_SOFTCONFIG_OUT_PEND_LIST (pPeerEntry),
                        &pAsafiPtr->TsAfiSafiNext);
        AFI_SAFI_NODE_FREE ((UINT1 *) pAsafiPtr);
    }

    /* Clear the peer routes from the Non-Sync list. */
    if (BGP4_LOCAL_SYNC_STATUS (BGP4_PEER_CXT_ID (pPeerEntry)) ==
        BGP4_ENABLE_SYNC)
    {
        Bgp4DshRemovePeerRoutesFromList (BGP4_NONSYNC_LIST
                                         (BGP4_PEER_CXT_ID (pPeerEntry)),
                                         pPeerEntry);
    }

    /* Free the Peer's route from the overlapping route list */
    Bgp4DshRemovePeerRoutesFromOverlapList (pPeerEntry);

    return BGP4_TRUE;
}

/*****************************************************************************/
/* Function Name : Bgp4DshReleasePeerRouteList                               */
/* Description   : Releases all the list of route profiles present in a      */
/*                 particular peer's route list                              */
/* Input(s)      : pPeerEntry - Peer Information                             */
/*                 u4Index - index corresponding to a particular <AFI, SAFI> */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
Bgp4DshReleasePeerRouteList (tBgp4PeerEntry * pPeerEntry, UINT4 u4Index)
{
    tRouteProfile      *pRoute = NULL;
    tRouteProfile      *pTmpRoute = NULL;

    /* Free the peer route list. */
    pRoute = BGP4_DLL_FIRST_ROUTE (BGP4_PEER_ROUTE_LIST (pPeerEntry, u4Index));
    while (pRoute != NULL)
    {
        pTmpRoute = BGP4_RT_PROTO_LINK_NEXT (pRoute);
        BGP4_RT_PROTO_LINK_PREV (pRoute) = NULL;
        BGP4_RT_PROTO_LINK_NEXT (pRoute) = NULL;
        Bgp4DshReleaseRtInfo (pRoute);
        pRoute = pTmpRoute;
    }
    BGP4_DLL_FIRST_ROUTE (BGP4_PEER_ROUTE_LIST (pPeerEntry, u4Index)) = NULL;
    BGP4_DLL_LAST_ROUTE (BGP4_PEER_ROUTE_LIST (pPeerEntry, u4Index)) = NULL;
    BGP4_DLL_COUNT (BGP4_PEER_ROUTE_LIST (pPeerEntry, u4Index)) = 0;
    return;
}

/*****************************************************************************/
/* Function Name : Bgp4DshReleasePeerAsafiRtLists                            */
/* Description   : Releases all the list of route profiles present in a      */
/*                 particular peer's entry for an <AFI, SAFI> information    */
/* Input(s)      : Peer Information which contains various list of route     */
/*                 profiles associated with it (pPeerEntry).                 */
/*                 u4Index - index corresponding to a particular <AFI, SAFI> */
/*                 u4Flag - Flag to indicate if throttling needs to be done  */
/*                 pu4NoofRoutes - MAX Number of route to be released if     */
/*                                 throttling needs to be applied.           */
/* Output(s)     : pu4NoofRoutes - Total no of routes that are released      */
/* Return(s)     : BGP4_TRUE/BGP4_FALSE.                                     */
/*                 BGP4_TRUE - if release activity complete                  */
/*                 BGP4_FALSE - if release of peer rt lists incomplete       */
/*****************************************************************************/
INT4
Bgp4DshReleasePeerAsafiRtLists (tBgp4PeerEntry * pPeerEntry, UINT4 u4Index,
                                UINT4 u4Flag, UINT4 *pu4NoofRoutes)
{
    tTMO_SLL           *pTsList = NULL;
    UINT4               u4RtCnt = 0;
    UINT4               u4TotalRtCnt = 0;

    switch (u4Index)
    {
        case BGP4_IPV4_UNI_INDEX:
            /*  Free the Peer's Output Route List */
            u4RtCnt = (u4Flag == BGP4_TRUE) ? *pu4NoofRoutes : 0;
            u4TotalRtCnt =
                Bgp4ClearHashTable (BGP4_PEER_IPV4_OUTPUT_ROUTES (pPeerEntry),
                                    u4RtCnt, BGP4_TRUE);

            if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
            {
                *pu4NoofRoutes = u4TotalRtCnt;
                return BGP4_FALSE;
            }

            /*  Free the Peer's New yetto Advt. Route List */
            u4RtCnt =
                (u4Flag == BGP4_TRUE) ? (*pu4NoofRoutes - u4TotalRtCnt) : 0;
            u4TotalRtCnt +=
                Bgp4ClearHashTable (BGP4_PEER_IPV4_NEW_ROUTES (pPeerEntry),
                                    u4RtCnt, BGP4_FALSE);

            if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
            {
                *pu4NoofRoutes = u4TotalRtCnt;
                return BGP4_FALSE;
            }

            /*  Free the Peer's  yetto Advt. Local Route List */
            u4RtCnt =
                (u4Flag == BGP4_TRUE) ? (*pu4NoofRoutes - u4TotalRtCnt) : 0;
            u4TotalRtCnt +=
                Bgp4ClearHashTable (BGP4_PEER_IPV4_NEW_LOCAL_ROUTES
                                    (pPeerEntry), u4RtCnt, BGP4_TRUE);

            if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
            {
                *pu4NoofRoutes = u4TotalRtCnt;
                return BGP4_FALSE;
            }

            /* Free the Advertised withdrawn and feasible route list */
            u4RtCnt =
                (u4Flag == BGP4_TRUE) ? (*pu4NoofRoutes - u4TotalRtCnt) : 0;
            u4TotalRtCnt +=
                Bgp4ClearHashTable (BGP4_PEER_IPV4_ADVT_WITH_ROUTES
                                    (pPeerEntry), u4RtCnt, BGP4_FALSE);

            if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
            {
                *pu4NoofRoutes = u4TotalRtCnt;
                return BGP4_FALSE;
            }

            u4RtCnt =
                (u4Flag == BGP4_TRUE) ? (*pu4NoofRoutes - u4TotalRtCnt) : 0;
            u4TotalRtCnt +=
                Bgp4ClearHashTable (BGP4_PEER_IPV4_ADVT_FEAS_ROUTES
                                    (pPeerEntry), u4RtCnt, BGP4_FALSE);

            if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
            {
                *pu4NoofRoutes = u4TotalRtCnt;
                return BGP4_FALSE;
            }

            /* Free the peer de-init route list */
            pTsList = BGP4_PEER_IPV4_DEINIT_RT_LIST (pPeerEntry);
            u4RtCnt =
                (u4Flag == BGP4_TRUE) ? (*pu4NoofRoutes - u4TotalRtCnt) : 0;
            u4TotalRtCnt += Bgp4DshReleaseList (pTsList, u4RtCnt);
            if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
            {
                *pu4NoofRoutes = u4TotalRtCnt;
                return BGP4_FALSE;
            }
            break;
#ifdef L3VPN
        case BGP4_IPV4_LBLD_INDEX:
            if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeerEntry) != NULL)
            {
                /*  Free the Peer's Output Route List */
                u4RtCnt = (u4Flag == BGP4_TRUE) ? *pu4NoofRoutes : 0;
                u4TotalRtCnt =
                    Bgp4ClearHashTable (BGP4_PEER_IPV4_LBLD_OUTPUT_ROUTES
                                        (pPeerEntry), u4RtCnt, BGP4_TRUE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                /*  Free the Peer's New yetto Advt. Route List */
                u4RtCnt = (u4Flag == BGP4_TRUE) ?
                    (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt +=
                    Bgp4ClearHashTable (BGP4_PEER_IPV4_LBLD_NEW_ROUTES
                                        (pPeerEntry), u4RtCnt, BGP4_FALSE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                /*  Free the Peer's  yetto Advt. Local Route List */
                u4RtCnt = (u4Flag == BGP4_TRUE) ?
                    (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt +=
                    Bgp4ClearHashTable (BGP4_PEER_IPV4_LBLD_NEW_LOCAL_ROUTES
                                        (pPeerEntry), u4RtCnt, BGP4_TRUE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                /* Free the Advertised withdrawn and feasible route list */
                u4RtCnt = (u4Flag == BGP4_TRUE) ?
                    (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt +=
                    Bgp4ClearHashTable (BGP4_PEER_IPV4_LBLD_ADVT_WITH_ROUTES
                                        (pPeerEntry), u4RtCnt, BGP4_FALSE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                u4RtCnt = (u4Flag == BGP4_TRUE) ?
                    (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt +=
                    Bgp4ClearHashTable (BGP4_PEER_IPV4_LBLD_ADVT_FEAS_ROUTES
                                        (pPeerEntry), u4RtCnt, BGP4_FALSE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                /* Free the peer de-init route list */
                pTsList = BGP4_PEER_IPV4_LBLD_DEINIT_RT_LIST (pPeerEntry);
                u4RtCnt = (u4Flag == BGP4_TRUE) ?
                    (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt += Bgp4DshReleaseList (pTsList, u4RtCnt);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }
            }
            break;
        case BGP4_VPN4_UNI_INDEX:
            if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeerEntry) != NULL)
            {
                /*  Free the Peer's Output Route List */
                u4RtCnt = (u4Flag == BGP4_TRUE) ? *pu4NoofRoutes : 0;
                u4TotalRtCnt =
                    Bgp4ClearHashTable (BGP4_PEER_VPN4_OUTPUT_ROUTES
                                        (pPeerEntry), u4RtCnt, BGP4_TRUE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                /*  Free the Peer's New yetto Advt. Route List */
                u4RtCnt = (u4Flag == BGP4_TRUE) ?
                    (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt +=
                    Bgp4ClearHashTable (BGP4_PEER_VPN4_NEW_ROUTES (pPeerEntry),
                                        u4RtCnt, BGP4_FALSE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                /*  Free the Peer's  yetto Advt. Local Route List */
                u4RtCnt = (u4Flag == BGP4_TRUE) ?
                    (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt +=
                    Bgp4ClearHashTable (BGP4_PEER_VPN4_NEW_LOCAL_ROUTES
                                        (pPeerEntry), u4RtCnt, BGP4_TRUE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                /* Free the Advertised withdrawn and feasible route list */
                u4RtCnt = (u4Flag == BGP4_TRUE) ?
                    (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt +=
                    Bgp4ClearHashTable (BGP4_PEER_VPN4_ADVT_WITH_ROUTES
                                        (pPeerEntry), u4RtCnt, BGP4_FALSE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                u4RtCnt = (u4Flag == BGP4_TRUE) ?
                    (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt +=
                    Bgp4ClearHashTable (BGP4_PEER_VPN4_ADVT_FEAS_ROUTES
                                        (pPeerEntry), u4RtCnt, BGP4_FALSE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                /* Free the peer de-init route list */
                pTsList = BGP4_PEER_VPN4_DEINIT_RT_LIST (pPeerEntry);
                u4RtCnt = (u4Flag == BGP4_TRUE) ?
                    (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt += Bgp4DshReleaseList (pTsList, u4RtCnt);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }
            }
            break;
#endif
#ifdef BGP4_IPV6_WANTED
        case BGP4_IPV6_UNI_INDEX:
            if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeerEntry) != NULL)
            {
                /*  Free the Peer's Output Route List */
                u4RtCnt = (u4Flag == BGP4_TRUE) ? *pu4NoofRoutes : 0;
                u4TotalRtCnt = Bgp4ClearHashTable
                    (BGP4_PEER_IPV6_OUTPUT_ROUTES (pPeerEntry), u4RtCnt,
                     BGP4_TRUE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                /*  Free the Peer's New yetto Advt. Route List */
                u4RtCnt =
                    (u4Flag == BGP4_TRUE) ? (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt +=
                    Bgp4ClearHashTable (BGP4_PEER_IPV6_NEW_ROUTES (pPeerEntry),
                                        u4RtCnt, BGP4_FALSE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                /*  Free the Peer's  yetto Advt. Local Route List */
                u4RtCnt =
                    (u4Flag == BGP4_TRUE) ? (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt +=
                    Bgp4ClearHashTable (BGP4_PEER_IPV6_NEW_LOCAL_ROUTES
                                        (pPeerEntry), u4RtCnt, BGP4_TRUE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                /* Free the Advertised withdrawn and feasible route list */
                u4RtCnt =
                    (u4Flag == BGP4_TRUE) ? (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt +=
                    Bgp4ClearHashTable (BGP4_PEER_IPV6_ADVT_WITH_ROUTES
                                        (pPeerEntry), u4RtCnt, BGP4_FALSE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                u4RtCnt =
                    (u4Flag == BGP4_TRUE) ? (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt +=
                    Bgp4ClearHashTable (BGP4_PEER_IPV6_ADVT_FEAS_ROUTES
                                        (pPeerEntry), u4RtCnt, BGP4_FALSE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                /* Free the peer de-init route list */
                pTsList = BGP4_PEER_IPV6_DEINIT_RT_LIST (pPeerEntry);
                u4RtCnt =
                    (u4Flag == BGP4_TRUE) ? (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt += Bgp4DshReleaseList (pTsList, u4RtCnt);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }
            }
            break;
#endif
#ifdef VPLSADS_WANTED
        case BGP4_L2VPN_VPLS_INDEX:
            if (BGP4_PEER_VPLS_AFISAFI_INSTANCE (pPeerEntry) != NULL)
            {
                /*  Free the Peer's Output Route List */
                u4RtCnt = (u4Flag == BGP4_TRUE) ? *pu4NoofRoutes : 0;
                u4TotalRtCnt = Bgp4ClearHashTable
                    (BGP4_PEER_VPLS_OUTPUT_ROUTES (pPeerEntry), u4RtCnt,
                     BGP4_TRUE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                /*  Free the Peer's New Advt. Route List */
                u4RtCnt =
                    (u4Flag == BGP4_TRUE) ? (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt +=
                    Bgp4ClearHashTable (BGP4_PEER_VPLS_NEW_ROUTES (pPeerEntry),
                                        u4RtCnt, BGP4_FALSE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                /*  Free the Peer's Advt. Local Route List */
                u4RtCnt =
                    (u4Flag == BGP4_TRUE) ? (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt +=
                    Bgp4ClearHashTable (BGP4_PEER_VPLS_NEW_LOCAL_ROUTES
                                        (pPeerEntry), u4RtCnt, BGP4_TRUE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                /* Free the Advertised withdrawn and feasible route list */
                u4RtCnt =
                    (u4Flag == BGP4_TRUE) ? (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt +=
                    Bgp4ClearHashTable (BGP4_PEER_VPLS_ADVT_WITH_ROUTES
                                        (pPeerEntry), u4RtCnt, BGP4_FALSE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                u4RtCnt =
                    (u4Flag == BGP4_TRUE) ? (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt +=
                    Bgp4ClearHashTable (BGP4_PEER_VPLS_ADVT_FEAS_ROUTES
                                        (pPeerEntry), u4RtCnt, BGP4_FALSE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                /* Free the peer de-init route list */
                pTsList = BGP4_PEER_VPLS_DEINIT_RT_LIST (pPeerEntry);
                u4RtCnt =
                    (u4Flag == BGP4_TRUE) ? (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt += Bgp4DshReleaseList (pTsList, u4RtCnt);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }
            }
            break;
#endif
#ifdef EVPN_WANTED
        case BGP4_L2VPN_EVPN_INDEX:
            if (BGP4_PEER_EVPN_AFISAFI_INSTANCE (pPeerEntry) != NULL)
            {
                /*  Free the Peer's Output Route List */
                u4RtCnt = (u4Flag == BGP4_TRUE) ? *pu4NoofRoutes : 0;
                u4TotalRtCnt = Bgp4ClearHashTable
                    (BGP4_PEER_EVPN_OUTPUT_ROUTES (pPeerEntry), u4RtCnt,
                     BGP4_TRUE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                /*  Free the Peer's New Advt. Route List */
                u4RtCnt =
                    (u4Flag == BGP4_TRUE) ? (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt +=
                    Bgp4ClearHashTable (BGP4_PEER_EVPN_NEW_ROUTES (pPeerEntry),
                                        u4RtCnt, BGP4_FALSE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }
                /*  Free the Peer's Advt. Local Route List */
                u4RtCnt =
                    (u4Flag == BGP4_TRUE) ? (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt +=
                    Bgp4ClearHashTable (BGP4_PEER_EVPN_NEW_LOCAL_ROUTES
                                        (pPeerEntry), u4RtCnt, BGP4_TRUE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                /* Free the Advertised withdrawn and feasible route list */
                u4RtCnt =
                    (u4Flag == BGP4_TRUE) ? (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt +=
                    Bgp4ClearHashTable (BGP4_PEER_EVPN_ADVT_WITH_ROUTES
                                        (pPeerEntry), u4RtCnt, BGP4_FALSE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }
                u4RtCnt =
                    (u4Flag == BGP4_TRUE) ? (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt +=
                    Bgp4ClearHashTable (BGP4_PEER_EVPN_ADVT_FEAS_ROUTES
                                        (pPeerEntry), u4RtCnt, BGP4_FALSE);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }

                /* Free the peer de-init route list */
                pTsList = BGP4_PEER_EVPN_DEINIT_RT_LIST (pPeerEntry);
                u4RtCnt =
                    (u4Flag == BGP4_TRUE) ? (*pu4NoofRoutes - u4TotalRtCnt) : 0;
                u4TotalRtCnt += Bgp4DshReleaseList (pTsList, u4RtCnt);
                if ((u4Flag == BGP4_TRUE) && (u4TotalRtCnt >= *pu4NoofRoutes))
                {
                    *pu4NoofRoutes = u4TotalRtCnt;
                    return BGP4_FALSE;
                }
            }
            break;
#endif

        default:
            *pu4NoofRoutes = 0;
            return BGP4_TRUE;
    }
    *pu4NoofRoutes = u4TotalRtCnt;
    return BGP4_TRUE;
}

/*****************************************************************************/
/* Function Name : Bgp4PeerHandleClearIpBgp                                  */
/* Description   : This function will restart either all the active peers    */
/*                 or will restart only a specific peer depanding on the     */
/*                 given input.                                              */
/* Input(s)      : u4PeerAddr - Specifies either a single peer or all peers  */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4PeerHandleClearIpBgp (UINT4 u4Context, tAddrPrefix PeerAddr)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         CurPeerAddr;
    tAddrPrefix         NextPeerAddr;
    tAddrPrefix         InvPrefix;
    INT4                i4Sts = BGP4_FAILURE;

    Bgp4InitAddrPrefixStruct (&(InvPrefix),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddr));
    if ((PrefixMatch (PeerAddr, InvPrefix)) != BGP4_TRUE)
    {
        /* Need to Reset only one peer. */
        pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddr);
        if (pPeer != NULL)
        {
            if ((BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_START) ||
                (BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_AUTO_START))
            {
                /* Peer is active. Reset the peer connection. */
                i4Sts = Bgp4DisablePeer (pPeer);
                if (i4Sts != BGP4_FAILURE)
                {
                    i4Sts = Bgp4EnablePeer (pPeer);
                }
            }
        }
        return i4Sts;
    }
    else
    {
        /* Need to reset all the active peers. Dont use the for loop
         * with BGP4_PEERENTRY_HEAD(BGP4_DFLT_VRFID), because some peers may have duplicate
         * entries in the Peer list. Such peers need to be processed only
         * once. */
        switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddr))
        {
            case BGP4_INET_AFI_IPV4:
                i4Sts =
                    Bgp4Ipv4GetFirstIndexBgpPeerTable (u4Context, &CurPeerAddr);
                break;

#ifdef BGP4_IPV6_WANTED
            case BGP4_INET_AFI_IPV6:
                i4Sts =
                    Bgp4Ipv6GetFirstIndexBgpPeerTable (u4Context, &CurPeerAddr);
                break;
#endif
            default:
                return BGP4_FAILURE;
        }

        if (i4Sts == BGP4_FAILURE)
        {
            /* No peer exists for this Address Family. */
            return BGP4_FAILURE;
        }

        do
        {
            pPeer = Bgp4SnmphGetPeerEntry (u4Context, CurPeerAddr);
            if (pPeer == NULL)
            {
                break;
            }
            /* Process the pPeer pointer. */
            if ((BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_START) ||
                (BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_AUTO_START))
            {
                Bgp4DisablePeer (pPeer);
                Bgp4EnablePeer (pPeer);
            }
            for (;;)
            {
                Bgp4InitAddrPrefixStruct (&(NextPeerAddr),
                                          BGP4_AFI_IN_ADDR_PREFIX_INFO
                                          (PeerAddr));
                i4Sts =
                    Bgp4GetNextIndexBgpPeerTable (u4Context, CurPeerAddr,
                                                  &NextPeerAddr);
                if (i4Sts == BGP4_FAILURE)
                {
                    break;
                }

                Bgp4CopyAddrPrefixStruct (&CurPeerAddr, NextPeerAddr);
                if (BGP4_AFI_IN_ADDR_PREFIX_INFO (NextPeerAddr) ==
                    BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddr))
                {
                    break;
                }
            }
            if (i4Sts == BGP4_FAILURE)
            {
                break;
            }
        }
        while (pPeer != NULL);
    }
    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Bgp4PeerAdvtDftRoute                                      */
/* Description   : This function will advertise the default route to the     */
/*                 peer with the local interface address as the next-hop.    */
/* Input(s)      : u4PeerAddr - Specifies the peer to which default route is */
/*                              to be advertised.                            */
/*                 u1Policy   - Flag indicating the new default route advt   */
/*                              status for the peer.                         */
/*                 u1IsReplace- Flag indicating whether the replacement flag */
/*                              should be set or not.                        */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4PeerAdvtDftRoute (tBgp4PeerEntry * pPeer, UINT1 u1Policy, UINT1 u1IsReplace)
{
    VOID               *pRibNode = NULL;
    tRouteProfile      *pDefaultRoute = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile       RtProfile;
    tBgp4Info          *pBgpInfo = NULL;
    tTMO_SLL            TsFeasList;
    tTMO_SLL            TsWithList;
    tAddrPrefix         RemAddr;
    tNetAddress         LocalAddr;
    UINT4               u4AsafiIndex = 0;
    UINT4               u4NegAsafiMask = 0;
    INT4                i4Status = BGP4_SUCCESS;
    UINT2               u2Afi = 0;
    UINT2               u2Afinfo = 0;
    UINT2               u2Safi = 0;

    TMO_SLL_Init (&TsFeasList);
    TMO_SLL_Init (&TsWithList);

    for (u4AsafiIndex = 0; u4AsafiIndex < BGP4_MPE_ADDR_FAMILY_MAX_INDEX;
         u4AsafiIndex++)
    {
        u2Afinfo = 0;
        u4NegAsafiMask = 0;
        i4Status = Bgp4GetAfiSafiFromIndex (u4AsafiIndex, &u2Afi, &u2Safi);
        if (i4Status == BGP4_FAILURE)
        {
            /* Address family not supported. */
        }
        if (u4AsafiIndex == BGP4_IPV4_UNI_INDEX)
        {
            u4NegAsafiMask = CAP_NEG_IPV4_UNI_MASK;
            u2Afinfo = BGP4_INET_AFI_IPV4;
        }
#ifdef BGP4_IPV6_WANTED
        else if (u4AsafiIndex == BGP4_IPV6_UNI_INDEX)
        {
            u4NegAsafiMask = CAP_NEG_IPV6_UNI_MASK;
            u2Afinfo = BGP4_INET_AFI_IPV6;
        }
#endif
#ifdef L3VPN
        /*Default routes are not supported for Labelled Ipv4 and Vpnv4 */
        else if (u4AsafiIndex == BGP4_IPV4_LBLD_INDEX)
        {
            continue;
        }
        else if (u4AsafiIndex == BGP4_VPN4_UNI_INDEX)
        {
            continue;
        }
#endif

        if (((BGP4_PEER_NEG_ASAFI_MASK (pPeer) != 0) &&
             ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) & u4NegAsafiMask)
              != u4NegAsafiMask)) ||
            ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) == 0) &&
             (u4NegAsafiMask != CAP_NEG_IPV4_UNI_MASK)))
        {
            /* This Address Family is not negotiated for the Peer.
             * No need to advertise the default route. */
            continue;
        }

        pDefaultRoute = Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
        if (pDefaultRoute == NULL)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Allocate Route Profile for Default "
                           "route FAILS!!!\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeer))));
            return (BGP4_FAILURE);
        }

        pBgpInfo = Bgp4MemAllocateBgp4info (sizeof (tBgp4Info));

        if (pBgpInfo == NULL)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Allocate BGP-INFO for Default "
                           "route FAILS!!!\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeer))));
            Bgp4DshReleaseRtInfo (pDefaultRoute);
            return (BGP4_FAILURE);
        }
        pDefaultRoute->pBgpCxtNode =
            Bgp4GetContextEntry (BGP4_PEER_CXT_ID (pPeer));

        BGP4_RT_PROTOCOL (pDefaultRoute) = BGP4_OTHERS_ID;

        BGP4_LINK_INFO_TO_PROFILE (pBgpInfo, pDefaultRoute);
        /* Fill the Default route attributes. */
        BGP4_INFO_ATTR_FLAG ((pBgpInfo)) |= BGP4_ATTR_ORIGIN_MASK;
        BGP4_INFO_ORIGIN ((pBgpInfo)) = BGP4_ATTR_ORIGIN_IGP;

        /* Fill the local-preference */
        BGP4_INFO_ATTR_FLAG ((pBgpInfo)) |= BGP4_ATTR_LOCAL_PREF;
        BGP4_INFO_RCVD_LOCAL_PREF ((pBgpInfo)) =
            BGP4_DEFAULT_LOCAL_PREF (BGP4_PEER_CXT_ID (pPeer));
        BGP4_RT_LOCAL_PREF ((pDefaultRoute)) =
            BGP4_DEFAULT_LOCAL_PREF (BGP4_PEER_CXT_ID (pPeer));

        /* Fill the MED Attribute. */
        BGP4_INFO_ATTR_FLAG ((pBgpInfo)) |= BGP4_ATTR_MED;
        BGP4_INFO_RCVD_MED ((pBgpInfo)) = 0;
        BGP4_RT_MED ((pDefaultRoute)) = 0;

        i4Status = Bgp4GetPeerNetworkAddress (pPeer, u2Afi, &RemAddr);
        if (i4Status == BGP4_FAILURE)
        {
            /* Network Address in not available for the peer. */
            Bgp4CopyAddrPrefixStruct (&RemAddr,
                                      BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
        }

        i4Status = Bgp4DftLocalAddrForPeer (pPeer, RemAddr, &LocalAddr,
                                            BGP4_TRUE, u2Afinfo);
        if (i4Status == BGP4_FAILURE)
        {
            BGP4_DBG (BGP4_DBG_ERROR,
                      "\tBgp4PeerAdvtDftRoute() : Local Address not"
                      "available for Default route.\n");
            Bgp4DshReleaseRtInfo (pDefaultRoute);
            continue;
        }

        /* Fill the next-hop. Use the peer local address as the next-hop
         * address. */
        BGP4_INFO_ATTR_FLAG (pBgpInfo) |= BGP4_ATTR_NEXT_HOP;
        Bgp4CopyAddrPrefixStruct ((&(BGP4_INFO_NEXTHOP_INFO (pBgpInfo))),
                                  LocalAddr.NetAddr);

        Bgp4InitNetAddressStruct ((&(BGP4_RT_NET_ADDRESS_INFO (pDefaultRoute))),
                                  u2Afi, (UINT1) u2Safi);

        /* Check whether the replacement flag should be set or not. */
        if (u1IsReplace == BGP4_TRUE)
        {
            BGP4_RT_SET_FLAG (pDefaultRoute, BGP4_RT_REPLACEMENT);
        }

        /* Increment the route-profile reference count so that it is
         * no release by any other routines. */
        BGP4_RT_REF_COUNT (pDefaultRoute)++;

        if (u1Policy == BGP4_DEF_ROUTE_ORIG_DISABLE)
        {
            /* Default route needs to be advertised as withdrawn. */
            BGP4_RT_SET_FLAG (pDefaultRoute, BGP4_RT_AGGR_WITHDRAWN);
            RtProfile.u1Ref = 0;
            RtProfile.u1Protocol = 0;
            RtProfile.p_RPNext = NULL;
            RtProfile.u4Flags = 0;
            RtProfile.pRtInfo = NULL;
            Bgp4InitNetAddressStruct (&(RtProfile.NetAddress), u2Afi,
                                      (UINT1) u2Safi);

            i4Status =
                Bgp4RibhLookupRtEntry (&RtProfile, BGP4_PEER_CXT_ID (pPeer),
                                       BGP4_TREE_FIND_EXACT, &pRtProfile,
                                       &pRibNode);
            if (i4Status == BGP4_SUCCESS)
            {
                pRtProfile = Bgp4DshGetBestRouteFromProfileList (pRtProfile,
                                                                 BGP4_PEER_CXT_ID
                                                                 (pPeer));
            }
            if ((i4Status == BGP4_FAILURE) || (pRtProfile == NULL))
            {
                /* No default routes present in RIB. Send the Default route
                 * generated as withdrawn to the peer.*/
                Bgp4DshAddRouteToList (&TsWithList, pDefaultRoute, 0);
            }
            else
            {
                /* Default route is present in RIB. Check for the advt status
                 * of the default route. */
                if (BGP4_DEF_ROUTE_ORIG_NEW_STATUS (BGP4_PEER_CXT_ID (pPeer)) ==
                    BGP4_DEF_ROUTE_ORIG_DISABLE)
                {
                    /* Default route in RIB can not be advertised as
                     * feasible route. Advertise as withdrawn. */
                    Bgp4DshAddRouteToList (&TsWithList, pDefaultRoute, 0);
                }
                else if (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeer), pPeer) ==
                         BGP4_INTERNAL_PEER)
                {
                    if (BGP4_RT_GET_FLAGS (pRtProfile) &
                        BGP4_RT_IN_INT_PEER_ADVT_LIST)
                    {
                        /* Route is in internal peer update list. Just remove
                         * route and the other route will be getting advertised
                         * as the new feasible route to this peer. */
                        Bgp4DshAddRouteToList (&TsWithList, pDefaultRoute, 0);
                    }
                    else
                    {
                        /* Advertise the route in the RIB to the peer as the
                         * best route. */
                        Bgp4DshAddRouteToList (&TsFeasList, pRtProfile, 0);
                    }
                }
                else
                {
                    if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                         BGP4_RT_IN_EXT_PEER_ADVT_LIST) ||
                        (BGP4_RT_GET_FLAGS (pRtProfile) &
                         BGP4_RT_IN_FIB_UPD_LIST))
                    {
                        /* Route is in external peer/FIB update list. Just
                         * remove the route and the other route will be getting
                         * advertised as the new feasible route to this peer.
                         */
                        Bgp4DshAddRouteToList (&TsWithList, pDefaultRoute, 0);
                    }
                    else
                    {
                        /* Advertise the route in the RIB to the peer as the
                         * best route. */
                        Bgp4DshAddRouteToList (&TsFeasList, pRtProfile, 0);
                    }
                }
            }
        }
        else
        {
            /* Advertise the Default route as feasible route. */
            Bgp4DshAddRouteToList (&TsFeasList, pDefaultRoute, 0);
        }

        Bgp4PeerSendRoutesToPeerTxQ (pPeer, &TsFeasList, &TsWithList,
                                     BGP4_FALSE);

        Bgp4DshReleaseRtInfo (pDefaultRoute);
        Bgp4DshReleaseList (&TsFeasList, 0);
        Bgp4DshReleaseList (&TsWithList, 0);
    }
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name : Bgp4IfacePeerAdminChange                                 */
/* Description   : This function either enables/disables peers on which     */
/*                 the specified interface index wre created                */
/* Input(s)      : u4IfIndex - interface index                              */
/* Output(s)     : None.                                                    */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                */
/****************************************************************************/
INT4
Bgp4IfacePeerAdminChange (UINT4 u4IfIndex, UINT1 u1Status)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tBgp4IfInfo        *pIfEntry = NULL;
    UINT4               u4Context = 0;
    UINT1               u1Ipv4AddressChange = 0;
    UINT4               u4CfaIfIndex = 0;
    pIfEntry = Bgp4IfGetEntry (u4IfIndex);
    if (pIfEntry != NULL)
    {
        u4Context = BGP4_IFACE_ENTRY_VRFID (pIfEntry);
    }

    /* For all the peers that match with this interface index,
     * generate the start event to initiate session with the peer if the
     * status is PEER_START, else generate stop event if the status
     * is PEER_STOP
     */
    if (gBgpCxtNode[u4Context] == NULL)
    {
        return BGP4_FAILURE;
    }

    u4CfaIfIndex = CfaGetIfInterface (u4IfIndex);

    if (NetIpv4GetCfaIfIndexFromPort (u4IfIndex,
                                      &u4CfaIfIndex) != NETIPV4_FAILURE)
    {
        CfaGetIfIpv4AddressChange (u4CfaIfIndex, &u1Ipv4AddressChange);
    }

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
    {
        /* Skip IPv6 BGP session flap when ipv4 address is changed
         * over the interface
         */
        if ((BGP4_PEER_REMOTE_ADDR_TYPE (pPeer) == BGP4_INET_AFI_IPV6)
            && (u1Ipv4AddressChange == CFA_TRUE))
        {
            continue;
        }

        if ((BGP4_PEER_IF_INDEX (pPeer) == u4IfIndex) &&
            (u1Status == BGP4_PEER_START) &&
            ((BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_START) ||
             (BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_AUTO_START)))
        {
            Bgp4EnablePeer (pPeer);
        }
        if ((BGP4_PEER_IF_INDEX (pPeer) == u4IfIndex) &&
            (u1Status == BGP4_PEER_STOP) &&
            ((BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_START) ||
             (BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_AUTO_START)))
        {
            Bgp4DisablePeer (pPeer);
        }
    }
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name : Bgp4SetBfdStatus                                         */
/* Description   : This function is to either enables or disable BFD        */
/*                 monitoring by Registering or De-registering with BFD     */
/* Input(s)      : u4ContextID - Context Identifier                         */
/*                 pPeerEntry  - Pointer to the Peer entry                  */
/* Output(s)     : None.                                                    */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                */
/****************************************************************************/
INT4
Bgp4SetBfdStatus (UINT4 u4ContextId, tBgp4PeerEntry * pPeerEntry)
{
    INT4                i4RetVal = BGP4_FAILURE;

    /* If the BGP session is not yet established, then it will register when
     * session becomes established. So Only if the session is already in
     * established state, then register with BFD module for monitoring the path*/
    if ((pPeerEntry->u1BfdStatus == BGP4_BFD_ENABLE) &&
        (BGP4_PEER_STATE (pPeerEntry) == BGP4_ESTABLISHED_STATE))
    {
        i4RetVal = Bgp4RegisterWithBfd (u4ContextId, pPeerEntry);
    }
    /* When BFD monitoring is disabled, De-register with BFD */
    else if ((pPeerEntry->u1BfdStatus == BGP4_BFD_DISABLE) &&
             (pPeerEntry->b1IsBfdRegistered == BGP4_TRUE))
    {
        i4RetVal = Bgp4DeRegisterWithBfd (u4ContextId, pPeerEntry, TRUE);
    }
    return i4RetVal;
}

/****************************************************************************/
/* Function Name : Bgp4ProcessPeerDownNotification                          */
/* Description   : This function is to process the session DOWN notification*/
/*                 received from the BFD module.                            */
/* Input(s)      : pPeerEntry  - Pointer to the Peer entry                  */
/*                 bIsSessCtrlPlaneInDep - Flag to decide whether the BFD   */
/*                 session is offloaded or not                              */
/* Output(s)     : None.                                                    */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                */
/****************************************************************************/
INT4
Bgp4ProcessPeerDownNotification (tBgp4PeerEntry * pPeerEntry,
                                 BOOL1 bIsSessCtrlPlaneInDep)
{
    /* If the BFD session is Control plane Independent (i.e) Offloaded,
     * then the BFD session DOWN notification should be considered as
     * Path failure. So BGP4_HOLD_TMR_EXP_EVT is called.
     */
    if (bIsSessCtrlPlaneInDep == OSIX_TRUE)
    {
        Bgp4SEMHProcessEvent (pPeerEntry, BGP4_HOLD_TMR_EXP_EVT, NULL, 0);
    }

    /* If the BFD session is not Control plane Independent (i.e) Not Offloaded,
     * then the BFD session DOWN notification may be due to path failure or 
     * Restart of peer(unplanned). So treat this notification as BGP4_TCP_CONN_CLOSED_EVT.
     * so that the learned routes will not be removed till Restart timer expiry*/
    else
    {
        Bgp4SEMHProcessEvent (pPeerEntry, BGP4_TCP_CONN_CLOSED_EVT, NULL, 0);
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, BGP4_SYSLOG_ID,
                  "BGP received session DOWN notification form BFD for peer "
                  "entry %s\n",
                  Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                   BGP4_PEER_REMOTE_ADDR_TYPE (pPeerEntry))));
    return BGP4_SUCCESS;
}
#endif /* _BGP4PEER_C */
