/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4ih.c,v 1.23 2017/09/15 06:19:52 siva Exp $
 *
 * Description: These routines are part of the Input Handler Module 
 *              which receives all the indication and presents the 
 *              same to the SEM after processing.                        
 *******************************************************************/

#ifndef BGP4IH_C
#define BGP4IH_C

#include "bgp4com.h"

/******************************************************************************/
/* Function Name : Bgp4IhHandleTimer                                          */
/* Description   : This function is the called from the Timer Expiry handler, */
/*                 on expiry of the following timer -                         */
/*                       BGP4_START_TIMER                                     */
/*                       BGP4_CONNECTRETRY_TIMER                              */
/*                       BGP4_HOLD_TIMER                                      */
/*                       BGP4_KEEPALIVE_TIMER                                 */
/* Input(s)     : ID of the expired timer (u4TimerId),                        */
/*                Peer Information (u4Ref).                                   */
/* Output(s)    : None.                                                       */
/* Return(s)    : BGP4_SUCCESS/BGP4_FAILURE.                                  */
/******************************************************************************/
INT4
Bgp4IhHandleTimer (UINT4 u4TimerId, FS_ULONG u4Tmr)
{
    UINT2               u2Event = BGP4_NO_EVT;
    tBgp4PeerEntry     *pPeer = NULL;
    tBgp4PeerEntry     *pPeer2 = NULL;
    UINT4               u4Context;

    pPeer2 = (tBgp4PeerEntry *) u4Tmr;

    u4Context = BGP4_PEER_CXT_ID (pPeer2);

    BgpSetContextId ((INT4) u4Context);

    /* set the state of the corresponding timer to inactive */

    if (u4TimerId == BGP4_START_TIMER)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer2)),
                       BGP4_TRC_FLAG, BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Start Timer Expired.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer2),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO (pPeer2))));
        u2Event = BGP4_START_EVT;
        pPeer2->peerLocal.tStartTmr.u4Flag = BGP4_INACTIVE;
    }

    else if (u4TimerId == BGP4_IDLE_HOLD_TIMER)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer2)),
                       BGP4_TRC_FLAG, BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : IdleHold Timer Expired.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer2),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO (pPeer2))));
        u2Event = BGP4_START_EVT;
        if (BGP4_PEER_DAMPED_STATUS (pPeer2) ==
            BGP4_DAMP_PEER_OSCILLATIONS_ENABLE)
        {
            BGP4_PEER_DAMPED_STATUS (pPeer2) =
                BGP4_DAMP_PEER_OSCILLATIONS_DISABLE;
        }
        pPeer2->peerLocal.tIdleHoldTmr.u4Flag = BGP4_INACTIVE;
    }

    else if (u4TimerId == BGP4_DELAY_OPEN_TIMER)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer2)),
                       BGP4_TRC_FLAG, BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Delay Open Timer Expired.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer2),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO (pPeer2))));
        u2Event = BGP4_DELAY_OPEN_TMR_EXP_EVT;
        pPeer2->peerLocal.tDelayOpenTmr.u4Flag = BGP4_INACTIVE;
    }

    else if (u4TimerId == BGP4_CONNECTRETRY_TIMER)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer2)),
                       BGP4_TRC_FLAG, BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Connect Retry Timer Expired.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer2),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO (pPeer2))));
        u2Event = BGP4_CONNECTRETRY_TMR_EXP_EVT;
        pPeer2->peerLocal.tConnRetryTmr.u4Flag = BGP4_INACTIVE;
    }
    else if (u4TimerId == BGP4_HOLD_TIMER)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer2)),
                       BGP4_TRC_FLAG, BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : HOLD Timer Expired!!!.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer2),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO (pPeer2))));
        u2Event = BGP4_HOLD_TMR_EXP_EVT;
        pPeer2->peerLocal.tHoldTmr.u4Flag = BGP4_INACTIVE;
    }
    else if (u4TimerId == BGP4_KEEPALIVE_TIMER)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer2)),
                       BGP4_TRC_FLAG, BGP4_KEEP_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Keep Alive Timer Expired.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer2),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO (pPeer2))));
        u2Event = BGP4_KEEPALIVE_TMR_EXP_EVT;
        pPeer2->peerLocal.tKeepAliveTmr.u4Flag = BGP4_INACTIVE;
    }
    else if (u4TimerId == BGP4_STALE_TIMER)
    {

        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer2)),
                       BGP4_TRC_FLAG, BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Stale Timer Expired.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer2),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO (pPeer2))));
        Bgp4GRProcessStaleTimerExpiry (pPeer2);
        pPeer2->peerLocal.tStaleTmr.u4Flag = BGP4_INACTIVE;
    }
    else if (u4TimerId == BGP4_PEER_RESTART_TIMER)
    {

        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer2)),
                       BGP4_TRC_FLAG, BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Stale Timer Expired.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer2),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO (pPeer2))));
        Bgp4GRProcessPeerRstTimerExpiry (pPeer2);
        pPeer2->peerLocal.tPeerRestartTmr.u4Flag = BGP4_INACTIVE;
    }

    else
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer2)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tInvalid BGP Peer Timer Event - %d\n", u4TimerId);
        return BGP4_FAILURE;
    }

    /* Process event for a valid peerentry */
    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (BGP4_PEER_CXT_ID (pPeer2)), pPeer,
                  tBgp4PeerEntry *)
    {
        if (pPeer == pPeer2)
        {
            break;
        }
    }
    if ((pPeer != NULL) && (u2Event != BGP4_NO_EVT))
    {
        Bgp4SEMHProcessEvent (pPeer, u2Event, NULL, 0);
    }
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4IphHandleControl                                       */
/* Description   : This function is the called from the TCP handler and SNMP  */
/*                 module whenever TCP related event or SNMP related event    */
/*                 occurs. It translates the event/control to the form which  */
/*                 the SEM handler understands & gives it to the SEM handler  */
/* Input(s)      : Peer Information (pPeerentry),                             */
/*                 Event / Control receivied (u1Control)                      */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                 */
/******************************************************************************/
INT4
Bgp4IphHandleControl (tBgp4PeerEntry * pPeerentry, UINT1 u1Control)
{
    UINT1               u1Event;

    switch (u1Control)
    {
        case BGP4_TCP_OPENED:
            u1Event = BGP4_TCP_CONN_OPENED_EVT;
            break;

        case BGP4_TCP_CLOSED:
            u1Event = BGP4_TCP_CONN_CLOSED_EVT;
            break;

        case BGP4_TCP_OPEN_FAILED:
            u1Event = BGP4_TCP_CONN_OPEN_FAIL_EVT;
            break;

        case BGP4_TCP_ERROR:
            u1Event = BGP4_TCP_FATAL_ERROR_EVT;
            break;

        case BGP4_SNMP_START:
            u1Event = BGP4_START_EVT;
            break;

        case BGP4_SNMP_STOP:
            u1Event = BGP4_STOP_EVT;
            break;

        default:
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                           "\tPEER %s Invalid Peer Event - %d\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))), u1Control);

            return BGP4_FAILURE;
    }

    if (u1Event == BGP4_START_EVT)
    {
        Bgp4EnablePeer (pPeerentry);
    }
    else if (u1Event == BGP4_STOP_EVT)
    {
        Bgp4DisablePeer (pPeerentry);
    }
    else
    {
        Bgp4SEMHProcessEvent (pPeerentry, u1Event, NULL, 0);
    }
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4IhHandlePacket                                         */
/* Description   : This function is the called from the TCP handler whenever  */
/*                 data is available in any of the BGP connection. This       */
/*                 function does the validation of the BGP common header.     */
/*                 After the validation of the buffer the result is intimated */
/*                 to the SEM handler which takes proper action according to  */
/*                 the validation result.                                     */
/* Input(s)      : Peer Information (pPeerentry),                             */
/*                 Buffer containing the BGP Message (pu1Buf),                */
/*                 Size of the BGP message buffer (u4Bufsize).                */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE/BGP4_PEER_DELETE                 */
/******************************************************************************/
INT4
Bgp4IhHandlePacket (tBgp4PeerEntry * pPeerentry, UINT1 *pu1Buf, UINT4 u4Bufsize)
{
    UINT1              *pu1Authdata = NULL;
    tBgp4PeerEntry     *pPeer = NULL;
    UINT4               u4Cxt = 0;
    UINT2               u2Msgsize;
    UINT2               u2Event = BGP4_NO_EVT;
    UINT1               u1ErrCode = 0;
    UINT1               u1MsgType;
    UINT1               u1ErrSubCode = 0;

    while (u4Bufsize > 0)
    {
        pu1Authdata = (UINT1 *) (pu1Buf + BGP4_MARKER_LEN);

        PTR_FETCH2 (u2Msgsize, (pu1Buf + BGP4_MARKER_LEN));
        u1MsgType = (UINT1) *(pu1Buf + BGP4_MARKER_LEN + sizeof (u2Msgsize));

        if ((u2Msgsize < BGP4_MIN_MSG_LEN) || (u2Msgsize > BGP4_MAX_MSG_LEN))
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                           "\tPEER %s: Received Message with Invalid Length.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));

            Bgp4EhSendError (pPeerentry, BGP4_MSG_HDR_ERR, BGP4_BAD_MSG_LEN,
                             pu1Buf + BGP4_MARKER_LEN, sizeof (UINT2));
            if (BGP4_PEER_STATE (pPeerentry) == BGP4_ESTABLISHED_STATE)
            {
                BGP4_PEER_ESTAB_TIME (pPeerentry) = Bgp4ElapTime ();
                Bgp4RibhEndConnection (pPeerentry);
            }
            Bgp4SemhProcessInvalidEvt (pPeerentry);
            return BGP4_FAILURE;
        }

        pu1Authdata = pu1Buf;
        if (Bgp4IhAuthMsg (pu1Authdata) == BGP4_FAILURE)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                           "\tPEER %s: Received Msg with Invalid Marker "
                           "Authentication.",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4EhSendError (pPeerentry, BGP4_MSG_HDR_ERR, BGP4_CONN_NOT_SYNC,
                             NULL, 0);
            if (BGP4_PEER_STATE (pPeerentry) == BGP4_ESTABLISHED_STATE)
            {
                BGP4_PEER_ESTAB_TIME (pPeerentry) = Bgp4ElapTime ();
                Bgp4RibhEndConnection (pPeerentry);
            }
            Bgp4SemhProcessInvalidEvt (pPeerentry);
            return BGP4_FAILURE;
        }

        if ((BGP4_TRC_FLAG & BGP4_DUMP_HEX_TRC) == BGP4_DUMP_HEX_TRC)
        {
            Bgp4DumpBuf (pu1Buf, u4Bufsize, BGP4_INCOMING);
        }

        switch (u1MsgType)
        {
            case BGP4_OPEN_MSG:
                if (u2Msgsize < BGP4_OPEN_MSG_MIN_LEN)
                {
                    u1ErrCode = BGP4_MSG_HDR_ERR;
                    u1ErrSubCode = BGP4_BAD_MSG_LEN;
                    u2Event = BGP4_ERROR_EVT;
                }
                else
                    u2Event = BGP4_OPEN_MSG_RECD_EVT;
                break;

            case BGP4_UPDATE_MSG:
                if (u2Msgsize < BGP4_UPDATE_MSG_MIN_LEN)
                {
                    u1ErrCode = BGP4_MSG_HDR_ERR;
                    u1ErrSubCode = BGP4_BAD_MSG_LEN;
                    u2Event = BGP4_ERROR_EVT;
                }
                else
                    u2Event = BGP4_UPDATE_MSG_RECD_EVT;
                break;

            case BGP4_NOTIFICATION_MSG:
                if (u2Msgsize < BGP4_NOTIFY_MSG_MIN_LEN)
                {
                    u1ErrCode = BGP4_MSG_HDR_ERR;
                    u1ErrSubCode = BGP4_BAD_MSG_LEN;
                    u2Event = BGP4_ERROR_EVT;
                }
                else
                {
                    u2Event = BGP4_NOTIFICATION_MSG_RECD_EVT;
                }
                break;

            case BGP4_KEEPALIVE_MSG:
                if (u2Msgsize == BGP4_KEEPALIVE_MSG_LEN)
                {
                    u2Event = BGP4_KEEPALIVE_MSG_RECD_EVT;
                }
                else
                {
                    u1ErrCode = BGP4_MSG_HDR_ERR;
                    u1ErrSubCode = BGP4_BAD_MSG_LEN;
                    u2Event = BGP4_ERROR_EVT;
                }
                break;

            case BGP4_ROUTE_REFRESH_MSG:
                if (u2Msgsize >= BGP4_RTREF_MSG_MIN_LEN)
                {
                    u2Event = BGP4_ROUTE_REFRESH_MSG_RECD_EVT;
                }
                else
                {
                    u1ErrCode = BGP4_MSG_HDR_ERR;
                    u1ErrSubCode = BGP4_BAD_MSG_LEN;
                    u2Event = BGP4_ERROR_EVT;
                }
                break;

            default:
                u1ErrCode = BGP4_MSG_HDR_ERR;
                u1ErrSubCode = BGP4_BAD_MSG_TYPE;
                u2Event = BGP4_ERROR_EVT;
                break;
        }

        if (u2Event == BGP4_ERROR_EVT)
        {
            BGP4_TRC_ARG5 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Error in Processing Header : \n"
                           "\t\tMsg Type = %d, Msg Len = %d, "
                           "Error Code = %d, Error Subcode = %d\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))), u1MsgType,
                           u2Msgsize, u1ErrCode, u1ErrSubCode);

            if (u1ErrSubCode == BGP4_BAD_MSG_TYPE)
            {
                Bgp4EhSendError (pPeerentry, u1ErrCode, u1ErrSubCode,
                                 (UINT1 *) &(u1MsgType), sizeof (UINT1));
            }
            else
            {
                Bgp4EhSendError (pPeerentry, BGP4_MSG_HDR_ERR,
                                 BGP4_BAD_MSG_LEN, pu1Buf + BGP4_MARKER_LEN,
                                 sizeof (UINT2));
            }
            if (BGP4_PEER_STATE (pPeerentry) == BGP4_ESTABLISHED_STATE)
            {
                BGP4_PEER_ESTAB_TIME (pPeerentry) = Bgp4ElapTime ();
                Bgp4RibhEndConnection (pPeerentry);
            }
            Bgp4SemhProcessInvalidEvt (pPeerentry);
            return BGP4_FAILURE;
        }
        else
        {
            u4Cxt = pPeerentry->pBgpCxtNode->u4ContextId;
            Bgp4SEMHProcessEvent (pPeerentry, u2Event,
                                  (pu1Buf + BGP4_MSG_COMMON_HDR_LEN),
                                  (u2Msgsize - BGP4_MSG_COMMON_HDR_LEN));
            /* If the received packet, has got an invalid data then the
             * processing of this data may result in closing the peer
             * session. In that case, BGP4_FAILURE needs to be return.
             * Current checking the TCP connection id to find whether the
             * peer session is closed or not. Check if peerentry is deleted 
             * from the global data. */
            TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Cxt), pPeer, tBgp4PeerEntry *)
            {
                if (pPeer == pPeerentry)
                {
                    break;        /* peer not deleted */
                }
            }
            if (pPeer == NULL)
            {
                /* Peer has been deleted. This can occur because of Collision
                 * Detection process. */
                return BGP4_PEER_DELETE;
            }
            if (BGP4_PEER_CONN_ID (pPeerentry) == BGP4_INV_CONN_ID)
            {
                /* Connection was closed while processing the data. */
                return BGP4_FAILURE;
            }
            u4Bufsize -= u2Msgsize;
            pu1Buf += u2Msgsize;

            BGP4_PEER_IN_MSGS (pPeerentry)++;
        }
    }                            /* end  while */
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4IhProcessBgpCompletePacket                             */
/* Description   : This function is called from the TCP handler whenever      */
/*                 data is available in any of the BGP connection. This       */
/*                 function does the validation of the BGP common header.     */
/*                 After the validation of the buffer the result is intimated */
/*                 to the bgp_ih_handle_packet which processes the complete   */
/*                 bgp packet received                                        */
/* Input(s)      : Peer Information (pPeerentry),                             */
/*                 Size of the New BGP message buffer (i4Msgsize).            */
/* Output(s)     : None                                                       */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                 */
/******************************************************************************/
INT4
Bgp4IhProcessBgpCompletePacket (tBgp4PeerEntry * pPeerentry, INT4 i4Msgsize)
{
    UINT1              *pu1Msg = NULL;
    UINT1              *pu1RecvMsg = NULL;
    UINT1              *pu1msgHdr = NULL;
    UINT4               u4MarkerOffset;
    UINT4               u4RdOffset = 0;
    UINT4               u4PrevResOffset = 0;
    INT4                i4PrcsMsgSize = 0;
    INT4                i4RetVal = BGP4_SUCCESS;
    UINT2               u2Msgsize;
    UINT1               u1MsgType;

    i4PrcsMsgSize = i4Msgsize;
    u4PrevResOffset = BGP4_PEER_RESIDUAL_BUF_OFFSET (pPeerentry);

    while ((i4PrcsMsgSize + u4PrevResOffset) > 0)
    {
        if (u4PrevResOffset > 0)
        {
            if ((i4PrcsMsgSize + u4PrevResOffset) >= BGP4_MSG_COMMON_HDR_LEN)
            {
                if (Bgp4IhScanForMarker
                    (BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry),
                     (UINT4) i4PrcsMsgSize + u4PrevResOffset,
                     &u4MarkerOffset) == BGP4_SUCCESS)
                {
                    if (u4MarkerOffset != 0)
                    {
                        /* Buffer does not contains Marker bits at the
                         * start of the message. So start processing from where
                         * the new complete message starts. */
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                  BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tProcess Message Error: No valid MARKER at"
                                  " start of the message\n");
                        BGP4_PEER_RESIDUAL_CUR_MSG_LEN (pPeerentry) = 0;
                        u4RdOffset = u4MarkerOffset;
                        if (u4MarkerOffset > u4PrevResOffset)
                        {
                            i4PrcsMsgSize -=
                                ((INT4) u4MarkerOffset -
                                 (INT4) u4PrevResOffset);
                        }
                        else
                        {
                            i4PrcsMsgSize +=
                                ((INT4) u4PrevResOffset -
                                 (INT4) u4MarkerOffset);
                        }
                        u4PrevResOffset = 0;
                        BGP4_ERR_TCP_RECV_MSG_ERROR (BGP4_PEER_CXT_ID
                                                     (pPeerentry))++;
                        continue;
                    }
                    else
                    {
                        /* Valid Marker field is present at the start of the
                         * buffer. Check whether the received new buffer also
                         * starts with the Marker field. If yes, then the
                         * previously stored message is incomplete. Skip the
                         * old message and process the new message. */
                        if ((i4PrcsMsgSize >= BGP4_MSG_COMMON_HDR_LEN) &&
                            (Bgp4IhScanForMarker
                             (BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry) +
                              u4PrevResOffset, (UINT4) i4PrcsMsgSize,
                              &u4MarkerOffset) == BGP4_SUCCESS))
                        {
                            pu1Msg = BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry);
                            PTR_FETCH2 (u2Msgsize, (pu1Msg + BGP4_MARKER_LEN));
                            u1MsgType = *(pu1Msg + BGP4_MARKER_LEN +
                                          sizeof (UINT2));
                            if (u2Msgsize > (u4PrevResOffset + u4MarkerOffset))
                            {
                                /* Old Residual message is incomplete. Means
                                 * the entire packet is not present. */
                                if ((u1MsgType == BGP4_ROUTE_REFRESH_MSG) ||
                                    (u1MsgType == BGP4_KEEPALIVE_MSG))
                                {
                                    /* Received Wrong message. Need to send
                                     * notification to peer. */
                                    BGP4_TRC (&
                                              (BGP4_PEER_REMOTE_ADDR_INFO
                                               (pPeerentry)), BGP4_TRC_FLAG,
                                              BGP4_ALL_FAILURE_TRC,
                                              BGP4_MOD_NAME,
                                              "\tProcess Message Error: "
                                              "Received invalid Packet.\n");
                                    i4RetVal =
                                        Bgp4IhHandlePacket (pPeerentry,
                                                            BGP4_PEER_RESIDUAL_BUF_DATA
                                                            (pPeerentry),
                                                            u4PrevResOffset +
                                                            (UINT4)
                                                            i4PrcsMsgSize);

                                    break;
                                }

                                BGP4_TRC (&
                                          (BGP4_PEER_REMOTE_ADDR_INFO
                                           (pPeerentry)), BGP4_TRC_FLAG,
                                          BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                                          "\tProcess Message Error: Incomplete"
                                          " Residual message.\n");
                                u4RdOffset = u4PrevResOffset + u4MarkerOffset;
                                i4PrcsMsgSize -= (INT4) u4MarkerOffset;
                                u4PrevResOffset = 0;
                                BGP4_ERR_TCP_RECV_MSG_ERROR (BGP4_PEER_CXT_ID
                                                             (pPeerentry))++;
                                continue;
                            }
                        }
                        else
                        {
                            u4RdOffset = 0;
                        }
                    }
                }
                else
                {
                    /* Buffer is incomplete and does not have the proper
                     * message header. */
                    BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                              BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                              BGP4_MOD_NAME,
                              "\tProcess Message Error: Received buffer"
                              "has no MARKER field.\n");
                    i4RetVal = Bgp4IhHandlePacket (pPeerentry,
                                                   BGP4_PEER_RESIDUAL_BUF_DATA
                                                   (pPeerentry),
                                                   u4PrevResOffset +
                                                   (UINT4) i4PrcsMsgSize);
                    break;
                }
            }
            else
            {
                /* Not yet received the complete packet. */
                BGP4_PEER_RESIDUAL_CUR_MSG_LEN (pPeerentry) = 0;
                /* Update the peer's residual buffer offset. */
                BGP4_PEER_RESIDUAL_BUF_OFFSET (pPeerentry) +=
                    (UINT4) i4PrcsMsgSize;
                return BGP4_SUCCESS;
            }

            /* Check whether the complete packet is received or not.
             *  If yes, then validate the packet. */
            pu1Msg = BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry);
            pu1msgHdr = pu1Msg;
            PTR_FETCH2 (u2Msgsize, (pu1msgHdr + BGP4_MARKER_LEN));
            BGP4_PEER_RESIDUAL_CUR_MSG_LEN (pPeerentry) = u2Msgsize;

            if (((i4PrcsMsgSize + u4PrevResOffset) >= u2Msgsize) ||
                (u2Msgsize > BGP4_MAX_MSG_LEN))
            {
                /* Has received the complete message. Need to process
                 * this BGP message. */
                i4RetVal = Bgp4IhHandlePacket (pPeerentry, pu1Msg, u2Msgsize);
                if (i4RetVal != BGP4_SUCCESS)
                {
                    /* INVALID packet. Connection will be closed.
                     * Residual buffer should have been freed */
                    break;
                }
                /* BGP message successfully processed. Need to process
                 * next message if available. */
                i4PrcsMsgSize -= (u2Msgsize - u4PrevResOffset);
                if (i4PrcsMsgSize > 0)
                {
                    /* Update the u4RdOffset value and continue. */
                    u4RdOffset += u2Msgsize;
                    u4PrevResOffset = 0;
                    BGP4_PEER_RESIDUAL_CUR_MSG_LEN (pPeerentry) = 0;
                    continue;
                }
                else
                {
                    /* No more message to process. */
                    break;
                }
            }
            else
            {
                /* Not yet received the complete packet. Update the peer's
                 * residual buffer offset. */
                BGP4_PEER_RESIDUAL_BUF_OFFSET (pPeerentry) += (UINT4) i4Msgsize;
                return BGP4_SUCCESS;
            }
        }
        else
        {
            if (u4RdOffset > BGP4_MAX_BUF2PROCESS)
            {
                /* Has finished processing more than required message.
                 * Rest will be processed in the next cycle. */
                pu1RecvMsg = MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);
                if (pu1RecvMsg == NULL)
                {
                    /* Allocate fails. Try later. */
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                              BGP4_MOD_NAME,
                              "\tMemory Allocation to hold Received Data "
                              "FAILED!!!\n");
                    gu4BgpDebugCnt[MAX_BGP_BUFNODE_MSGS_SIZING_ID]++;
                    i4RetVal = BGP4_FAILURE;
                    break;
                }
                MEMCPY (pu1RecvMsg,
                        BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry) +
                        u4RdOffset, i4PrcsMsgSize);

                /* Release the current buffer and update the residual
                 * buffer */
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                    BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry));
                BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry) = pu1RecvMsg;
                BGP4_PEER_RESIDUAL_CUR_MSG_LEN (pPeerentry) = 0;
                BGP4_PEER_RESIDUAL_BUF_OFFSET (pPeerentry) =
                    (UINT4) i4PrcsMsgSize;
                return BGP4_SUCCESS;
            }
            if (i4PrcsMsgSize >= BGP4_MSG_COMMON_HDR_LEN)
            {
                if (Bgp4IhScanForMarker (BGP4_PEER_RESIDUAL_BUF_DATA
                                         (pPeerentry) + u4RdOffset,
                                         (UINT4) i4PrcsMsgSize, &u4MarkerOffset)
                    == BGP4_SUCCESS)
                {
                    if (u4MarkerOffset != 0)
                    {
                        /* Buffer does not contains Marker bits at the
                         * start of the message. So start processing from where
                         * the new complete message starts. */
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                                  BGP4_MOD_NAME,
                                  "\tProcess Message Error: No Valid MARKER at"
                                  " start of the message.\n");
                        BGP4_PEER_RESIDUAL_CUR_MSG_LEN (pPeerentry) = 0;
                        i4PrcsMsgSize -= (INT4) u4MarkerOffset;
                        u4RdOffset += u4MarkerOffset;
                        BGP4_ERR_TCP_RECV_MSG_ERROR (BGP4_PEER_CXT_ID
                                                     (pPeerentry))++;
                    }
                    else
                    {
                        /* Valid Marker field is present. Check whether the
                         * complete message is present or not. If no, skip the
                         * current message and process the next message. */
                        pu1Msg = BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry) +
                            u4RdOffset;
                        PTR_FETCH2 (u2Msgsize, (pu1Msg + BGP4_MARKER_LEN));
                        u1MsgType = *(pu1Msg + BGP4_MARKER_LEN +
                                      sizeof (UINT2));
                        if ((Bgp4IhScanForMarker
                             (BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry) +
                              u4RdOffset + BGP4_MSG_COMMON_HDR_LEN,
                              (UINT4) (i4PrcsMsgSize - BGP4_MSG_COMMON_HDR_LEN),
                              &u4MarkerOffset) == BGP4_SUCCESS))
                        {
                            /* Buffer also hold more BGP message. Check whether
                             * the current message is received completely */
                            if (u2Msgsize <= (u4MarkerOffset +
                                              BGP4_MSG_COMMON_HDR_LEN))
                            {
                                /* Has received complete BGP packet. */
                            }
                            else
                            {
                                /* Not received the complete packet. */
                                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                          BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                                          "\tProcess Message Error: Received"
                                          " incomplete message.\n");
                                if ((u1MsgType == BGP4_ROUTE_REFRESH_MSG)
                                    || (u1MsgType == BGP4_KEEPALIVE_MSG))
                                {
                                    /* Received Wrong message. Need to send
                                     * notification to peer. */
                                    i4RetVal = Bgp4IhHandlePacket (pPeerentry,
                                                                   BGP4_PEER_RESIDUAL_BUF_DATA
                                                                   (pPeerentry)
                                                                   + u4RdOffset,
                                                                   (UINT4)
                                                                   i4PrcsMsgSize);

                                    break;
                                }
                                u4RdOffset += u4MarkerOffset +
                                    BGP4_MSG_COMMON_HDR_LEN;
                                i4PrcsMsgSize -= ((INT4) u4MarkerOffset +
                                                  BGP4_MSG_COMMON_HDR_LEN);
                                BGP4_ERR_TCP_RECV_MSG_ERROR (BGP4_PEER_CXT_ID
                                                             (pPeerentry))++;
                                continue;
                            }
                        }
                    }
                }
                else
                {
                    /* Buffer is incomplete and does not have the proper
                     * message header. */
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                              BGP4_MOD_NAME,
                              "\tProcess Message Error: Received buffer"
                              "has no MARKER field.\n");
                    i4RetVal = Bgp4IhHandlePacket (pPeerentry,
                                                   BGP4_PEER_RESIDUAL_BUF_DATA
                                                   (pPeerentry) + u4RdOffset,
                                                   (UINT4) i4PrcsMsgSize);
                    break;
                }

                /* Check whether the complete packet is received or not.
                 *  If yes, then validate the packet. */
                pu1Msg = BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry) + u4RdOffset;
                pu1msgHdr = pu1Msg;
                PTR_FETCH2 (u2Msgsize, (pu1msgHdr + BGP4_MARKER_LEN));

                if ((u2Msgsize <= i4PrcsMsgSize) ||
                    (u2Msgsize > BGP4_MAX_MSG_LEN))
                {
                    /* Has received complete BGP message. */
                    i4RetVal = Bgp4IhHandlePacket (pPeerentry, pu1Msg,
                                                   u2Msgsize);
                    if (i4RetVal != BGP4_SUCCESS)
                    {
                        /* INVALID packet. Connection will be closed. */
                        break;
                    }
                    u4RdOffset += u2Msgsize;
                    i4PrcsMsgSize -= u2Msgsize;
                    if (i4PrcsMsgSize == 0)
                    {
                        /* Has processed complete BGP message */
                        break;
                    }
                }
                else
                {
                    /* Store the fragmented msg in residual buffer */
                    pu1RecvMsg = MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);
                    if (pu1RecvMsg == NULL)
                    {
                        /* Allocate fails. Try later. */
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                                  BGP4_MOD_NAME,
                                  "\tMemory Allocation to hold Received Data "
                                  "FAILED!!!\n");
                        gu4BgpDebugCnt[MAX_BGP_BUFNODE_MSGS_SIZING_ID]++;
                        i4RetVal = BGP4_FAILURE;
                        break;
                    }
                    if (i4PrcsMsgSize <= BGP4_MAX_MSG_LEN)
                    {
                        MEMCPY (pu1RecvMsg,
                                BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry) +
                                u4RdOffset, i4PrcsMsgSize);
                    }

                    /* Release the current buffer and update the residual
                     * buffer */
                    MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                        BGP4_PEER_RESIDUAL_BUF_DATA
                                        (pPeerentry));
                    BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry) = pu1RecvMsg;
                    BGP4_PEER_RESIDUAL_CUR_MSG_LEN (pPeerentry) = u2Msgsize;
                    BGP4_PEER_RESIDUAL_BUF_OFFSET (pPeerentry) =
                        (UINT4) i4PrcsMsgSize;
                    return BGP4_SUCCESS;
                }
            }
            else
            {
                /* Store the fragmented msg in residual buffer */
                pu1RecvMsg = MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);
                if (pu1RecvMsg == NULL)
                {
                    /* Allocate fails. Try later. */
                    BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                              BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                              BGP4_MOD_NAME,
                              "\tMemory Allocation to hold Received Data "
                              "FAILED!!!\n");
                    gu4BgpDebugCnt[MAX_BGP_BUFNODE_MSGS_SIZING_ID]++;
                    i4RetVal = BGP4_FAILURE;
                    break;
                }
                MEMCPY (pu1RecvMsg,
                        BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry) +
                        u4RdOffset, i4PrcsMsgSize);

                /* Release the current buffer and update the residual
                 * buffer */
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                    BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry));
                BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry) = pu1RecvMsg;
                BGP4_PEER_RESIDUAL_CUR_MSG_LEN (pPeerentry) = 0;
                BGP4_PEER_RESIDUAL_BUF_OFFSET (pPeerentry) =
                    (UINT4) i4PrcsMsgSize;
                return BGP4_SUCCESS;
            }
        }
    }
    if (i4RetVal == BGP4_PEER_DELETE)
    {
        /* Peer has already been deleted. */
        i4RetVal = BGP4_FAILURE;
    }
    else
    {
        if (BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry) != NULL)
        {
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry));
        }
        BGP4_PEER_RESIDUAL_BUF_DATA (pPeerentry) = NULL;
        BGP4_PEER_RESIDUAL_CUR_MSG_LEN (pPeerentry) = 0;
        BGP4_PEER_RESIDUAL_BUF_OFFSET (pPeerentry) = 0;
    }
    return i4RetVal;

}

/******************************************************************************/
/* Function Name : Bgp4IhScanForMarker                                        */
/* Description   : This function scans the recvd buffer for marker fields     */
/*                 (16 FFs) to identify the start of the next bgp message     */
/*                 Note: For non-authenticated open messages and for update/  */
/*                 keep-alive and notification messages the marker fields in  */
/*                 the header are used for synchronization                    */
/* Input(s)      : Recvd buffer (pu1Data)                                     */
/*                 Recvd buffer Length (u4DataLen)                            */
/* Output(s)     : Length of Marker bits (pu4MarkerOffset)                    */
/* Return(s)     : BGP4_SUCCESS if the marker-fields are identified           */
/*                 BGP4_FAILURE otherwise.                                    */
/******************************************************************************/
INT4
Bgp4IhScanForMarker (UINT1 *pu1Data, UINT4 u4DataLen, UINT4 *pu4MarkerOffset)
{
    UINT1              *pu1ScanPtr = NULL;
    UINT4               u4OffsetCtr = 0;
    UINT4               u4ScanCtr = u4DataLen;
    UINT4               u4IsMarkerFound = BGP4_FALSE;

    pu1ScanPtr = pu1Data;
    while (u4ScanCtr > 0)
    {
        if (*pu1ScanPtr == BGP4_MARKER_BYTE_VAL)
        {
            /* check if this is the start of a bgp header */
            if (u4ScanCtr < BGP4_MSG_COMMON_HDR_LEN)
            {
                if (u4IsMarkerFound == BGP4_TRUE)
                {
                    return (BGP4_SUCCESS);
                }
                break;
            }
            if (Bgp4IhAuthMsg (pu1ScanPtr) == BGP4_SUCCESS)
            {
                *pu4MarkerOffset = u4OffsetCtr;
                u4IsMarkerFound = BGP4_TRUE;
            }
            else if (u4IsMarkerFound == BGP4_TRUE)
            {
                return (BGP4_SUCCESS);
            }
        }
        else if (u4IsMarkerFound == BGP4_TRUE)
        {
            return (BGP4_SUCCESS);
        }

        pu1ScanPtr++;
        u4OffsetCtr++;
        u4ScanCtr--;
    }
    *pu4MarkerOffset = 0;
    return (BGP4_FAILURE);
}

/******************************************************************************/
/* Function Name : Bgp4IhAuthMsg                                              */
/* Description   : This function is called while validating the BGP common    */
/*                 header. This function checks for the valid Authentication  */
/*                 field.                                                     */
/* Input(s)      : Peer Information (pPeerentry),s                            */
/*                 Authentication data which needs to be checked (pu1Authdata)*/
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the field is a valid one,                  */
/*                 BGP4_FAILURE otherwise.                                    */
/******************************************************************************/
INT4
Bgp4IhAuthMsg (UINT1 *pu1Authdata)
{
    UINT1               au1Marker[BGP4_MARKER_LEN];

    MEMSET (au1Marker, BGP4_MARKER_BYTE_VAL, BGP4_MARKER_LEN);

    return (MEMCMP (pu1Authdata, au1Marker, BGP4_MARKER_LEN) ? BGP4_FAILURE :
            BGP4_SUCCESS);
}
#endif /* BGP4IH_C */
