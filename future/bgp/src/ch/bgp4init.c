/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4init.c,v 1.75 2016/12/27 12:35:56 siva Exp $
 *
 * Description: This file contains the System Initialisation
 *
 *******************************************************************/

#ifndef BGP4INIT_C

#define BGP4INIT_C
#include "bgp4com.h"

extern UINT2        gu2RRDAsNum;
/****************************************************************************/
/* Function Name   : AdvRouteTblCmpFunc                         */
/* Description     : user compare function for  Adv Route  TBL RBTrees         */
/* Input(s)        : Two RBTree Nodes be compared                           */
/* Output(s)       : None                                                   */
/* Returns         : 1/(-1)/0                                               */
/****************************************************************************/

PRIVATE INT4
AdvRouteTblCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tAdvRoute *pRtProfile1 = (tAdvRoute *) pRBElem1;
    tAdvRoute *pRtProfile2 = (tAdvRoute *) pRBElem2;
    return (MEMCMP (&(pRtProfile1->pRouteProfile->NetAddress),
                   &(pRtProfile2->pRouteProfile->NetAddress),
                    sizeof (tNetAddress)));
}

/******************************************************************************/
/* Function Name : Bgp4Init                                                  */
/* Description   : This function intialises various confgn parameters to      */
/*                 their default values. This module needs to be called from  */
/*                 the system initialisation part.                            */
/* Input(s)      : None.                                                      */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4Init ()
{
    UINT4               u4InitialCount = BGP4_ONE_BYTE;
    UINT1              *pu1Block = NULL;
    UINT1              *pu1SdtBlock = NULL;
    UINT1              *pu1RstBlock = NULL;
    UINT4               u4Context = BGP4_DFLT_VRFID;

    /* BGP_GR : No need to re-register with RTM if Router has gracefully restarted
     * Set the global RTM registration id to 0. If this value is zero, only 
     * then registration with RTM will be performed. 
     */
    if (Bgp4CreatePools () == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME,
                  "\tBgp4Init() : MemPool Creation FAILED !!!\n");
        return (BGP4_FAILURE);
    }

    if (Bgp4InitTimer () == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME, "\tInitialising Timer FAILED !!! \n");
        return (BGP4_FAILURE);
    }

    /* Create the queue used by BGP */
    if (OsixQueCrt ((UINT1 *) BGP4_QUE, OSIX_MAX_Q_MSG_LEN,
                    BGP4_Q_DEPTH, &BGP4_Q_ID) != OSIX_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4InitEnv() : Bgp4 Queue Creation FAILED !!!\n");
        return BGP4_FAILURE;
    }

    /* Create the queue used for BGP-SNMP Response message. */
    if (OsixQueCrt ((UINT1 *) BGP4_RESP_QUE, OSIX_MAX_Q_MSG_LEN,
                    BGP4_RESP_Q_DEPTH, &BGP4_RES_Q_ID) != OSIX_SUCCESS)

    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4InitEnv() : Bgp4-SNMP Queue Creation FAILED !!!\n");
        return BGP4_FAILURE;
    }
    /* Create the Semaphore used for mutually exclusive use of 
     * the RIB table
     */
    if (OsixCreateSem ((const UINT1 *) "RIB1",
                       u4InitialCount, OSIX_DEFAULT_SEM_MODE,
                       &(gBgpNode.Bgp4RibSemId)) != OSIX_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4InitEnv() : Semaphore Creation FAILED !!!\n");
        return (BGP4_FAILURE);
    }

    /* Create the Semaphore for RTM RouteList */
    if (OsixSemCrt ((UINT1 *) "BGPRTMS",
                    &(gBgpNode.Bgp4RedistListSemId)) != OSIX_SUCCESS)
    {

    }
    OsixSemGive (gBgpNode.Bgp4RedistListSemId);
    /* Create the Bgp4 Take semaphore  */
    if (OsixSemCrt ((UINT1 *) "BGPSEM",
                    &(gBgpNode.Bgp4ProtoSemId)) != OSIX_SUCCESS)
    {
        return BGP4_FAILURE;
    }
    OsixSemGive (gBgpNode.Bgp4ProtoSemId);

    /* Start the one-second Timer */
    pu1Block = (UINT1 *) (MemAllocMemBlk (gBgpNode.Bgp4PollTmrPoolId));
    if (pu1Block == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "Bgp4Init() : Timer allocation failed for One sec timer !!!\n");
        return (BGP4_FAILURE);
    }
    gBgpNode.pBgpOneSecTmr = (tTmrAppTimer *) (VOID *) pu1Block;
    gBgpNode.pBgpOneSecTmr->u4Data = BGP4_ONESEC_TIMER;

    /* Allocate memory for the selection deferral timer  */
    pu1SdtBlock = (UINT1 *) MemAllocMemBlk (gBgpNode.Bgp4PollTmrPoolId);
    if (pu1SdtBlock == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "Bgp4Init() : Timer allocation failed for Selection Deferal Timer !!!\n");
        return (BGP4_FAILURE);
    }

    gBgpNode.pBgpSelectionDeferTmr = (tTmrAppTimer *) (VOID *) pu1SdtBlock;
    gBgpNode.pBgpSelectionDeferTmr->u4Data = BGP4_SELECTION_DEFERRAL_TIMER;

    /* Allocate memory for the selection deferral timer  */
    pu1SdtBlock = (UINT1 *) MemAllocMemBlk (gBgpNode.Bgp4PollTmrPoolId);
    if (pu1SdtBlock == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "Bgp4Init() : Timer allocation failed for Network MSR temp timer !!!\n");
        return (BGP4_FAILURE);
    }
    gBgpNode.pBgpNetworkTempMsrTmr = (tTmrAppTimer *) (VOID *) pu1SdtBlock;
    gBgpNode.pBgpNetworkTempMsrTmr->u4Data = BGP4_NETWORK_MSR_TEMP_TIMER;
    gBgpNode.u4BgpNetworkTempTmrRunning = OSIX_FALSE;

    /* Allocate memory for the restart Timer */
    pu1RstBlock = (UINT1 *) MemAllocMemBlk (gBgpNode.Bgp4PollTmrPoolId);
    if (pu1RstBlock == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "Bgp4Init() : Timer allocation failed for Restart Timer !!!\n");
        return (BGP4_FAILURE);
    }
    /* Start the restart timer with restart timer interval */
    gBgpNode.pBgpRestartTmr = (tTmrAppTimer *) (VOID *) pu1RstBlock;
    gBgpNode.pBgpRestartTmr->u4Data = BGP4_RESTART_TIMER;

#ifdef CLI_WANTED
    /* Allocate memory for CSR to support Graceful Restart */
    if (CsrMemAllocation (BGP_MODULE_ID) == CSR_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4InitEnv() : CsrMemAllocation FAILED !!!\n");
        return BGP4_FAILURE;
    }
#endif

    BGP4_GLB_LOCAL_AS_NO = BGP4_INV_AS;
    BGP4_TASK_INIT_STATUS = BGP4_FALSE;
    BGP4_DBG_FLAG = 0;
    BGP4_TRC_FLAG = 0;
    BGP4_MAX_PEERS_ALLOCATED =
        FsBGPSizingParams[MAX_BGP_PEERS_SIZING_ID].u4PreAllocatedUnits;
    BGP4_MAX_ROUTES_ALLOCATED =
        FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].u4PreAllocatedUnits;
    /* Number of interface configured */
    BGP4_IFACE_CNT = 0;
    BGP4_MSG_CNT = 0;
    BGP4_RTPROFILE_CNT = 0;
    BGP4_LINKNODE_CNT = 0;
    BGP4_INFO_CNT = 0;
    BGP4_ASNODE_CNT = 0;
    BGP4_INPUTQ_MSG_CNT = 0;
    BGP4_RFD_FLAG = BGP4_FALSE;
    BGP4_GLB_FOUR_BYTE_ASN_SUPPORT = BGP4_TRUE;
    BGP4_GLB_FOUR_BYTE_ASN_TYPE = BGP4_TRUE;
#ifdef L3VPN
    BGP4_ROUTE_LEAK_FLAG = BGP4_CLI_AFI_DISABLED;
#endif

    gBgpCxtNode = (tBgpCxtNode **)
        MemAllocMemBlk (gBgpNode.BgpGlobalCxtMemPoolId);
    if (gBgpCxtNode == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4Init() : Unable to allocate memory for gBgpCxtNode !!\n");
        return BGP4_FAILURE;
    }
    MEMSET (gBgpCxtNode, 0, sizeof (tBgpGlobalCxt) * MAX_BGP_GLOBAL_CXT);
    gBgpCxtNode[BGP4_DFLT_VRFID] = (tBgpCxtNode *)
        MemAllocMemBlk (gBgpNode.BgpCxtMemPoolId);
    if (gBgpCxtNode[BGP4_DFLT_VRFID] == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME,
                  "\tError - Context Node allocation failed!!!\n");
        return BGP4_FAILURE;
    }
    MEMSET (gBgpCxtNode[BGP4_DFLT_VRFID], 0, sizeof (tBgpCxtNode));
    gpBgpCurrCxtNode = gBgpCxtNode[BGP4_DFLT_VRFID];
    Bgp4GlbGRInit ();
    Bgp4InitContextGlobals (BGP4_DFLT_VRFID);

    if ((IssGetCsrRestoreFlag () == BGP4_TRUE))
    {
        BGP4_RTM_REG_ID (BGP4_DFLT_VRFID) = BGP_ID;
    }
    else
    {
        BGP4_RTM_REG_ID (BGP4_DFLT_VRFID) = 0;
    }

    /* Initialize global interface information list */
    TMO_SLL_Init (BGP4_IFACE_GLOBAL_LIST);

    Bgp4InitMed (VCM_INVALID_VC);

    Bgp4InitLp (VCM_INVALID_VC);

    Bgp4InitFilter (VCM_INVALID_VC);

    Bgp4InitAggr (VCM_INVALID_VC);

    if (CommInit (0) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME, "\tCOMM Initialization FAILED !!! \n");
        return (BGP4_FAILURE);
    }

    if (ExtCommInit (0, 0) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME, "\tECOMM Initialization FAILED !!! \n");
        return (BGP4_FAILURE);
    }

#ifdef VPLSADS_WANTED
    if (Bgp4VplsInit () != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC | BGP4_VPLS_CRI_TRC,
                  BGP4_MOD_NAME, "\tVPLS Initialization Failed !!!\n");
        return (BGP4_FAILURE);
    }
#endif

#ifdef L3VPN
    if (Bgp4Vpnv4Init () != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                  "\tVPNv4 Initialization Failed !!!\n");
        return (BGP4_FAILURE);
    }
#endif
#ifdef EVPN_WANTED
    if (Bgp4EvpnInit () != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                  "\tEVPN Initialization Failed !!!\n");
        return (BGP4_FAILURE);
    }
#endif
   if (Bgp4RedInitGlobalInfo () == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME, "BGP RM Initialization FAILED !!! \n");
        return (BGP4_FAILURE);
    }
    while (VcmGetNextActiveL3Context (u4Context, &u4Context) != VCM_FAILURE)
    {
        if (Bgp4CreateContext (u4Context) == BGP4_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                      "\tBgp4Init() : BGP Init Failed.\n");
            return (BGP4_FAILURE);
        }
    }

    return (BGP4_SUCCESS);
}

INT4
Bgp4InitContextGlobals (UINT4 u4Cxtid)
{
    INT4                i4RetSts = BGP4_FAILURE;
    if (Bgp4InitGlobals (u4Cxtid,BGP4_TRUE) == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME, "\tInitialising Globals FAILED !!!\n");
        return (BGP4_FAILURE);
    }
#ifdef RFD_WANTED
    /* Initialise RFD globals with Default values. When BGP
     * admin status is made up, if File system is supported by
     * the environment, then the value from file rfdconfig.size will
     * be taken by the RFD globals.
     */
    RfdUpdateDefaultConfigValues (u4Cxtid);
#endif
    if (Bgp4CreateIgpMetricTable (u4Cxtid) == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME,
                  "\tInitialising IGP Metric Table FAILED !!! \n");
        return (BGP4_FAILURE);
    }

    if (Bgp4CreateRcvdPADB (u4Cxtid) == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME,
                  "\tInitialising Received PA Database FAILED !!! \n");
        return (BGP4_FAILURE);
    }

    if (Bgp4CreateAdvtPADB (u4Cxtid) == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME,
                  "\tInitialising Advertise PA Database FAILED !!! \n");
        return (BGP4_FAILURE);
    }
#ifdef EVPN_WANTED
       gBgpCxtNode[u4Cxtid]->EvpnRoutes = RBTreeCreate(FsBGPSizingParams
                                                       [MAX_BGP_EVPN_VRF_VNI_CHG_NODES_SIZING_ID].
                                                        u4PreAllocatedUnits,
                                                        EvpnRouteTblCmpFunc);
       if (gBgpCxtNode[u4Cxtid]->EvpnRoutes == NULL)
       {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                     BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                     BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                     "\tBgp4InitPeerEntry() : Creation of Evpn Route Tbl "
                     "FAILED !!!\n");
            return BGP4_FAILURE;
       }
       gBgpCxtNode[u4Cxtid]->BgpMacDupTimer = 
           RBTreeCreate( FsBGPSizingParams[MAX_BGP_MAC_DUP_TIMERS_SIZING_ID].
                         u4PreAllocatedUnits,BgpMacDupTimerTblCmpFunc);
       if (gBgpCxtNode[u4Cxtid]->BgpMacDupTimer == NULL)
       {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                     BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                     BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                     "\tCreation of Bgp Mac Duplication Timer Tbl "
                     "FAILED !!!\n");
            return BGP4_FAILURE;
       }

#endif
    RflInitHandler (u4Cxtid);
    i4RetSts = CapsInitHandler (u4Cxtid);
    if (i4RetSts != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME, "\tCAPS Initialization FAILED !!! \n");
        return (BGP4_FAILURE);
    }

    if (Bgp4MpeInit (u4Cxtid) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                  "\tMPE Initialization Failed !!!\n");
        return (BGP4_FAILURE);
    }
    if (Bgp4InitEnv (u4Cxtid) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_INIT_SHUT_TRC | BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME, "\tEnvironment Initialization Failed !!!\n");
        return (BGP4_FAILURE);
    }

    Bgp4GRInit (u4Cxtid);
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4InitPeerEntry                                          */
/* Description   : This function intialises the BGP Peer entry with the       */
/*                 default informations.                                      */
/* Input(s)      : Peer Entry which needs to be initialized (pPeerentry).     */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                 */
/******************************************************************************/
INT4
Bgp4InitPeerEntry (tBgp4PeerEntry * pPeerentry)
{
    INT1                i1Index = 0;
    INT4                i4MaxRoutes = 0;

    BGP4_PEER_SENT_ROUTES(pPeerentry) = RBTreeCreateEmbedded(0,AdvRouteTblCmpFunc);
    if (BGP4_PEER_SENT_ROUTES (pPeerentry) == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tBgp4InitPeerEntry() : Creation of Adv Route Tbl "
                  "FAILED !!!\n");
        return BGP4_FAILURE;
    }

    TMO_SLL_Init_Node (BGP4_PEER_NEXT (pPeerentry));
    Bgp4InitAddrPrefixStruct (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)), 0);
    BGP4_PEER_ASNO (pPeerentry) = BGP4_INV_AS;

    BGP4_PEER_ADMIN_STATUS (pPeerentry) = BGP4_INVALID;
    Bgp4InitNetAddressStruct (&(BGP4_PEER_LOCAL_NETADDR_INFO (pPeerentry)), 0,
                              0);
    Bgp4InitAddrPrefixStruct (&(BGP4_PEER_NETWORK_ADDR_INFO (pPeerentry)), 0);
    Bgp4InitNetAddressStruct (&(BGP4_PEER_LCL_NETADDR_INFO (pPeerentry)), 0, 0);
#ifdef BGP4_IPV6_WANTED
    /* Initialize linklocal address - default with IPV6 */
    Bgp4InitAddrPrefixStruct (&(BGP4_PEER_LINK_LOCAL_ADDR_INFO (pPeerentry)),
                              BGP4_INET_AFI_IPV6);
#endif
    Bgp4InitAddrPrefixStruct (&(BGP4_PEER_GATEWAY_ADDR_INFO (pPeerentry)), 0);
    BGP4_PEER_DIRECTLY_CONNECTED (pPeerentry) = BGP4_FALSE;
    BGP4_PEER_IS_IN_TRANX_LIST (pPeerentry) = BGP4_FALSE;
    BGP4_PEER_CONN_RETRY_TIME (pPeerentry) = BGP4_DEF_CONNRETRYINTERVAL;
    BGP4_PEER_HOLD_TIME (pPeerentry) = BGP4_DEF_HOLDINTERVAL;
    BGP4_PEER_IDLE_HOLD_TIME (pPeerentry) = BGP4_DEF_IDLEHOLDINTERVAL;
    BGP4_PEER_DELAY_OPEN_TIME (pPeerentry) = BGP4_DEF_DELAYOPENINTERVAL;
    BGP4_PEER_KEEP_ALIVE_TIME (pPeerentry) = BGP4_DEF_KEEPALIVEINTERVAL;
    BGP4_PEER_MIN_AS_ORIG_TIME (pPeerentry) = BGP4_DEF_MINASORIGINTERVAL;
    BGP4_PEER_MIN_ADV_TIME (pPeerentry) = BGP4_DEF_EBGP_MINROUTEADVINTERVAL;
    BGP4_PEER_AUTH_TYPE (pPeerentry) = BGP4_NO_AUTH;
    BGP4_PEER_EBGP_MULTIHOP (pPeerentry) = BGP4_EBGP_MULTI_HOP_DISABLE;
    BGP4_PEER_SELF_NEXTHOP (pPeerentry) = BGP4_DISABLE_NEXTHOP_SELF;
    BGP4_PEER_OVERRIDE_CAPABILITY (pPeerentry) =
        BGP4_DISABLE_OVERRIDE_CAPABILITY;
    BGP4_CONFED_PEER_STATUS (pPeerentry) = BGP4_FALSE;
    BGP4_PEER_LOCAL_ADDR_CONFIGURED (pPeerentry) = BGP4_FALSE;
    BGP4_PEER_RFL_CLIENT (pPeerentry) = NON_CLIENT;
    BGP4_PEER_DEF_ROUTE_ORIG_STATUS (pPeerentry) = BGP4_DEF_ROUTE_ORIG_DISABLE;
    BGP4_PEER_HOLD_ADVT_ROUTES (pPeerentry) = BGP4_HOLD_ROUTES_DISABLE;
    BGP4_PEER_STATE (pPeerentry) = BGP4_IDLE_STATE;
    BGP4_PEER_PREV_STATE (pPeerentry) = BGP4_NO_STATE;
    BGP4_GET_PEER_PEND_FLAG (pPeerentry) = BGP4_PEER_NOPEND_JOBS;
    BGP4_SET_PEER_CURRENT_STATE (pPeerentry, BGP4_PEER_READY);
    /* RFC 4271 Update */
    BGP4_PEER_ALLOW_AUTOMATIC_START (pPeerentry) =
        BGP4_PEER_AUTOMATICSTART_DISABLE;
    BGP4_PEER_ALLOW_AUTOMATIC_STOP (pPeerentry) =
        BGP4_PEER_AUTOMATICSTOP_DISABLE;
    BGP4_DAMP_PEER_OSCILLATIONS (pPeerentry) =
        BGP4_DAMP_PEER_OSCILLATIONS_DISABLE;
    BGP4_PEER_DELAY_OPEN (pPeerentry) = BGP4_PEER_DELAY_OPEN_DISABLE;

    i4MaxRoutes = (INT4) FsBGPSizingParams
        [MAX_BGP_ROUTES_SIZING_ID].u4PreAllocatedUnits;
    BGP4_PEER_PREFIX_UPPER_LIMIT (pPeerentry) = (UINT4) i4MaxRoutes;

    BGP4_PEER_CURRENT_PREFIX_LIMIT (pPeerentry) = 0;
    BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT (pPeerentry) =
        BGP4_DEF_CONNECT_RETRY_COUNT;
    BGP4_PEER_TCP_CONFIGURED_CONNECT_COUNT (pPeerentry) = 0;
    BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT(pPeerentry) = 0;
    BGP4_PEER_FSM_HIST_HEAD (pPeerentry) = BGP4_INVALID_HIST;
    BGP4_PEER_FSM_HIST_TAIL (pPeerentry) = BGP4_INVALID_HIST;
    for (i1Index = 0; i1Index < BGP4_PEER_FSM_TRANS_HIST_MAX_INDX; i1Index++)
    {
        (BGP4_PEER_FSM_TRANSITION_HIST (pPeerentry, i1Index)) = 0;
    }

    BGP4_PEER_IN_UPDATES (pPeerentry) = BGP4_INIT_COUNT;
    BGP4_PEER_OUT_UPDATES (pPeerentry) = BGP4_INIT_COUNT;
    BGP4_PEER_IN_MSGS (pPeerentry) = BGP4_INIT_COUNT;
    BGP4_PEER_OUT_MSGS (pPeerentry) = BGP4_INIT_COUNT;
    BGP4_PEER_LAST_ERROR (pPeerentry) = BGP4_NO_ERROR;
    BGP4_PEER_LAST_ERROR_SUB_CODE (pPeerentry) = BGP4_NO_ERROR;
    BGP4_PEER_FSM_TRANS (pPeerentry) = BGP4_INIT_COUNT;
    BGP4_PEER_ESTAB_TIME (pPeerentry) = BGP4_INIT_TIME;
    BGP4_PEER_IN_UPDATE_ELAP_TIME (pPeerentry) = BGP4_INIT_TIME;
    BGP4_PEER_START_TIME (pPeerentry) = BGP4_DEF_STARTINTERVAL;
    BGP4_PEER_BGP_ID (pPeerentry) = BGP4_INV_IPADDRESS;
    BGP4_PEER_LOCAL_PORT (pPeerentry) = BGP4_INV_PORT;
    BGP4_PEER_REMOTE_PORT (pPeerentry) = BGP4_INV_PORT;
    BGP4_PEER_CONN_ID (pPeerentry) = BGP4_INV_CONN_ID;
    BGP4_PEER_NEG_VER (pPeerentry) = BGP4_INV_VER;
    BGP4_PEER_NEG_HOLD_INT (pPeerentry) = 0;
    BGP4_PEER_NEG_KEEP_ALIVE (pPeerentry) = 0;
    BGP4_PEER_NEG_CAP_MASK (pPeerentry) = 0;
    BGP4_PEER_NEG_ASAFI_MASK (pPeerentry) = 0;
    BGP4_PEER_SUP_ASAFI_MASK (pPeerentry) = 0;
    BGP4_PEER_CONN_PASSIVE (pPeerentry) = BGP4_FALSE;
    BGP4_PEER_HOPLIMIT (pPeerentry) = BGP4_DEFAULT_HOPLIMIT;
    BGP4_PEER_SENDBUF (pPeerentry) = BGP4_DEFAULT_SENDBUF;
    BGP4_PEER_RECVBUF (pPeerentry) = BGP4_DEFAULT_RECVBUF;
    BGP4_PEER_COMM_SEND_STATUS (pPeerentry) = BGP4_TRUE;
    BGP4_PEER_ECOMM_SEND_STATUS (pPeerentry) = BGP4_TRUE;
    BGP4_GET_CAPS_PEER_FLAGS (pPeerentry) = 0;

#ifdef L3VPN
    BGP4_VPN4_PEER_ROLE (pPeerentry) = BGP4_VPN4_NORMAL_PEER;
    BGP4_VPN4_PEER_CERT_STATUS (pPeerentry) = BGP4_VPN4_CE_RT_DONOTSEND;
    BGP4_VPN4_CE_PEER_SOO_EXT_COM (pPeerentry) = NULL;
#endif
    BGP4_PEER_IF_INDEX (pPeerentry) = BGP4_INVALID_IFINDX;
    /* Initialize the peer init netaddress structure with IPV4 family */
    Bgp4InitNetAddressStruct ((&(BGP4_PEER_INIT_NETADDR_INFO (pPeerentry))),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    BGP4_PEER_INIT_PA_ENTRY (pPeerentry) = NULL;
    BGP4_PEER_INIT_PA_HASHKEY (pPeerentry) = 0;

    TMO_SLL_Init (BGP4_PEER_SUP_CAPS_LIST (pPeerentry));
    TMO_SLL_Init (BGP4_PEER_RCVD_CAPS_LIST (pPeerentry));
    TMO_SLL_Init (BGP4_PEER_RCVD_ROUTES (pPeerentry));
    TMO_SLL_Init (BGP4_PEER_ADVT_MSG_LIST (pPeerentry));
    TMO_SLL_Init (BGP4_PEER_SOFTCONFIG_IN_PEND_LIST (pPeerentry));
    TMO_SLL_Init (BGP4_PEER_SOFTCONFIG_OUT_PEND_LIST (pPeerentry));
    BGP4_PEER_READVT_BUFFER (pPeerentry) = NULL;
    BGP4_PEER_TCPMD5_PTR (pPeerentry) = NULL;
    BGP4_PEER_TCPAO_MKT (pPeerentry) = NULL;
    BGP4_PEER_LOCAL_AS (pPeerentry) = BGP4_INV_AS;
    BGP4_PEER_LOCAL_AS_CFG (pPeerentry) = BGP4_FALSE;

    MEMSET (&pPeerentry->peerLocal.tStartTmr, 0, sizeof (tBgp4AppTimer));
    MEMSET (&pPeerentry->peerLocal.tHoldTmr, 0, sizeof (tBgp4AppTimer));
    MEMSET (&pPeerentry->peerLocal.tIdleHoldTmr, 0, sizeof (tBgp4AppTimer));
    MEMSET (&pPeerentry->peerLocal.tDelayOpenTmr, 0, sizeof (tBgp4AppTimer));
    MEMSET (&pPeerentry->peerLocal.tKeepAliveTmr, 0, sizeof (tBgp4AppTimer));
    MEMSET (&pPeerentry->peerLocal.tConnRetryTmr, 0, sizeof (tBgp4AppTimer));
    MEMSET (&pPeerentry->peerLocal.tRouteAdvTmr, 0, sizeof (tBgp4AppTimer));
    MEMSET (&pPeerentry->peerLocal.tOrigTmr, 0, sizeof (tBgp4AppTimer));
    MEMSET (&pPeerentry->peerLocal.tStaleTmr, 0, sizeof (tBgp4AppTimer));
    MEMSET (&pPeerentry->peerLocal.tPeerRestartTmr, 0, sizeof (tBgp4AppTimer));

    pPeerentry->peerLocal.tStartTmr.u4TimerId = BGP4_START_TIMER;
    pPeerentry->peerLocal.tHoldTmr.u4TimerId = BGP4_HOLD_TIMER;
    pPeerentry->peerLocal.tIdleHoldTmr.u4TimerId = BGP4_IDLE_HOLD_TIMER;
    pPeerentry->peerLocal.tDelayOpenTmr.u4TimerId = BGP4_DELAY_OPEN_TIMER;
    pPeerentry->peerLocal.tKeepAliveTmr.u4TimerId = BGP4_KEEPALIVE_TIMER;
    pPeerentry->peerLocal.tConnRetryTmr.u4TimerId = BGP4_CONNECTRETRY_TIMER;
    pPeerentry->peerLocal.tRouteAdvTmr.u4TimerId = BGP4_MINROUTEADV_TIMER;
    pPeerentry->peerLocal.tOrigTmr.u4TimerId = BGP4_MINASORIG_TIMER;
    pPeerentry->peerLocal.tStaleTmr.u4TimerId = BGP4_STALE_TIMER;
    pPeerentry->peerLocal.tPeerRestartTmr.u4TimerId = BGP4_PEER_RESTART_TIMER;
    pPeerentry->u1BfdStatus = BGP4_BFD_DISABLE;

    MEMSET (&(pPeerentry->ResidualBuf), 0, sizeof (tBufNode));

    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4InitGlobals                                            */
/* Description   : This function intialises the BGP related Global varibles.  */
/* Input(s)      :  u4CxtID -  Context Id                                     */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4InitGlobals (UINT4 u4CxtID,UINT1 u1Flag)
{
    UINT4               u4Indx;
    UINT4               u4ActiveContext = 0;
#ifdef L3VPN
    UINT1               u1LabelFlag = BGP4_FALSE;
#endif
    UINT4               u4Cnt =0;
    UINT4               u4BgpMaxPeers =0;

        u4BgpMaxPeers = FsBGPSizingParams[MAX_BGP_PEERS_SIZING_ID].
                    u4PreAllocatedUnits;
    BGP4_STATUS (u4CxtID) = BGP4_FALSE;
    if (BgpGetFirstActiveContextID (&u4ActiveContext) != SNMP_SUCCESS)
    {
        BGP4_GLB_LOCAL_AS_NO = BGP4_INV_AS;
        for (u4ActiveContext = 0;
             u4ActiveContext <
             FsBGPSizingParams[MAX_BGP_CXT_NODES_SIZING_ID].u4PreAllocatedUnits;
             u4ActiveContext++)
        {
            if (NULL == gBgpCxtNode[u4ActiveContext])
            {
                continue;
            }
            if (BGP4_LOCAL_AS_CFG (u4ActiveContext) == BGP4_FALSE)
            {
                BGP4_LOCAL_AS_NO (u4ActiveContext) = BGP4_GLB_LOCAL_AS_NO;
            }
        }
    }
    BGP4_LOCAL_AS_NO (u4CxtID) = BGP4_GLB_LOCAL_AS_NO;
    BGP4_LOCAL_AS_CFG (u4CxtID) = BGP4_FALSE;
    BGP4_LOCAL_ADMIN_STATUS (u4CxtID) = BGP4_ADMIN_DOWN;
    BGP4_CLI_ADMIN_STATUS (u4CxtID) = BGP4_DOWN;
    BGP4_LOCAL_BGP_ID_CONFIG_TYPE (u4CxtID) = BGP4_LOCAL_BGP_ID_DYNAMIC;
    BGP4_LOCAL_SYNC_STATUS (u4CxtID) = BGP4_DISABLE_SYNC;
    BGP4_LOCAL_LISTEN_PORT (u4CxtID) = BGP4_DEF_LSNPORT;
    BGP4_TRAP_STATUS (u4CxtID) = BGP4_TRAP_ENABLE;
#ifdef BGP_TCP4_WANTED
    BGP4_LOCAL_V4_LISTEN_CONN (u4CxtID) = BGP4_INV_CONN_ID;
#endif
#ifdef BGP_TCP6_WANTED
    BGP4_LOCAL_V6_LISTEN_CONN (u4CxtID) = BGP4_INV_CONN_ID;
#endif
    BGP4_DEFAULT_LOCAL_PREF (u4CxtID) = BGP4_DEF_LP;
    BGP4_DEFAULT_IGP_METRIC (u4CxtID) = BGP4_DEF_IGP_METRIC_VAL;
    BGP4_DEFAULT_AFI_SAFI_FLAG (u4CxtID) = BGP4_TRUE;
    BGP4_IPV4_AFI_FLAG (u4CxtID) = BGP4_TRUE;
    BGP4_IPV6_AFI_FLAG (u4CxtID) = BGP4_FALSE;
#ifdef L3VPN
   /* When address family is removed for Non-Default VRF, VPNv4 flag should not reset */
    if (u4CxtID == BGP4_DFLT_VRFID)
    {
        BGP4_VPNV4_AFI_FLAG = BGP4_FALSE;
    }
#endif
#ifdef EVPN_WANTED
   BGP4_EVPN_AFI_FLAG = BGP4_FALSE;
   BGP4_GLB_MAC_DUP_TIMER_INTERVAL = BGP4_DEF_MAC_MOB_DUP_INTERVAL;
   BGP4_GLB_MAX_MAC_MOVES  = BGP4_DEF_MAX_MAC_MOVES;
#endif
#ifdef VPLSADS_WANTED
    BGP4_L2VPN_AFI_FLAG (u4CxtID) = BGP4_FALSE;
#endif
    BGP4_NON_BGP_RT_EXPORT_POLICY (u4CxtID) = BGP4_EXT_OR_INT_PEER;
    Bgp4InitNetAddressStruct (&(BGP4_NON_BGP_INIT_ROUTE_INFO (u4CxtID)),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    BGP4_ALWAYS_COMPARE_MED (u4CxtID) = BGP4_MED_COMPARE_DISABLE;
    BGP4_OVERLAP_ROUTE_POLICY (u4CxtID) = BGP4_INSTALL_BOTH_RT;
    BGP4_GLOBAL_STATE (u4CxtID) = BGP4_GLOBAL_READY;
    BGP4_GET_GLOBAL_FLAG (u4CxtID) = BGP4_NO_PENDJOBS;
    BGP4_NH_CHG_PRCS_INTERVAL (u4CxtID) = BGP4_NH_CHG_PRCS_DEF_INTERVAL;
    BGP4_RTM_REG_FLAG_STATUS (u4CxtID) = BGP4_TRUE;
    /* Initialize confed global data */
    BGP4_CONFED_ID (u4CxtID) = BGP4_INV_AS;
    for (u4Indx = 0; u4Indx < BGP4_CONFED_MAX_MEMBER_AS; u4Indx++)
    {
        BGP4_CONFED_PEER (u4CxtID, u4Indx) = BGP4_INV_AS;
    }
    BGP4_CONFED_BESTPATH_COMPARE_MED (u4CxtID) = BGP4_FALSE;

    /* Initialize the FIB update list and FIB update hold down time */
    BGP4_FIB_LIST_FIRST_ROUTE (u4CxtID) = NULL;
    BGP4_FIB_LIST_LAST_ROUTE (u4CxtID) = NULL;
    BGP4_FIB_LIST_COUNT (u4CxtID) = 0;
    BGP4_FIB_UPD_HOLD_TIME (u4CxtID) = 0;

    /* Initialize the peer's advertisement list. */
    BGP4_EXT_PEER_LIST_FIRST_ROUTE (u4CxtID) = NULL;
    BGP4_EXT_PEER_LIST_LAST_ROUTE (u4CxtID) = NULL;
    BGP4_EXT_PEER_LIST_COUNT (u4CxtID) = 0;

    BGP4_INT_PEER_LIST_FIRST_ROUTE (u4CxtID) = NULL;
    BGP4_INT_PEER_LIST_LAST_ROUTE (u4CxtID) = NULL;
    BGP4_INT_PEER_LIST_COUNT (u4CxtID) = 0;

    BGP4_CHG_LIST_FIRST_ROUTE (u4CxtID) = NULL;
    BGP4_CHG_LIST_LAST_ROUTE (u4CxtID) = NULL;
    BGP4_CHG_LIST_COUNT (u4CxtID) = 0;
    BGP4_IBGP_REDISTRIBUTE_STATUS (u4CxtID) = BGP4_IBGP_REDISTRIBUTE_DISABLE;

    /* Creating RBTree for storing received ORF entries */
    if ((BGP4_ORF_ENTRY_TABLE (u4CxtID) =
         RBTreeCreateEmbedded (0, BgpOrfListTableRBCmp)) == NULL)
    {
        return BGP4_FAILURE;
    }
    /* Initialize the redistribution list */
    TMO_SLL_Init (BGP4_REDIST_LIST (u4CxtID));

    /* Creating RBTree for storing received network route entries */
    if ((BGP4_NETWORK_ROUTE_DATABASE(u4CxtID) =
         RBTreeCreateEmbedded (0, Bgp4RRDNetworkTableCmp)) == NULL)
    {
        return BGP4_FAILURE;
    }

    /* Initialize the initialisation peer list. */
    TMO_SLL_Init (BGP4_PEER_INIT_LIST (u4CxtID));

    /* Initialize the de-init peer list */
    TMO_SLL_Init (BGP4_PEER_DEINIT_LIST (u4CxtID));

    /* Initialize the outbound soft-reconfig list */
    TMO_SLL_Init (BGP4_SOFTCFG_OUTBOUND_LIST (u4CxtID));

    /* Initialize the list used for overlapping routes */
    TMO_SLL_Init (BGP4_OVERLAP_ROUTE_LIST (u4CxtID));

    TMO_SLL_Init (&(BGP4_TCPAO_MKT_LIST (u4CxtID)));
    /* Initialize the synchronisation parameters.. */
    TMO_SLL_Init (BGP4_NONSYNC_LIST (u4CxtID));
    Bgp4InitNetAddressStruct (&(BGP4_SYNC_INIT_ROUTE_NETADDR_INFO (u4CxtID)),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    BGP4_SYNC_INTERVAL (u4CxtID) = 0;

    /* Initialize the Next hop Metric Change parameters. */
    Bgp4InitNetAddressStruct (&(BGP4_NH_CHG_INIT_ROUTE_NETADDR_INFO (u4CxtID)),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    Bgp4InitAddrPrefixStruct (&(BGP4_NH_CHG_INIT_NEXTHOP_PEER_INFO (u4CxtID)),
                              BGP4_INET_AFI_IPV4);
    Bgp4InitAddrPrefixStruct (&(BGP4_NH_CHG_INIT_NEXTHOP_INFO (u4CxtID)),
                              BGP4_INET_AFI_IPV4);

    BGP4_NH_CHG_INTERVAL (u4CxtID) = 0;

#ifdef L3VPN
    Bgp4InitNetAddressStruct (&(BGP4_LSP_CHG_INIT_ROUTE_NETADDR_INFO),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_VPNV4_UNICAST);
    BGP4_LSP_CHG_INIT_VRFID (u4CxtID) = BGP4_DFLT_VRFID;
    /* Since the RouteMap Label Allocation Policy  is always fetched 
     * from the default context , During InitGlobals call from 
     * ClearandReinit, BGP4_LABEL_ALLOC_POLICY will be set to the 
     * default value as BGP4_PERVRF_LABEL_ALLOC_POLICY.
     * Inorder to Avoid resetting of the Allocation policy when there
     * exists an active VPN vrf, the initialisation of the route map 
     * allocation policy is always made only when all the VRFs are in 
     * BGP4_L3VPN_VRF_DELETE state */
    for (u4ActiveContext = 0;
          u4ActiveContext <
          FsBGPSizingParams[MAX_BGP_CXT_NODES_SIZING_ID].u4PreAllocatedUnits;
          u4ActiveContext++)
    {
        if (NULL == gBgpCxtNode[u4ActiveContext])
        {
            continue;
        }
        
        if ((BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4ActiveContext) == BGP4_L3VPN_VRF_CREATE) ||
            (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4ActiveContext) == BGP4_L3VPN_VRF_UP ) ||
            (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4ActiveContext) == BGP4_L3VPN_VRF_DOWN))
        {
            u1LabelFlag = BGP4_TRUE; 
        }
    }
     /* Initialise the attributes only when no active VPN vrfs exits. */
    if (u1LabelFlag != BGP4_TRUE)
    {
        BGP4_LABEL_ALLOC_POLICY (u4CxtID) = BGP4_PERVRF_LABEL_ALLOC_POLICY;
        BGP4_VPNV4_LABEL (u4CxtID) = 0;
        BGP4_L3VPN_ROUTE_COUNT (u4CxtID) = 0;
    }

    if ((BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4CxtID) != BGP4_L3VPN_VRF_CREATE) &&
        (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4CxtID) != BGP4_L3VPN_VRF_UP ) &&
        (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4CxtID) != BGP4_L3VPN_VRF_DOWN))
    {
        TMO_SLL_Init (BGP4_VPN4_VRF_SPEC_IMPORT_TARGETS (u4CxtID));
        TMO_SLL_Init (BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS (u4CxtID));
        MEMSET (BGP4_VPN4_VRF_SPEC_ROUTE_DISTING (u4CxtID), 0,
                BGP4_VPN4_ROUTE_DISTING_SIZE);
        BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4CxtID) = BGP4_L3VPN_VRF_DELETE;
    }
    /* tmp */
    BGP4_VPN4_VRF_SPEC_GET_FLAG (u4CxtID) = 0;
    Bgp4InitNetAddressStruct (&(BGP4_VPN4_VRF_SPEC_INIT_NET_ADDR (u4CxtID)),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_VPNV4_UNICAST);
#endif
    BGP4_NH_CHG_INIT_VRFID (u4CxtID) = u4CxtID;

    /* Route-reflector client count */
    BGP4_RR_CLIENT_CNT (u4CxtID) = 0;

    /* Initialize the Peer Data list */
    BGP4_PEER_DATA_PEER_PTR (u4CxtID) = (tBgp4PeerEntry **)
        MemAllocMemBlk (gBgpNode.Bgp4PeerDataEntryPoolId);

    if ((BGP4_PEER_DATA_PEER_PTR (u4CxtID)) == NULL)
    {
        return BGP4_FAILURE;
    }
    for (u4Cnt = 0; u4Cnt < u4BgpMaxPeers * BGP4_MAX_ENTRY_PER_PEER; u4Cnt++)
    {
        BGP4_PEER_DATA_PEER_PTR (u4CxtID)[u4Cnt] = NULL;
    }

    BGP4_MSG_CNT++;
    BGP4_PEER_DATA_MAX_PEER (u4CxtID) = 0;
    BGP4_PEER_DATA_MAX_FD (u4CxtID) = 0;

    /* Initialize the global read sock-desc set */
    BGP_FD_ZERO (BGP4_READ_SOCK_FD_SET (u4CxtID));
    BGP_FD_ZERO (BGP4_WRITE_SOCK_FD_SET (u4CxtID));

    /* Default route Origination Status. */
    BGP4_DEF_ROUTE_ORIG_STATUS (u4CxtID) = BGP4_DEF_ROUTE_ORIG_DISABLE;
    BGP4_DEF_ROUTE_ORIG_NEW_STATUS (u4CxtID) = BGP4_DEF_ROUTE_ORIG_DISABLE;

    /* Initialize the BGP error statistics. */
    BGP4_ERR_TCP_RECV_MSG_ERROR (u4CxtID) = 0;

    BGP4_RIB_ROUTE_CNT (u4CxtID) = 0;
    BGP4_TABLE_VERSION (u4CxtID) = 0;
    BGP4_PEER_CNT (u4CxtID) = 0;
    BGP4_DISP_TIME (u4CxtID)[BGP4_PREV_ENTRY_TIME_INDEX] = 0;
    BGP4_DISP_TIME (u4CxtID)[BGP4_PREV_EXIT_TIME_INDEX] = 0;
    BGP4_DISP_TIME (u4CxtID)[BGP4_CUR_ENTRY_TIME_INDEX] = 0;
    BGP4_DISP_TIME (u4CxtID)[BGP4_CUR_EXIT_TIME_INDEX] = 0;
    gBgpCxtNode[u4CxtID]->u1AllowPeerInit = BGP4_TRUE;
    gBgpCxtNode[u4CxtID]->u4ContextId = u4CxtID;
    COMM_LEN_ERROR_STATS (u4CxtID) = 0;
    EXT_COMM_LEN_ERROR_STATS (u4CxtID) = 0;
    MEMSET (gBgpCxtNode[u4CxtID]->au4BGP4RRDMetricVal,0,sizeof(INT4)*BGP4_MAX_METRIC_TYPE);

    BGP4_MPATH_EBGP_COUNT (u4CxtID) = BGP_DEFAULT_MAXPATH;
    BGP4_MPATH_IBGP_COUNT (u4CxtID) = BGP_DEFAULT_MAXPATH;
    BGP4_MPATH_EIBGP_COUNT (u4CxtID) = BGP_DEFAULT_MAXPATH;
    BGP4_MPATH_OPER_EBGP_COUNT (u4CxtID) = BGP_DEFAULT_MAXPATH;
    BGP4_MPATH_OPER_IBGP_COUNT (u4CxtID) = BGP_DEFAULT_MAXPATH;
    BGP4_MPATH_OPER_EIBGP_COUNT (u4CxtID) = BGP_DEFAULT_MAXPATH;

    /* Initialise 4-byte-ASN support status */
    if (u1Flag == BGP4_TRUE)
    {            
    BGP4_FOUR_BYTE_ASN_SUPPORT (u4CxtID) = BGP4_TRUE;
    }
    BGP4_FOUR_BYTE_ASN_TYPE (u4CxtID) = BGP4_TRUE;
    BGP4_IS_AS_RESET (u4CxtID) = BGP4_FALSE;

    return (BGP4_SUCCESS);
}

/******************************************************************************/
/* Function Name : Bgp4InitMedlpEntry                                         */
/* Description   : This function intialises the specified MED/LP entry with   */
/*                 default values.                                            */
/* Input(s)      : MED/LP entry which needs to be initialized (pMEDLPentry)   */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
VOID
Bgp4InitMedlpEntry (tBgp4MedlpEntry * pMEDLPentry)
{
    UINT4               u4Index;

    pMEDLPentry->VrfName.i4Length = 0;
    for (u4Index = 0; u4Index < BGP4_VPN4_MAX_VRF_NAME_SIZE; u4Index++)
    {
        pMEDLPentry->VrfName.au1VrfName[u4Index] = 0;
    }
    if (pMEDLPentry->au1RowActive != (UINT1) BGP4_TRUE)
    {
        pMEDLPentry->u4VrfId = BGP4_DFLT_VRFID;
        pMEDLPentry->u1AdminStatus = BGP4_INVALID;
        pMEDLPentry->u4ASNo = BGP4_INV_AS;
        Bgp4InitNetAddressStruct ((&(pMEDLPentry->InetAddress)),
                                  BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
        pMEDLPentry->au4InterAS[0] = BGP4_INV_AS;
        pMEDLPentry->u1Dir = BGP4_INCOMING;
        pMEDLPentry->u4MEDLP = BGP4_INV_MEDLP;
        pMEDLPentry->u1Applicability = BGP4_NORMAL;
        pMEDLPentry->au1RowActive = (UINT1) BGP4_FALSE;
    }
    else if ((pMEDLPentry->au1RowActive == BGP4_TRUE))
    {
        pMEDLPentry->u1AdminStatus = BGP4_ADMIN_UP;
    }
}

/******************************************************************************/
/* Function Name : Bgp4InitMed                                                */
/* Description   : This function intialises all the entries in the MED table  */
/*                 with default configuration values.                         */
/* Input(s)      : None                                                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : NOne                                                       */
/******************************************************************************/

VOID
Bgp4InitMed (UINT4 u4Context)
{
    INT4                i4Index;

    for (i4Index = 0; i4Index < BGP4_MAX_MED_ENTRIES; i4Index++)
    {
        if ((u4Context == VCM_INVALID_VC)
            || (u4Context == BGP4_MED_ENTRY_VRFID (i4Index)))
        {
            BGP4_MED_ENTRY_SET_ROW_ACTIVE (i4Index) = (UINT1) BGP_ZERO;
            Bgp4InitMedlpEntry (&(BGP4_MED_ENTRY[i4Index]));
        }
    }
    return;
}

/******************************************************************************/
/* Function Name : Bgp4InitLp                                                 */
/* Description   : This function intialises all the entries in the LP table   */
/*                 with default configuration values.                         */
/* Input(s)      : None                                                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : NOne                                                       */
/******************************************************************************/

VOID
Bgp4InitLp (UINT4 u4Context)
{
    INT4                i4Index;

    for (i4Index = 0; i4Index < BGP4_MAX_LP_ENTRIES; i4Index++)
    {
        if ((u4Context == VCM_INVALID_VC)
            || (u4Context == BGP4_LP_ENTRY_VRFID (i4Index)))
        {
            BGP4_LP_ENTRY_SET_ROW_ACTIVE (i4Index) = (UINT1) BGP_ZERO;
            Bgp4InitMedlpEntry (&(BGP4_LP_ENTRY[i4Index]));
            BGP4_LP_ENTRY_LOCAL_PREF (i4Index) = BGP4_DEF_LP;
        }
    }
    return;
}

/******************************************************************************/
/* Function Name : Bgp4InitFilterEntry                                        */
/* Description   : This function intialises the specified Filter Table entry  */
/*                 with the default values.                                   */
/* Input(s)      : Filter Table entry which needs to be initialized           */
/*                     (pUpdFilterentry).                                     */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/

VOID
Bgp4InitFilterEntry (tBgp4UpdatefilterEntry * pUpdFilterentry)
{
    UINT4               u4Index;

    pUpdFilterentry->VrfName.i4Length = 0;
    for (u4Index = 0; u4Index < BGP4_VPN4_MAX_VRF_NAME_SIZE; u4Index++)
    {
        pUpdFilterentry->VrfName.au1VrfName[u4Index] = 0;
    }
    if (pUpdFilterentry->au1RowActive != (UINT1) BGP4_TRUE)
    {
        pUpdFilterentry->u4VrfId = BGP4_DFLT_VRFID;
        pUpdFilterentry->u1AdminStatus = BGP4_INVALID;
        pUpdFilterentry->u4ASNo = BGP4_INV_AS;
        Bgp4InitNetAddressStruct ((&(pUpdFilterentry->UpdtInetAddress)),
                                  BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
        pUpdFilterentry->au4InterAS[0] = BGP4_INV_AS;
        pUpdFilterentry->u1Dir = BGP4_INCOMING;
        pUpdFilterentry->u1Action = BGP4_DENY;
        pUpdFilterentry->au1RowActive = (UINT1) BGP4_FALSE;
    }
    else if ((pUpdFilterentry->au1RowActive) == BGP4_TRUE)
    {
        pUpdFilterentry->u1AdminStatus = BGP4_ADMIN_UP;
    }
}

/******************************************************************************/
/* Function Name : Bgp4InitFilter                                             */
/* Description   : This function intialises all the entries in the Filter     */
/*                 table with default configuration values.                   */
/* Input(s)      : None                                                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : NOne                                                       */
/******************************************************************************/

VOID
Bgp4InitFilter (UINT4 u4Context)
{
    INT4                i4Index;

    for (i4Index = 0; i4Index < BGP4_MAX_UPDFILTER_ENTRIES; i4Index++)
    {
        if ((u4Context == VCM_INVALID_VC)
            || (u4Context == BGP4_FILTER_ENTRY_VRFID (i4Index)))
        {
            BGP4_FILTER_ENTRY_SET_ROW_ACTIVE (i4Index) = (UINT1) BGP_ZERO;
            Bgp4InitFilterEntry (&(BGP4_FILTER_ENTRY[i4Index]));
        }
    }
    return;
}

/******************************************************************************/
/* Function Name : Bgp4InitAggrEntry                                       */
/* Description   : This function intialises the specified Aggregate entry     */
/*                 with default values.                                       */
/* Input(s)      : Aggregate Table entry which needs to be initialised        */
/*                 (pAggrentry).                                             */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/

VOID
Bgp4InitAggrEntry (tBgp4AggrEntry * pAggrentry)
{
    UINT4               u4Index;

    pAggrentry->VrfName.i4Length = 0;
    for (u4Index = 0; u4Index < BGP4_VPN4_MAX_VRF_NAME_SIZE; u4Index++)
    {
        pAggrentry->VrfName.au1VrfName[u4Index] = 0;
    }
    pAggrentry->u1AdminStatus = BGP4_INVALID;
    pAggrentry->u4VrfId = BGP4_DFLT_VRFID;
    Bgp4InitNetAddressStruct (&(pAggrentry->AggrInetAddress),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    pAggrentry->u1AdvType = BGP4_SUMMARY;
    pAggrentry->u1AggrStatus = BGP4_AGGR_NO_FLAG;
    Bgp4InitNetAddressStruct (&(pAggrentry->AggrInitAddr),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    TMO_SLL_Init (&(pAggrentry->TSRouteProfile));
    MEMSET (pAggrentry->au1SuppressMapName, 0, RMAP_MAX_NAME_LEN + 4);
    MEMSET (pAggrentry->au1AdvMapName, 0, RMAP_MAX_NAME_LEN + 4);
    MEMSET (pAggrentry->au1AttMapName, 0, RMAP_MAX_NAME_LEN + 4);
    pAggrentry->u1AsSet = AS_SET_DISABLE;
    pAggrentry->u1SuppressMapLen = 0;
    pAggrentry->u1AdvMapLen = 0;
    pAggrentry->u1AttMapLen = 0;
    pAggrentry->u1AsAtomicAggr = 0;
}

/******************************************************************************/
/* Function Name : Bgp4InitAggr                                               */
/* Description   : This function intialises all the entries in the Aggregate  */
/*                 table with default configuration values.                   */
/* Input(s)      : None                                                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : None                                                       */
/******************************************************************************/

VOID
Bgp4InitAggr (UINT4 u4Context)
{
    INT4                i4Index;

    for (i4Index = 0; i4Index < BGP4_MAX_AGGR_ENTRIES; i4Index++)
    {
        if ((u4Context == VCM_INVALID_VC)
            || (u4Context == BGP4_AGGRENTRY_VRFID (i4Index)))
        {
            Bgp4InitAggrEntry (&(BGP4_AGGRENTRY[i4Index]));
        }
    }
    return;
}

/******************************************************************************/
/* Function Name : Bgp4InitRtProfile                                          */
/* Description   : This function intialises variables of type tRouteProfile to*/
/*                 their default values. This fn. is called whenever a memory */
/*                 is dynamically allocated for a route                       */
/* Input(s)      : pRtProfile - pointer to RouteProfile                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : None                                                       */
/******************************************************************************/
VOID
Bgp4InitRtProfile (tRouteProfile * pRtProfile)
{
#if defined(L3VPN)|| defined(VPLSADS_WANTED)
    UINT4               u4Index;
#endif
    MEMSET(pRtProfile,0,sizeof(tRouteProfile));
    /* Initialize the Route Profile structure */
    BGP4_RT_NEXT (pRtProfile) = NULL;
    BGP4_RT_REF_COUNT (pRtProfile) = 0;
    Bgp4InitNetAddressStruct (&(BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    Bgp4InitAddrPrefixStruct (&(BGP4_RT_IMMEDIATE_NEXTHOP_INFO (pRtProfile)),
                              BGP4_INET_AFI_IPV4);
    BGP4_RT_PEER_ENTRY (pRtProfile) = NULL;
    pRtProfile->pBgpCxtNode = NULL;
    BGP4_RT_PROTOCOL (pRtProfile) = 0;
    BGP4_RT_MED (pRtProfile) = BGP4_INV_MEDLP;
    BGP4_RT_LOCAL_PREF (pRtProfile) = BGP4_INV_MEDLP;
    BGP4_RT_NH_METRIC (pRtProfile) = BGP4_INVALID_NH_METRIC;
    BGP4_RT_BGP4_INF (pRtProfile) = 0;
    BGP4_RT_BGP_MED_CHANGE (pRtProfile) = 0;
    BGP4_RT_GET_FLAGS (pRtProfile) = 0;
    BGP4_RT_GET_EXT_FLAGS (pRtProfile) = 0;
    BGP4_RT_GET_ADV_FLAG (pRtProfile) = 0;	
    pRtProfile->pMultiPathRtNext = NULL;
    BGP4_RT_SET_RT_IF_INDEX (pRtProfile, BGP4_INVALID_IFINDX);
    BGP4_RT_CHG_LIST_LINK_PREV (pRtProfile) = NULL;
    BGP4_RT_CHG_LIST_LINK_NEXT (pRtProfile) = NULL;
    BGP4_RT_NEXTHOP_LINK_PREV (pRtProfile) = NULL;
    BGP4_RT_NEXTHOP_LINK_NEXT (pRtProfile) = NULL;
    BGP4_RT_IDENT_ATTR_LINK_PREV (pRtProfile) = NULL;
    BGP4_RT_IDENT_ATTR_LINK_NEXT (pRtProfile) = NULL;
    BGP4_RT_PROTO_LINK_PREV (pRtProfile) = NULL;
    BGP4_RT_PROTO_LINK_NEXT (pRtProfile) = NULL;
#ifdef RFD_WANTED
    BGP4_RT_DAMP_HIST (pRtProfile) = NULL;
#endif
    BGP4_RT_OUT_BGP4_INFO (pRtProfile) = NULL;
#ifdef L3VPN
    BGP4_RT_VPN4_LINK_NEXT (pRtProfile) = NULL;
    BGP4_RT_VPN4_LINK_PREV (pRtProfile) = NULL;
    TMO_SLL_Init (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile));
    BGP4_RT_LABEL_CNT (pRtProfile) = 0;
    BGP4_RT_HWSTATUS (pRtProfile) = 0;
    BGP4_RT_IGP_LSP_ID (pRtProfile) = BGP4_INVALID_IGP_LABEL;
    BGP4_RT_LABEL_INFO (pRtProfile) = 0;
    BGP4_RT_IS_LEAK_ROUTE (pRtProfile) = BGP4_FALSE;
    BGP4_RT_IS_IN_PADB (pRtProfile) = BGP4_FALSE;
    for (u4Index = 0; u4Index < BGP4_VPN4_ROUTE_DISTING_SIZE; u4Index++)
    {
        BGP4_RT_ROUTE_DISTING (pRtProfile)[u4Index] = 0;
    }
#endif
#ifdef VPLSADS_WANTED
#ifdef MPLS_WANTED
    BGP4_VPLS_RT_VPLS_SPEC_INFO (pRtProfile) = NULL;
#endif
    BGP4_VPLS_RT_LEN (pRtProfile) = 0;
    BGP4_VPLS_RT_VE_ID (pRtProfile) = 0;
    BGP4_VPLS_RT_VBS (pRtProfile) = 0;
    BGP4_VPLS_RT_VBO (pRtProfile) = 0;
    BGP4_VPLS_RT_LB (pRtProfile) = 0;

    for (u4Index = 0; u4Index < BGP4_VPLS_ROUTE_DISTING_SIZE; u4Index++)
    {
        BGP4_VPLS_RT_ROUTE_DISTING (pRtProfile)[u4Index] = 0;
    }
#endif
#ifdef EVPN_WANTED
    BGP4_EVPN_RT_TYPE (pRtProfile) = 0;
    MEMSET(BGP4_EVPN_RD_DISTING(pRtProfile), 0, MAX_LEN_RD_VALUE);
    MEMSET(BGP4_EVPN_ETHSEGID(pRtProfile), 0, MAX_LEN_ETH_SEG_ID);
    BGP4_EVPN_ETHTAG(pRtProfile) = 0;
    BGP4_EVPN_MACADDR_LEN(pRtProfile) = 0;
    BGP4_EVPN_IPADDR_LEN(pRtProfile) = 0;
    BGP4_EVPN_VNID(pRtProfile) = 0;
    MEMSET(BGP4_EVPN_MACADDR(pRtProfile), 0, sizeof(tMacAddr));
    MEMSET(&(BGP4_EVPN_IPADDR(pRtProfile)) , 0, BGP4_EVPN_IP6_ADDR_SIZE);
    BGP4_EVPN_MAC_DUPLICATE (pRtProfile) = BGP4_FALSE;
#endif
}

/******************************************************************************/
/* Function Name : Bgp4InitBgp4info                                           */
/* Description   : This function intialises variables of type tBgp4Info       */
/*                 their default values. This module needs to be called from  */
/*                 the system initialisation part.                            */
/* Input(s)      : None.                                                      */
/* Output(s)     : None.                                                      */
/* Return(s)     : None                                                       */
/******************************************************************************/
VOID
Bgp4InitBgp4info (tBgp4Info * pBgpInfo)
{
    MEMSET(pBgpInfo,0,sizeof(tBgp4Info));
    TMO_SLL_Init (BGP4_INFO_ASPATH (pBgpInfo));
    TMO_SLL_Init (BGP4_MPE_SNPA_INFO_LIST (pBgpInfo));
    BGP4_INFO_REF_COUNT (pBgpInfo) = 0;
    BGP4_INFO_ATTR_FLAG (pBgpInfo) = 0;
    BGP4_INFO_ORIGIN (pBgpInfo) = 0;
    BGP4_INFO_IS_RMAP_SET(pBgpInfo) = BGP4_FALSE;
    Bgp4InitAddrPrefixStruct (&(BGP4_INFO_NEXTHOP_INFO (pBgpInfo)),
                              BGP4_INET_AFI_IPV4);
#ifdef BGP4_IPV6_WANTED
    Bgp4InitAddrPrefixStruct (&(BGP4_INFO_LINK_LOCAL_ADDR_INFO (pBgpInfo)),
                              BGP4_INET_AFI_IPV6);
    BGP4_INFO_LINK_LOCAL_PRESENT (pBgpInfo) = BGP4_FALSE;
#endif
    BGP4_INFO_RCVD_MED (pBgpInfo) = 0;
    BGP4_INFO_RCVD_LOCAL_PREF (pBgpInfo) = 0;
    BGP4_INFO_AGGREGATOR (pBgpInfo) = NULL;
    BGP4_INFO_UNKNOWN_ATTR_LEN (pBgpInfo) = 0;
    BGP4_INFO_UNKNOWN_ATTR (pBgpInfo) = NULL;
    BGP4_INFO_RCVD_PA_ENTRY (pBgpInfo) = NULL;
    BGP4_INFO_COMM_ATTR (pBgpInfo) = NULL;
    BGP4_INFO_ECOMM_ATTR (pBgpInfo) = NULL;
    BGP4_INFO_ORIG_ID (pBgpInfo) = 0;
    BGP4_INFO_CLUS_LIST_ATTR (pBgpInfo) = NULL;
    BGP4_INFO_WEIGHT (pBgpInfo) = BGP4_WEIGHT_DEFAULT_VALUE;
}

/******************************************************************************/
/* Function Name : Bgp4InitBgp4PAInfo                                         */
/* Description   : This function intialises the given Bgp4 Recevied PA info to*/
/*                 their default values. This module needs to be called from  */
/*                 the system initialisation part.                            */
/* Input(s)      : None.                                                      */
/* Output(s)     : None.                                                      */
/* Return(s)     : None                                                       */
/******************************************************************************/
VOID
Bgp4InitBgp4PAInfo (tBgp4RcvdPathAttr * pBgp4RcvdPA)
{
    TMO_SLL_Init_Node (BGP4_PA_NEXT (pBgp4RcvdPA));
    BGP4_PA_IDENT_BEST_ROUTE_FIRST (pBgp4RcvdPA) = NULL;
    BGP4_PA_IDENT_BEST_ROUTE_LAST (pBgp4RcvdPA) = NULL;
    BGP4_PA_IDENT_BEST_ROUTE_COUNT (pBgp4RcvdPA) = 0;
    BGP4_PA_IDENT_NON_BEST_ROUTE_FIRST (pBgp4RcvdPA) = NULL;
    BGP4_PA_IDENT_NON_BEST_ROUTE_LAST (pBgp4RcvdPA) = NULL;
    BGP4_PA_IDENT_NON_BEST_ROUTE_COUNT (pBgp4RcvdPA) = 0;
    BGP4_PA_BGP4INFO (pBgp4RcvdPA) = NULL;
}

/******************************************************************************/
/* Function Name : Bgp4InitBgp4AdvtPA                                         */
/* Description   : This function intialises the given Bgp4 Advertised PA Info */
/*                 with default values.                                       */
/* Input(s)      : None.                                                      */
/* Output(s)     : None.                                                      */
/* Return(s)     : None                                                       */
/******************************************************************************/
VOID
Bgp4InitBgp4AdvtPA (tBgp4AdvtPathAttr * pBgp4AdvtPA)
{
    TMO_SLL_Init_Node (BGP4_ADVT_PA_NEXT (pBgp4AdvtPA));
    TMO_SLL_Init (BGP4_ADVT_PA_ROUTE_LIST (pBgp4AdvtPA));
    BGP4_ADVT_PA_BGP4INFO (pBgp4AdvtPA) = NULL;
    BGP4_ADVT_PA_PEER (pBgp4AdvtPA) = NULL;
    BGP4_ADVT_PA_AFI (pBgp4AdvtPA) = 0;
    BGP4_ADVT_PA_SAFI (pBgp4AdvtPA) = 0;
    BGP4_ADVT_PA_SIZE (pBgp4AdvtPA) = 0;
}

/******************************************************************************/
/* Function Name : Bgp4InitLinkNode                                           */
/* Description   : This function intialises variables of type tLinknode to    */
/*                 their default values. This fn. is called whenever a memory */
/*                 is dynamically allocated for a linknode                    */
/* Input(s)      : pLinkNode - pointer to LinkNode                            */
/* Output(s)     : None.                                                      */
/* Return(s)     : None                                                       */
/******************************************************************************/
VOID
Bgp4InitLinkNode (tLinkNode * pLinkNode)
{
    TMO_SLL_Init_Node (&(pLinkNode->TSNext));
    pLinkNode->pRouteProfile = NULL;
}

/******************************************************************************/
/* Function Name : Bgp4InitASNode                                             */
/* Description   : This function intialises variables of type tAsPath to      */
/*                 their default values. This fn. is called whenever a memory */
/*                 is dynamically allocated for a As Path attribute           */
/* Input(s)      : pPath - pointer to path attribute                          */
/* Output(s)     : None.                                                      */
/* Return(s)     : None                                                       */
/******************************************************************************/
VOID
Bgp4InitASNode (tAsPath * pPath)
{
    TMO_SLL_Init_Node (&(pPath->sllNode));
    pPath->u1Type = 0;
    pPath->u1Length = 0;
}

/******************************************************************************/
/* Function Name : Bgp4InitIgpMetricEntry                                     */
/* Description   : This function intialises all the variables in the Igp      */
/*                 metric entry.                                              */
/* Input(s)      : pIgpMetric - Pointer to the Igp Metric structure.          */
/* Output(s)     : None.                                                      */
/* Return(s)     : None                                                       */
/******************************************************************************/
VOID
Bgp4InitIgpMetricEntry (tBgp4IgpMetricEntry * pIgpMetric)
{
    Bgp4InitAddrPrefixStruct (&(pIgpMetric->LinkNexthop), BGP4_INET_AFI_IPV4);
    Bgp4InitAddrPrefixStruct (&(pIgpMetric->ImmediateNextHop),
                              BGP4_INET_AFI_IPV4);
    pIgpMetric->i4IgpMetric = BGP4_DEFAULT_NEXTHOP_METRIC;
#ifdef L3VPN
    pIgpMetric->u4VrfId = BGP4_DFLT_VRFID;
    pIgpMetric->u1LspStatus = BGP4_LSP_DOWN;
    pIgpMetric->u1LspCheck = BGP4_FALSE;
    pIgpMetric->u4IgpLspId = BGP4_INVALID_IGP_LABEL;
#endif
    pIgpMetric->u4IfIndex = BGP4_INVALID_IFINDX;
    pIgpMetric->u1Action = BGP4_NEXTHOP_METRIC_STABLE;
    pIgpMetric->u1IsResolved = BGP4_FALSE;
    TMO_SLL_Init_Node (&(pIgpMetric->NextIgpMetricEntry));
    BGP4_DLL_FIRST_ROUTE ((&(pIgpMetric->NextHopIdentRoute))) = NULL;
    BGP4_DLL_LAST_ROUTE ((&(pIgpMetric->NextHopIdentRoute))) = NULL;
    BGP4_DLL_COUNT ((&(pIgpMetric->NextHopIdentRoute))) = 0;
}

/*****************************************************************************/
/* Function Name : Bgp4ClearAndReInit                                        */
/* Description   : This function clear all the configurations and            */
/*               : re-initalizes all the configurable globals to the         */
/*               : default value. The BGP Identifier value is not altered.   */
/* Input(s)      : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4ClearAndReInit (UINT4 u4Context)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tBgp4PeerEntry     *pTempPeerEntry = NULL;
    tBgpPeerGroupEntry *pPeerGroup = NULL;
    tBgpPeerGroupEntry *pTmpPeerGrp = NULL;
    UINT4               u4ActiveContext = 0;

    /*clear TCP-AO MKT table */
    Bgp4TCPAOClearMKTTable (u4Context);

    /* Delete All the Peer Entries */
    BGP_SLL_DYN_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeerEntry,
                      pTempPeerEntry, tBgp4PeerEntry *)
    {
        /* Delete the Peer Entry */
        Bgp4SnmphDeletePeerEntry (pPeerEntry);
    }
    /* Deallocate the peer-data list */
    Bgp4DeallocatePeerDataList (BGP4_PEER_DATA_PEER_PTR (u4Context));

    /* Clear and Re-Init the Aggrgate Table */
    Bgp4AggrClearAggrgateTable (u4Context);

    /* Clear the peer group */
    BGP_SLL_DYN_Scan (BGP4_PEERGROUPENTRY_HEAD (u4Context), pPeerGroup,
                      pTmpPeerGrp, tBgpPeerGroupEntry *)
    {
        TMO_SLL_Delete (BGP4_PEERGROUPENTRY_HEAD (u4Context),
                        &(pPeerGroup->NextGroup));
        MemReleaseMemBlock (gBgpNode.Bgp4PeerGroupPoolId, (UINT1 *) pPeerGroup);
    }
#ifdef EVPN_WANTED
    /* Clear the MAC Mobility Timers */
    Bgp4EvpnClearMacMobDupTimers (u4Context);
#endif

    /* Re-Init MED, LP, UPdateFilter Table */
    Bgp4InitMed (u4Context);
    Bgp4InitLp (u4Context);
    Bgp4InitFilter (u4Context);
    Bgp4InitAggr (u4Context);

    /* Re-Initializing the Globals to their Default values */
    Bgp4InitGlobals (u4Context,BGP4_FALSE);
    if (BgpGetFirstActiveContextID (&u4ActiveContext) != SNMP_SUCCESS)
    {
        Bgp4GlbGRInit ();
    }
    Bgp4InitRedistribution (u4Context);

    Bgp4GRInit (u4Context);

    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4InitAddrPrefixStruct                                   */
/* Description   : This function intialises the tAddrPrefix structure with    */
/*                 given AFI                                                  */
/* Input(s)      : AddrPrefix - which needs to be initialized                 */
/*                 u2AddrFamily - address family                              */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS.                                              */
/******************************************************************************/
INT4
Bgp4InitAddrPrefixStruct (tAddrPrefix * AddrPrefix, UINT2 u2AddrFamily)
{
    UINT4               u4Index;

    AddrPrefix->u2Afi = u2AddrFamily;
    switch (u2AddrFamily)
    {
        case BGP4_INET_AFI_IPV4:
        {
            AddrPrefix->u2AddressLen = BGP4_IPV4_PREFIX_LEN;
            break;
        }
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
            AddrPrefix->u2AddressLen = BGP4_IPV6_PREFIX_LEN;
            break;
#endif
#ifdef VPLSADS_WANTED 
            /*this parameter will be called whenever the function called with the NetAdderss 
               tAddrPrefix. For Next hop, only Ipv4 and IPv6 will be used */
        case BGP4_INET_AFI_L2VPN:
            AddrPrefix->u2AddressLen = BGP4_VPLS_NLRI_PREFIX_LEN;
            break;
#endif
        default:
            AddrPrefix->u2AddressLen = BGP4_IPV4_PREFIX_LEN;
            break;
    }
    for (u4Index = 0; u4Index < BGP4_MAX_INET_ADDRESS_LEN; u4Index++)
    {
        AddrPrefix->au1Address[u4Index] = 0;
    }

#ifdef VPLSADS_WANTED
    /*Coverty Fix */
    MEMSET (&(AddrPrefix->au1Pad), 0, sizeof (AddrPrefix->au1Pad));
#endif

    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4InitNetAddressStruct                                   */
/* Description   : This function intialises the tNetAddress structure with    */
/*                 given <AFI, SAFI>                                          */
/* Input(s)      : AddrPrefix - which needs to be initialized                 */
/*                 u2AddrFamily - address family                              */
/*                 u1SubAddrFamily - subsequent address family                */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS.                                              */
/******************************************************************************/
INT4
Bgp4InitNetAddressStruct (tNetAddress * NetAddress, UINT2 u2AddrFamily,
                          UINT1 u1SubAddrFamily)
{
    NetAddress->u2Safi = u1SubAddrFamily;
    NetAddress->u2PrefixLen = BGP4_MIN_PREFIXLEN;
    Bgp4InitAddrPrefixStruct (&(NetAddress->NetAddr), u2AddrFamily);
#ifdef L3VPN
    if (u1SubAddrFamily == BGP4_INET_SAFI_VPNV4_UNICAST)
    {
        UINT4               u4Index;
        for (u4Index = 0; u4Index < BGP4_VPN4_PREFIX_LEN; u4Index++)
        {
            NetAddress->NetAddr.au1Address[u4Index] = 0;
        }
        NetAddress->NetAddr.u2AddressLen = BGP4_VPN4_PREFIX_LEN;
    }
#endif
    return BGP4_SUCCESS;
}

#ifdef RFD_WANTED
/******************************************************************************
     Function Name   :  RfdUpdateDefaultConfigValues
     Description     :  This module updates the Rfd configurable parameters
                        with the default values.
     Input(s)        :  None
     Output(s)       :  None
     Global Variables
     Referred        :  None

    Global Variables
    Modified         : RFD_CUTOFF_THRESHOLD(BGP4_DFLT_VRFID), RFD_REUSE_THRESHOLD(BGP4_DFLT_VRFID),
                       RFD_MAX_HOLD_DOWN_TIME(BGP4_DFLT_VRFID), RFD_DECAY_HALF_LIFE_TIME(BGP4_DFLT_VRFID),
                       RFD_DECAY_TIMER_GRANULARITY(BGP4_DFLT_VRFID), RFD_REUSE_TIMER_GRANULARITY(BGP4_DFLT_VRFID),
                       RFD_REUSE_INDEX_ARRAY_SIZE(BGP4_DFLT_VRFID)
    Exceptions or OS
    Err Handling     : None
    Use of Recursion : None

    Return(s)        : None
******************************************************************************/
VOID
RfdUpdateDefaultConfigValues (UINT4 u4CxtId)
{
    RFD_CUTOFF_THRESHOLD (u4CxtId) = RFD_DEF_CUTOFF_THRESHOLD;
    RFD_REUSE_THRESHOLD (u4CxtId) = RFD_DEF_REUSE_THRESHOLD;
    RFD_MAX_HOLD_DOWN_TIME (u4CxtId) = RFD_DEF_MAX_HOLD_DOWN_TIME;
    RFD_DECAY_HALF_LIFE_TIME (u4CxtId) = RFD_DEF_DECAY_HALF_LIFE_TIME;
    RFD_DECAY_TIMER_GRANULARITY (u4CxtId) = RFD_DEF_DECAY_TIMER_GRANULARITY;
    RFD_REUSE_TIMER_GRANULARITY (u4CxtId) = RFD_DEF_REUSE_TIMER_GRANULARITY;
    RFD_REUSE_INDEX_ARRAY_SIZE (u4CxtId) = RFD_DEF_REUSE_INDEX_ARRAY_SIZE;
    return;
}
#endif
#endif /* BGP4INIT_C */
