/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4eh.c,v 1.14 2017/09/15 06:19:52 siva Exp $
 *
 * Description: These routines are part of the Error Handler Module  
 *              which process the errors that occur in the SEM.        
 *
 *******************************************************************/
#ifndef BGP4EH_C
#define BGP4EH_C

#include "bgp4com.h"

/******************************************************************************/
/* Function Name : Bgp4EhSendError                                            */
/* Description   : Forms a NOTIFICATION message using the Error code, Error   */
/*                 Subcode and Error Data, and sends this message to the      */
/*                 peer specified.                                            */
/* Input(s)      : Peer Info. to which the notification message is going to   */
/*                 be send (pPeerentry),                                      */
/*                 Error Code which needs to be filled in the message         */
/*                 (u1Errcode),                                               */
/*                 Error Subcode (u1Errsubcode),                              */
/*                 Error Data (pu1Errdata),                                   */
/*                 Length of the Error Data (u4Datalen).                      */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4EhSendError (tBgp4PeerEntry * pPeerentry, UINT1 u1Errcode,
                 UINT1 u1Errsubcode, UINT1 *pu1Errdata, UINT4 u4Datalen)
{
    UINT1              *pu1Msg = NULL;

    pu1Msg = Bgp4MhFormNotification (pPeerentry, u1Errcode, u1Errsubcode,
                                     pu1Errdata, &u4Datalen);
    if (pu1Msg == NULL)
    {
        BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Error in Forming Notification Message"
                       " with Error Code %d, Error Subcode %d.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))), u1Errcode,
                       u1Errsubcode);
        return (BGP4_FAILURE);
    }
    /* In all error case, the error message should be sent immediately */
    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                   BGP4_TRC_FLAG, BGP4_TXQ_TRC, BGP4_MOD_NAME,
                   "\tPEER %s : Sending Notification Message with"
                   " with Error Code %d, Error Subcode %d.\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry))),
                   u1Errcode, u1Errsubcode);
    Bgp4TcphSendData (pPeerentry, pu1Msg, u4Datalen);
    MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1Msg);
    return (BGP4_SUCCESS);
}
#endif /* BGP4EH_C */
