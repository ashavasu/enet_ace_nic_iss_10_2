/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4tcph.c,v 1.20 2017/09/15 06:19:53 siva Exp $
 *
 * Description: These routines interface with the underlying TCP 
 *              layer.
 *
 *******************************************************************/
#ifndef BGP4TCPH_C
#define BGP4TCPH_C
#include "bgp4com.h"

/******************************************************************************/
/* Function Name : Bgp4TcphCommand                                            */
/* Description   : Depending on the Command Parameter specified either it     */
/*                  o  opens a connection with the specified peer,            */
/*                  o  closes the connection with the specified peer,         */
/*                  o  sends any data in the existing connection.             */
/* Input(s)      : Information about the peer (pPeerentry),                   */
/*                 Command which needs to be interpreted (u1CmdPrm),          */
/*                 Buffer which needs to be sent on a SEND command (pu1Buf),  */
/*                 Length of the buffer (u4Msgsize).                          */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation is successful,               */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4TcphCommand (tBgp4PeerEntry * pPeerentry,
                 UINT1 u1CmdPrm, UINT1 *pu1Buf, UINT4 u4Msgsize)
{
    INT4                i4Retcode = BGP4_FAILURE;

    switch (u1CmdPrm)
    {
        case BGP4_TCP_OPEN:
            i4Retcode = Bgp4TcphOpenConnection (pPeerentry);
            break;

        case BGP4_TCP_CLOSE:
            i4Retcode = Bgp4TcphCloseConnection (pPeerentry);
            break;

        case BGP4_TCP_SEND:
            /* When the peer state is established, add the message to
             * the begining of the Transmission Queue, because the
             * control messages need to be sent before the update message.
             * Else if the peer state is not established, add the message
             * to the end of the queue, else may result in improper advt of
             * messages leading to FSM error in the peer. */
            if (BGP4_PEER_STATE (pPeerentry) == BGP4_ESTABLISHED_STATE)
            {
                i4Retcode = Bgp4PeerAddMsgToTxQ (pPeerentry, pu1Buf, u4Msgsize,
                                                 BGP4_PREPEND);
            }
            else
            {
                i4Retcode = Bgp4PeerAddMsgToTxQ (pPeerentry, pu1Buf, u4Msgsize,
                                                 0);
            }

            if (i4Retcode == BGP4_FAILURE)
            {
                /* Adding message to the peer TxQ fails. Release the
                 * message buffer. */
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1Buf);
            }
            break;

        default:
            break;
    }

    return (i4Retcode);
}

/******************************************************************************/
/* Function Name : Bgp4TcphSendData                                           */
/* Description   : This function interacts with the lower layer namely SLI    */
/*                 (Socket Layer Interface) to send the specified data in     */
/*                 the established connection.                                */
/* Input(s)      : Peer to which the data needs to be send (pPeerentry),      */
/*                 Buffer which needs to be send (pu1Buf),                    */
/*                 Size of buffer (u4Msgsize)                                 */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_FAILURE if it fails.                                  */
/*               : Length of message that was successfully sent out           */
/*               : BGP_EWOULDBLOCK, if the message could not be Txmitted due  */
/*                 to non-availability of transmit buffer                     */
/******************************************************************************/
INT4
Bgp4TcphSendData (tBgp4PeerEntry * pPeerentry, UINT1 *pu1Buf, UINT4 u4Msgsize)
{
    INT4                i4RetVal = 0;
    INT4                i4Connid = 0;
    INT1                i1Errno = BGP4_FAILURE;

    if (u4Msgsize > BGP4_MAX_MSG_LEN)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG,
                       BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s : Invalid Msg Length. Send FAILS!!!\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        return (BGP4_FAILURE);
    }

    if (BGP4_PEER_CONN_ID (pPeerentry) != BGP4_INV_CONN_ID)
    {
        if ((BGP4_TRC_FLAG & BGP4_DUMP_HEX_TRC) == BGP4_DUMP_HEX_TRC)
        {
            Bgp4DumpBuf (pu1Buf, u4Msgsize, BGP4_OUTGOING);
        }
        i4Connid = BGP4_PEER_CONN_ID (pPeerentry);
        i4RetVal = SEND (i4Connid, (VOID *) pu1Buf, u4Msgsize, 0);
        if (i4RetVal == BGP4_FAILURE)
        {
            i1Errno = Bgp4TcphGetSockOpt (i4Connid);
            if ((i1Errno == BGP_EWOULDBLOCK) || (i1Errno == BGP_EAGAIN) ||
                (i1Errno == BGP_EINTR))
            {
                return (BGP_EWOULDBLOCK);
            }
            else
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG,
                               BGP4_ALL_FAILURE_TRC | BGP4_TXQ_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : Send Msg to Peer FAILS!!!.\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))));
                return (BGP4_FAILURE);
            }
        }
        else
        {
            if (i4RetVal == (INT4) u4Msgsize)
            {
                /* TCP has sent out the entire BGP message successfully */
                BGP4_PEER_OUT_MSGS (pPeerentry)++;
            }
            else
            {
                /* Fragmented/No BGP message sent out by TCP */
                i1Errno = Bgp4TcphGetSockOpt (i4Connid);
                if ((i1Errno == BGP_EWOULDBLOCK) || (i1Errno == BGP_EAGAIN) ||
                    (i1Errno == BGP_EINTR))
                {
                    if (i4RetVal == 0)
                    {
                        /* No bytes sent out. */
                        return (BGP_EWOULDBLOCK);
                    }
                }
                if (i1Errno == BGP_ENOTCONN)
                {
                    /* Connection is lost. Close the socket */
                    return BGP4_FAILURE;
                }
                /* Error not EAGAIN/EWOULDBLOCK. Only Partial transmission.
                 * So need to retransmit the remaining buffer. */
                return i4RetVal;
            }
        }
    }
    /* Packet has been sent out successfully. */
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name : Bgp4TcphReceiveData                                       */
/* Description   : This function checks for incoming data in the already     */
/*                 opened connections. If any data is present then the same  */
/*                 is read and given to the Input Handler for processing.    */
/* Input(s)      : pPeer                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/* Note          : Since all the connection are opened in the Non-Blocking   */
/*                 mode, the incoming data in all connections need to be     */
/*                 checked periodically. This routine is called from the     */
/*                 BGP task main loop and will get executed for every one    */
/*                 second. This may need to be changed while porting.        */
/*****************************************************************************/
INT4
Bgp4TcphReceiveData (tBgp4PeerEntry * pPeer)
{
    UINT1              *pu1RecvMsg = NULL;
    UINT4               u4ProcessPktSize = 0;
    UINT4               u4PktCount = 0;
    INT4                i4Msglen = 0;
    INT4                i4ConnId = 0;
    INT4                i4RetVal = BGP4_SUCCESS;
    UINT4               u4Context = 0;
    INT1                i1Errno = 0;

    if (pPeer == NULL)
    {
        return BGP4_SUCCESS;
    }

    u4Context = BGP4_PEER_CXT_ID (pPeer);
    if (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_DOWN)
    {
        return BGP4_SUCCESS;
    }

    i4ConnId = BGP4_PEER_CONN_ID (pPeer);

    BGP_FD_CLR (i4ConnId, BGP4_READ_SOCK_FD_BITS (u4Context));
    if ((BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_START) ||
        (BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_AUTO_START))
    {
        for (;;)
        {
            i4ConnId = BGP4_PEER_CONN_ID (pPeer);
            if (i4ConnId == BGP4_INV_CONN_ID)
            {
                /* Peer connection has been closed while processing the
                 * received message. So no need for further processing. */
                return BGP4_FAILURE;
            }
            /* Check whether max allowed message size has been processed or
             * not. If yes, then return. Remaining message will be processed
             * in next cycle. */
            if (u4ProcessPktSize >= BGP4_MAX_BUF2PROCESS)
            {
                return BGP4_SUCCESS;
            }

            if (u4PktCount > BGP4_MAX_PEERRTS2PROCESS)
            {
                return BGP4_SUCCESS;
            }
            /* Get the buffer from peer structure to store the received route
             * list. Also get the length of the message to be received. */
            if (BGP4_PEER_RESIDUAL_BUF_DATA (pPeer) == NULL)
            {
                /* Current buffer is empty. Need to get a new complete BGP
                 * message. */
                i4Msglen = BGP4_MAX_MSG_LEN;
                pu1RecvMsg = MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);
                if (pu1RecvMsg == NULL)
                {
                    /* Allocate fails. Try later. */
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                   BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                                   BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s: Memory Allocation to receive TCP"
                                   " Data FAILED!!!\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeer),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeer))));
                    gu4BgpDebugCnt[MAX_BGP_BUFNODE_MSGS_SIZING_ID]++;
                    return BGP4_FAILURE;
                }
                MEMSET (pu1RecvMsg, 0, i4Msglen);
                BGP4_PEER_RESIDUAL_BUF_DATA (pPeer) = pu1RecvMsg;
                BGP4_PEER_RESIDUAL_CUR_MSG_LEN (pPeer) = 0;
                BGP4_PEER_RESIDUAL_BUF_OFFSET (pPeer) = 0;
            }
            else
            {
                /* Already have received partial message. Needs to get the
                 * remaining message. */
                i4Msglen = BGP4_MAX_MSG_LEN -
                    BGP4_PEER_RESIDUAL_BUF_OFFSET (pPeer);
                pu1RecvMsg = BGP4_PEER_RESIDUAL_BUF_DATA (pPeer) +
                    BGP4_PEER_RESIDUAL_BUF_OFFSET (pPeer);
            }

            /* Receive the message */
            i4Msglen = RECV (i4ConnId, pu1RecvMsg, i4Msglen, 0);
            if (i4Msglen < 0)
            {
                /* No more message available. Check if socket blocking. If
                 * blocking, then we have finished reading all the available 
                 * message. Dont free the residual buffer, because
                 * we need to read the remaining message in the next cycle. */
                i1Errno = Bgp4TcphGetSockOpt (i4ConnId);
                if ((i1Errno == BGP_EWOULDBLOCK) || (i1Errno == BGP_EAGAIN) ||
                    (i1Errno == BGP_EINTR))
                {
                    return BGP4_SUCCESS;
                }
                else
                {
                    /* Close the connection in all other error case. */
                    Bgp4IphHandleControl (pPeer, BGP4_TCP_CLOSED);
                    return BGP4_FAILURE;
                }
            }
            if (i4Msglen > 0)
            {
                /* Process the received message. Process each complete bgp
                 * packet one-by-one. Check whether only partial message is
                 * received. If so handle the message in next receive call */
                u4ProcessPktSize += i4Msglen;
                i4RetVal = Bgp4IhProcessBgpCompletePacket (pPeer, i4Msglen);
                if (i4RetVal == BGP4_FAILURE)
                {
                    return BGP4_FAILURE;
                }
                ++u4PktCount;
            }
            else                /* i4Msglen == 0 */
            {
                break;
            }
        }
        if (u4PktCount == 0)
        {
            /* Remote peer has closed the connection */
            Bgp4IphHandleControl (pPeer, BGP4_TCP_CLOSED);
        }
        i4Msglen = 0;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4TcphCheckConnIsInProgress                             */
/* Description   : This function checks whether the connection is in Progress*/
/* Input(s)      : i4Connid,which connection id to be checked and            */
/*                 the peerentry pPeerentry.                                 */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TCPH_CONN_IN_PROGRESS in case the connection         */
/*                 is in progress                                            */
/*                 BGP4_TCPH_OPEN_FAILURE, if connect fails                  */
/*                 BGP4_SUCCESS if already connected                         */
/******************************************************************************/
INT4
Bgp4TcphCheckConnIsInProgress (INT4 i4Connid, tBgp4PeerEntry * pPeerentry)
{
    INT1                i1OptVal;

    i1OptVal = Bgp4TcphGetSockOpt (i4Connid);
    if (i1OptVal == BGP_EISCONN)    /* Already connected */
    {

        BGP_FD_CLR (i4Connid,
                    BGP4_WRITE_SOCK_FD_BITS (BGP4_PEER_CXT_ID (pPeerentry)));
        BGP_FD_CLR (i4Connid,
                    BGP4_WRITE_SOCK_FD_SET (BGP4_PEER_CXT_ID (pPeerentry)));

        /* Add to conn check list */
        BGP4_PEER_CONN_ID (pPeerentry) = i4Connid;
        Bgp4TcphMaxSockId (pPeerentry, BGP4_SELECT_ADD);

        /* for select */
        BGP_FD_SET (i4Connid,
                    BGP4_READ_SOCK_FD_SET (BGP4_PEER_CXT_ID (pPeerentry)));
        /* FILL UP THE DESTINATION PORT AND LOCAL PORT AND LOCAL IP ADDRESS 
         * IN THE PEERENTRY */
        Bgp4TcphFillAddresses (pPeerentry);
        Bgp4IphHandleControl (pPeerentry, BGP4_TCP_OPENED);
        return BGP4_SUCCESS;
    }
    else if ((i1OptVal == BGP_EINPROGRESS) || (i1OptVal == BGP_EALREADY))
    {                            /* Connection in Progress */
        return (BGP4_TCPH_CONN_IN_PROGRESS);
    }

    Bgp4IphHandleControl (pPeerentry, BGP4_TCP_OPEN_FAILED);
    return (BGP4_TCPH_OPEN_FAILURE);
}

/******************************************************************************/
/* Function Name : Bgp4TcphMaxSockId()                              */
/* Description   : This function returns maximum valid socket id*/
/* Input(s)      : None             */
/* Output(s)     : None.                                                      */
/* Return(s)     : 0 if none is valid*/
/*                 value returned if found.               */
/******************************************************************************/
INT4
Bgp4TcphMaxSockId (tBgp4PeerEntry * pPeer, UINT1 u1Flag)
{
    tBgp4PeerEntry     *pTmpPeer = NULL;
    UINT4               u4BgpMaxPeers;
    INT4                i4CompSockId = 0;
    INT4                i4SockId = BGP4_PEER_CONN_ID (pPeer);
    INT4                i4Count = 0;

    u4BgpMaxPeers = FsBGPSizingParams[MAX_BGP_PEERS_SIZING_ID].
        u4PreAllocatedUnits;

    if (u1Flag == BGP4_SELECT_ADD)
    {
        for (i4Count = 0; i4Count < (INT4) (u4BgpMaxPeers *
                                            BGP4_MAX_ENTRY_PER_PEER); i4Count++)
        {
            if (BGP4_PEER_DATA_PEER_PTR (BGP4_PEER_CXT_ID (pPeer))[i4Count] ==
                pPeer)
            {
                if (BGP4_PEER_DATA_MAX_FD (BGP4_PEER_CXT_ID (pPeer)) < i4SockId)
                {
                    BGP4_PEER_DATA_MAX_FD (BGP4_PEER_CXT_ID (pPeer)) = i4SockId;
                }
                break;
            }
        }
    }
    else
    {
        if (BGP4_PEER_DATA_MAX_FD (BGP4_PEER_CXT_ID (pPeer)) == i4SockId)
        {
            for (i4Count = 0; i4Count < (INT4) (u4BgpMaxPeers *
                                                BGP4_MAX_ENTRY_PER_PEER);
                 i4Count++)
            {
                pTmpPeer =
                    BGP4_PEER_DATA_PEER_PTR (BGP4_PEER_CXT_ID (pPeer))[i4Count];
                if (pTmpPeer == NULL)
                {
                    continue;
                }

                if ((BGP4_PEER_CONN_ID (pTmpPeer) == BGP4_INV_CONN_ID) ||
                    (BGP4_PEER_CONN_ID (pTmpPeer) == i4SockId))
                {
                    continue;
                }
                if (BGP4_PEER_CONN_ID (pTmpPeer) > i4CompSockId)
                {
                    i4CompSockId = BGP4_PEER_CONN_ID (pTmpPeer);
                }
            }
            BGP4_PEER_DATA_MAX_FD (BGP4_PEER_CXT_ID (pPeer)) = i4CompSockId;
        }
    }
    return (BGP4_PEER_DATA_MAX_FD (BGP4_PEER_CXT_ID (pPeer)));
}

/******************************************************************************/
/* Function Name : Bgp4TcphWaitOnSelect()                                     */
/* Description   : This function polls on select for any input data           */
/* Input(s)      : None                                                       */
/* Output(s)     : None.                                                      */
/* Return        : BGP4_TRUE  - if more peers remains to be processed.        */
/*               : BGP4_FALSE - if no more peers available for processing.    */
/*                 BGP4_RELINQUISH - if external events requires CPU.         */
/******************************************************************************/
INT4
Bgp4TcphWaitOnSelect (UINT4 u4Context, UINT2 u2Afi)
{
    struct timeval      TimeOut;
    BGP_FD_SET_STRUCT   TempBits;
    UINT4               u4Change;
    UINT4               u4PeerCount = 0;
    UINT4               u4Iterate = BGP4_TRUE;
    PRIVATE UINT4       u4Cnt = 0;
    INT4                i4Nselect = 0;
    INT4                i4SockId;
    INT4                i4MaxFd;
    INT4                i4ListenId = BGP4_INV_CONN_ID;
    INT4                i4Status = BGP4_FALSE;

    switch (u2Afi)
    {
#ifdef BGP_TCP4_WANTED
        case BGP4_INET_AFI_IPV4:
            i4ListenId = BGP4_LOCAL_V4_LISTEN_CONN (u4Context);
            break;
#endif
#ifdef BGP_TCP6_WANTED
        case BGP4_INET_AFI_IPV6:
            i4ListenId = BGP4_LOCAL_V6_LISTEN_CONN (u4Context);
            break;
#endif
        default:
            break;
    }

    /* Process message from peers only if the global status is UP. */
    if (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_DOWN)
    {
        return BGP4_FALSE;
    }

    BGP_FD_ZERO (&TempBits);

    TimeOut.tv_sec = 0;
    TimeOut.tv_usec = 0;

    MEMCPY (BGP4_WRITE_SOCK_FD_BITS (u4Context),
            BGP4_WRITE_SOCK_FD_SET (u4Context), sizeof (BGP_FD_SET_STRUCT));
    MEMCPY (BGP4_READ_SOCK_FD_BITS (u4Context),
            BGP4_READ_SOCK_FD_SET (u4Context), sizeof (BGP_FD_SET_STRUCT));

    if (i4ListenId != BGP4_INV_CONN_ID)
    {
        i4MaxFd = ((BGP4_PEER_DATA_MAX_FD (u4Context) > i4ListenId)
                   ? BGP4_PEER_DATA_MAX_FD (u4Context) : i4ListenId);
    }
    else
    {
        i4MaxFd = i4ListenId;
    }

    i4Nselect = BGP_SOCK_SELECT (i4MaxFd + BGP4_INCREMENT_BY_ONE,
                                 BGP4_READ_SOCK_FD_BITS (u4Context),
                                 BGP4_WRITE_SOCK_FD_BITS (u4Context),
                                 (BGP_FD_SET_STRUCT *) 0, &TimeOut);

    if (i4Nselect > 0)
    {
        if (u4Cnt == 0)
        {
            /* Since we are going to start processing from the first
             * peer in the peer list, no need for any iterations */
            u4Iterate = BGP4_FALSE;
        }

        while (u4Cnt <= BGP4_PEER_DATA_MAX_PEER (u4Context))
        {
            if (u4PeerCount >= BGP4_MAX_PEERS2PROCESS)
            {
                /* Has reached the MAX peers to be processed. Check for
                 * Tic Break event. If return value is BGP4_TURE, then the
                 * environment may require the CPU. So BGP should relinquish
                 * the CPU. */
                if (Bgp4IsCpuNeeded () == BGP4_TRUE)
                {
                    /* External events requires CPU. Relinquish CPU. */
                    i4Status = BGP4_RELINQUISH;
                    break;
                }
                else
                {
                    /* Peers are pending for processing. */
                    i4Status = BGP4_TRUE;
                    break;
                }
            }

            if (BGP4_PEER_DATA_PEER_PTR (u4Context)[u4Cnt] == NULL)
            {
                u4Cnt++;
                if ((u4Cnt > BGP4_PEER_DATA_MAX_PEER (u4Context)) &&
                    (u4Iterate == BGP4_TRUE))
                {
                    u4Iterate = BGP4_FALSE;
                    u4Cnt = 0;
                }
                continue;
            }

            u4Change = 0;
            i4SockId =
                BGP4_PEER_CONN_ID (BGP4_PEER_DATA_PEER_PTR (u4Context)[u4Cnt]);
            if (i4SockId == BGP4_INV_CONN_ID)
            {
                u4Cnt++;
                if ((u4Cnt > BGP4_PEER_DATA_MAX_PEER (u4Context)) &&
                    (u4Iterate == BGP4_TRUE))
                {
                    u4Iterate = BGP4_FALSE;
                    u4Cnt = 0;
                }
                continue;
            }

            if (BGP_FD_ISSET (i4SockId, BGP4_WRITE_SOCK_FD_BITS (u4Context)))
            {
                if (BGP4_PEER_STATE (BGP4_PEER_DATA_PEER_PTR (u4Context)[u4Cnt])
                    == BGP4_ESTABLISHED_STATE)
                {
                    /* Socket is ready for writing */
                    Bgp4PeerReTxmMsg (BGP4_PEER_DATA_PEER_PTR (u4Context)
                                      [u4Cnt]);
                }
                else
                {
                    /* Open TCP Connection. */
                    Bgp4TcphOpenConnection (BGP4_PEER_DATA_PEER_PTR (u4Context)
                                            [u4Cnt]);
                }
                u4Change++;
            }
            if (BGP_FD_ISSET (i4SockId, BGP4_READ_SOCK_FD_BITS (u4Context)))
            {
                Bgp4TcphReceiveData (BGP4_PEER_DATA_PEER_PTR (u4Context)
                                     [u4Cnt]);
                u4Change++;
            }
            else
            {
                /* Nothing to Read for this peer. Check whether any residual
                 * buffer available. If then process the residual data. */
                if ((BGP4_PEER_RESIDUAL_BUF_DATA
                     (BGP4_PEER_DATA_PEER_PTR (u4Context)[u4Cnt]) != NULL) &&
                    (BGP4_PEER_RESIDUAL_BUF_OFFSET
                     (BGP4_PEER_DATA_PEER_PTR (u4Context)[u4Cnt]) != 0))
                {
                    Bgp4IhProcessBgpCompletePacket (BGP4_PEER_DATA_PEER_PTR
                                                    (u4Context)[u4Cnt], 0);
                    u4Change++;
                }
            }

            if (u4Change > 0)
            {
                u4PeerCount++;
            }

            if ((u4Cnt == BGP4_PEER_DATA_MAX_PEER (u4Context)) &&
                (u4PeerCount < BGP4_MAX_PEERS2PROCESS))
            {
                if (u4Iterate == BGP4_TRUE)
                {
                    /* Have not processed the max peers. But still some peers
                     * may have got messages to process. */
                    u4Cnt = 0;
                    u4Iterate = BGP4_FALSE;
                    continue;
                }
            }
            /* Increment the peer index count */
            u4Cnt++;
        }

        /* Check for Pending incoming connection. */
        if ((i4ListenId != BGP4_INV_CONN_ID) &&
            (BGP_FD_ISSET (i4ListenId, BGP4_READ_SOCK_FD_BITS (u4Context))))
        {
            Bgp4TcphCheckIncomingConn (u4Context, u2Afi);
        }

        if (u4PeerCount >= BGP4_MAX_PEERS2PROCESS)
        {
            /* Processed required number of peer messages. */
            return i4Status;
        }

        /* Has completed processing for this cycle. */
        u4Cnt = 0;
    }
    return (BGP4_TRUE);
}

#endif /* BGP4TCPH_C */
