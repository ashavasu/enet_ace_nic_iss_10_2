/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4tmrh.c,v 1.25 2017/09/15 06:19:53 siva Exp $
 *
 * Description: These routines are part of the Timer Handler Module
 *              which provides the timer functionality in the system
 *
 *******************************************************************/
#ifndef BGP4TMRH_C
#define BGP4TMRH_C

#include "bgp4com.h"
/******************************************************************************/
/* Function Name : Bgp4TmrhStartTimer                                      */
/* Description   : This function is called whenever a Timer specified by      */
/*                 TimerID needs to be started for the specified duration.    */
/* Input(s)      : ID of the timer which needs to be started (u1_Timerid),    */
/*                 Reference parameter which will be used on processing       */
/*                 the timer expiry (u4_Ref),                                 */
/*                 Duration of the Timer (u4Duration).                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/* Note          : This function needs to be changed while porting.           */
/******************************************************************************/
INT4
Bgp4TmrhStartTimer (UINT4 u4TimerId, VOID *pRef, UINT4 u4Duration)
{
    tBgp4PeerEntry     *pPeerentry = (tBgp4PeerEntry *) pRef;
    tRtDampHist        *pRtDampHist = (tRtDampHist *) pRef;
    tBgp4AppTimer      *pBgpTmr = NULL;

    if (u4Duration == 0)
    {
        return (BGP4_FAILURE);
    }

    if (u4TimerId == BGP4_START_TIMER)
    {
        pBgpTmr = &(pPeerentry->peerLocal.tStartTmr);
        if (pPeerentry->peerLocal.tStartTmr.u4Flag == BGP4_ACTIVE)
        {
            return (BGP4_FAILURE);
        }
        pPeerentry->peerLocal.tStartTmr.u4Flag = BGP4_ACTIVE;
    }
    else if (u4TimerId == BGP4_HOLD_TIMER)
    {
        pBgpTmr = &(pPeerentry->peerLocal.tHoldTmr);
        if (pPeerentry->peerLocal.tHoldTmr.u4Flag == BGP4_ACTIVE)
        {
            return (BGP4_FAILURE);
        }
        pPeerentry->peerLocal.tHoldTmr.u4Flag = BGP4_ACTIVE;
    }
    else if (u4TimerId == BGP4_KEEPALIVE_TIMER)
    {
        pBgpTmr = &(pPeerentry->peerLocal.tKeepAliveTmr);
        if (pPeerentry->peerLocal.tKeepAliveTmr.u4Flag == BGP4_ACTIVE)
        {
            return (BGP4_FAILURE);
        }
        pPeerentry->peerLocal.tKeepAliveTmr.u4Flag = BGP4_ACTIVE;
    }

    else if (u4TimerId == BGP4_IDLE_HOLD_TIMER)
    {
        pBgpTmr = &(pPeerentry->peerLocal.tIdleHoldTmr);
        if (pPeerentry->peerLocal.tIdleHoldTmr.u4Flag == BGP4_ACTIVE)
        {
            return (BGP4_FAILURE);
        }
        pPeerentry->peerLocal.tIdleHoldTmr.u4Flag = BGP4_ACTIVE;
    }

    /* RFC 4271 Update */
    else if (u4TimerId == BGP4_DELAY_OPEN_TIMER)
    {
        pBgpTmr = &(pPeerentry->peerLocal.tDelayOpenTmr);
        if (pPeerentry->peerLocal.tDelayOpenTmr.u4Flag == BGP4_ACTIVE)
        {
            return (BGP4_FAILURE);
        }
        pPeerentry->peerLocal.tDelayOpenTmr.u4Flag = BGP4_ACTIVE;
    }

    else if (u4TimerId == BGP4_CONNECTRETRY_TIMER)
    {
        pBgpTmr = &(pPeerentry->peerLocal.tConnRetryTmr);
        if (pPeerentry->peerLocal.tConnRetryTmr.u4Flag == BGP4_ACTIVE)
        {
            return (BGP4_FAILURE);
        }
        pPeerentry->peerLocal.tConnRetryTmr.u4Flag = BGP4_ACTIVE;
    }
    else if (u4TimerId == BGP4_MINASORIG_TIMER)
    {
        pBgpTmr = &(pPeerentry->peerLocal.tOrigTmr);
        if (pPeerentry->peerLocal.tOrigTmr.u4Flag == BGP4_ACTIVE)
        {
            return (BGP4_FAILURE);
        }
        pPeerentry->peerLocal.tOrigTmr.u4Flag = BGP4_ACTIVE;
    }
    else if (u4TimerId == BGP4_MINROUTEADV_TIMER)
    {
        pBgpTmr = &(pPeerentry->peerLocal.tRouteAdvTmr);
        if (pPeerentry->peerLocal.tRouteAdvTmr.u4Flag == BGP4_ACTIVE)
        {
            return (BGP4_FAILURE);
        }
        pPeerentry->peerLocal.tRouteAdvTmr.u4Flag = BGP4_ACTIVE;
    }
    else if (u4TimerId == BGP4_STALE_TIMER)
    {
        pBgpTmr = &(pPeerentry->peerLocal.tStaleTmr);
        if (pPeerentry->peerLocal.tStaleTmr.u4Flag == BGP4_ACTIVE)
        {
            return (BGP4_FAILURE);
        }
        pPeerentry->peerLocal.tStaleTmr.u4Flag = BGP4_ACTIVE;
    }
    else if (u4TimerId == BGP4_PEER_RESTART_TIMER)
    {
        pBgpTmr = &(pPeerentry->peerLocal.tPeerRestartTmr);
        if (pPeerentry->peerLocal.tPeerRestartTmr.u4Flag == BGP4_ACTIVE)
        {
            return (BGP4_FAILURE);
        }
        pPeerentry->peerLocal.tPeerRestartTmr.u4Flag = BGP4_ACTIVE;
    }
    else if (u4TimerId == BGP4_ROUTE_REUSE_TIMER)
    {
        pBgpTmr = &(pRtDampHist->RtReuseTmr);
        if ((pRtDampHist->RtReuseTmr.u4Flag) == BGP4_ACTIVE)
        {
            BGP4_TIMER_STOP (BGP4_TIMER_LISTID, &pBgpTmr->Tmr);
        }
        pRtDampHist->RtReuseTmr.u4Flag = BGP4_ACTIVE;
    }
    else
    {
        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                  BGP4_TRC_FLAG, BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tInvalid Timer Id to start the timer.\n");
        return BGP4_FAILURE;
    }

    pBgpTmr->Tmr.u4Data = (FS_ULONG) pRef;
    return (BGP4_TIMER_START (BGP4_TIMER_LISTID, &pBgpTmr->Tmr, u4Duration));
}

/******************************************************************************/
/* Function Name : Bgp4TmrhStopTimer                                       */
/* Description   : This function is called to stop the specified timer.       */
/* Input(s)      : ID of the timer which needs to be stopped (u1_Timerid),    */
/*                 Reference parameter which will be used on processing       */
/*                 the timer expiry (u4_Ref),                                 */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/* Note          : This function needs to be changed while porting.           */
/******************************************************************************/
INT4
Bgp4TmrhStopTimer (UINT4 u4TimerId, VOID *pRef)
{
    tBgp4PeerEntry     *pPeerentry = (tBgp4PeerEntry *) pRef;
    tRtDampHist        *pRtDampHist = (tRtDampHist *) pRef;
    tBgp4AppTimer      *pTmr = NULL;

    if (u4TimerId == BGP4_START_TIMER)
    {
        pTmr = &(pPeerentry->peerLocal.tStartTmr);
        if (pPeerentry->peerLocal.tStartTmr.u4Flag == BGP4_INACTIVE)
        {
            return (BGP4_FAILURE);
        }
        pPeerentry->peerLocal.tStartTmr.u4Flag = BGP4_INACTIVE;
    }
    else if (u4TimerId == BGP4_HOLD_TIMER)
    {
        pTmr = &(pPeerentry->peerLocal.tHoldTmr);
        if (pPeerentry->peerLocal.tHoldTmr.u4Flag == BGP4_INACTIVE)
        {
            return (BGP4_FAILURE);
        }
        pPeerentry->peerLocal.tHoldTmr.u4Flag = BGP4_INACTIVE;
    }

    else if (u4TimerId == BGP4_IDLE_HOLD_TIMER)
    {
        pTmr = &(pPeerentry->peerLocal.tIdleHoldTmr);
        if (pPeerentry->peerLocal.tIdleHoldTmr.u4Flag == BGP4_INACTIVE)
        {
            return (BGP4_FAILURE);
        }
        pPeerentry->peerLocal.tIdleHoldTmr.u4Flag = BGP4_INACTIVE;
    }
    /* RFC 4271 Update */
    else if (u4TimerId == BGP4_DELAY_OPEN_TIMER)
    {
        pTmr = &(pPeerentry->peerLocal.tDelayOpenTmr);
        if (pPeerentry->peerLocal.tDelayOpenTmr.u4Flag == BGP4_INACTIVE)
        {
            return (BGP4_FAILURE);
        }
        pPeerentry->peerLocal.tDelayOpenTmr.u4Flag = BGP4_INACTIVE;
    }

    else if (u4TimerId == BGP4_KEEPALIVE_TIMER)
    {
        pTmr = &(pPeerentry->peerLocal.tKeepAliveTmr);
        if (pPeerentry->peerLocal.tKeepAliveTmr.u4Flag == BGP4_INACTIVE)
        {
            return (BGP4_FAILURE);
        }
        pPeerentry->peerLocal.tKeepAliveTmr.u4Flag = BGP4_INACTIVE;
    }
    else if (u4TimerId == BGP4_CONNECTRETRY_TIMER)
    {
        pTmr = &(pPeerentry->peerLocal.tConnRetryTmr);
        if (pPeerentry->peerLocal.tConnRetryTmr.u4Flag == BGP4_INACTIVE)
        {
            return (BGP4_FAILURE);
        }
        pPeerentry->peerLocal.tConnRetryTmr.u4Flag = BGP4_INACTIVE;
    }
    else if (u4TimerId == BGP4_MINASORIG_TIMER)
    {
        pTmr = &(pPeerentry->peerLocal.tOrigTmr);
        if (pPeerentry->peerLocal.tOrigTmr.u4Flag == BGP4_INACTIVE)
        {
            return (BGP4_FAILURE);
        }
        pPeerentry->peerLocal.tOrigTmr.u4Flag = BGP4_INACTIVE;
    }
    else if (u4TimerId == BGP4_MINROUTEADV_TIMER)
    {
        pTmr = &(pPeerentry->peerLocal.tRouteAdvTmr);
        if (pPeerentry->peerLocal.tRouteAdvTmr.u4Flag == BGP4_INACTIVE)
        {
            return (BGP4_FAILURE);
        }
        pPeerentry->peerLocal.tRouteAdvTmr.u4Flag = BGP4_INACTIVE;
    }
    else if (u4TimerId == BGP4_STALE_TIMER)
    {
        pTmr = &(pPeerentry->peerLocal.tStaleTmr);
        if (pPeerentry->peerLocal.tStaleTmr.u4Flag == BGP4_INACTIVE)
        {
            return BGP4_FAILURE;
        }
        pPeerentry->peerLocal.tStaleTmr.u4Flag = BGP4_INACTIVE;
    }
    else if (u4TimerId == BGP4_PEER_RESTART_TIMER)
    {
        pTmr = &(pPeerentry->peerLocal.tPeerRestartTmr);
        if (pPeerentry->peerLocal.tPeerRestartTmr.u4Flag == BGP4_INACTIVE)
        {
            return BGP4_FAILURE;
        }
        pPeerentry->peerLocal.tPeerRestartTmr.u4Flag = BGP4_INACTIVE;
    }
    else if (u4TimerId == BGP4_ROUTE_REUSE_TIMER)
    {
        pTmr = &(pRtDampHist->RtReuseTmr);
        if ((pRtDampHist->RtReuseTmr.u4Flag) == BGP4_INACTIVE)
        {
            return BGP4_FAILURE;
        }
        pRtDampHist->RtReuseTmr.u4Flag = BGP4_INACTIVE;
    }
    else
    {
        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                  BGP4_TRC_FLAG, BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tInvalid Timer Id to stop the timer.\n");
        return BGP4_FAILURE;
    }

    pTmr->Tmr.u4Data = (FS_ULONG) pRef;

    BGP4_TIMER_STOP (BGP4_TIMER_LISTID, &pTmr->Tmr);

    return (BGP4_SUCCESS);
}

/******************************************************************************/
/* Function Name : Bgp4TmrhProcessTimeout                                  */
/* Description   : This function is the Timer expiry handler. Depending on    */
/*                 the Timer ID specified corresponding action will be taken  */
/* Input(s)      : ID of the timer which needs to be started (u1_Timerid),    */
/*                 Reference parameter which will be used on processing       */
/*                 the timer expiry (u4_Ref),                                 */
/* Output(s)     : None.                                                      */
/* Return(s)     : None.                                                      */
/******************************************************************************/
INT4
Bgp4TmrhProcessTimeout ()
{
    tTmrAppTimer       *pTmr = NULL;
    tBgpCxtNode        *pBgpCxtNode = NULL;
    FS_ULONG            u4Ref;
    UINT4               u4TimerId;

    for (;;)
    {
        pTmr = TmrGetNextExpiredTimer (BGP4_TIMER_LISTID);

        if (pTmr == NULL)
        {
            break;
        }
        u4Ref = (FS_ULONG) pTmr->u4Data;
        /* Call routine to process the timer expiry event */
        if (u4Ref == (UINT4) BGP4_RESTART_TIMER)
        {
            BgpSetContextId (BGP4_INVALID_CONTEXT_ID);
            Bgp4GRProcessRestartTimerExpiry ();
        }
        else if (u4Ref == (UINT4) BGP4_SELECTION_DEFERRAL_TIMER)
        {
            BgpSetContextId (BGP4_INVALID_CONTEXT_ID);
            Bgp4GRProcessSelDefTimerExpiry ();

        }
        else if (u4Ref == (UINT4) BGP4_NETWORK_MSR_TEMP_TIMER)
        {
            BgpSetContextId (BGP4_INVALID_CONTEXT_ID);
            Bgp4GRProcessNetworkMsrTempTimerExpiry ();
        }
        /* Call routine to process the timer expiry event */
        else
        {
            u4TimerId = ((tBgp4AppTimer *) pTmr)->u4TimerId;
#ifdef RFD_WANTED
            if (u4TimerId == BGP4_RFD_REUSE_TIMER)
            {
                pBgpCxtNode =
                    (tBgpCxtNode *) ((tBgp4AppTimer *) pTmr)->Tmr.u4Data;
                /* check for Rfd Reuse timer expiry */

                BgpSetContextId (pBgpCxtNode->u4ContextId);
                RfdReuseListsHandler (pBgpCxtNode->u4ContextId);
                BGP4_TIMER_START (BGP4_TIMER_LISTID, pTmr,
                                  RFD_REUSE_TIMER_GRANULARITY (pBgpCxtNode->
                                                               u4ContextId));
            }
            else if (u4TimerId == BGP4_ROUTE_REUSE_TIMER)
            {
                Bgp4RtReuseTimerExpiry (u4TimerId, u4Ref);
            }
            else
#endif /*RFD_WANTED */
            {
                /* Call routine to process the timer expiry event */
                if ((u4TimerId == BGP4_MINROUTEADV_TIMER) ||
                    (u4TimerId == BGP4_MINASORIG_TIMER))
                {
                    Bgp4AhProcessTimer (u4TimerId, u4Ref);
                }
#ifdef EVPN_WANTED
                else if (u4TimerId == BGP4_MAC_MOB_DUP_TIMER)
                {
                    Bgp4ProcessMacMobDupTimerExpiry (u4TimerId, u4Ref);
                }
#endif
                else
                {
                    Bgp4IhHandleTimer (u4TimerId, u4Ref);
                }
            }
        }
    }

    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4OneSecTimerExpiry                                      */
/* Description   : This function  is used to restart the one-second timer     */
/* Input(s)      : pointer to tTmrAppTimer structure (pTmr)                   */
/* Output(s)     : None.                                                      */
/* Return(s)     : None.                                                      */
/******************************************************************************/
INT4
Bgp4OneSecTimerExpiry (tTmrAppTimer * pTmr)
{
    UINT4               u4ElapTime = Bgp4ElapTime ();

    pTmr->u4Data = (UINT4) BGP4_ONESEC_TIMER;

    /* Explicitly start the one-shot timer again */
    BGP4_TIMER_START (BGP4_1S_TIMER_LISTID, pTmr, BGP4_ONE_SECOND);

    /* Remove the advertised routes from the peer lists */
    if (u4ElapTime % BGP4_ADVT_LIST_CLEAN_GRANULARITY == 0)
    {
        BgpLock ();
        Bgp4AhProcessAdvtList ();
        BgpUnLock ();
    }

    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4DiffTime                                             */
/* Description   : This function gives the difference between the given two   */
/*                 times.                                                     */
/* Input(s)      : End Time value (i4EndTime),                              */
/*                 Start Time Value (i4StartTime)                           */
/* Output(s)     : None.                                                      */
/* Return(s)     : Diff Time.                                                 */
/******************************************************************************/
UINT4
Bgp4DiffTime (UINT4 u4EndTime, UINT4 u4StartTime)
{
    UINT4               u4Time;

    u4Time = ((u4EndTime < u4StartTime) ?
              ((BGP4_MAX_SYS_TIME_TICKS - u4StartTime) + u4EndTime) :
              (u4EndTime - u4StartTime));
    return (u4Time);
}

/******************************************************************************/
/* Function Name : Bgp4TmrhStopAllTimers                                  */
/* Description   : This function is called to stop all timer.                 */
/* Input(s)      : pPeerentry - Peer Entry                                   */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/* Note          : This function needs to be changed while porting.           */
/******************************************************************************/
INT4
Bgp4TmrhStopAllTimers (tBgp4PeerEntry * pPeerentry)
{
    Bgp4TmrhStopTimer (BGP4_START_TIMER, (VOID *) pPeerentry);
    Bgp4TmrhStopTimer (BGP4_HOLD_TIMER, (VOID *) pPeerentry);
    Bgp4TmrhStopTimer (BGP4_IDLE_HOLD_TIMER, (VOID *) pPeerentry);
    Bgp4TmrhStopTimer (BGP4_DELAY_OPEN_TIMER, (VOID *) pPeerentry);
    Bgp4TmrhStopTimer (BGP4_KEEPALIVE_TIMER, (VOID *) pPeerentry);
    Bgp4TmrhStopTimer (BGP4_CONNECTRETRY_TIMER, (VOID *) pPeerentry);
    Bgp4TmrhStopTimer (BGP4_MINASORIG_TIMER, (VOID *) pPeerentry);
    Bgp4TmrhStopTimer (BGP4_MINROUTEADV_TIMER, (VOID *) pPeerentry);
    return (BGP4_SUCCESS);
}

/****************************************************************************
 * FUNCTION NAME : Bgp4ElapTime
 * DESCRIPTION   : This function returns the time elapsed (sec) since the BGP 
 *                 is up. 
 * INPUTS        : None
 * OUTPUTS       : None
 * RETURNS       : time elapsed in seconds 
 *****************************************************************************/
UINT4
Bgp4ElapTime ()
{
    UINT4               u4ElapTime;

    BGP4_GET_SYS_TIME (&u4ElapTime);
    return (u4ElapTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
}

#endif /* BGP4TMRH_C */
