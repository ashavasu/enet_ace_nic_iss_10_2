/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4semh.c,v 1.43 2017/09/15 06:19:53 siva Exp $
 *
 * Description: These routines are part of the SEM Handler.  
 *
 *******************************************************************/
#ifndef BGP4SEMH_C
#define BGP4SEMH_C

#include "bgp4com.h"

EXTERN UINT1
     
         gau1BGP4FsmStates[BGP4_MAX_STATES + BGP4_ONE_BYTE][BGP4_MAX_STATE_LEN];

/****************************************************************************
 * DESCRIPTION : This function checks whether the Peer is eligible for being
 *               damped.
 *
 * INPUTS      : Information about the peer whose SEM state is going to change
 *                   (pPeerentry),
 * OUTPUTS     : None.
 *
 * RETURNS     : None. 
 *
 *****************************************************************************/
VOID
Bgp4SemhDampCheck (tBgp4PeerEntry * pPeerentry)
{
    BGP4_PEER_CURRENT_IDLE_HOLD_TIME (pPeerentry) *= 2;
    if (BGP4_PEER_CURRENT_IDLE_HOLD_TIME (pPeerentry) >
        (BGP4_MAX_IDLE_HOLD_INTERVAL / 2))
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG,
                       BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                       BGP4_TXQ_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Sending CEASE Message\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) = 0;
        BGP4_PEER_ALLOW_AUTOMATIC_START (pPeerentry) =
            BGP4_PEER_AUTOMATICSTART_DISABLE;
        BGP4_PEER_DAMPED_STATUS (pPeerentry) =
            BGP4_DAMP_PEER_OSCILLATIONS_DISABLE;
    }
    else
    {
        BGP4_PEER_DAMPED_STATUS (pPeerentry) =
            BGP4_DAMP_PEER_OSCILLATIONS_ENABLE;
        Bgp4TmrhStartTimer (BGP4_IDLE_HOLD_TIMER,
                            (VOID *) pPeerentry,
                            BGP4_PEER_CURRENT_IDLE_HOLD_TIME (pPeerentry));
    }
}

/****************************************************************************
 * DESCRIPTION : This function forms an sends the OPEN message
 *
 * INPUTS      : Information about the peer whose SEM state is going to change
 *                   (pPeerentry),
 * OUTPUTS     : None.
 *
 * RETURNS     : None. 
 *
 *****************************************************************************/
VOID
Bgp4SemhSendOpenMsg (tBgp4PeerEntry * pPeerentry)
{
    UINT1              *pu1Msg = NULL;
    UINT4               u4Msgsize;

    pu1Msg = Bgp4MhFormOpen (pPeerentry, &u4Msgsize);
    if (pu1Msg == NULL)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_ALL_FAILURE_TRC | BGP4_PEER_CON_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s : Failure in Forming OPEN Message.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                        (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        Bgp4SemhProcessInvalidEvt (pPeerentry);
    }

    BGP4_PEER_IN_MSGS (pPeerentry) = 0;
    BGP4_PEER_OUT_MSGS (pPeerentry) = 0;
    BGP4_RTREF_MSG_RCVD_CTR (pPeerentry, BGP4_IPV4_UNI_INDEX) = 0;
    BGP4_RTREF_MSG_SENT_CTR (pPeerentry, BGP4_IPV4_UNI_INDEX) = 0;
#ifdef BGP4_IPV6_WANTED
    BGP4_RTREF_MSG_RCVD_CTR (pPeerentry, BGP4_IPV6_UNI_INDEX) = 0;
    BGP4_RTREF_MSG_SENT_CTR (pPeerentry, BGP4_IPV6_UNI_INDEX) = 0;
#endif

    if (Bgp4TcphCommand (pPeerentry, BGP4_TCP_SEND, pu1Msg,
                         u4Msgsize) == BGP4_FAILURE)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_ALL_FAILURE_TRC | BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Failure sending OPEN Message.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                        (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
        Bgp4SemhProcessInvalidEvt (pPeerentry);
    }
    else
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : OPEN Message is sent.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                        (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))));
    }
}

/****************************************************************************
 * DESCRIPTION : This function processes the received OPEN message and 
 *               forms KEEPALIVE messages to be sent.
 *
 * INPUTS      : Information about the peer whose SEM state is going to change
 *                   (pPeerentry),
 * OUTPUTS     : None.
 *
 * RETURNS     : None. 
 *
 *****************************************************************************/
VOID
Bgp4SemhCheckOpenMsg (tBgp4PeerEntry * pPeerentry,
                      UINT1 *pu1Buf, UINT4 u4Buflen)
{
    tBgp4PeerEntry     *pTmpPeer = NULL;
    tBgp4PeerEntry     *pDupPeer = NULL;
    UINT1              *pu1Msg = NULL;
    UINT4               u4Msgsize;

    if (Bgp4MhProcessOpen (pPeerentry, pu1Buf, u4Buflen) == BGP4_SUCCESS)
    {

        pTmpPeer = pPeerentry;
        pPeerentry = Bgp4SemhResolveCollision (pPeerentry);
        if (pPeerentry == pTmpPeer)
        {
            pu1Msg = Bgp4MhFormKeepalive (pPeerentry, &u4Msgsize);
            if (pu1Msg == NULL)
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_ALL_FAILURE_TRC | BGP4_PEER_CON_TRC |
                               BGP4_KEEP_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Failure in Forming Keep-alive Message.\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))));
            }

            if (Bgp4TcphCommand (pPeerentry, BGP4_TCP_SEND, pu1Msg, u4Msgsize)
                == BGP4_FAILURE)
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_ALL_FAILURE_TRC | BGP4_PEER_CON_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : Failure in sending Keepalive Message.\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))));
            }
            else
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Keepalive Message is sent.\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))));
            }

            if (BGP4_PEER_HOLD_TIME (pPeerentry) > 0)
            {
                Bgp4TmrhStartTimer (BGP4_KEEPALIVE_TIMER,
                                    (VOID *) pPeerentry,
                                    BGP4_PEER_NEG_KEEP_ALIVE (pPeerentry));
                Bgp4TmrhStopTimer (BGP4_HOLD_TIMER, (VOID *) pPeerentry);
                Bgp4TmrhStartTimer (BGP4_HOLD_TIMER,
                                    (VOID *) pPeerentry,
                                    BGP4_PEER_NEG_HOLD_INT (pPeerentry));
            }
            else
            {
                Bgp4TmrhStopTimer (BGP4_KEEPALIVE_TIMER, (VOID *) pPeerentry);
                Bgp4TmrhStopTimer (BGP4_HOLD_TIMER, (VOID *) pPeerentry);
            }
        }

        BGP4_CHANGE_STATE (pPeerentry, BGP4_OPENCONFIRM_STATE);
        Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_OPENCONFIRM_STATE);
        if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
            (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
        {
            BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC |
                           BGP4_PEER_CON_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Changed State - Prev State: "
                           "(%s), Curr State: (%s)\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                            (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))),
                           gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                             (pPeerentry)],
                           gau1BGP4FsmStates[BGP4_PEER_STATE (pPeerentry)]);
        }
    }
    else
    {
        if (CAPS_ADVT_SUP (BGP4_PEER_CXT_ID (pPeerentry)) == BGP4_TRUE)
        {
            Bgp4TcphCommand (pPeerentry, BGP4_TCP_CLOSE, NULL, 0);
            BGP4_CHANGE_STATE (pPeerentry, BGP4_IDLE_STATE);
            Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_IDLE_STATE);
            if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
            {

                BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG,
                               BGP4_CONTROL_PATH_TRC |
                               BGP4_PEER_CON_TRC | BGP4_EVENTS_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : Changed State - Prev State: "
                               "(%s), Curr State: (%s)\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))),
                               gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                 (pPeerentry)],
                               gau1BGP4FsmStates[BGP4_PEER_STATE (pPeerentry)]);
            }
            if ((BGP4_GET_PEER_PEND_FLAG (pPeerentry) &
                 BGP4_PEER_MP_CAP_RECV_PEND_START) !=
                BGP4_PEER_MP_CAP_RECV_PEND_START)
            {
                Bgp4TmrhStartTimer (BGP4_START_TIMER,
                                    (VOID *) pPeerentry,
                                    BGP4_PEER_START_TIME (pPeerentry));
                BGP4_PEER_START_TIME (pPeerentry) *=
                    BGP4_START_TIME_EXPONENTIAL;
                BGP4_ADJUST_START_TIME_THRESHOLD (pPeerentry);
            }
            pDupPeer = Bgp4SnmphGetDuplicatePeerEntry (pPeerentry);
            if (pDupPeer != NULL)
            {
                Bgp4SEMHProcessEvent (pDupPeer, BGP4_STOP_EVT, NULL, 0);
                Bgp4SnmphDeletePeerEntry (pDupPeer);
            }
        }
        else
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_ALL_FAILURE_TRC | BGP4_PEER_CON_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Processing of Open Message failed.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                            (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4SemhProcessInvalidEvt (pPeerentry);
        }
    }
}

/****************************************************************************
 * DESCRIPTION : This function forms the core part of the Connection Handler.
 *               This SEM Handler routine changes the state of the SEM and
 *               does proper action on the transitions. 
 *
 * INPUTS      : Information about the peer whose SEM state is going to change
 *                   (pPeerentry),
 *               New Event which may cause the SEM state change (u1Event),
 *               Buffer which may be used for taking some action (pu1Buf),
 *               Length of the buffer (u4Buflen).
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : BGP4_SUCCESS
 *
 *****************************************************************************/
INT4
Bgp4SEMHProcessEvent (tBgp4PeerEntry * pPeerentry, UINT2 u2Event,    /* FSAP2 */
                      UINT1 *pu1Buf, UINT4 u4Buflen)
{

    switch (BGP4_PEER_STATE (pPeerentry))
    {
        case BGP4_IDLE_STATE:
            Bgp4SemhIdleState (pPeerentry, u2Event);
            break;

        case BGP4_CONNECT_STATE:
            if (BGP4_PEER_RCVD_CAPS_LIST (pPeerentry) != NULL)
            {
                CapsDeletePeerCapsInfo (pPeerentry);
            }
            Bgp4SemhConnectState (pPeerentry, u2Event, pu1Buf, u4Buflen);
            break;

        case BGP4_ACTIVE_STATE:
            Bgp4SemhActiveState (pPeerentry, u2Event, pu1Buf, u4Buflen);
            break;

        case BGP4_OPENSENT_STATE:
            Bgp4SemhOpensentState (pPeerentry, u2Event, pu1Buf, u4Buflen);
            break;

        case BGP4_OPENCONFIRM_STATE:
            Bgp4SemhOpenconfirmState (pPeerentry, u2Event, pu1Buf, u4Buflen);
            break;

        case BGP4_ESTABLISHED_STATE:
            Bgp4SemhEstablishedState (pPeerentry, u2Event, pu1Buf, u4Buflen);
            break;

        default:
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG,
                           BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Invalid Peer state (%d).\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))),
                           BGP4_PEER_STATE (pPeerentry));
            break;
    }
    return BGP4_SUCCESS;
}

/****************************************************************************
 * DESCRIPTION : This function is called from the SEM handler whenever any
 *               event has been received, when the peer is in IDLE state.
 *
 * INPUTS      : Information about the peer whose SEM state is going to change
 *                   (pPeerentry),
 *               New Event which may cause the SEM state change (u2Event),
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : BGP4_SUCCESS 
 * 
 *****************************************************************************/
INT4
Bgp4SemhIdleState (tBgp4PeerEntry * pPeerentry, UINT2 u2Event)
{
    INT4                i4RetVal;

    switch (u2Event)
    {
        case BGP4_START_EVT:

            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_EVENTS_TRC |
                           BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Start Event in IDLE State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            if (BGP4_PEER_PREV_STATE (pPeerentry) == BGP4_NO_STATE)
            {
                BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) = 0;
            }
            if (BGP4_PEER_CONN_PASSIVE (pPeerentry) == BGP4_TRUE)
            {
                BGP4_CHANGE_STATE (pPeerentry, BGP4_ACTIVE_STATE);
                Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_ACTIVE_STATE);

                if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                    (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
                {
                    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG,
                                   BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                                   BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s : Transport Mode is Passive."
                                   "Changed State - Prev State: (%s) ,Curr State: (%s)\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))),
                                   gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                     (pPeerentry)],
                                   gau1BGP4FsmStates[BGP4_PEER_STATE
                                                     (pPeerentry)]);
                }
            }
            else
            {
                if (BGP4_ADMIN_STATUS (BGP4_PEER_CXT_ID (pPeerentry)) !=
                    BGP4_ADMIN_DOWN)
                {

                    BGP4_PEER_TCP_CONFIGURED_CONNECT_COUNT (pPeerentry) = 0;
                    BGP4_CHANGE_STATE (pPeerentry, BGP4_CONNECT_STATE);

                    Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_CONNECT_STATE);

                    if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                        (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
                    {
                        BGP4_TRC_ARG3 (&
                                       (BGP4_PEER_REMOTE_ADDR_INFO
                                        (pPeerentry)), BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC
                                       | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                                       "\tPEER %s : Changed State - Prev State:"
                                       "(%s), Curr State: (%s)\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeerentry),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeerentry))),
                                       gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                         (pPeerentry)],
                                       gau1BGP4FsmStates[BGP4_PEER_STATE
                                                         (pPeerentry)]);
                    }

                    Bgp4TmrhStartTimer (BGP4_CONNECTRETRY_TIMER,
                                        (VOID *) pPeerentry,
                                        BGP4_PEER_CONN_RETRY_TIME (pPeerentry));
                    i4RetVal =
                        Bgp4TcphCommand (pPeerentry, BGP4_TCP_OPEN, NULL, 0);
                    if (i4RetVal == BGP4_SUCCESS)
                    {
                        Bgp4TmrhStopTimer (BGP4_CONNECTRETRY_TIMER,
                                           (VOID *) pPeerentry);
                    }
                    else
                    {
                        if (i4RetVal == BGP4_FAILURE)
                        {
                            BGP4_CHANGE_STATE (pPeerentry, BGP4_IDLE_STATE);
                            Bgp4AddTransitionToFsmHist (pPeerentry,
                                                        BGP4_IDLE_STATE);

                            BGP4_TRC_ARG1 (&
                                           (BGP4_PEER_REMOTE_ADDR_INFO
                                            (pPeerentry)), BGP4_TRC_FLAG,
                                           BGP4_CONTROL_PATH_TRC |
                                           BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                                           "\tPEER %s : TCP Open Failed in Idle State\n",
                                           Bgp4PrintIpAddr
                                           (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));

                            if ((BGP4_PEER_STATE (pPeerentry) <=
                                 BGP4_MAX_STATES)
                                && (BGP4_PEER_PREV_STATE (pPeerentry) <=
                                    BGP4_MAX_STATES))
                            {
                                BGP4_TRC_ARG3 (&
                                               (BGP4_PEER_REMOTE_ADDR_INFO
                                                (pPeerentry)), BGP4_TRC_FLAG,
                                               BGP4_CONTROL_PATH_TRC |
                                               BGP4_PEER_CON_TRC |
                                               BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                                               "\tPEER %s : Changed State - Prev State: "
                                               "(%s), Curr State: (%s)\n",
                                               Bgp4PrintIpAddr
                                               (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))),
                                               gau1BGP4FsmStates
                                               [BGP4_PEER_PREV_STATE
                                                (pPeerentry)],
                                               gau1BGP4FsmStates[BGP4_PEER_STATE
                                                                 (pPeerentry)]);
                            }
                            Bgp4TmrhStartTimer (BGP4_START_TIMER,
                                                (VOID *) pPeerentry,
                                                BGP4_PEER_START_TIME
                                                (pPeerentry));
                            BGP4_PEER_START_TIME (pPeerentry) *=
                                BGP4_START_TIME_EXPONENTIAL;
                            BGP4_ADJUST_START_TIME_THRESHOLD (pPeerentry);
                        }
                    }
                }
            }
            break;

        case BGP4_STOP_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Stop Event in IDLE State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4TmrhStopTimer (BGP4_START_TIMER, (VOID *) pPeerentry);
            break;

        default:
            break;
    }
    return BGP4_SUCCESS;
}

/****************************************************************************
 * DESCRIPTION : This function is called from the SEM handler whenever any
 *               event has been receivied when the peer is in CONNECT state.
 *
 * INPUTS      : Information about the peer whose SEM state is going to change
 *                   (pPeerentry),
 *               New Event which may cause the SEM state change (u2Event),
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : BGP4_SUCCESS 
 *
 *****************************************************************************/
INT4
Bgp4SemhConnectState (tBgp4PeerEntry * pPeerentry, UINT2 u2Event,    /* FSAP2 */
                      UINT1 *pu1Buf, UINT4 u4Buflen)
{
    UINT1              *pu1Msg = NULL;
    UINT4               u4Msgsize;
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
    UINT1               u1Ret = BGP4_TRUE;
#endif
#endif

    switch (u2Event)
    {
        case BGP4_START_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Start Event in CONNECT "
                           "State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            break;

        case BGP4_STOP_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Stop Event in CONNECT "
                           "State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));

            Bgp4TmrhStopTimer (BGP4_START_TIMER, (VOID *) pPeerentry);
            Bgp4TmrhStopTimer (BGP4_CONNECTRETRY_TIMER, (VOID *) pPeerentry);
            Bgp4TcphCommand (pPeerentry, BGP4_TCP_CLOSE, NULL, 0);
            BGP4_CHANGE_STATE (pPeerentry, BGP4_IDLE_STATE);
            Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_IDLE_STATE);
            if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
            {
                BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG,
                               BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                               BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Changed State - Prev State: (%s), "
                               "Curr State: (%s)\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))),
                               gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                 (pPeerentry)],
                               gau1BGP4FsmStates[BGP4_PEER_STATE (pPeerentry)]);
            }
            break;

        case BGP4_TCP_CONN_OPENED_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received TCP Open Event in CONNECT "
                           "State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            if ((BGP4_PEER_DELAY_OPEN (pPeerentry) ==
                 BGP4_PEER_DELAY_OPEN_ENABLE)
                && (BGP4_PEER_DELAY_OPEN_TIME (pPeerentry) != 0))
            {
                Bgp4TmrhStopTimer (BGP4_START_TIMER, (VOID *) pPeerentry);
                Bgp4TmrhStopTimer (BGP4_CONNECTRETRY_TIMER,
                                   (VOID *) pPeerentry);
                BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Delay Open Active,interval %d secs. \n ",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))),
                               BGP4_PEER_DELAY_OPEN_TIME (pPeerentry));

                Bgp4TmrhStartTimer (BGP4_DELAY_OPEN_TIMER, (VOID *) pPeerentry,
                                    BGP4_PEER_DELAY_OPEN_TIME (pPeerentry));
            }
            else
            {
                Bgp4TmrhStopTimer (BGP4_START_TIMER, (VOID *) pPeerentry);
                Bgp4TmrhStopTimer (BGP4_CONNECTRETRY_TIMER,
                                   (VOID *) pPeerentry);
                Bgp4TmrhStopTimer (BGP4_HOLD_TIMER, (VOID *) pPeerentry);

                Bgp4TmrhStartTimer (BGP4_HOLD_TIMER, (VOID *) pPeerentry,
                                    BGP4_PEER_HOLD_TIME (pPeerentry));
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
                /*Check if BGP node is a RR */
                if (Bgp4GRCheckRestartMode (BGP4_PEER_CXT_ID (pPeerentry)) ==
                    BGP4_RESTARTING_MODE)
                {
                    if (BGP4_RR_CLIENT_CNT (BGP4_PEER_CXT_ID (pPeerentry)) > 0)
                    {
                        u1Ret = Bgp4GRSendOpenToRFLPeer (pPeerentry);
                        if (u1Ret == BGP4_TRUE)
                        {
                            pu1Msg = Bgp4MhFormOpen (pPeerentry, &u4Msgsize);
                        }
                    }
                    else if ((BGP4_EOVPLS_FLAG (BGP4_PEER_CXT_ID (pPeerentry))
                              != BGP4_TRUE)
                             &&
                             (((BGP4_PEER_NEG_ASAFI_MASK (pPeerentry)) &
                               CAP_NEG_L2VPN_VPLS_MASK) ==
                              CAP_NEG_L2VPN_VPLS_MASK))
                    {
                        /* If BGP node is a restarting node then BGP node will not send
                         * open message until it get EOVPLS from MPLS-L2VPN*/
                        /* Do not send open Message */
                        BGP4_TRC_ARG1 (&
                                       (BGP4_PEER_REMOTE_ADDR_INFO
                                        (pPeerentry)), BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC |
                                       BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                                       "\tBGP speaker is restarting and not received"
                                       " EOVPLS not sending OPEN to PEER %s :  "
                                       "\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeerentry),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeerentry))));
                    }
                    else
                    {
                        pu1Msg = Bgp4MhFormOpen (pPeerentry, &u4Msgsize);
                    }
                }
                else
                {
#endif
#endif
                    pu1Msg = Bgp4MhFormOpen (pPeerentry, &u4Msgsize);
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
                }
#endif
#endif

                if (pu1Msg == NULL)
                {
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_ALL_FAILURE_TRC | BGP4_PEER_CON_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : Failure in Forming OPEN Message "
                                   "in CONNECT State.\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))));
                    Bgp4SemhProcessInvalidEvt (pPeerentry);
                    break;
                }

                BGP4_PEER_IN_MSGS (pPeerentry) = 0;
                BGP4_PEER_OUT_MSGS (pPeerentry) = 0;
                BGP4_RTREF_MSG_RCVD_CTR (pPeerentry, BGP4_IPV4_UNI_INDEX) = 0;
                BGP4_RTREF_MSG_SENT_CTR (pPeerentry, BGP4_IPV4_UNI_INDEX) = 0;
#ifdef BGP4_IPV6_WANTED
                BGP4_RTREF_MSG_RCVD_CTR (pPeerentry, BGP4_IPV6_UNI_INDEX) = 0;
                BGP4_RTREF_MSG_SENT_CTR (pPeerentry, BGP4_IPV6_UNI_INDEX) = 0;
#endif

                if (Bgp4TcphCommand (pPeerentry, BGP4_TCP_SEND, pu1Msg,
                                     u4Msgsize) == BGP4_FAILURE)
                {
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_ALL_FAILURE_TRC | BGP4_PEER_CON_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : Failure sending OPEN Message "
                                   "in CONNECT State.\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))));
                    Bgp4SemhProcessInvalidEvt (pPeerentry);
                    break;
                }
                else
                {
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s : OPEN Message is sent.\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))));
                }
                BGP4_CHANGE_STATE (pPeerentry, BGP4_OPENSENT_STATE);
                Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_OPENSENT_STATE);
                if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                    (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
                {
                    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG,
                                   BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                                   BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s : Changed State - Prev State: (%s), "
                                   "Curr State: (%s)\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))),
                                   gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                     (pPeerentry)],
                                   gau1BGP4FsmStates[BGP4_PEER_STATE
                                                     (pPeerentry)]);
                }
            }
            break;

        case BGP4_TCP_CONN_CLOSED_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received TCP Close Event in CONNECT "
                           "State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));

            Bgp4TcphCommand (pPeerentry, BGP4_TCP_CLOSE, NULL, 0);
            Bgp4TmrhStartTimer (BGP4_START_TIMER,
                                (VOID *) pPeerentry,
                                BGP4_PEER_START_TIME (pPeerentry));
            BGP4_PEER_START_TIME (pPeerentry) *= BGP4_START_TIME_EXPONENTIAL;
            BGP4_ADJUST_START_TIME_THRESHOLD (pPeerentry);
            BGP4_CHANGE_STATE (pPeerentry, BGP4_IDLE_STATE);
            Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_IDLE_STATE);
            if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
            {
                BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG,
                               BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                               BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Changed State - Prev State: (%s), "
                               "Curr State: (%s)\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))),
                               gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                 (pPeerentry)],
                               gau1BGP4FsmStates[BGP4_PEER_STATE (pPeerentry)]);
            }
            break;

        case BGP4_TCP_CONN_OPEN_FAIL_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received TCP Open Fail Event in "
                           "CONNECT State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));

            if (pPeerentry->peerLocal.tDelayOpenTmr.u4Flag == BGP4_ACTIVE)
            {
                Bgp4TmrhStopTimer (BGP4_CONNECTRETRY_TIMER,
                                   (VOID *) pPeerentry);
                Bgp4TmrhStartTimer (BGP4_CONNECTRETRY_TIMER,
                                    (VOID *) pPeerentry,
                                    BGP4_PEER_CONN_RETRY_TIME (pPeerentry));

                Bgp4TmrhStopTimer (BGP4_DELAY_OPEN_TIMER, (VOID *) pPeerentry);
                BGP4_CHANGE_STATE (pPeerentry, BGP4_ACTIVE_STATE);
                Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_ACTIVE_STATE);
            }
            else                /* As per RFC, In the case of TCP Fail event, if Delay open timer
                                 * is not running then move to IDLE state and release the
                                 * TCP resources */
            {
                Bgp4SemhProcessInvalidEvt (pPeerentry);
            }

            if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
            {
                BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG,
                               BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                               BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Changed State - Prev State: (%s), "
                               "Curr State: (%s)\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))),
                               gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                 (pPeerentry)],
                               gau1BGP4FsmStates[BGP4_PEER_STATE (pPeerentry)]);
            }
            break;

        case BGP4_CONNECTRETRY_TMR_EXP_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received CONNECT Retry Timer Expired "
                           "Event in CONNECT State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            BGP4_PEER_TCP_CONFIGURED_CONNECT_COUNT (pPeerentry) =
                (UINT1) (BGP4_PEER_TCP_CONFIGURED_CONNECT_COUNT (pPeerentry) +
                         1);
            if (BGP4_PEER_CONN_PASSIVE (pPeerentry) != BGP4_TRUE)
            {
                if ((BGP4_PEER_TCP_CONFIGURED_CONNECT_COUNT (pPeerentry) >=
                     BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT (pPeerentry))
                    && (pPeerentry->peerConfig.u1AutomaticStop ==
                        BGP4_PEER_AUTOMATICSTOP_ENABLE))

                {
                    Bgp4TmrhStopTimer (BGP4_START_TIMER, (VOID *) pPeerentry);
                    Bgp4TmrhStopTimer (BGP4_CONNECTRETRY_TIMER,
                                       (VOID *) pPeerentry);
                    Bgp4TmrhStopTimer (BGP4_DELAY_OPEN_TIMER,
                                       (VOID *) pPeerentry);
                    Bgp4TcphCommand (pPeerentry, BGP4_TCP_CLOSE, NULL, 0);
                    BGP4_CHANGE_STATE (pPeerentry, BGP4_IDLE_STATE);

                    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s : Connect Count %d exceeded Configured "
                                   "Retry Count %d.\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))),
                                   BGP4_PEER_TCP_CONFIGURED_CONNECT_COUNT
                                   (pPeerentry),
                                   BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT
                                   (pPeerentry));

                    /* CSR - 112488 AutoStart Enabled */
                    if (BGP4_PEER_ALLOW_AUTOMATIC_START (pPeerentry) ==
                        BGP4_PEER_AUTOMATICSTART_ENABLE)
                    {
                        Bgp4TmrhStartTimer (BGP4_IDLE_HOLD_TIMER,
                                            (VOID *) pPeerentry,
                                            BGP4_PEER_IDLE_HOLD_TIME
                                            (pPeerentry));
                    }

                    Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_IDLE_STATE);
                    if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                        (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
                    {
                        BGP4_TRC_ARG3 (&
                                       (BGP4_PEER_REMOTE_ADDR_INFO
                                        (pPeerentry)), BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC
                                       | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                                       "\tPEER %s : Changed State - Prev State: (%s), "
                                       "Curr State: (%s)\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeerentry),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeerentry))),
                                       gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                         (pPeerentry)],
                                       gau1BGP4FsmStates[BGP4_PEER_STATE
                                                         (pPeerentry)]);
                    }
                }
                else
                {
                    Bgp4TcphCommand (pPeerentry, BGP4_TCP_CLOSE, NULL, 0);
                    Bgp4TmrhStartTimer (BGP4_CONNECTRETRY_TIMER,
                                        (VOID *) pPeerentry,
                                        BGP4_PEER_CONN_RETRY_TIME (pPeerentry));
                    Bgp4TmrhStopTimer (BGP4_DELAY_OPEN_TIMER,
                                       (VOID *) pPeerentry);

                    if (BGP4_PEER_TCP_CONFIGURED_CONNECT_COUNT (pPeerentry) >=
                        BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT
                        (pPeerentry))
                    {
                        BGP4_TRC_ARG3 (&
                                       (BGP4_PEER_REMOTE_ADDR_INFO
                                        (pPeerentry)), BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC |
                                       BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                                       "\tPEER %s : Connect Count %d exceeded Configured "
                                       "Retry Count %d.\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeerentry),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeerentry))),
                                       BGP4_PEER_TCP_CONFIGURED_CONNECT_COUNT
                                       (pPeerentry),
                                       BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT
                                       (pPeerentry));

                        /* CSR - 112488 AutoStart Enabled */
                        if (BGP4_PEER_ALLOW_AUTOMATIC_START (pPeerentry) ==
                            BGP4_PEER_AUTOMATICSTART_ENABLE)
                        {
                            BGP4_CHANGE_STATE (pPeerentry, BGP4_IDLE_STATE);
                            Bgp4AddTransitionToFsmHist (pPeerentry,
                                                        BGP4_IDLE_STATE);
                            Bgp4TmrhStopTimer (BGP4_DELAY_OPEN_TIMER,
                                               (VOID *) pPeerentry);
                            Bgp4TmrhStartTimer (BGP4_IDLE_HOLD_TIMER,
                                                (VOID *) pPeerentry,
                                                BGP4_PEER_IDLE_HOLD_TIME
                                                (pPeerentry));
                        }
                        else
                        {
                            BGP4_CHANGE_STATE (pPeerentry, BGP4_ACTIVE_STATE);
                            Bgp4AddTransitionToFsmHist (pPeerentry,
                                                        BGP4_ACTIVE_STATE);
                        }
                        BGP4_PEER_TCP_CONFIGURED_CONNECT_COUNT (pPeerentry) = 0;
                    }
                    else
                    {
                        BGP4_CHANGE_STATE (pPeerentry, BGP4_CONNECT_STATE);
                        Bgp4AddTransitionToFsmHist (pPeerentry,
                                                    BGP4_CONNECT_STATE);
                    }
                    if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                        (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
                    {
                        BGP4_TRC_ARG3 (&
                                       (BGP4_PEER_REMOTE_ADDR_INFO
                                        (pPeerentry)), BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC
                                       | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                                       "\tPEER %s : Changed State - Prev State: (%s), "
                                       "Curr State: (%s)\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeerentry),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeerentry))),
                                       gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                         (pPeerentry)],
                                       gau1BGP4FsmStates[BGP4_PEER_STATE
                                                         (pPeerentry)]);
                    }
                    Bgp4TcphCommand (pPeerentry, BGP4_TCP_OPEN, NULL, 0);
                }
            }
            else
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Transport Mode is Passive.\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))));

                BGP4_CHANGE_STATE (pPeerentry, BGP4_ACTIVE_STATE);
                Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_ACTIVE_STATE);
                if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                    (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
                {
                    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG,
                                   BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                                   BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s : Changed State - Prev State: (%s), "
                                   "Curr State: (%s)\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))),
                                   gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                     (pPeerentry)],
                                   gau1BGP4FsmStates[BGP4_PEER_STATE
                                                     (pPeerentry)]);
                }

                BGP4_PEER_TCP_CONFIGURED_CONNECT_COUNT (pPeerentry) = 0;
            }
            break;

        case BGP4_DELAY_OPEN_TMR_EXP_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received DelayOpen Timer Expiry Event in CONNECT "
                           "State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));

            Bgp4TmrhStopTimer (BGP4_HOLD_TIMER, (VOID *) pPeerentry);
            Bgp4TmrhStartTimer (BGP4_HOLD_TIMER, (VOID *) pPeerentry,
                                BGP4_PEER_HOLD_TIME (pPeerentry));

            /* To Form and Send the Open Message */
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
            /*Check if BGP node is a RR */
            if (Bgp4GRCheckRestartMode (BGP4_PEER_CXT_ID (pPeerentry)) ==
                BGP4_RESTARTING_MODE)
            {
                if (BGP4_RR_CLIENT_CNT (BGP4_PEER_CXT_ID (pPeerentry)) > 0)
                {
                    u1Ret = Bgp4GRSendOpenToRFLPeer (pPeerentry);
                    if (u1Ret == BGP4_TRUE)
                    {
                        Bgp4SemhSendOpenMsg (pPeerentry);
                    }
                }
                else if ((BGP4_EOVPLS_FLAG (BGP4_PEER_CXT_ID (pPeerentry)) !=
                          BGP4_TRUE)
                         &&
                         (((BGP4_PEER_NEG_ASAFI_MASK (pPeerentry)) &
                           CAP_NEG_L2VPN_VPLS_MASK) == CAP_NEG_L2VPN_VPLS_MASK))
                {
                    /* If BGP node is a restarting node then BGP node will not send
                     * open message until it get EOVPLS from MPLS-L2VPN*/
                    /* Do not send open Message */
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                                   "\tBGP speaker is restarting and not received"
                                   " EOVPLS not sending OPEN to PEER %s :  "
                                   "\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))));
                }
                else
                {
                    Bgp4SemhSendOpenMsg (pPeerentry);
                }
            }
            else
            {
#endif
#endif
                Bgp4SemhSendOpenMsg (pPeerentry);
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
            }
#endif
#endif

            BGP4_CHANGE_STATE (pPeerentry, BGP4_OPENSENT_STATE);
            Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_OPENSENT_STATE);
            if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
            {
                BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG,
                               BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                               BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Changed State - Prev State: (%s), "
                               "Curr State: (%s)\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))),
                               gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                 (pPeerentry)],
                               gau1BGP4FsmStates[BGP4_PEER_STATE (pPeerentry)]);
            }
            break;

        case BGP4_OPEN_MSG_RECD_EVT:
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received OPEN Message Received Event (%d) Event in "
                           "CONNECT State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))), u2Event);

            if ((pPeerentry->peerLocal.tDelayOpenTmr.u4Flag == BGP4_ACTIVE) ||
                (BGP4_PEER_DELAY_OPEN_TIME (pPeerentry) ==
                 BGP4_DEF_DELAYOPENINTERVAL))
            {
                Bgp4TmrhStopTimer (BGP4_DELAY_OPEN_TIMER, (VOID *) pPeerentry);

                /* To Form and Send the Open Message */
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
                /*Check if BGP node is a RR */
                if (Bgp4GRCheckRestartMode (BGP4_PEER_CXT_ID (pPeerentry)) ==
                    BGP4_RESTARTING_MODE)
                {
                    if (BGP4_RR_CLIENT_CNT (BGP4_PEER_CXT_ID (pPeerentry)) > 0)
                    {
                        u1Ret = Bgp4GRSendOpenToRFLPeer (pPeerentry);
                        if (u1Ret == BGP4_TRUE)
                        {
                            Bgp4SemhSendOpenMsg (pPeerentry);
                        }
                    }
                    else if ((BGP4_EOVPLS_FLAG (BGP4_PEER_CXT_ID (pPeerentry))
                              != BGP4_TRUE)
                             &&
                             (((BGP4_PEER_NEG_ASAFI_MASK (pPeerentry)) &
                               CAP_NEG_L2VPN_VPLS_MASK) ==
                              CAP_NEG_L2VPN_VPLS_MASK))
                    {
                        /* If BGP node is a restarting node then BGP node will not send
                         * open message until it get EOVPLS from MPLS-L2VPN*/
                        /* Do not send open Message */
                        BGP4_TRC_ARG1 (&
                                       (BGP4_PEER_REMOTE_ADDR_INFO
                                        (pPeerentry)), BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC |
                                       BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                                       "\tBGP speaker is restarting and not received"
                                       " EOVPLS not sending OPEN to PEER %s :  "
                                       "\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeerentry),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeerentry))));
                    }
                    else
                    {
                        Bgp4SemhSendOpenMsg (pPeerentry);
                    }
                }
                else
                {
#endif
#endif
                    Bgp4SemhSendOpenMsg (pPeerentry);
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
                }
#endif
#endif
                /* To Process Open message */
                Bgp4SemhCheckOpenMsg (pPeerentry, pu1Buf, u4Buflen);
            }
            break;

        case BGP4_AUTOMATIC_STOP_EVT:
        case BGP4_HOLD_TMR_EXP_EVT:
        case BGP4_KEEPALIVE_TMR_EXP_EVT:
        case BGP4_TCP_FATAL_ERROR_EVT:
        case BGP4_KEEPALIVE_MSG_RECD_EVT:
        case BGP4_UPDATE_MSG_RECD_EVT:
        case BGP4_NOTIFICATION_MSG_RECD_EVT:
        case BGP4_ROUTE_REFRESH_MSG_RECD_EVT:
        case BGP4_ERROR_EVT:
        default:
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Invalid Event (%d) Event in "
                           "CONNECT State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))), u2Event);

            if (pPeerentry->peerConfig.u1AutomaticStop ==
                BGP4_PEER_AUTOMATICSTOP_ENABLE)
            {
                BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) += 1;
                if (BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) >
                    BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT (pPeerentry))
                {
                    if (BGP4_DAMP_PEER_OSCILLATIONS (pPeerentry) ==
                        BGP4_DAMP_PEER_OSCILLATIONS_ENABLE)
                    {
                        Bgp4SemhDampCheck (pPeerentry);
                    }
                }
            }
            Bgp4SemhProcessInvalidEvt (pPeerentry);
            break;
    }

    return BGP4_SUCCESS;
}

/****************************************************************************
 * DESCRIPTION : This function is called from the SEM handler whenever any
 *               event has been receivied when the peer is in ACTIVE state.
 *
 * INPUTS      : Information about the peer whose SEM state is going to change
 *                   (pPeerentry),
 *               New Event which may cause the SEM state change (u2Event),
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : BGP4_SUCCESS 
 *
 *****************************************************************************/
INT4
Bgp4SemhActiveState (tBgp4PeerEntry * pPeerentry, UINT2 u2Event,    /* FSAP2 */
                     UINT1 *pu1Buf, UINT4 u4Buflen)
{
    UINT1              *pu1Msg = NULL;
    UINT4               u4Msgsize;
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
    UINT1               u1Ret = BGP4_TRUE;
#endif
#endif

    switch (u2Event)
    {
        case BGP4_START_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Start Event in ACTIVE "
                           "State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            break;

        case BGP4_STOP_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Stop Event in ACTIVE State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4TmrhStopTimer (BGP4_START_TIMER, (VOID *) pPeerentry);
            Bgp4TmrhStopTimer (BGP4_CONNECTRETRY_TIMER, (VOID *) pPeerentry);
            Bgp4TmrhStopTimer (BGP4_DELAY_OPEN_TIMER, (VOID *) pPeerentry);
            Bgp4TcphCommand (pPeerentry, BGP4_TCP_CLOSE, NULL, 0);

            BGP4_CHANGE_STATE (pPeerentry, BGP4_IDLE_STATE);
            Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_IDLE_STATE);
            if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
            {
                BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG,
                               BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                               BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Changed State - Prev State: (%s), "
                               "Curr State: (%s)\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))),
                               gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                 (pPeerentry)],
                               gau1BGP4FsmStates[BGP4_PEER_STATE (pPeerentry)]);
            }
            break;

        case BGP4_TCP_CONN_OPENED_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received TCP Open Event in ACTIVE "
                           "State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            if ((BGP4_PEER_DELAY_OPEN (pPeerentry) ==
                 BGP4_PEER_DELAY_OPEN_ENABLE)
                && (BGP4_PEER_DELAY_OPEN_TIME (pPeerentry) != 0))
            {
                Bgp4TmrhStopTimer (BGP4_START_TIMER, (VOID *) pPeerentry);
                Bgp4TmrhStopTimer (BGP4_CONNECTRETRY_TIMER,
                                   (VOID *) pPeerentry);
                Bgp4TmrhStartTimer (BGP4_DELAY_OPEN_TIMER, (VOID *) pPeerentry,
                                    BGP4_PEER_DELAY_OPEN_TIME (pPeerentry));
                BGP4_CHANGE_STATE (pPeerentry, BGP4_ACTIVE_STATE);
                Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_ACTIVE_STATE);
            }
            else
            {
                Bgp4TmrhStopTimer (BGP4_START_TIMER, (VOID *) pPeerentry);
                Bgp4TmrhStopTimer (BGP4_CONNECTRETRY_TIMER,
                                   (VOID *) pPeerentry);
                Bgp4TmrhStopTimer (BGP4_HOLD_TIMER, (VOID *) pPeerentry);
                Bgp4TmrhStartTimer (BGP4_HOLD_TIMER, (VOID *) pPeerentry,
                                    BGP4_PEER_HOLD_TIME (pPeerentry));
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
                /*Check if BGP node is a RR */
                if (Bgp4GRCheckRestartMode (BGP4_PEER_CXT_ID (pPeerentry)) ==
                    BGP4_RESTARTING_MODE)
                {
                    if (BGP4_RR_CLIENT_CNT (BGP4_PEER_CXT_ID (pPeerentry)) > 0)
                    {
                        u1Ret = Bgp4GRSendOpenToRFLPeer (pPeerentry);
                        if (u1Ret == BGP4_TRUE)
                        {
                            pu1Msg = Bgp4MhFormOpen (pPeerentry, &u4Msgsize);
                        }
                    }
                    else if ((BGP4_EOVPLS_FLAG (BGP4_PEER_CXT_ID (pPeerentry))
                              != BGP4_TRUE)
                             &&
                             (((BGP4_PEER_NEG_ASAFI_MASK (pPeerentry)) &
                               CAP_NEG_L2VPN_VPLS_MASK) ==
                              CAP_NEG_L2VPN_VPLS_MASK))
                    {
                        /* If BGP node is a restarting node then BGP node will not send
                         * open message until it get EOVPLS from MPLS-L2VPN*/
                        /* Do not send open Message */
                        BGP4_TRC_ARG1 (&
                                       (BGP4_PEER_REMOTE_ADDR_INFO
                                        (pPeerentry)), BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC |
                                       BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                                       "\tBGP speaker is restarting and not received"
                                       " EOVPLS not sending OPEN to PEER %s :  "
                                       "\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeerentry),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeerentry))));
                    }
                    else
                    {
                        pu1Msg = Bgp4MhFormOpen (pPeerentry, &u4Msgsize);
                    }
                }
                else
                {
#endif
#endif
                    pu1Msg = Bgp4MhFormOpen (pPeerentry, &u4Msgsize);
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
                }
#endif
#endif

                if (pu1Msg == NULL)
                {
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_ALL_FAILURE_TRC | BGP4_PEER_CON_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : Failure in Forming OPEN Message "
                                   "in Active State.\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))));
                    Bgp4SemhProcessInvalidEvt (pPeerentry);
                    break;
                }

                BGP4_PEER_IN_MSGS (pPeerentry) = 0;
                BGP4_PEER_OUT_MSGS (pPeerentry) = 0;
                BGP4_RTREF_MSG_RCVD_CTR (pPeerentry, BGP4_IPV4_UNI_INDEX) = 0;
                BGP4_RTREF_MSG_SENT_CTR (pPeerentry, BGP4_IPV4_UNI_INDEX) = 0;
#ifdef BGP4_IPV6_WANTED
                BGP4_RTREF_MSG_RCVD_CTR (pPeerentry, BGP4_IPV6_UNI_INDEX) = 0;
                BGP4_RTREF_MSG_SENT_CTR (pPeerentry, BGP4_IPV6_UNI_INDEX) = 0;
#endif

                if (Bgp4TcphCommand (pPeerentry, BGP4_TCP_SEND, pu1Msg,
                                     u4Msgsize) == BGP4_FAILURE)
                {
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_ALL_FAILURE_TRC | BGP4_PEER_CON_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : Failure sending OPEN Message "
                                   "in CONNECT State.\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))));
                    Bgp4SemhProcessInvalidEvt (pPeerentry);
                    break;
                }
                else
                {
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s : OPEN Message is sent.\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))));
                }
                BGP4_CHANGE_STATE (pPeerentry, BGP4_OPENSENT_STATE);
                Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_OPENSENT_STATE);
            }

            if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
            {
                BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG,
                               BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                               BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Changed State - Prev State: (%s), "
                               "Curr State: (%s)\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))),
                               gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                 (pPeerentry)],
                               gau1BGP4FsmStates[BGP4_PEER_STATE (pPeerentry)]);
            }
            break;

        case BGP4_TCP_CONN_OPEN_FAIL_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received TCP Open Fail Event in ACTIVE "
                           "State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4TmrhStopTimer (BGP4_CONNECTRETRY_TIMER, (VOID *) pPeerentry);
            Bgp4TmrhStopTimer (BGP4_DELAY_OPEN_TIMER, (VOID *) pPeerentry);

            if (BGP4_PEER_ALLOW_AUTOMATIC_STOP (pPeerentry) ==
                BGP4_PEER_AUTOMATICSTOP_ENABLE)
            {
                BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) += 1;
                if (BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) >
                    BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT (pPeerentry))
                {
                    if (BGP4_DAMP_PEER_OSCILLATIONS (pPeerentry) ==
                        BGP4_DAMP_PEER_OSCILLATIONS_ENABLE)
                    {
                        Bgp4SemhDampCheck (pPeerentry);
                    }
                }
            }
            Bgp4SemhProcessInvalidEvt (pPeerentry);
            break;

        case BGP4_CONNECTRETRY_TMR_EXP_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Connect Retry Timer Expire "
                           "Event in ACTIVE State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4TcphCommand (pPeerentry, BGP4_TCP_CLOSE, NULL, 0);
            Bgp4TmrhStopTimer (BGP4_CONNECTRETRY_TIMER, (VOID *) pPeerentry);
            Bgp4TmrhStartTimer (BGP4_CONNECTRETRY_TIMER,
                                (VOID *) pPeerentry,
                                BGP4_PEER_CONN_RETRY_TIME (pPeerentry));
            BGP4_CHANGE_STATE (pPeerentry, BGP4_CONNECT_STATE);
            Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_CONNECT_STATE);
            if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
            {
                BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG,
                               BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                               BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Changed State - Prev State: (%s), "
                               "Curr State: (%s)\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))),
                               gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                 (pPeerentry)],
                               gau1BGP4FsmStates[BGP4_PEER_STATE (pPeerentry)]);
            }
            Bgp4TcphCommand (pPeerentry, BGP4_TCP_OPEN, NULL, 0);
            break;

        case BGP4_DELAY_OPEN_TMR_EXP_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received DelayOpen Timer Expiry Event in ACTIVE "
                           "State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));

            Bgp4TmrhStopTimer (BGP4_CONNECTRETRY_TIMER, (VOID *) pPeerentry);
            Bgp4TmrhStopTimer (BGP4_DELAY_OPEN_TIMER, (VOID *) pPeerentry);
            Bgp4TmrhStopTimer (BGP4_HOLD_TIMER, (VOID *) pPeerentry);
            Bgp4TmrhStartTimer (BGP4_HOLD_TIMER, (VOID *) pPeerentry,
                                BGP4_PEER_HOLD_TIME (pPeerentry));

            /* To Form and Send the Open Message */
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
            /*Check if BGP node is a RR */
            if (Bgp4GRCheckRestartMode (BGP4_PEER_CXT_ID (pPeerentry)) ==
                BGP4_RESTARTING_MODE)
            {
                if (BGP4_RR_CLIENT_CNT (BGP4_PEER_CXT_ID (pPeerentry)) > 0)
                {
                    u1Ret = Bgp4GRSendOpenToRFLPeer (pPeerentry);
                    if (u1Ret == BGP4_TRUE)
                    {
                        Bgp4SemhSendOpenMsg (pPeerentry);
                    }
                }
                else if ((BGP4_EOVPLS_FLAG (BGP4_PEER_CXT_ID (pPeerentry)) !=
                          BGP4_TRUE)
                         &&
                         (((BGP4_PEER_NEG_ASAFI_MASK (pPeerentry)) &
                           CAP_NEG_L2VPN_VPLS_MASK) == CAP_NEG_L2VPN_VPLS_MASK))
                {
                    /* If BGP node is a restarting node then BGP node will not send
                     * open message until it get EOVPLS from MPLS-L2VPN*/
                    /* Do not send open Message */
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                                   "\tBGP speaker is restarting and not received"
                                   " EOVPLS not sending OPEN to PEER %s :  "
                                   "\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))));
                }
                else
                {
                    Bgp4SemhSendOpenMsg (pPeerentry);
                }
            }
            else
            {
#endif
#endif
                Bgp4SemhSendOpenMsg (pPeerentry);
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
            }
#endif
#endif

            BGP4_CHANGE_STATE (pPeerentry, BGP4_OPENSENT_STATE);
            Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_OPENSENT_STATE);
            if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
            {
                BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG,
                               BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                               BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Changed State - Prev State: (%s), "
                               "Curr State: (%s)\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))),
                               gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                 (pPeerentry)],
                               gau1BGP4FsmStates[BGP4_PEER_STATE (pPeerentry)]);
            }
            break;

        case BGP4_OPEN_MSG_RECD_EVT:
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received OPEN Message Received Event (%d) Event in "
                           "CONNECT State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))), u2Event);

            if ((pPeerentry->peerLocal.tDelayOpenTmr.u4Flag == BGP4_ACTIVE) ||
                ((BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT (pPeerentry) ==
                  0)
                 && (Bgp4GRCheckRestartMode (BGP4_PEER_CXT_ID (pPeerentry)) ==
                     BGP4_RECEIVING_MODE)))
            {
                Bgp4TmrhStopTimer (BGP4_CONNECTRETRY_TIMER,
                                   (VOID *) pPeerentry);
                Bgp4TmrhStopTimer (BGP4_DELAY_OPEN_TIMER, (VOID *) pPeerentry);

                /* To Form and Send the Open Message */
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
                /*Check if BGP node is a RR */
                if (Bgp4GRCheckRestartMode (BGP4_PEER_CXT_ID (pPeerentry)) ==
                    BGP4_RESTARTING_MODE)
                {
                    if (BGP4_RR_CLIENT_CNT (BGP4_PEER_CXT_ID (pPeerentry)) > 0)
                    {
                        u1Ret = Bgp4GRSendOpenToRFLPeer (pPeerentry);
                        if (u1Ret == BGP4_TRUE)
                        {
                            Bgp4SemhSendOpenMsg (pPeerentry);
                        }
                    }
                    else if ((BGP4_EOVPLS_FLAG (BGP4_PEER_CXT_ID (pPeerentry))
                              != BGP4_TRUE)
                             &&
                             (((BGP4_PEER_NEG_ASAFI_MASK (pPeerentry)) &
                               CAP_NEG_L2VPN_VPLS_MASK) ==
                              CAP_NEG_L2VPN_VPLS_MASK))
                    {
                        /* If BGP node is a restarting node then BGP node will not send
                         * open message until it get EOVPLS from MPLS-L2VPN*/
                        /* Do not send open Message */
                        BGP4_TRC_ARG1 (&
                                       (BGP4_PEER_REMOTE_ADDR_INFO
                                        (pPeerentry)), BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC |
                                       BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                                       "\tBGP speaker is restarting and not received"
                                       " EOVPLS not sending OPEN to PEER %s :  "
                                       "\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeerentry),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeerentry))));
                    }
                    else
                    {
                        Bgp4SemhSendOpenMsg (pPeerentry);
                    }
                }
                else
                {
#endif
#endif
                    Bgp4SemhSendOpenMsg (pPeerentry);
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
                }
#endif
#endif
                /* To Process Open message */
                Bgp4SemhCheckOpenMsg (pPeerentry, pu1Buf, u4Buflen);

            }
            break;

        case BGP4_TCP_CONN_CLOSED_EVT:
        case BGP4_TCP_FATAL_ERROR_EVT:
        case BGP4_HOLD_TMR_EXP_EVT:
        case BGP4_KEEPALIVE_TMR_EXP_EVT:
        case BGP4_KEEPALIVE_MSG_RECD_EVT:
        case BGP4_UPDATE_MSG_RECD_EVT:
        case BGP4_NOTIFICATION_MSG_RECD_EVT:
        case BGP4_ROUTE_REFRESH_MSG_RECD_EVT:
        case BGP4_ERROR_EVT:
        default:
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Invalid Event (%d) in ACTIVE "
                           "State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))), u2Event);
            if (u2Event == BGP4_NOTIFICATION_MSG_RECD_EVT)
            {
                if (pPeerentry->peerLocal.tDelayOpenTmr.u4Flag == BGP4_ACTIVE)
                {
                    Bgp4TmrhStopTimer (BGP4_DELAY_OPEN_TIMER,
                                       (VOID *) pPeerentry);
                }
            }

            if (BGP4_PEER_ALLOW_AUTOMATIC_STOP (pPeerentry) ==
                BGP4_PEER_AUTOMATICSTOP_ENABLE)
            {
                BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) += 1;
                if (BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) >
                    BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT (pPeerentry))
                {
                    if (BGP4_DAMP_PEER_OSCILLATIONS (pPeerentry) ==
                        BGP4_DAMP_PEER_OSCILLATIONS_ENABLE)
                    {
                        Bgp4SemhDampCheck (pPeerentry);
                    }
                }
            }

            Bgp4SemhProcessInvalidEvt (pPeerentry);
            break;
    }

    return BGP4_SUCCESS;
}

/****************************************************************************
 * DESCRIPTION : This function is called from the SEM handler whenever any
 *               event has been receivied when the peer is in OPEN_SENT state.
 *
 * INPUTS      : Information about the peer whose SEM state is going to change
 *                   (pPeerentry),
 *               New Event which may cause the SEM state change (u1Event),
 *               Buffer which may be used for taking some action (pu1Buf),
 *               Length of the buffer (u4Buflen).
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : BGP4_SUCCESS.
 *
 *****************************************************************************/
INT4
Bgp4SemhOpensentState (tBgp4PeerEntry * pPeerentry,
                       UINT2 u2Event, UINT1 *pu1Buf, UINT4 u4Buflen)
{
    tBgp4PeerEntry     *pTmpPeer = NULL;
    tBgp4PeerEntry     *pDupPeer = NULL;
    UINT1              *pu1Msg = NULL;
    UINT4               u4Msgsize;
    UINT1               u1ErrorCode;
    UINT1               u1ErrSubCode;
    UINT1               u1ErrorData;

    switch (u2Event)
    {
        case BGP4_START_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Start Event in OPENSENT "
                           "State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            break;

        case BGP4_TCP_CONN_OPEN_FAIL_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received TCP Open Fail Event in "
                           "OPENSENT State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            break;

        case BGP4_STOP_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Stop Event in OPENSENT "
                           "State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4TmrhStopTimer (BGP4_HOLD_TIMER, (VOID *) pPeerentry);
            Bgp4TmrhStopTimer (BGP4_CONNECTRETRY_TIMER, (VOID *) pPeerentry);
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                           BGP4_TXQ_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Sending CEASE Message\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4EhSendError (pPeerentry, BGP4_CEASE, 0, NULL, 0);
            Bgp4TcphCommand (pPeerentry, BGP4_TCP_CLOSE, NULL, 0);
            BGP4_CHANGE_STATE (pPeerentry, BGP4_IDLE_STATE);
            Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_IDLE_STATE);
            if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
            {
                BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG,
                               BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                               BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Changed State - Prev State: (%s), "
                               "Curr State: (%s)\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))),
                               gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                 (pPeerentry)],
                               gau1BGP4FsmStates[BGP4_PEER_STATE (pPeerentry)]);
            }
            break;
        case BGP4_AUTOMATIC_STOP_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Automatic Stop Event in OPENSENT "
                           "State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            if (pPeerentry->peerConfig.u1AutomaticStop ==
                BGP4_PEER_AUTOMATICSTOP_ENABLE)
            {

                BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) += 1;
                if (BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) >
                    BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT (pPeerentry))
                {
                    if (BGP4_DAMP_PEER_OSCILLATIONS (pPeerentry) ==
                        BGP4_DAMP_PEER_OSCILLATIONS_ENABLE)
                    {
                        Bgp4SemhDampCheck (pPeerentry);
                    }
                }
            }
            Bgp4SemhProcessInvalidEvt (pPeerentry);
            break;

        case BGP4_TCP_CONN_CLOSED_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received TCP Close Event in OPENSENT "
                           "State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4TmrhStopTimer (BGP4_HOLD_TIMER, (VOID *) pPeerentry);
            Bgp4TmrhStopTimer (BGP4_CONNECTRETRY_TIMER, (VOID *) pPeerentry);
            Bgp4TcphCommand (pPeerentry, BGP4_TCP_CLOSE, NULL, 0);
            Bgp4TmrhStartTimer (BGP4_CONNECTRETRY_TIMER,
                                (VOID *) pPeerentry,
                                BGP4_PEER_CONN_RETRY_TIME (pPeerentry));
            BGP4_CHANGE_STATE (pPeerentry, BGP4_ACTIVE_STATE);
            Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_ACTIVE_STATE);
            if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
            {
                BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG,
                               BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                               BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Changed State - Prev State: (%s), "
                               "Curr State: (%s)\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))),
                               gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                 (pPeerentry)],
                               gau1BGP4FsmStates[BGP4_PEER_STATE (pPeerentry)]);
            }
            break;

        case BGP4_OPEN_MSG_RECD_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_EVENTS_TRC | BGP4_RX_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Received Open Message in "
                           "OPENSENT State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));

            if (Bgp4MhProcessOpen (pPeerentry, pu1Buf, u4Buflen) ==
                BGP4_SUCCESS)
            {

                pTmpPeer = pPeerentry;
                pPeerentry = Bgp4SemhResolveCollision (pPeerentry);
                if (pPeerentry == pTmpPeer)
                {
                    Bgp4TmrhStartTimer (BGP4_KEEPALIVE_TIMER,
                                        (VOID *) pPeerentry,
                                        BGP4_PEER_NEG_KEEP_ALIVE (pPeerentry));
                    Bgp4TmrhStopTimer (BGP4_HOLD_TIMER, (VOID *) pPeerentry);
                    Bgp4TmrhStopTimer (BGP4_DELAY_OPEN_TIMER,
                                       (VOID *) pPeerentry);
                    Bgp4TmrhStartTimer (BGP4_HOLD_TIMER, (VOID *) pPeerentry,
                                        BGP4_PEER_NEG_HOLD_INT (pPeerentry));
                    pu1Msg = Bgp4MhFormKeepalive (pPeerentry, &u4Msgsize);
                    if (pu1Msg == NULL)
                    {
                        BGP4_TRC_ARG1 (&
                                       (BGP4_PEER_REMOTE_ADDR_INFO
                                        (pPeerentry)), BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC |
                                       BGP4_ALL_FAILURE_TRC | BGP4_PEER_CON_TRC,
                                       BGP4_MOD_NAME,
                                       "\tPEER %s : Failure in Forming Keep-"
                                       "alive Message in OPENSENT State.\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeerentry),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeerentry))));
                        break;
                    }
                    if (Bgp4TcphCommand
                        (pPeerentry, BGP4_TCP_SEND, pu1Msg,
                         u4Msgsize) == BGP4_FAILURE)
                    {
                        BGP4_TRC_ARG1 (&
                                       (BGP4_PEER_REMOTE_ADDR_INFO
                                        (pPeerentry)), BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC |
                                       BGP4_ALL_FAILURE_TRC | BGP4_PEER_CON_TRC,
                                       BGP4_MOD_NAME,
                                       "\tPEER %s : Failure in sending Keepalive Message.\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeerentry),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeerentry))));
                    }
                    else
                    {
                        BGP4_TRC_ARG1 (&
                                       (BGP4_PEER_REMOTE_ADDR_INFO
                                        (pPeerentry)), BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC |
                                       BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                                       "\tPEER %s : Keepalive Message is sent.\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeerentry),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeerentry))));
                    }
                    BGP4_CHANGE_STATE (pPeerentry, BGP4_OPENCONFIRM_STATE);
                    Bgp4AddTransitionToFsmHist (pPeerentry,
                                                BGP4_OPENCONFIRM_STATE);

                    if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                        (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
                    {
                        BGP4_TRC_ARG3 (&
                                       (BGP4_PEER_REMOTE_ADDR_INFO
                                        (pPeerentry)), BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC | BGP4_EVENTS_TRC |
                                       BGP4_PEER_CON_TRC | BGP4_EVENTS_TRC,
                                       BGP4_MOD_NAME,
                                       "\tPEER %s : Changed State - Prev State: "
                                       "(%s), Curr State: (%s)\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeerentry),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeerentry))),
                                       gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                         (pPeerentry)],
                                       gau1BGP4FsmStates[BGP4_PEER_STATE
                                                         (pPeerentry)]);
                    }
                }
                break;
            }
            else
            {
                if (CAPS_ADVT_SUP (BGP4_PEER_CXT_ID (pPeerentry)) == BGP4_TRUE)
                {
                    Bgp4TcphCommand (pPeerentry, BGP4_TCP_CLOSE, NULL, 0);
                    BGP4_CHANGE_STATE (pPeerentry, BGP4_IDLE_STATE);
                    Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_IDLE_STATE);
                    if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                        (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
                    {

                        BGP4_TRC_ARG3 (&
                                       (BGP4_PEER_REMOTE_ADDR_INFO
                                        (pPeerentry)), BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC |
                                       BGP4_PEER_CON_TRC | BGP4_EVENTS_TRC,
                                       BGP4_MOD_NAME,
                                       "\tPEER %s : Changed State - Prev State: "
                                       "(%s), Curr State: (%s)\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeerentry),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeerentry))),
                                       gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                         (pPeerentry)],
                                       gau1BGP4FsmStates[BGP4_PEER_STATE
                                                         (pPeerentry)]);
                    }
                    if ((BGP4_GET_PEER_PEND_FLAG (pPeerentry) &
                         BGP4_PEER_MP_CAP_RECV_PEND_START) !=
                        BGP4_PEER_MP_CAP_RECV_PEND_START)
                    {
                        Bgp4TmrhStartTimer (BGP4_START_TIMER,
                                            (VOID *) pPeerentry,
                                            BGP4_PEER_START_TIME (pPeerentry));
                        BGP4_PEER_START_TIME (pPeerentry) *=
                            BGP4_START_TIME_EXPONENTIAL;
                        BGP4_ADJUST_START_TIME_THRESHOLD (pPeerentry);
                    }
                    pDupPeer = Bgp4SnmphGetDuplicatePeerEntry (pPeerentry);
                    if (pDupPeer != NULL)
                    {
                        Bgp4SEMHProcessEvent (pDupPeer, BGP4_STOP_EVT, NULL, 0);
                        Bgp4SnmphDeletePeerEntry (pDupPeer);
                    }
                }
                else
                {
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_ALL_FAILURE_TRC | BGP4_PEER_CON_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : Processing of Open Message failed.\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))));
                    Bgp4SemhProcessInvalidEvt (pPeerentry);
                }
            }
            break;

        case BGP4_HOLD_TMR_EXP_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_KEEP_TRC
                           | BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Hold Timer Expired Event in "
                           "OPENSENT State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4EhSendError (pPeerentry, BGP4_HOLD_TMR_EXPIRED_ERR, 0, NULL, 0);
            Bgp4TmrhStopTimer (BGP4_CONNECTRETRY_TIMER, (VOID *) pPeerentry);
            if (pPeerentry->peerConfig.u1AutomaticStop ==
                BGP4_PEER_AUTOMATICSTOP_ENABLE)
            {
                BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) += 1;
                if (BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) >
                    BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT (pPeerentry))
                {
                    if (BGP4_DAMP_PEER_OSCILLATIONS (pPeerentry) ==
                        BGP4_DAMP_PEER_OSCILLATIONS_ENABLE)
                    {
                        Bgp4SemhDampCheck (pPeerentry);
                    }
                }
            }
            Bgp4SemhProcessInvalidEvt (pPeerentry);
            break;

        case BGP4_TCP_FATAL_ERROR_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received TCP Fatal Error Event in "
                           "OPENSENT State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4SemhProcessInvalidEvt (pPeerentry);
            break;

        case BGP4_NOTIFICATION_MSG_RECD_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_EVENTS_TRC | BGP4_RX_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Received Notification Message in "
                           "OPENSENT State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));

            Bgp4MhProcessNotification (pPeerentry, pu1Buf, u4Buflen);
            if (CAPS_ADVT_SUP (BGP4_PEER_CXT_ID (pPeerentry)) == BGP4_TRUE)
            {
                u1ErrorCode = *(pu1Buf);
                if (u1ErrorCode == BGP4_OPEN_MSG_ERR)
                {
                    u1ErrSubCode = *(pu1Buf + BGP_ERROR_CODE_LENGTH);
                    u1ErrorData = *(pu1Buf + BGP_ERROR_CODE_LENGTH +
                                    BGP_ERROR_SUBCODE_LENGTH);
                    if (((u1ErrSubCode == UNSUPPORTED_OPTIONAL_PARAMETER) &&
                         (u1ErrorData == CAPABILITIES_OPTIONAL_PARAMETER)) ||
                        (u1ErrSubCode == UNSUPPORTED_CAPABILITY))
                    {
                        Bgp4CapsProcessNotification (pu1Buf, pPeerentry);
                        break;
                    }
                }
            }
            Bgp4SemhProcessInvalidEvt (pPeerentry);
            break;

        case BGP4_ROUTE_REFRESH_MSG_RECD_EVT:
        default:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                           "\tPEER %s - FSM Error in OPENSENT State\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4EhSendError (pPeerentry, BGP4_FSM_ERR, 0, NULL, 0);
            if (pPeerentry->peerConfig.u1AutomaticStop ==
                BGP4_PEER_AUTOMATICSTOP_ENABLE)
            {
                BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) += 1;
                if (BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) >
                    BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT (pPeerentry))
                {
                    if (BGP4_DAMP_PEER_OSCILLATIONS (pPeerentry) ==
                        BGP4_DAMP_PEER_OSCILLATIONS_ENABLE)
                    {
                        Bgp4SemhDampCheck (pPeerentry);
                    }
                }
            }
            Bgp4SemhProcessInvalidEvt (pPeerentry);
            break;
    }
    return BGP4_SUCCESS;
}

/****************************************************************************
 * DESCRIPTION : This function is called from the SEM handler whenever any
 *               event has been receivied when the peer is in OPEN_CONFIRM 
 *               state.
 *
 * INPUTS      : Information about the peer whose SEM state is going to change
 *                   (pPeerentry),
 *               New Event which may cause the SEM state change (u1Event),
 *               Buffer which may be used for taking some action (pu1Buf),
 *               Length of the buffer (u4Buflen).
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : BGP4_SUCCESS 
 *
 *****************************************************************************/
INT4
Bgp4SemhOpenconfirmState (tBgp4PeerEntry * pPeerentry,
                          UINT2 u2Event, UINT1 *pu1Buf, UINT4 u4Buflen)
{
    UINT1              *pu1Msg = NULL;
    UINT4               u4Msgsize;
    UINT4               u4NegOrfCapMask = 0;
    UINT4               u4AfiSafiMask = 0;
    UINT4               u4VrfId = 0;
    UINT1               u1ErrorCode;
    UINT1               u1ErrSubCode;
    UINT1               u1ErrorData;

    u4VrfId = BGP4_PEER_CXT_ID (pPeerentry);
    switch (u2Event)
    {
        case BGP4_START_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Start Event in OPENCONFIRM "
                           "State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            break;

        case BGP4_STOP_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Stop Event in OPENCONFIRM "
                           "State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4TmrhStopTimer (BGP4_HOLD_TIMER, (VOID *) pPeerentry);
            Bgp4TmrhStopTimer (BGP4_KEEPALIVE_TIMER, (VOID *) pPeerentry);
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                           BGP4_EVENTS_TRC | BGP4_TXQ_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Sending CEASE message\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4EhSendError (pPeerentry, BGP4_CEASE, 0, NULL, 0);
            Bgp4TcphCommand (pPeerentry, BGP4_TCP_CLOSE, NULL, 0);
            BGP4_CHANGE_STATE (pPeerentry, BGP4_IDLE_STATE);
            Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_IDLE_STATE);
            if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
            {
                BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG,
                               BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                               BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Changed State - Prev State: (%s), "
                               "Curr State: (%s)\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))),
                               gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                 (pPeerentry)],
                               gau1BGP4FsmStates[BGP4_PEER_STATE (pPeerentry)]);
            }
            break;

        case BGP4_AUTOMATIC_STOP_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Automatic Stop Event in OPENCONFIRM "
                           "State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            if (pPeerentry->peerConfig.u1AutomaticStop ==
                BGP4_PEER_AUTOMATICSTOP_ENABLE)
            {
                BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) += 1;
                if (BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) >
                    BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT (pPeerentry))
                {
                    if (BGP4_DAMP_PEER_OSCILLATIONS (pPeerentry) ==
                        BGP4_DAMP_PEER_OSCILLATIONS_ENABLE)
                    {
                        Bgp4SemhDampCheck (pPeerentry);
                    }
                }
            }
            Bgp4SemhProcessInvalidEvt (pPeerentry);
            break;

        case BGP4_KEEPALIVE_TMR_EXP_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_KEEP_TRC | BGP4_EVENTS_TRC | BGP4_RX_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Received Keep-Alive Timer Expire Event "
                           "in OPENCONFIRM State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4TmrhStartTimer (BGP4_KEEPALIVE_TIMER, (VOID *) pPeerentry,
                                BGP4_PEER_NEG_KEEP_ALIVE (pPeerentry));
            pu1Msg = Bgp4MhFormKeepalive (pPeerentry, &u4Msgsize);
            if (pu1Msg == NULL)
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_ALL_FAILURE_TRC | BGP4_PEER_CON_TRC |
                               BGP4_KEEP_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Failure in Forming Keep-Alive "
                               "Message in OPENCONFIRM State.\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))));
                break;
            }
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_KEEP_TRC | BGP4_TXQ_TRC |
                           BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Sending Keep Alive Message in "
                           "OPENCONFIRM State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4TcphCommand (pPeerentry, BGP4_TCP_SEND, pu1Msg, u4Msgsize);
            break;

        case BGP4_KEEPALIVE_MSG_RECD_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_RX_TRC | BGP4_KEEP_TRC | BGP4_EVENTS_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Received Keep Alive Message in "
                           "OPENCONFIRM State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));

            Bgp4TmrhStopTimer (BGP4_HOLD_TIMER, (VOID *) pPeerentry);
            Bgp4TmrhStartTimer (BGP4_HOLD_TIMER,
                                (VOID *) pPeerentry,
                                BGP4_PEER_NEG_HOLD_INT (pPeerentry));

            Bgp4SemhCloseParallelConn (pPeerentry);
            /* When the restarting Peer has sent keep-alive upon processing
             * the open message , stop the peer specific restart timer */
            Bgp4TmrhStopTimer (BGP4_PEER_RESTART_TIMER, (VOID *) pPeerentry);

            if ((BGP4_GET_NODE_STATUS == RM_ACTIVE)
                && (BGP4_PREV_RED_NODE_STATUS == RM_STANDBY)
                && (BGP4_RESTART_MODE (BGP4_PEER_CXT_ID (pPeerentry))
                    == BGP4_RESTARTING_MODE))
            {
                /* After restart if the peer restart timer started for deiniting
                 * the peer if session is not established. Stop the timer now.
                 * Also start the stale timer to delete the Stale RIB routes */
                Bgp4TmrhStartTimer (BGP4_STALE_TIMER, (VOID *) pPeerentry,
                                    BGP4_STALE_PATH_INTERVAL (BGP4_PEER_CXT_ID
                                                              (pPeerentry)));
            }
            BGP4_PEER_IN_UPDATES (pPeerentry) = 0;
            BGP4_PEER_OUT_UPDATES (pPeerentry) = 0;

            BGP4_PEER_FSM_TRANS (pPeerentry)++;
            BGP4_PEER_ESTAB_TIME (pPeerentry) = Bgp4ElapTime ();
            BGP4_CHANGE_STATE (pPeerentry, BGP4_ESTABLISHED_STATE);

            /* Register with BFD module for monitoring the neighbor IP path,
             * if BFD monitoring is enabled for the peer.*/
            if (pPeerentry->u1BfdStatus == BGP4_BFD_ENABLE)
            {
                Bgp4RegisterWithBfd (u4VrfId, pPeerentry);
            }

            BGP4_GET_AFISAFI_MASK (BGP4_PEER_REMOTE_ADDR_TYPE (pPeerentry),
                                   (UINT2) BGP4_INET_SAFI_UNICAST,
                                   u4AfiSafiMask);
            /* Fetch the ORF Capability negotiated mask for the given AFI,SAFI,ORF Send mode */
            if (Bgp4OrfGetOrfCapMask (u4AfiSafiMask, BGP4_CLI_ORF_MODE_SEND,
                                      &u4NegOrfCapMask) == BGP4_SUCCESS)
            {
                /* Check whether ORF send Capability is negotiated with
                 * the peer for this AFI,SAFI or not. If negotiated, then
                 * send the ORF/RR message for this <AFI,SAFI>. */
                if ((BGP4_PEER_NEG_CAP_MASK (pPeerentry) & u4NegOrfCapMask) ==
                    u4NegOrfCapMask)
                {
                    Bgp4RtRefMsgFormHandler (pPeerentry,
                                             BGP4_PEER_REMOTE_ADDR_TYPE
                                             (pPeerentry),
                                             (UINT1) BGP4_INET_SAFI_UNICAST,
                                             BGP4_TRUE);
                }
            }

            Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_ESTABLISHED_STATE);
            Bgp4RedSyncBgpPeerInfo (BGP4_PEER_CXT_ID (pPeerentry), pPeerentry);
            if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
            {
                BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG,
                               BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                               BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Changed State - Prev State: (%s), "
                               "Curr State: (%s)\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))),
                               gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                 (pPeerentry)],
                               gau1BGP4FsmStates[BGP4_PEER_STATE (pPeerentry)]);
            }
            BGP4_PEER_DIRECTLY_CONNECTED (pPeerentry) =
                Bgp4IsDirectlyConnected ((BGP4_PEER_REMOTE_ADDR_INFO
                                          (pPeerentry)), u4VrfId);
#ifdef BGP4_IPV6_WANTED
            Bgp4FillLinkLocalAddress (pPeerentry);
#endif

            /* Send START Indication to RIB Handler */
#ifdef RFD_WANTED
            if ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerentry), pPeerentry)
                 == BGP4_EXTERNAL_PEER)
                && (BGP4_CONFED_PEER_STATUS (pPeerentry) == BGP4_FALSE))
            {
                RfdPeersHandler (pPeerentry);
            }
#endif
            /* Sending Established Notification to SNMP Manager */
            if (BGP4_TRAP_STATUS (u4VrfId) == BGP4_TRAP_ENABLE)
            {
                Bgp4SnmpSendEstablishedStatusChgTrap (pPeerentry,
                                                      BGP4_PEER_ESTABLISHED_STATE_CHANGE_TRAP);
            }
            Bgp4RibhStartConnection (pPeerentry);
            break;

        case BGP4_NOTIFICATION_MSG_RECD_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_EVENTS_TRC | BGP4_RX_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Received Notification Message in "
                           "OPENCONFIRM State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4MhProcessNotification (pPeerentry, pu1Buf, u4Buflen);
            if (CAPS_ADVT_SUP (BGP4_PEER_CXT_ID (pPeerentry)) == BGP4_TRUE)
            {
                u1ErrorCode = *(pu1Buf);
                if (u1ErrorCode == BGP4_OPEN_MSG_ERR)
                {
                    u1ErrSubCode = *(pu1Buf + BGP_ERROR_CODE_LENGTH);
                    u1ErrorData = *(pu1Buf + BGP_ERROR_CODE_LENGTH +
                                    BGP_ERROR_SUBCODE_LENGTH);
                    if (((u1ErrSubCode == UNSUPPORTED_OPTIONAL_PARAMETER) &&
                         (u1ErrorData == CAPABILITIES_OPTIONAL_PARAMETER)) ||
                        (u1ErrSubCode == UNSUPPORTED_CAPABILITY))
                    {
                        Bgp4CapsProcessNotification (pu1Buf, pPeerentry);
                        break;
                    }
                }
            }
            Bgp4SemhProcessInvalidEvt (pPeerentry);
            break;

        case BGP4_HOLD_TMR_EXP_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_KEEP_TRC
                           | BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Hold Timer Expired Event in "
                           "OPENCONFIRM State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4EhSendError (pPeerentry, BGP4_HOLD_TMR_EXPIRED_ERR, 0, NULL, 0);
            if (pPeerentry->peerConfig.u1AutomaticStop ==
                BGP4_PEER_AUTOMATICSTOP_ENABLE)
            {
                BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) += 1;
                if (BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) >
                    BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT (pPeerentry))
                {
                    if (BGP4_DAMP_PEER_OSCILLATIONS (pPeerentry) ==
                        BGP4_DAMP_PEER_OSCILLATIONS_ENABLE)
                    {
                        Bgp4SemhDampCheck (pPeerentry);
                    }
                }
            }
            Bgp4SemhProcessInvalidEvt (pPeerentry);
            break;

        case BGP4_TCP_CONN_CLOSED_EVT:
        case BGP4_TCP_FATAL_ERROR_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received TCP Close Event in "
                           "OPENCONFIRM State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4SemhProcessInvalidEvt (pPeerentry);
            break;

        case BGP4_ROUTE_REFRESH_MSG_RECD_EVT:
        default:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                           "\tPEER %s - FSM Error in OPENCONFIRM State\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4EhSendError (pPeerentry, BGP4_FSM_ERR, 0, NULL, 0);
            if (pPeerentry->peerConfig.u1AutomaticStop ==
                BGP4_PEER_AUTOMATICSTOP_ENABLE)
            {
                BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) += 1;
                if (BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) >
                    BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT (pPeerentry))
                {
                    if (BGP4_DAMP_PEER_OSCILLATIONS (pPeerentry) ==
                        BGP4_DAMP_PEER_OSCILLATIONS_ENABLE)
                    {
                        Bgp4SemhDampCheck (pPeerentry);
                    }
                }
            }
            Bgp4SemhProcessInvalidEvt (pPeerentry);
            break;
    }

    return BGP4_SUCCESS;
}

/****************************************************************************
 * DESCRIPTION : This function is called from the SEM handler whenever any
 *               event has been receivied when the peer is in ESTABLISHED state.
 *
 * INPUTS      : Information about the peer whose SEM state is going to change
 *                   (pPeerentry),
 *               New Event which may cause the SEM state change (u1Event),
 *               Buffer which may be used for taking some action (pu1Buf),
 *               Length of the buffer (u4Buflen).
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : BGP4_SUCCESS 
 *
 *****************************************************************************/
INT4
Bgp4SemhEstablishedState (tBgp4PeerEntry * pPeerentry,
                          UINT2 u2Event, UINT1 *pu1Buf, UINT4 u4Buflen)
{
    tTMO_SLL            TsFeasibleroutes;
    tTMO_SLL            TsWithdrawnroutes;
    tTMO_SLL            TsNonFilteredRoutes;
    tRouteProfile      *pRtProfile = NULL;
    UINT1              *pu1Msg = NULL;
    UINT1               i1RetVal = 0;
    PRIVATE UINT4       u4KeepMsg = 0;
    UINT4               u4Msgsize;

    switch (u2Event)
    {
        case BGP4_START_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Start Event in ESTABLISHED"
                           "State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            break;

            /* RFC 4271 Update */
        case BGP4_AUTOMATIC_STOP_EVT:
        case BGP4_STOP_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Autostop / Stop Event in ESTABLISHED"
                           "State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));

            if ((u2Event == BGP4_STOP_EVT) ||
                ((u2Event == BGP4_AUTOMATIC_STOP_EVT) &&
                 (pPeerentry->peerConfig.u1AutomaticStop ==
                  BGP4_PEER_AUTOMATICSTOP_ENABLE)))
            {
                BGP4_PEER_ESTAB_TIME (pPeerentry) = Bgp4ElapTime ();

                /* Send STOP Indication to RIB Handler */
                if (CAPS_ADVT_SUP (BGP4_PEER_CXT_ID (pPeerentry)) == BGP4_TRUE)
                {
                    CapsDeletePeerCapsInfo (pPeerentry);
                    CapsClearSpkrAncdSts (pPeerentry);
                }
                /* Delete all ORF entries received from the peer */
                BgpOrfDelRcvdOrfEntries (pPeerentry);
                pPeerentry->u1IsOrfSent = 0;
                /* Delete the RBTree holding advertised routes to peer */
                pRtProfile =
                    RBTreeGetFirst (BGP4_PEER_SENT_ROUTES (pPeerentry));
                while (pRtProfile != NULL)
                {
                    Bgp4DelAdvtRoute (pPeerentry, pRtProfile);
                    pRtProfile =
                        RBTreeGetNext (BGP4_PEER_SENT_ROUTES (pPeerentry),
                                       (tRBElem *) pRtProfile, NULL);

                }

                /* Disable BFD monitoring, if it is enabled for this peer */
                if (pPeerentry->u1BfdStatus == BGP4_BFD_ENABLE)
                {
                    Bgp4DeRegisterWithBfd (BGP4_PEER_CXT_ID (pPeerentry),
                                           pPeerentry, TRUE);
                }
                Bgp4RibhEndConnection (pPeerentry);
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG, BGP4_TXQ_TRC |
                               BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : Sending CEASE Message.\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))));

                /* In case of GR-Switchover from Active to standby, 
                 * CEASE message need not be sent to the peer */
                if ((Bgp4GRCheckRestartMode (BGP4_PEER_CXT_ID (pPeerentry)) !=
                     BGP4_RESTARTING_MODE) &&
                    (BGP4_GET_NODE_STATUS != RM_STANDBY))
                {
                    Bgp4EhSendError (pPeerentry, BGP4_CEASE, 0, NULL, 0);
                }

                if (pPeerentry->peerConfig.u1AutomaticStop ==
                    BGP4_PEER_AUTOMATICSTOP_ENABLE)
                {
                    BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) += 1;
                    if (BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) >
                        BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT
                        (pPeerentry))
                    {
                        if (BGP4_DAMP_PEER_OSCILLATIONS (pPeerentry) ==
                            BGP4_DAMP_PEER_OSCILLATIONS_ENABLE)
                        {
                            Bgp4SemhDampCheck (pPeerentry);
                        }
                    }
                }

                Bgp4TcphCommand (pPeerentry, BGP4_TCP_CLOSE, NULL, 0);
                BGP4_CHANGE_STATE (pPeerentry, BGP4_IDLE_STATE);
                Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_IDLE_STATE);
                Bgp4RedSyncBgpPeerInfo (BGP4_PEER_CXT_ID (pPeerentry),
                                        pPeerentry);
                if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES)
                    && (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
                {
                    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG,
                                   BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                                   BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s : Changed State - Prev State: (%s), "
                                   "Curr State: (%s)\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))),
                                   gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                     (pPeerentry)],
                                   gau1BGP4FsmStates[BGP4_PEER_STATE
                                                     (pPeerentry)]);
                }
            }
            break;

        case BGP4_KEEPALIVE_TMR_EXP_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_KEEP_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Keep-Alive Timer Expire Event "
                           "in ESTABLISHED State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4TmrhStartTimer (BGP4_KEEPALIVE_TIMER, (VOID *) pPeerentry,
                                BGP4_PEER_NEG_KEEP_ALIVE (pPeerentry));
            pu1Msg = Bgp4MhFormKeepalive (pPeerentry, &u4Msgsize);
            if (pu1Msg == NULL)
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG,
                               BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                               BGP4_ALL_FAILURE_TRC | BGP4_KEEP_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : Error forming Keep-Alive Msg "
                               "in ESTABLISHED State.\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))));
                break;
            }

            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_KEEP_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Sending Keep Alive Message\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4TcphCommand (pPeerentry, BGP4_TCP_SEND, pu1Msg, u4Msgsize);

            break;

        case BGP4_KEEPALIVE_MSG_RECD_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_KEEP_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Received Keep Alive Message in "
                           "ESTABLISHED State\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4TmrhStopTimer (BGP4_HOLD_TIMER, (VOID *) pPeerentry);
            Bgp4TmrhStartTimer (BGP4_HOLD_TIMER,
                                (VOID *) pPeerentry,
                                BGP4_PEER_NEG_HOLD_INT (pPeerentry));
            u4KeepMsg++;
            break;

        case BGP4_UPDATE_MSG_RECD_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                           BGP4_UPD_MSG_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Update Message "
                           "in ESTABLISHED State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4TmrhStopTimer (BGP4_HOLD_TIMER, (VOID *) pPeerentry);
            Bgp4TmrhStartTimer (BGP4_HOLD_TIMER,
                                (VOID *) pPeerentry,
                                BGP4_PEER_NEG_HOLD_INT (pPeerentry));

            BGP4_PEER_IN_UPDATES (pPeerentry)++;
            BGP4_PEER_IN_UPDATE_ELAP_TIME (pPeerentry) = Bgp4ElapTime ();
            TMO_SLL_Init (&TsNonFilteredRoutes);
            TMO_SLL_Init (&TsFeasibleroutes);
            TMO_SLL_Init (&TsWithdrawnroutes);
            Bgp4MhDisplayRouteInfo (pPeerentry, &TsFeasibleroutes,
                                    &TsWithdrawnroutes);
            i1RetVal = Bgp4MhProcessUpdate (pPeerentry, pu1Buf, u4Buflen,
                                            &TsFeasibleroutes,
                                            &TsWithdrawnroutes);
            if (i1RetVal == BGP4_SUCCESS)
            {
                if (((BGP4_TRC_FLAG & BGP4_DUMP_HGH_TRC) == BGP4_DUMP_HGH_TRC)
                    || ((BGP4_TRC_FLAG & BGP4_DUMP_LOW_TRC) ==
                        BGP4_DUMP_LOW_TRC))
                {
                    Bgp4MhDisplayRouteInfo (pPeerentry, &TsFeasibleroutes,
                                            &TsWithdrawnroutes);
                }
                if (TMO_SLL_Count (&TsWithdrawnroutes) > 0)
                {
                    TMO_SLL_Concat (BGP4_PEER_RCVD_ROUTES (pPeerentry),
                                    &TsWithdrawnroutes);
                }

                if (TMO_SLL_Count (&TsFeasibleroutes) > 0)
                {
                    /* Apply configured Local Preference/MED for the received 
                     * routes */
                    Bgp4FiltApplyLPMED (pPeerentry, &(TsFeasibleroutes));

                    /* Apply the incoming Filter Configuration */
                    Bgp4FiltApplyInputFilter (&(TsFeasibleroutes), pPeerentry);

                    /* When moving from RM Standby state to RM Active State, 
                     * verify if the routes is already synced from Old Active
                     * instance. */
                    Bgp4CheckSyncedRoutes (pPeerentry, &TsFeasibleroutes);
                    /* Routes are available for Input Processing. The presence
                     * of routes in the peer Received route list acts as an
                     * trigger to the Input Ribin process handler. */
                    TMO_SLL_Concat (BGP4_PEER_RCVD_ROUTES (pPeerentry),
                                    &TsFeasibleroutes);
                }
                break;
            }
            else if (i1RetVal == BGP4_IGNORE)
            {
                /* If the capability is disabled , silently ignore the update */
                break;
            }

            BGP4_PEER_ESTAB_TIME (pPeerentry) = Bgp4ElapTime ();

            /* Send STOP Indication to RIB Handler */
            if (CAPS_ADVT_SUP (BGP4_PEER_CXT_ID (pPeerentry)) == BGP4_TRUE)
            {
                CapsDeletePeerCapsInfo (pPeerentry);
                CapsClearSpkrAncdSts (pPeerentry);
            }

            /* Delete all ORF entries received from the peer */
            BgpOrfDelRcvdOrfEntries (pPeerentry);
            pPeerentry->u1IsOrfSent = 0;

            /* Disable BFD monitoring, if it is enabled for this peer */
            if (pPeerentry->u1BfdStatus == BGP4_BFD_ENABLE)
            {
                Bgp4DeRegisterWithBfd (BGP4_PEER_CXT_ID (pPeerentry),
                                       pPeerentry, TRUE);
            }

            Bgp4RibhEndConnection (pPeerentry);
            Bgp4SemhProcessInvalidEvt (pPeerentry);
            break;

        case BGP4_NOTIFICATION_MSG_RECD_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_EVENTS_TRC | BGP4_RX_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Received Notification Message "
                           "in ESTABLISHED State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4MhProcessNotification (pPeerentry, pu1Buf, u4Buflen);
            BGP4_PEER_ESTAB_TIME (pPeerentry) = Bgp4ElapTime ();
            if (CAPS_ADVT_SUP (BGP4_PEER_CXT_ID (pPeerentry)) == BGP4_TRUE)
            {
                CapsDeletePeerCapsInfo (pPeerentry);
                CapsClearSpkrAncdSts (pPeerentry);
            }
            /* Delete all ORF entries received from the peer */
            BgpOrfDelRcvdOrfEntries (pPeerentry);
            pPeerentry->u1IsOrfSent = 0;

            /* Disable BFD monitoring, if it is enabled for this peer */
            if (pPeerentry->u1BfdStatus == BGP4_BFD_ENABLE)
            {
                Bgp4DeRegisterWithBfd (BGP4_PEER_CXT_ID (pPeerentry),
                                       pPeerentry, TRUE);
            }

#ifdef RFD_WANTED
            if ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerentry), pPeerentry)
                 == BGP4_EXTERNAL_PEER)
                && (BGP4_CONFED_PEER_STATUS (pPeerentry) == BGP4_FALSE))
            {
                BGP4_CHANGE_STATE (pPeerentry, BGP4_IDLE_STATE);
                Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_IDLE_STATE);

                if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                    (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
                {
                    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG,
                                   BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                                   BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s : Changed State - Prev State: (%s),"
                                   " Curr State: (%s)\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))),
                                   gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                     (pPeerentry)],
                                   gau1BGP4FsmStates[BGP4_PEER_STATE
                                                     (pPeerentry)]);
                }
                RfdPeersHandler (pPeerentry);
            }
#endif
            Bgp4RibhEndConnection (pPeerentry);
            Bgp4SemhProcessInvalidEvt (pPeerentry);
            break;

        case BGP4_HOLD_TMR_EXP_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_KEEP_TRC
                           | BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received Hold Timer Expire Event "
                           "in ESTABLISHED State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4EhSendError (pPeerentry, BGP4_HOLD_TMR_EXPIRED_ERR, 0, NULL, 0);
            BGP4_PEER_ESTAB_TIME (pPeerentry) = Bgp4ElapTime ();
            if (CAPS_ADVT_SUP (BGP4_PEER_CXT_ID (pPeerentry)) == BGP4_TRUE)
            {
                CapsDeletePeerCapsInfo (pPeerentry);
                CapsClearSpkrAncdSts (pPeerentry);
            }
            /* Delete all ORF entries received from the peer */
            BgpOrfDelRcvdOrfEntries (pPeerentry);
            pPeerentry->u1IsOrfSent = 0;

            /* Disable BFD monitoring, if it is enabled for this peer */
            if (pPeerentry->u1BfdStatus == BGP4_BFD_ENABLE)
            {
                Bgp4DeRegisterWithBfd (BGP4_PEER_CXT_ID (pPeerentry),
                                       pPeerentry, TRUE);
            }

#ifdef RFD_WANTED
            if ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerentry), pPeerentry)
                 == BGP4_EXTERNAL_PEER)
                && (BGP4_CONFED_PEER_STATUS (pPeerentry) == BGP4_FALSE))
            {
                BGP4_CHANGE_STATE (pPeerentry, BGP4_IDLE_STATE);
                Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_IDLE_STATE);
                if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                    (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
                {
                    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG,
                                   BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                                   BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s : Changed State - Prev State: (%s),"
                                   " Curr State: (%s)\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))),
                                   gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                     (pPeerentry)],
                                   gau1BGP4FsmStates[BGP4_PEER_STATE
                                                     (pPeerentry)]);
                }
                RfdPeersHandler (pPeerentry);
            }
#endif
            Bgp4RibhEndConnection (pPeerentry);
            if (pPeerentry->peerConfig.u1AutomaticStop ==
                BGP4_PEER_AUTOMATICSTOP_ENABLE)
            {
                BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) += 1;
                if (BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) >
                    BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT (pPeerentry))
                {
                    if (BGP4_DAMP_PEER_OSCILLATIONS (pPeerentry) ==
                        BGP4_DAMP_PEER_OSCILLATIONS_ENABLE)
                    {
                        Bgp4SemhDampCheck (pPeerentry);
                    }
                }
            }

            Bgp4SemhProcessInvalidEvt (pPeerentry);
            break;

        case BGP4_TCP_CONN_CLOSED_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received TCP close Event in ESTABLISHED"
                           "State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            BGP4_PEER_ESTAB_TIME (pPeerentry) = Bgp4ElapTime ();
#ifdef RFD_WANTED
            if ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerentry), pPeerentry)
                 == BGP4_EXTERNAL_PEER)
                && (BGP4_CONFED_PEER_STATUS (pPeerentry) == BGP4_FALSE))
            {
                BGP4_CHANGE_STATE (pPeerentry, BGP4_IDLE_STATE);
                Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_IDLE_STATE);
                if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                    (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
                {
                    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG,
                                   BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                                   BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s : Changed State - Prev State: (%s),"
                                   " Curr State: (%s)\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))),
                                   gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                     (pPeerentry)],
                                   gau1BGP4FsmStates[BGP4_PEER_STATE
                                                     (pPeerentry)]);
                }
                RfdPeersHandler (pPeerentry);
            }
#endif
            /* If the GR admin status is enabled and the peer is GR capable, 
             * do not release resources related to it */
            if ((Bgp4GRCheckGRCapability (BGP4_PEER_CXT_ID (pPeerentry)) ==
                 BGP4_TRUE)
                && (Bgp4GRIsPeerGRCapable (pPeerentry) == BGP4_SUCCESS)
                && (Bgp4GRCheckRestartMode (BGP4_PEER_CXT_ID (pPeerentry)) ==
                    BGP4_RECEIVING_MODE))
            {
                Bgp4GRProcessIdleState (pPeerentry);
            }
            if (CAPS_ADVT_SUP (BGP4_PEER_CXT_ID (pPeerentry)) == BGP4_TRUE)
            {
                CapsDeletePeerCapsInfo (pPeerentry);
                CapsClearSpkrAncdSts (pPeerentry);
            }
            /* Delete all ORF entries received from the peer */
            BgpOrfDelRcvdOrfEntries (pPeerentry);
            pPeerentry->u1IsOrfSent = 0;

            /* Disable BFD monitoring, if it is enabled for this peer */
            if (pPeerentry->u1BfdStatus == BGP4_BFD_ENABLE)
            {
                Bgp4DeRegisterWithBfd (BGP4_PEER_CXT_ID (pPeerentry),
                                       pPeerentry, TRUE);
            }

            Bgp4RibhEndConnection (pPeerentry);
            Bgp4SemhProcessInvalidEvt (pPeerentry);
            break;

        case BGP4_TCP_FATAL_ERROR_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Received TCP fatal error Event in "
                           "ESTABLISHED State.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            BGP4_PEER_ESTAB_TIME (pPeerentry) = Bgp4ElapTime ();
            if (CAPS_ADVT_SUP (BGP4_PEER_CXT_ID (pPeerentry)) == BGP4_TRUE)
            {
                CapsDeletePeerCapsInfo (pPeerentry);
                CapsClearSpkrAncdSts (pPeerentry);
            }
            /* Delete all ORF entries received from the peer */
            BgpOrfDelRcvdOrfEntries (pPeerentry);
            pPeerentry->u1IsOrfSent = 0;

            /* Disable BFD monitoring, if it is enabled for this peer */
            if (pPeerentry->u1BfdStatus == BGP4_BFD_ENABLE)
            {
                Bgp4DeRegisterWithBfd (BGP4_PEER_CXT_ID (pPeerentry),
                                       pPeerentry, TRUE);
            }

            Bgp4RibhEndConnection (pPeerentry);
            Bgp4SemhProcessInvalidEvt (pPeerentry);
            break;

        case BGP4_ROUTE_REFRESH_MSG_RECD_EVT:
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_RX_TRC |
                           BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                           "\tPEER %s - Received Route Refresh Message "
                           "in ESTABLISHED State\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4RtRefMsgRcvdHandler (pPeerentry, pu1Buf, u4Buflen);
            break;

        case BGP4_TCP_CONN_OPENED_EVT:
            /* Means for GR capable peer, TCP connection close already happened 
             * has not been detected. 
             * Hence process the Connection close procedure
             * before processing the open connection */
            if ((Bgp4GRCheckGRCapability (BGP4_PEER_CXT_ID (pPeerentry)) ==
                 BGP4_TRUE)
                && (Bgp4GRIsPeerGRCapable (pPeerentry) == BGP4_SUCCESS)
                && (Bgp4GRCheckRestartMode (BGP4_PEER_CXT_ID (pPeerentry)) ==
                    BGP4_RECEIVING_MODE))
            {
                BGP4_CHANGE_STATE (pPeerentry, BGP4_IDLE_STATE);
                Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_IDLE_STATE);
                Bgp4GRProcessIdleState (pPeerentry);
                Bgp4RibhEndConnection (pPeerentry);
                if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                    (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
                {
                    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG,
                                   BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                                   BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s : Changed State - Prev State: (%s),"
                                   " Curr State: (%s)\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))),
                                   gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                     (pPeerentry)],
                                   gau1BGP4FsmStates[BGP4_PEER_STATE
                                                     (pPeerentry)]);
                }
            }
            else                /* For peer that are not GR capbable,
                                   treat is as an invalid event */
            {
                /* Send STOP Indication to RIB Handler */
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                               "\tPEER %s - FSM Error in ESTABLISHED State\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))));
                Bgp4EhSendError (pPeerentry, BGP4_FSM_ERR, 0, NULL, 0);
                BGP4_PEER_ESTAB_TIME (pPeerentry) = Bgp4ElapTime ();
                if (CAPS_ADVT_SUP (BGP4_PEER_CXT_ID (pPeerentry)) == BGP4_TRUE)
                {
                    CapsDeletePeerCapsInfo (pPeerentry);
                    CapsClearSpkrAncdSts (pPeerentry);
                }
                /* Delete all ORF entries received from the peer */
                BgpOrfDelRcvdOrfEntries (pPeerentry);
                pPeerentry->u1IsOrfSent = 0;

                /* Disable BFD monitoring, if it is enabled for this peer */
                if (pPeerentry->u1BfdStatus == BGP4_BFD_ENABLE)
                {
                    Bgp4DeRegisterWithBfd (BGP4_PEER_CXT_ID (pPeerentry),
                                           pPeerentry, TRUE);
                }

                Bgp4RibhEndConnection (pPeerentry);
                Bgp4SemhProcessInvalidEvt (pPeerentry);
                break;
            }

            /* TCP Connection is opened from GR capable peer. Process it as if 
             * a new connection is established */
            if ((Bgp4GRCheckGRCapability (BGP4_PEER_CXT_ID (pPeerentry)) ==
                 BGP4_TRUE)
                && (Bgp4GRCheckRestartMode (BGP4_PEER_CXT_ID (pPeerentry)) !=
                    BGP4_RESTARTING_MODE))
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                               BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_EVENTS_TRC | BGP4_RX_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Received TCP Open Event in ESTABLISHED STATE "
                               "from GR Capable peer .\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerentry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerentry))));
                pu1Msg = Bgp4MhFormOpen (pPeerentry, &u4Msgsize);
                if (pu1Msg == NULL)
                {
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_ALL_FAILURE_TRC | BGP4_PEER_CON_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : Failure in Forming OPEN Message "
                                   "in ESTABLISHED State for GR capable peer .\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))));
                    Bgp4SemhProcessInvalidEvt (pPeerentry);
                    break;
                }

                BGP4_PEER_IN_MSGS (pPeerentry) = 0;
                BGP4_PEER_OUT_MSGS (pPeerentry) = 0;
                BGP4_RTREF_MSG_RCVD_CTR (pPeerentry, BGP4_IPV4_UNI_INDEX) = 0;
                BGP4_RTREF_MSG_SENT_CTR (pPeerentry, BGP4_IPV4_UNI_INDEX) = 0;
#ifdef BGP4_IPV6_WANTED
                BGP4_RTREF_MSG_RCVD_CTR (pPeerentry, BGP4_IPV6_UNI_INDEX) = 0;
                BGP4_RTREF_MSG_SENT_CTR (pPeerentry, BGP4_IPV6_UNI_INDEX) = 0;
#endif

                if (Bgp4TcphCommand (pPeerentry, BGP4_TCP_SEND, pu1Msg,
                                     u4Msgsize) == BGP4_FAILURE)
                {
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s : Failure sending OPEN Message "
                                   "in ESTABLISHED STATE for GR capable peer.\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))));
                    Bgp4SemhProcessInvalidEvt (pPeerentry);
                    break;
                }
                BGP4_CHANGE_STATE (pPeerentry, BGP4_OPENSENT_STATE);
                Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_OPENSENT_STATE);
                if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
                    (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
                {
                    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                                   BGP4_TRC_FLAG,
                                   BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                                   BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s : Changed State - Prev State: (%s), "
                                   "Curr State: (%s)\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerentry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerentry))),
                                   gau1BGP4FsmStates[BGP4_PEER_PREV_STATE
                                                     (pPeerentry)],
                                   gau1BGP4FsmStates[BGP4_PEER_STATE
                                                     (pPeerentry)]);
                }
            }
            break;

        default:
            /* Send STOP Indication to RIB Handler */
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                           "\tPEER %s - FSM Error in ESTABLISHED State\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry))));
            Bgp4EhSendError (pPeerentry, BGP4_FSM_ERR, 0, NULL, 0);
            BGP4_PEER_ESTAB_TIME (pPeerentry) = Bgp4ElapTime ();
            if (CAPS_ADVT_SUP (BGP4_PEER_CXT_ID (pPeerentry)) == BGP4_TRUE)
            {
                CapsDeletePeerCapsInfo (pPeerentry);
                CapsClearSpkrAncdSts (pPeerentry);
            }
            /* Delete all ORF entries received from the peer */
            BgpOrfDelRcvdOrfEntries (pPeerentry);
            pPeerentry->u1IsOrfSent = 0;

            /* Disable BFD monitoring, if it is enabled for this peer */
            if (pPeerentry->u1BfdStatus == BGP4_BFD_ENABLE)
            {
                Bgp4DeRegisterWithBfd (BGP4_PEER_CXT_ID (pPeerentry),
                                       pPeerentry, TRUE);
            }

            Bgp4RibhEndConnection (pPeerentry);
            if (pPeerentry->peerConfig.u1AutomaticStop ==
                BGP4_PEER_AUTOMATICSTOP_ENABLE)
            {
                BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) += 1;
                if (BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeerentry) >
                    BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT (pPeerentry))
                {
                    if (BGP4_DAMP_PEER_OSCILLATIONS (pPeerentry) ==
                        BGP4_DAMP_PEER_OSCILLATIONS_ENABLE)
                    {
                        Bgp4SemhDampCheck (pPeerentry);
                    }
                }
            }
            Bgp4SemhProcessInvalidEvt (pPeerentry);
            break;
    }                            /* end switch */
    return BGP4_SUCCESS;
}

/****************************************************************************
 * DESCRIPTION : This function is called whenever an invalid event has been
 *               receivied at any of the SEM state. The connection to the 
 *               the peer is closed and the SEM state is changed to IDLE.
 *
 * INPUTS      : Information about the peer (pPeerentry).
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : BGP4_SUCCESS 
 *
 *****************************************************************************/
INT4
Bgp4SemhProcessInvalidEvt (tBgp4PeerEntry * pPeerentry)
{
    Bgp4TcphCommand (pPeerentry, BGP4_TCP_CLOSE, NULL, 0);
    Bgp4RedSyncBgpPeerInfo (BGP4_PEER_CXT_ID (pPeerentry), pPeerentry);
    /* RFC 4271 Update - If the Idle Hold Timer exceeds its
     * Maximum Threshold,the peer admin status is set to Stop */
    if (BGP4_PEER_CURRENT_IDLE_HOLD_TIME (pPeerentry) >
        (BGP4_MAX_IDLE_HOLD_INTERVAL / 2))
    {
        BGP4_PEER_ADMIN_STATUS (pPeerentry) = BGP4_PEER_STOP;
    }
    BGP4_CHANGE_STATE (pPeerentry, BGP4_IDLE_STATE);
    Bgp4AddTransitionToFsmHist (pPeerentry, BGP4_IDLE_STATE);
    if ((BGP4_PEER_STATE (pPeerentry) <= BGP4_MAX_STATES) &&
        (BGP4_PEER_PREV_STATE (pPeerentry) <= BGP4_MAX_STATES))
    {
        BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                       BGP4_TRC_FLAG,
                       BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC |
                       BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Changed State - Prev State: (%s), "
                       "Curr State: (%s)\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                        (pPeerentry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerentry))),
                       gau1BGP4FsmStates[BGP4_PEER_PREV_STATE (pPeerentry)],
                       gau1BGP4FsmStates[BGP4_PEER_STATE (pPeerentry)]);
    }
    if ((BGP4_GET_PEER_CURRENT_STATE (pPeerentry) !=
         BGP4_PEER_DEINIT_INPROGRESS)
        && ((BGP4_GET_PEER_PEND_FLAG (pPeerentry) & BGP4_PEER_DELETE_PENDING) !=
            BGP4_PEER_DELETE_PENDING))
    {

        if (BGP4_PEER_ADMIN_STATUS (pPeerentry) == BGP4_PEER_AUTO_START)
        {
            Bgp4TmrhStartTimer (BGP4_IDLE_HOLD_TIMER,
                                (VOID *) pPeerentry,
                                BGP4_PEER_CURRENT_IDLE_HOLD_TIME (pPeerentry));
        }
        else
        {
            Bgp4TmrhStartTimer (BGP4_START_TIMER, (VOID *) pPeerentry,
                                BGP4_PEER_START_TIME (pPeerentry));

            BGP4_PEER_START_TIME (pPeerentry) *= BGP4_START_TIME_EXPONENTIAL;
            BGP4_ADJUST_START_TIME_THRESHOLD (pPeerentry);
        }
    }
    else
    {
        /* Set the peer Init pending flag. */
        if ((BGP4_GET_PEER_PEND_FLAG (pPeerentry) & BGP4_PEER_DELETE_PENDING) !=
            BGP4_PEER_DELETE_PENDING)
        {
            BGP4_SET_PEER_PEND_FLAG (pPeerentry, BGP4_PEER_INIT_PENDING);
        }
    }

    BGP4_PEER_RECVD_EOR_MARKER (pPeerentry) = BGP4_INVALID_STATUS;
    BGP4_EOR_RCVD (pPeerentry) = BGP4_INVALID_STATUS;
    BGP4_PEER_EOR_MARKER (pPeerentry) = BGP4_INVALID_STATUS;
    BGP4_PEER_SENT_EOR_MARKER (pPeerentry) = BGP4_INVALID_STATUS;
    return BGP4_SUCCESS;
}

/****************************************************************************
 * DESCRIPTION : This function is called to check whether any collision has
 *               occured when the connection to the peer is established.
 *               If so it takes proper action accordingly.
 *
 * INPUTS      : Information about the peer (pPeerentry)
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : pointer to peer entry 
 *
 *****************************************************************************/
tBgp4PeerEntry     *
Bgp4SemhResolveCollision (tBgp4PeerEntry * pPeerentry)
{
    tBgp4PeerEntry     *pPeerother = NULL;
    tBgp4PeerEntry     *pPeerdel = NULL;

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (BGP4_PEER_CXT_ID (pPeerentry)),
                  pPeerother, tBgp4PeerEntry *)
    {
        if ((pPeerentry != pPeerother)
            && (BGP4_PEER_STATE (pPeerother) == BGP4_OPENCONFIRM_STATE)
            && ((PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry),
                              BGP4_PEER_REMOTE_ADDR_INFO (pPeerother)))
                == BGP4_TRUE))
        {
            if (BGP4_PEER_BGP_ID (pPeerentry) >
                BGP4_LOCAL_BGP_ID (BGP4_PEER_CXT_ID (pPeerentry)))
            {
                /* Local Bgp Speaker BGP Id less than peers BGP ID.
                 * This speaker should close the connections that already
                 * exists and and accepts BGP connection initiated by the
                 * remote system. */
                if (BGP4_PEER_LOCAL_PORT (pPeerentry) == BGP4_DEF_LSNPORT)
                {
                    /* This connection is initiated by the peer */
                    pPeerdel = pPeerother;
                }
                else
                {
                    /* This connection initiated by the speaker */
                    pPeerdel = pPeerentry;
                }
            }
            else
            {
                /* Local BGP speaker should close newly created BGP connection
                 * and continue to use the existing one. */
                if (BGP4_PEER_LOCAL_PORT (pPeerentry) == BGP4_DEF_LSNPORT)
                {
                    /* Connection was intiated by the peer. */
                    pPeerdel = pPeerentry;
                }
                else
                {
                    /* Connection was intiated by the speaker. */
                    pPeerdel = pPeerother;
                }
            }
            break;
        }
    }
    if (pPeerdel != NULL)
    {
        /* This error can generate a TCP error while sending out the packet 
         * Which may result in an event being received for a peer that is not
         * present. Please lookinto it 
         */
        Bgp4EhSendError (pPeerdel, BGP4_CEASE, 0, NULL, 0);
        Bgp4TcphCommand (pPeerdel, BGP4_TCP_CLOSE, NULL, 0);
    }
    else
    {
        return (pPeerentry);
    }

    /* The timers are stopped blindly since the pPeerother will be only in the
     * open confirm state if control comes here 
     */
    Bgp4TmrhStopTimer (BGP4_HOLD_TIMER, (VOID *) pPeerother);
    Bgp4TmrhStopTimer (BGP4_KEEPALIVE_TIMER, (VOID *) pPeerother);
    if (pPeerdel == pPeerentry)
    {
        /* pPeerother is to be retained so start the timers. */
        Bgp4TmrhStartTimer (BGP4_HOLD_TIMER,
                            (VOID *) pPeerother,
                            BGP4_PEER_NEG_HOLD_INT (pPeerother));
        Bgp4TmrhStartTimer (BGP4_KEEPALIVE_TIMER,
                            (VOID *) pPeerother,
                            BGP4_PEER_NEG_KEEP_ALIVE (pPeerother));
        Bgp4SnmphDeletePeerEntry (pPeerentry);
        return (pPeerother);
    }
    else
    {
        Bgp4SnmphDeletePeerEntry (pPeerother);
        return (pPeerentry);
    }
}

/****************************************************************************
 * DESCRIPTION : This function is called to close any of the already existing
 *               parallel connection (two connections to the same peer).
 *
 * INPUTS      : Information about the peer whose SEM state is going to change
 *                   (pPeerentry),
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : INT4 
 *
 *****************************************************************************/
INT4
Bgp4SemhCloseParallelConn (tBgp4PeerEntry * pPeerentry)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tBgp4PeerEntry     *pTmpPeer = NULL;
#if (defined L3VPN || defined VPLSADS_WANTED)
    UINT4               u4Context = 0;
#endif

    BGP_SLL_DYN_Scan (BGP4_PEERENTRY_HEAD (BGP4_PEER_CXT_ID (pPeerentry)),
                      pPeer, pTmpPeer, tBgp4PeerEntry *)
    {
        if ((pPeerentry != pPeer)
            && ((PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry),
                              BGP4_PEER_REMOTE_ADDR_INFO (pPeer)))
                == BGP4_TRUE))
        {
            /* Stop the timers in the parallel connection */

            switch (BGP4_PEER_STATE (pPeer))
            {
                case BGP4_CONNECT_STATE:
                case BGP4_ACTIVE_STATE:
                    Bgp4TmrhStopTimer (BGP4_CONNECTRETRY_TIMER, (VOID *) pPeer);
                    break;

                case BGP4_OPENSENT_STATE:
                    break;

                case BGP4_OPENCONFIRM_STATE:
                    Bgp4TmrhStopTimer (BGP4_HOLD_TIMER, (VOID *) pPeer);
                    Bgp4TmrhStopTimer (BGP4_KEEPALIVE_TIMER, (VOID *) pPeer);
                    break;

                default:
                    break;
            }
            /* To Avoid Sending unwanted CEASE Notification during GR - 
               Force Switchover */
            if ((Bgp4GRCheckRestartMode (BGP4_PEER_CXT_ID (pPeerentry)) !=
                 BGP4_RESTARTING_MODE) &&
                (BGP4_PEER_STATE (pPeer) != BGP4_OPENCONFIRM_STATE))
            {
                Bgp4EhSendError (pPeer, BGP4_CEASE, 0, NULL, 0);
            }
            Bgp4TcphCommand (pPeer, BGP4_TCP_CLOSE, NULL, 0);
#if (defined L3VPN || defined VPLSADS_WANTED)
            u4Context = BGP4_PEER_CXT_ID (pPeer);

            if (BGP4_PEER_RFL_CLIENT (pPeer) == CLIENT)
            {
                BGP4_RR_CLIENT_CNT (u4Context)++;
            }
#endif
            Bgp4SnmphDeletePeerEntry (pPeer);

        }
    }
    return BGP4_SUCCESS;
}

/****************************************************************************
 * DESCRIPTION : This function is called to append the fsm state to the fsm
 *               transition history 
 *
 * INPUTS      : Information about the peer whose SEM state is going to change
 *                   (pPeerentry),
 *                 State from which FSM transitioned (i1BgpFsmState)  
 * OUTPUTS     : None.
 *
 * RETURNS     : BGP4_SUCCESS
 *
 *****************************************************************************/
INT4
Bgp4AddTransitionToFsmHist (tBgp4PeerEntry * pPeerentry, INT1 i1BgpFsmState)
{
    INT2                i2BgpFsmHistHead;
    INT2                i2BgpFsmHistTail;

    i2BgpFsmHistHead = BGP4_PEER_FSM_HIST_HEAD (pPeerentry);
    i2BgpFsmHistTail = BGP4_PEER_FSM_HIST_TAIL (pPeerentry);

    /* Sending Trap for Backward Transition in Peer state */
    if ((BGP4_PEER_PREV_STATE (pPeerentry) > BGP4_PEER_STATE (pPeerentry)) &&
        (BGP4_TRAP_STATUS (BGP4_PEER_CXT_ID (pPeerentry)) == BGP4_TRAP_ENABLE))
    {
        Bgp4SnmpSendBackwardTransitionTrap (pPeerentry,
                                            BGP4_PEER_BACKWARD_TRANSITION_TRAP);
    }

    i2BgpFsmHistTail = (INT2) ((i2BgpFsmHistTail + BGP4_INCREMENT_BY_ONE) %
                               BGP4_PEER_FSM_TRANS_HIST_MAX_INDX);
    if (i2BgpFsmHistHead == BGP4_INVALID_HIST)
    {
        i2BgpFsmHistHead = (INT2) ((i2BgpFsmHistHead + BGP4_INCREMENT_BY_ONE) %
                                   BGP4_PEER_FSM_TRANS_HIST_MAX_INDX);
    }
    else if (i2BgpFsmHistHead == i2BgpFsmHistTail)
    {
        i2BgpFsmHistHead = (INT2) ((i2BgpFsmHistHead + BGP4_INCREMENT_BY_ONE) %
                                   BGP4_PEER_FSM_TRANS_HIST_MAX_INDX);
    }
    BGP4_PEER_FSM_TRANSITION_HIST (pPeerentry, i2BgpFsmHistTail) =
        i1BgpFsmState;
    BGP4_PEER_FSM_HIST_HEAD (pPeerentry) = i2BgpFsmHistHead;
    BGP4_PEER_FSM_HIST_TAIL (pPeerentry) = i2BgpFsmHistTail;
    return BGP4_SUCCESS;
}

#endif /* !BGP4SEMH_C */
