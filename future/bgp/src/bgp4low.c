/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4low.c,v 1.78 2017/09/15 06:19:52 siva Exp $
 *
 * Description: Contains BGP low level routines
 *
 *******************************************************************/
#ifndef BGP4LOW_C
#define BGP4LOW_C

# include  "snmccons.h"
# include  "snmcdefn.h"
# include  "fssnmp.h"
# include  "bgp4com.h"
# include  "fsmpbgcli.h"

/* LOW LEVEL Routines for Table : BgpPeerTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceBgpPeerTable
 Input       :  The Indices
                BgpPeerRemoteAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceBgpPeerTable (UINT4 u4BgpPeerRemoteAddr)
{
    UINT2               u2Port = 0;
    UINT4               u4ContextId = 0;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    if (BGP4_IS_VALID_ADDRESS (u4BgpPeerRemoteAddr) == 0)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        return SNMP_FAILURE;
    }
    if (BGP4_IS_BROADCAST_ADDRESS (u4BgpPeerRemoteAddr) == BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        return SNMP_FAILURE;
    }
    if (u4BgpPeerRemoteAddr == BGP4_LOCAL_BGP_ID (u4Context))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        return SNMP_FAILURE;
    }
    /* Reject Peer Remote Address, if same as local address */
    if (IpIsLocalAddrInCxt (u4ContextId,
                            u4BgpPeerRemoteAddr, &u2Port) == IP_SUCCESS)
    {
        if (BgpGetIpIfAddr (u2Port) == u4BgpPeerRemoteAddr)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Peer Address.\n");
            return (SNMP_FAILURE);
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexBgpPeerTable
 Input       :  The Indices
                BgpPeerRemoteAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexBgpPeerTable (UINT4 *pu4BgpPeerRemoteAddr)
{
    tAddrPrefix         PeerAddress;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    Bgp4InitAddrPrefixStruct (&PeerAddress, BGP4_INET_AFI_IPV4);
    if (Bgp4Ipv4GetFirstIndexBgpPeerTable (u4Context, &PeerAddress) ==
        BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No IPv4 Peer exists in Peer Table.\n");
        return SNMP_FAILURE;
    }

    MEMCPY (pu4BgpPeerRemoteAddr, PeerAddress.au1Address, sizeof (UINT4));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexBgpPeerTable
 Input       :  The Indices
                BgpPeerRemoteAddr
                nextBgpPeerRemoteAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexBgpPeerTable (UINT4 u4BgpPeerRemoteAddr,
                             UINT4 *pu4NextBgpPeerRemoteAddr)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tAddrPrefix         PeerAddrInfo;
    UINT4               u4PeerAddress = 0;
    UINT2               u2Afi = 0;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeerEntry, tBgp4PeerEntry *)
    {
        u2Afi =
            BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_PEER_REMOTE_ADDR_INFO
                                          (pPeerEntry));
        if (u2Afi == BGP4_INET_AFI_IPV4)
        {
            PTR_FETCH_4 (u4PeerAddress, BGP4_PEER_REMOTE_ADDR (pPeerEntry));
            if (u4PeerAddress > u4BgpPeerRemoteAddr)
            {
                break;
            }
        }
    }

    if (pPeerEntry == NULL)
    {
        /* Next entry not present */
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Next peer does not exist.\n");
        return SNMP_FAILURE;
    }
    if (u4PeerAddress > u4BgpPeerRemoteAddr)
    {
        /* Given input peer does not exists. Return the next higher
         * peer Address */
        *pu4NextBgpPeerRemoteAddr = u4PeerAddress;
        return SNMP_SUCCESS;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerIdentifier
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerIdentifier (UINT4 u4BgpPeerRemoteAddr,
                         UINT4 *pu4RetValBgpPeerIdentifier)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    *pu4RetValBgpPeerIdentifier = BGP4_PEER_BGP_ID (pPeer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerState
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerState (UINT4 u4BgpPeerRemoteAddr, INT4 *pi4RetValBgpPeerState)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValBgpPeerState = (INT4) BGP4_PEER_STATE (pPeer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerAdminStatus
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerAdminStatus (UINT4 u4BgpPeerRemoteAddr,
                          INT4 *pi4RetValBgpPeerAdminStatus)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    switch (BGP4_PEER_ADMIN_STATUS (pPeer))
    {
        case BGP4_PEER_START:
            *pi4RetValBgpPeerAdminStatus = BGP4_PEER_START;
            break;
            /* RFC 4271 Update */
        case BGP4_PEER_AUTO_START:
            *pi4RetValBgpPeerAdminStatus = BGP4_PEER_AUTO_START;
            break;
        case BGP4_PEER_STOP:
            if ((BGP4_GET_PEER_PEND_FLAG (pPeer) &
                 BGP4_PEER_MULTIHOP_PEND_START) ==
                BGP4_PEER_MULTIHOP_PEND_START)
            {
                *pi4RetValBgpPeerAdminStatus = BGP4_PEER_START;
            }
            else
            {
                *pi4RetValBgpPeerAdminStatus = BGP4_PEER_STOP;
            }
            break;
        default:
            *pi4RetValBgpPeerAdminStatus = BGP4_INVALID;
            break;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerNegotiatedVersion
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerNegotiatedVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerNegotiatedVersion (UINT4 u4BgpPeerRemoteAddr,
                                INT4 *pi4RetValBgpPeerNegotiatedVersion)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValBgpPeerNegotiatedVersion = (INT4) BGP4_PEER_NEG_VER (pPeer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerLocalAddr
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerLocalAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerLocalAddr (UINT4 u4BgpPeerRemoteAddr,
                        UINT4 *pu4RetValBgpPeerLocalAddr)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);

    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }

    MEMCPY (pu4RetValBgpPeerLocalAddr, BGP4_PEER_LOCAL_ADDR (pPeer),
            sizeof (UINT4));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerLocalPort
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerLocalPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerLocalPort (UINT4 u4BgpPeerRemoteAddr,
                        INT4 *pi4RetValBgpPeerLocalPort)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValBgpPeerLocalPort = (INT4) BGP4_PEER_LOCAL_PORT (pPeer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerRemotePort
 Input       :  The Indices
                BgpPeerRemoteAddr
                The Object 
                retValBgpPeerRemotePort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerRemotePort (UINT4 u4BgpPeerRemoteAddr,
                         INT4 *pi4RetValBgpPeerRemotePort)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValBgpPeerRemotePort = (INT4) BGP4_PEER_REMOTE_PORT (pPeer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerRemoteAs
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerRemoteAs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerRemoteAs (UINT4 u4BgpPeerRemoteAddr,
                       INT4 *pi4RetValBgpPeerRemoteAs)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValBgpPeerRemoteAs = (INT4) BGP4_PEER_ASNO (pPeer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerInUpdates
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerInUpdates
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerInUpdates (UINT4 u4BgpPeerRemoteAddr,
                        UINT4 *pu4RetValBgpPeerInUpdates)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    *pu4RetValBgpPeerInUpdates = BGP4_PEER_IN_UPDATES (pPeer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerOutUpdates
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerOutUpdates
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerOutUpdates (UINT4 u4BgpPeerRemoteAddr,
                         UINT4 *pu4RetValBgpPeerOutUpdates)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    *pu4RetValBgpPeerOutUpdates = BGP4_PEER_OUT_UPDATES (pPeer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerInTotalMessages
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerInTotalMessages
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerInTotalMessages (UINT4 u4BgpPeerRemoteAddr,
                              UINT4 *pu4RetValBgpPeerInTotalMessages)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    *pu4RetValBgpPeerInTotalMessages = BGP4_PEER_IN_MSGS (pPeer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerOutTotalMessages
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerOutTotalMessages
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerOutTotalMessages (UINT4 u4BgpPeerRemoteAddr,
                               UINT4 *pu4RetValBgpPeerOutTotalMessages)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    *pu4RetValBgpPeerOutTotalMessages = BGP4_PEER_OUT_MSGS (pPeer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerLastError
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerLastError
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerLastError (UINT4 u4BgpPeerRemoteAddr,
                        tSNMP_OCTET_STRING_TYPE * pRetValBgpPeerLastError)
{
    tBgp4PeerEntry     *pPeer = NULL;
    UINT1               au1Array[BGP_MAX_ERR_ARRAY_SIZE];
    UINT1               u1Index = 0;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }

    if (BGP4_PEER_LAST_ERROR (pPeer) == BGP4_NO_ERROR)
    {
        *pRetValBgpPeerLastError->pu1_OctetList = '0';
        pRetValBgpPeerLastError->i4_Length = sizeof (INT1);
        return SNMP_SUCCESS;
    }
    au1Array[u1Index] = BGP4_PEER_LAST_ERROR (pPeer);
    u1Index++;
    au1Array[u1Index] = BGP4_PEER_LAST_ERROR_SUB_CODE (pPeer);
    STRNCPY (pRetValBgpPeerLastError->pu1_OctetList, au1Array,
             BGP_ERROR_CODE_LENGTH + BGP_ERROR_SUBCODE_LENGTH);
    pRetValBgpPeerLastError->i4_Length =
        BGP_ERROR_CODE_LENGTH + BGP_ERROR_SUBCODE_LENGTH;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerFsmEstablishedTransitions
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerFsmEstablishedTransitions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerFsmEstablishedTransitions (UINT4 u4BgpPeerRemoteAddr,
                                        UINT4
                                        *pu4RetValBgpPeerFsmEstablishedTransitions)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    *pu4RetValBgpPeerFsmEstablishedTransitions = BGP4_PEER_FSM_TRANS (pPeer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerFsmEstablishedTime
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerFsmEstablishedTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerFsmEstablishedTime (UINT4 u4BgpPeerRemoteAddr,
                                 UINT4 *pu4RetValBgpPeerFsmEstablishedTime)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }

    /* This value is not updated anywhere in the code. Needs to be done.?? */
    if (BGP4_PEER_ESTAB_TIME (pPeer) == BGP4_INIT_TIME)
    {
        *pu4RetValBgpPeerFsmEstablishedTime = 0;
    }
    else
    {
        *pu4RetValBgpPeerFsmEstablishedTime =
            ((UINT4) Bgp4ElapTime () - BGP4_PEER_ESTAB_TIME (pPeer));
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerConnectRetryInterval
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerConnectRetryInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerConnectRetryInterval (UINT4 u4BgpPeerRemoteAddr,
                                   INT4 *pi4RetValBgpPeerConnectRetryInterval)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValBgpPeerConnectRetryInterval =
            (INT4) BGP4_PEER_CONN_RETRY_TIME (pPeer);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerHoldTime
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerHoldTime (UINT4 u4BgpPeerRemoteAddr,
                       INT4 *pi4RetValBgpPeerHoldTime)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValBgpPeerHoldTime = (INT4) BGP4_PEER_NEG_HOLD_INT (pPeer);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerKeepAlive
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerKeepAlive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerKeepAlive (UINT4 u4BgpPeerRemoteAddr,
                        INT4 *pi4RetValBgpPeerKeepAlive)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValBgpPeerKeepAlive = (INT4) BGP4_PEER_NEG_KEEP_ALIVE (pPeer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerHoldTimeConfigured
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerHoldTimeConfigured
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerHoldTimeConfigured (UINT4 u4BgpPeerRemoteAddr,
                                 INT4 *pi4RetValBgpPeerHoldTimeConfigured)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValBgpPeerHoldTimeConfigured = (INT4) BGP4_PEER_HOLD_TIME (pPeer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerKeepAliveConfigured
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerKeepAliveConfigured
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerKeepAliveConfigured (UINT4 u4BgpPeerRemoteAddr,
                                  INT4 *pi4RetValBgpPeerKeepAliveConfigured)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValBgpPeerKeepAliveConfigured =
        (INT4) BGP4_PEER_KEEP_ALIVE_TIME (pPeer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerMinASOriginationInterval
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerMinASOriginationInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerMinASOriginationInterval (UINT4 u4BgpPeerRemoteAddr,
                                       INT4
                                       *pi4RetValBgpPeerMinASOriginationInterval)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer Exist.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValBgpPeerMinASOriginationInterval =
        (INT4) BGP4_PEER_MIN_AS_ORIG_TIME (pPeer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerMinRouteAdvertisementInterval
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerMinRouteAdvertisementInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerMinRouteAdvertisementInterval (UINT4 u4BgpPeerRemoteAddr,
                                            INT4
                                            *pi4RetValBgpPeerMinRouteAdvertisementInterval)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValBgpPeerMinRouteAdvertisementInterval =
        (INT4) BGP4_PEER_MIN_ADV_TIME (pPeer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpPeerInUpdateElapsedTime
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                retValBgpPeerInUpdateElapsedTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpPeerInUpdateElapsedTime (UINT4 u4BgpPeerRemoteAddr,
                                  UINT4 *pu4RetValBgpPeerInUpdateElapsedTime)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }

    /* while porting this needs to be changed. */
    if (BGP4_PEER_IN_UPDATE_ELAP_TIME (pPeer) == BGP4_INIT_TIME)
    {
        *pu4RetValBgpPeerInUpdateElapsedTime = 0;
    }
    else
    {
        *pu4RetValBgpPeerInUpdateElapsedTime =
            ((UINT4) Bgp4ElapTime () - BGP4_PEER_IN_UPDATE_ELAP_TIME (pPeer));
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetBgpPeerAdminStatus
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                setValBgpPeerAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBgpPeerAdminStatus (UINT4 u4BgpPeerRemoteAddr,
                          INT4 i4SetValBgpPeerAdminStatus)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tBgp4QMsg          *pQMsg = NULL;
    tAddrPrefix         PeerRemoteAddr;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&(PeerRemoteAddr), BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerRemoteAddr),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerRemoteAddr);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }

    /* Peer Admin status can't be changed before configuring the
     * peer's AS number */
    if (BGP4_PEER_ASNO (pPeer) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Peer AS number not configured.\n");
        return SNMP_FAILURE;
    }

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting BGP Peer Status Msg "
                  "to BGP Queue FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_Q_MSGS_SIZING_ID]++;
        return SNMP_FAILURE;
    }

    if ((i4SetValBgpPeerAdminStatus == BGP4_PEER_START) ||
        (i4SetValBgpPeerAdminStatus == BGP4_PEER_AUTO_START))
    {
        BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_SNMP_START_EVENT;
    }
    else
    {
        BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_SNMP_STOP_EVENT;
    }
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;

    MEMCPY (&(BGP4_INPUTQ_PEER_ADDR_INFO (pQMsg)),
            &(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)), sizeof (tAddrPrefix));

    BGP4_INPUTQ_CXT (pQMsg) = u4Context;
    /* Send the peer entry to the bgp-task */
    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetBgpPeerConnectRetryInterval
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                setValBgpPeerConnectRetryInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBgpPeerConnectRetryInterval (UINT4 u4BgpPeerRemoteAddr,
                                   INT4 i4SetValBgpPeerConnectRetryInterval)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    BGP4_PEER_CONN_RETRY_TIME (pPeer) =
        (UINT4) i4SetValBgpPeerConnectRetryInterval;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetBgpPeerHoldTimeConfigured
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                setValBgpPeerHoldTimeConfigured
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBgpPeerHoldTimeConfigured (UINT4 u4BgpPeerRemoteAddr,
                                 INT4 i4SetValBgpPeerHoldTimeConfigured)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    BGP4_PEER_HOLD_TIME (pPeer) = (UINT4) i4SetValBgpPeerHoldTimeConfigured;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetBgpPeerKeepAliveConfigured
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                setValBgpPeerKeepAliveConfigured
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBgpPeerKeepAliveConfigured (UINT4 u4BgpPeerRemoteAddr,
                                  INT4 i4SetValBgpPeerKeepAliveConfigured)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    BGP4_PEER_KEEP_ALIVE_TIME (pPeer) =
        (UINT4) i4SetValBgpPeerKeepAliveConfigured;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetBgpPeerMinASOriginationInterval
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                setValBgpPeerMinASOriginationInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBgpPeerMinASOriginationInterval (UINT4 u4BgpPeerRemoteAddr,
                                       INT4
                                       i4SetValBgpPeerMinASOriginationInterval)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    BGP4_PEER_MIN_AS_ORIG_TIME (pPeer) =
        (UINT4) i4SetValBgpPeerMinASOriginationInterval;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetBgpPeerMinRouteAdvertisementInterval
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                setValBgpPeerMinRouteAdvertisementInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetBgpPeerMinRouteAdvertisementInterval (UINT4 u4BgpPeerRemoteAddr,
                                            INT4
                                            i4SetValBgpPeerMinRouteAdvertisementInterval)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    BGP4_PEER_MIN_ADV_TIME (pPeer) =
        (UINT4) i4SetValBgpPeerMinRouteAdvertisementInterval;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2BgpPeerAdminStatus
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                testValBgpPeerAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BgpPeerAdminStatus (UINT4 *pu4ErrorCode, UINT4 u4BgpPeerRemoteAddr,
                             INT4 i4TestValBgpPeerAdminStatus)
{
    UINT4               u4ContextId = 0;
    UINT2               u2Port = 0;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    if (BGP4_IS_VALID_ADDRESS (u4BgpPeerRemoteAddr) == 0)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    if (BGP4_IS_BROADCAST_ADDRESS (u4BgpPeerRemoteAddr) == BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    if (u4BgpPeerRemoteAddr == BGP4_LOCAL_BGP_ID (u4Context))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    /* Reject Peer Remote Address, if same as local address */
    if (IpIsLocalAddrInCxt (u4ContextId,
                            u4BgpPeerRemoteAddr, &u2Port) == IP_SUCCESS)
    {
        if (BgpGetIpIfAddr (u2Port) == u4BgpPeerRemoteAddr)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Peer Address.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
            return (SNMP_FAILURE);
        }
    }

    if ((i4TestValBgpPeerAdminStatus == BGP4_PEER_START) ||
        (i4TestValBgpPeerAdminStatus == BGP4_PEER_AUTO_START) ||
        (i4TestValBgpPeerAdminStatus == BGP4_PEER_STOP))
    {
        return SNMP_SUCCESS;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - Invalid Peer Admin Status.\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_BGP4_PEER_ENABLED);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2BgpPeerConnectRetryInterval
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                testValBgpPeerConnectRetryInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BgpPeerConnectRetryInterval (UINT4 *pu4ErrorCode,
                                      UINT4 u4BgpPeerRemoteAddr,
                                      INT4 i4TestValBgpPeerConnectRetryInterval)
{
    UINT4               u4ContextId = 0;
    UINT2               u2Port = 0;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    if (BGP4_IS_VALID_ADDRESS (u4BgpPeerRemoteAddr) == 0)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    if (BGP4_IS_BROADCAST_ADDRESS (u4BgpPeerRemoteAddr) == BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    if (u4BgpPeerRemoteAddr == BGP4_LOCAL_BGP_ID (u4Context))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    /* Reject Peer Remote Address, if same as local address */
    if (IpIsLocalAddrInCxt (u4ContextId,
                            u4BgpPeerRemoteAddr, &u2Port) == IP_SUCCESS)
    {
        if (BgpGetIpIfAddr (u2Port) == u4BgpPeerRemoteAddr)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Peer Address.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
            return (SNMP_FAILURE);
        }
    }
    if ((i4TestValBgpPeerConnectRetryInterval > 0) &&
        (i4TestValBgpPeerConnectRetryInterval <= BGP4_MAX_CONNRETRY_INTERVAL))
    {
        return SNMP_SUCCESS;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - Invalid Peer Connect Retry Interval.\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_BGP4_INVALID_CONN_RETRY_INT_ERR);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2BgpPeerHoldTimeConfigured
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                testValBgpPeerHoldTimeConfigured
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BgpPeerHoldTimeConfigured (UINT4 *pu4ErrorCode,
                                    UINT4 u4BgpPeerRemoteAddr,
                                    INT4 i4TestValBgpPeerHoldTimeConfigured)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;
    UINT4               u4ContextId = 0;
    UINT2               u2Port = 0;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    if (BGP4_IS_VALID_ADDRESS (u4BgpPeerRemoteAddr) == 0)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    if (BGP4_IS_BROADCAST_ADDRESS (u4BgpPeerRemoteAddr) == BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    if (u4BgpPeerRemoteAddr == BGP4_LOCAL_BGP_ID (u4Context))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    /* Reject Peer Remote Address, if same as local address */
    if (IpIsLocalAddrInCxt (u4ContextId,
                            u4BgpPeerRemoteAddr, &u2Port) == IP_SUCCESS)
    {
        if (BgpGetIpIfAddr (u2Port) == u4BgpPeerRemoteAddr)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Peer Address.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
            return (SNMP_FAILURE);
        }
    }
    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer entry exist.\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    if (i4TestValBgpPeerHoldTimeConfigured <
        (INT4) BGP4_PEER_KEEP_ALIVE_TIME (pPeer))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Hold Time Interval.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_HOLD_TIME_VALUE_ERR);
        return SNMP_FAILURE;
    }
    if ((i4TestValBgpPeerHoldTimeConfigured == 0) ||
        ((i4TestValBgpPeerHoldTimeConfigured >= BGP4_MIN_HOLD_INTERVAL) &&
         (i4TestValBgpPeerHoldTimeConfigured <= BGP4_MAX_HOLD_INTERVAL)))
    {
        return SNMP_SUCCESS;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - Invalid Peer Hold Time Interval.\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_BGP4_INVALID_HOLD_TIME_VALUE_ERR);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2BgpPeerKeepAliveConfigured
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                testValBgpPeerKeepAliveConfigured
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BgpPeerKeepAliveConfigured (UINT4 *pu4ErrorCode,
                                     UINT4 u4BgpPeerRemoteAddr,
                                     INT4 i4TestValBgpPeerKeepAliveConfigured)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddrInfo;
    UINT4               u4ContextId = 0;
    UINT2               u2Port = 0;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    if (BGP4_IS_VALID_ADDRESS (u4BgpPeerRemoteAddr) == 0)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    if (BGP4_IS_BROADCAST_ADDRESS (u4BgpPeerRemoteAddr) == BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    if (u4BgpPeerRemoteAddr == BGP4_LOCAL_BGP_ID (u4Context))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    /* Reject Peer Remote Address, if same as local address */
    if (IpIsLocalAddrInCxt (u4ContextId,
                            u4BgpPeerRemoteAddr, &u2Port) == IP_SUCCESS)
    {
        if (BgpGetIpIfAddr (u2Port) == u4BgpPeerRemoteAddr)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Peer Address.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
            return (SNMP_FAILURE);
        }
    }

    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4BgpPeerRemoteAddr);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddrInfo);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    if (i4TestValBgpPeerKeepAliveConfigured >=
        (INT4) BGP4_PEER_HOLD_TIME (pPeer))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Keep Alive Interval.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_KEEP_ALIVE_VALUE_ERR);
        return SNMP_FAILURE;
    }
    if ((i4TestValBgpPeerKeepAliveConfigured == 0) ||
        ((i4TestValBgpPeerKeepAliveConfigured >= BGP4_MIN_KEEP_INTERVAL) &&
         (i4TestValBgpPeerKeepAliveConfigured <= (BGP4_MAX_KEEP_INTERVAL))))
    {
        return SNMP_SUCCESS;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - Invalid Peer Keep Alive Interval.\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_BGP4_INVALID_KEEP_ALIVE_VALUE_ERR);
    return SNMP_FAILURE;
}

/*********************************************************************
 Function    :  nmhTestv2BgpPeerMinASOriginationInterval
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                testValBgpPeerMinASOriginationInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BgpPeerMinASOriginationInterval (UINT4 *pu4ErrorCode,
                                          UINT4 u4BgpPeerRemoteAddr,
                                          INT4
                                          i4TestValBgpPeerMinASOriginationInterval)
{
    UINT4               u4ContextId = 0;
    UINT2               u2Port = 0;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    if (BGP4_IS_VALID_ADDRESS (u4BgpPeerRemoteAddr) == 0)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    if (BGP4_IS_BROADCAST_ADDRESS (u4BgpPeerRemoteAddr) == BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    if (u4BgpPeerRemoteAddr == BGP4_LOCAL_BGP_ID (u4Context))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    /* Reject Peer Remote Address, if same as local address */
    if (IpIsLocalAddrInCxt (u4ContextId,
                            u4BgpPeerRemoteAddr, &u2Port) == IP_SUCCESS)
    {
        if (BgpGetIpIfAddr (u2Port) == u4BgpPeerRemoteAddr)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Peer Address.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
            return (SNMP_FAILURE);
        }
    }
    if ((i4TestValBgpPeerMinASOriginationInterval >=
         BGP4_MIN_ASORIG_INTERVAL)
        && (i4TestValBgpPeerMinASOriginationInterval <=
            BGP4_MAX_ASORIG_INTERVAL))
    {
        return SNMP_SUCCESS;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - Invalid Peer Min AS Origination Interval.\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_BGP4_INVALID_AS_ORIG_INT_ERR);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2BgpPeerTable
 Input       :  The Indices
                BgpPeerRemoteAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2BgpPeerTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2BgpPeerMinRouteAdvertisementInterval
 Input       :  The Indices
                BgpPeerRemoteAddr

                The Object 
                testValBgpPeerMinRouteAdvertisementInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2BgpPeerMinRouteAdvertisementInterval (UINT4 *pu4ErrorCode,
                                               UINT4 u4BgpPeerRemoteAddr,
                                               INT4
                                               i4TestValBgpPeerMinRouteAdvertisementInterval)
{
    UINT4               u4ContextId = 0;
    UINT2               u2Port = 0;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    if (BGP4_IS_VALID_ADDRESS (u4BgpPeerRemoteAddr) == 0)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    if (BGP4_IS_BROADCAST_ADDRESS (u4BgpPeerRemoteAddr) == BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    if (u4BgpPeerRemoteAddr == BGP4_LOCAL_BGP_ID (u4Context))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    /* Reject Peer Remote Address, if same as local address */
    if (IpIsLocalAddrInCxt (u4ContextId,
                            u4BgpPeerRemoteAddr, &u2Port) == IP_SUCCESS)
    {
        if (BgpGetIpIfAddr (u2Port) == u4BgpPeerRemoteAddr)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Peer Address.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
            return (SNMP_FAILURE);
        }
    }
    if ((i4TestValBgpPeerMinRouteAdvertisementInterval >=
         BGP4_MIN_RA_INTERVAL)
        && (i4TestValBgpPeerMinRouteAdvertisementInterval <=
            BGP4_MAX_RA_INTERVAL))
    {
        return SNMP_SUCCESS;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - Invalid Peer Min Route Advertise Interval.\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_BGP4_INVALID_RT_ADV_INT_ERR);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Bgp4PathAttrTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceBgp4PathAttrTable
 Input       :  The Indices
                Bgp4PathAttrIpAddrPrefix
                Bgp4PathAttrIpAddrPrefixLen
                Bgp4PathAttrPeer
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceBgp4PathAttrTable (UINT4 u4Bgp4PathAttrIpAddrPrefix,
                                           INT4 i4Bgp4PathAttrIpAddrPrefixLen,
                                           UINT4 u4Bgp4PathAttrPeer)
{
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    /* IP Address Prefix Validation */
    if (BGP4_IS_MCAST_LOOP_ADDR (u4Bgp4PathAttrIpAddrPrefix) == 0)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Prefix Value.\n");
        return SNMP_FAILURE;
    }
    if (BGP4_IS_BROADCAST_ADDRESS (u4Bgp4PathAttrIpAddrPrefix) == BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Prefix Value.\n");
        return SNMP_FAILURE;
    }

    /* IP Address Prefix Length Validation */
    if ((i4Bgp4PathAttrIpAddrPrefixLen < 0) ||
        (i4Bgp4PathAttrIpAddrPrefixLen > BGP4_MAX_PREFIXLEN))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Prefix Length.\n");
        return SNMP_FAILURE;
    }

    /* BGP Peer IP Address validation */
    if (BGP4_IS_VALID_ADDRESS (u4Bgp4PathAttrPeer) == 0)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        return SNMP_FAILURE;
    }
    if (BGP4_IS_BROADCAST_ADDRESS (u4Bgp4PathAttrPeer) == BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexBgp4PathAttrTable
 Input       :  The Indices
                Bgp4PathAttrIpAddrPrefix
                Bgp4PathAttrIpAddrPrefixLen
                Bgp4PathAttrPeer
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexBgp4PathAttrTable (UINT4 *pu4Bgp4PathAttrIpAddrPrefix,
                                   INT4 *pi4Bgp4PathAttrIpAddrPrefixLen,
                                   UINT4 *pu4Bgp4PathAttrPeer)
{
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pNextRtProfile = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4RetVal = BGP4_FAILURE;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    /* Get the first route from IPV4-Unicast routing table */
    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    Bgp4GetRibNode (u4Context, CAP_MP_IPV4_UNICAST, &pRibNode);
    if (pRibNode == NULL)
    {
        bgp4RibUnlock ();
        return SNMP_FAILURE;
    }
    i4RetVal = Bgp4RibhGetFirstEntry (CAP_MP_IPV4_UNICAST, &pRtProfile,
                                      &pRibNode, TRUE);
    if (i4RetVal == BGP4_FAILURE)
    {
        /* No u4AfiSafi route. */
        bgp4RibUnlock ();
        return SNMP_FAILURE;
    }

    for (;;)
    {
        if ((BGP4_RT_PEER_ENTRY (pRtProfile) != NULL) &&
            ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_HISTORY) !=
             BGP4_RT_HISTORY) &&
            (BGP4_AFI_IN_ADDR_PREFIX_INFO
             (BGP4_PEER_REMOTE_ADDR_INFO (BGP4_RT_PEER_ENTRY (pRtProfile))) ==
             BGP4_INET_AFI_IPV4))
        {
            break;
        }

        if (BGP4_RT_PEER_ENTRY (pRtProfile) == NULL)
        {
            /* Non - BGP Route. Get the BGP Route. */
            pNextRtProfile = BGP4_RT_NEXT (pRtProfile);
            if (pNextRtProfile != NULL)
            {
                pRtProfile = pNextRtProfile;
                pNextRtProfile = NULL;
                continue;
            }
        }

        pNextRtProfile = NULL;
        /* Route is a History/Last route. History routes can never be used
         * for Index and any routes following this route in this RIB node
         * will also be history. So get the next route entry from the RIB */
        i4RetVal = Bgp4RibhGetNextEntry (pRtProfile, u4Context,
                                         &pNextRtProfile, &pRibNode, TRUE);
        if (i4RetVal == BGP4_FAILURE)
        {
            pRtProfile = NULL;
            break;
        }
        pRtProfile = pNextRtProfile;
        pNextRtProfile = NULL;
    }

    if (pRtProfile != NULL)
    {
        MEMCPY (pu4Bgp4PathAttrIpAddrPrefix, BGP4_RT_IP_PREFIX (pRtProfile),
                sizeof (UINT4));
        *pi4Bgp4PathAttrIpAddrPrefixLen = BGP4_RT_PREFIXLEN (pRtProfile);
        MEMCPY (pu4Bgp4PathAttrPeer,
                BGP4_PEER_REMOTE_ADDR (BGP4_RT_PEER_ENTRY (pRtProfile)),
                sizeof (UINT4));
        bgp4RibUnlock ();
        return SNMP_SUCCESS;
    }

    bgp4RibUnlock ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexBgp4PathAttrTable
 Input       :  The Indices
                Bgp4PathAttrIpAddrPrefix
                nextBgp4PathAttrIpAddrPrefix
                Bgp4PathAttrIpAddrPrefixLen
                nextBgp4PathAttrIpAddrPrefixLen
                Bgp4PathAttrPeer
                nextBgp4PathAttrPeer
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexBgp4PathAttrTable (UINT4 u4Bgp4PathAttrIpAddrPrefix,
                                  UINT4 *pu4NextBgp4PathAttrIpAddrPrefix,
                                  INT4 i4Bgp4PathAttrIpAddrPrefixLen,
                                  INT4 *pi4NextBgp4PathAttrIpAddrPrefixLen,
                                  UINT4 u4Bgp4PathAttrPeer,
                                  UINT4 *pu4NextBgp4PathAttrPeer)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tRouteProfile      *pFndRtProfile = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile       RtProfile;
    tRouteProfile      *pNextRtProfile = NULL;
    tBgp4Info           RtInfo;
    VOID               *pRibNode = NULL;
    tAddrPrefix         PeerAddrInfo;
    INT4                i4RetVal = BGP4_FAILURE;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    Bgp4InitBgp4info (&RtInfo);

    Bgp4InitAddrPrefixStruct (&PeerAddrInfo, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddrInfo),
                  u4Bgp4PathAttrPeer);

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
    {
        if ((PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pPeer), PeerAddrInfo))
            == BGP4_TRUE)
        {
            break;
        }
    }

    if (pPeer == NULL)
    {
        return SNMP_FAILURE;
    }

    Bgp4InitNetAddressStruct (&(RtProfile.NetAddress),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                  (RtProfile.NetAddress.NetAddr), u4Bgp4PathAttrIpAddrPrefix);
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtProfile.NetAddress) =
        (UINT2) i4Bgp4PathAttrIpAddrPrefixLen;
    BGP4_RT_PEER_ENTRY ((&RtProfile)) = pPeer;
    BGP4_RT_BGP4_INF ((&RtProfile)) = &RtInfo;

    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = Bgp4RibhLookupRtEntry (&RtProfile, u4Context,
                                      BGP4_TREE_FIND_EXACT,
                                      &pFndRtProfile, &pRibNode);
    if (i4RetVal == BGP4_FAILURE)
    {
        /* No matching route. */
        bgp4RibUnlock ();
        return SNMP_FAILURE;
    }

    pRtProfile = Bgp4DshGetPeerRtFromProfileList (pFndRtProfile, pPeer);
    if (pRtProfile != NULL)
    {
        /* Peer route is found. Get the next route. */
        pRtProfile = BGP4_RT_NEXT (pRtProfile);
    }

    for (;;)
    {
        if (pRtProfile != NULL)
        {
            if ((BGP4_RT_PEER_ENTRY (pRtProfile) != NULL) &&
                ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_HISTORY) !=
                 BGP4_RT_HISTORY) &&
                (BGP4_AFI_IN_ADDR_PREFIX_INFO
                 (BGP4_PEER_REMOTE_ADDR_INFO (BGP4_RT_PEER_ENTRY (pRtProfile)))
                 == BGP4_INET_AFI_IPV4))
            {
                break;
            }

            if (BGP4_RT_PEER_ENTRY (pRtProfile) == NULL)
            {
                /* Non - BGP Route. Get the BGP Route. */
                pNextRtProfile = BGP4_RT_NEXT (pRtProfile);
                if (pNextRtProfile != NULL)
                {
                    pRtProfile = pNextRtProfile;
                    pNextRtProfile = NULL;
                    continue;
                }
            }
        }

        pNextRtProfile = NULL;
        /* Route is a History/Last route. History routes can never be used
         * for Index and any routes following this route in this RIB node
         * will also be history. So get the next route entry from the RIB */
        i4RetVal = Bgp4RibhGetNextEntry (pFndRtProfile, u4Context,
                                         &pNextRtProfile, &pRibNode, TRUE);
        if (i4RetVal == BGP4_FAILURE)
        {
            pRtProfile = NULL;
            break;
        }
        pFndRtProfile = pNextRtProfile;
        pRtProfile = pNextRtProfile;
        pNextRtProfile = NULL;
    }

    if (pRtProfile != NULL)
    {
        /* Next route found. */
        MEMCPY (pu4NextBgp4PathAttrIpAddrPrefix,
                BGP4_RT_IP_PREFIX (pRtProfile), sizeof (UINT4));
        *pi4NextBgp4PathAttrIpAddrPrefixLen = BGP4_RT_PREFIXLEN (pRtProfile);
        MEMCPY (pu4NextBgp4PathAttrPeer,
                BGP4_PEER_REMOTE_ADDR (BGP4_RT_PEER_ENTRY (pRtProfile)),
                sizeof (UINT4));

        bgp4RibUnlock ();
        return SNMP_SUCCESS;
    }

    bgp4RibUnlock ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetRouteProfile
 Input       :  
                pu4NextBgp4PathAttrIpAddrPrefix
                pi4NextBgp4PathAttrIpAddrPrefixLen
                pu4NextBgp4PathAttrPeer
                pPeer
 Output      :  None
 Returns     :  BGP4_TRUE or BGP4_FALSE
****************************************************************************/
INT1
nmhGetRouteProfile (UINT4 *pu4NextBgp4PathAttrIpAddrPrefix,
                    INT4 *pi4NextBgp4PathAttrIpAddrPrefixLen,
                    UINT4 *pu4NextBgp4PathAttrPeer, tBgp4PeerEntry * pPeer)
{
    tRouteProfile      *pRtProfile = NULL;
    tBgp4Info          *pRtInfo = NULL;
    tBgp4PeerEntry     *pNextPeer = NULL;
    UINT2               u2Afi;
    INT1                i1RetVal;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (pPeer == NULL)
    {
        return BGP4_FALSE;
    }

    u2Afi = BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
    if (u2Afi != BGP4_INET_AFI_IPV4)
    {
        return BGP4_FALSE;
    }

    /* scan the RIB tree for the peer */
    pRtProfile = Bgp4RibhGetFirstAsafiPeerRtEntry (pPeer, CAP_MP_IPV4_UNICAST);
    for (;;)
    {
        if ((pRtProfile == NULL) ||
            ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_HISTORY) !=
             BGP4_RT_HISTORY))
        {
            break;
        }

        /* Route is a History route. This can never be a Index. Get the
         * next route from the peer */
        pRtProfile = Bgp4RibhGetNextIpv4PeerRtEntry (pRtProfile, pPeer);
    }

    if (pRtProfile != NULL)
    {
        pRtInfo = BGP4_RT_BGP_INFO (pRtProfile);
        if (pRtInfo != NULL)
        {
            MEMCPY (pu4NextBgp4PathAttrIpAddrPrefix,
                    BGP4_RT_IP_PREFIX (pRtProfile), sizeof (UINT4));
            *pi4NextBgp4PathAttrIpAddrPrefixLen =
                BGP4_RT_PREFIXLEN (pRtProfile);
            MEMCPY (pu4NextBgp4PathAttrPeer,
                    BGP4_PEER_REMOTE_ADDR (pPeer), sizeof (UINT4));
            return BGP4_TRUE;
        }
    }

    /* Get the next peer from peer list */
    do
    {
        pNextPeer =
            (tBgp4PeerEntry *) TMO_SLL_Next (BGP4_PEERENTRY_HEAD (u4Context),
                                             &pPeer->TsnNextpeer);
        u2Afi =
            BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
        if (u2Afi == BGP4_INET_AFI_IPV4)
        {
            break;
        }
        pPeer = pNextPeer;

    }
    while (pPeer != NULL);

    i1RetVal = nmhGetRouteProfile (pu4NextBgp4PathAttrIpAddrPrefix,
                                   pi4NextBgp4PathAttrIpAddrPrefixLen,
                                   pu4NextBgp4PathAttrPeer, pNextPeer);
    return (i1RetVal);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetBgp4PathAttrOrigin
 Input       :  The Indices
                Bgp4PathAttrIpAddrPrefix
                Bgp4PathAttrIpAddrPrefixLen
                Bgp4PathAttrPeer

                The Object 
                retValBgp4PathAttrOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgp4PathAttrOrigin (UINT4 u4Bgp4PathAttrIpAddrPrefix,
                          INT4 i4Bgp4PathAttrIpAddrPrefixLen,
                          UINT4 u4Bgp4PathAttrPeer,
                          INT4 *pi4RetValBgp4PathAttrOrigin)
{
    tRouteProfile      *pMatchRoute = NULL;
    tNetAddress         NetAddress;
    tAddrPrefix         PeerAddress;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    Bgp4InitNetAddressStruct (&NetAddress,
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress)),
                  u4Bgp4PathAttrIpAddrPrefix);
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress) =
        (UINT2) i4Bgp4PathAttrIpAddrPrefixLen;
    Bgp4InitAddrPrefixStruct (&PeerAddress, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddress),
                  u4Bgp4PathAttrPeer);
    pMatchRoute = Bgp4GetRoute (u4Context, NetAddress, PeerAddress, NULL);
    if ((pMatchRoute == NULL) ||
        ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_HISTORY) ==
         BGP4_RT_HISTORY))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Route exist.\n");
        bgp4RibUnlock ();
        return SNMP_FAILURE;
    }

    /* From SNMP view - Origin : 1,2,3. Actual Protocol uses - 0,1,2 */
    *pi4RetValBgp4PathAttrOrigin =
        (INT4) (BGP4_INFO_ORIGIN (BGP4_RT_BGP_INFO (pMatchRoute))) +
        BGP4_INCREMENT_BY_ONE;
    bgp4RibUnlock ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgp4PathAttrASPathSegment
 Input       :  The Indices
                Bgp4PathAttrIpAddrPrefix
                Bgp4PathAttrIpAddrPrefixLen
                Bgp4PathAttrPeer

                The Object 
                retValBgp4PathAttrASPathSegment
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgp4PathAttrASPathSegment (UINT4 u4Bgp4PathAttrIpAddrPrefix,
                                 INT4 i4Bgp4PathAttrIpAddrPrefixLen,
                                 UINT4 u4Bgp4PathAttrPeer,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValBgp4PathAttrASPathSegment)
{
    tRouteProfile      *pMatchRoute = NULL;
    tTMO_SLL           *pAsPathList = NULL;
    tAsPath            *pAsPath = NULL;
    tNetAddress         NetAddress;
    tAddrPrefix         PeerAddress;
    UINT1              *pu1AsNos = NULL;
    UINT1               au1Tmp[BGP4_MAX_TEMP_ASARRAY_SIZE];
    INT4                i4BytesFilled = 0;
    UINT1               u1AsCnt = 0;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    Bgp4InitNetAddressStruct (&NetAddress,
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress)),
                  u4Bgp4PathAttrIpAddrPrefix);
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress) =
        (UINT2) i4Bgp4PathAttrIpAddrPrefixLen;
    Bgp4InitAddrPrefixStruct (&PeerAddress, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddress),
                  u4Bgp4PathAttrPeer);
    pMatchRoute = Bgp4GetRoute (u4Context, NetAddress, PeerAddress, NULL);

    if ((pMatchRoute == NULL) ||
        ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_HISTORY) ==
         BGP4_RT_HISTORY))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Route exist.\n");
        pRetValBgp4PathAttrASPathSegment->i4_Length = i4BytesFilled;
        bgp4RibUnlock ();
        return SNMP_FAILURE;
    }
    pAsPathList = BGP4_INFO_ASPATH (BGP4_RT_BGP_INFO (pMatchRoute));
    TMO_SLL_Scan (pAsPathList, pAsPath, tAsPath *)
    {
        if ((i4BytesFilled + BGP4_ATTR_PATH_TYPE_LEN + BGP4_ATTR_PATH_LEN_LEN +
             (BGP4_ASPATH_LEN (pAsPath) * BGP4_AS_LENGTH)) >
            (BGP4_MAX_TEMP_ASARRAY_SIZE - 1))
        {
            /* ran out of space in the AS Path array */
            break;
        }
        au1Tmp[i4BytesFilled] = BGP4_ASPATH_TYPE (pAsPath);
        i4BytesFilled++;
        u1AsCnt = BGP4_ASPATH_LEN (pAsPath);
        au1Tmp[i4BytesFilled] = u1AsCnt;
        i4BytesFilled++;
        pu1AsNos = BGP4_ASPATH_NOS (pAsPath);
        u1AsCnt = (UINT1) (u1AsCnt * BGP4_AS_LENGTH);
        while (u1AsCnt > 0)
        {
            au1Tmp[i4BytesFilled] = *pu1AsNos;
            pu1AsNos++;
            u1AsCnt--;
            i4BytesFilled++;
        }
    }
    au1Tmp[i4BytesFilled] = '\0';
    pRetValBgp4PathAttrASPathSegment->i4_Length = i4BytesFilled;
    MEMCPY (pRetValBgp4PathAttrASPathSegment->pu1_OctetList, au1Tmp,
            i4BytesFilled);
    bgp4RibUnlock ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgp4PathAttrNextHop
 Input       :  The Indices
                Bgp4PathAttrIpAddrPrefix
                Bgp4PathAttrIpAddrPrefixLen
                Bgp4PathAttrPeer

                The Object 
                retValBgp4PathAttrNextHop
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgp4PathAttrNextHop (UINT4 u4Bgp4PathAttrIpAddrPrefix,
                           INT4 i4Bgp4PathAttrIpAddrPrefixLen,
                           UINT4 u4Bgp4PathAttrPeer,
                           UINT4 *pu4RetValBgp4PathAttrNextHop)
{
    tRouteProfile      *pMatchRoute = NULL;
    tNetAddress         NetAddress;
    tAddrPrefix         PeerAddress;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    Bgp4InitNetAddressStruct (&NetAddress,
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress)),
                  u4Bgp4PathAttrIpAddrPrefix);
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress) =
        (UINT2) i4Bgp4PathAttrIpAddrPrefixLen;
    Bgp4InitAddrPrefixStruct (&PeerAddress, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddress),
                  u4Bgp4PathAttrPeer);
    pMatchRoute = Bgp4GetRoute (u4Context, NetAddress, PeerAddress, NULL);

    if ((pMatchRoute == NULL) ||
        ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_HISTORY) ==
         BGP4_RT_HISTORY))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Route exist.\n");
        *pu4RetValBgp4PathAttrNextHop = 0;
        bgp4RibUnlock ();
        return SNMP_FAILURE;
    }
    MEMCPY (pu4RetValBgp4PathAttrNextHop,
            BGP4_INFO_NEXTHOP (BGP4_RT_BGP_INFO (pMatchRoute)), sizeof (UINT4));
    bgp4RibUnlock ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgp4PathAttrMultiExitDisc
 Input       :  The Indices
                Bgp4PathAttrIpAddrPrefix
                Bgp4PathAttrIpAddrPrefixLen
                Bgp4PathAttrPeer

                The Object 
                retValBgp4PathAttrMultiExitDisc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgp4PathAttrMultiExitDisc (UINT4 u4Bgp4PathAttrIpAddrPrefix,
                                 INT4 i4Bgp4PathAttrIpAddrPrefixLen,
                                 UINT4 u4Bgp4PathAttrPeer,
                                 INT4 *pi4RetValBgp4PathAttrMultiExitDisc)
{
    tRouteProfile      *pMatchRoute = NULL;
    tBgp4Info          *pBgp4Info = NULL;
    tNetAddress         NetAddress;
    tAddrPrefix         PeerAddress;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    Bgp4InitNetAddressStruct (&NetAddress,
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress)),
                  u4Bgp4PathAttrIpAddrPrefix);
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress) =
        (UINT2) i4Bgp4PathAttrIpAddrPrefixLen;
    Bgp4InitAddrPrefixStruct (&PeerAddress, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddress),
                  u4Bgp4PathAttrPeer);
    pMatchRoute = Bgp4GetRoute (u4Context, NetAddress, PeerAddress, NULL);
    if ((pMatchRoute == NULL) ||
        ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_HISTORY) ==
         BGP4_RT_HISTORY))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Route exist.\n");
        *pi4RetValBgp4PathAttrMultiExitDisc = 0;
        bgp4RibUnlock ();
        return SNMP_FAILURE;
    }
    pBgp4Info = BGP4_RT_BGP_INFO (pMatchRoute);
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_MED_MASK)
        == BGP4_ATTR_MED_MASK)
    {
        *pi4RetValBgp4PathAttrMultiExitDisc =
            (INT4) (BGP4_INFO_RCVD_MED (pBgp4Info));
    }
    else
    {
        *pi4RetValBgp4PathAttrMultiExitDisc = BGP4_INVALID_METRIC;
    }
    bgp4RibUnlock ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgp4PathAttrLocalPref
 Input       :  The Indices
                Bgp4PathAttrIpAddrPrefix
                Bgp4PathAttrIpAddrPrefixLen
                Bgp4PathAttrPeer

                The Object 
                retValBgp4PathAttrLocalPref
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgp4PathAttrLocalPref (UINT4 u4Bgp4PathAttrIpAddrPrefix,
                             INT4 i4Bgp4PathAttrIpAddrPrefixLen,
                             UINT4 u4Bgp4PathAttrPeer,
                             INT4 *pi4RetValBgp4PathAttrLocalPref)
{
    tRouteProfile      *pMatchRoute = NULL;
    tBgp4Info          *pBgp4Info = NULL;
    tNetAddress         NetAddress;
    tAddrPrefix         PeerAddress;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    Bgp4InitNetAddressStruct (&NetAddress,
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress)),
                  u4Bgp4PathAttrIpAddrPrefix);
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress) =
        (UINT2) i4Bgp4PathAttrIpAddrPrefixLen;
    Bgp4InitAddrPrefixStruct (&PeerAddress, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddress),
                  u4Bgp4PathAttrPeer);
    pMatchRoute = Bgp4GetRoute (u4Context, NetAddress, PeerAddress, NULL);
    if ((pMatchRoute == NULL) ||
        ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_HISTORY) ==
         BGP4_RT_HISTORY))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Route exist.\n");
        *pi4RetValBgp4PathAttrLocalPref = 0;
        bgp4RibUnlock ();
        return SNMP_FAILURE;
    }
    pBgp4Info = BGP4_RT_BGP_INFO (pMatchRoute);
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_LOCAL_PREF_MASK)
        == BGP4_ATTR_LOCAL_PREF_MASK)
    {
        *pi4RetValBgp4PathAttrLocalPref =
            (INT4) (BGP4_INFO_RCVD_LOCAL_PREF (pBgp4Info));
    }
    else
    {
        *pi4RetValBgp4PathAttrLocalPref = BGP4_INVALID_METRIC;
    }
    bgp4RibUnlock ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgp4PathAttrAtomicAggregate
 Input       :  The Indices
                Bgp4PathAttrIpAddrPrefix
                Bgp4PathAttrIpAddrPrefixLen
                Bgp4PathAttrPeer

                The Object 
                retValBgp4PathAttrAtomicAggregate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgp4PathAttrAtomicAggregate (UINT4 u4Bgp4PathAttrIpAddrPrefix,
                                   INT4 i4Bgp4PathAttrIpAddrPrefixLen,
                                   UINT4 u4Bgp4PathAttrPeer,
                                   INT4 *pi4RetValBgp4PathAttrAtomicAggregate)
{
    tRouteProfile      *pMatchRoute = NULL;
    tNetAddress         NetAddress;
    tAddrPrefix         PeerAddress;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    Bgp4InitNetAddressStruct (&NetAddress,
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress)),
                  u4Bgp4PathAttrIpAddrPrefix);
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress) =
        (UINT2) i4Bgp4PathAttrIpAddrPrefixLen;
    Bgp4InitAddrPrefixStruct (&PeerAddress, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddress),
                  u4Bgp4PathAttrPeer);
    pMatchRoute = Bgp4GetRoute (u4Context, NetAddress, PeerAddress, NULL);
    if ((pMatchRoute == NULL) ||
        ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_HISTORY) ==
         BGP4_RT_HISTORY))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Route exist.\n");
        *pi4RetValBgp4PathAttrAtomicAggregate = 0;
        bgp4RibUnlock ();
        return SNMP_FAILURE;
    }
    if ((BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pMatchRoute))
         & BGP4_ATTR_ATOMIC_AGGR_MASK) == BGP4_ATTR_ATOMIC_AGGR_MASK)
    {
        *pi4RetValBgp4PathAttrAtomicAggregate =
            BGP4_LESS_SPECIFIC_ROUTE_SELECTED;
        bgp4RibUnlock ();
        return SNMP_SUCCESS;
    }
    *pi4RetValBgp4PathAttrAtomicAggregate =
        BGP4_LESS_SPECIFIC_ROUTE_NOT_SELECTED;
    bgp4RibUnlock ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgp4PathAttrAggregatorAS
 Input       :  The Indices
                Bgp4PathAttrIpAddrPrefix
                Bgp4PathAttrIpAddrPrefixLen
                Bgp4PathAttrPeer

                The Object 
                retValBgp4PathAttrAggregatorAS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgp4PathAttrAggregatorAS (UINT4 u4Bgp4PathAttrIpAddrPrefix,
                                INT4 i4Bgp4PathAttrIpAddrPrefixLen,
                                UINT4 u4Bgp4PathAttrPeer,
                                INT4 *pi4RetValBgp4PathAttrAggregatorAS)
{
    tRouteProfile      *pMatchRoute = NULL;
    tBgp4Info          *pBgp4Info = NULL;
    tNetAddress         NetAddress;
    tAddrPrefix         PeerAddress;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    Bgp4InitNetAddressStruct (&NetAddress,
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress)),
                  u4Bgp4PathAttrIpAddrPrefix);
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress) =
        (UINT2) i4Bgp4PathAttrIpAddrPrefixLen;
    Bgp4InitAddrPrefixStruct (&PeerAddress, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddress),
                  u4Bgp4PathAttrPeer);
    pMatchRoute = Bgp4GetRoute (u4Context, NetAddress, PeerAddress, NULL);
    if ((pMatchRoute == NULL) ||
        ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_HISTORY) ==
         BGP4_RT_HISTORY))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching route exist.\n");
        *pi4RetValBgp4PathAttrAggregatorAS = 0;
        bgp4RibUnlock ();
        return SNMP_FAILURE;
    }
    pBgp4Info = BGP4_RT_BGP_INFO (pMatchRoute);
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_AGGREGATOR_MASK)
        == BGP4_ATTR_AGGREGATOR_MASK)
    {
        *pi4RetValBgp4PathAttrAggregatorAS =
            (INT4) (BGP4_INFO_AGGREGATOR_AS (pBgp4Info));
    }
    else
    {
        *pi4RetValBgp4PathAttrAggregatorAS = 0;
    }

    bgp4RibUnlock ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgp4PathAttrAggregatorAddr
 Input       :  The Indices
                Bgp4PathAttrIpAddrPrefix
                Bgp4PathAttrIpAddrPrefixLen
                Bgp4PathAttrPeer

                The Object 
                retValBgp4PathAttrAggregatorAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgp4PathAttrAggregatorAddr (UINT4 u4Bgp4PathAttrIpAddrPrefix,
                                  INT4 i4Bgp4PathAttrIpAddrPrefixLen,
                                  UINT4 u4Bgp4PathAttrPeer,
                                  UINT4 *pu4RetValBgp4PathAttrAggregatorAddr)
{
    tRouteProfile      *pMatchRoute = NULL;
    tBgp4Info          *pBgp4Info = NULL;
    tNetAddress         NetAddress;
    tAddrPrefix         PeerAddress;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    Bgp4InitNetAddressStruct (&NetAddress,
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress)),
                  u4Bgp4PathAttrIpAddrPrefix);
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress) =
        (UINT2) i4Bgp4PathAttrIpAddrPrefixLen;
    Bgp4InitAddrPrefixStruct (&PeerAddress, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddress),
                  u4Bgp4PathAttrPeer);
    pMatchRoute = Bgp4GetRoute (u4Context, NetAddress, PeerAddress, NULL);
    if ((pMatchRoute == NULL) ||
        ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_HISTORY) ==
         BGP4_RT_HISTORY))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching route exist.\n");
        *pu4RetValBgp4PathAttrAggregatorAddr = 0;
        bgp4RibUnlock ();
        return SNMP_FAILURE;
    }
    pBgp4Info = BGP4_RT_BGP_INFO (pMatchRoute);
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_AGGREGATOR_MASK)
        == BGP4_ATTR_AGGREGATOR_MASK)
    {
        *pu4RetValBgp4PathAttrAggregatorAddr =
            (UINT4) (BGP4_INFO_AGGREGATOR_NODE (pBgp4Info));
    }
    else
    {
        *pu4RetValBgp4PathAttrAggregatorAddr = 0;
    }
    bgp4RibUnlock ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgp4PathAttrCalcLocalPref
 Input       :  The Indices
                Bgp4PathAttrIpAddrPrefix
                Bgp4PathAttrIpAddrPrefixLen
                Bgp4PathAttrPeer

                The Object 
                retValBgp4PathAttrCalcLocalPref
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgp4PathAttrCalcLocalPref (UINT4 u4Bgp4PathAttrIpAddrPrefix,
                                 INT4 i4Bgp4PathAttrIpAddrPrefixLen,
                                 UINT4 u4Bgp4PathAttrPeer,
                                 INT4 *pi4RetValBgp4PathAttrCalcLocalPref)
{
    tRouteProfile      *pMatchRoute = NULL;
    tNetAddress         NetAddress;
    tAddrPrefix         PeerAddress;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    Bgp4InitNetAddressStruct (&NetAddress,
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress)),
                  u4Bgp4PathAttrIpAddrPrefix);
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress) =
        (UINT2) i4Bgp4PathAttrIpAddrPrefixLen;
    Bgp4InitAddrPrefixStruct (&PeerAddress, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddress),
                  u4Bgp4PathAttrPeer);

    pMatchRoute = Bgp4GetRoute (u4Context, NetAddress, PeerAddress, NULL);
    if ((pMatchRoute == NULL) ||
        ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_HISTORY) ==
         BGP4_RT_HISTORY))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching route exist.\n");
        *pi4RetValBgp4PathAttrCalcLocalPref = BGP4_INVALID_METRIC;
        bgp4RibUnlock ();
        return SNMP_FAILURE;
    }
    *pi4RetValBgp4PathAttrCalcLocalPref =
        (INT4) (BGP4_RT_LOCAL_PREF (pMatchRoute));
    bgp4RibUnlock ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgp4PathAttrBest
 Input       :  The Indices
                Bgp4PathAttrIpAddrPrefix
                Bgp4PathAttrIpAddrPrefixLen
                Bgp4PathAttrPeer

                The Object 
                retValBgp4PathAttrBest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgp4PathAttrBest (UINT4 u4Bgp4PathAttrIpAddrPrefix,
                        INT4 i4Bgp4PathAttrIpAddrPrefixLen,
                        UINT4 u4Bgp4PathAttrPeer,
                        INT4 *pi4RetValBgp4PathAttrBest)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tRouteProfile      *pRouteProfile = NULL;
    tRouteProfile      *pFndRtProfileList = NULL;
    VOID               *pRibNode = NULL;
    tRouteProfile       RtProfile;
    tRouteProfile      *pFindRtProfile = &RtProfile;
    tAddrPrefix         PathAttrPeer;
    INT4                i4RtFound = BGP4_FAILURE;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&PathAttrPeer, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PathAttrPeer),
                  u4Bgp4PathAttrPeer);
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PathAttrPeer);
    if (pPeer == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitNetAddressStruct ((&(RtProfile.NetAddress)),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                   (RtProfile.NetAddress)), u4Bgp4PathAttrIpAddrPrefix);
    BGP4_RT_PREFIXLEN (pFindRtProfile) = (UINT1) i4Bgp4PathAttrIpAddrPrefixLen;
    BGP4_RT_PROTOCOL (pFindRtProfile) = (UINT1) BGP_ID;
    BGP4_RT_GET_FLAGS (pFindRtProfile) = 0;

    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i4RtFound = Bgp4RibhLookupRtEntry (pFindRtProfile, u4Context,
                                       BGP4_TREE_FIND_EXACT,
                                       &pFndRtProfileList, &pRibNode);
    if (i4RtFound == BGP4_FAILURE)
    {
        /* At this point, the given input route is neither present 
         * in the Peer Input List and the BGP RIB TREE. Hence
         * return Failure
         */
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching route exist.\n");
        bgp4RibUnlock ();
        return SNMP_FAILURE;
    }

    pRouteProfile = Bgp4DshGetPeerRtFromProfileList (pFndRtProfileList, pPeer);
    if (pRouteProfile != NULL)
    {
        if ((BGP4_RT_GET_FLAGS (pRouteProfile) & BGP4_RT_BEST) == BGP4_RT_BEST)
        {
            *pi4RetValBgp4PathAttrBest = BGP4_BEST_ROUTE;
        }
        else
        {
            *pi4RetValBgp4PathAttrBest = BGP4_NOT_BEST_ROUTE;
        }
        bgp4RibUnlock ();
        return SNMP_SUCCESS;
    }
    else
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching route exist.\n");
        bgp4RibUnlock ();
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetBgp4PathAttrUnknown
 Input       :  The Indices
                Bgp4PathAttrIpAddrPrefix
                Bgp4PathAttrIpAddrPrefixLen
                Bgp4PathAttrPeer

                The Object 
                retValBgp4PathAttrUnknown
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgp4PathAttrUnknown (UINT4 u4Bgp4PathAttrIpAddrPrefix,
                           INT4 i4Bgp4PathAttrIpAddrPrefixLen,
                           UINT4 u4Bgp4PathAttrPeer,
                           tSNMP_OCTET_STRING_TYPE * pRetValBgp4PathAttrUnknown)
{
    tRouteProfile      *pMatchRoute = NULL;
    UINT1              *pCurrentUnknownInfo = NULL;
    tNetAddress         NetAddress;
    tAddrPrefix         PeerAddress;
    UINT2               u2UnknownInfoLen = 0;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    Bgp4InitNetAddressStruct (&NetAddress,
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress)),
                  u4Bgp4PathAttrIpAddrPrefix);
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress) =
        (UINT2) i4Bgp4PathAttrIpAddrPrefixLen;
    Bgp4InitAddrPrefixStruct (&PeerAddress, BGP4_INET_AFI_IPV4);
    PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddress),
                  u4Bgp4PathAttrPeer);
    pMatchRoute = Bgp4GetRoute (u4Context, NetAddress, PeerAddress, NULL);
    if ((pMatchRoute == NULL) ||
        ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_HISTORY) ==
         BGP4_RT_HISTORY))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching route exist.\n");
        bgp4RibUnlock ();
        return SNMP_FAILURE;
    }
    if ((BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pMatchRoute))
         & BGP4_ATTR_UNKNOWN_MASK) == BGP4_ATTR_UNKNOWN_MASK)
    {
        Bgp4GetUnknownBgpInfo (pMatchRoute, &pCurrentUnknownInfo,
                               &u2UnknownInfoLen);
        if (NULL != pCurrentUnknownInfo)
        {
            MEMCPY (pRetValBgp4PathAttrUnknown->pu1_OctetList,
                    pCurrentUnknownInfo, u2UnknownInfoLen);
            pRetValBgp4PathAttrUnknown->i4_Length = u2UnknownInfoLen;
            bgp4RibUnlock ();
            return SNMP_SUCCESS;
        }
    }
    pRetValBgp4PathAttrUnknown->i4_Length = 0;
    bgp4RibUnlock ();
    return SNMP_SUCCESS;
}

/****************************************************************************/
/* Function Name : Bgp4SnmphGetPeerEntry                                    */
/* Description   : Given an IP address of the Peer this routine returns the */
/*                 pointer to the corresponding BGP Peer entry.             */
/* Input(s)      : IP Address of the BGP Peer.                              */
/* Output(s)     : None.                                                    */
/* Return(s)     : Corresponding BGP Peer entry of the peer.                */
/****************************************************************************/
tBgp4PeerEntry     *
Bgp4SnmphGetPeerEntry (UINT4 u4Context, tAddrPrefix PeerAddress)
{
    tBgp4PeerEntry     *pPeer = NULL;

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
    {
        if ((PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                          PeerAddress)) == BGP4_TRUE)
        {
            break;
        }
    }
    return (pPeer);
}

/****************************************************************************/
/* Function Name : Bgp4SnmphGetDuplicatePeerEntry                           */
/* Description   : This routine checks whether any duplicate peer entry is  */
/*               : available for the given peer and if present returns that */
/*               : entry. Else returns NULL.                                */
/* Input(s)      : Peer for which duplicate entry needs to be found (pPeer) */
/* Output(s)     : None.                                                    */
/* Return(s)     : Corresponding BGP Peer entry of the peer.                */
/****************************************************************************/
tBgp4PeerEntry     *
Bgp4SnmphGetDuplicatePeerEntry (tBgp4PeerEntry * pPeer)
{
    tBgp4PeerEntry     *pDupPeer = NULL;
    UINT4               u4Context = pPeer->pBgpCxtNode->u4ContextId;
    BOOL1               bIsPeerFound = BGP4_FALSE;

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pDupPeer, tBgp4PeerEntry *)
    {
        if ((PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                          BGP4_PEER_REMOTE_ADDR_INFO (pDupPeer))) == BGP4_TRUE)
        {                        /* Both the entries Remote Address are same. */
            if (pDupPeer != pPeer)
            {                    /* Peer entries are different. Should be Duplicate peer */
                break;
            }
            /* Peer entries are identical. Get the next entry if
             * address does not match, then there is no duplicate peer */
            bIsPeerFound = BGP4_TRUE;
        }
        else
        {                        /* Remote Address does not match for both peer entries */
            if (bIsPeerFound == BGP4_TRUE)
            {
                /* Matching entry is found already. But next entry does
                 * not match for address. So no Duplicate peer exists. */
                return (NULL);
            }
        }
    }
    return (pDupPeer);
}

/*****************************************************************************/
/* Function Name : Bgp4SnmphDeletePeerEntry                                  */
/* Description   : Deletes a particular BGP Peer entry from the list of      */
/*               : Peers. Depanding upon the input value, either deletes all */
/*               : the peer entry in the list or delete only the specific    */
/*               : entry from the list.                                      */
/* Input(s)      : Peer Entry which needs to be removed(pPeer).              */
/* Output(s)     : None.                                                     */
/* Return(s)     : SNMP_SUCCESS/SNMP_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4SnmphDeletePeerEntry (tBgp4PeerEntry * pPeer)
{

    UINT4               u4Context = 0;

    u4Context = BGP4_PEER_CXT_ID (pPeer);

#ifdef L3VPN
    if (BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_PE_PEER)
    {
        Bgp4UpdateLabelInMplsFmTable (pPeer, BGP4_VPN4_UNI_INDEX,
                                      BGP4_LABEL_OPER_POP,
                                      BGP4_FM_LABEL_DELETE);
    }
    if (BGP4_PEER_NEG_ASAFI_MASK (pPeer) & CAP_NEG_LBL_IPV4_MASK)
    {
        Bgp4UpdateLabelInMplsFmTable (pPeer, BGP4_IPV4_LBLD_INDEX,
                                      BGP4_LABEL_OPER_POP,
                                      BGP4_FM_LABEL_DELETE);
    }
#endif

    /* update the rfl client count, if the peer is a client, then decrement
     * the counter
     */
    if (BGP4_PEER_RFL_CLIENT (pPeer) == CLIENT)
    {
        BGP4_RR_CLIENT_CNT (u4Context)--;
    }
    /* De-register with BFD, if BFD monitoring is enabled for this peer */
    if ((pPeer->u1BfdStatus == BGP4_BFD_ENABLE) &&
        (pPeer->b1IsBfdRegistered == BGP4_TRUE))
    {
        Bgp4DeRegisterWithBfd (BGP4_PEER_CXT_ID (pPeer), pPeer, TRUE);
    }

    TMO_SLL_Delete (BGP4_PEERENTRY_HEAD (u4Context), &pPeer->TsnNextpeer);
    BGP4_PEER_DATA_PEER_PTR (u4Context)[BGP4_PEER_ENTRY_INDEX (pPeer)] = NULL;
    if ((BGP4_PEER_ENTRY_INDEX (pPeer) ==
         (UINT1) BGP4_PEER_DATA_MAX_PEER (u4Context))
        && (BGP4_PEER_DATA_MAX_PEER (u4Context) != 0))
    {
        BGP4_PEER_DATA_MAX_PEER (u4Context) -= BGP4_DECREMENT_BY_ONE;
    }
    Bgp4ReleasePeerEntry (pPeer);
    BGP4_PEER_CNT (u4Context)--;
    return SNMP_SUCCESS;
}

/****************************************************************************/
/* Function Name : Bgp4SnmphAddPeerEntry                                    */
/* Description   : Adds a Peer entry into the BGP Peer Table for the peer   */
/*                 specified by the remote Address or the index.            */
/* Input(s)      : Index of the new entry                                   */
/* Output(s)     : None.                                                    */
/* Return(s)     : BGP Peer information if the operation is successful,     */
/*                 NULL  otherwise.                                         */
/****************************************************************************/
tBgp4PeerEntry     *
Bgp4SnmphAddPeerEntry (UINT4 u4Context, tAddrPrefix PeerAddress)
{
    tBgp4PeerEntry     *pPeerentry = NULL;
    tBgp4PeerEntry     *pPeer = NULL;
    tBgp4PeerEntry     *pPrevpeer = NULL;
    UINT4               u4Cnt;
    UINT4               u4BgpMaxPeers;
    INT4                i4Status;

    u4BgpMaxPeers = FsBGPSizingParams[MAX_BGP_PEERS_SIZING_ID].
        u4PreAllocatedUnits;

    if (BGP4_PEER_CNT (u4Context) >= u4BgpMaxPeers)
    {
        return (NULL);
    }
    pPeerentry = Bgp4AllocatePeerEntry (u4Context, sizeof (tBgp4PeerEntry));
    if (pPeerentry == NULL)
    {
        return (NULL);
    }

#ifdef ROUTEMAP_WANTED
    MEMSET (&(pPeerentry->FilterInRMap), 0, sizeof (tFilteringRMap));
    MEMSET (&(pPeerentry->FilterOutRMap), 0, sizeof (tFilteringRMap));
    MEMSET (&(pPeerentry->IpPrefixFilterIn), 0, sizeof (tFilteringRMap));
    MEMSET (&(pPeerentry->IpPrefixFilterOut), 0, sizeof (tFilteringRMap));

    MEMCPY (&(pPeerentry->shadowPeerConfig.FilterInRMap),
            &(pPeerentry->FilterInRMap), sizeof (tFilteringRMap));
    MEMCPY (&(pPeerentry->shadowPeerConfig.FilterOutRMap),
            &(pPeerentry->FilterOutRMap), sizeof (tFilteringRMap));
    MEMCPY (&(pPeerentry->shadowPeerConfig.IpPrefixFilterIn),
            &(pPeerentry->IpPrefixFilterIn), sizeof (tFilteringRMap));
    MEMCPY (&(pPeerentry->shadowPeerConfig.IpPrefixFilterOut),
            &(pPeerentry->IpPrefixFilterOut), sizeof (tFilteringRMap));
#endif

    BGP4_PEER_ADMIN_STATUS (pPeerentry) = BGP4_PEER_STOP;
    Bgp4CopyAddrPrefixStruct ((&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry))),
                              PeerAddress);

    pPrevpeer = pPeer = NULL;
    BGP4_CONFED_PEER_STATUS (pPeerentry) = BGP4_FALSE;
    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
    {
        if ((PrefixGreaterThan (BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                                BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)))
            == BGP4_TRUE)
        {
            break;
        }
        pPrevpeer = pPeer;
    }
    TMO_SLL_Insert (BGP4_PEERENTRY_HEAD (u4Context), &pPrevpeer->TsnNextpeer,
                    &pPeerentry->TsnNextpeer);
    BGP4_PEER_CNT (u4Context)++;
#ifdef ISS_WANTED
    if (MsrGetRestorationStatus () != ISS_TRUE)
    {
#endif
        /* Enable the MULTIPROTOCOL CAPABILITY 
         * Right now default - <IPV4, UNICAST, route-refresh> */
        if ((BGP4_DEFAULT_AFI_SAFI_FLAG (u4Context) == BGP4_TRUE) &&
            (BGP4_IPV4_AFI_FLAG (u4Context) == BGP4_TRUE))
        {
            i4Status =
                Bgp4SnmphEnableMPCapability (u4Context,
                                             BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry), BGP4_INET_AFI_IPV4,
                                             BGP4_INET_SAFI_UNICAST);
            if (i4Status == SNMP_FAILURE)
            {
                TMO_SLL_Delete (BGP4_PEERENTRY_HEAD (u4Context),
                                &pPeerentry->TsnNextpeer);
                Bgp4ReleasePeerEntry (pPeerentry);
                return (NULL);
            }
            /* Configure Route-Refresh capability for the peer */
            i4Status =
                Bgp4SnmphRtRefCapabilityEnable (u4Context,
                                                BGP4_PEER_REMOTE_ADDR_INFO
                                                (pPeerentry));
        }
#ifdef EVPN_WANTED
        pPeerentry->b1IsMPIpv4Enabled = BGP4_FALSE;
#endif
        /* If GR Admin status is enabled, then add Graceful restart capability to 
         * BGP4_PEER_SUP_CAPS_LIST */
        if (BGP4_GR_ADMIN_STATUS (u4Context) == BGP4_ENABLE_GR)
        {
            i4Status =
                Bgp4SnmphGrCapabilityEnable (u4Context,
                                             BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerentry));
            if (i4Status == SNMP_FAILURE)
            {
                TMO_SLL_Delete (BGP4_PEERENTRY_HEAD (u4Context),
                                &pPeerentry->TsnNextpeer);
                Bgp4ReleasePeerEntry (pPeerentry);
                return (NULL);
            }
        }
#ifdef ISS_WANTED
    }
#endif

    for (u4Cnt = 0; u4Cnt < u4BgpMaxPeers * BGP4_MAX_ENTRY_PER_PEER; u4Cnt++)
    {
        if (BGP4_PEER_DATA_PEER_PTR (u4Context)[u4Cnt] == NULL)
        {
            break;
        }
    }
    if (u4Cnt == (u4BgpMaxPeers * BGP4_MAX_ENTRY_PER_PEER))
    {
        return (pPeerentry);
    }
    BGP4_PEER_ENTRY_INDEX (pPeerentry) = (UINT2) u4Cnt;
    BGP4_PEER_DATA_PEER_PTR (u4Context)[u4Cnt] = pPeerentry;
    BGP4_PEER_RECVD_EOR_MARKER (pPeerentry) = BGP4_INVALID_STATUS;
    BGP4_EOR_RCVD (pPeerentry) = BGP4_INVALID_STATUS;
    BGP4_PEER_EOR_MARKER (pPeerentry) = BGP4_INVALID_STATUS;
    BGP4_PEER_SENT_EOR_MARKER (pPeerentry) = BGP4_INVALID_STATUS;
    BGP4_PEER_RESTART_INTERVAL (pPeerentry) = ZERO;
    BGP4_PEER_RESTART_MODE (pPeerentry) = BGP4_RESTART_MODE_NONE;
    /*TCP-AO initialized to Null */
    BGP4_PEER_TCPAO_MKT (pPeerentry) = NULL;

    BGP4_PEER_TCPAO_ICMPACCEPT (pPeerentry) = BGP4_TCPAO_ICMP_IGNORE;
    BGP4_PEER_TCPAO_NOMKTDISC (pPeerentry) = BGP4_TCPAO_NOMKT_DISCARD_TRUE;
    if (u4Cnt > BGP4_PEER_DATA_MAX_PEER (u4Context))
    {
        BGP4_PEER_DATA_MAX_PEER (u4Context) = u4Cnt;
    }
    return (pPeerentry);
}

/*****************************************************************************/
/* Function Name : Bgp4GetRoute                                              */
/* Description   : Gets the Route profile entry from the RIB Tree or from the*/
/*                 Input list with the matching informations as given.       */
/* Input(s)      : Destination IP Address (u4IpPrefix),                      */
/*                 Prefix Length (u1PrefixLen),                              */
/*                 IP Address of the Peer (u4PeerAddr)                       */
/*                 pRouteRibNode - RIB node in which this route is present.  */
/*                                 NULL if caller does not know this node.   */
/* Output(s)     : None.                                                     */
/* Return(s)     : Route Profile if a matching entry is found,               */
/*                 NULL if not.                                              */
/*****************************************************************************/
tRouteProfile      *
Bgp4GetRoute (UINT4 u4Context, tNetAddress GetRtAddress, tAddrPrefix PeerAddr,
              VOID *pRouteRibNode)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile       FindRtProfile;
    VOID               *pRibNode = NULL;
    INT4                i4RtFound;
    UINT4               u4PfxLen;
    UINT4               u4AsafiMask;

    pRibNode = pRouteRibNode;
    MEMSET (&FindRtProfile, 0, sizeof (tRouteProfile));
    pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerAddr);
    if (pPeerEntry == NULL)
    {
        return ((tRouteProfile *) NULL);
    }

    u4PfxLen = GetRtAddress.u2PrefixLen;
    BGP4_GET_AFISAFI_MASK ((BGP4_AFI_IN_ADDR_PREFIX_INFO
                            (GetRtAddress.NetAddr)),
                           BGP4_SAFI_IN_NET_ADDRESS_INFO (GetRtAddress),
                           u4AsafiMask);

    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
        {
            UINT4               u4Addr = 0;
            UINT4               u4Mask = 0;

            u4Mask = Bgp4GetSubnetmask ((UINT1) u4PfxLen);
            PTR_FETCH_4 (u4Addr,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (GetRtAddress.
                                                                NetAddr));
            u4Addr = u4Addr & u4Mask;
            PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                          (GetRtAddress.NetAddr), u4Addr);
            break;
        }
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
        {
            tIp6Addr            Address;
            UINT1               u1CopyBytes = 0;

            MEMSET (Address.u1_addr, 0, BGP4_IPV6_PREFIX_LEN);
            u1CopyBytes = Bgp4RoundToBytes ((UINT2) u4PfxLen);
            if (u1CopyBytes < BGP4_MAX_INET_ADDRESS_LEN)
            {
                MEMCPY (Address.u1_addr, GetRtAddress.NetAddr.au1Address,
                        u1CopyBytes);
            }
            MEMCPY (Address.u1_addr, GetRtAddress.NetAddr.au1Address,
                    BGP4_IPV6_PREFIX_LEN);
            break;
        }
#endif
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
        {
            UINT4               u4Addr = 0;
            UINT4               u4Mask = 0;

            u4Mask = Bgp4GetSubnetmask ((UINT1) u4PfxLen);
            PTR_FETCH_4 (u4Addr,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (GetRtAddress.
                                                                NetAddr));
            u4Addr = u4Addr & u4Mask;
            PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                          (GetRtAddress.NetAddr), u4Addr);
            break;
        }

        case CAP_MP_VPN4_UNICAST:

        {
            UINT4               u4Addr = 0;
            UINT4               u4Mask = 0;

            u4Mask = Bgp4GetSubnetmask ((UINT1) u4PfxLen);
            MEMCPY (BGP4_RT_ROUTE_DISTING ((&FindRtProfile)),
                    BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (GetRtAddress.
                                                           NetAddr),
                    BGP4_VPN4_ROUTE_DISTING_SIZE);
            PTR_FETCH_4 (u4Addr,
                         (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                          (GetRtAddress.NetAddr)) +
                         BGP4_VPN4_ROUTE_DISTING_SIZE);
            u4Addr = u4Addr & u4Mask;
            PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                          (GetRtAddress.NetAddr), u4Addr);
            break;
        }
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
        {

            /* Here we are creating a key to search in trie data structure
             * For L2VPN-VPLS the key is RD + VE-ID + VBO
             * And these are the sub-field of NLRI info which is present in 
             * GetRtAddress.NetAddr.tAddrPrefix.au1Address 
             * So no need to do anything here.*/
            break;
        }
#endif
        default:
            /* <AFI,SAFI> not supported. */
            return ((tRouteProfile *) NULL);
    }

    Bgp4CopyNetAddressStruct (&(FindRtProfile.NetAddress), GetRtAddress);
    BGP4_RT_PROTOCOL ((&FindRtProfile)) = (UINT1) BGP_ID;
    BGP4_RT_GET_FLAGS ((&FindRtProfile)) = 0;

    /* Search the RIB tree */
    i4RtFound = Bgp4RibhLookupRtEntry (&FindRtProfile, u4Context,
                                       BGP4_TREE_FIND_EXACT,
                                       &pRtProfile, &pRibNode);
    if (i4RtFound == BGP4_SUCCESS)
    {
        pRtProfile = Bgp4DshGetPeerRtFromProfileList (pRtProfile, pPeerEntry);
        if (pRtProfile != NULL)
        {
            return pRtProfile;
        }
    }
    return ((tRouteProfile *) NULL);
}

/****************************************************************************/
/* Function Name : Bgp4SnmphClonePeerEntry                                  */
/* Description   : Creates an identical peer entry. This function is used   */
/*                 by the connection handler while resolving the collision. */
/* Input(s)      : Peer entry for which cloning needs to be done (pPeerentry)*/
/* Output(s)     : None.                                                    */
/* Return(s)     : New Cloned peer infor is returned if the operation is    */
/*                 successful, NULL otherwise                               */
/*****************************************************************************/
tBgp4PeerEntry     *
Bgp4SnmphClonePeerEntry (tBgp4PeerEntry * pPeerentry)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tBgp4PeerEntry     *pPeercur = NULL;
    tBgp4PeerEntry     *pPrevpeer = NULL;
    UINT4               u4Cnt;
    UINT4               u4BgpMaxPeers;
    UINT4               u4Context;

    /*Soma - u4Context should get from pPeerentry */
    u4Context = BGP4_PEER_CXT_ID (pPeerentry);

    u4BgpMaxPeers = FsBGPSizingParams[MAX_BGP_PEERS_SIZING_ID].
        u4PreAllocatedUnits;

    pPeer = Bgp4AllocatePeerEntry (u4Context, sizeof (tBgp4PeerEntry));
    if (pPeer == NULL)
    {
        return (NULL);
    }
    BGP4_PEER_LOCAL_AS (pPeer) = BGP4_PEER_LOCAL_AS (pPeerentry);
    BGP4_PEER_LOCAL_AS_CFG (pPeer) = BGP4_PEER_LOCAL_AS_CFG (pPeerentry);
    Bgp4SnmphDupPeerEntry (pPeer, pPeerentry);
    pPrevpeer = pPeercur = NULL;
    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeercur, tBgp4PeerEntry *)
    {
        if ((PrefixGreaterThan (BGP4_PEER_REMOTE_ADDR_INFO (pPeercur),
                                BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)))
            == BGP4_TRUE)
        {
            break;
        }
        pPrevpeer = pPeercur;
    }
    TMO_SLL_Insert (BGP4_PEERENTRY_HEAD (u4Context), &pPrevpeer->TsnNextpeer,
                    &pPeer->TsnNextpeer);

    BGP4_PEER_CNT (u4Context)++;
    for (u4Cnt = 0; u4Cnt < u4BgpMaxPeers * BGP4_MAX_ENTRY_PER_PEER; u4Cnt++)
    {
        if (BGP4_PEER_DATA_PEER_PTR (u4Context)[u4Cnt] == NULL)
        {
            break;
        }
    }
    if (u4Cnt == (u4BgpMaxPeers * BGP4_MAX_ENTRY_PER_PEER))
    {
        return (pPeer);
    }
    BGP4_PEER_ENTRY_INDEX (pPeer) = (UINT2) u4Cnt;
    BGP4_PEER_DATA_PEER_PTR (u4Context)[u4Cnt] = pPeer;
    if (u4Cnt > BGP4_PEER_DATA_MAX_PEER (u4Context))
    {
        BGP4_PEER_DATA_MAX_PEER (u4Context) = u4Cnt;
    }
    return (pPeer);
}

INT4
CommCopyCommunityToProfile (tCommProfile * pCommProfile,
                            UINT4 u4Fsbgp4AddCommVal)
{
    pCommProfile->u4Comm = u4Fsbgp4AddCommVal;
    return COMM_SUCCESS;
}

INT4
CommCmpCommunity (UINT4 u4Fsbgp4AddCommVal, tCommProfile * pCommProfile)
{
    if (u4Fsbgp4AddCommVal == pCommProfile->u4Comm)
    {
        return COMM_TRUE;
    }
    else
    {
        return COMM_FALSE;
    }
}

INT4
CommValidateCommunity (UINT4 u4Comm)
{
    if ((u4Comm == NO_EXPORT) ||
        (u4Comm == NO_ADVERTISE) ||
        (u4Comm == NO_EXPORT_SUBCONFED) ||
        ((u4Comm >= MIN_CONFIG_COMM_VAL) && (u4Comm <= MAX_CONFIG_COMM_VAL)))
    {
        return COMM_SUCCESS;
    }
    return COMM_FAILURE;
}

/****************************************************************************/
/* Function Name : RemoveLeadingSpaces                                      */
/* Description   : Converts the Input string to the Lower case equivalent   */
/* Input(s)      : Input String (pi1DisplayStr)                             */
/* Output(s)     : None.                                                    */
/* Return(s)     : The String with leading spaces removed                   */
/****************************************************************************/
INT1               *
RemoveLeadingSpaces (INT1 *pi1DisplayStr)
{
    while (ISSPACE (*pi1DisplayStr) != 0)    /* If space  continue */
    {
        pi1DisplayStr++;
    }
    return (pi1DisplayStr);
}

/******************************************************************************/
/* Function Name : Bgp4SnmphDupPeerEntry                                      */
/* Description   : copies all info from source peer to dest peer              */
/* Input(s)      : pDestPeer , pSrcPeer                                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                  */
/*****************************************************************************/
INT4
Bgp4SnmphDupPeerEntry (tBgp4PeerEntry * pDestPeer, tBgp4PeerEntry * pSrcPeer)
{
    INT1                i1Index = 0;
    INT4                i4RetSts;
#ifdef L3VPN
    tExtCommProfile    *pSooExtCom = NULL;
#endif

    /* Copy info from peerConfig */
    Bgp4CopyAddrPrefixStruct ((&(BGP4_PEER_REMOTE_ADDR_INFO (pDestPeer))),
                              BGP4_PEER_REMOTE_ADDR_INFO (pSrcPeer));
    Bgp4CopyNetAddressStruct ((&(BGP4_PEER_LOCAL_NETADDR_INFO (pDestPeer))),
                              BGP4_PEER_LOCAL_NETADDR_INFO (pSrcPeer));
    Bgp4CopyAddrPrefixStruct ((&(BGP4_PEER_GATEWAY_ADDR_INFO (pDestPeer))),
                              BGP4_PEER_GATEWAY_ADDR_INFO (pSrcPeer));
    Bgp4CopyAddrPrefixStruct ((&(BGP4_PEER_LINK_LOCAL_ADDR_INFO (pDestPeer))),
                              BGP4_PEER_LINK_LOCAL_ADDR_INFO (pSrcPeer));
    Bgp4CopyAddrPrefixStruct ((&(BGP4_PEER_NETWORK_ADDR_INFO (pDestPeer))),
                              BGP4_PEER_NETWORK_ADDR_INFO (pSrcPeer));
    Bgp4CopyNetAddressStruct ((&(BGP4_PEER_LCL_NETADDR_INFO (pDestPeer))),
                              BGP4_PEER_LCL_NETADDR_INFO (pSrcPeer));
    BGP4_PEER_SENDBUF (pDestPeer) = BGP4_PEER_SENDBUF (pSrcPeer);
    BGP4_PEER_RECVBUF (pDestPeer) = BGP4_PEER_RECVBUF (pSrcPeer);
    BGP4_PEER_CONN_RETRY_TIME (pDestPeer) =
        BGP4_PEER_CONN_RETRY_TIME (pSrcPeer);
    BGP4_PEER_HOLD_TIME (pDestPeer) = BGP4_PEER_HOLD_TIME (pSrcPeer);
    BGP4_PEER_ALLOW_AUTOMATIC_STOP (pDestPeer) =
        BGP4_PEER_ALLOW_AUTOMATIC_STOP (pSrcPeer);
    BGP4_PEER_CURRENT_IDLE_HOLD_TIME (pDestPeer) =
        BGP4_PEER_CURRENT_IDLE_HOLD_TIME (pSrcPeer);
    BGP4_DAMP_PEER_OSCILLATIONS (pDestPeer) =
        BGP4_DAMP_PEER_OSCILLATIONS (pSrcPeer);
    BGP4_SET_PEER_CURRENT_STATE ((pDestPeer),
                                 BGP4_GET_PEER_CURRENT_STATE (pSrcPeer));

    BGP4_PEER_ALLOW_AUTOMATIC_START (pDestPeer) =
        BGP4_PEER_ALLOW_AUTOMATIC_START (pSrcPeer);
    BGP4_PEER_DELAY_OPEN (pDestPeer) = BGP4_PEER_DELAY_OPEN (pSrcPeer);
    BGP4_PEER_IDLE_HOLD_TIME (pDestPeer) = BGP4_PEER_IDLE_HOLD_TIME (pSrcPeer);
    BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT (pDestPeer) =
        BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT (pSrcPeer);
    BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pDestPeer) =
        BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pSrcPeer);
    BGP4_PEER_DAMPED_STATUS (pDestPeer) = BGP4_PEER_DAMPED_STATUS (pSrcPeer);
    BGP4_PEER_DELAY_OPEN_TIME (pDestPeer) =
        BGP4_PEER_DELAY_OPEN_TIME (pSrcPeer);
    BGP4_PEER_KEEP_ALIVE_TIME (pDestPeer) =
        BGP4_PEER_KEEP_ALIVE_TIME (pSrcPeer);
    BGP4_PEER_MIN_AS_ORIG_TIME (pDestPeer) =
        BGP4_PEER_MIN_AS_ORIG_TIME (pSrcPeer);
    BGP4_PEER_MIN_ADV_TIME (pDestPeer) = BGP4_PEER_MIN_ADV_TIME (pSrcPeer);
    BGP4_PEER_ASNO (pDestPeer) = BGP4_PEER_ASNO (pSrcPeer);
    BGP4_PEER_ADMIN_STATUS (pDestPeer) = BGP4_PEER_ADMIN_STATUS (pSrcPeer);
    BGP4_PEER_AUTH_TYPE (pDestPeer) = BGP4_PEER_AUTH_TYPE (pSrcPeer);
    BGP4_PEER_HOPLIMIT (pDestPeer) = BGP4_PEER_HOPLIMIT (pSrcPeer);
    BGP4_PEER_EBGP_MULTIHOP (pDestPeer) = BGP4_PEER_EBGP_MULTIHOP (pSrcPeer);
    BGP4_PEER_SELF_NEXTHOP (pDestPeer) = BGP4_PEER_SELF_NEXTHOP (pSrcPeer);
    BGP4_PEER_OVERRIDE_CAPABILITY (pDestPeer) =
        BGP4_PEER_OVERRIDE_CAPABILITY (pSrcPeer);
    BGP4_CONFED_PEER_STATUS (pDestPeer) = BGP4_CONFED_PEER_STATUS (pSrcPeer);
    BGP4_PEER_LOCAL_ADDR_CONFIGURED (pDestPeer) =
        BGP4_PEER_LOCAL_ADDR_CONFIGURED (pSrcPeer);
    BGP4_PEER_RFL_CLIENT (pDestPeer) = BGP4_PEER_RFL_CLIENT (pSrcPeer);
    BGP4_PEER_COMM_SEND_STATUS (pDestPeer) =
        BGP4_PEER_COMM_SEND_STATUS (pSrcPeer);
    BGP4_PEER_ECOMM_SEND_STATUS (pDestPeer) =
        BGP4_PEER_ECOMM_SEND_STATUS (pSrcPeer);
    BGP4_PEER_DEF_ROUTE_ORIG_STATUS (pDestPeer) =
        BGP4_PEER_DEF_ROUTE_ORIG_STATUS (pSrcPeer);
    BGP4_PEER_HOLD_ADVT_ROUTES (pDestPeer) =
        BGP4_PEER_HOLD_ADVT_ROUTES (pSrcPeer);

    /* Copy info from peerStatus */
    BGP4_PEER_STATE (pDestPeer) = BGP4_PEER_STATE (pSrcPeer);
    BGP4_PEER_PREV_STATE (pDestPeer) = BGP4_PEER_PREV_STATE (pSrcPeer);

    /* Copy info from peerStatistics */
    BGP4_PEER_IN_UPDATES (pDestPeer) = BGP4_PEER_IN_UPDATES (pSrcPeer);
    BGP4_PEER_OUT_UPDATES (pDestPeer) = BGP4_PEER_OUT_UPDATES (pSrcPeer);
    BGP4_PEER_IN_MSGS (pDestPeer) = BGP4_PEER_IN_MSGS (pSrcPeer);
    BGP4_PEER_OUT_MSGS (pDestPeer) = BGP4_PEER_OUT_MSGS (pSrcPeer);
    BGP4_PEER_PREFIX_UPPER_LIMIT (pDestPeer) =
        BGP4_PEER_PREFIX_UPPER_LIMIT (pSrcPeer);
    BGP4_PEER_LAST_ERROR (pDestPeer) = BGP4_PEER_LAST_ERROR (pSrcPeer);
    BGP4_PEER_LAST_ERROR_SUB_CODE (pDestPeer)
        = BGP4_PEER_LAST_ERROR_SUB_CODE (pSrcPeer);
    BGP4_PEER_FSM_TRANS (pDestPeer) = BGP4_PEER_FSM_TRANS (pSrcPeer);
    BGP4_PEER_FSM_HIST_HEAD (pDestPeer) = BGP4_PEER_FSM_HIST_HEAD (pSrcPeer);
    BGP4_PEER_FSM_HIST_TAIL (pDestPeer) = BGP4_PEER_FSM_HIST_TAIL (pSrcPeer);
    for (i1Index = 0; i1Index < BGP4_FSM_HIST_MAX_SIZE; i1Index++)
    {
        (BGP4_PEER_FSM_TRANSITION_HIST (pDestPeer, i1Index)) =
            (BGP4_PEER_FSM_TRANSITION_HIST (pSrcPeer, i1Index));
    }
    BGP4_PEER_ESTAB_TIME (pDestPeer) = BGP4_PEER_ESTAB_TIME (pSrcPeer);
    BGP4_PEER_IN_UPDATE_ELAP_TIME (pDestPeer)
        = BGP4_PEER_IN_UPDATE_ELAP_TIME (pSrcPeer);

    /* Copy info from peerLocal */
    BGP4_PEER_START_TIME (pDestPeer) = BGP4_PEER_START_TIME (pSrcPeer);
    BGP4_PEER_BGP_ID (pDestPeer) = BGP4_PEER_BGP_ID (pSrcPeer);
    BGP4_PEER_LOCAL_PORT (pDestPeer) = BGP4_PEER_LOCAL_PORT (pSrcPeer);
    BGP4_PEER_REMOTE_PORT (pDestPeer) = BGP4_PEER_REMOTE_PORT (pSrcPeer);
    BGP4_PEER_CONN_ID (pDestPeer) = BGP4_PEER_CONN_ID (pSrcPeer);
    BGP4_PEER_NEG_HOLD_INT (pDestPeer) = BGP4_PEER_NEG_HOLD_INT (pSrcPeer);
    BGP4_PEER_NEG_KEEP_ALIVE (pDestPeer) = BGP4_PEER_NEG_KEEP_ALIVE (pSrcPeer);
    BGP4_PEER_NEG_VER (pDestPeer) = BGP4_PEER_NEG_VER (pSrcPeer);
    BGP4_PEER_NEG_CAP_MASK (pDestPeer) = BGP4_PEER_NEG_CAP_MASK (pSrcPeer);
    BGP4_PEER_NEG_ASAFI_MASK (pDestPeer) = BGP4_PEER_NEG_ASAFI_MASK (pSrcPeer);
    BGP4_PEER_SUP_ASAFI_MASK (pDestPeer) = BGP4_PEER_SUP_ASAFI_MASK (pSrcPeer);
    i4RetSts = Bgp4DuplicatePeerCaps (BGP4_PEER_SUP_CAPS_LIST (pSrcPeer),
                                      BGP4_PEER_SUP_CAPS_LIST (pDestPeer));
    if (i4RetSts != BGP4_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    i4RetSts = Bgp4DuplicatePeerCaps (BGP4_PEER_RCVD_CAPS_LIST (pSrcPeer),
                                      BGP4_PEER_RCVD_CAPS_LIST (pDestPeer));
    if (i4RetSts != BGP4_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Duplicate the <AFI, SAFI> instances here,
     * Right now we are
     * supporting only <IPV6, unicast> 
     * when we go for any other Address Family , 
     * the code should be taken care to accomodate that AFI also
     */
    /* Peer Counters */
    if (BGP4_PEER_IPV4_AFISAFI_INSTANCE (pSrcPeer) != NULL)
    {
        BGP4_PEER_IPV4_PREFIX_RCVD_CNT (pDestPeer) =
            BGP4_PEER_IPV4_PREFIX_RCVD_CNT (pSrcPeer);
        BGP4_PEER_IPV4_PREFIX_SENT_CNT (pDestPeer) =
            BGP4_PEER_IPV4_PREFIX_SENT_CNT (pSrcPeer);
        BGP4_PEER_IPV4_WITHDRAWS_RCVD_CNT (pDestPeer) =
            BGP4_PEER_IPV4_WITHDRAWS_RCVD_CNT (pSrcPeer);
        BGP4_PEER_IPV4_WITHDRAWS_SENT_CNT (pDestPeer) =
            BGP4_PEER_IPV4_WITHDRAWS_SENT_CNT (pSrcPeer);
        BGP4_PEER_IPV4_IN_PREFIXES_CNT (pDestPeer) =
            BGP4_PEER_IPV4_IN_PREFIXES_CNT (pSrcPeer);
        BGP4_PEER_IPV4_IN_PREFIXES_ACPTD_CNT (pDestPeer) =
            BGP4_PEER_IPV4_IN_PREFIXES_ACPTD_CNT (pSrcPeer);
        BGP4_PEER_IPV4_IN_PREFIXES_RJCTD_CNT (pDestPeer) =
            BGP4_PEER_IPV4_IN_PREFIXES_RJCTD_CNT (pSrcPeer);
        BGP4_PEER_IPV4_OUT_PREFIXES_CNT (pDestPeer) =
            BGP4_PEER_IPV4_OUT_PREFIXES_CNT (pSrcPeer);
        BGP4_PEER_IPV4_INVALID_MPE_UPDATE_CNT (pDestPeer) =
            BGP4_PEER_IPV4_INVALID_MPE_UPDATE_CNT (pSrcPeer);
        BGP4_RTREF_MSG_SENT_CTR (pDestPeer, BGP4_IPV4_UNI_INDEX) =
            BGP4_RTREF_MSG_SENT_CTR (pSrcPeer, BGP4_IPV4_UNI_INDEX);
        BGP4_RTREF_MSG_RCVD_CTR (pDestPeer, BGP4_IPV4_UNI_INDEX) =
            BGP4_RTREF_MSG_RCVD_CTR (pSrcPeer, BGP4_IPV4_UNI_INDEX);
        BGP4_RTREF_MSG_TXERR_CTR (pDestPeer, BGP4_IPV4_UNI_INDEX) =
            BGP4_RTREF_MSG_TXERR_CTR (pSrcPeer, BGP4_IPV4_UNI_INDEX);
        BGP4_RTREF_MSG_RCVD_INVALID_CTR (pDestPeer, BGP4_IPV4_UNI_INDEX) =
            BGP4_RTREF_MSG_RCVD_INVALID_CTR (pSrcPeer, BGP4_IPV4_UNI_INDEX);
        BGP4_RTREF_MSG_REQ_PEER (pDestPeer, BGP4_IPV4_UNI_INDEX) =
            BGP4_RTREF_MSG_REQ_PEER (pSrcPeer, BGP4_IPV4_UNI_INDEX);
    }

#ifdef L3VPN
    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pSrcPeer) != NULL)
    {
        if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pDestPeer) == NULL)
        {
            BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pDestPeer) =
                Bgp4MpeAllocateAfiSafiInstance (pDestPeer,
                                                BGP4_IPV4_LBLD_INDEX);
            if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pDestPeer) == NULL)
            {
                /* This funciton needs to be made to return SUCCESS/FAILRUE */
                return SNMP_FAILURE;
            }
        }

        BGP4_PEER_IPV4_LBLD_PREFIX_RCVD_CNT (pDestPeer) =
            BGP4_PEER_IPV4_LBLD_PREFIX_RCVD_CNT (pSrcPeer);
        BGP4_PEER_IPV4_LBLD_PREFIX_SENT_CNT (pDestPeer) =
            BGP4_PEER_IPV4_LBLD_PREFIX_SENT_CNT (pSrcPeer);
        BGP4_PEER_IPV4_LBLD_WITHDRAWS_RCVD_CNT (pDestPeer) =
            BGP4_PEER_IPV4_LBLD_WITHDRAWS_RCVD_CNT (pSrcPeer);
        BGP4_PEER_IPV4_LBLD_WITHDRAWS_SENT_CNT (pDestPeer) =
            BGP4_PEER_IPV4_LBLD_WITHDRAWS_SENT_CNT (pSrcPeer);
        BGP4_PEER_IPV4_LBLD_IN_PREFIXES_CNT (pDestPeer) =
            BGP4_PEER_IPV4_LBLD_IN_PREFIXES_CNT (pSrcPeer);
        BGP4_PEER_IPV4_LBLD_IN_PREFIXES_ACPTD_CNT (pDestPeer) =
            BGP4_PEER_IPV4_LBLD_IN_PREFIXES_ACPTD_CNT (pSrcPeer);
        BGP4_PEER_IPV4_LBLD_IN_PREFIXES_RJCTD_CNT (pDestPeer) =
            BGP4_PEER_IPV4_LBLD_IN_PREFIXES_RJCTD_CNT (pSrcPeer);
        BGP4_PEER_IPV4_LBLD_OUT_PREFIXES_CNT (pDestPeer) =
            BGP4_PEER_IPV4_LBLD_OUT_PREFIXES_CNT (pSrcPeer);
        BGP4_PEER_IPV4_LBLD_INVALID_MPE_UPDATE_CNT (pDestPeer) =
            BGP4_PEER_IPV4_LBLD_INVALID_MPE_UPDATE_CNT (pSrcPeer);
        BGP4_RTREF_MSG_SENT_CTR (pDestPeer, BGP4_IPV4_LBLD_INDEX) =
            BGP4_RTREF_MSG_SENT_CTR (pSrcPeer, BGP4_IPV4_LBLD_INDEX);
        BGP4_RTREF_MSG_RCVD_CTR (pDestPeer, BGP4_IPV4_LBLD_INDEX) =
            BGP4_RTREF_MSG_RCVD_CTR (pSrcPeer, BGP4_IPV4_LBLD_INDEX);
        BGP4_RTREF_MSG_TXERR_CTR (pDestPeer, BGP4_IPV4_LBLD_INDEX) =
            BGP4_RTREF_MSG_TXERR_CTR (pSrcPeer, BGP4_IPV4_LBLD_INDEX);
        BGP4_RTREF_MSG_RCVD_INVALID_CTR (pDestPeer, BGP4_IPV4_LBLD_INDEX) =
            BGP4_RTREF_MSG_RCVD_INVALID_CTR (pSrcPeer, BGP4_IPV4_LBLD_INDEX);
        BGP4_RTREF_MSG_REQ_PEER (pDestPeer, BGP4_IPV4_LBLD_INDEX) =
            BGP4_RTREF_MSG_REQ_PEER (pSrcPeer, BGP4_IPV4_LBLD_INDEX);
    }
    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pSrcPeer) != NULL)
    {
        if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pDestPeer) == NULL)
        {
            BGP4_PEER_VPN4_AFISAFI_INSTANCE (pDestPeer) =
                Bgp4MpeAllocateAfiSafiInstance (pDestPeer, BGP4_VPN4_UNI_INDEX);
            if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pDestPeer) == NULL)
            {
                /* This funciton needs to be made to return SUCCESS/FAILRUE */
                return SNMP_FAILURE;
            }
        }

        BGP4_PEER_VPN4_PREFIX_RCVD_CNT (pDestPeer) =
            BGP4_PEER_VPN4_PREFIX_RCVD_CNT (pSrcPeer);
        BGP4_PEER_VPN4_PREFIX_SENT_CNT (pDestPeer) =
            BGP4_PEER_VPN4_PREFIX_SENT_CNT (pSrcPeer);
        BGP4_PEER_VPN4_WITHDRAWS_RCVD_CNT (pDestPeer) =
            BGP4_PEER_VPN4_WITHDRAWS_RCVD_CNT (pSrcPeer);
        BGP4_PEER_VPN4_WITHDRAWS_SENT_CNT (pDestPeer) =
            BGP4_PEER_VPN4_WITHDRAWS_SENT_CNT (pSrcPeer);
        BGP4_PEER_VPN4_IN_PREFIXES_CNT (pDestPeer) =
            BGP4_PEER_VPN4_IN_PREFIXES_CNT (pSrcPeer);
        BGP4_PEER_VPN4_IN_PREFIXES_ACPTD_CNT (pDestPeer) =
            BGP4_PEER_VPN4_IN_PREFIXES_ACPTD_CNT (pSrcPeer);
        BGP4_PEER_VPN4_IN_PREFIXES_RJCTD_CNT (pDestPeer) =
            BGP4_PEER_VPN4_IN_PREFIXES_RJCTD_CNT (pSrcPeer);
        BGP4_PEER_VPN4_OUT_PREFIXES_CNT (pDestPeer) =
            BGP4_PEER_VPN4_OUT_PREFIXES_CNT (pSrcPeer);
        BGP4_PEER_VPN4_INVALID_MPE_UPDATE_CNT (pDestPeer) =
            BGP4_PEER_VPN4_INVALID_MPE_UPDATE_CNT (pSrcPeer);
        BGP4_RTREF_MSG_SENT_CTR (pDestPeer, BGP4_VPN4_UNI_INDEX) =
            BGP4_RTREF_MSG_SENT_CTR (pSrcPeer, BGP4_VPN4_UNI_INDEX);
        BGP4_RTREF_MSG_RCVD_CTR (pDestPeer, BGP4_VPN4_UNI_INDEX) =
            BGP4_RTREF_MSG_RCVD_CTR (pSrcPeer, BGP4_VPN4_UNI_INDEX);
        BGP4_RTREF_MSG_TXERR_CTR (pDestPeer, BGP4_VPN4_UNI_INDEX) =
            BGP4_RTREF_MSG_TXERR_CTR (pSrcPeer, BGP4_VPN4_UNI_INDEX);
        BGP4_RTREF_MSG_RCVD_INVALID_CTR (pDestPeer, BGP4_VPN4_UNI_INDEX) =
            BGP4_RTREF_MSG_RCVD_INVALID_CTR (pSrcPeer, BGP4_VPN4_UNI_INDEX);
        BGP4_RTREF_MSG_REQ_PEER (pDestPeer, BGP4_VPN4_UNI_INDEX) =
            BGP4_RTREF_MSG_REQ_PEER (pSrcPeer, BGP4_VPN4_UNI_INDEX);
    }
#endif

#ifdef BGP4_IPV6_WANTED
    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pSrcPeer) != NULL)
    {
        if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pDestPeer) == NULL)
        {
            BGP4_PEER_IPV6_AFISAFI_INSTANCE (pDestPeer) =
                Bgp4MpeAllocateAfiSafiInstance (pDestPeer, BGP4_IPV6_UNI_INDEX);
            if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pDestPeer) == NULL)
            {
                /* This funciton needs to be made to return SUCCESS/FAILRUE */
                return SNMP_FAILURE;
            }
        }

        BGP4_PEER_IPV6_PREFIX_RCVD_CNT (pDestPeer) =
            BGP4_PEER_IPV6_PREFIX_RCVD_CNT (pSrcPeer);
        BGP4_PEER_IPV6_PREFIX_SENT_CNT (pDestPeer) =
            BGP4_PEER_IPV6_PREFIX_SENT_CNT (pSrcPeer);
        BGP4_PEER_IPV6_WITHDRAWS_RCVD_CNT (pDestPeer) =
            BGP4_PEER_IPV6_WITHDRAWS_RCVD_CNT (pSrcPeer);
        BGP4_PEER_IPV6_WITHDRAWS_SENT_CNT (pDestPeer) =
            BGP4_PEER_IPV6_WITHDRAWS_SENT_CNT (pSrcPeer);
        BGP4_PEER_IPV6_IN_PREFIXES_CNT (pDestPeer) =
            BGP4_PEER_IPV6_IN_PREFIXES_CNT (pSrcPeer);
        BGP4_PEER_IPV6_IN_PREFIXES_ACPTD_CNT (pDestPeer) =
            BGP4_PEER_IPV6_IN_PREFIXES_ACPTD_CNT (pSrcPeer);
        BGP4_PEER_IPV6_IN_PREFIXES_RJCTD_CNT (pDestPeer) =
            BGP4_PEER_IPV6_IN_PREFIXES_RJCTD_CNT (pSrcPeer);
        BGP4_PEER_IPV6_OUT_PREFIXES_CNT (pDestPeer) =
            BGP4_PEER_IPV6_OUT_PREFIXES_CNT (pSrcPeer);
        BGP4_PEER_IPV6_INVALID_MPE_UPDATE_CNT (pDestPeer) =
            BGP4_PEER_IPV6_INVALID_MPE_UPDATE_CNT (pSrcPeer);
        BGP4_RTREF_MSG_SENT_CTR (pDestPeer, BGP4_IPV6_UNI_INDEX) =
            BGP4_RTREF_MSG_SENT_CTR (pSrcPeer, BGP4_IPV6_UNI_INDEX);
        BGP4_RTREF_MSG_RCVD_CTR (pDestPeer, BGP4_IPV6_UNI_INDEX) =
            BGP4_RTREF_MSG_RCVD_CTR (pSrcPeer, BGP4_IPV6_UNI_INDEX);
        BGP4_RTREF_MSG_TXERR_CTR (pDestPeer, BGP4_IPV6_UNI_INDEX) =
            BGP4_RTREF_MSG_TXERR_CTR (pSrcPeer, BGP4_IPV6_UNI_INDEX);
        BGP4_RTREF_MSG_RCVD_INVALID_CTR (pDestPeer, BGP4_IPV6_UNI_INDEX) =
            BGP4_RTREF_MSG_RCVD_INVALID_CTR (pSrcPeer, BGP4_IPV6_UNI_INDEX);
        BGP4_RTREF_MSG_REQ_PEER (pDestPeer, BGP4_IPV6_UNI_INDEX) =
            BGP4_RTREF_MSG_REQ_PEER (pSrcPeer, BGP4_IPV6_UNI_INDEX);
    }
#endif
#ifdef ROUTEMAP_WANTED
    /* Copying Route  map */
    MEMCPY (&(pDestPeer->FilterInRMap), &(pSrcPeer->FilterInRMap),
            sizeof (tFilteringRMap));
    MEMCPY (&(pDestPeer->FilterOutRMap), &(pSrcPeer->FilterOutRMap),
            sizeof (tFilteringRMap));

    /* Copying Ip prefix list */
    MEMCPY (&(pDestPeer->IpPrefixFilterIn), &(pSrcPeer->IpPrefixFilterIn),
            sizeof (tFilteringRMap));
    MEMCPY (&(pDestPeer->IpPrefixFilterOut), &(pSrcPeer->IpPrefixFilterOut),
            sizeof (tFilteringRMap));
#endif
    /* TCP MD5 password */
    if (BGP4_PEER_TCPMD5_PTR (pSrcPeer) != NULL)
    {
        if (BGP4_PEER_TCPMD5_PTR (pDestPeer) == NULL)
        {
            BGP4_PEER_TCPMD5_PTR (pDestPeer) =
                Bgp4MemAllocateTcpMd5Buf (BGP4_PEER_CXT_ID (pSrcPeer),
                                          sizeof (tTcpMd5AuthPasswd));
            if (BGP4_PEER_TCPMD5_PTR (pDestPeer) == NULL)
            {
                return SNMP_FAILURE;
            }
            BGP4_PEER_TCPMD5_PASSWD_STATUS (pDestPeer) = BGP4_TCPMD5_PWD_SET;
        }
        if (BGP4_PEER_TCPMD5_PASSWD_LEN (pSrcPeer) <= BGP4_TCPMD5_PWD_SIZE)
        {
            MEMCPY (BGP4_PEER_TCPMD5_PASSWD (pDestPeer),
                    BGP4_PEER_TCPMD5_PASSWD (pSrcPeer),
                    BGP4_PEER_TCPMD5_PASSWD_LEN (pSrcPeer));
        }
        BGP4_PEER_TCPMD5_PASSWD_LEN (pDestPeer) =
            BGP4_PEER_TCPMD5_PASSWD_LEN (pSrcPeer);
    }
    if (BGP4_PEER_TCPAO_MKT (pSrcPeer) != NULL)
    {
        BGP4_PEER_TCPAO_MKT (pDestPeer) = BGP4_PEER_TCPAO_MKT (pSrcPeer);
        BGP4_PEER_TCPAO_ICMPACCEPT (pDestPeer) =
            BGP4_PEER_TCPAO_ICMPACCEPT (pSrcPeer);
        BGP4_PEER_TCPAO_NOMKTDISC (pDestPeer) =
            BGP4_PEER_TCPAO_NOMKTDISC (pDestPeer);
    }
#ifdef L3VPN
    if (BGP4_VPN4_CE_PEER_SOO_EXT_COM (pSrcPeer) != NULL)
    {
        EXT_COMM_PROFILE_CREATE (pSooExtCom);
        if (pSooExtCom == NULL)
        {
            return SNMP_FAILURE;
        }
        MEMCPY (pSooExtCom, BGP4_VPN4_CE_PEER_SOO_EXT_COM (pSrcPeer),
                sizeof (tExtCommProfile));
        BGP4_VPN4_CE_PEER_SOO_EXT_COM (pDestPeer) = pSooExtCom;
    }
    BGP4_VPN4_PEER_ROLE (pDestPeer) = BGP4_VPN4_PEER_ROLE (pSrcPeer);
    BGP4_VPN4_PEER_CERT_STATUS (pDestPeer) =
        BGP4_VPN4_PEER_CERT_STATUS (pSrcPeer);
#endif
    BGP4_PEER_IF_INDEX (pDestPeer) = BGP4_PEER_IF_INDEX (pSrcPeer);
    pDestPeer->u1BfdStatus = pSrcPeer->u1BfdStatus;
    MEMCPY (&(pDestPeer->shadowPeerConfig), &(pSrcPeer->shadowPeerConfig),
            sizeof (tBgp4ShadowPeerConfig));
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4SnmphDeletePeers                                      */
/* Description   : Deletes all peers with specified AFI.                     */
/* Input(s)      : u2Afi - Address family                                    */
/* Output(s)     : None.                                                     */
/* Return(s)     : SNMP_FAILURE/SNMP_SUCCESS                                 */
/*****************************************************************************/
INT4
Bgp4SnmphDeletePeers (UINT2 u2Afi)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    UINT4               u4Size = 0, u4PeerAddress = 0;
    UINT1               au1PeerOctetList[BGP4_PEER_OCTET_LIST_LEN];

    UINT4               u4Context;
    INT1                i1RetVal = 0;
    u4Context = gpBgpCurrCxtNode->u4ContextId;

    u4Size = (u2Afi == BGP4_INET_AFI_IPV4) ? sizeof (UINT4)
#ifdef BGP4_IPV6_WANTED
        : BGP4_IPV6_PREFIX_LEN;
#else
  :    0;
#endif

    MEMSET (au1PeerOctetList, 0, BGP4_PEER_OCTET_LIST_LEN);
    PeerAddr.pu1_OctetList = au1PeerOctetList;
    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
    {
        if (BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_PEER_REMOTE_ADDR_INFO (pPeer))
            == u2Afi)
        {
            PeerAddr.i4_Length = (INT4) u4Size;
            switch (u2Afi)
            {
                case BGP4_INET_AFI_IPV4:
                    PTR_FETCH4 (u4PeerAddress, BGP4_PEER_REMOTE_ADDR (pPeer));
                    PTR_ASSIGN_4 (PeerAddr.pu1_OctetList, u4PeerAddress);
                    break;

#ifdef BGP4_IPV6_WANTED
                case BGP4_INET_AFI_IPV6:
                    MEMCPY (PeerAddr.pu1_OctetList,
                            BGP4_PEER_REMOTE_ADDR (pPeer), u4Size);
                    break;
#endif

                default:
                    return SNMP_FAILURE;
            }
            i1RetVal =
                nmhSetFsbgp4mpePeerExtConfigurePeer (u2Afi,
                                                     &PeerAddr,
                                                     BGP4_PEER_DELETE);
            MEMSET (PeerAddr.pu1_OctetList, 0, PeerAddr.i4_Length);
            pPeer = NULL;
            UNUSED_PARAM (i1RetVal);
        }
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4SnmphDeactivateCapabilyForPeers                      */
/* Description   : Deactivates Capability for athe peers.                   */
/* Input(s)      : u2Afi - Address family                                    */
/*               : u1Safi - Safi                                             */
/* Output(s)     : None.                                                     */
/* Return(s)     : SNMP_FAILURE/SNMP_SUCCESS                                 */
/*****************************************************************************/
INT4
Bgp4SnmphDeactivateCapabilyForPeers (UINT2 u2Afi, UINT2 u2Safi)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         RemAddr;
    tBgp4QMsg          *pQMsg = NULL;
    INT4                i4RetVal;
    UINT4               u4AsafiMask;
    UINT4               u4NegAsafiMask;
    UINT4               u4RemAddr;
    UINT1               u1AsafiSupport = BGP4_FALSE;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    BGP4_GET_AFISAFI_MASK (u2Afi, u2Safi, u4AsafiMask);
    switch (u4AsafiMask)
    {
#ifdef L3VPN
        case CAP_MP_VPN4_UNICAST:
            u4NegAsafiMask = CAP_NEG_VPN4_UNI_MASK;
            break;
#endif
        case CAP_MP_IPV4_UNICAST:

            u4NegAsafiMask = CAP_NEG_IPV4_UNI_MASK;
            break;
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
            u4NegAsafiMask = CAP_NEG_IPV6_UNI_MASK;
            break;
#endif
        default:
            return BGP4_FAILURE;
    }

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
    {
        /*If the peer is Ipv6 peer, it has to modified */
        Bgp4InitAddrPrefixStruct (&RemAddr, BGP4_INET_AFI_IPV4);
        PTR_FETCH4 (u4RemAddr, BGP4_PEER_REMOTE_ADDR (pPeer));
        MEMCPY (RemAddr.au1Address, &u4RemAddr, sizeof (UINT4));
        if ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) & CAP_NEG_IPV4_UNI_MASK) ==
            CAP_NEG_IPV4_UNI_MASK)
        {
            u1AsafiSupport = BGP4_TRUE;
        }

#ifdef BGP4_IPV6_WANTED
        else if ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) & CAP_NEG_IPV6_UNI_MASK) ==
                 CAP_NEG_IPV6_UNI_MASK)
        {
            u1AsafiSupport = BGP4_TRUE;
        }
#endif

#ifdef L3VPN
        if (u4NegAsafiMask == CAP_NEG_VPN4_UNI_MASK)
        {

            u1AsafiSupport =
                Bgp4SnmphGetMPCapSupportStatus (u4Context,
                                                BGP4_PEER_REMOTE_ADDR_INFO
                                                (pPeer), u2Afi, (UINT1) u2Safi);
        }
#endif

        if (u1AsafiSupport == BGP4_TRUE)
        {
            i4RetVal =
                Bgp4SnmphDisableMPCapability (u4Context,
                                              BGP4_PEER_REMOTE_ADDR_INFO
                                              (pPeer), u2Afi, (UINT1) u2Safi);
            if (i4RetVal == BGP4_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                          BGP4_MOD_NAME,
                          "\tBgp4SnmphDeactivateCapabilyForPeers() : Unable to \
                            disable Vpnv4-unicast capability for peer\n");
                return BGP4_FAILURE;
            }
            Bgp4SnmphUpdateGrCapCode (BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
        }

        if ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) & u4NegAsafiMask)
            == u4NegAsafiMask)
        {

            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                          BGP4_MOD_NAME,
                          "\ttBgp4SnmphDeactivateCapabilyForPeers() : Unable to"
                          "trigger Clear ip bgp command\n");
                return BGP4_FAILURE;
            }

            BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_CLI_CLEAR_IP_BGP_EVENT;

            MEMCPY (&BGP4_INPUTQ_PEER_ADDR_INFO (pQMsg),
                    &(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                    sizeof (tAddrPrefix));

            BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
            BGP4_INPUTQ_CXT (pQMsg) = u4Context;
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                          BGP4_MOD_NAME,
                          "\tBgp4SnmphDeactivateCapabilyForPeers() : Unable to"
                          "trigger Clear ip bgp command\n");
                return BGP4_FAILURE;
            }
        }

    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4SnmphDisableCapsForAllPeers                           */
/* Description   : Disables all default caps with specified                  */
/*                 <AFI, SAFI>                                               */
/* Input(s)      : peer remote address (IPV4 specific)                       */
/*                 u2Afi - Address family                                    */
/*                 u1Safi - subsequent address family                        */
/* Output(s)     : None.                                                     */
/* Return(s)     : SNMP_FAILURE/SNMP_SUCCESS                                 */
/*****************************************************************************/
INT4
Bgp4SnmphDisableCapsForAllPeers ()
{
    tBgp4PeerEntry     *pPeer = NULL;

    UINT4               u4Context;
    INT4                i4RetVal;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
    {
        i4RetVal =
            Bgp4SnmphDisableMPCapability (u4Context,
                                          BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                                          BGP4_INET_AFI_IPV4,
                                          BGP4_INET_SAFI_UNICAST);
        i4RetVal =
            Bgp4SnmphDisableMPCapability (u4Context,
                                          BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                                          BGP4_INET_AFI_IPV6,
                                          BGP4_INET_SAFI_UNICAST);
        Bgp4SnmphRtRefCapabilityDisable (u4Context,
                                         BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
        Bgp4SnmphGrCapabilityDisable (BGP4_PEER_REMOTE_ADDR_INFO (pPeer));

        Bgp4Snmph4ByteAsnCapabilityDisable (BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
        UNUSED_PARAM (i4RetVal);

    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4SnmphDisableMPCapability                              */
/* Description   : Disables the MULTIPROTOCOL CAPABILITY with specified      */
/*                 <AFI, SAFI>                                               */
/* Input(s)      : peer remote address (IPV4 specific)                       */
/*                 u2Afi - Address family                                    */
/*                 u1Safi - subsequent address family                        */
/* Output(s)     : None.                                                     */
/* Return(s)     : SNMP_FAILURE/SNMP_SUCCESS                                 */
/*****************************************************************************/
INT4
Bgp4SnmphDisableMPCapability (UINT4 u4Context, tAddrPrefix PeerAddressInfo,
                              UINT2 u2Afi, UINT1 u1Safi)
{
    tSNMP_OCTET_STRING_TYPE MPCapValue;
    UINT1               u1AsafiSupport = BGP4_FALSE;
    UINT4               u4AsafiMask;
    UINT1               au1MpcapOctetList[CAP_MP_CAP_LENGTH];

    BGP4_GET_AFISAFI_MASK (u2Afi, u1Safi, u4AsafiMask);
    u1AsafiSupport = Bgp4SnmphGetMPCapSupportStatus (u4Context, PeerAddressInfo,
                                                     u2Afi, u1Safi);
    MEMSET (au1MpcapOctetList, 0, CAP_MP_CAP_LENGTH);
    if (u1AsafiSupport == BGP4_TRUE)
    {
        MPCapValue.pu1_OctetList = au1MpcapOctetList;
        MPCapValue.i4_Length = CAP_MP_CAP_LENGTH;
        PTR_ASSIGN4 (MPCapValue.pu1_OctetList, u4AsafiMask);
        /* Given <AFI, SAFI> is configured */

        /* DESTROY entry for the MULTI PROTOCOL CAPABILITY */
        Bgp4SetCapSupportedRowStatus (u4Context, PeerAddressInfo,
                                      CAP_CODE_MP_EXTN, CAP_MP_CAP_LENGTH,
                                      &MPCapValue, DESTROY);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/*****************************************************************************/
/* Function Name : Bgp4SnmphEnableMPCapability                               */
/* Description   : Configures the MULTIPROTOCOL CAPABILITY with specified    */
/*                 <AFI, SAFI>                                               */
/* Input(s)      : peer remote address (IPV4 specific)                       */
/*                 u2Afi - Address family                                    */
/*                 u1Safi - subsequent address family                        */
/* Output(s)     : None.                                                     */
/* Return(s)     : SNMP_FAILURE/SNMP_SUCCESS                                 */
/*****************************************************************************/
INT4
Bgp4SnmphEnableMPCapability (UINT4 u4Context, tAddrPrefix PeerAddressInfo,
                             UINT2 u2Afi, UINT1 u1Safi)
{
    tSNMP_OCTET_STRING_TYPE MpCapabilityValue;
    UINT1               u1AsafiSupport = BGP4_FALSE;
    UINT1               u1ResField = 0;
    UINT1               au1MpcapOctetList[BGP4_MPE_MP_CAP_LEN];

    u1AsafiSupport =
        Bgp4SnmphGetMPCapSupportStatus (u4Context, PeerAddressInfo, u2Afi,
                                        u1Safi);
    if (u1AsafiSupport == BGP4_TRUE)
    {
        /* Given <AFI, SAFI> is already configured */
        return SNMP_SUCCESS;
    }
    MEMSET (au1MpcapOctetList, 0, BGP4_MPE_MP_CAP_LEN);
    MpCapabilityValue.pu1_OctetList = au1MpcapOctetList;

    PTR_ASSIGN2 (MpCapabilityValue.pu1_OctetList, (u2Afi));
    *((MpCapabilityValue.pu1_OctetList) + BGP4_MPE_ADDRESS_FAMILY_LEN)
        = u1ResField;
    *((MpCapabilityValue.pu1_OctetList) + BGP4_MPE_ADDRESS_FAMILY_LEN
      + BGP4_MPE_MP_CAP_RESERVED_FIELD_LEN) = u1Safi;

    MpCapabilityValue.i4_Length = BGP4_MPE_MP_CAP_LEN;

    /* CREATE AND WAIT for the MULTI PROTOCOL CAPABILITY */
    Bgp4SetCapSupportedRowStatus (u4Context, PeerAddressInfo,
                                  CAP_CODE_MP_EXTN, CAP_MP_CAP_LENGTH,
                                  &MpCapabilityValue, CREATE_AND_GO);

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4SnmphGetMPCapSupportStatus                            */
/* Description   : Gets MULTIPROTOCOL CAPABILITY support status for specified*/
/*                 <AFI, SAFI>                                               */
/* Input(s)      : peer remote address (IPV4 specific)                       */
/*                 u2Afi - Address family                                    */
/*                 u1Safi - subsequent address family                        */
/* Output(s)     : None.                                                     */
/* Return(s)     : SNMP_FAILURE/SNMP_SUCCESS                                 */
/*****************************************************************************/
UINT1
Bgp4SnmphGetMPCapSupportStatus (UINT4 u4Context, tAddrPrefix PeerAddressInfo,
                                UINT2 u2Afi, UINT1 u1Safi)
{
    tSupCapsInfo       *pSpkrSupCap = NULL;
    tBgp4PeerEntry     *pPeerInfo = NULL;
    UINT2               u2SupportAfi = 0;
    UINT1               u1SupportSafi = 0;

    pPeerInfo = Bgp4SnmphGetPeerEntry (u4Context, PeerAddressInfo);
    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to find Peer Entry.\n");
        return BGP4_FALSE;
    }
    TMO_SLL_Scan (BGP4_PEER_SUP_CAPS_LIST (pPeerInfo), pSpkrSupCap,
                  tSupCapsInfo *)
    {
        /* Search only MULTI PROTOCOL CAPABILITIES */
        if (pSpkrSupCap->SupCapability.u1CapCode != CAP_CODE_MP_EXTN)
        {
            continue;
        }
        if (pSpkrSupCap->SupCapability.u1CapLength != CAP_MP_CAP_LENGTH)
        {
            continue;
        }

        /* Compare the <AFI, SAFI> */
        PTR_FETCH2 (u2SupportAfi, pSpkrSupCap->SupCapability.au1CapValue);
        u1SupportSafi = *(pSpkrSupCap->SupCapability.au1CapValue +
                          BGP4_MPE_ADDRESS_FAMILY_LEN +
                          BGP4_MPE_MP_CAP_RESERVED_FIELD_LEN);

        if ((u2Afi != u2SupportAfi) || (u1SupportSafi != u1Safi))
        {
            continue;
        }

        return BGP4_TRUE;
    }                            /*TMO_SLL_Scan */
    return BGP4_FALSE;
}

/*****************************************************************************/
/* Function Name : Bgp4SnmphRtRefCapabilityDisable                           */
/* Description   : Disables the ROUTE-REFRESH CAPABILITY                     */
/* Input(s)      : peer remote address                                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : SNMP_FAILURE/SNMP_SUCCESS                                 */
/*****************************************************************************/
INT4
Bgp4SnmphRtRefCapabilityDisable (UINT4 u4Context, tAddrPrefix PeerAddressInfo)
{
    UINT1               u1AsafiSupport = BGP4_FALSE;
    tSNMP_OCTET_STRING_TYPE RtRefCap;

    RtRefCap.i4_Length = 0;
    u1AsafiSupport =
        Bgp4SnmphGetRtRefCapSupportStatus (u4Context, PeerAddressInfo);
    if (u1AsafiSupport == BGP4_TRUE)
    {
        /* Route-Refresh is configured */

        /* DESTROY entry for the ROUTE REFRESH CAPABILITY */
        Bgp4SetCapSupportedRowStatus (u4Context, PeerAddressInfo,
                                      CAP_CODE_ROUTE_REFRESH, 0, &RtRefCap,
                                      DESTROY);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/*****************************************************************************/
/* Function Name : Bgp4SnmphOrfCapabilityDisable                             */
/* Description   : Disables the ORF CAPABILITY                               */
/* Input(s)      : u4Context - context identifeir                            */
/*                 PeerAddressInfo - peer remote address                     */
/*                 u2Afi - addres family identifier                          */
/*                 u2Safi - subsequent address family identifier             */
/*                 u1OrfType - ORF type                                      */
/*                 u1OrfMode - ORF mode (send/receive/both)                  */
/* Output(s)     : None.                                                     */
/* Return(s)     : SNMP_FAILURE/SNMP_SUCCESS                                 */
/*****************************************************************************/
INT4
Bgp4SnmphOrfCapabilityDisable (UINT4 u4Context, tAddrPrefix PeerAddressInfo,
                               UINT2 u2Afi, UINT1 u1Safi, UINT1 u1OrfType,
                               UINT1 u1OrfMode)
{
    tOrfCapsValue       OrfCapsValue;
    tSNMP_OCTET_STRING_TYPE OrfCap;
    UINT1               u1OrfCapSupport = BGP4_FALSE;

    u1OrfCapSupport =
        Bgp4SnmphGetOrfCapSupportStatus (u4Context, PeerAddressInfo,
                                         u2Afi, u1Safi, u1OrfType, u1OrfMode);

    if (u1OrfCapSupport == BGP4_TRUE)
    {
        /* ORF Capability with the given Afi, Safi, ORF type is supported */

        MEMSET (&OrfCapsValue, 0, sizeof (tOrfCapsValue));

        OrfCapsValue.u2Afi = OSIX_HTONS (u2Afi);
        OrfCapsValue.u1Safi = u1Safi;
        OrfCapsValue.u1OrfCount = 1;
        OrfCapsValue.u1OrfType = u1OrfType;
        OrfCapsValue.u1OrfMode = u1OrfMode;

        OrfCap.pu1_OctetList = (UINT1 *) &OrfCapsValue;
        OrfCap.i4_Length = BGP_ORF_CAPS_LEN;

        Bgp4SetCapSupportedRowStatus (u4Context, PeerAddressInfo,
                                      CAP_CODE_ORF, BGP_ORF_CAPS_LEN, &OrfCap,
                                      DESTROY);
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4SnmphGrCapabilityDisable                              */
/* Description   : Disables the GRACEFUL-RESTART  CAPABILITY                 */
/* Input(s)      : peer remote address                                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : SNMP_FAILURE/SNMP_SUCCESS                                 */
/*****************************************************************************/
INT4
Bgp4SnmphGrCapabilityDisable (tAddrPrefix PeerAddressInfo)
{
    UINT4               u4Context;
    UINT1               u1AsafiSupport = BGP4_FALSE;
    tSNMP_OCTET_STRING_TYPE GrCap;
    UINT1               au1GrCapOctetList[CAP_GR_CAP_LENGTH];

    GrCap.pu1_OctetList = au1GrCapOctetList;

    /* The capability value is all 0's since we are filling the 
     * capability value before sending the open message to the peer 
     * The memory is cleared in Bgp4MemAllocateMsg since it uses the call 
     * calloc ()*/

    MEMSET (au1GrCapOctetList, 0, CAP_GR_CAP_LENGTH);
    GrCap.i4_Length = CAP_GR_CAP_LENGTH;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    u1AsafiSupport = Bgp4SnmphGetGrCapSupportStatus (PeerAddressInfo);
    if (u1AsafiSupport == BGP4_TRUE)
    {
        /* Graceful restart is configured */

        /* DESTROY entry for the Graceful Restart CAPABILITY */
        Bgp4SetCapSupportedRowStatus (u4Context, PeerAddressInfo,
                                      CAP_CODE_GRACEFUL_RESTART,
                                      CAP_GR_CAP_LENGTH, &GrCap, DESTROY);
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4Snmph4ByteAsnCapabilityDisable                        */
/* Description   : Disables the 4-byte ASN  CAPABILITY                       */
/* Input(s)      : peer remote address                                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : SNMP_FAILURE/SNMP_SUCCESS                                 */
/*****************************************************************************/
INT4
Bgp4Snmph4ByteAsnCapabilityDisable (tAddrPrefix PeerAddressInfo)
{
    tBgp4PeerEntry     *pPeerInfo = NULL;
    UINT4               u4Context;
    UINT4               u4AsNo;
    UINT1               u1AsafiSupport = BGP4_FALSE;
    tSNMP_OCTET_STRING_TYPE FourByteAsnCap;
    UINT1               au1FourByteAsnCapOctetList[CAP_GR_CAP_LENGTH];

    FourByteAsnCap.pu1_OctetList = au1FourByteAsnCapOctetList;

    /* The capability value is all 0's since we are filling the
     * capability value before sending the open message to the peer
     * The memory is cleared in Bgp4MemAllocateMsg since it uses the call
     * calloc ()*/

    MEMSET (au1FourByteAsnCapOctetList, 0, CAP_4_BYTE_ASN_LENGTH);
    FourByteAsnCap.i4_Length = CAP_4_BYTE_ASN_LENGTH;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    pPeerInfo = Bgp4SnmphGetPeerEntry (u4Context, PeerAddressInfo);
    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to find Peer Entry.\n");
        return BGP4_FALSE;
    }

    if ((BGP4_CONFED_ID (BGP4_PEER_CXT_ID (pPeerInfo)) != BGP4_INV_AS) &&
        (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerInfo), pPeerInfo) ==
         BGP4_EXTERNAL_PEER) &&
        (BGP4_CONFED_PEER_STATUS (pPeerInfo) == BGP4_FALSE))
    {
        u4AsNo = BGP4_CONFED_ID (BGP4_PEER_CXT_ID (pPeerInfo));
    }
    else
    {
        u4AsNo = BGP4_PEER_LOCAL_AS (pPeerInfo);
    }

    PTR_ASSIGN4 (FourByteAsnCap.pu1_OctetList, u4AsNo);

    u1AsafiSupport = Bgp4SnmphGet4ByteAsnCapSupportStatus (PeerAddressInfo);
    if (u1AsafiSupport == BGP4_TRUE)
    {
        /* 4-byte ASN capabilility is configured */

        /* DESTROY entry for the 4 byte ASN CAPABILITY */
        Bgp4SetCapSupportedRowStatus (u4Context, PeerAddressInfo,
                                      CAP_CODE_4_OCTET_ASNO,
                                      CAP_4_BYTE_ASN_LENGTH, &FourByteAsnCap,
                                      DESTROY);
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4SnmphRtRefCapabilityEnable                            */
/* Description   : Configures the ROUTE-REFRESH CAPABILITY                   */
/* Input(s)      : peer remote address                                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : SNMP_FAILURE/SNMP_SUCCESS                                 */
/*****************************************************************************/
INT4
Bgp4SnmphRtRefCapabilityEnable (UINT4 u4Context, tAddrPrefix PeerAddressInfo)
{
    UINT1               u1AsafiSupport = BGP4_FALSE;
    tSNMP_OCTET_STRING_TYPE RtRefCap;

    RtRefCap.i4_Length = 0;
    u1AsafiSupport =
        Bgp4SnmphGetRtRefCapSupportStatus (u4Context, PeerAddressInfo);
    if (u1AsafiSupport == BGP4_TRUE)
    {
        /* Route-Refresh is already configured */
        return SNMP_SUCCESS;
    }
    /* CREATE AND WAIT for the MULTI PROTOCOL CAPABILITY */
    Bgp4SetCapSupportedRowStatus (u4Context, PeerAddressInfo,
                                  CAP_CODE_ROUTE_REFRESH, 0, &RtRefCap,
                                  CREATE_AND_GO);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4SnmphOrfCapabilityEnable                              */
/* Description   : Configures ORF CAPABILITY                                 */
/* Input(s)      : u4Context - context identifeir                            */
/*                 PeerAddressInfo - peer remote address                     */
/*                 u2Afi - addres family identifier                          */
/*                 u2Safi - subsequent address family identifier             */
/*                 u1OrfType - ORF type                                      */
/*                 u1OrfMode - ORF mode (send/receive/both)                  */
/* Output(s)     : None.                                                     */
/* Return(s)     : SNMP_FAILURE/SNMP_SUCCESS                                 */
/*****************************************************************************/
INT4
Bgp4SnmphOrfCapabilityEnable (UINT4 u4Context, tAddrPrefix PeerAddressInfo,
                              UINT2 u2Afi, UINT1 u1Safi, UINT1 u1OrfType,
                              UINT1 u1OrfMode)
{
    tSupCapsInfo       *pSpkrSupCap = NULL;
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tOrfCapsValue       OrfCapsValue;
    tSNMP_OCTET_STRING_TYPE OrfCap;
    UINT1               u1OrfCapSupport = BGP4_FALSE;

    u1OrfCapSupport =
        Bgp4SnmphGetOrfCapSupportStatus (u4Context, PeerAddressInfo,
                                         u2Afi, u1Safi, u1OrfType, u1OrfMode);

    pPeerInfo = Bgp4SnmphGetPeerEntry (u4Context, PeerAddressInfo);
    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to find Peer Entry.\n");
        return SNMP_FAILURE;
    }

    MEMSET (&OrfCapsValue, 0, sizeof (tOrfCapsValue));

    OrfCapsValue.u2Afi = OSIX_HTONS (u2Afi);
    OrfCapsValue.u1Safi = u1Safi;
    OrfCapsValue.u1OrfCount = 1;
    OrfCapsValue.u1OrfType = u1OrfType;
    OrfCapsValue.u1OrfMode = u1OrfMode;

    OrfCap.pu1_OctetList = (UINT1 *) &OrfCapsValue;
    OrfCap.i4_Length = BGP_ORF_CAPS_LEN;
    if (u1OrfCapSupport == BGP4_TRUE)
    {
        if (BGP4_PEER_SHADOW_ORF_MODE (pPeerInfo) == 0)
        {
            Bgp4SetCapSupportedRowStatus (u4Context, PeerAddressInfo,
                                          CAP_CODE_ORF, BGP_ORF_CAPS_LEN,
                                          &OrfCap, ACTIVE);
            BGP4_PEER_SHADOW_ORF_MODE (pPeerInfo) = u1OrfMode;
            return SNMP_SUCCESS;
        }
        else
        {
            /* ORF capability for the given orf-type, mode is already configured */
            return SNMP_SUCCESS;
        }
    }

    /* If the ORF Capability with the given AFI,SAFI,ORF-TYPE is supported,
     * but with different ORF mode then add the configured ORF mode in that entry*/
    if (u1OrfCapSupport == BGP_ORF_MODE_DIFFER)
    {
        Bgp4SetCapSupportedRowStatus (u4Context, PeerAddressInfo,
                                      CAP_CODE_ORF, BGP_ORF_CAPS_LEN, &OrfCap,
                                      NOT_IN_SERVICE);

        GetFindSpkrSupCap (pPeerInfo, CAP_CODE_ORF, BGP_ORF_CAPS_LEN, &OrfCap,
                           CAPS_SUP, &pSpkrSupCap);

        if (pSpkrSupCap == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Unable to find Capability entry.\n");
            return SNMP_FAILURE;
        }

        pSpkrSupCap->SupCapability.au1CapValue[BGP_CAP_ORF_MODE_OFFSET] |=
            u1OrfMode;

        Bgp4SetCapSupportedRowStatus (u4Context, PeerAddressInfo,
                                      CAP_CODE_ORF, BGP_ORF_CAPS_LEN, &OrfCap,
                                      ACTIVE);
        BGP4_PEER_SHADOW_ORF_MODE (pPeerInfo) = u1OrfMode;

    }
    else
    {
        Bgp4SetCapSupportedRowStatus (u4Context, PeerAddressInfo,
                                      CAP_CODE_ORF, BGP_ORF_CAPS_LEN, &OrfCap,
                                      CREATE_AND_GO);
        BGP4_PEER_SHADOW_ORF_MODE (pPeerInfo) = u1OrfMode;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4SnmphGrCapabilityEnable                               */
/* Description   : Configures the ROUTE-REFRESH CAPABILITY                   */
/* Input(s)      : peer remote address                                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : SNMP_FAILURE/SNMP_SUCCESS                                 */
/*****************************************************************************/
INT4
Bgp4SnmphGrCapabilityEnable (UINT4 u4Context, tAddrPrefix PeerAddressInfo)
{
    UINT1               u1AsafiSupport = BGP4_FALSE;
    tSNMP_OCTET_STRING_TYPE GrCap;
    UINT1               au1GrCapOctetList[CAP_GR_CAP_LENGTH];

    GrCap.pu1_OctetList = au1GrCapOctetList;

    /* The capability value is all 0's since we are filling the 
     * capability value before sending the open message to the peer 
     * The memory is cleared in Bgp4MemAllocateMsg since it uses the call 
     * calloc ()*/
    MEMSET (au1GrCapOctetList, 0, CAP_GR_CAP_LENGTH);
    GrCap.i4_Length = CAP_GR_CAP_LENGTH;

    u1AsafiSupport = Bgp4SnmphGetGrCapSupportStatus (PeerAddressInfo);
    if (u1AsafiSupport == BGP4_TRUE)
    {
        Bgp4SnmphUpdateGrCapCode (PeerAddressInfo);
        /* Graceful restart  is already configured */
        return SNMP_SUCCESS;
    }
    /* CREATE AND WAIT for the GRACEFUL RESTART  CAPABILITY */
    Bgp4SnmphUpdateGrCapCode (PeerAddressInfo);
    Bgp4SetCapSupportedRowStatus (u4Context, PeerAddressInfo,
                                  CAP_CODE_GRACEFUL_RESTART, CAP_GR_CAP_LENGTH,
                                  &GrCap, CREATE_AND_GO);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4Snmph4ByteAsnCapabilityEnable                         */
/* Description   : Configures the 4-byte ASN CAPABILITY                      */
/* Input(s)      : peer remote address                                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : SNMP_FAILURE/SNMP_SUCCESS                                 */
/*****************************************************************************/
INT4
Bgp4Snmph4ByteAsnCapabilityEnable (UINT4 u4Context, tAddrPrefix PeerAddressInfo)
{
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tSupCapsInfo       *pSpkrSupCap = NULL;
    UINT4               u4AsNo;
    UINT1               u1AsafiSupport = BGP4_FALSE;
    tSNMP_OCTET_STRING_TYPE FourByteAsnCap;
    UINT1               au1FourByteAsnCapOctetList[CAP_4_BYTE_ASN_LENGTH];

    FourByteAsnCap.pu1_OctetList = au1FourByteAsnCapOctetList;

    /* The capability value is all 0's since we are filling the
     * capability value before sending the open message to the peer
     * The memory is cleared in Bgp4MemAllocateMsg since it uses the call
     * calloc ()*/
    MEMSET (au1FourByteAsnCapOctetList, 0, CAP_4_BYTE_ASN_LENGTH);
    FourByteAsnCap.i4_Length = CAP_4_BYTE_ASN_LENGTH;

    pPeerInfo = Bgp4SnmphGetPeerEntry (u4Context, PeerAddressInfo);
    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to find Peer Entry.\n");
        return BGP4_FALSE;
    }

    if ((BGP4_CONFED_ID (BGP4_PEER_CXT_ID (pPeerInfo)) != BGP4_INV_AS) &&
        (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerInfo), pPeerInfo) ==
         BGP4_EXTERNAL_PEER) &&
        (BGP4_CONFED_PEER_STATUS (pPeerInfo) == BGP4_FALSE))
    {
        u4AsNo = BGP4_CONFED_ID (BGP4_PEER_CXT_ID (pPeerInfo));
    }
    else
    {
        u4AsNo = BGP4_PEER_LOCAL_AS (pPeerInfo);
    }

    PTR_ASSIGN4 (FourByteAsnCap.pu1_OctetList, u4AsNo);

    u1AsafiSupport = Bgp4SnmphGet4ByteAsnCapSupportStatus (PeerAddressInfo);
    if (u1AsafiSupport == BGP4_TRUE)
    {
        /* 4-byte ASN capabilility is configured */
        GetFindSpkrSupCap (pPeerInfo,
                           CAP_CODE_4_OCTET_ASNO,
                           CAP_4_BYTE_ASN_LENGTH,
                           &FourByteAsnCap, CAPS_SUP, &pSpkrSupCap);
        if (pSpkrSupCap == NULL)
        {
            /* 4-byte ASN capabilility is configured, but there is a
               mismatch between the configured PEER_AS_NO and 
               capability value */
            TMO_SLL_Scan (BGP4_PEER_SUP_CAPS_LIST (pPeerInfo), pSpkrSupCap,
                          tSupCapsInfo *)
            {
                /* Search only 4-byte ASN CAPABILITY Support */
                if (pSpkrSupCap->SupCapability.u1CapCode !=
                    CAP_CODE_4_OCTET_ASNO)
                {
                    continue;
                }

                MEMCPY (FourByteAsnCap.pu1_OctetList,
                        pSpkrSupCap->SupCapability.au1CapValue,
                        CAP_4_BYTE_ASN_LENGTH);

                Bgp4SetCapSupportedRowStatus (u4Context, PeerAddressInfo,
                                              CAP_CODE_4_OCTET_ASNO,
                                              CAP_4_BYTE_ASN_LENGTH,
                                              &FourByteAsnCap, DESTROY);
            }
        }
        else
        {
            return SNMP_SUCCESS;
        }
    }
    /* CREATE AND WAIT for the 4-byte ASN CAPABILITY */
    PTR_ASSIGN4 (FourByteAsnCap.pu1_OctetList, u4AsNo);

    if (u4AsNo != BGP4_INV_AS)
    {
        Bgp4SetCapSupportedRowStatus (u4Context, PeerAddressInfo,
                                      CAP_CODE_4_OCTET_ASNO,
                                      CAP_4_BYTE_ASN_LENGTH,
                                      &FourByteAsnCap, CREATE_AND_GO);
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4SnmphGetRtRefCapSupportStatus                         */
/* Description   : Gets ROUTE-REFRESH CAPABILITY support status              */
/* Input(s)      : peer remote address                                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE/BGP4_FALSE                                      */
/*****************************************************************************/
UINT1
Bgp4SnmphGetRtRefCapSupportStatus (UINT4 u4Context, tAddrPrefix PeerAddressInfo)
{
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tSupCapsInfo       *pSpkrSupCap = NULL;

    pPeerInfo = Bgp4SnmphGetPeerEntry (u4Context, PeerAddressInfo);
    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to find Peer Entry.\n");
        return BGP4_FALSE;
    }
    TMO_SLL_Scan (BGP4_PEER_SUP_CAPS_LIST (pPeerInfo), pSpkrSupCap,
                  tSupCapsInfo *)
    {
        /* Search only ROUTE-REFRESH CAPABILITY */
        if (pSpkrSupCap->SupCapability.u1CapCode != CAP_CODE_ROUTE_REFRESH)
        {
            continue;
        }
        return BGP4_TRUE;
    }                            /*TMO_SLL_Scan */

    return BGP4_FALSE;
}

/*****************************************************************************/
/* Function Name : Bgp4SnmphGetOrfCapSupportStatus                           */
/* Description   : Gets ORF CAPABILITY support status                        */
/* Input(s)      : u4Context - context identifier                            */
/*                 PeerAddressInfo - peer remote address                     */
/*                 u2Afi - Address family identifier                         */
/*                 u1Safi - Subsequent Address family identifier             */
/*                 u1OrfType - Type of ORF                                   */
/*                 u1OrfMode - ORF mode (send/receive)                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE will be returned if the given mode is supported */
/*                 BGP4_FALSE will be returned if no entry is present for    */
/*                 the particular AFI, SAFI, ORF Type combination            */
/*                 BGP_ORF_MODE_DIFFER will  be returned if entry with given */
/*                 AFI, SAFI, ORF Type is present but with different ORF mode*/
/*****************************************************************************/
UINT1
Bgp4SnmphGetOrfCapSupportStatus (UINT4 u4Context, tAddrPrefix PeerAddressInfo,
                                 UINT2 u2Afi, UINT1 u1Safi, UINT1 u1OrfType,
                                 UINT1 u1OrfMode)
{
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tSupCapsInfo       *pSpkrSupCap = NULL;
    tOrfCapsValue      *pOrfCapsValue = NULL;

    pPeerInfo = Bgp4SnmphGetPeerEntry (u4Context, PeerAddressInfo);
    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to find Peer Entry.\n");
        return BGP4_FALSE;
    }
    TMO_SLL_Scan (BGP4_PEER_SUP_CAPS_LIST (pPeerInfo), pSpkrSupCap,
                  tSupCapsInfo *)
    {
        /* Search only ORF CAPABILITY */
        if (pSpkrSupCap->SupCapability.u1CapCode != CAP_CODE_ORF)
        {
            continue;
        }
        else
        {
            pOrfCapsValue =
                (tOrfCapsValue *) (VOID *) pSpkrSupCap->SupCapability.
                au1CapValue;
            if ((OSIX_HTONS (pOrfCapsValue->u2Afi)) != u2Afi)
            {
                continue;
            }
            if (pOrfCapsValue->u1Safi != u1Safi)
            {
                continue;
            }
            if (pOrfCapsValue->u1OrfType != u1OrfType)
            {
                continue;
            }
            if ((pOrfCapsValue->u1OrfMode & u1OrfMode) != u1OrfMode)
            {
                return BGP_ORF_MODE_DIFFER;
            }
        }
        return BGP4_TRUE;
    }                            /*TMO_SLL_Scan */

    return BGP4_FALSE;
}

/*****************************************************************************/
/* Function Name : Bgp4SnmphGetGrCapSupportStatus                            */
/* Description   : Gets GRACEFUL-RESTART CAPABILITY support status           */
/* Input(s)      : peer remote address                                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE/BGP4_FALSE                                      */
/*****************************************************************************/
UINT1
Bgp4SnmphGetGrCapSupportStatus (tAddrPrefix PeerAddressInfo)
{
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tSupCapsInfo       *pSpkrSupCap = NULL;

    UINT4               u4Context;
    u4Context = gpBgpCurrCxtNode->u4ContextId;

    pPeerInfo = Bgp4SnmphGetPeerEntry (u4Context, PeerAddressInfo);
    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to find Peer Entry.\n");
        return BGP4_FALSE;
    }
    TMO_SLL_Scan (BGP4_PEER_SUP_CAPS_LIST (pPeerInfo), pSpkrSupCap,
                  tSupCapsInfo *)
    {
        /* Search only GRACEFUL-RESTART  CAPABILITY */
        if (pSpkrSupCap->SupCapability.u1CapCode != CAP_CODE_GRACEFUL_RESTART)
        {
            continue;
        }
        return BGP4_TRUE;
    }                            /*TMO_SLL_Scan */

    return BGP4_FALSE;
}

/*****************************************************************************/
/* Function Name : Bgp4SnmphGet4ByteAsnCapSupportStatus                      */
/* Description   : Gets 4-byte ASN CAPABILITY support status                 */
/* Input(s)      : peer remote address                                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE/BGP4_FALSE                                      */
/*****************************************************************************/
UINT1
Bgp4SnmphGet4ByteAsnCapSupportStatus (tAddrPrefix PeerAddressInfo)
{
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tSupCapsInfo       *pSpkrSupCap = NULL;

    UINT4               u4Context;
    u4Context = gpBgpCurrCxtNode->u4ContextId;

    pPeerInfo = Bgp4SnmphGetPeerEntry (u4Context, PeerAddressInfo);
    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to find Peer Entry.\n");
        return BGP4_FALSE;
    }
    TMO_SLL_Scan (BGP4_PEER_SUP_CAPS_LIST (pPeerInfo), pSpkrSupCap,
                  tSupCapsInfo *)
    {
        /* Search only 4-byte ASN CAPABILITY Support */
        if (pSpkrSupCap->SupCapability.u1CapCode != CAP_CODE_4_OCTET_ASNO)
        {
            continue;
        }
        return BGP4_TRUE;
    }                            /*TMO_SLL_Scan */

    return BGP4_FALSE;
}

INT4
Bgp4SetCapSupportedRowStatus (UINT4 u4Context, tAddrPrefix PeerRemAddrInfo,
                              INT4 i4Fsbgp4SupportedCapabilityCode,
                              INT4 i4Fsbgp4SupportedCapabilityLength,
                              tSNMP_OCTET_STRING_TYPE * pCapValue,
                              INT4 i4SetValFsbgp4CapSupportedCapsRowStatus)
{
    tBgp4PeerEntry     *pPeerEntry;
    tBgp4PeerEntry     *pDupPeer;
    tSupCapsInfo       *pSpkrSupCap = NULL;
    tBgp4QMsg          *pQMsg = NULL;
    INT4                i4RetSts;
    UINT4               u4RtMemSts;
    UINT4               u4AfiSafiMask = 0;
    UINT2               u2SupportAfi = 0;
    UINT1               u1SupportSafi = 0;
    UINT1               u1NegFlag = BGP4_FALSE;
    UINT1               u1CapConfig = BGP4_FALSE;
    tSNMP_OCTET_STRING_TYPE CapPeerRemoteIpAddr;
    UINT1               au1PeerAddr[BGP4_MAX_INET_ADDRESS_LEN];
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    UINT4               u4OrfCapMask = 0;
    UINT1               u1OrfMode = 0;
    UINT1               u1DelFlag = BGP4_TRUE;
    UINT1               u1SupOrfMode = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerRemAddrInfo);
    if (pPeerEntry == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    MEMSET (au1PeerAddr, 0, BGP4_MAX_INET_ADDRESS_LEN);
    CapPeerRemoteIpAddr.pu1_OctetList = au1PeerAddr;
    Bgp4LowSetIpAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO
                         (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                         &CapPeerRemoteIpAddr,
                         BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));

    switch (i4SetValFsbgp4CapSupportedCapsRowStatus)
    {
        case CREATE_AND_GO:
            i4RetSts = CapsSpkrSupCapCreate (pPeerEntry,
                                             (UINT1)
                                             i4Fsbgp4SupportedCapabilityCode,
                                             (UINT1)
                                             i4Fsbgp4SupportedCapabilityLength,
                                             pCapValue, &pSpkrSupCap);
            if (i4RetSts == CAPS_FAILURE)
            {
                return SNMP_FAILURE;
            }
            if (i4Fsbgp4SupportedCapabilityCode == CAP_CODE_ROUTE_REFRESH
                && pPeerEntry->pPeerGrp == NULL)
            {
                BGP4_PEER_SHADOW_MP_CAP_MASK (pPeerEntry) |=
                    CAP_NEG_ROUTE_REFRESH_MASK;
            }

            pSpkrSupCap->u1RowStatus = ACTIVE;
            u1CapConfig = BGP4_TRUE;
            break;

        case CREATE_AND_WAIT:
            i4RetSts = CapsSpkrSupCapCreate (pPeerEntry,
                                             (UINT1)
                                             i4Fsbgp4SupportedCapabilityCode,
                                             (UINT1)
                                             i4Fsbgp4SupportedCapabilityLength,
                                             pCapValue, &pSpkrSupCap);
            if (i4RetSts == CAPS_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to Create entry for Capability\n");
                return SNMP_FAILURE;
            }
            pSpkrSupCap->u1RowStatus = NOT_IN_SERVICE;
            break;

        case ACTIVE:
            GetFindSpkrSupCap (pPeerEntry,
                               (UINT1) i4Fsbgp4SupportedCapabilityCode,
                               (UINT1) i4Fsbgp4SupportedCapabilityLength,
                               pCapValue, CAPS_SUP, &pSpkrSupCap);
            if (pSpkrSupCap == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to find Capability Entry.\n");
                return SNMP_FAILURE;
            }
            if (i4Fsbgp4SupportedCapabilityCode == CAP_CODE_ROUTE_REFRESH
                && pPeerEntry->pPeerGrp == NULL)
            {
                BGP4_PEER_SHADOW_MP_CAP_MASK (pPeerEntry) |=
                    CAP_NEG_ROUTE_REFRESH_MASK;
            }

            pSpkrSupCap->u1RowStatus = ACTIVE;

            u1CapConfig = BGP4_TRUE;
            break;

        case NOT_IN_SERVICE:
            GetFindSpkrSupCap (pPeerEntry,
                               (UINT1) i4Fsbgp4SupportedCapabilityCode,
                               (UINT1) i4Fsbgp4SupportedCapabilityLength,
                               pCapValue, CAPS_SUP, &pSpkrSupCap);

            if (pSpkrSupCap == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to find Capability Entry.\n");
                return SNMP_FAILURE;
            }
            pSpkrSupCap->u1RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:

            GetFindSpkrSupCap (pPeerEntry,
                               (UINT1) i4Fsbgp4SupportedCapabilityCode,
                               (UINT1) i4Fsbgp4SupportedCapabilityLength,
                               pCapValue, CAPS_SUP, &pSpkrSupCap);
            if (pSpkrSupCap == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to find Capability Entry.\n");
                return SNMP_FAILURE;
            }

            switch (i4Fsbgp4SupportedCapabilityCode)
            {
                case CAP_CODE_MP_EXTN:
                    PTR_FETCH2 (u2SupportAfi,
                                pSpkrSupCap->SupCapability.au1CapValue);
                    u1SupportSafi = *(pSpkrSupCap->SupCapability.au1CapValue +
                                      BGP4_MPE_ADDRESS_FAMILY_LEN +
                                      BGP4_MPE_MP_CAP_RESERVED_FIELD_LEN);
                    BGP4_GET_AFISAFI_MASK (u2SupportAfi, u1SupportSafi,
                                           u4AfiSafiMask);
                    switch (u4AfiSafiMask)
                    {
                        case CAP_MP_IPV4_UNICAST:
                            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) &
                                 CAP_NEG_IPV4_UNI_MASK) ==
                                CAP_NEG_IPV4_UNI_MASK)
                            {
                                u1NegFlag = BGP4_TRUE;
                            }
                            BGP4_PEER_SUP_ASAFI_MASK (pPeerEntry) &=
                                ~((UINT4) CAP_NEG_IPV4_UNI_MASK);
                            break;
#ifdef BGP4_IPV6_WANTED
                        case CAP_MP_IPV6_UNICAST:
                            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) &
                                 CAP_NEG_IPV6_UNI_MASK) ==
                                CAP_NEG_IPV6_UNI_MASK)
                            {
                                u1NegFlag = BGP4_TRUE;
                            }
                            BGP4_PEER_SUP_ASAFI_MASK (pPeerEntry) &=
                                (UINT4) (~(CAP_NEG_IPV6_UNI_MASK));
                            break;
#endif
#ifdef L3VPN
                        case CAP_MP_LABELLED_IPV4:
                            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) &
                                 CAP_NEG_LBL_IPV4_MASK) ==
                                CAP_NEG_LBL_IPV4_MASK)
                            {
                                u1NegFlag = BGP4_TRUE;
                            }
                            BGP4_PEER_SUP_ASAFI_MASK (pPeerEntry) &=
                                ~((UINT4) CAP_NEG_LBL_IPV4_MASK);
                            break;

                        case CAP_MP_VPN4_UNICAST:
                            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) &
                                 CAP_NEG_VPN4_UNI_MASK) ==
                                CAP_NEG_VPN4_UNI_MASK)
                            {
                                u1NegFlag = BGP4_TRUE;
                            }
                            BGP4_PEER_SUP_ASAFI_MASK (pPeerEntry) &=
                                ~((UINT4) CAP_NEG_VPN4_UNI_MASK);
                            break;
#endif
#ifdef VPLSADS_WANTED
                        case CAP_MP_L2VPN_VPLS:
                            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) &
                                 CAP_NEG_L2VPN_VPLS_MASK) ==
                                CAP_NEG_L2VPN_VPLS_MASK)
                            {
                                u1NegFlag = BGP4_TRUE;
                            }
                            BGP4_PEER_SUP_ASAFI_MASK (pPeerEntry) &=
                                ~((UINT4) CAP_NEG_L2VPN_VPLS_MASK);
                            break;
#endif
#ifdef EVPN_WANTED
                        case CAP_MP_L2VPN_EVPN:
                            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) &
                                 CAP_NEG_L2VPN_EVPN_MASK) ==
                                CAP_NEG_L2VPN_EVPN_MASK)
                            {
                                u1NegFlag = BGP4_TRUE;
                            }
                            BGP4_PEER_SUP_ASAFI_MASK (pPeerEntry) &=
                                ~((UINT4) CAP_NEG_L2VPN_EVPN_MASK);
                            break;
#endif
                        default:
                            break;
                    }
                    break;
                case CAP_CODE_ROUTE_REFRESH:
                    if ((BGP4_PEER_NEG_CAP_MASK (pPeerEntry) &
                         CAP_NEG_ROUTE_REFRESH_MASK) ==
                        CAP_NEG_ROUTE_REFRESH_MASK)
                    {
                        u1NegFlag = BGP4_TRUE;
                    }
                    BGP4_PEER_NEG_CAP_MASK (pPeerEntry) &=
                        (UINT4) ~(CAP_NEG_ROUTE_REFRESH_MASK);
                    BGP4_PEER_SHADOW_MP_CAP_MASK (pPeerEntry) &=
                        ~(CAP_NEG_ROUTE_REFRESH_MASK);
                    break;
                case CAP_CODE_ORF:
                    PTR_FETCH2 (u2SupportAfi,
                                pSpkrSupCap->SupCapability.au1CapValue);
                    u1SupportSafi = *(pSpkrSupCap->SupCapability.au1CapValue +
                                      BGP4_MPE_ADDRESS_FAMILY_LEN +
                                      BGP4_MPE_MP_CAP_RESERVED_FIELD_LEN);

                    u1SupOrfMode =
                        pSpkrSupCap->SupCapability.
                        au1CapValue[BGP_CAP_ORF_MODE_OFFSET];
                    u1OrfMode =
                        pCapValue->pu1_OctetList[BGP_CAP_ORF_MODE_OFFSET];

                    /* If both ORF modes want to be disabled (i.e) u1OrfMode == BGP4_CLI_ORF_MODE_BOTH,
                     * or both the Supported mode and configured mode are same, then delete
                     * corresponding supported Capability entry.
                     * If not, do not delete the entry.
                     */
                    if ((u1SupOrfMode != u1OrfMode)
                        && (u1OrfMode != BGP4_CLI_ORF_MODE_BOTH))
                    {
                        u1DelFlag = BGP4_FALSE;
                    }
                    /* If the mode wants to be disabled is not enabled before,then return */
                    if (!(u1SupOrfMode & u1OrfMode))
                    {
                        return SNMP_SUCCESS;
                    }
                    /* If any of the ORF mode (send/receive) wants to be disabled is one of the
                     * supportd mode, then disable that mode alone
                     */
                    pSpkrSupCap->SupCapability.
                        au1CapValue[BGP_CAP_ORF_MODE_OFFSET] &=
                        (UINT1) ~u1OrfMode;
                    if (u1OrfMode & BGP4_CLI_ORF_MODE_RECEIVE)
                    {
                        BgpOrfDelRcvdOrfEntries (pPeerEntry);
                    }
                    BGP4_GET_AFISAFI_MASK (u2SupportAfi, u1SupportSafi,
                                           u4AfiSafiMask);

                    if (Bgp4OrfGetOrfCapMask (u4AfiSafiMask, u1SupOrfMode,
                                              &u4OrfCapMask) != BGP4_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                    u1NegFlag = BGP4_TRUE;
                    BGP4_PEER_NEG_CAP_MASK (pPeerEntry) &= ~(u4OrfCapMask);
                    BGP4_PEER_SHADOW_ORF_MODE (pPeerEntry) = 0;
                    break;

                case CAP_CODE_4_OCTET_ASNO:
                    if ((BGP4_PEER_NEG_CAP_MASK (pPeerEntry) &
                         CAP_NEG_4_OCTET_ASNO_MASK) ==
                        CAP_NEG_4_OCTET_ASNO_MASK)
                    {
                        u1NegFlag = BGP4_TRUE;
                    }
                    break;

                case CAP_CODE_GRACEFUL_RESTART:
                    if (((pSpkrSupCap)->SupCapability.u1CapLength) ==
                        CAP_GR_CAP_LENGTH)
                    {
                        (pSpkrSupCap)->SupCapability.u1CapLength =
                            MIN_CAP_GR_LENGTH;
                        /* As GR is enabled by default should not delete the entry */
                        u1DelFlag = BGP4_FALSE;
                    }
                    break;

                default:
                    break;
            }

            if (u1DelFlag == BGP4_TRUE)
            {
                TMO_SLL_Delete (BGP4_PEER_SUP_CAPS_LIST (pPeerEntry),
                                &pSpkrSupCap->NextCapability);
                u4RtMemSts = SPKR_SUP_CAPS_ENTRY_FREE (pSpkrSupCap);
                if (u4RtMemSts != CAPS_MEM_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                pSpkrSupCap = NULL;
                pDupPeer = Bgp4SnmphGetDuplicatePeerEntry (pPeerEntry);
                if (pDupPeer != NULL)
                {
                    GetFindSpkrSupCap (pDupPeer,
                                       (UINT1) i4Fsbgp4SupportedCapabilityCode,
                                       (UINT1)
                                       i4Fsbgp4SupportedCapabilityLength,
                                       pCapValue, CAPS_SUP, &pSpkrSupCap);
                    if (pSpkrSupCap == NULL)
                    {
                        return SNMP_FAILURE;
                    }
                    TMO_SLL_Delete (BGP4_PEER_SUP_CAPS_LIST (pDupPeer),
                                    &pSpkrSupCap->NextCapability);
                    u4RtMemSts = SPKR_SUP_CAPS_ENTRY_FREE (pSpkrSupCap);
                    if (u4RtMemSts != CAPS_MEM_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
            }

            if ((u1NegFlag == BGP4_TRUE) &&
                ((BGP4_PEER_ADMIN_STATUS (pPeerEntry) == BGP4_PEER_START) ||
                 (BGP4_PEER_ADMIN_STATUS (pPeerEntry) == BGP4_PEER_AUTO_START)))
            {
                /* Reset peer session */
                pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
                if (pQMsg == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG,
                              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                              BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                              "\tMemory Allocation for posting BGP Peer "
                              "CAP De-Activation to BGP Queue FAILED!!!\n");
                    gu4BgpDebugCnt[MAX_BGP_Q_MSGS_SIZING_ID]++;
                    return SNMP_FAILURE;
                }

                /* Post message to BGP task to indicate this event. */
                BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_CLI_CLEAR_IP_BGP_EVENT;
                BGP4_INPUTQ_CXT (pQMsg) = u4Context;
                BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
                Bgp4CopyAddrPrefixStruct
                    ((&(BGP4_INPUTQ_PEER_ADDR_INFO (pQMsg))),
                     BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));

                BGP4_INPUTQ_CXT (pQMsg) = u4Context;
                /* Send the peer entry to the bgp-task */
                if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
            break;

        default:
            return SNMP_FAILURE;
    }
    if (u1CapConfig == BGP4_TRUE)
    {

        switch (pSpkrSupCap->SupCapability.u1CapCode)
        {
            case CAP_CODE_MP_EXTN:
                PTR_FETCH2 (u2SupportAfi,
                            pSpkrSupCap->SupCapability.au1CapValue);
                u1SupportSafi = *(pSpkrSupCap->SupCapability.au1CapValue +
                                  BGP4_MPE_ADDRESS_FAMILY_LEN +
                                  BGP4_MPE_MP_CAP_RESERVED_FIELD_LEN);
                BGP4_GET_AFISAFI_MASK (u2SupportAfi, u1SupportSafi,
                                       u4AfiSafiMask);
                switch (u4AfiSafiMask)
                {
                    case CAP_MP_IPV4_UNICAST:
                        if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) &
                             CAP_NEG_IPV4_UNI_MASK) != CAP_NEG_IPV4_UNI_MASK)
                        {
                            u1NegFlag = BGP4_TRUE;
                        }
                        BGP4_PEER_SUP_ASAFI_MASK (pPeerEntry) |=
                            CAP_NEG_IPV4_UNI_MASK;
                        break;
#ifdef BGP4_IPV6_WANTED
                    case CAP_MP_IPV6_UNICAST:
                        if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) &
                             CAP_NEG_IPV6_UNI_MASK) != CAP_NEG_IPV6_UNI_MASK)
                        {
                            u1NegFlag = BGP4_TRUE;
                        }

                        BGP4_PEER_SUP_ASAFI_MASK (pPeerEntry) |=
                            CAP_NEG_IPV6_UNI_MASK;
                        break;
#endif
#ifdef L3VPN
                    case CAP_MP_LABELLED_IPV4:
                        if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) &
                             CAP_NEG_LBL_IPV4_MASK) != CAP_NEG_LBL_IPV4_MASK)
                        {
                            u1NegFlag = BGP4_TRUE;
                        }
                        BGP4_PEER_SUP_ASAFI_MASK (pPeerEntry) |=
                            CAP_NEG_LBL_IPV4_MASK;
                        break;

                    case CAP_MP_VPN4_UNICAST:
                        if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) &
                             CAP_NEG_VPN4_UNI_MASK) != CAP_NEG_VPN4_UNI_MASK)
                        {
                            u1NegFlag = BGP4_TRUE;
                        }
                        BGP4_PEER_SUP_ASAFI_MASK (pPeerEntry) |=
                            CAP_NEG_VPN4_UNI_MASK;
                        break;
#endif
#ifdef VPLSADS_WANTED
                    case CAP_MP_L2VPN_VPLS:
                        if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) &
                             CAP_NEG_L2VPN_VPLS_MASK) !=
                            CAP_NEG_L2VPN_VPLS_MASK)
                        {
                            u1NegFlag = BGP4_TRUE;
                        }
                        BGP4_PEER_SUP_ASAFI_MASK (pPeerEntry) |=
                            CAP_NEG_L2VPN_VPLS_MASK;
                        break;
#endif
#ifdef EVPN_WANTED
                    case CAP_MP_L2VPN_EVPN:
                        if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) &
                             CAP_NEG_L2VPN_EVPN_MASK) !=
                            CAP_NEG_L2VPN_EVPN_MASK)
                        {
                            u1NegFlag = BGP4_TRUE;
                        }
                        BGP4_PEER_SUP_ASAFI_MASK (pPeerEntry) |=
                            CAP_NEG_L2VPN_EVPN_MASK;
                        break;
#endif

                    default:
                        break;
                }
                break;
            case CAP_CODE_ROUTE_REFRESH:
                if ((BGP4_PEER_NEG_CAP_MASK (pPeerEntry) &
                     CAP_NEG_ROUTE_REFRESH_MASK) != CAP_NEG_ROUTE_REFRESH_MASK)
                {
                    u1NegFlag = BGP4_TRUE;
                }
                break;
            case CAP_CODE_ORF:
                PTR_FETCH2 (u2SupportAfi,
                            pSpkrSupCap->SupCapability.au1CapValue);
                u1SupportSafi = *(pSpkrSupCap->SupCapability.au1CapValue +
                                  BGP4_MPE_ADDRESS_FAMILY_LEN +
                                  BGP4_MPE_MP_CAP_RESERVED_FIELD_LEN);
                BGP4_GET_AFISAFI_MASK (u2SupportAfi, u1SupportSafi,
                                       u4AfiSafiMask);

                u1OrfMode =
                    *(pSpkrSupCap->SupCapability.au1CapValue +
                      BGP_CAP_ORF_MODE_OFFSET);

                if (Bgp4OrfGetOrfCapMask (u4AfiSafiMask, u1OrfMode,
                                          &u4OrfCapMask) != BGP4_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                /* Check whether the Capability is already negotiated */
                if ((BGP4_PEER_NEG_CAP_MASK (pPeerEntry) &
                     u4OrfCapMask) != u4OrfCapMask)
                {
                    u1NegFlag = BGP4_TRUE;
                }

                break;

            case CAP_CODE_4_OCTET_ASNO:
                if ((BGP4_PEER_NEG_CAP_MASK (pPeerEntry) &
                     CAP_NEG_4_OCTET_ASNO_MASK) != CAP_NEG_4_OCTET_ASNO_MASK)
                {
                    u1NegFlag = BGP4_TRUE;
                }
                break;
            default:
                break;
        }

        if (((u1NegFlag == BGP4_TRUE) &&
             ((BGP4_PEER_ADMIN_STATUS (pPeerEntry) == BGP4_PEER_START) ||
              (BGP4_PEER_ADMIN_STATUS (pPeerEntry) == BGP4_PEER_AUTO_START))) ||
            ((BGP4_GET_PEER_PEND_FLAG (pPeerEntry) &
              BGP4_PEER_MP_CAP_RECV_PEND_START) ==
             BGP4_PEER_MP_CAP_RECV_PEND_START))
        {
            /* Reset peer session */
            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC |
                          BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                          "\tMemory Allocation for posting BGP Peer "
                          "CAP Activation to BGP Queue FAILED!!!\n");
                gu4BgpDebugCnt[MAX_BGP_Q_MSGS_SIZING_ID]++;
                return SNMP_FAILURE;
            }

            /* Post message to BGP task to indicate this event. */
            BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_CLI_CLEAR_IP_BGP_EVENT;
            BGP4_INPUTQ_CXT (pQMsg) = u4Context;
            Bgp4CopyAddrPrefixStruct
                ((&(BGP4_INPUTQ_PEER_ADDR_INFO (pQMsg))),
                 BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));

            BGP4_INPUTQ_CXT (pQMsg) = u4Context;
            /* Send the peer entry to the bgp-task */
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                return SNMP_FAILURE;
            }
        }
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4mpeCapSupportedCapsRowStatus,
                          u4SeqNum, FALSE, BgpLock, BgpUnLock, 6, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %i %i %s %i", u4Context,
                      BGP4_AFI_IN_ADDR_PREFIX_INFO
                      (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                      &(CapPeerRemoteIpAddr), i4Fsbgp4SupportedCapabilityCode,
                      i4Fsbgp4SupportedCapabilityLength, pCapValue,
                      i4SetValFsbgp4CapSupportedCapsRowStatus));

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetBgpVersion
 Input       :  The Indices

                The Object 
                retValBgpVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpVersion (tSNMP_OCTET_STRING_TYPE * pRetValBgpVersion)
{
    UINT1               u1Version = (BGP4_ONE_BYTE_MSB >>
                                     (BGP4_VERSION_4 - BGP4_DECREMENT_BY_ONE));

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    STRNCPY (pRetValBgpVersion->pu1_OctetList, &u1Version, sizeof (UINT1));
    pRetValBgpVersion->i4_Length = sizeof (UINT1);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpLocalAs
 Input       :  The Indices

                The Object 
                retValBgpLocalAs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpLocalAs (INT4 *pi4RetValBgpLocalAs)
{
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    *pi4RetValBgpLocalAs = (INT4) BGP4_GLB_LOCAL_AS_NO;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetBgpIdentifier
 Input       :  The Indices

                The Object 
                retValBgpIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetBgpIdentifier (UINT4 *pu4RetValBgpIdentifier)
{

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    *pu4RetValBgpIdentifier = BGP4_LOCAL_BGP_ID (u4Context);
    return SNMP_SUCCESS;
}

tTcpAoAuthMKT      *
BgpFindTCPAOMktInContext (UINT4 u4Context, INT4 i4MktIndex)
{
    tTcpAoAuthMKT      *pMkt = NULL;

    TMO_SLL_Scan (&(BGP4_TCPAO_MKT_LIST (u4Context)), pMkt, tTcpAoAuthMKT *)
    {
        if (i4MktIndex == (INT4) pMkt->u1SendKeyId)
        {
            return pMkt;
        }
    }
    return NULL;
}

VOID
Bgp4TCPAOClearMKTTable (UINT4 u4Context)
{
    tTcpAoAuthMKT      *pMkt = NULL;
    tTcpAoAuthMKT      *pTempMkt = NULL;

    BGP_SLL_DYN_Scan (&(BGP4_TCPAO_MKT_LIST (u4Context)), pMkt, pTempMkt,
                      tTcpAoAuthMKT *)
    {
        TMO_SLL_Delete (&(BGP4_TCPAO_MKT_LIST (u4Context)),
                        (tTMO_SLL_NODE *) (pMkt));
        Bgp4MemReleaseTcpAoBuf (pMkt, u4Context);
    }

    return;
}

/****************************************************************************
 Function    :  Bgp4GetConfiguredPeerAttribute
 Description :  This function validates inherited peer attributes from peer
        group attributes and peer specific configurations and returns
        BGP4_SUCCESS if peer specific configuration exists (or) peer
                if not associated with peer group,
        BGP4_FAILURE if peer group configuration exists.
 Input       :  i4PeerRemoteAddrType
                PeerRemoteAddr
        u4PeerAttribute
        i4ObjectType
 Output      :  None
 Returns     :  BGP4_SUCCESS or BGP4_FAILURE
****************************************************************************/
INT4
Bgp4GetConfiguredPeerAttribute (INT4 i4PeerRemoteAddrType,
                                tSNMP_OCTET_STRING_TYPE * PeerRemoteAddr,
                                INT4 *i4TableObject, INT4 i4ObjectType)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddress;
    UINT4               u4Context = 0;
    INT4                i4Sts = 0;

    MEMSET (&PeerAddress, 0, sizeof (tAddrPrefix));
    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        return BGP4_FAILURE;
    }

    i4Sts = Bgp4LowGetIpAddress (i4PeerRemoteAddrType,
                                 PeerRemoteAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to get the Peer Address.\n");
        return BGP4_FAILURE;
    }
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);

    if (pPeer != NULL)
    {
        switch (i4ObjectType)
        {
            case BGP4_PEER_EXT_REMOTE_AS_OBJECT:
                *i4TableObject = (INT4) BGP4_PEER_SHADOW_REMOTE_AS_NO (pPeer);
                break;
            case BGP4_PEER_EXT_LOCAL_AS_OBJECT:
                *i4TableObject = (INT4) BGP4_PEER_SHADOW_LOCAL_AS_NO (pPeer);
                break;
            case BGP4_PEER_EXT_LOCAL_AS_CONF_OBJECT:
                *i4TableObject =
                    (INT4) BGP4_PEER_SHADOW_LOCAL_AS_CONFIG (pPeer);
                break;
            case BGP4_PEER_EXT_MULTIHOP_OBJECT:
                *i4TableObject = (INT4) BGP4_PEER_SHADOW_EBGP_MULTIHOP (pPeer);
                break;
            case BGP4_PEER_EXT_NEXTHOP_SELF_OBJECT:
                *i4TableObject = (INT4) BGP4_PEER_SHADOW_SELF_NEXTHOP (pPeer);
                break;
            case BGP4_PEER_EXT_HOPLIMIT_OBJECT:
                *i4TableObject = (INT4) BGP4_PEER_SHADOW_HOP_LIMIT (pPeer);
                break;
            case BGP4_PEER_EXT_TCP_SENDBUF_OBJECT:
                *i4TableObject = (INT4) BGP4_PEER_SHADOW_SEND_BUF (pPeer);
                break;
            case BGP4_PEER_EXT_TCP_RECVBUF_OBJECT:
                *i4TableObject = (INT4) BGP4_PEER_SHADOW_RECV_BUF (pPeer);
                break;
            case BGP4_PEER_EXT_COMM_SEND_STATUS_OBJECT:
                *i4TableObject =
                    (INT4) BGP4_PEER_SHADOW_COMM_SEND_STATUS (pPeer) +
                    BGP4_INCREMENT_BY_ONE;
                break;
            case BGP4_PEER_EXT_ECOMM_SEND_STATUS_OBJECT:
                *i4TableObject =
                    (INT4) BGP4_PEER_SHADOW_ECOMM_SEND_STATUS (pPeer) +
                    BGP4_INCREMENT_BY_ONE;
                break;
            case BGP4_PEER_EXT_PASSIVE_SET_OBJECT:
                *i4TableObject = (INT4) BGP4_PEER_SHADOW_PASSIVE (pPeer);
                break;
            case BGP4_PEER_EXT_OVERRIDE_CAPABILITY_OBJECT:
                *i4TableObject = (INT4) BGP4_PEER_SHADOW_OVERRIDE_CAP (pPeer);
                break;
            case BGP4_PEER_EXT_DEFAULT_ROUTE_OBJECT:
                *i4TableObject =
                    (INT4) BGP4_PEER_SHADOW_DFT_ROUTE_ORIG_STATUS (pPeer);
                break;
            case BGP4_PEER_EXT_RFL_CLIENT_OBJECT:
                *i4TableObject = (INT4) BGP4_PEER_SHADOW_RFL_CLIENT (pPeer);
                break;
            case BGP4_PEER_HOLD_TIME_CONF_OBJECT:
                *i4TableObject = (INT4) BGP4_PEER_SHADOW_HOLD_INTERVAL (pPeer);
                break;
            case BGP4_PEER_KEEPALIVE_CONF_OBJECT:
                *i4TableObject =
                    (INT4) BGP4_PEER_SHADOW_KEEP_ALIVE_INTERVAL (pPeer);
                break;
            case BGP4_PEER_CONNECT_RETRY_TIME_OBJECT:
                *i4TableObject =
                    (INT4) BGP4_PEER_SHADOW_CONNECT_RETRY_INTERVAL (pPeer);
                break;
            case BGP4_PEER_MINAS_ORIG_OBJECT:
                *i4TableObject =
                    (INT4) BGP4_PEER_SHADOW_MIN_AS_ORIG_INTERVAL (pPeer);
                break;
            case BGP4_PEER_MINROUTE_ADVT_OBJECT:
                *i4TableObject =
                    (INT4) BGP4_PEER_SHADOW_MIN_ROUTE_ADV_INTERVAL (pPeer);
                break;
            case BGP4_PEER_ALLOW_AUTOMATIC_START_OBJECT:
                *i4TableObject = (INT4) BGP4_PEER_SHADOW_AUTO_START (pPeer);
                break;
            case BGP4_PEER_ALLOW_AUTOMATIC_STOP_OBJECT:
                *i4TableObject = (INT4) BGP4_PEER_SHADOW_AUTO_STOP (pPeer);
                break;
            case BGP4_PEER_IDLE_HOLD_TIME_CONF_OBJECT:
                *i4TableObject =
                    (INT4) BGP4_PEER_SHADOW_IDLE_HOLDTIME_INTERVAL (pPeer);
                break;
            case BGP4_DAMP_PEER_OSCILLATIONS_OBJECT:
                *i4TableObject =
                    (INT4) BGP4_PEER_SHADOW_DAMP_PEER_OSC_STATUS (pPeer);
                break;
            case BGP4_PEER_DELAY_OPEN_OBJECT:
                *i4TableObject =
                    (INT4) BGP4_PEER_SHADOW_DELAY_OPEN_STATUS (pPeer);
                break;
            case BGP4_PEER_DELAY_OPEN_TIME_CONF_OBJECT:
                *i4TableObject =
                    (INT4) BGP4_PEER_SHADOW_DELAY_OPEN_TIME_INTERVAL (pPeer);
                break;
            case BGP4_PEER_PREFIX_UPPER_LIMIT_OBJECT:
                *i4TableObject =
                    (INT4) BGP4_PEER_SHADOW_PEER_PREFIX_UPPER_LIMIT (pPeer);
                break;
            case BGP4_PEER_TCP_CONNECT_RETRY_COUNT_OBJECT:
                *i4TableObject = (INT4) BGP4_PEER_SHADOW_CONNECT_COUNT (pPeer);
                break;
            case BGP4_PEER_EXT_ACTIVATE_MP_CAP_OBJECT:
                if (((BGP4_PEER_SHADOW_MP_CAP_MASK (pPeer)) &
                     CAP_NEG_ROUTE_REFRESH_MASK) == CAP_NEG_ROUTE_REFRESH_MASK)
                {
                    /* Peer specific configuration */
                    *i4TableObject = BGP4_TRUE;
                }
                else
                {
                    *i4TableObject = BGP4_FALSE;
                    return BGP4_SUCCESS;
                }
                break;
            case BGP4_PEER_EXT_ACTIVATE_ORF_CAP_OBJECT:
                *i4TableObject = (INT4) BGP4_PEER_SHADOW_ORF_MODE (pPeer);
                break;
            case BGP4_PEER_BFD_STATUS_CONF_OBJECT:
                *i4TableObject = (INT4) BGP4_PEER_SHADOW_BFD_STATUS (pPeer);
                break;

            default:
                return BGP4_FAILURE;
        }
        return BGP4_SUCCESS;
    }
    /* Peer Entry not available */
    return BGP4_FAILURE;
}

/*****************************************************************************/
/* Function Name : Bgp4SnmphUpdateGrCapCode                                  */
/* Description   : Update GR capcode according to the support capability     */
/* Input(s)      : peer remote address                                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE/BGP4_FALSE                                      */
/*****************************************************************************/
INT1
Bgp4SnmphUpdateGrCapCode (tAddrPrefix PeerAddressInfo)
{
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tSupCapsInfo       *pSpkrSupCap = NULL;

    UINT4               u4Context;
    u4Context = gpBgpCurrCxtNode->u4ContextId;

    pPeerInfo = Bgp4SnmphGetPeerEntry (u4Context, PeerAddressInfo);
    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to find Peer Entry.\n");
        return BGP4_FALSE;
    }
    TMO_SLL_Scan (BGP4_PEER_SUP_CAPS_LIST (pPeerInfo), pSpkrSupCap,
                  tSupCapsInfo *)
    {
        if (pSpkrSupCap->SupCapability.u1CapCode == CAP_CODE_GRACEFUL_RESTART)
        {

#ifdef BGP4_IPV6_WANTED
            /* Check for supported capability */
            if (((BGP4_PEER_SUP_ASAFI_MASK (pPeerInfo) &
                  ((UINT4) CAP_NEG_IPV6_UNI_MASK)) == BGP4_INET_AFI_IPV6)
                &&
                ((BGP4_PEER_SUP_ASAFI_MASK (pPeerInfo) &
                  ((UINT4) CAP_NEG_IPV4_UNI_MASK)) == BGP4_INET_AFI_IPV4))
            {
                pSpkrSupCap->SupCapability.u1CapLength = CAP_GR_CAP_LENGTH;
                return BGP4_TRUE;
            }
            else if (((BGP4_PEER_SUP_ASAFI_MASK (pPeerInfo) &
                       ((UINT4) CAP_NEG_IPV6_UNI_MASK)) == BGP4_INET_AFI_IPV6)
                     ||
                     ((BGP4_PEER_SUP_ASAFI_MASK (pPeerInfo) &
                       ((UINT4) CAP_NEG_IPV4_UNI_MASK)) == BGP4_INET_AFI_IPV4))
            {
                pSpkrSupCap->SupCapability.u1CapLength = MIN_CAP_GR_LENGTH;
                return BGP4_TRUE;
            }
#else
            if ((BGP4_PEER_SUP_ASAFI_MASK (pPeerInfo) &
                 ((UINT4) CAP_NEG_IPV4_UNI_MASK)) == BGP4_INET_AFI_IPV4)
            {
                pSpkrSupCap->SupCapability.u1CapLength = MIN_CAP_GR_LENGTH;
                return BGP4_TRUE;
            }

#endif
        }
    }                            /*TMO_SLL_Scan */

    return BGP4_FALSE;
}

/*****************************************************************************/
/* Function Name : Bgp4CheckDuplicateIntermediateAS                          */
/* Description   : Checks if duplicate entries for AS exist in input          */
/* Input(s)      : Intermediate AS list                                      */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE/BGP4_FALSE                                      */
/*****************************************************************************/
INT1
Bgp4CheckDuplicateIntermediateAS (UINT4 *au4InterAS, UINT2 u2Asno)
{
    UINT2               u2Index1, u2Index2;

    for (u2Index1 = 0; u2Index1 < u2Asno - 1; u2Index1++)
    {
        for (u2Index2 = u2Index1 + 1; u2Index2 < u2Asno; u2Index2++)
        {
            if (au4InterAS[u2Index1] == au4InterAS[u2Index2])
            {
                return BGP4_FALSE;
            }
        }
    }
    return BGP4_TRUE;
}

#endif
