/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsbgp4lw.c,v 1.13 2017/02/09 14:07:40 siva Exp $
 *
 * Description: Protocol Low Level Routines
 *********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "bgp4com.h"

/* LOW LEVEL Routines for Table : Fsbgp4PeerExtTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4PeerExtTable
Input       :  The Indices
Fsbgp4PeerExtPeerRemoteAddr
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4PeerExtTable (UINT4 u4Fsbgp4PeerExtPeerRemoteAddr)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4RemoteAddr = 0;
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr;

    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerExtPeerRemoteAddr);
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);

    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable (BGP4_INET_AFI_IPV4,
                                                       &Fsbgp4PeerExtPeerRemoteAddr)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4PeerExtTable
Input       :  The Indices
Fsbgp4PeerExtPeerRemoteAddr
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4PeerExtTable (UINT4 *pu4Fsbgp4PeerExtPeerRemoteAddr)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    INT4                i4AddrType;
    UINT4               au4RemoteAddr[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4RemoteAddr, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) au4RemoteAddr;

    if (nmhGetFirstIndexFsbgp4MpePeerExtTable (&i4AddrType,
                                               &Fsbgp4PeerExtPeerRemoteAddr)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4AddrType == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4Fsbgp4PeerExtPeerRemoteAddr,
                            Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpePeerExtTable (i4AddrType,
                                                     &i4AddrType,
                                                     &Fsbgp4PeerExtPeerRemoteAddr,
                                                     &Fsbgp4PeerExtPeerRemoteAddr)
               == SNMP_SUCCESS);

    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4PeerExtTable
Input       :  The Indices
Fsbgp4PeerExtPeerRemoteAddr
nextFsbgp4PeerExtPeerRemoteAddr
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4PeerExtTable (UINT4 u4Fsbgp4PeerExtPeerRemoteAddr,
                                   UINT4 *pu4NextFsbgp4PeerExtPeerRemoteAddr)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    INT4                i4AddrType = BGP4_INET_AFI_IPV4;
    UINT4               au4RemoteAddr[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4RemoteAddr, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) au4RemoteAddr;

    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerExtPeerRemoteAddr);
    if (nmhGetNextIndexFsbgp4MpePeerExtTable
        (BGP4_INET_AFI_IPV4, &i4AddrType, &Fsbgp4PeerExtPeerRemoteAddr,
         &Fsbgp4PeerExtPeerRemoteAddr) == SNMP_SUCCESS)
    {
        do
        {
            if (i4AddrType == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4NextFsbgp4PeerExtPeerRemoteAddr,
                            Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpePeerExtTable (i4AddrType,
                                                     &i4AddrType,
                                                     &Fsbgp4PeerExtPeerRemoteAddr,
                                                     &Fsbgp4PeerExtPeerRemoteAddr)
               == SNMP_SUCCESS);

    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4PeerExtConfigurePeer
Input       :  The Indices
Fsbgp4PeerExtPeerRemoteAddr

The Object 
retValFsbgp4PeerExtConfigurePeer
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4PeerExtConfigurePeer (UINT4 u4Fsbgp4PeerExtPeerRemoteAddr,
                                  INT4 *pi4RetValFsbgp4PeerExtConfigurePeer)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4RemoteAddr = 0;
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr;
    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerExtPeerRemoteAddr);
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);

    if (nmhGetFsbgp4mpePeerExtConfigurePeer (BGP4_INET_AFI_IPV4,
                                             &Fsbgp4PeerExtPeerRemoteAddr,
                                             pi4RetValFsbgp4PeerExtConfigurePeer)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4PeerExtPeerRemoteAs
Input       :  The Indices
Fsbgp4PeerExtPeerRemoteAddr

The Object 
retValFsbgp4PeerExtPeerRemoteAs
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4PeerExtPeerRemoteAs (UINT4 u4Fsbgp4PeerExtPeerRemoteAddr,
                                 UINT4 *pu4RetValFsbgp4PeerExtPeerRemoteAs)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4RemoteAddr = 0;
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr;
    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerExtPeerRemoteAddr);
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);

    if (nmhGetFsbgp4mpePeerExtPeerRemoteAs (BGP4_INET_AFI_IPV4,
                                            &Fsbgp4PeerExtPeerRemoteAddr,
                                            pu4RetValFsbgp4PeerExtPeerRemoteAs)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetFsbgp4PeerExtEBGPMultiHop
Input       :  The Indices
Fsbgp4PeerExtPeerRemoteAddr

The Object 
retValFsbgp4PeerExtEBGPMultiHop
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4PeerExtEBGPMultiHop (UINT4 u4Fsbgp4PeerExtPeerRemoteAddr,
                                 INT4 *pi4RetValFsbgp4PeerExtEBGPMultiHop)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4RemoteAddr = 0;
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr;
    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerExtPeerRemoteAddr);
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);

    if (nmhGetFsbgp4mpePeerExtEBGPMultiHop (BGP4_INET_AFI_IPV4,
                                            &Fsbgp4PeerExtPeerRemoteAddr,
                                            pi4RetValFsbgp4PeerExtEBGPMultiHop)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4PeerExtNextHopSelf
Input       :  The Indices
Fsbgp4PeerExtPeerRemoteAddr

The Object 
retValFsbgp4PeerExtNextHopSelf
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4PeerExtNextHopSelf (UINT4 u4Fsbgp4PeerExtPeerRemoteAddr,
                                INT4 *pi4RetValFsbgp4PeerExtNextHopSelf)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4RemoteAddr = 0;
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr;
    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerExtPeerRemoteAddr);
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);

    if (nmhGetFsbgp4mpePeerExtNextHopSelf (BGP4_INET_AFI_IPV4,
                                           &Fsbgp4PeerExtPeerRemoteAddr,
                                           pi4RetValFsbgp4PeerExtNextHopSelf)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4PeerExtConnSrcIfId
Input       :  The Indices
Fsbgp4PeerExtPeerRemoteAddr

The Object 
retValFsbgp4PeerExtConnSrcIfId
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURu4LocalAddrE
 ****************************************************************************/
INT1
nmhGetFsbgp4PeerExtConnSrcIfId (UINT4 u4Fsbgp4PeerExtPeerRemoteAddr,
                                INT4 *pi4RetValFsbgp4PeerExtConnSrcIfId)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4LocalAddr;
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4LocalAddr;
    UINT4               u4RemoteAddr1 = 0;
    UINT4               u4RemoteAddr2 = 0;

    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr1;
    Fsbgp4LocalAddr.pu1_OctetList = (UINT1 *) (VOID *) &u4RemoteAddr2;
    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerExtPeerRemoteAddr);
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);

    if (nmhGetFsbgp4mpePeerExtLclAddress (BGP4_INET_AFI_IPV4,
                                          &Fsbgp4PeerExtPeerRemoteAddr,
                                          &Fsbgp4LocalAddr) == SNMP_SUCCESS)
    {
        PTR_FETCH4 (u4LocalAddr, Fsbgp4LocalAddr.pu1_OctetList);
        if (u4LocalAddr)
        {
        if (Bgp4GetIfPort (u4LocalAddr, (UINT4 *)
                           pi4RetValFsbgp4PeerExtConnSrcIfId) == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
        else
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4PeerExtRflClient
Input       :  The Indices
Fsbgp4PeerExtPeerRemoteAddr

The Object 
retValFsbgp4PeerExtRflClient
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4PeerExtRflClient (UINT4 u4Fsbgp4PeerExtPeerRemoteAddr,
                              INT4 *pi4RetValFsbgp4PeerExtRflClient)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4RemoteAddr = 0;
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr;
    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerExtPeerRemoteAddr);
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);

    if (nmhGetFsbgp4mpePeerExtRflClient (BGP4_INET_AFI_IPV4,
                                         &Fsbgp4PeerExtPeerRemoteAddr,
                                         pi4RetValFsbgp4PeerExtRflClient)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4VpnRouteLeakStatus
 Input       :  The Indices

                The Object
                retValFsbgp4VpnRouteLeakStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsbgp4VpnRouteLeakStatus(INT4 *pi4RetValFsbgp4VpnRouteLeakStatus)
{
#ifdef L3VPN
    *pi4RetValFsbgp4VpnRouteLeakStatus = (INT4) BGP4_ROUTE_LEAK_FLAG;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4RetValFsbgp4VpnRouteLeakStatus);
    return SNMP_SUCCESS;
#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4PeerExtConfigurePeer
Input       :  The Indices
          Fsbgp4PeerExtPeerRemoteAddr
        The Object 
    setValFsbgp4PeerExtConfigurePeer
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4PeerExtConfigurePeer (UINT4 u4Fsbgp4PeerExtPeerRemoteAddr,
                                  INT4 i4SetValFsbgp4PeerExtConfigurePeer)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4RemoteAddr = 0;
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr;
    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerExtPeerRemoteAddr);
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);

    if (nmhSetFsbgp4mpePeerExtConfigurePeer (BGP4_INET_AFI_IPV4,
                                             &Fsbgp4PeerExtPeerRemoteAddr,
                                             i4SetValFsbgp4PeerExtConfigurePeer)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4PeerExtPeerRemoteAs
Input       :  The Indices
Fsbgp4PeerExtPeerRemoteAddr

The Object 
setValFsbgp4PeerExtPeerRemoteAs
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4PeerExtPeerRemoteAs (UINT4 u4Fsbgp4PeerExtPeerRemoteAddr,
                                 UINT4 u4SetValFsbgp4PeerExtPeerRemoteAs)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4RemoteAddr = 0;
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr;
    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerExtPeerRemoteAddr);
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);

    if (nmhSetFsbgp4mpePeerExtPeerRemoteAs (BGP4_INET_AFI_IPV4,
                                            &Fsbgp4PeerExtPeerRemoteAddr,
                                            u4SetValFsbgp4PeerExtPeerRemoteAs)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4PeerExtEBGPMultiHop
Input       :  The Indices
Fsbgp4PeerExtPeerRemoteAddr

The Object 
setValFsbgp4PeerExtEBGPMultiHop
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4PeerExtEBGPMultiHop (UINT4 u4Fsbgp4PeerExtPeerRemoteAddr,
                                 INT4 i4SetValFsbgp4PeerExtEBGPMultiHop)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4RemoteAddr = 0;
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr;
    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerExtPeerRemoteAddr);
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);

    if (nmhSetFsbgp4mpePeerExtEBGPMultiHop (BGP4_INET_AFI_IPV4,
                                            &Fsbgp4PeerExtPeerRemoteAddr,
                                            i4SetValFsbgp4PeerExtEBGPMultiHop)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4PeerExtNextHopSelf
Input       :  The Indices
Fsbgp4PeerExtPeerRemoteAddr

The Object 
setValFsbgp4PeerExtNextHopSelf
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4PeerExtNextHopSelf (UINT4 u4Fsbgp4PeerExtPeerRemoteAddr,
                                INT4 i4SetValFsbgp4PeerExtNextHopSelf)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4RemoteAddr = 0;
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr;
    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerExtPeerRemoteAddr);
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);

    if (nmhSetFsbgp4mpePeerExtNextHopSelf (BGP4_INET_AFI_IPV4,
                                           &Fsbgp4PeerExtPeerRemoteAddr,
                                           i4SetValFsbgp4PeerExtNextHopSelf)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4PeerExtRflClient
Input       :  The Indices
Fsbgp4PeerExtPeerRemoteAddr

The Object 
setValFsbgp4PeerExtRflClient
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4PeerExtRflClient (UINT4 u4Fsbgp4PeerExtPeerRemoteAddr,
                              INT4 i4SetValFsbgp4PeerExtRflClient)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4RemoteAddr = 0;
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr;
    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerExtPeerRemoteAddr);
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);

    if (nmhSetFsbgp4mpePeerExtRflClient (BGP4_INET_AFI_IPV4,
                                         &Fsbgp4PeerExtPeerRemoteAddr,
                                         i4SetValFsbgp4PeerExtRflClient)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsbgp4VpnRouteLeakStatus
 Input       :  The Indices

                The Object
                setValFsbgp4VpnRouteLeakStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsbgp4VpnRouteLeakStatus(INT4 i4SetValFsbgp4VpnRouteLeakStatus)
{
#ifdef L3VPN
    INT4  i4LeakStatus = 0;
    UINT4  u4Context = BGP4_DFLT_VRFID;
    INT4  i4RetVal = 0;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                BGP4_MGMT_TRC, BGP4_MOD_NAME, 
                "\tERROR - BGP Task not initialized.\n");                                               
        return SNMP_FAILURE;
    }

    nmhGetFsbgp4VpnRouteLeakStatus (&i4LeakStatus);

    if (i4SetValFsbgp4VpnRouteLeakStatus == BGP4_CLI_AFI_ENABLED)
    {
        if ( i4LeakStatus == BGP4_CLI_AFI_ENABLED)
        {
          return SNMP_SUCCESS;
        }
        BGP4_ROUTE_LEAK_FLAG = BGP4_CLI_AFI_ENABLED;
        
        /* If enabled ,Scan and Add all the leaked routes */
        if (u4Context == BGP4_DFLT_VRFID)
        {
            /*Do not Add leaked routes for Default Context*/
            i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
        }
        while (i4RetVal == SNMP_SUCCESS)
        {
            if ((BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4Context) !=
                        BGP4_L3VPN_VRF_UP))
            {
                i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
                continue;
            }
            Bgp4Vpnv4GetLeakRoutesFromPeer (u4Context);
            i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
        }

        return SNMP_SUCCESS;
    }
    else if (i4SetValFsbgp4VpnRouteLeakStatus == BGP4_CLI_AFI_DISABLED)
    {
        if (i4LeakStatus == BGP4_CLI_AFI_DISABLED)
        {
          return SNMP_SUCCESS;
        }
        /* If disabled , scan and delete all the leaked routes. */
        if (u4Context == BGP4_DFLT_VRFID)
        {
            /*Do not Delete leaked routes for Default Context*/
            i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
        }
        while (i4RetVal == SNMP_SUCCESS)
        {
            if ((BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4Context) !=
                        BGP4_L3VPN_VRF_UP))
            {
                i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
                continue;
            }
            Bgp4Vpnv4DelLeakRoutesFromPeer (u4Context);
            i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
        }

        BGP4_ROUTE_LEAK_FLAG = BGP4_CLI_AFI_DISABLED;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
#else
    UNUSED_PARAM (i4SetValFsbgp4VpnRouteLeakStatus);
    return SNMP_SUCCESS;
#endif
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4PeerExtConfigurePeer
Input       :  The Indices
Fsbgp4PeerExtPeerRemoteAddr

The Object 
testValFsbgp4PeerExtConfigurePeer
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4PeerExtConfigurePeer (UINT4 *pu4ErrorCode,
                                     UINT4 u4Fsbgp4PeerExtPeerRemoteAddr,
                                     INT4 i4TestValFsbgp4PeerExtConfigurePeer)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4RemoteAddr = 0;
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr;
    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerExtPeerRemoteAddr);
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);

    if (nmhTestv2Fsbgp4mpePeerExtConfigurePeer (pu4ErrorCode,
                                                BGP4_INET_AFI_IPV4,
                                                &Fsbgp4PeerExtPeerRemoteAddr,
                                                i4TestValFsbgp4PeerExtConfigurePeer)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4PeerExtPeerRemoteAs
Input       :  The Indices
Fsbgp4PeerExtPeerRemoteAddr

The Object 
testValFsbgp4PeerExtPeerRemoteAs
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4PeerExtPeerRemoteAs (UINT4 *pu4ErrorCode,
                                    UINT4 u4Fsbgp4PeerExtPeerRemoteAddr,
                                    UINT4 u4TestValFsbgp4PeerExtPeerRemoteAs)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4RemoteAddr = 0;
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr;
    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerExtPeerRemoteAddr);
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);

    if (nmhTestv2Fsbgp4mpePeerExtPeerRemoteAs (pu4ErrorCode,
                                               BGP4_INET_AFI_IPV4,
                                               &Fsbgp4PeerExtPeerRemoteAddr,
                                               u4TestValFsbgp4PeerExtPeerRemoteAs)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4PeerExtEBGPMultiHop
Input       :  The Indices
Fsbgp4PeerExtPeerRemoteAddr

The Object 
testValFsbgp4PeerExtEBGPMultiHop
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4PeerExtEBGPMultiHop (UINT4 *pu4ErrorCode,
                                    UINT4 u4Fsbgp4PeerExtPeerRemoteAddr,
                                    INT4 i4TestValFsbgp4PeerExtEBGPMultiHop)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4RemoteAddr = 0;
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr;
    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerExtPeerRemoteAddr);
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);

    if (nmhTestv2Fsbgp4mpePeerExtEBGPMultiHop (pu4ErrorCode,
                                               BGP4_INET_AFI_IPV4,
                                               &Fsbgp4PeerExtPeerRemoteAddr,
                                               i4TestValFsbgp4PeerExtEBGPMultiHop)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4PeerExtNextHopSelf
Input       :  The Indices
Fsbgp4PeerExtPeerRemoteAddr

The Object 
testValFsbgp4PeerExtNextHopSelf
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4PeerExtNextHopSelf (UINT4 *pu4ErrorCode,
                                   UINT4 u4Fsbgp4PeerExtPeerRemoteAddr,
                                   INT4 i4TestValFsbgp4PeerExtNextHopSelf)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4RemoteAddr = 0;
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr;
    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerExtPeerRemoteAddr);
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);

    if (nmhTestv2Fsbgp4mpePeerExtNextHopSelf (pu4ErrorCode,
                                              BGP4_INET_AFI_IPV4,
                                              &Fsbgp4PeerExtPeerRemoteAddr,
                                              i4TestValFsbgp4PeerExtNextHopSelf)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4PeerExtRflClient
Input       :  The Indices
Fsbgp4PeerExtPeerRemoteAddr

The Object 
testValFsbgp4PeerExtRflClient
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4PeerExtRflClient (UINT4 *pu4ErrorCode,
                                 UINT4 u4Fsbgp4PeerExtPeerRemoteAddr,
                                 INT4 i4TestValFsbgp4PeerExtRflClient)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4RemoteAddr = 0;
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr;
    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerExtPeerRemoteAddr);
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);

    if (nmhTestv2Fsbgp4mpePeerExtRflClient (pu4ErrorCode,
                                            BGP4_INET_AFI_IPV4,
                                            &Fsbgp4PeerExtPeerRemoteAddr,
                                            i4TestValFsbgp4PeerExtRflClient)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4VpnRouteLeakStatus
 Input       :  The Indices

                The Object
                testValFsbgp4VpnRouteLeakStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2Fsbgp4VpnRouteLeakStatus(UINT4 *pu4ErrorCode , 
                                       INT4 i4TestValFsbgp4VpnRouteLeakStatus)
{

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,                                                                                    BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,                                                    "\tERROR - BGP Task not initialized.\n");                                         
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;                                                                         
    }

    if ((i4TestValFsbgp4VpnRouteLeakStatus == BGP4_CLI_AFI_ENABLED) ||
            (i4TestValFsbgp4VpnRouteLeakStatus == BGP4_CLI_AFI_DISABLED))
    {
        return SNMP_SUCCESS;
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG,
            BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
            "\tERROR - Invalid Address Family.\n");

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;                                                                   return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4PeerExtTable
Input       :  The Indices
Fsbgp4PeerExtPeerRemoteAddr
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4PeerExtTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4VpnRouteLeakStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2Fsbgp4VpnRouteLeakStatus(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4MEDTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4MEDTable
Input       :  The Indices
Fsbgp4MEDIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4MEDTable (INT4 i4Fsbgp4MEDIndex)
{
    if (nmhValidateIndexInstanceFsbgp4MpeMEDTable (i4Fsbgp4MEDIndex) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4MEDTable
Input       :  The Indices
Fsbgp4MEDIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4MEDTable (INT4 *pi4Fsbgp4MEDIndex)
{
    INT4                i4Afi;
    if (nmhGetFirstIndexFsbgp4MpeMEDTable (pi4Fsbgp4MEDIndex) == SNMP_SUCCESS)
    {
        do
        {
            if (nmhGetFsbgp4mpeMEDIPAddrAfi (*pi4Fsbgp4MEDIndex, &i4Afi) ==
                SNMP_SUCCESS)
            {
                if (i4Afi != BGP4_INET_AFI_IPV6)
                {
                    return SNMP_SUCCESS;
                }
            }
        }
        while (nmhGetNextIndexFsbgp4MEDTable
               (*pi4Fsbgp4MEDIndex, pi4Fsbgp4MEDIndex) == SNMP_SUCCESS);

    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4MEDTable
Input       :  The Indices
Fsbgp4MEDIndex
nextFsbgp4MEDIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4MEDTable (INT4 i4Fsbgp4MEDIndex,
                               INT4 *pi4NextFsbgp4MEDIndex)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;

    if (nmhGetNextIndexFsbgp4MpeMEDTable (i4Fsbgp4MEDIndex,
                                          pi4NextFsbgp4MEDIndex) ==
        SNMP_SUCCESS)
    {
        do
        {
            if (nmhGetFsbgp4mpeMEDIPAddrAfi (*pi4NextFsbgp4MEDIndex,
                                             &i4Afi) == SNMP_SUCCESS)
            {
                if (i4Afi != BGP4_INET_AFI_IPV6)
                {
                    return SNMP_SUCCESS;
                }
            }
        }
        while (nmhGetNextIndexFsbgp4MEDTable (*pi4NextFsbgp4MEDIndex,
                                              pi4NextFsbgp4MEDIndex) ==
               SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4MEDAdminStatus
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
retValFsbgp4MEDAdminStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4MEDAdminStatus (INT4 i4Fsbgp4MEDIndex,
                            INT4 *pi4RetValFsbgp4MEDAdminStatus)
{
    INT4                i4Afi;
    if (nmhGetFsbgp4mpeMEDIPAddrAfi (i4Fsbgp4MEDIndex, &i4Afi) == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeMEDAdminStatus (i4Fsbgp4MEDIndex,
                                               pi4RetValFsbgp4MEDAdminStatus) ==
                SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4MEDRemoteAS
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
retValFsbgp4MEDRemoteAS
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4MEDRemoteAS (INT4 i4Fsbgp4MEDIndex,
                         UINT4 *pu4RetValFsbgp4MEDRemoteAS)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    if (nmhGetFsbgp4mpeMEDIPAddrAfi (i4Fsbgp4MEDIndex, &i4Afi) == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeMEDRemoteAS (i4Fsbgp4MEDIndex,
                                            pu4RetValFsbgp4MEDRemoteAS) ==
                SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4MEDIPAddrPrefix
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
retValFsbgp4MEDIPAddrPrefix
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4MEDIPAddrPrefix (INT4 i4Fsbgp4MEDIndex,
                             UINT4 *pu4RetValFsbgp4MEDIPAddrPrefix)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4MEDIPAddrPrefix;
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    UINT4               u4MEDIPAddr = 0;

    Fsbgp4MEDIPAddrPrefix.pu1_OctetList = (UINT1 *) (VOID *) &u4MEDIPAddr;
    if (nmhGetFsbgp4mpeMEDIPAddrAfi (i4Fsbgp4MEDIndex, &i4Afi) == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {

            if (nmhGetFsbgp4mpeMEDIPAddrPrefix (i4Fsbgp4MEDIndex,
                                                &Fsbgp4MEDIPAddrPrefix) ==
                SNMP_SUCCESS)
            {
                PTR_FETCH4 (*pu4RetValFsbgp4MEDIPAddrPrefix,
                            Fsbgp4MEDIPAddrPrefix.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4MEDIPAddrPrefixLen
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
retValFsbgp4MEDIPAddrPrefixLen
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4MEDIPAddrPrefixLen (INT4 i4Fsbgp4MEDIndex,
                                INT4 *pi4RetValFsbgp4MEDIPAddrPrefixLen)
{
    INT4                i4Afi;
    if (nmhGetFsbgp4mpeMEDIPAddrAfi (i4Fsbgp4MEDIndex, &i4Afi) == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeMEDIPAddrPrefixLen (i4Fsbgp4MEDIndex,
                                                   pi4RetValFsbgp4MEDIPAddrPrefixLen)
                == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4MEDIntermediateAS
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
retValFsbgp4MEDIntermediateAS
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4MEDIntermediateAS (INT4 i4Fsbgp4MEDIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsbgp4MEDIntermediateAS)
{
    INT4                i4Afi;
    if (nmhGetFsbgp4mpeMEDIPAddrAfi (i4Fsbgp4MEDIndex, &i4Afi) == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeMEDIntermediateAS (i4Fsbgp4MEDIndex,
                                                  pRetValFsbgp4MEDIntermediateAS)
                == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4MEDDirection
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
retValFsbgp4MEDDirection
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4MEDDirection (INT4 i4Fsbgp4MEDIndex,
                          INT4 *pi4RetValFsbgp4MEDDirection)
{
    INT4                i4Afi;
    if (nmhGetFsbgp4mpeMEDIPAddrAfi (i4Fsbgp4MEDIndex, &i4Afi) == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeMEDDirection (i4Fsbgp4MEDIndex,
                                             pi4RetValFsbgp4MEDDirection) ==
                SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4MEDValue
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
retValFsbgp4MEDValue
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4MEDValue (INT4 i4Fsbgp4MEDIndex, UINT4 *pu4RetValFsbgp4MEDValue)
{
    INT4                i4Afi;
    if (nmhGetFsbgp4mpeMEDIPAddrAfi (i4Fsbgp4MEDIndex, &i4Afi) == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeMEDValue (i4Fsbgp4MEDIndex,
                                         pu4RetValFsbgp4MEDValue) ==
                SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4MEDPreference
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
retValFsbgp4MEDPreference
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4MEDPreference (INT4 i4Fsbgp4MEDIndex,
                           INT4 *pi4RetValFsbgp4MEDPreference)
{
    INT4                i4Afi;
    if (nmhGetFsbgp4mpeMEDIPAddrAfi (i4Fsbgp4MEDIndex, &i4Afi) == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeMEDPreference (i4Fsbgp4MEDIndex,
                                              pi4RetValFsbgp4MEDPreference) ==
                SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4MEDAdminStatus
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
setValFsbgp4MEDAdminStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4MEDAdminStatus (INT4 i4Fsbgp4MEDIndex,
                            INT4 i4SetValFsbgp4MEDAdminStatus)
{
    if (nmhSetFsbgp4mpeMEDAdminStatus (i4Fsbgp4MEDIndex,
                                       i4SetValFsbgp4MEDAdminStatus) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4MEDRemoteAS
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
setValFsbgp4MEDRemoteAS
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4MEDRemoteAS (INT4 i4Fsbgp4MEDIndex, UINT4 u4SetValFsbgp4MEDRemoteAS)
{
    if (nmhSetFsbgp4mpeMEDRemoteAS (i4Fsbgp4MEDIndex,
                                    u4SetValFsbgp4MEDRemoteAS) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4MEDIPAddrPrefix
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
setValFsbgp4MEDIPAddrPrefix
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4MEDIPAddrPrefix (INT4 i4Fsbgp4MEDIndex,
                             UINT4 u4SetValFsbgp4MEDIPAddrPrefix)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4MEDIPAddrPrefix;
    UINT4               u4MEDIPAddr = 0;
    Fsbgp4MEDIPAddrPrefix.pu1_OctetList = (UINT1 *) (VOID *) &u4MEDIPAddr;
    PTR_ASSIGN4 (Fsbgp4MEDIPAddrPrefix.pu1_OctetList,
                 u4SetValFsbgp4MEDIPAddrPrefix);

    if (nmhSetFsbgp4mpeMEDIPAddrPrefix (i4Fsbgp4MEDIndex,
                                        &Fsbgp4MEDIPAddrPrefix) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4MEDIPAddrPrefixLen
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
setValFsbgp4MEDIPAddrPrefixLen
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4MEDIPAddrPrefixLen (INT4 i4Fsbgp4MEDIndex,
                                INT4 i4SetValFsbgp4MEDIPAddrPrefixLen)
{
    if (nmhSetFsbgp4mpeMEDIPAddrPrefixLen (i4Fsbgp4MEDIndex,
                                           i4SetValFsbgp4MEDIPAddrPrefixLen) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4MEDIntermediateAS
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
setValFsbgp4MEDIntermediateAS
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4MEDIntermediateAS (INT4 i4Fsbgp4MEDIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsbgp4MEDIntermediateAS)
{
    if (nmhSetFsbgp4mpeMEDIntermediateAS (i4Fsbgp4MEDIndex,
                                          pSetValFsbgp4MEDIntermediateAS) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4MEDDirection
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
setValFsbgp4MEDDirection
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4MEDDirection (INT4 i4Fsbgp4MEDIndex,
                          INT4 i4SetValFsbgp4MEDDirection)
{
    if (nmhSetFsbgp4mpeMEDDirection (i4Fsbgp4MEDIndex,
                                     i4SetValFsbgp4MEDDirection) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4MEDValue
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
setValFsbgp4MEDValue
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4MEDValue (INT4 i4Fsbgp4MEDIndex, UINT4 u4SetValFsbgp4MEDValue)
{
    if (nmhSetFsbgp4mpeMEDValue (i4Fsbgp4MEDIndex,
                                 u4SetValFsbgp4MEDValue) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4MEDPreference
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
setValFsbgp4MEDPreference
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4MEDPreference (INT4 i4Fsbgp4MEDIndex,
                           INT4 i4SetValFsbgp4MEDPreference)
{
    if (nmhSetFsbgp4mpeMEDPreference (i4Fsbgp4MEDIndex,
                                      i4SetValFsbgp4MEDPreference) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4MEDAdminStatus
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
testValFsbgp4MEDAdminStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4MEDAdminStatus (UINT4 *pu4ErrorCode, INT4 i4Fsbgp4MEDIndex,
                               INT4 i4TestValFsbgp4MEDAdminStatus)
{
    if (nmhTestv2Fsbgp4mpeMEDAdminStatus (pu4ErrorCode,
                                          i4Fsbgp4MEDIndex,
                                          i4TestValFsbgp4MEDAdminStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4MEDRemoteAS
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
testValFsbgp4MEDRemoteAS
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4MEDRemoteAS (UINT4 *pu4ErrorCode, INT4 i4Fsbgp4MEDIndex,
                            UINT4 u4TestValFsbgp4MEDRemoteAS)
{
    if (nmhTestv2Fsbgp4mpeMEDRemoteAS (pu4ErrorCode,
                                       i4Fsbgp4MEDIndex,
                                       u4TestValFsbgp4MEDRemoteAS)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4MEDIPAddrPrefix
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
testValFsbgp4MEDIPAddrPrefix
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4MEDIPAddrPrefix (UINT4 *pu4ErrorCode, INT4 i4Fsbgp4MEDIndex,
                                UINT4 u4TestValFsbgp4MEDIPAddrPrefix)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4MEDIPAddrPrefix;
    UINT4               u4MEDIPAddr = 0;
    Fsbgp4MEDIPAddrPrefix.pu1_OctetList = (UINT1 *) (VOID *) &u4MEDIPAddr;
    PTR_ASSIGN4 (Fsbgp4MEDIPAddrPrefix.pu1_OctetList,
                 u4TestValFsbgp4MEDIPAddrPrefix);
    if (nmhTestv2Fsbgp4mpeMEDIPAddrPrefix
        (pu4ErrorCode, i4Fsbgp4MEDIndex,
         &Fsbgp4MEDIPAddrPrefix) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4MEDIPAddrPrefixLen
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
testValFsbgp4MEDIPAddrPrefixLen
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4MEDIPAddrPrefixLen (UINT4 *pu4ErrorCode, INT4 i4Fsbgp4MEDIndex,
                                   INT4 i4TestValFsbgp4MEDIPAddrPrefixLen)
{
    if (nmhTestv2Fsbgp4mpeMEDIPAddrPrefixLen (pu4ErrorCode,
                                              i4Fsbgp4MEDIndex,
                                              i4TestValFsbgp4MEDIPAddrPrefixLen)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4MEDIntermediateAS
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
testValFsbgp4MEDIntermediateAS
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4MEDIntermediateAS (UINT4 *pu4ErrorCode, INT4 i4Fsbgp4MEDIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsbgp4MEDIntermediateAS)
{
    if (nmhTestv2Fsbgp4mpeMEDIntermediateAS (pu4ErrorCode,
                                             i4Fsbgp4MEDIndex,
                                             pTestValFsbgp4MEDIntermediateAS)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4MEDDirection
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
testValFsbgp4MEDDirection
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4MEDDirection (UINT4 *pu4ErrorCode, INT4 i4Fsbgp4MEDIndex,
                             INT4 i4TestValFsbgp4MEDDirection)
{
    if (nmhTestv2Fsbgp4mpeMEDDirection (pu4ErrorCode,
                                        i4Fsbgp4MEDIndex,
                                        i4TestValFsbgp4MEDDirection)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4MEDValue
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
testValFsbgp4MEDValue
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4MEDValue (UINT4 *pu4ErrorCode, INT4 i4Fsbgp4MEDIndex,
                         UINT4 u4TestValFsbgp4MEDValue)
{
    if (nmhTestv2Fsbgp4mpeMEDValue (pu4ErrorCode,
                                    i4Fsbgp4MEDIndex,
                                    u4TestValFsbgp4MEDValue) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4MEDPreference
Input       :  The Indices
Fsbgp4MEDIndex

The Object 
testValFsbgp4MEDPreference
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4MEDPreference (UINT4 *pu4ErrorCode, INT4 i4Fsbgp4MEDIndex,
                              INT4 i4TestValFsbgp4MEDPreference)
{
    if (nmhTestv2Fsbgp4mpeMEDPreference (pu4ErrorCode,
                                         i4Fsbgp4MEDIndex,
                                         i4TestValFsbgp4MEDPreference)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4MEDTable
Input       :  The Indices
Fsbgp4MEDIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4MEDTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4LocalPrefTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4LocalPrefTable
Input       :  The Indices
Fsbgp4LocalPrefIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4LocalPrefTable (INT4 i4Fsbgp4LocalPrefIndex)
{
    if (nmhValidateIndexInstanceFsbgp4MpeLocalPrefTable (i4Fsbgp4LocalPrefIndex)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4LocalPrefTable
Input       :  The Indices
Fsbgp4LocalPrefIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4LocalPrefTable (INT4 *pi4Fsbgp4LocalPrefIndex)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;

    if (nmhGetFirstIndexFsbgp4MpeLocalPrefTable (pi4Fsbgp4LocalPrefIndex)
        == SNMP_SUCCESS)
    {
        do
        {
            if (nmhGetFsbgp4mpeLocalPrefIPAddrAfi (*pi4Fsbgp4LocalPrefIndex,
                                                   &i4Afi) == SNMP_SUCCESS)
            {
                if (i4Afi != BGP4_INET_AFI_IPV6)
                {
                    return SNMP_SUCCESS;
                }
            }
        }
        while (nmhGetNextIndexFsbgp4MpeLocalPrefTable (*pi4Fsbgp4LocalPrefIndex,
                                                       pi4Fsbgp4LocalPrefIndex)
               == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4LocalPrefTable
Input       :  The Indices
Fsbgp4LocalPrefIndex
nextFsbgp4LocalPrefIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4LocalPrefTable (INT4 i4Fsbgp4LocalPrefIndex,
                                     INT4 *pi4NextFsbgp4LocalPrefIndex)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    if (nmhGetNextIndexFsbgp4MpeLocalPrefTable (i4Fsbgp4LocalPrefIndex,
                                                pi4NextFsbgp4LocalPrefIndex)
        == SNMP_SUCCESS)
    {
        do
        {
            if (nmhGetFsbgp4mpeLocalPrefIPAddrAfi (*pi4NextFsbgp4LocalPrefIndex,
                                                   &i4Afi) == SNMP_SUCCESS)
            {
                if (i4Afi != BGP4_INET_AFI_IPV6)
                {
                    return SNMP_SUCCESS;
                }
            }
        }
        while (nmhGetNextIndexFsbgp4MpeLocalPrefTable
               (*pi4NextFsbgp4LocalPrefIndex,
                pi4NextFsbgp4LocalPrefIndex) == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4LocalPrefAdminStatus
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
retValFsbgp4LocalPrefAdminStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4LocalPrefAdminStatus (INT4 i4Fsbgp4LocalPrefIndex,
                                  INT4 *pi4RetValFsbgp4LocalPrefAdminStatus)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;

    if (nmhGetFsbgp4mpeLocalPrefIPAddrAfi (i4Fsbgp4LocalPrefIndex,
                                           &i4Afi) == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeLocalPrefAdminStatus (i4Fsbgp4LocalPrefIndex,
                                                     pi4RetValFsbgp4LocalPrefAdminStatus)
                != SNMP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4LocalPrefRemoteAS
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
retValFsbgp4LocalPrefRemoteAS
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4LocalPrefRemoteAS (INT4 i4Fsbgp4LocalPrefIndex,
                               UINT4 *pu4RetValFsbgp4LocalPrefRemoteAS)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;

    if (nmhGetFsbgp4mpeLocalPrefIPAddrAfi (i4Fsbgp4LocalPrefIndex,
                                           &i4Afi) == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeLocalPrefRemoteAS (i4Fsbgp4LocalPrefIndex,
                                                  pu4RetValFsbgp4LocalPrefRemoteAS)
                == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetFsbgp4LocalPrefIPAddrPrefix
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
retValFsbgp4LocalPrefIPAddrPrefix
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4LocalPrefIPAddrPrefix (INT4 i4Fsbgp4LocalPrefIndex,
                                   UINT4 *pu4RetValFsbgp4LocalPrefIPAddrPrefix)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4LocalPrefIPAddrPrefix;
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    UINT4               u4PrefIPAddr = 0;

    Fsbgp4LocalPrefIPAddrPrefix.pu1_OctetList =
        (UINT1 *) (VOID *) &u4PrefIPAddr;

    if (nmhGetFsbgp4mpeLocalPrefIPAddrAfi (i4Fsbgp4LocalPrefIndex,
                                           &i4Afi) == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeLocalPrefIPAddrPrefix (i4Fsbgp4LocalPrefIndex,
                                                      &Fsbgp4LocalPrefIPAddrPrefix)
                == SNMP_SUCCESS)
            {
                PTR_FETCH4 (*pu4RetValFsbgp4LocalPrefIPAddrPrefix,
                            Fsbgp4LocalPrefIPAddrPrefix.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4LocalPrefIPAddrPrefixLen
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
retValFsbgp4LocalPrefIPAddrPrefixLen
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4LocalPrefIPAddrPrefixLen (INT4 i4Fsbgp4LocalPrefIndex,
                                      INT4
                                      *pi4RetValFsbgp4LocalPrefIPAddrPrefixLen)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    if (nmhGetFsbgp4mpeLocalPrefIPAddrAfi (i4Fsbgp4LocalPrefIndex,
                                           &i4Afi) == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeLocalPrefIPAddrPrefixLen (i4Fsbgp4LocalPrefIndex,
                                                         pi4RetValFsbgp4LocalPrefIPAddrPrefixLen)
                == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4LocalPrefIntermediateAS
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
retValFsbgp4LocalPrefIntermediateAS
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4LocalPrefIntermediateAS (INT4 i4Fsbgp4LocalPrefIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsbgp4LocalPrefIntermediateAS)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    if (nmhGetFsbgp4mpeLocalPrefIPAddrAfi (i4Fsbgp4LocalPrefIndex,
                                           &i4Afi) == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeLocalPrefIntermediateAS (i4Fsbgp4LocalPrefIndex,
                                                        pRetValFsbgp4LocalPrefIntermediateAS)
                == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetFsbgp4LocalPrefDirection
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
retValFsbgp4LocalPrefDirection
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4LocalPrefDirection (INT4 i4Fsbgp4LocalPrefIndex,
                                INT4 *pi4RetValFsbgp4LocalPrefDirection)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    if (nmhGetFsbgp4mpeLocalPrefIPAddrAfi (i4Fsbgp4LocalPrefIndex,
                                           &i4Afi) == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {

            if (nmhGetFsbgp4mpeLocalPrefDirection (i4Fsbgp4LocalPrefIndex,
                                                   pi4RetValFsbgp4LocalPrefDirection)
                == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetFsbgp4LocalPrefValue
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
retValFsbgp4LocalPrefValue
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4LocalPrefValue (INT4 i4Fsbgp4LocalPrefIndex,
                            UINT4 *pu4RetValFsbgp4LocalPrefValue)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    if (nmhGetFsbgp4mpeLocalPrefIPAddrAfi (i4Fsbgp4LocalPrefIndex,
                                           &i4Afi) == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {

            if (nmhGetFsbgp4mpeLocalPrefValue (i4Fsbgp4LocalPrefIndex,
                                               pu4RetValFsbgp4LocalPrefValue)
                == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetFsbgp4LocalPrefPreference
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
retValFsbgp4LocalPrefPreference
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4LocalPrefPreference (INT4 i4Fsbgp4LocalPrefIndex,
                                 INT4 *pi4RetValFsbgp4LocalPrefPreference)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    if (nmhGetFsbgp4mpeLocalPrefIPAddrAfi (i4Fsbgp4LocalPrefIndex,
                                           &i4Afi) == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeLocalPrefPreference (i4Fsbgp4LocalPrefIndex,
                                                    pi4RetValFsbgp4LocalPrefPreference)
                == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4LocalPrefAdminStatus
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
setValFsbgp4LocalPrefAdminStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4LocalPrefAdminStatus (INT4 i4Fsbgp4LocalPrefIndex,
                                  INT4 i4SetValFsbgp4LocalPrefAdminStatus)
{
    if (nmhSetFsbgp4mpeLocalPrefAdminStatus (i4Fsbgp4LocalPrefIndex,
                                             i4SetValFsbgp4LocalPrefAdminStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4LocalPrefRemoteAS
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
setValFsbgp4LocalPrefRemoteAS
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4LocalPrefRemoteAS (INT4 i4Fsbgp4LocalPrefIndex,
                               UINT4 u4SetValFsbgp4LocalPrefRemoteAS)
{
    if (nmhSetFsbgp4mpeLocalPrefRemoteAS (i4Fsbgp4LocalPrefIndex,
                                          u4SetValFsbgp4LocalPrefRemoteAS)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhSetFsbgp4LocalPrefIPAddrPrefix
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
setValFsbgp4LocalPrefIPAddrPrefix
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4LocalPrefIPAddrPrefix (INT4 i4Fsbgp4LocalPrefIndex,
                                   UINT4 u4SetValFsbgp4LocalPrefIPAddrPrefix)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4LocalPrefIPAddrPrefix;
    UINT4               u4PrefIPAddr = 0;
    Fsbgp4LocalPrefIPAddrPrefix.pu1_OctetList =
        (UINT1 *) (VOID *) &u4PrefIPAddr;

    PTR_ASSIGN4 (Fsbgp4LocalPrefIPAddrPrefix.pu1_OctetList,
                 u4SetValFsbgp4LocalPrefIPAddrPrefix);
    if (nmhSetFsbgp4mpeLocalPrefIPAddrPrefix (i4Fsbgp4LocalPrefIndex,
                                              &Fsbgp4LocalPrefIPAddrPrefix)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4LocalPrefIPAddrPrefixLen
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
setValFsbgp4LocalPrefIPAddrPrefixLen
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4LocalPrefIPAddrPrefixLen (INT4 i4Fsbgp4LocalPrefIndex,
                                      INT4
                                      i4SetValFsbgp4LocalPrefIPAddrPrefixLen)
{
    if (nmhSetFsbgp4mpeLocalPrefIPAddrPrefixLen (i4Fsbgp4LocalPrefIndex,
                                                 i4SetValFsbgp4LocalPrefIPAddrPrefixLen)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4LocalPrefIntermediateAS
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
setValFsbgp4LocalPrefIntermediateAS
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4LocalPrefIntermediateAS (INT4 i4Fsbgp4LocalPrefIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValFsbgp4LocalPrefIntermediateAS)
{
    if (nmhSetFsbgp4mpeLocalPrefIntermediateAS (i4Fsbgp4LocalPrefIndex,
                                                pSetValFsbgp4LocalPrefIntermediateAS)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4LocalPrefDirection
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
setValFsbgp4LocalPrefDirection
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4LocalPrefDirection (INT4 i4Fsbgp4LocalPrefIndex,
                                INT4 i4SetValFsbgp4LocalPrefDirection)
{
    if (nmhSetFsbgp4mpeLocalPrefDirection (i4Fsbgp4LocalPrefIndex,
                                           i4SetValFsbgp4LocalPrefDirection)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4LocalPrefValue
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
setValFsbgp4LocalPrefValue
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4LocalPrefValue (INT4 i4Fsbgp4LocalPrefIndex,
                            UINT4 u4SetValFsbgp4LocalPrefValue)
{
    if (nmhSetFsbgp4mpeLocalPrefValue (i4Fsbgp4LocalPrefIndex,
                                       u4SetValFsbgp4LocalPrefValue)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4LocalPrefPreference
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
setValFsbgp4LocalPrefPreference
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4LocalPrefPreference (INT4 i4Fsbgp4LocalPrefIndex,
                                 INT4 i4SetValFsbgp4LocalPrefPreference)
{
    if (nmhSetFsbgp4mpeLocalPrefPreference (i4Fsbgp4LocalPrefIndex,
                                            i4SetValFsbgp4LocalPrefPreference)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4LocalPrefAdminStatus
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
testValFsbgp4LocalPrefAdminStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4LocalPrefAdminStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4Fsbgp4LocalPrefIndex,
                                     INT4 i4TestValFsbgp4LocalPrefAdminStatus)
{
    if (nmhTestv2Fsbgp4mpeLocalPrefAdminStatus (pu4ErrorCode,
                                                i4Fsbgp4LocalPrefIndex,
                                                i4TestValFsbgp4LocalPrefAdminStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4LocalPrefRemoteAS
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
testValFsbgp4LocalPrefRemoteAS
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4LocalPrefRemoteAS (UINT4 *pu4ErrorCode,
                                  INT4 i4Fsbgp4LocalPrefIndex,
                                  UINT4 u4TestValFsbgp4LocalPrefRemoteAS)
{
    if (nmhTestv2Fsbgp4mpeLocalPrefRemoteAS (pu4ErrorCode,
                                             i4Fsbgp4LocalPrefIndex,
                                             u4TestValFsbgp4LocalPrefRemoteAS)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4LocalPrefIPAddrPrefix
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
testValFsbgp4LocalPrefIPAddrPrefix
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4LocalPrefIPAddrPrefix (UINT4 *pu4ErrorCode,
                                      INT4 i4Fsbgp4LocalPrefIndex,
                                      UINT4
                                      u4TestValFsbgp4LocalPrefIPAddrPrefix)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4LocalPrefIPAddrPrefix;
    UINT4               u4PrefIPAddr = 0;
    Fsbgp4LocalPrefIPAddrPrefix.pu1_OctetList =
        (UINT1 *) (VOID *) &u4PrefIPAddr;

    PTR_ASSIGN4 (Fsbgp4LocalPrefIPAddrPrefix.pu1_OctetList,
                 u4TestValFsbgp4LocalPrefIPAddrPrefix);
    if (nmhTestv2Fsbgp4mpeLocalPrefIPAddrPrefix (pu4ErrorCode,
                                                 i4Fsbgp4LocalPrefIndex,
                                                 &Fsbgp4LocalPrefIPAddrPrefix)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4LocalPrefIPAddrPrefixLen
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
testValFsbgp4LocalPrefIPAddrPrefixLen
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4LocalPrefIPAddrPrefixLen (UINT4 *pu4ErrorCode,
                                         INT4 i4Fsbgp4LocalPrefIndex,
                                         INT4
                                         i4TestValFsbgp4LocalPrefIPAddrPrefixLen)
{
    if (nmhTestv2Fsbgp4mpeLocalPrefIPAddrPrefixLen (pu4ErrorCode,
                                                    i4Fsbgp4LocalPrefIndex,
                                                    i4TestValFsbgp4LocalPrefIPAddrPrefixLen)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4LocalPrefIntermediateAS
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
testValFsbgp4LocalPrefIntermediateAS
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4LocalPrefIntermediateAS (UINT4 *pu4ErrorCode,
                                        INT4 i4Fsbgp4LocalPrefIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValFsbgp4LocalPrefIntermediateAS)
{
    if (nmhTestv2Fsbgp4mpeLocalPrefIntermediateAS (pu4ErrorCode,
                                                   i4Fsbgp4LocalPrefIndex,
                                                   pTestValFsbgp4LocalPrefIntermediateAS)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4LocalPrefDirection
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
testValFsbgp4LocalPrefDirection
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4LocalPrefDirection (UINT4 *pu4ErrorCode,
                                   INT4 i4Fsbgp4LocalPrefIndex,
                                   INT4 i4TestValFsbgp4LocalPrefDirection)
{
    if (nmhTestv2Fsbgp4mpeLocalPrefDirection (pu4ErrorCode,
                                              i4Fsbgp4LocalPrefIndex,
                                              i4TestValFsbgp4LocalPrefDirection)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4LocalPrefValue
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
testValFsbgp4LocalPrefValue
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4LocalPrefValue (UINT4 *pu4ErrorCode, INT4 i4Fsbgp4LocalPrefIndex,
                               UINT4 u4TestValFsbgp4LocalPrefValue)
{
    if (nmhTestv2Fsbgp4mpeLocalPrefValue (pu4ErrorCode,
                                          i4Fsbgp4LocalPrefIndex,
                                          u4TestValFsbgp4LocalPrefValue)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4LocalPrefPreference
Input       :  The Indices
Fsbgp4LocalPrefIndex

The Object 
testValFsbgp4LocalPrefPreference
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4LocalPrefPreference (UINT4 *pu4ErrorCode,
                                    INT4 i4Fsbgp4LocalPrefIndex,
                                    INT4 i4TestValFsbgp4LocalPrefPreference)
{
    if (nmhTestv2Fsbgp4mpeLocalPrefPreference (pu4ErrorCode,
                                               i4Fsbgp4LocalPrefIndex,
                                               i4TestValFsbgp4LocalPrefPreference)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4LocalPrefTable
Input       :  The Indices
Fsbgp4LocalPrefIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4LocalPrefTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4UpdateFilterTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4UpdateFilterTable
Input       :  The Indices
Fsbgp4UpdateFilterIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4UpdateFilterTable (INT4 i4Fsbgp4UpdateFilterIndex)
{
    if (nmhValidateIndexInstanceFsbgp4MpeUpdateFilterTable
        (i4Fsbgp4UpdateFilterIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4UpdateFilterTable
Input       :  The Indices
Fsbgp4UpdateFilterIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4UpdateFilterTable (INT4 *pi4Fsbgp4UpdateFilterIndex)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;

    if (nmhGetFirstIndexFsbgp4MpeUpdateFilterTable (pi4Fsbgp4UpdateFilterIndex)
        == SNMP_SUCCESS)
    {
        do
        {
            if (nmhGetFsbgp4mpeUpdateFilterIPAddrAfi
                (*pi4Fsbgp4UpdateFilterIndex, &i4Afi) == SNMP_SUCCESS)
            {
                if (i4Afi != BGP4_INET_AFI_IPV6)
                {
                    return SNMP_SUCCESS;
                }
            }
        }
        while (nmhGetNextIndexFsbgp4UpdateFilterTable
               (*pi4Fsbgp4UpdateFilterIndex,
                pi4Fsbgp4UpdateFilterIndex) == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4UpdateFilterTable
Input       :  The Indices
Fsbgp4UpdateFilterIndex
nextFsbgp4UpdateFilterIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4UpdateFilterTable (INT4 i4Fsbgp4UpdateFilterIndex,
                                        INT4 *pi4NextFsbgp4UpdateFilterIndex)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    if (nmhGetNextIndexFsbgp4MpeUpdateFilterTable (i4Fsbgp4UpdateFilterIndex,
                                                   pi4NextFsbgp4UpdateFilterIndex)
        == SNMP_SUCCESS)
    {
        do
        {
            if (nmhGetFsbgp4mpeUpdateFilterIPAddrAfi
                (*pi4NextFsbgp4UpdateFilterIndex, &i4Afi) == SNMP_SUCCESS)
            {
                if (i4Afi != BGP4_INET_AFI_IPV6)
                {
                    return SNMP_SUCCESS;
                }
            }
        }
        while (nmhGetNextIndexFsbgp4UpdateFilterTable
               (*pi4NextFsbgp4UpdateFilterIndex,
                pi4NextFsbgp4UpdateFilterIndex) == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4UpdateFilterAdminStatus
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
retValFsbgp4UpdateFilterAdminStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4UpdateFilterAdminStatus (INT4 i4Fsbgp4UpdateFilterIndex,
                                     INT4
                                     *pi4RetValFsbgp4UpdateFilterAdminStatus)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    if (nmhGetFsbgp4mpeUpdateFilterIPAddrAfi (i4Fsbgp4UpdateFilterIndex, &i4Afi)
        == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeUpdateFilterAdminStatus
                (i4Fsbgp4UpdateFilterIndex,
                 pi4RetValFsbgp4UpdateFilterAdminStatus) == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4UpdateFilterRemoteAS
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
retValFsbgp4UpdateFilterRemoteAS
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4UpdateFilterRemoteAS (INT4 i4Fsbgp4UpdateFilterIndex,
                                  UINT4 *pu4RetValFsbgp4UpdateFilterRemoteAS)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    if (nmhGetFsbgp4mpeUpdateFilterIPAddrAfi (i4Fsbgp4UpdateFilterIndex, &i4Afi)
        == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeUpdateFilterRemoteAS (i4Fsbgp4UpdateFilterIndex,
                                                     pu4RetValFsbgp4UpdateFilterRemoteAS)
                == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4UpdateFilterIPAddrPrefix
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
retValFsbgp4UpdateFilterIPAddrPrefix
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4UpdateFilterIPAddrPrefix (INT4 i4Fsbgp4UpdateFilterIndex,
                                      UINT4
                                      *pu4RetValFsbgp4UpdateFilterIPAddrPrefix)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4MEDIPAddrPrefix;
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    UINT4               u4MEDIPAddr = 0;

    Fsbgp4MEDIPAddrPrefix.pu1_OctetList = (UINT1 *) (VOID *) &u4MEDIPAddr;
    if (nmhGetFsbgp4mpeUpdateFilterIPAddrAfi (i4Fsbgp4UpdateFilterIndex, &i4Afi)
        == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {

            if (nmhGetFsbgp4mpeUpdateFilterIPAddrPrefix
                (i4Fsbgp4UpdateFilterIndex,
                 &Fsbgp4MEDIPAddrPrefix) == SNMP_SUCCESS)
            {
                PTR_FETCH4 (*pu4RetValFsbgp4UpdateFilterIPAddrPrefix,
                            Fsbgp4MEDIPAddrPrefix.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4UpdateFilterIPAddrPrefixLen
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
retValFsbgp4UpdateFilterIPAddrPrefixLen
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4UpdateFilterIPAddrPrefixLen (INT4 i4Fsbgp4UpdateFilterIndex,
                                         INT4
                                         *pi4RetValFsbgp4UpdateFilterIPAddrPrefixLen)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    if (nmhGetFsbgp4mpeUpdateFilterIPAddrAfi (i4Fsbgp4UpdateFilterIndex, &i4Afi)
        == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeUpdateFilterIPAddrPrefixLen
                (i4Fsbgp4UpdateFilterIndex,
                 pi4RetValFsbgp4UpdateFilterIPAddrPrefixLen) == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4UpdateFilterIntermediateAS
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
retValFsbgp4UpdateFilterIntermediateAS
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4UpdateFilterIntermediateAS (INT4 i4Fsbgp4UpdateFilterIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValFsbgp4UpdateFilterIntermediateAS)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    if (nmhGetFsbgp4mpeUpdateFilterIPAddrAfi (i4Fsbgp4UpdateFilterIndex, &i4Afi)
        == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeUpdateFilterIntermediateAS
                (i4Fsbgp4UpdateFilterIndex,
                 pRetValFsbgp4UpdateFilterIntermediateAS) == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4UpdateFilterDirection
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
retValFsbgp4UpdateFilterDirection
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4UpdateFilterDirection (INT4 i4Fsbgp4UpdateFilterIndex,
                                   INT4 *pi4RetValFsbgp4UpdateFilterDirection)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    if (nmhGetFsbgp4mpeUpdateFilterIPAddrAfi (i4Fsbgp4UpdateFilterIndex, &i4Afi)
        == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeUpdateFilterDirection (i4Fsbgp4UpdateFilterIndex,
                                                      pi4RetValFsbgp4UpdateFilterDirection)
                == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4UpdateFilterAction
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
retValFsbgp4UpdateFilterAction
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4UpdateFilterAction (INT4 i4Fsbgp4UpdateFilterIndex,
                                INT4 *pi4RetValFsbgp4UpdateFilterAction)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    if (nmhGetFsbgp4mpeUpdateFilterIPAddrAfi (i4Fsbgp4UpdateFilterIndex, &i4Afi)
        == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeUpdateFilterAction (i4Fsbgp4UpdateFilterIndex,
                                                   pi4RetValFsbgp4UpdateFilterAction)
                == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4UpdateFilterAdminStatus
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
setValFsbgp4UpdateFilterAdminStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4UpdateFilterAdminStatus (INT4 i4Fsbgp4UpdateFilterIndex,
                                     INT4 i4SetValFsbgp4UpdateFilterAdminStatus)
{

    if (nmhSetFsbgp4mpeUpdateFilterAdminStatus (i4Fsbgp4UpdateFilterIndex,
                                                i4SetValFsbgp4UpdateFilterAdminStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4UpdateFilterRemoteAS
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
setValFsbgp4UpdateFilterRemoteAS
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4UpdateFilterRemoteAS (INT4 i4Fsbgp4UpdateFilterIndex,
                                  UINT4 u4SetValFsbgp4UpdateFilterRemoteAS)
{
    if (nmhSetFsbgp4mpeUpdateFilterRemoteAS (i4Fsbgp4UpdateFilterIndex,
                                             u4SetValFsbgp4UpdateFilterRemoteAS)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsbgp4UpdateFilterIPAddrPrefix
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
setValFsbgp4UpdateFilterIPAddrPrefix
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4UpdateFilterIPAddrPrefix (INT4 i4Fsbgp4UpdateFilterIndex,
                                      UINT4
                                      u4SetValFsbgp4UpdateFilterIPAddrPrefix)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4UpdateFilterIPAddrPrefix;
    UINT4               u4FilterIPAddr = 0;

    Fsbgp4UpdateFilterIPAddrPrefix.pu1_OctetList =
        (UINT1 *) (VOID *) &u4FilterIPAddr;

    PTR_ASSIGN4 (Fsbgp4UpdateFilterIPAddrPrefix.pu1_OctetList,
                 u4SetValFsbgp4UpdateFilterIPAddrPrefix);

    if (nmhSetFsbgp4mpeUpdateFilterIPAddrPrefix (i4Fsbgp4UpdateFilterIndex,
                                                 &Fsbgp4UpdateFilterIPAddrPrefix)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsbgp4UpdateFilterIPAddrPrefixLen
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
setValFsbgp4UpdateFilterIPAddrPrefixLen
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4UpdateFilterIPAddrPrefixLen (INT4 i4Fsbgp4UpdateFilterIndex,
                                         INT4
                                         i4SetValFsbgp4UpdateFilterIPAddrPrefixLen)
{
    if (nmhSetFsbgp4mpeUpdateFilterIPAddrPrefixLen (i4Fsbgp4UpdateFilterIndex,
                                                    i4SetValFsbgp4UpdateFilterIPAddrPrefixLen)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsbgp4UpdateFilterIntermediateAS
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
setValFsbgp4UpdateFilterIntermediateAS
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4UpdateFilterIntermediateAS (INT4 i4Fsbgp4UpdateFilterIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pSetValFsbgp4UpdateFilterIntermediateAS)
{
    if (nmhSetFsbgp4mpeUpdateFilterIntermediateAS (i4Fsbgp4UpdateFilterIndex,
                                                   pSetValFsbgp4UpdateFilterIntermediateAS)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsbgp4UpdateFilterDirection
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
setValFsbgp4UpdateFilterDirection
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4UpdateFilterDirection (INT4 i4Fsbgp4UpdateFilterIndex,
                                   INT4 i4SetValFsbgp4UpdateFilterDirection)
{
    if (nmhSetFsbgp4mpeUpdateFilterDirection (i4Fsbgp4UpdateFilterIndex,
                                              i4SetValFsbgp4UpdateFilterDirection)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsbgp4UpdateFilterAction
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
setValFsbgp4UpdateFilterAction
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4UpdateFilterAction (INT4 i4Fsbgp4UpdateFilterIndex,
                                INT4 i4SetValFsbgp4UpdateFilterAction)
{
    if (nmhSetFsbgp4mpeUpdateFilterAction (i4Fsbgp4UpdateFilterIndex,
                                           i4SetValFsbgp4UpdateFilterAction)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4UpdateFilterAdminStatus
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
testValFsbgp4UpdateFilterAdminStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4UpdateFilterAdminStatus (UINT4 *pu4ErrorCode,
                                        INT4 i4Fsbgp4UpdateFilterIndex,
                                        INT4
                                        i4TestValFsbgp4UpdateFilterAdminStatus)
{
    if (nmhTestv2Fsbgp4mpeUpdateFilterAdminStatus (pu4ErrorCode,
                                                   i4Fsbgp4UpdateFilterIndex,
                                                   i4TestValFsbgp4UpdateFilterAdminStatus)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4UpdateFilterRemoteAS
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
testValFsbgp4UpdateFilterRemoteAS
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4UpdateFilterRemoteAS (UINT4 *pu4ErrorCode,
                                     INT4 i4Fsbgp4UpdateFilterIndex,
                                     UINT4 u4TestValFsbgp4UpdateFilterRemoteAS)
{
    if (nmhTestv2Fsbgp4mpeUpdateFilterRemoteAS (pu4ErrorCode,
                                                i4Fsbgp4UpdateFilterIndex,
                                                u4TestValFsbgp4UpdateFilterRemoteAS)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4UpdateFilterIPAddrPrefix
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
testValFsbgp4UpdateFilterIPAddrPrefix
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4UpdateFilterIPAddrPrefix (UINT4 *pu4ErrorCode,
                                         INT4 i4Fsbgp4UpdateFilterIndex,
                                         UINT4
                                         u4TestValFsbgp4UpdateFilterIPAddrPrefix)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4UpdateFilterIPAddrPrefix;
    UINT4               u4FilterIPAddr = 0;

    Fsbgp4UpdateFilterIPAddrPrefix.pu1_OctetList =
        (UINT1 *) (VOID *) &u4FilterIPAddr;
    PTR_ASSIGN4 (Fsbgp4UpdateFilterIPAddrPrefix.pu1_OctetList,
                 u4TestValFsbgp4UpdateFilterIPAddrPrefix);

    if (nmhTestv2Fsbgp4mpeUpdateFilterIPAddrPrefix (pu4ErrorCode,
                                                    i4Fsbgp4UpdateFilterIndex,
                                                    &Fsbgp4UpdateFilterIPAddrPrefix)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4UpdateFilterIPAddrPrefixLen
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
testValFsbgp4UpdateFilterIPAddrPrefixLen
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4UpdateFilterIPAddrPrefixLen (UINT4 *pu4ErrorCode,
                                            INT4 i4Fsbgp4UpdateFilterIndex,
                                            INT4
                                            i4TestValFsbgp4UpdateFilterIPAddrPrefixLen)
{
    if (nmhTestv2Fsbgp4mpeUpdateFilterIPAddrPrefixLen (pu4ErrorCode,
                                                       i4Fsbgp4UpdateFilterIndex,
                                                       i4TestValFsbgp4UpdateFilterIPAddrPrefixLen)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4UpdateFilterIntermediateAS
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
testValFsbgp4UpdateFilterIntermediateAS
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4UpdateFilterIntermediateAS (UINT4 *pu4ErrorCode,
                                           INT4 i4Fsbgp4UpdateFilterIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pTestValFsbgp4UpdateFilterIntermediateAS)
{
    if (nmhTestv2Fsbgp4mpeUpdateFilterIntermediateAS (pu4ErrorCode,
                                                      i4Fsbgp4UpdateFilterIndex,
                                                      pTestValFsbgp4UpdateFilterIntermediateAS)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4UpdateFilterDirection
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
testValFsbgp4UpdateFilterDirection
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4UpdateFilterDirection (UINT4 *pu4ErrorCode,
                                      INT4 i4Fsbgp4UpdateFilterIndex,
                                      INT4 i4TestValFsbgp4UpdateFilterDirection)
{
    if (nmhTestv2Fsbgp4mpeUpdateFilterDirection (pu4ErrorCode,
                                                 i4Fsbgp4UpdateFilterIndex,
                                                 i4TestValFsbgp4UpdateFilterDirection)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4UpdateFilterAction
Input       :  The Indices
Fsbgp4UpdateFilterIndex

The Object 
testValFsbgp4UpdateFilterAction
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4UpdateFilterAction (UINT4 *pu4ErrorCode,
                                   INT4 i4Fsbgp4UpdateFilterIndex,
                                   INT4 i4TestValFsbgp4UpdateFilterAction)
{
    if (nmhTestv2Fsbgp4mpeUpdateFilterAction (pu4ErrorCode,
                                              i4Fsbgp4UpdateFilterIndex,
                                              i4TestValFsbgp4UpdateFilterAction)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4UpdateFilterTable
Input       :  The Indices
Fsbgp4UpdateFilterIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4UpdateFilterTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4AggregateTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4AggregateTable
Input       :  The Indices
Fsbgp4AggregateIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4AggregateTable (INT4 i4Fsbgp4AggregateIndex)
{
    if (nmhValidateIndexInstanceFsbgp4MpeAggregateTable (i4Fsbgp4AggregateIndex)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4AggregateTable
Input       :  The Indices
Fsbgp4AggregateIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4AggregateTable (INT4 *pi4Fsbgp4AggregateIndex)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    if (nmhGetFirstIndexFsbgp4MpeAggregateTable (pi4Fsbgp4AggregateIndex)
        == SNMP_SUCCESS)
    {
        do
        {
            if (nmhGetFsbgp4mpeAggregateIPAddrAfi (*pi4Fsbgp4AggregateIndex,
                                                   &i4Afi) == SNMP_SUCCESS)
            {
                if (i4Afi != BGP4_INET_AFI_IPV6)
                {
                    return SNMP_SUCCESS;
                }
            }
        }
        while (nmhGetNextIndexFsbgp4MpeAggregateTable (*pi4Fsbgp4AggregateIndex,
                                                       pi4Fsbgp4AggregateIndex)
               == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4AggregateTable
Input       :  The Indices
Fsbgp4AggregateIndex
nextFsbgp4AggregateIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4AggregateTable (INT4 i4Fsbgp4AggregateIndex,
                                     INT4 *pi4NextFsbgp4AggregateIndex)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    if (nmhGetNextIndexFsbgp4MpeAggregateTable (i4Fsbgp4AggregateIndex,
                                                pi4NextFsbgp4AggregateIndex)
        == SNMP_SUCCESS)
    {
        do
        {
            if (nmhGetFsbgp4mpeAggregateIPAddrAfi (*pi4NextFsbgp4AggregateIndex,
                                                   &i4Afi) == SNMP_SUCCESS)
            {
                if (i4Afi != BGP4_INET_AFI_IPV6)
                {
                    return SNMP_SUCCESS;
                }
            }
        }
        while (nmhGetNextIndexFsbgp4MpeAggregateTable
               (*pi4NextFsbgp4AggregateIndex,
                pi4NextFsbgp4AggregateIndex) == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4AggregateAdminStatus
Input       :  The Indices
Fsbgp4AggregateIndex

The Object 
retValFsbgp4AggregateAdminStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4AggregateAdminStatus (INT4 i4Fsbgp4AggregateIndex,
                                  INT4 *pi4RetValFsbgp4AggregateAdminStatus)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    if (nmhGetFsbgp4mpeAggregateIPAddrAfi (i4Fsbgp4AggregateIndex,
                                           &i4Afi) == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeAggregateAdminStatus (i4Fsbgp4AggregateIndex,
                                                     pi4RetValFsbgp4AggregateAdminStatus)
                == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4AggregateIPAddrPrefix
Input       :  The Indices
Fsbgp4AggregateIndex

The Object 
retValFsbgp4AggregateIPAddrPrefix
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4AggregateIPAddrPrefix (INT4 i4Fsbgp4AggregateIndex,
                                   UINT4 *pu4RetValFsbgp4AggregateIPAddrPrefix)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4AggregateIndex;
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    UINT4               u4AggregateIndex = 0;

    Fsbgp4AggregateIndex.pu1_OctetList = (UINT1 *) (VOID *) &u4AggregateIndex;

    if (nmhGetFsbgp4mpeAggregateIPAddrAfi (i4Fsbgp4AggregateIndex,
                                           &i4Afi) == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeAggregateIPAddrPrefix (i4Fsbgp4AggregateIndex,
                                                      &Fsbgp4AggregateIndex)
                == SNMP_SUCCESS)
            {
                PTR_FETCH4 (*pu4RetValFsbgp4AggregateIPAddrPrefix,
                            Fsbgp4AggregateIndex.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4AggregateIPAddrPrefixLen
Input       :  The Indices
Fsbgp4AggregateIndex

The Object 
retValFsbgp4AggregateIPAddrPrefixLen
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4AggregateIPAddrPrefixLen (INT4 i4Fsbgp4AggregateIndex,
                                      INT4
                                      *pi4RetValFsbgp4AggregateIPAddrPrefixLen)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    if (nmhGetFsbgp4mpeAggregateIPAddrAfi (i4Fsbgp4AggregateIndex,
                                           &i4Afi) == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeAggregateIPAddrPrefixLen (i4Fsbgp4AggregateIndex,
                                                         pi4RetValFsbgp4AggregateIPAddrPrefixLen)
                == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4AggregateAdvertise
Input       :  The Indices
Fsbgp4AggregateIndex

The Object 
retValFsbgp4AggregateAdvertise
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4AggregateAdvertise (INT4 i4Fsbgp4AggregateIndex,
                                INT4 *pi4RetValFsbgp4AggregateAdvertise)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    if (nmhGetFsbgp4mpeAggregateIPAddrAfi (i4Fsbgp4AggregateIndex,
                                           &i4Afi) == SNMP_SUCCESS)
    {
        if (i4Afi != BGP4_INET_AFI_IPV6)
        {
            if (nmhGetFsbgp4mpeAggregateAdvertise (i4Fsbgp4AggregateIndex,
                                                   pi4RetValFsbgp4AggregateAdvertise)
                == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4AggregateAdminStatus
Input       :  The Indices
Fsbgp4AggregateIndex

The Object 
setValFsbgp4AggregateAdminStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4AggregateAdminStatus (INT4 i4Fsbgp4AggregateIndex,
                                  INT4 i4SetValFsbgp4AggregateAdminStatus)
{
    if (nmhSetFsbgp4mpeAggregateAdminStatus (i4Fsbgp4AggregateIndex,
                                             i4SetValFsbgp4AggregateAdminStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhSetFsbgp4AggregateIPAddrPrefix
Input       :  The Indices
Fsbgp4AggregateIndex

The Object 
setValFsbgp4AggregateIPAddrPrefix
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4AggregateIPAddrPrefix (INT4 i4Fsbgp4AggregateIndex,
                                   UINT4 u4SetValFsbgp4AggregateIPAddrPrefix)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4AggregateIndex;
    UINT4               u4AggregateIndex = 0;
    Fsbgp4AggregateIndex.pu1_OctetList = (UINT1 *) (VOID *) &u4AggregateIndex;
    PTR_ASSIGN4 (Fsbgp4AggregateIndex.pu1_OctetList,
                 u4SetValFsbgp4AggregateIPAddrPrefix);
    if (nmhSetFsbgp4mpeAggregateIPAddrPrefix (i4Fsbgp4AggregateIndex,
                                              &Fsbgp4AggregateIndex) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4AggregateIPAddrPrefixLen
Input       :  The Indices
Fsbgp4AggregateIndex

The Object 
setValFsbgp4AggregateIPAddrPrefixLen
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4AggregateIPAddrPrefixLen (INT4 i4Fsbgp4AggregateIndex,
                                      INT4
                                      i4SetValFsbgp4AggregateIPAddrPrefixLen)
{
    if (nmhSetFsbgp4mpeAggregateIPAddrPrefixLen (i4Fsbgp4AggregateIndex,
                                                 i4SetValFsbgp4AggregateIPAddrPrefixLen)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsbgp4AggregateAdvertise
Input       :  The Indices
Fsbgp4AggregateIndex

The Object 
setValFsbgp4AggregateAdvertise
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4AggregateAdvertise (INT4 i4Fsbgp4AggregateIndex,
                                INT4 i4SetValFsbgp4AggregateAdvertise)
{
    if (nmhSetFsbgp4mpeAggregateAdvertise (i4Fsbgp4AggregateIndex,
                                           i4SetValFsbgp4AggregateAdvertise)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4AggregateAdminStatus
Input       :  The Indices
Fsbgp4AggregateIndex

The Object 
testValFsbgp4AggregateAdminStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4AggregateAdminStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4Fsbgp4AggregateIndex,
                                     INT4 i4TestValFsbgp4AggregateAdminStatus)
{
    if (nmhTestv2Fsbgp4mpeAggregateAdminStatus (pu4ErrorCode,
                                                i4Fsbgp4AggregateIndex,
                                                i4TestValFsbgp4AggregateAdminStatus)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4AggregateIPAddrPrefix
Input       :  The Indices
Fsbgp4AggregateIndex

The Object 
testValFsbgp4AggregateIPAddrPrefix
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4AggregateIPAddrPrefix (UINT4 *pu4ErrorCode,
                                      INT4 i4Fsbgp4AggregateIndex,
                                      UINT4
                                      u4TestValFsbgp4AggregateIPAddrPrefix)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4AggregateIPAddrPrefix;
    UINT4               u4AggregateIPAddr;
    Fsbgp4AggregateIPAddrPrefix.pu1_OctetList =
        (UINT1 *) (VOID *) &u4AggregateIPAddr;
    PTR_ASSIGN4 (Fsbgp4AggregateIPAddrPrefix.pu1_OctetList,
                 u4TestValFsbgp4AggregateIPAddrPrefix);
    if (nmhTestv2Fsbgp4mpeAggregateIPAddrPrefix
        (pu4ErrorCode, i4Fsbgp4AggregateIndex,
         &Fsbgp4AggregateIPAddrPrefix) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4AggregateIPAddrPrefixLen
Input       :  The Indices
Fsbgp4AggregateIndex

The Object 
testValFsbgp4AggregateIPAddrPrefixLen
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4AggregateIPAddrPrefixLen (UINT4 *pu4ErrorCode,
                                         INT4 i4Fsbgp4AggregateIndex,
                                         INT4
                                         i4TestValFsbgp4AggregateIPAddrPrefixLen)
{
    if (nmhTestv2Fsbgp4mpeAggregateIPAddrPrefixLen (pu4ErrorCode,
                                                    i4Fsbgp4AggregateIndex,
                                                    i4TestValFsbgp4AggregateIPAddrPrefixLen)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4AggregateAdvertise
Input       :  The Indices
Fsbgp4AggregateIndex

The Object 
testValFsbgp4AggregateAdvertise
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4AggregateAdvertise (UINT4 *pu4ErrorCode,
                                   INT4 i4Fsbgp4AggregateIndex,
                                   INT4 i4TestValFsbgp4AggregateAdvertise)
{
    if (nmhTestv2Fsbgp4mpeAggregateAdvertise (pu4ErrorCode,
                                              i4Fsbgp4AggregateIndex,
                                              i4TestValFsbgp4AggregateAdvertise)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4AggregateTable
Input       :  The Indices
Fsbgp4AggregateIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4AggregateTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4ImportRouteTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4ImportRouteTable
Input       :  The Indices
Fsbgp4ImportRoutePrefix
Fsbgp4ImportRoutePrefixLen
Fsbgp4ImportRouteProtocol
Fsbgp4ImportRouteNextHop
Fsbgp4ImportRouteIfIndex
Fsbgp4ImportRouteMetric
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4ImportRouteTable (UINT4 u4Fsbgp4ImportRoutePrefix,
                                                INT4
                                                i4Fsbgp4ImportRoutePrefixLen,
                                                INT4
                                                i4Fsbgp4ImportRouteProtocol,
                                                UINT4
                                                u4Fsbgp4ImportRouteNextHop,
                                                INT4 i4Fsbgp4ImportRouteIfIndex,
                                                INT4 i4Fsbgp4ImportRouteMetric)
{
    UNUSED_PARAM (u4Fsbgp4ImportRoutePrefix);
    UNUSED_PARAM (i4Fsbgp4ImportRoutePrefixLen);
    UNUSED_PARAM (i4Fsbgp4ImportRouteProtocol);
    UNUSED_PARAM (u4Fsbgp4ImportRouteNextHop);
    UNUSED_PARAM (i4Fsbgp4ImportRouteIfIndex);
    UNUSED_PARAM (i4Fsbgp4ImportRouteMetric);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4ImportRouteTable
Input       :  The Indices
Fsbgp4ImportRoutePrefix
Fsbgp4ImportRoutePrefixLen
Fsbgp4ImportRouteProtocol
Fsbgp4ImportRouteNextHop
Fsbgp4ImportRouteIfIndex
Fsbgp4ImportRouteMetric
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4ImportRouteTable (UINT4 *pu4Fsbgp4ImportRoutePrefix,
                                        INT4 *pi4Fsbgp4ImportRoutePrefixLen,
                                        INT4 *pi4Fsbgp4ImportRouteProtocol,
                                        UINT4 *pu4Fsbgp4ImportRouteNextHop,
                                        INT4 *pi4Fsbgp4ImportRouteIfIndex,
                                        INT4 *pi4Fsbgp4ImportRouteMetric)
{

    UNUSED_PARAM (pu4Fsbgp4ImportRoutePrefix);
    UNUSED_PARAM (pi4Fsbgp4ImportRoutePrefixLen);
    UNUSED_PARAM (pi4Fsbgp4ImportRouteProtocol);
    UNUSED_PARAM (pu4Fsbgp4ImportRouteNextHop);
    UNUSED_PARAM (pi4Fsbgp4ImportRouteIfIndex);
    UNUSED_PARAM (pi4Fsbgp4ImportRouteMetric);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4ImportRouteTable
Input       :  The Indices
Fsbgp4ImportRoutePrefix
nextFsbgp4ImportRoutePrefix
Fsbgp4ImportRoutePrefixLen
nextFsbgp4ImportRoutePrefixLen
Fsbgp4ImportRouteProtocol
nextFsbgp4ImportRouteProtocol
Fsbgp4ImportRouteNextHop
nextFsbgp4ImportRouteNextHop
Fsbgp4ImportRouteIfIndex
nextFsbgp4ImportRouteIfIndex
Fsbgp4ImportRouteMetric
nextFsbgp4ImportRouteMetric
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4ImportRouteTable (UINT4 u4Fsbgp4ImportRoutePrefix,
                                       UINT4 *pu4NextFsbgp4ImportRoutePrefix,
                                       INT4 i4Fsbgp4ImportRoutePrefixLen,
                                       INT4 *pi4NextFsbgp4ImportRoutePrefixLen,
                                       INT4 i4Fsbgp4ImportRouteProtocol,
                                       INT4 *pi4NextFsbgp4ImportRouteProtocol,
                                       UINT4 u4Fsbgp4ImportRouteNextHop,
                                       UINT4 *pu4NextFsbgp4ImportRouteNextHop,
                                       INT4 i4Fsbgp4ImportRouteIfIndex,
                                       INT4 *pi4NextFsbgp4ImportRouteIfIndex,
                                       INT4 i4Fsbgp4ImportRouteMetric,
                                       INT4 *pi4NextFsbgp4ImportRouteMetric)
{
    UNUSED_PARAM (u4Fsbgp4ImportRoutePrefix);
    UNUSED_PARAM (i4Fsbgp4ImportRoutePrefixLen);
    UNUSED_PARAM (i4Fsbgp4ImportRouteProtocol);
    UNUSED_PARAM (u4Fsbgp4ImportRouteNextHop);
    UNUSED_PARAM (i4Fsbgp4ImportRouteIfIndex);
    UNUSED_PARAM (i4Fsbgp4ImportRouteMetric);
    UNUSED_PARAM (pu4NextFsbgp4ImportRoutePrefix);
    UNUSED_PARAM (pi4NextFsbgp4ImportRoutePrefixLen);
    UNUSED_PARAM (pi4NextFsbgp4ImportRouteProtocol);
    UNUSED_PARAM (pu4NextFsbgp4ImportRouteNextHop);
    UNUSED_PARAM (pi4NextFsbgp4ImportRouteIfIndex);
    UNUSED_PARAM (pi4NextFsbgp4ImportRouteMetric);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4ImportRouteAction
Input       :  The Indices
Fsbgp4ImportRoutePrefix
Fsbgp4ImportRoutePrefixLen
Fsbgp4ImportRouteProtocol
Fsbgp4ImportRouteNextHop
Fsbgp4ImportRouteIfIndex
Fsbgp4ImportRouteMetric

The Object 
retValFsbgp4ImportRouteAction
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4ImportRouteAction (UINT4 u4Fsbgp4ImportRoutePrefix,
                               INT4 i4Fsbgp4ImportRoutePrefixLen,
                               INT4 i4Fsbgp4ImportRouteProtocol,
                               UINT4 u4Fsbgp4ImportRouteNextHop,
                               INT4 i4Fsbgp4ImportRouteIfIndex,
                               INT4 i4Fsbgp4ImportRouteMetric,
                               INT4 *pi4RetValFsbgp4ImportRouteAction)
{
    UNUSED_PARAM (u4Fsbgp4ImportRoutePrefix);
    UNUSED_PARAM (i4Fsbgp4ImportRoutePrefixLen);
    UNUSED_PARAM (i4Fsbgp4ImportRouteProtocol);
    UNUSED_PARAM (u4Fsbgp4ImportRouteNextHop);
    UNUSED_PARAM (i4Fsbgp4ImportRouteIfIndex);
    UNUSED_PARAM (i4Fsbgp4ImportRouteMetric);
    UNUSED_PARAM (pi4RetValFsbgp4ImportRouteAction);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4ImportRouteAction
Input       :  The Indices
Fsbgp4ImportRoutePrefix
Fsbgp4ImportRoutePrefixLen
Fsbgp4ImportRouteProtocol
Fsbgp4ImportRouteNextHop
Fsbgp4ImportRouteIfIndex
Fsbgp4ImportRouteMetric

The Object 
setValFsbgp4ImportRouteAction
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4ImportRouteAction (UINT4 u4Fsbgp4ImportRoutePrefix,
                               INT4 i4Fsbgp4ImportRoutePrefixLen,
                               INT4 i4Fsbgp4ImportRouteProtocol,
                               UINT4 u4Fsbgp4ImportRouteNextHop,
                               INT4 i4Fsbgp4ImportRouteIfIndex,
                               INT4 i4Fsbgp4ImportRouteMetric,
                               INT4 i4SetValFsbgp4ImportRouteAction)
{
    UNUSED_PARAM (u4Fsbgp4ImportRoutePrefix);
    UNUSED_PARAM (i4Fsbgp4ImportRoutePrefixLen);
    UNUSED_PARAM (i4Fsbgp4ImportRouteProtocol);
    UNUSED_PARAM (u4Fsbgp4ImportRouteNextHop);
    UNUSED_PARAM (i4Fsbgp4ImportRouteIfIndex);
    UNUSED_PARAM (i4Fsbgp4ImportRouteMetric);
    UNUSED_PARAM (i4SetValFsbgp4ImportRouteAction);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4ImportRouteAction
Input       :  The Indices
Fsbgp4ImportRoutePrefix
Fsbgp4ImportRoutePrefixLen
Fsbgp4ImportRouteProtocol
Fsbgp4ImportRouteNextHop
Fsbgp4ImportRouteIfIndex
Fsbgp4ImportRouteMetric

The Object 
testValFsbgp4ImportRouteAction
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4ImportRouteAction (UINT4 *pu4ErrorCode,
                                  UINT4 u4Fsbgp4ImportRoutePrefix,
                                  INT4 i4Fsbgp4ImportRoutePrefixLen,
                                  INT4 i4Fsbgp4ImportRouteProtocol,
                                  UINT4 u4Fsbgp4ImportRouteNextHop,
                                  INT4 i4Fsbgp4ImportRouteIfIndex,
                                  INT4 i4Fsbgp4ImportRouteMetric,
                                  INT4 i4TestValFsbgp4ImportRouteAction)
{
    UNUSED_PARAM (u4Fsbgp4ImportRoutePrefix);
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsbgp4ImportRoutePrefixLen);
    UNUSED_PARAM (i4Fsbgp4ImportRouteProtocol);
    UNUSED_PARAM (u4Fsbgp4ImportRouteNextHop);
    UNUSED_PARAM (i4Fsbgp4ImportRouteIfIndex);
    UNUSED_PARAM (i4Fsbgp4ImportRouteMetric);
    UNUSED_PARAM (i4TestValFsbgp4ImportRouteAction);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4ImportRouteTable
Input       :  The Indices
Fsbgp4ImportRoutePrefix
Fsbgp4ImportRoutePrefixLen
Fsbgp4ImportRouteProtocol
Fsbgp4ImportRouteNextHop
Fsbgp4ImportRouteIfIndex
Fsbgp4ImportRouteMetric
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4ImportRouteTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4FsmTransitionHistTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4FsmTransitionHistTable
Input       :  The Indices
Fsbgp4Peer
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4FsmTransitionHistTable (UINT4 u4Fsbgp4Peer)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4Peer;
    UINT4               u4BgpPeer = 0;
    Fsbgp4Peer.pu1_OctetList = (UINT1 *) (VOID *) &u4BgpPeer;

    PTR_ASSIGN4 (Fsbgp4Peer.pu1_OctetList, u4Fsbgp4Peer);
    Fsbgp4Peer.i4_Length = sizeof (UINT4);

    if (nmhValidateIndexInstanceFsbgp4MpeFsmTransitionHistTable
        (BGP4_INET_AFI_IPV4, &Fsbgp4Peer) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4FsmTransitionHistTable
Input       :  The Indices
Fsbgp4Peer
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4FsmTransitionHistTable (UINT4 *pu4Fsbgp4Peer)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4Peer;
    INT4                i4PeerAfi;
    UINT4               au4BgpPeer[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4BgpPeer, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4Peer.pu1_OctetList = (UINT1 *) (VOID *) au4BgpPeer;

    if (nmhGetFirstIndexFsbgp4MpeFsmTransitionHistTable (&i4PeerAfi,
                                                         &Fsbgp4Peer)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4PeerAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4Fsbgp4Peer, Fsbgp4Peer.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeFsmTransitionHistTable
               (i4PeerAfi, &i4PeerAfi, &Fsbgp4Peer,
                &Fsbgp4Peer) == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4FsmTransitionHistTable
Input       :  The Indices
Fsbgp4Peer
nextFsbgp4Peer
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4FsmTransitionHistTable (UINT4 u4Fsbgp4Peer,
                                             UINT4 *pu4NextFsbgp4Peer)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4Peer;
    INT4                i4NextAddrType;
    UINT4               au4BgpPeer[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4BgpPeer, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4Peer.pu1_OctetList = (UINT1 *) (VOID *) au4BgpPeer;;

    PTR_ASSIGN4 (Fsbgp4Peer.pu1_OctetList, u4Fsbgp4Peer);

    if (nmhGetNextIndexFsbgp4MpeFsmTransitionHistTable (BGP4_INET_AFI_IPV4,
                                                        &i4NextAddrType,
                                                        &Fsbgp4Peer,
                                                        &Fsbgp4Peer)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4NextAddrType == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4NextFsbgp4Peer, Fsbgp4Peer.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeFsmTransitionHistTable (i4NextAddrType,
                                                               &i4NextAddrType,
                                                               &Fsbgp4Peer,
                                                               &Fsbgp4Peer)
               == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4FsmTransitionHist
Input       :  The Indices
Fsbgp4Peer

The Object 
retValFsbgp4FsmTransitionHist
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4FsmTransitionHist (UINT4 u4Fsbgp4Peer,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsbgp4FsmTransitionHist)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4Peer;
    UINT4               u4Bgp4Peer = 0;

    Fsbgp4Peer.pu1_OctetList = (UINT1 *) (VOID *) &u4Bgp4Peer;
    PTR_ASSIGN4 (Fsbgp4Peer.pu1_OctetList, u4Fsbgp4Peer);
    Fsbgp4Peer.i4_Length = sizeof (UINT4);

    if (nmhGetFsbgp4mpeFsmTransitionHist (BGP4_INET_AFI_IPV4,
                                          &Fsbgp4Peer,
                                          pRetValFsbgp4FsmTransitionHist)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Fsbgp4RflRouteReflectorTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4RflRouteReflectorTable
Input       :  The Indices
Fsbgp4RflPathAttrAddrPrefix
Fsbgp4RflPathAttrAddrPrefixLen
Fsbgp4RflPathAttrPeer
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4RflRouteReflectorTable (UINT4
                                                      u4Fsbgp4RflPathAttrAddrPrefix,
                                                      INT4
                                                      i4Fsbgp4RflPathAttrAddrPrefixLen,
                                                      UINT4
                                                      u4Fsbgp4RflPathAttrPeer)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4RflPathAttrAddrPrefix;
    tSNMP_OCTET_STRING_TYPE Fsbgp4RflPathAttrPeer;
    UINT4               u4PathAttrAddr = 0;
    UINT4               u4PathAttrPeer = 0;

    Fsbgp4RflPathAttrAddrPrefix.pu1_OctetList =
        (UINT1 *) (VOID *) &u4PathAttrAddr;
    Fsbgp4RflPathAttrPeer.pu1_OctetList = (UINT1 *) (VOID *) &u4PathAttrPeer;
    if (Fsbgp4RflPathAttrPeer.pu1_OctetList == NULL)
    {
        return SNMP_FAILURE;
    }
    PTR_ASSIGN4 (Fsbgp4RflPathAttrPeer.pu1_OctetList, u4Fsbgp4RflPathAttrPeer);
    PTR_ASSIGN4 (Fsbgp4RflPathAttrAddrPrefix.pu1_OctetList,
                 u4Fsbgp4RflPathAttrAddrPrefix);

    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable (BGP4_INET_AFI_IPV4,
                                                            BGP4_INET_SAFI_UNICAST,
                                                            &Fsbgp4RflPathAttrAddrPrefix,
                                                            i4Fsbgp4RflPathAttrAddrPrefixLen,
                                                            BGP4_INET_AFI_IPV4,
                                                            &Fsbgp4RflPathAttrPeer)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4RflRouteReflectorTable
Input       :  The Indices
Fsbgp4RflPathAttrAddrPrefix
Fsbgp4RflPathAttrAddrPrefixLen
Fsbgp4RflPathAttrPeer
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4RflRouteReflectorTable (UINT4
                                              *pu4Fsbgp4RflPathAttrAddrPrefix,
                                              INT4
                                              *pi4Fsbgp4RflPathAttrAddrPrefixLen,
                                              UINT4 *pu4Fsbgp4RflPathAttrPeer)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4RflPathAttrAddrPrefix;
    tSNMP_OCTET_STRING_TYPE Fsbgp4RflPathAttrPeer;
    INT4                i4RouteAfi;
    INT4                i4RouteSafi;
    INT4                i4PeerAfi;
    UINT4               au4PathAttrAddr[BGP4_MAX_INET_ADDRESS_LEN];
    UINT4               au4PathAttrPeer[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4PathAttrAddr, 0, BGP4_MAX_INET_ADDRESS_LEN);
    MEMSET (au4PathAttrPeer, 0, BGP4_MAX_INET_ADDRESS_LEN);

    Fsbgp4RflPathAttrAddrPrefix.pu1_OctetList =
        (UINT1 *) (VOID *) au4PathAttrAddr;
    Fsbgp4RflPathAttrPeer.pu1_OctetList = (UINT1 *) (VOID *) au4PathAttrPeer;

    if (nmhGetFirstIndexFsbgp4MpeBgp4PathAttrTable (&i4RouteAfi,
                                                    &i4RouteSafi,
                                                    &Fsbgp4RflPathAttrAddrPrefix,
                                                    pi4Fsbgp4RflPathAttrAddrPrefixLen,
                                                    &i4PeerAfi,
                                                    &Fsbgp4RflPathAttrPeer)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4RouteAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4Fsbgp4RflPathAttrAddrPrefix,
                            Fsbgp4RflPathAttrAddrPrefix.pu1_OctetList);

                PTR_FETCH4 (*pu4Fsbgp4RflPathAttrPeer,
                            Fsbgp4RflPathAttrPeer.pu1_OctetList);

                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeBgp4PathAttrTable (i4RouteAfi,
                                                          &i4RouteAfi,
                                                          i4RouteSafi,
                                                          &i4RouteSafi,
                                                          &Fsbgp4RflPathAttrAddrPrefix,
                                                          &Fsbgp4RflPathAttrAddrPrefix,
                                                          *pi4Fsbgp4RflPathAttrAddrPrefixLen,
                                                          pi4Fsbgp4RflPathAttrAddrPrefixLen,
                                                          i4PeerAfi,
                                                          &i4PeerAfi,
                                                          &Fsbgp4RflPathAttrPeer,
                                                          &Fsbgp4RflPathAttrPeer)
               == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4RflRouteReflectorTable
Input       :  The Indices
Fsbgp4RflPathAttrAddrPrefix
nextFsbgp4RflPathAttrAddrPrefix
Fsbgp4RflPathAttrAddrPrefixLen
nextFsbgp4RflPathAttrAddrPrefixLen
Fsbgp4RflPathAttrPeer
nextFsbgp4RflPathAttrPeer
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4RflRouteReflectorTable (UINT4
                                             u4Fsbgp4RflPathAttrAddrPrefix,
                                             UINT4
                                             *pu4NextFsbgp4RflPathAttrAddrPrefix,
                                             INT4
                                             i4Fsbgp4RflPathAttrAddrPrefixLen,
                                             INT4
                                             *pi4NextFsbgp4RflPathAttrAddrPrefixLen,
                                             UINT4 u4Fsbgp4RflPathAttrPeer,
                                             UINT4
                                             *pu4NextFsbgp4RflPathAttrPeer)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4RflPathAttrAddrPrefix;
    tSNMP_OCTET_STRING_TYPE Fsbgp4RflPathAttrPeer;
    INT4                i4RouteAfi;
    INT4                i4RouteSafi;
    INT4                i4PeerAfi;
    UINT4               au4PathAttrAddr[BGP4_MAX_INET_ADDRESS_LEN];
    UINT4               au4PathAttrPeer[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4PathAttrAddr, 0, BGP4_MAX_INET_ADDRESS_LEN);
    MEMSET (au4PathAttrPeer, 0, BGP4_MAX_INET_ADDRESS_LEN);

    Fsbgp4RflPathAttrAddrPrefix.pu1_OctetList =
        (UINT1 *) (VOID *) au4PathAttrAddr;
    Fsbgp4RflPathAttrPeer.pu1_OctetList = (UINT1 *) (VOID *) au4PathAttrPeer;

    PTR_ASSIGN4 (Fsbgp4RflPathAttrPeer.pu1_OctetList, u4Fsbgp4RflPathAttrPeer);
    PTR_ASSIGN4 (Fsbgp4RflPathAttrAddrPrefix.pu1_OctetList,
                 u4Fsbgp4RflPathAttrAddrPrefix);
    if (nmhGetNextIndexFsbgp4MpeBgp4PathAttrTable (BGP4_INET_AFI_IPV4,
                                                   &i4RouteAfi,
                                                   BGP4_INET_SAFI_UNICAST,
                                                   &i4RouteSafi,
                                                   &Fsbgp4RflPathAttrAddrPrefix,
                                                   &Fsbgp4RflPathAttrAddrPrefix,
                                                   i4Fsbgp4RflPathAttrAddrPrefixLen,
                                                   pi4NextFsbgp4RflPathAttrAddrPrefixLen,
                                                   BGP4_INET_AFI_IPV4,
                                                   &i4PeerAfi,
                                                   &Fsbgp4RflPathAttrPeer,
                                                   &Fsbgp4RflPathAttrPeer)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4RouteAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4NextFsbgp4RflPathAttrAddrPrefix,
                            Fsbgp4RflPathAttrAddrPrefix.pu1_OctetList);
                PTR_FETCH4 (*pu4NextFsbgp4RflPathAttrPeer,
                            Fsbgp4RflPathAttrPeer.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeBgp4PathAttrTable (i4RouteAfi,
                                                          &i4RouteAfi,
                                                          i4RouteSafi,
                                                          &i4RouteSafi,
                                                          &Fsbgp4RflPathAttrAddrPrefix,
                                                          &Fsbgp4RflPathAttrAddrPrefix,
                                                          *pi4NextFsbgp4RflPathAttrAddrPrefixLen,
                                                          pi4NextFsbgp4RflPathAttrAddrPrefixLen,
                                                          i4PeerAfi,
                                                          &i4PeerAfi,
                                                          &Fsbgp4RflPathAttrPeer,
                                                          &Fsbgp4RflPathAttrPeer)
               == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4RflPathAttrOriginatorId
Input       :  The Indices
Fsbgp4RflPathAttrAddrPrefix
Fsbgp4RflPathAttrAddrPrefixLen
Fsbgp4RflPathAttrPeer

The Object 
retValFsbgp4RflPathAttrOriginatorId
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RflPathAttrOriginatorId (UINT4 u4Fsbgp4RflPathAttrAddrPrefix,
                                     INT4 i4Fsbgp4RflPathAttrAddrPrefixLen,
                                     UINT4 u4Fsbgp4RflPathAttrPeer,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsbgp4RflPathAttrOriginatorId)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4RflPathAttrAddrPrefix;
    tSNMP_OCTET_STRING_TYPE Fsbgp4RflPathAttrPeer;
    UINT4               u4PathAttrAddr = 0;
    UINT4               u4PathAttrPeer = 0;

    Fsbgp4RflPathAttrAddrPrefix.pu1_OctetList =
        (UINT1 *) (VOID *) &u4PathAttrAddr;
    Fsbgp4RflPathAttrPeer.pu1_OctetList = (UINT1 *) (VOID *) &u4PathAttrPeer;

    PTR_ASSIGN4 (Fsbgp4RflPathAttrPeer.pu1_OctetList, u4Fsbgp4RflPathAttrPeer);
    PTR_ASSIGN4 (Fsbgp4RflPathAttrAddrPrefix.pu1_OctetList,
                 u4Fsbgp4RflPathAttrAddrPrefix);
    if (nmhGetFsbgp4mpebgp4PathAttrOriginatorId (BGP4_INET_AFI_IPV4,
                                                 BGP4_INET_SAFI_UNICAST,
                                                 &Fsbgp4RflPathAttrAddrPrefix,
                                                 i4Fsbgp4RflPathAttrAddrPrefixLen,
                                                 BGP4_INET_AFI_IPV4,
                                                 &Fsbgp4RflPathAttrPeer,
                                                 pRetValFsbgp4RflPathAttrOriginatorId)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsbgp4RflPathAttrClusterList
Input       :  The Indices
Fsbgp4RflPathAttrAddrPrefix
Fsbgp4RflPathAttrAddrPrefixLen
Fsbgp4RflPathAttrPeer

The Object 
retValFsbgp4RflPathAttrClusterList
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RflPathAttrClusterList (UINT4 u4Fsbgp4RflPathAttrAddrPrefix,
                                    INT4 i4Fsbgp4RflPathAttrAddrPrefixLen,
                                    UINT4 u4Fsbgp4RflPathAttrPeer,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsbgp4RflPathAttrClusterList)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4RflPathAttrAddrPrefix;
    tSNMP_OCTET_STRING_TYPE Fsbgp4RflPathAttrPeer;
    UINT4               u4PathAttrAddr = 0;
    UINT4               u4PathAttrPeer = 0;

    Fsbgp4RflPathAttrAddrPrefix.pu1_OctetList =
        (UINT1 *) (VOID *) &u4PathAttrAddr;
    Fsbgp4RflPathAttrPeer.pu1_OctetList = (UINT1 *) (VOID *) &u4PathAttrPeer;

    PTR_ASSIGN4 (Fsbgp4RflPathAttrPeer.pu1_OctetList, u4Fsbgp4RflPathAttrPeer);
    PTR_ASSIGN4 (Fsbgp4RflPathAttrAddrPrefix.pu1_OctetList,
                 u4Fsbgp4RflPathAttrAddrPrefix);
    if (nmhGetFsbgp4mpebgp4PathAttrClusterList (BGP4_INET_AFI_IPV4,
                                                BGP4_INET_SAFI_UNICAST,
                                                &Fsbgp4RflPathAttrAddrPrefix,
                                                i4Fsbgp4RflPathAttrAddrPrefixLen,
                                                BGP4_INET_AFI_IPV4,
                                                &Fsbgp4RflPathAttrPeer,
                                                pRetValFsbgp4RflPathAttrClusterList)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4RfdRtDampHistTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4RfdRtDampHistTable
Input       :  The Indices
Fsbgp4PathAttrAddrPrefix
Fsbgp4PathAttrAddrPrefixLen
Fsbgp4PathAttrPeer
Fsbgp4RtDampHistInstance
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4RfdRtDampHistTable (UINT4
                                                  u4Fsbgp4PathAttrAddrPrefix,
                                                  INT4
                                                  i4Fsbgp4PathAttrAddrPrefixLen,
                                                  UINT4 u4Fsbgp4PathAttrPeer,
                                                  INT4
                                                  i4Fsbgp4RtDampHistInstance)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PathAttrAddrPrefix;
    tSNMP_OCTET_STRING_TYPE Fsbgp4PathAttrPeer;
    UINT4               u4PathAttrAddr = 0;
    UINT4               u4PathAttrPeer = 0;

    UNUSED_PARAM (i4Fsbgp4RtDampHistInstance);
    Fsbgp4PathAttrPeer.pu1_OctetList = (UINT1 *) (VOID *) &u4PathAttrPeer;
    Fsbgp4PathAttrAddrPrefix.pu1_OctetList = (UINT1 *) (VOID *) &u4PathAttrAddr;

    PTR_ASSIGN4 (Fsbgp4PathAttrPeer.pu1_OctetList, u4Fsbgp4PathAttrPeer);
    PTR_ASSIGN4 (Fsbgp4PathAttrAddrPrefix.pu1_OctetList,
                 u4Fsbgp4PathAttrAddrPrefix);
    if (nmhValidateIndexInstanceFsbgp4MpeRfdRtDampHistTable (BGP4_INET_AFI_IPV4,
                                                             BGP4_INET_SAFI_UNICAST,
                                                             &Fsbgp4PathAttrAddrPrefix,
                                                             i4Fsbgp4PathAttrAddrPrefixLen,
                                                             BGP4_INET_AFI_IPV4,
                                                             &Fsbgp4PathAttrPeer)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4RfdRtDampHistTable
Input       :  The Indices
Fsbgp4PathAttrAddrPrefix
Fsbgp4PathAttrAddrPrefixLen
Fsbgp4PathAttrPeer
Fsbgp4RtDampHistInstance
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4RfdRtDampHistTable (UINT4 *pu4Fsbgp4PathAttrAddrPrefix,
                                          INT4 *pi4Fsbgp4PathAttrAddrPrefixLen,
                                          UINT4 *pu4Fsbgp4PathAttrPeer,
                                          INT4 *pi4Fsbgp4RtDampHistInstance)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PathAttrAddrPrefix;
    tSNMP_OCTET_STRING_TYPE Fsbgp4PathAttrPeer;
    INT4                i4RouteAfi;
    INT4                i4RouteSafi;
    INT4                i4PeerAfi;
    UINT4               u4PathAttrAddr = 0;
    UINT4               u4PathAttrPeer = 0;

    UNUSED_PARAM (pi4Fsbgp4RtDampHistInstance);
    Fsbgp4PathAttrPeer.pu1_OctetList = (UINT1 *) (VOID *) &u4PathAttrPeer;
    Fsbgp4PathAttrAddrPrefix.pu1_OctetList = (UINT1 *) (VOID *) &u4PathAttrAddr;

    if (nmhGetFirstIndexFsbgp4MpeRfdRtDampHistTable (&i4RouteAfi,
                                                     &i4RouteSafi,
                                                     &Fsbgp4PathAttrAddrPrefix,
                                                     pi4Fsbgp4PathAttrAddrPrefixLen,
                                                     &i4PeerAfi,
                                                     &Fsbgp4PathAttrPeer)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4RouteAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4Fsbgp4PathAttrAddrPrefix,
                            Fsbgp4PathAttrAddrPrefix.pu1_OctetList);
                PTR_FETCH4 (*pu4Fsbgp4PathAttrPeer,
                            Fsbgp4PathAttrPeer.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeRfdRtDampHistTable (i4RouteAfi,
                                                           &i4RouteAfi,
                                                           i4RouteSafi,
                                                           &i4RouteSafi,
                                                           &Fsbgp4PathAttrAddrPrefix,
                                                           &Fsbgp4PathAttrAddrPrefix,
                                                           *pi4Fsbgp4PathAttrAddrPrefixLen,
                                                           pi4Fsbgp4PathAttrAddrPrefixLen,
                                                           i4PeerAfi,
                                                           &i4PeerAfi,
                                                           &Fsbgp4PathAttrPeer,
                                                           &Fsbgp4PathAttrPeer)
               == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4RfdRtDampHistTable
Input       :  The Indices
Fsbgp4PathAttrAddrPrefix
nextFsbgp4PathAttrAddrPrefix
Fsbgp4PathAttrAddrPrefixLen
nextFsbgp4PathAttrAddrPrefixLen
Fsbgp4PathAttrPeer
nextFsbgp4PathAttrPeer
Fsbgp4RtDampHistInstance
nextFsbgp4RtDampHistInstance
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4RfdRtDampHistTable (UINT4 u4Fsbgp4PathAttrAddrPrefix,
                                         UINT4 *pu4NextFsbgp4PathAttrAddrPrefix,
                                         INT4 i4Fsbgp4PathAttrAddrPrefixLen,
                                         INT4
                                         *pi4NextFsbgp4PathAttrAddrPrefixLen,
                                         UINT4 u4Fsbgp4PathAttrPeer,
                                         UINT4 *pu4NextFsbgp4PathAttrPeer,
                                         INT4 i4Fsbgp4RtDampHistInstance,
                                         INT4 *pi4NextFsbgp4RtDampHistInstance)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PathAttrAddrPrefix;
    tSNMP_OCTET_STRING_TYPE Fsbgp4PathAttrPeer;
    INT4                i4RouteAfi;
    INT4                i4RouteSafi;
    INT4                i4PeerAfi;
    UINT4               au4PathAttrAddr[BGP4_MAX_INET_ADDRESS_LEN];
    UINT4               au4PathAttrPeer[BGP4_MAX_INET_ADDRESS_LEN];

    UNUSED_PARAM (i4Fsbgp4RtDampHistInstance);
    UNUSED_PARAM (pi4NextFsbgp4RtDampHistInstance);

    MEMSET (au4PathAttrAddr, 0, BGP4_MAX_INET_ADDRESS_LEN);
    MEMSET (au4PathAttrPeer, 0, BGP4_MAX_INET_ADDRESS_LEN);

    Fsbgp4PathAttrPeer.pu1_OctetList = (UINT1 *) (VOID *) au4PathAttrPeer;
    Fsbgp4PathAttrAddrPrefix.pu1_OctetList = (UINT1 *) (VOID *) au4PathAttrAddr;

    PTR_ASSIGN4 (Fsbgp4PathAttrPeer.pu1_OctetList, u4Fsbgp4PathAttrPeer);
    PTR_ASSIGN4 (Fsbgp4PathAttrAddrPrefix.pu1_OctetList,
                 u4Fsbgp4PathAttrAddrPrefix);
    if (nmhGetNextIndexFsbgp4MpeRfdRtDampHistTable (BGP4_INET_AFI_IPV4,
                                                    &i4RouteAfi,
                                                    BGP4_INET_SAFI_UNICAST,
                                                    &i4RouteSafi,
                                                    &Fsbgp4PathAttrAddrPrefix,
                                                    &Fsbgp4PathAttrAddrPrefix,
                                                    i4Fsbgp4PathAttrAddrPrefixLen,
                                                    pi4NextFsbgp4PathAttrAddrPrefixLen,
                                                    BGP4_INET_AFI_IPV4,
                                                    &i4PeerAfi,
                                                    &Fsbgp4PathAttrPeer,
                                                    &Fsbgp4PathAttrPeer)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4RouteAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4NextFsbgp4PathAttrAddrPrefix,
                            Fsbgp4PathAttrAddrPrefix.pu1_OctetList);
                PTR_FETCH4 (*pu4NextFsbgp4PathAttrPeer,
                            Fsbgp4PathAttrPeer.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeRfdRtDampHistTable (i4RouteAfi,
                                                           &i4RouteAfi,
                                                           i4RouteSafi,
                                                           &i4RouteSafi,
                                                           &Fsbgp4PathAttrAddrPrefix,
                                                           &Fsbgp4PathAttrAddrPrefix,
                                                           *pi4NextFsbgp4PathAttrAddrPrefixLen,
                                                           pi4NextFsbgp4PathAttrAddrPrefixLen,
                                                           i4PeerAfi,
                                                           &i4PeerAfi,
                                                           &Fsbgp4PathAttrPeer,
                                                           &Fsbgp4PathAttrPeer)
               == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4RfdRtFom
Input       :  The Indices
Fsbgp4PathAttrAddrPrefix
Fsbgp4PathAttrAddrPrefixLen
Fsbgp4PathAttrPeer
Fsbgp4RtDampHistInstance

The Object 
retValFsbgp4RfdRtFom
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RfdRtFom (UINT4 u4Fsbgp4PathAttrAddrPrefix,
                      INT4 i4Fsbgp4PathAttrAddrPrefixLen,
                      UINT4 u4Fsbgp4PathAttrPeer,
                      INT4 i4Fsbgp4RtDampHistInstance,
                      INT4 *pi4RetValFsbgp4RfdRtFom)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PathAttrAddrPrefix;
    tSNMP_OCTET_STRING_TYPE Fsbgp4PathAttrPeer;
    UINT4               u4PathAttrAddr = 0;
    UINT4               u4PathAttrPeer = 0;

    UNUSED_PARAM (i4Fsbgp4RtDampHistInstance);
    Fsbgp4PathAttrPeer.pu1_OctetList = (UINT1 *) (VOID *) &u4PathAttrPeer;
    Fsbgp4PathAttrAddrPrefix.pu1_OctetList = (UINT1 *) (VOID *) &u4PathAttrAddr;

    PTR_ASSIGN4 (Fsbgp4PathAttrPeer.pu1_OctetList, u4Fsbgp4PathAttrPeer);
    PTR_ASSIGN4 (Fsbgp4PathAttrAddrPrefix.pu1_OctetList,
                 u4Fsbgp4PathAttrAddrPrefix);
    if (nmhGetFsbgp4mpeRfdRtFom (BGP4_INET_AFI_IPV4,
                                 BGP4_INET_SAFI_UNICAST,
                                 &Fsbgp4PathAttrAddrPrefix,
                                 i4Fsbgp4PathAttrAddrPrefixLen,
                                 BGP4_INET_AFI_IPV4,
                                 &Fsbgp4PathAttrPeer,
                                 pi4RetValFsbgp4RfdRtFom) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4RfdRtLastUpdtTime
Input       :  The Indices
Fsbgp4PathAttrAddrPrefix
Fsbgp4PathAttrAddrPrefixLen
Fsbgp4PathAttrPeer
Fsbgp4RtDampHistInstance

The Object 
retValFsbgp4RfdRtLastUpdtTime
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RfdRtLastUpdtTime (UINT4 u4Fsbgp4PathAttrAddrPrefix,
                               INT4 i4Fsbgp4PathAttrAddrPrefixLen,
                               UINT4 u4Fsbgp4PathAttrPeer,
                               INT4 i4Fsbgp4RtDampHistInstance,
                               INT4 *pi4RetValFsbgp4RfdRtLastUpdtTime)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PathAttrAddrPrefix;
    tSNMP_OCTET_STRING_TYPE Fsbgp4PathAttrPeer;
    UINT4               u4PathAttrAddr = 0;
    UINT4               u4PathAttrPeer = 0;

    UNUSED_PARAM (i4Fsbgp4RtDampHistInstance);
    Fsbgp4PathAttrPeer.pu1_OctetList = (UINT1 *) (VOID *) &u4PathAttrPeer;
    Fsbgp4PathAttrAddrPrefix.pu1_OctetList = (UINT1 *) (VOID *) &u4PathAttrAddr;

    PTR_ASSIGN4 (Fsbgp4PathAttrPeer.pu1_OctetList, u4Fsbgp4PathAttrPeer);
    PTR_ASSIGN4 (Fsbgp4PathAttrAddrPrefix.pu1_OctetList,
                 u4Fsbgp4PathAttrAddrPrefix);
    if (nmhGetFsbgp4mpeRfdRtLastUpdtTime (BGP4_INET_AFI_IPV4,
                                          BGP4_INET_SAFI_UNICAST,
                                          &Fsbgp4PathAttrAddrPrefix,
                                          i4Fsbgp4PathAttrAddrPrefixLen,
                                          BGP4_INET_AFI_IPV4,
                                          &Fsbgp4PathAttrPeer,
                                          pi4RetValFsbgp4RfdRtLastUpdtTime)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4RfdRtState
Input       :  The Indices
Fsbgp4PathAttrAddrPrefix
Fsbgp4PathAttrAddrPrefixLen
Fsbgp4PathAttrPeer
Fsbgp4RtDampHistInstance

The Object 
retValFsbgp4RfdRtState
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RfdRtState (UINT4 u4Fsbgp4PathAttrAddrPrefix,
                        INT4 i4Fsbgp4PathAttrAddrPrefixLen,
                        UINT4 u4Fsbgp4PathAttrPeer,
                        INT4 i4Fsbgp4RtDampHistInstance,
                        INT4 *pi4RetValFsbgp4RfdRtState)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PathAttrAddrPrefix;
    tSNMP_OCTET_STRING_TYPE Fsbgp4PathAttrPeer;
    UINT4               u4PathAttrAddr = 0;
    UINT4               u4PathAttrPeer = 0;

    UNUSED_PARAM (i4Fsbgp4RtDampHistInstance);
    Fsbgp4PathAttrPeer.pu1_OctetList = (UINT1 *) (VOID *) &u4PathAttrPeer;
    Fsbgp4PathAttrAddrPrefix.pu1_OctetList = (UINT1 *) (VOID *) &u4PathAttrAddr;

    PTR_ASSIGN4 (Fsbgp4PathAttrPeer.pu1_OctetList, u4Fsbgp4PathAttrPeer);
    PTR_ASSIGN4 (Fsbgp4PathAttrAddrPrefix.pu1_OctetList,
                 u4Fsbgp4PathAttrAddrPrefix);
    if (nmhGetFsbgp4mpeRfdRtState (BGP4_INET_AFI_IPV4,
                                   BGP4_INET_SAFI_UNICAST,
                                   &Fsbgp4PathAttrAddrPrefix,
                                   i4Fsbgp4PathAttrAddrPrefixLen,
                                   BGP4_INET_AFI_IPV4,
                                   &Fsbgp4PathAttrPeer,
                                   pi4RetValFsbgp4RfdRtState) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4RfdRtStatus
Input       :  The Indices
Fsbgp4PathAttrAddrPrefix
Fsbgp4PathAttrAddrPrefixLen
Fsbgp4PathAttrPeer
Fsbgp4RtDampHistInstance

The Object 
retValFsbgp4RfdRtStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RfdRtStatus (UINT4 u4Fsbgp4PathAttrAddrPrefix,
                         INT4 i4Fsbgp4PathAttrAddrPrefixLen,
                         UINT4 u4Fsbgp4PathAttrPeer,
                         INT4 i4Fsbgp4RtDampHistInstance,
                         INT4 *pi4RetValFsbgp4RfdRtStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PathAttrAddrPrefix;
    tSNMP_OCTET_STRING_TYPE Fsbgp4PathAttrPeer;
    UINT4               u4PathAttrAddr = 0;
    UINT4               u4PathAttrPeer = 0;
    UNUSED_PARAM (i4Fsbgp4RtDampHistInstance);

    Fsbgp4PathAttrPeer.pu1_OctetList = (UINT1 *) (VOID *) &u4PathAttrPeer;
    Fsbgp4PathAttrAddrPrefix.pu1_OctetList = (UINT1 *) (VOID *) &u4PathAttrAddr;

    PTR_ASSIGN4 (Fsbgp4PathAttrPeer.pu1_OctetList, u4Fsbgp4PathAttrPeer);
    PTR_ASSIGN4 (Fsbgp4PathAttrAddrPrefix.pu1_OctetList,
                 u4Fsbgp4PathAttrAddrPrefix);

    if (nmhGetFsbgp4mpeRfdRtStatus (BGP4_INET_AFI_IPV4,
                                    BGP4_INET_SAFI_UNICAST,
                                    &Fsbgp4PathAttrAddrPrefix,
                                    i4Fsbgp4PathAttrAddrPrefixLen,
                                    BGP4_INET_AFI_IPV4,
                                    &Fsbgp4PathAttrPeer,
                                    pi4RetValFsbgp4RfdRtStatus) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Fsbgp4RfdPeerDampHistTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4RfdPeerDampHistTable
Input       :  The Indices
Fsbgp4PeerRemoteIpAddr
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4RfdPeerDampHistTable (UINT4
                                                    u4Fsbgp4PeerRemoteIpAddr)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4RemoteAddr = 0;
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr;
    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerRemoteIpAddr);
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);

    if (nmhValidateIndexInstanceFsbgp4MpeRfdPeerDampHistTable
        (BGP4_INET_AFI_IPV4, &Fsbgp4PeerExtPeerRemoteAddr) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4RfdPeerDampHistTable
Input       :  The Indices
Fsbgp4PeerRemoteIpAddr
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4RfdPeerDampHistTable (UINT4 *pu4Fsbgp4PeerRemoteIpAddr)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    INT4                i4PeerAfi;
    UINT4               au4RemoteAddr[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4RemoteAddr, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) au4RemoteAddr;

    if (nmhGetFirstIndexFsbgp4MpeRfdPeerDampHistTable (&i4PeerAfi,
                                                       &Fsbgp4PeerExtPeerRemoteAddr)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4PeerAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4Fsbgp4PeerRemoteIpAddr,
                            Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList);

                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeRfdPeerDampHistTable
               (i4PeerAfi, &i4PeerAfi, &Fsbgp4PeerExtPeerRemoteAddr,
                &Fsbgp4PeerExtPeerRemoteAddr) == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4RfdPeerDampHistTable
Input       :  The Indices
Fsbgp4PeerRemoteIpAddr
nextFsbgp4PeerRemoteIpAddr
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4RfdPeerDampHistTable (UINT4 u4Fsbgp4PeerRemoteIpAddr,
                                           UINT4 *pu4NextFsbgp4PeerRemoteIpAddr)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    INT4                i4Afi;
    UINT4               au4RemoteAddr[BGP4_MAX_INET_ADDRESS_LEN];
    MEMSET (au4RemoteAddr, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) au4RemoteAddr;

    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerRemoteIpAddr);
    if (nmhGetNextIndexFsbgp4MpeRfdPeerDampHistTable (BGP4_INET_AFI_IPV4,
                                                      &i4Afi,
                                                      &Fsbgp4PeerExtPeerRemoteAddr,
                                                      &Fsbgp4PeerExtPeerRemoteAddr)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4Afi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4NextFsbgp4PeerRemoteIpAddr,
                            Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList);

                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpePeerExtTable (i4Afi, &i4Afi,
                                                     &Fsbgp4PeerExtPeerRemoteAddr,
                                                     &Fsbgp4PeerExtPeerRemoteAddr)
               == SNMP_SUCCESS);

    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4RfdPeerFom
Input       :  The Indices
Fsbgp4PeerRemoteIpAddr

The Object 
retValFsbgp4RfdPeerFom
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RfdPeerFom (UINT4 u4Fsbgp4PeerRemoteIpAddr,
                        INT4 *pi4RetValFsbgp4RfdPeerFom)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4RemoteAddr = 0;
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr;
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);
    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerRemoteIpAddr);
    if (nmhGetFsbgp4mpeRfdPeerFom (BGP4_INET_AFI_IPV4,
                                   &Fsbgp4PeerExtPeerRemoteAddr,
                                   pi4RetValFsbgp4RfdPeerFom) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4RfdPeerLastUpdtTime
Input       :  The Indices
Fsbgp4PeerRemoteIpAddr

The Object 
retValFsbgp4RfdPeerLastUpdtTime
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RfdPeerLastUpdtTime (UINT4 u4Fsbgp4PeerRemoteIpAddr,
                                 INT4 *pi4RetValFsbgp4RfdPeerLastUpdtTime)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4RemoteAddr = 0;
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr;
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);
    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerRemoteIpAddr);

    if (nmhGetFsbgp4mpeRfdPeerLastUpdtTime (BGP4_INET_AFI_IPV4,
                                            &Fsbgp4PeerExtPeerRemoteAddr,
                                            pi4RetValFsbgp4RfdPeerLastUpdtTime)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4RfdPeerState
Input       :  The Indices
Fsbgp4PeerRemoteIpAddr

The Object 
retValFsbgp4RfdPeerState
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RfdPeerState (UINT4 u4Fsbgp4PeerRemoteIpAddr,
                          INT4 *pi4RetValFsbgp4RfdPeerState)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerExtPeerRemoteAddr;
    UINT4               u4RemoteAddr = 0;
    Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4RemoteAddr;
    Fsbgp4PeerExtPeerRemoteAddr.i4_Length = sizeof (UINT4);
    PTR_ASSIGN4 (Fsbgp4PeerExtPeerRemoteAddr.pu1_OctetList,
                 u4Fsbgp4PeerRemoteIpAddr);

    if (nmhGetFsbgp4mpeRfdPeerState (BGP4_INET_AFI_IPV4,
                                     &Fsbgp4PeerExtPeerRemoteAddr,
                                     pi4RetValFsbgp4RfdPeerState)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4RfdPeerStatus
Input       :  The Indices
Fsbgp4PeerRemoteIpAddr

The Object 
retValFsbgp4RfdPeerStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RfdPeerStatus (UINT4 u4Fsbgp4PeerRemoteIpAddr,
                           INT4 *pi4RetValFsbgp4RfdPeerStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerRemoteIpAddr;
    UINT4               u4RemoteIPAddr = 0;
    Fsbgp4PeerRemoteIpAddr.pu1_OctetList = (UINT1 *) (VOID *) &u4RemoteIPAddr;

    PTR_ASSIGN4 (Fsbgp4PeerRemoteIpAddr.pu1_OctetList,
                 u4Fsbgp4PeerRemoteIpAddr);
    Fsbgp4PeerRemoteIpAddr.i4_Length = sizeof (UINT4);
    if (nmhGetFsbgp4mpeRfdPeerStatus (BGP4_INET_AFI_IPV4,
                                      &Fsbgp4PeerRemoteIpAddr,
                                      pi4RetValFsbgp4RfdPeerStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Fsbgp4RfdRtsReuseListTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4RfdRtsReuseListTable
Input       :  The Indices
Fsbgp4RtIPPrefix
Fsbgp4RtIPPrefixLen
Fsbgp4PeerRemAddress
Fsbgp4RfdRtReuseListInstance
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4RfdRtsReuseListTable (UINT4 u4Fsbgp4RtIPPrefix,
                                                    INT4 i4Fsbgp4RtIPPrefixLen,
                                                    UINT4
                                                    u4Fsbgp4PeerRemAddress,
                                                    INT4
                                                    i4Fsbgp4RfdRtReuseListInstance)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4RtIPPrefix;
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerRemAddress;
    UINT4               u4RtIPPrefix = 0;
    UINT4               u4PeerRemAddr = 0;

    UNUSED_PARAM (i4Fsbgp4RfdRtReuseListInstance);
    Fsbgp4RtIPPrefix.pu1_OctetList = (UINT1 *) (VOID *) &u4RtIPPrefix;
    Fsbgp4PeerRemAddress.pu1_OctetList = (UINT1 *) (VOID *) &u4PeerRemAddr;

    PTR_ASSIGN4 (Fsbgp4RtIPPrefix.pu1_OctetList, u4Fsbgp4RtIPPrefix);
    PTR_ASSIGN4 (Fsbgp4PeerRemAddress.pu1_OctetList, u4Fsbgp4PeerRemAddress);
    if (nmhValidateIndexInstanceFsbgp4MpeRfdRtsReuseListTable
        (BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST, &Fsbgp4RtIPPrefix,
         i4Fsbgp4RtIPPrefixLen, BGP4_INET_AFI_IPV4,
         &Fsbgp4PeerRemAddress) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4RfdRtsReuseListTable
Input       :  The Indices
Fsbgp4RtIPPrefix
Fsbgp4RtIPPrefixLen
Fsbgp4PeerRemAddress
Fsbgp4RfdRtReuseListInstance
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4RfdRtsReuseListTable (UINT4 *pu4Fsbgp4RtIPPrefix,
                                            INT4 *pi4Fsbgp4RtIPPrefixLen,
                                            UINT4 *pu4Fsbgp4PeerRemAddress,
                                            INT4
                                            *pi4Fsbgp4RfdRtReuseListInstance)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4RtIPPrefix;
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerRemAddress;
    INT4                i4RouteAfi;
    INT4                i4RouteSafi;
    INT4                i4PeerAfi;
    UINT4               u4RtIPPrefix = 0;
    UINT4               u4PeerRemAddr = 0;

    UNUSED_PARAM (pi4Fsbgp4RfdRtReuseListInstance);
    Fsbgp4RtIPPrefix.pu1_OctetList = (UINT1 *) (VOID *) &u4RtIPPrefix;
    Fsbgp4PeerRemAddress.pu1_OctetList = (UINT1 *) (VOID *) &u4PeerRemAddr;

    if (nmhGetFirstIndexFsbgp4MpeRfdRtsReuseListTable (&i4RouteAfi,
                                                       &i4RouteSafi,
                                                       &Fsbgp4RtIPPrefix,
                                                       pi4Fsbgp4RtIPPrefixLen,
                                                       &i4PeerAfi,
                                                       &Fsbgp4PeerRemAddress)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4RouteAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4Fsbgp4RtIPPrefix,
                            Fsbgp4RtIPPrefix.pu1_OctetList);
                PTR_FETCH4 (*pu4Fsbgp4PeerRemAddress,
                            Fsbgp4PeerRemAddress.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeRfdRtsReuseListTable (i4RouteAfi,
                                                             &i4RouteAfi,
                                                             i4RouteSafi,
                                                             &i4RouteSafi,
                                                             &Fsbgp4PeerRemAddress,
                                                             &Fsbgp4PeerRemAddress,
                                                             *pi4Fsbgp4RtIPPrefixLen,
                                                             pi4Fsbgp4RtIPPrefixLen,
                                                             i4PeerAfi,
                                                             &i4PeerAfi,
                                                             &Fsbgp4RtIPPrefix,
                                                             &Fsbgp4RtIPPrefix)
               == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4RfdRtsReuseListTable
Input       :  The Indices
Fsbgp4RtIPPrefix
nextFsbgp4RtIPPrefix
Fsbgp4RtIPPrefixLen
nextFsbgp4RtIPPrefixLen
Fsbgp4PeerRemAddress
nextFsbgp4PeerRemAddress
Fsbgp4RfdRtReuseListInstance
nextFsbgp4RfdRtReuseListInstance
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4RfdRtsReuseListTable (UINT4 u4Fsbgp4RtIPPrefix,
                                           UINT4 *pu4NextFsbgp4RtIPPrefix,
                                           INT4 i4Fsbgp4RtIPPrefixLen,
                                           INT4 *pi4NextFsbgp4RtIPPrefixLen,
                                           UINT4 u4Fsbgp4PeerRemAddress,
                                           UINT4 *pu4NextFsbgp4PeerRemAddress,
                                           INT4 i4Fsbgp4RfdRtReuseListInstance,
                                           INT4
                                           *pi4NextFsbgp4RfdRtReuseListInstance)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4RtIPPrefix;
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerRemAddress;
    INT4                i4RouteAfi;
    INT4                i4RouteSafi;
    INT4                i4PeerAfi;
    UINT4               au4RtIPPrefix[BGP4_MAX_INET_ADDRESS_LEN];
    UINT4               au4PeerRemAddr[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4RtIPPrefix, 0, BGP4_MAX_INET_ADDRESS_LEN);
    MEMSET (au4PeerRemAddr, 0, BGP4_MAX_INET_ADDRESS_LEN);

    UNUSED_PARAM (i4Fsbgp4RfdRtReuseListInstance);
    UNUSED_PARAM (pi4NextFsbgp4RfdRtReuseListInstance);
    Fsbgp4RtIPPrefix.pu1_OctetList = (UINT1 *) (VOID *) au4RtIPPrefix;
    Fsbgp4PeerRemAddress.pu1_OctetList = (UINT1 *) (VOID *) au4PeerRemAddr;

    PTR_ASSIGN4 (Fsbgp4RtIPPrefix.pu1_OctetList, u4Fsbgp4RtIPPrefix);
    PTR_ASSIGN4 (Fsbgp4PeerRemAddress.pu1_OctetList, u4Fsbgp4PeerRemAddress);
    if (nmhGetNextIndexFsbgp4MpeRfdRtsReuseListTable (BGP4_INET_AFI_IPV4,
                                                      &i4RouteAfi,
                                                      BGP4_INET_SAFI_UNICAST,
                                                      &i4RouteSafi,
                                                      &Fsbgp4PeerRemAddress,
                                                      &Fsbgp4PeerRemAddress,
                                                      i4Fsbgp4RtIPPrefixLen,
                                                      pi4NextFsbgp4RtIPPrefixLen,
                                                      BGP4_INET_AFI_IPV4,
                                                      &i4PeerAfi,
                                                      &Fsbgp4RtIPPrefix,
                                                      &Fsbgp4RtIPPrefix)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4RouteAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4NextFsbgp4RtIPPrefix,
                            Fsbgp4RtIPPrefix.pu1_OctetList);
                PTR_FETCH4 (*pu4NextFsbgp4PeerRemAddress,
                            Fsbgp4PeerRemAddress.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeRfdRtsReuseListTable (i4RouteAfi,
                                                             &i4RouteAfi,
                                                             i4RouteSafi,
                                                             &i4RouteSafi,
                                                             &Fsbgp4PeerRemAddress,
                                                             &Fsbgp4PeerRemAddress,
                                                             *pi4NextFsbgp4RtIPPrefixLen,
                                                             pi4NextFsbgp4RtIPPrefixLen,
                                                             i4PeerAfi,
                                                             &i4PeerAfi,
                                                             &Fsbgp4RtIPPrefix,
                                                             &Fsbgp4RtIPPrefix)
               == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4RfdRtReuseListRtFom
Input       :  The Indices
Fsbgp4RtIPPrefix
Fsbgp4RtIPPrefixLen
Fsbgp4PeerRemAddress
Fsbgp4RfdRtReuseListInstance

The Object 
retValFsbgp4RfdRtReuseListRtFom
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RfdRtReuseListRtFom (UINT4 u4Fsbgp4RtIPPrefix,
                                 INT4 i4Fsbgp4RtIPPrefixLen,
                                 UINT4 u4Fsbgp4PeerRemAddress,
                                 INT4 i4Fsbgp4RfdRtReuseListInstance,
                                 INT4 *pi4RetValFsbgp4RfdRtReuseListRtFom)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4RtIPPrefix;
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerRemAddress;
    UINT4               u4RtIPPrefix = 0;
    UINT4               u4PeerRemAddr = 0;

    UNUSED_PARAM (i4Fsbgp4RfdRtReuseListInstance);
    Fsbgp4RtIPPrefix.pu1_OctetList = (UINT1 *) (VOID *) &u4RtIPPrefix;
    Fsbgp4PeerRemAddress.pu1_OctetList = (UINT1 *) (VOID *) &u4PeerRemAddr;
    PTR_ASSIGN4 (Fsbgp4RtIPPrefix.pu1_OctetList, u4Fsbgp4RtIPPrefix);
    PTR_ASSIGN4 (Fsbgp4PeerRemAddress.pu1_OctetList, u4Fsbgp4PeerRemAddress);

    if (nmhGetFsbgp4mpeRfdRtReuseListRtFom (BGP4_INET_AFI_IPV4,
                                            BGP4_INET_SAFI_UNICAST,
                                            &Fsbgp4RtIPPrefix,
                                            i4Fsbgp4RtIPPrefixLen,
                                            BGP4_INET_AFI_IPV4,
                                            &Fsbgp4PeerRemAddress,
                                            pi4RetValFsbgp4RfdRtReuseListRtFom)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4RfdRtReuseListRtLastUpdtTime
Input       :  The Indices
Fsbgp4RtIPPrefix
Fsbgp4RtIPPrefixLen
Fsbgp4PeerRemAddress
Fsbgp4RfdRtReuseListInstance

The Object 
retValFsbgp4RfdRtReuseListRtLastUpdtTime
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RfdRtReuseListRtLastUpdtTime (UINT4 u4Fsbgp4RtIPPrefix,
                                          INT4 i4Fsbgp4RtIPPrefixLen,
                                          UINT4 u4Fsbgp4PeerRemAddress,
                                          INT4 i4Fsbgp4RfdRtReuseListInstance,
                                          INT4
                                          *pi4RetValFsbgp4RfdRtReuseListRtLastUpdtTime)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4RtIPPrefix;
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerRemAddress;
    UINT4               u4RtIPPrefix = 0;
    UINT4               u4PeerRemAddr = 0;

    UNUSED_PARAM (i4Fsbgp4RfdRtReuseListInstance);
    Fsbgp4RtIPPrefix.pu1_OctetList = (UINT1 *) (VOID *) &u4RtIPPrefix;
    Fsbgp4PeerRemAddress.pu1_OctetList = (UINT1 *) (VOID *) &u4PeerRemAddr;
    PTR_ASSIGN4 (Fsbgp4RtIPPrefix.pu1_OctetList, u4Fsbgp4RtIPPrefix);
    PTR_ASSIGN4 (Fsbgp4PeerRemAddress.pu1_OctetList, u4Fsbgp4PeerRemAddress);

    if (nmhGetFsbgp4mpeRfdRtReuseListRtLastUpdtTime (BGP4_INET_AFI_IPV4,
                                                     BGP4_INET_SAFI_UNICAST,
                                                     &Fsbgp4RtIPPrefix,
                                                     i4Fsbgp4RtIPPrefixLen,
                                                     BGP4_INET_AFI_IPV4,
                                                     &Fsbgp4PeerRemAddress,
                                                     pi4RetValFsbgp4RfdRtReuseListRtLastUpdtTime)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetFsbgp4RfdRtReuseListRtState
Input       :  The Indices
Fsbgp4RtIPPrefix
Fsbgp4RtIPPrefixLen
Fsbgp4PeerRemAddress
Fsbgp4RfdRtReuseListInstance

The Object 
retValFsbgp4RfdRtReuseListRtState
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RfdRtReuseListRtState (UINT4 u4Fsbgp4RtIPPrefix,
                                   INT4 i4Fsbgp4RtIPPrefixLen,
                                   UINT4 u4Fsbgp4PeerRemAddress,
                                   INT4 i4Fsbgp4RfdRtReuseListInstance,
                                   INT4 *pi4RetValFsbgp4RfdRtReuseListRtState)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4RtIPPrefix;
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerRemAddress;
    UINT4               u4RtIPPrefix = 0;
    UINT4               u4PeerRemAddr = 0;

    UNUSED_PARAM (i4Fsbgp4RfdRtReuseListInstance);
    Fsbgp4RtIPPrefix.pu1_OctetList = (UINT1 *) (VOID *) &u4RtIPPrefix;
    Fsbgp4PeerRemAddress.pu1_OctetList = (UINT1 *) (VOID *) &u4PeerRemAddr;
    PTR_ASSIGN4 (Fsbgp4RtIPPrefix.pu1_OctetList, u4Fsbgp4RtIPPrefix);
    PTR_ASSIGN4 (Fsbgp4PeerRemAddress.pu1_OctetList, u4Fsbgp4PeerRemAddress);

    if (nmhGetFsbgp4mpeRfdRtReuseListRtState (BGP4_INET_AFI_IPV4,
                                              BGP4_INET_SAFI_UNICAST,
                                              &Fsbgp4RtIPPrefix,
                                              i4Fsbgp4RtIPPrefixLen,
                                              BGP4_INET_AFI_IPV4,
                                              &Fsbgp4PeerRemAddress,
                                              pi4RetValFsbgp4RfdRtReuseListRtState)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4RfdRtReuseListRtStatus
Input       :  The Indices
Fsbgp4RtIPPrefix
Fsbgp4RtIPPrefixLen
Fsbgp4PeerRemAddress
Fsbgp4RfdRtReuseListInstance

The Object 
retValFsbgp4RfdRtReuseListRtStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RfdRtReuseListRtStatus (UINT4 u4Fsbgp4RtIPPrefix,
                                    INT4 i4Fsbgp4RtIPPrefixLen,
                                    UINT4 u4Fsbgp4PeerRemAddress,
                                    INT4 i4Fsbgp4RfdRtReuseListInstance,
                                    INT4 *pi4RetValFsbgp4RfdRtReuseListRtStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4RtIPPrefix;
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerRemAddress;
    UINT4               u4RtIPPrefix = 0;
    UINT4               u4PeerRemAddr = 0;

    UNUSED_PARAM (i4Fsbgp4RfdRtReuseListInstance);
    Fsbgp4RtIPPrefix.pu1_OctetList = (UINT1 *) (VOID *) &u4RtIPPrefix;
    Fsbgp4PeerRemAddress.pu1_OctetList = (UINT1 *) (VOID *) &u4PeerRemAddr;

    PTR_ASSIGN4 (Fsbgp4RtIPPrefix.pu1_OctetList, u4Fsbgp4RtIPPrefix);
    PTR_ASSIGN4 (Fsbgp4PeerRemAddress.pu1_OctetList, u4Fsbgp4PeerRemAddress);

    if (nmhGetFsbgp4mpeRfdRtReuseListRtStatus (BGP4_INET_AFI_IPV4,
                                               BGP4_INET_SAFI_UNICAST,
                                               &Fsbgp4RtIPPrefix,
                                               i4Fsbgp4RtIPPrefixLen,
                                               BGP4_INET_AFI_IPV4,
                                               &Fsbgp4PeerRemAddress,
                                               pi4RetValFsbgp4RfdRtReuseListRtStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Fsbgp4RfdPeerReuseListTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4RfdPeerReuseListTable
Input       :  The Indices
Fsbgp4RfdPeerRemIpAddr
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4RfdPeerReuseListTable (UINT4
                                                     u4Fsbgp4RfdPeerRemIpAddr)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4RfdPeerRemIpAddr;
    UINT4               u4PeerRemIpAddr = 0;
    Fsbgp4RfdPeerRemIpAddr.pu1_OctetList = (UINT1 *) (VOID *) &u4PeerRemIpAddr;
    PTR_ASSIGN4 (Fsbgp4RfdPeerRemIpAddr.pu1_OctetList,
                 u4Fsbgp4RfdPeerRemIpAddr);
    Fsbgp4RfdPeerRemIpAddr.i4_Length = sizeof (UINT4);

    if (nmhValidateIndexInstanceFsbgp4MpeRfdPeerReuseListTable
        (BGP4_INET_AFI_IPV4, &Fsbgp4RfdPeerRemIpAddr) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4RfdPeerReuseListTable
Input       :  The Indices
Fsbgp4RfdPeerRemIpAddr
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4RfdPeerReuseListTable (UINT4 *pu4Fsbgp4RfdPeerRemIpAddr)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4RfdPeerRemIpAddr;
    INT4                i4PeerAfi;
    UINT4               au4PeerRemIpAddr[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4PeerRemIpAddr, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4RfdPeerRemIpAddr.pu1_OctetList = (UINT1 *) (VOID *) au4PeerRemIpAddr;

    if (nmhGetFirstIndexFsbgp4MpeRfdPeerReuseListTable (&i4PeerAfi,
                                                        &Fsbgp4RfdPeerRemIpAddr)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4PeerAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4Fsbgp4RfdPeerRemIpAddr,
                            Fsbgp4RfdPeerRemIpAddr.pu1_OctetList);

                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeRfdPeerReuseListTable
               (i4PeerAfi, &i4PeerAfi, &Fsbgp4RfdPeerRemIpAddr,
                &Fsbgp4RfdPeerRemIpAddr) == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4RfdPeerReuseListTable
Input       :  The Indices
Fsbgp4RfdPeerRemIpAddr
nextFsbgp4RfdPeerRemIpAddr
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4RfdPeerReuseListTable (UINT4 u4Fsbgp4RfdPeerRemIpAddr,
                                            UINT4
                                            *pu4NextFsbgp4RfdPeerRemIpAddr)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4RfdPeerRemIpAddr;
    INT4                i4PeerAfi = BGP4_INET_AFI_IPV4;
    UINT4               au4PeerRemIpAddr[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4PeerRemIpAddr, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4RfdPeerRemIpAddr.pu1_OctetList = (UINT1 *) (VOID *) au4PeerRemIpAddr;

    PTR_ASSIGN4 (Fsbgp4RfdPeerRemIpAddr.pu1_OctetList,
                 u4Fsbgp4RfdPeerRemIpAddr);

    if (nmhGetNextIndexFsbgp4MpeRfdPeerReuseListTable (BGP4_INET_AFI_IPV4,
                                                       &i4PeerAfi,
                                                       &Fsbgp4RfdPeerRemIpAddr,
                                                       &Fsbgp4RfdPeerRemIpAddr)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4PeerAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4NextFsbgp4RfdPeerRemIpAddr,
                            Fsbgp4RfdPeerRemIpAddr.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeRfdPeerReuseListTable
               (i4PeerAfi, &i4PeerAfi, &Fsbgp4RfdPeerRemIpAddr,
                &Fsbgp4RfdPeerRemIpAddr) == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4RfdPeerReuseListPeerFom
Input       :  The Indices
Fsbgp4RfdPeerRemIpAddr

The Object 
retValFsbgp4RfdPeerReuseListPeerFom
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RfdPeerReuseListPeerFom (UINT4 u4Fsbgp4RfdPeerRemIpAddr,
                                     INT4
                                     *pi4RetValFsbgp4RfdPeerReuseListPeerFom)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4RfdPeerRemIpAddr;
    UINT4               u4PeerIPAddr;

    Fsbgp4RfdPeerRemIpAddr.pu1_OctetList = (UINT1 *) (VOID *) &u4PeerIPAddr;
    PTR_ASSIGN4 (Fsbgp4RfdPeerRemIpAddr.pu1_OctetList,
                 u4Fsbgp4RfdPeerRemIpAddr);
    Fsbgp4RfdPeerRemIpAddr.i4_Length = sizeof (UINT4);

    if (nmhGetFsbgp4mpeRfdPeerReuseListPeerFom (BGP4_INET_AFI_IPV4,
                                                &Fsbgp4RfdPeerRemIpAddr,
                                                pi4RetValFsbgp4RfdPeerReuseListPeerFom)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4RfdPeerReuseListLastUpdtTime
Input       :  The Indices
Fsbgp4RfdPeerRemIpAddr

The Object 
retValFsbgp4RfdPeerReuseListLastUpdtTime
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RfdPeerReuseListLastUpdtTime (UINT4 u4Fsbgp4RfdPeerRemIpAddr,
                                          INT4
                                          *pi4RetValFsbgp4RfdPeerReuseListLastUpdtTime)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4RfdPeerRemIpAddr;
    UINT4               u4PeerRemIPAddr;

    Fsbgp4RfdPeerRemIpAddr.pu1_OctetList = (UINT1 *) (VOID *) &u4PeerRemIPAddr;
    PTR_ASSIGN4 (Fsbgp4RfdPeerRemIpAddr.pu1_OctetList,
                 u4Fsbgp4RfdPeerRemIpAddr);
    Fsbgp4RfdPeerRemIpAddr.i4_Length = sizeof (UINT4);

    if (nmhGetFsbgp4mpeRfdPeerReuseListLastUpdtTime (BGP4_INET_AFI_IPV4,
                                                     &Fsbgp4RfdPeerRemIpAddr,
                                                     pi4RetValFsbgp4RfdPeerReuseListLastUpdtTime)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4RfdPeerReuseListPeerState
Input       :  The Indices
Fsbgp4RfdPeerRemIpAddr

The Object 
retValFsbgp4RfdPeerReuseListPeerState
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RfdPeerReuseListPeerState (UINT4 u4Fsbgp4RfdPeerRemIpAddr,
                                       INT4
                                       *pi4RetValFsbgp4RfdPeerReuseListPeerState)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4RfdPeerRemIpAddr;
    UINT4               u4PeerRemIPAddr;

    Fsbgp4RfdPeerRemIpAddr.pu1_OctetList = (UINT1 *) (VOID *) &u4PeerRemIPAddr;
    PTR_ASSIGN4 (Fsbgp4RfdPeerRemIpAddr.pu1_OctetList,
                 u4Fsbgp4RfdPeerRemIpAddr);
    Fsbgp4RfdPeerRemIpAddr.i4_Length = sizeof (UINT4);

    if (nmhGetFsbgp4mpeRfdPeerReuseListPeerState (BGP4_INET_AFI_IPV4,
                                                  &Fsbgp4RfdPeerRemIpAddr,
                                                  pi4RetValFsbgp4RfdPeerReuseListPeerState)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4RfdPeerReuseListPeerStatus
Input       :  The Indices
Fsbgp4RfdPeerRemIpAddr

The Object 
retValFsbgp4RfdPeerReuseListPeerStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RfdPeerReuseListPeerStatus (UINT4 u4Fsbgp4RfdPeerRemIpAddr,
                                        INT4
                                        *pi4RetValFsbgp4RfdPeerReuseListPeerStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4RfdPeerRemIpAddr;
    UINT4               u4PeerRemIPAddr;

    Fsbgp4RfdPeerRemIpAddr.pu1_OctetList = (UINT1 *) (VOID *) &u4PeerRemIPAddr;
    PTR_ASSIGN4 (Fsbgp4RfdPeerRemIpAddr.pu1_OctetList,
                 u4Fsbgp4RfdPeerRemIpAddr);
    Fsbgp4RfdPeerRemIpAddr.i4_Length = sizeof (UINT4);

    if (nmhGetFsbgp4mpeRfdPeerReuseListPeerStatus (BGP4_INET_AFI_IPV4,
                                                   &Fsbgp4RfdPeerRemIpAddr,
                                                   pi4RetValFsbgp4RfdPeerReuseListPeerStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Fsbgp4CommRouteAddCommTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4CommRouteAddCommTable
Input       :  The Indices
Fsbgp4AddCommIpNetwork
Fsbgp4AddCommIpPrefixLen
Fsbgp4AddCommVal
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4CommRouteAddCommTable (UINT4
                                                     u4Fsbgp4AddCommIpNetwork,
                                                     INT4
                                                     i4Fsbgp4AddCommIpPrefixLen,
                                                     UINT4 u4Fsbgp4AddCommVal)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4AddCommIpNetwork;
    UINT4               u4CommIpNet = 0;

    Fsbgp4AddCommIpNetwork.pu1_OctetList = (UINT1 *) (VOID *) &u4CommIpNet;
    PTR_ASSIGN4 (Fsbgp4AddCommIpNetwork.pu1_OctetList,
                 u4Fsbgp4AddCommIpNetwork);
    Fsbgp4AddCommIpNetwork.i4_Length = sizeof (UINT4);

    if (nmhValidateIndexInstanceFsbgp4MpeCommRouteAddCommTable
        (BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST, &Fsbgp4AddCommIpNetwork,
         i4Fsbgp4AddCommIpPrefixLen, u4Fsbgp4AddCommVal) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4CommRouteAddCommTable
Input       :  The Indices
Fsbgp4AddCommIpNetwork
Fsbgp4AddCommIpPrefixLen
Fsbgp4AddCommVal
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4CommRouteAddCommTable (UINT4 *pu4Fsbgp4AddCommIpNetwork,
                                             INT4 *pi4Fsbgp4AddCommIpPrefixLen,
                                             UINT4 *pu4Fsbgp4AddCommVal)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4AddCommIpNetwork;
    INT4                i4PeerAfi;
    INT4                i4PeerSafi;
    UINT4               au4CommIpNet[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4CommIpNet, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4AddCommIpNetwork.pu1_OctetList = (UINT1 *) (VOID *) au4CommIpNet;

    if (nmhGetFirstIndexFsbgp4MpeCommRouteAddCommTable (&i4PeerAfi,
                                                        &i4PeerSafi,
                                                        &Fsbgp4AddCommIpNetwork,
                                                        pi4Fsbgp4AddCommIpPrefixLen,
                                                        pu4Fsbgp4AddCommVal) ==
        SNMP_SUCCESS)
    {
        do
        {
            if (i4PeerAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4Fsbgp4AddCommIpNetwork,
                            Fsbgp4AddCommIpNetwork.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeCommRouteAddCommTable
               (i4PeerAfi, &i4PeerAfi, i4PeerSafi, &i4PeerSafi,
                &Fsbgp4AddCommIpNetwork, &Fsbgp4AddCommIpNetwork,
                *pi4Fsbgp4AddCommIpPrefixLen, pi4Fsbgp4AddCommIpPrefixLen,
                *pu4Fsbgp4AddCommVal, pu4Fsbgp4AddCommVal) == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4CommRouteAddCommTable
Input       :  The Indices
Fsbgp4AddCommIpNetwork
nextFsbgp4AddCommIpNetwork
Fsbgp4AddCommIpPrefixLen
nextFsbgp4AddCommIpPrefixLen
Fsbgp4AddCommVal
nextFsbgp4AddCommVal
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4CommRouteAddCommTable (UINT4 u4Fsbgp4AddCommIpNetwork,
                                            UINT4
                                            *pu4NextFsbgp4AddCommIpNetwork,
                                            INT4 i4Fsbgp4AddCommIpPrefixLen,
                                            INT4
                                            *pi4NextFsbgp4AddCommIpPrefixLen,
                                            UINT4 u4Fsbgp4AddCommVal,
                                            UINT4 *pu4NextFsbgp4AddCommVal)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4AddCommIpNetwork;
    INT4                i4PeerAfi = BGP4_INET_AFI_IPV4;
    INT4                i4PeerSafi = BGP4_INET_SAFI_UNICAST;
    UINT4               au4CommIpNet[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4CommIpNet, 0, BGP4_MAX_INET_ADDRESS_LEN);

    Fsbgp4AddCommIpNetwork.pu1_OctetList = (UINT1 *) (VOID *) au4CommIpNet;
    PTR_ASSIGN4 (Fsbgp4AddCommIpNetwork.pu1_OctetList,
                 u4Fsbgp4AddCommIpNetwork);

    if (nmhGetNextIndexFsbgp4MpeCommRouteAddCommTable (BGP4_INET_AFI_IPV4,
                                                       &i4PeerAfi,
                                                       BGP4_INET_SAFI_UNICAST,
                                                       &i4PeerSafi,
                                                       &Fsbgp4AddCommIpNetwork,
                                                       &Fsbgp4AddCommIpNetwork,
                                                       i4Fsbgp4AddCommIpPrefixLen,
                                                       pi4NextFsbgp4AddCommIpPrefixLen,
                                                       u4Fsbgp4AddCommVal,
                                                       pu4NextFsbgp4AddCommVal)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4PeerAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4NextFsbgp4AddCommIpNetwork,
                            Fsbgp4AddCommIpNetwork.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeCommRouteAddCommTable
               (i4PeerAfi, &i4PeerAfi, i4PeerSafi, &i4PeerSafi,
                &Fsbgp4AddCommIpNetwork, &Fsbgp4AddCommIpNetwork,
                *pi4NextFsbgp4AddCommIpPrefixLen,
                pi4NextFsbgp4AddCommIpPrefixLen, *pu4NextFsbgp4AddCommVal,
                pu4NextFsbgp4AddCommVal) == SNMP_SUCCESS);

    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4AddCommRowStatus
Input       :  The Indices
Fsbgp4AddCommIpNetwork
Fsbgp4AddCommIpPrefixLen
Fsbgp4AddCommVal

The Object 
retValFsbgp4AddCommRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4AddCommRowStatus (UINT4 u4Fsbgp4AddCommIpNetwork,
                              INT4 i4Fsbgp4AddCommIpPrefixLen,
                              UINT4 u4Fsbgp4AddCommVal,
                              INT4 *pi4RetValFsbgp4AddCommRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4AddCommIpNetwork;
    UINT4               u4CommIpNet = 0;

    Fsbgp4AddCommIpNetwork.pu1_OctetList = (UINT1 *) (VOID *) &u4CommIpNet;
    PTR_ASSIGN4 (Fsbgp4AddCommIpNetwork.pu1_OctetList,
                 u4Fsbgp4AddCommIpNetwork);
    Fsbgp4AddCommIpNetwork.i4_Length = sizeof (UINT4);

    if (nmhGetFsbgp4mpeAddCommRowStatus (BGP4_INET_AFI_IPV4,
                                         BGP4_INET_SAFI_UNICAST,
                                         &Fsbgp4AddCommIpNetwork,
                                         i4Fsbgp4AddCommIpPrefixLen,
                                         u4Fsbgp4AddCommVal,
                                         pi4RetValFsbgp4AddCommRowStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4AddCommRowStatus
Input       :  The Indices
Fsbgp4AddCommIpNetwork
Fsbgp4AddCommIpPrefixLen
Fsbgp4AddCommVal

The Object 
setValFsbgp4AddCommRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4AddCommRowStatus (UINT4 u4Fsbgp4AddCommIpNetwork,
                              INT4 i4Fsbgp4AddCommIpPrefixLen,
                              UINT4 u4Fsbgp4AddCommVal,
                              INT4 i4SetValFsbgp4AddCommRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4AddCommIpNetwork;
    UINT4               u4CommIpNet = 0;
    Fsbgp4AddCommIpNetwork.pu1_OctetList = (UINT1 *) (VOID *) &u4CommIpNet;
    PTR_ASSIGN4 (Fsbgp4AddCommIpNetwork.pu1_OctetList,
                 u4Fsbgp4AddCommIpNetwork);
    Fsbgp4AddCommIpNetwork.i4_Length = sizeof (UINT4);

    if (nmhSetFsbgp4mpeAddCommRowStatus (BGP4_INET_AFI_IPV4,
                                         BGP4_INET_SAFI_UNICAST,
                                         &Fsbgp4AddCommIpNetwork,
                                         i4Fsbgp4AddCommIpPrefixLen,
                                         u4Fsbgp4AddCommVal,
                                         i4SetValFsbgp4AddCommRowStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4AddCommRowStatus
Input       :  The Indices
Fsbgp4AddCommIpNetwork
Fsbgp4AddCommIpPrefixLen
Fsbgp4AddCommVal

The Object 
testValFsbgp4AddCommRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4AddCommRowStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4Fsbgp4AddCommIpNetwork,
                                 INT4 i4Fsbgp4AddCommIpPrefixLen,
                                 UINT4 u4Fsbgp4AddCommVal,
                                 INT4 i4TestValFsbgp4AddCommRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4AddCommIpNetwork;
    UINT4               u4CommIpNet = 0;
    Fsbgp4AddCommIpNetwork.pu1_OctetList = (UINT1 *) (VOID *) &u4CommIpNet;
    PTR_ASSIGN4 (Fsbgp4AddCommIpNetwork.pu1_OctetList,
                 u4Fsbgp4AddCommIpNetwork);
    Fsbgp4AddCommIpNetwork.i4_Length = sizeof (UINT4);

    if (nmhTestv2Fsbgp4mpeAddCommRowStatus (pu4ErrorCode,
                                            BGP4_INET_AFI_IPV4,
                                            BGP4_INET_SAFI_UNICAST,
                                            &Fsbgp4AddCommIpNetwork,
                                            i4Fsbgp4AddCommIpPrefixLen,
                                            u4Fsbgp4AddCommVal,
                                            i4TestValFsbgp4AddCommRowStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4CommRouteAddCommTable
Input       :  The Indices
Fsbgp4AddCommIpNetwork
Fsbgp4AddCommIpPrefixLen
Fsbgp4AddCommVal
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4CommRouteAddCommTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4CommRouteDeleteCommTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4CommRouteDeleteCommTable
Input       :  The Indices
Fsbgp4DeleteCommIpNetwork
Fsbgp4DeleteCommIpPrefixLen
Fsbgp4DeleteCommVal
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4CommRouteDeleteCommTable (UINT4
                                                        u4Fsbgp4DeleteCommIpNetwork,
                                                        INT4
                                                        i4Fsbgp4DeleteCommIpPrefixLen,
                                                        UINT4
                                                        u4Fsbgp4DeleteCommVal)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4DeleteCommIpNetwork;
    UINT4               u4CommIpNet = 0;
    Fsbgp4DeleteCommIpNetwork.pu1_OctetList = (UINT1 *) (VOID *) &u4CommIpNet;
    PTR_ASSIGN4 (Fsbgp4DeleteCommIpNetwork.pu1_OctetList,
                 u4Fsbgp4DeleteCommIpNetwork);
    Fsbgp4DeleteCommIpNetwork.i4_Length = sizeof (UINT4);

    if (nmhValidateIndexInstanceFsbgp4MpeCommRouteDeleteCommTable
        (BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST, &Fsbgp4DeleteCommIpNetwork,
         i4Fsbgp4DeleteCommIpPrefixLen, u4Fsbgp4DeleteCommVal) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4CommRouteDeleteCommTable
Input       :  The Indices
Fsbgp4DeleteCommIpNetwork
Fsbgp4DeleteCommIpPrefixLen
Fsbgp4DeleteCommVal
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4CommRouteDeleteCommTable (UINT4
                                                *pu4Fsbgp4DeleteCommIpNetwork,
                                                INT4
                                                *pi4Fsbgp4DeleteCommIpPrefixLen,
                                                UINT4 *pu4Fsbgp4DeleteCommVal)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4DeleteCommIpNetwork;
    INT4                i4PeerAfi;
    INT4                i4PeerSafi;
    UINT4               au4CommIpNet[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4CommIpNet, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4DeleteCommIpNetwork.pu1_OctetList = (UINT1 *) (VOID *) au4CommIpNet;

    if (nmhGetFirstIndexFsbgp4MpeCommRouteDeleteCommTable (&i4PeerAfi,
                                                           &i4PeerSafi,
                                                           &Fsbgp4DeleteCommIpNetwork,
                                                           pi4Fsbgp4DeleteCommIpPrefixLen,
                                                           pu4Fsbgp4DeleteCommVal)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4PeerAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4Fsbgp4DeleteCommIpNetwork,
                            Fsbgp4DeleteCommIpNetwork.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeCommRouteDeleteCommTable
               (i4PeerAfi, &i4PeerAfi, i4PeerSafi, &i4PeerSafi,
                &Fsbgp4DeleteCommIpNetwork, &Fsbgp4DeleteCommIpNetwork,
                *pi4Fsbgp4DeleteCommIpPrefixLen, pi4Fsbgp4DeleteCommIpPrefixLen,
                *pu4Fsbgp4DeleteCommVal,
                pu4Fsbgp4DeleteCommVal) == SNMP_SUCCESS);

    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4CommRouteDeleteCommTable
Input       :  The Indices
Fsbgp4DeleteCommIpNetwork
nextFsbgp4DeleteCommIpNetwork
Fsbgp4DeleteCommIpPrefixLen
nextFsbgp4DeleteCommIpPrefixLen
Fsbgp4DeleteCommVal
nextFsbgp4DeleteCommVal
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4CommRouteDeleteCommTable (UINT4
                                               u4Fsbgp4DeleteCommIpNetwork,
                                               UINT4
                                               *pu4NextFsbgp4DeleteCommIpNetwork,
                                               INT4
                                               i4Fsbgp4DeleteCommIpPrefixLen,
                                               INT4
                                               *pi4NextFsbgp4DeleteCommIpPrefixLen,
                                               UINT4 u4Fsbgp4DeleteCommVal,
                                               UINT4
                                               *pu4NextFsbgp4DeleteCommVal)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4DeleteCommIpNetwork;
    INT4                i4NextAfi = BGP4_INET_AFI_IPV4;
    INT4                i4NextSafi = BGP4_INET_SAFI_UNICAST;
    UINT4               au4CommIpNet[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4CommIpNet, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4DeleteCommIpNetwork.pu1_OctetList = (UINT1 *) (VOID *) au4CommIpNet;
    PTR_ASSIGN4 (Fsbgp4DeleteCommIpNetwork.pu1_OctetList,
                 u4Fsbgp4DeleteCommIpNetwork);

    if (nmhGetNextIndexFsbgp4MpeCommRouteDeleteCommTable (BGP4_INET_AFI_IPV4,
                                                          &i4NextAfi,
                                                          BGP4_INET_SAFI_UNICAST,
                                                          &i4NextSafi,
                                                          &Fsbgp4DeleteCommIpNetwork,
                                                          &Fsbgp4DeleteCommIpNetwork,
                                                          i4Fsbgp4DeleteCommIpPrefixLen,
                                                          pi4NextFsbgp4DeleteCommIpPrefixLen,
                                                          u4Fsbgp4DeleteCommVal,
                                                          pu4NextFsbgp4DeleteCommVal)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4NextAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4NextFsbgp4DeleteCommIpNetwork,
                            Fsbgp4DeleteCommIpNetwork.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeCommRouteDeleteCommTable
               (i4NextAfi, &i4NextAfi, i4NextSafi, &i4NextSafi,
                &Fsbgp4DeleteCommIpNetwork, &Fsbgp4DeleteCommIpNetwork,
                *pi4NextFsbgp4DeleteCommIpPrefixLen,
                pi4NextFsbgp4DeleteCommIpPrefixLen, *pu4NextFsbgp4DeleteCommVal,
                pu4NextFsbgp4DeleteCommVal) == SNMP_SUCCESS);

    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4DeleteCommRowStatus
Input       :  The Indices
Fsbgp4DeleteCommIpNetwork
Fsbgp4DeleteCommIpPrefixLen
Fsbgp4DeleteCommVal

The Object 
retValFsbgp4DeleteCommRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4DeleteCommRowStatus (UINT4 u4Fsbgp4DeleteCommIpNetwork,
                                 INT4 i4Fsbgp4DeleteCommIpPrefixLen,
                                 UINT4 u4Fsbgp4DeleteCommVal,
                                 INT4 *pi4RetValFsbgp4DeleteCommRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4DeleteCommIpNetwork;
    UINT4               u4CommIpNet = 0;
    Fsbgp4DeleteCommIpNetwork.pu1_OctetList = (UINT1 *) (VOID *) &u4CommIpNet;
    PTR_ASSIGN4 (Fsbgp4DeleteCommIpNetwork.pu1_OctetList,
                 u4Fsbgp4DeleteCommIpNetwork);
    Fsbgp4DeleteCommIpNetwork.i4_Length = sizeof (UINT4);

    if (nmhGetFsbgp4mpeDeleteCommRowStatus (BGP4_INET_AFI_IPV4,
                                            BGP4_INET_SAFI_UNICAST,
                                            &Fsbgp4DeleteCommIpNetwork,
                                            i4Fsbgp4DeleteCommIpPrefixLen,
                                            u4Fsbgp4DeleteCommVal,
                                            pi4RetValFsbgp4DeleteCommRowStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4DeleteCommRowStatus
Input       :  The Indices
Fsbgp4DeleteCommIpNetwork
Fsbgp4DeleteCommIpPrefixLen
Fsbgp4DeleteCommVal

The Object 
setValFsbgp4DeleteCommRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4DeleteCommRowStatus (UINT4 u4Fsbgp4DeleteCommIpNetwork,
                                 INT4 i4Fsbgp4DeleteCommIpPrefixLen,
                                 UINT4 u4Fsbgp4DeleteCommVal,
                                 INT4 i4SetValFsbgp4DeleteCommRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4DeleteCommIpNetwork;
    UINT4               u4CommIpNet = 0;
    Fsbgp4DeleteCommIpNetwork.pu1_OctetList = (UINT1 *) (VOID *) &u4CommIpNet;
    PTR_ASSIGN4 (Fsbgp4DeleteCommIpNetwork.pu1_OctetList,
                 u4Fsbgp4DeleteCommIpNetwork);
    Fsbgp4DeleteCommIpNetwork.i4_Length = sizeof (UINT4);

    if (nmhSetFsbgp4mpeDeleteCommRowStatus (BGP4_INET_AFI_IPV4,
                                            BGP4_INET_SAFI_UNICAST,
                                            &Fsbgp4DeleteCommIpNetwork,
                                            i4Fsbgp4DeleteCommIpPrefixLen,
                                            u4Fsbgp4DeleteCommVal,
                                            i4SetValFsbgp4DeleteCommRowStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4DeleteCommRowStatus
Input       :  The Indices
Fsbgp4DeleteCommIpNetwork
Fsbgp4DeleteCommIpPrefixLen
Fsbgp4DeleteCommVal

The Object 
testValFsbgp4DeleteCommRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4DeleteCommRowStatus (UINT4 *pu4ErrorCode,
                                    UINT4 u4Fsbgp4DeleteCommIpNetwork,
                                    INT4 i4Fsbgp4DeleteCommIpPrefixLen,
                                    UINT4 u4Fsbgp4DeleteCommVal,
                                    INT4 i4TestValFsbgp4DeleteCommRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4DeleteCommIpNetwork;
    UINT4               u4CommIpNet = 0;
    Fsbgp4DeleteCommIpNetwork.pu1_OctetList = (UINT1 *) (VOID *) &u4CommIpNet;
    PTR_ASSIGN4 (Fsbgp4DeleteCommIpNetwork.pu1_OctetList,
                 u4Fsbgp4DeleteCommIpNetwork);
    Fsbgp4DeleteCommIpNetwork.i4_Length = sizeof (UINT4);

    if (nmhTestv2Fsbgp4mpeDeleteCommRowStatus (pu4ErrorCode,
                                               BGP4_INET_AFI_IPV4,
                                               BGP4_INET_SAFI_UNICAST,
                                               &Fsbgp4DeleteCommIpNetwork,
                                               i4Fsbgp4DeleteCommIpPrefixLen,
                                               u4Fsbgp4DeleteCommVal,
                                               i4TestValFsbgp4DeleteCommRowStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4CommRouteDeleteCommTable
Input       :  The Indices
Fsbgp4DeleteCommIpNetwork
Fsbgp4DeleteCommIpPrefixLen
Fsbgp4DeleteCommVal
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4CommRouteDeleteCommTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4CommRouteCommSetStatusTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4CommRouteCommSetStatusTable
Input       :  The Indices
Fsbgp4CommSetStatusIpNetwork
Fsbgp4CommSetStatusIpPrefixLen
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4CommRouteCommSetStatusTable (UINT4
                                                           u4Fsbgp4CommSetStatusIpNetwork,
                                                           INT4
                                                           i4Fsbgp4CommSetStatusIpPrefixLen)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommSetStatusIpNetwork;
    UINT4               u4StatusIpNet = 0;
    Fsbgp4CommSetStatusIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) &u4StatusIpNet;
    PTR_ASSIGN4 (Fsbgp4CommSetStatusIpNetwork.pu1_OctetList,
                 u4Fsbgp4CommSetStatusIpNetwork);
    Fsbgp4CommSetStatusIpNetwork.i4_Length = sizeof (UINT4);

    if (nmhValidateIndexInstanceFsbgp4MpeCommRouteCommSetStatusTable
        (BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST,
         &Fsbgp4CommSetStatusIpNetwork,
         i4Fsbgp4CommSetStatusIpPrefixLen) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4CommRouteCommSetStatusTable
Input       :  The Indices
Fsbgp4CommSetStatusIpNetwork
Fsbgp4CommSetStatusIpPrefixLen
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4CommRouteCommSetStatusTable (UINT4
                                                   *pu4Fsbgp4CommSetStatusIpNetwork,
                                                   INT4
                                                   *pi4Fsbgp4CommSetStatusIpPrefixLen)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommSetStatusIpNetwork;
    INT4                i4PeerAfi;
    INT4                i4PeerSafi;
    UINT4               au4StatusIpNet[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4StatusIpNet, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4CommSetStatusIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) au4StatusIpNet;

    if (nmhGetFirstIndexFsbgp4MpeCommRouteCommSetStatusTable (&i4PeerAfi,
                                                              &i4PeerSafi,
                                                              &Fsbgp4CommSetStatusIpNetwork,
                                                              pi4Fsbgp4CommSetStatusIpPrefixLen)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4PeerAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4Fsbgp4CommSetStatusIpNetwork,
                            Fsbgp4CommSetStatusIpNetwork.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeCommRouteCommSetStatusTable
               (i4PeerAfi, &i4PeerAfi, i4PeerSafi, &i4PeerSafi,
                &Fsbgp4CommSetStatusIpNetwork, &Fsbgp4CommSetStatusIpNetwork,
                *pi4Fsbgp4CommSetStatusIpPrefixLen,
                pi4Fsbgp4CommSetStatusIpPrefixLen) == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4CommRouteCommSetStatusTable
Input       :  The Indices
Fsbgp4CommSetStatusIpNetwork
nextFsbgp4CommSetStatusIpNetwork
Fsbgp4CommSetStatusIpPrefixLen
nextFsbgp4CommSetStatusIpPrefixLen
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4CommRouteCommSetStatusTable (UINT4
                                                  u4Fsbgp4CommSetStatusIpNetwork,
                                                  UINT4
                                                  *pu4NextFsbgp4CommSetStatusIpNetwork,
                                                  INT4
                                                  i4Fsbgp4CommSetStatusIpPrefixLen,
                                                  INT4
                                                  *pi4NextFsbgp4CommSetStatusIpPrefixLen)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommSetStatusIpNetwork;
    INT4                i4NextAfi = BGP4_INET_AFI_IPV4;
    INT4                i4NextSafi = BGP4_INET_SAFI_UNICAST;
    UINT4               au4StatusIpNet[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4StatusIpNet, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4CommSetStatusIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) au4StatusIpNet;
    PTR_ASSIGN4 (Fsbgp4CommSetStatusIpNetwork.pu1_OctetList,
                 u4Fsbgp4CommSetStatusIpNetwork);

    if (nmhGetNextIndexFsbgp4MpeCommRouteCommSetStatusTable (BGP4_INET_AFI_IPV4,
                                                             &i4NextAfi,
                                                             BGP4_INET_SAFI_UNICAST,
                                                             &i4NextSafi,
                                                             &Fsbgp4CommSetStatusIpNetwork,
                                                             &Fsbgp4CommSetStatusIpNetwork,
                                                             i4Fsbgp4CommSetStatusIpPrefixLen,
                                                             pi4NextFsbgp4CommSetStatusIpPrefixLen)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4NextAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4NextFsbgp4CommSetStatusIpNetwork,
                            Fsbgp4CommSetStatusIpNetwork.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeCommRouteCommSetStatusTable
               (i4NextAfi, &i4NextAfi, i4NextSafi, &i4NextSafi,
                &Fsbgp4CommSetStatusIpNetwork, &Fsbgp4CommSetStatusIpNetwork,
                *pi4NextFsbgp4CommSetStatusIpPrefixLen,
                pi4NextFsbgp4CommSetStatusIpPrefixLen) == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4CommSetStatus
Input       :  The Indices
Fsbgp4CommSetStatusIpNetwork
Fsbgp4CommSetStatusIpPrefixLen

The Object 
retValFsbgp4CommSetStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4CommSetStatus (UINT4 u4Fsbgp4CommSetStatusIpNetwork,
                           INT4 i4Fsbgp4CommSetStatusIpPrefixLen,
                           INT4 *pi4RetValFsbgp4CommSetStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommSetStatusIpNetwork;
    UINT4               u4StatusIpNet = 0;
    Fsbgp4CommSetStatusIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) &u4StatusIpNet;
    PTR_ASSIGN4 (Fsbgp4CommSetStatusIpNetwork.pu1_OctetList,
                 u4Fsbgp4CommSetStatusIpNetwork);
    Fsbgp4CommSetStatusIpNetwork.i4_Length = sizeof (UINT4);

    if (nmhGetFsbgp4mpeCommSetStatus (BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST,
                                      &Fsbgp4CommSetStatusIpNetwork,
                                      i4Fsbgp4CommSetStatusIpPrefixLen,
                                      pi4RetValFsbgp4CommSetStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4CommSetStatusRowStatus
Input       :  The Indices
Fsbgp4CommSetStatusIpNetwork
Fsbgp4CommSetStatusIpPrefixLen

The Object 
retValFsbgp4CommSetStatusRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4CommSetStatusRowStatus (UINT4 u4Fsbgp4CommSetStatusIpNetwork,
                                    INT4 i4Fsbgp4CommSetStatusIpPrefixLen,
                                    INT4 *pi4RetValFsbgp4CommSetStatusRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommSetStatusIpNetwork;
    UINT4               u4StatusIpNet = 0;

    Fsbgp4CommSetStatusIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) &u4StatusIpNet;
    PTR_ASSIGN4 (Fsbgp4CommSetStatusIpNetwork.pu1_OctetList,
                 u4Fsbgp4CommSetStatusIpNetwork);
    Fsbgp4CommSetStatusIpNetwork.i4_Length = sizeof (UINT4);

    if (nmhGetFsbgp4mpeCommSetStatusRowStatus (BGP4_INET_AFI_IPV4,
                                               BGP4_INET_SAFI_UNICAST,
                                               &Fsbgp4CommSetStatusIpNetwork,
                                               i4Fsbgp4CommSetStatusIpPrefixLen,
                                               pi4RetValFsbgp4CommSetStatusRowStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4CommSetStatus
Input       :  The Indices
Fsbgp4CommSetStatusIpNetwork
Fsbgp4CommSetStatusIpPrefixLen

The Object 
setValFsbgp4CommSetStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4CommSetStatus (UINT4 u4Fsbgp4CommSetStatusIpNetwork,
                           INT4 i4Fsbgp4CommSetStatusIpPrefixLen,
                           INT4 i4SetValFsbgp4CommSetStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommSetStatusIpNetwork;
    UINT4               u4StatusIpNet = 0;
    Fsbgp4CommSetStatusIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) &u4StatusIpNet;
    PTR_ASSIGN4 (Fsbgp4CommSetStatusIpNetwork.pu1_OctetList,
                 u4Fsbgp4CommSetStatusIpNetwork);
    Fsbgp4CommSetStatusIpNetwork.i4_Length = sizeof (UINT4);

    if (nmhSetFsbgp4mpeCommSetStatus (BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST,
                                      &Fsbgp4CommSetStatusIpNetwork,
                                      i4Fsbgp4CommSetStatusIpPrefixLen,
                                      i4SetValFsbgp4CommSetStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhSetFsbgp4CommSetStatusRowStatus
Input       :  The Indices
Fsbgp4CommSetStatusIpNetwork
Fsbgp4CommSetStatusIpPrefixLen

The Object 
setValFsbgp4CommSetStatusRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4CommSetStatusRowStatus (UINT4 u4Fsbgp4CommSetStatusIpNetwork,
                                    INT4 i4Fsbgp4CommSetStatusIpPrefixLen,
                                    INT4 i4SetValFsbgp4CommSetStatusRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommSetStatusIpNetwork;
    UINT4               u4StatusIpNet = 0;
    Fsbgp4CommSetStatusIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) &u4StatusIpNet;
    PTR_ASSIGN4 (Fsbgp4CommSetStatusIpNetwork.pu1_OctetList,
                 u4Fsbgp4CommSetStatusIpNetwork);
    Fsbgp4CommSetStatusIpNetwork.i4_Length = sizeof (UINT4);

    if (nmhSetFsbgp4mpeCommSetStatusRowStatus (BGP4_INET_AFI_IPV4,
                                               BGP4_INET_SAFI_UNICAST,
                                               &Fsbgp4CommSetStatusIpNetwork,
                                               i4Fsbgp4CommSetStatusIpPrefixLen,
                                               i4SetValFsbgp4CommSetStatusRowStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4CommSetStatus
Input       :  The Indices
Fsbgp4CommSetStatusIpNetwork
Fsbgp4CommSetStatusIpPrefixLen

The Object 
testValFsbgp4CommSetStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4CommSetStatus (UINT4 *pu4ErrorCode,
                              UINT4 u4Fsbgp4CommSetStatusIpNetwork,
                              INT4 i4Fsbgp4CommSetStatusIpPrefixLen,
                              INT4 i4TestValFsbgp4CommSetStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommSetStatusIpNetwork;
    UINT4               u4StatusIpNet = 0;
    Fsbgp4CommSetStatusIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) &u4StatusIpNet;
    PTR_ASSIGN4 (Fsbgp4CommSetStatusIpNetwork.pu1_OctetList,
                 u4Fsbgp4CommSetStatusIpNetwork);
    Fsbgp4CommSetStatusIpNetwork.i4_Length = sizeof (UINT4);

    if (nmhTestv2Fsbgp4mpeCommSetStatus (pu4ErrorCode,
                                         BGP4_INET_AFI_IPV4,
                                         BGP4_INET_SAFI_UNICAST,
                                         &Fsbgp4CommSetStatusIpNetwork,
                                         i4Fsbgp4CommSetStatusIpPrefixLen,
                                         i4TestValFsbgp4CommSetStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4CommSetStatusRowStatus
Input       :  The Indices
Fsbgp4CommSetStatusIpNetwork
Fsbgp4CommSetStatusIpPrefixLen

The Object 
testValFsbgp4CommSetStatusRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4CommSetStatusRowStatus (UINT4 *pu4ErrorCode,
                                       UINT4 u4Fsbgp4CommSetStatusIpNetwork,
                                       INT4 i4Fsbgp4CommSetStatusIpPrefixLen,
                                       INT4
                                       i4TestValFsbgp4CommSetStatusRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommSetStatusIpNetwork;
    UINT4               u4StatusIpNet = 0;
    Fsbgp4CommSetStatusIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) &u4StatusIpNet;
    PTR_ASSIGN4 (Fsbgp4CommSetStatusIpNetwork.pu1_OctetList,
                 u4Fsbgp4CommSetStatusIpNetwork);
    Fsbgp4CommSetStatusIpNetwork.i4_Length = sizeof (UINT4);

    if (nmhTestv2Fsbgp4mpeCommSetStatusRowStatus (pu4ErrorCode,
                                                  BGP4_INET_AFI_IPV4,
                                                  BGP4_INET_SAFI_UNICAST,
                                                  &Fsbgp4CommSetStatusIpNetwork,
                                                  i4Fsbgp4CommSetStatusIpPrefixLen,
                                                  i4TestValFsbgp4CommSetStatusRowStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4CommRouteCommSetStatusTable
Input       :  The Indices
Fsbgp4CommSetStatusIpNetwork
Fsbgp4CommSetStatusIpPrefixLen
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4CommRouteCommSetStatusTable (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4CommPeerSendStatusTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4CommPeerSendStatusTable
Input       :  The Indices
Fsbgp4PeerAddress
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4CommPeerSendStatusTable (UINT4
                                                       u4Fsbgp4PeerAddress)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerAddress;
    UINT4               u4PeerAddr = 0;
    Fsbgp4PeerAddress.pu1_OctetList = (UINT1 *) (VOID *) &u4PeerAddr;
    PTR_ASSIGN4 (Fsbgp4PeerAddress.pu1_OctetList, u4Fsbgp4PeerAddress);
    Fsbgp4PeerAddress.i4_Length = sizeof (UINT4);

    if (nmhValidateIndexInstanceFsbgp4MpePeerExtTable (BGP4_INET_AFI_IPV4,
                                                       &Fsbgp4PeerAddress)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4CommPeerSendStatusTable
Input       :  The Indices
Fsbgp4PeerAddress
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4CommPeerSendStatusTable (UINT4 *pu4Fsbgp4PeerAddress)
{
    if (nmhGetFirstIndexFsbgp4PeerExtTable (pu4Fsbgp4PeerAddress)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4CommPeerSendStatusTable
Input       :  The Indices
Fsbgp4PeerAddress
nextFsbgp4PeerAddress
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4CommPeerSendStatusTable (UINT4 u4Fsbgp4PeerAddress,
                                              UINT4 *pu4NextFsbgp4PeerAddress)
{

    if (nmhGetNextIndexFsbgp4PeerExtTable
        (u4Fsbgp4PeerAddress, pu4NextFsbgp4PeerAddress) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4CommSendStatus
Input       :  The Indices
Fsbgp4PeerAddress

The Object 
retValFsbgp4CommSendStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4CommSendStatus (UINT4 u4Fsbgp4PeerAddress,
                            INT4 *pi4RetValFsbgp4CommSendStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerAddress;
    UINT4               u4PeerAddr = 0;
    Fsbgp4PeerAddress.pu1_OctetList = (UINT1 *) (VOID *) &u4PeerAddr;
    PTR_ASSIGN4 (Fsbgp4PeerAddress.pu1_OctetList, u4Fsbgp4PeerAddress);
    Fsbgp4PeerAddress.i4_Length = sizeof (UINT4);

    if (nmhGetFsbgp4mpePeerExtCommSendStatus (BGP4_INET_AFI_IPV4,
                                              &Fsbgp4PeerAddress,
                                              pi4RetValFsbgp4CommSendStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4CommPeerSendRowStatus
Input       :  The Indices
Fsbgp4PeerAddress

The Object 
retValFsbgp4CommPeerSendRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4CommPeerSendRowStatus (UINT4 u4Fsbgp4PeerAddress,
                                   INT4 *pi4RetValFsbgp4CommPeerSendRowStatus)
{
    UNUSED_PARAM (u4Fsbgp4PeerAddress);
    UNUSED_PARAM (pi4RetValFsbgp4CommPeerSendRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4CommSendStatus
Input       :  The Indices
Fsbgp4PeerAddress

The Object 
setValFsbgp4CommSendStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4CommSendStatus (UINT4 u4Fsbgp4PeerAddress,
                            INT4 i4SetValFsbgp4CommSendStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerAddress;
    UINT4               u4PeerAddr = 0;
    Fsbgp4PeerAddress.pu1_OctetList = (UINT1 *) (VOID *) &u4PeerAddr;
    PTR_ASSIGN4 (Fsbgp4PeerAddress.pu1_OctetList, u4Fsbgp4PeerAddress);
    Fsbgp4PeerAddress.i4_Length = sizeof (UINT4);

    if (nmhSetFsbgp4mpePeerExtCommSendStatus (BGP4_INET_AFI_IPV4,
                                              &Fsbgp4PeerAddress,
                                              i4SetValFsbgp4CommSendStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4CommPeerSendRowStatus
Input       :  The Indices
Fsbgp4PeerAddress

The Object 
setValFsbgp4CommPeerSendRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4CommPeerSendRowStatus (UINT4 u4Fsbgp4PeerAddress,
                                   INT4 i4SetValFsbgp4CommPeerSendRowStatus)
{
    UNUSED_PARAM (u4Fsbgp4PeerAddress);
    UNUSED_PARAM (i4SetValFsbgp4CommPeerSendRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4CommSendStatus
Input       :  The Indices
Fsbgp4PeerAddress

The Object 
testValFsbgp4CommSendStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4CommSendStatus (UINT4 *pu4ErrorCode, UINT4 u4Fsbgp4PeerAddress,
                               INT4 i4TestValFsbgp4CommSendStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerAddress;
    UINT4               u4PeerAddr = 0;
    Fsbgp4PeerAddress.pu1_OctetList = (UINT1 *) (VOID *) &u4PeerAddr;
    PTR_ASSIGN4 (Fsbgp4PeerAddress.pu1_OctetList, u4Fsbgp4PeerAddress);
    Fsbgp4PeerAddress.i4_Length = sizeof (UINT4);

    if (nmhTestv2Fsbgp4mpePeerExtCommSendStatus (pu4ErrorCode,
                                                 BGP4_INET_AFI_IPV4,
                                                 &Fsbgp4PeerAddress,
                                                 i4TestValFsbgp4CommSendStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4CommPeerSendRowStatus
Input       :  The Indices
Fsbgp4PeerAddress

The Object 
testValFsbgp4CommPeerSendRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4CommPeerSendRowStatus (UINT4 *pu4ErrorCode,
                                      UINT4 u4Fsbgp4PeerAddress,
                                      INT4 i4TestValFsbgp4CommPeerSendRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Fsbgp4PeerAddress);
    UNUSED_PARAM (i4TestValFsbgp4CommPeerSendRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4CommPeerSendStatusTable
Input       :  The Indices
Fsbgp4PeerAddress
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4CommPeerSendStatusTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4CommReceivedRouteCommTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4CommReceivedRouteCommTable
Input       :  The Indices
Fsbgp4IpNet
Fsbgp4IPPrefixLength
Fsbgp4PeerRemAddr
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4CommReceivedRouteCommTable (UINT4 u4Fsbgp4IpNet,
                                                          INT4
                                                          i4Fsbgp4IPPrefixLength,
                                                          UINT4
                                                          u4Fsbgp4PeerRemAddr)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommIpNet;
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommPeerRemAddr;
    UINT4               u4CommPeerRemAddr = 0;
    UINT4               u4CommIPNet = 0;

    Fsbgp4CommIpNet.pu1_OctetList = (UINT1 *) (VOID *) &u4CommIPNet;
    Fsbgp4CommPeerRemAddr.pu1_OctetList = (UINT1 *) (VOID *) &u4CommPeerRemAddr;

    PTR_ASSIGN4 (Fsbgp4CommIpNet.pu1_OctetList, u4Fsbgp4IpNet);
    PTR_ASSIGN4 (Fsbgp4CommPeerRemAddr.pu1_OctetList, u4Fsbgp4PeerRemAddr);

    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable (BGP4_INET_AFI_IPV4,
                                                            BGP4_INET_SAFI_UNICAST,
                                                            &Fsbgp4CommIpNet,
                                                            i4Fsbgp4IPPrefixLength,
                                                            BGP4_INET_AFI_IPV4,
                                                            &Fsbgp4CommPeerRemAddr)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4CommReceivedRouteCommTable
Input       :  The Indices
Fsbgp4IpNet
Fsbgp4IPPrefixLength
Fsbgp4PeerRemAddr
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4CommReceivedRouteCommTable (UINT4 *pu4Fsbgp4IpNet,
                                                  INT4 *pi4Fsbgp4IPPrefixLength,
                                                  UINT4 *pu4Fsbgp4PeerRemAddr)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommIpNet;
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommPeerRemAddr;
    INT4                i4RouteAfi;
    INT4                i4RouteSafi;
    INT4                i4PeerAfi;
    UINT4               au4CommIpNet[BGP4_MAX_INET_ADDRESS_LEN];
    UINT4               au4CommPeerRemAddr[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4CommIpNet, 0, BGP4_MAX_INET_ADDRESS_LEN);
    MEMSET (au4CommPeerRemAddr, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4CommIpNet.pu1_OctetList = (UINT1 *) (VOID *) au4CommIpNet;
    Fsbgp4CommPeerRemAddr.pu1_OctetList = (UINT1 *) (VOID *) au4CommPeerRemAddr;
    if (nmhGetFirstIndexFsbgp4MpeBgp4PathAttrTable (&i4RouteAfi,
                                                    &i4RouteSafi,
                                                    &Fsbgp4CommIpNet,
                                                    pi4Fsbgp4IPPrefixLength,
                                                    &i4PeerAfi,
                                                    &Fsbgp4CommPeerRemAddr)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4RouteAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4Fsbgp4IpNet, Fsbgp4CommIpNet.pu1_OctetList);
                PTR_FETCH4 (*pu4Fsbgp4PeerRemAddr,
                            Fsbgp4CommPeerRemAddr.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeBgp4PathAttrTable
               (i4RouteAfi, &i4RouteAfi, i4RouteSafi, &i4RouteSafi,
                &Fsbgp4CommIpNet, &Fsbgp4CommIpNet, *pi4Fsbgp4IPPrefixLength,
                pi4Fsbgp4IPPrefixLength, i4PeerAfi, &i4PeerAfi,
                &Fsbgp4CommPeerRemAddr,
                &Fsbgp4CommPeerRemAddr) == SNMP_SUCCESS);

    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4CommReceivedRouteCommTable
Input       :  The Indices
Fsbgp4IpNet
nextFsbgp4IpNet
Fsbgp4IPPrefixLength
nextFsbgp4IPPrefixLength
Fsbgp4PeerRemAddr
nextFsbgp4PeerRemAddr
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4CommReceivedRouteCommTable (UINT4 u4Fsbgp4IpNet,
                                                 UINT4 *pu4NextFsbgp4IpNet,
                                                 INT4 i4Fsbgp4IPPrefixLength,
                                                 INT4
                                                 *pi4NextFsbgp4IPPrefixLength,
                                                 UINT4 u4Fsbgp4PeerRemAddr,
                                                 UINT4
                                                 *pu4NextFsbgp4PeerRemAddr)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommIpNet;
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommPeerRemAddr;
    INT4                i4RouteAfi;
    INT4                i4RouteSafi;
    INT4                i4PeerAfi;
    UINT4               au4CommIpNet[BGP4_MAX_INET_ADDRESS_LEN];
    UINT4               au4CommPeerRemAddr[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4CommIpNet, 0, BGP4_MAX_INET_ADDRESS_LEN);
    MEMSET (au4CommPeerRemAddr, 0, BGP4_MAX_INET_ADDRESS_LEN);

    Fsbgp4CommIpNet.pu1_OctetList = (UINT1 *) (VOID *) au4CommIpNet;
    Fsbgp4CommPeerRemAddr.pu1_OctetList = (UINT1 *) (VOID *) au4CommPeerRemAddr;

    PTR_ASSIGN4 (Fsbgp4CommIpNet.pu1_OctetList, u4Fsbgp4IpNet);
    PTR_ASSIGN4 (Fsbgp4CommPeerRemAddr.pu1_OctetList, u4Fsbgp4PeerRemAddr);

    if (nmhGetNextIndexFsbgp4MpeBgp4PathAttrTable (BGP4_INET_AFI_IPV4,
                                                   &i4RouteAfi,
                                                   BGP4_INET_SAFI_UNICAST,
                                                   &i4RouteSafi,
                                                   &Fsbgp4CommIpNet,
                                                   &Fsbgp4CommIpNet,
                                                   i4Fsbgp4IPPrefixLength,
                                                   pi4NextFsbgp4IPPrefixLength,
                                                   BGP4_INET_AFI_IPV4,
                                                   &i4PeerAfi,
                                                   &Fsbgp4CommPeerRemAddr,
                                                   &Fsbgp4CommPeerRemAddr)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4RouteAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4NextFsbgp4IpNet, Fsbgp4CommIpNet.pu1_OctetList);
                PTR_FETCH4 (*pu4NextFsbgp4PeerRemAddr,
                            Fsbgp4CommPeerRemAddr.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeBgp4PathAttrTable
               (i4RouteAfi, &i4RouteAfi, i4RouteSafi, &i4RouteSafi,
                &Fsbgp4CommIpNet, &Fsbgp4CommIpNet,
                *pi4NextFsbgp4IPPrefixLength, pi4NextFsbgp4IPPrefixLength,
                i4PeerAfi, &i4PeerAfi, &Fsbgp4CommPeerRemAddr,
                &Fsbgp4CommPeerRemAddr) == SNMP_SUCCESS);

    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4ReceivedRouteCommPath
Input       :  The Indices
Fsbgp4IpNet
Fsbgp4IPPrefixLength
Fsbgp4PeerRemAddr

The Object 
retValFsbgp4ReceivedRouteCommPath
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4ReceivedRouteCommPath (UINT4 u4Fsbgp4IpNet,
                                   INT4 i4Fsbgp4IPPrefixLength,
                                   UINT4 u4Fsbgp4PeerRemAddr,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsbgp4ReceivedRouteCommPath)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommIpNet;
    tSNMP_OCTET_STRING_TYPE Fsbgp4CommPeerRemAddr;
    UINT4               au4CommIpNet[BGP4_MAX_INET_ADDRESS_LEN];
    UINT4               au4CommPeerRemAddr[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4CommIpNet, 0, BGP4_MAX_INET_ADDRESS_LEN);
    MEMSET (au4CommPeerRemAddr, 0, BGP4_MAX_INET_ADDRESS_LEN);

    Fsbgp4CommIpNet.pu1_OctetList = (UINT1 *) (VOID *) au4CommIpNet;
    Fsbgp4CommPeerRemAddr.pu1_OctetList = (UINT1 *) (VOID *) au4CommPeerRemAddr;
    PTR_ASSIGN4 (Fsbgp4CommIpNet.pu1_OctetList, u4Fsbgp4IpNet);
    PTR_ASSIGN4 (Fsbgp4CommPeerRemAddr.pu1_OctetList, u4Fsbgp4PeerRemAddr);

    if (nmhGetFsbgp4mpebgp4PathAttrCommunity (BGP4_INET_AFI_IPV4,
                                              BGP4_INET_SAFI_UNICAST,
                                              &Fsbgp4CommIpNet,
                                              i4Fsbgp4IPPrefixLength,
                                              BGP4_INET_AFI_IPV4,
                                              &Fsbgp4CommPeerRemAddr,
                                              pRetValFsbgp4ReceivedRouteCommPath)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4ExtCommRouteAddExtCommTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4ExtCommRouteAddExtCommTable
Input       :  The Indices
Fsbgp4AddExtCommIpNetwork
Fsbgp4AddExtCommIpPrefixLen
Fsbgp4AddExtCommVal
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4ExtCommRouteAddExtCommTable (UINT4
                                                           u4Fsbgp4AddExtCommIpNetwork,
                                                           INT4
                                                           i4Fsbgp4AddExtCommIpPrefixLen,
                                                           tSNMP_OCTET_STRING_TYPE
                                                           *
                                                           pFsbgp4AddExtCommVal)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4AddExtCommIpNetwork;
    UINT4               u4CommIpNet = 0;

    Fsbgp4AddExtCommIpNetwork.pu1_OctetList = (UINT1 *) (VOID *) &u4CommIpNet;
    PTR_ASSIGN4 (Fsbgp4AddExtCommIpNetwork.pu1_OctetList,
                 u4Fsbgp4AddExtCommIpNetwork);
    if (nmhValidateIndexInstanceFsbgp4MpeExtCommRouteAddExtCommTable
        (BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST, &Fsbgp4AddExtCommIpNetwork,
         i4Fsbgp4AddExtCommIpPrefixLen, pFsbgp4AddExtCommVal) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4ExtCommRouteAddExtCommTable
Input       :  The Indices
Fsbgp4AddExtCommIpNetwork
Fsbgp4AddExtCommIpPrefixLen
Fsbgp4AddExtCommVal
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4ExtCommRouteAddExtCommTable (UINT4
                                                   *pu4Fsbgp4AddExtCommIpNetwork,
                                                   INT4
                                                   *pi4Fsbgp4AddExtCommIpPrefixLen,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pFsbgp4AddExtCommVal)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4AddExtCommIpNetwork;
    INT4                i4Afi;
    INT4                i4Safi;
    UINT4               u4CommIpNet = 0;

    Fsbgp4AddExtCommIpNetwork.pu1_OctetList = (UINT1 *) (VOID *) &u4CommIpNet;
    if (nmhGetFirstIndexFsbgp4MpeExtCommRouteAddExtCommTable (&i4Afi,
                                                              &i4Safi,
                                                              &Fsbgp4AddExtCommIpNetwork,
                                                              pi4Fsbgp4AddExtCommIpPrefixLen,
                                                              pFsbgp4AddExtCommVal)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4Afi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4Fsbgp4AddExtCommIpNetwork,
                            Fsbgp4AddExtCommIpNetwork.pu1_OctetList);
                return SNMP_SUCCESS;
            }

        }
        while (nmhGetNextIndexFsbgp4MpeExtCommRouteAddExtCommTable
               (i4Afi, &i4Afi, i4Safi, &i4Safi, &Fsbgp4AddExtCommIpNetwork,
                &Fsbgp4AddExtCommIpNetwork, *pi4Fsbgp4AddExtCommIpPrefixLen,
                pi4Fsbgp4AddExtCommIpPrefixLen, pFsbgp4AddExtCommVal,
                pFsbgp4AddExtCommVal) == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4ExtCommRouteAddExtCommTable
Input       :  The Indices
Fsbgp4AddExtCommIpNetwork
nextFsbgp4AddExtCommIpNetwork
Fsbgp4AddExtCommIpPrefixLen
nextFsbgp4AddExtCommIpPrefixLen
Fsbgp4AddExtCommVal
nextFsbgp4AddExtCommVal
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4ExtCommRouteAddExtCommTable (UINT4
                                                  u4Fsbgp4AddExtCommIpNetwork,
                                                  UINT4
                                                  *pu4NextFsbgp4AddExtCommIpNetwork,
                                                  INT4
                                                  i4Fsbgp4AddExtCommIpPrefixLen,
                                                  INT4
                                                  *pi4NextFsbgp4AddExtCommIpPrefixLen,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsbgp4AddExtCommVal,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pNextFsbgp4AddExtCommVal)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4AddExtCommIpNetwork;
    INT4                i4Afi;
    INT4                i4Safi;
    UINT4               au4CommIpNet[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4CommIpNet, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4AddExtCommIpNetwork.pu1_OctetList = (UINT1 *) (VOID *) au4CommIpNet;
    PTR_ASSIGN4 (Fsbgp4AddExtCommIpNetwork.pu1_OctetList,
                 u4Fsbgp4AddExtCommIpNetwork);

    if (nmhGetNextIndexFsbgp4MpeExtCommRouteAddExtCommTable (BGP4_INET_AFI_IPV4,
                                                             &i4Afi,
                                                             BGP4_INET_SAFI_UNICAST,
                                                             &i4Safi,
                                                             &Fsbgp4AddExtCommIpNetwork,
                                                             &Fsbgp4AddExtCommIpNetwork,
                                                             i4Fsbgp4AddExtCommIpPrefixLen,
                                                             pi4NextFsbgp4AddExtCommIpPrefixLen,
                                                             pFsbgp4AddExtCommVal,
                                                             pNextFsbgp4AddExtCommVal)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4Afi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4NextFsbgp4AddExtCommIpNetwork,
                            Fsbgp4AddExtCommIpNetwork.pu1_OctetList);
                return SNMP_SUCCESS;
            }

        }
        while (nmhGetNextIndexFsbgp4MpeExtCommRouteAddExtCommTable
               (i4Afi, &i4Afi, i4Safi, &i4Safi, &Fsbgp4AddExtCommIpNetwork,
                &Fsbgp4AddExtCommIpNetwork, *pi4NextFsbgp4AddExtCommIpPrefixLen,
                pi4NextFsbgp4AddExtCommIpPrefixLen, pNextFsbgp4AddExtCommVal,
                pNextFsbgp4AddExtCommVal) == SNMP_SUCCESS);

    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4AddExtCommRowStatus
Input       :  The Indices
Fsbgp4AddExtCommIpNetwork
Fsbgp4AddExtCommIpPrefixLen
Fsbgp4AddExtCommVal

The Object 
retValFsbgp4AddExtCommRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4AddExtCommRowStatus (UINT4 u4Fsbgp4AddExtCommIpNetwork,
                                 INT4 i4Fsbgp4AddExtCommIpPrefixLen,
                                 tSNMP_OCTET_STRING_TYPE * pFsbgp4AddExtCommVal,
                                 INT4 *pi4RetValFsbgp4AddExtCommRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4AddExtCommIpNetwork;
    UINT4               u4CommIpNet = 0;

    Fsbgp4AddExtCommIpNetwork.pu1_OctetList = (UINT1 *) (VOID *) &u4CommIpNet;
    PTR_ASSIGN4 (Fsbgp4AddExtCommIpNetwork.pu1_OctetList,
                 u4Fsbgp4AddExtCommIpNetwork);
    if (nmhGetFsbgp4mpeAddExtCommRowStatus (BGP4_INET_AFI_IPV4,
                                            BGP4_INET_SAFI_UNICAST,
                                            &Fsbgp4AddExtCommIpNetwork,
                                            i4Fsbgp4AddExtCommIpPrefixLen,
                                            pFsbgp4AddExtCommVal,
                                            pi4RetValFsbgp4AddExtCommRowStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4AddExtCommRowStatus
Input       :  The Indices
Fsbgp4AddExtCommIpNetwork
Fsbgp4AddExtCommIpPrefixLen
Fsbgp4AddExtCommVal

The Object 
setValFsbgp4AddExtCommRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4AddExtCommRowStatus (UINT4 u4Fsbgp4AddExtCommIpNetwork,
                                 INT4 i4Fsbgp4AddExtCommIpPrefixLen,
                                 tSNMP_OCTET_STRING_TYPE * pFsbgp4AddExtCommVal,
                                 INT4 i4SetValFsbgp4AddExtCommRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4AddExtCommIpNetwork;
    UINT4               u4CommIpNet = 0;

    Fsbgp4AddExtCommIpNetwork.pu1_OctetList = (UINT1 *) (VOID *) &u4CommIpNet;
    PTR_ASSIGN4 (Fsbgp4AddExtCommIpNetwork.pu1_OctetList,
                 u4Fsbgp4AddExtCommIpNetwork);
    if (nmhSetFsbgp4mpeAddExtCommRowStatus (BGP4_INET_AFI_IPV4,
                                            BGP4_INET_SAFI_UNICAST,
                                            &Fsbgp4AddExtCommIpNetwork,
                                            i4Fsbgp4AddExtCommIpPrefixLen,
                                            pFsbgp4AddExtCommVal,
                                            i4SetValFsbgp4AddExtCommRowStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4AddExtCommRowStatus
Input       :  The Indices
Fsbgp4AddExtCommIpNetwork
Fsbgp4AddExtCommIpPrefixLen
Fsbgp4AddExtCommVal

The Object 
testValFsbgp4AddExtCommRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4AddExtCommRowStatus (UINT4 *pu4ErrorCode,
                                    UINT4 u4Fsbgp4AddExtCommIpNetwork,
                                    INT4 i4Fsbgp4AddExtCommIpPrefixLen,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsbgp4AddExtCommVal,
                                    INT4 i4TestValFsbgp4AddExtCommRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4AddExtCommIpNetwork;
    UINT4               u4CommIpNet = 0;

    Fsbgp4AddExtCommIpNetwork.pu1_OctetList = (UINT1 *) (VOID *) &u4CommIpNet;
    PTR_ASSIGN4 (Fsbgp4AddExtCommIpNetwork.pu1_OctetList,
                 u4Fsbgp4AddExtCommIpNetwork);
    if (nmhTestv2Fsbgp4mpeAddExtCommRowStatus (pu4ErrorCode,
                                               BGP4_INET_AFI_IPV4,
                                               BGP4_INET_SAFI_UNICAST,
                                               &Fsbgp4AddExtCommIpNetwork,
                                               i4Fsbgp4AddExtCommIpPrefixLen,
                                               pFsbgp4AddExtCommVal,
                                               i4TestValFsbgp4AddExtCommRowStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4ExtCommRouteAddExtCommTable
Input       :  The Indices
Fsbgp4AddExtCommIpNetwork
Fsbgp4AddExtCommIpPrefixLen
Fsbgp4AddExtCommVal
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4ExtCommRouteAddExtCommTable (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4ExtCommRouteDeleteExtCommTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4ExtCommRouteDeleteExtCommTable
Input       :  The Indices
Fsbgp4DeleteExtCommIpNetwork
Fsbgp4DeleteExtCommIpPrefixLen
Fsbgp4DeleteExtCommVal
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4ExtCommRouteDeleteExtCommTable (UINT4
                                                              u4Fsbgp4DeleteExtCommIpNetwork,
                                                              INT4
                                                              i4Fsbgp4DeleteExtCommIpPrefixLen,
                                                              tSNMP_OCTET_STRING_TYPE
                                                              *
                                                              pFsbgp4DeleteExtCommVal)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4DeleteExtCommIpNetwork;
    UINT4               u4CommIpNet = 0;

    Fsbgp4DeleteExtCommIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) &u4CommIpNet;
    PTR_ASSIGN4 (Fsbgp4DeleteExtCommIpNetwork.pu1_OctetList,
                 u4Fsbgp4DeleteExtCommIpNetwork);
    if (nmhValidateIndexInstanceFsbgp4MpeExtCommRouteDeleteExtCommTable
        (BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST,
         &Fsbgp4DeleteExtCommIpNetwork, i4Fsbgp4DeleteExtCommIpPrefixLen,
         pFsbgp4DeleteExtCommVal) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4ExtCommRouteDeleteExtCommTable
Input       :  The Indices
Fsbgp4DeleteExtCommIpNetwork
Fsbgp4DeleteExtCommIpPrefixLen
Fsbgp4DeleteExtCommVal
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4ExtCommRouteDeleteExtCommTable (UINT4
                                                      *pu4Fsbgp4DeleteExtCommIpNetwork,
                                                      INT4
                                                      *pi4Fsbgp4DeleteExtCommIpPrefixLen,
                                                      tSNMP_OCTET_STRING_TYPE *
                                                      pFsbgp4DeleteExtCommVal)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4DeleteExtCommIpNetwork;
    INT4                i4Afi;
    INT4                i4Safi;
    UINT4               au4CommIpNet[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4CommIpNet, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4DeleteExtCommIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) au4CommIpNet;
    if (nmhGetFirstIndexFsbgp4MpeExtCommRouteDeleteExtCommTable
        (&i4Afi, &i4Safi, &Fsbgp4DeleteExtCommIpNetwork,
         pi4Fsbgp4DeleteExtCommIpPrefixLen,
         pFsbgp4DeleteExtCommVal) == SNMP_SUCCESS)
    {
        do
        {
            if (i4Afi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4Fsbgp4DeleteExtCommIpNetwork,
                            Fsbgp4DeleteExtCommIpNetwork.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeExtCommRouteDeleteExtCommTable
               (i4Afi, &i4Afi, i4Safi, &i4Safi, &Fsbgp4DeleteExtCommIpNetwork,
                &Fsbgp4DeleteExtCommIpNetwork,
                *pi4Fsbgp4DeleteExtCommIpPrefixLen,
                pi4Fsbgp4DeleteExtCommIpPrefixLen, pFsbgp4DeleteExtCommVal,
                pFsbgp4DeleteExtCommVal) == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4ExtCommRouteDeleteExtCommTable
Input       :  The Indices
Fsbgp4DeleteExtCommIpNetwork
nextFsbgp4DeleteExtCommIpNetwork
Fsbgp4DeleteExtCommIpPrefixLen
nextFsbgp4DeleteExtCommIpPrefixLen
Fsbgp4DeleteExtCommVal
nextFsbgp4DeleteExtCommVal
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4ExtCommRouteDeleteExtCommTable (UINT4
                                                     u4Fsbgp4DeleteExtCommIpNetwork,
                                                     UINT4
                                                     *pu4NextFsbgp4DeleteExtCommIpNetwork,
                                                     INT4
                                                     i4Fsbgp4DeleteExtCommIpPrefixLen,
                                                     INT4
                                                     *pi4NextFsbgp4DeleteExtCommIpPrefixLen,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pFsbgp4DeleteExtCommVal,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pNextFsbgp4DeleteExtCommVal)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4DeleteExtCommIpNetwork;
    INT4                i4NextAfi;
    INT4                i4NextSafi;
    UINT4               au4CommIpNet[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4CommIpNet, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4DeleteExtCommIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) au4CommIpNet;
    PTR_ASSIGN4 (Fsbgp4DeleteExtCommIpNetwork.pu1_OctetList,
                 u4Fsbgp4DeleteExtCommIpNetwork);

    if (nmhGetNextIndexFsbgp4MpeExtCommRouteDeleteExtCommTable
        (BGP4_INET_AFI_IPV4, &i4NextAfi, BGP4_INET_SAFI_UNICAST, &i4NextSafi,
         &Fsbgp4DeleteExtCommIpNetwork, &Fsbgp4DeleteExtCommIpNetwork,
         i4Fsbgp4DeleteExtCommIpPrefixLen,
         pi4NextFsbgp4DeleteExtCommIpPrefixLen, pFsbgp4DeleteExtCommVal,
         pNextFsbgp4DeleteExtCommVal) == SNMP_SUCCESS)
    {
        do
        {
            if (i4NextAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4NextFsbgp4DeleteExtCommIpNetwork,
                            Fsbgp4DeleteExtCommIpNetwork.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeExtCommRouteDeleteExtCommTable
               (i4NextAfi, &i4NextAfi, i4NextSafi, &i4NextSafi,
                &Fsbgp4DeleteExtCommIpNetwork, &Fsbgp4DeleteExtCommIpNetwork,
                *pi4NextFsbgp4DeleteExtCommIpPrefixLen,
                pi4NextFsbgp4DeleteExtCommIpPrefixLen,
                pNextFsbgp4DeleteExtCommVal,
                pNextFsbgp4DeleteExtCommVal) == SNMP_SUCCESS);

    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4DeleteExtCommRowStatus
Input       :  The Indices
Fsbgp4DeleteExtCommIpNetwork
Fsbgp4DeleteExtCommIpPrefixLen
Fsbgp4DeleteExtCommVal

The Object 
retValFsbgp4DeleteExtCommRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4DeleteExtCommRowStatus (UINT4 u4Fsbgp4DeleteExtCommIpNetwork,
                                    INT4 i4Fsbgp4DeleteExtCommIpPrefixLen,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsbgp4DeleteExtCommVal,
                                    INT4 *pi4RetValFsbgp4DeleteExtCommRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4DeleteExtCommIpNetwork;
    UINT4               u4CommIpNet = 0;

    Fsbgp4DeleteExtCommIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) &u4CommIpNet;
    PTR_ASSIGN4 (Fsbgp4DeleteExtCommIpNetwork.pu1_OctetList,
                 u4Fsbgp4DeleteExtCommIpNetwork);
    if (nmhGetFsbgp4mpeDeleteExtCommRowStatus
        (BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST,
         &Fsbgp4DeleteExtCommIpNetwork, i4Fsbgp4DeleteExtCommIpPrefixLen,
         pFsbgp4DeleteExtCommVal,
         pi4RetValFsbgp4DeleteExtCommRowStatus) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4DeleteExtCommRowStatus
Input       :  The Indices
Fsbgp4DeleteExtCommIpNetwork
Fsbgp4DeleteExtCommIpPrefixLen
Fsbgp4DeleteExtCommVal

The Object 
setValFsbgp4DeleteExtCommRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4DeleteExtCommRowStatus (UINT4 u4Fsbgp4DeleteExtCommIpNetwork,
                                    INT4 i4Fsbgp4DeleteExtCommIpPrefixLen,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsbgp4DeleteExtCommVal,
                                    INT4 i4SetValFsbgp4DeleteExtCommRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4DeleteExtCommIpNetwork;
    UINT4               u4CommIpNet = 0;

    Fsbgp4DeleteExtCommIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) &u4CommIpNet;
    PTR_ASSIGN4 (Fsbgp4DeleteExtCommIpNetwork.pu1_OctetList,
                 u4Fsbgp4DeleteExtCommIpNetwork);
    if (nmhSetFsbgp4mpeDeleteExtCommRowStatus
        (BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST,
         &Fsbgp4DeleteExtCommIpNetwork, i4Fsbgp4DeleteExtCommIpPrefixLen,
         pFsbgp4DeleteExtCommVal,
         i4SetValFsbgp4DeleteExtCommRowStatus) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4DeleteExtCommRowStatus
Input       :  The Indices
Fsbgp4DeleteExtCommIpNetwork
Fsbgp4DeleteExtCommIpPrefixLen
Fsbgp4DeleteExtCommVal

The Object 
testValFsbgp4DeleteExtCommRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4DeleteExtCommRowStatus (UINT4 *pu4ErrorCode,
                                       UINT4 u4Fsbgp4DeleteExtCommIpNetwork,
                                       INT4 i4Fsbgp4DeleteExtCommIpPrefixLen,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsbgp4DeleteExtCommVal,
                                       INT4
                                       i4TestValFsbgp4DeleteExtCommRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4DeleteExtCommIpNetwork;
    UINT4               u4CommIpNet = 0;

    Fsbgp4DeleteExtCommIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) &u4CommIpNet;
    PTR_ASSIGN4 (Fsbgp4DeleteExtCommIpNetwork.pu1_OctetList,
                 u4Fsbgp4DeleteExtCommIpNetwork);
    if (nmhTestv2Fsbgp4mpeDeleteExtCommRowStatus
        (pu4ErrorCode, BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST,
         &Fsbgp4DeleteExtCommIpNetwork, i4Fsbgp4DeleteExtCommIpPrefixLen,
         pFsbgp4DeleteExtCommVal,
         i4TestValFsbgp4DeleteExtCommRowStatus) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4ExtCommRouteDeleteExtCommTable
Input       :  The Indices
Fsbgp4DeleteExtCommIpNetwork
Fsbgp4DeleteExtCommIpPrefixLen
Fsbgp4DeleteExtCommVal
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4ExtCommRouteDeleteExtCommTable (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4ExtCommRouteExtCommSetStatusTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4ExtCommRouteExtCommSetStatusTable
Input       :  The Indices
Fsbgp4ExtCommSetStatusIpNetwork
Fsbgp4ExtCommSetStatusIpPrefixLen
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4ExtCommRouteExtCommSetStatusTable (UINT4
                                                                 u4Fsbgp4ExtCommSetStatusIpNetwork,
                                                                 INT4
                                                                 i4Fsbgp4ExtCommSetStatusIpPrefixLen)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4ExtCommSetStatusIpNetwork;
    UINT4               u4StatusIpNet = 0;

    Fsbgp4ExtCommSetStatusIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) &u4StatusIpNet;
    PTR_ASSIGN4 (Fsbgp4ExtCommSetStatusIpNetwork.pu1_OctetList,
                 u4Fsbgp4ExtCommSetStatusIpNetwork);

    if (nmhValidateIndexInstanceFsbgp4MpeExtCommRouteExtCommSetStatusTable
        (BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST,
         &Fsbgp4ExtCommSetStatusIpNetwork,
         i4Fsbgp4ExtCommSetStatusIpPrefixLen) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4ExtCommRouteExtCommSetStatusTable
Input       :  The Indices
Fsbgp4ExtCommSetStatusIpNetwork
Fsbgp4ExtCommSetStatusIpPrefixLen
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4ExtCommRouteExtCommSetStatusTable (UINT4
                                                         *pu4Fsbgp4ExtCommSetStatusIpNetwork,
                                                         INT4
                                                         *pi4Fsbgp4ExtCommSetStatusIpPrefixLen)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4ExtCommSetStatusIpNetwork;
    INT4                i4Afi;
    INT4                i4Safi;
    UINT4               au4StatusIpNet[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4StatusIpNet, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4ExtCommSetStatusIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) au4StatusIpNet;

    if (nmhGetFirstIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable (&i4Afi,
                                                                    &i4Safi,
                                                                    &Fsbgp4ExtCommSetStatusIpNetwork,
                                                                    pi4Fsbgp4ExtCommSetStatusIpPrefixLen)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4Afi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4Fsbgp4ExtCommSetStatusIpNetwork,
                            Fsbgp4ExtCommSetStatusIpNetwork.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable
               (i4Afi, &i4Afi, i4Safi, &i4Safi,
                &Fsbgp4ExtCommSetStatusIpNetwork,
                &Fsbgp4ExtCommSetStatusIpNetwork,
                *pi4Fsbgp4ExtCommSetStatusIpPrefixLen,
                pi4Fsbgp4ExtCommSetStatusIpPrefixLen) == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4ExtCommRouteExtCommSetStatusTable
Input       :  The Indices
Fsbgp4ExtCommSetStatusIpNetwork
nextFsbgp4ExtCommSetStatusIpNetwork
Fsbgp4ExtCommSetStatusIpPrefixLen
nextFsbgp4ExtCommSetStatusIpPrefixLen
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4ExtCommRouteExtCommSetStatusTable (UINT4
                                                        u4Fsbgp4ExtCommSetStatusIpNetwork,
                                                        UINT4
                                                        *pu4NextFsbgp4ExtCommSetStatusIpNetwork,
                                                        INT4
                                                        i4Fsbgp4ExtCommSetStatusIpPrefixLen,
                                                        INT4
                                                        *pi4NextFsbgp4ExtCommSetStatusIpPrefixLen)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4ExtCommSetStatusIpNetwork;
    INT4                i4NextAfi;
    INT4                i4NextSafi;
    UINT4               au4StatusIpNet[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4StatusIpNet, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4ExtCommSetStatusIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) au4StatusIpNet;

    PTR_ASSIGN4 (Fsbgp4ExtCommSetStatusIpNetwork.pu1_OctetList,
                 u4Fsbgp4ExtCommSetStatusIpNetwork);

    if (nmhGetNextIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable
        (BGP4_INET_AFI_IPV4, &i4NextAfi, BGP4_INET_SAFI_UNICAST, &i4NextSafi,
         &Fsbgp4ExtCommSetStatusIpNetwork, &Fsbgp4ExtCommSetStatusIpNetwork,
         i4Fsbgp4ExtCommSetStatusIpPrefixLen,
         pi4NextFsbgp4ExtCommSetStatusIpPrefixLen) == SNMP_SUCCESS)
    {
        do
        {
            if (i4NextAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4NextFsbgp4ExtCommSetStatusIpNetwork,
                            Fsbgp4ExtCommSetStatusIpNetwork.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable
               (i4NextAfi, &i4NextAfi, i4NextSafi, &i4NextSafi,
                &Fsbgp4ExtCommSetStatusIpNetwork,
                &Fsbgp4ExtCommSetStatusIpNetwork,
                *pi4NextFsbgp4ExtCommSetStatusIpPrefixLen,
                pi4NextFsbgp4ExtCommSetStatusIpPrefixLen) == SNMP_SUCCESS);

    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4ExtCommSetStatus
Input       :  The Indices
Fsbgp4ExtCommSetStatusIpNetwork
Fsbgp4ExtCommSetStatusIpPrefixLen

The Object 
retValFsbgp4ExtCommSetStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4ExtCommSetStatus (UINT4 u4Fsbgp4ExtCommSetStatusIpNetwork,
                              INT4 i4Fsbgp4ExtCommSetStatusIpPrefixLen,
                              INT4 *pi4RetValFsbgp4ExtCommSetStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4ExtCommSetStatusIpNetwork;
    UINT4               u4StatusIpNet = 0;
    Fsbgp4ExtCommSetStatusIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) &u4StatusIpNet;
    PTR_ASSIGN4 (Fsbgp4ExtCommSetStatusIpNetwork.pu1_OctetList,
                 u4Fsbgp4ExtCommSetStatusIpNetwork);
    if (nmhGetFsbgp4mpeExtCommSetStatus
        (BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST,
         &Fsbgp4ExtCommSetStatusIpNetwork, i4Fsbgp4ExtCommSetStatusIpPrefixLen,
         pi4RetValFsbgp4ExtCommSetStatus) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4ExtCommSetStatusRowStatus
Input       :  The Indices
Fsbgp4ExtCommSetStatusIpNetwork
Fsbgp4ExtCommSetStatusIpPrefixLen

The Object 
retValFsbgp4ExtCommSetStatusRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4ExtCommSetStatusRowStatus (UINT4 u4Fsbgp4ExtCommSetStatusIpNetwork,
                                       INT4 i4Fsbgp4ExtCommSetStatusIpPrefixLen,
                                       INT4
                                       *pi4RetValFsbgp4ExtCommSetStatusRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4ExtCommSetStatusIpNetwork;
    UINT4               u4StatusIpNet = 0;

    Fsbgp4ExtCommSetStatusIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) &u4StatusIpNet;
    PTR_ASSIGN4 (Fsbgp4ExtCommSetStatusIpNetwork.pu1_OctetList,
                 u4Fsbgp4ExtCommSetStatusIpNetwork);
    if (nmhGetFsbgp4mpeExtCommSetStatusRowStatus
        (BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST,
         &Fsbgp4ExtCommSetStatusIpNetwork, i4Fsbgp4ExtCommSetStatusIpPrefixLen,
         pi4RetValFsbgp4ExtCommSetStatusRowStatus) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4ExtCommSetStatus
Input       :  The Indices
Fsbgp4ExtCommSetStatusIpNetwork
Fsbgp4ExtCommSetStatusIpPrefixLen

The Object 
setValFsbgp4ExtCommSetStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4ExtCommSetStatus (UINT4 u4Fsbgp4ExtCommSetStatusIpNetwork,
                              INT4 i4Fsbgp4ExtCommSetStatusIpPrefixLen,
                              INT4 i4SetValFsbgp4ExtCommSetStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4ExtCommSetStatusIpNetwork;
    UINT4               u4StatusIpNet = 0;
    Fsbgp4ExtCommSetStatusIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) &u4StatusIpNet;
    PTR_ASSIGN4 (Fsbgp4ExtCommSetStatusIpNetwork.pu1_OctetList,
                 u4Fsbgp4ExtCommSetStatusIpNetwork);
    if (nmhSetFsbgp4mpeExtCommSetStatus
        (BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST,
         &Fsbgp4ExtCommSetStatusIpNetwork, i4Fsbgp4ExtCommSetStatusIpPrefixLen,
         i4SetValFsbgp4ExtCommSetStatus) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4ExtCommSetStatusRowStatus
Input       :  The Indices
Fsbgp4ExtCommSetStatusIpNetwork
Fsbgp4ExtCommSetStatusIpPrefixLen

The Object 
setValFsbgp4ExtCommSetStatusRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4ExtCommSetStatusRowStatus (UINT4 u4Fsbgp4ExtCommSetStatusIpNetwork,
                                       INT4 i4Fsbgp4ExtCommSetStatusIpPrefixLen,
                                       INT4
                                       i4SetValFsbgp4ExtCommSetStatusRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4ExtCommSetStatusIpNetwork;
    UINT4               u4StatusIpNet = 0;
    Fsbgp4ExtCommSetStatusIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) &u4StatusIpNet;
    PTR_ASSIGN4 (Fsbgp4ExtCommSetStatusIpNetwork.pu1_OctetList,
                 u4Fsbgp4ExtCommSetStatusIpNetwork);
    if (nmhSetFsbgp4mpeExtCommSetStatusRowStatus
        (BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST,
         &Fsbgp4ExtCommSetStatusIpNetwork, i4Fsbgp4ExtCommSetStatusIpPrefixLen,
         i4SetValFsbgp4ExtCommSetStatusRowStatus) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4ExtCommSetStatus
Input       :  The Indices
Fsbgp4ExtCommSetStatusIpNetwork
Fsbgp4ExtCommSetStatusIpPrefixLen

The Object 
testValFsbgp4ExtCommSetStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4ExtCommSetStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4Fsbgp4ExtCommSetStatusIpNetwork,
                                 INT4 i4Fsbgp4ExtCommSetStatusIpPrefixLen,
                                 INT4 i4TestValFsbgp4ExtCommSetStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4ExtCommSetStatusIpNetwork;
    UINT4               u4StatusIpNet = 0;
    Fsbgp4ExtCommSetStatusIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) &u4StatusIpNet;
    PTR_ASSIGN4 (Fsbgp4ExtCommSetStatusIpNetwork.pu1_OctetList,
                 u4Fsbgp4ExtCommSetStatusIpNetwork);
    if (nmhTestv2Fsbgp4mpeExtCommSetStatus
        (pu4ErrorCode, BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST,
         &Fsbgp4ExtCommSetStatusIpNetwork, i4Fsbgp4ExtCommSetStatusIpPrefixLen,
         i4TestValFsbgp4ExtCommSetStatus) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4ExtCommSetStatusRowStatus
Input       :  The Indices
Fsbgp4ExtCommSetStatusIpNetwork
Fsbgp4ExtCommSetStatusIpPrefixLen

The Object 
testValFsbgp4ExtCommSetStatusRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4ExtCommSetStatusRowStatus (UINT4 *pu4ErrorCode,
                                          UINT4
                                          u4Fsbgp4ExtCommSetStatusIpNetwork,
                                          INT4
                                          i4Fsbgp4ExtCommSetStatusIpPrefixLen,
                                          INT4
                                          i4TestValFsbgp4ExtCommSetStatusRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4ExtCommSetStatusIpNetwork;
    UINT4               u4StatusIpNet = 0;
    Fsbgp4ExtCommSetStatusIpNetwork.pu1_OctetList =
        (UINT1 *) (VOID *) &u4StatusIpNet;
    PTR_ASSIGN4 (Fsbgp4ExtCommSetStatusIpNetwork.pu1_OctetList,
                 u4Fsbgp4ExtCommSetStatusIpNetwork);
    if (nmhTestv2Fsbgp4mpeExtCommSetStatusRowStatus
        (pu4ErrorCode, BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST,
         &Fsbgp4ExtCommSetStatusIpNetwork, i4Fsbgp4ExtCommSetStatusIpPrefixLen,
         i4TestValFsbgp4ExtCommSetStatusRowStatus) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4ExtCommRouteExtCommSetStatusTable
Input       :  The Indices
Fsbgp4ExtCommSetStatusIpNetwork
Fsbgp4ExtCommSetStatusIpPrefixLen
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4ExtCommRouteExtCommSetStatusTable (UINT4 *pu4ErrorCode,
                                                 tSnmpIndexList *
                                                 pSnmpIndexList,
                                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4ExtCommPeerSendStatusTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4ExtCommPeerSendStatusTable
Input       :  The Indices
Fsbgp4ExtCommPeerAddress
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4ExtCommPeerSendStatusTable (UINT4
                                                          u4Fsbgp4ExtCommPeerAddress)
{

    if (nmhValidateIndexInstanceFsbgp4PeerExtTable (u4Fsbgp4ExtCommPeerAddress)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4ExtCommPeerSendStatusTable
Input       :  The Indices
Fsbgp4ExtCommPeerAddress
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4ExtCommPeerSendStatusTable (UINT4
                                                  *pu4Fsbgp4ExtCommPeerAddress)
{
    if (nmhGetFirstIndexFsbgp4PeerExtTable (pu4Fsbgp4ExtCommPeerAddress)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4ExtCommPeerSendStatusTable
Input       :  The Indices
Fsbgp4ExtCommPeerAddress
nextFsbgp4ExtCommPeerAddress
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4ExtCommPeerSendStatusTable (UINT4
                                                 u4Fsbgp4ExtCommPeerAddress,
                                                 UINT4
                                                 *pu4NextFsbgp4ExtCommPeerAddress)
{
    if (nmhGetNextIndexFsbgp4PeerExtTable (u4Fsbgp4ExtCommPeerAddress,
                                           pu4NextFsbgp4ExtCommPeerAddress)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4ExtCommPeerSendStatus
Input       :  The Indices
Fsbgp4ExtCommPeerAddress

The Object 
retValFsbgp4ExtCommPeerSendStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4ExtCommPeerSendStatus (UINT4 u4Fsbgp4ExtCommPeerAddress,
                                   INT4 *pi4RetValFsbgp4ExtCommPeerSendStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4ExtCommPeerAddress;
    UINT4               u4CommPeer = 0;
    Fsbgp4ExtCommPeerAddress.pu1_OctetList = (UINT1 *) (VOID *) &u4CommPeer;
    PTR_ASSIGN4 (Fsbgp4ExtCommPeerAddress.pu1_OctetList,
                 u4Fsbgp4ExtCommPeerAddress);

    if (nmhGetFsbgp4mpePeerExtCommSendStatus (BGP4_INET_AFI_IPV4,
                                              &Fsbgp4ExtCommPeerAddress,
                                              pi4RetValFsbgp4ExtCommPeerSendStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4ExtCommPeerSendStatusRowStatus
Input       :  The Indices
Fsbgp4ExtCommPeerAddress

The Object 
retValFsbgp4ExtCommPeerSendStatusRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4ExtCommPeerSendStatusRowStatus (UINT4 u4Fsbgp4ExtCommPeerAddress,
                                            INT4
                                            *pi4RetValFsbgp4ExtCommPeerSendStatusRowStatus)
{
    UNUSED_PARAM (u4Fsbgp4ExtCommPeerAddress);
    UNUSED_PARAM (pi4RetValFsbgp4ExtCommPeerSendStatusRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4ExtCommPeerSendStatus
Input       :  The Indices
Fsbgp4ExtCommPeerAddress

The Object 
setValFsbgp4ExtCommPeerSendStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4ExtCommPeerSendStatus (UINT4 u4Fsbgp4ExtCommPeerAddress,
                                   INT4 i4SetValFsbgp4ExtCommPeerSendStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4ExtCommPeerAddress;
    UINT4               u4CommPeer = 0;
    Fsbgp4ExtCommPeerAddress.pu1_OctetList = (UINT1 *) (VOID *) &u4CommPeer;
    PTR_ASSIGN4 (Fsbgp4ExtCommPeerAddress.pu1_OctetList,
                 u4Fsbgp4ExtCommPeerAddress);

    if (nmhSetFsbgp4mpePeerExtCommSendStatus (BGP4_INET_AFI_IPV4,
                                              &Fsbgp4ExtCommPeerAddress,
                                              i4SetValFsbgp4ExtCommPeerSendStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4ExtCommPeerSendStatusRowStatus
Input       :  The Indices
Fsbgp4ExtCommPeerAddress

The Object 
setValFsbgp4ExtCommPeerSendStatusRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4ExtCommPeerSendStatusRowStatus (UINT4 u4Fsbgp4ExtCommPeerAddress,
                                            INT4
                                            i4SetValFsbgp4ExtCommPeerSendStatusRowStatus)
{
    UNUSED_PARAM (u4Fsbgp4ExtCommPeerAddress);
    UNUSED_PARAM (i4SetValFsbgp4ExtCommPeerSendStatusRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4ExtCommPeerSendStatus
Input       :  The Indices
Fsbgp4ExtCommPeerAddress

The Object 
testValFsbgp4ExtCommPeerSendStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4ExtCommPeerSendStatus (UINT4 *pu4ErrorCode,
                                      UINT4 u4Fsbgp4ExtCommPeerAddress,
                                      INT4 i4TestValFsbgp4ExtCommPeerSendStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4ExtCommPeerAddress;
    UINT4               u4CommPeer = 0;
    Fsbgp4ExtCommPeerAddress.pu1_OctetList = (UINT1 *) (VOID *) &u4CommPeer;
    PTR_ASSIGN4 (Fsbgp4ExtCommPeerAddress.pu1_OctetList,
                 u4Fsbgp4ExtCommPeerAddress);

    if (nmhTestv2Fsbgp4mpePeerExtCommSendStatus (pu4ErrorCode,
                                                 BGP4_INET_AFI_IPV4,
                                                 &Fsbgp4ExtCommPeerAddress,
                                                 i4TestValFsbgp4ExtCommPeerSendStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4ExtCommPeerSendStatusRowStatus
Input       :  The Indices
Fsbgp4ExtCommPeerAddress

The Object 
testValFsbgp4ExtCommPeerSendStatusRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4ExtCommPeerSendStatusRowStatus (UINT4 *pu4ErrorCode,
                                               UINT4 u4Fsbgp4ExtCommPeerAddress,
                                               INT4
                                               i4TestValFsbgp4ExtCommPeerSendStatusRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Fsbgp4ExtCommPeerAddress);
    UNUSED_PARAM (i4TestValFsbgp4ExtCommPeerSendStatusRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4ExtCommPeerSendStatusTable
Input       :  The Indices
Fsbgp4ExtCommPeerAddress
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4ExtCommPeerSendStatusTable (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4PeerLinkBwTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4PeerLinkBwTable
Input       :  The Indices
Fsbgp4PeerLinkRemAddr
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4PeerLinkBwTable (UINT4 u4Fsbgp4PeerLinkRemAddr)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerLinkRemAddr;
    UINT4               u4LinkRemAddr = 0;
    Fsbgp4PeerLinkRemAddr.pu1_OctetList = (UINT1 *) (VOID *) &u4LinkRemAddr;
    PTR_ASSIGN4 (Fsbgp4PeerLinkRemAddr.pu1_OctetList, u4Fsbgp4PeerLinkRemAddr);
    Fsbgp4PeerLinkRemAddr.i4_Length = sizeof (UINT4);

    if (nmhValidateIndexInstanceFsbgp4MpePeerLinkBwTable (BGP4_INET_AFI_IPV4,
                                                          &Fsbgp4PeerLinkRemAddr)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4PeerLinkBwTable
Input       :  The Indices
Fsbgp4PeerLinkRemAddr
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4PeerLinkBwTable (UINT4 *pu4Fsbgp4PeerLinkRemAddr)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerLinkRemAddr;
    INT4                i4Afi;
    UINT4               au4LinkRemAddr[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4LinkRemAddr, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4PeerLinkRemAddr.pu1_OctetList = (UINT1 *) (VOID *) au4LinkRemAddr;

    if (nmhGetFirstIndexFsbgp4MpePeerLinkBwTable (&i4Afi,
                                                  &Fsbgp4PeerLinkRemAddr)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4Afi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4Fsbgp4PeerLinkRemAddr,
                            Fsbgp4PeerLinkRemAddr.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpePeerLinkBwTable (i4Afi, &i4Afi,
                                                        &Fsbgp4PeerLinkRemAddr,
                                                        &Fsbgp4PeerLinkRemAddr)
               == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4PeerLinkBwTable
Input       :  The Indices
Fsbgp4PeerLinkRemAddr
nextFsbgp4PeerLinkRemAddr
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4PeerLinkBwTable (UINT4 u4Fsbgp4PeerLinkRemAddr,
                                      UINT4 *pu4NextFsbgp4PeerLinkRemAddr)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerLinkRemAddr;
    INT4                i4NextAfi;
    UINT4               au4LinkRemAddr[BGP4_MAX_INET_ADDRESS_LEN];

    MEMSET (au4LinkRemAddr, 0, BGP4_MAX_INET_ADDRESS_LEN);
    Fsbgp4PeerLinkRemAddr.pu1_OctetList = (UINT1 *) (VOID *) au4LinkRemAddr;

    PTR_ASSIGN4 (Fsbgp4PeerLinkRemAddr.pu1_OctetList, u4Fsbgp4PeerLinkRemAddr);
    if (nmhGetNextIndexFsbgp4MpePeerLinkBwTable (BGP4_INET_AFI_IPV4,
                                                 &i4NextAfi,
                                                 &Fsbgp4PeerLinkRemAddr,
                                                 &Fsbgp4PeerLinkRemAddr)
        == SNMP_SUCCESS)
    {
        do
        {
            if (i4NextAfi == BGP4_INET_AFI_IPV4)
            {
                PTR_FETCH4 (*pu4NextFsbgp4PeerLinkRemAddr,
                            Fsbgp4PeerLinkRemAddr.pu1_OctetList);
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpePeerLinkBwTable (i4NextAfi, &i4NextAfi,
                                                        &Fsbgp4PeerLinkRemAddr,
                                                        &Fsbgp4PeerLinkRemAddr)
               == SNMP_SUCCESS);

    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4LinkBandWidth
Input       :  The Indices
Fsbgp4PeerLinkRemAddr

The Object 
retValFsbgp4LinkBandWidth
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4LinkBandWidth (UINT4 u4Fsbgp4PeerLinkRemAddr,
                           UINT4 *pu4RetValFsbgp4LinkBandWidth)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerLinkRemAddr;
    UINT4               u4LinkRemAddr = 0;

    Fsbgp4PeerLinkRemAddr.pu1_OctetList = (UINT1 *) (VOID *) &u4LinkRemAddr;
    PTR_ASSIGN4 (Fsbgp4PeerLinkRemAddr.pu1_OctetList, u4Fsbgp4PeerLinkRemAddr);

    if (nmhGetFsbgp4mpeLinkBandWidth (BGP4_INET_AFI_IPV4,
                                      &Fsbgp4PeerLinkRemAddr,
                                      pu4RetValFsbgp4LinkBandWidth)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4PeerLinkBwRowStatus
Input       :  The Indices
Fsbgp4PeerLinkRemAddr

The Object 
retValFsbgp4PeerLinkBwRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4PeerLinkBwRowStatus (UINT4 u4Fsbgp4PeerLinkRemAddr,
                                 INT4 *pi4RetValFsbgp4PeerLinkBwRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerLinkRemAddr;
    UINT4               u4LinkRemAddr = 0;
    Fsbgp4PeerLinkRemAddr.pu1_OctetList = (UINT1 *) (VOID *) &u4LinkRemAddr;
    PTR_ASSIGN4 (Fsbgp4PeerLinkRemAddr.pu1_OctetList, u4Fsbgp4PeerLinkRemAddr);

    if (nmhGetFsbgp4mpePeerLinkBwRowStatus (BGP4_INET_AFI_IPV4,
                                            &Fsbgp4PeerLinkRemAddr,
                                            pi4RetValFsbgp4PeerLinkBwRowStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4LinkBandWidth
Input       :  The Indices
Fsbgp4PeerLinkRemAddr

The Object 
setValFsbgp4LinkBandWidth
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4LinkBandWidth (UINT4 u4Fsbgp4PeerLinkRemAddr,
                           UINT4 u4SetValFsbgp4LinkBandWidth)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerLinkRemAddr;
    UINT4               u4LinkRemAddr = 0;
    Fsbgp4PeerLinkRemAddr.pu1_OctetList = (UINT1 *) (VOID *) &u4LinkRemAddr;
    PTR_ASSIGN4 (Fsbgp4PeerLinkRemAddr.pu1_OctetList, u4Fsbgp4PeerLinkRemAddr);

    if (nmhSetFsbgp4mpeLinkBandWidth (BGP4_INET_AFI_IPV4,
                                      &Fsbgp4PeerLinkRemAddr,
                                      u4SetValFsbgp4LinkBandWidth)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4PeerLinkBwRowStatus
Input       :  The Indices
Fsbgp4PeerLinkRemAddr

The Object 
setValFsbgp4PeerLinkBwRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4PeerLinkBwRowStatus (UINT4 u4Fsbgp4PeerLinkRemAddr,
                                 INT4 i4SetValFsbgp4PeerLinkBwRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerLinkRemAddr;
    UINT4               u4LinkRemAddr = 0;

    Fsbgp4PeerLinkRemAddr.pu1_OctetList = (UINT1 *) (VOID *) &u4LinkRemAddr;
    PTR_ASSIGN4 (Fsbgp4PeerLinkRemAddr.pu1_OctetList, u4Fsbgp4PeerLinkRemAddr);

    if (nmhSetFsbgp4mpePeerLinkBwRowStatus (BGP4_INET_AFI_IPV4,
                                            &Fsbgp4PeerLinkRemAddr,
                                            i4SetValFsbgp4PeerLinkBwRowStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4LinkBandWidth
Input       :  The Indices
Fsbgp4PeerLinkRemAddr

The Object 
testValFsbgp4LinkBandWidth
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4LinkBandWidth (UINT4 *pu4ErrorCode,
                              UINT4 u4Fsbgp4PeerLinkRemAddr,
                              UINT4 u4TestValFsbgp4LinkBandWidth)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerLinkRemAddr;
    UINT4               u4LinkRemAddr = 0;

    Fsbgp4PeerLinkRemAddr.pu1_OctetList = (UINT1 *) (VOID *) &u4LinkRemAddr;
    PTR_ASSIGN4 (Fsbgp4PeerLinkRemAddr.pu1_OctetList, u4Fsbgp4PeerLinkRemAddr);

    if (nmhTestv2Fsbgp4mpeLinkBandWidth (pu4ErrorCode,
                                         BGP4_INET_AFI_IPV4,
                                         &Fsbgp4PeerLinkRemAddr,
                                         u4TestValFsbgp4LinkBandWidth)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4PeerLinkBwRowStatus
Input       :  The Indices
Fsbgp4PeerLinkRemAddr

The Object 
testValFsbgp4PeerLinkBwRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4PeerLinkBwRowStatus (UINT4 *pu4ErrorCode,
                                    UINT4 u4Fsbgp4PeerLinkRemAddr,
                                    INT4 i4TestValFsbgp4PeerLinkBwRowStatus)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4PeerLinkRemAddr;
    UINT4               u4LinkRemAddr = 0;

    Fsbgp4PeerLinkRemAddr.pu1_OctetList = (UINT1 *) (VOID *) &u4LinkRemAddr;
    PTR_ASSIGN4 (Fsbgp4PeerLinkRemAddr.pu1_OctetList, u4Fsbgp4PeerLinkRemAddr);

    if (nmhTestv2Fsbgp4mpePeerLinkBwRowStatus (pu4ErrorCode,
                                               BGP4_INET_AFI_IPV4,
                                               &Fsbgp4PeerLinkRemAddr,
                                               i4TestValFsbgp4PeerLinkBwRowStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4PeerLinkBwTable
Input       :  The Indices
Fsbgp4PeerLinkRemAddr
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4PeerLinkBwTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4ExtCommReceivedRouteExtCommTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4ExtCommReceivedRouteExtCommTable
Input       :  The Indices
Fsbgp4ExtCommIpNet
Fsbgp4ExtCommIPPrefixLength
Fsbgp4ExtCommPeerRemAddr
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4ExtCommReceivedRouteExtCommTable (UINT4
                                                                u4Fsbgp4ExtCommIpNet,
                                                                INT4
                                                                i4Fsbgp4ExtCommIPPrefixLength,
                                                                UINT4
                                                                u4Fsbgp4ExtCommPeerRemAddr)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4ExtCommIpNet;
    tSNMP_OCTET_STRING_TYPE Fsbgp4ExtCommPeerRemAddr;
    UINT4               u4CommIpNet = 0;
    UINT4               u4CommPeerRemAddr = 0;

    Fsbgp4ExtCommIpNet.pu1_OctetList = (UINT1 *) (VOID *) &u4CommIpNet;
    Fsbgp4ExtCommPeerRemAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4CommPeerRemAddr;

    PTR_ASSIGN4 (Fsbgp4ExtCommIpNet.pu1_OctetList, u4Fsbgp4ExtCommIpNet);
    PTR_ASSIGN4 (Fsbgp4ExtCommPeerRemAddr.pu1_OctetList,
                 u4Fsbgp4ExtCommPeerRemAddr);

    if (nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable (BGP4_INET_AFI_IPV4,
                                                            BGP4_INET_SAFI_UNICAST,
                                                            &Fsbgp4ExtCommIpNet,
                                                            i4Fsbgp4ExtCommIPPrefixLength,
                                                            BGP4_INET_AFI_IPV4,
                                                            &Fsbgp4ExtCommPeerRemAddr)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4ExtCommReceivedRouteExtCommTable
Input       :  The Indices
Fsbgp4ExtCommIpNet
Fsbgp4ExtCommIPPrefixLength
Fsbgp4ExtCommPeerRemAddr
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4ExtCommReceivedRouteExtCommTable (UINT4
                                                        *pu4Fsbgp4ExtCommIpNet,
                                                        INT4
                                                        *pi4Fsbgp4ExtCommIPPrefixLength,
                                                        UINT4
                                                        *pu4Fsbgp4ExtCommPeerRemAddr)
{
    UNUSED_PARAM(pu4Fsbgp4ExtCommIpNet);
    UNUSED_PARAM(pi4Fsbgp4ExtCommIPPrefixLength);
    UNUSED_PARAM(pu4Fsbgp4ExtCommPeerRemAddr);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4ExtCommReceivedRouteExtCommTable
Input       :  The Indices
Fsbgp4ExtCommIpNet
nextFsbgp4ExtCommIpNet
Fsbgp4ExtCommIPPrefixLength
nextFsbgp4ExtCommIPPrefixLength
Fsbgp4ExtCommPeerRemAddr
nextFsbgp4ExtCommPeerRemAddr
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4ExtCommReceivedRouteExtCommTable (UINT4
                                                       u4Fsbgp4ExtCommIpNet,
                                                       UINT4
                                                       *pu4NextFsbgp4ExtCommIpNet,
                                                       INT4
                                                       i4Fsbgp4ExtCommIPPrefixLength,
                                                       INT4
                                                       *pi4NextFsbgp4ExtCommIPPrefixLength,
                                                       UINT4
                                                       u4Fsbgp4ExtCommPeerRemAddr,
                                                       UINT4
                                                       *pu4NextFsbgp4ExtCommPeerRemAddr)
{
    UNUSED_PARAM(u4Fsbgp4ExtCommIpNet);
    UNUSED_PARAM(pu4NextFsbgp4ExtCommIpNet);
    UNUSED_PARAM(i4Fsbgp4ExtCommIPPrefixLength);
    UNUSED_PARAM(pi4NextFsbgp4ExtCommIPPrefixLength);
    UNUSED_PARAM(u4Fsbgp4ExtCommPeerRemAddr);
    UNUSED_PARAM(pu4NextFsbgp4ExtCommPeerRemAddr);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4ReceivedRouteExtCommPath
Input       :  The Indices
Fsbgp4ExtCommIpNet
Fsbgp4ExtCommIPPrefixLength
Fsbgp4ExtCommPeerRemAddr

The Object 
retValFsbgp4ReceivedRouteExtCommPath
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4ReceivedRouteExtCommPath (UINT4 u4Fsbgp4ExtCommIpNet,
                                      INT4 i4Fsbgp4ExtCommIPPrefixLength,
                                      UINT4 u4Fsbgp4ExtCommPeerRemAddr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsbgp4ReceivedRouteExtCommPath)
{
    tSNMP_OCTET_STRING_TYPE Fsbgp4ExtCommIpNet;
    tSNMP_OCTET_STRING_TYPE Fsbgp4ExtCommPeerRemAddr;
    UINT4               u4CommIpNet = 0;
    UINT4               u4CommPeerRemAddr = 0;

    Fsbgp4ExtCommIpNet.pu1_OctetList = (UINT1 *) (VOID *) &u4CommIpNet;
    Fsbgp4ExtCommPeerRemAddr.pu1_OctetList =
        (UINT1 *) (VOID *) &u4CommPeerRemAddr;

    PTR_ASSIGN4 (Fsbgp4ExtCommIpNet.pu1_OctetList, u4Fsbgp4ExtCommIpNet);
    PTR_ASSIGN4 (Fsbgp4ExtCommPeerRemAddr.pu1_OctetList,
                 u4Fsbgp4ExtCommPeerRemAddr);

    if (nmhGetFsbgp4mpebgp4PathAttrExtCommunity (BGP4_INET_AFI_IPV4,
                                                 BGP4_INET_SAFI_UNICAST,
                                                 &Fsbgp4ExtCommIpNet,
                                                 i4Fsbgp4ExtCommIPPrefixLength,
                                                 BGP4_INET_AFI_IPV4,
                                                 &Fsbgp4ExtCommPeerRemAddr,
                                                 pRetValFsbgp4ReceivedRouteExtCommPath)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4CapSupportedCapsTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4CapSupportedCapsTable
Input       :  The Indices
Fsbgp4CapPeerRemoteIpAddr
Fsbgp4SupportedCapabilityCode
Fsbgp4SupportedCapabilityInstance
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4CapSupportedCapsTable (UINT4
                                                     u4Fsbgp4CapPeerRemoteIpAddr,
                                                     INT4
                                                     i4Fsbgp4SupportedCapabilityCode,
                                                     INT4
                                                     i4Fsbgp4SupportedCapabilityInstance)
{
    UNUSED_PARAM (u4Fsbgp4CapPeerRemoteIpAddr);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityCode);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityInstance);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4CapSupportedCapsTable
Input       :  The Indices
Fsbgp4CapPeerRemoteIpAddr
Fsbgp4SupportedCapabilityCode
Fsbgp4SupportedCapabilityInstance
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4CapSupportedCapsTable (UINT4
                                             *pu4Fsbgp4CapPeerRemoteIpAddr,
                                             INT4
                                             *pi4Fsbgp4SupportedCapabilityCode,
                                             INT4
                                             *pi4Fsbgp4SupportedCapabilityInstance)
{
    UNUSED_PARAM (pu4Fsbgp4CapPeerRemoteIpAddr);
    UNUSED_PARAM (pi4Fsbgp4SupportedCapabilityCode);
    UNUSED_PARAM (pi4Fsbgp4SupportedCapabilityInstance);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4CapSupportedCapsTable
Input       :  The Indices
Fsbgp4CapPeerRemoteIpAddr
nextFsbgp4CapPeerRemoteIpAddr
Fsbgp4SupportedCapabilityCode
nextFsbgp4SupportedCapabilityCode
Fsbgp4SupportedCapabilityInstance
nextFsbgp4SupportedCapabilityInstance
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4CapSupportedCapsTable (UINT4 u4Fsbgp4CapPeerRemoteIpAddr,
                                            UINT4
                                            *pu4NextFsbgp4CapPeerRemoteIpAddr,
                                            INT4
                                            i4Fsbgp4SupportedCapabilityCode,
                                            INT4
                                            *pi4NextFsbgp4SupportedCapabilityCode,
                                            INT4
                                            i4Fsbgp4SupportedCapabilityInstance,
                                            INT4
                                            *pi4NextFsbgp4SupportedCapabilityInstance)
{
    UNUSED_PARAM (u4Fsbgp4CapPeerRemoteIpAddr);
    UNUSED_PARAM (pu4NextFsbgp4CapPeerRemoteIpAddr);
    UNUSED_PARAM (pi4NextFsbgp4SupportedCapabilityCode);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityCode);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityInstance);
    UNUSED_PARAM (pi4NextFsbgp4SupportedCapabilityInstance);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4SupportedCapabilityLength
Input       :  The Indices
Fsbgp4CapPeerRemoteIpAddr
Fsbgp4SupportedCapabilityCode
Fsbgp4SupportedCapabilityInstance

The Object 
retValFsbgp4SupportedCapabilityLength
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4SupportedCapabilityLength (UINT4 u4Fsbgp4CapPeerRemoteIpAddr,
                                       INT4 i4Fsbgp4SupportedCapabilityCode,
                                       INT4 i4Fsbgp4SupportedCapabilityInstance,
                                       INT4
                                       *pi4RetValFsbgp4SupportedCapabilityLength)
{

    UNUSED_PARAM (u4Fsbgp4CapPeerRemoteIpAddr);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityCode);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityInstance);
    UNUSED_PARAM (pi4RetValFsbgp4SupportedCapabilityLength);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4SupportedCapabilityValue
Input       :  The Indices
Fsbgp4CapPeerRemoteIpAddr
Fsbgp4SupportedCapabilityCode
Fsbgp4SupportedCapabilityInstance

The Object 
retValFsbgp4SupportedCapabilityValue
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4SupportedCapabilityValue (UINT4 u4Fsbgp4CapPeerRemoteIpAddr,
                                      INT4 i4Fsbgp4SupportedCapabilityCode,
                                      INT4 i4Fsbgp4SupportedCapabilityInstance,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsbgp4SupportedCapabilityValue)
{
    UNUSED_PARAM (u4Fsbgp4CapPeerRemoteIpAddr);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityCode);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityInstance);
    UNUSED_PARAM (pRetValFsbgp4SupportedCapabilityValue);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4CapSupportedCapsRowStatus
Input       :  The Indices
Fsbgp4CapPeerRemoteIpAddr
Fsbgp4SupportedCapabilityCode
Fsbgp4SupportedCapabilityInstance

The Object 
retValFsbgp4CapSupportedCapsRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4CapSupportedCapsRowStatus (UINT4 u4Fsbgp4CapPeerRemoteIpAddr,
                                       INT4 i4Fsbgp4SupportedCapabilityCode,
                                       INT4 i4Fsbgp4SupportedCapabilityInstance,
                                       INT4
                                       *pi4RetValFsbgp4CapSupportedCapsRowStatus)
{
    UNUSED_PARAM (u4Fsbgp4CapPeerRemoteIpAddr);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityCode);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityInstance);
    UNUSED_PARAM (pi4RetValFsbgp4CapSupportedCapsRowStatus);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4SupportedCapabilityLength
Input       :  The Indices
Fsbgp4CapPeerRemoteIpAddr
Fsbgp4SupportedCapabilityCode
Fsbgp4SupportedCapabilityInstance

The Object 
setValFsbgp4SupportedCapabilityLength
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4SupportedCapabilityLength (UINT4 u4Fsbgp4CapPeerRemoteIpAddr,
                                       INT4 i4Fsbgp4SupportedCapabilityCode,
                                       INT4 i4Fsbgp4SupportedCapabilityInstance,
                                       INT4
                                       i4SetValFsbgp4SupportedCapabilityLength)
{
    UNUSED_PARAM (u4Fsbgp4CapPeerRemoteIpAddr);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityCode);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityInstance);
    UNUSED_PARAM (i4SetValFsbgp4SupportedCapabilityLength);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4SupportedCapabilityValue
Input       :  The Indices
Fsbgp4CapPeerRemoteIpAddr
Fsbgp4SupportedCapabilityCode
Fsbgp4SupportedCapabilityInstance

The Object 
setValFsbgp4SupportedCapabilityValue
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4SupportedCapabilityValue (UINT4 u4Fsbgp4CapPeerRemoteIpAddr,
                                      INT4 i4Fsbgp4SupportedCapabilityCode,
                                      INT4 i4Fsbgp4SupportedCapabilityInstance,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSetValFsbgp4SupportedCapabilityValue)
{

    UNUSED_PARAM (u4Fsbgp4CapPeerRemoteIpAddr);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityCode);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityInstance);
    UNUSED_PARAM (pSetValFsbgp4SupportedCapabilityValue);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsbgp4CapSupportedCapsRowStatus
Input       :  The Indices
Fsbgp4CapPeerRemoteIpAddr
Fsbgp4SupportedCapabilityCode
Fsbgp4SupportedCapabilityInstance

The Object 
setValFsbgp4CapSupportedCapsRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4CapSupportedCapsRowStatus (UINT4 u4Fsbgp4CapPeerRemoteIpAddr,
                                       INT4 i4Fsbgp4SupportedCapabilityCode,
                                       INT4 i4Fsbgp4SupportedCapabilityInstance,
                                       INT4
                                       i4SetValFsbgp4CapSupportedCapsRowStatus)
{
    UNUSED_PARAM (u4Fsbgp4CapPeerRemoteIpAddr);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityCode);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityInstance);
    UNUSED_PARAM (i4SetValFsbgp4CapSupportedCapsRowStatus);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4SupportedCapabilityLength
Input       :  The Indices
Fsbgp4CapPeerRemoteIpAddr
Fsbgp4SupportedCapabilityCode
Fsbgp4SupportedCapabilityInstance

The Object 
testValFsbgp4SupportedCapabilityLength
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4SupportedCapabilityLength (UINT4 *pu4ErrorCode,
                                          UINT4 u4Fsbgp4CapPeerRemoteIpAddr,
                                          INT4 i4Fsbgp4SupportedCapabilityCode,
                                          INT4
                                          i4Fsbgp4SupportedCapabilityInstance,
                                          INT4
                                          i4TestValFsbgp4SupportedCapabilityLength)
{
    UNUSED_PARAM (u4Fsbgp4CapPeerRemoteIpAddr);
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityCode);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityInstance);
    UNUSED_PARAM (i4TestValFsbgp4SupportedCapabilityLength);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4SupportedCapabilityValue
Input       :  The Indices
Fsbgp4CapPeerRemoteIpAddr
Fsbgp4SupportedCapabilityCode
Fsbgp4SupportedCapabilityInstance

The Object 
testValFsbgp4SupportedCapabilityValue
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4SupportedCapabilityValue (UINT4 *pu4ErrorCode,
                                         UINT4 u4Fsbgp4CapPeerRemoteIpAddr,
                                         INT4 i4Fsbgp4SupportedCapabilityCode,
                                         INT4
                                         i4Fsbgp4SupportedCapabilityInstance,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pTestValFsbgp4SupportedCapabilityValue)
{
    UNUSED_PARAM (u4Fsbgp4CapPeerRemoteIpAddr);
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityCode);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityInstance);
    UNUSED_PARAM (pTestValFsbgp4SupportedCapabilityValue);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Fsbgp4CapSupportedCapsRowStatus
Input       :  The Indices
Fsbgp4CapPeerRemoteIpAddr
Fsbgp4SupportedCapabilityCode
Fsbgp4SupportedCapabilityInstance

The Object 
testValFsbgp4CapSupportedCapsRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4CapSupportedCapsRowStatus (UINT4 *pu4ErrorCode,
                                          UINT4 u4Fsbgp4CapPeerRemoteIpAddr,
                                          INT4 i4Fsbgp4SupportedCapabilityCode,
                                          INT4
                                          i4Fsbgp4SupportedCapabilityInstance,
                                          INT4
                                          i4TestValFsbgp4CapSupportedCapsRowStatus)
{
    UNUSED_PARAM (u4Fsbgp4CapPeerRemoteIpAddr);
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityCode);
    UNUSED_PARAM (i4Fsbgp4SupportedCapabilityInstance);
    UNUSED_PARAM (i4TestValFsbgp4CapSupportedCapsRowStatus);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4CapSupportedCapsTable
Input       :  The Indices
Fsbgp4CapPeerRemoteIpAddr
Fsbgp4SupportedCapabilityCode
Fsbgp4SupportedCapabilityInstance
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4CapSupportedCapsTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4StrictCapabilityMatchTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4StrictCapabilityMatchTable
Input       :  The Indices
Fsbgp4PeerRemIpAddr
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4StrictCapabilityMatchTable (UINT4
                                                          u4Fsbgp4PeerRemIpAddr)
{
    UNUSED_PARAM (u4Fsbgp4PeerRemIpAddr);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4StrictCapabilityMatchTable
Input       :  The Indices
Fsbgp4PeerRemIpAddr
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4StrictCapabilityMatchTable (UINT4 *pu4Fsbgp4PeerRemIpAddr)
{
    UNUSED_PARAM (pu4Fsbgp4PeerRemIpAddr);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4StrictCapabilityMatchTable
Input       :  The Indices
Fsbgp4PeerRemIpAddr
nextFsbgp4PeerRemIpAddr
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4StrictCapabilityMatchTable (UINT4 u4Fsbgp4PeerRemIpAddr,
                                                 UINT4
                                                 *pu4NextFsbgp4PeerRemIpAddr)
{
    UNUSED_PARAM (u4Fsbgp4PeerRemIpAddr);
    UNUSED_PARAM (pu4NextFsbgp4PeerRemIpAddr);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4StrictCapabilityMatch
Input       :  The Indices
Fsbgp4PeerRemIpAddr

The Object 
retValFsbgp4StrictCapabilityMatch
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4StrictCapabilityMatch (UINT4 u4Fsbgp4PeerRemIpAddr,
                                   INT4 *pi4RetValFsbgp4StrictCapabilityMatch)
{
    UNUSED_PARAM (u4Fsbgp4PeerRemIpAddr);
    UNUSED_PARAM (pi4RetValFsbgp4StrictCapabilityMatch);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4StrictCapabilityMatch
Input       :  The Indices
Fsbgp4PeerRemIpAddr

The Object 
setValFsbgp4StrictCapabilityMatch
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4StrictCapabilityMatch (UINT4 u4Fsbgp4PeerRemIpAddr,
                                   INT4 i4SetValFsbgp4StrictCapabilityMatch)
{
    UNUSED_PARAM (u4Fsbgp4PeerRemIpAddr);
    UNUSED_PARAM (i4SetValFsbgp4StrictCapabilityMatch);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4StrictCapabilityMatch
Input       :  The Indices
Fsbgp4PeerRemIpAddr

The Object 
testValFsbgp4StrictCapabilityMatch
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4StrictCapabilityMatch (UINT4 *pu4ErrorCode,
                                      UINT4 u4Fsbgp4PeerRemIpAddr,
                                      INT4 i4TestValFsbgp4StrictCapabilityMatch)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Fsbgp4PeerRemIpAddr);
    UNUSED_PARAM (i4TestValFsbgp4StrictCapabilityMatch);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4StrictCapabilityMatchTable
Input       :  The Indices
Fsbgp4PeerRemIpAddr
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4StrictCapabilityMatchTable (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4CapsAnnouncedTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4CapsAnnouncedTable
Input       :  The Indices
Fsbgp4PeerIpAddr
Fsbgp4PeerCapAnnouncedCode
Fsbgp4PeerCapAnnouncedInstance
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4CapsAnnouncedTable (UINT4 u4Fsbgp4PeerIpAddr,
                                                  INT4
                                                  i4Fsbgp4PeerCapAnnouncedCode,
                                                  INT4
                                                  i4Fsbgp4PeerCapAnnouncedInstance)
{
    UNUSED_PARAM (u4Fsbgp4PeerIpAddr);
    UNUSED_PARAM (i4Fsbgp4PeerCapAnnouncedCode);
    UNUSED_PARAM (i4Fsbgp4PeerCapAnnouncedInstance);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4CapsAnnouncedTable
Input       :  The Indices
Fsbgp4PeerIpAddr
Fsbgp4PeerCapAnnouncedCode
Fsbgp4PeerCapAnnouncedInstance
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4CapsAnnouncedTable (UINT4 *pu4Fsbgp4PeerIpAddr,
                                          INT4 *pi4Fsbgp4PeerCapAnnouncedCode,
                                          INT4
                                          *pi4Fsbgp4PeerCapAnnouncedInstance)
{
    UNUSED_PARAM (pu4Fsbgp4PeerIpAddr);
    UNUSED_PARAM (pi4Fsbgp4PeerCapAnnouncedCode);
    UNUSED_PARAM (pi4Fsbgp4PeerCapAnnouncedInstance);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4CapsAnnouncedTable
Input       :  The Indices
Fsbgp4PeerIpAddr
nextFsbgp4PeerIpAddr
Fsbgp4PeerCapAnnouncedCode
nextFsbgp4PeerCapAnnouncedCode
Fsbgp4PeerCapAnnouncedInstance
nextFsbgp4PeerCapAnnouncedInstance
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4CapsAnnouncedTable (UINT4 u4Fsbgp4PeerIpAddr,
                                         UINT4 *pu4NextFsbgp4PeerIpAddr,
                                         INT4 i4Fsbgp4PeerCapAnnouncedCode,
                                         INT4
                                         *pi4NextFsbgp4PeerCapAnnouncedCode,
                                         INT4 i4Fsbgp4PeerCapAnnouncedInstance,
                                         INT4
                                         *pi4NextFsbgp4PeerCapAnnouncedInstance)
{
    UNUSED_PARAM (u4Fsbgp4PeerIpAddr);
    UNUSED_PARAM (pu4NextFsbgp4PeerIpAddr);
    UNUSED_PARAM (i4Fsbgp4PeerCapAnnouncedCode);
    UNUSED_PARAM (pi4NextFsbgp4PeerCapAnnouncedCode);
    UNUSED_PARAM (i4Fsbgp4PeerCapAnnouncedInstance);
    UNUSED_PARAM (pi4NextFsbgp4PeerCapAnnouncedInstance);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4PeerCapAnnouncedLength
Input       :  The Indices
Fsbgp4PeerIpAddr
Fsbgp4PeerCapAnnouncedCode
Fsbgp4PeerCapAnnouncedInstance

The Object 
retValFsbgp4PeerCapAnnouncedLength
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4PeerCapAnnouncedLength (UINT4 u4Fsbgp4PeerIpAddr,
                                    INT4 i4Fsbgp4PeerCapAnnouncedCode,
                                    INT4 i4Fsbgp4PeerCapAnnouncedInstance,
                                    INT4 *pi4RetValFsbgp4PeerCapAnnouncedLength)
{
    UNUSED_PARAM (u4Fsbgp4PeerIpAddr);
    UNUSED_PARAM (i4Fsbgp4PeerCapAnnouncedCode);
    UNUSED_PARAM (i4Fsbgp4PeerCapAnnouncedInstance);
    UNUSED_PARAM (pi4RetValFsbgp4PeerCapAnnouncedLength);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4PeerCapAnnouncedValue
Input       :  The Indices
Fsbgp4PeerIpAddr
Fsbgp4PeerCapAnnouncedCode
Fsbgp4PeerCapAnnouncedInstance

The Object 
retValFsbgp4PeerCapAnnouncedValue
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4PeerCapAnnouncedValue (UINT4 u4Fsbgp4PeerIpAddr,
                                   INT4 i4Fsbgp4PeerCapAnnouncedCode,
                                   INT4 i4Fsbgp4PeerCapAnnouncedInstance,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsbgp4PeerCapAnnouncedValue)
{
    UNUSED_PARAM (u4Fsbgp4PeerIpAddr);
    UNUSED_PARAM (i4Fsbgp4PeerCapAnnouncedCode);
    UNUSED_PARAM (i4Fsbgp4PeerCapAnnouncedInstance);
    UNUSED_PARAM (pRetValFsbgp4PeerCapAnnouncedValue);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Fsbgp4CapReceivedCapsTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4CapReceivedCapsTable
Input       :  The Indices
Fsbgp4PeerRemoteAddress
Fsbgp4PeerCapReceivedCode
Fsbgp4PeerCapReceivedInstance
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4CapReceivedCapsTable (UINT4
                                                    u4Fsbgp4PeerRemoteAddress,
                                                    INT4
                                                    i4Fsbgp4PeerCapReceivedCode,
                                                    INT4
                                                    i4Fsbgp4PeerCapReceivedInstance)
{
    UNUSED_PARAM (u4Fsbgp4PeerRemoteAddress);
    UNUSED_PARAM (i4Fsbgp4PeerCapReceivedCode);
    UNUSED_PARAM (i4Fsbgp4PeerCapReceivedInstance);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4CapReceivedCapsTable
Input       :  The Indices
Fsbgp4PeerRemoteAddress
Fsbgp4PeerCapReceivedCode
Fsbgp4PeerCapReceivedInstance
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4CapReceivedCapsTable (UINT4 *pu4Fsbgp4PeerRemoteAddress,
                                            INT4 *pi4Fsbgp4PeerCapReceivedCode,
                                            INT4
                                            *pi4Fsbgp4PeerCapReceivedInstance)
{
    UNUSED_PARAM (pu4Fsbgp4PeerRemoteAddress);
    UNUSED_PARAM (pi4Fsbgp4PeerCapReceivedCode);
    UNUSED_PARAM (pi4Fsbgp4PeerCapReceivedInstance);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4CapReceivedCapsTable
Input       :  The Indices
Fsbgp4PeerRemoteAddress
nextFsbgp4PeerRemoteAddress
Fsbgp4PeerCapReceivedCode
nextFsbgp4PeerCapReceivedCode
Fsbgp4PeerCapReceivedInstance
nextFsbgp4PeerCapReceivedInstance
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4CapReceivedCapsTable (UINT4 u4Fsbgp4PeerRemoteAddress,
                                           UINT4
                                           *pu4NextFsbgp4PeerRemoteAddress,
                                           INT4 i4Fsbgp4PeerCapReceivedCode,
                                           INT4
                                           *pi4NextFsbgp4PeerCapReceivedCode,
                                           INT4 i4Fsbgp4PeerCapReceivedInstance,
                                           INT4
                                           *pi4NextFsbgp4PeerCapReceivedInstance)
{
    UNUSED_PARAM (u4Fsbgp4PeerRemoteAddress);
    UNUSED_PARAM (i4Fsbgp4PeerCapReceivedCode);
    UNUSED_PARAM (i4Fsbgp4PeerCapReceivedInstance);
    UNUSED_PARAM (pu4NextFsbgp4PeerRemoteAddress);
    UNUSED_PARAM (pi4NextFsbgp4PeerCapReceivedCode);
    UNUSED_PARAM (pi4NextFsbgp4PeerCapReceivedInstance);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4PeerCapReceivedLength
Input       :  The Indices
Fsbgp4PeerRemoteAddress
Fsbgp4PeerCapReceivedCode
Fsbgp4PeerCapReceivedInstance

The Object 
retValFsbgp4PeerCapReceivedLength
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4PeerCapReceivedLength (UINT4 u4Fsbgp4PeerRemoteAddress,
                                   INT4 i4Fsbgp4PeerCapReceivedCode,
                                   INT4 i4Fsbgp4PeerCapReceivedInstance,
                                   INT4 *pi4RetValFsbgp4PeerCapReceivedLength)
{
    UNUSED_PARAM (u4Fsbgp4PeerRemoteAddress);
    UNUSED_PARAM (i4Fsbgp4PeerCapReceivedCode);
    UNUSED_PARAM (i4Fsbgp4PeerCapReceivedInstance);
    UNUSED_PARAM (pi4RetValFsbgp4PeerCapReceivedLength);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4PeerCapReceivedValue
Input       :  The Indices
Fsbgp4PeerRemoteAddress
Fsbgp4PeerCapReceivedCode
Fsbgp4PeerCapReceivedInstance

The Object 
retValFsbgp4PeerCapReceivedValue
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4PeerCapReceivedValue (UINT4 u4Fsbgp4PeerRemoteAddress,
                                  INT4 i4Fsbgp4PeerCapReceivedCode,
                                  INT4 i4Fsbgp4PeerCapReceivedInstance,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsbgp4PeerCapReceivedValue)
{
    UNUSED_PARAM (u4Fsbgp4PeerRemoteAddress);
    UNUSED_PARAM (i4Fsbgp4PeerCapReceivedCode);
    UNUSED_PARAM (i4Fsbgp4PeerCapReceivedInstance);
    UNUSED_PARAM (pRetValFsbgp4PeerCapReceivedValue);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Fsbgp4CapAcceptedCapsTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4CapAcceptedCapsTable
Input       :  The Indices
Fsbgp4CapAcceptedPeerRemAddr
Fsbgp4PeerCapAcceptedCode
Fsbgp4PeerCapAcceptedInstance
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4CapAcceptedCapsTable (UINT4
                                                    u4Fsbgp4CapAcceptedPeerRemAddr,
                                                    INT4
                                                    i4Fsbgp4PeerCapAcceptedCode,
                                                    INT4
                                                    i4Fsbgp4PeerCapAcceptedInstance)
{
    UNUSED_PARAM (u4Fsbgp4CapAcceptedPeerRemAddr);
    UNUSED_PARAM (i4Fsbgp4PeerCapAcceptedCode);
    UNUSED_PARAM (i4Fsbgp4PeerCapAcceptedInstance);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4CapAcceptedCapsTable
Input       :  The Indices
Fsbgp4CapAcceptedPeerRemAddr
Fsbgp4PeerCapAcceptedCode
Fsbgp4PeerCapAcceptedInstance
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4CapAcceptedCapsTable (UINT4
                                            *pu4Fsbgp4CapAcceptedPeerRemAddr,
                                            INT4 *pi4Fsbgp4PeerCapAcceptedCode,
                                            INT4
                                            *pi4Fsbgp4PeerCapAcceptedInstance)
{
    UNUSED_PARAM (pu4Fsbgp4CapAcceptedPeerRemAddr);
    UNUSED_PARAM (pi4Fsbgp4PeerCapAcceptedCode);
    UNUSED_PARAM (pi4Fsbgp4PeerCapAcceptedInstance);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4CapAcceptedCapsTable
Input       :  The Indices
Fsbgp4CapAcceptedPeerRemAddr
nextFsbgp4CapAcceptedPeerRemAddr
Fsbgp4PeerCapAcceptedCode
nextFsbgp4PeerCapAcceptedCode
Fsbgp4PeerCapAcceptedInstance
nextFsbgp4PeerCapAcceptedInstance
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4CapAcceptedCapsTable (UINT4 u4Fsbgp4CapAcceptedPeerRemAddr,
                                           UINT4
                                           *pu4NextFsbgp4CapAcceptedPeerRemAddr,
                                           INT4 i4Fsbgp4PeerCapAcceptedCode,
                                           INT4
                                           *pi4NextFsbgp4PeerCapAcceptedCode,
                                           INT4 i4Fsbgp4PeerCapAcceptedInstance,
                                           INT4
                                           *pi4NextFsbgp4PeerCapAcceptedInstance)
{
    UNUSED_PARAM (u4Fsbgp4CapAcceptedPeerRemAddr);
    UNUSED_PARAM (pu4NextFsbgp4CapAcceptedPeerRemAddr);
    UNUSED_PARAM (i4Fsbgp4PeerCapAcceptedCode);
    UNUSED_PARAM (pi4NextFsbgp4PeerCapAcceptedCode);
    UNUSED_PARAM (i4Fsbgp4PeerCapAcceptedInstance);
    UNUSED_PARAM (pi4NextFsbgp4PeerCapAcceptedInstance);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4PeerCapAcceptedLength
Input       :  The Indices
Fsbgp4CapAcceptedPeerRemAddr
Fsbgp4PeerCapAcceptedCode
Fsbgp4PeerCapAcceptedInstance

The Object 
retValFsbgp4PeerCapAcceptedLength
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4PeerCapAcceptedLength (UINT4 u4Fsbgp4CapAcceptedPeerRemAddr,
                                   INT4 i4Fsbgp4PeerCapAcceptedCode,
                                   INT4 i4Fsbgp4PeerCapAcceptedInstance,
                                   INT4 *pi4RetValFsbgp4PeerCapAcceptedLength)
{
    UNUSED_PARAM (u4Fsbgp4CapAcceptedPeerRemAddr);
    UNUSED_PARAM (i4Fsbgp4PeerCapAcceptedCode);
    UNUSED_PARAM (i4Fsbgp4PeerCapAcceptedInstance);
    UNUSED_PARAM (pi4RetValFsbgp4PeerCapAcceptedLength);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4PeerCapAcceptedValue
Input       :  The Indices
Fsbgp4CapAcceptedPeerRemAddr
Fsbgp4PeerCapAcceptedCode
Fsbgp4PeerCapAcceptedInstance

The Object 
retValFsbgp4PeerCapAcceptedValue
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4PeerCapAcceptedValue (UINT4 u4Fsbgp4CapAcceptedPeerRemAddr,
                                  INT4 i4Fsbgp4PeerCapAcceptedCode,
                                  INT4 i4Fsbgp4PeerCapAcceptedInstance,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsbgp4PeerCapAcceptedValue)
{
    UNUSED_PARAM (u4Fsbgp4CapAcceptedPeerRemAddr);
    UNUSED_PARAM (i4Fsbgp4PeerCapAcceptedCode);
    UNUSED_PARAM (i4Fsbgp4PeerCapAcceptedInstance);
    UNUSED_PARAM (pRetValFsbgp4PeerCapAcceptedValue);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4RtRefreshAllPeerInboundRequest
Input       :  The Indices

The Object 
retValFsbgp4RtRefreshAllPeerInboundRequest
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RtRefreshAllPeerInboundRequest (INT4
                                            *pi4RetValFsbgp4RtRefreshAllPeerInboundRequest)
{
    /* currently not supported */
    *pi4RetValFsbgp4RtRefreshAllPeerInboundRequest = 0;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4RtRefreshAllPeerInboundRequest
Input       :  The Indices

The Object 
setValFsbgp4RtRefreshAllPeerInboundRequest
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4RtRefreshAllPeerInboundRequest (INT4
                                            i4SetValFsbgp4RtRefreshAllPeerInboundRequest)
{
    UNUSED_PARAM (i4SetValFsbgp4RtRefreshAllPeerInboundRequest);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4RtRefreshAllPeerInboundRequest
Input       :  The Indices

The Object 
testValFsbgp4RtRefreshAllPeerInboundRequest
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4RtRefreshAllPeerInboundRequest (UINT4 *pu4ErrorCode,
                                               INT4
                                               i4TestValFsbgp4RtRefreshAllPeerInboundRequest)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsbgp4RtRefreshAllPeerInboundRequest);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4RtRefreshAllPeerInboundRequest
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4RtRefreshAllPeerInboundRequest (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Fsbgp4RtRefreshInboundTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4RtRefreshInboundTable
Input       :  The Indices
Fsbgp4RtRefreshInboundPeerType
Fsbgp4RtRefreshInboundPeerAddr
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4RtRefreshInboundTable (INT4
                                                     i4Fsbgp4RtRefreshInboundPeerType,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pFsbgp4RtRefreshInboundPeerAddr)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRtRefreshInboundTable
        (i4Fsbgp4RtRefreshInboundPeerType, pFsbgp4RtRefreshInboundPeerAddr,
         BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4RtRefreshInboundTable
Input       :  The Indices
Fsbgp4RtRefreshInboundPeerType
Fsbgp4RtRefreshInboundPeerAddr
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4RtRefreshInboundTable (INT4
                                             *pi4Fsbgp4RtRefreshInboundPeerType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsbgp4RtRefreshInboundPeerAddr)
{
    INT4                i4Afi = 0;
    INT4                i4Safi = 0;
    return (nmhGetFirstIndexFsbgp4MpeRtRefreshInboundTable
            (pi4Fsbgp4RtRefreshInboundPeerType, pFsbgp4RtRefreshInboundPeerAddr,
             &i4Afi, &i4Safi));

}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4RtRefreshInboundTable
Input       :  The Indices
Fsbgp4RtRefreshInboundPeerType
nextFsbgp4RtRefreshInboundPeerType
Fsbgp4RtRefreshInboundPeerAddr
nextFsbgp4RtRefreshInboundPeerAddr
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4RtRefreshInboundTable (INT4
                                            i4Fsbgp4RtRefreshInboundPeerType,
                                            INT4
                                            *pi4NextFsbgp4RtRefreshInboundPeerType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsbgp4RtRefreshInboundPeerAddr,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pNextFsbgp4RtRefreshInboundPeerAddr)
{
    INT4                i4Afi = 0;
    INT4                i4Safi = 0;
    return (nmhGetNextIndexFsbgp4MpeRtRefreshInboundTable
            (i4Fsbgp4RtRefreshInboundPeerType,
             pi4NextFsbgp4RtRefreshInboundPeerType,
             pFsbgp4RtRefreshInboundPeerAddr,
             pNextFsbgp4RtRefreshInboundPeerAddr, i4Afi, &i4Afi, i4Safi,
             &i4Safi));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4RtRefreshInboundRequest
Input       :  The Indices
Fsbgp4RtRefreshInboundPeerType
Fsbgp4RtRefreshInboundPeerAddr

The Object 
retValFsbgp4RtRefreshInboundRequest
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RtRefreshInboundRequest (INT4 i4Fsbgp4RtRefreshInboundPeerType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsbgp4RtRefreshInboundPeerAddr,
                                     INT4
                                     *pi4RetValFsbgp4RtRefreshInboundRequest)
{
    if (nmhGetFsbgp4mpeRtRefreshInboundRequest
        (i4Fsbgp4RtRefreshInboundPeerType, pFsbgp4RtRefreshInboundPeerAddr,
         BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST,
         pi4RetValFsbgp4RtRefreshInboundRequest) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4RtRefreshInboundRequest
Input       :  The Indices
Fsbgp4RtRefreshInboundPeerType
Fsbgp4RtRefreshInboundPeerAddr

The Object 
setValFsbgp4RtRefreshInboundRequest
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4RtRefreshInboundRequest (INT4 i4Fsbgp4RtRefreshInboundPeerType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsbgp4RtRefreshInboundPeerAddr,
                                     INT4 i4SetValFsbgp4RtRefreshInboundRequest)
{

    if (nmhSetFsbgp4mpeRtRefreshInboundRequest
        (i4Fsbgp4RtRefreshInboundPeerType, pFsbgp4RtRefreshInboundPeerAddr,
         BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST,
         i4SetValFsbgp4RtRefreshInboundRequest) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4RtRefreshInboundRequest
Input       :  The Indices
Fsbgp4RtRefreshInboundPeerType
Fsbgp4RtRefreshInboundPeerAddr

The Object 
testValFsbgp4RtRefreshInboundRequest
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4RtRefreshInboundRequest (UINT4 *pu4ErrorCode,
                                        INT4 i4Fsbgp4RtRefreshInboundPeerType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsbgp4RtRefreshInboundPeerAddr,
                                        INT4
                                        i4TestValFsbgp4RtRefreshInboundRequest)
{
    if (nmhTestv2Fsbgp4mpeRtRefreshInboundRequest (pu4ErrorCode,
                                                   i4Fsbgp4RtRefreshInboundPeerType,
                                                   pFsbgp4RtRefreshInboundPeerAddr,
                                                   BGP4_INET_AFI_IPV4,
                                                   BGP4_INET_SAFI_UNICAST,
                                                   i4TestValFsbgp4RtRefreshInboundRequest)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4RtRefreshInboundTable
Input       :  The Indices
Fsbgp4RtRefreshInboundPeerType
Fsbgp4RtRefreshInboundPeerAddr
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4RtRefreshInboundTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4RtRefreshStatisticsTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4RtRefreshStatisticsTable
Input       :  The Indices
Fsbgp4RtRefreshStatisticsPeerType
Fsbgp4RtRefreshStatisticsPeerAddr
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4RtRefreshStatisticsTable (INT4
                                                        i4Fsbgp4RtRefreshStatisticsPeerType,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pFsbgp4RtRefreshStatisticsPeerAddr)
{
    if (nmhValidateIndexInstanceFsbgp4MpeRtRefreshStatisticsTable
        (i4Fsbgp4RtRefreshStatisticsPeerType,
         pFsbgp4RtRefreshStatisticsPeerAddr, BGP4_INET_AFI_IPV4,
         BGP4_INET_SAFI_UNICAST) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4RtRefreshStatisticsTable
Input       :  The Indices
Fsbgp4RtRefreshStatisticsPeerType
Fsbgp4RtRefreshStatisticsPeerAddr
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4RtRefreshStatisticsTable (INT4
                                                *pi4Fsbgp4RtRefreshStatisticsPeerType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsbgp4RtRefreshStatisticsPeerAddr)
{
    INT4                i4Afi;
    INT4                i4Safi;

    if (nmhGetFirstIndexFsbgp4MpeRtRefreshStatisticsTable
        (pi4Fsbgp4RtRefreshStatisticsPeerType,
         pFsbgp4RtRefreshStatisticsPeerAddr, &i4Afi, &i4Safi) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4RtRefreshStatisticsTable
Input       :  The Indices
Fsbgp4RtRefreshStatisticsPeerType
nextFsbgp4RtRefreshStatisticsPeerType
Fsbgp4RtRefreshStatisticsPeerAddr
nextFsbgp4RtRefreshStatisticsPeerAddr
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4RtRefreshStatisticsTable (INT4
                                               i4Fsbgp4RtRefreshStatisticsPeerType,
                                               INT4
                                               *pi4NextFsbgp4RtRefreshStatisticsPeerType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsbgp4RtRefreshStatisticsPeerAddr,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pNextFsbgp4RtRefreshStatisticsPeerAddr)
{
    INT4                i4Afi = BGP4_INET_AFI_IPV4;
    INT4                i4Safi = BGP4_INET_SAFI_UNICAST;

    if (nmhGetNextIndexFsbgp4MpeRtRefreshStatisticsTable
        (i4Fsbgp4RtRefreshStatisticsPeerType,
         pi4NextFsbgp4RtRefreshStatisticsPeerType,
         pFsbgp4RtRefreshStatisticsPeerAddr,
         pNextFsbgp4RtRefreshStatisticsPeerAddr, BGP4_INET_AFI_IPV4, &i4Afi,
         BGP4_INET_SAFI_UNICAST, &i4Safi) == SNMP_SUCCESS)
    {
        do
        {
            if (MEMCMP (pFsbgp4RtRefreshStatisticsPeerAddr->pu1_OctetList,
                        pNextFsbgp4RtRefreshStatisticsPeerAddr->pu1_OctetList,
                        pFsbgp4RtRefreshStatisticsPeerAddr->i4_Length) != 0)
            {
                return SNMP_SUCCESS;
            }
        }
        while (nmhGetNextIndexFsbgp4MpeRtRefreshStatisticsTable
               (*pi4NextFsbgp4RtRefreshStatisticsPeerType,
                pi4NextFsbgp4RtRefreshStatisticsPeerType,
                pNextFsbgp4RtRefreshStatisticsPeerAddr,
                pNextFsbgp4RtRefreshStatisticsPeerAddr, i4Afi, &i4Afi, i4Safi,
                &i4Safi) == SNMP_SUCCESS);
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4RtRefreshStatisticsRtRefMsgSentCntr
Input       :  The Indices
Fsbgp4RtRefreshStatisticsPeerType
Fsbgp4RtRefreshStatisticsPeerAddr

The Object 
retValFsbgp4RtRefreshStatisticsRtRefMsgSentCntr
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RtRefreshStatisticsRtRefMsgSentCntr (INT4
                                                 i4Fsbgp4RtRefreshStatisticsPeerType,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pFsbgp4RtRefreshStatisticsPeerAddr,
                                                 UINT4
                                                 *pu4RetValFsbgp4RtRefreshStatisticsRtRefMsgSentCntr)
{
    if (nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgSentCntr
        (i4Fsbgp4RtRefreshStatisticsPeerType,
         pFsbgp4RtRefreshStatisticsPeerAddr, BGP4_INET_AFI_IPV4,
         BGP4_INET_SAFI_UNICAST,
         pu4RetValFsbgp4RtRefreshStatisticsRtRefMsgSentCntr) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetFsbgp4RtRefreshStatisticsRtRefMsgTxErrCntr
Input       :  The Indices
Fsbgp4RtRefreshStatisticsPeerType
Fsbgp4RtRefreshStatisticsPeerAddr

The Object 
retValFsbgp4RtRefreshStatisticsRtRefMsgTxErrCntr
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RtRefreshStatisticsRtRefMsgTxErrCntr (INT4
                                                  i4Fsbgp4RtRefreshStatisticsPeerType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsbgp4RtRefreshStatisticsPeerAddr,
                                                  UINT4
                                                  *pu4RetValFsbgp4RtRefreshStatisticsRtRefMsgTxErrCntr)
{
    if (nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgTxErrCntr
        (i4Fsbgp4RtRefreshStatisticsPeerType,
         pFsbgp4RtRefreshStatisticsPeerAddr, BGP4_INET_AFI_IPV4,
         BGP4_INET_SAFI_UNICAST,
         pu4RetValFsbgp4RtRefreshStatisticsRtRefMsgTxErrCntr) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4RtRefreshStatisticsRtRefMsgRcvdCntr
Input       :  The Indices
Fsbgp4RtRefreshStatisticsPeerType
Fsbgp4RtRefreshStatisticsPeerAddr

The Object 
retValFsbgp4RtRefreshStatisticsRtRefMsgRcvdCntr
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RtRefreshStatisticsRtRefMsgRcvdCntr (INT4
                                                 i4Fsbgp4RtRefreshStatisticsPeerType,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pFsbgp4RtRefreshStatisticsPeerAddr,
                                                 UINT4
                                                 *pu4RetValFsbgp4RtRefreshStatisticsRtRefMsgRcvdCntr)
{
    if (nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgRcvdCntr
        (i4Fsbgp4RtRefreshStatisticsPeerType,
         pFsbgp4RtRefreshStatisticsPeerAddr, BGP4_INET_AFI_IPV4,
         BGP4_INET_SAFI_UNICAST,
         pu4RetValFsbgp4RtRefreshStatisticsRtRefMsgRcvdCntr) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsbgp4RtRefreshStatisticsRtRefMsgInvalidCntr
Input       :  The Indices
Fsbgp4RtRefreshStatisticsPeerType
Fsbgp4RtRefreshStatisticsPeerAddr

The Object 
retValFsbgp4RtRefreshStatisticsRtRefMsgInvalidCntr
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4RtRefreshStatisticsRtRefMsgInvalidCntr (INT4
                                                    i4Fsbgp4RtRefreshStatisticsPeerType,
                                                    tSNMP_OCTET_STRING_TYPE *
                                                    pFsbgp4RtRefreshStatisticsPeerAddr,
                                                    UINT4
                                                    *pu4RetValFsbgp4RtRefreshStatisticsRtRefMsgInvalidCntr)
{
    if (nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgInvalidCntr
        (i4Fsbgp4RtRefreshStatisticsPeerType,
         pFsbgp4RtRefreshStatisticsPeerAddr, BGP4_INET_AFI_IPV4,
         BGP4_INET_SAFI_UNICAST,
         pu4RetValFsbgp4RtRefreshStatisticsRtRefMsgInvalidCntr) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4SoftReconfigAllPeerOutboundRequest
Input       :  The Indices

The Object 
retValFsbgp4SoftReconfigAllPeerOutboundRequest
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4SoftReconfigAllPeerOutboundRequest (INT4
                                                *pi4RetValFsbgp4SoftReconfigAllPeerOutboundRequest)
{
    /* currently not supported */
    *pi4RetValFsbgp4SoftReconfigAllPeerOutboundRequest = 0;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4SoftReconfigAllPeerOutboundRequest
Input       :  The Indices

The Object 
setValFsbgp4SoftReconfigAllPeerOutboundRequest
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4SoftReconfigAllPeerOutboundRequest (INT4
                                                i4SetValFsbgp4SoftReconfigAllPeerOutboundRequest)
{
    UNUSED_PARAM (i4SetValFsbgp4SoftReconfigAllPeerOutboundRequest);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4SoftReconfigAllPeerOutboundRequest
Input       :  The Indices

The Object 
testValFsbgp4SoftReconfigAllPeerOutboundRequest
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4SoftReconfigAllPeerOutboundRequest (UINT4 *pu4ErrorCode,
                                                   INT4
                                                   i4TestValFsbgp4SoftReconfigAllPeerOutboundRequest)
{
    UNUSED_PARAM (i4TestValFsbgp4SoftReconfigAllPeerOutboundRequest);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_FAILURE;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4SoftReconfigAllPeerOutboundRequest
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4SoftReconfigAllPeerOutboundRequest (UINT4 *pu4ErrorCode,
                                                  tSnmpIndexList *
                                                  pSnmpIndexList,
                                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Fsbgp4SoftReconfigOutboundTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsbgp4SoftReconfigOutboundTable
Input       :  The Indices
Fsbgp4SoftReconfigOutboundPeerType
Fsbgp4SoftReconfigOutboundPeerAddr
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4SoftReconfigOutboundTable (INT4
                                                         i4Fsbgp4SoftReconfigOutboundPeerType,
                                                         tSNMP_OCTET_STRING_TYPE
                                                         *
                                                         pFsbgp4SoftReconfigOutboundPeerAddr)
{
    if (nmhValidateIndexInstanceFsbgp4MpeSoftReconfigOutboundTable
        (i4Fsbgp4SoftReconfigOutboundPeerType,
         pFsbgp4SoftReconfigOutboundPeerAddr, BGP4_INET_AFI_IPV4,
         BGP4_INET_SAFI_UNICAST) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsbgp4SoftReconfigOutboundTable
Input       :  The Indices
Fsbgp4SoftReconfigOutboundPeerType
Fsbgp4SoftReconfigOutboundPeerAddr
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4SoftReconfigOutboundTable (INT4
                                                 *pi4Fsbgp4SoftReconfigOutboundPeerType,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pFsbgp4SoftReconfigOutboundPeerAddr)
{
    INT4                i4Afi = 0;
    INT4                i4Safi = 0;
    return (nmhGetFirstIndexFsbgp4MpeSoftReconfigOutboundTable
            (pi4Fsbgp4SoftReconfigOutboundPeerType,
             pFsbgp4SoftReconfigOutboundPeerAddr, &i4Afi, &i4Safi));
}

/****************************************************************************
Function    :  nmhGetNextIndexFsbgp4SoftReconfigOutboundTable
Input       :  The Indices
Fsbgp4SoftReconfigOutboundPeerType
nextFsbgp4SoftReconfigOutboundPeerType
Fsbgp4SoftReconfigOutboundPeerAddr
nextFsbgp4SoftReconfigOutboundPeerAddr
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4SoftReconfigOutboundTable (INT4
                                                i4Fsbgp4SoftReconfigOutboundPeerType,
                                                INT4
                                                *pi4NextFsbgp4SoftReconfigOutboundPeerType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsbgp4SoftReconfigOutboundPeerAddr,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pNextFsbgp4SoftReconfigOutboundPeerAddr)
{
    INT4                i4Afi = 0;
    INT4                i4Safi = 0;
    return (nmhGetNextIndexFsbgp4MpeSoftReconfigOutboundTable
            (i4Fsbgp4SoftReconfigOutboundPeerType,
             pi4NextFsbgp4SoftReconfigOutboundPeerType,
             pFsbgp4SoftReconfigOutboundPeerAddr,
             pNextFsbgp4SoftReconfigOutboundPeerAddr, i4Afi, &i4Afi, i4Safi,
             &i4Safi));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsbgp4SoftReconfigOutboundRequest
Input       :  The Indices
Fsbgp4SoftReconfigOutboundPeerType
Fsbgp4SoftReconfigOutboundPeerAddr

The Object 
retValFsbgp4SoftReconfigOutboundRequest
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsbgp4SoftReconfigOutboundRequest (INT4
                                         i4Fsbgp4SoftReconfigOutboundPeerType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsbgp4SoftReconfigOutboundPeerAddr,
                                         INT4
                                         *pi4RetValFsbgp4SoftReconfigOutboundRequest)
{
    if (nmhGetFsbgp4mpeSoftReconfigOutboundRequest
        (i4Fsbgp4SoftReconfigOutboundPeerType,
         pFsbgp4SoftReconfigOutboundPeerAddr, BGP4_INET_AFI_IPV4,
         BGP4_INET_SAFI_UNICAST,
         pi4RetValFsbgp4SoftReconfigOutboundRequest) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsbgp4SoftReconfigOutboundRequest
Input       :  The Indices
Fsbgp4SoftReconfigOutboundPeerType
Fsbgp4SoftReconfigOutboundPeerAddr

The Object 
setValFsbgp4SoftReconfigOutboundRequest
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsbgp4SoftReconfigOutboundRequest (INT4
                                         i4Fsbgp4SoftReconfigOutboundPeerType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsbgp4SoftReconfigOutboundPeerAddr,
                                         INT4
                                         i4SetValFsbgp4SoftReconfigOutboundRequest)
{
    if (nmhSetFsbgp4mpeSoftReconfigOutboundRequest
        (i4Fsbgp4SoftReconfigOutboundPeerType,
         pFsbgp4SoftReconfigOutboundPeerAddr, BGP4_INET_AFI_IPV4,
         BGP4_INET_SAFI_UNICAST,
         i4SetValFsbgp4SoftReconfigOutboundRequest) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Fsbgp4SoftReconfigOutboundRequest
Input       :  The Indices
Fsbgp4SoftReconfigOutboundPeerType
Fsbgp4SoftReconfigOutboundPeerAddr

The Object 
testValFsbgp4SoftReconfigOutboundRequest
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Fsbgp4SoftReconfigOutboundRequest (UINT4 *pu4ErrorCode,
                                            INT4
                                            i4Fsbgp4SoftReconfigOutboundPeerType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsbgp4SoftReconfigOutboundPeerAddr,
                                            INT4
                                            i4TestValFsbgp4SoftReconfigOutboundRequest)
{
    if (nmhTestv2Fsbgp4mpeSoftReconfigOutboundRequest (pu4ErrorCode,
                                                       i4Fsbgp4SoftReconfigOutboundPeerType,
                                                       pFsbgp4SoftReconfigOutboundPeerAddr,
                                                       BGP4_INET_AFI_IPV4,
                                                       BGP4_INET_SAFI_UNICAST,
                                                       i4TestValFsbgp4SoftReconfigOutboundRequest)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Fsbgp4SoftReconfigOutboundTable
Input       :  The Indices
Fsbgp4SoftReconfigOutboundPeerType
Fsbgp4SoftReconfigOutboundPeerAddr
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Fsbgp4SoftReconfigOutboundTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsBgp4ORFListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsBgp4ORFListTable
 Input       :  The Indices
                FsBgp4ORFPeerAddrType
                FsBgp4ORFPeerAddr
                FsBgp4ORFAfi
                FsBgp4ORFSafi
                FsBgp4ORFType
                FsBgp4ORFSequence
                FsBgp4ORFAddrPrefix
                FsBgp4ORFAddrPrefixLen
                FsBgp4ORFMinLength
                FsBgp4ORFMaxLength
                FsBgp4ORFAction
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsBgp4ORFListTable (INT4 i4FsBgp4ORFPeerAddrType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsBgp4ORFPeerAddr,
                                            INT4 i4FsBgp4ORFAfi,
                                            INT4 i4FsBgp4ORFSafi,
                                            UINT4 u4FsBgp4ORFType,
                                            UINT4 u4FsBgp4ORFSequence,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsBgp4ORFAddrPrefix,
                                            UINT4 u4FsBgp4ORFAddrPrefixLen,
                                            UINT4 u4FsBgp4ORFMinLength,
                                            UINT4 u4FsBgp4ORFMaxLength,
                                            INT4 i4FsBgp4ORFAction)
{
    tAddrPrefix         PeerAddress;
    tAddrPrefix         AddrPrefix;
    INT4                i4Sts;

    MEMSET (&PeerAddress, 0, sizeof (tAddrPrefix));
    MEMSET (&AddrPrefix, 0, sizeof (tAddrPrefix));

    UNUSED_PARAM (u4FsBgp4ORFSequence);
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetIpAddress (i4FsBgp4ORFPeerAddrType,
                                 pFsBgp4ORFPeerAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to get the Peer Address.\n");
        return SNMP_FAILURE;
    }

    if (Bgp4IsValidAddress (PeerAddress, BGP4_FALSE) != BGP4_TRUE)
    {
        return SNMP_FAILURE;
    }
    if ((i4FsBgp4ORFAfi != BGP4_INET_AFI_IPV4)
        && (i4FsBgp4ORFAfi != BGP4_INET_AFI_IPV6))
    {
        return SNMP_FAILURE;
    }
    if (i4FsBgp4ORFSafi != BGP4_INET_SAFI_UNICAST)
    {
        return SNMP_FAILURE;
    }
    if (u4FsBgp4ORFType != BGP_ORF_TYPE_ADDRESS_PREFIX)
    {
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetIpAddress (i4FsBgp4ORFAfi,
                                 pFsBgp4ORFAddrPrefix, &AddrPrefix);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to get the Peer Address.\n");
        return SNMP_FAILURE;
    }
    if (Bgp4IsValidAddress (AddrPrefix, BGP4_TRUE) != BGP4_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (i4FsBgp4ORFAfi == BGP4_INET_AFI_IPV4)
    {
        if ((u4FsBgp4ORFAddrPrefixLen > BGP4_MAX_PREFIXLEN) ||
            (u4FsBgp4ORFMinLength > BGP4_MAX_PREFIXLEN) ||
            (u4FsBgp4ORFMaxLength > BGP4_MAX_PREFIXLEN))
        {
            return SNMP_FAILURE;
        }
    }
#ifdef BGP4_IPV6_WANTED
    if (i4FsBgp4ORFAfi == BGP4_INET_AFI_IPV6)
    {
        if ((u4FsBgp4ORFAddrPrefixLen > BGP4_MAX_IPV6_PREFIXLEN) ||
            (u4FsBgp4ORFMinLength > BGP4_MAX_IPV6_PREFIXLEN) ||
            (u4FsBgp4ORFMaxLength > BGP4_MAX_IPV6_PREFIXLEN))
        {
            return SNMP_FAILURE;
        }
    }
#endif
    if ((i4FsBgp4ORFAction != BGP4_ORF_PERMIT)
        && (i4FsBgp4ORFAction != BGP4_ORF_DENY))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsBgp4ORFListTable
 Input       :  The Indices
                FsBgp4ORFPeerAddrType
                FsBgp4ORFPeerAddr
                FsBgp4ORFAfi
                FsBgp4ORFSafi
                FsBgp4ORFType
                FsBgp4ORFSequence
                FsBgp4ORFAddrPrefix
                FsBgp4ORFAddrPrefixLen
                FsBgp4ORFMinLength
                FsBgp4ORFMaxLength
                FsBgp4ORFAction
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsBgp4ORFListTable (INT4 *pi4FsBgp4ORFPeerAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsBgp4ORFPeerAddr, INT4 *pi4FsBgp4ORFAfi,
                                    INT4 *pi4FsBgp4ORFSafi,
                                    UINT4 *pu4FsBgp4ORFType,
                                    UINT4 *pu4FsBgp4ORFSequence,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsBgp4ORFAddrPrefix,
                                    UINT4 *pu4FsBgp4ORFAddrPrefixLen,
                                    UINT4 *pu4FsBgp4ORFMinLength,
                                    UINT4 *pu4FsBgp4ORFMaxLength,
                                    INT4 *pi4FsBgp4ORFAction)
{
    tBgp4OrfEntry      *pOrfEntry = NULL;
    UINT4               u4Context = 0;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    pOrfEntry = RBTreeGetFirst (BGP4_ORF_ENTRY_TABLE (u4Context));

    if (pOrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4FsBgp4ORFPeerAddrType = pOrfEntry->PeerAddr.u2Afi;

    MEMCPY (pFsBgp4ORFPeerAddr->pu1_OctetList, pOrfEntry->PeerAddr.au1Address,
            pOrfEntry->PeerAddr.u2AddressLen);
    pFsBgp4ORFPeerAddr->i4_Length = pOrfEntry->PeerAddr.u2AddressLen;

    *pi4FsBgp4ORFAfi = pOrfEntry->u2Afi;
    *pi4FsBgp4ORFSafi = pOrfEntry->u1Safi;
    *pu4FsBgp4ORFType = pOrfEntry->u1OrfType;
    *pu4FsBgp4ORFSequence = pOrfEntry->u4SeqNo;

    MEMCPY (pFsBgp4ORFAddrPrefix->pu1_OctetList,
            pOrfEntry->AddrPrefix.au1Address,
            pOrfEntry->AddrPrefix.u2AddressLen);
    pFsBgp4ORFAddrPrefix->i4_Length = pOrfEntry->AddrPrefix.u2AddressLen;

    *pu4FsBgp4ORFAddrPrefixLen = pOrfEntry->u1PrefixLen;
    *pu4FsBgp4ORFMinLength = pOrfEntry->u1MinPrefixLen;
    *pu4FsBgp4ORFMaxLength = pOrfEntry->u1MaxPrefixLen;
    *pi4FsBgp4ORFAction = pOrfEntry->u1Match;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsBgp4ORFListTable
 Input       :  The Indices
                FsBgp4ORFPeerAddrType
                nextFsBgp4ORFPeerAddrType
                FsBgp4ORFPeerAddr
                nextFsBgp4ORFPeerAddr
                FsBgp4ORFAfi
                nextFsBgp4ORFAfi
                FsBgp4ORFSafi
                nextFsBgp4ORFSafi
                FsBgp4ORFType
                nextFsBgp4ORFType
                FsBgp4ORFSequence
                nextFsBgp4ORFSequence
                FsBgp4ORFAddrPrefix
                nextFsBgp4ORFAddrPrefix
                FsBgp4ORFAddrPrefixLen
                nextFsBgp4ORFAddrPrefixLen
                FsBgp4ORFMinLength
                nextFsBgp4ORFMinLength
                FsBgp4ORFMaxLength
                nextFsBgp4ORFMaxLength
                FsBgp4ORFAction
                nextFsBgp4ORFAction
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsBgp4ORFListTable (INT4 i4FsBgp4ORFPeerAddrType,
                                   INT4 *pi4NextFsBgp4ORFPeerAddrType,
                                   tSNMP_OCTET_STRING_TYPE * pFsBgp4ORFPeerAddr,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextFsBgp4ORFPeerAddr, INT4 i4FsBgp4ORFAfi,
                                   INT4 *pi4NextFsBgp4ORFAfi,
                                   INT4 i4FsBgp4ORFSafi,
                                   INT4 *pi4NextFsBgp4ORFSafi,
                                   UINT4 u4FsBgp4ORFType,
                                   UINT4 *pu4NextFsBgp4ORFType,
                                   UINT4 u4FsBgp4ORFSequence,
                                   UINT4 *pu4NextFsBgp4ORFSequence,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsBgp4ORFAddrPrefix,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextFsBgp4ORFAddrPrefix,
                                   UINT4 u4FsBgp4ORFAddrPrefixLen,
                                   UINT4 *pu4NextFsBgp4ORFAddrPrefixLen,
                                   UINT4 u4FsBgp4ORFMinLength,
                                   UINT4 *pu4NextFsBgp4ORFMinLength,
                                   UINT4 u4FsBgp4ORFMaxLength,
                                   UINT4 *pu4NextFsBgp4ORFMaxLength,
                                   INT4 i4FsBgp4ORFAction,
                                   INT4 *pi4NextFsBgp4ORFAction)
{
    tBgp4OrfEntry      *pOrfEntry = NULL;
    tBgp4OrfEntry       OrfEntry;
    UINT4               u4Context = 0;

    MEMSET (&OrfEntry, 0, sizeof (tBgp4OrfEntry));

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    OrfEntry.PeerAddr.u2Afi = (UINT2) i4FsBgp4ORFPeerAddrType;
    MEMCPY (OrfEntry.PeerAddr.au1Address, pFsBgp4ORFPeerAddr->pu1_OctetList,
            pFsBgp4ORFPeerAddr->i4_Length);
    OrfEntry.PeerAddr.u2AddressLen = (UINT2) pFsBgp4ORFPeerAddr->i4_Length;

    OrfEntry.u2Afi = (UINT2) i4FsBgp4ORFAfi;
    OrfEntry.u1Safi = (UINT1) i4FsBgp4ORFSafi;
    OrfEntry.u1OrfType = u4FsBgp4ORFType;
    OrfEntry.u4SeqNo = u4FsBgp4ORFSequence;

    OrfEntry.AddrPrefix.u2Afi = (UINT2) i4FsBgp4ORFAfi;
    MEMCPY (OrfEntry.AddrPrefix.au1Address, pFsBgp4ORFAddrPrefix->pu1_OctetList,
            pFsBgp4ORFAddrPrefix->i4_Length);
    OrfEntry.AddrPrefix.u2AddressLen = (UINT2) pFsBgp4ORFAddrPrefix->i4_Length;

    OrfEntry.u1PrefixLen = (UINT1) u4FsBgp4ORFAddrPrefixLen;
    OrfEntry.u1MinPrefixLen = (UINT1) u4FsBgp4ORFMinLength;
    OrfEntry.u1MaxPrefixLen = (UINT1) u4FsBgp4ORFMaxLength;
    OrfEntry.u1Match = (UINT1) i4FsBgp4ORFAction;

    pOrfEntry =
        RBTreeGetNext (BGP4_ORF_ENTRY_TABLE (u4Context), (tRBElem *) & OrfEntry,
                       NULL);
    if (pOrfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsBgp4ORFPeerAddrType = pOrfEntry->PeerAddr.u2Afi;

    MEMCPY (pNextFsBgp4ORFPeerAddr->pu1_OctetList,
            pOrfEntry->PeerAddr.au1Address, pOrfEntry->PeerAddr.u2AddressLen);
    pNextFsBgp4ORFPeerAddr->i4_Length = pOrfEntry->PeerAddr.u2AddressLen;

    *pi4NextFsBgp4ORFAfi = pOrfEntry->u2Afi;
    *pi4NextFsBgp4ORFSafi = pOrfEntry->u1Safi;
    *pu4NextFsBgp4ORFType = pOrfEntry->u1OrfType;
    *pu4NextFsBgp4ORFSequence = pOrfEntry->u4SeqNo;

    MEMCPY (pNextFsBgp4ORFAddrPrefix->pu1_OctetList,
            pOrfEntry->AddrPrefix.au1Address,
            pOrfEntry->AddrPrefix.u2AddressLen);
    pNextFsBgp4ORFAddrPrefix->i4_Length = pOrfEntry->AddrPrefix.u2AddressLen;

    *pu4NextFsBgp4ORFAddrPrefixLen = pOrfEntry->u1PrefixLen;
    *pu4NextFsBgp4ORFMinLength = pOrfEntry->u1MinPrefixLen;
    *pu4NextFsBgp4ORFMaxLength = pOrfEntry->u1MaxPrefixLen;
    *pi4NextFsBgp4ORFAction = pOrfEntry->u1Match;

    return SNMP_SUCCESS;
}
