/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgrfdlow.c,v 1.20 2016/12/27 12:35:55 siva Exp $
 *
 * Description: Contains BGP Route Flap Damp feature low level routines
 *
 *******************************************************************/
# include   "bgp4com.h"
# include  "include.h"
# include  "fsbgpcon.h"
# include  "fsbgpogi.h"
# include  "midconst.h"
#include "fsmpbgcli.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4RfdCutOff
 Input       :  The Indices

                The Object 
                retValFsbgp4RfdCutOff
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4RfdCutOff (INT4 *pi4RetValFsbgp4RfdCutOff)
{
#ifdef RFD_WANTED
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4RfdCutOff = (INT4) RFD_CUTOFF_THRESHOLD (u4Context);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4RetValFsbgp4RfdCutOff);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4RfdReuse
 Input       :  The Indices

                The Object 
                retValFsbgp4RfdReuse
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4RfdReuse (INT4 *pi4RetValFsbgp4RfdReuse)
{
#ifdef RFD_WANTED
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4RfdReuse = (INT4) RFD_REUSE_THRESHOLD (u4Context);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4RetValFsbgp4RfdReuse);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4RfdCeiling
 Input       :  The Indices

                The Object 
                retValFsbgp4RfdCeiling
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4RfdCeiling (INT4 *pi4RetValFsbgp4RfdCeiling)
{
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

#ifdef RFD_WANTED
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4RfdCeiling =
        (INT4) RFD_CEIL (RFD_CEILING_VALUE (u4Context));
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4RetValFsbgp4RfdCeiling);
    UNUSED_PARAM (u4Context);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4RfdMaxHoldDownTime
 Input       :  The Indices

                The Object 
                retValFsbgp4RfdMaxHoldDownTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4RfdMaxHoldDownTime (INT4 *pi4RetValFsbgp4RfdMaxHoldDownTime)
{
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

#ifdef RFD_WANTED
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4RfdMaxHoldDownTime = RFD_MAX_HOLD_DOWN_TIME (u4Context);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4RetValFsbgp4RfdMaxHoldDownTime);
    UNUSED_PARAM (u4Context);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4RfdDecayHalfLifeTime
 Input       :  The Indices

                The Object 
                retValFsbgp4RfdDecayHalfLifeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4RfdDecayHalfLifeTime (INT4 *pi4RetValFsbgp4RfdDecayHalfLifeTime)
{
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

#ifdef RFD_WANTED
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4RfdDecayHalfLifeTime = RFD_DECAY_HALF_LIFE_TIME (u4Context);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4RetValFsbgp4RfdDecayHalfLifeTime);
    UNUSED_PARAM (u4Context);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4RfdDecayTimerGranularity
 Input       :  The Indices

                The Object 
                retValFsbgp4RfdDecayTimerGranularity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4RfdDecayTimerGranularity (INT4
                                      *pi4RetValFsbgp4RfdDecayTimerGranularity)
{
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

#ifdef RFD_WANTED
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4RfdDecayTimerGranularity =
        RFD_DECAY_TIMER_GRANULARITY (u4Context);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4RetValFsbgp4RfdDecayTimerGranularity);
    UNUSED_PARAM (u4Context);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4RfdReuseTimerGranularity
 Input       :  The Indices

                The Object 
                retValFsbgp4RfdReuseTimerGranularity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4RfdReuseTimerGranularity (INT4
                                      *pi4RetValFsbgp4RfdReuseTimerGranularity)
{
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

#ifdef RFD_WANTED
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4RfdReuseTimerGranularity =
        RFD_REUSE_TIMER_GRANULARITY (u4Context);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4RetValFsbgp4RfdReuseTimerGranularity);
    UNUSED_PARAM (u4Context);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4RfdReuseIndxArraySize
 Input       :  The Indices

                The Object 
                retValFsbgp4RfdReuseIndxArraySize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4RfdReuseIndxArraySize (INT4 *pi4RetValFsbgp4RfdReuseIndxArraySize)
{
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

#ifdef RFD_WANTED
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4RfdReuseIndxArraySize =
        RFD_REUSE_INDEX_ARRAY_SIZE (u4Context);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4RetValFsbgp4RfdReuseIndxArraySize);
    UNUSED_PARAM (u4Context);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4RfdAdminStatus
 Input       :  The Indices

                The Object 
                retValFsbgp4RfdAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4RfdAdminStatus (INT4 *pi4RetValFsbgp4RfdAdminStatus)
{
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

#ifdef RFD_WANTED
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4RfdAdminStatus = RFD_ADMIN_STATUS (u4Context);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4RetValFsbgp4RfdAdminStatus);
    UNUSED_PARAM (u4Context);
    return SNMP_FAILURE;
#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4RfdCutOff
 Input       :  The Indices

                The Object 
                setValFsbgp4RfdCutOff
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4RfdCutOff (INT4 i4SetValFsbgp4RfdCutOff)
{
    UINT4               u4Context;

#ifdef RFD_WANTED
    tBgpSystemSize      BgpSystemSize;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    RFD_CUTOFF_THRESHOLD (u4Context) = i4SetValFsbgp4RfdCutOff;
    GetBgpSizingParams (&BgpSystemSize);
    BgpSystemSize.u4BgpRfdCutOffThreshold =
        (UINT4) RFD_CUTOFF_THRESHOLD (u4Context);
    SetBgpSizingParams (&BgpSystemSize);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4RfdCutOff, u4SeqNum, FALSE,
                          BgpLock, BgpUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gpBgpCurrCxtNode->u4ContextId,
                      i4SetValFsbgp4RfdCutOff));
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFsbgp4RfdCutOff);
    UNUSED_PARAM (u4Context);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsbgp4RfdReuse
 Input       :  The Indices

                The Object 
                setValFsbgp4RfdReuse
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4RfdReuse (INT4 i4SetValFsbgp4RfdReuse)
{
    UINT4               u4Context;

#ifdef RFD_WANTED
    tBgpSystemSize      BgpSystemSize;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    u4Context = gpBgpCurrCxtNode->u4ContextId;
    RFD_REUSE_THRESHOLD (u4Context) = i4SetValFsbgp4RfdReuse;
    GetBgpSizingParams (&BgpSystemSize);
    BgpSystemSize.u4BgpRfdReuseThreshold =
        (UINT4) RFD_REUSE_THRESHOLD (u4Context);
    SetBgpSizingParams (&BgpSystemSize);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4RfdReuse, u4SeqNum, FALSE,
                          BgpLock, BgpUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gpBgpCurrCxtNode->u4ContextId,
                      i4SetValFsbgp4RfdReuse));
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFsbgp4RfdReuse);
    UNUSED_PARAM (u4Context);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsbgp4RfdMaxHoldDownTime
 Input       :  The Indices

                The Object 
                setValFsbgp4RfdMaxHoldDownTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4RfdMaxHoldDownTime (INT4 i4SetValFsbgp4RfdMaxHoldDownTime)
{
    UINT4               u4Context;

#ifdef RFD_WANTED
    tBgpSystemSize      BgpSystemSize;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (i4SetValFsbgp4RfdMaxHoldDownTime <
        RFD_DECAY_TIMER_GRANULARITY (u4Context))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - RFD MaxHold Time is Less than Decay Timer\n");
        return SNMP_FAILURE;
    }

    RFD_MAX_HOLD_DOWN_TIME (u4Context) =
        (UINT2) i4SetValFsbgp4RfdMaxHoldDownTime;
    GetBgpSizingParams (&BgpSystemSize);
    BgpSystemSize.u4BgpRfdMaxHoldDownTime = RFD_MAX_HOLD_DOWN_TIME (u4Context);
    SetBgpSizingParams (&BgpSystemSize);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4RfdMaxHoldDownTime, u4SeqNum,
                          FALSE, BgpLock, BgpUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gpBgpCurrCxtNode->u4ContextId,
                      i4SetValFsbgp4RfdMaxHoldDownTime));
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFsbgp4RfdMaxHoldDownTime);
    UNUSED_PARAM (u4Context);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsbgp4RfdDecayHalfLifeTime
 Input       :  The Indices

                The Object 
                setValFsbgp4RfdDecayHalfLifeTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4RfdDecayHalfLifeTime (INT4 i4SetValFsbgp4RfdDecayHalfLifeTime)
{
    UINT4               u4Context;

#ifdef RFD_WANTED
    tBgpSystemSize      BgpSystemSize;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    RFD_DECAY_HALF_LIFE_TIME (u4Context) =
        (UINT2) i4SetValFsbgp4RfdDecayHalfLifeTime;
    GetBgpSizingParams (&BgpSystemSize);
    BgpSystemSize.u4BgpRfdDecayHalfLifeTime =
        RFD_DECAY_HALF_LIFE_TIME (u4Context);
    SetBgpSizingParams (&BgpSystemSize);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4RfdDecayHalfLifeTime,
                          u4SeqNum, FALSE, BgpLock, BgpUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gpBgpCurrCxtNode->u4ContextId,
                      i4SetValFsbgp4RfdDecayHalfLifeTime));
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFsbgp4RfdDecayHalfLifeTime);
    UNUSED_PARAM (u4Context);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsbgp4RfdDecayTimerGranularity
 Input       :  The Indices

                The Object 
                setValFsbgp4RfdDecayTimerGranularity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4RfdDecayTimerGranularity (INT4
                                      i4SetValFsbgp4RfdDecayTimerGranularity)
{
    UINT4               u4Context;

#ifdef RFD_WANTED
    tBgpSystemSize      BgpSystemSize;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (i4SetValFsbgp4RfdDecayTimerGranularity >
        RFD_MAX_HOLD_DOWN_TIME (u4Context))

    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - RFD Decay Timer Granularity Value is Greater than MaxHold Time\n");
        return SNMP_FAILURE;

    }

    RFD_DECAY_TIMER_GRANULARITY (u4Context) =
        (UINT2) i4SetValFsbgp4RfdDecayTimerGranularity;
    GetBgpSizingParams (&BgpSystemSize);
    BgpSystemSize.u4BgpRfdDecayTimerGranularity =
        RFD_DECAY_TIMER_GRANULARITY (u4Context);
    SetBgpSizingParams (&BgpSystemSize);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4RfdDecayTimerGranularity,
                          u4SeqNum, FALSE, BgpLock, BgpUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gpBgpCurrCxtNode->u4ContextId,
                      i4SetValFsbgp4RfdDecayTimerGranularity));
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFsbgp4RfdDecayTimerGranularity);
    UNUSED_PARAM (u4Context);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsbgp4RfdReuseTimerGranularity
 Input       :  The Indices

                The Object 
                setValFsbgp4RfdReuseTimerGranularity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4RfdReuseTimerGranularity (INT4
                                      i4SetValFsbgp4RfdReuseTimerGranularity)
{
    UINT4               u4Context;

#ifdef RFD_WANTED
    tBgpSystemSize      BgpSystemSize;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    RFD_REUSE_TIMER_GRANULARITY (u4Context) =
        (UINT2) i4SetValFsbgp4RfdReuseTimerGranularity;
    GetBgpSizingParams (&BgpSystemSize);
    BgpSystemSize.u4BgpRfdReuseTimerGranularity =
        RFD_REUSE_TIMER_GRANULARITY (u4Context);
    SetBgpSizingParams (&BgpSystemSize);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4RfdReuseTimerGranularity,
                          u4SeqNum, FALSE, BgpLock, BgpUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gpBgpCurrCxtNode->u4ContextId,
                      i4SetValFsbgp4RfdReuseTimerGranularity));
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFsbgp4RfdReuseTimerGranularity);
    UNUSED_PARAM (u4Context);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsbgp4RfdReuseIndxArraySize
 Input       :  The Indices

                The Object 
                setValFsbgp4RfdReuseIndxArraySize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4RfdReuseIndxArraySize (INT4 i4SetValFsbgp4RfdReuseIndxArraySize)
{
    UINT4               u4Context;

#ifdef RFD_WANTED
    tBgpSystemSize      BgpSystemSize;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    RFD_REUSE_INDEX_ARRAY_SIZE (u4Context) =
        (UINT2) i4SetValFsbgp4RfdReuseIndxArraySize;
    GetBgpSizingParams (&BgpSystemSize);
    BgpSystemSize.u4BgpRfdReuseArraySize =
        RFD_REUSE_INDEX_ARRAY_SIZE (u4Context);
    SetBgpSizingParams (&BgpSystemSize);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4RfdReuseIndxArraySize,
                          u4SeqNum, FALSE, BgpLock, BgpUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gpBgpCurrCxtNode->u4ContextId,
                      i4SetValFsbgp4RfdReuseIndxArraySize));
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFsbgp4RfdReuseIndxArraySize);
    UNUSED_PARAM (u4Context);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsbgp4RfdAdminStatus
 Input       :  The Indices

                The Object 
                setValFsbgp4RfdAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4RfdAdminStatus (INT4 i4SetValFsbgp4RfdAdminStatus)
{
    UINT4               u4Context;

#ifdef RFD_WANTED
    tBgp4QMsg          *pQMsg = NULL;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (RFD_ADMIN_STATUS (u4Context) == i4SetValFsbgp4RfdAdminStatus)
    {
        return SNMP_SUCCESS;
    }

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting BGP Peer Status Msg "
                  "to BGP Queue FAILED!!!\n");
        return SNMP_FAILURE;
    }
    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_RFD_ADMIN_CHG_EVENT;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
    BGP4_INPUTQ_DATA (pQMsg) = (UINT4) i4SetValFsbgp4RfdAdminStatus;
    BGP4_INPUTQ_CXT (pQMsg) = u4Context;
    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4RfdAdminStatus, u4SeqNum,
                          FALSE, BgpLock, BgpUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gpBgpCurrCxtNode->u4ContextId,
                      i4SetValFsbgp4RfdAdminStatus));
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFsbgp4RfdReuseIndxArraySize);
    UNUSED_PARAM (u4Context);
    return SNMP_FAILURE;
#endif

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4RfdCutOff
 Input       :  The Indices

                The Object 
                testValFsbgp4RfdCutOff
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4RfdCutOff (UINT4 *pu4ErrorCode, INT4 i4TestValFsbgp4RfdCutOff)
{
    UINT4               u4Context;

#ifdef RFD_WANTED
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP AS number not Configured.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }
    if (RFD_ADMIN_STATUS (u4Context) == BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Rfd module is admin up.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_RFD_ENABLE_ERR);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4RfdCutOff < RFD_MIN_CUTOFF_THRESHOLD) ||
        (i4TestValFsbgp4RfdCutOff > RFD_MAX_CUTOFF_THRESHOLD))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid RFD Cut-Off Threshold Value.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_RFD_CUTOFF_VALUE_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (i4TestValFsbgp4RfdCutOff);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4RfdReuse
 Input       :  The Indices

                The Object 
                testValFsbgp4RfdReuse
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4RfdReuse (UINT4 *pu4ErrorCode, INT4 i4TestValFsbgp4RfdReuse)
{
    UINT4               u4Context;

#ifdef RFD_WANTED
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP AS number not configured.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }
    if (RFD_ADMIN_STATUS (u4Context) == BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Rfd module is admin up.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_RFD_ENABLE_ERR);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4RfdReuse < RFD_MIN_REUSE_THRESHOLD) ||
        (i4TestValFsbgp4RfdReuse > RFD_MAX_REUSE_THRESHOLD))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid RFD Reuse Threshold.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_RFD_REUSE_VALUE_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsbgp4RfdReuse);
    UNUSED_PARAM (u4Context);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4RfdMaxHoldDownTime
 Input       :  The Indices

                The Object 
                testValFsbgp4RfdMaxHoldDownTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4RfdMaxHoldDownTime (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsbgp4RfdMaxHoldDownTime)
{
    UINT4               u4Context;

#ifdef RFD_WANTED
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP AS number not configured.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }
    if(i4TestValFsbgp4RfdMaxHoldDownTime <  
          RFD_DECAY_HALF_LIFE_TIME (u4Context))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - RFD MaxHold Time is Less than Decay Half Life Time\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_HIGHER_DECAY_HALF_LIFE_TIME);
        return SNMP_FAILURE;

    }
    if (RFD_ADMIN_STATUS (u4Context) == BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Rfd module is admin up.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_RFD_ENABLE_ERR);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4RfdMaxHoldDownTime < RFD_MIN_MAXHOLDDOWNTIME) ||
        (i4TestValFsbgp4RfdMaxHoldDownTime > RFD_MAX_MAXHOLDDOWNTIME))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid RFD Maximum Hold Down Time.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_MAX_HOLD_TIME_VALUE_ERR);
        return SNMP_FAILURE;
    }
    if (i4TestValFsbgp4RfdMaxHoldDownTime <
        RFD_DECAY_TIMER_GRANULARITY (u4Context))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - RFD MaxHold Time is Less than Decay Timer\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_HIGHER_DECAY_TIMER_GRAN_VALUE_ERR);
        return SNMP_FAILURE;
    }
    if (((i4TestValFsbgp4RfdMaxHoldDownTime + 1) * sizeof (float)) >
        (MAX_BGP_RFD_DECAY_ARRAY_SIZE *
         FsBGPSizingParams[MAX_BGP_RFD_DECAY_ARRAY_BLOCKS].u4PreAllocatedUnits))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid RFD Maximum Reuse Index Array size.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_RFD_DECAY_ARRAY_OVERFLOW);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsbgp4RfdMaxHoldDownTime);
    UNUSED_PARAM (u4Context);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4RfdDecayHalfLifeTime
 Input       :  The Indices

                The Object 
                testValFsbgp4RfdDecayHalfLifeTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4RfdDecayHalfLifeTime (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsbgp4RfdDecayHalfLifeTime)
{
    UINT4               u4Context;

#ifdef RFD_WANTED
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP AS number not configured.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }
    if (RFD_ADMIN_STATUS (u4Context) == BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Rfd module is admin up.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_RFD_ENABLE_ERR);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4RfdDecayHalfLifeTime < RFD_MIN_DECAY_HALF_LIFE_TIME) ||
        (i4TestValFsbgp4RfdDecayHalfLifeTime > RFD_MAX_DECAY_HALF_LIFE_TIME))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid RFD Decay Half Life Time Value.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_DECAY_HALF_LIFE_TIME_VALUE_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (i4TestValFsbgp4RfdDecayHalfLifeTime);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4RfdDecayTimerGranularity
 Input       :  The Indices

                The Object 
                testValFsbgp4RfdDecayTimerGranularity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4RfdDecayTimerGranularity (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4TestValFsbgp4RfdDecayTimerGranularity)
{
    UINT4               u4Context;

#ifdef RFD_WANTED
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP AS number not configured.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }
    if (RFD_ADMIN_STATUS (u4Context) == BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Rfd module is admin up.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_RFD_ENABLE_ERR);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4RfdDecayTimerGranularity <
         RFD_MIN_DECAY_TIMER_GRANULARITY) ||
        (i4TestValFsbgp4RfdDecayTimerGranularity >
         RFD_MAX_DECAY_TIMER_GRANULARITY))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid RFD Decay Timer Granularity Value.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_DECAY_TIMER_GRAN_VALUE_ERR);
        return SNMP_FAILURE;
    }
    if (i4TestValFsbgp4RfdDecayTimerGranularity >
        RFD_MAX_HOLD_DOWN_TIME (u4Context))

    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - RFD Decay Timer Granularity Value is Greater than MaxHold Time\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_HIGHER_DECAY_TIMER_GRAN_VALUE_ERR);
        return SNMP_FAILURE;

    }

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (i4TestValFsbgp4RfdDecayTimerGranularity);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4RfdReuseTimerGranularity
 Input       :  The Indices

                The Object 
                testValFsbgp4RfdReuseTimerGranularity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4RfdReuseTimerGranularity (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4TestValFsbgp4RfdReuseTimerGranularity)
{
    UINT4               u4Context;

#ifdef RFD_WANTED
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP AS not configured.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }
    if (RFD_ADMIN_STATUS (u4Context) == BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Rfd module is admin up.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_RFD_ENABLE_ERR);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4RfdReuseTimerGranularity <
         RFD_MIN_REUSE_TIMER_GRANULARITY) ||
        (i4TestValFsbgp4RfdReuseTimerGranularity >
         RFD_MAX_REUSE_TIMER_GRANULARITY))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalie RFD Reuse Timer Granularity.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_REUSE_TIMER_GRAN_VALUE_ERR);
        return SNMP_FAILURE;
    }
    if (i4TestValFsbgp4RfdReuseTimerGranularity >
        (RFD_DECAY_HALF_LIFE_TIME (u4Context)/4))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - RFD Decay Timer Granularity Value is Greater than MaxHold Time\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_HIGHER_REUSE_TIMER_GRAN_VALUE_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (i4TestValFsbgp4RfdReuseTimerGranularity);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4RfdReuseIndxArraySize
 Input       :  The Indices

                The Object 
                testValFsbgp4RfdReuseIndxArraySize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4RfdReuseIndxArraySize (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFsbgp4RfdReuseIndxArraySize)
{
    UINT4               u4Context;

#ifdef RFD_WANTED
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP AS number not configured.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }
    if (RFD_ADMIN_STATUS (u4Context) == BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Rfd module is admin up.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_RFD_ENABLE_ERR);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4RfdReuseIndxArraySize <
         RFD_MIN_REUSE_INDEX_ARRAY_SIZE) ||
        (i4TestValFsbgp4RfdReuseIndxArraySize > RFD_MAX_REUSE_INDEX_ARRAY_SIZE))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid RFD Maximum Reuse Index Array size.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_REUSE_IND_ARRAY_VALUE_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (i4TestValFsbgp4RfdReuseIndxArraySize);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4RfdAdminStatus
 Input       :  The Indices

                The Object 
                testValFsbgp4RfdAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4RfdAdminStatus (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsbgp4RfdAdminStatus)
{
    UINT4               u4Context;

#ifdef RFD_WANTED
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "ERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "ERROR - BGP AS number not configured.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4RfdAdminStatus != BGP4_TRUE) &&
        (i4TestValFsbgp4RfdAdminStatus != BGP4_FALSE))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "ERROR - Invalid RFD Admin status.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_RFD_ADMIN_STAT);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (i4TestValFsbgp4RfdAdminStatus);
    return SNMP_FAILURE;
#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4RfdCutOff
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4RfdCutOff (UINT4 *pu4ErrorCode,
                         tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4RfdReuse
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4RfdReuse (UINT4 *pu4ErrorCode,
                        tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4RfdMaxHoldDownTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4RfdMaxHoldDownTime (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4RfdDecayHalfLifeTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4RfdDecayHalfLifeTime (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4RfdDecayTimerGranularity
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4RfdDecayTimerGranularity (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4RfdReuseTimerGranularity
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4RfdReuseTimerGranularity (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4RfdReuseIndxArraySize
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4RfdReuseIndxArraySize (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4RfdAdminStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4RfdAdminStatus (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4MpeRfdRtDampHistTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4MpeRfdRtDampHistTable
 Input       :  The Indices
                Fsbgp4PathAttrAddrPrefixAfi
                Fsbgp4PathAttrAddrPrefixSafi
                Fsbgp4PathAttrAddrPrefix
                Fsbgp4PathAttrAddrPrefixLen
                Fsbgp4PathAttrPeerType
                Fsbgp4PathAttrPeer
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4MpeRfdRtDampHistTable (INT4
                                                     i4Fsbgp4PathAttrAddrPrefixAfi,
                                                     INT4
                                                     i4Fsbgp4PathAttrAddrPrefixSafi,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pFsbgp4PathAttrAddrPrefix,
                                                     INT4
                                                     i4Fsbgp4PathAttrAddrPrefixLen,
                                                     INT4
                                                     i4Fsbgp4PathAttrPeerType,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pFsbgp4PathAttrPeer)
{
#ifdef RFD_WANTED
    tNetAddress         RtAddress;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4PathAttrPeerType,
                                 pFsbgp4PathAttrPeer, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4SnmphGetNetAddress (i4Fsbgp4PathAttrAddrPrefixAfi,
                                    i4Fsbgp4PathAttrAddrPrefixSafi,
                                    pFsbgp4PathAttrAddrPrefix,
                                    PeerAddress, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4PathAttrAddrPrefixLen;

    i4Sts = Bgp4ValidateFsbgp4RfdDampHistTableIndex (RtAddress, PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid RFD Damp History Table Index.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4PathAttrAddrPrefixAfi);
    UNUSED_PARAM (i4Fsbgp4PathAttrAddrPrefixSafi);
    UNUSED_PARAM (pFsbgp4PathAttrAddrPrefix);
    UNUSED_PARAM (i4Fsbgp4PathAttrAddrPrefixLen);
    UNUSED_PARAM (i4Fsbgp4PathAttrPeerType);
    UNUSED_PARAM (pFsbgp4PathAttrPeer);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4MpeRfdRtDampHistTable
 Input       :  The Indices
                Fsbgp4PathAttrAddrPrefixAfi
                Fsbgp4PathAttrAddrPrefixSafi
                Fsbgp4PathAttrAddrPrefix
                Fsbgp4PathAttrAddrPrefixLen
                Fsbgp4PathAttrPeerType
                Fsbgp4PathAttrPeer
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4MpeRfdRtDampHistTable (INT4
                                             *pi4Fsbgp4PathAttrAddrPrefixAfi,
                                             INT4
                                             *pi4Fsbgp4PathAttrAddrPrefixSafi,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsbgp4PathAttrAddrPrefix,
                                             INT4
                                             *pi4Fsbgp4PathAttrAddrPrefixLen,
                                             INT4 *pi4Fsbgp4PathAttrPeerType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsbgp4PathAttrPeer)
{
#ifdef RFD_WANTED
    tNetAddress         RtAddress;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts =
        Bgp4GetFirstIndexFsbgp4RfdDampHistTable (u4Context, &RtAddress,
                                                 &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - RFD Damp History List not present.\n");
        return SNMP_FAILURE;
    }

    *pi4Fsbgp4PathAttrAddrPrefixAfi =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                      (RtAddress));
    *pi4Fsbgp4PathAttrAddrPrefixSafi =
        BGP4_SAFI_IN_NET_ADDRESS_INFO (RtAddress);
    *pi4Fsbgp4PathAttrAddrPrefixLen =
        BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress);
    i4Sts =
        Bgp4LowSetRouteAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO
                                (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                 (RtAddress)),
                                BGP4_SAFI_IN_NET_ADDRESS_INFO (RtAddress),
                                pFsbgp4PathAttrAddrPrefix, RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4Fsbgp4PathAttrPeerType = BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress);
    i4Sts = Bgp4LowSetIpAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress),
                                 pFsbgp4PathAttrPeer, PeerAddress);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4Fsbgp4PathAttrAddrPrefixAfi);
    UNUSED_PARAM (pi4Fsbgp4PathAttrAddrPrefixSafi);
    UNUSED_PARAM (pFsbgp4PathAttrAddrPrefix);
    UNUSED_PARAM (pi4Fsbgp4PathAttrAddrPrefixLen);
    UNUSED_PARAM (pi4Fsbgp4PathAttrPeerType);
    UNUSED_PARAM (pFsbgp4PathAttrPeer);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4MpeRfdRtDampHistTable
 Input       :  The Indices
                Fsbgp4PathAttrAddrPrefixAfi
                nextFsbgp4PathAttrAddrPrefixAfi
                Fsbgp4PathAttrAddrPrefixSafi
                nextFsbgp4PathAttrAddrPrefixSafi
                Fsbgp4PathAttrAddrPrefix
                nextFsbgp4PathAttrAddrPrefix
                Fsbgp4PathAttrAddrPrefixLen
                nextFsbgp4PathAttrAddrPrefixLen
                Fsbgp4PathAttrPeerType
                nextFsbgp4PathAttrPeerType
                Fsbgp4PathAttrPeer
                nextFsbgp4PathAttrPeer
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4MpeRfdRtDampHistTable (INT4 i4Fsbgp4PathAttrAddrPrefixAfi,
                                            INT4
                                            *pi4NextFsbgp4PathAttrAddrPrefixAfi,
                                            INT4 i4Fsbgp4PathAttrAddrPrefixSafi,
                                            INT4
                                            *pi4NextFsbgp4PathAttrAddrPrefixSafi,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsbgp4PathAttrAddrPrefix,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pNextFsbgp4PathAttrAddrPrefix,
                                            INT4 i4Fsbgp4PathAttrAddrPrefixLen,
                                            INT4
                                            *pi4NextFsbgp4PathAttrAddrPrefixLen,
                                            INT4 i4Fsbgp4PathAttrPeerType,
                                            INT4 *pi4NextFsbgp4PathAttrPeerType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsbgp4PathAttrPeer,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pNextFsbgp4PathAttrPeer)
{
#ifdef RFD_WANTED
    tNetAddress         RtAddress;
    tNetAddress         NextRtAddress;
    tAddrPrefix         PeerAddress;
    tAddrPrefix         NextPeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4PathAttrPeerType,
                                 pFsbgp4PathAttrPeer, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4SnmphGetNetAddress (i4Fsbgp4PathAttrAddrPrefixAfi,
                                    i4Fsbgp4PathAttrAddrPrefixSafi,
                                    pFsbgp4PathAttrAddrPrefix,
                                    PeerAddress, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4PathAttrAddrPrefixLen;

    i4Sts =
        Bgp4GetNextIndexFsbgp4RfdDampHistTable (u4Context, RtAddress,
                                                PeerAddress, &NextRtAddress,
                                                &NextPeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4NextFsbgp4PathAttrAddrPrefixAfi =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                      (NextRtAddress));
    *pi4NextFsbgp4PathAttrAddrPrefixSafi =
        BGP4_SAFI_IN_NET_ADDRESS_INFO (NextRtAddress);
    *pi4NextFsbgp4PathAttrAddrPrefixLen =
        BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NextRtAddress);
    i4Sts =
        Bgp4LowSetRouteAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO
                                (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                 (NextRtAddress)),
                                BGP4_SAFI_IN_NET_ADDRESS_INFO (NextRtAddress),
                                pNextFsbgp4PathAttrAddrPrefix, NextRtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsbgp4PathAttrPeerType =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (NextPeerAddress);
    i4Sts = Bgp4LowSetIpAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO (NextPeerAddress),
                                 pNextFsbgp4PathAttrPeer, NextPeerAddress);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4NextFsbgp4PathAttrAddrPrefixAfi);
    UNUSED_PARAM (pi4NextFsbgp4PathAttrAddrPrefixSafi);
    UNUSED_PARAM (pNextFsbgp4PathAttrAddrPrefix);
    UNUSED_PARAM (pi4NextFsbgp4PathAttrAddrPrefixLen);
    UNUSED_PARAM (pi4NextFsbgp4PathAttrPeerType);
    UNUSED_PARAM (pNextFsbgp4PathAttrPeer);
    UNUSED_PARAM (i4Fsbgp4PathAttrAddrPrefixAfi);
    UNUSED_PARAM (i4Fsbgp4PathAttrAddrPrefixSafi);
    UNUSED_PARAM (pFsbgp4PathAttrAddrPrefix);
    UNUSED_PARAM (i4Fsbgp4PathAttrAddrPrefixLen);
    UNUSED_PARAM (i4Fsbgp4PathAttrPeerType);
    UNUSED_PARAM (pFsbgp4PathAttrPeer);
    return SNMP_FAILURE;
#endif
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRfdRtFom
 Input       :  The Indices
                Fsbgp4PathAttrAddrPrefixAfi
                Fsbgp4PathAttrAddrPrefixSafi
                Fsbgp4PathAttrAddrPrefix
                Fsbgp4PathAttrAddrPrefixLen
                Fsbgp4PathAttrPeerType
                Fsbgp4PathAttrPeer

                The Object 
                retValFsbgp4RfdRtFom
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRfdRtFom (INT4 i4Fsbgp4PathAttrAddrPrefixAfi,
                         INT4 i4Fsbgp4PathAttrAddrPrefixSafi,
                         tSNMP_OCTET_STRING_TYPE * pFsbgp4PathAttrAddrPrefix,
                         INT4 i4Fsbgp4PathAttrAddrPrefixLen,
                         INT4 i4Fsbgp4PathAttrPeerType,
                         tSNMP_OCTET_STRING_TYPE * pFsbgp4PathAttrPeer,
                         INT4 *pi4RetValFsbgp4RfdRtFom)
{
#ifdef RFD_WANTED
    tNetAddress         RtAddress;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    if (BGP4_LOCAL_ADMIN_STATUS (u4Context) != BGP4_ADMIN_UP)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Admin Status is DOWN.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4PathAttrPeerType,
                                 pFsbgp4PathAttrPeer, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4SnmphGetNetAddress (i4Fsbgp4PathAttrAddrPrefixAfi,
                                    i4Fsbgp4PathAttrAddrPrefixSafi,
                                    pFsbgp4PathAttrAddrPrefix,
                                    PeerAddress, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4PathAttrAddrPrefixLen;

    i4Sts =
        Bgp4GetFsbgp4RfdDampHistTableIntegerObject (u4Context, RtAddress,
                                                    PeerAddress,
                                                    BGP4_RFD_RT_FOM_OBJECT,
                                                    pi4RetValFsbgp4RfdRtFom);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Damped Route exist.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4PathAttrAddrPrefixAfi);
    UNUSED_PARAM (i4Fsbgp4PathAttrAddrPrefixSafi);
    UNUSED_PARAM (pFsbgp4PathAttrAddrPrefix);
    UNUSED_PARAM (i4Fsbgp4PathAttrAddrPrefixLen);
    UNUSED_PARAM (i4Fsbgp4PathAttrPeerType);
    UNUSED_PARAM (pFsbgp4PathAttrPeer);
    UNUSED_PARAM (pi4RetValFsbgp4RfdRtFom);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRfdRtLastUpdtTime
 Input       :  The Indices
                Fsbgp4PathAttrAddrPrefixAfi
                Fsbgp4PathAttrAddrPrefixSafi
                Fsbgp4PathAttrAddrPrefix
                Fsbgp4PathAttrAddrPrefixLen
                Fsbgp4PathAttrPeerType
                Fsbgp4PathAttrPeer

                The Object 
                retValFsbgp4RfdRtLastUpdtTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRfdRtLastUpdtTime (INT4 i4Fsbgp4PathAttrAddrPrefixAfi,
                                  INT4 i4Fsbgp4PathAttrAddrPrefixSafi,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsbgp4PathAttrAddrPrefix,
                                  INT4 i4Fsbgp4PathAttrAddrPrefixLen,
                                  INT4 i4Fsbgp4PathAttrPeerType,
                                  tSNMP_OCTET_STRING_TYPE * pFsbgp4PathAttrPeer,
                                  INT4 *pi4RetValFsbgp4RfdRtLastUpdtTime)
{
#ifdef RFD_WANTED
    tNetAddress         RtAddress;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4PathAttrPeerType,
                                 pFsbgp4PathAttrPeer, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4SnmphGetNetAddress (i4Fsbgp4PathAttrAddrPrefixAfi,
                                    i4Fsbgp4PathAttrAddrPrefixSafi,
                                    pFsbgp4PathAttrAddrPrefix,
                                    PeerAddress, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4PathAttrAddrPrefixLen;

    i4Sts =
        Bgp4GetFsbgp4RfdDampHistTableIntegerObject (u4Context, RtAddress,
                                                    PeerAddress,
                                                    BGP4_RFD_RT_LASTUPDT_TIME_OBJECT,
                                                    pi4RetValFsbgp4RfdRtLastUpdtTime);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Damped Route exist.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4PathAttrAddrPrefixAfi);
    UNUSED_PARAM (i4Fsbgp4PathAttrAddrPrefixSafi);
    UNUSED_PARAM (pFsbgp4PathAttrAddrPrefix);
    UNUSED_PARAM (i4Fsbgp4PathAttrAddrPrefixLen);
    UNUSED_PARAM (i4Fsbgp4PathAttrPeerType);
    UNUSED_PARAM (pFsbgp4PathAttrPeer);
    UNUSED_PARAM (pi4RetValFsbgp4RfdRtLastUpdtTime);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRfdRtState
 Input       :  The Indices
                Fsbgp4PathAttrAddrPrefixAfi
                Fsbgp4PathAttrAddrPrefixSafi
                Fsbgp4PathAttrAddrPrefix
                Fsbgp4PathAttrAddrPrefixLen
                Fsbgp4PathAttrPeerType
                Fsbgp4PathAttrPeer

                The Object 
                retValFsbgp4RfdRtState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRfdRtState (INT4 i4Fsbgp4PathAttrAddrPrefixAfi,
                           INT4 i4Fsbgp4PathAttrAddrPrefixSafi,
                           tSNMP_OCTET_STRING_TYPE * pFsbgp4PathAttrAddrPrefix,
                           INT4 i4Fsbgp4PathAttrAddrPrefixLen,
                           INT4 i4Fsbgp4PathAttrPeerType,
                           tSNMP_OCTET_STRING_TYPE * pFsbgp4PathAttrPeer,
                           INT4 *pi4RetValFsbgp4RfdRtState)
{
#ifdef RFD_WANTED
    tNetAddress         RtAddress;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4PathAttrPeerType,
                                 pFsbgp4PathAttrPeer, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4SnmphGetNetAddress (i4Fsbgp4PathAttrAddrPrefixAfi,
                                    i4Fsbgp4PathAttrAddrPrefixSafi,
                                    pFsbgp4PathAttrAddrPrefix,
                                    PeerAddress, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4PathAttrAddrPrefixLen;

    i4Sts =
        Bgp4GetFsbgp4RfdDampHistTableIntegerObject (u4Context, RtAddress,
                                                    PeerAddress,
                                                    BGP4_RFD_RT_STATE_OBJECT,
                                                    pi4RetValFsbgp4RfdRtState);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Damped Route exist.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4PathAttrAddrPrefixAfi);
    UNUSED_PARAM (i4Fsbgp4PathAttrAddrPrefixSafi);
    UNUSED_PARAM (pFsbgp4PathAttrAddrPrefix);
    UNUSED_PARAM (i4Fsbgp4PathAttrAddrPrefixLen);
    UNUSED_PARAM (i4Fsbgp4PathAttrPeerType);
    UNUSED_PARAM (pFsbgp4PathAttrPeer);
    UNUSED_PARAM (pi4RetValFsbgp4RfdRtState);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRfdRtStatus
 Input       :  The Indices
                Fsbgp4PathAttrAddrPrefixAfi
                Fsbgp4PathAttrAddrPrefixSafi
                Fsbgp4PathAttrAddrPrefix
                Fsbgp4PathAttrAddrPrefixLen
                Fsbgp4PathAttrPeerType
                Fsbgp4PathAttrPeer

                The Object 
                retValFsbgp4RfdRtStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRfdRtStatus (INT4 i4Fsbgp4PathAttrAddrPrefixAfi,
                            INT4 i4Fsbgp4PathAttrAddrPrefixSafi,
                            tSNMP_OCTET_STRING_TYPE * pFsbgp4PathAttrAddrPrefix,
                            INT4 i4Fsbgp4PathAttrAddrPrefixLen,
                            INT4 i4Fsbgp4PathAttrPeerType,
                            tSNMP_OCTET_STRING_TYPE * pFsbgp4PathAttrPeer,
                            INT4 *pi4RetValFsbgp4RfdRtStatus)
{
#ifdef RFD_WANTED
    tNetAddress         RtAddress;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4PathAttrPeerType,
                                 pFsbgp4PathAttrPeer, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4SnmphGetNetAddress (i4Fsbgp4PathAttrAddrPrefixAfi,
                                    i4Fsbgp4PathAttrAddrPrefixSafi,
                                    pFsbgp4PathAttrAddrPrefix,
                                    PeerAddress, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4PathAttrAddrPrefixLen;

    i4Sts =
        Bgp4GetFsbgp4RfdDampHistTableIntegerObject (u4Context, RtAddress,
                                                    PeerAddress,
                                                    BGP4_RFD_RT_STATUS_OBJECT,
                                                    pi4RetValFsbgp4RfdRtStatus);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Damped Route exist.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4PathAttrAddrPrefixAfi);
    UNUSED_PARAM (i4Fsbgp4PathAttrAddrPrefixSafi);
    UNUSED_PARAM (pFsbgp4PathAttrAddrPrefix);
    UNUSED_PARAM (i4Fsbgp4PathAttrAddrPrefixLen);
    UNUSED_PARAM (i4Fsbgp4PathAttrPeerType);
    UNUSED_PARAM (pFsbgp4PathAttrPeer);
    UNUSED_PARAM (pi4RetValFsbgp4RfdRtStatus);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRfdRtFlapCount
 Input       :  The Indices
                Fsbgp4mpePathAttrAddrPrefixAfi
                Fsbgp4mpePathAttrAddrPrefixSafi
                Fsbgp4mpePathAttrAddrPrefix
                Fsbgp4mpePathAttrAddrPrefixLen
                Fsbgp4mpePathAttrPeerType
                Fsbgp4mpePathAttrPeer

                The Object
                retValFsbgp4mpeRfdRtFlapCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRfdRtFlapCount (INT4 i4Fsbgp4mpePathAttrAddrPrefixAfi,
                               INT4 i4Fsbgp4mpePathAttrAddrPrefixSafi,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsbgp4mpePathAttrAddrPrefix,
                               INT4 i4Fsbgp4mpePathAttrAddrPrefixLen,
                               INT4 i4Fsbgp4mpePathAttrPeerType,
                               tSNMP_OCTET_STRING_TYPE * pFsbgp4mpePathAttrPeer,
                               INT4 *pi4RetValFsbgp4mpeRfdRtFlapCount)
{
#ifdef RFD_WANTED
    tNetAddress         RtAddress;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4mpePathAttrPeerType,
                                 pFsbgp4mpePathAttrPeer, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4SnmphGetNetAddress (i4Fsbgp4mpePathAttrAddrPrefixAfi,
                                    i4Fsbgp4mpePathAttrAddrPrefixSafi,
                                    pFsbgp4mpePathAttrAddrPrefix,
                                    PeerAddress, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4mpePathAttrAddrPrefixLen;

    i4Sts =
        Bgp4GetFsbgp4RfdDampHistTableIntegerObject (u4Context, RtAddress,
                                                    PeerAddress,
                                                    BGP4_RFD_RT_FLAP_COUNT_OBJECT,
                                                    pi4RetValFsbgp4mpeRfdRtFlapCount);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Damped Route exist.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4mpePathAttrAddrPrefixAfi);
    UNUSED_PARAM (i4Fsbgp4mpePathAttrAddrPrefixSafi);
    UNUSED_PARAM (pFsbgp4mpePathAttrAddrPrefix);
    UNUSED_PARAM (i4Fsbgp4mpePathAttrAddrPrefixLen);
    UNUSED_PARAM (i4Fsbgp4mpePathAttrPeerType);
    UNUSED_PARAM (pFsbgp4mpePathAttrPeer);
    UNUSED_PARAM (pi4RetValFsbgp4mpeRfdRtFlapCount);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRfdRtFlapTime
 Input       :  The Indices
                Fsbgp4mpePathAttrAddrPrefixAfi
                Fsbgp4mpePathAttrAddrPrefixSafi
                Fsbgp4mpePathAttrAddrPrefix
                Fsbgp4mpePathAttrAddrPrefixLen
                Fsbgp4mpePathAttrPeerType
                Fsbgp4mpePathAttrPeer

                The Object
                retValFsbgp4mpeRfdRtFlapTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRfdRtFlapTime (INT4 i4Fsbgp4mpePathAttrAddrPrefixAfi,
                              INT4 i4Fsbgp4mpePathAttrAddrPrefixSafi,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsbgp4mpePathAttrAddrPrefix,
                              INT4 i4Fsbgp4mpePathAttrAddrPrefixLen,
                              INT4 i4Fsbgp4mpePathAttrPeerType,
                              tSNMP_OCTET_STRING_TYPE * pFsbgp4mpePathAttrPeer,
                              INT4 *pi4RetValFsbgp4mpeRfdRtFlapTime)
{
#ifdef RFD_WANTED
    tNetAddress         RtAddress;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4mpePathAttrPeerType,
                                 pFsbgp4mpePathAttrPeer, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4SnmphGetNetAddress (i4Fsbgp4mpePathAttrAddrPrefixAfi,
                                    i4Fsbgp4mpePathAttrAddrPrefixSafi,
                                    pFsbgp4mpePathAttrAddrPrefix,
                                    PeerAddress, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4mpePathAttrAddrPrefixLen;

    i4Sts =
        Bgp4GetFsbgp4RfdDampHistTableIntegerObject (u4Context, RtAddress,
                                                    PeerAddress,
                                                    BGP4_RFD_RT_FLAP_TIME_OBJECT,
                                                    pi4RetValFsbgp4mpeRfdRtFlapTime);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Damped Route exist.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4mpePathAttrAddrPrefixAfi);
    UNUSED_PARAM (i4Fsbgp4mpePathAttrAddrPrefixSafi);
    UNUSED_PARAM (pFsbgp4mpePathAttrAddrPrefix);
    UNUSED_PARAM (i4Fsbgp4mpePathAttrAddrPrefixLen);
    UNUSED_PARAM (i4Fsbgp4mpePathAttrPeerType);
    UNUSED_PARAM (pFsbgp4mpePathAttrPeer);
    UNUSED_PARAM (pi4RetValFsbgp4mpeRfdRtFlapTime);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRfdRtReuseTime
 Input       :  The Indices
                Fsbgp4mpePathAttrAddrPrefixAfi
                Fsbgp4mpePathAttrAddrPrefixSafi
                Fsbgp4mpePathAttrAddrPrefix
                Fsbgp4mpePathAttrAddrPrefixLen
                Fsbgp4mpePathAttrPeerType
                Fsbgp4mpePathAttrPeer

                The Object
                retValFsbgp4mpeRfdRtReuseTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRfdRtReuseTime (INT4 i4Fsbgp4mpePathAttrAddrPrefixAfi,
                               INT4 i4Fsbgp4mpePathAttrAddrPrefixSafi,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsbgp4mpePathAttrAddrPrefix,
                               INT4 i4Fsbgp4mpePathAttrAddrPrefixLen,
                               INT4 i4Fsbgp4mpePathAttrPeerType,
                               tSNMP_OCTET_STRING_TYPE * pFsbgp4mpePathAttrPeer,
                               INT4 *pi4RetValFsbgp4mpeRfdRtReuseTime)
{
#ifdef RFD_WANTED
    tNetAddress         RtAddress;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4mpePathAttrPeerType,
                                 pFsbgp4mpePathAttrPeer, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4SnmphGetNetAddress (i4Fsbgp4mpePathAttrAddrPrefixAfi,
                                    i4Fsbgp4mpePathAttrAddrPrefixSafi,
                                    pFsbgp4mpePathAttrAddrPrefix,
                                    PeerAddress, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4mpePathAttrAddrPrefixLen;

    i4Sts =
        Bgp4GetFsbgp4RfdDampHistTableIntegerObject (u4Context, RtAddress,
                                                    PeerAddress,
                                                    BGP4_RFD_RT_REUSE_TIME_OBJECT,
                                                    pi4RetValFsbgp4mpeRfdRtReuseTime);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Damped Route exist.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4mpePathAttrAddrPrefixAfi);
    UNUSED_PARAM (i4Fsbgp4mpePathAttrAddrPrefixSafi);
    UNUSED_PARAM (pFsbgp4mpePathAttrAddrPrefix);
    UNUSED_PARAM (i4Fsbgp4mpePathAttrAddrPrefixLen);
    UNUSED_PARAM (i4Fsbgp4mpePathAttrPeerType);
    UNUSED_PARAM (pFsbgp4mpePathAttrPeer);
    UNUSED_PARAM (pi4RetValFsbgp4mpeRfdRtReuseTime);
    return SNMP_FAILURE;
#endif
}

/* LOW LEVEL Routines for Table : Fsbgp4MpeRfdPeerDampHistTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4MpeRfdPeerDampHistTable
 Input       :  The Indices
                Fsbgp4PeerRemoteIpAddrType
                Fsbgp4PeerRemoteIpAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4MpeRfdPeerDampHistTable (INT4
                                                       i4Fsbgp4PeerRemoteIpAddrType,
                                                       tSNMP_OCTET_STRING_TYPE *
                                                       pFsbgp4PeerRemoteIpAddr)
{
#ifdef RFD_WANTED
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4PeerRemoteIpAddrType,
                                 pFsbgp4PeerRemoteIpAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4ValidateFsbgp4PeerDampHistTableIndex (PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4PeerRemoteIpAddrType);
    UNUSED_PARAM (pFsbgp4PeerRemoteIpAddr);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4MpeRfdPeerDampHistTable
 Input       :  The Indices
                Fsbgp4PeerRemoteIpAddrType
                Fsbgp4PeerRemoteIpAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4MpeRfdPeerDampHistTable (INT4
                                               *pi4Fsbgp4PeerRemoteIpAddrType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsbgp4PeerRemoteIpAddr)
{
#ifdef RFD_WANTED
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4GetFirstIndexFsbgp4PeerDampHistTable (u4Context, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Peer Damp History List empty.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowSetIpAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress),
                                 pFsbgp4PeerRemoteIpAddr, PeerAddress);
    *pi4Fsbgp4PeerRemoteIpAddrType = BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4Fsbgp4PeerRemoteIpAddrType);
    UNUSED_PARAM (pFsbgp4PeerRemoteIpAddr);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4MpeRfdPeerDampHistTable
 Input       :  The Indices
                Fsbgp4PeerRemoteIpAddrType
                nextFsbgp4PeerRemoteIpAddrType
                Fsbgp4PeerRemoteIpAddr
                nextFsbgp4PeerRemoteIpAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4MpeRfdPeerDampHistTable (INT4 i4Fsbgp4PeerRemoteIpAddrType,
                                              INT4
                                              *pi4NextFsbgp4PeerRemoteIpAddrType,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsbgp4PeerRemoteIpAddr,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pNextFsbgp4PeerRemoteIpAddr)
{
#ifdef RFD_WANTED
    tAddrPrefix         PeerAddress;
    tAddrPrefix         NextPeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4PeerRemoteIpAddrType,
                                 pFsbgp4PeerRemoteIpAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4GetNextIndexFsbgp4PeerDampHistTable (u4Context, PeerAddress,
                                                     &NextPeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowSetIpAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO (NextPeerAddress),
                                 pNextFsbgp4PeerRemoteIpAddr, NextPeerAddress);
    *pi4NextFsbgp4PeerRemoteIpAddrType =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (NextPeerAddress);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4PeerRemoteIpAddrType);
    UNUSED_PARAM (pFsbgp4PeerRemoteIpAddr);
    UNUSED_PARAM (pi4NextFsbgp4PeerRemoteIpAddrType);
    UNUSED_PARAM (pNextFsbgp4PeerRemoteIpAddr);
    return SNMP_FAILURE;
#endif
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRfdPeerFom
 Input       :  The Indices
                Fsbgp4PeerRemoteIpAddrType
                Fsbgp4PeerRemoteIpAddr

                The Object 
                retValFsbgp4RfdPeerFom
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRfdPeerFom (INT4 i4Fsbgp4PeerRemoteIpAddrType,
                           tSNMP_OCTET_STRING_TYPE * pFsbgp4PeerRemoteIpAddr,
                           INT4 *pi4RetValFsbgp4RfdPeerFom)
{
#ifdef RFD_WANTED
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4PeerRemoteIpAddrType,
                                 pFsbgp4PeerRemoteIpAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts =
        Bgp4GetFsbgp4PeerDampHistTableIntegerObject (u4Context, PeerAddress,
                                                     BGP4_RFD_PEER_FOM_OBJECT,
                                                     pi4RetValFsbgp4RfdPeerFom);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Damped Peer exist.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4PeerRemoteIpAddrType);
    UNUSED_PARAM (pFsbgp4PeerRemoteIpAddr);
    UNUSED_PARAM (pi4RetValFsbgp4RfdPeerFom);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRfdPeerLastUpdtTime
 Input       :  The Indices
                Fsbgp4PeerRemoteIpAddrType
                Fsbgp4PeerRemoteIpAddr

                The Object 
                retValFsbgp4RfdPeerLastUpdtTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRfdPeerLastUpdtTime (INT4 i4Fsbgp4PeerRemoteIpAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsbgp4PeerRemoteIpAddr,
                                    INT4 *pi4RetValFsbgp4RfdPeerLastUpdtTime)
{
#ifdef RFD_WANTED
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4PeerRemoteIpAddrType,
                                 pFsbgp4PeerRemoteIpAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts =
        Bgp4GetFsbgp4PeerDampHistTableIntegerObject (u4Context, PeerAddress,
                                                     BGP4_RFD_PEER_LASTUPDT_TIME_OBJECT,
                                                     pi4RetValFsbgp4RfdPeerLastUpdtTime);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer Damp History exist.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4PeerRemoteIpAddrType);
    UNUSED_PARAM (pFsbgp4PeerRemoteIpAddr);
    UNUSED_PARAM (pi4RetValFsbgp4RfdPeerLastUpdtTime);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRfdPeerState
 Input       :  The Indices
                Fsbgp4PeerRemoteIpAddrType
                Fsbgp4PeerRemoteIpAddr

                The Object 
                retValFsbgp4RfdPeerState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRfdPeerState (INT4 i4Fsbgp4PeerRemoteIpAddrType,
                             tSNMP_OCTET_STRING_TYPE * pFsbgp4PeerRemoteIpAddr,
                             INT4 *pi4RetValFsbgp4RfdPeerState)
{
#ifdef RFD_WANTED
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4PeerRemoteIpAddrType,
                                 pFsbgp4PeerRemoteIpAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts =
        Bgp4GetFsbgp4PeerDampHistTableIntegerObject (u4Context, PeerAddress,
                                                     BGP4_RFD_PEER_STATE_OBJECT,
                                                     pi4RetValFsbgp4RfdPeerState);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer Damp History exist.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4PeerRemoteIpAddrType);
    UNUSED_PARAM (pFsbgp4PeerRemoteIpAddr);
    UNUSED_PARAM (pi4RetValFsbgp4RfdPeerState);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRfdPeerStatus
 Input       :  The Indices
                Fsbgp4PeerRemoteIpAddrType
                Fsbgp4PeerRemoteIpAddr

                The Object 
                retValFsbgp4RfdPeerStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRfdPeerStatus (INT4 i4Fsbgp4PeerRemoteIpAddrType,
                              tSNMP_OCTET_STRING_TYPE * pFsbgp4PeerRemoteIpAddr,
                              INT4 *pi4RetValFsbgp4RfdPeerStatus)
{
#ifdef RFD_WANTED
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4PeerRemoteIpAddrType,
                                 pFsbgp4PeerRemoteIpAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts =
        Bgp4GetFsbgp4PeerDampHistTableIntegerObject (u4Context, PeerAddress,
                                                     BGP4_RFD_PEER_STATUS_OBJECT,
                                                     pi4RetValFsbgp4RfdPeerStatus);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer Damp History exist.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4PeerRemoteIpAddrType);
    UNUSED_PARAM (pFsbgp4PeerRemoteIpAddr);
    UNUSED_PARAM (pi4RetValFsbgp4RfdPeerStatus);
    return SNMP_FAILURE;
#endif
}

/* LOW LEVEL Routines for Table : Fsbgp4MpeRfdRtsReuseListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4MpeRfdRtsReuseListTable
 Input       :  The Indices
                Fsbgp4RtAfi
                Fsbgp4RtSafi
                Fsbgp4RtIPPrefix
                Fsbgp4RtIPPrefixLen
                Fsbgp4RfdRtsReusePeerType
                Fsbgp4PeerRemAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4MpeRfdRtsReuseListTable (INT4 i4Fsbgp4RtAfi,
                                                       INT4 i4Fsbgp4RtSafi,
                                                       tSNMP_OCTET_STRING_TYPE *
                                                       pFsbgp4RtIPPrefix,
                                                       INT4
                                                       i4Fsbgp4RtIPPrefixLen,
                                                       INT4
                                                       i4Fsbgp4RfdRtsReusePeerType,
                                                       tSNMP_OCTET_STRING_TYPE *
                                                       pFsbgp4PeerRemAddress)
{
#ifdef RFD_WANTED
    tNetAddress         RtAddress;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4RfdRtsReusePeerType,
                                 pFsbgp4PeerRemAddress, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4SnmphGetNetAddress (i4Fsbgp4RtAfi,
                                    i4Fsbgp4RtSafi,
                                    pFsbgp4RtIPPrefix, PeerAddress, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4RtIPPrefixLen;

    i4Sts =
        Bgp4ValidateFsbgp4RfdRtsReuseListTableIndex (RtAddress, PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid RFD Route Reuse List Table index.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4RtAfi);
    UNUSED_PARAM (i4Fsbgp4RtSafi);
    UNUSED_PARAM (pFsbgp4RtIPPrefix);
    UNUSED_PARAM (i4Fsbgp4RtIPPrefixLen);
    UNUSED_PARAM (i4Fsbgp4RfdRtsReusePeerType);
    UNUSED_PARAM (pFsbgp4PeerRemAddress);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4MpeRfdRtsReuseListTable
 Input       :  The Indices
                Fsbgp4RtAfi
                Fsbgp4RtSafi
                Fsbgp4RtIPPrefix
                Fsbgp4RtIPPrefixLen
                Fsbgp4RfdRtsReusePeerType
                Fsbgp4PeerRemAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4MpeRfdRtsReuseListTable (INT4 *pi4Fsbgp4RtAfi,
                                               INT4 *pi4Fsbgp4RtSafi,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsbgp4RtIPPrefix,
                                               INT4 *pi4Fsbgp4RtIPPrefixLen,
                                               INT4
                                               *pi4Fsbgp4RfdRtsReusePeerType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsbgp4PeerRemAddress)
{
#ifdef RFD_WANTED
    tNetAddress         RtAddress;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts =
        Bgp4GetFirstIndexFsbgp4RfdRtsReuseListTable (u4Context, &RtAddress,
                                                     &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4Fsbgp4RtAfi =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                      (RtAddress));
    *pi4Fsbgp4RtSafi = BGP4_SAFI_IN_NET_ADDRESS_INFO (RtAddress);
    *pi4Fsbgp4RtIPPrefixLen = BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress);
    i4Sts =
        Bgp4LowSetRouteAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO
                                (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                 (RtAddress)),
                                BGP4_SAFI_IN_NET_ADDRESS_INFO (RtAddress),
                                pFsbgp4RtIPPrefix, RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4Fsbgp4RfdRtsReusePeerType = BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress);
    i4Sts = Bgp4LowGetIpAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress),
                                 pFsbgp4PeerRemAddress, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4Fsbgp4RtAfi);
    UNUSED_PARAM (pi4Fsbgp4RtSafi);
    UNUSED_PARAM (pFsbgp4RtIPPrefix);
    UNUSED_PARAM (pi4Fsbgp4RtIPPrefixLen);
    UNUSED_PARAM (pi4Fsbgp4RfdRtsReusePeerType);
    UNUSED_PARAM (pFsbgp4PeerRemAddress);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgpMpe4RfdRtsReuseListTable
 Input       :  The Indices
                Fsbgp4RtAfi
                nextFsbgp4RtAfi
                Fsbgp4RtSafi
                nextFsbgp4RtSafi
                Fsbgp4RtIPPrefix
                nextFsbgp4RtIPPrefix
                Fsbgp4RtIPPrefixLen
                nextFsbgp4RtIPPrefixLen
                Fsbgp4RfdRtsReusePeerType
                nextFsbgp4RfdRtsReusePeerType
                Fsbgp4PeerRemAddress
                nextFsbgp4PeerRemAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4MpeRfdRtsReuseListTable (INT4 i4Fsbgp4RtAfi,
                                              INT4 *pi4NextFsbgp4RtAfi,
                                              INT4 i4Fsbgp4RtSafi,
                                              INT4 *pi4NextFsbgp4RtSafi,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsbgp4RtIPPrefix,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pNextFsbgp4RtIPPrefix,
                                              INT4 i4Fsbgp4RtIPPrefixLen,
                                              INT4 *pi4NextFsbgp4RtIPPrefixLen,
                                              INT4 i4Fsbgp4RfdRtsReusePeerType,
                                              INT4
                                              *pi4NextFsbgp4RfdRtsReusePeerType,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsbgp4PeerRemAddress,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pNextFsbgp4PeerRemAddress)
{
#ifdef RFD_WANTED
    tNetAddress         RtAddress;
    tNetAddress         NextRtAddress;
    tAddrPrefix         PeerAddress;
    tAddrPrefix         NextPeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4RfdRtsReusePeerType,
                                 pFsbgp4PeerRemAddress, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4SnmphGetNetAddress (i4Fsbgp4RtAfi,
                                    i4Fsbgp4RtSafi,
                                    pFsbgp4RtIPPrefix, PeerAddress, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4RtIPPrefixLen;

    i4Sts =
        Bgp4GetNextIndexFsbgp4RfdRtsReuseListTable (u4Context, RtAddress,
                                                    PeerAddress, &NextRtAddress,
                                                    &NextPeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsbgp4RtAfi =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                      (NextRtAddress));
    *pi4NextFsbgp4RtSafi = BGP4_SAFI_IN_NET_ADDRESS_INFO (NextRtAddress);
    *pi4NextFsbgp4RtIPPrefixLen =
        BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NextRtAddress);
    i4Sts =
        Bgp4LowSetRouteAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO
                                (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                 (NextRtAddress)),
                                BGP4_SAFI_IN_NET_ADDRESS_INFO (NextRtAddress),
                                pNextFsbgp4RtIPPrefix, NextRtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsbgp4RfdRtsReusePeerType =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (NextPeerAddress);
    i4Sts = Bgp4LowGetIpAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO (NextPeerAddress),
                                 pNextFsbgp4PeerRemAddress, &NextPeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4RtAfi);
    UNUSED_PARAM (i4Fsbgp4RtSafi);
    UNUSED_PARAM (pFsbgp4RtIPPrefix);
    UNUSED_PARAM (i4Fsbgp4RtIPPrefixLen);
    UNUSED_PARAM (i4Fsbgp4RfdRtsReusePeerType);
    UNUSED_PARAM (pFsbgp4PeerRemAddress);
    UNUSED_PARAM (pi4NextFsbgp4RtAfi);
    UNUSED_PARAM (pi4NextFsbgp4RtSafi);
    UNUSED_PARAM (pNextFsbgp4RtIPPrefix);
    UNUSED_PARAM (pi4NextFsbgp4RtIPPrefixLen);
    UNUSED_PARAM (pi4NextFsbgp4RfdRtsReusePeerType);
    UNUSED_PARAM (pNextFsbgp4PeerRemAddress);
    return SNMP_FAILURE;
#endif
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRfdRtReuseListRtFom
 Input       :  The Indices
                Fsbgp4RtAfi
                Fsbgp4RtSafi
                Fsbgp4RtIPPrefix
                Fsbgp4RtIPPrefixLen
                Fsbgp4RfdRtsReusePeerType
                Fsbgp4PeerRemAddress

                The Object 
                retValFsbgp4RfdRtReuseListRtFom
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRfdRtReuseListRtFom (INT4 i4Fsbgp4RtAfi,
                                    INT4 i4Fsbgp4RtSafi,
                                    tSNMP_OCTET_STRING_TYPE * pFsbgp4RtIPPrefix,
                                    INT4 i4Fsbgp4RtIPPrefixLen,
                                    INT4 i4Fsbgp4RfdRtsReusePeerType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsbgp4PeerRemAddress,
                                    INT4 *pi4RetValFsbgp4RfdRtReuseListRtFom)
{
#ifdef RFD_WANTED
    tNetAddress         RtAddress;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4RfdRtsReusePeerType,
                                 pFsbgp4PeerRemAddress, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4SnmphGetNetAddress (i4Fsbgp4RtAfi,
                                    i4Fsbgp4RtSafi,
                                    pFsbgp4RtIPPrefix, PeerAddress, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4RtIPPrefixLen;

    i4Sts =
        Bgp4GetFsbgp4RfdRtsReuseListTableIntegerObject (u4Context, RtAddress,
                                                        PeerAddress,
                                                        BGP4_RFD_REUSE_RT_FOM_OBJECT,
                                                        pi4RetValFsbgp4RfdRtReuseListRtFom);

    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Route exist in RFD "
                  "Route Damp History.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4RtAfi);
    UNUSED_PARAM (i4Fsbgp4RtSafi);
    UNUSED_PARAM (pFsbgp4RtIPPrefix);
    UNUSED_PARAM (i4Fsbgp4RtIPPrefixLen);
    UNUSED_PARAM (i4Fsbgp4RfdRtsReusePeerType);
    UNUSED_PARAM (pFsbgp4PeerRemAddress);
    UNUSED_PARAM (pi4RetValFsbgp4RfdRtReuseListRtFom);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRfdRtReuseListRtLastUpdtTime
 Input       :  The Indices
                Fsbgp4RtAfi
                Fsbgp4RtSafi
                Fsbgp4RtIPPrefix
                Fsbgp4RtIPPrefixLen
                Fsbgp4RfdRtsReusePeerType
                Fsbgp4PeerRemAddress

                The Object 
                retValFsbgp4RfdRtReuseListRtLastUpdtTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRfdRtReuseListRtLastUpdtTime (INT4 i4Fsbgp4RtAfi,
                                             INT4 i4Fsbgp4RtSafi,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsbgp4RtIPPrefix,
                                             INT4 i4Fsbgp4RtIPPrefixLen,
                                             INT4 i4Fsbgp4RfdRtsReusePeerType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsbgp4PeerRemAddress,
                                             INT4
                                             *pi4RetValFsbgp4RfdRtReuseListRtLastUpdtTime)
{
#ifdef RFD_WANTED
    tNetAddress         RtAddress;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4RfdRtsReusePeerType,
                                 pFsbgp4PeerRemAddress, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4SnmphGetNetAddress (i4Fsbgp4RtAfi,
                                    i4Fsbgp4RtSafi,
                                    pFsbgp4RtIPPrefix, PeerAddress, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4RtIPPrefixLen;

    i4Sts =
        Bgp4GetFsbgp4RfdRtsReuseListTableIntegerObject (u4Context, RtAddress,
                                                        PeerAddress,
                                                        BGP4_RFD_REUSE_RT_LASTUPDT_TIME_OBJECT,
                                                        pi4RetValFsbgp4RfdRtReuseListRtLastUpdtTime);

    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Route exist in Routes "
                  "Damp History.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4RtAfi);
    UNUSED_PARAM (i4Fsbgp4RtSafi);
    UNUSED_PARAM (pFsbgp4RtIPPrefix);
    UNUSED_PARAM (i4Fsbgp4RtIPPrefixLen);
    UNUSED_PARAM (i4Fsbgp4RfdRtsReusePeerType);
    UNUSED_PARAM (pFsbgp4PeerRemAddress);
    UNUSED_PARAM (pi4RetValFsbgp4RfdRtReuseListRtLastUpdtTime);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRfdRtReuseListRtState
 Input       :  The Indices
                Fsbgp4RtAfi
                Fsbgp4RtSafi
                Fsbgp4RtIPPrefix
                Fsbgp4RtIPPrefixLen
                Fsbgp4RfdRtsReusePeerType
                Fsbgp4PeerRemAddress

                The Object 
                retValFsbgp4RfdRtReuseListRtState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRfdRtReuseListRtState (INT4 i4Fsbgp4RtAfi,
                                      INT4 i4Fsbgp4RtSafi,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsbgp4RtIPPrefix,
                                      INT4 i4Fsbgp4RtIPPrefixLen,
                                      INT4 i4Fsbgp4RfdRtsReusePeerType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsbgp4PeerRemAddress,
                                      INT4
                                      *pi4RetValFsbgp4RfdRtReuseListRtState)
{
#ifdef RFD_WANTED
    tNetAddress         RtAddress;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4RfdRtsReusePeerType,
                                 pFsbgp4PeerRemAddress, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4SnmphGetNetAddress (i4Fsbgp4RtAfi,
                                    i4Fsbgp4RtSafi,
                                    pFsbgp4RtIPPrefix, PeerAddress, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4RtIPPrefixLen;

    i4Sts =
        Bgp4GetFsbgp4RfdRtsReuseListTableIntegerObject (u4Context, RtAddress,
                                                        PeerAddress,
                                                        BGP4_RFD_REUSE_RT_STATE_OBJECT,
                                                        pi4RetValFsbgp4RfdRtReuseListRtState);

    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching route exist in Route Damp History.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4RtAfi);
    UNUSED_PARAM (i4Fsbgp4RtSafi);
    UNUSED_PARAM (pFsbgp4RtIPPrefix);
    UNUSED_PARAM (i4Fsbgp4RtIPPrefixLen);
    UNUSED_PARAM (i4Fsbgp4RfdRtsReusePeerType);
    UNUSED_PARAM (pFsbgp4PeerRemAddress);
    UNUSED_PARAM (pi4RetValFsbgp4RfdRtReuseListRtState);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRfdRtReuseListRtStatus
 Input       :  The Indices
                Fsbgp4RtAfi
                Fsbgp4RtSafi
                Fsbgp4RtIPPrefix
                Fsbgp4RtIPPrefixLen
                Fsbgp4RfdRtsReusePeerType
                Fsbgp4PeerRemAddress

                The Object 
                retValFsbgp4RfdRtReuseListRtStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRfdRtReuseListRtStatus (INT4 i4Fsbgp4RtAfi,
                                       INT4 i4Fsbgp4RtSafi,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsbgp4RtIPPrefix,
                                       INT4 i4Fsbgp4RtIPPrefixLen,
                                       INT4 i4Fsbgp4RfdRtsReusePeerType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsbgp4PeerRemAddress,
                                       INT4
                                       *pi4RetValFsbgp4RfdRtReuseListRtStatus)
{
#ifdef RFD_WANTED
    tNetAddress         RtAddress;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4RfdRtsReusePeerType,
                                 pFsbgp4PeerRemAddress, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4SnmphGetNetAddress (i4Fsbgp4RtAfi,
                                    i4Fsbgp4RtSafi,
                                    pFsbgp4RtIPPrefix, PeerAddress, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4RtIPPrefixLen;

    i4Sts =
        Bgp4GetFsbgp4RfdRtsReuseListTableIntegerObject (u4Context, RtAddress,
                                                        PeerAddress,
                                                        BGP4_RFD_REUSE_RT_STATUS_OBJECT,
                                                        pi4RetValFsbgp4RfdRtReuseListRtStatus);

    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Route exist in Route Damp History.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4RtAfi);
    UNUSED_PARAM (i4Fsbgp4RtSafi);
    UNUSED_PARAM (pFsbgp4RtIPPrefix);
    UNUSED_PARAM (i4Fsbgp4RtIPPrefixLen);
    UNUSED_PARAM (i4Fsbgp4RfdRtsReusePeerType);
    UNUSED_PARAM (pFsbgp4PeerRemAddress);
    UNUSED_PARAM (pi4RetValFsbgp4RfdRtReuseListRtStatus);
    return SNMP_FAILURE;
#endif
}

/* LOW LEVEL Routines for Table : Fsbgp4MpeRfdPeerReuseListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4MpeRfdPeerReuseListTable
 Input       :  The Indices
                Fsbgp4RfdPeerRemIpAddrType
                Fsbgp4RfdPeerRemIpAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4MpeRfdPeerReuseListTable (INT4
                                                        i4Fsbgp4RfdPeerRemIpAddrType,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pFsbgp4RfdPeerRemIpAddr)
{
#ifdef RFD_WANTED
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4RfdPeerRemIpAddrType,
                                 pFsbgp4RfdPeerRemIpAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4ValidateFsbgp4RfdPeerReuseListTableIndex (PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid RFD Route Reuse List Table index.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4RfdPeerRemIpAddrType);
    UNUSED_PARAM (pFsbgp4RfdPeerRemIpAddr);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4MpeRfdPeerReuseListTable
 Input       :  The Indices
                Fsbgp4RfdPeerRemIpAddrType
                Fsbgp4RfdPeerRemIpAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4MpeRfdPeerReuseListTable (INT4
                                                *pi4Fsbgp4RfdPeerRemIpAddrType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsbgp4RfdPeerRemIpAddr)
{
#ifdef RFD_WANTED
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts =
        Bgp4GetFirstIndexFsbgp4RfdPeerReuseListTable (u4Context, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowSetIpAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress),
                                 pFsbgp4RfdPeerRemIpAddr, PeerAddress);
    *pi4Fsbgp4RfdPeerRemIpAddrType = BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pi4Fsbgp4RfdPeerRemIpAddrType);
    UNUSED_PARAM (pFsbgp4RfdPeerRemIpAddr);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4MpeRfdPeerReuseListTable
 Input       :  The Indices
                Fsbgp4RfdPeerRemIpAddrType
                nextFsbgp4RfdPeerRemIpAddrType
                Fsbgp4RfdPeerRemIpAddr
                nextFsbgp4RfdPeerRemIpAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4MpeRfdPeerReuseListTable (INT4
                                               i4Fsbgp4RfdPeerRemIpAddrType,
                                               INT4
                                               *pi4NextFsbgp4RfdPeerRemIpAddrType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsbgp4RfdPeerRemIpAddr,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pNextFsbgp4RfdPeerRemIpAddr)
{
#ifdef RFD_WANTED
    tAddrPrefix         PeerAddress;
    tAddrPrefix         NextPeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4RfdPeerRemIpAddrType,
                                 pFsbgp4RfdPeerRemIpAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4GetNextIndexFsbgp4RfdPeerReuseListTable (u4Context, PeerAddress,
                                                         &NextPeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowSetIpAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO (NextPeerAddress),
                                 pNextFsbgp4RfdPeerRemIpAddr, NextPeerAddress);
    *pi4NextFsbgp4RfdPeerRemIpAddrType =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (NextPeerAddress);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4RfdPeerRemIpAddrType);
    UNUSED_PARAM (pFsbgp4RfdPeerRemIpAddr);
    UNUSED_PARAM (pi4NextFsbgp4RfdPeerRemIpAddrType);
    UNUSED_PARAM (pNextFsbgp4RfdPeerRemIpAddr);
    return SNMP_FAILURE;
#endif
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRfdPeerReuseListPeerFom
 Input       :  The Indices
                Fsbgp4RfdPeerRemIpAddrType
                Fsbgp4RfdPeerRemIpAddr

                The Object 
                retValFsbgp4RfdPeerReuseListPeerFom
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRfdPeerReuseListPeerFom (INT4 i4Fsbgp4RfdPeerRemIpAddrType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsbgp4RfdPeerRemIpAddr,
                                        INT4
                                        *pi4RetValFsbgp4RfdPeerReuseListPeerFom)
{
#ifdef RFD_WANTED
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4RfdPeerRemIpAddrType,
                                 pFsbgp4RfdPeerRemIpAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts =
        Bgp4GetFsbgp4RfdPeerReuseListTableIntegerObject (u4Context, PeerAddress,
                                                         BGP4_RFD_REUSE_PEER_FOM_OBJECT,
                                                         pi4RetValFsbgp4RfdPeerReuseListPeerFom);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist in Peer Damp History.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4RfdPeerRemIpAddrType);
    UNUSED_PARAM (pFsbgp4RfdPeerRemIpAddr);
    UNUSED_PARAM (pi4RetValFsbgp4RfdPeerReuseListPeerFom);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRfdPeerReuseListLastUpdtTime
 Input       :  The Indices
                Fsbgp4RfdPeerRemIpAddrType
                Fsbgp4RfdPeerRemIpAddr

                The Object 
                retValFsbgp4RfdPeerReuseListLastUpdtTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRfdPeerReuseListLastUpdtTime (INT4 i4Fsbgp4RfdPeerRemIpAddrType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsbgp4RfdPeerRemIpAddr,
                                             INT4
                                             *pi4RetValFsbgp4RfdPeerReuseListLastUpdtTime)
{
#ifdef RFD_WANTED
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4RfdPeerRemIpAddrType,
                                 pFsbgp4RfdPeerRemIpAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts =
        Bgp4GetFsbgp4RfdPeerReuseListTableIntegerObject (u4Context, PeerAddress,
                                                         BGP4_RFD_REUSE_PEER_LASTUPDT_TIME_OBJECT,
                                                         pi4RetValFsbgp4RfdPeerReuseListLastUpdtTime);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching peer exist in Peer Damp "
                  "History List.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4RfdPeerRemIpAddrType);
    UNUSED_PARAM (pFsbgp4RfdPeerRemIpAddr);
    UNUSED_PARAM (pi4RetValFsbgp4RfdPeerReuseListLastUpdtTime);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRfdPeerReuseListPeerState
 Input       :  The Indices
                Fsbgp4RfdPeerRemIpAddrType
                Fsbgp4RfdPeerRemIpAddr

                The Object 
                retValFsbgp4RfdPeerReuseListPeerState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRfdPeerReuseListPeerState (INT4 i4Fsbgp4RfdPeerRemIpAddrType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsbgp4RfdPeerRemIpAddr,
                                          INT4
                                          *pi4RetValFsbgp4RfdPeerReuseListPeerState)
{
#ifdef RFD_WANTED
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4RfdPeerRemIpAddrType,
                                 pFsbgp4RfdPeerRemIpAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts =
        Bgp4GetFsbgp4RfdPeerReuseListTableIntegerObject (u4Context, PeerAddress,
                                                         BGP4_RFD_REUSE_PEER_STATE_OBJECT,
                                                         pi4RetValFsbgp4RfdPeerReuseListPeerState);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching peer exist in Peer Damp History.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4RfdPeerRemIpAddrType);
    UNUSED_PARAM (pFsbgp4RfdPeerRemIpAddr);
    UNUSED_PARAM (pi4RetValFsbgp4RfdPeerReuseListPeerState);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRfdPeerReuseListPeerStatus
 Input       :  The Indices
                Fsbgp4RfdPeerRemIpAddrType
                Fsbgp4RfdPeerRemIpAddr

                The Object 
                retValFsbgp4RfdPeerReuseListPeerStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRfdPeerReuseListPeerStatus (INT4 i4Fsbgp4RfdPeerRemIpAddrType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsbgp4RfdPeerRemIpAddr,
                                           INT4
                                           *pi4RetValFsbgp4RfdPeerReuseListPeerStatus)
{
#ifdef RFD_WANTED
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4RfdPeerRemIpAddrType,
                                 pFsbgp4RfdPeerRemIpAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts =
        Bgp4GetFsbgp4RfdPeerReuseListTableIntegerObject (u4Context, PeerAddress,
                                                         BGP4_RFD_REUSE_PEER_STATUS_OBJECT,
                                                         pi4RetValFsbgp4RfdPeerReuseListPeerStatus);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist in Peer Reuse List.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4RfdPeerRemIpAddrType);
    UNUSED_PARAM (pFsbgp4RfdPeerRemIpAddr);
    UNUSED_PARAM (pi4RetValFsbgp4RfdPeerReuseListPeerStatus);
    return SNMP_FAILURE;
#endif
}
