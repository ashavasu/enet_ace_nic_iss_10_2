/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgecfilt.c,v 1.11 2015/09/04 13:08:29 siva Exp $
 *
 * Description: This file contains Extended Community Attribute
 *              processing functions.
 *
 *******************************************************************/
#ifndef _BGECFILT_C
#define _BGECFILT_C

#include "bgp4com.h"

/*****************************************************************************/
/*    Function Name   : EcommApplyRoutesInboundFilterPolicy                  */
/*                                                                           */
/*    Description     : Validates the input for ext-comm path attribute      */
/*                      present in the given route profile. Applies the      */
/*                      inbound filter policy if any configured and returns  */
/*                      the filtered results.                                */
/*                                                                           */
/*    Input(s)        : pFeasibleRoute - pointer to the feasible             */
/*                      route profile, that is to be processed for           */
/*                      extended-community path attribute.                   */
/*                                                                           */
/*    Output(s)       : None.                                                */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*  Returns         :  EXT_COMM_SUCCESS - If route is not filtered           */
/*                     EXT_COMM_FAILURE - If route is filtered               */
/*****************************************************************************/
INT4
EcommApplyRoutesInboundFilterPolicy (tRouteProfile * pFeasibleRoute)
{
    tBgp4Info          *pRtInfo = NULL;
    UINT2               u2ExtCommPathStartOffSet;
    INT1                i1ExtCommInputFilterStatus;

    u2ExtCommPathStartOffSet = 0;
    i1ExtCommInputFilterStatus = ACCEPT_INCOMING_EXT_COMM;

    if ((BGP4_RT_PROTOCOL (pFeasibleRoute)) == BGP_ID)
    {
        pRtInfo = BGP4_RT_BGP_INFO (pFeasibleRoute);

        /* Check for the presence of ext-comm  path attribute */
        if (BGP4_INFO_ECOMM_ATTR (pRtInfo) == NULL)
        {
            /* ext-comm path attribute not present. Return all the routes
             * in the list for installation. */
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tCheckExtCommAttribute() : Ext-Comm Attrib not present\n");
            return EXT_COMM_SUCCESS;
        }
        else
        {
            /* Ext-Comm is present. */
            u2ExtCommPathStartOffSet = 0;
#ifdef VPLSADS_WANTED
            if ((BGP4_RT_AFI_INFO (pFeasibleRoute) == BGP4_INET_AFI_L2VPN) &&
                (BGP4_RT_SAFI_INFO (pFeasibleRoute) == BGP4_INET_SAFI_VPLS))
            {
                /* l2vpn,vpls routes will get checked against import RT List */
                return EXT_COMM_SUCCESS;
            }
#endif
#ifdef EVPN_WANTED
            if ((BGP4_RT_AFI_INFO (pFeasibleRoute) == BGP4_INET_AFI_L2VPN) &&
                (BGP4_RT_SAFI_INFO (pFeasibleRoute) == BGP4_INET_SAFI_EVPN))
            {
                /* L2VPN,EVPN routes will get checked against import RT List */
                return EXT_COMM_SUCCESS;
            }
#endif
        }
    }
    else
    {
        /* The route is not learned via BGP Peer. */
        return EXT_COMM_SUCCESS;
    }

    /* Ext-Comm  path exists so start processing */
    i1ExtCommInputFilterStatus =
        ExtCommGetInputFilterStatus (pFeasibleRoute, u2ExtCommPathStartOffSet);
    if (i1ExtCommInputFilterStatus == ACCEPT_INCOMING_EXT_COMM)
    {
        /* After applying incoming ext-comm  filter the list
         * of routes is found to be accepted. */
        return EXT_COMM_SUCCESS;
    }
    else
    {
        /* Route is filtered based on Extended-Community */
        return EXT_COMM_FAILURE;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name   : ExtCommFormListForInst                               */
/*                                                                           */
/*    Description     : Validates the input for extended community path      */
/*                      attribute.                                           */
/*                      Applies input extended community filtering and gives */
/*                      the accepted list of route profiles.                 */
/*                                                                           */
/*    Input(s)        : pFeasibleRoutesList - pointer to the list of feasible*/
/*                      route profiles, that are to be processed for         */
/*                      extended community path attribute. It's SLL of type  */
/*                      tLinkNode.                                           */
/*                                                                           */
/*                      pPeerInfo - Pointer to peer Information              */
/*                                                                           */
/*    Output(s)       : None.                                                */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*  Returns         :  EXT_COMM_SUCCESS - if the routes are not filtered.    */
/*                     EXT_COMM_FAILURE - if the routes are filtered out.    */
/*****************************************************************************/
INT4
ExtCommFormListForInst (tTMO_SLL * pFeasibleRoutesList,
                        tBgp4PeerEntry * pPeerInfo)
{
    tRouteProfile      *pFeasibleRoute = NULL;
    tLinkNode          *pLinkNode = NULL;
    INT4                i4RetVal;

    UNUSED_PARAM (pPeerInfo);
    /* It is necessary that the Feasible Route List contains list of routes
     * that have identical path attributes. Hence it is sufficient to
     * process one route and decide whether to filter the entire list
     * or not. */
    pLinkNode = ((tLinkNode *) TMO_SLL_First (pFeasibleRoutesList));
    if (pLinkNode == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                  BGP4_MOD_NAME,
                  "\tExtCommFormListForInst() : RECEIVED ZERO FEASIBLE "
                  "ROUTES\n");
        return EXT_COMM_FAILURE;
    }

    pFeasibleRoute = pLinkNode->pRouteProfile;
    i4RetVal = EcommApplyRoutesInboundFilterPolicy (pFeasibleRoute);
    if (i4RetVal == EXT_COMM_SUCCESS)
    {
        /* Route is not filtered out. All routes in the list also shall
         * not be filtered since all are received in the same update
         * message. */
        return EXT_COMM_SUCCESS;
    }
    else
    {
        /* Route is filtered out. */
        TMO_SLL_Scan (pFeasibleRoutesList, pLinkNode, tLinkNode *)
        {
            pFeasibleRoute = pLinkNode->pRouteProfile;
            BGP4_RT_SET_FLAG (pFeasibleRoute, BGP4_RT_FILTERED_INPUT);
        }
        return EXT_COMM_FAILURE;
    }
}

/*****************************************************************************/
/*    Function Name   : ExtCommApplyOutboundFilterPolicy                     */
/*    Description     : Applies extended community output filtering policy   */
/*                      for given input route.                               */
/*                                                                           */
/*    Input(s)        : pRtProfile - Pointer to the route profile            */
/*                                                                           */
/*                      pPeerInfo - Pointer to peer Information              */
/*                                                                           */
/*    Output(s)       : pRtProfile - Updated for whether the route is        */
/*                                   filtered or not.                        */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*  Returns         :  COMM_SUCCESS or COMM_FAILURE                          */
/*****************************************************************************/
INT4
ExtCommApplyOutboundFilterPolicy (tRouteProfile * pRtProfile,
                                  tBgp4PeerEntry * pPeerInfo)
{
    tBgp4Info          *pRtInfo = NULL;
    UINT2               u2ExtCommPathStartOffSet = 0;
    INT1                i1ExtCommOutputFilterStatus;

    UNUSED_PARAM (pPeerInfo);

    if ((BGP4_RT_PROTOCOL (pRtProfile)) == BGP_ID)
    {
        pRtInfo = BGP4_RT_BGP_INFO (pRtProfile);

        /* Check for the presence of ext-comm  path attribute */
        if (BGP4_INFO_ECOMM_ATTR (pRtInfo) != NULL)
        {
            /* Attribute is present, so apply ext-comm outgoing
             * filtering
             */

            i1ExtCommOutputFilterStatus =
                ExtCommGetOutputFilterStatus (pRtProfile,
                                              u2ExtCommPathStartOffSet);
            switch (i1ExtCommOutputFilterStatus)
            {
                case ADVERTISE_OUTGOING_EXT_COMM:
                    /* The routes can be advertised. */
                    break;

                case FILTER_OUTGOING_EXT_COMM:
                    /* Route is found to be filtered             */
                    BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_FILTERED_OUTPUT);
                    break;

                default:
                    break;
            }
        }
    }

    return COMM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name   : ExtCommGetInputFilterStatus                          */
/*                                                                           */
/*    Description     : Gets the Input Filter Status of the route            */
/*                      after applying ext-comm (s) input filter policy.     */
/*    Input(s)        : pFeasibleRoute - pointer to the route profile that is*/
/*                      to be processed.                                     */
/*                                                                           */
/*    Output(s)       : u2ExtCommPathStartOffSet - Holds the offset,         */
/*                      from where  the ext-comm  path attribute starts.     */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred : EXT_COMM_INPUT_FILTER_TBL                    */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*  Returns         :                                                        */
/*     ACCEPT_INCOMING_EXT_COMM /  DENY_INCOMING_EXT_COMM                    */
/*****************************************************************************/
INT1
ExtCommGetInputFilterStatus (const tRouteProfile * pFeasibleRoute,
                             UINT2 u2ExtCommPathStartOffSet)
{
    tBgp4Info          *pRouteInfo = NULL;
    tExtCommFilterInfo *pExtCommFilterInfo = NULL;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT4               u4Index = 0;
    tExtCommFilterInfo  ExtCommFilterInfo;
    INT4                i4RetSts = EXT_COMM_FAILURE;
    UINT2               u2HashKey;
    INT1                i1ExtCommInputFilterStatus;
    INT1                i1TempInputFilterStatus;

    UNUSED_PARAM (u2ExtCommPathStartOffSet);
    i1ExtCommInputFilterStatus = ACCEPT_INCOMING_EXT_COMM;
    i1TempInputFilterStatus = ACCEPT_INCOMING_EXT_COMM;

    if (pFeasibleRoute == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                  BGP4_MOD_NAME,
                  "\tExtCommGetInputFilterStatus() : Received NULL Route "
                  "Profile\n");
        return EXT_COMM_FAILURE;
    }

    pRouteInfo = BGP4_RT_BGP_INFO (pFeasibleRoute);

    /* Ext-Comm  path exists so start processing */
    for (u4Index = 0; u4Index < BGP4_INFO_ECOMM_COUNT (pRouteInfo); u4Index++)
    {
        MEMCPY ((UINT1 *) au1ExtComm,
                (UINT1 *) (BGP4_INFO_ECOMM_ATTR_VAL (pRouteInfo) +
                           +(u4Index * EXT_COMM_VALUE_LEN)),
                EXT_COMM_VALUE_LEN);

        i1TempInputFilterStatus = ACCEPT_INCOMING_EXT_COMM;

        PTR_FETCH2 (u2HashKey, au1ExtComm);
        i4RetSts = ExtCommValidateExtCommunityType (u2HashKey);
        if (i4RetSts == EXT_COMM_FAILURE)
        {
            i1TempInputFilterStatus = DENY_INCOMING_EXT_COMM;
            return i1TempInputFilterStatus;
        }
        MEMCPY (ExtCommFilterInfo.au1ExtComm, au1ExtComm, EXT_COMM_VALUE_LEN);
        ExtCommFilterInfo.u4Context = BGP4_RT_CXT_ID (pFeasibleRoute);
        pExtCommFilterInfo = BGP4_RB_TREE_GET (EXT_COMM_INPUT_FILTER_TBL,
                                               (tBgp4RBElem
                                                *) (&ExtCommFilterInfo));
        if (pExtCommFilterInfo != NULL)
        {
            i1TempInputFilterStatus = pExtCommFilterInfo->i1FilterPolicy;
        }

        if (i1TempInputFilterStatus == DENY_INCOMING_EXT_COMM)
        {
            return i1TempInputFilterStatus;
        }

        i1ExtCommInputFilterStatus =
            i1ExtCommInputFilterStatus & i1TempInputFilterStatus;

    }
    return i1ExtCommInputFilterStatus;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name   : ExtCommGetOutputFilterStatus                         */
/*                                                                           */
/*    Description     : Gets the Output Filter Status of the route           */
/*                      after applying ext-comm (s) input filter policy.     */
/*                                                                           */
/*    Input(s)        : pBestRoute - pointer to the route profile that is    */
/*                      to be processed.                                     */
/*                                                                           */
/*    Output(s)       : u2ExtCommPathStartOffSet - Holds the offset,         */
/*                      from where  the ext-comm  path attribute starts.     */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred : EXT_COMM_OUTPUT_FILTER_TBL                   */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*  Returns         :                                                        */
/*     ADVERTISE_OUTGOING_EXT_COMM /  FILTER_OUTGOING_EXT_COMM               */
/*****************************************************************************/
INT1
ExtCommGetOutputFilterStatus (const tRouteProfile * pBestRoute,
                              UINT2 u2ExtCommPathStartOffSet)
{
    tBgp4Info          *pRouteInfo = NULL;
    tExtCommFilterInfo *pExtCommFilterInfo = NULL;
    tExtCommFilterInfo  ExtCommFilterInfo;
    UINT4               u4Index = 0;
    INT1                i1ExtCommOutputFilterStatus;
    INT1                i1TempOutputFilterStatus;
    INT4                i4RetSts = EXT_COMM_FAILURE;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT2               u2HashKey;

    UNUSED_PARAM (u2ExtCommPathStartOffSet);
    i1ExtCommOutputFilterStatus = ADVERTISE_OUTGOING_EXT_COMM;
    i1TempOutputFilterStatus = ADVERTISE_OUTGOING_EXT_COMM;

    if (pBestRoute == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                  BGP4_MOD_NAME,
                  "\tExtCommGetOutputFilterStatus() : Received NULL Route "
                  "Profile\n");
        return EXT_COMM_FAILURE;
    }

    pRouteInfo = BGP4_RT_BGP_INFO (pBestRoute);

    /* Ext-Comm  path exists so start processing */
    for (u4Index = 0; u4Index < BGP4_INFO_ECOMM_COUNT (pRouteInfo); u4Index++)
    {
        MEMCPY ((UINT1 *) au1ExtComm,
                (UINT1 *) (BGP4_INFO_ECOMM_ATTR_VAL (pRouteInfo) +
                           +(u4Index * EXT_COMM_VALUE_LEN)),
                EXT_COMM_VALUE_LEN);
        i1TempOutputFilterStatus = ADVERTISE_OUTGOING_EXT_COMM;
        PTR_FETCH2 (u2HashKey, au1ExtComm);
        i4RetSts = ExtCommValidateExtCommunityType (u2HashKey);
        if (i4RetSts == EXT_COMM_FAILURE)
        {
            i1TempOutputFilterStatus = FILTER_OUTGOING_EXT_COMM;
            return i1TempOutputFilterStatus;
        }
#ifdef VPLSADS_WANTED
        if (u2HashKey == ECOMM_VALUE_LAYER2_INFO)
        {
            /*Layer 2 PE info is Ext.Comm but not present in filter table */
            continue;
        }

#endif
        ExtCommFilterInfo.u4Context = BGP4_RT_CXT_ID (pBestRoute);
        MEMCPY (ExtCommFilterInfo.au1ExtComm, au1ExtComm, EXT_COMM_VALUE_LEN);
        pExtCommFilterInfo = BGP4_RB_TREE_GET (EXT_COMM_OUTPUT_FILTER_TBL,
                                               (tBgp4RBElem
                                                *) (&ExtCommFilterInfo));
        if (pExtCommFilterInfo != NULL)
        {
            i1TempOutputFilterStatus = pExtCommFilterInfo->i1FilterPolicy;
        }

        if (i1TempOutputFilterStatus == FILTER_OUTGOING_EXT_COMM)
        {
            return i1TempOutputFilterStatus;
        }
        i1ExtCommOutputFilterStatus =
            i1ExtCommOutputFilterStatus & i1TempOutputFilterStatus;
    }
    return i1ExtCommOutputFilterStatus;
}

#endif /* _BGECFILT_C */
