/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgecinit.c,v 1.11 2013/02/07 12:18:53 siva Exp $
 *
 * Description: This file contains Extended Community Attribute
 *              processing functions.
 *
 *******************************************************************/
#ifndef _BGECINIT_C
#define _BGECINIT_C

#define ECOM_GLB_VAR

#include "bgp4com.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name   : ExtCommInit                                          */
/*                                                                           */
/*    Description     : This module does                                     */
/*                      Allocates memory for                                 */
/*                        - Incoming Ext Community filter table entries      */
/*                        - Outgoing Ext Community filter table entries      */
/*                        - Routes Ext Community configuration table entries */
/*                        - Route Ext Community Path memory units.           */
/*                        - Ext Community profiles                           */
/*                        - Peer Ext Community Send Status memory units      */
/*                        - Peer Link BandWidth memory units                 */
/*                                                                           */
/*                      Creates Hash Table for                               */
/*                        - Incoming Ext community filter table              */
/*                        - Outgoing Ext community filter table              */
/*                        - Routes Ext community additive configuration table*/
/*                        - Routes Ext community delete configuration table  */
/*                        - Peer Ext Community Send Status configuration     */
/*                          table                                            */
/*                        - Peer Link BandWidth Table                        */
/*                                                                           */
/*  If initialisation is successful this returns success to FutureBGP4.      */
/*  Else returns failure to FutureBGP4.                                      */
/*                                                                           */
/*  Input(s)        : i4MaxRoutes - indicates the maximum number of routes   */
/*                    BGP Routing table can store.                           */
/*                    i4MaxPeers - indicates the maximum number of peers     */
/*                    BGP supports.                                          */
/*                                                                           */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*  INPUT_EXT_COMM_FILTER_ENTRY_POOL_ID,                                     */
/*  OUTPUT_EXT_COMM_FILTER_ENTRY_POOL_ID,                                    */
/*  ROUTE_CONF_EXT_COMM_POOL_ID, EXT_COMM_PROFILE_POOL_ID,                   */
/*  PEER_LINK_BANDWIDTH_POOL_ID                                              */
/*  ROUTES_EXT_COMM_SET_STATUS_POOL_ID                                       */
/*                                                                           */
/*  EXT_COMM_INPUT_FILTER_TBL, EXT_COMM_OUTPUT_FILTER_TBL,                   */
/*  ROUTES_EXT_COMM_ADD_TBL, ROUTES_EXT_COMM_DELETE_TBL,                     */
/*  PEER_LINK_BANDWIDTH_TBL                                                  */
/*  ROUTES_EXT_COMM_SET_STATUS_TBL                                           */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*  Returns         :  EXT_COMM_SUCCESS or EXT_COMM_FAILURE                  */
/*                                                                           */
/*****************************************************************************/
INT4
ExtCommInit (INT4 i4MaxRoutes, INT4 i4MaxPeers)
{
    UNUSED_PARAM (i4MaxRoutes);
    UNUSED_PARAM (i4MaxPeers);

    ROUTES_EXT_COMM_ADD_TBL = 0;
    ROUTES_EXT_COMM_DELETE_TBL = 0;
    EXT_COMM_INPUT_FILTER_TBL = 0;
    EXT_COMM_OUTPUT_FILTER_TBL = 0;
    PEER_LINK_BANDWIDTH_TBL = 0;
    ROUTES_EXT_COMM_SET_STATUS_TBL = 0;

    /*  HASH TBL creation for the Tables --
     *  EXT_COMM_INPUT_FILTER_TBL, EXT_COMM_OUTPUT_FILTER_TBL,
     *  ROUTES_EXT_COMM_ADD_TBL, ROUTES_EXT_COMM_DELETE_TBL,
     *  PEER_LINK_BANDWIDTH_TBL
     *  ROUTES_EXT_COMM_SET_STATUS_TBL
     */

    EXT_COMM_INPUT_FILTER_TBL =
        BGP4_RB_TREE_CREATE (FsBGPSizingParams
                             [MAX_BGP_EXT_COMM_IN_FILTER_INFOS_SIZING_ID].
                             u4PreAllocatedUnits, ExtCommFiltTblCmpFunc);
    if (EXT_COMM_INPUT_FILTER_TBL == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tExtCommInit() : Creation of Input Ext-Comm Filter Hash Tbl "
                  "FAILED !!!\n");
        ExtCommShut ();
        return EXT_COMM_FAILURE;
    }

    EXT_COMM_OUTPUT_FILTER_TBL =
        BGP4_RB_TREE_CREATE (FsBGPSizingParams
                             [MAX_BGP_EXT_COMM_OUT_FILTER_INFOS_SIZING_ID].
                             u4PreAllocatedUnits, ExtCommFiltTblCmpFunc);

    if (EXT_COMM_OUTPUT_FILTER_TBL == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tExtCommInit() : Creation of Output Ext-Comm Filter Hash Tbl "
                  "FAILED !!!\n");
        ExtCommShut ();
        return EXT_COMM_FAILURE;
    }

    ROUTES_EXT_COMM_ADD_TBL =
        BGP4_RB_TREE_CREATE (FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                             u4PreAllocatedUnits, ExtCommRtTblCmpFunc);

    if (ROUTES_EXT_COMM_ADD_TBL == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tExtCommInit() : Creation of Ext-Comm Add_Conf Hash Tbl "
                  "FAILED !!!\n");
        ExtCommShut ();
        return EXT_COMM_FAILURE;
    }

    ROUTES_EXT_COMM_DELETE_TBL =
        BGP4_RB_TREE_CREATE (FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                             u4PreAllocatedUnits, ExtCommRtTblCmpFunc);

    if (ROUTES_EXT_COMM_DELETE_TBL == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tExtCommInit() : Creation of Ext-Comm Del_Conf Hash Tbl "
                  "FAILED !!!\n");
        ExtCommShut ();
        return EXT_COMM_FAILURE;
    }

    PEER_LINK_BANDWIDTH_TBL =
        BGP4_RB_TREE_CREATE (FsBGPSizingParams[MAX_BGP_PEERS_SIZING_ID].
                             u4PreAllocatedUnits, ExtCommPeerTblCmpFunc);

    if (PEER_LINK_BANDWIDTH_TBL == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tExtCommInit() : Creation of Peer LinkBw Hash Tbl "
                  "FAILED !!!\n");
        ExtCommShut ();
        return EXT_COMM_FAILURE;
    }
    ROUTES_EXT_COMM_SET_STATUS_TBL =
        BGP4_RB_TREE_CREATE (FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                             u4PreAllocatedUnits, ExtCommRtStatTblCmpFunc);

    if (ROUTES_EXT_COMM_SET_STATUS_TBL == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tExtCommInit() : Creation of Routes Set Status Hash Tbl "
                  "FAILED !!!\n");
        ExtCommShut ();
        return EXT_COMM_FAILURE;
    }

    return EXT_COMM_SUCCESS;
}

/****************************************************************************/
/* Function Name   : ExtCommRtTblCmpFunc                                    */
/* Description     : user compare function for ECOM ADD & DEL TBL RBTrees   */
/* Input(s)        : Two RBTree Nodes be compared                           */
/* Output(s)       : None                                                   */
/* Returns         : 1/(-1)/0                                               */
/****************************************************************************/
INT4
ExtCommRtTblCmpFunc (tBgp4RBElem * pRBElem1, tBgp4RBElem * pRBElem2)
{
    tNetAddress         NetAddress1 =
        ((tRouteConfExtComm *) pRBElem1)->RtNetAddress;
    tNetAddress         NetAddress2 =
        ((tRouteConfExtComm *) pRBElem2)->RtNetAddress;
    UINT4               u4Context1 =
        ((tRouteConfExtComm *) pRBElem1)->u4Context;
    UINT4               u4Context2 =
        ((tRouteConfExtComm *) pRBElem2)->u4Context;

    if (u4Context1 > u4Context2)
    {
        return 1;
    }
    if (u4Context1 < u4Context2)
    {
        return -1;
    }

    if (BGP4_AFI_IN_ADDR_PREFIX_INFO (NetAddress1.NetAddr) >
        BGP4_AFI_IN_ADDR_PREFIX_INFO (NetAddress2.NetAddr))
    {
        return 1;
    }
    else if (BGP4_AFI_IN_ADDR_PREFIX_INFO (NetAddress1.NetAddr) <
             BGP4_AFI_IN_ADDR_PREFIX_INFO (NetAddress2.NetAddr))
    {
        return -1;
    }
    else
    {

        if ((PrefixGreaterThan
             (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress1),
              BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress2))) ==
            BGP4_TRUE)
        {
            return 1;
        }
        else if ((PrefixLessThan
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress1),
                   BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress2))) ==
                 BGP4_TRUE)
        {
            return -1;
        }
        else
        {
            if (BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress1) >
                BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress2))
            {
                return 1;
            }
            else if (BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress1) <
                     BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress2))
            {
                return -1;
            }
            return 0;
        }
    }
}

/****************************************************************************/
/* Function Name   : ExtCommRtStatTblCmpFunc                                */
/* Description     : user compare function for ECOM RT STATUS TBL RBTrees   */
/* Input(s)        : Two RBTree Nodes be compared                           */
/* Output(s)       : None                                                   */
/* Returns         : 1/(-1)/0                                               */
/****************************************************************************/
INT4
ExtCommRtStatTblCmpFunc (tBgp4RBElem * pRBElem1, tBgp4RBElem * pRBElem2)
{

    tNetAddress         NetAddress1 =
        ((tRouteExtCommSetStatus *) pRBElem1)->RtNetAddress;
    tNetAddress         NetAddress2 =
        ((tRouteExtCommSetStatus *) pRBElem2)->RtNetAddress;
    UINT4               u4Context1 =
        ((tRouteExtCommSetStatus *) pRBElem1)->u4Context;
    UINT4               u4Context2 =
        ((tRouteExtCommSetStatus *) pRBElem2)->u4Context;

    if (u4Context1 > u4Context2)
    {
        return 1;
    }
    if (u4Context1 < u4Context2)
    {
        return -1;
    }

    if (BGP4_AFI_IN_ADDR_PREFIX_INFO (NetAddress1.NetAddr) >
        BGP4_AFI_IN_ADDR_PREFIX_INFO (NetAddress2.NetAddr))
    {
        return 1;
    }
    else if (BGP4_AFI_IN_ADDR_PREFIX_INFO (NetAddress1.NetAddr) <
             BGP4_AFI_IN_ADDR_PREFIX_INFO (NetAddress2.NetAddr))
    {
        return -1;
    }
    else
    {

        if ((PrefixGreaterThan
             (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress1),
              BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress2))) ==
            BGP4_TRUE)
        {
            return 1;
        }
        else if ((PrefixLessThan
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress1),
                   BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddress2))) ==
                 BGP4_TRUE)
        {
            return -1;
        }
        else
        {
            if (BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress1) >
                BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress2))
            {
                return 1;
            }
            else if (BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress1) <
                     BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddress2))
            {
                return -1;
            }
            return 0;
        }
    }

}

/****************************************************************************/
/* Function Name   : ExtCommPeerTblCmpFunc                                  */
/* Description     : user compare function for ECOM PEER SENDSTS TBL RBTrees*/
/* Input(s)        : Two RBTree Nodes be compared                           */
/* Output(s)       : None                                                   */
/* Returns         : 1/(-1)/0                                               */
/****************************************************************************/
INT4
ExtCommPeerTblCmpFunc (tBgp4RBElem * pRBElem1, tBgp4RBElem * pRBElem2)
{

    tAddrPrefix         PeerAddress1 =
        ((tPeerLinkBandWidth *) pRBElem1)->PeerAddress;
    tAddrPrefix         PeerAddress2 =
        ((tPeerLinkBandWidth *) pRBElem2)->PeerAddress;
    UINT4               u4Context1 =
        ((tPeerLinkBandWidth *) pRBElem1)->u4Context;
    UINT4               u4Context2 =
        ((tPeerLinkBandWidth *) pRBElem2)->u4Context;

    if (u4Context1 > u4Context2)
    {
        return 1;
    }
    if (u4Context1 < u4Context2)
    {
        return -1;
    }

    if (BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress1) >
        BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress1))
    {
        return 1;
    }
    else if (BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress1) <
             BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress2))
    {
        return -1;
    }
    else
    {

        if (PrefixGreaterThan (PeerAddress1, PeerAddress2) == BGP4_TRUE)
        {
            return 1;
        }

        else if (PrefixLessThan (PeerAddress1, PeerAddress2) == BGP4_TRUE)
        {
            return -1;
        }
        else
        {
            return 0;
        }

    }
}

/****************************************************************************/
/* Function Name   : ExtCommFiltTblCmpFunc                                  */
/* Description     : user compare function for IN & OUT FILT TBL RBTrees    */
/* Input(s)        : Two RBTree Nodes be compared                           */
/* Output(s)       : None                                                   */
/* Returns         : 1/(-1)/0                                               */
/****************************************************************************/
INT4
ExtCommFiltTblCmpFunc (tBgp4RBElem * pRBElem1, tBgp4RBElem * pRBElem2)
{
    tExtCommFilterInfo *pExtCommFilterInfo1 = NULL;
    tExtCommFilterInfo *pExtCommFilterInfo2 = NULL;
    INT4                i4Cmp;

    pExtCommFilterInfo1 = (tExtCommFilterInfo *) pRBElem1;
    pExtCommFilterInfo2 = (tExtCommFilterInfo *) pRBElem2;
    if (pExtCommFilterInfo1->u4Context > pExtCommFilterInfo2->u4Context)
    {
        return 1;
    }
    if (pExtCommFilterInfo1->u4Context < pExtCommFilterInfo2->u4Context)
    {
        return -1;
    }
    i4Cmp = MEMCMP (pExtCommFilterInfo1->au1ExtComm,
                    pExtCommFilterInfo2->au1ExtComm, EXT_COMM_VALUE_LEN);
    return i4Cmp;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name   : ExtCommShut                                          */
/*                                                                           */
/*    Description     : This module does                                     */
/*                      Free memory of                                       */
/*                        - Incoming Ext Community filter table entries      */
/*                        - Outgoing Ext Community filter table entries      */
/*                        - Routes Ext Community configuration table entries */
/*                        - Route Ext Community Path memory units.           */
/*                        - Ext Community profiles                           */
/*                        - Peer Ext Community Send Status memory units      */
/*                        - Peer Link BandWidth memory units                 */
/*                                                                           */
/*                      Free Hash Table of                                   */
/*                        - Incoming Ext community filter table              */
/*                        - Outgoing Ext community filter table              */
/*                        - Routes Ext community additive configuration table*/
/*                        - Routes Ext community delete configuration table  */
/*                        - Peer Ext Community Send Status configuration     */
/*                          table                                            */
/*                        - Peer Link BandWidth Table                        */
/*                                                                           */
/*  If shut down is successful this returns success                          */
/*  Else returns failure                                                     */
/*                                                                           */
/*  Input(s)       : None                                                    */
/*                                                                           */
/*  Output(s)       : None                                                   */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*  INPUT_EXT_COMM_FILTER_ENTRY_POOL_ID,                                     */
/*  OUTPUT_EXT_COMM_FILTER_ENTRY_POOL_ID,                                    */
/*  ROUTE_CONF_EXT_COMM_POOL_ID, EXT_COMM_PROFILE_POOL_ID,                   */
/*  PEER_LINK_BANDWIDTH_POOL_ID                                              */
/*  ROUTES_EXT_COMM_SET_STATUS_POOL_ID                                       */
/*                                                                           */
/*  EXT_COMM_INPUT_FILTER_TBL, EXT_COMM_OUTPUT_FILTER_TBL,                   */
/*  ROUTES_EXT_COMM_ADD_TBL, ROUTES_EXT_COMM_DELETE_TBL,                     */
/*  PEER_LINK_BANDWIDTH_TBL                                                  */
/*  ROUTES_EXT_COMM_SET_STATUS_TBL                                           */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*  Returns         :  EXT_COMM_SUCCESS or EXT_COMM_FAILURE                  */
/*                                                                           */
/*****************************************************************************/
INT4
ExtCommShut (VOID)
{
    INT4                i4ExtCommShutDownStatus = EXT_COMM_SUCCESS;

    /*  HASH TBL Free for the Tables -- 
     *  EXT_COMM_INPUT_FILTER_TBL, EXT_COMM_OUTPUT_FILTER_TBL,
     *  ROUTES_EXT_COMM_ADD_TBL, ROUTES_EXT_COMM_DELETE_TBL,
     *  PEER_LINK_BANDWIDTH_TBL,
     *  ROUTES_EXT_COMM_SET_STATUS_TBL
     */

    if (EXT_COMM_INPUT_FILTER_TBL != NULL)
    {
        BGP4_RB_TREE_DELETE (EXT_COMM_INPUT_FILTER_TBL);
        EXT_COMM_INPUT_FILTER_TBL = NULL;
    }

    if (EXT_COMM_OUTPUT_FILTER_TBL != NULL)
    {
        BGP4_RB_TREE_DELETE (EXT_COMM_OUTPUT_FILTER_TBL);
        EXT_COMM_OUTPUT_FILTER_TBL = NULL;
    }

    if (ROUTES_EXT_COMM_ADD_TBL != NULL)
    {
        BGP4_RB_TREE_DELETE (ROUTES_EXT_COMM_ADD_TBL);
        ROUTES_EXT_COMM_ADD_TBL = NULL;
    }

    if (ROUTES_EXT_COMM_DELETE_TBL != NULL)
    {
        BGP4_RB_TREE_DELETE (ROUTES_EXT_COMM_DELETE_TBL);
        ROUTES_EXT_COMM_DELETE_TBL = NULL;
    }

    if (PEER_LINK_BANDWIDTH_TBL != NULL)
    {
        BGP4_RB_TREE_DELETE (PEER_LINK_BANDWIDTH_TBL);
        PEER_LINK_BANDWIDTH_TBL = NULL;
    }
    if (ROUTES_EXT_COMM_SET_STATUS_TBL != NULL)
    {
        BGP4_RB_TREE_DELETE (ROUTES_EXT_COMM_SET_STATUS_TBL);
        ROUTES_EXT_COMM_SET_STATUS_TBL = NULL;
    }

    /*  MemPool Freeing for the Pools --
     *  INPUT_EXT_COMM_FILTER_ENTRY_POOL_ID,
     *  OUTPUT_EXT_COMM_FILTER_ENTRY_POOL_ID,
     *  ROUTE_CONF_EXT_COMM_POOL_ID, EXT_COMM_PROFILE_POOL_ID,
     *  PEER_LINK_BANDWIDTH_POOL_ID,
     *  ROUTES_EXT_COMM_SET_STATUS_POOL_ID
     */

    INPUT_EXT_COMM_FILTER_ENTRY_POOL_ID = 0;

    OUTPUT_EXT_COMM_FILTER_ENTRY_POOL_ID = 0;

    ROUTE_CONF_EXT_COMM_POOL_ID = 0;

    EXT_COMM_PROFILE_POOL_ID = 0;

    PEER_LINK_BANDWIDTH_POOL_ID = 0;
    ROUTES_EXT_COMM_SET_STATUS_POOL_ID = 0;

    return i4ExtCommShutDownStatus;
}

#endif /* ifndef  _BGECINIT_C */
