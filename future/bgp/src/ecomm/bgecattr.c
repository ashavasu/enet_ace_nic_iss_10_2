/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgecattr.c,v 1.18 2017/09/15 06:19:53 siva Exp $
 *
 * Description: This file contains Extended Community Attribute
 *              processing functions.
 *
 *******************************************************************/
#ifndef _BGECATTR_C
#define _BGECATTR_C

#include "bgp4com.h"

/*****************************************************************************/
/*    Function Name   : ExtCommGetUpdExtCommAttrib                           */
/*                                                                           */
/*    Description     : Gets the ext-comm path attribute for route           */
/*                      pRouteProfile and update the BGP4-INFO to be         */
/*                      advertised.                                          */
/*                                                                           */
/*    Input(s)        : pRouteProfile    - pointer to route profile of NLRI  */
/*                      pPeerEntry - Pointer to peer Information             */
/*                                                                           */
/*    Output(s)       : pAdvtBgpInfo - Pointer to updated Path Attribute to  */
/*                                     be advertised.                        */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*                                                                           */
/*  Returns         :   EXT_COMM_SUCCESS / EXT_COMM_FAILURE                  */
/*                                                                           */
/*****************************************************************************/
INT4
ExtCommGetUpdExtCommAttrib (tRouteProfile * pRouteProfile,
                            tBgp4Info * pAdvtBgpInfo,
                            tBgp4PeerEntry * pPeerEntry, tBgp4Info * pRtInfo)
{
    tExtCommunity      *pExtComm = NULL;
    INT4                i4RouteExtConfCommStatus;
    INT4                i4ExtCommFormStatus;
    INT1                i1ExtCommSetStatus;

    i1ExtCommSetStatus = ExtCommGetExtCommSetStatus (pRouteProfile);
    if (i1ExtCommSetStatus == EXT_COMM_SET_NONE)
    {
        /* Ext Comm Set None is TRUE hence route should be considered for
         * advertising, existing ext-comm or new configured ext-comms is
         * not sent 
         */
        BGP4_TRC_ARG1 (NULL,
                       BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                       "\tExtended Community None is set. Hence the existing ext-community or "
                       "new configured ext-communities are not sent for the route %s.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRouteProfile),
                                        BGP4_RT_AFI_INFO (pRouteProfile)));
        return EXT_COMM_SUCCESS;
    }

    EXT_COMMUNITY_NODE_CREATE (pExtComm);
    if (pExtComm == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for Extended Community Attribute "
                  "FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_EXT_COMM_NODE_SIZING_ID]++;
        return (EXT_COMM_FAILURE);
    }
    pExtComm->pu1EcommVal = NULL;
    pExtComm->u2EcommCnt = 0;
    pExtComm->u1Flag = 0;
    BGP4_INFO_ECOMM_ATTR (pAdvtBgpInfo) = pExtComm;

    /* If i1ExtCommSetStatus is EXT_COMM_MODIFY means whatever ext-comms
     * configured as delete will be deleted in the received ext-comm info
     */

    if (i1ExtCommSetStatus == EXT_COMM_MODIFY)
    {
        i4ExtCommFormStatus = ExtCommFormRouteExtCommPath (pRouteProfile,
                                                           pAdvtBgpInfo,
                                                           pPeerEntry, pRtInfo);

        if (i4ExtCommFormStatus == EXT_COMM_FAILURE)
        {
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC |
                           BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                           BGP4_MOD_NAME,
                           "\tUnable to form Ext-community path attribute for route %s/%d "
                           "when Ext-community modify is set.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRouteProfile),
                                            BGP4_RT_AFI_INFO (pRouteProfile)),
                           BGP4_RT_PREFIXLEN (pRouteProfile));
            if (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo) != NULL)
            {
                ATTRIBUTE_NODE_FREE (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo));
            }
            EXT_COMMUNITY_NODE_FREE (BGP4_INFO_ECOMM_ATTR (pAdvtBgpInfo));
            BGP4_INFO_ECOMM_ATTR (pAdvtBgpInfo) = NULL;
            return EXT_COMM_FAILURE;
        }
    }

    /* If i1ExtCommSetStatus is EXT_COMM_MODIFY or EXT_SET means
     * whatever ext-comms configured as additive will be added to the
     * ext-comm info to be sent
     */
    i4RouteExtConfCommStatus = ExtCommUpdateRouteExtCommAdd (pRouteProfile,
                                                             pAdvtBgpInfo);
    if (i4RouteExtConfCommStatus == EXT_COMM_FAILURE)
    {
        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC |
                       BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                       BGP4_MOD_NAME,
                       "\tUnable to form ext-community path attribute for route %s/%d \n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRouteProfile),
                                        BGP4_RT_AFI_INFO (pRouteProfile)),
                       BGP4_RT_PREFIXLEN (pRouteProfile));
        if (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo) != NULL)
        {
            ATTRIBUTE_NODE_FREE (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo));
        }
        EXT_COMMUNITY_NODE_FREE (BGP4_INFO_ECOMM_ATTR (pAdvtBgpInfo));
        BGP4_INFO_ECOMM_ATTR (pAdvtBgpInfo) = NULL;
        return EXT_COMM_FAILURE;
    }

#ifdef L3VPN
    if ((BGP4_VPN4_PEER_ROLE (pPeerEntry) == BGP4_VPN4_PE_PEER) &&
        (BGP4_RT_AFI_INFO (pRouteProfile) == BGP4_INET_AFI_IPV4) &&
        (BGP4_RT_SAFI_INFO (pRouteProfile) == BGP4_INET_SAFI_VPNV4_UNICAST))
    {
        i4ExtCommFormStatus = Bgp4Vpnv4FillVpnExtComm (pRouteProfile,
                                                       pPeerEntry,
                                                       pAdvtBgpInfo);
        if (i4ExtCommFormStatus == EXT_COMM_FAILURE)
        {
            if (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo) != NULL)
            {
                ATTRIBUTE_NODE_FREE (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo));
            }
            EXT_COMMUNITY_NODE_FREE (BGP4_INFO_ECOMM_ATTR (pAdvtBgpInfo));
            BGP4_INFO_ECOMM_ATTR (pAdvtBgpInfo) = NULL;
            return EXT_COMM_FAILURE;
        }
    }
#endif
#ifdef EVPN_WANTED
    if ((BGP4_RT_AFI_INFO (pRouteProfile) == BGP4_INET_AFI_L2VPN) &&
        (BGP4_RT_SAFI_INFO (pRouteProfile) == BGP4_INET_SAFI_EVPN))
    {
        i4ExtCommFormStatus = Bgp4EvpnFillExtComm (pRouteProfile,
                                                   pPeerEntry, pAdvtBgpInfo);
        if (i4ExtCommFormStatus == EXT_COMM_FAILURE)
        {
            if (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo) != NULL)
            {
                ATTRIBUTE_NODE_FREE (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo));
            }
            EXT_COMMUNITY_NODE_FREE (BGP4_INFO_ECOMM_ATTR (pAdvtBgpInfo));
            BGP4_INFO_ECOMM_ATTR (pAdvtBgpInfo) = NULL;
            return EXT_COMM_FAILURE;
        }
    }
#endif
#ifdef VPLSADS_WANTED
    if ((BGP4_RT_AFI_INFO (pRouteProfile) == BGP4_INET_AFI_L2VPN) &&
        (BGP4_RT_SAFI_INFO (pRouteProfile) == BGP4_INET_SAFI_VPLS))
    {

        i4ExtCommFormStatus = Bgp4VplsFillVplsExtComm (pRouteProfile,
                                                       pPeerEntry,
                                                       pAdvtBgpInfo);
        if (i4ExtCommFormStatus == EXT_COMM_FAILURE)
        {
            if (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo) != NULL)
            {
                ATTRIBUTE_NODE_FREE (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo));
            }
            EXT_COMMUNITY_NODE_FREE (BGP4_INFO_ECOMM_ATTR (pAdvtBgpInfo));
            BGP4_INFO_ECOMM_ATTR (pAdvtBgpInfo) = NULL;
            return EXT_COMM_FAILURE;
        }

    }
#endif

    /* Check if any ext-community exist. If no ext-community exist, then
     * unlink the ext-community pointer from Advertised BGP4-INFO and
     * free it. */
    if (BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo) == 0)
    {
        EXT_COMMUNITY_NODE_FREE (BGP4_INFO_ECOMM_ATTR (pAdvtBgpInfo));
        BGP4_INFO_ECOMM_ATTR (pAdvtBgpInfo) = NULL;
    }
    else
    {
        BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_ECOMM_MASK;
    }
    return EXT_COMM_SUCCESS;
}

/*****************************************************************************/
/*    Function Name   : ExtCommFormRouteExtCommPath                          */
/*                                                                           */
/*    Description     : Copies the received ext-comm  path attribute to the  */
/*                      Route ext-comm  path after checking each ext-comm    */
/*                      in route ext-comm  delete table                      */
/*                                                                           */
/*    Input(s)        : pRouteProfile    - pointer to route profile of NLRI  */
/*                      pPeerEntry - Pointer to peer Information             */
/*                                                                           */
/*    Output(s)       : pAdvtBgpInfo - Updated BGP4-INFO to be advertised.   */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*                                                                           */
/*  Returns         :   EXT_COMM_SUCCESS /  EXT_COMM_FAILURE                 */
/*                                                                           */
/*****************************************************************************/
INT4
ExtCommFormRouteExtCommPath (tRouteProfile * pRouteProfile,
                             tBgp4Info * pAdvtBgpInfo,
                             tBgp4PeerEntry * pPeerEntry, tBgp4Info * pRtInfo)
{

    tBgp4Info          *pRouteInfo = NULL;
    tPeerLinkBandWidth *pPeerLinkBandWidth = NULL;
    tPeerLinkBandWidth  PeerLinkBandwidth;
    UINT1              *pu1EcommBuf = NULL;
    UINT4               u4BgpLocalAS;
    UINT2               u2WriteEcommCnt = 0;
    UINT2               u2ExtCommType = 0;
    INT1                i1PeerType;
    INT1                i1IsRouteExtCommDeleteStatus;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT4               u4Index;
    UINT1               u1IsPartialSet = BGP4_TRUE;

    u2WriteEcommCnt = 0;

    /* Get the peer type whether it is internal or external */
    i1PeerType = BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerEntry), pPeerEntry);

    if ((BGP4_RT_PROTOCOL (pRouteProfile) == BGP_ID))
    {
        if ((pRtInfo != NULL) && (pRtInfo->pExtCommunity != NULL))
        {
            pRouteInfo = pRtInfo;
        }
        else
        {
            pRouteInfo = BGP4_RT_BGP_INFO (pRouteProfile);
        }
        /* Check for the presence of ext-comm  path attribute */
        if (BGP4_INFO_ECOMM_ATTR (pRouteInfo) == NULL)
        {
            /* Ext-Community path attribute NOT present */
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tExt-Community path attribute is not present for the route %s\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRouteProfile),
                                            BGP4_RT_AFI_INFO (pRouteProfile)));
            return EXT_COMM_SUCCESS;
        }

        /* To begin with Allocate a Buffer of size BGP4_MAX_MSG_LEN
         * and fill in the Ext-community Attributes. If the length of
         * this attribute goes greater than BGP4_MAX_MSG_LEN set the
         * Partial Flag Bit. Else if the length of the Ext-community
         * attribute is less, then release this and reallocate buffer
         * equal to the correct size. */
        pu1EcommBuf = MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);
        if (pu1EcommBuf == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for Filling "
                      "Ext-Community Attribute FAILED!!!\n");
            gu4BgpDebugCnt[MAX_BGP_BUFNODE_MSGS_SIZING_ID]++;
            return EXT_COMM_FAILURE;
        }

        /* Process all the Ext-communities */
        u2WriteEcommCnt = 0;
        for (u4Index = 0; u4Index < BGP4_INFO_ECOMM_COUNT (pRouteInfo);
             u4Index++)
        {
            if ((u2WriteEcommCnt * EXT_COMM_VALUE_LEN) >= BGP4_MAX_MSG_LEN)
            {
                /* Have processed more Ext-community than available buffer.
                 * Also more Ext-communities available. Set the partial bit. */
                u1IsPartialSet = BGP4_TRUE;
                break;
            }
            MEMCPY ((UINT1 *) au1ExtComm,
                    (BGP4_INFO_ECOMM_ATTR_VAL (pRouteInfo) +
                     (u4Index * EXT_COMM_VALUE_LEN)), EXT_COMM_VALUE_LEN);
            PTR_FETCH2 (u2ExtCommType, au1ExtComm);
            i1IsRouteExtCommDeleteStatus =
                ExtCommIsRouteExtCommDelete (pRouteProfile, au1ExtComm);
            if (i1IsRouteExtCommDeleteStatus == EXT_COMM_FALSE)
            {
                /* Ext-Comm  NOT to be deleted */

#ifdef L3VPN
                /* If the extended community is of route-target type and is
                 * received from a CE peer which is not permitted to send,
                 * then remove that route target value
                 */
                if ((BGP4_IS_RT_EXT_COMMUNITY (u2ExtCommType) == BGP4_TRUE) &&
                    (BGP4_RT_PROTOCOL (pRouteProfile) == BGP_ID) &&
                    (BGP4_RT_GET_FLAGS (pRouteProfile) &
                     BGP4_RT_CONVERTED_TO_VPNV4) &&
                    (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRouteProfile))
                     == BGP4_VPN4_CE_PEER) &&
                    (BGP4_VPN4_PEER_CERT_STATUS
                     (BGP4_RT_PEER_ENTRY (pRouteProfile)) ==
                     BGP4_VPN4_CE_RT_DONOTSEND))
                {
                    /* Ignore this route-target */
                    continue;
                }
#endif
                /* Check if the extended comm is transitive or 
                 * non-transitive. If transitive send it to all
                 * peers. If non-transitive dont sent it to external
                 * peers. */
                if ((u2ExtCommType & 0x4000) == 0x4000)
                {
                    /* Extended community is not transitive. Send only to
                     * internal peers. */
                    if (i1PeerType == BGP4_INTERNAL_PEER)
                    {
                        /* The route is to internal peer so this
                         * extended community can be advertised */
                        MEMCPY (pu1EcommBuf +
                                (u2WriteEcommCnt * EXT_COMM_VALUE_LEN),
                                (UINT1 *) &au1ExtComm, EXT_COMM_VALUE_LEN);
                        u2WriteEcommCnt++;
                    }
                    else
                    {
                        /* The route is to be advertised to external peer.
                         * So extended community is deleted */
                    }
                }
                else
                {
                    /* The ext-comm is transitive so send it to all peers. */
                    MEMCPY (pu1EcommBuf +
                            (u2WriteEcommCnt * EXT_COMM_VALUE_LEN),
                            (UINT1 *) &au1ExtComm, EXT_COMM_VALUE_LEN);
                    u2WriteEcommCnt++;
                }
            }
            else
            {
                /* The received ext-comm  is deleted for the route  */
            }
        }

        /* The route is to external, we have to add the link bandwidth 
         * if it is available */

        if (((u2WriteEcommCnt * EXT_COMM_VALUE_LEN) < BGP4_MAX_MSG_LEN) &&
            (i1PeerType == BGP4_EXTERNAL_PEER))
        {
            u4BgpLocalAS =
                ((BGP4_CONFED_ID (BGP4_PEER_CXT_ID (pPeerEntry)) ==
                  BGP4_INV_AS) ?
                 BGP4_LOCAL_AS_NO (BGP4_PEER_CXT_ID (pPeerEntry)) :
                 BGP4_CONFED_ID (BGP4_PEER_CXT_ID (pPeerEntry)));

            Bgp4CopyAddrPrefixStruct (&(PeerLinkBandwidth.PeerAddress),
                                      BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));
            PeerLinkBandwidth.u4Context = BGP4_PEER_CXT_ID (pPeerEntry);
            pPeerLinkBandWidth =
                BGP4_RB_TREE_GET (PEER_LINK_BANDWIDTH_TBL,
                                  (tBgp4RBElem *) & PeerLinkBandwidth);
            if (pPeerLinkBandWidth != NULL)
            {
                UINT2               u2WriteEcommLen =
                    (UINT2) (u2WriteEcommCnt * EXT_COMM_VALUE_LEN);
                PTR_ASSIGN2 ((pu1EcommBuf + u2WriteEcommLen),
                             ((LINK_BANDWIDTH_COMMUNITY)));
                u2WriteEcommLen += sizeof (UINT2);

                if (u2WriteEcommLen < BGP4_MAX_MSG_LEN)
                {
                    if (u4BgpLocalAS > BGP4_MAX_TWO_BYTE_AS)
                    {
                        u4BgpLocalAS = BGP4_AS_TRANS;
                    }

                    PTR_ASSIGN2 ((pu1EcommBuf + u2WriteEcommLen),
                                 (UINT2) u4BgpLocalAS);
                    u2WriteEcommLen += sizeof (UINT2);
                }

                if (u2WriteEcommLen < BGP4_MAX_MSG_LEN)
                {
                    PTR_ASSIGN4 ((pu1EcommBuf + u2WriteEcommLen),
                                 (pPeerLinkBandWidth->u4LinkBandWidth));
                }
                u2WriteEcommCnt++;
                if ((u2WriteEcommCnt * EXT_COMM_VALUE_LEN) >= BGP4_MAX_MSG_LEN)
                {
                    u1IsPartialSet = BGP4_TRUE;
                }
            }
        }                        /* IF (i1PeerType  = BGP4_EXTERNAL_PEER) */

        /* Now Reallocate the Buffer for storing the Community Attribute. */
        if (u2WriteEcommCnt > 0)
        {
            ATTRIBUTE_NODE_CREATE (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo));
            if (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo) == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                          "\tMemory Allocation for Filling Ext-Community "
                          "Attribute FAILED!!!\n");
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1EcommBuf);
                gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
                return EXT_COMM_FAILURE;
            }
            BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo) = u2WriteEcommCnt;
            MEMCPY (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo), pu1EcommBuf,
                    u2WriteEcommCnt * EXT_COMM_VALUE_LEN);
            BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_ECOMM_MASK;
            if (((BGP4_INFO_ECOMM_ATTR_FLAG (pRouteInfo) &
                  BGP4_PARTIAL_FLAG_MASK) == BGP4_PARTIAL_FLAG_MASK) ||
                (u1IsPartialSet == BGP4_TRUE))
            {
                /* Partial Bit should be set. */
                BGP4_INFO_ECOMM_ATTR_FLAG (pAdvtBgpInfo) |=
                    BGP4_PARTIAL_FLAG_MASK;
            }
        }

        /* Release the Old Buffer. */
        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1EcommBuf);
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                       "\tThe Received Ext-Community path attribute is successfully filled "
                       "for the route %s\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRouteProfile),
                                        BGP4_RT_AFI_INFO (pRouteProfile)));
        return EXT_COMM_SUCCESS;
    }
    else
    {
        /* Protocol not BGP so no need for updating received ext-comm path */
        return EXT_COMM_SUCCESS;
    }
}

/*****************************************************************************/
/*    Function Name   : ExtCommUpdateRouteExtCommAdd                         */
/*                                                                           */
/*    Description     : Update the Ext-Communities as per policy.            */
/*                                                                           */
/*    Input(s)        : pRouteProfile - pointer to route profile of NLRI     */
/*                                                                           */
/*    Output(s)       : pAdvtBgpInfo - Pointer to Updated BGP4-INFO to be    */
/*                                     advertised.                           */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred : ROUTES_EXT_COMM_ADD_TBL                      */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*                                                                           */
/*  Returns         :   EXT_COMM_SUCCESS                                     */
/*                                                                           */
/*****************************************************************************/
INT4
ExtCommUpdateRouteExtCommAdd (tRouteProfile * pRouteProfile,
                              tBgp4Info * pAdvtBgpInfo)
{

    tRouteConfExtComm  *pRouteConfExtComm = NULL;
    tExtCommProfile    *pExtCommProfile = NULL;
    UINT1              *pu1EcommBuf = NULL;
    UINT1              *pu1ReallocEcommBuf = NULL;
    tRouteConfExtComm   RouteConfExtComm;
    UINT2               u2WriteEcommCnt = 0;
    UINT1               u1IsPartialSet = BGP4_FALSE;

    RouteConfExtComm.u4Context = BGP4_RT_CXT_ID (pRouteProfile);
    Bgp4CopyNetAddressStruct (&(RouteConfExtComm.RtNetAddress),
                              BGP4_RT_NET_ADDRESS_INFO (pRouteProfile));

    pRouteConfExtComm = BGP4_RB_TREE_GET (ROUTES_EXT_COMM_ADD_TBL,
                                          (tBgp4RBElem *) (&RouteConfExtComm));
    if (pRouteConfExtComm != NULL)
    {
        TMO_SLL_Scan (&(pRouteConfExtComm->TSConfExtComms),
                      pExtCommProfile, tExtCommProfile *)
        {
            if (pu1EcommBuf == NULL)
            {
                /* Allocate a Buffer of size BGP4_MAX_MSG_LEN and keep
                 * storing the Ext-Communities. If the total Ecomm size
                 * exceeds this limit, then set the partial bit. */
                pu1EcommBuf = MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);
                if (pu1EcommBuf == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                              BGP4_MOD_NAME,
                              "\tMemory Allocation for Filling "
                              "Ext-Community Attribute FAILED!!!\n");
                    gu4BgpDebugCnt[MAX_BGP_BUFNODE_MSGS_SIZING_ID]++;
                    return EXT_COMM_FAILURE;
                }
            }
            if ((u2WriteEcommCnt * EXT_COMM_VALUE_LEN) < BGP4_MAX_MSG_LEN)
            {
                MEMCPY (pu1EcommBuf +
                        (u2WriteEcommCnt * EXT_COMM_VALUE_LEN),
                        (UINT1 *) (pExtCommProfile->au1ExtComm),
                        EXT_COMM_VALUE_LEN);
                u2WriteEcommCnt++;
            }
            else
            {
                u1IsPartialSet = BGP4_TRUE;
                break;
            }
        }
    }

    if (u2WriteEcommCnt > 0)
    {
        /* Reallocate the memory for the Ext-Community attribute. */
        ATTRIBUTE_NODE_CREATE (pu1ReallocEcommBuf);
        if (pu1ReallocEcommBuf == NULL)
        {
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1EcommBuf);
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for Filling Ext-Community"
                      " Attribute FAILED!!!\n");
            gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
            return COMM_FAILURE;
        }

        /* Fill Comm Attribute present in the BGP4-INFO */
        MEMCPY (pu1ReallocEcommBuf, BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo),
                BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo) * EXT_COMM_VALUE_LEN);

        /* Fill Ecomm Attribute Configured. */
        if ((u2WriteEcommCnt * EXT_COMM_VALUE_LEN) < BGP4_MAX_MSG_LEN)
        {
            MEMCPY (pu1ReallocEcommBuf +
                    (BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo) * EXT_COMM_VALUE_LEN),
                    pu1EcommBuf, u2WriteEcommCnt * EXT_COMM_VALUE_LEN);
        }
        /* Release the old comm buffer from BGP4-INFO */
        ATTRIBUTE_NODE_FREE (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo));
        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1EcommBuf);
        /* Update the Community attribute in BGP4-INFO */
        BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo) = pu1ReallocEcommBuf;
        BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo) += u2WriteEcommCnt;
        BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_ECOMM_MASK;
        if (u1IsPartialSet == BGP4_TRUE)
        {
            BGP4_INFO_ECOMM_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_PARTIAL_FLAG_MASK;
        }
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                       "\tThe Ext-Community path attribute is successfully updated "
                       "for the route %s\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRouteProfile),
                                        BGP4_RT_AFI_INFO (pRouteProfile)));
    }
    else
    {
        if (pRouteConfExtComm != NULL)
        {
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1EcommBuf);
        }
    }
    return EXT_COMM_SUCCESS;
}

/*****************************************************************************/
/*    Function Name   : ExtCommIsRouteExtCommDelete                          */
/*                                                                           */
/*    Description     : To Check whether this ext-comm  is in  delete list   */
/*                                                                           */
/*    Input(s)        : pRouteProfile - pointer to route profile of NLRI     */
/*                      u2ExtCommType - The ext-comm  which is to checked in */
/*                      ROUTES_EXT_COMM_DELETE_TBL, whether  ext-comm  is to */
/*                      be deleted or not                                    */
/*                                                                           */
/*    Output(s)       : None                                                 */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred : ROUTES_EXT_COMM_DELETE_TBL                   */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*                                                                           */
/*  Returns         :     EXT_COMM_TRUE / EXT_COMM_FALSE                     */
/*                                                                           */
/*****************************************************************************/
INT1
ExtCommIsRouteExtCommDelete (tRouteProfile * pRouteProfile, UINT1 *au1ExtComm)
{
    tExtCommProfile    *pExtCommProfile = NULL;
    INT1                i1ExtCommDeleteStatus;
    tRouteConfExtComm  *pRouteConfExtComm = NULL;
    tRouteConfExtComm   RouteConfExtComm;
    INT4                i4MatchFnd;

    i1ExtCommDeleteStatus = EXT_COMM_FALSE;

    if (pRouteProfile == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC |
                  BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                  BGP4_MOD_NAME,
                  "\tReceived Route profile is NULL while checking whether "
                  "Ext-community is in delete list.\n");
        return EXT_COMM_FAILURE;
    }

    Bgp4CopyNetAddressStruct (&(RouteConfExtComm.RtNetAddress),
                              BGP4_RT_NET_ADDRESS_INFO (pRouteProfile));

    RouteConfExtComm.u4Context = BGP4_RT_CXT_ID (pRouteProfile);
    pRouteConfExtComm = BGP4_RB_TREE_GET (ROUTES_EXT_COMM_ADD_TBL,
                                          (tBgp4RBElem *) (&RouteConfExtComm));
    if (pRouteConfExtComm != NULL)
    {
        TMO_SLL_Scan (&(pRouteConfExtComm->TSConfExtComms),
                      pExtCommProfile, tExtCommProfile *)
        {
            /* Read the Extended Community Type present in au1ExtComm of
             * pExtCommProfile into u2ReadVal of size 2 bytes
             */
            i4MatchFnd = MEMCMP (au1ExtComm,
                                 pExtCommProfile->au1ExtComm,
                                 EXT_COMM_VALUE_LEN);
            if (i4MatchFnd == 0)
            {
                /* The ext-comm  has to deleted since it is
                 * present in the delete list
                 */
                return EXT_COMM_TRUE;
            }
        }
    }
    /* The route is not configured to be deleted */
    return i1ExtCommDeleteStatus;
}

/*****************************************************************************/
/*    Function Name   : ExtCommGetExtCommSetStatus                           */
/*                                                                           */
/*    Description     : Gets the Ext-Comm  Set None Status, which means the  */
/*                      route has to be sent without ext-comm  attribute     */
/*                                                                           */
/*    Input(s)        : pRouteProfile - pointer to route profile of NLRI     */
/*                                                                           */
/*    Output(s)       : None                                                 */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred : ROUTES_EXT_COMM_ADD_TBL                      */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*                                                                           */
/*  Returns         :     EXT_COMM_SET/EXT_COMM_SET_NONE/EXT_COMM_MODIFY     */
/*                                                                           */
/*****************************************************************************/
INT1
ExtCommGetExtCommSetStatus (tRouteProfile * pRouteProfile)
{
    tRouteExtCommSetStatus *pRouteExtCommSetStatus = NULL;
    tRouteExtCommSetStatus RouteExtCommSetStatus;
    INT1                i1ExtCommSetStatus;
#ifdef VPLSADS_WANTED
    UINT4               u4AsafiMask;
#endif

    i1ExtCommSetStatus = EXT_COMM_MODIFY;

    if (pRouteProfile == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC,
                  BGP4_MOD_NAME,
                  "\tExtCommGetExtCommSetStatus() : Received NULL Route "
                  "Profile\n");
        return EXT_COMM_FAILURE;
    }
#ifdef VPLSADS_WANTED
    /*ADS-VPLS related processing */
    /* ROUTES_EXT_COMM_SET_STATUS_TBL has prefixes for IPv4/Ipv6 family 
       and does not contain l2vpn,vpls prefixes but routes 
       locally generated need to forward */
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRouteProfile),
                           BGP4_RT_SAFI_INFO (pRouteProfile), u4AsafiMask);
    if (CAP_MP_L2VPN_VPLS == u4AsafiMask)
    {
        if (BGP4_RT_PROTOCOL (pRouteProfile) != BGP_ID)
        {
            return EXT_COMM_SET;
        }
    }
#endif

    RouteExtCommSetStatus.u4Context = BGP4_RT_CXT_ID (pRouteProfile);
    Bgp4CopyNetAddressStruct (&(RouteExtCommSetStatus.RtNetAddress),
                              BGP4_RT_NET_ADDRESS_INFO (pRouteProfile));
    pRouteExtCommSetStatus =
        BGP4_RB_TREE_GET (ROUTES_EXT_COMM_SET_STATUS_TBL,
                          (tBgp4RBElem *) (&RouteExtCommSetStatus));
    if (pRouteExtCommSetStatus != NULL)
    {
        i1ExtCommSetStatus = pRouteExtCommSetStatus->i1ExtCommSetStatus;
    }
    return i1ExtCommSetStatus;
}

/*****************************************************************************/
/* Function Name : Bgp4AttrUnknownLength                                     */
/* Description   : This routine estimates the number of bytes required to    */
/*                 fill the unknown field.                                   */
/* Input(s)      : BGP Information which contains the unknown field          */
/*                 (pBgp4Info)                                               */
/* Output(s)     : None.                                                     */
/* Return(s)     : Number of bytes required.                                 */
/*****************************************************************************/
UINT2
ExtCommAttrLength (tBgp4Info * pBgp4Info)
{
    UINT2               u2EcommAttrLen = 0;

    BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\tExtCommAttrLength() :");
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_ECOMM_MASK) ==
        BGP4_ATTR_ECOMM_MASK)
    {
        u2EcommAttrLen = (UINT2) (BGP4_INFO_ECOMM_COUNT (pBgp4Info) *
                                  EXT_COMM_VALUE_LEN);
        if (u2EcommAttrLen > BGP4_ONE_BYTE_MAX_VAL)
        {
            u2EcommAttrLen += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                BGP4_EXTENDED_ATTR_LEN_SIZE;
        }
        else
        {
            u2EcommAttrLen += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                BGP4_NORMAL_ATTR_LEN_SIZE;
        }
    }

    BGP4_DBG1 (BGP4_DBG_CTRL_FLOW, " Len. = %d\n", u2EcommAttrLen);
    return (u2EcommAttrLen);
}

/*****************************************************************************/
/*    Function Name   :  FillCalcExtCommInUpdMesg                            */
/*                                                                           */
/*    Description     : Incoming update message is updated with              */
/*                      ext-comm  path attribute                             */
/*                                                                           */
/*    Input(s)        : pu1UpdateMesg - Incoming update message              */
/*                                                                           */
/*                      pAdvtBgpInfo - Pointer to the path attribute with    */
/*                      Ext-Comm to be filled.                               */
/*                                                                           */
/*    Output(s)       : None.                                                */
/*                                                                           */
/*  <OPTIONAL Fields>:                                                       */
/*  Global Variables Referred :                                              */
/*  Global variables Modified :                                              */
/*                                                                           */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion : None                                                  */
/*                                                                           */
/*  Returns         :   Number of bytes filled with ext-comm path attribute. */
/*                                                                           */
/*****************************************************************************/
UINT2
FillCalcExtCommInUpdMesg (UINT1 *pu1UpdateMesg, tBgp4Info * pAdvtBgpInfo)
{
    UINT2               u2ByteWriteOffset = 0;
    UINT2               u2ExtCommLen = 0;
    UINT1               u1FlagVal = 0;
    UINT1               u1IsExtLenBitSet = BGP4_FALSE;

    if ((BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) & BGP4_ATTR_ECOMM_MASK) !=
        BGP4_ATTR_ECOMM_MASK)
    {
        return 0;
    }

    u2ExtCommLen = (UINT2) (BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo) *
                            EXT_COMM_VALUE_LEN);
    u1IsExtLenBitSet = (u2ExtCommLen > BGP4_ONE_BYTE_MAX_VAL) ?
        BGP4_TRUE : BGP4_FALSE;
    /* Fill the Ext communities  - update the Type Flag, Type Code,
     * Length of ext-comm  attribute */

    u1FlagVal = (UINT1) (BGP4_OPTIONAL_FLAG_MASK | BGP4_TRANSITIVE_FLAG_MASK);
    if (u1IsExtLenBitSet == BGP4_TRUE)
    {
        u1FlagVal |= (UINT1) BGP4_EXT_LEN_FLAG_MASK;
    }
    if (BGP4_INFO_ECOMM_ATTR_FLAG (pAdvtBgpInfo) & BGP4_PARTIAL_FLAG_MASK)
    {
        u1FlagVal |= (UINT1) BGP4_PARTIAL_FLAG_MASK;
    }

    *(pu1UpdateMesg + u2ByteWriteOffset) = u1FlagVal;
    u2ByteWriteOffset += BGP4_ATYPE_FLAGS_LEN;

    *(pu1UpdateMesg + u2ByteWriteOffset) = EXT_COMM_TYPE_CODE;
    u2ByteWriteOffset += BGP4_ATYPE_CODE_LEN;

    /* Fill Length field. */
    if (u1IsExtLenBitSet == BGP4_FALSE)
    {
        *(pu1UpdateMesg + u2ByteWriteOffset) = (UINT1) u2ExtCommLen;
        u2ByteWriteOffset += BGP4_NORMAL_ATTR_LEN_SIZE;
    }
    else
    {
        PTR_ASSIGN2 ((pu1UpdateMesg + u2ByteWriteOffset), (u2ExtCommLen));
        u2ByteWriteOffset += BGP4_EXTENDED_ATTR_LEN_SIZE;
    }

    /* Fill ECOMM Value. */
    MEMCPY ((pu1UpdateMesg + u2ByteWriteOffset),
            BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo), u2ExtCommLen);
    u2ByteWriteOffset = (UINT2) (u2ByteWriteOffset + u2ExtCommLen);
    return u2ByteWriteOffset;
}

#endif /* _BGECATTR_C */
