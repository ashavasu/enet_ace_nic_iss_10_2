/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgrfllow.c,v 1.20 2013/04/29 13:25:31 siva Exp $
 *
 * Description: Contains BGP Route Reflector feature low level routines
 *
 *******************************************************************/
# include  "bgp4com.h"
# include  "include.h"
# include  "fsbgpcon.h"
# include  "fsbgpogi.h"
# include  "midconst.h"
# include  "snmccons.h"
# include  "snmcdefn.h"
#include "fsmpbgcli.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4RflbgpClusterId
 Input       :  The Indices

                The Object 
                retValFsbgp4RflbgpClusterId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4RflbgpClusterId (tSNMP_OCTET_STRING_TYPE *
                             pRetValFsbgp4RflbgpClusterId)
{
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    PTR_ASSIGN4 ((pRetValFsbgp4RflbgpClusterId->pu1_OctetList),
                 RFL_CLUSTER_ID (u4Context));
    if ((RFL_CLIENT_SUPPORT (u4Context) != RFL_NONE)
        && (RFL_CLUSTER_ID (u4Context) == 0))
    {
        PTR_ASSIGN4 ((pRetValFsbgp4RflbgpClusterId->pu1_OctetList),
                     BGP4_LOCAL_BGP_ID (u4Context));
    }
    pRetValFsbgp4RflbgpClusterId->i4_Length = CLUSTER_ID_LENGTH;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4RflRflSupport
 Input       :  The Indices

                The Object 
                retValFsbgp4RflRflSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4RflRflSupport (INT4 *pi4RetValFsbgp4RflRflSupport)
{
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    if ((RFL_CLIENT_SUPPORT (u4Context) == CLIENT_SUPPORT) ||
        (RFL_CLIENT_SUPPORT (u4Context) == NO_CLIENT_SUPPORT))
    {
        *pi4RetValFsbgp4RflRflSupport = RFL_CLIENT_SUPPORT (u4Context);
    }
    else
    {
        *pi4RetValFsbgp4RflRflSupport = RFL_NONE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4RflbgpClusterId
 Input       :  The Indices

                The Object 
                setValFsbgp4RflbgpClusterId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4RflbgpClusterId (tSNMP_OCTET_STRING_TYPE *
                             pSetValFsbgp4RflbgpClusterId)
{
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i1RetVal = RflSetFsbgp4RflbgpClusterId
        (OSIX_NTOHL
         (*((UINT4 *) (VOID *) pSetValFsbgp4RflbgpClusterId->pu1_OctetList)));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4RflbgpClusterId, u4SeqNum,
                          FALSE, BgpLock, BgpUnLock, 1, i1RetVal);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", gpBgpCurrCxtNode->u4ContextId,
                      pSetValFsbgp4RflbgpClusterId));
    return i1RetVal;
}

/****************************************************************************
 Function    :  RflSetFsbgp4RflbgpClusterId
 Input       :  The Indices

                The Object
                u4SetValFsbgp4RflbgpClusterId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = RflSetFsbgp4RflbgpClusterId ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
RflSetFsbgp4RflbgpClusterId (UINT4 u4SetValFsbgp4RflbgpClusterId)
{
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP AS number not configured.\n");
        return SNMP_FAILURE;
    }
    if (Bgp4EnqueMsgToBgpQ (u4Context, BGP4_SNMP_RFL_CLUSTER_ID_EVENT,
                            u4SetValFsbgp4RflbgpClusterId) == SNMP_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to Post message to BGP Task.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsbgp4RflRflSupport
 Input       :  The Indices

                The Object 
                setValFsbgp4RflRflSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4RflRflSupport (INT4 i4SetValFsbgp4RflRflSupport)
{
    UINT4               u4Context;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP AS number not configured.\n");
        return SNMP_FAILURE;
    }
    if (Bgp4EnqueMsgToBgpQ (u4Context, BGP4_SNMP_RFL_CLIENT_SUPP_EVENT,
                            (UINT4) i4SetValFsbgp4RflRflSupport) ==
        SNMP_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to Post message to BGP Task.\n");
        return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4RflRflSupport, u4SeqNum,
                          FALSE, BgpLock, BgpUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gpBgpCurrCxtNode->u4ContextId,
                      i4SetValFsbgp4RflRflSupport));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4RflbgpClusterId
 Input       :  The Indices

                The Object 
                testValFsbgp4RflbgpClusterId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4RflbgpClusterId (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsbgp4RflbgpClusterId)
{
    UINT4               u4bgpClusterId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (pTestValFsbgp4RflbgpClusterId->i4_Length != CLUSTER_ID_LENGTH)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Cluster-Id length.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    PTR_FETCH4 (u4bgpClusterId, (pTestValFsbgp4RflbgpClusterId->pu1_OctetList));

    return (RflTestv2Fsbgp4RflbgpClusterId (pu4ErrorCode, u4bgpClusterId));
}

/****************************************************************************
 Function    :  RflTestv2Fsbgp4RflbgpClusterId
 Input       :  The Indices

                The Object
                u4bgpClusterId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Fsbgp4RflbgpClusterId ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
RflTestv2Fsbgp4RflbgpClusterId (UINT4 *pu4ErrorCode, UINT4 u4bgpClusterId)
{
    UNUSED_PARAM (pu4ErrorCode);
    /* To disable Route Reflector feature */
    if (u4bgpClusterId == NO_CLUSTER_ID)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4RflRflSupport
 Input       :  The Indices

                The Object 
                testValFsbgp4RflRflSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4RflRflSupport (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsbgp4RflRflSupport)
{
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsbgp4RflRflSupport == NO_CLIENT_SUPPORT) ||
        (i4TestValFsbgp4RflRflSupport == CLIENT_SUPPORT))
    {
        return SNMP_SUCCESS;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - Invalid RFL Client Support.\n");
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    CLI_SET_ERR (CLI_BGP4_INVALID_RFL_OPT_VALUE_ERR);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4RflbgpClusterId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4RflbgpClusterId (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4RflRflSupport
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4RflRflSupport (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
