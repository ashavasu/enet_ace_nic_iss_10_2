/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgrrlow.c,v 1.24 2016/12/27 12:35:55 siva Exp $
 *
 * Description: This file contains functions related to
 *              Route-Refresh feature.
 *
 *******************************************************************/

# include  "bgp4com.h"
# include  "include.h"
# include  "fsbgpcon.h"
# include  "fsbgpogi.h"
# include  "midconst.h"
# include "fsmpbgcli.h"

/* LOW LEVEL Routines for Table : Fsbgp4MpeRtRefreshInboundTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4MpeRtRefreshInboundTable
 Input       :  The Indices
                Fsbgp4RtRefreshInboundPeerType
                Fsbgp4RtRefreshInboundPeerAddr
                Fsbgp4RtRefreshInboundAfi
                Fsbgp4RtRefreshInboundSafi
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4MpeRtRefreshInboundTable (INT4
                                                        i4Fsbgp4RtRefreshInboundPeerType,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pFsbgp4RtRefreshInboundPeerAddr,
                                                        INT4
                                                        i4Fsbgp4RtRefreshInboundAfi,
                                                        INT4
                                                        i4Fsbgp4RtRefreshInboundSafi)
{
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;
    UINT1               u1ValidStatus;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4RtRefreshInboundPeerType,
                                 pFsbgp4RtRefreshInboundPeerAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4ValidateBgpPeerTableIndex (PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        return SNMP_FAILURE;
    }
    u1ValidStatus =
        Bgp4MpeValidateAddressType ((UINT4) i4Fsbgp4RtRefreshInboundAfi);
    if (u1ValidStatus == BGP4_FALSE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address Type.\n");
        return SNMP_FAILURE;
    }
    u1ValidStatus =
        Bgp4MpeValidateSubAddressType ((UINT4) i4Fsbgp4RtRefreshInboundSafi);
    if (u1ValidStatus == BGP4_FALSE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Sub-Address Type.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4MpeRtRefreshInboundTable
 Input       :  The Indices
                Fsbgp4RtRefreshInboundPeerType
                Fsbgp4RtRefreshInboundPeerAddr
                Fsbgp4RtRefreshInboundAfi
                Fsbgp4RtRefreshInboundSafi
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4MpeRtRefreshInboundTable (INT4
                                                *pi4Fsbgp4RtRefreshInboundPeerType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsbgp4RtRefreshInboundPeerAddr,
                                                INT4
                                                *pi4Fsbgp4RtRefreshInboundAfi,
                                                INT4
                                                *pi4Fsbgp4RtRefreshInboundSafi)
{
    UNUSED_PARAM (pi4Fsbgp4RtRefreshInboundSafi);
    UNUSED_PARAM (pFsbgp4RtRefreshInboundPeerAddr);
    UNUSED_PARAM (pi4Fsbgp4RtRefreshInboundPeerType);
    UNUSED_PARAM (pi4Fsbgp4RtRefreshInboundAfi);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4RtRefreshInboundTable
 Input       :  The Indices
                Fsbgp4RtRefreshInboundPeerType
                nextFsbgp4RtRefreshInboundPeerType
                Fsbgp4RtRefreshInboundPeerAddr
                nextFsbgp4RtRefreshInboundPeerAddr
                Fsbgp4RtRefreshInboundAfi
                nextFsbgp4RtRefreshInboundAfi
                Fsbgp4RtRefreshInboundSafi
                nextFsbgp4RtRefreshInboundSafi
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4MpeRtRefreshInboundTable (INT4
                                               i4Fsbgp4RtRefreshInboundPeerType,
                                               INT4
                                               *pi4NextFsbgp4RtRefreshInboundPeerType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsbgp4RtRefreshInboundPeerAddr,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pNextFsbgp4RtRefreshInboundPeerAddr,
                                               INT4 i4Fsbgp4RtRefreshInboundAfi,
                                               INT4
                                               *pi4NextFsbgp4RtRefreshInboundAfi,
                                               INT4
                                               i4Fsbgp4RtRefreshInboundSafi,
                                               INT4
                                               *pi4NextFsbgp4RtRefreshInboundSafi)
{
    UNUSED_PARAM (i4Fsbgp4RtRefreshInboundSafi);
    UNUSED_PARAM (i4Fsbgp4RtRefreshInboundPeerType);
    UNUSED_PARAM (i4Fsbgp4RtRefreshInboundAfi);
    UNUSED_PARAM (pFsbgp4RtRefreshInboundPeerAddr);
    UNUSED_PARAM (pi4NextFsbgp4RtRefreshInboundSafi);
    UNUSED_PARAM (pNextFsbgp4RtRefreshInboundPeerAddr);
    UNUSED_PARAM (pi4NextFsbgp4RtRefreshInboundAfi);
    UNUSED_PARAM (pi4NextFsbgp4RtRefreshInboundPeerType);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRtRefreshInboundRequest
 Input       :  The Indices
                Fsbgp4RtRefreshInboundPeerType
                Fsbgp4RtRefreshInboundPeerAddr
                Fsbgp4RtRefreshInboundAfi
                Fsbgp4RtRefreshInboundSafi

                The Object 
                retValFsbgp4RtRefreshInboundRequest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRtRefreshInboundRequest (INT4 i4Fsbgp4RtRefreshInboundPeerType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsbgp4RtRefreshInboundPeerAddr,
                                        INT4 i4Fsbgp4RtRefreshInboundAfi,
                                        INT4 i4Fsbgp4RtRefreshInboundSafi,
                                        INT4
                                        *pi4RetValFsbgp4RtRefreshInboundRequest)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tAddrPrefix         PeerAddress;
    tAddrPrefix         InvPrefix;
    INT4                i4Status = 0;
    UINT4               u4Index;
    INT4                i4ReqForAll = BGP4_FALSE;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Status = Bgp4LowGetIpAddress (i4Fsbgp4RtRefreshInboundPeerType,
                                    pFsbgp4RtRefreshInboundPeerAddr,
                                    &PeerAddress);
    if (i4Status == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i4Status = Bgp4GetAfiSafiIndex ((UINT2) i4Fsbgp4RtRefreshInboundAfi,
                                    (UINT1) i4Fsbgp4RtRefreshInboundSafi,
                                    &u4Index);
    if (i4Status == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to get the AFI/SAFI index.\n");
        return SNMP_FAILURE;
    }

    Bgp4InitAddrPrefixStruct (&(InvPrefix),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress));
    i4ReqForAll = PrefixMatch (InvPrefix, PeerAddress);
    if (i4ReqForAll != BGP4_TRUE)
    {
        pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
        if (pPeerEntry == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - No matching Peer exist.\n");
            return SNMP_FAILURE;
        }
        /* Check whether the <AFI, SAFI> is negotiated or not */
        if ((pPeerEntry->apAsafiInstance[u4Index] == NULL) ||
            (BGP4_PEER_RTREF_DATA_PTR (pPeerEntry, u4Index) == NULL))
        {
            return SNMP_SUCCESS;
        }
        *pi4RetValFsbgp4RtRefreshInboundRequest =
            BGP4_RTREF_MSG_REQ_PEER (pPeerEntry, u4Index);
    }
    else
    {
        *pi4RetValFsbgp4RtRefreshInboundRequest = TRUE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRtRefreshInboundPrefixFilter
 Input       :  The Indices
                Fsbgp4mpeRtRefreshInboundPeerType
                Fsbgp4mpeRtRefreshInboundPeerAddr
                Fsbgp4mpeRtRefreshInboundAfi
                Fsbgp4mpeRtRefreshInboundSafi

                The Object
                retValFsbgp4mpeRtRefreshInboundPrefixFilter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRtRefreshInboundPrefixFilter (INT4
                                             i4Fsbgp4mpeRtRefreshInboundPeerType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsbgp4mpeRtRefreshInboundPeerAddr,
                                             INT4
                                             i4Fsbgp4mpeRtRefreshInboundAfi,
                                             INT4
                                             i4Fsbgp4mpeRtRefreshInboundSafi,
                                             INT4
                                             *pi4RetValFsbgp4mpeRtRefreshInboundPrefixFilter)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tAddrPrefix         PeerAddress;
    INT4                i4Status = 0;
    UINT4               u4Index;
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Status = Bgp4LowGetIpAddress (i4Fsbgp4mpeRtRefreshInboundPeerType,
                                    pFsbgp4mpeRtRefreshInboundPeerAddr,
                                    &PeerAddress);
    if (i4Status == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i4Status = Bgp4GetAfiSafiIndex ((UINT2) i4Fsbgp4mpeRtRefreshInboundAfi,
                                    (UINT1) i4Fsbgp4mpeRtRefreshInboundSafi,
                                    &u4Index);
    if (i4Status == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to get the AFI/SAFI index.\n");
        return SNMP_FAILURE;
    }

    pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeerEntry == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    /* Check whether the <AFI, SAFI> is negotiated or not */
    if ((pPeerEntry->apAsafiInstance[u4Index] == NULL) ||
        (BGP4_PEER_RTREF_DATA_PTR (pPeerEntry, u4Index) == NULL))
    {
        return SNMP_SUCCESS;
    }
    *pi4RetValFsbgp4mpeRtRefreshInboundPrefixFilter =
        BGP4_ORF_MSG_REQ_PEER (pPeerEntry, u4Index);

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4mpeRtRefreshInboundRequest
 Input       :  The Indices
                Fsbgp4RtRefreshInboundPeerType
                Fsbgp4RtRefreshInboundPeerAddr
                Fsbgp4RtRefreshInboundAfi
                Fsbgp4RtRefreshInboundSafi

                The Object 
                setValFsbgp4RtRefreshInboundRequest
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4mpeRtRefreshInboundRequest (INT4 i4Fsbgp4RtRefreshInboundPeerType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsbgp4RtRefreshInboundPeerAddr,
                                        INT4 i4Fsbgp4RtRefreshInboundAfi,
                                        INT4 i4Fsbgp4RtRefreshInboundSafi,
                                        INT4
                                        i4SetValFsbgp4RtRefreshInboundRequest)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tBgp4QMsg          *pQMsg = NULL;
    tAddrPrefix         PeerAddress;
    tAddrPrefix         InvPrefix;
    INT4                i4Status = 0;
    UINT4               u4AfiSafiMask;
    UINT4               u4Index;
    INT4                i4ReqForAll = BGP4_FALSE;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Status = Bgp4LowGetIpAddress (i4Fsbgp4RtRefreshInboundPeerType,
                                    pFsbgp4RtRefreshInboundPeerAddr,
                                    &PeerAddress);
    if (i4Status == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to extract Peer Address.\n");
        return SNMP_FAILURE;
    }
    if (!((i4Fsbgp4RtRefreshInboundAfi == 0) &&
          (i4Fsbgp4RtRefreshInboundSafi == 0)))
    {
        i4Status = Bgp4GetAfiSafiIndex ((UINT2) i4Fsbgp4RtRefreshInboundAfi,
                                        (UINT1) i4Fsbgp4RtRefreshInboundSafi,
                                        &u4Index);
        if (i4Status == BGP4_FAILURE)
        {
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_MGMT_TRC, BGP4_MOD_NAME,
                           "\tERROR - <AFI %d> <SAFI %d> not supported.\n",
                           i4Fsbgp4RtRefreshInboundAfi,
                           i4Fsbgp4RtRefreshInboundSafi);
            return SNMP_FAILURE;
        }
    }

    Bgp4InitAddrPrefixStruct (&(InvPrefix),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress));
    i4ReqForAll = PrefixMatch (InvPrefix, PeerAddress);
    if (i4ReqForAll != BGP4_TRUE)
    {
        pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
        if (pPeerEntry == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - No matching Peer exist.\n");
            return SNMP_FAILURE;
        }
        if (!((i4Fsbgp4RtRefreshInboundAfi == 0) &&
              (i4Fsbgp4RtRefreshInboundSafi == 0)))
        {
            /* Check whether the <AFI, SAFI> is negotiated or not */
            if ((pPeerEntry->apAsafiInstance[u4Index] == NULL) ||
                (BGP4_PEER_RTREF_DATA_PTR (pPeerEntry, u4Index) == NULL))
            {
                return SNMP_SUCCESS;
            }
        }
    }

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting BGP Route Refresh "
                  "Msg to BGP Queue FAILED!!!\n");
        return SNMP_FAILURE;
    }
    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_SNMP_INBOUND_SOFTCONFIG_RTREF_EVENT;
    Bgp4CopyAddrPrefixStruct (&(BGP4_INPUTQ_RTREF_PEER_ADDR_INFO (pQMsg)),
                              PeerAddress);
    BGP4_GET_AFISAFI_MASK ((UINT2) i4Fsbgp4RtRefreshInboundAfi,
                           (UINT2) i4Fsbgp4RtRefreshInboundSafi, u4AfiSafiMask);
    BGP4_INPUTQ_RTREF_ASAFI_MASK (pQMsg) = u4AfiSafiMask;
#ifdef L3VPN
    if (u4AfiSafiMask == CAP_MP_VPN4_UNICAST)
    {
        /* inbound policy is changed for vpnv4 routes. Hence send the
         * route-refresh to PE peers
         */
        BGP4_INPUTQ_RTREF_OPER_TYPE (pQMsg) = BGP4_RR_ADVT_PE_PEERS;
    }
    else
    {
        BGP4_INPUTQ_RTREF_OPER_TYPE (pQMsg) = BGP4_RR_ADVT_DEFAULT;
    }
#endif

#ifdef VPLSADS_WANTED
    if (u4AfiSafiMask == CAP_MP_L2VPN_VPLS)
    {
        /* inbound policy is changed for VPLS routes. Hence send the
         * route-refresh to internal peers
         */
        BGP4_INPUTQ_RTREF_OPER_TYPE (pQMsg) = BGP4_RR_ADVT_VPLS_PE_PEERS;
    }
#endif
#ifdef EVPN_WANTED
    if (u4AfiSafiMask == CAP_MP_L2VPN_EVPN)
    {
        /* inbound policy is changed for EVPN routes. Hence send the
         * route-refresh to internal peers
         */
        BGP4_INPUTQ_RTREF_OPER_TYPE (pQMsg) = BGP4_RR_ADVT_EVPN_PEERS;
    }
#endif

    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
    BGP4_INPUTQ_CXT (pQMsg) = u4Context;
    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (i4ReqForAll != BGP4_TRUE)
    {
        if (!((i4Fsbgp4RtRefreshInboundAfi == 0) &&
              (i4Fsbgp4RtRefreshInboundSafi == 0)))
        {
            BGP4_RTREF_MSG_REQ_PEER (pPeerEntry, u4Index) = (UINT1)
                (i4SetValFsbgp4RtRefreshInboundRequest);
        }
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4mpeRtRefreshInboundRequest,
                          u4SeqNum, FALSE, BgpLock, BgpUnLock, 5, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %i %i %i",
                      gpBgpCurrCxtNode->u4ContextId,
                      i4Fsbgp4RtRefreshInboundPeerType,
                      pFsbgp4RtRefreshInboundPeerAddr,
                      i4Fsbgp4RtRefreshInboundAfi, i4Fsbgp4RtRefreshInboundSafi,
                      i4SetValFsbgp4RtRefreshInboundRequest));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsbgp4mpeRtRefreshInboundPrefixFilter
 Input       :  The Indices
                Fsbgp4mpeRtRefreshInboundPeerType
                Fsbgp4mpeRtRefreshInboundPeerAddr
                Fsbgp4mpeRtRefreshInboundAfi
                Fsbgp4mpeRtRefreshInboundSafi

                The Object
                setValFsbgp4mpeRtRefreshInboundPrefixFilter
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4mpeRtRefreshInboundPrefixFilter (INT4
                                             i4Fsbgp4mpeRtRefreshInboundPeerType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsbgp4mpeRtRefreshInboundPeerAddr,
                                             INT4
                                             i4Fsbgp4mpeRtRefreshInboundAfi,
                                             INT4
                                             i4Fsbgp4mpeRtRefreshInboundSafi,
                                             INT4
                                             i4SetValFsbgp4mpeRtRefreshInboundPrefixFilter)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tBgp4QMsg          *pQMsg = NULL;
    tAddrPrefix         PeerAddress;
    tAddrPrefix         InvPrefix;
    INT4                i4Status = 0;
    UINT4               u4AfiSafiMask = 0;
    UINT4               u4Index = 0;
    INT4                i4ReqForAll = BGP4_FALSE;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Status = Bgp4LowGetIpAddress (i4Fsbgp4mpeRtRefreshInboundPeerType,
                                    pFsbgp4mpeRtRefreshInboundPeerAddr,
                                    &PeerAddress);
    if (i4Status == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to extract Peer Address.\n");
        return SNMP_FAILURE;
    }
    i4Status = Bgp4GetAfiSafiIndex ((UINT2) i4Fsbgp4mpeRtRefreshInboundAfi,
                                    (UINT2) i4Fsbgp4mpeRtRefreshInboundSafi,
                                    &u4Index);
    if (i4Status == BGP4_FAILURE)
    {
        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_MGMT_TRC, BGP4_MOD_NAME,
                       "\tERROR - <AFI %d> <SAFI %d> not supported.\n",
                       i4Fsbgp4mpeRtRefreshInboundAfi,
                       i4Fsbgp4mpeRtRefreshInboundSafi);
        return SNMP_FAILURE;
    }

    Bgp4InitAddrPrefixStruct (&(InvPrefix),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress));
    i4ReqForAll = PrefixMatch (InvPrefix, PeerAddress);
    if (i4ReqForAll != BGP4_TRUE)
    {
        pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
        if (pPeerEntry == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - No matching Peer exist.\n");
            return SNMP_FAILURE;
        }
        /* Check whether the <AFI, SAFI> is negotiated or not */
        if ((pPeerEntry->apAsafiInstance[u4Index] == NULL) ||
            (BGP4_PEER_RTREF_DATA_PTR (pPeerEntry, u4Index) == NULL))
        {
            return SNMP_SUCCESS;
        }
    }
    if (i4ReqForAll != BGP4_TRUE)
    {
        BGP4_ORF_MSG_REQ_PEER (pPeerEntry, u4Index) = (UINT1)
            (i4SetValFsbgp4mpeRtRefreshInboundPrefixFilter);
    }

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting BGP ORF message request "
                  "to BGP Queue FAILED!!!\n");
        return SNMP_FAILURE;
    }
    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_SNMP_INBOUND_ORF_EVENT;
    Bgp4CopyAddrPrefixStruct (&(BGP4_INPUTQ_RTREF_PEER_ADDR_INFO (pQMsg)),
                              PeerAddress);
    BGP4_GET_AFISAFI_MASK ((UINT2) i4Fsbgp4mpeRtRefreshInboundAfi,
                           (UINT2) i4Fsbgp4mpeRtRefreshInboundSafi,
                           u4AfiSafiMask);
    BGP4_INPUTQ_RTREF_ASAFI_MASK (pQMsg) = u4AfiSafiMask;

    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
    BGP4_INPUTQ_CXT (pQMsg) = u4Context;

    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIBgp4mpeRtRefreshInboundPrefixFilter, u4SeqNum,
                          FALSE, BgpLock, BgpUnLock, 5, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %i %i %i",
                      gpBgpCurrCxtNode->u4ContextId,
                      i4Fsbgp4mpeRtRefreshInboundPeerType,
                      pFsbgp4mpeRtRefreshInboundPeerAddr,
                      i4Fsbgp4mpeRtRefreshInboundAfi,
                      i4Fsbgp4mpeRtRefreshInboundSafi,
                      i4SetValFsbgp4mpeRtRefreshInboundPrefixFilter));
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4mpeRtRefreshInboundRequest
 Input       :  The Indices
                Fsbgp4RtRefreshInboundPeerType
                Fsbgp4RtRefreshInboundPeerAddr
                Fsbgp4RtRefreshInboundAfi
                Fsbgp4RtRefreshInboundSafi

                The Object 
                testValFsbgp4RtRefreshInboundRequest
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4mpeRtRefreshInboundRequest (UINT4 *pu4ErrorCode,
                                           INT4
                                           i4Fsbgp4RtRefreshInboundPeerType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsbgp4RtRefreshInboundPeerAddr,
                                           INT4 i4Fsbgp4RtRefreshInboundAfi,
                                           INT4 i4Fsbgp4RtRefreshInboundSafi,
                                           INT4
                                           i4TestValFsbgp4RtRefreshInboundRequest)
{
    tAddrPrefix         PeerAddress;
    tAddrPrefix         InvPrefix;
    INT4                i4Status = 0;
    tBgp4PeerEntry     *pPeerEntry = NULL;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    i4Status = Bgp4LowGetIpAddress (i4Fsbgp4RtRefreshInboundPeerType,
                                    pFsbgp4RtRefreshInboundPeerAddr,
                                    &PeerAddress);
    if (i4Status == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Status = Bgp4IsValidAddress (PeerAddress, BGP4_FALSE);
    Bgp4InitAddrPrefixStruct (&(InvPrefix),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress));
    if ((i4Status == BGP4_FALSE) &&
        ((PrefixMatch (InvPrefix, PeerAddress) != BGP4_TRUE)))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }

    if (i4TestValFsbgp4RtRefreshInboundRequest != BGP4_RTREF_REQ_SET)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Route Refresh Inbound Request.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_RT_REF_IN_REQ_ERR);
        return SNMP_FAILURE;
    }

    if (!((i4Fsbgp4RtRefreshInboundAfi == 0) &&
          (i4Fsbgp4RtRefreshInboundSafi == 0)))
    {
        i4Status =
            Bgp4MpeValidateAddressType ((UINT4) i4Fsbgp4RtRefreshInboundAfi);
        if (i4Status == BGP4_FALSE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Peer Address Type.\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        i4Status =
            Bgp4MpeValidateSubAddressType ((UINT4)
                                           i4Fsbgp4RtRefreshInboundSafi);
        if (i4Status == BGP4_FALSE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Peer Sub-Address Type.\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    if (PrefixMatch (InvPrefix, PeerAddress) != BGP4_TRUE)
    {
        pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
        if (pPeerEntry == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_BGP4_PEER_NOT_ENABLED);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4mpeRtRefreshInboundPrefixFilter
 Input       :  The Indices
                Fsbgp4mpeRtRefreshInboundPeerType
                Fsbgp4mpeRtRefreshInboundPeerAddr
                Fsbgp4mpeRtRefreshInboundAfi
                Fsbgp4mpeRtRefreshInboundSafi

                The Object
                testValFsbgp4mpeRtRefreshInboundPrefixFilter
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4mpeRtRefreshInboundPrefixFilter (UINT4 *pu4ErrorCode,
                                                INT4
                                                i4Fsbgp4mpeRtRefreshInboundPeerType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsbgp4mpeRtRefreshInboundPeerAddr,
                                                INT4
                                                i4Fsbgp4mpeRtRefreshInboundAfi,
                                                INT4
                                                i4Fsbgp4mpeRtRefreshInboundSafi,
                                                INT4
                                                i4TestValFsbgp4mpeRtRefreshInboundPrefixFilter)
{
    tAddrPrefix         PeerAddress;
    tAddrPrefix         InvPrefix;
    INT4                i4Status = 0;
    tBgp4PeerEntry     *pPeerEntry = NULL;
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    i4Status = Bgp4LowGetIpAddress (i4Fsbgp4mpeRtRefreshInboundPeerType,
                                    pFsbgp4mpeRtRefreshInboundPeerAddr,
                                    &PeerAddress);
    if (i4Status == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to extract Peer Address.\n");
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }
    i4Status = Bgp4IsValidAddress (PeerAddress, BGP4_FALSE);
    Bgp4InitAddrPrefixStruct (&(InvPrefix),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress));
    if ((i4Status == BGP4_FALSE) &&
        ((PrefixMatch (InvPrefix, PeerAddress) != BGP4_TRUE)))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return SNMP_FAILURE;
    }

    if (i4TestValFsbgp4mpeRtRefreshInboundPrefixFilter != BGP4_ORF_REQ_SET)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Inbound ORF Request.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Fsbgp4mpeRtRefreshInboundAfi != 0) &&
        (i4Fsbgp4mpeRtRefreshInboundSafi != 0))
    {
        i4Status =
            Bgp4MpeValidateAddressType ((UINT4) i4Fsbgp4mpeRtRefreshInboundAfi);
        if (i4Status == BGP4_FALSE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Peer Address Type.\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        i4Status =
            Bgp4MpeValidateSubAddressType ((UINT4)
                                           i4Fsbgp4mpeRtRefreshInboundSafi);
        if (i4Status == BGP4_FALSE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Peer Sub-Address Type.\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    if (PrefixMatch (InvPrefix, PeerAddress) != BGP4_TRUE)
    {
        pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
        if (pPeerEntry == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_BGP4_PEER_NOT_ENABLED);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4MpeRtRefreshInboundTable
 Input       :  The Indices
                Fsbgp4RtRefreshInboundPeerType
                Fsbgp4RtRefreshInboundPeerAddr
                Fsbgp4RtRefreshInboundAfi
                Fsbgp4RtRefreshInboundSafi
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4MpeRtRefreshInboundTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4MpeRtRefreshStatisticsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4MpeRtRefreshStatisticsTable
 Input       :  The Indices
                Fsbgp4RtRefreshStatisticsPeerType
                Fsbgp4RtRefreshStatisticsPeerAddr
                Fsbgp4RtRefreshStatisticsAfi
                Fsbgp4RtRefreshStatisticsSafi
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4MpeRtRefreshStatisticsTable (INT4
                                                           i4Fsbgp4RtRefreshStatisticsPeerType,
                                                           tSNMP_OCTET_STRING_TYPE
                                                           *
                                                           pFsbgp4RtRefreshStatisticsPeerAddr,
                                                           INT4
                                                           i4Fsbgp4RtRefreshStatisticsAfi,
                                                           INT4
                                                           i4Fsbgp4RtRefreshStatisticsSafi)
{
    tAddrPrefix         PeerAddress;
    INT4                i4Status = 0;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Status = Bgp4LowGetIpAddress (i4Fsbgp4RtRefreshStatisticsPeerType,
                                    pFsbgp4RtRefreshStatisticsPeerAddr,
                                    &PeerAddress);
    if (i4Status == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Status = Bgp4IsValidAddress (PeerAddress, BGP4_FALSE);
    if (i4Status == BGP4_FALSE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        return SNMP_FAILURE;
    }
    i4Status =
        Bgp4MpeValidateAddressType ((UINT4) i4Fsbgp4RtRefreshStatisticsAfi);
    if (i4Status == BGP4_FALSE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address Type.\n");
        return SNMP_FAILURE;
    }
    i4Status =
        Bgp4MpeValidateSubAddressType ((UINT4) i4Fsbgp4RtRefreshStatisticsSafi);
    if (i4Status == BGP4_FALSE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Sub-Address Type.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4MpeRtRefreshStatisticsTable
 Input       :  The Indices
                Fsbgp4RtRefreshStatisticsPeerType
                Fsbgp4RtRefreshStatisticsPeerAddr
                Fsbgp4RtRefreshStatisticsAfi
                Fsbgp4RtRefreshStatisticsSafi
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4MpeRtRefreshStatisticsTable (INT4
                                                   *pi4Fsbgp4RtRefreshStatisticsPeerType,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pFsbgp4RtRefreshStatisticsPeerAddr,
                                                   INT4
                                                   *pi4Fsbgp4RtRefreshStatisticsAfi,
                                                   INT4
                                                   *pi4Fsbgp4RtRefreshStatisticsSafi)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         InvPrefix;
    INT4                i4Sts;
    UINT4               u4Index = 0;
    UINT2               u2Afi = 0;
    UINT2               u2Safi = 0;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    /* Get first peer entry */
    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
    {
        Bgp4InitAddrPrefixStruct (&InvPrefix,
                                  BGP4_AFI_IN_ADDR_PREFIX_INFO
                                  (BGP4_PEER_REMOTE_ADDR_INFO (pPeer)));
        if (((PrefixMatch (InvPrefix, BGP4_PEER_REMOTE_ADDR_INFO (pPeer))) !=
             BGP4_TRUE) && (BGP4_PEER_RTREF_DATA_PTR (pPeer, u4Index) != NULL))
        {
            /* Currently only IPV4 is supported */
            *pi4Fsbgp4RtRefreshStatisticsPeerType =
                BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_PEER_REMOTE_ADDR_INFO
                                              (pPeer));
            i4Sts =
                Bgp4LowSetIpAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO
                                     (BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                     pFsbgp4RtRefreshStatisticsPeerAddr,
                                     BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
           if (i4Sts == BGP4_FAILURE)
           {
               BGP4_TRC (NULL, BGP4_TRC_FLAG,
                 BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                "\tERROR - nmhGetFirstIndexFsbgp4MpeRtRefreshStatisticsTable()"
                "Setting Ip address failed.\n");
           }
           Bgp4GetAfiSafiFromIndex (BGP4_IPV4_UNI_INDEX, &u2Afi, &u2Safi);
           *pi4Fsbgp4RtRefreshStatisticsAfi = (INT4) u2Afi;
           *pi4Fsbgp4RtRefreshStatisticsSafi = (INT4) u2Safi;

            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4MpeRtRefreshStatisticsTable
 Input       :  The Indices
                Fsbgp4RtRefreshStatisticsPeerType
                nextFsbgp4RtRefreshStatisticsPeerType
                Fsbgp4RtRefreshStatisticsPeerAddr
                nextFsbgp4RtRefreshStatisticsPeerAddr
                Fsbgp4RtRefreshStatisticsAfi
                nextFsbgp4RtRefreshStatisticsAfi
                Fsbgp4RtRefreshStatisticsSafi
                nextFsbgp4RtRefreshStatisticsSafi
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4MpeRtRefreshStatisticsTable (INT4
                                                  i4Fsbgp4RtRefreshStatisticsPeerType,
                                                  INT4
                                                  *pi4NextFsbgp4RtRefreshStatisticsPeerType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsbgp4RtRefreshStatisticsPeerAddr,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pNextFsbgp4RtRefreshStatisticsPeerAddr,
                                                  INT4
                                                  i4Fsbgp4RtRefreshStatisticsAfi,
                                                  INT4
                                                  *pi4NextFsbgp4RtRefreshStatisticsAfi,
                                                  INT4
                                                  i4Fsbgp4RtRefreshStatisticsSafi,
                                                  INT4
                                                  *pi4NextFsbgp4RtRefreshStatisticsSafi)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tAddrPrefix         PeerAddress;
    tAddrPrefix         NextPeerAddress;
    INT4                i4Status = 0;
    UINT4               u4Index;
    UINT2               u2Afi = 0;
    UINT2               u2Safi = 0;


    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Status = Bgp4LowGetIpAddress (i4Fsbgp4RtRefreshStatisticsPeerType,
                                    pFsbgp4RtRefreshStatisticsPeerAddr,
                                    &PeerAddress);
    if (i4Status == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeerEntry == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }

    i4Status = Bgp4GetAfiSafiIndex ((UINT2) i4Fsbgp4RtRefreshStatisticsAfi,
                                    (UINT1) i4Fsbgp4RtRefreshStatisticsSafi,
                                    &u4Index);
    if (i4Status == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to get AFI/SAFI Index.\n");
        return SNMP_FAILURE;
    }
    for (u4Index = u4Index + 1; u4Index < BGP4_MPE_ADDR_FAMILY_MAX_INDEX;
         u4Index++)
    {
        if ((pPeerEntry->apAsafiInstance[u4Index] != NULL) &&
            (BGP4_PEER_RTREF_DATA_PTR (pPeerEntry, u4Index) != NULL))
        {
            *pi4NextFsbgp4RtRefreshStatisticsPeerType =
                i4Fsbgp4RtRefreshStatisticsPeerType;
            MEMCPY (pNextFsbgp4RtRefreshStatisticsPeerAddr->pu1_OctetList,
                    pFsbgp4RtRefreshStatisticsPeerAddr->pu1_OctetList,
                    pFsbgp4RtRefreshStatisticsPeerAddr->i4_Length);
            pNextFsbgp4RtRefreshStatisticsPeerAddr->i4_Length =
                pFsbgp4RtRefreshStatisticsPeerAddr->i4_Length;
            Bgp4GetAfiSafiFromIndex (u4Index, &u2Afi, &u2Safi);
            *pi4NextFsbgp4RtRefreshStatisticsAfi = u2Afi;
            *pi4NextFsbgp4RtRefreshStatisticsSafi = u2Safi;

            return SNMP_SUCCESS;
        }
    }

    i4Status = nmhGetNextIndexBgpRtRefPeerTable (PeerAddress, &NextPeerAddress);
    if (i4Status == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsbgp4RtRefreshStatisticsPeerType =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (NextPeerAddress);
    i4Status =
        Bgp4LowSetIpAddress (BGP4_AFI_IN_ADDR_PREFIX_INFO (NextPeerAddress),
                             pNextFsbgp4RtRefreshStatisticsPeerAddr,
                             NextPeerAddress);
    *pi4NextFsbgp4RtRefreshStatisticsAfi = BGP4_INET_AFI_IPV4;
    *pi4NextFsbgp4RtRefreshStatisticsSafi = BGP4_INET_SAFI_UNICAST;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgSentCntr
 Input       :  The Indices
                Fsbgp4RtRefreshStatisticsPeerType
                Fsbgp4RtRefreshStatisticsPeerAddr
                Fsbgp4RtRefreshStatisticsAfi
                Fsbgp4RtRefreshStatisticsSafi

                The Object 
                retValFsbgp4RtRefreshStatisticsRtRefMsgSentCntr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgSentCntr (INT4
                                                    i4Fsbgp4RtRefreshStatisticsPeerType,
                                                    tSNMP_OCTET_STRING_TYPE *
                                                    pFsbgp4RtRefreshStatisticsPeerAddr,
                                                    INT4
                                                    i4Fsbgp4RtRefreshStatisticsAfi,
                                                    INT4
                                                    i4Fsbgp4RtRefreshStatisticsSafi,
                                                    UINT4
                                                    *pu4RetValFsbgp4RtRefreshStatisticsRtRefMsgSentCntr)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tAddrPrefix         PeerAddress;
    INT4                i4Status = 0;
    UINT4               u4Index;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Status = Bgp4LowGetIpAddress (i4Fsbgp4RtRefreshStatisticsPeerType,
                                    pFsbgp4RtRefreshStatisticsPeerAddr,
                                    &PeerAddress);
    if (i4Status == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeerEntry == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    i4Status = Bgp4GetAfiSafiIndex ((UINT2) i4Fsbgp4RtRefreshStatisticsAfi,
                                    (UINT1) i4Fsbgp4RtRefreshStatisticsSafi,
                                    &u4Index);
    if (i4Status == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to get AFI/SAFI Index.\n");
        return SNMP_FAILURE;
    }

    /* Check whether the <AFI, SAFI> is negotiated or not */
    if ((pPeerEntry->apAsafiInstance[u4Index] == NULL) ||
        (BGP4_PEER_RTREF_DATA_PTR (pPeerEntry, u4Index) == NULL))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Route Refresh not enabled for Peer.\n");
        return SNMP_FAILURE;
    }
    *pu4RetValFsbgp4RtRefreshStatisticsRtRefMsgSentCntr =
        BGP4_RTREF_MSG_SENT_CTR (pPeerEntry, u4Index);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgTxErrCntr
 Input       :  The Indices
                Fsbgp4RtRefreshStatisticsPeerType
                Fsbgp4RtRefreshStatisticsPeerAddr
                Fsbgp4RtRefreshStatisticsAfi
                Fsbgp4RtRefreshStatisticsSafi

                The Object 
                retValFsbgp4RtRefreshStatisticsRtRefMsgTxErrCntr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgTxErrCntr (INT4
                                                     i4Fsbgp4RtRefreshStatisticsPeerType,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pFsbgp4RtRefreshStatisticsPeerAddr,
                                                     INT4
                                                     i4Fsbgp4RtRefreshStatisticsAfi,
                                                     INT4
                                                     i4Fsbgp4RtRefreshStatisticsSafi,
                                                     UINT4
                                                     *pu4RetValFsbgp4RtRefreshStatisticsRtRefMsgTxErrCntr)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tAddrPrefix         PeerAddress;
    INT4                i4Status = 0;
    UINT4               u4Index;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Status = Bgp4LowGetIpAddress (i4Fsbgp4RtRefreshStatisticsPeerType,
                                    pFsbgp4RtRefreshStatisticsPeerAddr,
                                    &PeerAddress);
    if (i4Status == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeerEntry == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    i4Status = Bgp4GetAfiSafiIndex ((UINT2) i4Fsbgp4RtRefreshStatisticsAfi,
                                    (UINT1) i4Fsbgp4RtRefreshStatisticsSafi,
                                    &u4Index);
    if (i4Status == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to get AFI/SAFI Index.\n");
        return SNMP_FAILURE;
    }
    /* Check whether the <AFI, SAFI> is negotiated or not */
    if ((pPeerEntry->apAsafiInstance[u4Index] == NULL) ||
        (BGP4_PEER_RTREF_DATA_PTR (pPeerEntry, u4Index) == NULL))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Route Refresh not enabled for Peer.\n");
        return SNMP_FAILURE;
    }
    *pu4RetValFsbgp4RtRefreshStatisticsRtRefMsgTxErrCntr =
        BGP4_RTREF_MSG_TXERR_CTR (pPeerEntry, u4Index);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgRcvdCntr
 Input       :  The Indices
                Fsbgp4RtRefreshStatisticsPeerType
                Fsbgp4RtRefreshStatisticsPeerAddr
                Fsbgp4RtRefreshStatisticsAfi
                Fsbgp4RtRefreshStatisticsSafi

                The Object 
                retValFsbgp4RtRefreshStatisticsRtRefMsgRcvdCntr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgRcvdCntr (INT4
                                                    i4Fsbgp4RtRefreshStatisticsPeerType,
                                                    tSNMP_OCTET_STRING_TYPE *
                                                    pFsbgp4RtRefreshStatisticsPeerAddr,
                                                    INT4
                                                    i4Fsbgp4RtRefreshStatisticsAfi,
                                                    INT4
                                                    i4Fsbgp4RtRefreshStatisticsSafi,
                                                    UINT4
                                                    *pu4RetValFsbgp4RtRefreshStatisticsRtRefMsgRcvdCntr)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tAddrPrefix         PeerAddress;
    INT4                i4Status = 0;
    UINT4               u4Index;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Status = Bgp4LowGetIpAddress (i4Fsbgp4RtRefreshStatisticsPeerType,
                                    pFsbgp4RtRefreshStatisticsPeerAddr,
                                    &PeerAddress);
    if (i4Status == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeerEntry == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    i4Status = Bgp4GetAfiSafiIndex ((UINT2) i4Fsbgp4RtRefreshStatisticsAfi,
                                    (UINT1) i4Fsbgp4RtRefreshStatisticsSafi,
                                    &u4Index);
    if (i4Status == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to get AFI/SAFI Index.\n");
        return SNMP_FAILURE;
    }
    /* Check whether the <AFI, SAFI> is negotiated or not */
    if ((pPeerEntry->apAsafiInstance[u4Index] == NULL) ||
        (BGP4_PEER_RTREF_DATA_PTR (pPeerEntry, u4Index) == NULL))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Route Refresh not enabled for Peer.\n");
        return SNMP_FAILURE;
    }
    *pu4RetValFsbgp4RtRefreshStatisticsRtRefMsgRcvdCntr =
        BGP4_RTREF_MSG_RCVD_CTR (pPeerEntry, u4Index);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgInvalidCntr
 Input       :  The Indices
                Fsbgp4RtRefreshStatisticsPeerType
                Fsbgp4RtRefreshStatisticsPeerAddr
                Fsbgp4RtRefreshStatisticsAfi
                Fsbgp4RtRefreshStatisticsSafi

                The Object 
                retValFsbgp4RtRefreshStatisticsRtRefMsgInvalidCntr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgInvalidCntr (INT4
                                                       i4Fsbgp4RtRefreshStatisticsPeerType,
                                                       tSNMP_OCTET_STRING_TYPE *
                                                       pFsbgp4RtRefreshStatisticsPeerAddr,
                                                       INT4
                                                       i4Fsbgp4RtRefreshStatisticsAfi,
                                                       INT4
                                                       i4Fsbgp4RtRefreshStatisticsSafi,
                                                       UINT4
                                                       *pu4RetValFsbgp4RtRefreshStatisticsRtRefMsgInvalidCntr)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tAddrPrefix         PeerAddress;
    INT4                i4Status = 0;
    UINT4               u4Index;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Status = Bgp4LowGetIpAddress (i4Fsbgp4RtRefreshStatisticsPeerType,
                                    pFsbgp4RtRefreshStatisticsPeerAddr,
                                    &PeerAddress);
    if (i4Status == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeerEntry == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Peer exist.\n");
        return SNMP_FAILURE;
    }
    i4Status = Bgp4GetAfiSafiIndex ((UINT2) i4Fsbgp4RtRefreshStatisticsAfi,
                                    (UINT1) i4Fsbgp4RtRefreshStatisticsSafi,
                                    &u4Index);
    if (i4Status == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to get AFI/SAFI Index.\n");
        return SNMP_FAILURE;
    }
    /* Check whether the <AFI, SAFI> is negotiated or not */
    if ((pPeerEntry->apAsafiInstance[u4Index] == NULL) ||
        (BGP4_PEER_RTREF_DATA_PTR (pPeerEntry, u4Index) == NULL))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Route Refresh not enabled for Peer.\n");
        return SNMP_FAILURE;
    }
    *pu4RetValFsbgp4RtRefreshStatisticsRtRefMsgInvalidCntr =
        BGP4_RTREF_MSG_RCVD_INVALID_CTR (pPeerEntry, u4Index);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4MpeSoftReconfigOutboundTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4MpeSoftReconfigOutboundTable
 Input       :  The Indices
                Fsbgp4SoftReconfigOutboundPeerType
                Fsbgp4SoftReconfigOutboundPeerAddr
                Fsbgp4SoftReconfigOutboundAfi
                Fsbgp4SoftReconfigOutboundSafi
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4MpeSoftReconfigOutboundTable (INT4
                                                            i4Fsbgp4SoftReconfigOutboundPeerType,
                                                            tSNMP_OCTET_STRING_TYPE
                                                            *
                                                            pFsbgp4SoftReconfigOutboundPeerAddr,
                                                            INT4
                                                            i4Fsbgp4SoftReconfigOutboundAfi,
                                                            INT4
                                                            i4Fsbgp4SoftReconfigOutboundSafi)
{
    tAddrPrefix         PeerAddress;
    INT4                i4Status = 0;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Status = Bgp4LowGetIpAddress (i4Fsbgp4SoftReconfigOutboundPeerType,
                                    pFsbgp4SoftReconfigOutboundPeerAddr,
                                    &PeerAddress);
    if (i4Status == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Status = Bgp4IsValidAddress (PeerAddress, BGP4_FALSE);
    if (i4Status == BGP4_FALSE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Invalid Peer Address.\n");
        return SNMP_FAILURE;
    }
    i4Status =
        Bgp4MpeValidateAddressType ((UINT4) i4Fsbgp4SoftReconfigOutboundAfi);
    if (i4Status == BGP4_FALSE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Invalid Peer Address Type.\n");
        return SNMP_FAILURE;
    }
    i4Status =
        Bgp4MpeValidateSubAddressType ((UINT4)
                                       i4Fsbgp4SoftReconfigOutboundSafi);
    if (i4Status == BGP4_FALSE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Invalid Peer Sub-Address Type.\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4MpeSoftReconfigOutboundTable
 Input       :  The Indices
                Fsbgp4SoftReconfigOutboundPeerType
                Fsbgp4SoftReconfigOutboundPeerAddr
                Fsbgp4SoftReconfigOutboundAfi
                Fsbgp4SoftReconfigOutboundSafi
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4MpeSoftReconfigOutboundTable (INT4
                                                    *pi4Fsbgp4SoftReconfigOutboundPeerType,
                                                    tSNMP_OCTET_STRING_TYPE *
                                                    pFsbgp4SoftReconfigOutboundPeerAddr,
                                                    INT4
                                                    *pi4Fsbgp4SoftReconfigOutboundAfi,
                                                    INT4
                                                    *pi4Fsbgp4SoftReconfigOutboundSafi)
{
    UNUSED_PARAM (pi4Fsbgp4SoftReconfigOutboundSafi);
    UNUSED_PARAM (pi4Fsbgp4SoftReconfigOutboundPeerType);
    UNUSED_PARAM (pi4Fsbgp4SoftReconfigOutboundAfi);
    UNUSED_PARAM (pFsbgp4SoftReconfigOutboundPeerAddr);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4MpeSoftReconfigOutboundTable
 Input       :  The Indices
                Fsbgp4SoftReconfigOutboundPeerType
                nextFsbgp4SoftReconfigOutboundPeerType
                Fsbgp4SoftReconfigOutboundPeerAddr
                nextFsbgp4SoftReconfigOutboundPeerAddr
                Fsbgp4SoftReconfigOutboundAfi
                nextFsbgp4SoftReconfigOutboundAfi
                Fsbgp4SoftReconfigOutboundSafi
                nextFsbgp4SoftReconfigOutboundSafi
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4MpeSoftReconfigOutboundTable (INT4
                                                   i4Fsbgp4SoftReconfigOutboundPeerType,
                                                   INT4
                                                   *pi4NextFsbgp4SoftReconfigOutboundPeerType,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pFsbgp4SoftReconfigOutboundPeerAddr,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pNextFsbgp4SoftReconfigOutboundPeerAddr,
                                                   INT4
                                                   i4Fsbgp4SoftReconfigOutboundAfi,
                                                   INT4
                                                   *pi4NextFsbgp4SoftReconfigOutboundAfi,
                                                   INT4
                                                   i4Fsbgp4SoftReconfigOutboundSafi,
                                                   INT4
                                                   *pi4NextFsbgp4SoftReconfigOutboundSafi)
{
    UNUSED_PARAM (i4Fsbgp4SoftReconfigOutboundSafi);
    UNUSED_PARAM (i4Fsbgp4SoftReconfigOutboundAfi);
    UNUSED_PARAM (i4Fsbgp4SoftReconfigOutboundPeerType);
    UNUSED_PARAM (pi4NextFsbgp4SoftReconfigOutboundSafi);
    UNUSED_PARAM (pi4NextFsbgp4SoftReconfigOutboundAfi);
    UNUSED_PARAM (pi4NextFsbgp4SoftReconfigOutboundPeerType);
    UNUSED_PARAM (pFsbgp4SoftReconfigOutboundPeerAddr);
    UNUSED_PARAM (pNextFsbgp4SoftReconfigOutboundPeerAddr);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeSoftReconfigOutboundRequest
 Input       :  The Indices
                Fsbgp4SoftReconfigOutboundPeerType
                Fsbgp4SoftReconfigOutboundPeerAddr
                Fsbgp4SoftReconfigOutboundAfi
                Fsbgp4SoftReconfigOutboundSafi

                The Object 
                retValFsbgp4SoftReconfigOutboundRequest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeSoftReconfigOutboundRequest (INT4
                                            i4Fsbgp4SoftReconfigOutboundPeerType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsbgp4SoftReconfigOutboundPeerAddr,
                                            INT4
                                            i4Fsbgp4SoftReconfigOutboundAfi,
                                            INT4
                                            i4Fsbgp4SoftReconfigOutboundSafi,
                                            INT4
                                            *pi4RetValFsbgp4SoftReconfigOutboundRequest)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tAddrPrefix         PeerAddress;
    tAddrPrefix         InvPrefix;
    INT4                i4Status = 0;
    UINT4               u4Index;
    INT4                i4ReqForAll = BGP4_FALSE;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Status = Bgp4LowGetIpAddress (i4Fsbgp4SoftReconfigOutboundPeerType,
                                    pFsbgp4SoftReconfigOutboundPeerAddr,
                                    &PeerAddress);
    if (i4Status == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Status = Bgp4GetAfiSafiIndex ((UINT2) i4Fsbgp4SoftReconfigOutboundAfi,
                                    (UINT1) i4Fsbgp4SoftReconfigOutboundSafi,
                                    &u4Index);
    if (i4Status == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to get AFI/SAFI Index.\n");
        return SNMP_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&(InvPrefix),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress));
    i4ReqForAll = PrefixMatch (InvPrefix, PeerAddress);
    if (i4ReqForAll != BGP4_TRUE)
    {
        pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
        if (pPeerEntry == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - No matching Peer exist.\n");
            return SNMP_FAILURE;
        }
        /* Check whether the <AFI, SAFI> is negotiated or not */
        if (pPeerEntry->apAsafiInstance[u4Index] == NULL)
        {
            return SNMP_FAILURE;
        }
        *pi4RetValFsbgp4SoftReconfigOutboundRequest =
            BGP4_SOFTCFG_OUT_REQ_PEER (pPeerEntry, u4Index);
    }
    else
    {
        *pi4RetValFsbgp4SoftReconfigOutboundRequest = TRUE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4mpeSoftReconfigOutboundRequest
 Input       :  The Indices
                Fsbgp4SoftReconfigOutboundPeerType
                Fsbgp4SoftReconfigOutboundPeerAddr
                Fsbgp4SoftReconfigOutboundAfi
                Fsbgp4SoftReconfigOutboundSafi

                The Object 
                setValFsbgp4SoftReconfigOutboundRequest
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4mpeSoftReconfigOutboundRequest (INT4
                                            i4Fsbgp4SoftReconfigOutboundPeerType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsbgp4SoftReconfigOutboundPeerAddr,
                                            INT4
                                            i4Fsbgp4SoftReconfigOutboundAfi,
                                            INT4
                                            i4Fsbgp4SoftReconfigOutboundSafi,
                                            INT4
                                            i4SetValFsbgp4SoftReconfigOutboundRequest)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tBgp4QMsg          *pQMsg = NULL;
    tAddrPrefix         PeerAddress;
    tAddrPrefix         InvPrefix;
    INT4                i4Status = 0;
    UINT4               u4AfiSafiMask;
    UINT4               u4Index;
    INT4                i4ReqForAll = BGP4_FALSE;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Status = Bgp4LowGetIpAddress (i4Fsbgp4SoftReconfigOutboundPeerType,
                                    pFsbgp4SoftReconfigOutboundPeerAddr,
                                    &PeerAddress);
    if (i4Status == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (!((i4Fsbgp4SoftReconfigOutboundAfi == 0) &&
          (i4Fsbgp4SoftReconfigOutboundSafi == 0)))
    {
        i4Status = Bgp4GetAfiSafiIndex ((UINT2) i4Fsbgp4SoftReconfigOutboundAfi,
                                        (UINT1)
                                        i4Fsbgp4SoftReconfigOutboundSafi,
                                        &u4Index);
        if (i4Status == BGP4_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Unable to get AFI/SAFI Index.\n");
            return SNMP_FAILURE;
        }
    }

    Bgp4InitAddrPrefixStruct (&(InvPrefix),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress));
    i4ReqForAll = PrefixMatch (InvPrefix, PeerAddress);
    if (i4ReqForAll != BGP4_TRUE)
    {
        pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
        if (pPeerEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        /* Check whether the <AFI, SAFI> is negotiated or not */
        if (!((i4Fsbgp4SoftReconfigOutboundAfi == 0) &&
              (i4Fsbgp4SoftReconfigOutboundSafi == 0)))
        {
            if (pPeerEntry->apAsafiInstance[u4Index] == NULL)
            {
                return SNMP_FAILURE;
            }
        }
    }

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting BGP Outbound "
                  "Refresh Msg to BGP Queue FAILED!!!\n");
        return SNMP_FAILURE;
    }
    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_SNMP_OUTBOUND_SOFTCONFIG_EVENT;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
    Bgp4CopyAddrPrefixStruct (&(BGP4_INPUTQ_RTREF_PEER_ADDR_INFO (pQMsg)),
                              PeerAddress);
    BGP4_GET_AFISAFI_MASK ((UINT2) i4Fsbgp4SoftReconfigOutboundAfi,
                           (UINT2) i4Fsbgp4SoftReconfigOutboundSafi,
                           u4AfiSafiMask);
    BGP4_INPUTQ_RTREF_ASAFI_MASK (pQMsg) = u4AfiSafiMask;

    BGP4_INPUTQ_CXT (pQMsg) = u4Context;
    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (i4ReqForAll != BGP4_TRUE)
    {
        if (!((i4Fsbgp4SoftReconfigOutboundAfi == 0) &&
              (i4Fsbgp4SoftReconfigOutboundSafi == 0)))
        {
            BGP4_SOFTCFG_OUT_REQ_PEER (pPeerEntry, u4Index) = (UINT1)
                (i4SetValFsbgp4SoftReconfigOutboundRequest);
        }
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIBgp4mpeSoftReconfigOutboundRequest, u4SeqNum,
                          FALSE, BgpLock, BgpUnLock, 5, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %i %i %i",
                      gpBgpCurrCxtNode->u4ContextId,
                      i4Fsbgp4SoftReconfigOutboundPeerType,
                      pFsbgp4SoftReconfigOutboundPeerAddr,
                      i4Fsbgp4SoftReconfigOutboundAfi,
                      i4Fsbgp4SoftReconfigOutboundSafi,
                      i4SetValFsbgp4SoftReconfigOutboundRequest));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4mpeSoftReconfigOutboundRequest
 Input       :  The Indices
                Fsbgp4SoftReconfigOutboundPeerType
                Fsbgp4SoftReconfigOutboundPeerAddr
                Fsbgp4SoftReconfigOutboundAfi
                Fsbgp4SoftReconfigOutboundSafi

                The Object 
                testValFsbgp4SoftReconfigOutboundRequest
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4mpeSoftReconfigOutboundRequest (UINT4 *pu4ErrorCode,
                                               INT4
                                               i4Fsbgp4SoftReconfigOutboundPeerType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsbgp4SoftReconfigOutboundPeerAddr,
                                               INT4
                                               i4Fsbgp4SoftReconfigOutboundAfi,
                                               INT4
                                               i4Fsbgp4SoftReconfigOutboundSafi,
                                               INT4
                                               i4TestValFsbgp4SoftReconfigOutboundRequest)
{
    tAddrPrefix         PeerAddress;
    tAddrPrefix         InvPrefix;
    INT4                i4Status = 0;
    tBgp4PeerEntry     *pPeerEntry = NULL;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    i4Status = Bgp4LowGetIpAddress (i4Fsbgp4SoftReconfigOutboundPeerType,
                                    pFsbgp4SoftReconfigOutboundPeerAddr,
                                    &PeerAddress);
    if (i4Status == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Status = Bgp4IsValidAddress (PeerAddress, BGP4_FALSE);
    Bgp4InitAddrPrefixStruct (&(InvPrefix),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress));
    if ((i4Status == BGP4_FALSE) &&
        ((PrefixMatch (InvPrefix, PeerAddress) != BGP4_TRUE)))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!((i4Fsbgp4SoftReconfigOutboundAfi == 0) &&
          (i4Fsbgp4SoftReconfigOutboundSafi == 0)))
    {
        i4Status =
            Bgp4MpeValidateAddressType ((UINT4)
                                        i4Fsbgp4SoftReconfigOutboundAfi);
        if (i4Status == BGP4_FALSE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - BGP Invalid Address Type.\n");
            return SNMP_FAILURE;
        }
        i4Status =
            Bgp4MpeValidateSubAddressType ((UINT4)
                                           i4Fsbgp4SoftReconfigOutboundSafi);
        if (i4Status == BGP4_FALSE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - BGP Invalid Sub-Address Type.\n");
            return SNMP_FAILURE;
        }
    }

    if (i4TestValFsbgp4SoftReconfigOutboundRequest != BGP4_RTREF_REQ_SET)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Soft Reconfig Outbound Request.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_SFT_OUT_REQ_ERR);
        return SNMP_FAILURE;
    }
    if (PrefixMatch (InvPrefix, PeerAddress) != BGP4_TRUE)
    {
        pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
        if (pPeerEntry == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_BGP4_PEER_NOT_ENABLED);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4MpeSoftReconfigOutboundTable
 Input       :  The Indices
                Fsbgp4SoftReconfigOutboundPeerType
                Fsbgp4SoftReconfigOutboundPeerAddr
                Fsbgp4SoftReconfigOutboundAfi
                Fsbgp4SoftReconfigOutboundSafi
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4MpeSoftReconfigOutboundTable (UINT4 *pu4ErrorCode,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexBgpRtRefPeerTable
 Input       :  The Indices
                BgpPeerRemoteAddr
                nextBgpPeerRemoteAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexBgpRtRefPeerTable (tAddrPrefix BgpPeerRemoteAddr,
                                  tAddrPrefix * pNextBgpPeerRemoteAddr)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    UINT4               u4Index;
    UINT1               u1Found = BGP4_FALSE;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, BgpPeerRemoteAddr);
    if (pPeerEntry == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching peer exists.\n");
        return SNMP_FAILURE;
    }

    do
    {
        pPeerEntry =
            (tBgp4PeerEntry *) TMO_SLL_Next (BGP4_PEERENTRY_HEAD (u4Context),
                                             &pPeerEntry->TsnNextpeer);
        if ((pPeerEntry != NULL) &&
            ((PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry),
                           BgpPeerRemoteAddr)) != BGP4_TRUE))
        {
            for (u4Index = 0; u4Index < BGP4_MPE_ADDR_FAMILY_MAX_INDEX;
                 u4Index++)
            {
                if ((pPeerEntry->apAsafiInstance[u4Index] != NULL) &&
                    (BGP4_PEER_RTREF_DATA_PTR (pPeerEntry, u4Index) != NULL))
                {
                    u1Found = BGP4_TRUE;
                    break;
                }
            }
        }
        if (u1Found == BGP4_TRUE)
        {
            break;
        }
    }
    while (pPeerEntry != NULL);

    if (pPeerEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    Bgp4CopyAddrPrefixStruct (pNextBgpPeerRemoteAddr,
                              BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));
    return SNMP_SUCCESS;
}
