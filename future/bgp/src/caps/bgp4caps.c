/********************************************************************
 * Copyright (C) Aricent Inc, All Rights Reserved
 *
 * $Id: bgp4caps.c,v 1.25 2017/09/15 06:19:52 siva Exp $
 *
 * Description: This file contains the Capability Handling Routines
 *
 *******************************************************************/

#define _BGCAPSFIL_C
#include "bgp4com.h"

/* This Array contains the Mandatory Capability parameter Values */
static UINT1        au1MandatoryCaps[MAX_CAP_OPTPARAM_LEN] =
    { CAP_CODE_MP_EXTN };
static UINT2        au2MandatoryCapAFIs[CAP_MAX_OPT_CAP_AFI_VALUE] =
    { CAP_CODE_MP_EXTN_AFI_IPV4, CAP_CODE_MP_EXTN_AFI_IPV6,
CAP_CODE_MP_EXTN_AFI_L2VPN };
static UINT2       
    au2MandatoryCapSAFIs[CAP_MAX_OPT_CAP_AFI_VALUE][CAP_MAX_OPT_CAP_SAFI_VALUE]
    =
    { {CAP_CODE_MP_EXTN_SAFI_UNICAST, CAP_CODE_MP_EXTN_SAFI_VPNV4_UNICAST,
       CAP_CODE_MP_EXTN_SAFI_LABEL},
{CAP_CODE_MP_EXTN_SAFI_UNICAST, 0, 0},
{CAP_CODE_MP_EXTN_SAFI_VPLS, CAP_CODE_MP_EXTN_SAFI_EVPN, 0}
};

/***************************************************************************
    Function Name     :   CapsInitHandler                               
    Description       :   This function allocates memory required for 
                          Capabilities Advertisement and intializes the error 
                          counts.     
    Inputs            :   None                                          
    Outputs           :   None                                        
    Global Variables 
    Referred          :      CAPS_VALIDATION_ERR(BGP4_DFLT_VRFID) , CAPS_PROCESS_ERR(BGP4_DFLT_VRFID) , 
                             CAPS_NOTFMSG_SEND_ERR(BGP4_DFLT_VRFID) ,PEER_CAPS_INFO_MEMPOOLID , 
                             PEER_CAPS_RCVD_MEMPOOLID , 
                             SPKR_CAPS_INFO_MEMPOOLID,
                             SPKR_SUP_CAPS_MEMPOOLID   
    Global Variables 
    Modified          :   CAPS_VALIDATION_ERR(BGP4_DFLT_VRFID) , CAPS_PROCESS_ERR(BGP4_DFLT_VRFID) , 
                          CAPS_NOTFMSG_SEND_ERR(BGP4_DFLT_VRFID) ,PEER_CAPS_INFO_MEMPOOLID , 
                          PEER_CAPS_RCVD_MEMPOOLID , 
                          SPKR_CAPS_INFO_MEMPOOLID , 
                          SPKR_SUP_CAPS_MEMPOOLID    
    Exceptions or OS 
    Err Handling      :   None
    Use of Recursion  :   None
    Returns           :   CAPS_SUCCESS 
                          CAPS_FAILURE                  
***************************************************************************/
INT4
CapsInitHandler (UINT4 u4CxtId)
{
    CAPS_ADVT_SUP (u4CxtId) = CAPS_TRUE;
    CAPS_VALIDATION_ERR (u4CxtId) = 0;
    CAPS_PROCESS_ERR (u4CxtId) = 0;
    CAPS_NOTFMSG_SEND_ERR (u4CxtId) = 0;
    return CAPS_SUCCESS;
}

/***************************************************************************
   Function Name     :  CapsFormHandler                                 
   Description       :  On receiving Peer Information and OPEN message ,this 
                        module checks whether the Capabilities have to be 
                        filled in this OPEN message or not by checking the 
                        corresponding UnsupOptAutoRepeering and UnsupCapsRepeer
                        ing flags of this peer from PEER_CAPS_HASH_TABLE. If 
                        these flags are CAPS_FALSE , this module 
                        invokes CapsUpdtCapsInOpnMsg to fill the Capabilities 
                        that are supported by the router for this peer.
                 
   Input(s)          :  pu1OpnMsgToFillCaps -Pointer to the OPEN message formed 
                                             by router which is to be 
                                             advertised to its peer.  
                        pPeerEntry   - Pointer to the peer information.
   Output(s)         :  pOpnMsgToFillCaps - Pointer to the OPEN message  
                                            without Capabilities , 
                                            if Auto Re-peering is needed 
                                            without Capabilities Optional 
                                            Parameter.  Otherwise pointer 
                                            to the OPEN message with 
                                            Capabilities.
   Global Variables 
   Referred          :    PEER_CAPS_HASH_TABLE , SPKR_CAPS_HASH_TABLE
   Global Variables
   Modified          :  PEER_CAPS_HASH_TABLE 
   Exceptions or OS 
   Err Handling      :  None
   Use of Recursion  :  None
   Returns           :  CAPS_SUCCESS  - On Success
                        CAPS_FAILURE  - On Failure
                 
***************************************************************************/
INT4
CapsFormHandler (UINT1 *pu1OpnMsgToFillCaps, tBgp4PeerEntry * pPeerEntry)
{
    tSupCapsInfo       *pSupCap = NULL;
    INT4                i4RetSts;
    UINT1               u1FillCaps = CAPS_TRUE;

    if (TMO_SLL_Count (BGP4_PEER_SUP_CAPS_LIST (pPeerEntry)) == 0)
    {
        /* Capabilities are not configured for this peer */
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Capabilities Not Configured.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerEntry))));
        return CAPS_SUCCESS;
    }

    /* Check whether the Notification message with Error Sub-Code 
     * Unsupported Optional Parameter has been already received from 
     * this peer */
    if ((BGP4_GET_CAPS_PEER_FLAGS (pPeerEntry) &
         BGP4_CAPS_UNSUP_OPT_REPEERING) == BGP4_CAPS_UNSUP_OPT_REPEERING)
    {
        u1FillCaps = CAPS_FALSE;

        BGP4_RESET_CAPS_PEER_FLAGS (pPeerEntry, BGP4_CAPS_UNSUP_OPT_REPEERING);
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s : AutoRepeering without Capabilities\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerEntry))));
    }

    if (u1FillCaps == CAPS_TRUE)
    {
        /* Fill the Capabilities in the OPEN message */
        i4RetSts =
            CapsUpdtCapsInOpnMsg (BGP4_PEER_CXT_ID (pPeerEntry),
                                  pu1OpnMsgToFillCaps,
                                  BGP4_PEER_SUP_CAPS_LIST (pPeerEntry),
                                  pPeerEntry);
        if (i4RetSts == CAPS_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Filling capabilities in OPEN message failed.\n");
        }

    }
    else
    {
        TMO_SLL_Scan (BGP4_PEER_SUP_CAPS_LIST (pPeerEntry),
                      pSupCap, tSupCapsInfo *)
        {
            BGP4_PEER_SUP_CAPS_ANCD_STATUS (pSupCap) = CAPS_FALSE;
        }
    }
    return CAPS_SUCCESS;
}

/******************************************************************************
    Function Name      :  CapsUpdtCapsInOpnMsg
    Description       :   This module fills the OPEN message with 
                          Capabilities that are supported by the router 
                          for this peer.
    Input(s)          :   pu1OpnMsgToFillCaps - Pointer to the OPEN Message 
                                        that is formed by the router.
                          pSpkrSupCapsLst   - Pointer to the List 
                                        of Capabilities that are 
                                        supported by the router for this peer
    Output(s)         :   pu1OpnMsgToFillCaps  - Pointer to the OPEN message
                                        filled with Capabilities that are 
                                        supported by the router for this peer.
    Global Variables 
    Referred          :   SPKR_CAPS_HASH_TABLE 
    Global Variables 
    Modified          :   None
    Exceptions or OS 
    Err Handling      :   None
    Use of Recursion  :   None
    Returns           :   CAPS_SUCCESS     - On Success
                          CAPS_FAILURE     - On Failure
*******************************************************************************/
INT4
CapsUpdtCapsInOpnMsg (UINT4 u4CxtId, UINT1 *pu1OpnMsgToFillCaps,
                      tTMO_SLL * pSpkrSupCapsLst, tBgp4PeerEntry * pPeerEntry)
{
    tSupCapsInfo       *pSpkrSupCaps = NULL;
    UINT1               u1OptParamLen;
    UINT1               u1CapsTotalLen = 0;
    UINT1               u1CapLength = 0;
    UINT1               u1CapsOffset;

    /*Get the Optional Parameter length from OPEN message. This is 0 
       if no other Optional Parameter is present in this message */
    u1OptParamLen = (*(pu1OpnMsgToFillCaps + OPTPARAM_LENGTH_OFFSET));
    u1CapsOffset = (UINT1) (OPTIONAL_PARAMETER_OFFSET + u1OptParamLen +
                            OPTPARAM_TYPE_LENGTH_SIZE);

    /*Copy all the Capabilities that are supported for this peer , in the 
       OPEN message */
    TMO_SLL_Scan (pSpkrSupCapsLst, pSpkrSupCaps, tSupCapsInfo *)
    {
        BGP4_PEER_SUP_CAPS_ANCD_STATUS (pSpkrSupCaps) = BGP4_FALSE;
        if (pSpkrSupCaps->u1RowStatus == ACTIVE)
        {
            u1CapLength = pSpkrSupCaps->SupCapability.u1CapLength;
            if ((pSpkrSupCaps->SupCapability.u1CapCode ==
                 CAP_CODE_GRACEFUL_RESTART)
                && (BGP4_IPV6_AFI_FLAG (u4CxtId) != BGP4_CLI_AFI_ENABLED)
                && (u1CapLength != MIN_CAP_GR_LENGTH))
            {
                u1CapLength = (u1CapLength - 4);
            }

            if ((u1OptParamLen + u1CapsTotalLen + CAP_CODE_LENGTH_SIZE +
                 u1CapLength) <= MAX_CAP_OPTPARAM_LEN)
            {
                /*Copy Capability Code */
                *(pu1OpnMsgToFillCaps + u1CapsOffset + u1CapsTotalLen) =
                    pSpkrSupCaps->SupCapability.u1CapCode;

                /*Copy Capability Length */
                *(pu1OpnMsgToFillCaps + u1CapsOffset + u1CapsTotalLen +
                  CAP_CODE_SIZE) = u1CapLength;
                BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                               BGP4_TRC_FLAG, BGP4_RX_TRC | BGP4_PEER_CON_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : Forming Open Msg With %s CAPABILITY "
                               " and length %d\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerEntry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerEntry))),
                               Bgp4PrintCodeName (pSpkrSupCaps->SupCapability.
                                                  u1CapCode,
                                                  BGP4_SUP_CAP_CODE_NAME),
                               u1CapLength);

                if (pSpkrSupCaps->SupCapability.u1CapCode ==
                    CAP_CODE_GRACEFUL_RESTART)

                {
                    Bgp4GRFillRestartCap (u4CxtId,
                                          (pu1OpnMsgToFillCaps + u1CapsOffset +
                                           u1CapsTotalLen +
                                           CAP_CODE_LENGTH_SIZE), pPeerEntry);
                }
                else
                {
                    /*Copy the Capability value */
                    if (u1CapLength <= MAX_CAP_LENGTH)
                    {
                        MEMCPY ((pu1OpnMsgToFillCaps + u1CapsOffset +
                                 u1CapsTotalLen + CAP_CODE_LENGTH_SIZE),
                                (pSpkrSupCaps->SupCapability.au1CapValue),
                                (u1CapLength));
                    }
                    else
                    {
                        return CAPS_FAILURE;
                    }
                }
                /*Calculate length of Capabilities Optional Parameter */
                u1CapsTotalLen =
                    (UINT1) (u1CapsTotalLen + CAP_CODE_LENGTH_SIZE +
                             (u1CapLength));
                BGP4_PEER_SUP_CAPS_ANCD_STATUS (pSpkrSupCaps) = BGP4_TRUE;
            }
        }
    }

    if (u1CapsTotalLen > 0)
    {
        /* Copy Optional Parameter Type */
        *(pu1OpnMsgToFillCaps + OPTIONAL_PARAMETER_OFFSET +
          u1OptParamLen) = CAPABILITIES_OPTIONAL_PARAMETER;

        /* Copy Capabilities Optional parameter Length in OPEN message */
        *(pu1OpnMsgToFillCaps + OPTIONAL_PARAMETER_OFFSET +
          u1OptParamLen + OPTPARAM_TYPE_SIZE) = u1CapsTotalLen;

        /*Calculate the total length of optional paramter */
        u1OptParamLen = (UINT1) (u1OptParamLen + u1CapsTotalLen +
                                 OPTPARAM_TYPE_LENGTH_SIZE);

        /* Update Optional Parameter Length in OPEN message */
        *(pu1OpnMsgToFillCaps + OPTPARAM_LENGTH_OFFSET) = u1OptParamLen;
    }

    return CAPS_SUCCESS;
}

/******************************************************************************
     Function Name    : CapsRcvdHandler
     Description      : This module receives the supported and received 
                        Capabilities and checks whether at least one common 
                        Capability is there or not. If not , this sends the 
                        NOTIFICATION message with Error Sub-Code Unsupported 
                        Capability and Error Data with all unsupported 
                        Capabilities. Then if NO_CAPABILITY_MATCH_REPEERING 
                        is needed , this sets the UnsupCapsRepeering flag to 
                        establisH peering without Capabilities Optional 
                        Parameter.
     Input(s)         : pu1RcvdCaps  -  Pointer to the Capabilities that are
                                        received from peer.
                        u2RcvdCapsLen  -  Total length of Capabilities 
                                          received.
                        pSpkrSupCapsLst  -Pointer to the  SLL that contains 
                                          the Capabilities that are 
                                          supported by the router for 
                                          this peer.
                        pPeerCaps  - Pointer to the Peer Capabilities in 
                                     which the  Capabilities information of 
                                     the peer has to be stored.
                        pPeerEntry  -   Pointer to the peer information.

     Output(s)          : SPKR_CAPS_HASH_TABLE  , PEER_CAPS_HASH_TABLE
     Global Variables 
     Referred           : PEER_CAPS_HASH_TABLE
     Global Variables 
     Modified           : None
     Exceptions or OS 
     Err Handling       : None
     Use of Recursion   : None

     Return(s)          : CAPS_SUCCESS - On Success of Capabilities processing
                          CAPS_FAILURE - On Failure of Capabilities processing
******************************************************************************/
INT4
CapsRcvdHandler (UINT1 *pu1OpnMsgRcvd, tBgp4PeerEntry * pPeerEntry,
                 UINT2 *pu2RcvdCapsLen)
{
    tSupCapsInfo       *pSpkrSupCap = NULL;
    tSupCapsInfo       *pPeerCapsRcvd = NULL;
    tSupCapsInfo       *pPeerAltCapsRcvd = NULL;
    UINT1              *pu1CapsBuf = NULL;
    UINT1              *pu1MsgBuf = NULL;
    INT4                i4RetSts;
    INT4                i4Index;
    INT4                i4AfiIndex = 0;
    INT4                i4SafiIndex = 0;
    UINT2               u2AfiValue = 0;
    UINT1               u1SafiValue = 0;
    UINT4               u4AsafiMask;
    UINT4               u4AsNo;
    UINT2               u2CapErrDataLen = 0;
    UINT1               au1CapErrData[MAX_CAP_OPTPARAM_LEN];
    UINT1               u1CommnCapFound = CAPS_FALSE;
    UINT1               u1RcvdComnCaps = 0;
    UINT1               u1SupCapsCount = 0;
    UINT1               u1CapCode;
    UINT1               u1CapLength;
    UINT1               u1OptParamLen;
    UINT1               u1OptParamType;
    UINT1               u1CapsTotLen = 0;
    UINT1               u1MandatoryCapsFound = CAPS_FALSE;
    UINT1               u1MandCapsFound = CAPS_FALSE;
    INT4                i4RetValue = 0;
    UINT1               u1RcvdOrfMode = 0;
    UINT1               u1SupOrfMode = 0;
    UINT1               u1NegOrfMode = 0;

    /* For all Capabilities that are supported by the router for this peer */
    *pu2RcvdCapsLen = 0;

    /* Get the Optional Parameter Length from OPEN message */

    u1OptParamLen = (*(pu1OpnMsgRcvd + OPTPARAM_LENGTH_OFFSET));
    /* Reach the position of first Optional Parameter in the OPEN message */
    pu1CapsBuf = pu1OpnMsgRcvd + OPTIONAL_PARAMETER_OFFSET;

    while (pu1CapsBuf < (pu1OpnMsgRcvd + OPTIONAL_PARAMETER_OFFSET +
                         u1OptParamLen))
    {
        /* Check for the Capabilities Optional Parameter type */
        u1OptParamType = (*pu1CapsBuf);
        if (u1OptParamType != CAPABILITIES_OPTIONAL_PARAMETER)
        {
            pu1CapsBuf = pu1CapsBuf + (*(pu1CapsBuf + OPTPARAM_TYPE_SIZE)) +
                OPTPARAM_TYPE_LENGTH_SIZE;
            continue;
        }
        /* Capabilities Optional parameter Length should be greater 
           than 2 bytes */

        u1CapsTotLen = (*(pu1CapsBuf + OPTPARAM_TYPE_SIZE));
        if (u1CapsTotLen < MIN_CAP_OPT_LEN)
        {
            break;
        }
        pu1MsgBuf = pu1CapsBuf + OPTPARAM_TYPE_LENGTH_SIZE;
        /* For all Capabilities present in the OPEN message */
        while (pu1MsgBuf < (pu1CapsBuf + u1CapsTotLen +
                            OPTPARAM_TYPE_LENGTH_SIZE))
        {
            u1CommnCapFound = BGP4_FALSE;
            u1CapCode = (*pu1MsgBuf);
            u1CapLength = (*(pu1MsgBuf + CAP_CODE_SIZE));
            *pu2RcvdCapsLen += (UINT2) (CAP_CODE_LENGTH_SIZE + u1CapLength);

            BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                           BGP4_TRC_FLAG, BGP4_RX_TRC | BGP4_PEER_CON_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Received Open Msg With %s CAPABILITY "
                           " and length %d\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerEntry))),
                           Bgp4PrintCodeName (u1CapCode,
                                              BGP4_SUP_CAP_CODE_NAME),
                           u1CapLength);

            u1SupCapsCount = 0;
            TMO_SLL_Scan (BGP4_PEER_SUP_CAPS_LIST (pPeerEntry),
                          pSpkrSupCap, tSupCapsInfo *)
            {
                if (pSpkrSupCap->u1RowStatus != ACTIVE)
                {
                    continue;
                }
                u1SupCapsCount++;
                if ((pSpkrSupCap->SupCapability.u1CapCode) != u1CapCode)
                {
                    continue;
                }
                /* Compare the supported Capability length with received 
                   Capability length */
                if (((pSpkrSupCap->SupCapability.u1CapLength) != u1CapLength) &&
                    ((u1CapCode != CAP_CODE_GRACEFUL_RESTART)))
                {
                    continue;
                }
                if (u1CapCode == CAP_CODE_GRACEFUL_RESTART)
                {
                    u1CommnCapFound = CAPS_TRUE;
                    u1RcvdComnCaps++;
                    break;
                }

                if (u1CapCode == CAP_CODE_4_OCTET_ASNO)
                {
                    PTR_FETCH4 (u4AsNo, pu1MsgBuf + CAP_CODE_LENGTH_SIZE);
                    if (BGP4_PEER_ASNO (pPeerEntry) != u4AsNo)
                    {
                        BGP4_TRC_ARG2 (&
                                       (BGP4_PEER_REMOTE_ADDR_INFO
                                        (pPeerEntry)), BGP4_TRC_FLAG,
                                       BGP4_ALL_FAILURE_TRC |
                                       BGP4_CONTROL_PATH_TRC |
                                       BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                                       "\tPEER %s - OPEN MSG ERR - Invalid AS No %u\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeerEntry),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeerEntry))),
                                       u4AsNo);
                        Bgp4EhSendError (pPeerEntry, BGP4_OPEN_MSG_ERR,
                                         BGP4_AS_UNACCEPTABLE, NULL, 0);
                        return (CAPS_FAILURE);
                    }
                    u1CommnCapFound = CAPS_TRUE;
                    u1RcvdComnCaps++;
                    break;
                }

                /* Compare Capability value */
                if (u1CapLength <= MAX_CAP_LENGTH)
                {
                    if (u1CapCode == CAP_CODE_ORF)
                    {
                        /* For ORF capability it is not necessary that the whole 
                         * received Capability value needs to be match with the Capability
                         * value supported. But AFI,SAFI and ORF Type should match */

                        if (MEMCMP ((pu1MsgBuf + CAP_CODE_LENGTH_SIZE),
                                    (pSpkrSupCap->SupCapability.au1CapValue),
                                    BGP_CAP_ORF_MODE_OFFSET) != CAPS_MEM_EQUAL)
                        {
                            continue;
                        }

                        /* If ORF send capability is received and receive capability is
                         * advertised, then ORF receive capability is succesfully negotiated 
                         *and vice versa */
                        u1RcvdOrfMode =
                            *(pu1MsgBuf + CAP_CODE_LENGTH_SIZE +
                              BGP_CAP_ORF_MODE_OFFSET);
                        u1SupOrfMode =
                            pSpkrSupCap->SupCapability.
                            au1CapValue[BGP_CAP_ORF_MODE_OFFSET];

                        if ((u1RcvdOrfMode & BGP4_CLI_ORF_MODE_SEND) &&
                            (u1SupOrfMode & BGP4_CLI_ORF_MODE_RECEIVE))
                        {
                            u1CommnCapFound = CAPS_TRUE;
                            u1NegOrfMode |= BGP4_CLI_ORF_MODE_RECEIVE;
                            u1RcvdComnCaps++;
                        }
                        if ((u1RcvdOrfMode & BGP4_CLI_ORF_MODE_RECEIVE) &&
                            (u1SupOrfMode & BGP4_CLI_ORF_MODE_SEND))
                        {
                            u1CommnCapFound = CAPS_TRUE;
                            u1NegOrfMode |= BGP4_CLI_ORF_MODE_SEND;
                            u1RcvdComnCaps++;
                        }
                    }
                    else if (MEMCMP ((pu1MsgBuf + CAP_CODE_LENGTH_SIZE),
                                     (pSpkrSupCap->SupCapability.au1CapValue),
                                     u1CapLength) == CAPS_MEM_EQUAL)
                    {
                        u1CommnCapFound = CAPS_TRUE;
                        u1RcvdComnCaps++;
                    }
                    if (u1CommnCapFound == CAPS_TRUE)
                    {
                        break;
                    }
                }
            }
            PEER_CAPS_RCVD_ENTRY_CREATE (pPeerCapsRcvd);
            if (pPeerCapsRcvd == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                          "\tAllocate Memory for Received Capabilities "
                          "FAILED !!!\n");
                gu4BgpDebugCnt[MAX_BGP_SUPP_CAP_INFOS_SIZING_ID]++;
                return CAPS_FAILURE;
            }
            pPeerCapsRcvd->SupCapability.u1CapCode = u1CapCode;
            pPeerCapsRcvd->SupCapability.u1CapLength = u1CapLength;

            if (((pPeerCapsRcvd->SupCapability.u1CapLength) > 0) &&
                (pPeerCapsRcvd->SupCapability.u1CapLength <= MAX_CAP_LENGTH))
            {
                MEMCPY ((pPeerCapsRcvd->SupCapability.au1CapValue),
                        (pu1MsgBuf + CAP_CODE_LENGTH_SIZE),
                        (pPeerCapsRcvd->SupCapability.u1CapLength));
            }

            BGP4_PEER_RCVD_CAPS_NEGOTIATED_STATUS (pPeerCapsRcvd) =
                u1CommnCapFound;
            TMO_SLL_Add (BGP4_PEER_RCVD_CAPS_LIST (pPeerEntry),
                         &pPeerCapsRcvd->NextCapability);
            if (u1CommnCapFound == BGP4_TRUE)
            {
                PTR_FETCH4 (u4AsafiMask, (pu1MsgBuf + CAP_CODE_LENGTH_SIZE));
                i4RetSts = Bgp4ProcessCapNegotiation (pPeerEntry, u1CapCode,
                                                      u4AsafiMask,
                                                      u1NegOrfMode);
                if (i4RetSts == CAPS_FAILURE)
                {
                    return CAPS_FAILURE;
                }
            }
            pu1MsgBuf = pu1MsgBuf + CAP_CODE_LENGTH_SIZE + u1CapLength;
        }
        if (Bgp4GRIsPeerGRCapable (pPeerEntry) == BGP4_FAILURE)
        {
            pPeerEntry->peerStatus.u1RestartMode = BGP4_RESTART_MODE_NONE;
        }
        /* Reach the position of next Optional parameter in the OPEN message */
        pu1CapsBuf = pu1CapsBuf + (*(pu1CapsBuf + OPTPARAM_TYPE_SIZE)) +
            OPTPARAM_TYPE_LENGTH_SIZE;
    }
    /* The peer which is restart capable before restart and 
     * now it has come up without restart capability
     * then delete all the preserved routes */
    if ((pPeerEntry->peerStatus.u1RestartMode == BGP4_RECEIVING_MODE) &&
        ((Bgp4GRIsPeerGRCapable (pPeerEntry)) == BGP4_FAILURE))
    {
        BGP4_PEER_RESTART_MODE (pPeerEntry) = BGP4_RESTART_MODE_NONE;
        Bgp4GRDeleteAllStaleRoutes (pPeerEntry);
        Bgp4TmrhStopTimer (BGP4_STALE_TIMER, (VOID *) pPeerEntry);
        Bgp4TmrhStopTimer (BGP4_PEER_RESTART_TIMER, (VOID *) pPeerEntry);
    }
    /* Received capability code will be present in the RecvdList. An array with 
     * Mandatory capability code will be checker against the received capability list.
     * If any one of the capability code was not matched then, Notification message
     * with error code wil be sent to peer.*/

    for (i4Index = 0; i4Index < CAP_MAX_MANDATORY; i4Index++)
    {
        TMO_SLL_Scan (BGP4_PEER_RCVD_CAPS_LIST (pPeerEntry),
                      pPeerAltCapsRcvd, tSupCapsInfo *)
        {
            if (au1MandatoryCaps[i4Index] ==
                pPeerAltCapsRcvd->SupCapability.u1CapCode)
            {
                /* i4Index represent the Number of Mandatory parameters.
                 * By default, we have 2 Mandatory parameters, IPv4 unicast and 
                 * Route refresh. Since i4Index starts from 0, the value is 1 in the 
                 * below case*/

                if (i4Index == (CAP_MAX_MANDATORY - 1))
                {
                    PTR_FETCH2 (u2AfiValue,
                                pPeerAltCapsRcvd->SupCapability.au1CapValue);
                    for (i4AfiIndex = 0; i4AfiIndex < CAP_MAX_OPT_CAP_AFI_VALUE;
                         i4AfiIndex++)
                    {
                        if (au2MandatoryCapAFIs[i4AfiIndex] == u2AfiValue)
                        {
                            u1SafiValue =
                                pPeerAltCapsRcvd->SupCapability.au1CapValue[3];
                            for (i4SafiIndex = 0;
                                 i4SafiIndex < CAP_MAX_OPT_CAP_SAFI_VALUE;
                                 i4SafiIndex++)
                            {
                                if (au2MandatoryCapSAFIs[i4AfiIndex]
                                    [i4SafiIndex] == u1SafiValue)
                                {
                                    u1MandatoryCapsFound = BGP4_TRUE;
                                    break;
                                }
                                else
                                {
                                    u1MandatoryCapsFound = BGP4_FALSE;
                                }
                            }
                            if (u1MandatoryCapsFound == BGP4_FALSE)
                            {
                                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                          BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                                          "\tInvalid SAFI in Received Capabilities !!!\n");
                                break;
                            }
                        }
                        else
                        {
                            u1MandatoryCapsFound = BGP4_FALSE;
                        }
                        if (u1MandatoryCapsFound == BGP4_TRUE)
                        {
                            /* correct AFI found */
                            break;
                        }
                    }
                    if (u1MandatoryCapsFound == BGP4_TRUE)
                    {
                        break;
                    }

                }
            }
        }
        continue;
    }

    if ((u1MandatoryCapsFound == BGP4_FALSE) || (u1RcvdComnCaps == 0) ||
        ((BGP4_PEER_SUP_ASAFI_MASK (pPeerEntry) != 0) &&
         (BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) == 0)))
    {
        /* Check if router supports atleast one capabilities for this peer */
        if (u1SupCapsCount > 0)
        {
            CAPS_PROCESS_ERR (BGP4_PEER_CXT_ID (pPeerEntry))++;
            /*If there is no common Capability , send the 
             * Notification message with Error Code Unsupported Capability 
             * and Error Data with all Capabilities that are supported 
             * by the router for this peer */
            TMO_SLL_Scan (BGP4_PEER_SUP_CAPS_LIST (pPeerEntry),
                          pSpkrSupCap, tSupCapsInfo *)
            {

                /*Notification message should contain only mandatory capability 
                 *as per RFC-5492*/
                i4RetValue =
                    IsCapMandatory (pSpkrSupCap->SupCapability.u1CapCode,
                                    &u1MandCapsFound);

                if ((pSpkrSupCap->u1RowStatus != ACTIVE) ||
                    (u1MandCapsFound == BGP4_FALSE))
                {
                    u1MandCapsFound = BGP4_FALSE;
                    continue;
                }
                u1MandCapsFound = BGP4_FALSE;

                if ((u2CapErrDataLen + CAP_CODE_LENGTH_SIZE +
                     (pSpkrSupCap->SupCapability.u1CapLength)) <=
                    MAX_CAP_OPTPARAM_LEN)
                {
                    au1CapErrData[u2CapErrDataLen] =
                        pSpkrSupCap->SupCapability.u1CapCode;
                    au1CapErrData[CAP_CODE_SIZE + u2CapErrDataLen] =
                        pSpkrSupCap->SupCapability.u1CapLength;
                    if ((pSpkrSupCap->SupCapability.u1CapLength > 0) &&
                        (pSpkrSupCap->SupCapability.u1CapLength <
                         MAX_CAP_LENGTH))
                    {
                        MEMCPY ((au1CapErrData + CAP_CODE_LENGTH_SIZE +
                                 u2CapErrDataLen),
                                (pSpkrSupCap->SupCapability.au1CapValue),
                                (pSpkrSupCap->SupCapability.u1CapLength));
                    }

                    /* Update the Error Length */
                    u2CapErrDataLen = (UINT2)
                        (u2CapErrDataLen + CAP_CODE_LENGTH_SIZE +
                         (pSpkrSupCap->SupCapability.u1CapLength));
                }
            }
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                           BGP4_TRC_FLAG, BGP4_PEER_CON_TRC |
                           BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Sending Open Msg Error - Unsupported "
                           "Capability\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerEntry))));
            i4RetSts =
                Bgp4EhSendError (pPeerEntry, BGP4_OPEN_MSG_ERR,
                                 UNSUPPORTED_CAPABILITY, au1CapErrData,
                                 (UINT4) u2CapErrDataLen);
            if (i4RetSts != CAPS_SUCCESS)
            {
                CAPS_NOTFMSG_SEND_ERR (BGP4_PEER_CXT_ID (pPeerEntry))++;
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_CONTROL_PATH_TRC | BGP4_PEER_CON_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : Send Unsupported Capability Error "
                               "Notification FAILED!!!\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerEntry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerEntry))));
            }
            /*When there is no common MP capability, the connection 
             * should not be established*/
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, BGP4_SYSLOG_ID,
                          "CapsRcvdHandler:  Peering failed"
                          "Peer not supporting mandatory capability"));
            BGP4_SET_PEER_PEND_FLAG (pPeerEntry,
                                     BGP4_PEER_MP_CAP_RECV_PEND_START);
            CapsDeletePeerCapsInfo (pPeerEntry);
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Processing Capabilities FAILED !!!\n",
                           Bgp4PrintIpAddr
                           (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                            (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry))));
            return CAPS_FAILURE;
        }
    }
    UNUSED_PARAM (i4RetValue);
    return CAPS_SUCCESS;

}

INT4
Bgp4ProcessCapNegotiation (tBgp4PeerEntry * pPeerEntry,
                           UINT1 u1CapCode, UINT4 u4AsafiMask,
                           UINT1 u1NegOrfMode)
{
    INT4                i4RetSts = CAPS_SUCCESS;
    UINT4               u4OrfCapNegMask = 0;
    switch (u1CapCode)
    {
        case CAP_CODE_MP_EXTN:
            i4RetSts = Bgp4ProcessMpCapNegotiation (pPeerEntry, u4AsafiMask);
            break;
        case CAP_CODE_ROUTE_REFRESH:
            BGP4_PEER_NEG_CAP_MASK (pPeerEntry) |= CAP_NEG_ROUTE_REFRESH_MASK;
            return CAPS_SUCCESS;
            break;
        case CAP_CODE_GRACEFUL_RESTART:
            if (Bgp4GRCheckGRCapability (BGP4_PEER_CXT_ID (pPeerEntry))
                == BGP4_TRUE)
            {
                Bgp4GRProcessRestartCap (pPeerEntry);
            }
            break;
        case CAP_CODE_4_OCTET_ASNO:
            BGP4_PEER_NEG_CAP_MASK (pPeerEntry) |= CAP_NEG_4_OCTET_ASNO_MASK;
            return CAPS_SUCCESS;
        case CAP_CODE_ORF:
            if (Bgp4OrfGetOrfCapMask
                (u4AsafiMask, u1NegOrfMode, &u4OrfCapNegMask) != BGP4_SUCCESS)
            {
                return CAPS_FAILURE;
            }
            BGP4_PEER_NEG_CAP_MASK (pPeerEntry) |= u4OrfCapNegMask;
            break;

        default:
            /* Some other capability received other than MP, Graceful restart 
             * and Route Refresh */
            /* Don't return failure */
            return CAPS_SUCCESS;
    }
    return i4RetSts;
}

INT4
Bgp4ProcessMpCapNegotiation (tBgp4PeerEntry * pPeerEntry, UINT4 u4AsafiMask)
{

    BGP4_PEER_NEG_CAP_MASK (pPeerEntry) |= CAP_NEG_MP_EXTN_MASK;
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
        {
            BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) |= CAP_NEG_IPV4_UNI_MASK;
        }
            break;
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
        {
#ifdef L3VPN
            /* If the peer is a CE peer, dont accept this capability */
            if (BGP4_VPN4_PEER_ROLE (pPeerEntry) == BGP4_VPN4_CE_PEER)
            {
                break;
            }
#endif
            BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) |= CAP_NEG_IPV6_UNI_MASK;
            /* IPV6-unicast is negotiated, then
             * create an instance correspond to this */
            if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeerEntry) == NULL)
            {
                BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeerEntry) =
                    Bgp4MpeAllocateAfiSafiInstance (pPeerEntry,
                                                    BGP4_IPV6_UNI_INDEX);
                if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeerEntry) == NULL)
                {
                    /* memory failed. return  failure */
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                                   BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                                   BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s : Allocating Memory "
                                   "for IPv6 Peer\n\t\tInstance "
                                   "FAILED !!!\n",
                                   Bgp4PrintIpAddr
                                   (BGP4_PEER_REMOTE_ADDR
                                    (pPeerEntry),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry))));
                    gu4BgpDebugCnt[MAX_BGP_AFI_SAFI_SPEC_INFOS_SIZING_ID]++;
                    return CAPS_FAILURE;
                }
            }
        }
            break;
#endif
#ifdef L3VPN
            /* Carrying Label Information - RFC 3107 */
        case CAP_MP_LABELLED_IPV4:
        {
            /* If the peer is a CE peer, dont accept this capability */
            if (BGP4_VPN4_PEER_ROLE (pPeerEntry) == BGP4_VPN4_CE_PEER)
            {
                break;
            }
            /* Labelled IPv4 routes capability */
            BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) |= CAP_NEG_LBL_IPV4_MASK;
            /* IPV4-Label is negotiated, then
             * create an instance correspond to this */
            if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeerEntry) == NULL)
            {
                BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeerEntry) =
                    Bgp4MpeAllocateAfiSafiInstance (pPeerEntry,
                                                    BGP4_IPV4_LBLD_INDEX);
                if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeerEntry) == NULL)
                {
                    /* memory failed. return * failure */
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                                   BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                                   BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s : Allocating Memory "
                                   "for IPv4 Label\n\t\tInstance "
                                   "FAILED !!!\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerEntry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerEntry))));
                    gu4BgpDebugCnt[MAX_BGP_AFI_SAFI_SPEC_INFOS_SIZING_ID]++;
                    return CAPS_FAILURE;
                }
            }
            Bgp4UpdateLabelInMplsFmTable (pPeerEntry,
                                          BGP4_IPV4_LBLD_INDEX,
                                          BGP4_LABEL_OPER_POP,
                                          BGP4_FM_LABEL_ADD);
        }
            break;

        case CAP_MP_VPN4_UNICAST:
        {
            /* If the peer is a CE peer, dont accept this capability */
            if (BGP4_VPN4_PEER_ROLE (pPeerEntry) == BGP4_VPN4_CE_PEER)
            {
                break;
            }
            if (BGP4_PEER_CXT_ID (pPeerEntry) != BGP4_DFLT_VRFID)
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                               BGP4_MOD_NAME, "PEER %s : VPN capability "
                               "can be only negotitated in the default VR\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerEntry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerEntry))));

                return CAPS_FAILURE;
            }
            BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) |= CAP_NEG_VPN4_UNI_MASK;
            /* VPNv4-unicast is negotiated, then
             * create an instance correspond to this */
            if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeerEntry) == NULL)
            {
                BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeerEntry) =
                    Bgp4MpeAllocateAfiSafiInstance (pPeerEntry,
                                                    BGP4_VPN4_UNI_INDEX);
                if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeerEntry) == NULL)
                {
                    /* memory failed. return  failure */
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                                   BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                                   BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s : Allocating Memory "
                                   "for IPv6 Peer\n\t\tInstance "
                                   "FAILED !!!\n",
                                   Bgp4PrintIpAddr
                                   (BGP4_PEER_REMOTE_ADDR
                                    (pPeerEntry),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry))));
                    gu4BgpDebugCnt[MAX_BGP_AFI_SAFI_SPEC_INFOS_SIZING_ID]++;
                    return CAPS_FAILURE;
                }
            }
            /* Set this peer type as PE peer as the 
             * vpnv4-unicast capability is negotiated */
            BGP4_VPN4_PEER_ROLE (pPeerEntry) = BGP4_VPN4_PE_PEER;

            Bgp4UpdateLabelInMplsFmTable (pPeerEntry, BGP4_VPN4_UNI_INDEX,
                                          BGP4_LABEL_OPER_POP,
                                          BGP4_FM_LABEL_ADD);
        }
            break;
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
        {
            BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) |= CAP_NEG_L2VPN_VPLS_MASK;
            /* L2VPN-VPLS is negotiated, then
             * create an instance corresponding to this */
            if (BGP4_PEER_VPLS_AFISAFI_INSTANCE (pPeerEntry) == NULL)
            {
                BGP4_PEER_VPLS_AFISAFI_INSTANCE (pPeerEntry) =
                    Bgp4MpeAllocateAfiSafiInstance (pPeerEntry,
                                                    BGP4_L2VPN_VPLS_INDEX);
                if (BGP4_PEER_VPLS_AFISAFI_INSTANCE (pPeerEntry) == NULL)
                {
                    /* memory failed. return  failure */
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                                   BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                                   BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s : Allocating Memory "
                                   "for L2VPN Peer\n\t\tInstance "
                                   "FAILED !!!\n",
                                   Bgp4PrintIpAddr
                                   (BGP4_PEER_REMOTE_ADDR
                                    (pPeerEntry),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry))));
                    gu4BgpDebugCnt[MAX_BGP_AFI_SAFI_SPEC_INFOS_SIZING_ID]++;
                    return CAPS_FAILURE;
                }
            }
        }
            break;
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
        {
            BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) |= CAP_NEG_L2VPN_EVPN_MASK;
            /* EVPN is negotiated, then
             * create an instance corresponding to this */
            if (BGP4_PEER_EVPN_AFISAFI_INSTANCE (pPeerEntry) == NULL)
            {
                BGP4_PEER_EVPN_AFISAFI_INSTANCE (pPeerEntry) =
                    Bgp4MpeAllocateAfiSafiInstance (pPeerEntry,
                                                    BGP4_L2VPN_EVPN_INDEX);
                if (BGP4_PEER_EVPN_AFISAFI_INSTANCE (pPeerEntry) == NULL)
                {
                    /* memory failed. return  failure */
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                                   BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                                   BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s : Allocating Memory "
                                   "for EVPN Peer\n\t\tInstance "
                                   "FAILED !!!\n",
                                   Bgp4PrintIpAddr
                                   (BGP4_PEER_REMOTE_ADDR
                                    (pPeerEntry),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry))));
                    gu4BgpDebugCnt[MAX_BGP_AFI_SAFI_SPEC_INFOS_SIZING_ID]++;
                    return CAPS_FAILURE;
                }
            }
        }
            break;
#endif
        default:
            break;
    }
    return CAPS_SUCCESS;
}

/*****************************************************************************
    Function Name   :    CapsNotfMsgClassifier
    Description     :    This module receives the NOTIFICATION message and 
                         classifies the message based on the Error Sub-Code. 
                         If Error Sub-Code is Unsupported Optional Parameter,
                         this gives the peer information to 
                         UnsupOptNotfMsgHandler. If the Error Sub-Code is 
                         Unsupported Capability , this gives peer information 
                         to UnsupCapsNotfMsgHandler.
    Input(s)         :   pu1NotfMsgRcvd - Pointer to the NOTIFICATION message 
                                          that is received from peer.
                         pPeerEntry  - Pointer to the peer information.
    Output(s)        :     None
    Global Variables 
    Referred         :     None
    Global Variables 
    Modified         :   None
    Exceptions or OS 
    Err Handling     :   None
    Use of Recursion :   None
    Return(s)         :   CAPS_SUCCESS  - On Success   
                         CAPS_FAILURE  - On Failure
*******************************************************************************/
INT4
Bgp4CapsProcessNotification (UINT1 *pu1NotfMsgRcvd, tBgp4PeerEntry * pPeerEntry)
{

    UINT1               u1ErrorCode;
    UINT1               u1ErrSubCode;
    UINT1               u1ErrorData;
    /* Get the Error Code of Notification message */

    u1ErrorCode = (*pu1NotfMsgRcvd);
    /* Check for OPEN message Error code */
    if (u1ErrorCode == BGP4_OPEN_MSG_ERR)
    {
        /* Get Error Sub-Code of Notification message */
        u1ErrSubCode = (*(pu1NotfMsgRcvd + BGP_ERROR_CODE_LENGTH));
        /* Check for Error Sub-Code Unsupported Optional parameter and Error 
         * Data Capabilities Optional parameter */
        if (u1ErrSubCode == UNSUPPORTED_OPTIONAL_PARAMETER)
        {
            u1ErrorData = (*(pu1NotfMsgRcvd + BGP_ERROR_CODE_LENGTH +
                             BGP_ERROR_SUBCODE_LENGTH));

            if (u1ErrorData == CAPABILITIES_OPTIONAL_PARAMETER)
            {
                /* Process the Unsupported optional parameter 
                 * Notification message */
                BGP4_SET_CAPS_PEER_FLAGS (pPeerEntry,
                                          BGP4_CAPS_UNSUP_OPT_REPEERING);
                Bgp4SemhProcessInvalidEvt (pPeerEntry);
                return BGP4_SUCCESS;
            }
        }

        /* Check for Unsupported Capability Error Sub-Code */
        else if (u1ErrSubCode == UNSUPPORTED_CAPABILITY)
        {
            /* Process the Unsupported capability notification message */
            Bgp4SemhProcessInvalidEvt (pPeerEntry);
            return BGP4_SUCCESS;
        }
    }
    return CAPS_FAILURE;
}

/*****************************************************************************
     Function Name    :  CapsDeletePeerCapsInfo
     Description      :  This module deletes the peer capabilities information 
                         from PEER_CAPS_HASH_TABLE corresponding to pPeerEntry.
     Input(s)          :  pPeerEntry -  Pointer to the peer information
     Output(s)          :  None
     Global Variables 
     Referred         :  PEER_CAPS_HASH_TABLE , PEER_CAPS_INFO_MEMPOOLD , 
                     PEER_CAPS_RCVD_MEMPOOLID
     Global Variables 
     Modified         :  PEER_CAPS_HASH_TABLE
     Exceptions or OS 
     Err Handling     :  None
     Use of Recursion :  None
     Return(s)          :  CAPS_SUCCESS 
                         CAPS_FAILURE
*****************************************************************************/
INT4
CapsDeletePeerCapsInfo (tBgp4PeerEntry * pPeerEntry)
{
    tSupCapsInfo       *pPeerCapsRcvd = NULL;
    tSupCapsInfo       *pTmpPeerCapsRcvd = NULL;
    UINT4               u4MemRetSts;

    BGP_SLL_DYN_Scan (BGP4_PEER_RCVD_CAPS_LIST (pPeerEntry),
                      pPeerCapsRcvd, pTmpPeerCapsRcvd, tSupCapsInfo *)
    {
        TMO_SLL_Delete (BGP4_PEER_RCVD_CAPS_LIST (pPeerEntry),
                        &pPeerCapsRcvd->NextCapability);
        u4MemRetSts = PEER_CAPS_RCVD_ENTRY_FREE (pPeerCapsRcvd);
        if (u4MemRetSts != CAPS_MEM_SUCCESS)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Deleting Received Capabilities "
                           "FAILED !!!\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                            (pPeerEntry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerEntry))));
            return CAPS_FAILURE;
        }
    }
    TMO_SLL_Init (BGP4_PEER_RCVD_CAPS_LIST (pPeerEntry));
    return CAPS_SUCCESS;
}

/*****************************************************************************
     Function Name     :  CapsDeleteSpkrCapsInfo
     Description       :  This module deletes the speaker capabilities 
                          information from SPKR_CAPS_HASH_TABLE corresponding 
                          to Peer Remote Address.
     Input(s)          :  pPeerEntry -  Pointer to the peer information
     Output(s)         :  None
     Global Variables 
     Referred          :  SPKR_CAPS_HASH_TABLE , SPKR_SUP_CAPS_MEMPOOLID , 
                          SPKR_CAPS_INFO_MEMPOOLID
     Global Variables 
     Modified          :  SPKR_CAPS_HASH_TABLE 
     Exceptions or OS 
     Err Handling      :  None
     Use of Recursion  :  None
     Return(s)           :  CAPS_SUCCESS 
                          CAPS_FAILURE
******************************************************************************/
INT4
CapsDeleteSpkrCapsInfo (tBgp4PeerEntry * pPeerEntry)
{
    tSupCapsInfo       *pSpkrSupCap = NULL;
    tSupCapsInfo       *pTmpSpkrSupCap = NULL;
    INT4                i4RetSts;

    BGP_SLL_DYN_Scan (BGP4_PEER_SUP_CAPS_LIST (pPeerEntry), pSpkrSupCap,
                      pTmpSpkrSupCap, tSupCapsInfo *)
    {
        /*check the TMO_SLL_Delete second arguement in all places */
        TMO_SLL_Delete (BGP4_PEER_SUP_CAPS_LIST (pPeerEntry),
                        &pSpkrSupCap->NextCapability);
        i4RetSts = SPKR_SUP_CAPS_ENTRY_FREE (pSpkrSupCap);
        if (i4RetSts != CAPS_MEM_SUCCESS)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Deleting Speaker Supported "
                           "Capabilities FAILED !!!\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                            (pPeerEntry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerEntry))));
            return CAPS_FAILURE;
        }
    }
    TMO_SLL_Init (BGP4_PEER_SUP_CAPS_LIST (pPeerEntry));
    return CAPS_SUCCESS;
}

/*****************************************************************************
     Function Name     : Bgp4DuplicatePeerCaps 
     Description       :  This module Duplicates the peer supported 
                          capabilities and peer received  capabilities 
     Input(s)          :  pSrcCapsList-  Pointer to source capabilities 
                       :  pDestCapsList - Pointer to destination capabilities
     Output(s)         :  None
     Global Variables 
     Referred          :  SPKR_SUP_CAPS_MEMPOOLID , 
                          SPKR_CAPS_INFO_MEMPOOLID
     Global Variables 
     Modified          :  None 
     Exceptions or OS 
     Err Handling      :  None
     Use of Recursion  :  None
     Return(s)           :  CAPS_SUCCESS 
                          CAPS_FAILURE
******************************************************************************/

INT4
Bgp4DuplicatePeerCaps (tTMO_SLL * pSrcCapsList, tTMO_SLL * pDestCapsList)
{
    tSupCapsInfo       *pSpkrCapsInfo;
    tSupCapsInfo       *pTmpSpkrSupCap;

    TMO_SLL_Scan (pSrcCapsList, pTmpSpkrSupCap, tSupCapsInfo *)
    {
        SPKR_SUP_CAPS_ENTRY_CREATE (pSpkrCapsInfo);
        if (pSpkrCapsInfo == NULL)
        {
            return CAPS_FAILURE;
        }
        TMO_SLL_Init_Node (&(pSpkrCapsInfo->NextCapability));
        pSpkrCapsInfo->SupCapability.u1CapCode =
            pTmpSpkrSupCap->SupCapability.u1CapCode;
        pSpkrCapsInfo->SupCapability.u1CapLength =
            pTmpSpkrSupCap->SupCapability.u1CapLength;
        if ((pTmpSpkrSupCap->SupCapability.u1CapLength > 0) &&
            (pTmpSpkrSupCap->SupCapability.u1CapLength <= MAX_CAP_LENGTH))
        {
            MEMCPY (pSpkrCapsInfo->SupCapability.au1CapValue,
                    pTmpSpkrSupCap->SupCapability.au1CapValue,
                    pTmpSpkrSupCap->SupCapability.u1CapLength);
        }
        pSpkrCapsInfo->u1RowStatus = pTmpSpkrSupCap->u1RowStatus;
        pSpkrCapsInfo->u1CapsFlag = pTmpSpkrSupCap->u1CapsFlag;
        TMO_SLL_Add (pDestCapsList, &pSpkrCapsInfo->NextCapability);
    }
    return CAPS_SUCCESS;
}

/*****************************************************************************
     Function Name     :  CapsClearSpkrAncdSts
     Description       :  This module clears the Announced Capabilities flag 
                          if it set 
     Input(s)          :  pPeerEntry -  Pointer to the peer information
     Output(s)         :  None
     Global Variables 
     Referred          :  SPKR_CAPS_HASH_TABLE 
     Global Variables 
     Modified          :  SPKR_CAPS_HASH_TABLE 
     Exceptions or OS 
     Err Handling      :  None
     Use of Recursion  :  None
     Return(s)           :  CAPS_SUCCESS 
                          CAPS_FAILURE
******************************************************************************/
INT4
CapsClearSpkrAncdSts (tBgp4PeerEntry * pPeerEntry)
{
    tSupCapsInfo       *pSupCap = NULL;
    TMO_SLL_Scan (BGP4_PEER_SUP_CAPS_LIST (pPeerEntry), pSupCap, tSupCapsInfo *)
    {
        BGP4_PEER_SUP_CAPS_ANCD_STATUS (pSupCap) = CAPS_FALSE;
    }

    return CAPS_SUCCESS;
}

/*****************************************************************************
     Function Name     :  CapsShutDown 
     Description       :  This module releases all the memory allocated for 
                          capabilities advertisement
     Input(s)          :  pPeerEntry -  Pointer to the peer information
     Output(s)         :  None
     Global Variables 
     Referred          :  None  
                     
     Global Variables 
     Modified          :  None  
     Exceptions or OS 
     Err Handling      :  None
     Use of Recursion  :  None
     Return(s)           :  CAPS_SUCCESS 
                          CAPS_FAILURE
******************************************************************************/
INT4
CapsShutDown (UINT4 u4Context)
{
    CapsDeletePeerCaps (u4Context);
    CapsDeleteSpeakerCaps (u4Context);
    return CAPS_SUCCESS;
}

/*********************************************************************************
     Function Name     :  IsCapMandatory
     Description       :  This function checks whether the u1CapCode is mandatory
     Input(s)          :  u1CapCode
     Output(s)         :  u1MandCapsFound
     Return(s)         :  CAPS_SUCCESS 
**********************************************************************************/
INT4
IsCapMandatory (UINT1 u1CapCode, UINT1 *pu1MandCapsFound)
{
    INT4                i4Index;
    for (i4Index = 0; i4Index < CAP_MAX_MANDATORY; i4Index++)
    {
        if (au1MandatoryCaps[i4Index] == u1CapCode)
        {
            /*i4Index represent the Number of Mandatory parameters.
             * By default, we have IPv4 unicast as Mandatory parameter*/
            *pu1MandCapsFound = BGP4_TRUE;
            break;
        }
    }
    return CAPS_SUCCESS;
}

/*************************End of File ****************************************/
