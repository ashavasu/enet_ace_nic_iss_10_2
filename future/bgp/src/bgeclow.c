/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgeclow.c,v 1.32 2015/02/24 11:57:42 siva Exp $
 *
 * Description: Contains BGP Community feature low level routines
 *
 *******************************************************************/

# include  "bgp4com.h"
# include  "include.h"
# include  "fsbgpcon.h"
# include  "fsbgpogi.h"
# include  "midconst.h"
# include  "bgecm.h"
#include "fsmpbgcli.h"

INT4                ExtCommGetPeerLinkBandWidthEntry (tAddrPrefix,
                                                      tPeerLinkBandWidth **);
INT4                ExtCommCreatePeerLinkBandWidthEntry (tAddrPrefix,
                                                         tPeerLinkBandWidth **);
INT4                ExtCommCreateAddExtCommEntry (tNetAddress,
                                                  tSNMP_OCTET_STRING_TYPE *,
                                                  tExtCommProfile **);
INT4                ExtCommDeleteAddExtCommEntry (tNetAddress,
                                                  tSNMP_OCTET_STRING_TYPE *,
                                                  tExtCommProfile **);
INT4                ExtCommGetAddExtCommEntry (tNetAddress,
                                               tSNMP_OCTET_STRING_TYPE *,
                                               tExtCommProfile **);
INT4                ExtCommCreateDeleteExtCommEntry (tNetAddress,
                                                     tSNMP_OCTET_STRING_TYPE *,
                                                     tExtCommProfile **);
INT4                ExtCommDeleteDeleteExtCommEntry (tNetAddress,
                                                     tSNMP_OCTET_STRING_TYPE *,
                                                     tExtCommProfile **);
INT4                ExtCommGetDeleteExtCommEntry (tNetAddress, UINT1 *,
                                                  tExtCommProfile **);
INT4                ExtCommGetRouteExtCommSetStatusEntry (tNetAddress,
                                                          tRouteExtCommSetStatus
                                                          **);
INT4                ExtCommCreateRouteExtCommSetStatusEntry (tNetAddress,
                                                             tRouteExtCommSetStatus
                                                             **);
INT4                Bgp4ValidateExtCommRouteAddCommTableIndex (tNetAddress,
                                                               tSNMP_OCTET_STRING_TYPE
                                                               *);
INT4                ExtCommCopyExtCommunityFromProfile (tSNMP_OCTET_STRING_TYPE
                                                        *, tExtCommProfile *);
INT4                ExtCommCopyExtCommunityToProfile (tExtCommProfile *,
                                                      tSNMP_OCTET_STRING_TYPE
                                                      *);
UINT1               ExtCommCmpExtCommunity (tSNMP_OCTET_STRING_TYPE *,
                                            tExtCommProfile *);
INT4                ExtCommValidateExtCommunity (tSNMP_OCTET_STRING_TYPE *);

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4ExtCommMaxInFTblEntries
 Input       :  The Indices

                The Object 
                retValFsbgp4ExtCommMaxInFTblEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4ExtCommMaxInFTblEntries (INT4
                                     *pi4RetValFsbgp4ExtCommMaxInFTblEntries)
{
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4ExtCommMaxInFTblEntries =
        FsBGPSizingParams[MAX_BGP_EXT_COMM_IN_FILTER_INFOS_SIZING_ID].
        u4MemPreAllocated;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4ExtCommMaxOutFTblEntries
 Input       :  The Indices

                The Object 
                retValFsbgp4ExtCommMaxOutFTblEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4ExtCommMaxOutFTblEntries (INT4
                                      *pi4RetValFsbgp4ExtCommMaxOutFTblEntries)
{

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4ExtCommMaxOutFTblEntries =
        FsBGPSizingParams[MAX_BGP_EXT_COMM_OUT_FILTER_INFOS_SIZING_ID].
        u4MemPreAllocated;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4ExtCommMaxInFTblEntries
 Input       :  The Indices

                The Object 
                setValFsbgp4ExtCommMaxInFTblEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4ExtCommMaxInFTblEntries (INT4 i4SetValFsbgp4ExtCommMaxInFTblEntries)
{
    tBgpSystemSize      BgpSystemSize;
    UNUSED_PARAM (i4SetValFsbgp4ExtCommMaxInFTblEntries);

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    GetBgpSizingParams (&BgpSystemSize);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsbgp4ExtCommMaxOutFTblEntries
 Input       :  The Indices

                The Object 
                setValFsbgp4ExtCommMaxOutFTblEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4ExtCommMaxOutFTblEntries (INT4
                                      i4SetValFsbgp4ExtCommMaxOutFTblEntries)
{
    tBgpSystemSize      BgpSystemSize;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    GetBgpSizingParams (&BgpSystemSize);

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4ExtCommMaxOutFTblEntries,
                          u4SeqNum, FALSE, BgpLock, BgpUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gpBgpCurrCxtNode->u4ContextId,
                      i4SetValFsbgp4ExtCommMaxOutFTblEntries));

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4ExtCommMaxInFTblEntries
 Input       :  The Indices

                The Object 
                testValFsbgp4ExtCommMaxInFTblEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4ExtCommMaxInFTblEntries (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValFsbgp4ExtCommMaxInFTblEntries)
{
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4ExtCommMaxInFTblEntries > 0) &&
        (i4TestValFsbgp4ExtCommMaxInFTblEntries <=
         MAX_INPUT_EXT_COMM_FILTER_TBL_ENTRIES))
    {
        return SNMP_SUCCESS;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - Invalid Ext-Comm Max Input Filter "
              "Table Index Value.\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4ExtCommMaxOutFTblEntries
 Input       :  The Indices

                The Object 
                testValFsbgp4ExtCommMaxOutFTblEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4ExtCommMaxOutFTblEntries (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4TestValFsbgp4ExtCommMaxOutFTblEntries)
{
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4ExtCommMaxOutFTblEntries > 0) &&
        (i4TestValFsbgp4ExtCommMaxOutFTblEntries <=
         MAX_OUTPUT_EXT_COMM_FILTER_TBL_ENTRIES))
    {
        return SNMP_SUCCESS;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - Invalid Ext-Comm Max Output Filter "
              "Table Index Value.\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4ExtCommMaxInFTblEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4ExtCommMaxInFTblEntries (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4ExtCommMaxOutFTblEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4ExtCommMaxOutFTblEntries (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4MpeExtCommRouteAddExtCommTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4MpeExtCommRouteAddExtCommTable
 Input       :  The Indices
                Fsbgp4AddExtCommRtAfi
                Fsbgp4AddExtCommRtSafi
                Fsbgp4AddExtCommIpNetwork
                Fsbgp4AddExtCommIpPrefixLen
                Fsbgp4AddExtCommVal
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4MpeExtCommRouteAddExtCommTable (INT4
                                                              i4Fsbgp4AddExtCommRtAfi,
                                                              INT4
                                                              i4Fsbgp4AddExtCommRtSafi,
                                                              tSNMP_OCTET_STRING_TYPE
                                                              *
                                                              pFsbgp4AddExtCommIpNetwork,
                                                              INT4
                                                              i4Fsbgp4AddExtCommIpPrefixLen,
                                                              tSNMP_OCTET_STRING_TYPE
                                                              *
                                                              pFsbgp4AddExtCommVal)
{
    tNetAddress         RtAddress;
    INT4                i4Sts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4AddExtCommRtAfi,
                                    i4Fsbgp4AddExtCommRtSafi,
                                    pFsbgp4AddExtCommIpNetwork, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4AddExtCommIpPrefixLen;
    i4Sts = Bgp4ValidateExtCommRouteAddCommTableIndex (RtAddress,
                                                       pFsbgp4AddExtCommVal);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Prefix/Length/Ext-Comm Value.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4MpeExtCommRouteAddExtCommTable
 Input       :  The Indices
                Fsbgp4AddExtCommRtAfi
                Fsbgp4AddExtCommRtSafi
                Fsbgp4AddExtCommIpNetwork
                Fsbgp4AddExtCommIpPrefixLen
                Fsbgp4AddExtCommVal
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4MpeExtCommRouteAddExtCommTable (INT4
                                                      *pi4Fsbgp4AddExtCommRtAfi,
                                                      INT4
                                                      *pi4Fsbgp4AddExtCommRtSafi,
                                                      tSNMP_OCTET_STRING_TYPE *
                                                      pFsbgp4AddExtCommIpNetwork,
                                                      INT4
                                                      *pi4Fsbgp4AddExtCommIpPrefixLen,
                                                      tSNMP_OCTET_STRING_TYPE *
                                                      pFsbgp4AddExtCommVal)
{
    tRouteConfExtComm  *pRouteConfExtComm = NULL;
    tExtCommProfile    *pExtCommProfile = NULL;
    tRouteConfExtComm   RouteConfExtComm;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    MEMSET (&RouteConfExtComm, 0, sizeof (tRouteConfExtComm));
    RouteConfExtComm.u4Context = u4Context;

    pRouteConfExtComm = (tRouteConfExtComm *)
        BGP4_RB_TREE_GET_NEXT (ROUTES_EXT_COMM_ADD_TBL,
                               (tBgp4RBElem *) (&RouteConfExtComm), NULL);
    if ((pRouteConfExtComm != NULL)
        && (pRouteConfExtComm->u4Context == u4Context))
    {
        pExtCommProfile =
            (tExtCommProfile *)
            TMO_SLL_First (&(pRouteConfExtComm->TSConfExtComms));
        if (pExtCommProfile != NULL)
        {
            switch (EXT_COMM_RT_AFI_IN_ROUTE_CONF_STRUCT (pRouteConfExtComm))
            {
                case BGP4_INET_AFI_IPV4:
                {
                    UINT4               u4IpAddress;
                    PTR_FETCH_4 (u4IpAddress,
                                 EXT_COMM_RT_IP_PREFIX_IN_ROUTE_CONF_STRUCT
                                 (pRouteConfExtComm));
                    PTR_ASSIGN4 (pFsbgp4AddExtCommIpNetwork->pu1_OctetList,
                                 (u4IpAddress));
                    pFsbgp4AddExtCommIpNetwork->i4_Length =
                        BGP4_IPV4_PREFIX_LEN;
                    *pi4Fsbgp4AddExtCommRtAfi = BGP4_INET_AFI_IPV4;
                    *pi4Fsbgp4AddExtCommRtSafi = BGP4_INET_SAFI_UNICAST;
                    *pi4Fsbgp4AddExtCommIpPrefixLen =
                        EXT_COMM_RT_PREFIXLEN_IN_ROUTE_CONF_STRUCT
                        (pRouteConfExtComm);
                }
                    break;
#ifdef BGP4_IPV6_WANTED
                case BGP4_INET_AFI_IPV6:
                    MEMCPY (pFsbgp4AddExtCommIpNetwork->pu1_OctetList,
                            EXT_COMM_RT_IP_PREFIX_IN_ROUTE_CONF_STRUCT
                            (pRouteConfExtComm), BGP4_IPV6_PREFIX_LEN);
                    pFsbgp4AddExtCommIpNetwork->i4_Length =
                        BGP4_IPV6_PREFIX_LEN;
                    *pi4Fsbgp4AddExtCommRtAfi = BGP4_INET_AFI_IPV6;
                    *pi4Fsbgp4AddExtCommRtSafi = BGP4_INET_SAFI_UNICAST;
                    *pi4Fsbgp4AddExtCommIpPrefixLen =
                        EXT_COMM_RT_PREFIXLEN_IN_ROUTE_CONF_STRUCT
                        (pRouteConfExtComm);
                    break;
#endif
                default:
                    return SNMP_FAILURE;
            }
            ExtCommCopyExtCommunityFromProfile (pFsbgp4AddExtCommVal,
                                                pExtCommProfile);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4MpeExtCommRouteAddExtCommTable
 Input       :  The Indices
                Fsbgp4AddExtCommRtAfi
                nextFsbgp4AddExtCommRtAfi
                Fsbgp4AddExtCommRtSafi
                nextFsbgp4AddExtCommRtSafi
                Fsbgp4AddExtCommIpNetwork
                nextFsbgp4AddExtCommIpNetwork
                Fsbgp4AddExtCommIpPrefixLen
                nextFsbgp4AddExtCommIpPrefixLen
                Fsbgp4AddExtCommVal
                nextFsbgp4AddExtCommVal
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4MpeExtCommRouteAddExtCommTable (INT4
                                                     i4Fsbgp4AddExtCommRtAfi,
                                                     INT4
                                                     *pi4NextFsbgp4AddExtCommRtAfi,
                                                     INT4
                                                     i4Fsbgp4AddExtCommRtSafi,
                                                     INT4
                                                     *pi4NextFsbgp4AddExtCommRtSafi,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pFsbgp4AddExtCommIpNetwork,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pNextFsbgp4AddExtCommIpNetwork,
                                                     INT4
                                                     i4Fsbgp4AddExtCommIpPrefixLen,
                                                     INT4
                                                     *pi4NextFsbgp4AddExtCommIpPrefixLen,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pFsbgp4AddExtCommVal,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pNextFsbgp4AddExtCommVal)
{
    tRouteConfExtComm  *pRouteConfExtComm = NULL;
    tExtCommProfile    *pExtCommProfile = NULL;
    tNetAddress         RtAddress;
    INT4                i4Sts;
    tRouteConfExtComm   RouteConfExtComm;
    UINT1               u1NodeDel = BGP4_FALSE;
    UINT1               u1MatchFnd = COMM_FALSE;
    UINT1               u1MatchExtComm = COMM_FALSE;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4AddExtCommRtAfi,
                                    i4Fsbgp4AddExtCommRtSafi,
                                    pFsbgp4AddExtCommIpNetwork, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4AddExtCommIpPrefixLen;

    RouteConfExtComm.u4Context = u4Context;

    Bgp4CopyNetAddressStruct (&(RouteConfExtComm.RtNetAddress), RtAddress);
    pRouteConfExtComm = BGP4_RB_TREE_GET (ROUTES_EXT_COMM_ADD_TBL,
                                          (tBgp4RBElem *) (&RouteConfExtComm));
    if (pRouteConfExtComm == NULL)
    {
        u1NodeDel = BGP4_TRUE;
    }
    if (u1NodeDel != BGP4_TRUE)
    {
        TMO_SLL_Scan (&(pRouteConfExtComm->TSConfExtComms),
                      pExtCommProfile, tExtCommProfile *)
        {
            if (u1MatchFnd == COMM_TRUE)
            {
                switch (EXT_COMM_RT_AFI_IN_ROUTE_CONF_STRUCT
                        (pRouteConfExtComm))
                {
                    case BGP4_INET_AFI_IPV4:
                    {
                        UINT4               u4IpAddress;
                        PTR_FETCH_4 (u4IpAddress,
                                     EXT_COMM_RT_IP_PREFIX_IN_ROUTE_CONF_STRUCT
                                     (pRouteConfExtComm));
                        PTR_ASSIGN4 (pNextFsbgp4AddExtCommIpNetwork->
                                     pu1_OctetList, (u4IpAddress));
                        pNextFsbgp4AddExtCommIpNetwork->i4_Length =
                            BGP4_IPV4_PREFIX_LEN;
                        *pi4NextFsbgp4AddExtCommRtAfi = BGP4_INET_AFI_IPV4;
                        *pi4NextFsbgp4AddExtCommRtSafi = BGP4_INET_SAFI_UNICAST;
                        *pi4NextFsbgp4AddExtCommIpPrefixLen =
                            EXT_COMM_RT_PREFIXLEN_IN_ROUTE_CONF_STRUCT
                            (pRouteConfExtComm);
                    }
                        break;
#ifdef BGP4_IPV6_WANTED
                    case BGP4_INET_AFI_IPV6:
                        MEMCPY (pNextFsbgp4AddExtCommIpNetwork->
                                pu1_OctetList,
                                EXT_COMM_RT_IP_PREFIX_IN_ROUTE_CONF_STRUCT
                                (pRouteConfExtComm), BGP4_IPV6_PREFIX_LEN);
                        pNextFsbgp4AddExtCommIpNetwork->i4_Length =
                            BGP4_IPV6_PREFIX_LEN;
                        *pi4NextFsbgp4AddExtCommRtAfi = BGP4_INET_AFI_IPV6;
                        *pi4NextFsbgp4AddExtCommRtSafi = BGP4_INET_SAFI_UNICAST;
                        *pi4NextFsbgp4AddExtCommIpPrefixLen =
                            EXT_COMM_RT_PREFIXLEN_IN_ROUTE_CONF_STRUCT
                            (pRouteConfExtComm);
                        break;
#endif
                    default:
                        return SNMP_FAILURE;
                }
                ExtCommCopyExtCommunityFromProfile
                    (pNextFsbgp4AddExtCommVal, pExtCommProfile);
                return SNMP_SUCCESS;
            }

            u1MatchExtComm =
                ExtCommCmpExtCommunity (pFsbgp4AddExtCommVal, pExtCommProfile);
            if (((PrefixMatch
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                   (EXT_COMM_NET_ADDRESS_IN_ROUTE_CONF_STRUCT
                    (pRouteConfExtComm)),
                   BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (RtAddress))) ==
                 BGP4_TRUE)
                &&
                (EXT_COMM_RT_PREFIXLEN_IN_ROUTE_CONF_STRUCT (pRouteConfExtComm)
                 == BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress))
                && (u1MatchExtComm == COMM_TRUE))
            {
                u1MatchFnd = COMM_TRUE;

            }
        }
    }

    RouteConfExtComm.u4Context = u4Context;

    pRouteConfExtComm =
        BGP4_RB_TREE_GET_NEXT (ROUTES_EXT_COMM_ADD_TBL,
                               (tBgp4RBElem *) (&RouteConfExtComm), NULL);
    if ((pRouteConfExtComm != NULL)
        && (pRouteConfExtComm->u4Context == u4Context))
    {
        pExtCommProfile = (tExtCommProfile *)
            TMO_SLL_First (&(pRouteConfExtComm->TSConfExtComms));
        if (pExtCommProfile == NULL)
        {
            return SNMP_FAILURE;
        }

        switch (EXT_COMM_RT_AFI_IN_ROUTE_CONF_STRUCT (pRouteConfExtComm))
        {
            case BGP4_INET_AFI_IPV4:
            {
                UINT4               u4IpAddress;
                PTR_FETCH_4 (u4IpAddress,
                             EXT_COMM_RT_IP_PREFIX_IN_ROUTE_CONF_STRUCT
                             (pRouteConfExtComm));
                PTR_ASSIGN4 (pNextFsbgp4AddExtCommIpNetwork->pu1_OctetList,
                             (u4IpAddress));
                pNextFsbgp4AddExtCommIpNetwork->i4_Length =
                    BGP4_IPV4_PREFIX_LEN;
                *pi4NextFsbgp4AddExtCommRtAfi = BGP4_INET_AFI_IPV4;
                *pi4NextFsbgp4AddExtCommRtSafi = BGP4_INET_SAFI_UNICAST;
                *pi4NextFsbgp4AddExtCommIpPrefixLen =
                    EXT_COMM_RT_PREFIXLEN_IN_ROUTE_CONF_STRUCT
                    (pRouteConfExtComm);
            }
                break;
#ifdef BGP4_IPV6_WANTED
            case BGP4_INET_AFI_IPV6:
                MEMCPY (pNextFsbgp4AddExtCommIpNetwork->
                        pu1_OctetList,
                        EXT_COMM_RT_IP_PREFIX_IN_ROUTE_CONF_STRUCT
                        (pRouteConfExtComm), BGP4_IPV6_PREFIX_LEN);
                pNextFsbgp4AddExtCommIpNetwork->i4_Length =
                    BGP4_IPV6_PREFIX_LEN;
                *pi4NextFsbgp4AddExtCommRtAfi = BGP4_INET_AFI_IPV6;
                *pi4NextFsbgp4AddExtCommRtSafi = BGP4_INET_SAFI_UNICAST;
                *pi4NextFsbgp4AddExtCommIpPrefixLen =
                    EXT_COMM_RT_PREFIXLEN_IN_ROUTE_CONF_STRUCT
                    (pRouteConfExtComm);
                break;
#endif
            default:
                return SNMP_FAILURE;
        }
        ExtCommCopyExtCommunityFromProfile
            (pNextFsbgp4AddExtCommVal, pExtCommProfile);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeAddExtCommRowStatus
 Input       :  The Indices
                Fsbgp4AddExtCommRtAfi
                Fsbgp4AddExtCommRtSafi
                Fsbgp4AddExtCommIpNetwork
                Fsbgp4AddExtCommIpPrefixLen
                Fsbgp4AddExtCommVal

                The Object 
                retValFsbgp4AddExtCommRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeAddExtCommRowStatus (INT4 i4Fsbgp4AddExtCommRtAfi,
                                    INT4 i4Fsbgp4AddExtCommRtSafi,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsbgp4AddExtCommIpNetwork,
                                    INT4 i4Fsbgp4AddExtCommIpPrefixLen,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsbgp4AddExtCommVal,
                                    INT4 *pi4RetValFsbgp4AddExtCommRowStatus)
{
    tExtCommProfile    *pExtCommProfile = NULL;
    tNetAddress         RtAddress;
    INT4                i4Sts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4AddExtCommRtAfi,
                                    i4Fsbgp4AddExtCommRtSafi,
                                    pFsbgp4AddExtCommIpNetwork, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4AddExtCommIpPrefixLen;

    ExtCommGetAddExtCommEntry (RtAddress,
                               pFsbgp4AddExtCommVal, &pExtCommProfile);
    if (pExtCommProfile == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Ext-Comm entry exists..\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4AddExtCommRowStatus = pExtCommProfile->u1RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4mpeAddExtCommRowStatus
 Input       :  The Indices
                Fsbgp4AddExtCommRtAfi
                Fsbgp4AddExtCommRtSafi
                Fsbgp4AddExtCommIpNetwork
                Fsbgp4AddExtCommIpPrefixLen
                Fsbgp4AddExtCommVal

                The Object 
                setValFsbgp4AddExtCommRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4mpeAddExtCommRowStatus (INT4 i4Fsbgp4AddExtCommRtAfi,
                                    INT4 i4Fsbgp4AddExtCommRtSafi,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsbgp4AddExtCommIpNetwork,
                                    INT4 i4Fsbgp4AddExtCommIpPrefixLen,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsbgp4AddExtCommVal,
                                    INT4 i4SetValFsbgp4AddExtCommRowStatus)
{
    tExtCommProfile    *pExtCommProfile = NULL;
    tNetAddress         RtAddress;
    INT4                i4RetSts;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4RetSts = Bgp4LowGetRouteAddress (i4Fsbgp4AddExtCommRtAfi,
                                       i4Fsbgp4AddExtCommRtSafi,
                                       pFsbgp4AddExtCommIpNetwork, &RtAddress);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4AddExtCommIpPrefixLen;

    switch (i4SetValFsbgp4AddExtCommRowStatus)
    {
        case CREATE_AND_GO:
            i4RetSts =
                ExtCommCreateAddExtCommEntry (RtAddress,
                                              pFsbgp4AddExtCommVal,
                                              &pExtCommProfile);
            if (i4RetSts == EXT_COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create entry in "
                          "Ext-Comm Add Table.\n");
                return SNMP_FAILURE;
            }
            pExtCommProfile->u1RowStatus = ACTIVE;
            break;

        case CREATE_AND_WAIT:
            i4RetSts =
                ExtCommCreateAddExtCommEntry (RtAddress,
                                              pFsbgp4AddExtCommVal,
                                              &pExtCommProfile);
            if (i4RetSts == EXT_COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create entry in "
                          "Ext-Comm Add Table.\n");
                return SNMP_FAILURE;
            }
            pExtCommProfile->u1RowStatus = NOT_IN_SERVICE;
            break;

        case ACTIVE:
            ExtCommGetAddExtCommEntry (RtAddress,
                                       pFsbgp4AddExtCommVal, &pExtCommProfile);
            if (pExtCommProfile == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching entry exist in "
                          "Ext-Comm Add Table.\n");
                return SNMP_FAILURE;
            }
            pExtCommProfile->u1RowStatus = ACTIVE;
            break;

        case NOT_IN_SERVICE:
            ExtCommGetAddExtCommEntry (RtAddress,
                                       pFsbgp4AddExtCommVal, &pExtCommProfile);
            if (pExtCommProfile == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching entry exist in "
                          "Ext-Comm Add Table.\n");
                return SNMP_FAILURE;
            }
            pExtCommProfile->u1RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:
            i4RetSts =
                ExtCommDeleteAddExtCommEntry (RtAddress,
                                              pFsbgp4AddExtCommVal,
                                              &pExtCommProfile);
            break;

        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4mpeAddExtCommRowStatus,
                          u4SeqNum, TRUE, BgpLock, BgpUnLock, 6, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %s %i %s %i",
                      gpBgpCurrCxtNode->u4ContextId, i4Fsbgp4AddExtCommRtAfi,
                      i4Fsbgp4AddExtCommRtSafi, pFsbgp4AddExtCommIpNetwork,
                      i4Fsbgp4AddExtCommIpPrefixLen, pFsbgp4AddExtCommVal,
                      i4SetValFsbgp4AddExtCommRowStatus));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4mpeAddExtCommRowStatus
 Input       :  The Indices
                Fsbgp4AddExtCommRtAfi
                Fsbgp4AddExtCommRtSafi
                Fsbgp4AddExtCommIpNetwork
                Fsbgp4AddExtCommIpPrefixLen
                Fsbgp4AddExtCommVal

                The Object 
                testValFsbgp4AddExtCommRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4mpeAddExtCommRowStatus (UINT4 *pu4ErrorCode,
                                       INT4 i4Fsbgp4AddExtCommRtAfi,
                                       INT4 i4Fsbgp4AddExtCommRtSafi,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsbgp4AddExtCommIpNetwork,
                                       INT4 i4Fsbgp4AddExtCommIpPrefixLen,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsbgp4AddExtCommVal,
                                       INT4 i4TestValFsbgp4AddExtCommRowStatus)
{
    tExtCommProfile    *pExtCommProfile = NULL;
    tNetAddress         RtAddress;
    INT4                i4RetSts;
    INT4                i4AddStatus = EXT_COMM_FAILURE;
    UINT4               u4Context = 0;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Local AS not configured..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }
#ifdef L3VPN
    if ((i4Fsbgp4AddExtCommRtSafi == BGP4_INET_SAFI_LABEL) ||
        (i4Fsbgp4AddExtCommRtSafi == BGP4_INET_SAFI_VPNV4_UNICAST))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#endif

    i4RetSts = Bgp4LowGetRouteAddress (i4Fsbgp4AddExtCommRtAfi,
                                       i4Fsbgp4AddExtCommRtSafi,
                                       pFsbgp4AddExtCommIpNetwork, &RtAddress);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4AddExtCommIpPrefixLen;
    i4RetSts = Bgp4ValidateExtCommRouteAddCommTableIndex (RtAddress,
                                                          pFsbgp4AddExtCommVal);
    if (i4RetSts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Input Prefix/Length Value.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PREFIX_ERR);
        return SNMP_FAILURE;
    }
    switch (i4TestValFsbgp4AddExtCommRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            i4AddStatus =
                ExtCommGetAddExtCommEntry (RtAddress,
                                           pFsbgp4AddExtCommVal,
                                           &pExtCommProfile);
            if (i4AddStatus != EXT_COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Matching entry already exists in "
                          "Ext-Comm Add Table.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
        case NOT_IN_SERVICE:
            i4AddStatus =
                ExtCommGetAddExtCommEntry (RtAddress,
                                           pFsbgp4AddExtCommVal,
                                           &pExtCommProfile);
            if (i4AddStatus != EXT_COMM_SUCCESS)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching entry exist in "
                          "Ext-Comm Add Table.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if ((pExtCommProfile->u1RowStatus == ACTIVE) ||
                (pExtCommProfile->u1RowStatus == NOT_IN_SERVICE) ||
                (pExtCommProfile->u1RowStatus == NOT_READY))
            {
                return SNMP_SUCCESS;
            }
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        case DESTROY:
            ExtCommGetAddExtCommEntry (RtAddress,
                                       pFsbgp4AddExtCommVal, &pExtCommProfile);
            return SNMP_SUCCESS;

        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4MpeExtCommRouteAddExtCommTable
 Input       :  The Indices
                Fsbgp4AddExtCommRtAfi
                Fsbgp4AddExtCommRtSafi
                Fsbgp4AddExtCommIpNetwork
                Fsbgp4AddExtCommIpPrefixLen
                Fsbgp4AddExtCommVal
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4MpeExtCommRouteAddExtCommTable (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4MpeExtCommRouteDeleteExtCommTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4MpeExtCommRouteDeleteExtCommTable
 Input       :  The Indices
                Fsbgp4DeleteExtCommRtAfi
                Fsbgp4DeleteExtCommRtSafi
                Fsbgp4DeleteExtCommIpNetwork
                Fsbgp4DeleteExtCommIpPrefixLen
                Fsbgp4DeleteExtCommVal
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4MpeExtCommRouteDeleteExtCommTable (INT4
                                                                 i4Fsbgp4DeleteExtCommRtAfi,
                                                                 INT4
                                                                 i4Fsbgp4DeleteExtCommRtSafi,
                                                                 tSNMP_OCTET_STRING_TYPE
                                                                 *
                                                                 pFsbgp4DeleteExtCommIpNetwork,
                                                                 INT4
                                                                 i4Fsbgp4DeleteExtCommIpPrefixLen,
                                                                 tSNMP_OCTET_STRING_TYPE
                                                                 *
                                                                 pFsbgp4DeleteExtCommVal)
{
    tNetAddress         RtAddress;
    INT4                i4Sts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4DeleteExtCommRtAfi,
                                    i4Fsbgp4DeleteExtCommRtSafi,
                                    pFsbgp4DeleteExtCommIpNetwork, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4DeleteExtCommIpPrefixLen;
    i4Sts = Bgp4ValidateExtCommRouteAddCommTableIndex (RtAddress,
                                                       pFsbgp4DeleteExtCommVal);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Prefix/Length/Ext-Comm Value.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4MpeExtCommRouteDeleteExtCommTable
 Input       :  The Indices
                Fsbgp4DeleteExtCommRtAfi
                Fsbgp4DeleteExtCommRtSafi
                Fsbgp4DeleteExtCommIpNetwork
                Fsbgp4DeleteExtCommIpPrefixLen
                Fsbgp4DeleteExtCommVal
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4MpeExtCommRouteDeleteExtCommTable (INT4
                                                         *pi4Fsbgp4DeleteExtCommRtAfi,
                                                         INT4
                                                         *pi4Fsbgp4DeleteExtCommRtSafi,
                                                         tSNMP_OCTET_STRING_TYPE
                                                         *
                                                         pFsbgp4DeleteExtCommIpNetwork,
                                                         INT4
                                                         *pi4Fsbgp4DeleteExtCommIpPrefixLen,
                                                         tSNMP_OCTET_STRING_TYPE
                                                         *
                                                         pFsbgp4DeleteExtCommVal)
{
    tRouteConfExtComm  *pRouteConfExtComm = NULL;
    tExtCommProfile    *pExtCommProfile = NULL;
    tRouteConfExtComm   RouteConfExtComm;
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    MEMSET (&RouteConfExtComm, 0, sizeof (tRouteConfExtComm));
    RouteConfExtComm.u4Context = u4Context;

    pRouteConfExtComm = (tRouteConfExtComm *)
        BGP4_RB_TREE_GET_NEXT (ROUTES_EXT_COMM_DELETE_TBL,
                               (tBgp4RBElem *) (&RouteConfExtComm), NULL);
    if ((pRouteConfExtComm != NULL)
        && (pRouteConfExtComm->u4Context == u4Context))
    {
        pExtCommProfile =
            (tExtCommProfile *)
            TMO_SLL_First (&(pRouteConfExtComm->TSConfExtComms));
        if (pExtCommProfile != NULL)
        {
            switch (EXT_COMM_RT_AFI_IN_ROUTE_CONF_STRUCT (pRouteConfExtComm))
            {
                case BGP4_INET_AFI_IPV4:
                {
                    UINT4               u4IpAddress;
                    PTR_FETCH_4 (u4IpAddress,
                                 EXT_COMM_RT_IP_PREFIX_IN_ROUTE_CONF_STRUCT
                                 (pRouteConfExtComm));
                    PTR_ASSIGN4 (pFsbgp4DeleteExtCommIpNetwork->pu1_OctetList,
                                 (u4IpAddress));
                    pFsbgp4DeleteExtCommIpNetwork->i4_Length =
                        BGP4_IPV4_PREFIX_LEN;
                    *pi4Fsbgp4DeleteExtCommRtAfi = BGP4_INET_AFI_IPV4;
                    *pi4Fsbgp4DeleteExtCommRtSafi = BGP4_INET_SAFI_UNICAST;
                    *pi4Fsbgp4DeleteExtCommIpPrefixLen =
                        EXT_COMM_RT_PREFIXLEN_IN_ROUTE_CONF_STRUCT
                        (pRouteConfExtComm);

                }
                    break;
#ifdef BGP4_IPV6_WANTED
                case BGP4_INET_AFI_IPV6:
                    MEMCPY (pFsbgp4DeleteExtCommIpNetwork->pu1_OctetList,
                            EXT_COMM_RT_IP_PREFIX_IN_ROUTE_CONF_STRUCT
                            (pRouteConfExtComm), BGP4_IPV6_PREFIX_LEN);
                    pFsbgp4DeleteExtCommIpNetwork->i4_Length =
                        BGP4_IPV6_PREFIX_LEN;
                    *pi4Fsbgp4DeleteExtCommRtAfi = BGP4_INET_AFI_IPV6;
                    *pi4Fsbgp4DeleteExtCommRtSafi = BGP4_INET_SAFI_UNICAST;
                    *pi4Fsbgp4DeleteExtCommIpPrefixLen =
                        EXT_COMM_RT_PREFIXLEN_IN_ROUTE_CONF_STRUCT
                        (pRouteConfExtComm);
                    break;
#endif
                default:
                    return SNMP_FAILURE;
            }
            ExtCommCopyExtCommunityFromProfile (pFsbgp4DeleteExtCommVal,
                                                pExtCommProfile);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4MpeExtCommRouteDeleteExtCommTable
 Input       :  The Indices
                Fsbgp4DeleteExtCommRtAfi
                nextFsbgp4DeleteExtCommRtAfi
                Fsbgp4DeleteExtCommRtSafi
                nextFsbgp4DeleteExtCommRtSafi
                Fsbgp4DeleteExtCommIpNetwork
                nextFsbgp4DeleteExtCommIpNetwork
                Fsbgp4DeleteExtCommIpPrefixLen
                nextFsbgp4DeleteExtCommIpPrefixLen
                Fsbgp4DeleteExtCommVal
                nextFsbgp4DeleteExtCommVal
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4MpeExtCommRouteDeleteExtCommTable (INT4
                                                        i4Fsbgp4DeleteExtCommRtAfi,
                                                        INT4
                                                        *pi4NextFsbgp4DeleteExtCommRtAfi,
                                                        INT4
                                                        i4Fsbgp4DeleteExtCommRtSafi,
                                                        INT4
                                                        *pi4NextFsbgp4DeleteExtCommRtSafi,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pFsbgp4DeleteExtCommIpNetwork,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pNextFsbgp4DeleteExtCommIpNetwork,
                                                        INT4
                                                        i4Fsbgp4DeleteExtCommIpPrefixLen,
                                                        INT4
                                                        *pi4NextFsbgp4DeleteExtCommIpPrefixLen,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pFsbgp4DeleteExtCommVal,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pNextFsbgp4DeleteExtCommVal)
{
    tRouteConfExtComm  *pRouteConfExtComm = NULL;
    tExtCommProfile    *pExtCommProfile = NULL;
    tNetAddress         RtAddress;
    tRouteConfExtComm   RouteConfExtComm;
    UINT1               u1NodeDel = BGP4_FALSE;
    INT4                i4Sts;
    UINT1               u1MatchFnd = COMM_FALSE;
    UINT1               u1MatchExtComm = COMM_FALSE;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4DeleteExtCommRtAfi,
                                    i4Fsbgp4DeleteExtCommRtSafi,
                                    pFsbgp4DeleteExtCommIpNetwork, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4DeleteExtCommIpPrefixLen;

    RouteConfExtComm.u4Context = u4Context;

    Bgp4CopyNetAddressStruct (&(RouteConfExtComm.RtNetAddress), RtAddress);
    pRouteConfExtComm = BGP4_RB_TREE_GET (ROUTES_EXT_COMM_DELETE_TBL,
                                          (tBgp4RBElem *) (&RouteConfExtComm));
    if (pRouteConfExtComm == NULL)
    {
        u1NodeDel = BGP4_TRUE;
    }
    if (u1NodeDel != BGP4_TRUE)
    {
        TMO_SLL_Scan (&(pRouteConfExtComm->TSConfExtComms),
                      pExtCommProfile, tExtCommProfile *)
        {
            if (u1MatchFnd == COMM_TRUE)
            {
                switch (EXT_COMM_RT_AFI_IN_ROUTE_CONF_STRUCT
                        (pRouteConfExtComm))
                {
                    case BGP4_INET_AFI_IPV4:
                    {
                        UINT4               u4IpAddress;
                        PTR_FETCH_4 (u4IpAddress,
                                     EXT_COMM_RT_IP_PREFIX_IN_ROUTE_CONF_STRUCT
                                     (pRouteConfExtComm));
                        PTR_ASSIGN4 (pNextFsbgp4DeleteExtCommIpNetwork->
                                     pu1_OctetList, (u4IpAddress));
                        pNextFsbgp4DeleteExtCommIpNetwork->i4_Length =
                            BGP4_IPV4_PREFIX_LEN;
                        *pi4NextFsbgp4DeleteExtCommRtAfi = BGP4_INET_AFI_IPV4;
                        *pi4NextFsbgp4DeleteExtCommRtSafi =
                            BGP4_INET_SAFI_UNICAST;
                        *pi4NextFsbgp4DeleteExtCommIpPrefixLen =
                            EXT_COMM_RT_PREFIXLEN_IN_ROUTE_CONF_STRUCT
                            (pRouteConfExtComm);
                    }
                        break;
#ifdef BGP4_IPV6_WANTED
                    case BGP4_INET_AFI_IPV6:
                        MEMCPY (pNextFsbgp4DeleteExtCommIpNetwork->
                                pu1_OctetList,
                                EXT_COMM_RT_IP_PREFIX_IN_ROUTE_CONF_STRUCT
                                (pRouteConfExtComm), BGP4_IPV6_PREFIX_LEN);
                        pNextFsbgp4DeleteExtCommIpNetwork->i4_Length =
                            BGP4_IPV6_PREFIX_LEN;
                        *pi4NextFsbgp4DeleteExtCommRtAfi = BGP4_INET_AFI_IPV6;
                        *pi4NextFsbgp4DeleteExtCommRtSafi =
                            BGP4_INET_SAFI_UNICAST;
                        *pi4NextFsbgp4DeleteExtCommIpPrefixLen =
                            EXT_COMM_RT_PREFIXLEN_IN_ROUTE_CONF_STRUCT
                            (pRouteConfExtComm);
                        break;
#endif
                    default:
                        return SNMP_FAILURE;
                }
                ExtCommCopyExtCommunityFromProfile
                    (pNextFsbgp4DeleteExtCommVal, pExtCommProfile);
                return SNMP_SUCCESS;
            }

            u1MatchExtComm =
                ExtCommCmpExtCommunity (pFsbgp4DeleteExtCommVal,
                                        pExtCommProfile);
            if (((PrefixMatch
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                   (EXT_COMM_NET_ADDRESS_IN_ROUTE_CONF_STRUCT
                    (pRouteConfExtComm)),
                   BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (RtAddress))) ==
                 BGP4_TRUE)
                &&
                (EXT_COMM_RT_PREFIXLEN_IN_ROUTE_CONF_STRUCT (pRouteConfExtComm)
                 == BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress))
                && (u1MatchExtComm == COMM_TRUE))
            {
                u1MatchFnd = COMM_TRUE;

            }
        }
    }

    RouteConfExtComm.u4Context = u4Context;

    pRouteConfExtComm =
        BGP4_RB_TREE_GET_NEXT (ROUTES_EXT_COMM_DELETE_TBL,
                               (tBgp4RBElem *) (&RouteConfExtComm), NULL);
    if ((pRouteConfExtComm != NULL)
        && (pRouteConfExtComm->u4Context == u4Context))
    {
        pExtCommProfile = (tExtCommProfile *)
            TMO_SLL_First (&(pRouteConfExtComm->TSConfExtComms));
        if (pExtCommProfile == NULL)
        {
            return SNMP_FAILURE;
        }
        switch (EXT_COMM_RT_AFI_IN_ROUTE_CONF_STRUCT (pRouteConfExtComm))
        {
            case BGP4_INET_AFI_IPV4:
            {
                UINT4               u4IpAddress;
                PTR_FETCH_4 (u4IpAddress,
                             EXT_COMM_RT_IP_PREFIX_IN_ROUTE_CONF_STRUCT
                             (pRouteConfExtComm));
                PTR_ASSIGN4 (pNextFsbgp4DeleteExtCommIpNetwork->pu1_OctetList,
                             (u4IpAddress));
                pNextFsbgp4DeleteExtCommIpNetwork->i4_Length =
                    BGP4_IPV4_PREFIX_LEN;
                *pi4NextFsbgp4DeleteExtCommRtAfi = BGP4_INET_AFI_IPV4;
                *pi4NextFsbgp4DeleteExtCommRtSafi = BGP4_INET_SAFI_UNICAST;
                *pi4NextFsbgp4DeleteExtCommIpPrefixLen =
                    EXT_COMM_RT_PREFIXLEN_IN_ROUTE_CONF_STRUCT
                    (pRouteConfExtComm);
            }
                break;
#ifdef BGP4_IPV6_WANTED
            case BGP4_INET_AFI_IPV6:
                MEMCPY (pNextFsbgp4DeleteExtCommIpNetwork->
                        pu1_OctetList,
                        EXT_COMM_RT_IP_PREFIX_IN_ROUTE_CONF_STRUCT
                        (pRouteConfExtComm), BGP4_IPV6_PREFIX_LEN);
                pNextFsbgp4DeleteExtCommIpNetwork->i4_Length =
                    BGP4_IPV6_PREFIX_LEN;
                *pi4NextFsbgp4DeleteExtCommRtAfi = BGP4_INET_AFI_IPV6;
                *pi4NextFsbgp4DeleteExtCommRtSafi = BGP4_INET_SAFI_UNICAST;
                *pi4NextFsbgp4DeleteExtCommIpPrefixLen =
                    EXT_COMM_RT_PREFIXLEN_IN_ROUTE_CONF_STRUCT
                    (pRouteConfExtComm);
                break;
#endif
            default:
                return SNMP_FAILURE;
        }
        ExtCommCopyExtCommunityFromProfile
            (pNextFsbgp4DeleteExtCommVal, pExtCommProfile);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeDeleteExtCommRowStatus
 Input       :  The Indices
                Fsbgp4DeleteExtCommRtAfi
                Fsbgp4DeleteExtCommRtSafi
                Fsbgp4DeleteExtCommIpNetwork
                Fsbgp4DeleteExtCommIpPrefixLen
                Fsbgp4DeleteExtCommVal

                The Object 
                retValFsbgp4DeleteExtCommRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeDeleteExtCommRowStatus (INT4 i4Fsbgp4DeleteExtCommRtAfi,
                                       INT4 i4Fsbgp4DeleteExtCommRtSafi,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsbgp4DeleteExtCommIpNetwork,
                                       INT4 i4Fsbgp4DeleteExtCommIpPrefixLen,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsbgp4DeleteExtCommVal,
                                       INT4
                                       *pi4RetValFsbgp4DeleteExtCommRowStatus)
{
    tExtCommProfile    *pExtCommProfile = NULL;
    tNetAddress         RtAddress;
    INT4                i4Sts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4DeleteExtCommRtAfi,
                                    i4Fsbgp4DeleteExtCommRtSafi,
                                    pFsbgp4DeleteExtCommIpNetwork, &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching entry present in "
                  "Ext-Comm Delete Table.\n");
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4DeleteExtCommIpPrefixLen;

    ExtCommGetDeleteExtCommEntry (RtAddress,
                                  pFsbgp4DeleteExtCommVal->pu1_OctetList,
                                  &pExtCommProfile);
    if (pExtCommProfile == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching entry present in "
                  "Ext-Comm Delete Table.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4DeleteExtCommRowStatus = pExtCommProfile->u1RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4mpeDeleteExtCommRowStatus
 Input       :  The Indices
                Fsbgp4DeleteExtCommRtAfi
                Fsbgp4DeleteExtCommRtSafi
                Fsbgp4DeleteExtCommIpNetwork
                Fsbgp4DeleteExtCommIpPrefixLen
                Fsbgp4DeleteExtCommVal

                The Object 
                setValFsbgp4DeleteExtCommRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4mpeDeleteExtCommRowStatus (INT4 i4Fsbgp4DeleteExtCommRtAfi,
                                       INT4 i4Fsbgp4DeleteExtCommRtSafi,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsbgp4DeleteExtCommIpNetwork,
                                       INT4 i4Fsbgp4DeleteExtCommIpPrefixLen,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsbgp4DeleteExtCommVal,
                                       INT4
                                       i4SetValFsbgp4DeleteExtCommRowStatus)
{
    tExtCommProfile    *pExtCommProfile = NULL;
    tNetAddress         RtAddress;
    INT4                i4RetSts;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4RetSts = Bgp4LowGetRouteAddress (i4Fsbgp4DeleteExtCommRtAfi,
                                       i4Fsbgp4DeleteExtCommRtSafi,
                                       pFsbgp4DeleteExtCommIpNetwork,
                                       &RtAddress);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4DeleteExtCommIpPrefixLen;

    switch (i4SetValFsbgp4DeleteExtCommRowStatus)
    {
        case CREATE_AND_GO:
            i4RetSts =
                ExtCommCreateDeleteExtCommEntry (RtAddress,
                                                 pFsbgp4DeleteExtCommVal,
                                                 &pExtCommProfile);
            if (i4RetSts == EXT_COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create entry in "
                          "Ext-Comm Delete Table.\n");
                return SNMP_FAILURE;
            }
            pExtCommProfile->u1RowStatus = ACTIVE;
            break;

        case CREATE_AND_WAIT:
            i4RetSts =
                ExtCommCreateDeleteExtCommEntry (RtAddress,
                                                 pFsbgp4DeleteExtCommVal,
                                                 &pExtCommProfile);
            if (i4RetSts == EXT_COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create entry in "
                          "Ext-Comm Delete Table.\n");
                return SNMP_FAILURE;
            }
            pExtCommProfile->u1RowStatus = NOT_IN_SERVICE;
            break;

        case ACTIVE:
            ExtCommGetDeleteExtCommEntry (RtAddress,
                                          (UINT1 *) pFsbgp4DeleteExtCommVal->
                                          pu1_OctetList, &pExtCommProfile);
            if (pExtCommProfile == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching entry exist in "
                          "Ext-Comm Delete Table.\n");
                return SNMP_FAILURE;
            }
            pExtCommProfile->u1RowStatus = ACTIVE;
            break;

        case NOT_IN_SERVICE:
            ExtCommGetDeleteExtCommEntry (RtAddress,
                                          (UINT1 *) pFsbgp4DeleteExtCommVal->
                                          pu1_OctetList, &pExtCommProfile);

            if (pExtCommProfile == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching entry exist in "
                          "Ext-Comm Delete Table.\n");
                return SNMP_FAILURE;
            }
            pExtCommProfile->u1RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:
            i4RetSts =
                ExtCommDeleteDeleteExtCommEntry (RtAddress,
                                                 pFsbgp4DeleteExtCommVal,
                                                 &pExtCommProfile);
            break;

        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4mpeDeleteExtCommRowStatus,
                          u4SeqNum, TRUE, BgpLock, BgpUnLock, 6, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %s %i %s %i",
                      gpBgpCurrCxtNode->u4ContextId, i4Fsbgp4DeleteExtCommRtAfi,
                      i4Fsbgp4DeleteExtCommRtSafi,
                      pFsbgp4DeleteExtCommIpNetwork,
                      i4Fsbgp4DeleteExtCommIpPrefixLen, pFsbgp4DeleteExtCommVal,
                      i4SetValFsbgp4DeleteExtCommRowStatus));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4mpeDeleteExtCommRowStatus
 Input       :  The Indices
                Fsbgp4DeleteExtCommRtAfi
                Fsbgp4DeleteExtCommRtSafi
                Fsbgp4DeleteExtCommIpNetwork
                Fsbgp4DeleteExtCommIpPrefixLen
                Fsbgp4DeleteExtCommVal

                The Object 
                testValFsbgp4DeleteExtCommRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4mpeDeleteExtCommRowStatus (UINT4 *pu4ErrorCode,
                                          INT4 i4Fsbgp4DeleteExtCommRtAfi,
                                          INT4 i4Fsbgp4DeleteExtCommRtSafi,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsbgp4DeleteExtCommIpNetwork,
                                          INT4 i4Fsbgp4DeleteExtCommIpPrefixLen,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsbgp4DeleteExtCommVal,
                                          INT4
                                          i4TestValFsbgp4DeleteExtCommRowStatus)
{
    tExtCommProfile    *pExtCommProfile = NULL;
    tNetAddress         RtAddress;
    INT4                i4RetSts;
    INT4                i4DelStatus = EXT_COMM_FAILURE;
    UINT4               u4Context;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Local AS not configured..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }
#ifdef L3VPN
    if ((i4Fsbgp4DeleteExtCommRtSafi == BGP4_INET_SAFI_LABEL) ||
        (i4Fsbgp4DeleteExtCommRtSafi == BGP4_INET_SAFI_VPNV4_UNICAST))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#endif

    i4RetSts = Bgp4LowGetRouteAddress (i4Fsbgp4DeleteExtCommRtAfi,
                                       i4Fsbgp4DeleteExtCommRtSafi,
                                       pFsbgp4DeleteExtCommIpNetwork,
                                       &RtAddress);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4DeleteExtCommIpPrefixLen;
    i4RetSts = Bgp4ValidateExtCommRouteAddCommTableIndex (RtAddress,
                                                          pFsbgp4DeleteExtCommVal);
    if (i4RetSts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Prefix/Length/Ext-Comm Value.\n");
        return SNMP_FAILURE;
    }
    switch (i4TestValFsbgp4DeleteExtCommRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            i4DelStatus =
                ExtCommGetDeleteExtCommEntry (RtAddress,
                                              pFsbgp4DeleteExtCommVal->
                                              pu1_OctetList, &pExtCommProfile);
            if (i4DelStatus != EXT_COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Matching entry already exist "
                          "in Ext-Comm Delete Table.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
        case NOT_IN_SERVICE:
            i4DelStatus =
                ExtCommGetDeleteExtCommEntry (RtAddress,
                                              pFsbgp4DeleteExtCommVal->
                                              pu1_OctetList, &pExtCommProfile);
            if (i4DelStatus != EXT_COMM_SUCCESS)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Matching entry already exist in "
                          "Ext-Comm Delete Table.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if ((pExtCommProfile->u1RowStatus == ACTIVE) ||
                (pExtCommProfile->u1RowStatus == NOT_IN_SERVICE) ||
                (pExtCommProfile->u1RowStatus == NOT_READY))
            {
                return SNMP_SUCCESS;
            }
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        case DESTROY:
            return SNMP_SUCCESS;

        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4MpeExtCommRouteDeleteExtCommTable
 Input       :  The Indices
                Fsbgp4DeleteExtCommRtAfi
                Fsbgp4DeleteExtCommRtSafi
                Fsbgp4DeleteExtCommIpNetwork
                Fsbgp4DeleteExtCommIpPrefixLen
                Fsbgp4DeleteExtCommVal
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4MpeExtCommRouteDeleteExtCommTable (UINT4 *pu4ErrorCode,
                                                 tSnmpIndexList *
                                                 pSnmpIndexList,
                                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4MpeExtCommRouteExtCommSetStatusTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4MpeExtCommRouteExtCommSetStatusTable
 Input       :  The Indices
                Fsbgp4ExtCommSetStatusRtAfi
                Fsbgp4ExtCommSetStatusRtSafi
                Fsbgp4ExtCommSetStatusIpNetwork
                Fsbgp4ExtCommSetStatusIpPrefixLen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4MpeExtCommRouteExtCommSetStatusTable (INT4
                                                                    i4Fsbgp4ExtCommSetStatusRtAfi,
                                                                    INT4
                                                                    i4Fsbgp4ExtCommSetStatusRtSafi,
                                                                    tSNMP_OCTET_STRING_TYPE
                                                                    *
                                                                    pFsbgp4ExtCommSetStatusIpNetwork,
                                                                    INT4
                                                                    i4Fsbgp4ExtCommSetStatusIpPrefixLen)
{
    tNetAddress         RtAddress;
    INT4                i4Sts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4ExtCommSetStatusRtAfi,
                                    i4Fsbgp4ExtCommSetStatusRtSafi,
                                    pFsbgp4ExtCommSetStatusIpNetwork,
                                    &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4ExtCommSetStatusIpPrefixLen;
    i4Sts = Bgp4ValidateCommRouteCommSetStatusTableIndex (RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Prefix/Length Value.\n");
        CLI_SET_ERR (CLI_BGP4_INVALID_PREFIX_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable
 Input       :  The Indices
                Fsbgp4ExtCommSetStatusRtAfi
                Fsbgp4ExtCommSetStatusRtSafi
                Fsbgp4ExtCommSetStatusIpNetwork
                Fsbgp4ExtCommSetStatusIpPrefixLen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable (INT4
                                                            *pi4Fsbgp4ExtCommSetStatusRtAfi,
                                                            INT4
                                                            *pi4Fsbgp4ExtCommSetStatusRtSafi,
                                                            tSNMP_OCTET_STRING_TYPE
                                                            *
                                                            pFsbgp4ExtCommSetStatusIpNetwork,
                                                            INT4
                                                            *pi4Fsbgp4ExtCommSetStatusIpPrefixLen)
{
    tRouteExtCommSetStatus *pRouteExtCommSetStatus = NULL;
    tRouteExtCommSetStatus RouteExtCommSetStatus;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    MEMSET (&RouteExtCommSetStatus, 0, sizeof (tRouteExtCommSetStatus));
    RouteExtCommSetStatus.u4Context = u4Context;

    pRouteExtCommSetStatus = (tRouteExtCommSetStatus *)
        BGP4_RB_TREE_GET_NEXT (ROUTES_EXT_COMM_SET_STATUS_TBL,
                               (tBgp4RBElem *) (&RouteExtCommSetStatus), NULL);
    if ((pRouteExtCommSetStatus != NULL)
        && (pRouteExtCommSetStatus->u4Context == u4Context))
    {
        switch (EXT_COMM_RT_AFI_IN_ROUTE_SET_STATUS_STRUCT
                (pRouteExtCommSetStatus))
        {
            case BGP4_INET_AFI_IPV4:
            {
                UINT4               u4IpAddress;
                PTR_FETCH_4 (u4IpAddress,
                             EXT_COMM_RT_IP_PREFIX_IN_ROUTE_SET_STATUS_STRUCT
                             (pRouteExtCommSetStatus));
                PTR_ASSIGN4 (pFsbgp4ExtCommSetStatusIpNetwork->pu1_OctetList,
                             (u4IpAddress));
                pFsbgp4ExtCommSetStatusIpNetwork->i4_Length =
                    BGP4_IPV4_PREFIX_LEN;
                *pi4Fsbgp4ExtCommSetStatusRtAfi = BGP4_INET_AFI_IPV4;
                *pi4Fsbgp4ExtCommSetStatusRtSafi = BGP4_INET_SAFI_UNICAST;
                *pi4Fsbgp4ExtCommSetStatusIpPrefixLen =
                    EXT_COMM_RT_PREFIXLEN_IN_ROUTE_SET_STATUS_STRUCT
                    (pRouteExtCommSetStatus);
            }
                break;
#ifdef BGP4_IPV6_WANTED
            case BGP4_INET_AFI_IPV6:
                MEMCPY (pFsbgp4ExtCommSetStatusIpNetwork->pu1_OctetList,
                        EXT_COMM_RT_IP_PREFIX_IN_ROUTE_SET_STATUS_STRUCT
                        (pRouteExtCommSetStatus), BGP4_IPV6_PREFIX_LEN);
                pFsbgp4ExtCommSetStatusIpNetwork->i4_Length =
                    BGP4_IPV6_PREFIX_LEN;

                *pi4Fsbgp4ExtCommSetStatusRtAfi = BGP4_INET_AFI_IPV6;
                *pi4Fsbgp4ExtCommSetStatusRtSafi = BGP4_INET_SAFI_UNICAST;
                *pi4Fsbgp4ExtCommSetStatusIpPrefixLen =
                    EXT_COMM_RT_PREFIXLEN_IN_ROUTE_SET_STATUS_STRUCT
                    (pRouteExtCommSetStatus);
                break;
#endif
            default:
                return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable
 Input       :  The Indices
                Fsbgp4ExtCommSetStatusRtAfi
                nextFsbgp4ExtCommSetStatusRtAfi
                Fsbgp4ExtCommSetStatusRtSafi
                nextFsbgp4ExtCommSetStatusRtSafi
                Fsbgp4ExtCommSetStatusIpNetwork
                nextFsbgp4ExtCommSetStatusIpNetwork
                Fsbgp4ExtCommSetStatusIpPrefixLen
                nextFsbgp4ExtCommSetStatusIpPrefixLen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable (INT4
                                                           i4Fsbgp4ExtCommSetStatusRtAfi,
                                                           INT4
                                                           *pi4NextFsbgp4ExtCommSetStatusRtAfi,
                                                           INT4
                                                           i4Fsbgp4ExtCommSetStatusRtSafi,
                                                           INT4
                                                           *pi4NextFsbgp4ExtCommSetStatusRtSafi,
                                                           tSNMP_OCTET_STRING_TYPE
                                                           *
                                                           pFsbgp4ExtCommSetStatusIpNetwork,
                                                           tSNMP_OCTET_STRING_TYPE
                                                           *
                                                           pNextFsbgp4ExtCommSetStatusIpNetwork,
                                                           INT4
                                                           i4Fsbgp4ExtCommSetStatusIpPrefixLen,
                                                           INT4
                                                           *pi4NextFsbgp4ExtCommSetStatusIpPrefixLen)
{
    tRouteExtCommSetStatus *pRouteExtCommSetStatus = NULL;
    tNetAddress         RtAddress;
    INT4                i4Sts;
    tRouteExtCommSetStatus RouteExtCommSetStatus;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4ExtCommSetStatusRtAfi,
                                    i4Fsbgp4ExtCommSetStatusRtSafi,
                                    pFsbgp4ExtCommSetStatusIpNetwork,
                                    &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4ExtCommSetStatusIpPrefixLen;

    RouteExtCommSetStatus.u4Context = u4Context;

    Bgp4CopyNetAddressStruct (&(RouteExtCommSetStatus.RtNetAddress), RtAddress);
    pRouteExtCommSetStatus =
        BGP4_RB_TREE_GET_NEXT (ROUTES_EXT_COMM_SET_STATUS_TBL,
                               (tBgp4RBElem *) (&RouteExtCommSetStatus), NULL);
    if ((pRouteExtCommSetStatus != NULL)
        && (pRouteExtCommSetStatus->u4Context == u4Context))
    {

        switch (EXT_COMM_RT_AFI_IN_ROUTE_SET_STATUS_STRUCT
                (pRouteExtCommSetStatus))
        {
            case BGP4_INET_AFI_IPV4:
            {
                UINT4               u4IpAddress;
                PTR_FETCH_4 (u4IpAddress,
                             EXT_COMM_RT_IP_PREFIX_IN_ROUTE_SET_STATUS_STRUCT
                             (pRouteExtCommSetStatus));
                PTR_ASSIGN4 (pNextFsbgp4ExtCommSetStatusIpNetwork->
                             pu1_OctetList, (u4IpAddress));
                pNextFsbgp4ExtCommSetStatusIpNetwork->i4_Length =
                    BGP4_IPV4_PREFIX_LEN;
                *pi4NextFsbgp4ExtCommSetStatusRtAfi = BGP4_INET_AFI_IPV4;
                *pi4NextFsbgp4ExtCommSetStatusRtSafi = BGP4_INET_SAFI_UNICAST;
                *pi4NextFsbgp4ExtCommSetStatusIpPrefixLen =
                    EXT_COMM_RT_PREFIXLEN_IN_ROUTE_SET_STATUS_STRUCT
                    (pRouteExtCommSetStatus);
            }
                break;
#ifdef BGP4_IPV6_WANTED
            case BGP4_INET_AFI_IPV6:
                MEMCPY (pNextFsbgp4ExtCommSetStatusIpNetwork->
                        pu1_OctetList,
                        EXT_COMM_RT_IP_PREFIX_IN_ROUTE_SET_STATUS_STRUCT
                        (pRouteExtCommSetStatus), BGP4_IPV6_PREFIX_LEN);
                pNextFsbgp4ExtCommSetStatusIpNetwork->i4_Length =
                    BGP4_IPV6_PREFIX_LEN;
                *pi4NextFsbgp4ExtCommSetStatusRtAfi = BGP4_INET_AFI_IPV6;
                *pi4NextFsbgp4ExtCommSetStatusRtSafi = BGP4_INET_SAFI_UNICAST;
                *pi4NextFsbgp4ExtCommSetStatusIpPrefixLen =
                    EXT_COMM_RT_PREFIXLEN_IN_ROUTE_SET_STATUS_STRUCT
                    (pRouteExtCommSetStatus);
                break;
#endif
            default:
                return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4ExtCommSetStatus
 Input       :  The Indices
                Fsbgp4ExtCommSetStatusRtAfi
                Fsbgp4ExtCommSetStatusRtSafi
                Fsbgp4ExtCommSetStatusIpNetwork
                Fsbgp4ExtCommSetStatusIpPrefixLen

                The Object 
                retValFsbgp4ExtCommSetStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeExtCommSetStatus (INT4 i4Fsbgp4ExtCommSetStatusRtAfi,
                                 INT4 i4Fsbgp4ExtCommSetStatusRtSafi,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsbgp4ExtCommSetStatusIpNetwork,
                                 INT4 i4Fsbgp4ExtCommSetStatusIpPrefixLen,
                                 INT4 *pi4RetValFsbgp4ExtCommSetStatus)
{
    tRouteExtCommSetStatus *pRouteExtCommSetStatus = NULL;
    tNetAddress         RtAddress;
    INT4                i4Sts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4ExtCommSetStatusRtAfi,
                                    i4Fsbgp4ExtCommSetStatusRtSafi,
                                    pFsbgp4ExtCommSetStatusIpNetwork,
                                    &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4ExtCommSetStatusIpPrefixLen;

    ExtCommGetRouteExtCommSetStatusEntry (RtAddress, &pRouteExtCommSetStatus);

    if (pRouteExtCommSetStatus == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Ext-Comm Set Status entry exist.\n");
        return SNMP_FAILURE;
    }
    if ((pRouteExtCommSetStatus->i1ExtCommSetStatus > EXT_COMM_NONE) &&
        (pRouteExtCommSetStatus->i1ExtCommSetStatus <= EXT_COMM_MODIFY))
    {
        *pi4RetValFsbgp4ExtCommSetStatus =
            pRouteExtCommSetStatus->i1ExtCommSetStatus;
    }
    else
    {
        *pi4RetValFsbgp4ExtCommSetStatus = EXT_COMM_NONE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeExtCommSetStatusRowStatus
 Input       :  The Indices
                Fsbgp4ExtCommSetStatusRtAfi
                Fsbgp4ExtCommSetStatusRtSafi
                Fsbgp4ExtCommSetStatusIpNetwork
                Fsbgp4ExtCommSetStatusIpPrefixLen

                The Object 
                retValFsbgp4ExtCommSetStatusRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeExtCommSetStatusRowStatus (INT4 i4Fsbgp4ExtCommSetStatusRtAfi,
                                          INT4 i4Fsbgp4ExtCommSetStatusRtSafi,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsbgp4ExtCommSetStatusIpNetwork,
                                          INT4
                                          i4Fsbgp4ExtCommSetStatusIpPrefixLen,
                                          INT4
                                          *pi4RetValFsbgp4ExtCommSetStatusRowStatus)
{
    tRouteExtCommSetStatus *pRouteExtCommSetStatus = NULL;
    tNetAddress         RtAddress;
    INT4                i4Sts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4ExtCommSetStatusRtAfi,
                                    i4Fsbgp4ExtCommSetStatusRtSafi,
                                    pFsbgp4ExtCommSetStatusIpNetwork,
                                    &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4ExtCommSetStatusIpPrefixLen;

    ExtCommGetRouteExtCommSetStatusEntry (RtAddress, &pRouteExtCommSetStatus);

    if (pRouteExtCommSetStatus == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Ext-Comm Set Status entry exist.\n");
        return SNMP_FAILURE;
    }
    if ((pRouteExtCommSetStatus->u1RowStatus != ACTIVE) &&
        (pRouteExtCommSetStatus->u1RowStatus != NOT_IN_SERVICE))
    {
        *pi4RetValFsbgp4ExtCommSetStatusRowStatus = NOT_READY;
    }
    else
    {
        *pi4RetValFsbgp4ExtCommSetStatusRowStatus =
            pRouteExtCommSetStatus->u1RowStatus;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4mpeExtCommSetStatus
 Input       :  The Indices
                Fsbgp4ExtCommSetStatusRtAfi
                Fsbgp4ExtCommSetStatusRtSafi
                Fsbgp4ExtCommSetStatusIpNetwork
                Fsbgp4ExtCommSetStatusIpPrefixLen

                The Object 
                setValFsbgp4ExtCommSetStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4mpeExtCommSetStatus (INT4 i4Fsbgp4ExtCommSetStatusRtAfi,
                                 INT4 i4Fsbgp4ExtCommSetStatusRtSafi,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsbgp4ExtCommSetStatusIpNetwork,
                                 INT4 i4Fsbgp4ExtCommSetStatusIpPrefixLen,
                                 INT4 i4SetValFsbgp4ExtCommSetStatus)
{
    tRouteExtCommSetStatus *pRouteExtCommSetStatus = NULL;
    tNetAddress         RtAddress;
    INT4                i4Sts;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4ExtCommSetStatusRtAfi,
                                    i4Fsbgp4ExtCommSetStatusRtSafi,
                                    pFsbgp4ExtCommSetStatusIpNetwork,
                                    &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4ExtCommSetStatusIpPrefixLen;

    ExtCommGetRouteExtCommSetStatusEntry (RtAddress, &pRouteExtCommSetStatus);
    if (pRouteExtCommSetStatus == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Ext-Comm Set Status entry exist.\n");
        return SNMP_FAILURE;
    }
    pRouteExtCommSetStatus->i1ExtCommSetStatus =
        (INT1) i4SetValFsbgp4ExtCommSetStatus;
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4mpeExtCommSetStatus, u4SeqNum,
                          FALSE, BgpLock, BgpUnLock, 5, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %s %i %i",
                      gpBgpCurrCxtNode->u4ContextId,
                      i4Fsbgp4ExtCommSetStatusRtAfi,
                      i4Fsbgp4ExtCommSetStatusRtSafi,
                      pFsbgp4ExtCommSetStatusIpNetwork,
                      i4Fsbgp4ExtCommSetStatusIpPrefixLen,
                      i4SetValFsbgp4ExtCommSetStatus));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsbgp4mpeExtCommSetStatusRowStatus
 Input       :  The Indices
                Fsbgp4ExtCommSetStatusRtAfi
                Fsbgp4ExtCommSetStatusRtSafi
                Fsbgp4ExtCommSetStatusIpNetwork
                Fsbgp4ExtCommSetStatusIpPrefixLen

                The Object 
                setValFsbgp4ExtCommSetStatusRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4mpeExtCommSetStatusRowStatus (INT4 i4Fsbgp4ExtCommSetStatusRtAfi,
                                          INT4 i4Fsbgp4ExtCommSetStatusRtSafi,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsbgp4ExtCommSetStatusIpNetwork,
                                          INT4
                                          i4Fsbgp4ExtCommSetStatusIpPrefixLen,
                                          INT4
                                          i4SetValFsbgp4ExtCommSetStatusRowStatus)
{
    tRouteExtCommSetStatus *pRouteExtCommSetStatus = NULL;
    tNetAddress         RtAddress;
    UINT4               u4RtMemSts;
    UINT4               u4Ret;
    INT4                i4RetSts;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4RetSts = Bgp4LowGetRouteAddress (i4Fsbgp4ExtCommSetStatusRtAfi,
                                       i4Fsbgp4ExtCommSetStatusRtSafi,
                                       pFsbgp4ExtCommSetStatusIpNetwork,
                                       &RtAddress);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4ExtCommSetStatusIpPrefixLen;
    switch (i4SetValFsbgp4ExtCommSetStatusRowStatus)
    {
        case CREATE_AND_GO:
            i4RetSts =
                ExtCommCreateRouteExtCommSetStatusEntry (RtAddress,
                                                         &pRouteExtCommSetStatus);
            if (i4RetSts == EXT_COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create Ext-Comm Set "
                          "Status entry.\n");
                return SNMP_FAILURE;
            }
            pRouteExtCommSetStatus->u1RowStatus = ACTIVE;
            break;

        case CREATE_AND_WAIT:
            i4RetSts =
                ExtCommCreateRouteExtCommSetStatusEntry (RtAddress,
                                                         &pRouteExtCommSetStatus);
            if (i4RetSts == EXT_COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create Ext-Comm Set "
                          "Status entry.\n");
                return SNMP_FAILURE;
            }
            (*pRouteExtCommSetStatus).i1ExtCommSetStatus = EXT_COMM_MODIFY;
            (*pRouteExtCommSetStatus).u1RowStatus = NOT_IN_SERVICE;
            break;

        case ACTIVE:
            ExtCommGetRouteExtCommSetStatusEntry (RtAddress,
                                                  &pRouteExtCommSetStatus);
            if (pRouteExtCommSetStatus == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create Ext-Comm Set "
                          "Status entry.\n");
                return SNMP_FAILURE;
            }
            pRouteExtCommSetStatus->u1RowStatus = ACTIVE;
            break;

        case NOT_IN_SERVICE:
            ExtCommGetRouteExtCommSetStatusEntry (RtAddress,
                                                  &pRouteExtCommSetStatus);
            if (pRouteExtCommSetStatus == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create Ext-Comm Set "
                          "Status entry.\n");
                return SNMP_FAILURE;
            }
            pRouteExtCommSetStatus->u1RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:
            ExtCommGetRouteExtCommSetStatusEntry (RtAddress,
                                                  &pRouteExtCommSetStatus);
            if (pRouteExtCommSetStatus == NULL)
            {
                break;
            }

            u4Ret = BGP4_RB_TREE_REMOVE (ROUTES_EXT_COMM_SET_STATUS_TBL,
                                         (tBgp4RBElem *)
                                         pRouteExtCommSetStatus);
            if (u4Ret == RB_FAILURE)
            {
                return SNMP_FAILURE;
            }
            u4RtMemSts =
                EXT_COMM_ROUTES_EXT_COMM_SET_STATUS_ENTRY_FREE
                (pRouteExtCommSetStatus);
            if (u4RtMemSts == MEM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_BUFFER_TRC,BGP4_MOD_NAME,
                      "\tERROR - Memrelease failed.\n");
            }

            break;

        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status.\n");
            return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4mpeExtCommSetStatusRowStatus,
                          u4SeqNum, TRUE, BgpLock, BgpUnLock, 5, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %s %i %i",
                      gpBgpCurrCxtNode->u4ContextId,
                      i4Fsbgp4ExtCommSetStatusRtAfi,
                      i4Fsbgp4ExtCommSetStatusRtSafi,
                      pFsbgp4ExtCommSetStatusIpNetwork,
                      i4Fsbgp4ExtCommSetStatusIpPrefixLen,
                      i4SetValFsbgp4ExtCommSetStatusRowStatus));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4ExtCommSetStatus
 Input       :  The Indices
                Fsbgp4ExtCommSetStatusRtAfi
                Fsbgp4ExtCommSetStatusRtSafi
                Fsbgp4ExtCommSetStatusIpNetwork
                Fsbgp4ExtCommSetStatusIpPrefixLen

                The Object 
                testValFsbgp4ExtCommSetStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4mpeExtCommSetStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4Fsbgp4ExtCommSetStatusRtAfi,
                                    INT4 i4Fsbgp4ExtCommSetStatusRtSafi,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsbgp4ExtCommSetStatusIpNetwork,
                                    INT4 i4Fsbgp4ExtCommSetStatusIpPrefixLen,
                                    INT4 i4TestValFsbgp4ExtCommSetStatus)
{
    tNetAddress         RtAddress;
    INT4                i4Sts;
    UINT4               u4Context;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Local AS not configured..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }
#ifdef L3VPN
    if ((i4Fsbgp4ExtCommSetStatusRtSafi == BGP4_INET_SAFI_LABEL) ||
        (i4Fsbgp4ExtCommSetStatusRtSafi == BGP4_INET_SAFI_VPNV4_UNICAST))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#endif

    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4ExtCommSetStatusRtAfi,
                                    i4Fsbgp4ExtCommSetStatusRtSafi,
                                    pFsbgp4ExtCommSetStatusIpNetwork,
                                    &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4ExtCommSetStatusIpPrefixLen;
    i4Sts = Bgp4ValidateCommRouteCommSetStatusTableIndex (RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Input Prefix/Mask Value.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PREFIX_ERR);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4ExtCommSetStatus < EXT_COMM_SET) ||
        (i4TestValFsbgp4ExtCommSetStatus > EXT_COMM_MODIFY))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Ext-Comm Set Status action Value.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_ECOMM_SET_STATUS_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4mpeExtCommSetStatusRowStatus
 Input       :  The Indices
                Fsbgp4ExtCommSetStatusRtAfi
                Fsbgp4ExtCommSetStatusRtSafi
                Fsbgp4ExtCommSetStatusIpNetwork
                Fsbgp4ExtCommSetStatusIpPrefixLen

                The Object 
                testValFsbgp4ExtCommSetStatusRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4mpeExtCommSetStatusRowStatus (UINT4 *pu4ErrorCode,
                                             INT4 i4Fsbgp4ExtCommSetStatusRtAfi,
                                             INT4
                                             i4Fsbgp4ExtCommSetStatusRtSafi,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsbgp4ExtCommSetStatusIpNetwork,
                                             INT4
                                             i4Fsbgp4ExtCommSetStatusIpPrefixLen,
                                             INT4
                                             i4TestValFsbgp4ExtCommSetStatusRowStatus)
{
    tRouteExtCommSetStatus *pRouteExtCommSetStatus = NULL;
    tNetAddress         RtAddress;
    INT4                i4Sts;
    INT4                i4GetStatus = EXT_COMM_FAILURE;
    UINT4               u4Context = 0;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Local AS not configured..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }
#ifdef L3VPN
    if ((i4Fsbgp4ExtCommSetStatusRtSafi == BGP4_INET_SAFI_LABEL) ||
        (i4Fsbgp4ExtCommSetStatusRtSafi == BGP4_INET_SAFI_VPNV4_UNICAST))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#endif

    i4Sts = Bgp4LowGetRouteAddress (i4Fsbgp4ExtCommSetStatusRtAfi,
                                    i4Fsbgp4ExtCommSetStatusRtSafi,
                                    pFsbgp4ExtCommSetStatusIpNetwork,
                                    &RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RtAddress) =
        (UINT2) i4Fsbgp4ExtCommSetStatusIpPrefixLen;
    i4Sts = Bgp4ValidateCommRouteCommSetStatusTableIndex (RtAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    switch (i4TestValFsbgp4ExtCommSetStatusRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            i4GetStatus =
                ExtCommGetRouteExtCommSetStatusEntry
                (RtAddress, &pRouteExtCommSetStatus);
            if (i4GetStatus != EXT_COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Matching Ext-Comm Set Status entry "
                          "exist.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
        case NOT_IN_SERVICE:
            i4GetStatus =
                ExtCommGetRouteExtCommSetStatusEntry
                (RtAddress, &pRouteExtCommSetStatus);
            if (i4GetStatus != EXT_COMM_SUCCESS)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Ext-Comm Set Status "
                          "entry exist.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if ((pRouteExtCommSetStatus->u1RowStatus == ACTIVE) ||
                (pRouteExtCommSetStatus->u1RowStatus == NOT_IN_SERVICE) ||
                (pRouteExtCommSetStatus->u1RowStatus == NOT_READY))
            {
                return SNMP_SUCCESS;
            }
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        case DESTROY:
            ExtCommGetRouteExtCommSetStatusEntry
                (RtAddress, &pRouteExtCommSetStatus);
            return SNMP_SUCCESS;
        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            break;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - Invalid Row Status Value.\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4MpeExtCommRouteExtCommSetStatusTable
 Input       :  The Indices
                Fsbgp4ExtCommSetStatusRtAfi
                Fsbgp4ExtCommSetStatusRtSafi
                Fsbgp4ExtCommSetStatusIpNetwork
                Fsbgp4ExtCommSetStatusIpPrefixLen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4MpeExtCommRouteExtCommSetStatusTable (UINT4 *pu4ErrorCode,
                                                    tSnmpIndexList *
                                                    pSnmpIndexList,
                                                    tSNMP_VAR_BIND *
                                                    pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4ExtCommInFilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4ExtCommInFilterTable
 Input       :  The Indices
                Fsbgp4ExtCommInFilterCommVal
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4ExtCommInFilterTable (tSNMP_OCTET_STRING_TYPE *
                                                    pFsbgp4ExtCommInFilterCommVal)
{
    INT4                i4RetSts;
    UINT2               u2ExtCommType;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    PTR_FETCH2 (u2ExtCommType, (pFsbgp4ExtCommInFilterCommVal->pu1_OctetList));
    i4RetSts = ExtCommValidateExtCommunityType (u2ExtCommType);
    if (i4RetSts == EXT_COMM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Ext-Comm Value.\n");
        CLI_SET_ERR (CLI_BGP4_ECOMM_VALUE_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4ExtCommInFilterTable
 Input       :  The Indices
                Fsbgp4ExtCommInFilterCommVal
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4ExtCommInFilterTable (tSNMP_OCTET_STRING_TYPE *
                                            pFsbgp4ExtCommInFilterCommVal)
{
    tExtCommFilterInfo *pExtCommFilterInfo = NULL;

    tExtCommFilterInfo  ExtCommFilterInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    MEMSET (&ExtCommFilterInfo, 0, sizeof (tExtCommFilterInfo));
    ExtCommFilterInfo.u4Context = u4Context;

    pExtCommFilterInfo = (tExtCommFilterInfo *)
        BGP4_RB_TREE_GET_NEXT (EXT_COMM_INPUT_FILTER_TBL,
                               (tBgp4RBElem *) (&ExtCommFilterInfo), NULL);
    if ((pExtCommFilterInfo != NULL)
        && (pExtCommFilterInfo->u4Context == u4Context))
    {
        MEMCPY (pFsbgp4ExtCommInFilterCommVal->pu1_OctetList,
                (UINT1 *) pExtCommFilterInfo->au1ExtComm, EXT_COMM_VALUE_LEN);
        pFsbgp4ExtCommInFilterCommVal->i4_Length = EXT_COMM_VALUE_LEN;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4ExtCommInFilterTable
 Input       :  The Indices
                Fsbgp4ExtCommInFilterCommVal
                nextFsbgp4ExtCommInFilterCommVal
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4ExtCommInFilterTable (tSNMP_OCTET_STRING_TYPE *
                                           pFsbgp4ExtCommInFilterCommVal,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pNextFsbgp4ExtCommInFilterCommVal)
{
    tExtCommFilterInfo *pExtCommFilterInfo = NULL;
    tExtCommFilterInfo  ExtCommFilterInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    MEMCPY (ExtCommFilterInfo.au1ExtComm,
            pFsbgp4ExtCommInFilterCommVal->pu1_OctetList, EXT_COMM_VALUE_LEN);

    ExtCommFilterInfo.u4Context = u4Context;

    pExtCommFilterInfo =
        BGP4_RB_TREE_GET_NEXT (EXT_COMM_INPUT_FILTER_TBL,
                               (tBgp4RBElem *) (&ExtCommFilterInfo), NULL);
    if ((pExtCommFilterInfo != NULL)
        && (pExtCommFilterInfo->u4Context == u4Context))
    {
        MEMCPY (pNextFsbgp4ExtCommInFilterCommVal->pu1_OctetList,
                (UINT1 *) pExtCommFilterInfo->au1ExtComm, EXT_COMM_VALUE_LEN);
        pNextFsbgp4ExtCommInFilterCommVal->i4_Length = EXT_COMM_VALUE_LEN;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4ExtCommIncomingFilterStatus
 Input       :  The Indices
                Fsbgp4ExtCommInFilterCommVal

                The Object 
                retValFsbgp4ExtCommIncomingFilterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4ExtCommIncomingFilterStatus (tSNMP_OCTET_STRING_TYPE *
                                         pFsbgp4ExtCommInFilterCommVal,
                                         INT4
                                         *pi4RetValFsbgp4ExtCommIncomingFilterStatus)
{
    tExtCommFilterInfo *pExtCommFilterInfo = NULL;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    ExtCommGetInputFilterEntry (pFsbgp4ExtCommInFilterCommVal->pu1_OctetList,
                                &pExtCommFilterInfo);

    if (pExtCommFilterInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Ext-Comm Incoming Filter "
                  "entry exist.\n");
        return SNMP_FAILURE;
    }

    if ((pExtCommFilterInfo->i1FilterPolicy == EXT_COMM_ACCEPT) ||
        (pExtCommFilterInfo->i1FilterPolicy == EXT_COMM_DENY))
    {
        *pi4RetValFsbgp4ExtCommIncomingFilterStatus =
            pExtCommFilterInfo->i1FilterPolicy + BGP4_INCREMENT_BY_ONE;
    }
    else
    {
        *pi4RetValFsbgp4ExtCommIncomingFilterStatus = EXT_COMM_NONE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4ExtCommInFilterRowStatus
 Input       :  The Indices
                Fsbgp4ExtCommInFilterCommVal

                The Object 
                retValFsbgp4ExtCommInFilterRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4ExtCommInFilterRowStatus (tSNMP_OCTET_STRING_TYPE *
                                      pFsbgp4ExtCommInFilterCommVal,
                                      INT4
                                      *pi4RetValFsbgp4ExtCommInFilterRowStatus)
{
    tExtCommFilterInfo *pExtCommFilterInfo = NULL;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    ExtCommGetInputFilterEntry (pFsbgp4ExtCommInFilterCommVal->pu1_OctetList,
                                &pExtCommFilterInfo);

    if (pExtCommFilterInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Ext-Comm Incoming Filter "
                  "entry exist.\n");
        return SNMP_FAILURE;
    }

    if ((pExtCommFilterInfo->u1RowStatus != ACTIVE) &&
        (pExtCommFilterInfo->u1RowStatus != NOT_IN_SERVICE))
    {
        *pi4RetValFsbgp4ExtCommInFilterRowStatus = NOT_READY;
    }
    else
    {
        *pi4RetValFsbgp4ExtCommInFilterRowStatus =
            pExtCommFilterInfo->u1RowStatus;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4ExtCommIncomingFilterStatus
 Input       :  The Indices
                Fsbgp4ExtCommInFilterCommVal

                The Object 
                setValFsbgp4ExtCommIncomingFilterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4ExtCommIncomingFilterStatus (tSNMP_OCTET_STRING_TYPE *
                                         pFsbgp4ExtCommInFilterCommVal,
                                         INT4
                                         i4SetValFsbgp4ExtCommIncomingFilterStatus)
{
    tExtCommFilterInfo *pExtCommFilterInfo = NULL;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    ExtCommGetInputFilterEntry (pFsbgp4ExtCommInFilterCommVal->pu1_OctetList,
                                &pExtCommFilterInfo);

    if (pExtCommFilterInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Ext-Comm Incoming Filter "
                  "entry exist.\n");
        return SNMP_FAILURE;
    }
    pExtCommFilterInfo->i1FilterPolicy = (INT1)
        (i4SetValFsbgp4ExtCommIncomingFilterStatus - BGP4_DECREMENT_BY_ONE);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4ExtCommIncomingFilterStatus,
                          u4SeqNum, FALSE, BgpLock, BgpUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i", gpBgpCurrCxtNode->u4ContextId,
                      pFsbgp4ExtCommInFilterCommVal,
                      i4SetValFsbgp4ExtCommIncomingFilterStatus));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsbgp4ExtCommInFilterRowStatus
 Input       :  The Indices
                Fsbgp4ExtCommInFilterCommVal

                The Object 
                setValFsbgp4ExtCommInFilterRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4ExtCommInFilterRowStatus (tSNMP_OCTET_STRING_TYPE *
                                      pFsbgp4ExtCommInFilterCommVal,
                                      INT4
                                      i4SetValFsbgp4ExtCommInFilterRowStatus)
{
    tExtCommFilterInfo *pExtCommFilterInfo = NULL;
    INT4                i4RetSts;
    UINT4               u4Ret;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    switch (i4SetValFsbgp4ExtCommInFilterRowStatus)
    {
        case CREATE_AND_GO:
            i4RetSts =
                ExtCommCreateInputFilterEntry (pFsbgp4ExtCommInFilterCommVal->
                                               pu1_OctetList,
                                               &pExtCommFilterInfo);
            if (i4RetSts == EXT_COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create Ext-Comm "
                          "Incoming Filter entry.\n");
                return SNMP_FAILURE;
            }
            pExtCommFilterInfo->u1RowStatus = ACTIVE;
#ifdef VPLSADS_WANTED
            pExtCommFilterInfo->u1CommOwner = EXT_COMM_OWNER_CLI;
#endif
            break;
        case CREATE_AND_WAIT:
            i4RetSts =
                ExtCommCreateInputFilterEntry (pFsbgp4ExtCommInFilterCommVal->
                                               pu1_OctetList,
                                               &pExtCommFilterInfo);
            if (i4RetSts == EXT_COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create Ext-Comm Incoming "
                          "Filter entry.\n");
                return SNMP_FAILURE;
            }
            pExtCommFilterInfo->u1RowStatus = NOT_IN_SERVICE;
#ifdef VPLSADS_WANTED
            pExtCommFilterInfo->u1CommOwner = EXT_COMM_OWNER_CLI;
#endif
            break;

        case ACTIVE:
            ExtCommGetInputFilterEntry (pFsbgp4ExtCommInFilterCommVal->
                                        pu1_OctetList, &pExtCommFilterInfo);

            if (pExtCommFilterInfo == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Ext-Comm Incoming "
                          "Filter entry exist.\n");
                return SNMP_FAILURE;
            }
            pExtCommFilterInfo->u1RowStatus = ACTIVE;
            break;

        case NOT_IN_SERVICE:
            ExtCommGetInputFilterEntry (pFsbgp4ExtCommInFilterCommVal->
                                        pu1_OctetList, &pExtCommFilterInfo);

            if (pExtCommFilterInfo == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Ext-Comm Incoming Filter "
                          "entry exist.\n");
                return SNMP_FAILURE;
            }
            pExtCommFilterInfo->u1RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:
            ExtCommGetInputFilterEntry (pFsbgp4ExtCommInFilterCommVal->
                                        pu1_OctetList, &pExtCommFilterInfo);

            if (pExtCommFilterInfo == NULL)
            {
                return SNMP_SUCCESS;
            }
            u4Ret = BGP4_RB_TREE_REMOVE (EXT_COMM_INPUT_FILTER_TBL,
                                         (tBgp4RBElem *) pExtCommFilterInfo);
            if (u4Ret == RB_FAILURE)
            {
                return SNMP_FAILURE;
            }
            EXT_COMM_INPUT_FILTER_ENTRY_FREE (pExtCommFilterInfo);
            break;

        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4ExtCommInFilterRowStatus,
                          u4SeqNum, TRUE, BgpLock, BgpUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i", gpBgpCurrCxtNode->u4ContextId,
                      pFsbgp4ExtCommInFilterCommVal,
                      i4SetValFsbgp4ExtCommInFilterRowStatus));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4ExtCommIncomingFilterStatus
 Input       :  The Indices
                Fsbgp4ExtCommInFilterCommVal

                The Object 
                testValFsbgp4ExtCommIncomingFilterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4ExtCommIncomingFilterStatus (UINT4 *pu4ErrorCode,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsbgp4ExtCommInFilterCommVal,
                                            INT4
                                            i4TestValFsbgp4ExtCommIncomingFilterStatus)
{
    INT4                i4RetSts;
    UINT2               u2ExtCommType;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    PTR_FETCH2 (u2ExtCommType, (pFsbgp4ExtCommInFilterCommVal->pu1_OctetList));
    i4RetSts = ExtCommValidateExtCommunityType (u2ExtCommType);
    if (i4RetSts == EXT_COMM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Ext-Comm Value.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_ECOMM_VALUE_ERR);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4ExtCommIncomingFilterStatus == EXT_COMM_ACCEPT +
         BGP4_INCREMENT_BY_ONE) ||
        (i4TestValFsbgp4ExtCommIncomingFilterStatus == EXT_COMM_DENY +
         BGP4_INCREMENT_BY_ONE))
    {
        return SNMP_SUCCESS;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - Invalid Ext-Comm Incoming Filter Status Value.\n");
    CLI_SET_ERR (CLI_BGP4_ECOMM_IN_FLT_STATUS_ERR);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4ExtCommInFilterRowStatus
 Input       :  The Indices
                Fsbgp4ExtCommInFilterCommVal

                The Object 
                testValFsbgp4ExtCommInFilterRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4ExtCommInFilterRowStatus (UINT4 *pu4ErrorCode,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsbgp4ExtCommInFilterCommVal,
                                         INT4
                                         i4TestValFsbgp4ExtCommInFilterRowStatus)
{
    tExtCommFilterInfo *pExtCommFilterInfo = NULL;
    INT4                i4RetSts;
    INT4                i4GetInpFilterStatus = EXT_COMM_FAILURE;
    UINT2               u2ExtCommType;
    UINT4               u4BgpMaxInFilterEntries = 0;
    UINT4               u4BgpCurInFilterEntries = 0;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    PTR_FETCH2 (u2ExtCommType, (pFsbgp4ExtCommInFilterCommVal->pu1_OctetList));
    i4RetSts = ExtCommValidateExtCommunityType (u2ExtCommType);
    if (i4RetSts == EXT_COMM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Ext-Comm Value.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_ECOMM_VALUE_ERR);
        return SNMP_FAILURE;
    }

    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Local AS not configured..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }
    switch (i4TestValFsbgp4ExtCommInFilterRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            i4GetInpFilterStatus =
                ExtCommGetInputFilterEntry (pFsbgp4ExtCommInFilterCommVal->
                                            pu1_OctetList, &pExtCommFilterInfo);
            if (i4GetInpFilterStatus != EXT_COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Matching Ext-Comm Incoming Filter "
                          "entry exist.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_BGP4_ECOMM_ENTRY_EXISTS_ERR);
                return SNMP_FAILURE;
            }
            u4BgpMaxInFilterEntries =
                FsBGPSizingParams[MAX_BGP_EXT_COMM_IN_FILTER_INFOS_SIZING_ID].
                u4PreAllocatedUnits;
            BGP4_RB_TREE_GET_COUNT (EXT_COMM_INPUT_FILTER_TBL,
                                    &u4BgpCurInFilterEntries);
            if (u4BgpCurInFilterEntries >= u4BgpMaxInFilterEntries)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Max entries created.\n");
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_BGP4_EXCEED_MAX_ENTRIES);
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
        case NOT_IN_SERVICE:
            i4GetInpFilterStatus =
                ExtCommGetInputFilterEntry (pFsbgp4ExtCommInFilterCommVal->
                                            pu1_OctetList, &pExtCommFilterInfo);
            if (i4GetInpFilterStatus != EXT_COMM_SUCCESS)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Ext-Comm Incoming Filter "
                          "entry exist.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if ((pExtCommFilterInfo->u1RowStatus == ACTIVE) ||
                (pExtCommFilterInfo->u1RowStatus == NOT_IN_SERVICE) ||
                (pExtCommFilterInfo->u1RowStatus == NOT_READY))
            {
                return SNMP_SUCCESS;
            }
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        case DESTROY:
            return SNMP_SUCCESS;
        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4ExtCommInFilterTable
 Input       :  The Indices
                Fsbgp4ExtCommInFilterCommVal
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4ExtCommInFilterTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4ExtCommOutFilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4ExtCommOutFilterTable
 Input       :  The Indices
                Fsbgp4ExtCommOutFilterCommVal
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4ExtCommOutFilterTable (tSNMP_OCTET_STRING_TYPE *
                                                     pFsbgp4ExtCommOutFilterCommVal)
{
    INT4                i4RetSts;
    UINT2               u2ExtCommType;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    PTR_FETCH2 (u2ExtCommType, (pFsbgp4ExtCommOutFilterCommVal->pu1_OctetList));
    i4RetSts = ExtCommValidateExtCommunityType (u2ExtCommType);
    if (i4RetSts == EXT_COMM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Ext-Comm Value.\n");
        CLI_SET_ERR (CLI_BGP4_ECOMM_VALUE_ERR);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4ExtCommOutFilterTable
 Input       :  The Indices
                Fsbgp4ExtCommOutFilterCommVal
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4ExtCommOutFilterTable (tSNMP_OCTET_STRING_TYPE *
                                             pFsbgp4ExtCommOutFilterCommVal)
{
    tExtCommFilterInfo *pExtCommFilterInfo = NULL;
    tExtCommFilterInfo  ExtCommFilterInfo;
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    MEMSET (&ExtCommFilterInfo, 0, sizeof (tExtCommFilterInfo));
    ExtCommFilterInfo.u4Context = u4Context;

    pExtCommFilterInfo = (tExtCommFilterInfo *)
        BGP4_RB_TREE_GET_NEXT (EXT_COMM_OUTPUT_FILTER_TBL,
                               (tBgp4RBElem *) (&ExtCommFilterInfo), NULL);
    if ((pExtCommFilterInfo != NULL)
        && (pExtCommFilterInfo->u4Context == u4Context))
    {
        MEMCPY (pFsbgp4ExtCommOutFilterCommVal->pu1_OctetList,
                (UINT1 *) pExtCommFilterInfo->au1ExtComm, EXT_COMM_VALUE_LEN);
        pFsbgp4ExtCommOutFilterCommVal->i4_Length = EXT_COMM_VALUE_LEN;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4ExtCommOutFilterTable
 Input       :  The Indices
                Fsbgp4ExtCommOutFilterCommVal
                nextFsbgp4ExtCommOutFilterCommVal
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4ExtCommOutFilterTable (tSNMP_OCTET_STRING_TYPE *
                                            pFsbgp4ExtCommOutFilterCommVal,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pNextFsbgp4ExtCommOutFilterCommVal)
{
    tExtCommFilterInfo *pExtCommFilterInfo = NULL;
    tExtCommFilterInfo  ExtCommFilterInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    MEMCPY (ExtCommFilterInfo.au1ExtComm,
            pFsbgp4ExtCommOutFilterCommVal->pu1_OctetList, EXT_COMM_VALUE_LEN);

    ExtCommFilterInfo.u4Context = u4Context;

    pExtCommFilterInfo =
        BGP4_RB_TREE_GET_NEXT (EXT_COMM_OUTPUT_FILTER_TBL,
                               (tBgp4RBElem *) (&ExtCommFilterInfo), NULL);
    if ((pExtCommFilterInfo != NULL)
        && (pExtCommFilterInfo->u4Context == u4Context))
    {
        MEMCPY (pNextFsbgp4ExtCommOutFilterCommVal->pu1_OctetList,
                (UINT1 *) pExtCommFilterInfo->au1ExtComm, EXT_COMM_VALUE_LEN);
        pNextFsbgp4ExtCommOutFilterCommVal->i4_Length = EXT_COMM_VALUE_LEN;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4ExtCommOutgoingFilterStatus
 Input       :  The Indices
                Fsbgp4ExtCommOutFilterCommVal

                The Object 
                retValFsbgp4ExtCommOutgoingFilterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4ExtCommOutgoingFilterStatus (tSNMP_OCTET_STRING_TYPE *
                                         pFsbgp4ExtCommOutFilterCommVal,
                                         INT4
                                         *pi4RetValFsbgp4ExtCommOutgoingFilterStatus)
{
    tExtCommFilterInfo *pExtCommFilterInfo = NULL;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    ExtCommGetOutputFilterEntry (pFsbgp4ExtCommOutFilterCommVal->pu1_OctetList,
                                 &pExtCommFilterInfo);

    if (pExtCommFilterInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Ext-Comm Outgoing Filter "
                  "entry exist.\n");
        return SNMP_FAILURE;
    }

    if ((pExtCommFilterInfo->i1FilterPolicy == EXT_COMM_ADVERTISE) ||
        (pExtCommFilterInfo->i1FilterPolicy == EXT_COMM_FILTER))
    {
        *pi4RetValFsbgp4ExtCommOutgoingFilterStatus =
            pExtCommFilterInfo->i1FilterPolicy + BGP4_INCREMENT_BY_ONE;
    }
    else
    {
        *pi4RetValFsbgp4ExtCommOutgoingFilterStatus = EXT_COMM_NONE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4ExtCommOutFilterRowStatus
 Input       :  The Indices
                Fsbgp4ExtCommOutFilterCommVal

                The Object 
                retValFsbgp4ExtCommOutFilterRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4ExtCommOutFilterRowStatus (tSNMP_OCTET_STRING_TYPE *
                                       pFsbgp4ExtCommOutFilterCommVal,
                                       INT4
                                       *pi4RetValFsbgp4ExtCommOutFilterRowStatus)
{
    tExtCommFilterInfo *pExtCommFilterInfo = NULL;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    ExtCommGetOutputFilterEntry (pFsbgp4ExtCommOutFilterCommVal->pu1_OctetList,
                                 &pExtCommFilterInfo);

    if (pExtCommFilterInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Ext-Comm Outgoing Filter "
                  "entry exist.\n");
        return SNMP_FAILURE;
    }

    if ((pExtCommFilterInfo->u1RowStatus != ACTIVE) &&
        (pExtCommFilterInfo->u1RowStatus != NOT_IN_SERVICE))
    {
        *pi4RetValFsbgp4ExtCommOutFilterRowStatus = NOT_READY;
    }
    else
    {
        *pi4RetValFsbgp4ExtCommOutFilterRowStatus =
            pExtCommFilterInfo->u1RowStatus;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4ExtCommOutgoingFilterStatus
 Input       :  The Indices
                Fsbgp4ExtCommOutFilterCommVal

                The Object 
                setValFsbgp4ExtCommOutgoingFilterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4ExtCommOutgoingFilterStatus (tSNMP_OCTET_STRING_TYPE *
                                         pFsbgp4ExtCommOutFilterCommVal,
                                         INT4
                                         i4SetValFsbgp4ExtCommOutgoingFilterStatus)
{
    tExtCommFilterInfo *pExtCommFilterInfo = NULL;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    ExtCommGetOutputFilterEntry (pFsbgp4ExtCommOutFilterCommVal->pu1_OctetList,
                                 &pExtCommFilterInfo);

    if (pExtCommFilterInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Ext-Comm Outgoing Filter "
                  "entry exist.\n");
        return SNMP_FAILURE;
    }
    pExtCommFilterInfo->i1FilterPolicy = (INT1)
        (i4SetValFsbgp4ExtCommOutgoingFilterStatus - BGP4_DECREMENT_BY_ONE);

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4ExtCommOutgoingFilterStatus,
                          u4SeqNum, FALSE, BgpLock, BgpUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i", gpBgpCurrCxtNode->u4ContextId,
                      pFsbgp4ExtCommOutFilterCommVal,
                      i4SetValFsbgp4ExtCommOutgoingFilterStatus));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsbgp4ExtCommOutFilterRowStatus
 Input       :  The Indices
                Fsbgp4ExtCommOutFilterCommVal

                The Object 
                setValFsbgp4ExtCommOutFilterRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4ExtCommOutFilterRowStatus (tSNMP_OCTET_STRING_TYPE *
                                       pFsbgp4ExtCommOutFilterCommVal,
                                       INT4
                                       i4SetValFsbgp4ExtCommOutFilterRowStatus)
{
    tExtCommFilterInfo *pExtCommFilterInfo = NULL;
    INT4                i4RetSts;
    UINT4               u4Ret;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    switch (i4SetValFsbgp4ExtCommOutFilterRowStatus)
    {
        case CREATE_AND_GO:
            i4RetSts =
                ExtCommCreateOutputFilterEntry (pFsbgp4ExtCommOutFilterCommVal->
                                                pu1_OctetList,
                                                &pExtCommFilterInfo);
            if (i4RetSts == EXT_COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to Create Ext-Comm "
                          "Outgoing Filter entry.\n");
                return SNMP_FAILURE;
            }
            pExtCommFilterInfo->u1RowStatus = ACTIVE;
#ifdef VPLSADS_WANTED
            pExtCommFilterInfo->u1CommOwner = EXT_COMM_OWNER_CLI;
#endif
            break;
        case CREATE_AND_WAIT:
            i4RetSts =
                ExtCommCreateOutputFilterEntry (pFsbgp4ExtCommOutFilterCommVal->
                                                pu1_OctetList,
                                                &pExtCommFilterInfo);
            if (i4RetSts == EXT_COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to create Ext-Comm Outgoing "
                          "Filter entry.\n");
                return SNMP_FAILURE;
            }
            pExtCommFilterInfo->u1RowStatus = NOT_IN_SERVICE;
#ifdef VPLSADS_WANTED
            pExtCommFilterInfo->u1CommOwner = EXT_COMM_OWNER_CLI;
#endif
            break;

        case ACTIVE:
            ExtCommGetOutputFilterEntry (pFsbgp4ExtCommOutFilterCommVal->
                                         pu1_OctetList, &pExtCommFilterInfo);
            if (pExtCommFilterInfo == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Ext-Comm Outgoing Filter "
                          "entry exist.\n");
                return SNMP_FAILURE;
            }
            pExtCommFilterInfo->u1RowStatus = ACTIVE;
            break;

        case NOT_IN_SERVICE:
            ExtCommGetOutputFilterEntry (pFsbgp4ExtCommOutFilterCommVal->
                                         pu1_OctetList, &pExtCommFilterInfo);
            if (pExtCommFilterInfo == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Ext-Comm Outgoing Filter "
                          "entry exist.\n");
                return SNMP_FAILURE;
            }
            pExtCommFilterInfo->u1RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:
            ExtCommGetOutputFilterEntry (pFsbgp4ExtCommOutFilterCommVal->
                                         pu1_OctetList, &pExtCommFilterInfo);
            if (pExtCommFilterInfo == NULL)
            {
                break;
            }
            u4Ret = BGP4_RB_TREE_REMOVE (EXT_COMM_OUTPUT_FILTER_TBL,
                                         (tBgp4RBElem *) pExtCommFilterInfo);
            if (u4Ret == RB_FAILURE)
            {
                return SNMP_FAILURE;
            }
            EXT_COMM_OUTPUT_FILTER_ENTRY_FREE (pExtCommFilterInfo);
            break;

        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4ExtCommOutFilterRowStatus,
                          u4SeqNum, TRUE, BgpLock, BgpUnLock, 2, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i", gpBgpCurrCxtNode->u4ContextId,
                      pFsbgp4ExtCommOutFilterCommVal,
                      i4SetValFsbgp4ExtCommOutFilterRowStatus));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4ExtCommOutgoingFilterStatus
 Input       :  The Indices
                Fsbgp4ExtCommOutFilterCommVal

                The Object 
                testValFsbgp4ExtCommOutgoingFilterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4ExtCommOutgoingFilterStatus (UINT4 *pu4ErrorCode,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsbgp4ExtCommOutFilterCommVal,
                                            INT4
                                            i4TestValFsbgp4ExtCommOutgoingFilterStatus)
{
    INT4                i4RetSts;
    UINT2               u2ExtCommType;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }
    PTR_FETCH2 (u2ExtCommType, (pFsbgp4ExtCommOutFilterCommVal->pu1_OctetList));
    i4RetSts = ExtCommValidateExtCommunityType (u2ExtCommType);
    if (i4RetSts == EXT_COMM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Ext-Comm Value.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_ECOMM_VALUE_ERR);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4ExtCommOutgoingFilterStatus == EXT_COMM_ADVERTISE +
         BGP4_INCREMENT_BY_ONE)
        || (i4TestValFsbgp4ExtCommOutgoingFilterStatus == EXT_COMM_FILTER +
            BGP4_INCREMENT_BY_ONE))
    {
        return SNMP_SUCCESS;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - Invalid Ext-Comm Outgoing Filter Status Value.\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_BGP4_ECOMM_OUT_FLT_STATUS_ERR);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4ExtCommOutFilterRowStatus
 Input       :  The Indices
                Fsbgp4ExtCommOutFilterCommVal

                The Object 
                testValFsbgp4ExtCommOutFilterRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4ExtCommOutFilterRowStatus (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsbgp4ExtCommOutFilterCommVal,
                                          INT4
                                          i4TestValFsbgp4ExtCommOutFilterRowStatus)
{
    tExtCommFilterInfo *pExtCommFilterInfo = NULL;
    INT4                i4RetSts;
    INT4                i4GetOutFilterStatus = EXT_COMM_FAILURE;
    UINT2               u2ExtCommType;
    UINT4               u4BgpMaxOutFilterEntries = 0;
    UINT4               u4BgpCurOutFilterEntries = 0;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_TASK_NOT_INIT_ERR);
        return SNMP_FAILURE;
    }

    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Local AS not configured..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return SNMP_FAILURE;
    }

    PTR_FETCH2 (u2ExtCommType, (pFsbgp4ExtCommOutFilterCommVal->pu1_OctetList));
    i4RetSts = ExtCommValidateExtCommunityType (u2ExtCommType);
    if (i4RetSts == EXT_COMM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Ext-Comm Value.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_BGP4_ECOMM_VALUE_ERR);
        return SNMP_FAILURE;
    }

    switch (i4TestValFsbgp4ExtCommOutFilterRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            i4GetOutFilterStatus =
                ExtCommGetOutputFilterEntry (pFsbgp4ExtCommOutFilterCommVal->
                                             pu1_OctetList,
                                             &pExtCommFilterInfo);
            if (i4GetOutFilterStatus != EXT_COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Matching Ext-Comm Outgoing Filter "
                          "entry exist.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_BGP4_ECOMM_ENTRY_EXISTS_ERR);
                return SNMP_FAILURE;
            }
            u4BgpMaxOutFilterEntries =
                FsBGPSizingParams[MAX_BGP_EXT_COMM_OUT_FILTER_INFOS_SIZING_ID].
                u4PreAllocatedUnits;
            BGP4_RB_TREE_GET_COUNT (EXT_COMM_OUTPUT_FILTER_TBL,
                                    &u4BgpCurOutFilterEntries);
            if (u4BgpCurOutFilterEntries >= u4BgpMaxOutFilterEntries)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Max entries created.\n");
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_BGP4_EXCEED_MAX_ENTRIES);
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
        case NOT_IN_SERVICE:
            i4GetOutFilterStatus =
                ExtCommGetOutputFilterEntry (pFsbgp4ExtCommOutFilterCommVal->
                                             pu1_OctetList,
                                             &pExtCommFilterInfo);
            if (i4GetOutFilterStatus != EXT_COMM_SUCCESS)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Ext-Comm Outgoing Filter "
                          "entry exist.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if ((pExtCommFilterInfo->u1RowStatus == ACTIVE) ||
                (pExtCommFilterInfo->u1RowStatus == NOT_IN_SERVICE) ||
                (pExtCommFilterInfo->u1RowStatus == NOT_READY))
            {
                return SNMP_SUCCESS;
            }
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        case DESTROY:
            return SNMP_SUCCESS;
        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4ExtCommOutFilterTable
 Input       :  The Indices
                Fsbgp4ExtCommOutFilterCommVal
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4ExtCommOutFilterTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4MpePeerLinkBwTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4MpePeerLinkBwTable
 Input       :  The Indices
                Fsbgp4PeerLinkType
                Fsbgp4PeerLinkRemAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4MpePeerLinkBwTable (INT4 i4Fsbgp4PeerLinkType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsbgp4PeerLinkRemAddr)
{
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4PeerLinkType,
                                 pFsbgp4PeerLinkRemAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4ValidateBgpPeerTableIndex (PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Link Remote Address.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4MpePeerLinkBwTable
 Input       :  The Indices
                Fsbgp4PeerLinkType
                Fsbgp4PeerLinkRemAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4MpePeerLinkBwTable (INT4 *pi4Fsbgp4PeerLinkType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsbgp4PeerLinkRemAddr)
{
    tPeerLinkBandWidth *pTmpPeerLinkBW = NULL;
    tPeerLinkBandWidth  TmpPeerLinkBW;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    MEMSET (&TmpPeerLinkBW, 0, sizeof (tPeerLinkBandWidth));
    TmpPeerLinkBW.u4Context = u4Context;
    pTmpPeerLinkBW = (tPeerLinkBandWidth *)
        BGP4_RB_TREE_GET_NEXT (PEER_LINK_BANDWIDTH_TBL,
                               (tBgp4RBElem *) (&TmpPeerLinkBW), NULL);
    if ((pTmpPeerLinkBW != NULL) && (pTmpPeerLinkBW->u4Context == u4Context))
    {
        switch (BGP4_AFI_IN_ADDR_PREFIX_INFO
                (EXT_COMM_PEER_INFO_IN_PEER_LINKBW_STRUCT (pTmpPeerLinkBW)))
        {
            case BGP4_INET_AFI_IPV4:
            {
                UINT4               u4Peer;
                PTR_FETCH_4 (u4Peer,
                             BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                             (EXT_COMM_PEER_INFO_IN_PEER_LINKBW_STRUCT
                              (pTmpPeerLinkBW)));
                PTR_ASSIGN4 (pFsbgp4PeerLinkRemAddr->pu1_OctetList, (u4Peer));
                pFsbgp4PeerLinkRemAddr->i4_Length = BGP4_IPV4_PREFIX_LEN;
                *pi4Fsbgp4PeerLinkType = BGP4_INET_AFI_IPV4;
            }
                break;
#ifdef BGP4_IPV6_WANTED
            case BGP4_INET_AFI_IPV6:
                MEMCPY (pFsbgp4PeerLinkRemAddr->pu1_OctetList,
                        BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                        (EXT_COMM_PEER_INFO_IN_PEER_LINKBW_STRUCT
                         (pTmpPeerLinkBW)), BGP4_IPV6_PREFIX_LEN);

                pFsbgp4PeerLinkRemAddr->i4_Length = BGP4_IPV6_PREFIX_LEN;
                *pi4Fsbgp4PeerLinkType = BGP4_INET_AFI_IPV6;
                break;
#endif
            default:
                return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4MpePeerLinkBwTable
 Input       :  The Indices
                Fsbgp4PeerLinkType
                nextFsbgp4PeerLinkType
                Fsbgp4PeerLinkRemAddr
                nextFsbgp4PeerLinkRemAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4MpePeerLinkBwTable (INT4 i4Fsbgp4PeerLinkType,
                                         INT4 *pi4NextFsbgp4PeerLinkType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsbgp4PeerLinkRemAddr,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextFsbgp4PeerLinkRemAddr)
{
    tPeerLinkBandWidth *pPeerLinkBandWidth = NULL;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;
    tPeerLinkBandWidth  PeerLinkBandwidth;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4PeerLinkType,
                                 pFsbgp4PeerLinkRemAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    PeerLinkBandwidth.u4Context = u4Context;
    Bgp4CopyAddrPrefixStruct (&(PeerLinkBandwidth.PeerAddress), PeerAddress);

    pPeerLinkBandWidth =
        BGP4_RB_TREE_GET_NEXT (PEER_LINK_BANDWIDTH_TBL,
                               (tBgp4RBElem *) (&PeerLinkBandwidth), NULL);
    if ((pPeerLinkBandWidth != NULL)
        && (pPeerLinkBandWidth->u4Context == u4Context))
    {
        switch (BGP4_AFI_IN_ADDR_PREFIX_INFO
                (EXT_COMM_PEER_INFO_IN_PEER_LINKBW_STRUCT (pPeerLinkBandWidth)))
        {
            case BGP4_INET_AFI_IPV4:
            {
                UINT4               u4Peer;
                PTR_FETCH_4 (u4Peer,
                             BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                             (EXT_COMM_PEER_INFO_IN_PEER_LINKBW_STRUCT
                              (pPeerLinkBandWidth)));
                PTR_ASSIGN4 (pNextFsbgp4PeerLinkRemAddr->pu1_OctetList,
                             (u4Peer));
                pNextFsbgp4PeerLinkRemAddr->i4_Length = BGP4_IPV4_PREFIX_LEN;
                *pi4NextFsbgp4PeerLinkType = BGP4_INET_AFI_IPV4;
            }
                break;
#ifdef BGP4_IPV6_WANTED
            case BGP4_INET_AFI_IPV6:
                MEMCPY (pNextFsbgp4PeerLinkRemAddr->pu1_OctetList,
                        BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                        (EXT_COMM_PEER_INFO_IN_PEER_LINKBW_STRUCT
                         (pPeerLinkBandWidth)), BGP4_IPV6_PREFIX_LEN);
                pNextFsbgp4PeerLinkRemAddr->i4_Length = BGP4_IPV6_PREFIX_LEN;
                *pi4NextFsbgp4PeerLinkType = BGP4_INET_AFI_IPV6;
                break;
#endif
            default:
                return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeLinkBandWidth
 Input       :  The Indices
                Fsbgp4PeerLinkType
                Fsbgp4PeerLinkRemAddr

                The Object 
                retValFsbgp4LinkBandWidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeLinkBandWidth (INT4 i4Fsbgp4PeerLinkType,
                              tSNMP_OCTET_STRING_TYPE * pFsbgp4PeerLinkRemAddr,
                              UINT4 *pu4RetValFsbgp4LinkBandWidth)
{
    tPeerLinkBandWidth *pPeerLinkBandWidth = NULL;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4PeerLinkType,
                                 pFsbgp4PeerLinkRemAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    ExtCommGetPeerLinkBandWidthEntry (PeerAddress, &pPeerLinkBandWidth);
    if (pPeerLinkBandWidth == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Link BandWidth entry exist.\n");
        return SNMP_FAILURE;
    }

    if ((pPeerLinkBandWidth->u1RowStatus == NOT_READY) ||
        (pPeerLinkBandWidth->u4LinkBandWidth < BAND_WIDTH_MIN_IN_BPS) ||
        (pPeerLinkBandWidth->u4LinkBandWidth > BAND_WIDTH_MAX_IN_BPS))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Entry exist - Link Bandwidth not configured.\n");
        return SNMP_FAILURE;
    }

    *pu4RetValFsbgp4LinkBandWidth = pPeerLinkBandWidth->u4LinkBandWidth;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpePeerLinkBwRowStatus
 Input       :  The Indices
                Fsbgp4PeerLinkType
                Fsbgp4PeerLinkRemAddr

                The Object 
                retValFsbgp4PeerLinkBwRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpePeerLinkBwRowStatus (INT4 i4Fsbgp4PeerLinkType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsbgp4PeerLinkRemAddr,
                                    INT4 *pi4RetValFsbgp4PeerLinkBwRowStatus)
{
    tPeerLinkBandWidth *pPeerLinkBandWidth = NULL;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4PeerLinkType,
                                 pFsbgp4PeerLinkRemAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    ExtCommGetPeerLinkBandWidthEntry (PeerAddress, &pPeerLinkBandWidth);
    if (pPeerLinkBandWidth == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Link BandWidth entry exist.\n");
        return SNMP_FAILURE;
    }
    if ((pPeerLinkBandWidth->u1RowStatus != ACTIVE) &&
        (pPeerLinkBandWidth->u1RowStatus != NOT_IN_SERVICE))
    {
        *pi4RetValFsbgp4PeerLinkBwRowStatus = NOT_READY;
    }
    else
    {
        *pi4RetValFsbgp4PeerLinkBwRowStatus = pPeerLinkBandWidth->u1RowStatus;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4mpeLinkBandWidth
 Input       :  The Indices
                Fsbgp4PeerLinkType
                Fsbgp4PeerLinkRemAddr

                The Object 
                setValFsbgp4LinkBandWidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4mpeLinkBandWidth (INT4 i4Fsbgp4PeerLinkType,
                              tSNMP_OCTET_STRING_TYPE * pFsbgp4PeerLinkRemAddr,
                              UINT4 u4SetValFsbgp4LinkBandWidth)
{
    tPeerLinkBandWidth *pPeerLinkBandWidth = NULL;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4PeerLinkType,
                                 pFsbgp4PeerLinkRemAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    ExtCommGetPeerLinkBandWidthEntry (PeerAddress, &pPeerLinkBandWidth);
    if (pPeerLinkBandWidth == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching Link BandWidth entry exist.\n");
        return SNMP_FAILURE;
    }
    pPeerLinkBandWidth->u4LinkBandWidth = u4SetValFsbgp4LinkBandWidth;
    if (pPeerLinkBandWidth->u1RowStatus == NOT_READY)
    {
        pPeerLinkBandWidth->u1RowStatus = NOT_IN_SERVICE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4mpeLinkBandWidth, u4SeqNum,
                          FALSE, BgpLock, BgpUnLock, 3, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %u",
                      gpBgpCurrCxtNode->u4ContextId, i4Fsbgp4PeerLinkType,
                      pFsbgp4PeerLinkRemAddr, u4SetValFsbgp4LinkBandWidth));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsbgp4mpePeerLinkBwRowStatus
 Input       :  The Indices
                Fsbgp4PeerLinkType
                Fsbgp4PeerLinkRemAddr

                The Object 
                setValFsbgp4PeerLinkBwRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4mpePeerLinkBwRowStatus (INT4 i4Fsbgp4PeerLinkType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsbgp4PeerLinkRemAddr,
                                    INT4 i4SetValFsbgp4PeerLinkBwRowStatus)
{
    tPeerLinkBandWidth *pPeerLinkBandWidth = NULL;
    tAddrPrefix         PeerAddress;
    UINT4               u4RtMemSts;
    INT4                i4RetSts;
    UINT4               u4Ret;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    i4RetSts = Bgp4LowGetIpAddress (i4Fsbgp4PeerLinkType,
                                    pFsbgp4PeerLinkRemAddr, &PeerAddress);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFsbgp4PeerLinkBwRowStatus)
    {
        case CREATE_AND_GO:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Row Status Value - CreateAndGo - "
                      "Not supported.\n");
            return SNMP_FAILURE;

        case CREATE_AND_WAIT:
            i4RetSts =
                ExtCommCreatePeerLinkBandWidthEntry (PeerAddress,
                                                     &pPeerLinkBandWidth);
            if (i4RetSts == EXT_COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to Create Link BandWidth entry.\n");
                return SNMP_FAILURE;
            }
            pPeerLinkBandWidth->u1RowStatus = NOT_READY;
            break;

        case ACTIVE:
            ExtCommGetPeerLinkBandWidthEntry (PeerAddress, &pPeerLinkBandWidth);
            if (pPeerLinkBandWidth == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Link BandWidth entry "
                          "exist.\n");
                return SNMP_FAILURE;
            }
            pPeerLinkBandWidth->u1RowStatus = ACTIVE;
            break;

        case NOT_IN_SERVICE:
            ExtCommGetPeerLinkBandWidthEntry (PeerAddress, &pPeerLinkBandWidth);
            if (pPeerLinkBandWidth == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Link BandWidth entry "
                          "exist.\n");
                return SNMP_FAILURE;
            }
            pPeerLinkBandWidth->u1RowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:
            ExtCommGetPeerLinkBandWidthEntry (PeerAddress, &pPeerLinkBandWidth);
            if (pPeerLinkBandWidth == NULL)
            {
                break;
            }
            u4Ret = BGP4_RB_TREE_REMOVE (PEER_LINK_BANDWIDTH_TBL,
                                         (tBgp4RBElem *) pPeerLinkBandWidth);
            if (u4Ret == RB_FAILURE)
            {
                return SNMP_FAILURE;
            }

            u4RtMemSts = PEER_LINK_BANDWIDTH_ENTRY_FREE (pPeerLinkBandWidth);
            if (u4RtMemSts == MEM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_BUFFER_TRC,BGP4_MOD_NAME,
                      "\tERROR - Memrelease failed.\n");
            }

            break;

        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4mpePeerLinkBwRowStatus,
                          u4SeqNum, TRUE, BgpLock, BgpUnLock, 3, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %i",
                      gpBgpCurrCxtNode->u4ContextId, i4Fsbgp4PeerLinkType,
                      pFsbgp4PeerLinkRemAddr,
                      i4SetValFsbgp4PeerLinkBwRowStatus));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4mpeLinkBandWidth
 Input       :  The Indices
                Fsbgp4PeerLinkType
                Fsbgp4PeerLinkRemAddr

                The Object 
                testValFsbgp4LinkBandWidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4mpeLinkBandWidth (UINT4 *pu4ErrorCode,
                                 INT4 i4Fsbgp4PeerLinkType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsbgp4PeerLinkRemAddr,
                                 UINT4 u4TestValFsbgp4LinkBandWidth)
{
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;
    UINT4               u4Context = 0;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Local AS not configured..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4PeerLinkType,
                                 pFsbgp4PeerLinkRemAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Link Remote Address.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4ValidateBgpPeerTableIndex (PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Link Remote Address.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4TestValFsbgp4LinkBandWidth < BAND_WIDTH_MIN_IN_BPS) ||
        (u4TestValFsbgp4LinkBandWidth > BAND_WIDTH_MAX_IN_BPS))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Link BandWidth Value.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4mpePeerLinkBwRowStatus
 Input       :  The Indices
                Fsbgp4PeerLinkType
                Fsbgp4PeerLinkRemAddr

                The Object 
                testValFsbgp4PeerLinkBwRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4mpePeerLinkBwRowStatus (UINT4 *pu4ErrorCode,
                                       INT4 i4Fsbgp4PeerLinkType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsbgp4PeerLinkRemAddr,
                                       INT4 i4TestValFsbgp4PeerLinkBwRowStatus)
{
    tPeerLinkBandWidth *pPeerLinkBandWidth = NULL;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;
    UINT4               u4Context = 0;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Local AS not configured..\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4PeerLinkType,
                                 pFsbgp4PeerLinkRemAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4ValidateBgpPeerTableIndex (PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    switch (i4TestValFsbgp4PeerLinkBwRowStatus)
    {
        case CREATE_AND_GO:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Row Status Value - CreateAndGo - "
                      "Not supported.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        case CREATE_AND_WAIT:
            i4Sts =
                ExtCommGetPeerLinkBandWidthEntry (PeerAddress,
                                                  &pPeerLinkBandWidth);
            if (i4Sts != EXT_COMM_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Matching Link BandWidth entry exist.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
        case NOT_IN_SERVICE:
            i4Sts =
                ExtCommGetPeerLinkBandWidthEntry (PeerAddress,
                                                  &pPeerLinkBandWidth);
            if (i4Sts != EXT_COMM_SUCCESS)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Link BandWidth entry "
                          "exist.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if ((pPeerLinkBandWidth->u4LinkBandWidth < BAND_WIDTH_MIN_IN_BPS) ||
                (pPeerLinkBandWidth->u4LinkBandWidth > BAND_WIDTH_MAX_IN_BPS))
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Link Bandwidth is not configured.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if ((pPeerLinkBandWidth->u1RowStatus == ACTIVE) ||
                (pPeerLinkBandWidth->u1RowStatus == NOT_IN_SERVICE) ||
                (pPeerLinkBandWidth->u1RowStatus == NOT_READY))
            {
                return SNMP_SUCCESS;
            }
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        case DESTROY:
            ExtCommGetPeerLinkBandWidthEntry (PeerAddress, &pPeerLinkBandWidth);
            return SNMP_SUCCESS;
        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4MpePeerLinkBwTable
 Input       :  The Indices
                Fsbgp4PeerLinkType
                Fsbgp4PeerLinkRemAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4MpePeerLinkBwTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

INT4
Bgp4ValidateExtCommRouteAddCommTableIndex (tNetAddress RoutePrefix,
                                           tSNMP_OCTET_STRING_TYPE * pEcommVal)
{
    INT4                i4RetStatus;
    UINT1               u1ValidStatus;

    /* Validate the prefix length  */
    u1ValidStatus = Bgp4IsValidPrefixLength (RoutePrefix);
    if (u1ValidStatus == BGP4_FALSE)
    {
        CLI_SET_ERR (CLI_BGP4_INVALID_PREFIX_LEN_ERR);
        return BGP4_FAILURE;
    }

    /* Validate the prefix */
    u1ValidStatus =
        Bgp4IsValidAddress (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                            (RoutePrefix), BGP4_FALSE);
    if (u1ValidStatus != BGP4_TRUE)
    {
        CLI_SET_ERR (CLI_BGP4_INVALID_PREFIX_ERR);
        return BGP4_FAILURE;
    }

    /* Validate the extended community value */
    i4RetStatus = ExtCommValidateExtCommunity (pEcommVal);
    if (i4RetStatus == EXT_COMM_FAILURE)
    {
        CLI_SET_ERR (CLI_BGP4_INVALID_ECOMM_VAL_ERR);
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

INT4
ExtCommGetPeerLinkBandWidthEntry (tAddrPrefix PeerLinkRemAddr,
                                  tPeerLinkBandWidth ** ppPeerLinkBandWidth)
{
    tPeerLinkBandWidth *pTmpPeerLinkBW = NULL;
    tPeerLinkBandWidth  PeerLinkBandwidth;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    PeerLinkBandwidth.u4Context = u4Context;
    Bgp4CopyAddrPrefixStruct (&(EXT_COMM_PEER_INFO_IN_PEER_LINKBW_STRUCT
                                ((&PeerLinkBandwidth))), PeerLinkRemAddr);

    pTmpPeerLinkBW = (tPeerLinkBandWidth *)
        BGP4_RB_TREE_GET (PEER_LINK_BANDWIDTH_TBL,
                          (tBgp4RBElem *) & PeerLinkBandwidth);
    if (pTmpPeerLinkBW != NULL)
    {
        *ppPeerLinkBandWidth = pTmpPeerLinkBW;
        return EXT_COMM_SUCCESS;
    }
    /* Peer doesnot have link bandwidth */
    *ppPeerLinkBandWidth = NULL;
    return EXT_COMM_FAILURE;
}

INT4
ExtCommCreatePeerLinkBandWidthEntry (tAddrPrefix PeerLinkRemAddr,
                                     tPeerLinkBandWidth ** ppPeerLinkBandWidth)
{
    UINT4               u4Ret;

    PEER_LINK_BANDWIDTH_ENTRY_CREATE (*ppPeerLinkBandWidth);
    if (*ppPeerLinkBandWidth == NULL)
    {
        return EXT_COMM_FAILURE;
    }
    TMO_SLL_Init_Node (&((*ppPeerLinkBandWidth)->PeerLinkBandWidthNext));
    Bgp4CopyAddrPrefixStruct (&
                              (EXT_COMM_PEER_INFO_IN_PEER_LINKBW_STRUCT
                               ((*ppPeerLinkBandWidth))), PeerLinkRemAddr);
    (*ppPeerLinkBandWidth)->u4LinkBandWidth = 0;

    u4Ret = BGP4_RB_TREE_ADD (PEER_LINK_BANDWIDTH_TBL,
                              (tBgp4RBElem *) (*ppPeerLinkBandWidth));
    if (u4Ret == RB_FAILURE)
    {
        return EXT_COMM_FAILURE;
    }
    return EXT_COMM_SUCCESS;
}

INT4
ExtCommCreateAddExtCommEntry (tNetAddress RcvdPrefix,
                              tSNMP_OCTET_STRING_TYPE * pFsbgp4AddExtCommVal,
                              tExtCommProfile ** ppExtCommProfile)
{
    tRouteConfExtComm  *pRouteConfExtComm = NULL;
    tExtCommProfile    *pExtCommProfile = NULL;
    tRouteConfExtComm   RouteConfExtComm;
    UINT4               u4Ret;
    UINT1               u1MatchFnd = EXT_COMM_FALSE;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    Bgp4CopyNetAddressStruct (&(RouteConfExtComm.RtNetAddress), RcvdPrefix);

    RouteConfExtComm.u4Context = u4Context;
    pRouteConfExtComm = BGP4_RB_TREE_GET (ROUTES_EXT_COMM_ADD_TBL,
                                          (tBgp4RBElem *) (&RouteConfExtComm));
    if (pRouteConfExtComm != NULL)
    {
        u1MatchFnd = EXT_COMM_TRUE;
    }

    if (u1MatchFnd == EXT_COMM_FALSE)
    {
        EXT_COMM_ROUTE_CONF_EXT_COMM_NODE_CREATE (pRouteConfExtComm);
        if (pRouteConfExtComm == NULL)
        {
            return EXT_COMM_FAILURE;
        }
        Bgp4CopyNetAddressStruct (&
                                  (EXT_COMM_NET_ADDRESS_IN_ROUTE_CONF_STRUCT
                                   (pRouteConfExtComm)), RcvdPrefix);
        TMO_SLL_Init (&pRouteConfExtComm->TSConfExtComms);
        u4Ret = BGP4_RB_TREE_ADD (ROUTES_EXT_COMM_ADD_TBL,
                                  (tBgp4RBElem *) pRouteConfExtComm);
        if (u4Ret == RB_FAILURE)
        {
            return EXT_COMM_FAILURE;
        }
    }

    /* Allocate tExtCommProfile and link it to */
    /* TSConfExtComms of tRouteConfExtComm Node   */

    EXT_COMM_EXT_COMM_PROFILE_NODE_CREATE (pExtCommProfile);
    if (pExtCommProfile == NULL)
    {
        return EXT_COMM_FAILURE;
    }
    ExtCommCopyExtCommunityToProfile (pExtCommProfile, pFsbgp4AddExtCommVal);

    TMO_SLL_Add (&pRouteConfExtComm->TSConfExtComms,
                 &pExtCommProfile->ExtCommProfileNext);
    *ppExtCommProfile = pExtCommProfile;
    return EXT_COMM_SUCCESS;
}

INT4
ExtCommDeleteAddExtCommEntry (tNetAddress RcvdPrefix,
                              tSNMP_OCTET_STRING_TYPE * pFsbgp4AddExtCommVal,
                              tExtCommProfile ** ppExtCommProfile)
{

    tRouteConfExtComm  *pRouteConfExtComm = NULL;
    tRouteConfExtComm  *pTmpRouteConfExtComm = NULL;
    tExtCommProfile    *pExtCommProfile = NULL;
    tExtCommProfile    *pTmpExtCommProfile = NULL;
    UINT1               u1MatchFnd = EXT_COMM_FALSE;
    tRouteConfExtComm   RouteConfExtComm;
    UINT4               u4Ret;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    RouteConfExtComm.u4Context = u4Context;

    UNUSED_PARAM (ppExtCommProfile);
    Bgp4CopyNetAddressStruct (&(RouteConfExtComm.RtNetAddress), RcvdPrefix);
    pRouteConfExtComm =
        BGP4_RB_TREE_GET (ROUTES_EXT_COMM_ADD_TBL,
                          (tBgp4RBElem *) (&RouteConfExtComm));
    if (pRouteConfExtComm != NULL)
    {
        BGP_SLL_DYN_Scan (&(pRouteConfExtComm->TSConfExtComms),
                          pExtCommProfile, pTmpExtCommProfile,
                          tExtCommProfile *)
        {
            u1MatchFnd = ExtCommCmpExtCommunity (pFsbgp4AddExtCommVal,
                                                 pExtCommProfile);
            if (u1MatchFnd == COMM_TRUE)
            {
                pTmpRouteConfExtComm = pRouteConfExtComm;
                TMO_SLL_Delete (&(pRouteConfExtComm->TSConfExtComms),
                                &pExtCommProfile->ExtCommProfileNext);
                EXT_COMM_EXT_COMM_PROFILE_NODE_FREE (pExtCommProfile);
                break;
            }
        }
    }
    else
    {
        return EXT_COMM_FAILURE;
    }

    /* Free tRouteConfExtComm if TSConfExtComms of tRouteConfExtComm does not */
    /* contain any ExtCommProfile Node   */
    if (u1MatchFnd == COMM_TRUE)
    {
        if (TMO_SLL_Count (&(pTmpRouteConfExtComm->TSConfExtComms)) == 0)
        {
            u4Ret = BGP4_RB_TREE_REMOVE (ROUTES_EXT_COMM_ADD_TBL,
                                         (tBgp4RBElem *) pTmpRouteConfExtComm);
            if (u4Ret == RB_FAILURE)
            {
                return EXT_COMM_FAILURE;
            }
            EXT_COMM_ROUTE_CONF_EXT_COMM_NODE_FREE (pTmpRouteConfExtComm);
        }
    }
    return EXT_COMM_SUCCESS;
}

INT4
ExtCommGetAddExtCommEntry (tNetAddress RcvdPrefix,
                           tSNMP_OCTET_STRING_TYPE * pFsbgp4AddExtCommVal,
                           tExtCommProfile ** ppExtCommProfile)
{
    tRouteConfExtComm  *pRouteConfExtComm = NULL;
    tExtCommProfile    *pExtCommProfile = NULL;
    tRouteConfExtComm   RouteConfExtComm;
    UINT1               u1MatchFnd = COMM_FALSE;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    RouteConfExtComm.u4Context = u4Context;
    Bgp4CopyNetAddressStruct (&(RouteConfExtComm.RtNetAddress), RcvdPrefix);
    pRouteConfExtComm =
        BGP4_RB_TREE_GET (ROUTES_EXT_COMM_ADD_TBL,
                          (tBgp4RBElem *) (&RouteConfExtComm));
    if (pRouteConfExtComm != NULL)
    {
        TMO_SLL_Scan (&(pRouteConfExtComm->TSConfExtComms),
                      pExtCommProfile, tExtCommProfile *)
        {
            u1MatchFnd = ExtCommCmpExtCommunity (pFsbgp4AddExtCommVal,
                                                 pExtCommProfile);
            if (u1MatchFnd == COMM_TRUE)
            {
                *ppExtCommProfile = pExtCommProfile;
                return EXT_COMM_SUCCESS;
            }
        }
    }
    return EXT_COMM_FAILURE;
}

INT4
ExtCommCreateDeleteExtCommEntry (tNetAddress RcvdPrefix,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsbgp4DeleteExtCommVal,
                                 tExtCommProfile ** ppExtCommProfile)
{
    tRouteConfExtComm  *pRouteConfExtComm = NULL;
    tRouteConfExtComm   RouteConfExtComm;
    tExtCommProfile    *pExtCommProfile = NULL;
    UINT4               u4Ret;
    UINT1               u1MatchFnd = EXT_COMM_FALSE;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    RouteConfExtComm.u4Context = u4Context;

    Bgp4CopyNetAddressStruct (&(RouteConfExtComm.RtNetAddress), RcvdPrefix);
    pRouteConfExtComm =
        BGP4_RB_TREE_GET (ROUTES_EXT_COMM_DELETE_TBL,
                          (tBgp4RBElem *) (&RouteConfExtComm));
    if (pRouteConfExtComm != NULL)
    {
        u1MatchFnd = EXT_COMM_TRUE;
    }
    if (u1MatchFnd == EXT_COMM_FALSE)
    {
        EXT_COMM_ROUTE_CONF_EXT_COMM_NODE_CREATE (pRouteConfExtComm);
        if (pRouteConfExtComm == NULL)
        {
            return EXT_COMM_FAILURE;
        }
        Bgp4CopyNetAddressStruct (&
                                  (EXT_COMM_NET_ADDRESS_IN_ROUTE_CONF_STRUCT
                                   (pRouteConfExtComm)), RcvdPrefix);
        TMO_SLL_Init (&pRouteConfExtComm->TSConfExtComms);
        u4Ret = BGP4_RB_TREE_ADD (ROUTES_EXT_COMM_DELETE_TBL,
                                  (tBgp4RBElem *) pRouteConfExtComm);
        if (u4Ret == RB_FAILURE)
        {
            return EXT_COMM_FAILURE;
        }
    }

    /* Allocate tExtCommProfile and link it to */
    /* TSConfExtComms of tRouteConfExtComm Node   */

    EXT_COMM_EXT_COMM_PROFILE_NODE_CREATE (pExtCommProfile);
    if (pExtCommProfile == NULL)
    {
        return EXT_COMM_FAILURE;
    }
    ExtCommCopyExtCommunityToProfile (pExtCommProfile, pFsbgp4DeleteExtCommVal);

    TMO_SLL_Add (&(pRouteConfExtComm->TSConfExtComms),
                 &pExtCommProfile->ExtCommProfileNext);
    *ppExtCommProfile = pExtCommProfile;

    return EXT_COMM_SUCCESS;
}

INT4
ExtCommDeleteDeleteExtCommEntry (tNetAddress RcvdPrefix,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsbgp4DeleteExtCommVal,
                                 tExtCommProfile ** ppExtCommProfile)
{
    tRouteConfExtComm  *pRouteConfExtComm = NULL;
    tRouteConfExtComm  *pTmpRouteConfExtComm = NULL;
    tExtCommProfile    *pExtCommProfile = NULL;
    tExtCommProfile    *pTmpExtCommProfile = NULL;
    tRouteConfExtComm   RouteConfExtComm;
    UINT4               u4Ret;
    UINT1               u1MatchFnd = EXT_COMM_FALSE;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    RouteConfExtComm.u4Context = u4Context;

    UNUSED_PARAM (ppExtCommProfile);
    Bgp4CopyNetAddressStruct (&(RouteConfExtComm.RtNetAddress), RcvdPrefix);
    pRouteConfExtComm =
        BGP4_RB_TREE_GET (ROUTES_EXT_COMM_DELETE_TBL,
                          (tBgp4RBElem *) (&RouteConfExtComm));
    if (pRouteConfExtComm != NULL)
    {
        BGP_SLL_DYN_Scan (&(pRouteConfExtComm->TSConfExtComms),
                          pExtCommProfile, pTmpExtCommProfile,
                          tExtCommProfile *)
        {
            u1MatchFnd =
                ExtCommCmpExtCommunity (pFsbgp4DeleteExtCommVal,
                                        pExtCommProfile);
            if (u1MatchFnd == COMM_TRUE)
            {
                pTmpRouteConfExtComm = pRouteConfExtComm;
                TMO_SLL_Delete (&(pRouteConfExtComm->TSConfExtComms),
                                &pExtCommProfile->ExtCommProfileNext);
                EXT_COMM_EXT_COMM_PROFILE_NODE_FREE (pExtCommProfile);
                break;
            }
        }
    }
    else
    {
        return EXT_COMM_FAILURE;
    }

    /* Free tRouteConfExtComm if TSConfExtComms of tRouteConfExtComm does not */
    /* contain any ExtCommProfile Node   */
    if (u1MatchFnd == COMM_TRUE)
    {
        if (TMO_SLL_Count (&(pTmpRouteConfExtComm->TSConfExtComms)) == 0)
        {
            u4Ret = BGP4_RB_TREE_REMOVE (ROUTES_EXT_COMM_DELETE_TBL,
                                         (tBgp4RBElem *) pTmpRouteConfExtComm);
            if (u4Ret == RB_FAILURE)
            {
                return EXT_COMM_FAILURE;
            }
            EXT_COMM_ROUTE_CONF_EXT_COMM_NODE_FREE (pTmpRouteConfExtComm);
        }
    }
    return EXT_COMM_SUCCESS;
}

INT4
ExtCommGetDeleteExtCommEntry (tNetAddress RcvdPrefix,
                              UINT1 *pFsbgp4DeleteExtCommVal,
                              tExtCommProfile ** ppExtCommProfile)
{
    tRouteConfExtComm  *pRouteConfExtComm = NULL;
    tExtCommProfile    *pExtCommProfile = NULL;
    tRouteConfExtComm   RouteConfExtComm;
    INT4                i4MatchFnd;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    RouteConfExtComm.u4Context = u4Context;

    Bgp4CopyNetAddressStruct (&(RouteConfExtComm.RtNetAddress), RcvdPrefix);
    pRouteConfExtComm =
        BGP4_RB_TREE_GET (ROUTES_EXT_COMM_DELETE_TBL,
                          (tBgp4RBElem *) (&RouteConfExtComm));
    if (pRouteConfExtComm != NULL)
    {
        TMO_SLL_Scan (&(pRouteConfExtComm->TSConfExtComms),
                      pExtCommProfile, tExtCommProfile *)
        {
            i4MatchFnd = MEMCMP (pFsbgp4DeleteExtCommVal,
                                 pExtCommProfile->au1ExtComm,
                                 EXT_COMM_VALUE_LEN);
            if (i4MatchFnd == 0)
            {
                *ppExtCommProfile = pExtCommProfile;
                return EXT_COMM_SUCCESS;
            }
        }
    }
    return EXT_COMM_FAILURE;
}

INT4
ExtCommGetRouteExtCommSetStatusEntry (tNetAddress RcvdPrefix,
                                      tRouteExtCommSetStatus **
                                      ppRouteExtCommSetStatus)
{
    tRouteExtCommSetStatus *pTmpRouteExtCommSetStatus = NULL;
    tRouteExtCommSetStatus RouteExtCommSetStatus;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    RouteExtCommSetStatus.u4Context = u4Context;

    Bgp4CopyNetAddressStruct (&(RouteExtCommSetStatus.RtNetAddress),
                              RcvdPrefix);
    pTmpRouteExtCommSetStatus =
        BGP4_RB_TREE_GET (ROUTES_EXT_COMM_SET_STATUS_TBL,
                          (tBgp4RBElem *) (&RouteExtCommSetStatus));
    if (pTmpRouteExtCommSetStatus != NULL)
    {
        *ppRouteExtCommSetStatus = pTmpRouteExtCommSetStatus;
        return EXT_COMM_SUCCESS;
    }
    /* No set status configured for this route */
    *ppRouteExtCommSetStatus = NULL;
    return EXT_COMM_FAILURE;
}

INT4
ExtCommCreateRouteExtCommSetStatusEntry (tNetAddress RcvdPrefix,
                                         tRouteExtCommSetStatus **
                                         ppRouteExtCommSetStatus)
{
    INT4                i4GetExtCommEntrySts;
    UINT4               u4Ret;

    i4GetExtCommEntrySts =
        ExtCommGetRouteExtCommSetStatusEntry (RcvdPrefix,
                                              ppRouteExtCommSetStatus);
    if (i4GetExtCommEntrySts == EXT_COMM_SUCCESS)
    {
        /* Entry already present */
        return EXT_COMM_FAILURE;
    }

    EXT_COMM_ROUTES_EXT_COMM_SET_STATUS_ENTRY_CREATE (*ppRouteExtCommSetStatus);
    if (*ppRouteExtCommSetStatus == NULL)
    {
        return EXT_COMM_FAILURE;
    }
    Bgp4CopyNetAddressStruct (&
                              (EXT_COMM_NET_ADDRESS_IN_ROUTE_SET_STATUS_STRUCT
                               ((*ppRouteExtCommSetStatus))), RcvdPrefix);
    /* set the SetStatus to default value 
     * With this default value only, the entry will be active when it 
     * is created by issuing CREATE_AND_GO,
     * This default value will be modified with the given value from the agent
     * when issed CREATE_AND_WAIT
     */
    (*ppRouteExtCommSetStatus)->i1ExtCommSetStatus = EXT_COMM_MODIFY;
    u4Ret = BGP4_RB_TREE_ADD (ROUTES_EXT_COMM_SET_STATUS_TBL,
                              (tBgp4RBElem *) (*ppRouteExtCommSetStatus));
    if (u4Ret == RB_FAILURE)
    {
        return EXT_COMM_FAILURE;
    }
    return SNMP_SUCCESS;
}

INT4
ExtCommCreateInputFilterEntry (UINT1 *pu1Fsbgp4ExtCommInFilterCommVal,
                               tExtCommFilterInfo ** ppExtCommFilterInfo)
{
    UINT4               u4Ret;


    EXT_COMM_INPUT_FILTER_ENTRY_CREATE (*ppExtCommFilterInfo);
    if (*ppExtCommFilterInfo == NULL)
    {
        return EXT_COMM_FAILURE;
    }
    TMO_SLL_Init_Node (&((*ppExtCommFilterInfo)->ExtCommInfoNext));


    MEMCPY ((UINT1 *) ((*ppExtCommFilterInfo)->au1ExtComm),
            pu1Fsbgp4ExtCommInFilterCommVal, EXT_COMM_VALUE_LEN);
    (*ppExtCommFilterInfo)->u4Context = gpBgpCurrCxtNode->u4ContextId;

    /* set the SetStatus to default value 
     * With this default value only, the entry will be active when it is 
     * created by issuing CREATE_AND_GO,
     * This default value will be modified with the given value from the agent
     * when issed CREATE_AND_WAIT
     */
    (*ppExtCommFilterInfo)->i1FilterPolicy = EXT_COMM_ACCEPT;
    u4Ret = BGP4_RB_TREE_ADD (EXT_COMM_INPUT_FILTER_TBL,
                              (tBgp4RBElem *) (*ppExtCommFilterInfo));
    if (u4Ret == RB_FAILURE)
    {
        return EXT_COMM_FAILURE;
    }
    return EXT_COMM_SUCCESS;
}

INT4
ExtCommGetInputFilterEntry (UINT1 *pu1Fsbgp4ExtCommInFilterCommVal,
                            tExtCommFilterInfo ** ppExtCommFilterInfo)
{
    tExtCommFilterInfo *pTmpExtCommFilterInfo = NULL;
    tExtCommFilterInfo  ExtCommFilterInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    ExtCommFilterInfo.u4Context = u4Context;

    MEMCPY (ExtCommFilterInfo.au1ExtComm,
            pu1Fsbgp4ExtCommInFilterCommVal, EXT_COMM_VALUE_LEN);
    pTmpExtCommFilterInfo =
        BGP4_RB_TREE_GET (EXT_COMM_INPUT_FILTER_TBL,
                          (tBgp4RBElem *) (&ExtCommFilterInfo));
    if (pTmpExtCommFilterInfo != NULL)
    {
        *ppExtCommFilterInfo = pTmpExtCommFilterInfo;
        return EXT_COMM_SUCCESS;
    }
    /* Router doesnot have  Filtering policy */
    /* configured for this  ext -community   */
    *ppExtCommFilterInfo = NULL;
    return EXT_COMM_FAILURE;
}

INT4
ExtCommCreateOutputFilterEntry (UINT1 *pu1Fsbgp4ExtCommOutFilterCommVal,
                                tExtCommFilterInfo ** ppExtCommFilterInfo)
{
    UINT4               u4Ret;

    EXT_COMM_OUTPUT_FILTER_ENTRY_CREATE (*ppExtCommFilterInfo);
    if (*ppExtCommFilterInfo == NULL)
    {
        return EXT_COMM_FAILURE;
    }
    TMO_SLL_Init_Node (&((*ppExtCommFilterInfo)->ExtCommInfoNext));


    MEMCPY ((UINT1 *) ((*ppExtCommFilterInfo)->au1ExtComm),
            pu1Fsbgp4ExtCommOutFilterCommVal, EXT_COMM_VALUE_LEN);
    /* set the SetStatus to default value 
     * With this default value only, the entry will be active when it is 
     * created by issuing CREATE_AND_GO,
     * This default value will be modified with the given value from the agent
     * when issed CREATE_AND_WAIT
     */
    (*ppExtCommFilterInfo)->i1FilterPolicy =
        EXT_COMM_ADVERTISE + BGP4_INCREMENT_BY_ONE;

    u4Ret = BGP4_RB_TREE_ADD (EXT_COMM_OUTPUT_FILTER_TBL,
                              (tBgp4RBElem *) (*ppExtCommFilterInfo));
    if (u4Ret == RB_FAILURE)
    {
        return EXT_COMM_FAILURE;
    }
    return EXT_COMM_SUCCESS;
}

INT4
ExtCommGetOutputFilterEntry (UINT1 *pu1Fsbgp4ExtCommOutFilterCommVal,
                             tExtCommFilterInfo ** ppExtCommFilterInfo)
{
    tExtCommFilterInfo *pTmpExtCommFilterInfo = NULL;
    tExtCommFilterInfo  ExtCommFilterInfo;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    ExtCommFilterInfo.u4Context = u4Context;

    MEMCPY (ExtCommFilterInfo.au1ExtComm,
            pu1Fsbgp4ExtCommOutFilterCommVal, EXT_COMM_VALUE_LEN);
    pTmpExtCommFilterInfo =
        BGP4_RB_TREE_GET (EXT_COMM_OUTPUT_FILTER_TBL,
                          (tBgp4RBElem *) (&ExtCommFilterInfo));
    if (pTmpExtCommFilterInfo != NULL)
    {
        *ppExtCommFilterInfo = pTmpExtCommFilterInfo;
        return EXT_COMM_SUCCESS;
    }
    /* Router doesnot have  Filtering policy */
    /* configured for this  ext -community   */
    *ppExtCommFilterInfo = NULL;
    return EXT_COMM_FAILURE;
}

INT4
ExtCommCopyExtCommunityFromProfile (tSNMP_OCTET_STRING_TYPE *
                                    pFsbgp4AddExtCommVal,
                                    tExtCommProfile * pExtCommProfile)
{
    MEMCPY (pFsbgp4AddExtCommVal->pu1_OctetList,
            (UINT1 *) pExtCommProfile->au1ExtComm, EXT_COMM_VALUE_LEN);
    pFsbgp4AddExtCommVal->i4_Length = EXT_COMM_VALUE_LEN;
    return EXT_COMM_SUCCESS;
}

INT4
ExtCommCopyExtCommunityToProfile (tExtCommProfile * pExtCommProfile,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsbgp4AddExtCommVal)
{
    MEMCPY ((UINT1 *) pExtCommProfile->au1ExtComm,
            pFsbgp4AddExtCommVal->pu1_OctetList, EXT_COMM_VALUE_LEN);
    return EXT_COMM_SUCCESS;
}

UINT1
ExtCommCmpExtCommunity (tSNMP_OCTET_STRING_TYPE * pFsbgp4AddExtCommVal,
                        tExtCommProfile * pExtCommProfile)
{
    INT4                i4MemCmpStatus;
    i4MemCmpStatus = MEMCMP (pFsbgp4AddExtCommVal->pu1_OctetList,
                             (UINT1 *) pExtCommProfile->au1ExtComm,
                             EXT_COMM_VALUE_LEN);
    if (i4MemCmpStatus == 0)
    {
        return COMM_TRUE;
    }
    else
    {
        return COMM_FALSE;
    }
}

INT4
ExtCommValidateExtCommunity (tSNMP_OCTET_STRING_TYPE * pFsbgp4AddExtCommVal)
{
    INT4                i4RetSts;
    UINT2               u2Fsbgp4ExtCommInFilterCommVal = 0;

    PTR_FETCH2 ((u2Fsbgp4ExtCommInFilterCommVal),
                (pFsbgp4AddExtCommVal->pu1_OctetList));
    i4RetSts = ExtCommValidateExtCommunityType (u2Fsbgp4ExtCommInFilterCommVal);
    if (i4RetSts == EXT_COMM_FAILURE)
    {
        return EXT_COMM_FAILURE;
    }

    i4RetSts = ExtCommValidateExtCommunityVal (pFsbgp4AddExtCommVal);

    return i4RetSts;
}
