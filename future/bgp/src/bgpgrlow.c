/*****************************************************************************/
/* Copyright (C) 2009 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/* $Id: bgpgrlow.c,v 1.21 2017/02/09 14:07:40 siva Exp $                     */
/*****************************************************************************/
/*    FILE  NAME            : bgpgrlow.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        :                                                */
/*    MODULE NAME           : BGP                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 22 July 2009                                   */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           :  This file contains low level routines         */
/*                             for BGP GR                                    */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    22 July 2009           Initial Creation                        */
/*---------------------------------------------------------------------------*/
#ifndef BGPGRLOW_C
#define BGPGRLOW_C

# include  "lr.h"
# include  "fssnmp.h"
# include  "bgp4com.h"
# include  "fsmpbgcli.h"

/****************************************************************************
 Function    :  nmhGetFsbgp4GRAdminStatus
 Input       :  The Indices

                The Object 
                retValFsbgp4GRAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4GRAdminStatus (INT4 *pi4RetValFsbgp4GRAdminStatus)
{
    *pi4RetValFsbgp4GRAdminStatus = BGP4_GLB_GR_ADMIN_STATUS;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4GRRestartTimeInterval
 Input       :  The Indices

                The Object 
                retValFsbgp4GRRestartTimeInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4GRRestartTimeInterval (INT4 *pi4RetValFsbgp4GRRestartTimeInterval)
{

    *pi4RetValFsbgp4GRRestartTimeInterval = (INT4) BGP4_GLB_RESTART_TIME_INTERVAL;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4GRSelectionDeferralTimeInterval
 Input       :  The Indices

                The Object 
                retValFsbgp4GRSelectionDeferralTimeInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4GRSelectionDeferralTimeInterval (INT4
                                             *pi4RetValFsbgp4GRSelectionDeferralTimeInterval)
{

    *pi4RetValFsbgp4GRSelectionDeferralTimeInterval =
        (INT4) BGP4_GLB_SEL_DEF_TIMER_INTERVAL;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4GRStaleTimeInterval
 Input       :  The Indices

                The Object 
                retValFsbgp4GRStaleTimeInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4GRStaleTimeInterval (INT4 *pi4RetValFsbgp4GRStaleTimeInterval)
{

    *pi4RetValFsbgp4GRStaleTimeInterval = (INT4) BGP4_GLB_STALE_PATH_INTERVAL;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsbgp4GRMode
 Input       :  The Indices

                The Object 
                retValFsbgp4GRMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4GRMode (INT4 *pi4RetValFsbgp4GRMode)
{
    *pi4RetValFsbgp4GRMode = BGP4_GLB_RESTART_MODE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4RestartSupport
 Input       :  The Indices

                The Object 
                retValFsbgp4RestartSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4RestartSupport (INT4 *pi4RetValFsbgp4RestartSupport)
{
    *pi4RetValFsbgp4RestartSupport = BGP4_GLB_RESTART_SUPPORT;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4RestartStatus
 Input       :  The Indices

                The Object 
                retValFsbgp4RestartStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4RestartStatus (INT4 *pi4RetValFsbgp4RestartStatus)
{
    *pi4RetValFsbgp4RestartStatus = BGP4_GLB_RESTART_STATUS;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsbgp4RestartExitReason
 Input       :  The Indices

                The Object 
                retValFsbgp4RestartExitReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4RestartExitReason (INT4 *pi4RetValFsbgp4RestartExitReason)
{
    *pi4RetValFsbgp4RestartExitReason = BGP4_GLB_RESTART_EXIT_REASON;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4RestartReason
 Input       :  The Indices

                The Object 
                retValFsbgp4RestartReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4RestartReason (INT4 *pi4RetValFsbgp4RestartReason)
{
    *pi4RetValFsbgp4RestartReason = BGP4_GLB_RESTART_REASON;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4ForwardingPreservation
 Input       :  The Indices

                The Object 
                retValFsbgp4ForwardingPreservation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4ForwardingPreservation (INT4 *pi4RetValFsbgp4ForwardingPreservation)
{
    if (gBgpNode.u4GrAfiPreserved != ZERO)
    {
        *pi4RetValFsbgp4ForwardingPreservation = BGP4_TRUE;
    }
    else
    {
        *pi4RetValFsbgp4ForwardingPreservation = BGP4_FALSE;
    }

    return SNMP_SUCCESS;
}

/* LOW level set routines for all objects */
/****************************************************************************
 Function    :  nmhSetFsbgp4GRAdminStatus
 Input       :  The Indices

                The Object 
                setValFsbgp4GRAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4GRAdminStatus (INT4 i4SetValFsbgp4GRAdminStatus)
{
    tSupCapsInfo       *pSupCap = NULL;
    tTMO_SLL           *pCapsList = NULL;
    INT4                i4Status = ZERO;
    tBgp4PeerEntry     *pPeer = NULL;
    UINT4               u4TmpContext = 0;
    UINT4               u4Context;
    INT4                i4RetVal;
    INT4                i4SetStatus = SNMP_FAILURE;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
#ifdef L2RED_WANTED
    /* When BGP is registered with RM, BGP GR cannot be disabled */
    if (i4SetValFsbgp4GRAdminStatus == BGP4_DISABLE_GR)
    {
        return SNMP_FAILURE;
    }
#endif
    gBgpNode.u1GrAdminStatus = (UINT1) i4SetValFsbgp4GRAdminStatus;
    if (i4SetValFsbgp4GRAdminStatus == BGP4_ENABLE_GR)
    {
        BGP4_GLB_RESTART_MODE = BGP4_RECEIVING_MODE;
        if (BGP4_GLB_RESTART_SUPPORT == BGP4_GR_SUPPORT_NONE)
        {
            BGP4_GLB_RESTART_SUPPORT = BGP4_GR_SUPPORT_PLANNED;
        }
    }
#ifndef L2RED_WANTED
    else if (i4SetValFsbgp4GRAdminStatus == BGP4_DISABLE_GR)
    {
        BGP4_GLB_RESTART_MODE = BGP4_RESTART_MODE_NONE;
    }
#endif

    i4RetVal = BgpGetFirstActiveContextID (&u4TmpContext);

    while (i4RetVal == SNMP_SUCCESS)
    {
        i4SetStatus = (INT4) BgpSetContext (u4TmpContext);
        if (i4SetStatus == SNMP_FAILURE)
        {
            continue;
        }
        BGP4_GR_ADMIN_STATUS (u4TmpContext) =
            (UINT1) i4SetValFsbgp4GRAdminStatus;

        if (i4SetValFsbgp4GRAdminStatus == BGP4_ENABLE_GR)
        {
            /* Scan the peer list to add this feature to all the peers */
            TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4TmpContext), pPeer,
                          tBgp4PeerEntry *)
            {
                i4Status =
                    Bgp4SnmphGrCapabilityEnable (u4TmpContext,
                                                 BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeer));
                if (i4Status == SNMP_FAILURE)
                {
                    TMO_SLL_Delete (BGP4_PEERENTRY_HEAD (u4TmpContext),
                                    &pPeer->TsnNextpeer);
                    Bgp4ReleasePeerEntry (pPeer);
                    return SNMP_FAILURE;
                }

                pCapsList = BGP4_PEER_SUP_CAPS_LIST (pPeer);
                TMO_SLL_Scan (pCapsList, pSupCap, tSupCapsInfo *)
                {
                    if (pSupCap->SupCapability.u1CapCode !=
                        CAP_CODE_GRACEFUL_RESTART)
                    {
                        continue;
                    }
                    BGP4_PEER_SUP_CAPS_CONFIGURED_STATUS (pSupCap) = BGP4_TRUE;
                }
            }                    /* TMO_SLL_Scan */

            /* This block is to execute the restart procedure when
             * the restarting is in progress state */
            if (BGP4_RESTART_EXIT_REASON (u4TmpContext) ==
                BGP4_GR_EXIT_INPROGRESS)
            {
                /* Process the restarting speaker */
                Bgp4GRRestartProcess (u4TmpContext);
            }
            else                /* If not restarting change to receiving mode */
            {
                BGP4_RESTART_MODE (u4TmpContext) = BGP4_RECEIVING_MODE;
                if (BGP4_RESTART_SUPPORT (u4TmpContext) == BGP4_GR_SUPPORT_NONE)
                {
                    BGP4_RESTART_SUPPORT (u4TmpContext) =
                        BGP4_GR_SUPPORT_PLANNED;
                }
            }
        }
        else if (i4SetValFsbgp4GRAdminStatus == BGP4_DISABLE_GR)
        {

            /* Scan the peer list to add this feature to all the peers */
            TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4TmpContext), pPeer,
                          tBgp4PeerEntry *)
            {
                i4Status =
                    Bgp4SnmphGrCapabilityDisable (BGP4_PEER_REMOTE_ADDR_INFO
                                                  (pPeer));
                if (i4Status == SNMP_FAILURE)
                {
                    TMO_SLL_Delete (BGP4_PEERENTRY_HEAD (u4TmpContext),
                                    &pPeer->TsnNextpeer);
                    Bgp4ReleasePeerEntry (pPeer);
                    return SNMP_FAILURE;
                }
            }                    /* TMO_SLL_Scan */
            /* Since the capability is disabled */
            BGP4_RESTART_MODE (u4TmpContext) = BGP4_RESTART_MODE_NONE;
            BGP4_RESTART_SUPPORT (u4TmpContext) = BGP4_GR_SUPPORT_NONE;
        }

        BgpReleaseContext ();
        u4Context = u4TmpContext;
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4TmpContext);
    }
    if (
#ifndef L2RED_WANTED
           (i4SetValFsbgp4GRAdminStatus == BGP4_DISABLE_GR) &&
#endif
           (IssGetCsrRestoresFlag (BGP_MODULE_ID) != BGP4_TRUE))
    {
#ifdef MSR_WANTED
        FlashRemoveFile (BGP4_GR_CONF);
        FlashRemoveFile (BGP4_CSR_CONFIG_FILE);
#endif
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4GRAdminStatus, u4SeqNum,
                          FALSE, BgpLock, BgpUnLock, 0, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsbgp4GRAdminStatus));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsbgp4GRRestartTimeInterval
 Input       :  The Indices

                The Object 
                setValFsbgp4GRRestartTimeInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4GRRestartTimeInterval (INT4 i4SetValFsbgp4GRRestartTimeInterval)
{
    UINT4               u4TmpContext = 0;
    UINT4               u4Context;
    INT4                i4RetVal;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    BGP4_GLB_RESTART_TIME_INTERVAL = (UINT4) i4SetValFsbgp4GRRestartTimeInterval;

    i4RetVal = BgpGetFirstActiveContextID (&u4TmpContext);

    while (i4RetVal == SNMP_SUCCESS)
    {
        BGP4_RESTART_TIME_INTERVAL (u4TmpContext) =
            (UINT4) i4SetValFsbgp4GRRestartTimeInterval;
        /* this is done to set gr mib objects in each context */

        u4Context = u4TmpContext;
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4TmpContext);
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4GRRestartTimeInterval,
                          u4SeqNum, FALSE, BgpLock, BgpUnLock, 0, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                      i4SetValFsbgp4GRRestartTimeInterval));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsbgp4GRSelectionDeferralTimeInterval
 Input       :  The Indices

                The Object 
                setValFsbgp4GRSelectionDeferralTimeInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4GRSelectionDeferralTimeInterval (INT4
                                             i4SetValFsbgp4GRSelectionDeferralTimeInterval)
{
    UINT4               u4TmpContext = 0;
    UINT4               u4Context;
    INT4                i4RetVal;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    BGP4_GLB_SEL_DEF_TIMER_INTERVAL =
        (UINT4) i4SetValFsbgp4GRSelectionDeferralTimeInterval;
    i4RetVal = BgpGetFirstActiveContextID (&u4TmpContext);

    while (i4RetVal == SNMP_SUCCESS)
    {
        BGP4_SEL_DEF_TIMER_INTERVAL (u4TmpContext) =
            (UINT4) i4SetValFsbgp4GRSelectionDeferralTimeInterval;
        u4Context = u4TmpContext;
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4TmpContext);
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,
                          FsMIBgp4GRSelectionDeferralTimeInterval, u4SeqNum,
                          FALSE, BgpLock, BgpUnLock, 0, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                      i4SetValFsbgp4GRSelectionDeferralTimeInterval));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsbgp4GRStaleTimeInterval
 Input       :  The Indices

                The Object 
                setValFsbgp4GRStaleTimeInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4GRStaleTimeInterval (INT4 i4SetValFsbgp4GRStaleTimeInterval)
{
    UINT4               u4TmpContext = 0;
    UINT4               u4Context;
    INT4                i4RetVal;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    BGP4_GLB_STALE_PATH_INTERVAL = (UINT4) i4SetValFsbgp4GRStaleTimeInterval;
    i4RetVal = BgpGetFirstActiveContextID (&u4TmpContext);

    while (i4RetVal == SNMP_SUCCESS)
    {

        BGP4_STALE_PATH_INTERVAL (u4TmpContext) =
            (UINT4) i4SetValFsbgp4GRStaleTimeInterval;

        u4Context = u4TmpContext;
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4TmpContext);
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4GRStaleTimeInterval, u4SeqNum,
                          FALSE, BgpLock, BgpUnLock, 0, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsbgp4GRStaleTimeInterval));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsbgp4RestartSupport
 Input       :  The Indices

                The Object 
                setValFsbgp4RestartSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4RestartSupport (INT4 i4SetValFsbgp4RestartSupport)
{
    UINT4               u4TmpContext = 0;
    UINT4               u4Context;
    INT4                i4RetVal;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    if ((i4SetValFsbgp4RestartSupport == BGP4_GR_SUPPORT_NONE) &&
        (BGP4_GLB_GR_ADMIN_STATUS == BGP4_ENABLE_GR))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "Restart Support None cannot be configured when GR Admin status is enabled.\n");
        return SNMP_FAILURE;
    }

    BGP4_GLB_RESTART_SUPPORT = (UINT1) i4SetValFsbgp4RestartSupport;

    i4RetVal = BgpGetFirstActiveContextID (&u4TmpContext);

    while (i4RetVal == SNMP_SUCCESS)
    {
        BGP4_RESTART_SUPPORT (u4TmpContext) =
            (UINT1) i4SetValFsbgp4RestartSupport;

        u4Context = u4TmpContext;
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4TmpContext);

    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4RestartSupport, u4SeqNum,
                          FALSE, BgpLock, BgpUnLock, 0, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsbgp4RestartSupport));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsbgp4RestartReason
 Input       :  The Indices

                The Object 
                setValFsbgp4RestartReason
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4RestartReason (INT4 i4SetValFsbgp4RestartReason)
{
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    BGP4_GLB_RESTART_REASON = (UINT1) i4SetValFsbgp4RestartReason;

#ifdef BGP_DEBUG
    if (BGP4_GLB_RESTART_REASON == BGP4_GR_REASON_UNKNOWN)
    {
#ifdef ISS_WANTED
        if (MsrGetRestorationStatus () != ISS_TRUE)
        {
#endif
            Bgp4GRUnPlannedShutdownProcess ();
#ifdef ISS_WANTED
        }
#endif
    }
#endif /* BGP_DEBUG */
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4RestartReason, u4SeqNum,
                          FALSE, BgpLock, BgpUnLock, 0, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsbgp4RestartReason));

    return SNMP_SUCCESS;
}

/* LOW level Test routines for all objects */
/****************************************************************************
 Function    :  nmhTestv2Fsbgp4GRAdminStatus
 Input       :  The Indices

                The Object 
                testValFsbgp4GRAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4GRAdminStatus (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsbgp4GRAdminStatus)
{
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

#ifdef L2RED_WANTED
    /* When RM is enabled, BGP GR cannot be disabled */
    if (i4TestValFsbgp4GRAdminStatus == BGP4_DISABLE_GR)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    else if (i4TestValFsbgp4GRAdminStatus == BGP4_ENABLE_GR)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
#else
    if ((i4TestValFsbgp4GRAdminStatus == BGP4_ENABLE_GR) ||
        (i4TestValFsbgp4GRAdminStatus == BGP4_DISABLE_GR))
    {
        return SNMP_SUCCESS;
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
              BGP4_MOD_NAME,
              "\tERROR - BGP-GR capability cannot be enabled \r\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4GRRestartTimeInterval
 Input       :  The Indices

                The Object 
                testValFsbgp4GRRestartTimeInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4GRRestartTimeInterval (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFsbgp4GRRestartTimeInterval)
{
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        return SNMP_FAILURE;
    }

    if ((i4TestValFsbgp4GRRestartTimeInterval < BGP4_MIN_RESTART_TIME) ||
        (i4TestValFsbgp4GRRestartTimeInterval > BGP4_MAX_RESTART_TIME))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tInvalid Restart Interval \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4GRSelectionDeferralTimeInterval
 Input       :  The Indices

                The Object 
                testValFsbgp4GRSelectionDeferralTimeInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4GRSelectionDeferralTimeInterval (UINT4 *pu4ErrorCode,
                                                INT4
                                                i4TestValFsbgp4GRSelectionDeferralTimeInterval)
{
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        return SNMP_FAILURE;
    }

    if ((i4TestValFsbgp4GRSelectionDeferralTimeInterval <
         BGP4_MIN_SEL_DEF_TIME) ||
        (i4TestValFsbgp4GRSelectionDeferralTimeInterval >
         BGP4_MAX_SEL_DEF_TIME))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tInvalid Selection deferral timer Interval \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4GRStaleTimeInterval
 Input       :  The Indices

                The Object 
                testValFsbgp4GRStaleTimeInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4GRStaleTimeInterval (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFsbgp4GRStaleTimeInterval)
{

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        return SNMP_FAILURE;
    }

    if ((i4TestValFsbgp4GRStaleTimeInterval < BGP4_MIN_STALE_TIME) ||
        (i4TestValFsbgp4GRStaleTimeInterval > BGP4_MAX_STALE_TIME))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tInvalid Stale Timer Interval \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4RestartSupport
 Input       :  The Indices

                The Object 
                testValFsbgp4RestartSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4RestartSupport (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsbgp4RestartSupport)
{
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        return SNMP_FAILURE;
    }

    if ((i4TestValFsbgp4RestartSupport != BGP4_GR_SUPPORT_NONE) &&
        (i4TestValFsbgp4RestartSupport != BGP4_GR_SUPPORT_PLANNED) &&
        (i4TestValFsbgp4RestartSupport != BGP4_GR_SUPPORT_BOTH))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tInvalid Restart Support \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((BGP4_GLB_GR_ADMIN_STATUS != BGP4_DISABLE_GR) &&
        (i4TestValFsbgp4RestartSupport == BGP4_GR_SUPPORT_NONE))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "Restart Support None cannot be configured when GR Admin status is enabled.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4RestartReason
 Input       :  The Indices

                The Object 
                testValFsbgp4RestartReason
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4RestartReason (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsbgp4RestartReason)
{
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tBGP Task not initialized\n");
        return SNMP_FAILURE;
    }

    if ((i4TestValFsbgp4RestartReason != BGP4_GR_REASON_UNKNOWN) &&
        (i4TestValFsbgp4RestartReason != BGP4_GR_REASON_RESTART) &&
        (i4TestValFsbgp4RestartReason != BGP4_GR_REASON_UPGRADE))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tInvalid Restart Reason \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low level dependency routines for all objects */
/****************************************************************************
 Function    :  nmhDepv2Fsbgp4GRAdminStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4GRAdminStatus (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4GRRestartTimeInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4GRRestartTimeInterval (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4GRSelectionDeferralTimeInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4GRSelectionDeferralTimeInterval (UINT4 *pu4ErrorCode,
                                               tSnmpIndexList * pSnmpIndexList,
                                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4GRStaleTimeInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4GRStaleTimeInterval (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4RestartSupport
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4RestartSupport (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4RestartReason
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4RestartReason (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

#endif /* BGPGRLOW_C */
