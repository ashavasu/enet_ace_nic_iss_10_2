/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: bgrbifun.c,v 1.24 2017/09/15 06:19:55 siva Exp $
 * *
 * * Description: BGP Rib handling routines
 * *********************************************************************/
#ifndef BGRBIFUN_C
#define BGRBIFUN_C

#include "bgp4com.h"
#include "bgp4trie.h"

/* Becareful - the order of the address family masks of the following 
 * structures MUST match
 */
PRIVATE UINT4       gau4NegAsafiMask[] = {
    CAP_NEG_IPV4_UNI_MASK,
#ifdef L3VPN
    CAP_NEG_LBL_IPV4_MASK,
#endif
#ifdef VPLSADS_WANTED
    CAP_MP_L2VPN_VPLS,
#endif
#ifdef BGP4_IPV6_WANTED
    CAP_NEG_IPV6_UNI_MASK,
#endif
#ifdef L3VPN
    CAP_NEG_VPN4_UNI_MASK,
#endif
#ifdef EVPN_WANTED
    CAP_NEG_L2VPN_EVPN_MASK
#endif
};

PRIVATE UINT4       gau4AsafiMask[] = {
    CAP_MP_IPV4_UNICAST,
#ifdef L3VPN
    CAP_MP_LABELLED_IPV4,
#endif
#ifdef VPLSADS_WANTED
    CAP_MP_L2VPN_VPLS,
#endif
#ifdef BGP4_IPV6_WANTED
    CAP_MP_IPV6_UNICAST,
#endif
#ifdef L3VPN
    CAP_MP_VPN4_UNICAST,
#endif
#ifdef EVPN_WANTED
    CAP_NEG_L2VPN_EVPN_MASK
#endif
};

/*****************************************************************************/
/* Function Name : Bgp4RibhFreeRibtree                                       */
/* Description   : This function is used to remove the entire RIB tree. This */
/*                 function is throttled and will process maximum of         */
/*                 BGP4_MAX_DELRTS2PROCESS routes. The best route will be    */
/*                 removed from FIB.                                         */
/* Input(s)      : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return        : BGP4_TRUE  - if more routes remains to be processed.      */
/*               : BGP4_FALSE - if no more routes available for processing.  */
/*                 BGP4_RELINQUISH - if external events requires CPU         */
/*****************************************************************************/
INT4
Bgp4RibhFreeRibtree (UINT4 u4Context)
{
    /* Variable Declarations */
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pNextRtProfile = NULL;
    VOID               *pRibNode = NULL;
    PRIVATE UINT4       u4DelMask = CAP_MP_IPV4_UNICAST;
    UINT4               u4DelRtCnt = 0;
    INT4                i4Status = BGP4_FAILURE;
    UINT1               u1IsNotBestRoute = BGP4_FALSE;
    UINT1               u1IsBestRoute = BGP4_FALSE;
    UINT1               u1IsMpathRoute = BGP4_FALSE;

#ifdef L3VPN
    tTMO_SLL            DummyFeasRtList;
    tTMO_SLL            PeerWithRtList;
    tBgp4PeerEntry     *pPeerentry = NULL;
    tLinkNode          *pRtLink = NULL;
    tLinkNode          *pTempLinkNode = NULL;

    TMO_SLL_Init (&DummyFeasRtList);
    TMO_SLL_Init (&PeerWithRtList);

#endif

    if (BGP4_GLOBAL_STATE (u4Context) == BGP4_GLOBAL_READY)
    {
        return BGP4_FALSE;
    }

    /* Get First route from IPV4 RIB */
#ifdef L3VPN
    if (u4DelMask == CAP_MP_VPN4_UNICAST)
    {
        pRibNode = BGP4_VPN4_RIB_TREE (u4Context);    /* vpn-rib */
    }
#endif
    Bgp4GetRibNode (u4Context, u4DelMask, &pRibNode);
    if (pRibNode == NULL)
    {
        return BGP4_FALSE;
    }

    i4Status = Bgp4RibhGetFirstEntry (u4DelMask, &pRtProfile, &pRibNode, TRUE);
    if (i4Status == BGP4_FAILURE)
    {
        /* Update u4DelMask */
        switch (u4DelMask)
        {
            case CAP_MP_IPV4_UNICAST:
                /* IPV4 is over. move to IPV6 */
#ifdef BGP4_IPV6_WANTED
                u4DelMask = CAP_MP_IPV6_UNICAST;
                Bgp4GetRibNode (u4Context, u4DelMask, &pRibNode);
                if (pRibNode == NULL)
                {
                    return BGP4_FALSE;
                }
                i4Status = Bgp4RibhGetFirstEntry (u4DelMask,
                                                  &pRtProfile, &pRibNode, TRUE);
                if (i4Status == BGP4_SUCCESS)
                {
                    /* Route Found in IpV6, so delete it */
                    break;
                }
#endif /* BGP4_IPV6_WANTED */
#ifdef L3VPN
                u4DelMask = CAP_MP_VPN4_UNICAST;
                pRibNode = BGP4_VPN4_RIB_TREE (u4Context);    /* vpn-rib */
                i4Status = Bgp4RibhGetFirstEntry (u4DelMask,
                                                  &pRtProfile, &pRibNode, TRUE);
                if (i4Status == BGP4_SUCCESS)
                {
                    /* Route Found in VPN4, so delete it */
                    break;
                }
#endif
#ifdef VPLSADS_WANTED
                u4DelMask = CAP_MP_L2VPN_VPLS;
                pRibNode = BGP4_VPLS_RIB_TREE (u4Context);    /* L2VPN-VPLS rib */
                i4Status = Bgp4RibhGetFirstEntry (u4DelMask,
                                                  &pRtProfile, &pRibNode, TRUE);
                if (i4Status == BGP4_SUCCESS)
                {
                    /* Route Found in L2VPN-VPLS, so delete it */
                    break;
                }
#endif
#ifdef EVPN_WANTED
                u4DelMask = CAP_MP_L2VPN_EVPN;
                pRibNode = BGP4_EVPN_RIB_TREE (u4Context);    /* EVPN rib */
                i4Status = Bgp4RibhGetFirstEntry (u4DelMask,
                                                  &pRtProfile, &pRibNode, TRUE);
                if (i4Status == BGP4_SUCCESS)
                {
                    /* Route Found in EVPN, so delete it */
                    break;
                }
#endif
                u4DelMask = CAP_MP_IPV4_UNICAST;
                return BGP4_FALSE;

#ifdef BGP4_IPV6_WANTED
            case CAP_MP_IPV6_UNICAST:
                /* IPV6 is over. move to VPN4 */
#ifdef L3VPN
                u4DelMask = CAP_MP_VPN4_UNICAST;
                pRibNode = BGP4_VPN4_RIB_TREE (u4Context);    /* vpn-rib */
                i4Status = Bgp4RibhGetFirstEntry (u4DelMask,
                                                  &pRtProfile, &pRibNode, TRUE);
                if (i4Status == BGP4_SUCCESS)
                {
                    /* Route Found in VPN4, so delete it */
                    break;
                }
#endif
#ifdef VPLSADS_WANTED
                u4DelMask = CAP_MP_L2VPN_VPLS;
                pRibNode = BGP4_VPLS_RIB_TREE (u4Context);    /* L2VPN-VPLS rib */
                i4Status = Bgp4RibhGetFirstEntry (u4DelMask,
                                                  &pRtProfile, &pRibNode, TRUE);
                if (i4Status == BGP4_SUCCESS)
                {
                    /* Route Found in L2VPN-VPLS, so delete it */
                    break;
                }
#endif
#ifdef EVPN_WANTED
                u4DelMask = CAP_MP_L2VPN_EVPN;
                pRibNode = BGP4_EVPN_RIB_TREE (u4Context);    /* EVPN rib */
                i4Status = Bgp4RibhGetFirstEntry (u4DelMask,
                                                  &pRtProfile, &pRibNode, TRUE);
                if (i4Status == BGP4_SUCCESS)
                {
                    /* Route Found in EVPN, so delete it */
                    break;
                }
#endif
                u4DelMask = CAP_MP_IPV4_UNICAST;
                return BGP4_FALSE;
#endif /* IPV6_WANTED */

#ifdef L3VPN
            case CAP_MP_VPN4_UNICAST:
                /* VPN4 is over, then move to L2VPN-VPLS in any */
#ifdef VPLSADS_WANTED
                u4DelMask = CAP_MP_L2VPN_VPLS;
                pRibNode = BGP4_VPLS_RIB_TREE (u4Context);    /* L2VPN-VPLS rib */
                i4Status = Bgp4RibhGetFirstEntry (u4DelMask,
                                                  &pRtProfile, &pRibNode, TRUE);
                if (i4Status == BGP4_SUCCESS)
                {
                    /* Route Found in L2VPN-VPLS, so delete it */
                    break;
                }
#endif
#ifdef EVPN_WANTED
                u4DelMask = CAP_MP_L2VPN_EVPN;
                pRibNode = BGP4_EVPN_RIB_TREE (u4Context);    /* EVPN rib */
                i4Status = Bgp4RibhGetFirstEntry (u4DelMask,
                                                  &pRtProfile, &pRibNode, TRUE);
                if (i4Status == BGP4_SUCCESS)
                {
                    /* Route Found in EVPN, so delete it */
                    break;
                }
#endif
                u4DelMask = CAP_MP_IPV4_UNICAST;
                return BGP4_FALSE;
#endif
#ifdef VPLSADS_WANTED
            case CAP_MP_L2VPN_VPLS:
                /* L2VPN-VPLS routes also over, then return BGP4_FALSE */
#ifdef EVPN_WANTED
                u4DelMask = CAP_MP_L2VPN_EVPN;
                pRibNode = BGP4_EVPN_RIB_TREE (u4Context);    /* EVPN rib */
                i4Status = Bgp4RibhGetFirstEntry (u4DelMask,
                                                  &pRtProfile, &pRibNode, TRUE);
                if (i4Status == BGP4_SUCCESS)
                {
                    /* Route Found in EVPN, so delete it */
                    break;
                }
#endif
                u4DelMask = CAP_MP_IPV4_UNICAST;
                return BGP4_FALSE;
#endif
#ifdef EVPN_WANTED
            case CAP_MP_L2VPN_EVPN:
                /* L2VPN-EVPN routes also over, then return BGP4_FALSE */
                u4DelMask = CAP_MP_IPV4_UNICAST;
                return BGP4_FALSE;
#endif
            default:
                u4DelMask = CAP_MP_IPV4_UNICAST;
                return BGP4_FALSE;
        }
    }

    for (;;)
    {
        /* For the First Route check for the validity of the route and set
         * the u1IsNotBestRoute flag accordingly. This is to ensure that
         * the route is properly removed from RCVD_PA_DB
         */
        u1IsNotBestRoute = ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_HISTORY) ?
                            BGP4_HISTORY :
                            ((BGP4_IS_ROUTE_VALID (pRtProfile) == BGP4_TRUE) ?
                             BGP4_FALSE : BGP4_TRUE));
        BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_HISTORY);
        u1IsBestRoute = (BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_BEST) ?
            BGP4_TRUE : BGP4_FALSE;
        u1IsMpathRoute =
            (BGP4_RT_GET_EXT_FLAGS (pRtProfile) & BGP4_RT_MULTIPATH) ? BGP4_TRUE
            : BGP4_FALSE;
        for (;;)
        {
            pNextRtProfile = BGP4_RT_NEXT (pRtProfile);

            BGP4_RT_REF_COUNT (pRtProfile)++;

            /* Delete route in RIB */
            Bgp4RibhDelRtEntry (pRtProfile, pRibNode,
                                u4Context, u1IsNotBestRoute);

            if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_IN_FIB_UPD_LIST)
                == BGP4_RT_IN_FIB_UPD_LIST)
            {
                /* Route not yet added to the FIB. */
                if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_WITHDRAWN) !=
                    BGP4_RT_WITHDRAWN)
                {
                    BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_IN_FIB_UPD_LIST);
                    Bgp4DshDelinkRouteFromList (BGP4_FIB_UPD_LIST (u4Context),
                                                pRtProfile,
                                                BGP4_FIB_UPD_LIST_INDEX);
                }
            }
            else
            {
                /* Delete the route in FIB if the route was the best route. */
#ifdef L3VPN
                if ((BGP4_RT_AFI_INFO (pRtProfile) == BGP4_INET_AFI_IPV4) &&
                    (BGP4_RT_SAFI_INFO (pRtProfile) ==
                     BGP4_INET_SAFI_VPNV4_UNICAST))
                {
                    /* set the flag for the route to be withdrawn */
                    Bgp4Vpnv4SetFlagsForAllVrf (pRtProfile,
                                                (BGP4_RT_VRF_WITHDRAWN |
                                                 BGP4_RT_VRF_ADVT_NOTTO_EXTERNAL));
                    BGP4_TRC_ARG2 (NULL,
                                   BGP4_TRC_FLAG, BGP4_DETAIL_TRC,
                                   BGP4_MOD_NAME,
                                   "\tCalling Bgp4Vpnv4DelRtFromVrfs from Func[%s] Line[%d]\n",
                                   __func__, __LINE__);
                    Bgp4Vpnv4DelRtFromVrfs (pRtProfile);
                    if (u4Context != BGP4_DFLT_VRFID)
                    {
                        /* Add the RtProfile to the Withdrawn routeList for 
                         * sending to PE peer of Default VRF */
                        Bgp4DshAddRouteToList (&PeerWithRtList, pRtProfile, 0);
                    }
                }
#endif
                if (((u1IsBestRoute == BGP4_TRUE) || (u1IsMpathRoute == TRUE))
                    && ((BGP4_RT_GET_EXT_FLAGS (pRtProfile) & BGP4_RT_IN_RTM) ==
                        BGP4_RT_IN_RTM))
                {
                    /* Route already added to FIB. Need to remove it. */
                    BGP4_RT_SET_FLAG (pRtProfile, (BGP4_RT_WITHDRAWN |
                                                   BGP4_RT_ADVT_NOTTO_EXTERNAL));
                    BGP4_TRC_ARG2 (NULL,
                                   BGP4_TRC_FLAG, BGP4_DETAIL_TRC,
                                   BGP4_MOD_NAME,
                                   "\tCalling Bgp4IgphUpdateRouteInFIB from Func[%s] Line[%d]\n",
                                   __func__, __LINE__);
                    Bgp4IgphUpdateRouteInFIB (pRtProfile);
                }
            }
            Bgp4DshReleaseRtInfo (pRtProfile);

            u4DelRtCnt++;

            if (pNextRtProfile == NULL)
            {
                break;
            }

            pRtProfile = pNextRtProfile;
            pNextRtProfile = NULL;
            /* For routes in node other than first route, set the
             * u1IsNotBestRoute as BGP4_TRUE/BGP4_HISTORY and this ensure
             * that the routes are removed from the RCVD_PA_DB correctly. */
            u1IsNotBestRoute = ((BGP4_RT_GET_FLAGS (pRtProfile) &
                                 BGP4_RT_HISTORY) ? BGP4_HISTORY : BGP4_TRUE);
            BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_HISTORY);
            u1IsBestRoute = (BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_BEST) ?
                BGP4_TRUE : BGP4_FALSE;
            u1IsMpathRoute =
                (BGP4_RT_GET_EXT_FLAGS (pRtProfile) & BGP4_RT_MULTIPATH) ?
                BGP4_TRUE : BGP4_FALSE;
        }
#ifdef L3VPN
        /* When during shuting of the BGP session with CE peers, WITHDRAW
         * messages has to be sent to the PE peer of the default VRF also.
         * Hence the route profiles are also advertised as withdrawn routes
         * to the PE peers. */
        if ((TMO_SLL_Count (&PeerWithRtList) != 0) &&
            (gBgpCxtNode[BGP4_DFLT_VRFID] != NULL))
        {
            TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (BGP4_DFLT_VRFID), pPeerentry,
                          tBgp4PeerEntry *)
            {
                if (BGP4_PEER_STATE (pPeerentry) != BGP4_ESTABLISHED_STATE)
                {
                    /* Connection not in established state. So no need to
                     * advertise to this peer. */
                    continue;
                }
                if (BGP4_VPN4_PEER_ROLE (pPeerentry) != BGP4_VPN4_PE_PEER)
                {
                    /* The Peer is not a PE Peer. Hence the route from CE
                     * need not be advertised to the peer */
                    continue;
                }

                /* Route can be advertised to this peer. */
                Bgp4PeerSendRoutesToPeerTxQ (pPeerentry, &DummyFeasRtList,
                                             &PeerWithRtList, BGP4_TRUE);
            }

            BGP_SLL_DYN_Scan (&PeerWithRtList, pRtLink, pTempLinkNode,
                              tLinkNode *)
            {
                Bgp4DshRemoveNodeFromList (&PeerWithRtList, pRtLink);
            }

            Bgp4DshReleaseList (&DummyFeasRtList, 0);
        }
#endif

        pRtProfile = NULL;
#ifdef L3VPN
        if (u4DelMask == CAP_MP_VPN4_UNICAST)
        {
            pRibNode = BGP4_VPN4_RIB_TREE (u4Context);    /* vpn-rib */
        }
#endif

        Bgp4GetRibNode (u4Context, u4DelMask, &pRibNode);
        if (pRibNode == NULL)
        {
            return BGP4_FALSE;
        }
        i4Status = Bgp4RibhGetFirstEntry (u4DelMask, &pRtProfile, &pRibNode,
                                          TRUE);
        if (i4Status == BGP4_FAILURE)
        {
            /* Update u4DelMask */
            switch (u4DelMask)
            {
                case CAP_MP_IPV4_UNICAST:
                    /* IPV4 is over. move to IPV6 */
#ifdef BGP4_IPV6_WANTED
                    u4DelMask = CAP_MP_IPV6_UNICAST;
                    Bgp4GetRibNode (u4Context, u4DelMask, &pRibNode);
                    if (pRibNode == NULL)
                    {
                        return BGP4_FALSE;
                    }
                    i4Status = Bgp4RibhGetFirstEntry (u4DelMask,
                                                      &pRtProfile, &pRibNode,
                                                      TRUE);
                    if (i4Status == BGP4_SUCCESS)
                    {
                        /* Route Found in IpV6, so delete it */
                        break;
                    }
#endif
#ifdef L3VPN
                    /* IPV6 is over. move to VPN4 */
                    u4DelMask = CAP_MP_VPN4_UNICAST;
                    pRibNode = BGP4_VPN4_RIB_TREE (u4Context);    /* vpn-rib */
                    i4Status = Bgp4RibhGetFirstEntry (u4DelMask,
                                                      &pRtProfile, &pRibNode,
                                                      TRUE);
                    if (i4Status == BGP4_SUCCESS)
                    {
                        /*Route Found in VPN4, so delete it */
                        break;
                    }
#endif
#ifdef VPLSADS_WANTED
                    /*IPv6 and VPN4 is over. move to L2VPN-VPLS */
                    u4DelMask = CAP_MP_L2VPN_VPLS;
                    pRibNode = BGP4_VPLS_RIB_TREE (u4Context);    /* L2VPN-VPLS rib */
                    i4Status = Bgp4RibhGetFirstEntry (u4DelMask,
                                                      &pRtProfile, &pRibNode,
                                                      TRUE);
                    if (i4Status == BGP4_SUCCESS)
                    {
                        /* Route Found in L2VPN-VPLS, so delete it */
                        break;
                    }
#endif
#ifdef EVPN_WANTED
                    u4DelMask = CAP_MP_L2VPN_EVPN;
                    pRibNode = BGP4_EVPN_RIB_TREE (u4Context);    /* EVPN rib */
                    i4Status = Bgp4RibhGetFirstEntry (u4DelMask,
                                                      &pRtProfile, &pRibNode,
                                                      TRUE);
                    if (i4Status == BGP4_SUCCESS)
                    {
                        /* Route Found in EVPN, so delete it */
                        break;
                    }
#endif
                    u4DelMask = CAP_MP_IPV4_UNICAST;
                    return BGP4_FALSE;
#ifdef BGP4_IPV6_WANTED
                case CAP_MP_IPV6_UNICAST:
                    /* IPV6 routes also over, then return to VPNv4 if any */
#ifdef L3VPN
                    u4DelMask = CAP_MP_VPN4_UNICAST;
                    pRibNode = BGP4_VPN4_RIB_TREE (u4Context);
                    i4Status = Bgp4RibhGetFirstEntry (u4DelMask,
                                                      &pRtProfile, &pRibNode,
                                                      TRUE);
                    if (i4Status == BGP4_SUCCESS)
                    {
                        /*Route Found in VPnv4 RIB, so delete it */
                        break;
                    }
#endif
#ifdef VPLSADS_WANTED
                    /*IPv6 and VPN4 is over. move to L2VPN-VPLS */
                    u4DelMask = CAP_MP_L2VPN_VPLS;
                    pRibNode = BGP4_VPLS_RIB_TREE (u4Context);    /* L2VPN-VPLS rib */
                    i4Status = Bgp4RibhGetFirstEntry (u4DelMask,
                                                      &pRtProfile, &pRibNode,
                                                      TRUE);
                    if (i4Status == BGP4_SUCCESS)
                    {
                        /* Route Found in L2VPN-VPLS, so delete it */
                        break;
                    }
#endif /*VPLSADS_WANTED */
                    u4DelMask = CAP_MP_IPV4_UNICAST;
                    return BGP4_FALSE;
#endif /* BGP4_IPV6_WANTED */

#ifdef L3VPN
                case CAP_MP_VPN4_UNICAST:
                    /* VPNv4 routes also over, then return L2VPN-VPLS in any */
#ifdef VPLSADS_WANTED
                    u4DelMask = CAP_MP_L2VPN_VPLS;
                    pRibNode = BGP4_VPLS_RIB_TREE (u4Context);    /* L2VPN-VPLS rib */
                    i4Status = Bgp4RibhGetFirstEntry (u4DelMask,
                                                      &pRtProfile, &pRibNode,
                                                      TRUE);
                    if (i4Status == BGP4_SUCCESS)
                    {
                        /* Route Found in L2VPN-VPLS, so delete it */
                        break;
                    }
#endif /*VPLSADS_WANTED */
                    u4DelMask = CAP_MP_IPV4_UNICAST;
                    return BGP4_FALSE;
#endif /*L3VPN */
#ifdef VPLSADS_WANTED
                case CAP_MP_L2VPN_VPLS:
                    /* L2VPN-VPLS routes also over, then return BGP4_FALSE */
                    u4DelMask = CAP_MP_IPV4_UNICAST;
                    return BGP4_FALSE;
#endif
#ifdef EVPN_WANTED
                case CAP_MP_L2VPN_EVPN:
                    /* EVPN routes also over, then return BGP4_FALSE */
                    u4DelMask = CAP_MP_IPV4_UNICAST;
                    return BGP4_FALSE;
#endif
                default:
                    u4DelMask = CAP_MP_IPV4_UNICAST;
                    return BGP4_FALSE;
            }
        }

        if (u4DelRtCnt >= BGP4_MAX_DELRTS2PROCESS)
        {
            if (Bgp4IsCpuNeeded () == BGP4_TRUE)
            {
                /* External events requires CPU. Relinquish CPU. */
                return (BGP4_RELINQUISH);
            }
            break;
        }
    }
    return BGP4_TRUE;
}

/*****************************************************************************/
/* Function Name : Bgp4RibhClearRIB                                          */
/* Description   : This function is removes all the routes present in the    */
/*                 RIB.                                                      */
/* Input(s)      : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4RibhClearRIB (UINT4 u4Context)
{
    /* Variable Declarations */
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pNextRtProfile = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4Status = BGP4_FAILURE;
    UINT4               u4AsafiMask = CAP_MP_IPV4_UNICAST;
    UINT1               u1IsNotBestRoute = BGP4_FALSE;
    UINT1               u1IsBestRoute = BGP4_FALSE;
    UINT1               u1IsMpathRoute = BGP4_FALSE;
#ifdef L3VPN
    tTMO_SLL            DummyFeasRtList;
    tTMO_SLL            PeerWithRtList;
    tBgp4PeerEntry     *pPeerentry = NULL;
    tLinkNode          *pRtLink = NULL;
    tLinkNode          *pTempLinkNode = NULL;

    TMO_SLL_Init (&DummyFeasRtList);
    TMO_SLL_Init (&PeerWithRtList);

#endif

    for (;;)
    {
        pRibNode = NULL;
        pRtProfile = NULL;
        /* Get First route From RIB */
#ifdef L3VPN
        if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
        {
            pRibNode = BGP4_VPN4_RIB_TREE (u4Context);    /* vpn-rib */
        }
#endif
        Bgp4GetRibNode (u4Context, u4AsafiMask, &pRibNode);
        if (pRibNode != NULL)
        {
            i4Status =
                Bgp4RibhGetFirstEntry (u4AsafiMask, &pRtProfile, &pRibNode,
                                       TRUE);
        }
        if (i4Status == BGP4_FAILURE)
        {
            switch (u4AsafiMask)
            {
                case CAP_MP_IPV4_UNICAST:
                    /* IPV4 is over. move to IPV6 */
#ifdef BGP4_IPV6_WANTED
                    u4AsafiMask = CAP_MP_IPV6_UNICAST;
                    Bgp4GetRibNode (u4Context, u4AsafiMask, &pRibNode);
                    if (pRibNode != NULL)
                    {
                        i4Status = Bgp4RibhGetFirstEntry (u4AsafiMask,
                                                          &pRtProfile,
                                                          &pRibNode, TRUE);
                    }
                    if (i4Status == BGP4_SUCCESS)
                    {
                        /* IPV6 route is found in RIB */
                        break;
                    }
#endif
#ifdef L3VPN
                    u4AsafiMask = CAP_MP_VPN4_UNICAST;
                    pRibNode = BGP4_VPN4_RIB_TREE (u4Context);
                    i4Status = Bgp4RibhGetFirstEntry (u4AsafiMask,
                                                      &pRtProfile,
                                                      &pRibNode, TRUE);
                    if (i4Status == BGP4_SUCCESS)
                    {
                        /* VPN4 route is found in VPN-RIB */
                        break;
                    }
#endif
#ifdef VPLSADS_WANTED
                    u4AsafiMask = CAP_MP_L2VPN_VPLS;
                    pRibNode = BGP4_VPLS_RIB_TREE (u4Context);
                    i4Status = Bgp4RibhGetFirstEntry (u4AsafiMask,
                                                      &pRtProfile,
                                                      &pRibNode, TRUE);
                    if (i4Status == BGP4_SUCCESS)
                    {
                        /* VPLS route is found in VPLS-RIB */
                        break;
                    }
#endif
#ifdef EVPN_WANTED
                    u4AsafiMask = CAP_MP_L2VPN_EVPN;
                    pRibNode = BGP4_EVPN_RIB_TREE (u4Context);
                    i4Status = Bgp4RibhGetFirstEntry (u4AsafiMask,
                                                      &pRtProfile,
                                                      &pRibNode, TRUE);
                    if (i4Status == BGP4_SUCCESS)
                    {
                        /* EVPN route is found in EVPN-RIB */
                        break;
                    }

#endif
                    return BGP4_SUCCESS;

#ifdef BGP4_IPV6_WANTED
                case CAP_MP_IPV6_UNICAST:
#ifdef L3VPN
                    u4AsafiMask = CAP_MP_VPN4_UNICAST;
                    pRibNode = BGP4_VPN4_RIB_TREE (u4Context);
                    i4Status = Bgp4RibhGetFirstEntry (u4AsafiMask,
                                                      &pRtProfile,
                                                      &pRibNode, TRUE);
                    if (i4Status == BGP4_SUCCESS)
                    {
                        /* VPN4 route is found in VPN-RIB */
                        break;
                    }
#endif
#ifdef VPLSADS_WANTED
                    u4AsafiMask = CAP_MP_L2VPN_VPLS;
                    pRibNode = BGP4_VPLS_RIB_TREE (u4Context);
                    i4Status = Bgp4RibhGetFirstEntry (u4AsafiMask,
                                                      &pRtProfile,
                                                      &pRibNode, TRUE);
                    if (i4Status == BGP4_SUCCESS)
                    {
                        /* VPLS route is found in VPLS-RIB */
                        break;
                    }
#endif
#ifdef EVPN_WANTED
                    u4AsafiMask = CAP_MP_L2VPN_EVPN;
                    pRibNode = BGP4_EVPN_RIB_TREE (u4Context);
                    i4Status = Bgp4RibhGetFirstEntry (u4AsafiMask,
                                                      &pRtProfile,
                                                      &pRibNode, TRUE);
                    if (i4Status == BGP4_SUCCESS)
                    {
                        /* EVPN route is found in EVPN-RIB */
                        break;
                    }

#endif
                    return BGP4_SUCCESS;
#endif

#ifdef L3VPN
                case CAP_MP_VPN4_UNICAST:
#ifdef VPLSADS_WANTED
                    u4AsafiMask = CAP_MP_L2VPN_VPLS;
                    pRibNode = BGP4_VPLS_RIB_TREE (u4Context);
                    i4Status = Bgp4RibhGetFirstEntry (u4AsafiMask,
                                                      &pRtProfile,
                                                      &pRibNode, TRUE);
                    if (i4Status == BGP4_SUCCESS)
                    {
                        /* VPLS route is found in VPLS-RIB */
                        break;
                    }
#endif
#ifdef EVPN_WANTED
                    u4AsafiMask = CAP_MP_L2VPN_EVPN;
                    pRibNode = BGP4_EVPN_RIB_TREE (u4Context);
                    i4Status = Bgp4RibhGetFirstEntry (u4AsafiMask,
                                                      &pRtProfile,
                                                      &pRibNode, TRUE);
                    if (i4Status == BGP4_SUCCESS)
                    {
                        /* EVPN route is found in EVPN-RIB */
                        break;
                    }

#endif
                    /* VPNv4 routes also over, then return FALSE */
                    return BGP4_SUCCESS;
#endif
#ifdef VPLSADS_WANTED
                case CAP_MP_L2VPN_VPLS:
#ifdef EVPN_WANTED
                    u4AsafiMask = CAP_MP_L2VPN_EVPN;
                    pRibNode = BGP4_EVPN_RIB_TREE (u4Context);
                    i4Status = Bgp4RibhGetFirstEntry (u4AsafiMask,
                                                      &pRtProfile,
                                                      &pRibNode, TRUE);
                    if (i4Status == BGP4_SUCCESS)
                    {
                        /* EVPN route is found in EVPN-RIB */
                        break;
                    }

#endif
                    /* VPLS routes also over, then return FALSE */
                    return BGP4_SUCCESS;
#endif
                default:
                    return BGP4_SUCCESS;
            }
        }

        /* When pRtProfile is null, there are no routes to clean up.
         * hence return success */
        if (pRtProfile == NULL)
        {
            return BGP4_SUCCESS;
        }
        /* For the First Route check for the validity of the route and set
         * the u1IsNotBestRoute flag accordingly. This is to ensure that
         * the route is properly removed from RCVD_PA_DB
         */
        u1IsNotBestRoute = ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_HISTORY) ?
                            BGP4_HISTORY :
                            ((BGP4_IS_ROUTE_VALID (pRtProfile) == BGP4_TRUE) ?
                             BGP4_FALSE : BGP4_TRUE));
        BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_HISTORY);
        u1IsBestRoute = (BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_BEST) ?
            BGP4_TRUE : BGP4_FALSE;
        u1IsMpathRoute =
            (BGP4_RT_GET_EXT_FLAGS (pRtProfile) & BGP4_RT_MULTIPATH) ? BGP4_TRUE
            : BGP4_FALSE;
        for (;;)
        {
            pNextRtProfile = BGP4_RT_NEXT (pRtProfile);

            BGP4_RT_REF_COUNT (pRtProfile)++;

            /* Delete route in RIB */
            Bgp4RibhDelRtEntry (pRtProfile, pRibNode,
                                BGP4_RT_CXT_ID (pRtProfile), u1IsNotBestRoute);

#ifdef L3VPN
            /* Delete the route from all the installed VRFs */
            if ((BGP4_RT_AFI_INFO (pRtProfile) == BGP4_INET_AFI_IPV4) &&
                (BGP4_RT_SAFI_INFO (pRtProfile) ==
                 BGP4_INET_SAFI_VPNV4_UNICAST))
            {
                BGP4_TRC_ARG2 (NULL,
                               BGP4_TRC_FLAG, BGP4_DETAIL_TRC, BGP4_MOD_NAME,
                               "\tCalling Bgp4Vpnv4DelRtFromVrfs from Func[%s] Line[%d]\n",
                               __func__, __LINE__);
                Bgp4Vpnv4DelRtFromVrfs (pRtProfile);
                if (u4Context != BGP4_DFLT_VRFID)
                {
                    /* Add the RtProfile to the Withdrawn routeList for 
                     * sending to PE peer of Default VRF */
                    Bgp4DshAddRouteToList (&PeerWithRtList, pRtProfile, 0);
                }

            }
#endif
            /* Delete the route in FIB if the route was the best route. */
            if (((u1IsBestRoute == BGP4_TRUE) || (u1IsMpathRoute == BGP4_TRUE))
                && ((BGP4_RT_GET_EXT_FLAGS (pRtProfile) & BGP4_RT_IN_RTM) ==
                    BGP4_RT_IN_RTM))
            {
                BGP4_RT_SET_FLAG (pRtProfile, (BGP4_RT_WITHDRAWN |
                                               BGP4_RT_ADVT_NOTTO_EXTERNAL));
                BGP4_TRC_ARG2 (NULL,
                               BGP4_TRC_FLAG, BGP4_DETAIL_TRC, BGP4_MOD_NAME,
                               "\tCalling Bgp4IgphUpdateRouteInFIB from Func[%s] Line[%d]\n",
                               __func__, __LINE__);
                Bgp4IgphUpdateRouteInFIB (pRtProfile);
            }
            Bgp4DshReleaseRtInfo (pRtProfile);

            if (pNextRtProfile == NULL)
            {
                break;
            }

            pRtProfile = pNextRtProfile;
            pNextRtProfile = NULL;
            /* For routes in node other than first route, set the
             * u1IsNotBestRoute as BGP4_TRUE/BGP4_HISTORY and this ensure
             * that the routes are removed from the RCVD_PA_DB correctly. */
            u1IsNotBestRoute = ((BGP4_RT_GET_FLAGS (pRtProfile) &
                                 BGP4_RT_HISTORY) ? BGP4_HISTORY : BGP4_TRUE);
            BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_HISTORY);
            u1IsBestRoute = (BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_BEST) ?
                BGP4_TRUE : BGP4_FALSE;
            u1IsMpathRoute =
                (BGP4_RT_GET_EXT_FLAGS (pRtProfile) & BGP4_RT_MULTIPATH) ?
                BGP4_TRUE : BGP4_FALSE;
        }
#ifdef L3VPN
        /* When during shuting of the BGP session with CE peers, WITHDRAW
         * messages has to be sent to the PE peer of the default VRF also.
         * Hence the route profiles are also advertised as withdrawn routes
         * to the PE peers. */
        if ((TMO_SLL_Count (&PeerWithRtList) != 0) &&
            (gBgpCxtNode[BGP4_DFLT_VRFID] != NULL))
        {
            TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (BGP4_DFLT_VRFID), pPeerentry,
                          tBgp4PeerEntry *)
            {
                if (BGP4_PEER_STATE (pPeerentry) != BGP4_ESTABLISHED_STATE)
                {
                    /* Connection not in established state. So no need to
                     * advertise to this peer. */
                    continue;
                }
                if (BGP4_VPN4_PEER_ROLE (pPeerentry) != BGP4_VPN4_PE_PEER)
                {
                    /* The Peer is not a PE Peer. Hence the route from CE
                     * need not be advertised to the peer */
                    continue;
                }

                /* Route can be advertised to this peer. */
                Bgp4PeerSendRoutesToPeerTxQ (pPeerentry, &DummyFeasRtList,
                                             &PeerWithRtList, BGP4_TRUE);
            }

            BGP_SLL_DYN_Scan (&PeerWithRtList, pRtLink, pTempLinkNode,
                              tLinkNode *)
            {
                Bgp4DshRemoveNodeFromList (&PeerWithRtList, pRtLink);
            }

            Bgp4DshReleaseList (&DummyFeasRtList, 0);
        }
#endif
    }
}

/*****************************************************************************/
/* Function Name : Bgp4RibhAddRtEntry                                        */
/* Description   : This function is called whenever a route entry is to be   */
/*                 added into RIB information.                               */
/* Input(s)      : pAddRtProfile - information about the route entry         */
/*                 u4VrfId - VRF identifier                                  */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4RibhAddRtEntry (tRouteProfile * pAddRtProfile, UINT4 u4VrfId)
{
    tRouteProfile      *pVpnRtProfile = NULL;
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tBgp4RcvdPathAttr  *pBgp4RcvdPA = NULL;
    tBGP4_DLL          *pImportList = BGP4_STATIC_IMPORT_LIST (u4VrfId);
    UINT4               u4AFIndex = BGP4_IPV4_UNI_INDEX;
    UINT4               u4AsafiMask;
    INT4                i4Status = BGP4_SUCCESS;
    UINT1               u1IsBestRoute = BGP4_FALSE;
    UINT1               u1AdvFlag = 0;
    UINT1               au1PrintBuf[BGP4_PRINT_BUF_SIZE];

    MEMSET (au1PrintBuf, 0, BGP4_PRINT_BUF_SIZE);
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pAddRtProfile),
                           BGP4_RT_SAFI_INFO (pAddRtProfile), u4AsafiMask);
    Bgp4GetAfiSafiIndex (BGP4_RT_AFI_INFO (pAddRtProfile),
                         BGP4_RT_SAFI_INFO (pAddRtProfile), &u4AFIndex);

#ifdef L3VPN_WANTED
    BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_FDB_TRC,
                   BGP4_MOD_NAME,
                   "\tReceived route %s with VRF ID %u for adding the entry into RIB information.\r\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pAddRtProfile),
                                    BGP4_RT_AFI_INFO (pAddRtProfile)), u4VrfId);

    if (pAddRtProfile->pRtInfo != NULL)
    {
        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tReceived route with nexthop %s and immediate nexthop %s.\r\n",
                       Bgp4PrintIpAddr ((pAddRtProfile->pRtInfo->NextHopInfo.
                                         au1Address),
                                        BGP4_RT_AFI_INFO (pAddRtProfile)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP
                                        (pAddRtProfile),
                                        BGP4_RT_AFI_INFO (pAddRtProfile)));

    }
#endif

#ifdef L3VPN
    if (BGP4_RT_GET_FLAGS (pAddRtProfile) & BGP4_RT_CONVERTED_TO_VPNV4)
    {
        u4AFIndex = BGP4_IPV4_UNI_INDEX;
    }
#endif
    pPeerInfo = (BGP4_RT_PROTOCOL (pAddRtProfile) == BGP_ID) ?
        BGP4_RT_PEER_ENTRY (pAddRtProfile) : NULL;
    if (((NULL == pPeerInfo)) && (BGP4_RT_PROTOCOL (pAddRtProfile) == BGP_ID))
    {
        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC | BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                       "\tThe Peer info of received route %s of VRF ID %u is NULL.\r\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pAddRtProfile),
                                        BGP4_RT_AFI_INFO (pAddRtProfile)),
                       u4VrfId);
        return BGP4_FAILURE;
    }

#ifdef L3VPN_TEST
    if (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pAddRtProfile)) ==
        BGP4_VPN4_CE_PEER)
    {
        u4AFIndex = BGP4_IPV4_UNI_INDEX;
    }
#endif
    if ((BGP4_RT_PROTOCOL (pAddRtProfile) == BGP_ID) &&
        (BGP4_IS_ROUTE_VALID (pAddRtProfile) == BGP4_FALSE))
    {
        BGP4_PEER_IN_PREFIXES_RJCTD_CNT (pPeerInfo, u4AFIndex)++;
    }
    /*Multipath config */
    if (BGP4_MPATH_EBGP_COUNT (u4VrfId) != BGP_DEFAULT_MAXPATH)
    {
        BGP4_MPATH_OPER_EBGP_COUNT (u4VrfId) = BGP4_MPATH_EBGP_COUNT (u4VrfId);
    }
    if (BGP4_MPATH_IBGP_COUNT (u4VrfId) != BGP_DEFAULT_MAXPATH)
    {
        BGP4_MPATH_OPER_IBGP_COUNT (u4VrfId) = BGP4_MPATH_IBGP_COUNT (u4VrfId);
    }
    if (BGP4_MPATH_EIBGP_COUNT (u4VrfId) != BGP_DEFAULT_MAXPATH)
    {
        BGP4_MPATH_OPER_EIBGP_COUNT (u4VrfId) =
            BGP4_MPATH_EIBGP_COUNT (u4VrfId);
    }

    if ((BGP4_RT_GET_FLAGS (pAddRtProfile) & BGP4_RT_FILTERED_INPUT) ==
        BGP4_RT_FILTERED_INPUT)
    {
        /* Route to be added to the RIB is filter because of the
         * Incoming Filter policy. Check whether the Route-Refresh
         * capabality is negociated with the peer from which this
         * route is learnt. If negocitated, then dont add the route to
         * the RIB. */
        if (BGP4_RT_PROTOCOL (pAddRtProfile) == BGP_ID)
        {
            if ((BGP4_PEER_NEG_CAP_MASK (pPeerInfo) &
                 CAP_NEG_ROUTE_REFRESH_MASK) == CAP_NEG_ROUTE_REFRESH_MASK)
            {
                /* Route Refresh Capability has been negocitad. Then no need to
                 * add the route to RIB. */
#ifdef RFD_WANTED
                if (BGP4_RT_DAMP_HIST (pAddRtProfile) != NULL)
                {
                    /* Remove the route from REUSE list if it is present. */
                    RfdRemoveRtFromRtsReuseList
                        (pAddRtProfile, BGP4_RT_DAMP_HIST (pAddRtProfile));
                    Bgp4MemReleaseRfdDampHist
                        (BGP4_RT_DAMP_HIST (pAddRtProfile));
                    BGP4_RT_DAMP_HIST (pAddRtProfile) = NULL;
                    Bgp4DshReleaseRtInfo (pAddRtProfile);
                }
#endif
                return BGP4_SUCCESS;
            }
        }
    }

#ifdef L3VPN
    BGP4_RT_SET_FLAG (pAddRtProfile, BGP4_RT_PROCESS_RIB);
    if ((u4AsafiMask == CAP_MP_VPN4_UNICAST) && (u4VrfId != BGP4_DFLT_VRFID))
    {
        Bgp4Vpnv4SetVrfFlags (pAddRtProfile, u4VrfId, BGP4_RT_PROCESS_VRF_RIB);
    }
#endif
    /* Add the route to RIB. */
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
#endif
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
        case CAP_MP_VPN4_UNICAST:
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
#endif
            i4Status = Bgp4TrieAddEntry (pAddRtProfile, u4VrfId,
                                         &u1IsBestRoute);
            if (i4Status == BGP4_SUCCESS)
            {

                u1AdvFlag = pAddRtProfile->u1AdvFlag;
                if ((u1AdvFlag & BGP4_NETWORK_ADV_ROUTE) ==
                    BGP4_NETWORK_ADV_ROUTE)
                {
                    SPRINTF ((char *) au1PrintBuf,
                             "Route %s added in RIB successfully",
                             Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pAddRtProfile),
                                              BGP4_RT_AFI_INFO
                                              (pAddRtProfile)));
                    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, BGP4_SYSLOG_ID,
                                  (char *) au1PrintBuf));
                }
            }
            break;
        default:
            return BGP4_FAILURE;
    }
#ifdef L3VPN
    BGP4_RT_RESET_FLAG (pAddRtProfile, BGP4_RT_PROCESS_RIB);
    if ((u4AsafiMask == CAP_MP_VPN4_UNICAST) && (u4VrfId != BGP4_DFLT_VRFID))
    {
        Bgp4Vpnv4ResetVrfFlags (pAddRtProfile, u4VrfId,
                                BGP4_RT_PROCESS_VRF_RIB);
    }

#endif
    if (i4Status == BGP4_SUCCESS)
    {
        BGP4_RIB_ROUTES_COUNT (u4VrfId)++;
        BGP4_RT_RESET_EXT_FLAG (pAddRtProfile, BGP4_ROUTE_WITHDRAWN);
        Bgp4RedSyncRouteInfo (pAddRtProfile);
        /* Only Routes learnt from BGP Peers which are either VALID or
         * NEXT_HOP_UNREACHABLE are added to the Next-Hop List. */
        if ((BGP4_RT_PROTOCOL (pAddRtProfile) == BGP_ID) &&
            (BGP4_RT_NEXTHOP_LINK_PREV (pAddRtProfile) == NULL) &&
            (BGP4_RT_NEXTHOP_LINK_NEXT (pAddRtProfile) == NULL) &&
            ((BGP4_IS_ROUTE_VALID (pAddRtProfile) == BGP4_TRUE) ||
             ((BGP4_RT_GET_FLAGS (pAddRtProfile) &
               BGP4_RT_NEXT_HOP_UNKNOWN) == BGP4_RT_NEXT_HOP_UNKNOWN)))
        {
            /* Route is successfully added to the RIB. Now also update
             * the nexthop route list, so that this route gets processed
             * periodically for the next hop changes. */
            if (BGP4_RT_BGP_INFO (pAddRtProfile) != NULL)
            {
                Bgp4AddRouteToIdentNextHopList (pAddRtProfile,
                                                u4VrfId,
                                                BGP4_INFO_NEXTHOP_INFO
                                                (BGP4_RT_BGP_INFO
                                                 (pAddRtProfile)));
            }
            else
            {
                return BGP4_FAILURE;
            }
        }

        /* Now route is present in the RIB. Link the received Path 
         * Attribute info with the RCVD_PA_DB.
         */
#ifdef L3VPN
        if ((pPeerInfo != NULL) &&
            (BGP4_VPN4_PEER_ROLE (pPeerInfo) == BGP4_VPN4_PE_PEER) &&
            (u4AsafiMask == CAP_MP_VPN4_UNICAST) && (u4VrfId != 0))
        {
            /* The vpnv4 routes received from PE peers are already
             * added in the context - 0 PADB table. It is not required to
             * added in non zero context PADB table also
             * */
            pBgp4RcvdPA = NULL;
        }
        else
        {
#endif
            pBgp4RcvdPA =
                Bgp4RcvdPADBAddEntry (u4VrfId, BGP4_RT_BGP_INFO (pAddRtProfile),
                                      BGP4_RT_AFI_INFO (pAddRtProfile),
                                      BGP4_RT_SAFI_INFO (pAddRtProfile));
#ifdef L3VPN
        }
        if ((BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED) &&
            (BGP4_RT_IS_IN_PADB (pAddRtProfile) == BGP4_TRUE))
        {
            /* If route leaking is enabled and profile is added in an
               another VRF's PADB table, no need to add again in this 
               VRF's PADB table */

            pBgp4RcvdPA = NULL;
            /* Incoming Route is a VRF Leaked route */
            BGP4_RT_IS_LEAK_ROUTE (pAddRtProfile) = BGP4_TRUE;
        }
#endif
        if (pBgp4RcvdPA != NULL)
        {
            if (BGP4_RT_BGP_INFO (pAddRtProfile) !=
                BGP4_PA_BGP4INFO (pBgp4RcvdPA))
            {
                /* BGP4_INFO stored in route differs from the BGP4_INFO in
                 * the RCVD_PA_ENTRY. Update the route with the BGP4_INFO in
                 * RCVD_PA_ENTRY.
                 */
                Bgp4DshReleaseBgpInfo (BGP4_RT_BGP_INFO (pAddRtProfile));
                BGP4_LINK_INFO_TO_PROFILE (BGP4_PA_BGP4INFO (pBgp4RcvdPA),
                                           pAddRtProfile);
            }
            /* If the info is linked with RCVD_PA_DB, then add the route to
             * the RCVD_PA_DB. */
            Bgp4RcvdPADBAddRoute (pAddRtProfile, u1IsBestRoute);
#ifdef L3VPN
            if (u4VrfId != BGP4_DFLT_VRFID)
            {
                BGP4_RT_IS_IN_PADB (pAddRtProfile) = BGP4_TRUE;
            }
#endif
        }

        if ((BGP4_RT_PROTOCOL (pAddRtProfile) == BGP_ID) && (pPeerInfo != NULL))
        {
#ifdef L3VPN
            if ((BGP4_VPN4_PEER_ROLE (pPeerInfo) == BGP4_VPN4_PE_PEER) &&
                (u4AsafiMask == CAP_MP_VPN4_UNICAST) && (u4VrfId != 0))
            {
                return BGP4_SUCCESS;
            }
#endif
            BGP4_PEER_IN_PREFIXES_CNT (pPeerInfo, u4AFIndex)++;
            BGP4_PEER_IN_PREFIXES_ACPTD_CNT (pPeerInfo, u4AFIndex)++;
            /* Add the route to the Protocol_link list. Always add the
             * route to the beginning of this list. */
            Bgp4DshLinkRouteToList (BGP4_PEER_ROUTE_LIST (pPeerInfo,
                                                          u4AFIndex),
                                    pAddRtProfile, BGP4_PREPEND,
                                    BGP4_PROTOCOL_LIST_INDEX);
        }
        else
        {
            /* Only Redistribute routes has to be added to the ImportRtList
             * and not Network routes which will come in the same flow. */
            if (BGP4_RT_GET_ADV_FLAG (pAddRtProfile) == BGP4_REDIST_ADV_ROUTE)
            {
                /* Non-BGP Route. Update the corresponding Import List. */
                if (BGP4_RT_PROTOCOL (pAddRtProfile) == BGP4_STATIC_ID)
                {
                    pImportList = BGP4_STATIC_IMPORT_LIST (u4VrfId);
                }
                else if (BGP4_RT_PROTOCOL (pAddRtProfile) == BGP4_LOCAL_ID)
                {
                    pImportList = BGP4_DIRECT_IMPORT_LIST (u4VrfId);
                }
                else if (BGP4_RT_PROTOCOL (pAddRtProfile) == BGP4_RIP_ID)
                {
                    pImportList = BGP4_RIP_IMPORT_LIST (u4VrfId);
                }
                else if (BGP4_RT_PROTOCOL (pAddRtProfile) == BGP4_OSPF_ID)
                {
                    pImportList = BGP4_OSPF_IMPORT_LIST (u4VrfId);
                }
                else if (BGP4_RT_PROTOCOL (pAddRtProfile) == BGP4_ISIS_ID)
                {
                    if (BGP4_RT_METRIC_TYPE (pAddRtProfile) == IP_ISIS_LEVEL1)
                    {
                        pImportList = BGP4_ISISL1_IMPORT_LIST (u4VrfId);
                    }
                    else
                    {
                        pImportList = BGP4_ISISL2_IMPORT_LIST (u4VrfId);
                    }
                }
                /* Add the route to the Protocol_link list if the route is not
                 * present in the list. Always add the route to the beginning of
                 * this list. */
                if ((BGP4_RT_PROTO_LINK_PREV (pAddRtProfile) == NULL) &&
                    (BGP4_RT_PROTO_LINK_NEXT (pAddRtProfile) == NULL) &&
                    (BGP4_DLL_FIRST_ROUTE (pImportList) != pAddRtProfile) &&
                    (BGP4_DLL_LAST_ROUTE (pImportList) != pAddRtProfile))
                {
                    /* Route is not present in the List. Add it. */
                    Bgp4DshLinkRouteToList (pImportList, pAddRtProfile,
                                            BGP4_PREPEND,
                                            BGP4_PROTOCOL_LIST_INDEX);
                }
            }
        }
    }
    else
    {
        /* Adding Route to RIB Failed. */
#ifdef RFD_WANTED
        if (BGP4_RT_DAMP_HIST (pAddRtProfile) != NULL)
        {
            /* Remove the route from REUSE list if it is present. */
            RfdRemoveRtFromRtsReuseList (pAddRtProfile,
                                         BGP4_RT_DAMP_HIST (pAddRtProfile));
            Bgp4MemReleaseRfdDampHist (BGP4_RT_DAMP_HIST (pAddRtProfile));
            BGP4_RT_DAMP_HIST (pAddRtProfile) = NULL;
            Bgp4DshReleaseRtInfo (pAddRtProfile);
        }
#endif
    }

#ifdef L3VPN_TEST
    if ((u4VrfId != BGP4_DFLT_VRFID)
        && (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) == BGP4_L3VPN_VRF_UP))
    {
        /* This is a VPNv4 route and is intended to be added into BGP-VRF,
         * Just add the route into VPN table, no need to do any other processing
         */
        if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
        {
            pVpnRtProfile = Bgp4DuplicateRouteProfile (pAddRtProfile);
            /*pVpnRtProfile = pAddRtProfile; */
            Bgp4Vpn4ConvertRtToVpn4Route (pVpnRtProfile, u4VrfId);
            Bgp4Vpnv4SetVrfFlags (pVpnRtProfile, u4VrfId,
                                  BGP4_RT_PROCESS_VRF_RIB);
            i4Status =
                Bgp4TrieAddEntry (pVpnRtProfile, u4VrfId, &u1IsBestRoute);
            Bgp4Vpnv4ResetVrfFlags (pVpnRtProfile, u4VrfId,
                                    BGP4_RT_PROCESS_VRF_RIB);
            if (i4Status == BGP4_SUCCESS)
            {
                if ((BGP4_RT_PROTOCOL (pVpnRtProfile) == BGP_ID) &&
                    ((BGP4_IS_ROUTE_VALID (pVpnRtProfile) == BGP4_TRUE) ||
                     ((BGP4_RT_GET_FLAGS (pVpnRtProfile) &
                       BGP4_RT_NEXT_HOP_UNKNOWN) == BGP4_RT_NEXT_HOP_UNKNOWN)))
                {
                    /* Route is successfully added to the RIB. Now also update
                     * the nexthop route list, so that this route gets processed
                     * periodically for the next hop changes. */
                    if ((BGP4_VPN4_PEER_ROLE
                         (BGP4_RT_PEER_ENTRY (pVpnRtProfile)) ==
                         BGP4_VPN4_CE_PEER)
                        && (BGP4_RT_CXT_ID (pVpnRtProfile) != u4VrfId))
                    {
                        Bgp4AddRouteToIdentNextHopList (pVpnRtProfile, u4VrfId,
                                                        BGP4_INFO_NEXTHOP_INFO
                                                        (BGP4_RT_BGP_INFO
                                                         (pVpnRtProfile)));
                    }
                }
            }
        }
    }
#else
    UNUSED_PARAM (pVpnRtProfile);
#endif

    BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                   BGP4_FDB_TRC, BGP4_MOD_NAME,
                   "\tAdding the received route %s with VRF ID %u to RIB information was %s.\r\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pAddRtProfile),
                                    BGP4_RT_AFI_INFO (pAddRtProfile)), u4VrfId,
                   Bgp4PrintCodeName ((UINT1) i4Status, BGP4_RETURN_STATUS));
    return i4Status;
}

/*****************************************************************************/
/* Function Name : Bgp4RibhLookupRtEntry                                     */
/* Description   : This function is called whenever a route entry is to be   */
/*                 searched in RIB information.                              */
/* Input(s)      : pRtProfile - route entry information to be searched       */
/*                 u4MatchType -                                             */
/*                      BGP4_TREE_FIND_EXACT : matches exactly with the given*/
/*                                             route entry                   */
/*                      BGP4_TREE_FIND : matches with the first overlap      */
/*                                       route (could be less/more specific) */
/*                 u4VrfId - VRF identifier                                  */
/* Output(s)     : pRtProfileList - pointer to the stored route information  */
/*                 in RIB corresponding to input pRtProfile                  */
/*                 pRibNode - pointer to the node where pRtProfile list is   */
/*                 stored.                                                   */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4RibhLookupRtEntry (tRouteProfile * pRtProfile,
                       UINT4 u4VrfId,
                       UINT4 u4MatchType,
                       tRouteProfile ** ppRtProfileList, VOID **ppRibNode)
{
    INT4                i4RtFound = BGP4_FAILURE;

    if (u4MatchType == BGP4_TREE_FIND_EXACT)
    {
        i4RtFound = Bgp4TrieLookupExactEntry (pRtProfile, u4VrfId,
                                              ppRtProfileList, ppRibNode);
    }
    else
    {
        i4RtFound = Bgp4TrieLookupOverlapEntry (pRtProfile, u4VrfId,
                                                ppRtProfileList, ppRibNode);
    }
    return i4RtFound;
}

/*****************************************************************************/
/* Function Name : Bgp4RibhGetFirstEntry                                     */
/* Description   : This function will give you the first route entry present */
/*                 in RIB information                                        */
/* Input(s)      : u4AsafiMask - <AFI, SAFI> mask which uniquely identifies  */
/*                 a pair of <AFI, SAFI>                                     */
/*                 i4Flag - Flag passed to trie API for taking semaphore     */
/* Output(s)     : pRtProfileList - pointer to the stored route information  */
/*                 in RIB corresponding to input pRtProfile                  */
/*                 pRibNode - pointer to the node where pRtProfileList is    */
/*                 stored.                                                   */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4RibhGetFirstEntry (UINT4 u4AsafiMask,
                       tRouteProfile ** ppRtProfileList,
                       VOID **ppRibNode, INT4 i4Flag)
{
    INT4                i4Status;

    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
            i4Status = Bgp4TrieGetFirstEntry (CAP_MP_IPV4_UNICAST,
                                              ppRtProfileList, ppRibNode,
                                              i4Flag);
            return (i4Status);
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
            i4Status = Bgp4TrieGetFirstEntry (CAP_MP_IPV6_UNICAST,
                                              ppRtProfileList, ppRibNode,
                                              i4Flag);
            return (i4Status);
#endif

#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
            i4Status = Bgp4TrieGetFirstEntry (CAP_MP_L2VPN_VPLS,
                                              ppRtProfileList, ppRibNode,
                                              i4Flag);
            return (i4Status);
#endif

#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
            i4Status = Bgp4TrieGetFirstEntry (CAP_MP_LABELLED_IPV4,
                                              ppRtProfileList, ppRibNode,
                                              i4Flag);
            return (i4Status);

        case CAP_MP_VPN4_UNICAST:
            i4Status = Bgp4TrieGetFirstEntry (CAP_MP_VPN4_UNICAST,
                                              ppRtProfileList, ppRibNode,
                                              i4Flag);
            return (i4Status);
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
            i4Status = Bgp4TrieGetFirstEntry (CAP_MP_L2VPN_EVPN,
                                              ppRtProfileList, ppRibNode,
                                              i4Flag);
            return (i4Status);
#endif
        default:
            return BGP4_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name : Bgp4RibhGetNextEntry                                      */
/* Description   : This function will give you the next route entry present  */
/*                 in RIB for a specified route entry                        */
/*                 It uses the pRibNode pointer for getting the next entry   */
/*                 inorder to avoid the RIB Traversal                        */
/* Input(s)      : pRtProfile - route entry for which the next route info    */
/*                 is to be got from RIB.                                    */
/*                 i4Flag - Flag passed to trie API for taking semaphore     */
/*                 ppRibNode - pointer to the node where pRtProfile is       */
/*                 stored. (if you dont know this node pointer, then simply  */
/*                 NULL has to be passed)                                    */
/* Output(s)     : pRtProfileList - pointer to the next route information    */
/*                 in RIB corresponding to input pRtProfile                  */
/*                 pRibNode - pointer to the node where pRtProfileList is    */
/*                 stored.                                                   */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4RibhGetNextEntry (tRouteProfile * pRtProfile, UINT4 u4VrfId,
                      tRouteProfile ** ppRtProfileList,
                      VOID **ppRibNode, INT4 i4Flag)
{
    INT4                i4Status;
    UINT4               u4AsafiMask;

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
#endif
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
        case CAP_MP_VPN4_UNICAST:
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
#endif
            i4Status = Bgp4TrieGetNextEntry (pRtProfile, u4VrfId,
                                             ppRtProfileList, ppRibNode,
                                             i4Flag);
            return i4Status;
        default:
            return BGP4_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name : Bgp4RibhTraverseAndGetNextEntry                           */
/* Description   : This function will give you the next route entry present  */
/*                 in RIB for a specified route entry by traversing the RIB  */
/*                 for specified route and then gives the next entry         */
/* Input(s)      : pRtProfile - route entry for which the next route info    */
/*                 is to be got from RIB.                                    */
/* Output(s)     : pRtProfileList - pointer to the next route information    */
/*                 in RIB corresponding to input pRtProfile                  */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4RibhTraverseAndGetNextEntry (tRouteProfile * pRtProfile, VOID *pRibNode,
                                 tRouteProfile ** ppRtProfileList)
{
    INT4                i4Status;
    UINT4               u4AsafiMask;

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
#endif
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
        case CAP_MP_VPN4_UNICAST:
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
#endif
            i4Status = Bgp4TrieTraverseAndGetNextEntry (pRtProfile, pRibNode,
                                                        ppRtProfileList);
            return i4Status;
        default:
            return BGP4_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name : Bgp4RibhGetPrevEntry                                      */
/* Description   : This function will give you the previous route entry      */
/*                 present in RIB for a specified route entry                */
/* Input(s)      : pRtProfile - route entry for which the previous route info*/
/*                 is to be got from RIB.                                    */
/*                 pRibNode - pointer to the node where pRtProfile is        */
/*                 stored. (if you dont know this node pointer, then simply  */
/*                 NULL has to be passed)                                    */
/* Output(s)     : pRtProfileList - pointer to the previous route information*/
/*                 in RIB corresponding to input pRtProfile                  */
/*                 pRibNode - pointer to the node where pRtProfileList is    */
/*                 stored.                                                   */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4RibhGetPrevEntry (tRouteProfile * pRtProfile,
                      tRouteProfile ** ppRtProfileList, VOID **ppRibNode)
{
    INT4                i4Status;
    UINT4               u4AsafiMask;

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
#endif
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
        case CAP_MP_VPN4_UNICAST:
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
#endif
            i4Status = Bgp4TrieGetPrevEntry (pRtProfile,
                                             ppRtProfileList, ppRibNode);
            return i4Status;
        default:
            return BGP4_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name : Bgp4RibhTraverseAndGetPrevEntry                           */
/* Description   : This function will give you the prev route entry present  */
/*                 in RIB for a specified route entry by traversing the RIB  */
/*                 for specified route and then gives the prev entry         */
/* Input(s)      : pRtProfile - route entry for which the previous route info*/
/*                 is to be got from RIB.                                    */
/* Output(s)     : pRtProfileList - pointer to the previous route information*/
/*                 in RIB corresponding to input pRtProfile                  */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4RibhTraverseAndGetPrevEntry (tRouteProfile * pRtProfile, VOID *pRibNode,
                                 tRouteProfile ** ppRtProfileList)
{
    INT4                i4Status;
    UINT4               u4AsafiMask;

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
#endif
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
        case CAP_MP_VPN4_UNICAST:
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
#endif
            i4Status = Bgp4TrieTraverseAndGetPrevEntry (pRtProfile,
                                                        pRibNode,
                                                        ppRtProfileList);
            return i4Status;
        default:
            return BGP4_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name : Bgp4RibhDelRtEntry                                        */
/* Description   : This function is called whenever a route entry is to be   */
/*                 delted from RIB information.                              */
/* Input(s)      : pDelRtProfile - information about the route entry         */
/*               : pRibNode - Pointer to the Rib Node where this route is    */
/*                 present. Ensure that the correct leaf node pointer is     */
/*                 passed. If not known, set this to NULL.                   */
/*               : u1IsRouteInvalid - This flag indicates whether the route  */
/*                                    is invalid route or not. If this value */
/*                                    BGP4_TRUE, then the route is Invalid   */
/*                                    route. If BGP4_FALSE, then the route   */
/*                                    is not INVALID route. If BGP4_HISTORY, */
/*                                    if the route was earlier stored as     */
/*                                    a HISTORY route in the RIB.            */
/*                 u4VrfId - VRF identifier                                  */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4RibhDelRtEntry (tRouteProfile * pDelRtProfile, VOID *pRibNode,
                    UINT4 u4VrfId, UINT1 u1IsRouteInvalid)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tBGP4_DLL          *pImportList = NULL;
    UINT4               u4AFIndex = BGP4_IPV4_UNI_INDEX;
    UINT4               u4AsafiMask;
    INT4                i4Status = BGP4_FAILURE;
    UINT1               u1IsBestRoute = BGP4_FALSE;
#ifdef EVPN_WANTED
    INT4                i4RetVal = BGP4_FAILURE;
#endif
    UINT1               u1AdvFlag = 0;
    UINT1               au1PrintBuf[BGP4_PRINT_BUF_SIZE];

    MEMSET (au1PrintBuf, 0, BGP4_PRINT_BUF_SIZE);

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pDelRtProfile),
                           BGP4_RT_SAFI_INFO (pDelRtProfile), u4AsafiMask);

    Bgp4GetAfiSafiIndex (BGP4_RT_AFI_INFO (pDelRtProfile),
                         BGP4_RT_SAFI_INFO (pDelRtProfile), &u4AFIndex);

#ifdef L3VPN_WANTED
    BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_FDB_TRC,
                   BGP4_MOD_NAME,
                   "\tReceived route %s with VRF ID %u for deleting the entry from RIB information.\r\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pDelRtProfile),
                                    BGP4_RT_AFI_INFO (pDelRtProfile)), u4VrfId);

    if (pDelRtProfile->pRtInfo != NULL)
    {
        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tReceived route with nexthop %s and immediate nexthop %s.\r\n",
                       Bgp4PrintIpAddr ((pDelRtProfile->pRtInfo->NextHopInfo.
                                         au1Address),
                                        BGP4_RT_AFI_INFO (pDelRtProfile)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP
                                        (pDelRtProfile),
                                        BGP4_RT_AFI_INFO (pDelRtProfile)));

    }
#endif

    pPeerEntry = (BGP4_RT_PROTOCOL (pDelRtProfile) == BGP_ID) ?
        BGP4_RT_PEER_ENTRY (pDelRtProfile) : NULL;
#ifdef L3VPN
    if (BGP4_RT_GET_FLAGS (pDelRtProfile) & BGP4_RT_CONVERTED_TO_VPNV4)
    {
        u4AFIndex = BGP4_IPV4_UNI_INDEX;
    }
#endif

    if (u4VrfId == BGP4_RT_CXT_ID (pDelRtProfile))
    {
        if ((BGP4_RT_PROTOCOL (pDelRtProfile) == BGP_ID) &&
            (u1IsRouteInvalid == BGP4_TRUE) &&
            (BGP4_LOCAL_ADMIN_STATUS (u4VrfId) == BGP4_ADMIN_UP))
        {
            BGP4_PEER_IN_PREFIXES_RJCTD_CNT (pPeerEntry, u4AFIndex)--;
        }

#ifdef L3VPN
        BGP4_RT_RESET_FLAG (pDelRtProfile, BGP4_RT_PROCESS_RIB);
#endif
        switch (u4AsafiMask)
        {
            case CAP_MP_IPV4_UNICAST:
#ifdef BGP4_IPV6_WANTED
            case CAP_MP_IPV6_UNICAST:
#endif
#ifdef L3VPN
            case CAP_MP_LABELLED_IPV4:
#endif
#ifdef VPLSADS_WANTED
            case CAP_MP_L2VPN_VPLS:
#endif
                i4Status = Bgp4TrieDeleteEntry (pDelRtProfile, pRibNode,
                                                u4VrfId, &u1IsBestRoute);
                if (i4Status == BGP4_SUCCESS)
                {
                    u1AdvFlag = pDelRtProfile->u1AdvFlag;
                    if ((u1AdvFlag & BGP4_NETWORK_ADV_ROUTE) ==
                        BGP4_NETWORK_ADV_ROUTE)
                    {
                        SPRINTF ((char *) au1PrintBuf,
                                 "Route %s deleted from RIB successfully",
                                 Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                  (pDelRtProfile),
                                                  BGP4_RT_AFI_INFO
                                                  (pDelRtProfile)));
                        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, BGP4_SYSLOG_ID,
                                      (char *) au1PrintBuf));
                    }
                }

#ifdef VPLSADS_WANTED
                if ((u4VrfId == BGP4_DFLT_VRFID)
                    && (BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) > 0))
                {
                    Bgp4VplsRrPrune (pDelRtProfile);
                }
#endif
                break;
#ifdef L3VPN
            case CAP_MP_VPN4_UNICAST:
                i4Status = Bgp4TrieDeleteEntry (pDelRtProfile, pRibNode,
                                                u4VrfId, &u1IsBestRoute);
                if ((u4VrfId == BGP4_DFLT_VRFID)
                    && (BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) > 0))
                {
                    Bgp4Vpnv4RrPrune (pDelRtProfile);
                }
                break;
#endif
#ifdef EVPN_WANTED
            case CAP_MP_L2VPN_EVPN:
                i4Status = Bgp4TrieDeleteEntry (pDelRtProfile, pRibNode,
                                                u4VrfId, &u1IsBestRoute);
                if ((u4VrfId == BGP4_DFLT_VRFID)
                    && (BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) > 0))
                {
                    i4RetVal = Bgp4EvpnRrPrune (pDelRtProfile);
                }
                UNUSED_PARAM (i4RetVal);
                break;
#endif
            default:
                return BGP4_FAILURE;
        }
#ifdef L3VPN
        BGP4_RT_RESET_FLAG (pDelRtProfile, BGP4_RT_PROCESS_RIB);
#endif
        if (i4Status == BGP4_SUCCESS)
        {
            if (u1IsRouteInvalid != BGP4_HISTORY)
            {
                /* For history routes, the route deletion sync message would
                 * have been already sent to Standby. Hence, it is not needed
                 * to sync it again.
                 * For the IGP routes learnt via redistribution, withdrawn flag 
                 * will not be sent, hence standby is not aware of route deletion 
                 * Hence, explicitely setting withdrawn flag for the routes here */
                BGP4_RT_SET_EXT_FLAG (pDelRtProfile, BGP4_ROUTE_WITHDRAWN);
                BGP4_RT_RESET_EXT_FLAG (pDelRtProfile,
                                        BGP4_ROUTE_BEST_ROUTE_UPDATE);
                Bgp4RedSyncRouteInfo (pDelRtProfile);
            }
        }
        /* History route will be stored only in the RIB and Peer Route List.
         * Not in any other database. */

        /* For L2VPN-VPLS, it will go to else part and delete route 
           form peer list */
        if (u1IsRouteInvalid != BGP4_HISTORY)
        {
#ifdef L3VPN
            if (BGP4_RT_PROTOCOL (pDelRtProfile) == BGP_ID)
#endif
            {
                if (BGP4_RT_BGP_INFO (pDelRtProfile) != NULL)
                {
                    Bgp4RemoveRouteFromIdentNextHopList (pDelRtProfile,
                                                         u4VrfId,
                                                         BGP4_INFO_NEXTHOP_INFO
                                                         (BGP4_RT_BGP_INFO
                                                          (pDelRtProfile)));
                }
            }
            if (pPeerEntry != NULL)
            {
                /* Update the Peer In-Prefix Count */
                BGP4_PEER_IN_PREFIXES_CNT (pPeerEntry, u4AFIndex)--;
                BGP4_PEER_IN_PREFIXES_ACPTD_CNT (pPeerEntry, u4AFIndex)--;
                if ((BGP4_RT_GET_FLAGS (pDelRtProfile) & BGP4_RT_HISTORY) !=
                    BGP4_RT_HISTORY)
                {
                    /* Remove the route from the Protocol-link List provided
                     * if we need not store the route as HISTORY  route. */
                    if (BGP4_PEER_ROUTE_LIST (pPeerEntry, u4AFIndex) != NULL)
                    {
                        Bgp4DshDelinkRouteFromList (BGP4_PEER_ROUTE_LIST
                                                    (pPeerEntry, u4AFIndex),
                                                    pDelRtProfile,
                                                    BGP4_PROTOCOL_LIST_INDEX);
                    }
                }
            }
            else
            {
                /* Only Redistribute routes has to be deleted from the ImportRtList
                 * and as Network routes will not be added to the same. */
                if (BGP4_RT_GET_ADV_FLAG (pDelRtProfile) ==
                    BGP4_REDIST_ADV_ROUTE)
                {
                    switch (BGP4_RT_PROTOCOL (pDelRtProfile))
                    {
                            /*Non-BGP Route. Update the corresponding Import List. */
                        case BGP4_STATIC_ID:
                        {
                            pImportList = BGP4_STATIC_IMPORT_LIST (u4VrfId);
                            Bgp4DshDelinkRouteFromList (pImportList,
                                                        pDelRtProfile,
                                                        BGP4_PROTOCOL_LIST_INDEX);
                            break;
                        }
                        case BGP4_LOCAL_ID:
                        {
                            pImportList = BGP4_DIRECT_IMPORT_LIST (u4VrfId);
                            Bgp4DshDelinkRouteFromList (pImportList,
                                                        pDelRtProfile,
                                                        BGP4_PROTOCOL_LIST_INDEX);
                            break;
                        }
                        case BGP4_RIP_ID:
                        {
                            pImportList = BGP4_RIP_IMPORT_LIST (u4VrfId);
                            Bgp4DshDelinkRouteFromList (pImportList,
                                                        pDelRtProfile,
                                                        BGP4_PROTOCOL_LIST_INDEX);
                            break;
                        }
                        case BGP4_OSPF_ID:
                        {
                            pImportList = BGP4_OSPF_IMPORT_LIST (u4VrfId);
                            Bgp4DshDelinkRouteFromList (pImportList,
                                                        pDelRtProfile,
                                                        BGP4_PROTOCOL_LIST_INDEX);
                            break;
                        }
                        case BGP4_ISIS_ID:
                        {
                            if (BGP4_RT_METRIC_TYPE (pDelRtProfile) ==
                                IP_ISIS_LEVEL1)
                            {
                                pImportList = BGP4_ISISL1_IMPORT_LIST (u4VrfId);
                                Bgp4DshDelinkRouteFromList (pImportList,
                                                            pDelRtProfile,
                                                            BGP4_PROTOCOL_LIST_INDEX);
                            }
                            else
                            {
                                pImportList = BGP4_ISISL2_IMPORT_LIST (u4VrfId);
                                Bgp4DshDelinkRouteFromList (pImportList,
                                                            pDelRtProfile,
                                                            BGP4_PROTOCOL_LIST_INDEX);
                            }
                            break;
                        }
                        default:
                        {
                            break;
                        }
                            /* Delink the route from the Protocol_link list. */
                    }            /* End of switch */
                }
            }
            if (u1IsRouteInvalid == BGP4_TRUE)
            {
                /* Route is not the best one. Over-write value returned 
                 * by RIB */
                u1IsBestRoute = BGP4_FALSE;
            }
            /* Remove the route from the RCVD_PA_DB. */
            Bgp4RcvdPADBDeleteRoute (pDelRtProfile, u1IsBestRoute);
        }
        else
        {
            if (pPeerEntry != NULL)
            {
                /* Route is earlier stored as HISTORY route. Delink it. */
                Bgp4DshDelinkRouteFromList (BGP4_PEER_ROUTE_LIST (pPeerEntry,
                                                                  u4AFIndex),
                                            pDelRtProfile,
                                            BGP4_PROTOCOL_LIST_INDEX);
            }
        }
    }
#ifdef L3VPN
    else if ((u4VrfId != BGP4_DFLT_VRFID) &&
             (u4AsafiMask == CAP_MP_VPN4_UNICAST))
    {
        /* Route is Vpnv4 route and is intended to be removed from BGP-VRF.
         * Just remove the route from VRF
         */
        Bgp4Vpnv4SetVrfFlags (pDelRtProfile, u4VrfId, BGP4_RT_PROCESS_VRF_RIB);
        i4Status = Bgp4TrieDeleteEntry (pDelRtProfile, pRibNode,
                                        u4VrfId, &u1IsBestRoute);
        Bgp4Vpnv4ResetVrfFlags (pDelRtProfile, u4VrfId,
                                BGP4_RT_PROCESS_VRF_RIB);
        if (i4Status == BGP4_SUCCESS)
        {
            /* Since the route is no more present, remove the route from
             * the nexthop list. */
            if ((BGP4_IS_ROUTE_VALID (pDelRtProfile) == BGP4_TRUE) &&
                (BGP4_RT_PROTOCOL (pDelRtProfile) == BGP_ID) &&
                (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pDelRtProfile))
                 == BGP4_VPN4_CE_PEER))
            {
                Bgp4RemoveRouteFromIdentNextHopList (pDelRtProfile, u4VrfId,
                                                     BGP4_INFO_NEXTHOP_INFO
                                                     (BGP4_RT_BGP_INFO
                                                      (pDelRtProfile)));
            }
        }
    }
#endif
    BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                   BGP4_FDB_TRC, BGP4_MOD_NAME,
                   "\tDeleting the received route %s with VRF ID %u from RIB information was %s.\r\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pDelRtProfile),
                                    BGP4_RT_AFI_INFO (pDelRtProfile)), u4VrfId,
                   Bgp4PrintCodeName ((UINT1) i4Status, BGP4_RETURN_STATUS));
    return i4Status;
}

/*****************************************************************************/
/* Function Name : Bgp4RibhGetFirstOverlapMoreSpecificRoute                  */
/* Description   : This function takes the route profile as input and checks */
/*                 whether an overlapping more-specific route is present in  */
/*                 the LOCAL RIB or not. If yes, then returns the first such */
/*                 overlapping more-specific route.                          */
/* Input(s)      : Pointer to the route profile for which the overlapping    */
/*                 more specific route needs to be found.                    */
/* Output(s)     : Pointer to the first overlapping more-specific route to   */
/*                 the given route. (pDestRoute)                             */
/*                 Pointer to the treenode in which the route is present     */
/*                 (pRibNode)                                                */
/* Return(s)     : BGP4_SUCCESS - if overlapping more-specific route present */
/*                 BGP4_FAILURE - if not present.                            */
/*****************************************************************************/
INT4
Bgp4RibhGetFirstOverlapMoreSpecificRoute (tRouteProfile * pRtProfile,
                                          tRouteProfile ** pDestRoute,
                                          VOID **pRibNode)
{
    tRouteProfile      *pCurRtProfile = NULL;
    tRouteProfile      *pNextRt = NULL;
    UINT4               u4PrefixLen = 0;
    INT4                i4RetVal = 0;
    UINT4               u4AsafiMask;
    UINT1               u1MaxPrefLen;

    u4PrefixLen = BGP4_RT_PREFIXLEN (pRtProfile);

    i4RetVal = Bgp4RibhLookupRtEntry (pRtProfile, BGP4_RT_CXT_ID (pRtProfile),
                                      BGP4_TREE_FIND, &pCurRtProfile, pRibNode);
    if (i4RetVal == BGP4_FAILURE)
    {
        /* No  overlapping route present in the RIB. */
        *pDestRoute = NULL;
        *pRibNode = NULL;
        return BGP4_FAILURE;
    }
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
        case CAP_MP_VPN4_UNICAST:
#endif
            u1MaxPrefLen = BGP4_MAX_PREFIXLEN;
            break;
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
            u1MaxPrefLen = BGP4_MAX_IPV6_PREFIXLEN;
            break;
#endif
        default:
            *pDestRoute = NULL;
            *pRibNode = NULL;
            return (BGP4_FAILURE);
    }

    for (;;)
    {
        BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pCurRtProfile),
                               BGP4_RT_SAFI_INFO (pCurRtProfile), u4AsafiMask);
#ifdef L3VPN
        if ((u4AsafiMask != CAP_MP_VPN4_UNICAST) ||
            ((u4AsafiMask == CAP_MP_VPN4_UNICAST) &&
             (MEMCMP (BGP4_RT_ROUTE_DISTING (pRtProfile),
                      BGP4_RT_ROUTE_DISTING (pCurRtProfile),
                      BGP4_VPN4_ROUTE_DISTING_SIZE) == 0)))
#endif
        {
            if ((AddrGreaterThan
                 (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                  (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
                  BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                  (BGP4_RT_NET_ADDRESS_INFO (pCurRtProfile)),
                  u1MaxPrefLen)) != BGP4_TRUE)
            {
                /* The input route is either less than or equal to the current
                 * route. Check whether the routes are overlapping and the
                 * current route is more-specific to the input route. If yes,
                 * then return the current route. Else there is no more-specific
                 * overlapping route present for the input route. */
                if (u4PrefixLen > BGP4_RT_PREFIXLEN (pCurRtProfile))
                {
                    /* The current route cant be a overlapping more-specific route
                     * route. Process the next route. */
                }
                else
                {
                    /* Current route's prefix len is greater than the input routes
                     * prefix length. So current route is more-specific to input
                     * route. */
                    if ((AddrMatch
                         (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                          (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
                          BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                          (BGP4_RT_NET_ADDRESS_INFO (pCurRtProfile)),
                          u4PrefixLen)) == BGP4_TRUE)
                    {
                        /* Addr matches. Current route is overlapping
                         * more-specific route to the input route. */
                        *pDestRoute = pCurRtProfile;
                        return BGP4_SUCCESS;
                    }
                    else
                    {
                        /* Route is more-specific but not overlapping. so no more
                         * overlapping route will be present in the RIB. */
                        break;
                    }
                }
            }
        }
#ifdef L3VPN
        else
        {
            /* Input route is greater than the current route in the RIB. So
             * get the next entry and process. */
        }
#endif
        i4RetVal =
            Bgp4RibhGetNextEntry (pCurRtProfile, BGP4_RT_CXT_ID (pRtProfile),
                                  &pNextRt, pRibNode, TRUE);
        if (i4RetVal == BGP4_FAILURE)
        {
            /* No more route present in the RIB. */
            break;
        }

        pCurRtProfile = pNextRt;
        pNextRt = NULL;
    }

    *pDestRoute = NULL;
    *pRibNode = NULL;
    return (BGP4_FAILURE);
}

/*****************************************************************************/
/* Function Name : Bgp4RibhGetFirstPeerRtEntry                               */
/* Description   : This function is called to obtain the first peer route in */
/*                 the path attribute table.                                 */
/* Input(s)      : Head Node of the Tree from which search needs to be done  */
/*                 (pTreeNode)                                               */
/*                 Peer Information (u4Peer)                                 */
/* Output(s)     : None.                                                     */
/* Return(s)     : First peer route in the Path Attribute Table.             */
/*****************************************************************************/
tRouteProfile      *
Bgp4RibhGetFirstPeerRtEntry (tBgp4PeerEntry * pPeer)
{
    tRouteProfile      *pRtProfileList = NULL;
    tRouteProfile      *pPeerRt = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4Status = BGP4_FAILURE;
    UINT4               u4Index;

    for (u4Index = 0; u4Index < (BGP4_MPE_ADDR_FAMILY_MAX_INDEX - 1); u4Index++)
    {
        if ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) & gau4NegAsafiMask[u4Index])
            != gau4NegAsafiMask[u4Index])
        {
            continue;
        }

        pRtProfileList = Bgp4RibhGetFirstAsafiPeerRtEntry (pPeer,
                                                           gau4AsafiMask
                                                           [u4Index]);
        if (pRtProfileList == NULL)
        {
            continue;
        }
        for (;;)
        {
            pPeerRt = Bgp4DshGetPeerRtFromProfileList (pRtProfileList, pPeer);
            if (pPeerRt != NULL)
            {
                return (pPeerRt);
            }
            i4Status =
                Bgp4RibhGetNextEntry (pRtProfileList, BGP4_PEER_CXT_ID (pPeer),
                                      &pRtProfileList, &pRibNode, TRUE);
            if (i4Status == BGP4_FAILURE)
            {
                break;
            }
        }
    }
    return (NULL);
}

/*****************************************************************************/
/* Function Name : Bgp4RibhGetFirstAsafiPeerRtEntry                          */
/* Description   : This function is called to obtain the first peer route in */
/*                 the path attribute table.                                 */
/* Input(s)      : Peer Information (u4Peer)                                 */
/*                 u4AsafiMask - mask that uniquely identifies <AFI, SAFI>   */
/* Output(s)     : None.                                                     */
/* Return(s)     : First peer route in the Path Attribute Table.             */
/*****************************************************************************/
tRouteProfile      *
Bgp4RibhGetFirstAsafiPeerRtEntry (tBgp4PeerEntry * pPeer, UINT4 u4AsafiMask)
{
    tRouteProfile      *pRtProfileList = NULL;
    tRouteProfile      *pGetNextRtProfileList = NULL;
    tRouteProfile      *pPeerRt = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4Status = BGP4_FAILURE;

#ifdef L3VPN
    if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
    {
        pRibNode = BGP4_VPN4_RIB_TREE (BGP4_PEER_CXT_ID (pPeer));
    }
#endif
    Bgp4GetRibNode (BGP4_PEER_CXT_ID (pPeer), u4AsafiMask, &pRibNode);
    if (pRibNode == NULL)
    {
        return (NULL);
    }
    i4Status = Bgp4RibhGetFirstEntry (u4AsafiMask, &pRtProfileList, &pRibNode,
                                      TRUE);
    if (pRtProfileList == NULL)
    {
        return (NULL);
    }

    for (;;)
    {
        pPeerRt = Bgp4DshGetPeerRtFromProfileList (pRtProfileList, pPeer);
        if (pPeerRt != NULL)
        {
            return (pPeerRt);
        }
        i4Status =
            Bgp4RibhGetNextEntry (pRtProfileList, BGP4_PEER_CXT_ID (pPeer),
                                  &pGetNextRtProfileList, &pRibNode, TRUE);
        if (i4Status == BGP4_FAILURE)
        {
            return (NULL);
        }
        pRtProfileList = pGetNextRtProfileList;
    }
}

/*****************************************************************************/
/* Function Name : Bgp4RibhGetNextIpv4PeerRtEntry                            */
/* Description   : This function will gets the next route information learned*/
/*                 from a specified peer for a given IPV4 route              */
/* Input(s)      : pRtProfile - route information for which the next peer    */
/*                 route entry is to be given                                */
/*                 pPeer - the peer information from which the next route    */
/*                 should be learnt                                          */
/* Output(s)     : None.                                                     */
/* Return(s)     : Next route information, if present                        */
/*                 NULL, if not present                                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4RibhGetNextIpv4PeerRtEntry (tRouteProfile * pRtProfile,
                                tBgp4PeerEntry * pPeer)
{
    tRouteProfile      *pRtProfileList = NULL;
    tRouteProfile      *pFndRtProfile = NULL;
    tRouteProfile      *pNextRtList = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4Status = BGP4_FAILURE;
    UINT4               u4AsafiMask;
    INT4                i4RtFound;
    UINT4               u4Context = 0;

    u4AsafiMask = ((UINT4) BGP4_RT_AFI_INFO (pRtProfile) << BGP4_TWO_BYTE_BITS)
        | BGP4_RT_SAFI_INFO (pRtProfile);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
        case CAP_MP_VPN4_UNICAST:
#endif
            break;
        default:
            return NULL;
    }

    u4Context = BGP4_PEER_CXT_ID (pPeer);
    i4RtFound = Bgp4RibhLookupRtEntry (pRtProfile, u4Context,
                                       BGP4_TREE_FIND_EXACT,
                                       &pRtProfileList, &pRibNode);
    if (i4RtFound == BGP4_FAILURE)
    {
        /* Input route is not present in RIB. */
        return NULL;
    }
    for (;;)
    {
        i4Status = Bgp4RibhGetNextEntry (pRtProfileList, u4Context,
                                         &pNextRtList, &pRibNode, TRUE);
        if (i4Status == BGP4_FAILURE)
        {
            /* No more route is present in RIB. */
            break;
        }
        pRtProfileList = pNextRtList;
        pNextRtList = NULL;

        pFndRtProfile = Bgp4DshGetPeerRtFromProfileList (pRtProfileList, pPeer);
        if (pFndRtProfile != NULL)
        {
            return (pFndRtProfile);
        }
    }
    return (NULL);
}

#ifdef BGP4_IPV6_WANTED
/*****************************************************************************/
/* Function Name : Bgp4RibhGetNextIpv6PeerRtEntry                            */
/* Description   : This function will gets the next route information learned*/
/*                 from a specified peer for a given IPV6 route              */
/* Input(s)      : pRtProfile - route information for which the next peer    */
/*                 route entry is to be given                                */
/*                 pPeer - the peer information from which the next route    */
/*                 should be learnt                                          */
/* Output(s)     : None.                                                     */
/* Return(s)     : Next route information, if present                        */
/*                 NULL, if not present                                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4RibhGetNextIpv6PeerRtEntry (tRouteProfile * pRtProfile,
                                tBgp4PeerEntry * pPeer)
{
    tRouteProfile      *pRtProfileList = NULL;
    tRouteProfile      *pFndRtProfile = NULL;
    tRouteProfile      *pNextRtList = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4Status = BGP4_FAILURE;
    UINT4               u4AsafiMask;
    INT4                i4RtFound;

    u4AsafiMask = ((UINT4) BGP4_RT_AFI_INFO (pRtProfile) << BGP4_TWO_BYTE_BITS)
        | BGP4_RT_SAFI_INFO (pRtProfile);
    if (u4AsafiMask != CAP_MP_IPV6_UNICAST)
    {
        return NULL;
    }

    i4RtFound = Bgp4RibhLookupRtEntry (pRtProfile, BGP4_PEER_CXT_ID (pPeer),
                                       BGP4_TREE_FIND_EXACT,
                                       &pRtProfileList, &pRibNode);
    if (i4RtFound == BGP4_FAILURE)
    {
        /* Input route is not present in RIB. */
        return NULL;
    }
    for (;;)
    {
        i4Status =
            Bgp4RibhGetNextEntry (pRtProfileList, BGP4_PEER_CXT_ID (pPeer),
                                  &pNextRtList, &pRibNode, TRUE);
        if (i4Status == BGP4_FAILURE)
        {
            break;
        }
        pRtProfileList = pNextRtList;
        pNextRtList = NULL;

        pFndRtProfile = Bgp4DshGetPeerRtFromProfileList (pRtProfileList, pPeer);
        if (pFndRtProfile != NULL)
        {
            return (pFndRtProfile);
        }
    }
    return (NULL);
}
#endif

/*****************************************************************************/
/* Function Name : Bgp4RibhGetNextPeerRtEntry                                */
/* Description   : This function will gets the next route information learned*/
/*                 from a specified peer for a given route                   */
/* Input(s)      : pRtProfile - route information for which the next peer    */
/*                 route entry is to be given                                */
/*                 pPeer - the peer information from which the next route    */
/*                 should be learnt                                          */
/* Output(s)     : None.                                                     */
/* Return(s)     : Next route information, if present                        */
/*                 NULL, if not present                                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4RibhGetNextPeerRtEntry (tRouteProfile * pRtProfile, tBgp4PeerEntry * pPeer)
{
    tRouteProfile      *pRtProfileList = NULL;
    tRouteProfile      *pFndRtProfile = NULL;
    tRouteProfile      *pNextRtList = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4Status = BGP4_FAILURE;
    UINT4               u4Index;
    UINT4               u4AsafiMask;
    INT4                i4RtFound;

    u4AsafiMask = ((UINT4) BGP4_RT_AFI_INFO (pRtProfile) << BGP4_TWO_BYTE_BITS)
        | BGP4_RT_SAFI_INFO (pRtProfile);
    /* Get the index corresponding to this mask */
    for (u4Index = 0; u4Index < (BGP4_MPE_ADDR_FAMILY_MAX_INDEX - 1); u4Index++)
    {
        if (gau4AsafiMask[u4Index] == u4AsafiMask)
        {
            break;
        }
    }

    i4RtFound = Bgp4RibhLookupRtEntry (pRtProfile, BGP4_PEER_CXT_ID (pPeer),
                                       BGP4_TREE_FIND_EXACT,
                                       &pRtProfileList, &pRibNode);
    if (i4RtFound == BGP4_FAILURE)
    {
        /* Input route is not present in RIB. */
        return NULL;
    }
    for (;;)
    {
        i4Status =
            Bgp4RibhGetNextEntry (pRtProfileList, BGP4_PEER_CXT_ID (pPeer),
                                  &pNextRtList, &pRibNode, TRUE);
        if (i4Status == BGP4_FAILURE)
        {
            break;
        }
        pRtProfileList = pNextRtList;
        pNextRtList = NULL;

        pFndRtProfile = Bgp4DshGetPeerRtFromProfileList (pRtProfileList, pPeer);
        if (pFndRtProfile != NULL)
        {
            return (pFndRtProfile);
        }
    }

    /* Route is not present in this <AFI, SAFI> RIB, go to the next RIB */
    for (++u4Index; u4Index < (BGP4_MPE_ADDR_FAMILY_MAX_INDEX - 1); u4Index++)
    {
        if ((gau4AsafiMask[u4Index] != CAP_MP_IPV4_UNICAST)
            && ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                 gau4AsafiMask[u4Index]) != gau4AsafiMask[u4Index]))
        {
            continue;
        }
        pRtProfileList =
            Bgp4RibhGetFirstAsafiPeerRtEntry (pPeer, gau4AsafiMask[u4Index]);
        if (pRtProfileList != NULL)
        {
            return (pRtProfileList);
        }
    }
    return (NULL);
}

/*****************************************************************************/
/* Function Name : Bgp4RibhGetFirstImportRtEntry                             */
/* Description   : This function will gets the first imported route info     */
/*                 (Always scans according to <AFI, SAFI> indices.           */
/* Input(s)      : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : route information, if present                             */
/*                 NULL, if not present                                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4RibhGetFirstImportRtEntry (UINT4 u4Context, VOID **pRibNode)
{
    tRouteProfile      *pRtProfileList = NULL;
    tRouteProfile      *pRtProfile = NULL;
    INT4                i4Status = BGP4_FAILURE;
    UINT4               u4Index;

    for (u4Index = 0; u4Index < (BGP4_MPE_ADDR_FAMILY_MAX_INDEX - 1); u4Index++)
    {
        Bgp4GetRibNode (u4Context, gau4AsafiMask[u4Index], pRibNode);
        if (*pRibNode == NULL)
        {
            continue;
        }
        pRtProfileList =
            Bgp4RibhGetFirstImportAsafiRtEntry (gau4AsafiMask[u4Index],
                                                pRibNode);
        if (pRtProfileList == NULL)
        {
            continue;
        }
        for (;;)
        {
            pRtProfile = pRtProfileList;
            while (pRtProfile)
            {
                if ((pRtProfile->u1Protocol != BGP_ID) &&
                    (!(BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGR_ROUTE)))
                {
                    return (pRtProfile);
                }
                pRtProfile = BGP4_RT_NEXT (pRtProfile);
            }
            i4Status = Bgp4RibhGetNextEntry (pRtProfileList, u4Context,
                                             &pRtProfileList, pRibNode, TRUE);
            if (i4Status == BGP4_FAILURE)
            {
                break;
            }
        }
    }
    return (NULL);
}

/*****************************************************************************/
/* Function Name : Bgp4RibhGetFirstImportAsafiRtEntry                        */
/* Description   : This function will gets the first imported route info     */
/*                 in a specified <AFI, SAFI> pair RIB                       */
/* Input(s)      : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : route information, if present                             */
/*                 NULL, if not present                                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4RibhGetFirstImportAsafiRtEntry (UINT4 u4AsafiMask, VOID **pRibNode)
{
    tRouteProfile      *pRtProfileList = NULL;
    tRouteProfile      *pRtProfile = NULL;
    INT4                i4Status = BGP4_FAILURE;
    UINT4               u4Context = 0;

    i4Status = Bgp4RibhGetFirstEntry (u4AsafiMask, &pRtProfileList,
                                      pRibNode, TRUE);
    if (i4Status == BGP4_FAILURE)
    {
        return (NULL);
    }
    for (;;)
    {
        pRtProfile = pRtProfileList;
        u4Context = BGP4_RT_CXT_ID (pRtProfile);
        while (pRtProfile)
        {
            if ((pRtProfile->u1Protocol != BGP_ID) &&
                (!(BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGR_ROUTE)))
            {
                return (pRtProfile);
            }
            pRtProfile = BGP4_RT_NEXT (pRtProfile);
        }
        i4Status = Bgp4RibhGetNextEntry (pRtProfileList, u4Context,
                                         &pRtProfileList, pRibNode, TRUE);
        if (i4Status == BGP4_FAILURE)
        {
            return (NULL);
        }
    }
}

/*****************************************************************************/
/* Function Name : Bgp4RibhGetNextImportRtEntry                              */
/* Description   : This function will gets the next imported route info      */
/*                 for a given imported route                                */
/* Input(s)      : pRtProfile - route information for which the next imported*/
/*                 route entry is to be given                                */
/* Output(s)     : None.                                                     */
/* Return(s)     : Next route information, if present                        */
/*                 NULL, if not present                                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4RibhGetNextImportRtEntry (tRouteProfile * pRtProfile, VOID **pRibNode)
{
    tRouteProfile      *pRtProfileList = NULL;
    tRouteProfile      *pFndRtProfile = NULL;
    INT4                i4Status = BGP4_FAILURE;
    UINT4               u4AsafiMask;
    UINT4               u4Context = 0;

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
#ifdef L3VPN
    if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
    {
        return NULL;
    }
#endif
    pFndRtProfile = pRtProfile;
    u4Context = BGP4_RT_CXT_ID (pRtProfile);

    for (;;)
    {
        i4Status = Bgp4RibhGetNextEntry (pFndRtProfile, u4Context,
                                         &pRtProfileList, pRibNode, TRUE);
        if (i4Status == BGP4_FAILURE)
        {
            break;
        }

        pFndRtProfile = pRtProfileList;
        while (pRtProfileList)
        {
            if ((pRtProfileList->u1Protocol != BGP_ID) &&
                (pRtProfileList->u1Protocol != PROTO_INVALID) &&
                (!(BGP4_RT_GET_FLAGS (pRtProfileList) & BGP4_RT_AGGR_ROUTE)))
            {
                return (pRtProfileList);
            }
            pRtProfileList = BGP4_RT_NEXT (pRtProfileList);
        }
    }

    Bgp4RibhGetFirstRouteFromNextAddrFamily (u4Context, u4AsafiMask,
                                             &pRtProfileList);
    return (pRtProfileList);
}

INT4
Bgp4RibhGetFirstRouteFromNextAddrFamily (UINT4 u4Context, UINT4 u4AsafiMask,
                                         tRouteProfile ** ppNextRtProfile)
{
    tRouteProfile      *pRtProfileList = NULL;
    VOID               *pRibNode;
    UINT4               u4Index;

    *ppNextRtProfile = NULL;
    /* Get the index corresponding to this mask */

    for (u4Index = 0; u4Index < (BGP4_MPE_ADDR_FAMILY_MAX_INDEX - 1); u4Index++)
    {
        if (gau4AsafiMask[u4Index] == u4AsafiMask)
        {
            break;
        }
    }

    /* Route is not present in this <AFI, SAFI> RIB, go to the next RIB */
    for (++u4Index; u4Index < (BGP4_MPE_ADDR_FAMILY_MAX_INDEX - 1); u4Index++)
    {
        switch (gau4AsafiMask[u4Index])
        {
#ifdef L3VPN
            case CAP_MP_VPN4_UNICAST:
            case CAP_MP_LABELLED_IPV4:
                continue;
#endif
#ifdef VPLSADS_WANTED
            case CAP_MP_L2VPN_VPLS:
                continue;
#endif
#ifdef EVPN_WANTED
            case CAP_MP_L2VPN_EVPN:
                continue;
#endif
            default:
            {
                Bgp4GetRibNode (u4Context, gau4AsafiMask[u4Index], &pRibNode);
                if (pRibNode == NULL)
                {
                    continue;
                }
                pRtProfileList =
                    Bgp4RibhGetFirstImportAsafiRtEntry (gau4AsafiMask[u4Index],
                                                        &pRibNode);
            }
                if (pRtProfileList != NULL)
                {
                    *ppNextRtProfile = pRtProfileList;
                }
                break;
        }
    }
    return BGP4_SUCCESS;
}

#ifdef L3VPN
/*****************************************************************************/
/* Function Name : Bgp4RibhGetNextVrfImportRtEntry                           */
/* Description   : This function will gets the next imported route info 
 *                 from Vrf   for a given imported route                     */
/* Input(s)      : pRtProfile - route information for which the next imported*/
/*                 route entry is to be given                                */
/* Output(s)     : None.                                                     */
/* Return(s)     : Next route information, if present                        */
/*                 NULL, if not present                                      */
/*****************************************************************************/
INT4
Bgp4RibhGetNextVrfImportRtEntry (tRouteProfile * pRtProfile,
                                 UINT4 u4VrfId, tRouteProfile ** pNextRtProfile,
                                 UINT4 *pu4VrfId)
{
    tRouteProfile      *pRtProfileList = NULL;
    tRouteProfile      *pFndRtProfile = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4Status = BGP4_FAILURE;
    UINT1               u1NextVrf = BGP4_FALSE;

    pFndRtProfile = pRtProfile;
    do
    {
        if (u1NextVrf == BGP4_TRUE)
        {
            pRibNode = BGP4_VPN4_RIB_TREE (u4VrfId);
            i4Status = Bgp4RibhGetFirstEntry (CAP_MP_VPN4_UNICAST,
                                              &pRtProfileList, &pRibNode, TRUE);
            if (i4Status == BGP4_FAILURE)
            {
                continue;
            }
            while (pRtProfileList)
            {
                if ((pRtProfileList->u1Protocol != BGP_ID) &&
                    (pRtProfileList->u1Protocol != PROTO_INVALID) &&
                    (!(BGP4_RT_GET_FLAGS (pRtProfileList) &
                       BGP4_RT_AGGR_ROUTE)))
                {
                    *pNextRtProfile = pRtProfileList;
                    *pu4VrfId = u4VrfId;
                    return BGP4_SUCCESS;
                }
                pRtProfileList = BGP4_RT_NEXT (pRtProfileList);
                pFndRtProfile = pRtProfileList;
            }
        }
        for (;;)
        {
            i4Status = Bgp4RibhGetNextEntry (pFndRtProfile, u4VrfId,
                                             &pRtProfileList, &pRibNode, TRUE);
            if (i4Status == BGP4_FAILURE)
            {
                u1NextVrf = BGP4_TRUE;
                break;
            }
            pFndRtProfile = pRtProfileList;
            while (pRtProfileList)
            {
                if ((pRtProfileList->u1Protocol != BGP_ID) &&
                    (pRtProfileList->u1Protocol != PROTO_INVALID) &&
                    (!(BGP4_RT_GET_FLAGS (pRtProfileList) &
                       BGP4_RT_AGGR_ROUTE)))
                {
                    *pNextRtProfile = pRtProfileList;
                    *pu4VrfId = u4VrfId;
                    return BGP4_SUCCESS;
                }
                pRtProfileList = BGP4_RT_NEXT (pRtProfileList);
            }
        }
    }
    while (BgpGetNextActiveContextID (u4VrfId, &u4VrfId) == SNMP_SUCCESS);

    *pNextRtProfile = NULL;
    *pu4VrfId = 0;

    return BGP4_FAILURE;
}
#endif

#endif /* BGRBIFUN_C */
