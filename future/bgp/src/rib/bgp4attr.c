/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4attr.c,v 1.46 2017/09/15 06:19:55 siva Exp $
 *
 * Description: This file contains modules which are used to process 
 *              the Path attributes when the update needs to be sent 
 *              to other known peers.
 *
 *******************************************************************/

#ifndef   BGP4ATTR_C
#define   BGP4ATTR_C

#include "bgp4com.h"

PRIVATE
    INT4        Bgp4IsRouteInList (tBGP4_DLL * pList, tRouteProfile * pRoute);
/*****************************************************************************/
/* Function Name : Bgp4PAFormHashKey                                         */
/* Description   : This routine forms the Key for the given BGP Info.        */
/* Input(s)      : pBgpInfo - Pointer to the BGP Info for which the hash key */
/*                            needs to be found.                             */
/*                 u4MaxIndex - Max Bucket size of the corresponding Hash Tbl*/
/* Output(s)     : pu4HashKey - Key derived from the incoming bgp info.      */
/* Return(s)     : BGP4_SUCCESS / BGP4_FAILURE                               */
/*****************************************************************************/
INT4
Bgp4PAFormHashKey (tBgp4Info * pBgpInfo, UINT4 *pu4HashKey, UINT4 u4MaxIndex)
{
    tSnpaInfoNode      *pSnpaInfoNode = NULL;
    tAsPath            *pAspath = NULL;
    UINT1              *pau1Buf = NULL;
    UINT4              *pu4AsList = NULL;
    UINT4               u4Index;
    UINT4               u4HashKey = 0;
    UINT1               u1AsCount = 0;

#ifdef VPLSADS_WANTED
    UNUSED_PARAM (pSnpaInfoNode);
#endif

    u4HashKey += BGP4_INFO_ATTR_FLAG (pBgpInfo);

    /* Origin Attribute. */
    u4HashKey += BGP4_INFO_ORIGIN (pBgpInfo);

    /* AS Path Attribute */
    pAspath = (tAsPath *) TMO_SLL_First (BGP4_INFO_ASPATH (pBgpInfo));
    while (pAspath != NULL)
    {
        u4HashKey += BGP4_ASPATH_TYPE (pAspath);
        u4HashKey += BGP4_ASPATH_LEN (pAspath);

        u1AsCount = BGP4_ASPATH_LEN (pAspath);
        pu4AsList = (UINT4 *) (VOID *) BGP4_ASPATH_NOS (pAspath);
        while (u1AsCount > 0)
        {
            u4HashKey += OSIX_NTOHL (*pu4AsList);
            u1AsCount--;
            pu4AsList++;
        }

        /* Next AS segment. */
        pAspath = (tAsPath *) TMO_SLL_Next (BGP4_INFO_ASPATH (pBgpInfo),
                                            &pAspath->sllNode);
    }

    /* Next-Hop Attribute. */
    pau1Buf = BGP4_INFO_NEXTHOP (pBgpInfo);
    for (u4Index = 0; u4Index < BGP4_MAX_INET_ADDRESS_LEN; u4Index++)
    {
        u4HashKey += pau1Buf[u4Index];
    }

    /* MED Attribute. */
    if ((BGP4_INFO_ATTR_FLAG (pBgpInfo) & BGP4_ATTR_MED_MASK) ==
        BGP4_ATTR_MED_MASK)
    {
        u4HashKey += BGP4_INFO_RCVD_MED (pBgpInfo);
    }

    if ((BGP4_INFO_ATTR_FLAG (pBgpInfo) & BGP4_ATTR_SEND_MED_MASK) ==
        BGP4_ATTR_SEND_MED_MASK)
    {
        u4HashKey += BGP4_INFO_SEND_MED (pBgpInfo);
    }

    /* Local Preference. */
    if ((BGP4_INFO_ATTR_FLAG (pBgpInfo) & BGP4_ATTR_LOCAL_PREF_MASK) ==
        BGP4_ATTR_LOCAL_PREF_MASK)
    {
        u4HashKey += BGP4_INFO_RCVD_LOCAL_PREF (pBgpInfo);
    }

    /* Aggregator Attribute. */
    if ((BGP4_INFO_ATTR_FLAG (pBgpInfo) & BGP4_ATTR_AGGREGATOR_MASK) ==
        BGP4_ATTR_AGGREGATOR_MASK)
    {
        u4HashKey += BGP4_INFO_AGGREGATOR_AS (pBgpInfo);
        u4HashKey += BGP4_INFO_AGGREGATOR_NODE (pBgpInfo);
        u4HashKey += BGP4_INFO_AGGREGATOR_FLAG (pBgpInfo);
    }

    /* Atomic Aggregator Attribute. */
    if ((BGP4_INFO_ATTR_FLAG (pBgpInfo) * BGP4_ATTR_ATOMIC_AGGR_MASK) ==
        BGP4_ATTR_ATOMIC_AGGR_MASK)
    {
        u4HashKey++;
    }

    /* Unknown Attribute. */
    if ((BGP4_INFO_ATTR_FLAG (pBgpInfo) & BGP4_ATTR_UNKNOWN_MASK)
        == BGP4_ATTR_UNKNOWN_MASK)
    {
        pau1Buf = BGP4_INFO_UNKNOWN_ATTR (pBgpInfo);
        for (u4Index = 0; u4Index < BGP4_INFO_UNKNOWN_ATTR_LEN (pBgpInfo);
             u4Index++)
        {
            u4HashKey += pau1Buf[u4Index];
        }
    }

    /* Community Attribute */
    if (BGP4_INFO_COMM_ATTR (pBgpInfo) != NULL)
    {
        u4HashKey += BGP4_INFO_COMM_ATTR_FLAG (pBgpInfo);
        pau1Buf = BGP4_INFO_COMM_ATTR_VAL (pBgpInfo);
        for (u4Index = 0; u4Index < (UINT4)
             (BGP4_INFO_COMM_COUNT (pBgpInfo) * COMM_VALUE_LEN); u4Index++)
        {
            u4HashKey += pau1Buf[u4Index];
        }
    }

    /* Extended Community Attribute. */
    if (BGP4_INFO_ECOMM_ATTR (pBgpInfo) != NULL)
    {
        u4HashKey += BGP4_INFO_ECOMM_ATTR_FLAG (pBgpInfo);
        pau1Buf = BGP4_INFO_ECOMM_ATTR_VAL (pBgpInfo);
        for (u4Index = 0; u4Index < (UINT4)
             (BGP4_INFO_ECOMM_COUNT (pBgpInfo) * EXT_COMM_VALUE_LEN); u4Index++)
        {
            u4HashKey += pau1Buf[u4Index];
        }
    }

    /* Originator-Id Attribute. */
    if ((BGP4_INFO_ATTR_FLAG (pBgpInfo) & BGP4_ATTR_ORIG_ID_MASK)
        == BGP4_ATTR_ORIG_ID_MASK)
    {
        u4HashKey += BGP4_INFO_ORIG_ID (pBgpInfo);
    }

    /* Cluster-List Attribute */
    if (BGP4_INFO_CLUS_LIST_ATTR (pBgpInfo) != NULL)
    {
        u4HashKey += BGP4_INFO_CLUS_LIST_ATTR_FLAG (pBgpInfo);
        pau1Buf = BGP4_INFO_CLUS_LIST_ATTR_VAL (pBgpInfo);
        for (u4Index = 0; u4Index < (UINT4)
             (BGP4_INFO_CLUS_LIST_COUNT (pBgpInfo) * CLUSTER_ID_LENGTH);
             u4Index++)
        {
            u4HashKey += pau1Buf[u4Index];
        }
    }

#ifdef BGP4_IPV6_WANTED
    if (BGP4_INFO_LINK_LOCAL_PRESENT (pBgpInfo) == BGP4_TRUE)
    {
        /* Link Local Address is present. Include it for Key calculation. */
        pau1Buf = BGP4_INFO_LINK_LOCAL_ADDR (pBgpInfo);
        for (u4Index = 0; u4Index < BGP4_MAX_INET_ADDRESS_LEN; u4Index++)
        {
            u4HashKey += pau1Buf[u4Index];
        }
    }
#endif

#ifndef VPLSADS_WANTED
    TMO_SLL_Scan (BGP4_MPE_SNPA_INFO_LIST (pBgpInfo), pSnpaInfoNode,
                  tSnpaInfoNode *)
    {
        pau1Buf = BGP4_MPE_SNPA_NODE_INFO (pSnpaInfoNode);
        for (u4Index = 0; u4Index < BGP4_MAX_INET_ADDRESS_LEN; u4Index++)
        {
            u4HashKey += pau1Buf[u4Index];
        }
    }
#endif

    *pu4HashKey = u4HashKey % u4MaxIndex;
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name : Bgp4CreateRcvdPADB                                         */
/* Description   : This function create the Receive Path Attribtue Database.  */
/* Input(s)      : None                                                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/

INT4
Bgp4CreateRcvdPADB (UINT4 u4CxtId)
{
    UINT4               u4AsafiIndex = 0;

    for (u4AsafiIndex = 0; u4AsafiIndex < BGP4_MPE_ADDR_FAMILY_MAX_INDEX;
         u4AsafiIndex++)
    {
#ifdef L3VPN
        if (u4AsafiIndex == BGP4_IPV4_LBLD_INDEX)
        {
            continue;
        }
#endif
        BGP4_RCVD_PA_DB (u4CxtId, u4AsafiIndex) =
            TMO_HASH_Create_Table (BGP4_MAX_RCVD_PA_HASH_INDEX, NULL, FALSE);

        if (BGP4_RCVD_PA_DB (u4CxtId, u4AsafiIndex) == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC |
                      BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tBgp4CreateRcvdPATable() : CREATING HASH TABLE For"
                      "Received Path Attribute FAILED !!!\n");
            Bgp4DeleteRcvdPADB (u4CxtId);
            return (BGP4_FAILURE);
        }
    }
    return (BGP4_SUCCESS);
}

/******************************************************************************/
/* Function Name : Bgp4DeleteRcvdPADB                                         */
/* Description   : This function delete the Received Path Attribute Database  */
/* Input(s)      : None                                                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4DeleteRcvdPADB (UINT4 u4Context)
{
    tTMO_HASH_NODE     *pNode = NULL;
    tTMO_HASH_NODE     *pNextNode = NULL;
    tBgp4RcvdPathAttr  *pRcvdPAEntry = NULL;
    UINT4               u4AsafiIndex = 0;
    UINT4               u4HashKey;
    UINT2               u2Afi = 0;
    UINT2               u2Safi = 0;

    for (u4AsafiIndex = 0; u4AsafiIndex < BGP4_MPE_ADDR_FAMILY_MAX_INDEX;
         u4AsafiIndex++)
    {
#ifdef L3VPN
        if (u4AsafiIndex == BGP4_IPV4_LBLD_INDEX)
        {
            continue;
        }
#endif
        if (BGP4_RCVD_PA_DB (u4Context, u4AsafiIndex) != NULL)
        {
            /* When the PADB table is deleted, in some rare cases, the entry has
             * non-zero count value with null route profiles. So to avoid crash 
             * in such scenario, as a workaround as the route profile mempools will be freed
             * subsequently, the PA entry is alone freed here */
            TMO_HASH_Scan_Table (BGP4_RCVD_PA_DB (u4Context, u4AsafiIndex),
                                 u4HashKey)
            {
                TMO_HASH_DYN_Scan_Bucket (BGP4_RCVD_PA_DB
                                          (u4Context, u4AsafiIndex), u4HashKey,
                                          pNode, pNextNode, tTMO_HASH_NODE *)
                {
                    pRcvdPAEntry = (tBgp4RcvdPathAttr *) pNode;
                    Bgp4GetAfiSafiFromIndex (u4AsafiIndex, &u2Afi, &u2Safi);
                    Bgp4RcvdPAReleasePAInfo (u4Context, pRcvdPAEntry, u2Afi,
                                             u2Safi);
                }
            }
            BGP4_RCVD_PA_DB (u4Context, u4AsafiIndex) = NULL;
        }
    }
    return (BGP4_SUCCESS);
}

/****************************************************************************/
/* Function    :  Bgp4RcvdPAReleasePAInfo                                   */
/* Description :  This function checks whether this PA entry is associated  */
/*                with any Peer_Init Info or not. If yes, then update that  */
/*                PeerInit Info. Then free the PA Info memory.              */
/* Input       :  pDelRcvdPA  - Received Path Attribute entry to be deleted */
/*             :  u2Afi - Address Family to which this entry belongs.       */
/*             :  u2Safi - Address Family to which this entry belongs.      */
/* Output      :  none.                                                     */
/* Returns     :  None.                                                     */
/****************************************************************************/
VOID
Bgp4RcvdPAReleasePAInfo (UINT4 u4ContextId, tBgp4RcvdPathAttr * pDelRcvdPA,
                         UINT2 u2Afi, UINT2 u2Safi)
{
    tBgp4RcvdPathAttr  *pNextRcvdPA = NULL;
    tBgp4PeerEntry     *pPeerentry = NULL;
    UINT4               u4HashKey = 0;
    UINT4               u4NextHashKey = 0;
    UINT4               u4IsNextPAFound = BGP4_FALSE;
    UINT4               u4AsafiIndex = 0;
    INT4                i4Status = BGP4_SUCCESS;

    Bgp4PAFormHashKey (BGP4_PA_BGP4INFO (pDelRcvdPA), &u4HashKey,
                       BGP4_MAX_RCVD_PA_HASH_INDEX);
    Bgp4GetAfiSafiIndex (u2Afi, u2Safi, &u4AsafiIndex);
#ifdef L3VPN
    /* Carrying Label Information - RFC 3107 */
    /* Since we are maining same RIB for both IPv4 unlabelled and labelled
     * routes, the received PA database should consider the both as same
     */
    if (u4AsafiIndex == BGP4_IPV4_LBLD_INDEX)
    {
        u4AsafiIndex = BGP4_IPV4_UNI_INDEX;
    }
#endif

    /* Scan all the peers in the peer init list. */
    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4ContextId), pPeerentry,
                  tBgp4PeerEntry *)
    {
        if (BGP4_PEER_INIT_PA_ENTRY (pPeerentry) == pDelRcvdPA)
        {
            /* Matching Get the next RCVD_PA_ENTRY. */
            if (u4IsNextPAFound == BGP4_FALSE)
            {
                i4Status =
                    Bgp4RcvdPADBGetNextEntry (u4ContextId, pDelRcvdPA,
                                              u4HashKey, u2Afi, u2Safi,
                                              &pNextRcvdPA, &u4NextHashKey);
                if (i4Status == BGP4_FAILURE)
                {
                    /* No more RCVD_PA_ENTRY exists. */
                    pNextRcvdPA = NULL;
                    u4NextHashKey = BGP4_MAX_RCVD_PA_HASH_INDEX;
                }
                u4IsNextPAFound = BGP4_TRUE;
            }

            /* Update the PeerInit Info Structure. */
            BGP4_PEER_INIT_PA_ENTRY (pPeerentry) = pNextRcvdPA;
            BGP4_PEER_INIT_PA_HASHKEY (pPeerentry) = u4NextHashKey;
        }
    }

    BGP4_INFO_RCVD_PA_ENTRY (BGP4_PA_BGP4INFO (pDelRcvdPA)) = NULL;
    Bgp4DshReleaseBgpInfo (BGP4_PA_BGP4INFO (pDelRcvdPA));
    BGP4_PA_BGP4INFO (pDelRcvdPA) = NULL;

    TMO_HASH_Delete_Node (BGP4_RCVD_PA_DB (u4ContextId, u4AsafiIndex),
                          &pDelRcvdPA->NextNonIdentPA, u4HashKey);
    Bgp4MemReleasePAInfo (pDelRcvdPA);
    return;
}

/****************************************************************************/
/* Function    :  Bgp4RcvdPAClearPAEntry                                    */
/* Description :  This function clear all the structures associated with the*/
/*                given Received Path Attribute entry.                      */
/* Input       :  pDelRcvdPA  - Received Path Attribute entry to be deleted */
/* Output      :  none.                                                     */
/* Returns     :  None.                                                     */
/****************************************************************************/
VOID
Bgp4RcvdPAClearPAEntry (tTMO_HASH_NODE * pDelRcvdPA)
{
    tBgp4RcvdPathAttr  *pRcvdPAEntry = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pNextRtProfile = NULL;
    UINT2               u2Afi = 0;
    UINT2               u2Safi = 0;
    UINT4               u4ContextId = 0;

    pRcvdPAEntry = (tBgp4RcvdPathAttr *) pDelRcvdPA;

    /* First De-Link all the route-profile associated with this entry.
     * At no point of time, there will be an entry present in RCVD_PA
     * DB without any route. Extract the AFI, SAFI from that route. */
    if (BGP4_PA_IDENT_BEST_ROUTE_COUNT (pRcvdPAEntry) > 0)
    {
        pRtProfile = BGP4_PA_IDENT_BEST_ROUTE_FIRST (pRcvdPAEntry);
    }
    else
    {
        pRtProfile = BGP4_PA_IDENT_NON_BEST_ROUTE_FIRST (pRcvdPAEntry);
    }

    u2Afi = BGP4_RT_AFI_INFO (pRtProfile);
    u2Safi = BGP4_RT_SAFI_INFO (pRtProfile);
    u4ContextId = BGP4_RT_CXT_ID (pRtProfile);

    while (BGP4_PA_IDENT_BEST_ROUTE_COUNT (pRcvdPAEntry) > 0)
    {
        pRtProfile = BGP4_PA_IDENT_BEST_ROUTE_FIRST (pRcvdPAEntry);
        if (pRtProfile == NULL)
        {
            break;
        }
        pNextRtProfile = BGP4_RT_IDENT_ATTR_LINK_NEXT (pRtProfile);

        /* Update the DLL Head. */
        BGP4_PA_IDENT_BEST_ROUTE_FIRST (pRcvdPAEntry) = pNextRtProfile;

        /* Update the next-route prev. */
        if (pNextRtProfile != NULL)
        {
            BGP4_RT_IDENT_ATTR_LINK_PREV (pNextRtProfile) = NULL;
        }

        /* Clear the info from the Current route. */
        BGP4_RT_IDENT_ATTR_LINK_PREV (pRtProfile) = NULL;
        BGP4_RT_IDENT_ATTR_LINK_NEXT (pRtProfile) = NULL;

        /* Route has been released from the PA List. Release it. */
        Bgp4DshReleaseRtInfo (pRtProfile);

        /* Decrement the route count */
        BGP4_PA_IDENT_BEST_ROUTE_COUNT (pRcvdPAEntry)--;
    }

    while (BGP4_PA_IDENT_NON_BEST_ROUTE_COUNT (pRcvdPAEntry) > 0)
    {
        pRtProfile = BGP4_PA_IDENT_NON_BEST_ROUTE_FIRST (pRcvdPAEntry);
        if (pRtProfile == NULL)
        {
            break;
        }
        pNextRtProfile = BGP4_RT_IDENT_ATTR_LINK_NEXT (pRtProfile);

        /* Update the DLL Head. */
        BGP4_PA_IDENT_NON_BEST_ROUTE_FIRST (pRcvdPAEntry) = pNextRtProfile;

        /* Update the next-route prev. */
        if (pNextRtProfile != NULL)
        {
            BGP4_RT_IDENT_ATTR_LINK_PREV (pNextRtProfile) = NULL;
        }

        /* Clear the info from the Current route. */
        BGP4_RT_IDENT_ATTR_LINK_PREV (pRtProfile) = NULL;
        BGP4_RT_IDENT_ATTR_LINK_NEXT (pRtProfile) = NULL;

        /* Route has been released from the PA List. Release it. */
        Bgp4DshReleaseRtInfo (pRtProfile);

        /* Decrement the route count */
        BGP4_PA_IDENT_NON_BEST_ROUTE_COUNT (pRcvdPAEntry)--;
    }

    /* Release the Received PA entry. */
    Bgp4RcvdPAReleasePAInfo (u4ContextId, pRcvdPAEntry, u2Afi, u2Safi);
    return;
}

/****************************************************************************/
/* Function    :  Bgp4RcvdPADBAddEntry                                      */
/* Description :  This function adds a new entry to the given Path Attrib   */
/*             :  in the Recevied Path Attribute Database.                  */
/* Input       :  pRcvdBgpInfo - Pointer to the Received BGP Info.          */
/*             :  u2Afi - Address Family to which this entry belongs.       */
/*             :  u2Safi - Address Family to which this entry belongs.      */
/* Output      :  None                                                      */
/* Returns     :  Pointer to the RCVD_PA entry to which this bgp_info is    */
/*             :  added or                                                  */
/*             :  NULL - if any failure.                                    */
/****************************************************************************/
tBgp4RcvdPathAttr  *
Bgp4RcvdPADBAddEntry (UINT4 u4CxtId, tBgp4Info * pRcvdBgpInfo, UINT2 u2Afi,
                      UINT2 u2Safi)
{
    tBgp4RcvdPathAttr  *pBgp4RcvdPA = NULL;
    tBgp4Info          *pCurBgpInfo = NULL;
    UINT4               u4HashKey = 0;
    UINT4               u4AsafiIndex = 0;
    UINT1               u1IsMatchEntryExist = BGP4_FALSE;

    if (BGP4_INFO_RCVD_PA_ENTRY (pRcvdBgpInfo) != NULL)
    {
        /* Info already added to RCVD_PA_DB. so return */
        return (BGP4_INFO_RCVD_PA_ENTRY (pRcvdBgpInfo));
    }

    Bgp4GetAfiSafiIndex (u2Afi, u2Safi, &u4AsafiIndex);
#ifdef L3VPN
    /* Carrying Label Information - RFC 3107 */
    /* Since we are maining same RIB for both IPv4 unlabelled and labelled
     * routes, the received PA database should consider the both as same
     */
    if (u4AsafiIndex == BGP4_IPV4_LBLD_INDEX)
    {
        u4AsafiIndex = BGP4_IPV4_UNI_INDEX;
    }
#endif

    Bgp4PAFormHashKey (pRcvdBgpInfo, &u4HashKey, BGP4_MAX_RCVD_PA_HASH_INDEX);
    /* Check whether a matching entry exists for this BgpInfo or not. */
    TMO_HASH_Scan_Bucket (BGP4_RCVD_PA_DB (u4CxtId, u4AsafiIndex), u4HashKey,
                          pBgp4RcvdPA, tBgp4RcvdPathAttr *)
    {
        pCurBgpInfo = BGP4_PA_BGP4INFO (pBgp4RcvdPA);
        if (Bgp4AttrIsBgpinfosIdentical (pRcvdBgpInfo, pCurBgpInfo) == TRUE)
        {
            u1IsMatchEntryExist = BGP4_TRUE;
            break;
        }
    }

    if (u1IsMatchEntryExist == BGP4_FALSE)
    {
        /* No matching entry exist for the given BGP4INFO. Allocate a new
         * PA entry and link the received BGP4INFO with that entry. */
        pBgp4RcvdPA = Bgp4MemAllocatePAInfo (sizeof (tBgp4RcvdPathAttr));
        if (pBgp4RcvdPA == NULL)
        {
            return ((tBgp4RcvdPathAttr *) NULL);
        }
        TMO_HASH_Add_Node (BGP4_RCVD_PA_DB (u4CxtId, u4AsafiIndex),
                           &pBgp4RcvdPA->NextNonIdentPA, u4HashKey, NULL);

        /* Attach the received BGP4_INFO with this PA Entry. */
        BGP4_PA_BGP4INFO (pBgp4RcvdPA) = pRcvdBgpInfo;
        BGP4_INFO_REF_COUNT (pRcvdBgpInfo)++;
        BGP4_INFO_RCVD_PA_ENTRY (pRcvdBgpInfo) = pBgp4RcvdPA;
    }

    return (pBgp4RcvdPA);
}

/****************************************************************************/
/* Function    :  Bgp4RcvdPADBAddRoute                                      */
/* Description :  This function adds the route to the Received Path         */
/*             :  Attribute Database according to the Best route status. If */
/*             :  the route is best route it will be added to the best route*/
/*             :  list else the route will be added to the non-best route   */
/*             :  list.                                                     */
/* Input       :  pRtProfile - Pointer to the Route Profile structure.      */
/*             :  u1IsRouteBest - Status explaining whether the route is    */
/*             :                  best or not.                              */
/* Output      :  None                                                      */
/* Returns     :  BGP4_SUCCESS - if route is added successfully.            */
/*             :  BGP4_FAILURE - if route is not added.                     */
/****************************************************************************/
INT4
Bgp4RcvdPADBAddRoute (tRouteProfile * pRtProfile, UINT1 u1IsRouteBest)
{
    tBgp4RcvdPathAttr  *pRcvdPAEntry = NULL;
    tBGP4_DLL          *pBgp4Dll = NULL;
    tBGP4_DLL          *pBgp4Dll1 = NULL;
    tRouteProfile      *pPrevRoute = NULL;
    tRouteProfile      *pNextRoute = NULL;
    UINT1               u1IsRtAlreadyPresent = BGP4_FALSE;

    pRcvdPAEntry = BGP4_INFO_RCVD_PA_ENTRY (BGP4_RT_BGP_INFO (pRtProfile));
    if (pRcvdPAEntry == NULL)
    {
        return BGP4_FAILURE;
    }

    /* First check whether the route is already present in any of the list.
     * If yes, then un-link the route from that list. */
    if (u1IsRouteBest == BGP4_TRUE)
    {
        /* Route is Best route. Check for the route in non-Best route list. */
        pBgp4Dll = BGP4_PA_IDENT_NON_BEST_ROUTE_LIST (pRcvdPAEntry);
        pBgp4Dll1 = BGP4_PA_IDENT_BEST_ROUTE_LIST (pRcvdPAEntry);
    }
    else
    {
        /* Route is not Best route. Check for the route in Best route list. */
        pBgp4Dll = BGP4_PA_IDENT_BEST_ROUTE_LIST (pRcvdPAEntry);
        pBgp4Dll1 = BGP4_PA_IDENT_NON_BEST_ROUTE_LIST (pRcvdPAEntry);
    }
    if (Bgp4IsRouteInList (pBgp4Dll1, pRtProfile) == BGP4_SUCCESS)
    {
        return BGP4_SUCCESS;
    }

    if (Bgp4IsRouteInList (pBgp4Dll, pRtProfile) == BGP4_SUCCESS)
    {
        if ((BGP4_RT_IDENT_ATTR_LINK_PREV (pRtProfile) != NULL) ||
            (BGP4_RT_IDENT_ATTR_LINK_NEXT (pRtProfile) != NULL) ||
            (BGP4_DLL_FIRST_ROUTE (pBgp4Dll) == pRtProfile) ||
            (BGP4_DLL_LAST_ROUTE (pBgp4Dll) == pRtProfile))
        {
            /* Route is already present in the list. Unlink it. */
            pPrevRoute = BGP4_RT_IDENT_ATTR_LINK_PREV (pRtProfile);
            pNextRoute = BGP4_RT_IDENT_ATTR_LINK_NEXT (pRtProfile);

            if (pPrevRoute != NULL)
            {
                /* Update the previous routes next ident PA route pointer */
                BGP4_RT_IDENT_ATTR_LINK_NEXT (pPrevRoute) = pNextRoute;
            }
            else
            {
                /* This route is the first route in the list. Update the
                 * list first route value. */
                BGP4_DLL_FIRST_ROUTE (pBgp4Dll) = pNextRoute;
            }

            if (pNextRoute != NULL)
            {
                /* Update the next routes prev ident PA route pointer */
                BGP4_RT_IDENT_ATTR_LINK_PREV (pNextRoute) = pPrevRoute;
            }
            else
            {
                /* This route is the last route in the list. Update the list
                 * last route value. */
                BGP4_DLL_LAST_ROUTE (pBgp4Dll) = pPrevRoute;
            }

            /* Decrement the RCVD_PA entry route count. */
            BGP4_DLL_COUNT (pBgp4Dll)--;

            /* Update the Route profile RCVD_PA entry value. */
            BGP4_RT_IDENT_ATTR_LINK_PREV (pRtProfile) = NULL;
            BGP4_RT_IDENT_ATTR_LINK_NEXT (pRtProfile) = NULL;
            u1IsRtAlreadyPresent = BGP4_TRUE;
        }
    }

    /* Add the route to the RCVD_PA_ENTRY according to u1IsRouteBest flag. */
    if (u1IsRouteBest == BGP4_TRUE)
    {
        /* Add the route to the beginning of the Best route list. */
        BGP4_RT_IDENT_ATTR_LINK_PREV (pRtProfile) = NULL;
        BGP4_RT_IDENT_ATTR_LINK_NEXT (pRtProfile) =
            BGP4_PA_IDENT_BEST_ROUTE_FIRST (pRcvdPAEntry);
        if (BGP4_PA_IDENT_BEST_ROUTE_FIRST (pRcvdPAEntry) != NULL)
        {
            /* This route is not the first route added to this list. So update
             * the previous routes pointer. */
            BGP4_RT_IDENT_ATTR_LINK_PREV (BGP4_PA_IDENT_BEST_ROUTE_FIRST
                                          (pRcvdPAEntry)) = pRtProfile;
        }
        BGP4_PA_IDENT_BEST_ROUTE_FIRST (pRcvdPAEntry) = pRtProfile;
        if (BGP4_PA_IDENT_BEST_ROUTE_LAST (pRcvdPAEntry) == NULL)
        {
            /* This route is the first route added to this list. */
            BGP4_PA_IDENT_BEST_ROUTE_LAST (pRcvdPAEntry) = pRtProfile;
        }
        /* Increment the route count. */
        BGP4_PA_IDENT_BEST_ROUTE_COUNT (pRcvdPAEntry)++;
    }
    else
    {
        /* Add the route to the end of Non-Best route list. */
        BGP4_RT_IDENT_ATTR_LINK_NEXT (pRtProfile) = NULL;
        BGP4_RT_IDENT_ATTR_LINK_PREV (pRtProfile) =
            BGP4_PA_IDENT_NON_BEST_ROUTE_LAST (pRcvdPAEntry);
        if (BGP4_PA_IDENT_NON_BEST_ROUTE_LAST (pRcvdPAEntry) != NULL)
        {
            BGP4_RT_IDENT_ATTR_LINK_NEXT (BGP4_PA_IDENT_NON_BEST_ROUTE_LAST
                                          (pRcvdPAEntry)) = pRtProfile;
        }
        BGP4_PA_IDENT_NON_BEST_ROUTE_LAST (pRcvdPAEntry) = pRtProfile;
        if (BGP4_PA_IDENT_NON_BEST_ROUTE_FIRST (pRcvdPAEntry) == NULL)
        {
            /* This route is the first route added to this list. */
            BGP4_PA_IDENT_NON_BEST_ROUTE_FIRST (pRcvdPAEntry) = pRtProfile;
        }
        /* Increment the route count. */
        BGP4_PA_IDENT_NON_BEST_ROUTE_COUNT (pRcvdPAEntry)++;
    }

    if (u1IsRtAlreadyPresent == BGP4_FALSE)
    {
        /* Route is not present in the list previously.
         * Increment the route profile referece count */
        BGP4_RT_REF_COUNT (pRtProfile)++;
    }

    return (BGP4_SUCCESS);
}

/****************************************************************************/
/* Function    :  Bgp4RcvdPADBDeleteRoute                                   */
/* Description :  This function deletes the existint Route from the         */
/*             :  Received Path Attribute Database.                         */
/* Input       :  pRtProfile - Pointer to the route that needs to be        */
/*             :               removed from RCVD_PA Database.               */
/*             :  u1IsRouteBest - Status explaining whether the route is    */
/*             :                  best or not.                              */
/* Output      :  None                                                      */
/* Returns     :  BGP4_SUCCESS -  if entry removed. Else returns.           */
/*             :  BGP4_FAILURE -  other wise.                               */
/****************************************************************************/
INT4
Bgp4RcvdPADBDeleteRoute (tRouteProfile * pRtProfile, UINT1 u1IsRouteBest)
{
    tBgp4RcvdPathAttr  *pDelRcvdPA = NULL;
    tRouteProfile      *pPrevRoute = NULL;
    tRouteProfile      *pNextRoute = NULL;
    tBgp4Info          *pRcvdBgp4Info = NULL;
    tBGP4_DLL          *pBgp4Dll = NULL;
    UINT2               u2Afi = 0;
    UINT2               u2Safi = 0;
    UINT4               u4ContextId = BGP4_RT_CXT_ID (pRtProfile);

    pRcvdBgp4Info = BGP4_RT_BGP_INFO (pRtProfile);
    if (pRcvdBgp4Info == NULL)
    {
        return BGP4_FAILURE;
    }
    pDelRcvdPA = BGP4_INFO_RCVD_PA_ENTRY (pRcvdBgp4Info);
    if (pDelRcvdPA == NULL)
    {
        return BGP4_FAILURE;
    }

    /* Extract the <AFI,SAFI> information from the route. */
    u2Afi = BGP4_RT_AFI_INFO (pRtProfile);
    u2Safi = BGP4_RT_SAFI_INFO (pRtProfile);

    if (u1IsRouteBest == BGP4_TRUE)
    {
        /* Route is the best route. Need to remove the route from the
         * best route list. */
        pBgp4Dll = BGP4_PA_IDENT_BEST_ROUTE_LIST (pDelRcvdPA);
    }
    else
    {
        /* Route is the not best route. Need to remove the route from the
         * non-best route list. */
        pBgp4Dll = BGP4_PA_IDENT_NON_BEST_ROUTE_LIST (pDelRcvdPA);
    }

    /* Remove the route from this RCVD_PA Entry. */
    pPrevRoute = BGP4_RT_IDENT_ATTR_LINK_PREV (pRtProfile);
    pNextRoute = BGP4_RT_IDENT_ATTR_LINK_NEXT (pRtProfile);

    if (Bgp4IsRouteInList (pBgp4Dll, pRtProfile) == BGP4_FAILURE)
    {
        if (u1IsRouteBest == BGP4_TRUE)
        {
            pBgp4Dll = BGP4_PA_IDENT_NON_BEST_ROUTE_LIST (pDelRcvdPA);
        }
        else
        {
            pBgp4Dll = BGP4_PA_IDENT_BEST_ROUTE_LIST (pDelRcvdPA);
        }
        if (Bgp4IsRouteInList (pBgp4Dll, pRtProfile) == BGP4_FAILURE)
        {
            return BGP4_FAILURE;
        }
    }

    if ((BGP4_DLL_FIRST_ROUTE (pBgp4Dll) != pRtProfile) &&
        (BGP4_DLL_LAST_ROUTE (pBgp4Dll) != pRtProfile) &&
        (pPrevRoute == NULL) && (pNextRoute == NULL))
    {
        /* Route is not present in this RCVD_PA Entry */
        return BGP4_FAILURE;
    }

    if (pPrevRoute != NULL)
    {
        /* Update the previous routes next ident PA route pointer */
        BGP4_RT_IDENT_ATTR_LINK_NEXT (pPrevRoute) = pNextRoute;
    }
    else
    {
        /* This route is the first route in the list. Update the
         * list first route value. */
        BGP4_DLL_FIRST_ROUTE (pBgp4Dll) = pNextRoute;
    }

    if (pNextRoute != NULL)
    {
        /* Update the next routes prev ident PA route pointer */
        BGP4_RT_IDENT_ATTR_LINK_PREV (pNextRoute) = pPrevRoute;
    }
    else
    {
        /* This route is the last route in the list. Update the list
         * last route value. */
        BGP4_DLL_LAST_ROUTE (pBgp4Dll) = pPrevRoute;
    }

    /* Decrement the RCVD_PA entry route count. */
    BGP4_DLL_COUNT (pBgp4Dll)--;

    /* Update the Route profile RCVD_PA entry value. */
    BGP4_RT_IDENT_ATTR_LINK_PREV (pRtProfile) = NULL;
    BGP4_RT_IDENT_ATTR_LINK_NEXT (pRtProfile) = NULL;

    /* Release the route profile pointer. */
    Bgp4DshReleaseRtInfo (pRtProfile);

    if (BGP4_PA_IDENT_ROUTE_COUNT (pDelRcvdPA) == 0)
    {
        /* There is no more route entry present with this RCVD_PA. So free
         * the RCVD_PA entry and the associated BGP4_INFO. */
        Bgp4RcvdPAReleasePAInfo (u4ContextId, pDelRcvdPA, u2Afi, u2Safi);
    }

    return (BGP4_SUCCESS);
}

/****************************************************************************/
/* Function    :  Bgp4RcvdPADBGetNextEntry                                  */
/* Description :  This function searches for an entry in the Received PA    */
/*                database correspond to the given input and return the     */
/*                next entry if present.                                    */
/* Input       :  pRcvdPAEntry - Pointer to the Received PA Entry           */
/*             :  u4HashIndex  - Hash Bucket Index where the PA Entry exist */
/*             :  u2Afi - Address Family to which this entry belongs.       */
/*             :  u2Safi - Address Family to which this entry belongs.      */
/* Output      :  ppNextRcvdPAEntry - Pointer to the next Received PA entry.*/
/*             :  pu4NextHashIndex  - Hash Bucket Index where the next PA   */
/*             :                      entry is present.                     */
/* Returns     :  BGP4_SUCCESS - if the entry is present.                   */
/*                BGP4_FAILURE - if the entry is not present.               */
/****************************************************************************/
INT4
Bgp4RcvdPADBGetNextEntry (UINT4 u4ContextId, tBgp4RcvdPathAttr * pRcvdPAEntry,
                          UINT4 u4HashIndex, UINT2 u2Afi, UINT2 u2Safi,
                          tBgp4RcvdPathAttr ** ppNextRcvdPAEntry,
                          UINT4 *pu4NextHashIndex)
{
    tBgp4RcvdPathAttr  *pBgp4RcvdPA = NULL;
    UINT4               u4HashKey = u4HashIndex;
    UINT4               u4AsafiIndex = 0;

    *ppNextRcvdPAEntry = NULL;
    *pu4NextHashIndex = 0;

    /* Fix for klocwork warning NULL pointer dereference */
    if (pRcvdPAEntry == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_MGMT_TRC | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "[ERROR]: pRcvdBgpInfo is NULL in "
                  "Bgp4RcvdPADBGetNextEntry Entry\r\n");
        return BGP4_FAILURE;
    }

    pBgp4RcvdPA = NULL;
    Bgp4GetAfiSafiIndex (u2Afi, u2Safi, &u4AsafiIndex);
#ifdef L3VPN
    /* Carrying Label Information - RFC 3107 */
    /* Since we are maining same RIB for both IPv4 unlabelled and labelled
     * routes, the received PA database should consider the both as same
     */
    if (u4AsafiIndex == BGP4_IPV4_LBLD_INDEX)
    {
        u4AsafiIndex = BGP4_IPV4_UNI_INDEX;
    }
#endif

    /* Try and get the next entry to the pRcvdPAEntry in the same bucket. */
    *ppNextRcvdPAEntry = (tBgp4RcvdPathAttr *)
        TMO_HASH_Get_Next_Bucket_Node (BGP4_RCVD_PA_DB
                                       (u4ContextId, u4AsafiIndex), u4HashKey,
                                       &pRcvdPAEntry->NextNonIdentPA);
    if (*ppNextRcvdPAEntry != NULL)
    {
        /* Found next entry. */
        *pu4NextHashIndex = u4HashKey;
        return (BGP4_SUCCESS);
    }

    u4HashKey++;

    /* No more entry is present in the current bucket. Try and
     * get the first entry from the next bucket. */
    if (u4HashKey >= BGP4_MAX_RCVD_PA_HASH_INDEX)
    {
        /* Already we are at the end of the Hash Table. */
        return (BGP4_FAILURE);
    }

    BGP4_HASH_Scan_Table (BGP4_RCVD_PA_DB (u4ContextId, u4AsafiIndex),
                          u4HashKey, u4HashKey)
    {
        pBgp4RcvdPA = (tBgp4RcvdPathAttr *)
            TMO_HASH_Get_First_Bucket_Node (BGP4_RCVD_PA_DB
                                            (u4ContextId, u4AsafiIndex),
                                            u4HashKey);
        if (pBgp4RcvdPA != NULL)
        {
            *ppNextRcvdPAEntry = pBgp4RcvdPA;
            *pu4NextHashIndex = u4HashKey;
            return BGP4_SUCCESS;
        }
    }

    return (BGP4_FAILURE);
}

/****************************************************************************/
/* Function    :  Bgp4RcvdPADBGetFirstBestRoute                             */
/* Description :  This function searches for the first best route in the    */
/*             :  Received PA Database and returns that route.              */
/* Input       :  u2Afi - Address Family to which this entry belongs.       */
/*             :  u2Safi - Address Family to which this entry belongs.      */
/* Output      :  ppBestRtProfile - Pointer to the first best route.        */
/* Returns     :  BGP4_SUCCESS - if the route is present.                   */
/*                BGP4_FAILURE - if the route is not present.               */
/****************************************************************************/
INT4
Bgp4RcvdPADBGetFirstBestRoute (UINT4 u4CxtId, tRouteProfile ** ppBestRtProfile,
                               UINT2 u2Afi, UINT2 u2Safi)
{
    tBgp4RcvdPathAttr  *pBgp4RcvdPA = NULL;
    UINT4               u4HashKey = 0;
    UINT4               u4AsafiIndex = 0;

    *ppBestRtProfile = NULL;
    Bgp4GetAfiSafiIndex (u2Afi, u2Safi, &u4AsafiIndex);
#ifdef L3VPN
    /* Carrying Label Information - RFC 3107 */
    /* Since we are maining same RIB for both IPv4 unlabelled and labelled
     * routes, the received PA database should consider the both as same
     */
    if (u4AsafiIndex == BGP4_IPV4_LBLD_INDEX)
    {
        u4AsafiIndex = BGP4_IPV4_UNI_INDEX;
    }
#endif

    /* Try and get the first best route present in the RCVD_PA_DB. */
    TMO_HASH_Scan_Table (BGP4_RCVD_PA_DB (u4CxtId, u4AsafiIndex), u4HashKey)
    {
        TMO_HASH_Scan_Bucket (BGP4_RCVD_PA_DB (u4CxtId, u4AsafiIndex),
                              u4HashKey, pBgp4RcvdPA, tBgp4RcvdPathAttr *)
        {
            if (BGP4_PA_IDENT_BEST_ROUTE_FIRST (pBgp4RcvdPA) != NULL)
            {
                *ppBestRtProfile = BGP4_PA_IDENT_BEST_ROUTE_FIRST (pBgp4RcvdPA);
                return (BGP4_SUCCESS);
            }
        }
    }

    return (BGP4_FAILURE);
}

/****************************************************************************/
/* Function    :  Bgp4RcvdPADBGetNextBestRoute                              */
/* Description :  This function searches for the route in the Received PA   */
/*             :  database Best route list correspond to the given input and*/
/*             :  return the next route if present in best route list.      */
/* Input       :  pRtProfile - Pointer to the route profile                 */
/* Output      :  ppNextRtProfile - Pointer to the next Route profile       */
/* Returns     :  BGP4_SUCCESS - if the route is present.                   */
/*                BGP4_FAILURE - if the route is not present.               */
/****************************************************************************/
INT4
Bgp4RcvdPADBGetNextBestRoute (tRouteProfile * pRtProfile,
                              tRouteProfile ** ppNextRtProfile)
{
    tBgp4RcvdPathAttr  *pBgp4RcvdPA = NULL;
    tBgp4RcvdPathAttr  *pNextBgp4RcvdPA = NULL;
    UINT4               u4HashKey = 0;
    UINT4               u4NextHashKey = 0;

    *ppNextRtProfile = NULL;

    /* Try and get the next route to the pRtProfile present in the same
     * RCVD_PA_ENTRY. */
    *ppNextRtProfile = BGP4_RT_IDENT_ATTR_LINK_NEXT (pRtProfile);
    if (*ppNextRtProfile != NULL)
    {
        /* Found next entry. */
        return (BGP4_SUCCESS);
    }

    /* No more route is present in this RCVD_PA_ENTRY. Try and
     * get the first route from the next entry. */
    pBgp4RcvdPA = BGP4_INFO_RCVD_PA_ENTRY (BGP4_RT_BGP_INFO (pRtProfile));
    Bgp4PAFormHashKey (BGP4_RT_BGP_INFO (pRtProfile), &u4HashKey,
                       BGP4_MAX_RCVD_PA_HASH_INDEX);

    for (;;)
    {
        if (Bgp4RcvdPADBGetNextEntry
            (BGP4_RT_CXT_ID (pRtProfile), pBgp4RcvdPA, u4HashKey,
             BGP4_RT_AFI_INFO (pRtProfile), BGP4_RT_SAFI_INFO (pRtProfile),
             &pNextBgp4RcvdPA, &u4NextHashKey) == BGP4_FAILURE)
        {
            /* No more entry is present. */
            break;
        }

        if (BGP4_PA_IDENT_BEST_ROUTE_FIRST (pNextBgp4RcvdPA) != NULL)
        {
            *ppNextRtProfile = BGP4_PA_IDENT_BEST_ROUTE_FIRST (pNextBgp4RcvdPA);
            return (BGP4_SUCCESS);
        }

        /* No best route is present in this entry. Try and get from
         * next entry. */
        pBgp4RcvdPA = pNextBgp4RcvdPA;
        pNextBgp4RcvdPA = NULL;
        u4HashKey = u4NextHashKey;
        u4NextHashKey = 0;
    }

    return (BGP4_FAILURE);
}

/******************************************************************************/
/* Function Name : Bgp4CreateAdvtPADB                                         */
/* Description   : This function create the Advertise Database that store the */
/*               : the route with identical outbount Path Attributes.         */
/* Input(s)      : None                                                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4CreateAdvtPADB (UINT4 u4CxtId)
{
    /* HASH TBL creation for Advertise Database */
    BGP4_ADVT_PA_DB (u4CxtId) =
        TMO_HASH_Create_Table (BGP4_MAX_ADVT_PA_HASH_INDEX, NULL, FALSE);

    if (BGP4_ADVT_PA_DB (u4CxtId) == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tBgp4CreateAdvtDB() : CREATING HASH TABLE For"
                  "ADVERTISE DATABASE FAILED !!!\n");
        return (BGP4_FAILURE);
    }
    return (BGP4_SUCCESS);
}

/******************************************************************************/
/* Function Name : Bgp4DeleteAdvtPADB                                         */
/* Description   : This function delete the Advertise Database.               */
/* Input(s)      : None                                                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
INT4
Bgp4DeleteAdvtPADB (UINT4 u4Context)
{
    /* Delete the hash table BGP4_ADVT_PA_DB() */
    if (BGP4_ADVT_PA_DB (u4Context) != NULL)
    {
        TMO_HASH_Delete_Table (BGP4_ADVT_PA_DB (u4Context),
                               Bgp4AdvtPADBClearAdvtEntry);
        BGP4_ADVT_PA_DB (u4Context) = NULL;
    }

    return (BGP4_SUCCESS);
}

/****************************************************************************/
/* Function    :  Bgp4AdvtPADBClearAdvtDB                                   */
/* Description :  This function will free all the route associated with the */
/*                ADVT_PA_DB and free all the entry.                        */
/* Input       :  None.                                                     */
/* Output      :  None.                                                     */
/* Returns     :  None.                                                     */
/****************************************************************************/
VOID
Bgp4AdvtPADBClearAdvtDB (UINT4 u4Context)
{
    tBgp4AdvtPathAttr  *pAdvtPANode = NULL;
    tBgp4AdvtPathAttr  *pTempAdvtPANode = NULL;
    UINT4               u4HashKey = 0;

    TMO_HASH_Scan_Table (BGP4_ADVT_PA_DB (u4Context), u4HashKey)
    {
        BGP4_HASH_DYN_Scan_Bucket (BGP4_ADVT_PA_DB (u4Context), u4HashKey,
                                   pAdvtPANode, pTempAdvtPANode,
                                   tBgp4AdvtPathAttr *)
        {
            Bgp4AdvtPADBClearAdvtEntry (&pAdvtPANode->NextNonIdentPA);
        }
    }

}

/****************************************************************************/
/* Function    :  Bgp4AdvtPADBClearAdvtEntry                                */
/* Description :  This function will free all the route associated with this*/
/*                entry and free the Outbound BGP4_INFO associated with this*/
/*                entry.                                                    */
/* Input       :  pDelHashNode  - Advertise DB entry to be deleted.         */
/* Output      :  none.                                                     */
/* Returns     :  None.                                                     */
/****************************************************************************/
VOID
Bgp4AdvtPADBClearAdvtEntry (tTMO_HASH_NODE * pDelHashNode)
{
    tBgp4AdvtPathAttr  *pDelAdvtPA = NULL;
    tBufNode           *pBufNode = NULL;
    tBufNode           *pTempBufNode = NULL;
    UINT4               u4HashKey = 0;
    UINT4               u4Context = 0;

    pDelAdvtPA = (tBgp4AdvtPathAttr *) pDelHashNode;

    if (BGP4_ADVT_PA_BGP4INFO (pDelAdvtPA) == NULL)
    {
        /* release the main entry and stop */
        Bgp4MemReleaseAdvtPAInfo (pDelAdvtPA);
        return;
    }
    Bgp4PAFormHashKey (BGP4_ADVT_PA_BGP4INFO (pDelAdvtPA), &u4HashKey,
                       BGP4_MAX_ADVT_PA_HASH_INDEX);

    u4Context = BGP4_PEER_CXT_ID (pDelAdvtPA->pAdvtPeer);
    /* Clear Buffer list. */
    BGP_SLL_DYN_Scan (BGP4_ADVT_PA_ROUTE_LIST (pDelAdvtPA), pBufNode,
                      pTempBufNode, tBufNode *)
    {
        /* Delete the node from list and free the list. */
        TMO_SLL_Delete (BGP4_ADVT_PA_ROUTE_LIST (pDelAdvtPA),
                        &pBufNode->TSNext);
        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pBufNode->pu1Msg);
        MemReleaseMemBlock (gBgpNode.Bgp4BufNodesPoolId, (UINT1 *) pBufNode);
    }

    Bgp4DshReleaseBgpInfo (BGP4_ADVT_PA_BGP4INFO (pDelAdvtPA));
    BGP4_ADVT_PA_BGP4INFO (pDelAdvtPA) = NULL;
    BGP4_ADVT_PA_AFI (pDelAdvtPA) = 0;
    BGP4_ADVT_PA_SAFI (pDelAdvtPA) = 0;
    BGP4_ADVT_PA_SIZE (pDelAdvtPA) = 0;
    BGP4_ADVT_PA_PEER (pDelAdvtPA) = NULL;

    TMO_HASH_Delete_Node (BGP4_ADVT_PA_DB (u4Context),
                          &pDelAdvtPA->NextNonIdentPA, u4HashKey);
    Bgp4MemReleaseAdvtPAInfo (pDelAdvtPA);
    return;
}

/****************************************************************************/
/* Function    :  Bgp4AdvtPADBAddEntry                                      */
/* Description :  This function adds a new entry to the given Path Attrib   */
/*             :  in the Advertise Path Attribute Database.                 */
/* Input       :  pPeerInfo - Peer to which this info needs to be advt.     */
/*             :  pAdvtBgpInfo - Pointer to the Advertised BGP Info.        */
/*             :  u2Afi - Address Family corresponding to this BGP Info     */
/*             :  u2Safi - Sub-Address Family corresponding to this BGP Info*/
/* Output      :  None                                                      */
/* Returns     :  Pointer to the ADVT_PA entry to which this bgp_info is    */
/*             :  added or                                                  */
/*             :  NULL - if any failure.                                    */
/****************************************************************************/
tBgp4AdvtPathAttr  *
Bgp4AdvtPADBAddEntry (tBgp4PeerEntry * pPeerInfo, tBgp4Info * pAdvtBgpInfo,
                      UINT2 u2Afi, UINT2 u2Safi)
{
    tBgp4AdvtPathAttr  *pBgp4AdvtPA = NULL;
    tBgp4Info          *pCurBgpInfo = NULL;
    UINT4               u4HashKey = 0;
    UINT1               u1IsMatchEntryExist = BGP4_FALSE;

    Bgp4PAFormHashKey (pAdvtBgpInfo, &u4HashKey, BGP4_MAX_ADVT_PA_HASH_INDEX);

    /* Check whether a matching entry exists for this BgpInfo or not. */
    TMO_HASH_Scan_Bucket (BGP4_ADVT_PA_DB (BGP4_PEER_CXT_ID (pPeerInfo)),
                          u4HashKey, pBgp4AdvtPA, tBgp4AdvtPathAttr *)
    {
        pCurBgpInfo = BGP4_ADVT_PA_BGP4INFO (pBgp4AdvtPA);
        if ((Bgp4AttrIsBgpinfosIdentical (pAdvtBgpInfo, pCurBgpInfo) == TRUE) &&
            (u2Afi == BGP4_ADVT_PA_AFI (pBgp4AdvtPA)) &&
            (u2Safi == BGP4_ADVT_PA_SAFI (pBgp4AdvtPA)))
        {
            u1IsMatchEntryExist = BGP4_TRUE;
            break;
        }
    }

    if (u1IsMatchEntryExist == BGP4_FALSE)
    {
        /* No matching entry exist for the given BGP4INFO. Allocate a new
         * PA entry and link the received BGP4INFO with that entry. */
        pBgp4AdvtPA = Bgp4MemAllocateAdvtPAInfo (sizeof (tBgp4AdvtPathAttr));
        if (pBgp4AdvtPA == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for Advertise pBgp4AdvtPA FAILED!!!\n");
            gu4BgpDebugCnt[MAX_BGP_ADVT_PATH_ATTRIBS_SIZING_ID]++;
            return ((tBgp4AdvtPathAttr *) NULL);
        }
        TMO_HASH_Add_Node (BGP4_ADVT_PA_DB (BGP4_PEER_CXT_ID (pPeerInfo)),
                           &pBgp4AdvtPA->NextNonIdentPA, u4HashKey, NULL);

        /* Attach the received BGP4_INFO with this ADVT_PA Entry. */
        BGP4_ADVT_PA_BGP4INFO (pBgp4AdvtPA) = pAdvtBgpInfo;
        BGP4_ADVT_PA_AFI (pBgp4AdvtPA) = u2Afi;
        BGP4_ADVT_PA_SAFI (pBgp4AdvtPA) = u2Safi;
        BGP4_ADVT_PA_SIZE (pBgp4AdvtPA) =
            Bgp4AttrAttributeSize (pPeerInfo, pAdvtBgpInfo, u2Afi, u2Safi);
        /* peer informatin. This will be used when filling NLRI information
         * in update message. 
         */
        BGP4_ADVT_PA_PEER (pBgp4AdvtPA) = pPeerInfo;
        BGP4_INFO_REF_COUNT (pAdvtBgpInfo)++;
    }

    return (pBgp4AdvtPA);
}

/*****************************************************************************/
/* Function Name : Bgp4FormBgp4Hdr                                           */
/* Description   : This routine forms the BGP Common header.                 */
/* Input(s)      : BGP Message Type which needs to be filled in the header   */
/*                 (u1MsgType)                                               */
/* Output(s)     : None.                                                     */
/* Return(s)     : Newly formed buffer containing the BGP update.            */
/*****************************************************************************/
UINT1              *
Bgp4FormBgp4Hdr (UINT1 u1MsgType)
{
    UINT1              *pu1MsgHdr = NULL;

    pu1MsgHdr = MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);
    if (pu1MsgHdr == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                  BGP4_MOD_NAME,
                  "\tMemory Allocation to Form BGP Message FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_BUFNODE_MSGS_SIZING_ID]++;
        return (NULL);

    }

    MEMSET (pu1MsgHdr, BGP4_MARKER_BYTE_VAL, BGP4_MARKER_LEN);
    *(UINT1 *) (pu1MsgHdr + BGP4_MARKER_LEN + sizeof (UINT2)) = u1MsgType;
    return ((UINT1 *) pu1MsgHdr);
}

/*****************************************************************************/
/* Function Name : Bgp4RoundToBytes                                          */
/* Description   : This routine returns the no. of bytes present in the      */
/*                 passed value.                                             */
/* Input(s)      : value in bits (u2PrefixLenInBits)                         */
/* Output(s)     : None.                                                     */
/* Return(s)     : The no. of bytes present in the passed input value        */
/*****************************************************************************/
UINT1
Bgp4RoundToBytes (UINT2 u2PrefixLenInBits)
{
    if ((u2PrefixLenInBits % BGP4_ONE_BYTE_BITS) == 0)
    {
        return (UINT1) (u2PrefixLenInBits / BGP4_ONE_BYTE_BITS);
    }
    else
    {
        return (UINT1) ((u2PrefixLenInBits / BGP4_ONE_BYTE_BITS) +
                        BGP4_ONE_BYTE);
    }
}

/********************************************************************/
/* Function Name   : Bgp4AttrFillPathAttributes                     */
/* Description     : Fills the path attributes to be advertised for */
/*                   this route.                                    */
/* Input(s)        : pPeerInfo - pointer to the peer information    */
/*                               to which the UPDATE is to be sent  */
/*                   pRtProfile - pointer to the route profile      */
/* Output(s)       : pAdvtBgpInfo - Updated BGP4-INFO to be         */
/*                                  advertised.                     */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4AttrFillPathAttributes (tBgp4PeerEntry * pPeerEntry,
                            tRouteProfile * pRtProfile,
                            tBgp4Info * pAdvtBgpInfo, tBgp4Info * pRtInfo)
{
    tBgp4PeerEntry     *pSrcPeer = NULL;
    tBgp4Info          *pRcvdBgp4Info = NULL;
    UINT4               u4AsafiMask = 0;
    INT4                i4Status = BGP4_FAILURE;
    UINT4               u4RecvMEDSend = 0;

    pRcvdBgp4Info = BGP4_RT_BGP_INFO (pRtProfile);

    BGP4_DBG (BGP4_DBG_ENTRY, "\tBgp4AttrFillPathAttributes() ...\n");

    /* Filling the ORIGIN type */
    if (pRtInfo != NULL && BGP4_INFO_ORIGIN (pRtInfo) != 0)
    {
        BGP4_INFO_ORIGIN (pRtInfo) = BGP4_INFO_ORIGIN (pRtInfo) - 1;
        i4Status = Bgp4AttrFillOrigin (pRtInfo, pAdvtBgpInfo);
    }
    else
    {
        i4Status = Bgp4AttrFillOrigin (pRcvdBgp4Info, pAdvtBgpInfo);
    }
    BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                   BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                   "\tPEER %s : Origin attribute is filled successfully with value %s\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry))),
                   Bgp4PrintCodeName (BGP4_INFO_ORIGIN (pAdvtBgpInfo),
                                      BGP4_ORIGIN_CODE_NAME));

    /* Filling the AS Path */
    i4Status = Bgp4AttrFillAspath (pRcvdBgp4Info, pAdvtBgpInfo, pPeerEntry);
    if (i4Status == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }

    Bgp4GetPeerAdvtAfiSafi (pRtProfile, pPeerEntry, &u4AsafiMask);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
            /*  Filling the Next Hop */
            if (BGP4_AFI_IN_ADDR_PREFIX_INFO
                (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)) == BGP4_INET_AFI_IPV4)
            {
                i4Status = Bgp4AttrFillNexthop (pRtProfile, pAdvtBgpInfo,
                                                pPeerEntry);
                if (i4Status == BGP4_FAILURE)
                {
                    return BGP4_FAILURE;
                }
            }
            break;
        default:
            break;
    }

    /*  Filling the MED */
#ifdef VPLSADS_WANTED
    if (u4AsafiMask == CAP_MP_L2VPN_VPLS)
    {
        /* No MED value for VPLS */
    }
    else
    {
#endif
        if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
        {
            /* If MED received from an external peer, it MUST NOT be 
             * propagated to external peers. But still rcvd MED can send
             * to the internal peers. MED should be unchanged when
             * advertizing the route to confed peers or reflecting the route.
             */
            pSrcPeer = BGP4_RT_PEER_ENTRY (pRtProfile);

            /* unchanged MED shud be advertized to confed peers */
            if (((BGP4_CONFED_ID (BGP4_PEER_CXT_ID (pPeerEntry)) != BGP4_INV_AS)
                 && (BGP4_CONFED_PEER_STATUS (pPeerEntry) == BGP4_TRUE))
                ||
                ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerEntry), pPeerEntry)
                  == BGP4_INTERNAL_PEER) && (pSrcPeer != NULL)
                 &&
                 (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerEntry), pSrcPeer)
                  == BGP4_INTERNAL_PEER)))
            {
                Bgp4AttrFillMed (pRtProfile, pAdvtBgpInfo, pPeerEntry, pRtInfo,
                                 BGP4_MED_RECV_SEND);
            }
            else if ((pSrcPeer != NULL) &&
                     (BGP4_GET_PEER_TYPE
                      (BGP4_PEER_CXT_ID (pPeerEntry),
                       pSrcPeer) == BGP4_EXTERNAL_PEER))
            {
                if (BGP4_GET_PEER_TYPE
                    (BGP4_PEER_CXT_ID (pPeerEntry),
                     pPeerEntry) == BGP4_INTERNAL_PEER)
                {
                    /* Sending to Internal Peer. The received MED can be 
                     * sent or if any configuration exist, configured MED 
                     * can be sent. */
                    if ((pRtInfo != NULL) && (pRtInfo->u4SendMED != 0))
                    {
                        u4RecvMEDSend = pRtInfo->u4SendMED;
                    }
                    else
                    {
                        u4RecvMEDSend = BGP4_MED_RECV_SEND;
                    }
                }
                else
                {
                    /* Sending to External Peer. The received MED can't be 
                     * sent but if any configuration exists, then the configured
                     * MED can be sent. */
                    if ((pRtInfo != NULL) && (pRtInfo->u4SendMED != 0))
                    {
                        u4RecvMEDSend = pRtInfo->u4SendMED;
                    }
                    else
                    {
                        u4RecvMEDSend = BGP4_MED_RECV_BLOCK;
                    }
                }
                if (pRtInfo == NULL)
                {
                    Bgp4AttrFillMed (pRtProfile, pAdvtBgpInfo, pPeerEntry,
                                     pRtInfo, u4RecvMEDSend);
                }
                else if (((pRtInfo->u4AttrFlag) & BGP4_ATTR_SEND_MED_MASK) ==
                         BGP4_ATTR_SEND_MED_MASK)
                {
                    BGP4_INFO_RCVD_MED (pAdvtBgpInfo) = u4RecvMEDSend;
                    BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_MED_MASK;
                    BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                                   BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : MED %d attribute is successfully filled.\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerEntry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerEntry))),
                                   BGP4_INFO_RCVD_MED (pAdvtBgpInfo));
                }
            }
            else                /* Received from internal peer */
            {
                /* Sending to Internal Peer. Routes learnt from
                 * internal peers are not advertised to other internal peers.
                 * Sending to External Peer. The received MED can't be 
                 * sent but if any configuration exists, then the configured
                 * MED can be sent. */
                if (BGP4_GET_PEER_TYPE
                    (BGP4_PEER_CXT_ID (pPeerEntry),
                     pPeerEntry) == BGP4_INTERNAL_PEER)
                {
                    /* Sending to Internal Peer. Routes learnt from
                     * internal peers are not advertised to other 
                     * internal peers. */
                    u4RecvMEDSend = BGP4_MED_RECV_BLOCK;
                }
                else
                {
                    /* Sending to External Peer. The received MED can't be 
                     * sent but if any configuration exists, then the configured
                     * MED can be sent. */
                    if ((pRtInfo != NULL) && (pRtInfo->u4SendMED != 0))
                    {
                        u4RecvMEDSend = pRtInfo->u4SendMED;
                    }
                    else
                    {
                        u4RecvMEDSend = BGP4_MED_RECV_BLOCK;
                    }
                }
                /*u4RecvMEDSend = BGP4_MED_RECV_BLOCK; */
                if (pRtInfo == NULL)
                {
                    Bgp4AttrFillMed (pRtProfile, pAdvtBgpInfo, pPeerEntry,
                                     pRtInfo, u4RecvMEDSend);
                }
                else if (((pRtInfo->u4AttrFlag) & BGP4_ATTR_SEND_MED_MASK) ==
                         BGP4_ATTR_SEND_MED_MASK)
                {
                    BGP4_INFO_RCVD_MED (pAdvtBgpInfo) = u4RecvMEDSend;
                    BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_MED_MASK;
                    BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                                   BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : MED %d attribute is successfully filled.\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerEntry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerEntry))),
                                   BGP4_INFO_RCVD_MED (pAdvtBgpInfo));
                }
            }
        }
        else                    /* Protocol is other than BGP */
        {
            /* For non-BGP routes received from RTM other than DIRECT routes,
             * the received MED needs to be sent to both internal and external
             * peers. Direct routes needs to be advertised with received MED value.
             * But if any configuration exists, then the configured MED will
             * be sent. */
            u4RecvMEDSend = BGP4_MED_RECV_SEND;
            Bgp4AttrFillMed (pRtProfile, pAdvtBgpInfo, pPeerEntry, pRtInfo,
                             u4RecvMEDSend);
        }
#ifdef VPLSADS_WANTED
    }
#endif
    /*  Filling the Local Preference */
    /*  Local Preference is only propagated to internal peers. 
     *  Local Preference should be unchanged when advertizing the route
     *  to confed peers or reflecting the route. */
    if ((pRtInfo != NULL && BGP4_INFO_RCVD_LOCAL_PREF (pRtInfo) != 0) &&
        (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerEntry), pPeerEntry) !=
         BGP4_EXTERNAL_PEER))
    {                            /* LP has set through Route map configured */
        BGP4_INFO_RCVD_LOCAL_PREF (pAdvtBgpInfo) =
            BGP4_INFO_RCVD_LOCAL_PREF (pRtInfo);
        BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_LOCAL_PREF_MASK;
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                       BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Local Preference %d attribute is successfully filled.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerEntry))),
                       BGP4_INFO_RCVD_LOCAL_PREF (pAdvtBgpInfo));
    }
    else
    {
#ifdef VPLSADS_WANTED
        if (u4AsafiMask == CAP_MP_L2VPN_VPLS)
        {
            BGP4_INFO_RCVD_LOCAL_PREF (pAdvtBgpInfo) =
                BGP4_INFO_RCVD_LOCAL_PREF (pRcvdBgp4Info);
            BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_LOCAL_PREF_MASK;
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                           BGP4_TRC_FLAG, BGP4_VPLS_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Local Preference %d attribute is successfully filled.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerEntry))),
                           BGP4_INFO_RCVD_LOCAL_PREF (pAdvtBgpInfo));

        }
        else
        {
#endif
            pSrcPeer = ((BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID) ?
                        BGP4_RT_PEER_ENTRY (pRtProfile) : NULL);

            if ((BGP4_CONFED_PEER_STATUS (pPeerEntry) == BGP4_TRUE) ||
                ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerEntry), pPeerEntry)
                  == BGP4_INTERNAL_PEER) && (pSrcPeer != NULL)
                 &&
                 (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerEntry), pSrcPeer)
                  == BGP4_INTERNAL_PEER)))
            {
                Bgp4ConfedPreserveLP (pRtProfile, pAdvtBgpInfo);
            }
            else if (BGP4_GET_PEER_TYPE
                     (BGP4_PEER_CXT_ID (pPeerEntry),
                      pPeerEntry) == BGP4_INTERNAL_PEER)
            {
                Bgp4AttrFillLp (pRtProfile, pAdvtBgpInfo, pPeerEntry);
            }
        }
#ifdef VPLSADS_WANTED
    }
#endif

    /*  Filling the Aggregator */
    if (Bgp4AttrFillAggr (pRtProfile, pAdvtBgpInfo) == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }

    /* ATTR : 8 */
    /* When peer community send-status is true, then only
     * fill the community attribute information
     */
    if (BGP4_PEER_COMM_SEND_STATUS (pPeerEntry) != COMM_FALSE)
    {
        i4Status = CommGetUpdCommAttrib (pRtProfile, pAdvtBgpInfo, pRtInfo);
        if (i4Status == COMM_FAILURE)
        {
            return BGP4_FAILURE;
        }
    }

    /* ATTR : 9-10 */
    pSrcPeer = ((BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID) ?
                BGP4_RT_PEER_ENTRY (pRtProfile) : NULL);

    /* When the route reflector reflect a route, the Originator Id
     * attribute and Cluster List attributes need to be filled. */
    if ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerEntry), pPeerEntry) ==
         BGP4_INTERNAL_PEER) && ((pSrcPeer != NULL)
                                 &&
                                 (BGP4_GET_PEER_TYPE
                                  (BGP4_PEER_CXT_ID (pPeerEntry),
                                   pSrcPeer) == BGP4_INTERNAL_PEER)))
    {
        i4Status = RflFillRflPathAttrInfo (pRtProfile, pAdvtBgpInfo);
        if (i4Status == RFL_FAILURE)
        {
            return BGP4_FAILURE;
        }
    }

    /* ATTR : 14 */
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef BGP4_IPV6_WANTED
            /* If the Peer is V6, then Update advertised BGP4-INFO */
            if (BGP4_AFI_IN_ADDR_PREFIX_INFO
                (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)) == BGP4_INET_AFI_IPV6)
            {
                i4Status = Bgp4MpeFillPathAttribute (pRtProfile, pAdvtBgpInfo,
                                                     pPeerEntry);
                if (i4Status == BGP4_FAILURE)
                {
                    return BGP4_FAILURE;
                }
            }
            break;
        case CAP_MP_IPV6_UNICAST:
            i4Status = Bgp4MpeFillPathAttribute (pRtProfile, pAdvtBgpInfo,
                                                 pPeerEntry);
            if (i4Status == BGP4_FAILURE)
            {
                return BGP4_FAILURE;
            }
#endif
            break;

#ifdef VPLSADS_WANTED
            /*ADS-VPLS Related Processing */
        case CAP_MP_L2VPN_VPLS:
            i4Status = Bgp4MpeFillPathAttribute (pRtProfile, pAdvtBgpInfo,
                                                 pPeerEntry);
            if (i4Status == BGP4_FAILURE)
            {
                return BGP4_FAILURE;
            }

            break;

#endif

#ifdef L3VPN
            /* Carrying Label Information - RFC 3107 */
        case CAP_MP_LABELLED_IPV4:
            i4Status = Bgp4MpeFillPathAttribute (pRtProfile, pAdvtBgpInfo,
                                                 pPeerEntry);
            if (i4Status == BGP4_FAILURE)
            {
                return BGP4_FAILURE;
            }
            break;

        case CAP_MP_VPN4_UNICAST:
            /* If the route is VPNv4 and the peer is CE, then fill in
             * normal NLRI field of update message ,else fill in MP_REACH_NLRI
             * attribute
             */
            if (BGP4_VPN4_PEER_ROLE (pPeerEntry) != BGP4_VPN4_CE_PEER)
            {
                i4Status = Bgp4MpeFillPathAttribute (pRtProfile, pAdvtBgpInfo,
                                                     pPeerEntry);
                if (i4Status == BGP4_FAILURE)
                {
                    return BGP4_FAILURE;
                }
            }
            break;
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
            i4Status = Bgp4MpeFillPathAttribute (pRtProfile, pAdvtBgpInfo,
                                                 pPeerEntry);
            if (i4Status == BGP4_FAILURE)
            {
                return BGP4_FAILURE;
            }
            break;

#endif
        default:
            break;
    }

    /* ATTR : 16 */
    /* If the peer ecommunity send-status is set to true, then only
     * get the extended community attribute information
     */
    if (BGP4_PEER_ECOMM_SEND_STATUS (pPeerEntry) != EXT_COMM_FALSE)
    {
        i4Status = ExtCommGetUpdExtCommAttrib (pRtProfile, pAdvtBgpInfo,
                                               pPeerEntry, pRtInfo);

        if (i4Status == BGP4_FAILURE)
        {
            return BGP4_FAILURE;
        }
    }

    /* Unknown Attribute. */
    i4Status = Bgp4AttrFillUnknown (BGP4_RT_BGP_INFO (pRtProfile),
                                    pAdvtBgpInfo);
    if (i4Status == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AttrAttributeSize                                     */
/* Description   : This routine gives the number of bytes required to form   */
/*                 the update which describes about the particular route to  */
/*                 the specified peer.                                       */
/* Input(s)      : peer to which this update is going to be send             */
/*                 (pPeerInfo),                                              */
/*                 Path attribute which is going to be advertised            */
/*                 (pBgp4Info).                                              */
/*                 Address Family to which this Bgp Info belongs.            */
/*                 Sub-Address Family to which this Bgp Info belongs.        */
/* Output(s)     : none.                                                     */
/* Return(s)     : number of bytes required to be filled .                   */
/*****************************************************************************/
UINT2
Bgp4AttrAttributeSize (tBgp4PeerEntry * pPeerInfo,
                       tBgp4Info * pBgp4Info, UINT2 u2Afi, UINT2 u2Safi)
{
    INT4                i4SpeakerPeerCap;
    UINT4               u4AsafiMask = 0;
    UINT2               u2PathAttrLen = 0;
    UINT2               u2AttrLen = 0;

    /* ATTR 1 : ORIGIN */
    u2PathAttrLen = BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
        BGP4_NORMAL_ATTR_LEN_SIZE + BGP4_ATTR_ORIGIN_LEN;

    /* ATTR 2 & 17: AS-PATH & AS4-PATH */
    i4SpeakerPeerCap = Bgp4GetSpeakerPeer4ByteAsnCapability (pPeerInfo);
    u2AttrLen = Bgp4AttrAsPathSize (pBgp4Info, i4SpeakerPeerCap);
    if (u2AttrLen > 0)
    {
        u2PathAttrLen += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN + u2AttrLen;
        u2PathAttrLen += (u2AttrLen > BGP4_ONE_BYTE_MAX_VAL) ?
            BGP4_EXTENDED_ATTR_LEN_SIZE : BGP4_NORMAL_ATTR_LEN_SIZE;
    }
    u2AttrLen = Bgp4AttrAs4PathSize (pBgp4Info, i4SpeakerPeerCap);
    if (u2AttrLen > 0)
    {
        u2PathAttrLen += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN + u2AttrLen;
        u2PathAttrLen += (u2AttrLen > BGP4_ONE_BYTE_MAX_VAL) ?
            BGP4_EXTENDED_ATTR_LEN_SIZE : BGP4_NORMAL_ATTR_LEN_SIZE;
    }

    /* ATTR 3 : NEXT-HOP */
    u4AsafiMask = ((UINT4) u2Afi << BGP4_TWO_BYTE_BITS) | u2Safi;
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
            if (BGP4_AFI_IN_ADDR_PREFIX_INFO
                (BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)) == BGP4_INET_AFI_IPV4)
            {
                u2PathAttrLen += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                    BGP4_NORMAL_ATTR_LEN_SIZE + BGP4_ATTR_NEXTHOP_LEN;
            }
            break;
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
            if (BGP4_AFI_IN_ADDR_PREFIX_INFO
                (BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)) == BGP4_INET_AFI_IPV4)
            {
                u2PathAttrLen += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                    BGP4_NORMAL_ATTR_LEN_SIZE + BGP4_ATTR_NEXTHOP_LEN;
            }
            break;
#endif
#ifdef L3VPN
        case CAP_MP_VPN4_UNICAST:
            if (BGP4_VPN4_PEER_ROLE (pPeerInfo) == BGP4_VPN4_CE_PEER)
            {
                u2PathAttrLen =
                    (UINT2) (u2PathAttrLen +
                             (BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                              BGP4_NORMAL_ATTR_LEN_SIZE +
                              BGP4_ATTR_NEXTHOP_LEN));
            }
            break;
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
            if (BGP4_AFI_IN_ADDR_PREFIX_INFO
                (BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)) == BGP4_INET_AFI_L2VPN)
            {
                u2PathAttrLen = (UINT2) (u2PathAttrLen +
                                         (BGP4_ATYPE_FLAGS_LEN +
                                          BGP4_ATYPE_CODE_LEN +
                                          BGP4_NORMAL_ATTR_LEN_SIZE +
                                          BGP4_ATTR_NEXTHOP_LEN));
            }
            break;
#endif
        default:
            break;
    }

    /* ATTR 4 : MED */
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_MED_MASK) ==
        BGP4_ATTR_MED_MASK)
    {
        u2PathAttrLen += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_NORMAL_ATTR_LEN_SIZE + BGP4_ATTR_MED_LEN;
    }
    /* ATTR 5 : LOCAL_PREF */
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_LOCAL_PREF_MASK) ==
        BGP4_ATTR_LOCAL_PREF_MASK)
    {
        u2PathAttrLen += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_NORMAL_ATTR_LEN_SIZE + BGP4_ATTR_LOCAL_PREF_LEN;
    }

    /* ATTR 6 : ATOMIC_AGGREGATOR */
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_ATOMIC_AGGR_MASK)
        == BGP4_ATTR_ATOMIC_AGGR_MASK)
    {
        u2PathAttrLen += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_NORMAL_ATTR_LEN_SIZE + BGP4_ATTR_ATOMIC_AGGR_LEN;
    }

    /* ATTR 7 & 18: AGGREGATOR & AGGREGATOR_FOUR (Added for 4-byte ASN support) */
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_AGGREGATOR_MASK)
        == BGP4_ATTR_AGGREGATOR_MASK)
    {
        if (i4SpeakerPeerCap == BGP4_4BYTE_ASN_SPEAKER_AND_PEER)
        {
            u2PathAttrLen += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                BGP4_NORMAL_ATTR_LEN_SIZE + BGP4_ATTR_AGGREGATOR_FOUR_LEN;
        }
        else
        {
            u2PathAttrLen += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                BGP4_NORMAL_ATTR_LEN_SIZE + BGP4_ATTR_AGGREGATOR_LEN;

            if (BGP4_INFO_AGGREGATOR_AS (pBgp4Info) > BGP4_MAX_TWO_BYTE_AS)
            {
                u2PathAttrLen += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                    BGP4_NORMAL_ATTR_LEN_SIZE + BGP4_ATTR_AGGREGATOR_FOUR_LEN;
            }
        }
    }

    /* ATTR 8 : COMMUNITY */
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_COMM_MASK) ==
        BGP4_ATTR_COMM_MASK)
    {
        u2AttrLen = (UINT2) (BGP4_INFO_COMM_COUNT (pBgp4Info) * COMM_VALUE_LEN);
        u2PathAttrLen += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN + u2AttrLen;
        u2PathAttrLen += (u2AttrLen > BGP4_ONE_BYTE_MAX_VAL) ?
            BGP4_EXTENDED_ATTR_LEN_SIZE : BGP4_NORMAL_ATTR_LEN_SIZE;
    }

    /* ATTR 9 : ORIG-ID */
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_ORIG_ID_MASK) ==
        BGP4_ATTR_ORIG_ID_MASK)
    {
        u2PathAttrLen += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_NORMAL_ATTR_LEN_SIZE + ORIGINATOR_ID_LENGTH;
    }

    /* ATTR 10 : CLUSTER-LIST */
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_CLUS_LIST_MASK) ==
        BGP4_ATTR_CLUS_LIST_MASK)
    {
        u2AttrLen = (UINT2) (BGP4_INFO_CLUS_LIST_COUNT (pBgp4Info) *
                             CLUSTER_ID_LENGTH);
        u2PathAttrLen += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN + u2AttrLen;
        u2PathAttrLen += (u2AttrLen > BGP4_ONE_BYTE_MAX_VAL) ?
            BGP4_EXTENDED_ATTR_LEN_SIZE : BGP4_NORMAL_ATTR_LEN_SIZE;
    }

    /* ATTR 14 : MP_REACH */
#ifdef L3VPN

    if (!((((u2Afi == BGP4_INET_AFI_IPV4) &&
            (u2Safi == BGP4_INET_SAFI_UNICAST) &&
            (BGP4_AFI_IN_ADDR_PREFIX_INFO
             (BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)) ==
             BGP4_INET_AFI_IPV4))) ||
          (((u2Afi == BGP4_INET_AFI_IPV4) &&
            (u2Safi == BGP4_INET_SAFI_VPNV4_UNICAST) &&
            (BGP4_VPN4_PEER_ROLE (pPeerInfo) == BGP4_VPN4_CE_PEER)))))
#else
    if (!((u2Afi == BGP4_INET_AFI_IPV4) && (u2Safi == BGP4_INET_SAFI_UNICAST) &&
          (BGP4_AFI_IN_ADDR_PREFIX_INFO
           (BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)) == BGP4_INET_AFI_IPV4)))
#endif
    {
        /* For MP_REACH attribute the length field is always 2 bytes. */
        u2PathAttrLen =
            (UINT2) (u2PathAttrLen + BGP4_ATYPE_FLAGS_LEN +
                     BGP4_ATYPE_CODE_LEN + BGP4_EXTENDED_ATTR_LEN_SIZE +
                     BGP4_MPE_ADDRESS_FAMILY_LEN +
                     BGP4_MPE_SUB_ADDRESS_FAMILY_LEN);
        /* Next-HOP */
        switch (u4AsafiMask)
        {
            case CAP_MP_IPV4_UNICAST:
                u2PathAttrLen =
                    (UINT2) (u2PathAttrLen + BGP4_ATTR_NEXTHOP_LEN + 1);
                break;
#ifdef VPLSADS_WANTED
            case CAP_MP_L2VPN_VPLS:

                u2PathAttrLen =
                    (UINT2) (u2PathAttrLen + BGP4_ATTR_NEXTHOP_LEN + 1);
                break;
#endif
#ifdef EVPN_WANTED
            case CAP_MP_L2VPN_EVPN:
                u2PathAttrLen =
                    (UINT2) (u2PathAttrLen + BGP4_EVPN_MAC_NLRI_PREFIX_LEN + 1);
#endif
#ifdef BGP4_IPV6_WANTED
            case CAP_MP_IPV6_UNICAST:
                if (BGP4_INFO_LINK_LOCAL_PRESENT (pBgp4Info) == BGP4_TRUE)
                {
                    u2PathAttrLen =
                        (UINT2) (u2PathAttrLen + (BGP4_IPV6_PREFIX_LEN * 2) +
                                 1);
                }
                else
                {
                    u2PathAttrLen =
                        (UINT2) (u2PathAttrLen + BGP4_IPV6_PREFIX_LEN + 1);
                }
                break;
#endif
#ifdef L3VPN
                /* Carrying Label Information - RFC 3107 */
            case CAP_MP_LABELLED_IPV4:
                u2PathAttrLen =
                    (UINT2) (u2PathAttrLen + (BGP4_IPV4_PREFIX_LEN + 1));
                break;
            case CAP_MP_VPN4_UNICAST:
                u2PathAttrLen =
                    (UINT2) (u2PathAttrLen + (BGP4_VPN4_PREFIX_LEN + 1));
                break;
#endif
            default:
                break;
        }
        /* SNPA */
        u2PathAttrLen += 1;
    }

    /* ATTR 15 : MP_UNREACH -> No need to fill for this. Always MP_UNREACH
     * messages are send seperately. */

    /* ATTR 16 : EXT-COMMUNITY */
    u2PathAttrLen += ExtCommAttrLength (pBgp4Info);

    /* UNKNOWN ATTRIBUTE */
    u2PathAttrLen += Bgp4AttrUnknownLength (pBgp4Info);

    BGP4_DBG1 (BGP4_DBG_CTRL_FLOW,
               "\tBgp4AttrAttributeSize() : Length = %d\n ", u2PathAttrLen);
    return u2PathAttrLen;
}

/*****************************************************************************/
/* Function Name : Bgp4AttrFillPrefix                                        */
/* Description   : This routine fills the NLRI information in the buffer.    */
/* Input(s)      : Route profile which is going to be advertised             */
/*                 (pRtProfile),                                             */
/*                 Buffer in which NLRI is going to be filled (pu1MsgBuf)    */
/* Output(s)     : None.                                                     */
/* Return(s)     : Number of bytes required to be filled .                   */
/*****************************************************************************/
UINT2
Bgp4AttrFillPrefix (UINT1 *pu1MsgBuf, tRouteProfile * pRtProfile)
{
    UINT1               u1PrefixLenBytes = 0;
    UINT2               u2BytesFilled = 0;
    UINT4               u4IpPrefix;

    switch (BGP4_RT_AFI_INFO (pRtProfile))
    {
        case BGP4_INET_AFI_IPV4:
            *pu1MsgBuf = (UINT1) BGP4_RT_PREFIXLEN (pRtProfile);
            pu1MsgBuf++;
            u2BytesFilled++;
            u1PrefixLenBytes =
                Bgp4RoundToBytes (BGP4_RT_PREFIXLEN (pRtProfile));
            PTR_FETCH4 (u4IpPrefix, BGP4_RT_IP_PREFIX (pRtProfile));
            MEMCPY (pu1MsgBuf, (UINT1 *) (&u4IpPrefix),
                    MEM_MAX_BYTES (u1PrefixLenBytes, sizeof (UINT4)));
            BGP4_TRC_ARG3 (NULL,
                           BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tFilling Prefix attribute in update message - "
                           " PrefixLen = %s, Prefix = %s\n",
                           Bgp4PrintIpMask ((UINT1)
                                            BGP4_RT_PREFIXLEN (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            break;

#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
            *pu1MsgBuf = (UINT1) BGP4_RT_PREFIXLEN (pRtProfile);
            pu1MsgBuf++;
            u2BytesFilled++;
            u1PrefixLenBytes =
                Bgp4RoundToBytes (BGP4_RT_PREFIXLEN (pRtProfile));
            if (u1PrefixLenBytes <= BGP4_MAX_INET_ADDRESS_LEN)
            {
                MEMCPY (pu1MsgBuf, BGP4_RT_IP_PREFIX (pRtProfile),
                        u1PrefixLenBytes);
            }
            BGP4_TRC_ARG3 (NULL,
                           BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tFilling Prefix attribute in update message - "
                           " PrefixLen = %s, Prefix = %s\n",
                           Bgp4PrintIpMask ((UINT1)
                                            BGP4_RT_PREFIXLEN (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            break;
#endif
        default:
            return 0;
    }
    pu1MsgBuf += u1PrefixLenBytes;

    u2BytesFilled += u1PrefixLenBytes;

    return u2BytesFilled;
}

#ifdef L3VPN
/*****************************************************************************/
/* Function Name : Bgp4AttrFillVpnv4Prefix                                   */
/* Description   : This routine fills the vpnv4 nlri in the buffer.          */
/* Input(s)      : Route profile which is going to be advertised             */
/*                 (pRtProfile),                                             */
/*                 Buffer in which NLRI is going to be filled (pu1MsgBuf)    */
/* Output(s)     : None.                                                     */
/* Return(s)     : Number of bytes required to be filled .                   */
/*****************************************************************************/
UINT2
Bgp4AttrFillVpnv4Prefix (UINT1 *pu1MsgBuf, tRouteProfile * pRtProfile)
{
    UINT1               u1PrefixLenBytes = 0;
    UINT4               u4IpPrefix;

    u1PrefixLenBytes = Bgp4RoundToBytes (BGP4_RT_PREFIXLEN (pRtProfile));
    PTR_FETCH4 (u4IpPrefix, BGP4_RT_IP_PREFIX (pRtProfile));
    BGP4_DBG3 (BGP4_DBG_CTRL_FLOW,
               "\tBgp4AttrFillPrefix() : PrefixLen = %d, "
               "Prefix = %x, %x\n ", u1PrefixLenBytes, u4IpPrefix,
               (u4IpPrefix));
    MEMCPY (pu1MsgBuf, (UINT1 *) (&u4IpPrefix),
            MEM_MAX_BYTES (u1PrefixLenBytes, sizeof (UINT4)));
    return u1PrefixLenBytes;
}
#endif

/*****************************************************************************/
/* Function Name : Bgp4AttrAsPathSize                                        */
/* Description   : This routine estimates the amount of buffer required to   */
/*                 fill the AS_PATH information present in the Route profile.*/
/* Input(s)      : BGP information which contains the AS PATH (pBgp4Info),   */
/*                 Speaker and Peer's 4byte ASN Capability                   */
/* Output(s)     : None.                                                     */
/* Return(s)     : Number of bytes required.                                 */
/*****************************************************************************/
UINT2
Bgp4AttrAsPathSize (tBgp4Info * pBgp4Info, INT4 i4SpeakerPeerCap)
{
    tAsPath            *pTsAspath = NULL;
    UINT2               u2PathSize = 0;

    BGP4_DBG1 (BGP4_DBG_CTRL_FLOW,
               "\tBgp4AttrAsPathSize() : "
               "Attr Flag %x\n ", BGP4_INFO_ATTR_FLAG (pBgp4Info));

    if (TMO_SLL_Count (BGP4_INFO_ASPATH (pBgp4Info)) > 0)
    {
        BGP4_DBG1 (BGP4_DBG_CTRL_FLOW,
                   "\tBgp4AttrAsPathSize() : ASPath present, "
                   "Path size = %d\n",
                   TMO_SLL_Count (BGP4_INFO_ASPATH (pBgp4Info)));
        TMO_SLL_Scan (BGP4_INFO_ASPATH (pBgp4Info), pTsAspath, tAsPath *)
        {
            /* Seg. type and length field */
            u2PathSize += BGP4_ATTR_PATH_TYPE_LEN + BGP4_ATTR_PATH_LEN_LEN;

            if (i4SpeakerPeerCap == BGP4_4BYTE_ASN_SPEAKER_AND_PEER)
            {
                /* AS path length is in 4 bytes (uint) format */
                u2PathSize += (BGP4_ASPATH_LEN (pTsAspath) * BGP4_AS4_LENGTH);
            }
            else
            {
                /* AS path length is in 2bytes (uint) format */
                u2PathSize =
                    (UINT2) (u2PathSize +
                             (BGP4_ASPATH_LEN (pTsAspath) * BGP4_AS_LENGTH));
            }
        }
    }
    BGP4_DBG1 (BGP4_DBG_CTRL_FLOW,
               "\tBgp4AttrAsPathSize() : ASPath Size = %d\n", u2PathSize);
    return u2PathSize;
}

/*****************************************************************************/
/* Function Name : Bgp4AttrAs4PathSize                                       */
/* Description   : This routine estimates the amount of buffer required to   */
/*                 fill the AS4_PATH information present in the Route profile*/
/* Input(s)      : BGP information which contains the AS PATH (pBgp4Info),   */
/*                 Speaker and Peer's 4byte ASN Capability                   */
/* Output(s)     : None.                                                     */
/* Return(s)     : Number of bytes required.                                 */
/*****************************************************************************/
UINT2
Bgp4AttrAs4PathSize (tBgp4Info * pBgp4Info, INT4 i4SpeakerPeerCap)
{
    tAsPath            *pTsAspath = NULL;
    INT4                i4FourByteAsnCount = 0;
    INT4                i4FourByteAsnFirst = -1;
    INT4                i4FourByteAsnLast = 0;
    INT4                i4Index;
    UINT4              *pu4AsList;
    UINT2               u2PathSize = 0;

    BGP4_DBG1 (BGP4_DBG_CTRL_FLOW,
               "\tBgp4AttrAs4PathSize() : "
               "Attr Flag %x\n ", BGP4_INFO_ATTR_FLAG (pBgp4Info));

    if ((i4SpeakerPeerCap != BGP4_4BYTE_ASN_SPEAKER_AND_PEER) &&
        (TMO_SLL_Count (BGP4_INFO_ASPATH (pBgp4Info)) > 0))
    {
        BGP4_DBG1 (BGP4_DBG_CTRL_FLOW,
                   "\tBgp4AttrAs4PathSize() : ASPath present, "
                   "Path size = %d\n",
                   TMO_SLL_Count (BGP4_INFO_ASPATH (pBgp4Info)));
        TMO_SLL_Scan (BGP4_INFO_ASPATH (pBgp4Info), pTsAspath, tAsPath *)
        {
            if ((BGP4_ASPATH_TYPE (pTsAspath) != BGP4_ATTR_PATH_CONFED_SET) &&
                (BGP4_ASPATH_TYPE (pTsAspath) !=
                 BGP4_ATTR_PATH_CONFED_SEQUENCE))
            {
                pu4AsList = (UINT4 *) (VOID *) BGP4_ASPATH_NOS (pTsAspath);
                for (i4Index = 0; i4Index < BGP4_ASPATH_LEN (pTsAspath);
                     i4Index++)
                {
                    /* When there is  non-mappable 4-byte ASN in the path list with
                     * FourByteAsnFound flag is set*/
                    BGP4_FOUR_BYTE_ASN_FOUND (i4FourByteAsnFirst,
                                              i4FourByteAsnLast, i4Index);
                    pu4AsList++;
                }

                if (i4FourByteAsnFirst != -1)
                {
                    i4FourByteAsnCount =
                        i4FourByteAsnLast - i4FourByteAsnFirst + 1;

                    /* Seg. type and length field */
                    u2PathSize =
                        (UINT2) (u2PathSize + BGP4_ATTR_PATH_TYPE_LEN +
                                 BGP4_ATTR_PATH_LEN_LEN);

                    /* AS path length is in 4 bytes (uint) format */
                    u2PathSize =
                        (UINT2) (u2PathSize +
                                 (i4FourByteAsnCount * BGP4_AS4_LENGTH));

                }
            }
            i4FourByteAsnFirst = -1;
        }
    }
    BGP4_DBG1 (BGP4_DBG_CTRL_FLOW,
               "\tBgp4AttrAs4PathSize() : ASPath Size = %d\n", u2PathSize);
    return u2PathSize;
}

/*****************************************************************************/
/* Function Name : Bgp4AttrFillOrigin                                        */
/* Description   : This routine fills the Origin information in the BGP-INFO */
/*                 to be advertised.                                         */
/* Input(s)      : Received Path Attribute information (pRcvdBgpInfo)        */
/* Output(s)     : Updated Path Attribute information that is advertised.    */
/*                 (pAdvtBgpInfo)                                            */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4AttrFillOrigin (tBgp4Info * pRcvdBgpInfo, tBgp4Info * pAdvtBgpInfo)
{
    BGP4_INFO_ORIGIN (pAdvtBgpInfo) = BGP4_INFO_ORIGIN (pRcvdBgpInfo);
    BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_ORIGIN_MASK;
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name   : Bgp4AttrFillAspath                                       */
/* Description     : This function fills the AS path attribute from the       */
/*                   received BGP-INFO to the advertised BGP-INFO             */
/*                   When propagating route:                                  */
/*                   To internal peer, received AS path attribute should be   */
/*                   sent as it is received.                                  */
/*                   To Confederation peer, prepend Member-AS number in       */
/*                   AS_CONFED_SEQUENCE segment of the received AS path       */
/*                   attribute.                                               */
/*                   To external peer, prepend Confed-Id (or local-AS number, */
/*                   if confederation doesn't exists) in AS_SEQUENCE segment  */
/*                   of the received and sanitized AS path attribute.         */
/*                   When originating routes:                                 */
/*                   To internal peer, send empty AS path attribute.          */
/*                   To Confederation peer, send Member-AS number in          */
/*                   AS_CONFED_SEQUENCE.                                      */
/*                   To external peer, send Confed-Id (or local-AS number, if */
/*                   confederation doesn't exists) in AS_SEQUENCE.            */
/* Input(s)        : pRcvdBgpInfo - Received BGP path attributes information. */
/*                   pPeerInfo - Information about peer to which this update  */
/*                               message is to be send.                       */
/* Output(s)       : pAdvtBgpInfo - Updated BGP path attribute information    */
/*                                  to be advertised.                         */
/* Global Variables Referred : BGP4_CONFED_ID(BGP4_DFLT_VRFID) and BGP4_LOCAL_AS_NO(BGP4_DFLT_VRFID)            */
/* Global variables Modified : None                                           */
/* Exceptions or Operating System Error Handling : None                       */
/* Use of Recursion: None.                                                    */
/* Returns         : BGP4_SUCCESS/BGP4_FAILURE                                */
/******************************************************************************/
INT4
Bgp4AttrFillAspath (tBgp4Info * pRcvdBgpInfo, tBgp4Info * pAdvtBgpInfo,
                    tBgp4PeerEntry * pPeerInfo)
{
    tAsPath            *pFirstAspath = NULL;
    tAsPath            *pTsAspath = NULL;
    tAsPath            *pNewASPath = NULL;
    tAsPath            *pLocalASPath = NULL;
    UINT4               u4PeerType;
    UINT4               u4ASNO = 0;
    UINT4               u4LocalAs = 0;
    UINT1               u1IsAddNewFirstPath = BGP4_TRUE;
    UINT1               u1LocalAsPeer = BGP_ZERO;

    BGP4_DBG (BGP4_DBG_ENTRY, "\tBgp4AttrFillAspath() ...\n");
    if (BGP4_PEER_LOCAL_AS_CFG (pPeerInfo) == BGP4_TRUE)
    {
        u1LocalAsPeer = BGP4_PEER_LCL_AS;
    }

    u4PeerType = BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerInfo), pPeerInfo);

    if (u4PeerType == BGP4_EXTERNAL_PEER)
    {
        pNewASPath = Bgp4MemGetASNode (sizeof (tAsPath));
        if (pNewASPath == NULL)
        {
            BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                      BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for Advertise BgpInfo's AS Node"
                      " FAILED!!!\n");
            gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
            return (BGP4_FAILURE);
        }

        if (BGP4_CONFED_PEER_STATUS (pPeerInfo) == BGP4_TRUE)
        {
            /* CONFED-External Peer. */
            BGP4_ASPATH_TYPE (pNewASPath) = BGP4_ATTR_PATH_CONFED_SEQUENCE;
            u4ASNO = BGP4_PEER_LOCAL_AS (pPeerInfo);
        }
        else
        {
            /* External Peer */
            BGP4_ASPATH_TYPE (pNewASPath) = BGP4_ATTR_PATH_SEQUENCE;
            u4ASNO =
                ((BGP4_CONFED_ID (BGP4_PEER_CXT_ID (pPeerInfo)) ==
                  BGP4_INV_AS) ? BGP4_PEER_LOCAL_AS (pPeerInfo) :
                 BGP4_CONFED_ID (BGP4_PEER_CXT_ID (pPeerInfo)));
        }

        pFirstAspath =
            (tAsPath *) TMO_SLL_First (BGP4_INFO_ASPATH (pRcvdBgpInfo));

        if (pFirstAspath != NULL)
        {
            /* AS-Path is not empty. */
            if (BGP4_ASPATH_TYPE (pNewASPath) ==
                BGP4_ASPATH_TYPE (pFirstAspath))
            {
                if (BGP4_ASPATH_LEN (pFirstAspath) == BGP4_MAX_BYTE_VALUE)
                {
                    /* AS-Path segment is already full. So need to add only
                     * the u2ASNO to new Path Segment. Allocate a new path
                     * segment and add the new path to it. */
                    u1IsAddNewFirstPath = BGP4_TRUE;
                }
                else
                {
                    /* Add the First AS-Path Segment to the new Path. */
                    if (u1LocalAsPeer == BGP4_PEER_LCL_AS)
                    {
                        /* When a local-as is configured allocate sapce for copying it also */
                        BGP4_ASPATH_LEN (pNewASPath) = (UINT1)
                            (BGP4_MIN_AS_IN_SEG +
                             BGP4_ASPATH_LEN (pFirstAspath) +
                             BGP4_MIN_AS_IN_SEG);
                    }
/*RM#26398-Hide password in sh hist */
                    else
                    {
                        BGP4_ASPATH_LEN (pNewASPath) = (UINT1)
                            (BGP4_ASPATH_LEN (pFirstAspath) +
                             BGP4_MIN_AS_IN_SEG);
                    }

                    if (BGP4_AS4_SEG_LEN <
                        (BGP4_ASPATH_LEN (pNewASPath) * BGP4_AS4_LENGTH))
                    {
                        Bgp4MemReleaseASNode (pNewASPath);
                        BGP4_TRC_ARG3 (&
                                       (BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                                       BGP4_TRC_FLAG,
                                       BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                                       BGP4_MOD_NAME,
                                       "\tPEER %s : Size of AS4 segment length %d is less than AS path length %d\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeerInfo),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeerInfo))),
                                       BGP4_AS4_SEG_LEN,
                                       (BGP4_ASPATH_LEN (pNewASPath) *
                                        BGP4_AS4_LENGTH));
                        return (BGP4_FAILURE);
                    }
                    MEMSET (BGP4_ASPATH_NOS (pNewASPath), 0, BGP4_AS4_SEG_LEN);    /* In the path list, ASNs are 
                                                                                   stored as 4 bytes */
                    PTR_ASSIGN4 (BGP4_ASPATH_NOS (pNewASPath), u4ASNO);

                    if (u1LocalAsPeer == BGP4_PEER_LCL_AS)
                    {
                        /* when local-as is configured for neighbour get this info and copyin after copying Peer-Asno */
                        u4LocalAs =
                            BGP4_LOCAL_AS_NO (BGP4_PEER_CXT_ID (pPeerInfo));

                        pLocalASPath = Bgp4MemGetASNode (sizeof (tAsPath));
                        if (pLocalASPath == NULL)
                        {
                            BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                                      BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                                      BGP4_MOD_NAME,
                                      "\tMemory Allocation for Advertise BgpInfo's AS Node"
                                      " FAILED!!!\n");
                            gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
                            return (BGP4_FAILURE);
                        }
                        else
                        {
                            BGP4_ASPATH_LEN (pLocalASPath) = BGP4_MIN_AS_IN_SEG;
                            if (BGP4_AS4_SEG_LEN <
                                (BGP4_ASPATH_LEN (pLocalASPath) *
                                 BGP4_AS4_LENGTH))
                            {
                                Bgp4MemReleaseASNode (pLocalASPath);
                                BGP4_TRC_ARG3 (&
                                               (BGP4_PEER_REMOTE_ADDR_INFO
                                                (pPeerInfo)), BGP4_TRC_FLAG,
                                               BGP4_ALL_FAILURE_TRC |
                                               BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                                               "\tPEER %s : Size of AS4 segment length %d is less than local AS path length %d\n",
                                               Bgp4PrintIpAddr
                                               (BGP4_PEER_REMOTE_ADDR
                                                (pPeerInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerInfo))),
                                               BGP4_AS4_SEG_LEN,
                                               (BGP4_ASPATH_LEN (pLocalASPath) *
                                                BGP4_AS4_LENGTH));
                                return (BGP4_FAILURE);
                            }
                            MEMSET (BGP4_ASPATH_NOS (pLocalASPath), 0,
                                    BGP4_AS4_SEG_LEN);
                            PTR_ASSIGN4 (BGP4_ASPATH_NOS (pLocalASPath),
                                         u4LocalAs);

                            /* move the buffer pointer and copying local-as */
                            MEMCPY (BGP4_ASPATH_NOS (pNewASPath) +
                                    BGP4_AS4_LENGTH,
                                    BGP4_ASPATH_NOS (pLocalASPath),
                                    BGP4_ASPATH_LEN (pLocalASPath) *
                                    BGP4_AS4_LENGTH);
                            /* move the buffer pointer and copying the 1st first path AS */
                            MEMCPY (BGP4_ASPATH_NOS (pNewASPath) +
                                    (2 * BGP4_AS4_LENGTH),
                                    BGP4_ASPATH_NOS (pFirstAspath),
                                    BGP4_ASPATH_LEN (pFirstAspath) *
                                    BGP4_AS4_LENGTH);
                        }
                    }
                    else
                    {
                        MEMCPY (BGP4_ASPATH_NOS (pNewASPath) + BGP4_AS4_LENGTH,
                                BGP4_ASPATH_NOS (pFirstAspath),
                                BGP4_ASPATH_LEN (pFirstAspath) *
                                BGP4_AS4_LENGTH);
                    }
                    TMO_SLL_Add (BGP4_INFO_ASPATH (pAdvtBgpInfo),
                                 &pNewASPath->sllNode);
                    u1IsAddNewFirstPath = BGP4_FALSE;
                }
            }
            else
            {
                /* Need to add only u2ASNO to the new AS-Path Segment. */
                u1IsAddNewFirstPath = BGP4_TRUE;
            }
        }
        else
        {
            /* AS-Path is empty. Fill only u2ASNO and return . */
            u1IsAddNewFirstPath = BGP4_TRUE;
        }

        if (u1IsAddNewFirstPath == BGP4_TRUE)
        {
            /* If this flag is true then to the new AS-Path
             * segment add only the u2ASNO and if necessary allocate
             * another Path Segment and add the First AS-Path. */

            if (u1LocalAsPeer == BGP4_PEER_LCL_AS)
            {
                BGP4_ASPATH_LEN (pNewASPath) = (2 * BGP4_MIN_AS_IN_SEG);
            }
            else
            {
                BGP4_ASPATH_LEN (pNewASPath) = BGP4_MIN_AS_IN_SEG;
            }
            if (BGP4_AS4_SEG_LEN <
                (BGP4_ASPATH_LEN (pNewASPath) * BGP4_AS4_LENGTH))
            {
                Bgp4MemReleaseASNode (pNewASPath);
                BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                               BGP4_TRC_FLAG,
                               BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : Size of AS4 segment length %d is less than AS path length %d\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerInfo))),
                               BGP4_AS4_SEG_LEN,
                               (BGP4_ASPATH_LEN (pNewASPath) *
                                BGP4_AS4_LENGTH));
                return (BGP4_FAILURE);
            }
            MEMSET (BGP4_ASPATH_NOS (pNewASPath), 0, BGP4_AS4_SEG_LEN);
            PTR_ASSIGN4 (BGP4_ASPATH_NOS (pNewASPath), u4ASNO);
            if (u1LocalAsPeer == BGP4_PEER_LCL_AS)
            {
                /* when local-as is configured for neighbour get this info and copyin after copying Peer-Asno */
                u4LocalAs = BGP4_LOCAL_AS_NO (BGP4_PEER_CXT_ID (pPeerInfo));
                pLocalASPath = Bgp4MemGetASNode (sizeof (tAsPath));
                if (pLocalASPath == NULL)
                {
                    BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                              BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                              BGP4_MOD_NAME,
                              "\tMemory Allocation for Advertise BgpInfo's AS Node"
                              " FAILED!!!\n");
                    gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
                }
                else
                {
                    BGP4_ASPATH_LEN (pLocalASPath) = BGP4_MIN_AS_IN_SEG;
                    if (BGP4_AS4_SEG_LEN <
                        (BGP4_ASPATH_LEN (pLocalASPath) * BGP4_AS4_LENGTH))

                    {
                        Bgp4MemReleaseASNode (pLocalASPath);
                        BGP4_TRC_ARG3 (&
                                       (BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                                       BGP4_TRC_FLAG,
                                       BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                                       BGP4_MOD_NAME,
                                       "\tPEER %s : Size of AS4 segment length %d is less than local AS path length %d\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeerInfo),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeerInfo))),
                                       BGP4_AS4_SEG_LEN,
                                       (BGP4_ASPATH_LEN (pLocalASPath) *
                                        BGP4_AS4_LENGTH));
                        return (BGP4_FAILURE);
                    }
                    MEMSET (BGP4_ASPATH_NOS (pLocalASPath), 0, BGP4_AS4_SEG_LEN);    /* In the path list, ASNs are */
                    PTR_ASSIGN4 (BGP4_ASPATH_NOS (pLocalASPath), u4LocalAs);
                    /* move the buffer pointer and copying local-as */
                    MEMCPY (BGP4_ASPATH_NOS (pNewASPath) + BGP4_AS4_LENGTH,
                            BGP4_ASPATH_NOS (pLocalASPath),
                            BGP4_ASPATH_LEN (pLocalASPath) * BGP4_AS4_LENGTH);
                }
            }

            TMO_SLL_Add (BGP4_INFO_ASPATH (pAdvtBgpInfo), &pNewASPath->sllNode);
        }
    }

    TMO_SLL_Scan (BGP4_INFO_ASPATH (pRcvdBgpInfo), pTsAspath, tAsPath *)
    {
        if ((u4PeerType == BGP4_EXTERNAL_PEER) &&
            (BGP4_CONFED_PEER_STATUS (pPeerInfo) == BGP4_FALSE))
        {
            /* If external peer is not in the confed, sanitize as_path. */
            if ((BGP4_ASPATH_TYPE (pTsAspath) ==
                 BGP4_ATTR_PATH_CONFED_SET) ||
                (BGP4_ASPATH_TYPE (pTsAspath) ==
                 BGP4_ATTR_PATH_CONFED_SEQUENCE))
            {
                continue;
            }
        }

        if ((pTsAspath == pFirstAspath) && (u1IsAddNewFirstPath == BGP4_FALSE))
        {
            /* No need to add the First AS-Path. Since we have already
             * added it. */
            continue;
        }

        /* Copy the Current AS-Path into New AS-Path. */
        pNewASPath = Bgp4MemGetASNode (sizeof (tAsPath));
        if (pNewASPath == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                      BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tBgp4AttrFillAspath() : GET_ASNode failed !!! \n");
            gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
            return BGP4_FAILURE;
        }
        BGP4_ASPATH_TYPE (pNewASPath) = BGP4_ASPATH_TYPE (pTsAspath);
        BGP4_ASPATH_LEN (pNewASPath) = BGP4_ASPATH_LEN (pTsAspath);
        if (BGP4_AS4_SEG_LEN < (BGP4_ASPATH_LEN (pNewASPath) * BGP4_AS4_LENGTH))
        {
            BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG,
                           BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Size of AS4 segment length %d is less than AS path length %d\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))), BGP4_AS4_SEG_LEN,
                           (BGP4_ASPATH_LEN (pNewASPath) * BGP4_AS4_LENGTH));
            Bgp4MemReleaseASNode (pNewASPath);
            return BGP4_FAILURE;
        }
        MEMSET (BGP4_ASPATH_NOS (pNewASPath), 0, BGP4_AS4_SEG_LEN);
        MEMCPY (BGP4_ASPATH_NOS (pNewASPath), BGP4_ASPATH_NOS (pTsAspath),
                (BGP4_ASPATH_LEN (pTsAspath) * BGP4_AS4_LENGTH));

        TMO_SLL_Add (BGP4_INFO_ASPATH (pAdvtBgpInfo), &pNewASPath->sllNode);
    }
    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                   BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                   BGP4_MOD_NAME,
                   "\tPEER %s : AS Path attribute is successfully filled.\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo))));
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AttrFillNexthop                                       */
/* Description   : This routine fills the NextHop information in the         */
/*                 BGP4-INFO that needs to be advertised.                    */
/* Input(s)      : Route profile which is going to be advertised             */
/*                 (pRtProfile),                                             */
/*                 Peer Information (pPeerInfo)                              */
/* Output(s)     : Updated BGP4-INFO to be advertised (pAdvtBgpInfo)         */
/* Return(s)     : BGP4_SUCCESS/ BGP4_FAILURE                                */
/*****************************************************************************/
INT4
Bgp4AttrFillNexthop (tRouteProfile * pRtProfile, tBgp4Info * pAdvtBgpInfo,
                     tBgp4PeerEntry * pPeerInfo)
{
    tBgp4PeerEntry     *pSrcPeer = NULL;
    tNetAddress         LocalAddr;
    tAddrPrefix         RemAddr;
    tAddrPrefix         GatewayAddress;
    tBgp4Info          *pBgp4Info = NULL;
    INT4                i4Ret;
    UINT4               u4Nexthop = 0;
    UINT4               u4GatewayFlag = BGP4_FALSE;
    UINT4               u4AsafiMask;
    INT4                i4LocalAddrPresent = BGP4_TRUE;
#ifdef BGP4_IPV6_WANTED
    INT4                i4NetAddrPresent = BGP4_TRUE;
    UINT4               u4IfAddr = 0;
    UINT2               u2Port = 0;
#endif

#ifdef EVPN_WANTED
    INT4                i4RetVal = BGP4_FAILURE;
#endif
    pBgp4Info = BGP4_RT_BGP_INFO (pRtProfile);
    Bgp4GetPeerAdvtAfiSafi (pRtProfile, pPeerInfo, &u4AsafiMask);

#ifdef VPLSADS_WANTED
    /*ADS-VPLS Related processing */
    if (u4AsafiMask == CAP_MP_L2VPN_VPLS)
    {
        Bgp4InitAddrPrefixStruct (&(BGP4_INFO_NEXTHOP_INFO (pAdvtBgpInfo)),
                                  BGP4_INET_AFI_IPV4);
        Bgp4CopyAddrPrefixStruct (&RemAddr,
                                  BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo));

        if ((BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) > 0) &&
            (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID))
        {
            PTR_FETCH_4 (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                         BGP4_INFO_NEXTHOP (pBgp4Info));
            BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_NEXT_HOP_MASK;

            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_VPLS_TRC, BGP4_MOD_NAME,
                           "\tPEER %s :  Nexthop %s Attribute is successfully filled for"
                           "VPLS route\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))),
                           Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_INFO_NEXTHOP_INFO
                                             (pAdvtBgpInfo))));

            return BGP4_SUCCESS;

        }
        i4Ret =
            Bgp4GetLocalAddrForPeer (pPeerInfo, RemAddr, &LocalAddr, BGP4_TRUE);
        if (i4Ret == BGP4_FAILURE)
        {
            /* Unable to find the corresponding local address */
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_VPLS_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Failure in finding the corresponding local address "
                           "from the peer.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            return BGP4_FAILURE;
        }
        PTR_FETCH_4 (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                     BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalAddr.NetAddr));
        BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_NEXT_HOP_MASK;
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_VPLS_TRC, BGP4_MOD_NAME,
                       "\tPEER %s :  Nexthop %s Attribute is successfully filled for"
                       "VPLS route\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))),
                       Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_INFO_NEXTHOP_INFO
                                         (pAdvtBgpInfo))));
        return BGP4_SUCCESS;

    }

#endif
#ifdef EVPN_WANTED
    if (u4AsafiMask == CAP_MP_L2VPN_EVPN)
    {
        i4RetVal =
            Bgp4AttrFillEvpnNextHop (pRtProfile, pAdvtBgpInfo, pPeerInfo);
        if (i4RetVal != BGP4_FAILURE)
        {
            return BGP4_SUCCESS;
        }
    }
#endif

    pSrcPeer = (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID) ?
        BGP4_RT_PEER_ENTRY (pRtProfile) : NULL;

    /* This function is used for <ipv4, unicast> and
     * <vpnv4, unicast> families to fill IPv4 nexthop values
     */
    if (BGP4_RT_AFI_INFO (pRtProfile) != BGP4_INET_AFI_IPV4)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s : The route %s does not support IPV4 Address family."
                       "Failure in filling nexthop attribute.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_FAILURE;
    }

    Bgp4InitAddrPrefixStruct (&(BGP4_INFO_NEXTHOP_INFO (pAdvtBgpInfo)),
                              BGP4_INET_AFI_IPV4);
#ifdef BGP4_IPV6_WANTED
    BGP4_INFO_LINK_LOCAL_PRESENT (pAdvtBgpInfo) = BGP4_FALSE;
#endif

    i4Ret = Bgp4GetPeerNetworkAddress (pPeerInfo, BGP4_INET_AFI_IPV4, &RemAddr);
    if (i4Ret == BGP4_FAILURE)
    {
        /* Network address (v4) is not configured for peer. Now try and get the
         * corresponding IPv4 address of the interface through which the
         * peer is reachable. */
        Bgp4CopyAddrPrefixStruct (&RemAddr,
                                  BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo));
#ifdef BGP4_IPV6_WANTED
        i4NetAddrPresent = BGP4_FALSE;
#endif
    }

    i4Ret = Bgp4GetLocalAddrForPeer (pPeerInfo, RemAddr, &LocalAddr, BGP4_TRUE);
    if (i4Ret == BGP4_FAILURE)
    {
        /* The network address configured for the peer doesn't seem to be
         * correct. There is no reachability information corresponding
         * to the configured network address
         */
        i4LocalAddrPresent = BGP4_FALSE;
    }

    Bgp4GetPeerAdvtAfiSafi (pRtProfile, pPeerInfo, &u4AsafiMask);

#ifdef BGP4_IPV6_WANTED
    if ((i4NetAddrPresent == BGP4_FALSE) && (i4LocalAddrPresent == BGP4_TRUE))
    {
        /* The network address is not configured but the corresponding
         * interface address via which the peer is reachable is found. Get
         * the v4 address of that interface and use it as Local Address. */
        if (Ip6IsLocalSubnet (LocalAddr.NetAddr.au1Address, &u2Port) ==
            IP_SUCCESS)
        {
            /* Get u2Port corresponding v4 address */
            u4IfAddr = BgpGetIpIfAddr (u2Port);
            if (u4IfAddr == 0)
            {
                /* v4 Address not configured for this interface. */
                i4LocalAddrPresent = BGP4_FALSE;
            }
            else
            {
                Bgp4InitAddrPrefixStruct (&(LocalAddr.NetAddr),
                                          BGP4_INET_AFI_IPV4);
                PTR_ASSIGN_4 (LocalAddr.NetAddr.au1Address, u4IfAddr);
            }
        }
        else
        {
            i4LocalAddrPresent = BGP4_FALSE;
        }
    }
#endif

    i4Ret = Bgp4GetPeerGatewayAddress (pPeerInfo, BGP4_INET_AFI_IPV4,
                                       &GatewayAddress);
    if (i4Ret == BGP4_SUCCESS)
    {
        /* check if gateway is configured for the peer */
        u4GatewayFlag = BGP4_TRUE;
    }

    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGR_ROUTE) ==
        BGP4_RT_AGGR_ROUTE)
    {
        /* Self-Aggregated route */
        if (i4LocalAddrPresent == BGP4_FALSE)
        {
            /* Unable to find the corresponding local address */
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG,
                           BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Failure in finding the corresponding local address "
                           "for the peer.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            return BGP4_FAILURE;
        }
        BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                  "\tBgp4AttrFillNexthop() : Self-Aggr Rt\n");
        PTR_FETCH_4 (u4Nexthop,
                     BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalAddr.NetAddr));
    }
    /* confed peer */
    else if (BGP4_CONFED_PEER_STATUS (pPeerInfo) == BGP4_TRUE)
    {
        PTR_FETCH_4 (u4Nexthop, BGP4_INFO_NEXTHOP (pBgp4Info));
        /* if directly connected route OR route with nexthop as 0 */
        if (u4Nexthop == 0 ||
            (BGP4_PEER_SELF_NEXTHOP (pPeerInfo) == BGP4_ENABLE_NEXTHOP_SELF))
        {
            if (i4LocalAddrPresent == BGP4_FALSE)
            {
                /* Unable to find the corresponding local address */
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                               BGP4_TRC_FLAG,
                               BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : Failure in finding the corresponding local address "
                               "for the peer.\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerInfo))));
                return BGP4_FAILURE;
            }
            PTR_FETCH_4 (u4Nexthop,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalAddr.
                                                                NetAddr));
        }
    }
    else if (                    /* Always preserve nexthop for confed peers or when reflecting
                                 * routes to clients/non-clients.  */
                /* reflection by RR */
                ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerInfo), pPeerInfo)
                  == BGP4_INTERNAL_PEER) && (pSrcPeer != NULL)
                 && (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerInfo), pSrcPeer)
                     == BGP4_INTERNAL_PEER)))
    {
        PTR_FETCH_4 (u4Nexthop, BGP4_INFO_NEXTHOP (pBgp4Info));
        /* if directly connected route OR route with nexthop as 0 */
        if (u4Nexthop == 0)
        {
            if (i4LocalAddrPresent == BGP4_FALSE)
            {
                /* Unable to find the corresponding local address */
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                               BGP4_TRC_FLAG,
                               BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : Failure in finding the corresponding local address "
                               "when nexthop is zero.\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerInfo))));
                return BGP4_FAILURE;
            }
            PTR_FETCH_4 (u4Nexthop,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalAddr.
                                                                NetAddr));
        }
    }
    else if (BGP4_PEER_SELF_NEXTHOP (pPeerInfo) == BGP4_ENABLE_NEXTHOP_SELF)
    {
        if (i4LocalAddrPresent == BGP4_FALSE)
        {
            /* Unable to find the corresponding local address */
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG,
                           BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Failure in finding the corresponding local address "
                           "for the peer with nexthop self enabled.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            return BGP4_FAILURE;
        }
        PTR_FETCH_4 (u4Nexthop,
                     BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalAddr.NetAddr));
    }
    else
        /* if peer is external peer */
    if ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerInfo), pPeerInfo) ==
             BGP4_EXTERNAL_PEER)
            && (BGP4_CONFED_PEER_STATUS (pPeerInfo) == BGP4_FALSE))
    {
        /* if gateway is configured and is in the same subnet,advertise it */
        if ((u4GatewayFlag == BGP4_TRUE) &&
            (!((BGP4_PEER_EBGP_MULTIHOP (pPeerInfo) ==
                BGP4_EBGP_MULTI_HOP_DISABLE) &&
               (Bgp4IsOnSameSubnet (GatewayAddress, pPeerInfo) == FALSE))))
        {
            PTR_FETCH_4 (u4Nexthop,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                         (GatewayAddress));
        }
        else /* if peer is multi-hop enabled */
        if (BGP4_PEER_EBGP_MULTIHOP (pPeerInfo) == BGP4_EBGP_MULTI_HOP_ENABLE)
        {
            if (i4LocalAddrPresent == BGP4_FALSE)
            {
                /* Unable to find the corresponding local address */
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                               BGP4_TRC_FLAG,
                               BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : Failure in finding the corresponding local address "
                               "for the peer which has EBGP multihop enabled.\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerInfo))));
                return BGP4_FAILURE;
            }
            PTR_FETCH_4 (u4Nexthop,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalAddr.
                                                                NetAddr));
        }
        if (i4LocalAddrPresent == BGP4_FALSE)
        {
            /* Unable to find the corresponding local address */
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG,
                           BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Failure in finding the corresponding local address "
                           "for the peer.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            return BGP4_FAILURE;
        }
        PTR_FETCH_4 (u4Nexthop,
                     BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalAddr.NetAddr));

    }
    else                        /* if peer is internal */
    {
        /* Advertise configured gateway */
        if (u4GatewayFlag == BGP4_TRUE)
        {
            PTR_FETCH_4 (u4Nexthop,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                         (GatewayAddress));
        }
        else
        {
            PTR_FETCH_4 (u4Nexthop, BGP4_INFO_NEXTHOP (pBgp4Info));
            if ((Bgp4IsOnSameSubnet (BGP4_INFO_NEXTHOP_INFO (pBgp4Info),
                                     pPeerInfo))
                && (i4LocalAddrPresent == BGP4_TRUE)
                &&
                ((PrefixMatch
                  (BGP4_INFO_NEXTHOP_INFO (pBgp4Info),
                   BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo))) == BGP4_TRUE))
            {
                PTR_FETCH_4 (u4Nexthop,
                             BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalAddr.
                                                                    NetAddr));
            }

#ifdef L3VPN
            switch (u4AsafiMask)
            {
                case CAP_MP_VPN4_UNICAST:
                case CAP_MP_LABELLED_IPV4:
                    if ((!(BGP4_RT_LABEL_CNT (pRtProfile) > 0)) ||
                        (((BGP4_RT_LABEL_CNT (pRtProfile)) &&
                          Bgp4IsOnSameSubnet (BGP4_INFO_NEXTHOP_INFO
                                              (pBgp4Info),
                                              pPeerInfo) == FALSE)))
                    {
                        u4Nexthop = 0;
                    }
                    break;
                default:
                    break;
            }
#endif
            /* if directly connected route OR route with nexthop as 0 */
            if (u4Nexthop == 0)
            {
                if (i4LocalAddrPresent == BGP4_FALSE)
                {
                    /* Unable to find the corresponding local address */
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                                   BGP4_TRC_FLAG,
                                   BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : Failure in finding the corresponding local address "
                                   "when the nexthop is zero.\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerInfo),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerInfo))));
                    return BGP4_FAILURE;
                }
                PTR_FETCH_4 (u4Nexthop,
                             BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalAddr.
                                                                    NetAddr));
            }
        }
    }

    /* Update the NextHop Value of the Advertised BGP4-INFO */
    PTR_ASSIGN_4 (BGP4_INFO_NEXTHOP (pAdvtBgpInfo), u4Nexthop);
    BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_NEXT_HOP_MASK;
    BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                   BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                   "\tPEER %s : The Nexthop %s attribute is successfully filled.\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo))),
                   Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_INFO_NEXTHOP_INFO (pAdvtBgpInfo))));
    return BGP4_SUCCESS;

}

/*****************************************************************************/
/* Function Name : Bgp4AttrFillMed                                           */
/* Description   : This routine fills the MED information in the BGP4-INFO   */
/*                 to be advertised.                                         */
/* Input(s)      : Route profile which is going to be advertised             */
/*                 (pRtProfile),                                             */
/*                 Peer Information (pPeerInfo)                              */
/*                 Received MED Send Status (u4RecvMEDSend)                  */
/* Output(s)     : Updated BGP4-INFO to be advertised (pAdvtBgpInfo)         */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4AttrFillMed (tRouteProfile * pRtProfile, tBgp4Info * pAdvtBgpInfo,
                 tBgp4PeerEntry * pPeerInfo, tBgp4Info * pRtInfo,
                 UINT4 u4RecvMEDSend)
{
    UINT4               u4MedValue = BGP4_INV_MEDLP;

    u4MedValue = Bgp4ConfigMED (pPeerInfo, pRtProfile, BGP4_OUTGOING);

    if (u4MedValue == BGP4_INV_MEDLP)
    {
        /* While sending out, if nothing is configured in the table, then 
         * send the MED value used in the decision process depanding on
         * the RecvMEDSend Status. 
         */
        if (u4RecvMEDSend == BGP4_MED_RECV_BLOCK)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : No MED is configured in the med table. "
                           "And received MED send status is blocked.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            return BGP4_SUCCESS;
        }

        if ((pRtInfo != NULL)
            && ((BGP4_INFO_ATTR_FLAG (pRtInfo) & BGP4_ATTR_SEND_MED_MASK) ==
                BGP4_ATTR_SEND_MED_MASK))
        {
            u4MedValue = BGP4_INFO_SEND_MED (pRtInfo);
        }
        if (u4MedValue == BGP4_INV_MEDLP)
        {
            u4MedValue = BGP4_RT_MED (pRtProfile);
        }
        /* If u4MedValue is 0 and MED Attr is not received in the
         * update message then dont sent MED. Else send MED as 0.*/
        if ((u4MedValue == BGP4_INV_MEDLP) &&
            (((BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pRtProfile))) &
              BGP4_ATTR_MED_MASK) != BGP4_ATTR_MED_MASK))
        {
            return BGP4_SUCCESS;
        }
    }

    BGP4_INFO_RCVD_MED (pAdvtBgpInfo) = u4MedValue;
    BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_MED_MASK;
    BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                   BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                   "\tPEER %s : MED attribute is successfully filled with value %d.\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo))),
                   BGP4_INFO_RCVD_MED (pAdvtBgpInfo));
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AttrFillLp                                            */
/* Description   : This routine fills the LP information in the BGP4-INFO    */
/*                 to the advertised.                                        */
/* Input(s)      : Route profile which is going to be advertised             */
/*                 (pRtProfile),                                             */
/*                 Peer Information (pPeerInfo)                              */
/* Output(s)     : Updated BGP4-INFO to be advertised (pAdvtBgpInfo)         */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4AttrFillLp (tRouteProfile * pRtProfile, tBgp4Info * pAdvtBgpInfo,
                tBgp4PeerEntry * pPeerInfo)
{
    UINT4               u4LpValue = BGP4_INV_MEDLP;
    UINT4               u4PeerType;

    u4PeerType = BGP4_GET_PEER_TYPE (BGP4_RT_CXT_ID (pRtProfile), pPeerInfo);

    u4LpValue = Bgp4ConfigLP (pPeerInfo, pRtProfile, BGP4_OUTGOING);

    if ((u4LpValue != BGP4_INV_MEDLP) || (u4PeerType == BGP4_INTERNAL_PEER))
    {
        BGP4_INFO_RCVD_LOCAL_PREF (pAdvtBgpInfo) = u4LpValue;
        BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_LOCAL_PREF_MASK;
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Local Preference %d attribute is successfully filled.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))),
                       BGP4_INFO_RCVD_LOCAL_PREF (pAdvtBgpInfo));
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AttrFillAggr                                          */
/* Description   : This routine fills the Aggregator information in the      */
/*                 BGP4-INFO to be advertised.                               */
/* Input(s)      : Route profile which is going to be advertised             */
/*                 (pRtProfile),                                             */
/* Output(s)     : Updated BGP4-INFO to be advertised (pAdvtBgpInfo)         */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4AttrFillAggr (tRouteProfile * pRtProfile, tBgp4Info * pAdvtBgpInfo)
{
    tBgp4Info          *pBgp4Info = NULL;
    UINT4               u4Index = 0;

    pBgp4Info = BGP4_RT_BGP_INFO (pRtProfile);

    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGR_ROUTE) ==
        BGP4_RT_AGGR_ROUTE)
    {
        u4Index = Bgp4AggrFindAggrEntry (pRtProfile, u4Index);
        if (u4Index == 0)
        {
            BGP4_TRC_ARG1 (NULL,
                           BGP4_TRC_FLAG,
                           BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tNo Aggregate Entry present for the route %s.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            return BGP4_FAILURE;
        }
    }

    /* Filling the Atomic Aggregate field  */
    if (((BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
         && ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_ATOMIC_AGGR_MASK)
             == BGP4_ATTR_ATOMIC_AGGR_MASK))
        || ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGR_ROUTE) ==
            BGP4_RT_AGGR_ROUTE) ||
        ((BGP4_OVERLAP_ROUTE_POLICY (BGP4_RT_CXT_ID (pRtProfile)) ==
          BGP4_INSTALL_LESSSPEC_RT) && (u4Index != 0)))

    {
        if (((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_ATOMIC_AGGR_MASK) ==
             BGP4_ATTR_ATOMIC_AGGR_MASK) ||
            (((BGP4_AGGRENTRY_AS_SET (u4Index - BGP4_DECREMENT_BY_ONE))
              == AS_SET_DISABLE) && (BGP4_AGGRENTRY_AS_ATOMIC_AGGR
                                     (u4Index - BGP4_DECREMENT_BY_ONE) ==
                                     BGP4_TRUE)))
        {
            BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_ATOMIC_AGGR_MASK;
        }
    }

    /* Filling the Aggregator Field. */
    if (((BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
         && ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_AGGREGATOR_MASK)
             == BGP4_ATTR_AGGREGATOR_MASK))
        || ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGR_ROUTE) ==
            BGP4_RT_AGGR_ROUTE))
    {
        AGGR_NODE_CREATE (BGP4_INFO_AGGREGATOR (pAdvtBgpInfo));

        if (BGP4_INFO_AGGREGATOR (pAdvtBgpInfo) == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                      BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for Aggregator Attribute "
                      "FAILED!!!\n");
            gu4BgpDebugCnt[MAX_BGP_AGGR_NODE_SIZING_ID]++;
            return (BGP4_FAILURE);
        }

        if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGR_ROUTE) ==
            BGP4_RT_AGGR_ROUTE)
        {
            /* Fill the Aggregator-AS and Aggregator-Id fields. */
            BGP4_INFO_AGGREGATOR_AS (pAdvtBgpInfo) =
                (BGP4_CONFED_ID (BGP4_RT_CXT_ID (pRtProfile)) == BGP4_INV_AS) ?
                BGP4_LOCAL_AS_NO (BGP4_RT_CXT_ID (pRtProfile)) :
                BGP4_CONFED_ID (BGP4_RT_CXT_ID (pRtProfile));
            BGP4_INFO_AGGREGATOR_NODE (pAdvtBgpInfo) =
                BGP4_LOCAL_BGP_ID (BGP4_RT_CXT_ID (pRtProfile));
            BGP4_INFO_AGGREGATOR_FLAG (pAdvtBgpInfo) = 0;
        }
        else
        {
            /* Filling the Aggregator field  */
            BGP4_INFO_AGGREGATOR_NODE (pAdvtBgpInfo) =
                BGP4_INFO_AGGREGATOR_NODE (pBgp4Info);
            BGP4_INFO_AGGREGATOR_AS (pAdvtBgpInfo) =
                BGP4_INFO_AGGREGATOR_AS (pBgp4Info);
            BGP4_INFO_AGGREGATOR_FLAG (pAdvtBgpInfo) =
                BGP4_INFO_AGGREGATOR_FLAG (pBgp4Info);
        }
        BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_AGGREGATOR_MASK;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AttrUnknownLength                                     */
/* Description   : This routine estimates the number of bytes required to    */
/*                 fill the unknown field.                                   */
/* Input(s)      : BGP Information which contains the unknown field          */
/*                 (pBgp4Info)                                               */
/* Output(s)     : None.                                                     */
/* Return(s)     : Number of bytes required.                                 */
/*****************************************************************************/
UINT2
Bgp4AttrUnknownLength (tBgp4Info * pBgp4Info)
{
    UINT2               u2UnknownAttrLen = 0;

    BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\tBgp4AttrUnknownLength() :");
    if ((pBgp4Info != NULL) &&
        ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_UNKNOWN_MASK) ==
         BGP4_ATTR_UNKNOWN_MASK))
    {
        u2UnknownAttrLen = (UINT2) (BGP4_INFO_UNKNOWN_ATTR_LEN (pBgp4Info));
    }

    BGP4_DBG1 (BGP4_DBG_CTRL_FLOW, " Len. = %d\n", u2UnknownAttrLen);
    return (u2UnknownAttrLen);
}

/*****************************************************************************/
/* Function Name : Bgp4AttrFillUnknown                                       */
/* Description   : This routine fills the Unknown information in BGP4-INFO   */
/*                 to be advertised.                                         */
/* Input(s)      : BGP Info with received Path attributes (pRcvdBgp4Info)    */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4AttrFillUnknown (tBgp4Info * pRcvdBgp4Info, tBgp4Info * pAdvtBgpInfo)
{
    BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\tBgp4AttrFillUnknown() : Attr = \n");

    if ((BGP4_INFO_ATTR_FLAG (pRcvdBgp4Info) & BGP4_ATTR_UNKNOWN_MASK) ==
        BGP4_ATTR_UNKNOWN_MASK)
    {
        ATTRIBUTE_NODE_CREATE (BGP4_INFO_UNKNOWN_ATTR (pAdvtBgpInfo));
        if (BGP4_INFO_UNKNOWN_ATTR (pAdvtBgpInfo) == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for Filling Unknown Attribute "
                      "FAILED!!!\n");
            gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
            return BGP4_FAILURE;
        }
        BGP4_INFO_UNKNOWN_ATTR_LEN (pAdvtBgpInfo) =
            BGP4_INFO_UNKNOWN_ATTR_LEN (pRcvdBgp4Info);
        MEMCPY (BGP4_INFO_UNKNOWN_ATTR (pAdvtBgpInfo),
                BGP4_INFO_UNKNOWN_ATTR (pRcvdBgp4Info),
                BGP4_INFO_UNKNOWN_ATTR_LEN (pRcvdBgp4Info));
        BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_UNKNOWN_MASK;
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                  BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                  "\tUnknown attribute is successfully filled\n");
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AttrIsAspathIdentical                                 */
/* Description   : This routine checks whether the AS PATH is identical in   */
/*                 the given two BGP informations.                           */
/* Input(s)      : BGP Informations (pBgp4Info1, pBgp4Info2)                 */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRUE if they are identical,                               */
/*                 FALSE if they are not.                                    */
/*****************************************************************************/
BOOL1
Bgp4AttrIsAspathIdentical (tBgp4Info * pBgp4Info1, tBgp4Info * pBgp4Info2)
{
    tAsPath            *pAspath1 = NULL;
    tAsPath            *pAspath2 = NULL;

    pAspath1 = (tAsPath *) TMO_SLL_First (BGP4_INFO_ASPATH (pBgp4Info1));
    pAspath2 = (tAsPath *) TMO_SLL_First (BGP4_INFO_ASPATH (pBgp4Info2));

    while ((pAspath1 != NULL) && (pAspath2 != NULL))
    {
        if (BGP4_ASPATH_TYPE (pAspath1) != BGP4_ASPATH_TYPE (pAspath2))
        {
            return FALSE;
        }

        if (BGP4_ASPATH_LEN (pAspath1) != BGP4_ASPATH_LEN (pAspath2))
        {
            return FALSE;
        }

        if (MEMCMP (BGP4_ASPATH_NOS (pAspath1),
                    BGP4_ASPATH_NOS (pAspath2),
                    (BGP4_ASPATH_LEN (pAspath1) * BGP4_AS4_LENGTH)) != 0)
        {
            return FALSE;
        }

        pAspath1 = (tAsPath *) TMO_SLL_Next (BGP4_INFO_ASPATH (pBgp4Info1),
                                             &pAspath1->sllNode);
        pAspath2 = (tAsPath *) TMO_SLL_Next (BGP4_INFO_ASPATH (pBgp4Info2),
                                             &pAspath2->sllNode);
    }

    /* Paths are identical. Entire ASPATH has been checked. */
    if ((pAspath1 == NULL) && (pAspath2 == NULL))
    {
        return TRUE;
    }

    return FALSE;
}

/*****************************************************************************/
/* Function Name : Bgp4AttrIsSNPAIdentical                                   */
/* Description   : This routine checks whether the SNPAINFO is identical in  */
/*                 the given two BGP informations.                           */
/* Input(s)      : BGP Informations (pBgp4Info1, pBgp4Info2)                 */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRUE if they are identical,                               */
/*                 FALSE if they are not.                                    */
/*****************************************************************************/
BOOL1
Bgp4AttrIsSNPAIdentical (tBgp4Info * pBgp4Info1, tBgp4Info * pBgp4Info2)
{
    tSnpaInfoNode      *pSnpaInfo1 = NULL;
    tSnpaInfoNode      *pSnpaInfo2 = NULL;

    if (TMO_SLL_Count (BGP4_MPE_SNPA_INFO_LIST (pBgp4Info1)) !=
        TMO_SLL_Count (BGP4_MPE_SNPA_INFO_LIST (pBgp4Info2)))
    {
        return FALSE;
    }

    if (BGP4_MPE_SNPA_INFO_LIST (pBgp4Info1) ==
        BGP4_MPE_SNPA_INFO_LIST (pBgp4Info2))
    {
        /* Either both SNPA-INFO are NULL or pointing to the same
         * pointer. So in any case, the SNPA Info are identical. */
        return TRUE;
    }

    pSnpaInfo1 =
        (tSnpaInfoNode *) TMO_SLL_First (BGP4_MPE_SNPA_INFO_LIST (pBgp4Info1));
    pSnpaInfo2 =
        (tSnpaInfoNode *) TMO_SLL_First (BGP4_MPE_SNPA_INFO_LIST (pBgp4Info2));

    while ((pSnpaInfo1 != NULL) && (pSnpaInfo2 != NULL))
    {
        if (BGP4_MPE_SNPA_NODE_LEN (pSnpaInfo1) !=
            BGP4_MPE_SNPA_NODE_LEN (pSnpaInfo2))
        {
            return FALSE;
        }

        if (MEMCMP (BGP4_MPE_SNPA_NODE_INFO (pSnpaInfo1),
                    BGP4_MPE_SNPA_NODE_INFO (pSnpaInfo2),
                    (BGP4_MPE_SNPA_NODE_LEN (pSnpaInfo1))) != 0)
        {
            return FALSE;
        }

        pSnpaInfo1 = (tSnpaInfoNode *)
            TMO_SLL_Next (BGP4_MPE_SNPA_INFO_LIST (pBgp4Info1),
                          &pSnpaInfo1->TSNextSNPA);
        pSnpaInfo2 = (tSnpaInfoNode *)
            TMO_SLL_Next (BGP4_MPE_SNPA_INFO_LIST (pBgp4Info2),
                          &pSnpaInfo2->TSNextSNPA);
    }

    /* SNPA-INFO are identical. Entire SNPA-INFO has been checked. */
    if ((pSnpaInfo1 == NULL) && (pSnpaInfo2 == NULL))
    {
        return TRUE;
    }

    return FALSE;
}

/*****************************************************************************/
/* Function Name : Bgp4AttrGetfirstAsno                                      */
/* Description   : This routine returns the first AS number present in the   */
/*                 AS_PATH information present.                              */
/* Input(s)      : BGP Information (pBgp4Info)                               */
/* Output(s)     : None.                                                     */
/* Return(s)     : First AS number present.                                  */
/* Note          : This module is called by the Decision process.            */
/*****************************************************************************/
UINT2
Bgp4AttrGetfirstAsno (tBgp4Info * pBgp4Info)
{
    tAsPath            *pAspath = NULL;
    UINT2               u2FirstAS = 0;

    pAspath = (tAsPath *) TMO_SLL_First (BGP4_INFO_ASPATH (pBgp4Info));

    if (pAspath != NULL)
    {
        if (BGP4_ASPATH_TYPE (pAspath) == BGP4_ATTR_PATH_SEQUENCE)
        {
            if (BGP4_ASPATH_LEN (pAspath) > 0)
            {
                PTR_FETCH2 (u2FirstAS, BGP4_ASPATH_NOS (pAspath));
            }
        }
    }
    return u2FirstAS;
}

/*****************************************************************************/
/* Function Name : Bgp4AttrIsBgpinfosIdentical                               */
/* Description   : This routine checks for the identity of the given two     */
/*                 BGP4 informations.                                        */
/* Input(s)      : BGP informations (pBgp4Info1, pBgp4Info2)                 */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRUE if the informations are identical,                   */
/*                 FALSE otherwise.                                          */
/*****************************************************************************/
BOOL1
Bgp4AttrIsBgpinfosIdentical (tBgp4Info * pBgp4Info1, tBgp4Info * pBgp4Info2)
{
    UINT4               u4UnknownInfo1;
    UINT4               u4UnknownInfo2;

    if ((pBgp4Info1 == NULL) || (pBgp4Info2 == NULL))
    {
        return FALSE;
    }

    if (BGP4_INFO_ATTR_FLAG (pBgp4Info1) != BGP4_INFO_ATTR_FLAG (pBgp4Info2))
    {
        return FALSE;
    }

    if (BGP4_INFO_ORIGIN (pBgp4Info1) != BGP4_INFO_ORIGIN (pBgp4Info2))
    {
        return FALSE;
    }

    if (Bgp4AttrIsAspathIdentical (pBgp4Info1, pBgp4Info2) == FALSE)
    {
        return FALSE;
    }

    if ((PrefixMatch (BGP4_INFO_NEXTHOP_INFO (pBgp4Info1),
                      BGP4_INFO_NEXTHOP_INFO (pBgp4Info2))) != BGP4_TRUE)
    {
        return FALSE;
    }

    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info1) & BGP4_ATTR_MED_MASK) ==
        BGP4_ATTR_MED_MASK)
    {
        if (BGP4_INFO_RCVD_MED (pBgp4Info1) != BGP4_INFO_RCVD_MED (pBgp4Info2))
        {
            return FALSE;
        }
    }

    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info1) & BGP4_ATTR_LOCAL_PREF_MASK) ==
        BGP4_ATTR_LOCAL_PREF_MASK)
    {
        if (BGP4_INFO_RCVD_LOCAL_PREF (pBgp4Info1)
            != BGP4_INFO_RCVD_LOCAL_PREF (pBgp4Info2))
        {
            return FALSE;
        }
    }

    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info1) & BGP4_ATTR_AGGREGATOR_MASK) !=
        (BGP4_INFO_ATTR_FLAG (pBgp4Info2) & BGP4_ATTR_AGGREGATOR_MASK))
    {
        return FALSE;
    }

    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info1) & BGP4_ATTR_AGGREGATOR_MASK) ==
        BGP4_ATTR_AGGREGATOR_MASK)
    {
        if (BGP4_INFO_AGGREGATOR_AS (pBgp4Info1)
            != BGP4_INFO_AGGREGATOR_AS (pBgp4Info2))
        {
            return FALSE;
        }

        if (BGP4_INFO_AGGREGATOR_NODE (pBgp4Info1)
            != BGP4_INFO_AGGREGATOR_NODE (pBgp4Info2))
        {
            return FALSE;
        }
        if (BGP4_INFO_AGGREGATOR_FLAG (pBgp4Info1)
            != BGP4_INFO_AGGREGATOR_FLAG (pBgp4Info2))
        {
            return FALSE;
        }
    }

    if (((BGP4_INFO_ATTR_FLAG (pBgp4Info1) & BGP4_ATTR_UNKNOWN_MASK)
         == BGP4_ATTR_UNKNOWN_MASK) &&
        ((BGP4_INFO_ATTR_FLAG (pBgp4Info2) & BGP4_ATTR_UNKNOWN_MASK)
         == BGP4_ATTR_UNKNOWN_MASK))
    {
        u4UnknownInfo1 = BGP4_INFO_UNKNOWN_ATTR_LEN (pBgp4Info1);
        u4UnknownInfo2 = BGP4_INFO_UNKNOWN_ATTR_LEN (pBgp4Info2);

        if (u4UnknownInfo1 != u4UnknownInfo2)
        {
            return FALSE;
        }
        /* Now compare the BGP Unknown information */
        if ((MEMCMP (BGP4_INFO_UNKNOWN_ATTR (pBgp4Info1),
                     BGP4_INFO_UNKNOWN_ATTR (pBgp4Info2), u4UnknownInfo2)) != 0)
        {
            return FALSE;
        }
    }

    /* Community Attribute */
    if ((BGP4_INFO_COMM_ATTR (pBgp4Info1) != NULL) &&
        (BGP4_INFO_COMM_ATTR (pBgp4Info2) != NULL))
    {
        if ((BGP4_INFO_COMM_ATTR_FLAG (pBgp4Info1) !=
             BGP4_INFO_COMM_ATTR_FLAG (pBgp4Info2)) ||
            (BGP4_INFO_COMM_COUNT (pBgp4Info1) !=
             BGP4_INFO_COMM_COUNT (pBgp4Info2)))
        {
            return FALSE;
        }
        if (MEMCMP (BGP4_INFO_COMM_ATTR_VAL (pBgp4Info1),
                    BGP4_INFO_COMM_ATTR_VAL (pBgp4Info2),
                    (BGP4_INFO_COMM_COUNT (pBgp4Info1) * COMM_VALUE_LEN)) != 0)
        {
            return FALSE;
        }
    }

    /* Extended Community Attribute. */
    if ((BGP4_INFO_ECOMM_ATTR (pBgp4Info1) != NULL) &&
        (BGP4_INFO_ECOMM_ATTR (pBgp4Info2) != NULL))
    {
        if ((BGP4_INFO_ECOMM_ATTR_FLAG (pBgp4Info1) !=
             BGP4_INFO_ECOMM_ATTR_FLAG (pBgp4Info2)) ||
            (BGP4_INFO_ECOMM_COUNT (pBgp4Info1) !=
             BGP4_INFO_ECOMM_COUNT (pBgp4Info2)))
        {
            return FALSE;
        }
        if (MEMCMP (BGP4_INFO_ECOMM_ATTR_VAL (pBgp4Info1),
                    BGP4_INFO_ECOMM_ATTR_VAL (pBgp4Info2),
                    (BGP4_INFO_ECOMM_COUNT (pBgp4Info1) *
                     EXT_COMM_VALUE_LEN)) != 0)
        {
            return FALSE;
        }
    }

    /* Originator-Id Attribute. */
    if (BGP4_INFO_ORIG_ID (pBgp4Info1) != BGP4_INFO_ORIG_ID (pBgp4Info2))
    {
        return FALSE;
    }

    /* Cluster-List Attribute */
    if ((BGP4_INFO_CLUS_LIST_ATTR (pBgp4Info1) != NULL) &&
        (BGP4_INFO_CLUS_LIST_ATTR (pBgp4Info2) != NULL))
    {
        if ((BGP4_INFO_CLUS_LIST_ATTR_FLAG (pBgp4Info1) !=
             BGP4_INFO_CLUS_LIST_ATTR_FLAG (pBgp4Info2)) ||
            (BGP4_INFO_CLUS_LIST_COUNT (pBgp4Info1) !=
             BGP4_INFO_CLUS_LIST_COUNT (pBgp4Info2)))
        {
            return FALSE;
        }
        if (MEMCMP (BGP4_INFO_CLUS_LIST_ATTR_VAL (pBgp4Info1),
                    BGP4_INFO_CLUS_LIST_ATTR_VAL (pBgp4Info2),
                    (BGP4_INFO_CLUS_LIST_COUNT (pBgp4Info1) *
                     CLUSTER_ID_LENGTH)) != 0)
        {
            return FALSE;
        }
    }

#ifdef BGP4_IPV6_WANTED
    if (BGP4_INFO_LINK_LOCAL_PRESENT (pBgp4Info1) !=
        BGP4_INFO_LINK_LOCAL_PRESENT (pBgp4Info2))
    {
        return FALSE;
    }

    if (BGP4_INFO_LINK_LOCAL_PRESENT (pBgp4Info1) == BGP4_TRUE)
    {
        if ((PrefixMatch (BGP4_INFO_LINK_LOCAL_ADDR_INFO (pBgp4Info1),
                          BGP4_INFO_LINK_LOCAL_ADDR_INFO (pBgp4Info2)))
            != BGP4_TRUE)
        {
            return FALSE;
        }
    }
#endif

#ifndef VPLSADS_WANTED
    if (Bgp4AttrIsSNPAIdentical (pBgp4Info1, pBgp4Info2) == FALSE)
    {
        return FALSE;
    }
#endif
    return TRUE;
}

/*****************************************************************************/
/* Function Name : Bgp4AttrIsBgpinfosIdenticalImport                         */
/* Description   : This routine checks for the identity of the given two     */
/*                 BGP4 informations.                                        */
/* Input(s)      : BGP informations (pBgp4Info1, pBgp4Info2)                 */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRUE if the informations are identical,                   */
/*                 FALSE otherwise.                                          */
/*****************************************************************************/
BOOL1
Bgp4AttrIsBgpinfosIdenticalImport (tBgp4Info * pBgp4Info1,
                                   tBgp4Info * pBgp4Info2,
                                   UINT1 u1ExistingRibAdvFlag,
                                   UINT1 u1IncomingPrefixAdvFlag)
{
    UINT4               u4UnknownInfo1;
    UINT4               u4UnknownInfo2;
    UINT4               u4RtAttrFlag1 = 0;
    UINT4               u4RtAttrFlag2 = 0;

    if ((pBgp4Info1 == NULL) || (pBgp4Info2 == NULL))
    {
        return FALSE;
    }

    u4RtAttrFlag1 = (BGP4_INFO_ATTR_FLAG (pBgp4Info1) & (~BGP4_ATTR_COMM_MASK));
    u4RtAttrFlag2 = (BGP4_INFO_ATTR_FLAG (pBgp4Info2) & (~BGP4_ATTR_COMM_MASK));

    if (u4RtAttrFlag1 != u4RtAttrFlag2)
    {
        return FALSE;
    }

    if (BGP4_INFO_ORIGIN (pBgp4Info1) != BGP4_INFO_ORIGIN (pBgp4Info2))
    {
        return FALSE;
    }

    if (Bgp4AttrIsAspathIdentical (pBgp4Info1, pBgp4Info2) == FALSE)
    {
        return FALSE;
    }
    if ((PrefixMatch (BGP4_INFO_NEXTHOP_INFO (pBgp4Info1),
                      BGP4_INFO_NEXTHOP_INFO (pBgp4Info2))) != BGP4_TRUE)
    {
        return FALSE;
    }
    if (((u1ExistingRibAdvFlag & BGP4_REDIST_ADV_ROUTE) ==
         BGP4_REDIST_ADV_ROUTE)
        && ((u1ExistingRibAdvFlag & BGP4_NETWORK_ADV_ROUTE) !=
            BGP4_NETWORK_ADV_ROUTE))
    {
        if (((u1IncomingPrefixAdvFlag & BGP4_REDIST_ADV_ROUTE) ==
             BGP4_REDIST_ADV_ROUTE)
            && ((u1IncomingPrefixAdvFlag & BGP4_NETWORK_ADV_ROUTE) !=
                BGP4_NETWORK_ADV_ROUTE))

        {
            if ((BGP4_INFO_ATTR_FLAG (pBgp4Info1) & BGP4_ATTR_MED_MASK) ==
                BGP4_ATTR_MED_MASK)
            {

                if (BGP4_INFO_RCVD_MED (pBgp4Info1) !=
                    BGP4_INFO_RCVD_MED (pBgp4Info2))
                {
                    return FALSE;
                }
            }
            if ((BGP4_INFO_ATTR_FLAG (pBgp4Info1) & BGP4_ATTR_LOCAL_PREF_MASK)
                == BGP4_ATTR_LOCAL_PREF_MASK)
            {
                if (BGP4_INFO_RCVD_LOCAL_PREF (pBgp4Info1)
                    != BGP4_INFO_RCVD_LOCAL_PREF (pBgp4Info2))
                {
                    return FALSE;
                }
            }
        }
    }

    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info1) & BGP4_ATTR_AGGREGATOR_MASK) !=
        (BGP4_INFO_ATTR_FLAG (pBgp4Info2) & BGP4_ATTR_AGGREGATOR_MASK))
    {
        return FALSE;
    }

    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info1) & BGP4_ATTR_AGGREGATOR_MASK) ==
        BGP4_ATTR_AGGREGATOR_MASK)
    {
        if (BGP4_INFO_AGGREGATOR_AS (pBgp4Info1)
            != BGP4_INFO_AGGREGATOR_AS (pBgp4Info2))
        {
            return FALSE;
        }

        if (BGP4_INFO_AGGREGATOR_NODE (pBgp4Info1)
            != BGP4_INFO_AGGREGATOR_NODE (pBgp4Info2))
        {
            return FALSE;
        }
        if (BGP4_INFO_AGGREGATOR_FLAG (pBgp4Info1)
            != BGP4_INFO_AGGREGATOR_FLAG (pBgp4Info2))
        {
            return FALSE;
        }
    }

    if (((BGP4_INFO_ATTR_FLAG (pBgp4Info1) & BGP4_ATTR_UNKNOWN_MASK)
         == BGP4_ATTR_UNKNOWN_MASK) &&
        ((BGP4_INFO_ATTR_FLAG (pBgp4Info2) & BGP4_ATTR_UNKNOWN_MASK)
         == BGP4_ATTR_UNKNOWN_MASK))
    {
        u4UnknownInfo1 = BGP4_INFO_UNKNOWN_ATTR_LEN (pBgp4Info1);
        u4UnknownInfo2 = BGP4_INFO_UNKNOWN_ATTR_LEN (pBgp4Info2);

        if (u4UnknownInfo1 != u4UnknownInfo2)
        {
            return FALSE;
        }
        /* Now compare the BGP Unknown information */
        if ((MEMCMP (BGP4_INFO_UNKNOWN_ATTR (pBgp4Info1),
                     BGP4_INFO_UNKNOWN_ATTR (pBgp4Info2), u4UnknownInfo2)) != 0)
        {
            return FALSE;
        }
    }
    /* Extended Community Attribute. */
    if ((BGP4_INFO_ECOMM_ATTR (pBgp4Info1) != NULL) &&
        (BGP4_INFO_ECOMM_ATTR (pBgp4Info2) != NULL))
    {
        if ((BGP4_INFO_ECOMM_ATTR_FLAG (pBgp4Info1) !=
             BGP4_INFO_ECOMM_ATTR_FLAG (pBgp4Info2)) ||
            (BGP4_INFO_ECOMM_COUNT (pBgp4Info1) !=
             BGP4_INFO_ECOMM_COUNT (pBgp4Info2)))
        {
            return FALSE;
        }
        if (MEMCMP (BGP4_INFO_ECOMM_ATTR_VAL (pBgp4Info1),
                    BGP4_INFO_ECOMM_ATTR_VAL (pBgp4Info2),
                    (BGP4_INFO_ECOMM_COUNT (pBgp4Info1) *
                     EXT_COMM_VALUE_LEN)) != 0)
        {
            return FALSE;
        }
    }

    /* Originator-Id Attribute. */
    if (BGP4_INFO_ORIG_ID (pBgp4Info1) != BGP4_INFO_ORIG_ID (pBgp4Info2))
    {
        return FALSE;
    }

    /* Cluster-List Attribute */
    if ((BGP4_INFO_CLUS_LIST_ATTR (pBgp4Info1) != NULL) &&
        (BGP4_INFO_CLUS_LIST_ATTR (pBgp4Info2) != NULL))
    {
        if ((BGP4_INFO_CLUS_LIST_ATTR_FLAG (pBgp4Info1) !=
             BGP4_INFO_CLUS_LIST_ATTR_FLAG (pBgp4Info2)) ||
            (BGP4_INFO_CLUS_LIST_COUNT (pBgp4Info1) !=
             BGP4_INFO_CLUS_LIST_COUNT (pBgp4Info2)))
        {
            return FALSE;
        }
        if (MEMCMP (BGP4_INFO_CLUS_LIST_ATTR_VAL (pBgp4Info1),
                    BGP4_INFO_CLUS_LIST_ATTR_VAL (pBgp4Info2),
                    (BGP4_INFO_CLUS_LIST_COUNT (pBgp4Info1) *
                     CLUSTER_ID_LENGTH)) != 0)
        {
            return FALSE;
        }
    }

#ifdef BGP4_IPV6_WANTED
    if (BGP4_INFO_LINK_LOCAL_PRESENT (pBgp4Info1) !=
        BGP4_INFO_LINK_LOCAL_PRESENT (pBgp4Info2))
    {
        return FALSE;
    }

    if (BGP4_INFO_LINK_LOCAL_PRESENT (pBgp4Info1) == BGP4_TRUE)
    {
        if ((PrefixMatch (BGP4_INFO_LINK_LOCAL_ADDR_INFO (pBgp4Info1),
                          BGP4_INFO_LINK_LOCAL_ADDR_INFO (pBgp4Info2)))
            != BGP4_TRUE)
        {
            return FALSE;
        }
    }
#endif

#ifndef VPLSADS_WANTED
    if (Bgp4AttrIsSNPAIdentical (pBgp4Info1, pBgp4Info2) == FALSE)
    {
        return FALSE;
    }
#endif
    return TRUE;
}

#ifdef L3VPN
/*****************************************************************************/
/* Function Name : Bgp4IsSameBgpLabel                                       */
/* Description   : This routine checks for the identity of the given two     */
/*                 BGP4 informations.                                        */
/* Input(s)      : BGP informations (pBgp4Info1, pBgp4Info2)                 */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRUE if the informations are identical,                   */
/*                 FALSE otherwise.                                          */
/*****************************************************************************/
BOOL1
Bgp4IsSameBgpLabel (tRouteProfile * pRtProfile1, tRouteProfile * pRtProfile2)
{
    if ((!BGP4_RT_LABEL_CNT (pRtProfile1)) &&
        (!BGP4_RT_LABEL_CNT (pRtProfile2)))
    {
        return TRUE;
    }
    if (BGP4_RT_LABEL_CNT (pRtProfile1) != BGP4_RT_LABEL_CNT (pRtProfile2))
    {
        return FALSE;
    }
    if ((BGP4_RT_LABEL_INFO (pRtProfile1)) !=
        (BGP4_RT_LABEL_INFO (pRtProfile2)))
    {
        return FALSE;
    }
    return TRUE;

}
#endif

/**************************************************************************
 Function Name   : Bgp4GetUnknownBgpInfo
 Description     : Extract all the unknown BGP Path Attribute information
                   from the input route profile.
 Input(s)        : pRouteInfo  - pointer to input route profile
 Output(s)       : pUnknown - pointer to the unknown attribute information
                   pu2UnknownLen - Unknown Attribute Length
 Global Variables Referred :None
 Global variables Modified :None
 Exceptions or Operating System Error Handling : None
 Use of Recursion          : None.
 Returns                   :  BGP4_SUCCESS or BGP4_FAILURE
**************************************************************************/
INT4
Bgp4GetUnknownBgpInfo (const tRouteProfile * pRouteInfo,
                       UINT1 **pUnknown, UINT2 *pu2UnknownLen)
{
    tBgp4Info          *pRtInfo = NULL;

    if (BGP4_RT_PROTOCOL (pRouteInfo) != BGP_ID)
    {
        /* Unknown info not present. */
        return BGP4_FAILURE;
    }

    /* This information presents in pRtInfo field */
    pRtInfo = BGP4_RT_BGP_INFO (pRouteInfo);
    if (BGP4_INFO_UNKNOWN_ATTR (pRtInfo) != NULL)
    {
        *pUnknown = BGP4_INFO_UNKNOWN_ATTR (pRtInfo);
        *pu2UnknownLen = (UINT2) BGP4_INFO_UNKNOWN_ATTR_LEN (pRtInfo);
        return RFL_SUCCESS;
    }
    else
    {
        *pUnknown = NULL;
        *pu2UnknownLen = 0;
        return RFL_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name : Bgp4AttrGetUnknown                                        */
/* Description   : This routine gets the Unknown information in the buffer.  */
/* Input(s)      : RtProfile - route profile information.                    */
/*                 Buffer in which Unknown is going to be filled (pu1MsgBuf) */
/*                 Peer Information (pPeerInfo)                              */
/* Output(s)     : pu1MsgBuf - pointer to the unknown information            */
/* Return(s)     : Number of bytes present for Unknown information           */
/*****************************************************************************/
UINT2
Bgp4AttrGetUnknown (tRouteProfile * pRtProfile, UINT1 *pu1MsgBuf)
{
    UINT1              *pu1UnknownStr = NULL;
    tBgp4Info          *pBgp4Info = NULL;
    UINT2               u2BytesFilled = 0;
    UINT2               u2BytesProcessed = 0;
    UINT2               u2UnknownLen = 0;
    UINT2               u2AttrLen = 0;

    BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\tBgp4AttrGetUnknown() : Attr = \n");

    pBgp4Info = BGP4_RT_BGP_INFO (pRtProfile);
    if ((pBgp4Info != NULL) &&
        ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_UNKNOWN_MASK) ==
         BGP4_ATTR_UNKNOWN_MASK))
    {
        pu1UnknownStr = BGP4_INFO_UNKNOWN_ATTR (pBgp4Info);
        u2UnknownLen = (UINT2) BGP4_INFO_UNKNOWN_ATTR_LEN (pBgp4Info);

        while (u2BytesProcessed < u2UnknownLen)
        {
            if (((*pu1UnknownStr) & BGP4_EXT_LEN_FLAG_MASK) ==
                BGP4_EXT_LEN_FLAG_MASK)
            {
                PTR_FETCH2 (u2AttrLen, (pu1UnknownStr + BGP4_ATYPE_FLAGS_LEN +
                                        BGP4_ATYPE_CODE_LEN));

                if (((*pu1UnknownStr) & BGP4_TRANSITIVE_FLAG_MASK) ==
                    BGP4_TRANSITIVE_FLAG_MASK)
                {
                    BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                              "\tBgp4AttrFillUnknown() : transitive flag "
                              "is set\n");
                    MEMCPY (pu1MsgBuf, pu1UnknownStr,
                            (u2AttrLen + BGP4_ATYPE_FLAGS_LEN +
                             BGP4_ATYPE_CODE_LEN +
                             BGP4_EXTENDED_ATTR_LEN_SIZE));
                    (*pu1MsgBuf) |= BGP4_PARTIAL_FLAG_MASK;
                    u2BytesFilled += (u2AttrLen + BGP4_ATYPE_FLAGS_LEN +
                                      BGP4_ATYPE_CODE_LEN +
                                      BGP4_EXTENDED_ATTR_LEN_SIZE);
                    pu1MsgBuf += (u2AttrLen + BGP4_ATYPE_FLAGS_LEN +
                                  BGP4_ATYPE_CODE_LEN +
                                  BGP4_EXTENDED_ATTR_LEN_SIZE);
                }
                else
                {
                    BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                              "\tBgp4AttrFillUnknown() : Transitive Flag "
                              "is not set\n");
                }
                pu1UnknownStr += (u2AttrLen + BGP4_ATYPE_FLAGS_LEN +
                                  BGP4_ATYPE_CODE_LEN +
                                  BGP4_EXTENDED_ATTR_LEN_SIZE);
                u2BytesProcessed += (u2AttrLen + BGP4_ATYPE_FLAGS_LEN +
                                     BGP4_ATYPE_CODE_LEN +
                                     BGP4_NORMAL_ATTR_LEN_SIZE);
            }
            else
            {
                u2AttrLen = *(UINT1 *) (pu1UnknownStr + BGP4_ATYPE_FLAGS_LEN +
                                        BGP4_ATYPE_CODE_LEN);
                BGP4_DBG1 (BGP4_DBG_CTRL_FLOW,
                           "\tBgp4AttrFillUnknown() : Attr_len = %d\n",
                           u2AttrLen);
                if (((*pu1UnknownStr) & BGP4_TRANSITIVE_FLAG_MASK) ==
                    BGP4_TRANSITIVE_FLAG_MASK)
                {
                    BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                              "\tBgp4AttrFillUnknown() : transitive flag "
                              "is set\n");
                    MEMCPY (pu1MsgBuf, pu1UnknownStr,
                            (u2AttrLen + BGP4_ATYPE_FLAGS_LEN +
                             BGP4_ATYPE_CODE_LEN + BGP4_NORMAL_ATTR_LEN_SIZE));
                    (*pu1MsgBuf) |= BGP4_PARTIAL_FLAG_MASK;
                    u2BytesFilled += (u2AttrLen + BGP4_ATYPE_FLAGS_LEN +
                                      BGP4_ATYPE_CODE_LEN +
                                      BGP4_NORMAL_ATTR_LEN_SIZE);
                    pu1MsgBuf += (u2AttrLen + BGP4_ATYPE_FLAGS_LEN +
                                  BGP4_ATYPE_CODE_LEN +
                                  BGP4_NORMAL_ATTR_LEN_SIZE);
                }
                else
                {
                    BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                              "\tBgp4AttrFillUnknown() : transitive flag "
                              "is not set\n");
                }
                pu1UnknownStr += (u2AttrLen + BGP4_ATYPE_FLAGS_LEN +
                                  BGP4_ATYPE_CODE_LEN +
                                  BGP4_NORMAL_ATTR_LEN_SIZE);
                u2BytesProcessed += (u2AttrLen + BGP4_ATYPE_FLAGS_LEN +
                                     BGP4_ATYPE_CODE_LEN +
                                     BGP4_NORMAL_ATTR_LEN_SIZE);
            }
        }
    }
    return u2BytesFilled;
}

/*****************************************************************************/
/* Function Name : Bgp4IsRouteInList                                         */
/* Description   : This routine verifies whether the given route is present  */
/*                  in the given list                                        */
/* Input(s)      : pList - Given list containing routes.                     */
/*                 pRoute - Route to be searched                             */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
PRIVATE INT4
Bgp4IsRouteInList (tBGP4_DLL * pList, tRouteProfile * pRoute)
{
    tRouteProfile      *pNext;

    pNext = BGP4_DLL_FIRST_ROUTE (pList);
    while (pNext != NULL)
    {
        if (pNext == pRoute)
        {
            return BGP4_SUCCESS;
        }
        pNext = BGP4_RT_IDENT_ATTR_LINK_NEXT (pNext);
    }
    return BGP4_FAILURE;
}

#endif /* BGP4ATTR_C */
