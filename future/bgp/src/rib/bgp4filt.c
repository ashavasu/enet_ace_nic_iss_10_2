
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4filt.c,v 1.31 2017/09/15 06:19:55 siva Exp $
 *
 * Description: Contains the list of functions which does filtering, 
 *              MED and LP computation.
 *
 *******************************************************************/

#ifndef BGP4FILTER_C
#define BGP4FILTER_C

#include  "bgp4com.h"

/*****************************************************************************/
/* Function Name : Bgp4UpdateFilter                                          */
/* Description   : This function checks using the configured Filter Table,   */
/*                 so that the specified route profile can be advertised to  */
/*                 the given peer or not.                                    */
/* Input(s)      : Peer information to which route is going to be advt.      */
/*                 (pPeer),                                                  */
/*                 Route profile which is going to be advertised             */
/*                 (pRtProfile),                                             */
/*                 Direction of filtering (u4Dir)                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_ALLOW if the route can be advertised,                */
/*                 BGP4_DENY if it cannot.                                   */
/*****************************************************************************/
UINT4
Bgp4UpdateFilter (tBgp4PeerEntry * pPeer,
                  tRouteProfile * pRtProfile, UINT4 u4Dir)
{
    tBgp4OrfEntry       OrfEntry;
    tBgp4OrfEntry      *pOrfEntry = NULL;
    INT4                i4FilterTblIndex = 0;
    INT4                i4Action = BGP4_ALLOW;
#ifdef ROUTEMAP_WANTED
    tFilteringRMap     *pFilterRMap = NULL;
    UINT1               u1PrefixListPresent = BGP4_FALSE;
#endif /* ROUTEMAP_WANTED */

    MEMSET (&OrfEntry, 0, sizeof (tBgp4OrfEntry));

    for (i4FilterTblIndex = 0;
         i4FilterTblIndex < BGP4_MAX_UPDFILTER_ENTRIES; i4FilterTblIndex++)
    {
        if ((BGP4_FILTER_ENTRY_ADMIN (i4FilterTblIndex) == BGP4_INVALID)
            || (BGP4_FILTER_ENTRY_ADMIN (i4FilterTblIndex) == BGP4_ADMIN_DOWN))
        {
            continue;
        }
        if (BGP4_FILTER_ENTRY_VRFID (i4FilterTblIndex) !=
            BGP4_PEER_CXT_ID (pPeer))
        {
            continue;
        }

        BGP4_DBG (BGP4_DBG_ENTRY, "\tBgp4UpdateFilter() : .......\n ");

        if ((BGP4_FILTER_ENTRY[i4FilterTblIndex]).u1Dir == (UINT1) u4Dir)
        {
            if (Bgp4IsThisMatching (pPeer, pRtProfile, u4Dir,
                                    BGP4_FILTER_ENTRY[i4FilterTblIndex]) ==
                TRUE)
            {
                BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                               BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_FDB_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Filter entry found for the route %s.\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeer))),
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)));
                i4Action = BGP4_FILTER_ENTRY_ACTION (i4FilterTblIndex);
                if (i4Action == BGP4_DENY)
                {
                    BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_FDB_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s : Filter action is deny for the route %s.\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeer),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeer))),
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pRtProfile)));
                    return i4Action;
                }
                break;
            }
            else
            {
                i4Action = BGP4_DENY;
            }
        }
    }
    if (i4Action == BGP4_DENY)
    {
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Filter action is deny for the route %s.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO (pPeer))),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_DENY;
    }
    /* Apply ORF filters for the route profile if the direction is OUT */
    if (u4Dir == BGP4_OUTGOING)
    {
        MEMCPY (&OrfEntry.PeerAddr, &(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                sizeof (tAddrPrefix));

        pOrfEntry =
            RBTreeGetNext (BGP4_ORF_ENTRY_TABLE (BGP4_PEER_CXT_ID (pPeer)),
                           (tRBElem *) & OrfEntry, NULL);

        /* If no ORF entry is present for a partiuclar peer,
         * then no need to do ORF filtering */
        if ((pOrfEntry != NULL) && (MEMCMP (&pOrfEntry->PeerAddr,
                                            &(BGP4_PEER_REMOTE_ADDR_INFO
                                              (pPeer)),
                                            sizeof (tAddrPrefix)) == 0))
        {
            do
            {
                if (BgpOrfIsAddrPrefixMatching
                    (&(BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
                     pOrfEntry) == BGP4_TRUE)
                {
                    if (pOrfEntry->u1Match == BGP4_ORF_PERMIT)
                    {
                        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                                       "\tPEER %s : ORF filter entry is present for the route %s."
                                       " The filter action is permit.\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeer),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeer))),
                                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                        (pRtProfile),
                                                        BGP4_RT_AFI_INFO
                                                        (pRtProfile)));
                        return BGP4_ALLOW;;
                    }
                    else if (pOrfEntry->u1Match == BGP4_ORF_DENY)
                    {
                        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                                       "\tPEER %s : ORF filter entry is present for the route %s."
                                       " The filter action is deny.\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeer),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeer))),
                                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                        (pRtProfile),
                                                        BGP4_RT_AFI_INFO
                                                        (pRtProfile)));
                        return BGP4_DENY;;
                    }
                }
                pOrfEntry =
                    RBTreeGetNext (BGP4_ORF_ENTRY_TABLE
                                   (BGP4_PEER_CXT_ID (pPeer)),
                                   (tRBElem *) pOrfEntry, NULL);

                if (pOrfEntry == NULL)
                {
                    break;
                }
                if (MEMCMP
                    (&pOrfEntry->PeerAddr,
                     &(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                     sizeof (tAddrPrefix)) != 0)
                {
                    break;
                }
            }
            while (1);
            return BGP4_DENY;
        }
    }

#ifdef ROUTEMAP_WANTED

    if (u4Dir == BGP4_INCOMING)
    {                            /* Apply RouteMap configured for the peer  */
        if (pPeer->IpPrefixFilterIn.u1Status == ACTIVE)
        {
            pFilterRMap = &(pPeer->IpPrefixFilterIn);
            u1PrefixListPresent = BGP4_TRUE;
        }

    }
    else
    {
        if (pPeer->IpPrefixFilterOut.u1Status == ACTIVE)
        {
            pFilterRMap = &(pPeer->IpPrefixFilterOut);
            u1PrefixListPresent = BGP4_TRUE;
        }

    }

    if (u1PrefixListPresent == BGP4_TRUE)
    {
        if (BGP4_FAILURE == Bgp4ApplyPrefixFilter (pFilterRMap, pRtProfile))
        {
            /* Stop processing this route. */
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Prefix list is present. Prefix filter entry "
                           "is not found for the route %s. Hence the route is not proccessed.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeer))),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            return BGP4_DENY;
        }
    }
    pFilterRMap = NULL;

    if (u4Dir == BGP4_INCOMING)
    {                            /* Apply RouteMap configured for the peer  */

        if (pPeer->FilterInRMap.u1RowStatus == ACTIVE)
        {
            pFilterRMap = &(pPeer->FilterInRMap);
        }
        else
        {
            pFilterRMap =
                &(gBgpCxtNode[BGP4_PEER_CXT_ID (pPeer)]->
                  DistributeInFilterRMap);
        }
    }
    else
    {
        if (pPeer->FilterOutRMap.u1RowStatus == ACTIVE)
        {
            pFilterRMap = &(pPeer->FilterOutRMap);
        }
        else
        {
            pFilterRMap =
                &(gBgpCxtNode[BGP4_PEER_CXT_ID (pPeer)]->
                  DistributeOutFilterRMap);
        }
    }

    BGP4_DBG (BGP4_DBG_ENTRY, "\tBgp4ApplyInOutFilter() : .......\n ");
    if (BGP4_FAILURE == Bgp4ApplyInOutFilter (pFilterRMap, pRtProfile, u4Dir))
    {
        /* Stop processing this route. */
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Routemap entry not found for the route %s. "
                       "Hence the route is not proccessed.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO (pPeer))),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_DENY;
    }
#endif /* ROUTEMAP_WANTED */

    BGP4_DBG (BGP4_DBG_CTRL_FLOW,
              "\tBgp4UpdateFilter(): Entry doesnt match. recvd route\n");

    return BGP4_ALLOW;            /* Default Policy */
}

/*****************************************************************************/
/* Function Name : Bgp4ConfigMED                                             */
/* Description   : This function returns the MED value to be advertised / to */
/*                 be used for decision process using the configured MED     */
/*                 Table.                                                    */
/* Input(s)      : Peer information to which route is going to be advt.      */
/*                 (pPeer),                                                  */
/*                 Route profile which is going to be advertised             */
/*                 (pRtProfile),                                             */
/*                 Direction (u4Dir)                                         */
/* Output(s)     : None.                                                     */
/* Return(s)     : MED value if configured,                                  */
/*                 BGP4_INV_MEDLP if not.                                    */
/*****************************************************************************/
UINT4
Bgp4ConfigMED (tBgp4PeerEntry * pPeer, tRouteProfile * pRtProfile, UINT4 u4Dir)
{
    INT4                i4MedTblIndex = 0;
    tBgp4Info          *pDupBgp4Info = NULL;
    tBgp4Info          *pOldBgp4Info = NULL;

    for (i4MedTblIndex = 0; i4MedTblIndex < BGP4_MAX_MED_ENTRIES;
         i4MedTblIndex++)
    {
        if ((BGP4_MED_ENTRY_ADMIN (i4MedTblIndex) == BGP4_INVALID)
            || (BGP4_MED_ENTRY_ADMIN (i4MedTblIndex) == BGP4_ADMIN_DOWN))
        {
            continue;
        }

        if (BGP4_MED_ENTRY_VRFID (i4MedTblIndex) != BGP4_PEER_CXT_ID (pPeer))
        {
            continue;
        }

        /* CSR 127981 Currently skipping this check 
           if ((u4PeerType == BGP4_INTERNAL_PEER)
           && (BGP4_MED_ENTRY_DIRECTION (i4MedTblIndex) == BGP4_OUTGOING)
           && (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID))
           {
           continue;
           }
         */

        if (Bgp4IsThisMatching (pPeer, pRtProfile, u4Dir,
                                *((tBgp4UpdatefilterEntry *) &
                                  BGP4_MED_ENTRY[i4MedTblIndex])) == TRUE)
        {
            if (BGP4_MED_ENTRY_APPL (i4MedTblIndex) == BGP4_OVERRIDING)
            {

                if (((BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pRtProfile)))
                     & BGP4_ATTR_MED_MASK) != BGP4_ATTR_MED_MASK)
                {
                    pOldBgp4Info = BGP4_RT_BGP_INFO (pRtProfile);
                    pDupBgp4Info = Bgp4DuplicateBgp4Info (pRtProfile->pRtInfo);
                    if (pDupBgp4Info != NULL)
                    {
                        BGP4_LINK_INFO_TO_PROFILE (pDupBgp4Info, pRtProfile);
                        Bgp4DshReleaseBgpInfo (pOldBgp4Info);
                    }
                    if (u4Dir == BGP4_INCOMING)
                    {
                        BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pRtProfile))
                            |= BGP4_ATTR_MED_MASK;
                    }
                }
                BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                               BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_PEER_CON_TRC | BGP4_UPD_MSG_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : The MED value for the route %s is %d\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeer))),
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               BGP4_MED_ENTRY_MED (i4MedTblIndex));
                return ((UINT4) (BGP4_MED_ENTRY_MED (i4MedTblIndex)));
            }
            else
            {
                if (((BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pRtProfile)))
                     & BGP4_ATTR_MED_MASK) != BGP4_ATTR_MED_MASK)
                {
                    pOldBgp4Info = BGP4_RT_BGP_INFO (pRtProfile);
                    pDupBgp4Info = Bgp4DuplicateBgp4Info (pRtProfile->pRtInfo);
                    if (pDupBgp4Info != NULL)
                    {
                        BGP4_LINK_INFO_TO_PROFILE (pDupBgp4Info, pRtProfile);
                        Bgp4DshReleaseBgpInfo (pOldBgp4Info);
                    }
                    if (u4Dir == BGP4_INCOMING)
                    {
                        BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pRtProfile))
                            |= BGP4_ATTR_MED_MASK;
                    }
                    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_PEER_CON_TRC | BGP4_UPD_MSG_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : The MED value for the route %s is %d\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeer),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeer))),
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pRtProfile)),
                                   BGP4_MED_ENTRY_MED (i4MedTblIndex));
                    return ((UINT4) (BGP4_MED_ENTRY_MED (i4MedTblIndex)));
                }
                else
                {
                    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_PEER_CON_TRC | BGP4_UPD_MSG_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : The MED value for the route %s is %d\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeer),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeer))),
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pRtProfile)),
                                   BGP4_INFO_RCVD_MED (BGP4_RT_BGP_INFO
                                                       (pRtProfile)));
                    return (UINT4) (BGP4_INFO_RCVD_MED
                                    (BGP4_RT_BGP_INFO (pRtProfile)));
                }
            }
        }
    }

    if (u4Dir == BGP4_INCOMING)
    {
        if ((BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
            && (BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pRtProfile)) &
                BGP4_ATTR_MED_MASK))
        {
            BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_PEER_CON_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : The MED value for the route %s is %d\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeer))),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           BGP4_INFO_RCVD_MED (BGP4_RT_BGP_INFO (pRtProfile)));
            return (UINT4) (BGP4_INFO_RCVD_MED (BGP4_RT_BGP_INFO (pRtProfile)));
        }
    }

    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                   BGP4_PEER_CON_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                   "\tPEER %s : The MED value for the route %s is %d\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeer))),
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   BGP4_INV_MEDLP);
    return BGP4_INV_MEDLP;
}

/*****************************************************************************/
/* Function Name : Bgp4ConfigLP                                              */
/* Description   : This function returns the LP value to be advertised / to  */
/*                 be used for decision process using the configured LP      */
/*                 Table.                                                    */
/* Input(s)      : Peer information to which route is going to be advt.      */
/*                 (pPeer),                                                  */
/*                 Route profile which is going to be advertised             */
/*                 (pRtProfile),                                             */
/*                 Direction (u4Dir)                                         */
/* Output(s)     : None.                                                     */
/* Return(s)     : LP value if configured,                                   */
/*                 BGP4_INV_MEDLP if not.                                    */
/*****************************************************************************/
UINT4
Bgp4ConfigLP (tBgp4PeerEntry * pPeer, tRouteProfile * pRtProfile, UINT4 u4Dir)
{
    UINT4               u4PeerType;
    INT4                i4LpTblIndex = 0;

    u4PeerType = BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeer), pPeer);

    for (i4LpTblIndex = 0; i4LpTblIndex < BGP4_MAX_LP_ENTRIES; i4LpTblIndex++)
    {
        if ((BGP4_LP_ENTRY_ADMIN (i4LpTblIndex) == BGP4_INVALID)
            || (BGP4_LP_ENTRY_ADMIN (i4LpTblIndex) == BGP4_ADMIN_DOWN))
        {
            continue;
        }
        if (BGP4_LP_ENTRY_VRFID (i4LpTblIndex) != BGP4_PEER_CXT_ID (pPeer))
        {
            continue;
        }
        if ((u4PeerType == BGP4_EXTERNAL_PEER)
            && (BGP4_LP_ENTRY_DIRECTION (i4LpTblIndex) == BGP4_OUTGOING))
        {
            continue;
        }

        if (Bgp4IsThisMatching (pPeer, pRtProfile, u4Dir,
                                *((tBgp4UpdatefilterEntry *) &
                                  BGP4_LP_ENTRY[i4LpTblIndex])) == TRUE)
        {
            if (u4Dir == BGP4_OUTGOING)
            {
                BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                               BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_PEER_CON_TRC | BGP4_UPD_MSG_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : The local preference value for the "
                               "route %s is %d\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeer))),
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               BGP4_LP_ENTRY_LOCAL_PREF (i4LpTblIndex));
                return ((UINT4) (BGP4_LP_ENTRY_LOCAL_PREF (i4LpTblIndex)));
            }
            else
            {
                if (BGP4_LP_ENTRY_APPL (i4LpTblIndex) == BGP4_OVERRIDING)
                {
                    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_PEER_CON_TRC | BGP4_UPD_MSG_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : The local preference value for the "
                                   "route %s is %d\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeer),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeer))),
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pRtProfile)),
                                   BGP4_LP_ENTRY_LOCAL_PREF (i4LpTblIndex));
                    return ((UINT4) (BGP4_LP_ENTRY_LOCAL_PREF (i4LpTblIndex)));
                }
                else
                {
                    if (!(BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pRtProfile))
                          & BGP4_ATTR_LOCAL_PREF_MASK))
                    {
                        BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                       BGP4_PEER_CON_TRC | BGP4_UPD_MSG_TRC,
                                       BGP4_MOD_NAME,
                                       "\tPEER %s : The local preference value for the "
                                       "route %s is %d\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeer),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeer))),
                                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                        (pRtProfile),
                                                        BGP4_RT_AFI_INFO
                                                        (pRtProfile)),
                                       BGP4_LP_ENTRY_LOCAL_PREF (i4LpTblIndex));
                        return ((UINT4)
                                (BGP4_LP_ENTRY_LOCAL_PREF (i4LpTblIndex)));
                    }
                    else
                    {
                        BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                       BGP4_PEER_CON_TRC | BGP4_UPD_MSG_TRC,
                                       BGP4_MOD_NAME,
                                       "\tPEER %s : The local preference value for the "
                                       "route %s is %d\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeer),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeer))),
                                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                        (pRtProfile),
                                                        BGP4_RT_AFI_INFO
                                                        (pRtProfile)),
                                       BGP4_INFO_RCVD_LOCAL_PREF
                                       (BGP4_RT_BGP_INFO (pRtProfile)));
                        return (UINT4) (BGP4_INFO_RCVD_LOCAL_PREF
                                        (BGP4_RT_BGP_INFO (pRtProfile)));
                    }
                }
            }
        }
    }

    /*  For Updates sent to the internal peers, the default LocalPref.
     *  configured should be used. 
     */
    if (((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeer), pPeer)) ==
         BGP4_EXTERNAL_PEER) && (u4Dir == BGP4_INCOMING))
    {
        /*RFC 5065, 5.2: the restriction against sending the LOCAL_PREF attribute
         *    to peers in a neighboring autonomous system within the same
         *       confederation is removed. */
        /*So install recieved local preference */
        if ((BGP4_CONFED_ID (BGP4_PEER_CXT_ID (pPeer)) != BGP4_INV_AS) &&
            (BGP4_CONFED_PEER_STATUS (pPeer) == BGP4_TRUE))
        {
            BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_PEER_CON_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : The local preference value for the "
                           "route %s is %d\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeer))),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           BGP4_INFO_RCVD_LOCAL_PREF (BGP4_RT_BGP_INFO
                                                      (pRtProfile)));
            return (UINT4) (BGP4_INFO_RCVD_LOCAL_PREF
                            (BGP4_RT_BGP_INFO (pRtProfile)));
        }
        else
        {
            if (BGP4_RT_LOCAL_PREF (pRtProfile) == 0)
            {
                BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                               BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_PEER_CON_TRC | BGP4_UPD_MSG_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : The local preference value for the "
                               "route %s is %d\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeer))),
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               BGP4_DEFAULT_LOCAL_PREF (BGP4_PEER_CXT_ID
                                                        (pPeer)));
                return (BGP4_DEFAULT_LOCAL_PREF (BGP4_PEER_CXT_ID (pPeer)));
            }
            BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_PEER_CON_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : The local preference value for the "
                           "route %s is %d\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeer))),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           BGP4_RT_LOCAL_PREF (pRtProfile));
            return (UINT4) (BGP4_RT_LOCAL_PREF (pRtProfile));
        }
    }
    else if ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeer), pPeer)) ==
             BGP4_INTERNAL_PEER)
    {
        switch (u4Dir)
        {
            case BGP4_OUTGOING:
                if (((BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pRtProfile)) &
                      BGP4_ATTR_LOCAL_PREF_MASK) != BGP4_ATTR_LOCAL_PREF_MASK)
                    && (BGP4_RT_LOCAL_PREF (pRtProfile) == BGP4_INV_MEDLP))
                {
                    /* Local Pref not received along with the route and there
                     * no policy to configure Local Pref in incoming direction.
                     * Fill default Local Pref  */
                    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_PEER_CON_TRC | BGP4_UPD_MSG_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : The local preference value for the "
                                   "route %s is %d\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeer),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeer))),
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pRtProfile)),
                                   BGP4_DEFAULT_LOCAL_PREF (BGP4_PEER_CXT_ID
                                                            (pPeer)));
                    return (BGP4_DEFAULT_LOCAL_PREF (BGP4_PEER_CXT_ID (pPeer)));
                }
                else
                {
                    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_PEER_CON_TRC | BGP4_UPD_MSG_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : The local preference value for the "
                                   "route %s is %d\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeer),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeer))),
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pRtProfile)),
                                   BGP4_RT_LOCAL_PREF (pRtProfile));
                    return (UINT4) (BGP4_RT_LOCAL_PREF (pRtProfile));
                }
            case BGP4_INCOMING:
                if (BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pRtProfile)) &
                    BGP4_ATTR_LOCAL_PREF_MASK)
                {
                    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_PEER_CON_TRC | BGP4_UPD_MSG_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : The local preference value for the "
                                   "route %s is %d\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeer),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeer))),
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pRtProfile)),
                                   BGP4_INFO_RCVD_LOCAL_PREF (BGP4_RT_BGP_INFO
                                                              (pRtProfile)));
                    return (UINT4) (BGP4_INFO_RCVD_LOCAL_PREF
                                    (BGP4_RT_BGP_INFO (pRtProfile)));
                }
                else
                {
                    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_PEER_CON_TRC | BGP4_UPD_MSG_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : The local preference value for the "
                                   "route %s is %d\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeer),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeer))),
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pRtProfile)),
                                   BGP4_DEFAULT_LOCAL_PREF (BGP4_PEER_CXT_ID
                                                            (pPeer)));
                    return BGP4_DEFAULT_LOCAL_PREF (BGP4_PEER_CXT_ID (pPeer));
                }
            default:
                BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                               BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_PEER_CON_TRC | BGP4_UPD_MSG_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : The local preference value for the "
                               "route %s is %d\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeer))),
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               BGP4_INV_MEDLP);
                return BGP4_INV_MEDLP;

        }
    }
    else
    {
        BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_PEER_CON_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : The local preference value for the "
                       "route %s is %d\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO (pPeer))),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       BGP4_INV_MEDLP);
        return BGP4_INV_MEDLP;
    }
}

/*****************************************************************************/
/* Function Name : Bgp4IsThisMatching                                        */
/* Description   : This function checks whether the entry contains the given */
/*                 informations in it or not.                                */
/* Input(s)      : Peer information (pPeer),                                 */
/*                 Route profile (pRtProfile),                               */
/*                 Direction (u4Dir)                                         */
/*                 Table Entry (TblEntry)                                    */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRUE if the informations matches with the entry,          */
/*                 FALSE if not.                                             */
/*****************************************************************************/
BOOL1
Bgp4IsThisMatching (tBgp4PeerEntry * pPeer,
                    tRouteProfile * pRtProfile,
                    UINT4 u4Dir, tBgp4UpdatefilterEntry TblEntry)
{
    tNetAddress         Ipaddr;
    tNetAddress         FilterIpaddr;
#ifdef BGP4_IPV6_WANTED
    tNetAddress         IpaddrDefault;
#endif
#ifdef L3VPN
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
    UINT4               u4AsafiMask;
    UINT4               u4VrfId = -1;
#endif
    UINT4               u4AdvtAsafiMask;
    UINT4               u4MEDAsafiMask;
    UINT1               u1FilterPrefixlen;
    UINT1               u1Match;
    UINT1               u1MatchDefault = FALSE;

    /* For Incoming Non-BGP updates Filtering need not be applied. */
    if ((BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID) && (u4Dir == BGP4_INCOMING))
    {
        return FALSE;
    }

    if (TblEntry.u1Dir != (UINT1) u4Dir)
    {
        return FALSE;
    }

#ifdef L3VPN
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
    if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
    {
        /* Match Vrf Name */
        if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
        {
            /* BGP update - route must be received from a CE peer */
            if (BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_CE_PEER)
            {
                u4VrfId = BGP4_PEER_CXT_ID (BGP4_RT_PEER_ENTRY (pRtProfile));
            }
            else
            {
                if (!(BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_PE_PEER))
                {
                    return FALSE;
                }
            }
        }
        else
        {
            /* Non-BGP updates */
            /* Note: This is assumed to be for any vpnv4 route, the first
             * vrf information in the installed vrf list is non-null
             */
            pRtVrfInfo =
                TMO_SLL_First (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile));
            if (pRtVrfInfo != NULL)
            {
                u4VrfId = BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrfInfo);
            }
        }
        if ((INT4) u4VrfId != -1)
        {
            if (u4VrfId != TblEntry.u4VrfId)
            {
                return FALSE;
            }
        }
    }
#endif
    Bgp4CopyNetAddressStruct (&Ipaddr, BGP4_RT_NET_ADDRESS_INFO (pRtProfile));
    Bgp4CopyNetAddressStruct (&FilterIpaddr, TblEntry.UpdtInetAddress);
    u1FilterPrefixlen = (UINT1) (TblEntry.UpdtInetAddress.u2PrefixLen);

    if (TblEntry.u4ASNo != BGP4_INV_AS)
    {
        if (u4Dir == BGP4_OUTGOING)
        {
            if (BGP4_PEER_ASNO (pPeer) != TblEntry.u4ASNo)
            {
                return FALSE;
            }
        }
        else
        {
            if (BGP4_PEER_ASNO (BGP4_RT_PEER_ENTRY (pRtProfile)) !=
                TblEntry.u4ASNo)
            {
                return FALSE;
            }
        }
    }

    Bgp4GetPeerAdvtAfiSafi (pRtProfile, pPeer, &u4AdvtAsafiMask);
    BGP4_GET_AFISAFI_MASK (BGP4_AFI_IN_ADDR_PREFIX_INFO
                           (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                            (FilterIpaddr)),
                           BGP4_SAFI_IN_NET_ADDRESS_INFO (FilterIpaddr),
                           u4MEDAsafiMask);

    if ((u4MEDAsafiMask == CAP_MP_IPV4_UNICAST)
        && (u1FilterPrefixlen == BGP4_MAX_PREFIXLEN))
    {
        if (BGP4_TRUE ==
            AddrMatchDefault (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                              (Ipaddr),
                              BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                              (FilterIpaddr), (UINT4) u1FilterPrefixlen,
                              BGP4_INET_AFI_IPV4))
        {
            u1MatchDefault = TRUE;
        }
    }
#ifdef BGP4_IPV6_WANTED
    else if ((u4MEDAsafiMask == CAP_MP_IPV6_UNICAST)
             && (u1FilterPrefixlen == BGP4_MAX_IPV6_PREFIXLEN))
    {
        MEMSET (&IpaddrDefault, 0, sizeof (tNetAddress));
        if (BGP4_TRUE ==
            AddrMatchDefault (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                              (Ipaddr),
                              BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                              (IpaddrDefault), (UINT4) u1FilterPrefixlen,
                              BGP4_INET_AFI_IPV6))
        {
            if (BGP4_TRUE ==
                AddrMatchDefault (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                  (Ipaddr),
                                  BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                  (FilterIpaddr), (UINT4) u1FilterPrefixlen,
                                  BGP4_INET_AFI_IPV6))
            {
                u1MatchDefault = TRUE;
            }
        }
    }
#endif
    /* If u1FilterPrefixlen is Zero, then it matches all the prefixes.
     * If u1FilterPrefixlen is non-Zero but if Ipaddr is zero, then
     * this configuration is for Default Route.
     */
    if ((u1FilterPrefixlen != BGP4_MIN_PREFIXLEN) && (u1MatchDefault == FALSE))
    {
        if (u4AdvtAsafiMask != u4MEDAsafiMask)
        {
            BGP4_DBG (BGP4_DBG_ERROR,
                      "\tBgp4IsThisMatching() : Afi,Safi Don't Match \n");
            return FALSE;        /* Prefix doesn't match */
        }
        switch (u4MEDAsafiMask)
        {
            case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
            case CAP_MP_LABELLED_IPV4:
#endif
#ifdef BGP4_IPV6_WANTED
            case CAP_MP_IPV6_UNICAST:
#endif
            {
                u1Match =
                    (UINT1) (AddrMatch
                             (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                              (Ipaddr),
                              BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                              (FilterIpaddr), (UINT4) u1FilterPrefixlen));
                break;
            }
#ifdef L3VPN
            case CAP_MP_VPN4_UNICAST:
            {
                u1Match =
                    (UINT1) (Vpnv4AddrMatch
                             (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                              (FilterIpaddr), pRtProfile,
                              (UINT4) u1FilterPrefixlen));
                break;
            }
#endif
            default:
                return BGP4_FALSE;
        }
        if (u1Match == BGP4_FALSE)
        {
            BGP4_DBG (BGP4_DBG_ERROR,
                      "\tBgp4IsThisMatching() : Address Doesn't Match \n");
            return FALSE;        /* Prefix doesn't match */
        }
        BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                  "\tBgp4IsThisMatching() : Address Matches \n");
    }
    else if (u1MatchDefault == FALSE)
    {
        if (u4AdvtAsafiMask != u4MEDAsafiMask)
        {
            BGP4_DBG (BGP4_DBG_ERROR,
                      "\tBgp4IsThisMatching() : Afi,Safi Don't Match \n");
            return FALSE;        /* Prefix doesn't match */
        }

        return TRUE;
    }
    if ((Ipaddr.u2PrefixLen != FilterIpaddr.u2PrefixLen)
        && (u1MatchDefault == FALSE))
    {
        /* If prefin len is not matching then return */
        return FALSE;
    }

    if (Bgp4IsInterASMatch (pPeer, pRtProfile, u4Dir,
                            TblEntry.au4InterAS) == FALSE)
    {
        return FALSE;
    }

    return TRUE;
}

/*****************************************************************************/
/* Function Name : Bgp4IsInterASMatch                                        */
/* Description   : This function checks whether all the AS number present in */
/*                 the string are there in the AS_PATH information or not.   */
/* Input(s)      : Peer information (pPeer),                                 */
/*                 Route profile (pRtProfile),                               */
/*                 Direction (u4Dir)                                         */
/*                 List of ASes (pu2Interas)                                 */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRUE if all the ASes specified in the string are present, */
/*                 FALSE if not.                                             */
/*****************************************************************************/
BOOL1
Bgp4IsInterASMatch (tBgp4PeerEntry * pPeer,
                    tRouteProfile * pRtProfile, UINT4 u4Dir, UINT4 *pu4Interas)
{
    tTMO_SLL           *pTsAspath = NULL;
    UINT4              *pu4TmpAsStr = NULL;

    pu4TmpAsStr = pu4Interas;
    BGP4_DBG (BGP4_DBG_CTRL_FLOW,
              "\tBgp4IsInterASMatch() : Configured inter AS nos. are\n");
    while (*pu4TmpAsStr != BGP4_INV_AS)
    {
        BGP4_DBG1 (BGP4_DBG_CTRL_FLOW, "\t%d\n", *pu4TmpAsStr);
        pu4TmpAsStr++;
    }

    if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
    {
        pTsAspath = BGP4_INFO_ASPATH (BGP4_RT_BGP_INFO (pRtProfile));
    }

    while (*pu4Interas != BGP4_INV_AS)
    {
        if (*pu4Interas == BGP4_PEER_LOCAL_AS (pPeer))
        {
            /* Check for the presence of our AS no. in the
             *  AS path. This will be applicable only while
             * sending the update out.
             */
            BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                      "\tBgp4IsInterASMatch() : Checking for Local AS\n");

            if (u4Dir == BGP4_INCOMING)
            {                    /* This is an error - AS Loop */
                BGP4_DBG (BGP4_DBG_ERROR, "\tBgp4IsInterASMatch() : AS loop\n");
                return FALSE;
            }
            else
            {
                /* For internal updates the Local AS no. will
                 *  not be filled. 
                 */
                if ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeer), pPeer)) ==
                    BGP4_INTERNAL_PEER)
                {
                    return FALSE;
                }
            }
        }
        else
        {
            if (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID)
            {
                BGP4_DBG (BGP4_DBG_ERROR,
                          "\tBgp4IsInterASMatch(): recvd route protocol not BGP\n");
                return FALSE;
            }

            if (Bgp4IsASnoFound (pTsAspath, *pu4Interas) == FALSE)
            {
                BGP4_DBG (BGP4_DBG_ERROR,
                          "\tBgp4IsInterASMatch(): Intermediate AS nos donot match\n");
                return FALSE;
            }
        }
        pu4Interas++;
    }
    return TRUE;
}

/*****************************************************************************/
/* Function Name : Bgp4IsASnoFound                                           */
/* Description   : This function checks whether a particular AS number is    */
/*                 present in the AS_PATH information.                       */
/* Input(s)      : List of AS_Sequence which forms the AS_PATH information   */
/*                 (pTsAspath),                                              */
/*                 AS number to be searched for (u2ASno)                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRUE if it is found,                                      */
/*                 FALSE if not.                                             */
/*****************************************************************************/
BOOL1
Bgp4IsASnoFound (tTMO_SLL * pTsAspath, UINT4 u4ASno)
{
    tAsPath            *pAspath = NULL;
    UINT4              *pu4AsList = NULL;
    UINT1               u1AsCount;

    BGP4_DBG1 (BGP4_DBG_ALL, "\tBgp4IsASnoFound() : Find ASno = %u\n ", u4ASno);

    TMO_SLL_Scan (pTsAspath, pAspath, tAsPath *)
    {
        u1AsCount = BGP4_ASPATH_LEN (pAspath);
        pu4AsList = (UINT4 *) (VOID *) BGP4_ASPATH_NOS (pAspath);

        while (u1AsCount > 0)
        {
            if (OSIX_NTOHL (*pu4AsList) == u4ASno)
            {
                return TRUE;
            }
            u1AsCount--;
            pu4AsList++;
        }
    }
    return FALSE;
}

/*****************************************************************************/
/*  Function Name   :   Bgp4FiltApplyOutgoingFilter                          */
/*  Description     :   This function applies all the outgoing filters on    */
/*                      the given route including comm/ecomm policy and      */
/*                      return the status whether the route is filtered or   */
/*                      not.                                                 */
/*  Input(s)        :   pPeerInfo -                                          */
/*                         Pointer to the peer information from which the    */
/*                         route refresh message is received.                */
/*                      pRtProfile -                                         */
/*                         Pointer to the Route profile                      */
/*  Output(s)       :   None                                                 */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  BGP4_SUCCESS - if route can be advertised */
/*                              :  BGP4_FAILURE - if route is filtered       */
/*                              :  BGP4_RT_ADVT_WITHDRAWN - if route is to be*/
/*                                 advertised as withdrawn.                  */
/*****************************************************************************/
INT4
Bgp4FiltApplyOutgoingFilter (tBgp4PeerEntry * pPeerInfo,
                             tRouteProfile * pRtProfile)
{
    tTMO_HASH_TABLE    *pHashTab = NULL;
    UINT1               u1IsRouteFiltered = BGP4_FALSE;
    INT4                i4RetVal = BGP4_FAILURE;
    UINT4               u4AsafiMask;
#ifdef L3VPN
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
    UINT1               u1NoExpTgts = BGP4_FALSE;
#endif

    Bgp4GetPeerAdvtAfiSafi (pRtProfile, pPeerInfo, &u4AsafiMask);
    /* Check whether the route is present in the Peers Output list */
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
            pHashTab = BGP4_PEER_IPV4_OUTPUT_ROUTES (pPeerInfo);
            break;
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
            pHashTab = BGP4_PEER_IPV4_LBLD_OUTPUT_ROUTES (pPeerInfo);
            break;
        case CAP_MP_VPN4_UNICAST:
            pHashTab = BGP4_PEER_VPN4_OUTPUT_ROUTES (pPeerInfo);
            break;
#endif
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
            if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeerInfo) != NULL)
            {
                pHashTab = BGP4_PEER_IPV6_OUTPUT_ROUTES (pPeerInfo);
            }
            break;
#endif
#ifdef VPLSADS_WANTED
            /*ADS-VPLS related Processing */
        case CAP_MP_L2VPN_VPLS:
        {
            pHashTab = BGP4_PEER_VPLS_OUTPUT_ROUTES (pPeerInfo);
        }
            break;
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
            pHashTab = BGP4_PEER_EVPN_OUTPUT_ROUTES (pPeerInfo);
            break;
#endif
        default:
            return (BGP4_FAILURE);
    }

    if (pHashTab == NULL)
    {
        return BGP4_FAILURE;
    }

    if (Bgp4HashTblGetEntry (pHashTab, pRtProfile, BGP4_FALSE) != NULL)
    {
        /* Route already present in the Output list. Means that there
         * was some outbound filter policy existed that has filtered this
         * route. */
        u1IsRouteFiltered = BGP4_TRUE;
    }

    /* Reset the  Output Filter flag and dont advt flag. Set them if
     * the route is actually filter out. */
    BGP4_RT_RESET_FLAG (pRtProfile,
                        (BGP4_RT_FILTERED_OUTPUT | BGP4_RT_DONT_ADVT));

#ifdef L3VPN
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);

    /* If the export route targets are configured for the installed
     * vrf, (explicit NULL export target) then do not send this route;
     * the route is either received from a CE peer or is 
     * originated at PE.
     */
    /* Apply Export Targets Filter when VRF is in export route-target
     * change processing. when it is operationally up, it will be taken
     * care in AhIsRouteCanbeadvt ()
     */
    if ((u4AsafiMask == CAP_MP_VPN4_UNICAST) &&
        (BGP4_VPN4_PEER_ROLE (pPeerInfo) != BGP4_VPN4_CE_PEER))
    {
        if (BGP4_RR_CLIENT_CNT (BGP4_PEER_CXT_ID (pPeerInfo)) == 0)
        {
            TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile),
                          pRtVrfInfo, tRtInstallVrfInfo *)
            {
                if ((BGP4_VPN4_VRF_SPEC_GET_FLAG
                     (BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrfInfo)) &
                     BGP4_VRF_EXP_TGT_CHG_INPROGRESS)
                    &&
                    (TMO_SLL_Count
                     (BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS
                      (BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrfInfo))) <= 0))
                {
                    BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_FILTERED_OUTPUT);
                    u1NoExpTgts = BGP4_TRUE;
                    break;
                }
            }
        }
    }

    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_FILTERED_OUTPUT) ||
        (BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_DONT_ADVT))
    {
        /* Route is either filtered or blocked from advertisement */
    }
    else
    {
#endif
        /* Apply the Community Output Filter */
        CommApplyOutboundFilterPolicy (pRtProfile, pPeerInfo);
        if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_FILTERED_OUTPUT) ||
            (BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_DONT_ADVT))
        {
            /* Route is either filtered or blocked from advertisement */
        }
        else
        {
            /* Route has passed Community Output Filter policy. Now
             * apply the Extended Community Output Filter */
            ExtCommApplyOutboundFilterPolicy (pRtProfile, pPeerInfo);
            if (BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_FILTERED_OUTPUT)
            {
                /* Route is filtered from advertisement */
            }
            else
            {
                /* Route has passed Extended Community Output Filter policy. Now
                 * apply the other Output Filter policies */
#ifdef VPLSADS_WANTED
                if (u4AsafiMask == CAP_MP_L2VPN_VPLS)
                {
                    /* update Filter and Orf filter are supported for address prefix
                       (Ipv4/Ipv6) and does not catre afi/safi processing */
                    return BGP4_SUCCESS;
                }
#endif
                if (Bgp4UpdateFilter (pPeerInfo, pRtProfile, BGP4_OUTGOING)
                    == BGP4_ALLOW)
                {
                    /* Route is not filtered out. */
                    BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_FDB_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s : Route %s is not filtered out by filtering policies\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerInfo),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerInfo))),
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pRtProfile)));
                    return (BGP4_SUCCESS);
                }
            }
        }
#ifdef L3VPN
    }
#endif

    /* Route is filter out. */
    if (u1IsRouteFiltered == BGP4_TRUE)
    {
        /* Route already present in the Peer's Output list. Need to be 
         * removed from the list. */
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Route %s is already filtered out. Hence removing "
                       "the route from peer's output list.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        Bgp4HashTblDeleteEntry (pHashTab, pRtProfile, BGP4_FALSE);
    }
    else
    {
        /* Route getting filtered out for the first time. Check if the
         * replacement flag is set or not. If replacement flag is set, then
         * the route should have been sent out earlier. Now needs to sent this
         * route as withdrawn */
#ifndef L3VPN
        if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_REPLACEMENT) ==
            BGP4_RT_REPLACEMENT)
        {
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Route %s is filtered out. Since it is a "
                           "replacement route advertising it as withdrawn.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            i4RetVal = BGP4_RT_ADVT_WITHDRAWN;
        }
#else
        if (u1NoExpTgts == BGP4_TRUE)
        {
            return BGP4_NO_EXPORT_TARGETS;
        }
        if (((u4AsafiMask != CAP_MP_VPN4_UNICAST) ||
             ((u4AsafiMask == CAP_MP_VPN4_UNICAST) &&
              (BGP4_VPN4_PEER_ROLE (pPeerInfo) != BGP4_VPN4_CE_PEER)))
            && ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_REPLACEMENT) ==
                BGP4_RT_REPLACEMENT))
        {
            i4RetVal = BGP4_RT_ADVT_WITHDRAWN;
        }
        else if ((u4AsafiMask == CAP_MP_VPN4_UNICAST) &&
                 (BGP4_VPN4_PEER_ROLE (pPeerInfo) == BGP4_VPN4_CE_PEER))
        {
            Bgp4Vpnv4GetInstallVrfInfo (pRtProfile,
                                        BGP4_PEER_CXT_ID (pPeerInfo),
                                        &pRtVrfInfo);
            if (pRtVrfInfo == NULL)
            {
                /* vrf would have been deleted */
                return BGP4_SUCCESS;
            }
            if (BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo) &
                BGP4_RT_VRF_REPLACEMENT)
            {
                return BGP4_RT_ADVT_WITHDRAWN;
            }
        }
#endif
    }

    /* Reset the flags. */
    BGP4_RT_RESET_FLAG (pRtProfile,
                        (BGP4_RT_FILTERED_OUTPUT | BGP4_RT_DONT_ADVT));

    if (u1IsRouteFiltered == BGP4_TRUE)
    {
        return BGP4_RT_DONT_ADVT;
    }
    return (i4RetVal);
}

/*****************************************************************************/
/*  Function Name   :   Bgp4FiltApplyInboundSoftCfgSendRtRef                 */
/*                                                                           */
/*  Description     :   This function checks for route-refresh capability    */
/*                      supported by peers in the input peerlist.            */
/*                      * If peer supports that capability, then the route-  */
/*                        message is sent to the peer, Else                  */
/*                      * Soft-reconfiguration is applied on the Rib-IN      */
/*                                                                           */
/*  Input(s)        :   None                                                 */
/*  Output(s)       :   None                                                 */
/*                                                                           */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Return        : BGP4_TRUE  - if more peers remains to be processed.      */
/*                : BGP4_FALSE - if no more peers needs to be processed      */
/*                  BGP4_RELINQUISH - if external events requires CPU        */
/*****************************************************************************/
INT4
Bgp4FiltApplyInboundSoftCfgSendRtRef (UINT4 u4Context)
{
    /* Variable Declarations */
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tPeerNode          *pTmpPeerNode = NULL;
    tPeerNode          *pPeerNode = NULL;
    UINT4               u4PeerCnt = 0;
    INT4                i4RetVal = BGP4_FAILURE;

    /* Check if PeerInfo List is NULL */
    if (TMO_SLL_Count (BGP4_RTREF_PEER_LIST (u4Context)) == 0)
    {
        return BGP4_FALSE;
    }

    /* 
     * Apply incoming policy on the IN-RIB if the route-refresh
     * capability is not supported, else send route-refresh
     * capability to the specified peer.
     */
    BGP_SLL_DYN_Scan (BGP4_RTREF_PEER_LIST (u4Context), pPeerNode, pTmpPeerNode,
                      tPeerNode *)
    {
        pPeerInfo = pPeerNode->pPeer;
        i4RetVal = Bgp4FiltApplyIncomingSoftReconfig (pPeerInfo);
        if (i4RetVal == BGP4_SOFTRECONFIG_COMPLETE)
        {
            /*  Reset the peer current status */
            BGP4_RESET_PEER_CURRENT_STATE (pPeerInfo);
            BGP4_SET_PEER_CURRENT_STATE (pPeerInfo, BGP4_PEER_READY);

            /* Reset the peer soft-init information */
            Bgp4InitNetAddressStruct (&
                                      (BGP4_PEER_INIT_NETADDR_INFO (pPeerInfo)),
                                      BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);
            BGP4_PEER_SOFTCONFIG_INITIAL_AFISAFI (pPeerInfo) = 0;
            BGP4_PEER_SOFTCONFIG_INBOUND_AFI (pPeerInfo) = 0;
            BGP4_PEER_SOFTCONFIG_INBOUND_SAFI (pPeerInfo) = 0;
            /*  Remove peer from soft-reconfig list */
            TMO_SLL_Delete (BGP4_RTREF_PEER_LIST (u4Context),
                            &pPeerNode->TSNext);

            BGP_PEER_NODE_FREE (pPeerNode);
        }
        u4PeerCnt++;
        if (u4PeerCnt > BGP4_MAX_PEERS2PROCESS)
        {
            break;
        }
    }

    /*Updating distance configured */
    BGPUpdateDistanceChangeForExistRoutes (u4Context, CAP_MP_IPV4_UNICAST);
#ifdef BGP4_IPV6_WANTED
    BGPUpdateDistanceChangeForExistRoutes (u4Context, CAP_MP_IPV6_UNICAST);
#endif

    if (Bgp4IsCpuNeeded () == BGP4_TRUE)
    {
        /* External events requires CPU. Relinquish CPU. */
        return (BGP4_RELINQUISH);
    }

    if (TMO_SLL_Count (BGP4_RTREF_PEER_LIST (u4Context)) > 0)
    {
        return (BGP4_TRUE);
    }
    else
    {
        return (BGP4_FALSE);
    }
}

/*****************************************************************************/
/*  Function Name   :   Bgp4FiltApplyOutBoundSoftCfg                         */
/*                                                                           */
/*  Description     :   This function applies outbound soft-reconfig         */
/*                      for peers in the outbound softconfig list            */
/*                                                                           */
/*  Input(s)        :   None                                                 */
/*  Output(s)       :   None                                                 */
/*                                                                           */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  BGP4_RIB_TREE(BGP4_DFLT_VRFID)                             */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Return        : BGP4_TRUE  - if more peers remains to be processed.      */
/*                : BGP4_FALSE - if no more peers needs to be processed      */
/*                  BGP4_RELINQUISH - if external events requires CPU        */
/*****************************************************************************/
INT4
Bgp4FiltApplyOutBoundSoftCfg (UINT4 u4Context)
{
    /* Variable Declarations */
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tPeerNode          *pTmpPeerNode = NULL;
    tPeerNode          *pPeerNode = NULL;
    UINT4               u4PeerCnt = 0;
    INT4                i4RetVal = BGP4_FAILURE;

    /* Check if PeerInfo List is NULL */
    if (TMO_SLL_Count (BGP4_SOFTCFG_OUTBOUND_LIST (u4Context)) == 0)
    {
        return BGP4_FALSE;
    }

    BGP_SLL_DYN_Scan (BGP4_SOFTCFG_OUTBOUND_LIST (u4Context), pPeerNode,
                      pTmpPeerNode, tPeerNode *)
    {
        pPeerInfo = pPeerNode->pPeer;
        i4RetVal = Bgp4PeerProcessSoftInitRoutes (pPeerInfo,
                                                  BGP4_MAX_SOFTCFGRTS2PROCESS);
        if (i4RetVal == BGP4_SOFTRECONFIG_COMPLETE)
        {
            /* If there are no more route in this <AFI,SAFI>
             * then reset the PEER init
             */

            Bgp4InitNetAddressStruct (&
                                      (BGP4_PEER_INIT_NETADDR_INFO (pPeerInfo)),
                                      BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);

            if (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_UP)
            {
                /* Need to advertise the aggregated routes to the peer. */
                Bgp4AggrAdvtAggrRouteToPeer (pPeerInfo);
            }

            /*  Reset the peer current status */
            BGP4_RESET_PEER_CURRENT_STATE (pPeerInfo);
            BGP4_SET_PEER_CURRENT_STATE (pPeerInfo, BGP4_PEER_READY);

            if (BGP4_PEER_DEF_ROUTE_ORIG_STATUS (pPeerInfo) ==
                BGP4_DEF_ROUTE_ORIG_ENABLE)
            {
                /* Need to advertise default-route. */
                Bgp4PeerAdvtDftRoute (pPeerInfo, BGP4_DEF_ROUTE_ORIG_ENABLE,
                                      BGP4_TRUE);
            }

            /*  Remove peer from soft-reconfig list */
            TMO_SLL_Delete (BGP4_SOFTCFG_OUTBOUND_LIST (u4Context),
                            &pPeerNode->TSNext);
            BGP4_PEER_SOFTCONFIG_INITIAL_AFISAFI (pPeerInfo) = 0;
            BGP4_PEER_SOFTCONFIG_OUTBOUND_AFI (pPeerInfo) = 0;
            BGP4_PEER_SOFTCONFIG_OUTBOUND_SAFI (pPeerInfo) = 0;
            BGP4_PEER_INIT_PA_HASHKEY (pPeerInfo) = 0;
            BGP4_PEER_INIT_PA_ENTRY (pPeerInfo) = NULL;
            BGP_PEER_NODE_FREE (pPeerNode);
        }
        u4PeerCnt++;
        if (u4PeerCnt > BGP4_MAX_PEERS2PROCESS)
        {
            break;
        }
    }
    if (Bgp4IsCpuNeeded () == BGP4_TRUE)
    {
        /* External events requires CPU. Relinquish CPU. */
        return (BGP4_RELINQUISH);
    }

    if (TMO_SLL_Count (BGP4_SOFTCFG_OUTBOUND_LIST (u4Context)) > 0)
    {
        return (BGP4_TRUE);
    }
    else
    {
        return (BGP4_FALSE);
    }
}

/*****************************************************************************/
/*  Function Name   :   Bgp4FiltApplyIncomingSoftReconfig                    */
/*  Description     :   This function applies soft reconfiguration on the    */
/*                      RIB-IN for the Specific <AFI,SAFI> stored in Peer    */
/*                      SOFT-IN Parameters.                                  */
/*  Input(s)        :   pPeerInfo -                                          */
/*                  :       Pointer to the peer information for whom the     */
/*                  :       soft reconfiguration has to be applied           */
/*  Output(s)       :   None                                                 */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  BGP4_SUCCESS - If any route is available. */
/*                              :  BGP4_SOFTRECONFIG_COMPLETE - if no more   */
/*                                 route.                                    */
/*                              :  BGP4_FAILURE - if any error               */
/*****************************************************************************/
INT4
Bgp4FiltApplyIncomingSoftReconfig (tBgp4PeerEntry * pPeerInfo)
{
    /* Variable Declarations */
    tAddrPrefix         InvPrefix;
    tRouteProfile       RtProfile;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pAddRtProfile = NULL;
    tRouteProfile      *pNextRtList = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4Status = 0;
    UINT4               u4AfiSafiIndex = 0;
    UINT4               u4RtCnt = 0;

    Bgp4InitAddrPrefixStruct (&(InvPrefix), 0);

    /* Here we should know for which address family, the softconf/routerefresh
     * request is issued. Based on that, get the corresponding RIB information.
     */
    Bgp4GetAfiSafiIndex (BGP4_PEER_SOFTCONFIG_INBOUND_AFI (pPeerInfo),
                         BGP4_PEER_SOFTCONFIG_INBOUND_SAFI (pPeerInfo),
                         &u4AfiSafiIndex);

    if ((BGP4_PEER_AFI_SAFI_INSTANCE (pPeerInfo, u4AfiSafiIndex) == NULL) ||
        (BGP4_LOCAL_ADMIN_STATUS (BGP4_PEER_CXT_ID (pPeerInfo)) ==
         BGP4_ADMIN_DOWN))
    {
        /* <AFI,SAFI> not negotiated. No need to do any processing
         * for this <AFI,SAFI> */
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Address family is not negotiated for this peer. "
                       "Soft reconfiguration processing is not done.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))));
        return (BGP4_SOFTRECONFIG_COMPLETE);
    }

    if ((MEMCMP (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (InvPrefix),
                 BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                 (BGP4_PEER_INIT_PREFIX_INFO (pPeerInfo)),
                 BGP4_MAX_INET_ADDRESS_LEN)) == 0)
    {
        /* Start processing from the first route for this peer
         * present in the Peer-Route List. */
        pRtProfile =
            BGP4_DLL_FIRST_ROUTE (BGP4_PEER_ROUTE_LIST (pPeerInfo,
                                                        u4AfiSafiIndex));
        if (pRtProfile == NULL)
        {
            /* No route is present in the peer for this <AFI,SAFI>. */
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : No route is present in the peer for this "
                           "address family. Soft reconfiguration processing is not done\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            return (BGP4_SOFTRECONFIG_COMPLETE);
        }
    }
    else
    {
        /* Get the peer-route corresponding to the int 
         * rt-info from the RIB */
        RtProfile.u1Ref = 0;
        RtProfile.u1Protocol = 0;
        RtProfile.p_RPNext = 0;
        RtProfile.u4Flags = 0;
        RtProfile.pRtInfo = 0;
        Bgp4CopyNetAddressStruct (&(RtProfile.NetAddress),
                                  BGP4_PEER_INIT_NETADDR_INFO (pPeerInfo));
        i4Status =
            Bgp4RibhLookupRtEntry (&RtProfile, BGP4_PEER_CXT_ID (pPeerInfo),
                                   BGP4_TREE_FIND_EXACT, &pRtProfile,
                                   &pRibNode);
        if (i4Status == BGP4_FAILURE)
        {
            /* No more routes present in RIB for this <AFI,SAFI> */
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : No more routes present in RIB for this "
                           "address family. Soft reconfiguration processing is not done\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            return (BGP4_SOFTRECONFIG_COMPLETE);
        }

        pRtProfile = Bgp4DshGetPeerRtFromProfileList (pRtProfile, pPeerInfo);
        if (pRtProfile == NULL)
        {
            /* No Peer routes present in RIB for this <AFI,SAFI> */
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : No peer routes present in RIB for this "
                           "address family. Soft reconfiguration processing is not done\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            return (BGP4_SOFTRECONFIG_COMPLETE);
        }
    }

    for (;;)
    {
        BGP4_RT_REF_COUNT (pRtProfile)++;
        /* Get the next-Route profile. */
        pNextRtList = BGP4_RT_PROTO_LINK_NEXT (pRtProfile);

        /* Process the current route */
        if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_HISTORY) ==
            BGP4_RT_HISTORY)
        {
            /* Route is HISTORY. No need to process this route. */
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Route %s is a history route. So no need to "
                           "process soft reconfiguration for the route.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            pAddRtProfile = NULL;
        }
        else
        {
            pAddRtProfile = Bgp4FiltApplyInputFilterOnRoute (pRtProfile,
                                                             pPeerInfo);
        }
        if (pAddRtProfile != NULL)
        {
            /* There is change in INBOUND Policy for this route. Route is
             * available for Input Processing. Add the routes to the Peer's
             * Recv route list. Ensure that these routes are added to the
             * beginning of the Peer's Received Route List. */
            Bgp4DshDelinkRouteFromChgList (pAddRtProfile);
            if ((BGP4_RT_GET_FLAGS (pAddRtProfile) & BGP4_RT_FILTERED_INPUT) !=
                BGP4_RT_FILTERED_INPUT)
            {
                BGP4_RT_RESET_FLAG (pAddRtProfile, BGP4_RT_WITHDRAWN);
            }
            else
            {
                BGP4_RT_SET_FLAG (pAddRtProfile, BGP4_RT_WITHDRAWN);
            }
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : There is a change in the route %s by inbound "
                           "policy. So adding the route to peer's received route list.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pAddRtProfile),
                                            BGP4_RT_AFI_INFO (pAddRtProfile)));
            Bgp4DshAddRouteToList (BGP4_PEER_RCVD_ROUTES (pPeerInfo),
                                   pAddRtProfile, BGP4_PREPEND);
        }
        Bgp4DshReleaseRtInfo (pRtProfile);
        u4RtCnt++;

        if (pNextRtList == NULL)
        {
            /* No more route in this <AFI,SAFI> */
            break;
        }
        if (u4RtCnt >= BGP4_MAX_SOFTCFGRTS2PROCESS)
        {
            /* Store the next route to be processed in peer init route info.
             * This route will be processed in the next TIC */
            Bgp4CopyNetAddressStruct (&
                                      (BGP4_PEER_INIT_NETADDR_INFO (pPeerInfo)),
                                      BGP4_RT_NET_ADDRESS_INFO (pNextRtList));
            break;
        }
        pRtProfile = pNextRtList;
        pNextRtList = NULL;
    }

    if (u4RtCnt < BGP4_MAX_SOFTCFGRTS2PROCESS)
    {
        /* Have processed only less than max-number of routes. This is
         * because, no more route is available. */
        return BGP4_SOFTRECONFIG_COMPLETE;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   :   Bgp4FiltApplyInputFilter                             */
/*                                                                           */
/*  Description     :   This function applies incoming filter policies on the*/
/*                      the input feasible lists including comm/ecomm policy */
/*                      and outputs the feasible routes that pass through the*/
/*                      filter mechanism.                                    */
/*                      If there is a old route in the RIB for a filtered    */
/*                      route, it is deleted and advertised as withdrawn to  */
/*                      the corresponding peers.                             */
/*                                                                           */
/*  Input(s)        :   pInputList - List of routes on which the input filter*/
/*                      needs to be applied.                                 */
/*                      pPeerInfo -                                          */
/*                         Pointer to the peer information from which the    */
/*                         routes are received.                              */
/*                                                                           */
/*  Output(s)       :   None                                                 */
/*                                                                           */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  BGP4_RIB_TREE(BGP4_DFLT_VRFID)                             */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*                                                                           */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  BGP4_SUCCESS/BGP4_FAILURE                 */
/*****************************************************************************/
INT4
Bgp4FiltApplyInputFilter (tTMO_SLL * pInputList, tBgp4PeerEntry * pPeerInfo)
{
    /* Variable Declarations */
#ifdef L3VPN
    tTMO_SLL            TsSelectedCERtsList;
#endif
    tLinkNode          *pLinkNode = NULL;
    tRouteProfile      *pRtProfile = NULL;
    INT4                i4RetStatus = 0;

    i4RetStatus = CommFormListForInst (pInputList, pPeerInfo);
    if (i4RetStatus == COMM_FAILURE)
    {
        return i4RetStatus;
    }

    i4RetStatus = ExtCommFormListForInst (pInputList, pPeerInfo);
    if (i4RetStatus == EXT_COMM_FAILURE)
    {
        return i4RetStatus;
    }
#ifdef VPLSADS_WANTED
    i4RetStatus = Bgp4VplsApplyImportRTFilter (pInputList, pPeerInfo);
    if (i4RetStatus == BGP4_FAILURE)
    {
        return i4RetStatus;
    }
#endif

#ifdef L3VPN
    if (BGP4_VPN4_PEER_ROLE (pPeerInfo) == BGP4_VPN4_CE_PEER)
    {
        TMO_SLL_Init (&TsSelectedCERtsList);
        /* if the memory fails in attaching VRF id information to the route,
         * those routes alone will be filtered, the rest of selected routes
         * must be processed for the remaining filtering policies
         */
        i4RetStatus = Bgp4Vpnv4ApplyCEToPERTFilter (pInputList, pPeerInfo,
                                                    &TsSelectedCERtsList);
        /* the selected CE rts list and Input rts list contain the same routes
         * if all the routes are accepted, else, the selected routes contain
         * only those routes for which VRF id information is linked.
         * for the remaining routes present in Input rts list, 
         * filter flag wil be set
         */
        if (i4RetStatus == BGP4_DENY)
        {
            Bgp4DshReleaseList (&TsSelectedCERtsList, 0);
            return BGP4_FAILURE;
        }
        i4RetStatus = Bgp4Vpnv4ApplyImportRTFilter (&TsSelectedCERtsList,
                                                    pPeerInfo);
        Bgp4DshReleaseList (&TsSelectedCERtsList, 0);
    }
    else
    {
        i4RetStatus = Bgp4Vpnv4ApplyImportRTFilter (pInputList, pPeerInfo);
    }
    if (i4RetStatus == BGP4_FAILURE)
    {
        return i4RetStatus;
    }
#endif
#ifdef EVPN_WANTED
    i4RetStatus = Bgp4EvpnApplyImportRTFilter (pInputList, pPeerInfo);
    if (i4RetStatus == BGP4_FAILURE)
    {
        return i4RetStatus;
    }

#endif
    TMO_SLL_Scan (pInputList, pLinkNode, tLinkNode *)
    {
        pRtProfile = pLinkNode->pRouteProfile;
        if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_FILTERED_INPUT) ==
            BGP4_RT_FILTERED_INPUT)
        {
            continue;
        }
        if (Bgp4UpdateFilter (pPeerInfo, pRtProfile, BGP4_INCOMING)
            == BGP4_DENY)
        {
            /* Route is Filtered. */
            BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_FILTERED_INPUT);
        }
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   :   Bgp4FiltApplyInputFilterOnRoute                      */
/*                                                                           */
/*  Description     :   This function applies incoming filter policies on the*/
/*                      the input feasible route including comm/ecomm policy */
/*                      If there is a old route in the RIB for a filtered    */
/*                      route, it is deleted and will be advertised as       */
/*                      withdrawn to the corresponding peers.                */
/*                                                                           */
/*  Input(s)        :   pFeasibleRoute - Route on which the input filter     */
/*                      needs to be applied.                                 */
/*                      pPeerInfo -                                          */
/*                         Pointer to the peer information from which the    */
/*                         route is received.                                */
/*                                                                           */
/*  Output(s)       :   pFiltList - List of routes that passed the input     */
/*                      filter mechanism.                                    */
/*                                                                           */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  BGP4_RIB_TREE(BGP4_DFLT_VRFID)                             */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*                                                                           */
/*  Use of Recursion        :  None                                          */
/*  Returns                 :  Pointer to the route profile if it needs to be*/
/*                             processed else NULL                           */
/*****************************************************************************/
tRouteProfile      *
Bgp4FiltApplyInputFilterOnRoute (tRouteProfile * pFeasibleRoute,
                                 tBgp4PeerEntry * pPeerInfo)
{
    /* Variable Declarations */
    tRouteProfile      *pRtProfile = NULL;
    UINT4               u4Med = 0;
    UINT4               u4LocalPref = 0;
    UINT4               u4CurFiltStatus = BGP4_FALSE;
    UINT4               u4NewFiltStatus = BGP4_TRUE;
    INT4                i4RetVal = 0;

    if ((BGP4_RT_GET_FLAGS (pFeasibleRoute) & BGP4_RT_FILTERED_INPUT) ==
        BGP4_RT_FILTERED_INPUT)
    {
        u4CurFiltStatus = BGP4_TRUE;
    }

    i4RetVal = CommApplyRoutesInboundFilterPolicy (pFeasibleRoute);
    if (i4RetVal == COMM_SUCCESS)
    {
        /* Route has cleared the COMMUNITIES filter policy. Apply
         * the EXTENDED COMMUNITY filter policy. */
        i4RetVal = EcommApplyRoutesInboundFilterPolicy (pFeasibleRoute);
        if (i4RetVal == EXT_COMM_SUCCESS)
        {
            /* Route has cleared the EXTENDED COMMUUNITIES filter policy.
             * Apply other filter policy. */
            if (Bgp4UpdateFilter (pPeerInfo, pFeasibleRoute, BGP4_INCOMING)
                == BGP4_ALLOW)
            {
                /* Route is not filtered out. */
                u4NewFiltStatus = BGP4_FALSE;
            }
        }
    }

    /* Apply configured Local Preference for the received routes */
    u4LocalPref = Bgp4ConfigLP (pPeerInfo, pFeasibleRoute, BGP4_INCOMING);

    /* Apply Configured MED on Received Route */
    u4Med = Bgp4ConfigMED (pPeerInfo, pFeasibleRoute, BGP4_INCOMING);

    /* Check whether there is any change in the previous and current status. */
    if ((u4LocalPref != BGP4_RT_LOCAL_PREF (pFeasibleRoute)) ||
        (u4Med != BGP4_RT_MED (pFeasibleRoute)) ||
        (u4CurFiltStatus != u4NewFiltStatus))
    {
        /* There is a change in the configuration along the incoming direction
         * for this route. This situation is quiet tricky. If the route
         * is INVALID earlier, just update the new LP, MED and Filter status
         * and return. Else if the route is Filtered earlier, withdraw the
         * route from RIB and allow the same route profile to be added back
         * to the Peer's RCVD_LIST. Else if the route is valid, then need to
         * allocate a new route profile and add the new route profile with the
         * updated values to the peer's RCVD_LIST
         */
        if (((BGP4_RT_GET_FLAGS (pFeasibleRoute) & BGP4_RT_OVERLAP) ==
             BGP4_RT_OVERLAP) ||
            ((BGP4_RT_GET_FLAGS (pFeasibleRoute) & BGP4_RT_NEXT_HOP_UNKNOWN) ==
             BGP4_RT_NEXT_HOP_UNKNOWN) ||
            ((BGP4_RT_GET_FLAGS (pFeasibleRoute) & BGP4_RT_DAMPED) ==
             BGP4_RT_DAMPED) ||
            ((BGP4_RT_GET_FLAGS (pFeasibleRoute) & BGP4_RT_HISTORY) ==
             BGP4_RT_HISTORY))
        {
            /* Route is invalid. */
            BGP4_RT_LOCAL_PREF (pFeasibleRoute) = u4LocalPref;
            BGP4_RT_MED (pFeasibleRoute) = u4Med;
            if (u4NewFiltStatus == BGP4_TRUE)
            {
                /* The route is Filtered as per the current policy. */
                BGP4_RT_SET_FLAG (pFeasibleRoute, BGP4_RT_FILTERED_INPUT);
            }
            else
            {
                /* The route is not Filtered as per the current policy. */
                BGP4_RT_RESET_FLAG (pFeasibleRoute, BGP4_RT_FILTERED_INPUT);
            }
            return (NULL);
        }
        else if (u4CurFiltStatus == BGP4_TRUE)
        {
            /* The route is currently invalid. So just withdraw the route */
#ifdef RFD_WANTED
            BGP4_RT_SET_FLAG (pFeasibleRoute, BGP4_RT_NO_RFD);
#endif
            Bgp4RibhProcessWithdRoute (pPeerInfo, pFeasibleRoute, NULL);
            if (u4NewFiltStatus == BGP4_FALSE)
            {
                BGP4_RT_RESET_FLAG (pFeasibleRoute, BGP4_RT_FILTERED_INPUT);
            }
            pRtProfile = pFeasibleRoute;
#ifdef RFD_WANTED
            BGP4_RT_SET_FLAG (pFeasibleRoute, BGP4_RT_NO_RFD);
#endif
        }
        else
        {
            /* The route is currently valid. Allocate a new route profile and
             * update it. */
            pRtProfile = Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
            if (pRtProfile == NULL)
            {
                /* Allocation FAILS. We are not able to handle the status
                 * change for this route.
                 */
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Allocate Route Profile for SOFT-IN"
                               "change route FAILS!!!\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerInfo))));
                return (NULL);
            }
            /* Update the new route profile with the existing one. */
            BGP4_LINK_INFO_TO_PROFILE (BGP4_RT_BGP_INFO (pFeasibleRoute),
                                       pRtProfile);
            BGP4_RT_PEER_ENTRY (pRtProfile)
                = BGP4_RT_PEER_ENTRY (pFeasibleRoute);
            BGP4_RT_PROTOCOL (pRtProfile) = BGP4_RT_PROTOCOL (pFeasibleRoute);
            Bgp4CopyNetAddressStruct
                (&(BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
                 (BGP4_RT_NET_ADDRESS_INFO (pFeasibleRoute)));

            if ((u4Med != BGP4_INV_MEDLP)
                && (u4Med !=
                    BGP4_INFO_RCVD_MED (BGP4_RT_BGP_INFO (pFeasibleRoute))))
            {
                BGP4_RT_MED (pFeasibleRoute) = u4Med;
            }
            if ((u4LocalPref != BGP4_INV_MEDLP)
                && (u4LocalPref !=
                    BGP4_INFO_RCVD_LOCAL_PREF (BGP4_RT_BGP_INFO
                                               (pFeasibleRoute))))
            {
                BGP4_RT_LOCAL_PREF (pFeasibleRoute) = u4LocalPref;
            }

            if (u4NewFiltStatus == BGP4_TRUE)
            {
                BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_FILTERED_INPUT);
            }
        }
        /* Update the Local_Pref and MED */
        BGP4_RT_LOCAL_PREF (pRtProfile) = u4LocalPref;
        BGP4_RT_MED (pRtProfile) = u4Med;
        return (pRtProfile);
    }
    return (NULL);
}

/*****************************************************************************/
/* Function Name : Bgp4FiltApplyLPMED                                        */
/* Description   : This function applies the LP/MED values to be used for    */
/*                 decision process using the configured LP/MED Tables       */
/* Input(s)      : Peer information to which route is going to be advt.      */
/*                 (pPeerInfo),                                              */
/*                 Feasible Route-Profile  List                              */
/*                 (pFeasRtList),                                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4FiltApplyLPMED (tBgp4PeerEntry * pPeerInfo, tTMO_SLL * pFeasRtList)
{
    /* Variable declarations */
    tLinkNode          *pLinkNode = NULL;
    tRouteProfile      *pRtProfile = NULL;

    TMO_SLL_Scan (pFeasRtList, pLinkNode, tLinkNode *)
    {
        pRtProfile = pLinkNode->pRouteProfile;
        /* Apply Configured Degree of Preference on Received Route */
        BGP4_RT_LOCAL_PREF (pRtProfile) =
            Bgp4ConfigLP (pPeerInfo, pRtProfile, BGP4_INCOMING);

        /* Apply Configured MED on Received Route */
        BGP4_RT_MED (pRtProfile) =
            Bgp4ConfigMED (pPeerInfo, pRtProfile, BGP4_INCOMING);
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4FiltOutBoundSoftConfigHandler                         */
/* Description   : This function handles outbound soft-reconfiguration for a */
/*                 given peer. If PeerAddress is 0, then outbound soft-      */
/*                 reconfig is applied for all peers for all negotiated      */
/*                 <AFI,SAFI> pairs.                                         */
/* Input(s)      : u4PeerAddr - Peer Remote Address                          */
/*                 u2Afi -  Address Family of BGP Peer                       */
/*                 u1Safi - Sub-address family of BGP Peer                   */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4FiltOutBoundSoftConfigHandler (UINT4 u4Context, tAddrPrefix PeerAddr,
                                   UINT2 u2Afi, UINT1 u1Safi)
{
    /* Variable Declarations */
    tAddrPrefix         AddrPrefix;
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tPeerNode          *pSoftCfgPeerNode = NULL;
    tAfiSafiNode       *pPendReq = NULL;
    INT4                i4Status = BGP4_FALSE;
    UINT4               u4NegMask = 0;
    UINT4               u4AsafiMask = 0;
    UINT4               u4AsafiIndex = 0;
    UINT4               u4TempAsafiIndex = 0;
    UINT1               u1IsRefReqForAllPeer = BGP4_FALSE;
    UINT2               u2TempAfi = 0;
    UINT2               u2TempSafi = 0;
    INT4                i4RetStatus = BGP4_FAILURE;

    Bgp4InitAddrPrefixStruct (&(AddrPrefix),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddr));
    if ((PrefixMatch (PeerAddr, AddrPrefix)) == BGP4_TRUE)
    {
        /* Outbound Soft Request for all peers */
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                  BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                  "\tOutbound soft reconfiguration request is initiated "
                  "for all peers.\n");
        u1IsRefReqForAllPeer = BGP4_TRUE;
    }
    else
    {
        /* Outbound Soft Request for Specific peers */
        BGP4_TRC_ARG1 (&(PeerAddr), BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                       "\tOutbound soft reconfiguration request is initiated "
                       "for peer %s.\n",
                       Bgp4PrintIpAddr (PeerAddr.au1Address, u2Afi));
        u1IsRefReqForAllPeer = BGP4_FALSE;
    }

    if ((u2Afi != 0) && (u1Safi != 0))
    {
        /* Request is for Specific <AFI,SAFI>. */
        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                       "\tOutbound soft reconfiguration request is initiated "
                       "for address family %d and sub address family %d.\n",
                       u2Afi, u1Safi);
        u2TempAfi = u2Afi;
        u2TempSafi = u1Safi;
        Bgp4GetAfiSafiIndex (u2TempAfi, u2TempSafi, &u4TempAsafiIndex);
    }

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeerEntry, tBgp4PeerEntry *)
    {
        if ((PrefixMatch (AddrPrefix,
                          BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)))
            == BGP4_TRUE)
        {
            /* Current Peer Prefix matches with the Previous Peer's
             * Prefix. So this is the duplicate entry. */
            continue;
        }
        else
        {
            Bgp4CopyAddrPrefixStruct (&(AddrPrefix),
                                      BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));
        }

        if (u1IsRefReqForAllPeer == BGP4_FALSE)
        {
            /* Request for Specific Peer. */
            if (PrefixMatch (AddrPrefix, PeerAddr) == BGP4_FALSE)
            {
                /* Peer does not match the input. */
                continue;
            }
        }
        else
        {
            /* Request for all Peers. */
            if (BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix) !=
                BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddr))
            {
                /* Peer Address Family not matching. */
                continue;
            }
        }

        if (BGP4_PEER_STATE (pPeerEntry) != BGP4_ESTABLISHED_STATE)
        {
            /* Peer is not in Established State. No need for any
             * processing. */
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Peer is not in established state. Soft "
                           "reconfiguration process is not done.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerEntry))));
            if (u1IsRefReqForAllPeer == BGP4_FALSE)
            {
                if ((u2Afi != 0) && (u1Safi != 0))
                {
                    if (BGP4_PEER_AFI_SAFI_INSTANCE (pPeerEntry,
                                                     u4TempAsafiIndex) != NULL)
                    {
                        BGP4_SOFTCFG_OUT_REQ_PEER (pPeerEntry,
                                                   u4TempAsafiIndex) = 0;
                    }
                    break;
                }
            }
            else
            {
                continue;
            }
        }

        for (u4AsafiIndex = 0; u4AsafiIndex < BGP4_MPE_ADDR_FAMILY_MAX_INDEX;
             u4AsafiIndex++)
        {
            /* Check whether the Soft-Outbound request if for specific
             * <AFI,SAFI> pair or for all the supported <AFI,SAFI>.
             * If <AFI,SAFI> is <0,0>, then the request is for all
             * the supported <AFI,SAFI>. */
            if ((u2Afi != 0) && (u1Safi != 0))
            {
                /* Request is for Specific <AFI,SAFI>. */
                if (u4TempAsafiIndex != u4AsafiIndex)
                {
                    continue;
                }
            }
            else
            {
                /* Get the <AFI,SAFI> index and mask */
                i4RetStatus =
                    Bgp4GetAfiSafiFromIndex (u4AsafiIndex, &u2TempAfi,
                                             &u2TempSafi);
                if (i4RetStatus == BGP4_FAILURE)
                {
                    continue;
                }
            }
            BGP4_GET_AFISAFI_MASK (u2TempAfi, u2TempSafi, u4AsafiMask);

            /* Check whether the Peer support this <AFI,SAFI> instance. */
            switch (u4AsafiIndex)
            {
                case BGP4_IPV4_UNI_INDEX:
                    u4NegMask = CAP_NEG_IPV4_UNI_MASK;
                    break;
#ifdef L3VPN
                case BGP4_IPV4_LBLD_INDEX:
                    u4NegMask = CAP_NEG_LBL_IPV4_MASK;
                    break;
                case BGP4_VPN4_UNI_INDEX:
                    u4NegMask = CAP_NEG_VPN4_UNI_MASK;
                    break;
#endif
#ifdef VPLSADS_WANTED
                case BGP4_L2VPN_VPLS_INDEX:
                    u4NegMask = CAP_NEG_L2VPN_VPLS_MASK;
                    break;
#endif
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    u4NegMask = CAP_NEG_IPV6_UNI_MASK;
                    break;
#endif
#ifdef EVPN_WANTED
                case BGP4_L2VPN_EVPN_INDEX:
                    u4NegMask = CAP_NEG_L2VPN_EVPN_MASK;
                    break;
#endif
            }

            if (((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) != 0) &&
                 ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) & u4NegMask) !=
                  u4NegMask)) ||
                ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) == 0) &&
                 (u4NegMask != CAP_NEG_IPV4_UNI_MASK)))
            {
                /* The <AFI,SAFI> instance is not negotiated for this
                 * peer. */
                if ((u2Afi != 0) && (u1Safi != 0))
                {
                    if (BGP4_PEER_AFI_SAFI_INSTANCE (pPeerEntry,
                                                     u4TempAsafiIndex) != NULL)
                    {
                        BGP4_SOFTCFG_OUT_REQ_PEER (pPeerEntry,
                                                   u4TempAsafiIndex) = 0;
                    }
                    break;
                }
                else
                {
                    /* Look for next <AFI,SAFI> */
                    continue;
                }
            }

            BGP4_SOFTCFG_OUT_REQ_PEER (pPeerEntry, u4AsafiIndex) = 0;
            /* Check the current status of the peer */
            switch (BGP4_GET_PEER_CURRENT_STATE (pPeerEntry))
            {
                case BGP4_PEER_READY:
                case BGP4_PEER_STALE_DEL_INPROGRESS:
                    /* Add the peer to the soft-out-reconfig list */
                    BGP_PEER_NODE_CREATE (pSoftCfgPeerNode);
                    if (pSoftCfgPeerNode == NULL)
                    {
                        BGP4_TRC_ARG1 (&
                                       (BGP4_PEER_REMOTE_ADDR_INFO
                                        (pPeerEntry)), BGP4_TRC_FLAG,
                                       BGP4_ALL_FAILURE_TRC | BGP4_EVENTS_TRC |
                                       BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                       "\tPEER %s : Memory Allocation to "
                                       "handle Route Refresh Request "
                                       "FAILED!!!\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeerEntry),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeerEntry))));
                        gu4BgpDebugCnt[MAX_BGP_PEER_NODE_SIZING_ID]++;
                        return BGP4_FAILURE;
                    }
                    pSoftCfgPeerNode->pPeer = pPeerEntry;
                    BGP4_PEER_SOFTCONFIG_INITIAL_AFISAFI (pPeerEntry) =
                        u4AsafiMask;
                    BGP4_PEER_SOFTCONFIG_OUTBOUND_AFI (pPeerEntry) = u2TempAfi;
                    BGP4_PEER_SOFTCONFIG_OUTBOUND_SAFI (pPeerEntry) =
                        u2TempSafi;
                    Bgp4InitNetAddressStruct (&
                                              (BGP4_PEER_INIT_NETADDR_INFO
                                               (pPeerEntry)), u2TempAfi,
                                              (UINT1) u2TempSafi);
                    TMO_SLL_Add (BGP4_SOFTCFG_OUTBOUND_LIST (u4Context),
                                 &pSoftCfgPeerNode->TSNext);
                    BGP4_SET_PEER_CURRENT_STATE (pPeerEntry,
                                                 BGP4_PEER_SOFTCONFIG_OUTBOUND_INPROGRESS);
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                                   BGP4_TRC_FLAG, BGP4_PEER_CON_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : Added the peer to soft configuration outbound list "
                                   "to handle Route Refresh Request.\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerEntry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerEntry))));
                    break;

                case BGP4_PEER_SOFTCONFIG_OUTBOUND_INPROGRESS:
                    if ((BGP4_PEER_SOFTCONFIG_OUTBOUND_AFI (pPeerEntry) ==
                         u2TempAfi) &&
                        (BGP4_PEER_SOFTCONFIG_OUTBOUND_SAFI (pPeerEntry) ==
                         u2TempSafi))
                    {
                        /* Soft-Out is already in-progress for this
                         * <AFI,SAFI> So the new request is rejected. */
                        BGP4_TRC_ARG1 (&
                                       (BGP4_PEER_REMOTE_ADDR_INFO
                                        (pPeerEntry)), BGP4_TRC_FLAG,
                                       BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                                       "\tPEER %s : The Outbound route refresh request is "
                                       "already in-progress for this peer. So new request is rejected.\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeerEntry),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeerEntry))));
                        break;
                    }
                    /* For other <AFI,SAFI>, need to add the request in Pending
                     * list and process it later. For this reason, allow the
                     * code to FALL-THROUGH and no break needed. */
                case BGP4_PEER_SOFTCONFIG_INBOUND_INPROGRESS:
                case BGP4_PEER_REUSE_INPROGRESS:
                    i4Status = Bgp4IsAfiSafiOutboundReqPending (pPeerEntry,
                                                                u4AsafiMask);
                    if (i4Status == BGP4_TRUE)
                    {
                        break;
                    }
                    else
                    {
                        AFI_SAFI_NODE_CREATE (pPendReq);
                        if (pPendReq == NULL)
                        {
                            return BGP4_FAILURE;
                        }
                        TMO_SLL_Init_Node (&pPendReq->TsAfiSafiNext);
                        pPendReq->u4AfiSafiMask = u4AsafiMask;
                        TMO_SLL_Add (BGP4_PEER_SOFTCONFIG_OUT_PEND_LIST
                                     (pPeerEntry), &pPendReq->TsAfiSafiNext);
                    }
                    BGP4_SET_PEER_PEND_FLAG (pPeerEntry,
                                             BGP4_PEER_SOFTCONFIG_OUTBOUND_PEND);
                    break;
                case BGP4_PEER_INIT_INPROGRESS:
                case BGP4_PEER_DEINIT_INPROGRESS:
                default:
                    /* Under these situation, SOFT-OUTBOUND configuration is
                     * not accepted. */
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                                   BGP4_TRC_FLAG, BGP4_PEER_CON_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : The peer initialisation or de-initialisation is "
                                   "in-progress. It is invalid peer state to handle route refresh request.\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerEntry),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerEntry))));

                    break;
            }

            if ((u2Afi != 0) && (u1Safi != 0))
            {
                /* Request is for specific <AFI,SAFI> */
                break;
            }
        }
        if (u1IsRefReqForAllPeer == BGP4_FALSE)
        {
            /* Request is for single peer only and it has been processed.
             * So break out of the loop. */
            break;
        }
    }                            /* TMO_SLL_Scan */
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   :   Bgp4SoftOutPendHandler                               */
/*                                                                           */
/*  Description     :   Handles the pending route-request requests           */
/*                                                                           */
/*  Input(s)        :   pPeerInfo - pointer to the peer information          */
/*                                                                           */
/*  Output(s)       :   None                                                 */
/*                                                                           */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  BGP4_PEERENTRY_HEAD(BGP4_DFLT_VRFID)                       */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*                                                                           */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :                                            */
/*****************************************************************************/
INT4
Bgp4SoftOutPendHandler (tBgp4PeerEntry * pPeerInfo)
{
    /* Variable Declarations */
    tPeerNode          *pRtRefPeerNode = NULL;
    tAfiSafiNode       *pAsafiMask = NULL;
    UINT4               u4AsafiMask = 0;
    UINT2               u2Afi = 0;
    UINT2               u2Safi = 0;

    if (BGP4_PEER_STATE (pPeerInfo) != BGP4_ESTABLISHED_STATE)
    {
        return BGP4_FAILURE;
    }

    pAsafiMask = (tAfiSafiNode *)
        TMO_SLL_First (BGP4_PEER_SOFTCONFIG_OUT_PEND_LIST (pPeerInfo));
    if (pAsafiMask == NULL)
    {
        /* List is empty. Clear the pend flag. */
        BGP4_RESET_PEER_PEND_FLAG (pPeerInfo,
                                   BGP4_PEER_SOFTCONFIG_OUTBOUND_PEND);
        return BGP4_SUCCESS;
    }

    u4AsafiMask = pAsafiMask->u4AfiSafiMask;

    Bgp4GetAfiSafiFromMask (u4AsafiMask, &u2Afi, &u2Safi);

    /* Check the current status of the peer */
    switch (BGP4_GET_PEER_CURRENT_STATE (pPeerInfo))
    {
        case BGP4_PEER_READY:
            BGP_PEER_NODE_CREATE (pRtRefPeerNode);
            if (pRtRefPeerNode == NULL)
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : Memory Allocation to handle "
                               "Route Refresh Request FAILED!!!\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerInfo))));
                gu4BgpDebugCnt[MAX_BGP_PEER_NODE_SIZING_ID]++;
                return BGP4_FAILURE;
            }
            pRtRefPeerNode->pPeer = pPeerInfo;
            BGP4_PEER_SOFTCONFIG_INITIAL_AFISAFI (pPeerInfo) = u4AsafiMask;
            BGP4_PEER_SOFTCONFIG_OUTBOUND_AFI (pPeerInfo) = u2Afi;
            BGP4_PEER_SOFTCONFIG_OUTBOUND_SAFI (pPeerInfo) = u2Safi;
            Bgp4InitAddrPrefixStruct (&(BGP4_PEER_INIT_PREFIX_INFO (pPeerInfo)),
                                      u2Afi);
            TMO_SLL_Add (BGP4_SOFTCFG_OUTBOUND_LIST
                         (BGP4_PEER_CXT_ID (pPeerInfo)),
                         &pRtRefPeerNode->TSNext);

            BGP4_SET_PEER_CURRENT_STATE (pPeerInfo,
                                         BGP4_PEER_SOFTCONFIG_OUTBOUND_INPROGRESS);
            TMO_SLL_Delete (BGP4_PEER_SOFTCONFIG_OUT_PEND_LIST (pPeerInfo),
                            &pAsafiMask->TsAfiSafiNext);
            if (TMO_SLL_Count (BGP4_PEER_SOFTCONFIG_OUT_PEND_LIST (pPeerInfo))
                == 0)
            {
                BGP4_RESET_PEER_PEND_FLAG (pPeerInfo,
                                           BGP4_PEER_SOFTCONFIG_OUTBOUND_PEND);
            }
            AFI_SAFI_NODE_FREE ((UINT1 *) pAsafiMask);
            break;

        case BGP4_PEER_SOFTCONFIG_INBOUND_INPROGRESS:
        case BGP4_PEER_SOFTCONFIG_OUTBOUND_INPROGRESS:
        case BGP4_PEER_INIT_INPROGRESS:
        case BGP4_PEER_DEINIT_INPROGRESS:
        case BGP4_PEER_REUSE_INPROGRESS:
        default:
            return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

#endif /* BGP4FILTER_C */
