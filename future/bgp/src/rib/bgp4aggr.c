/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
  * $Id: bgp4aggr.c,v 1.46 2017/09/15 06:19:54 siva Exp $
 *
 * Description: This file contains various module which are used 
 *              internally by the aggregator module. This Aggregation  
 *              submodule is called by the RIB handler after 
 *              processing the updates and before giving the updates 
 *              to the Advertisement Handler.
 *
 *******************************************************************/
#ifndef   BGP4AGGR_C
#define   BGP4AGGR_C

#include "bgp4com.h"

/*****************************************************************************/
/* Function Name : Bgp4AggregationHandler                                    */
/* Description   : This routine handles all the aggregation related          */
/*                 operations. This function process all the configured      */
/*                 aggregate entries and if any change needs to be done,     */
/*                 process all those changes.                                */
/* Input(s)      : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return        : BGP4_TRUE  - if more routes available for processing.     */
/*               : BGP4_FALSE - if no more routes available for processing.  */
/*                 BGP4_RELINQUISH - if external events requires CPU         */
/*****************************************************************************/
INT4
Bgp4AggregationHandler (UINT4 u4Context)
{
    tBgp4AggrEntry     *pAggrEntry = NULL;
    tRouteProfile      *pBestRoute = NULL;
    UINT4               u4Index = 0;
    INT4                i4RetVal = BGP4_SUCCESS;
    INT4                i4Status = BGP4_FALSE;
    UINT1               u1CanAggrRtBeAdvt = BGP4_FALSE;
    tRouteProfile      *pAggrRoute = NULL;

    for (u4Index = 0; u4Index < BGP4_MAX_AGGR_ENTRIES; u4Index++)
    {
        pAggrEntry = (&(BGP4_AGGRENTRY[u4Index]));
        if (BGP4_AGGRENTRY_VRFID (u4Index) != u4Context)
        {
            continue;
        }
        if ((BGP4_AGGR_GET_STATUS (pAggrEntry) & BGP4_AGGR_CREATE_IN_PROGRESS)
            == BGP4_AGGR_CREATE_IN_PROGRESS)
        {
            /* Process Aggregate entry Admin Status UP. */
            i4RetVal = Bgp4AggrProcessAdminUp (u4Index);
            if (i4RetVal == BGP4_INIT_COMPLETE)
            {
                /* Reset the Aggregate entry Init route info. */
                Bgp4InitNetAddressStruct (&
                                          (gBgpNode.aBGP4Aggrtbl[u4Index].
                                           AggrInitAddr),
                                          BGP4_AGGRENTRY_IPADDR_FAMILY
                                          (u4Index),
                                          (UINT1)
                                          BGP4_AGGRENTRY_IPSUBADDR_FAMILY
                                          (u4Index));

                if (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_UP)
                {
                    /* Check and Advertise the new aggregated route. */
                    u1CanAggrRtBeAdvt = Bgp4AggrCanAggrRouteBeAdvt
                        (BGP4_AGGRENTRY_AGGR_ROUTE (u4Index), &pBestRoute);
                    if (u1CanAggrRtBeAdvt == BGP4_TRUE)
                    {
                        if (pBestRoute == NULL)
                        {
                            /* Aggregate route is the new best route. */
                            Bgp4AggrAddAggrRouteToAdvtList (u4Index, 0);
                        }
                        else
                        {
                            /* BGP route exist. Check for the status of this
                             * route and process accordingly */
                            if (((BGP4_RT_GET_FLAGS (pBestRoute)) &
                                 (BGP4_RT_IN_FIB_UPD_LIST)) ==
                                BGP4_RT_IN_FIB_UPD_LIST)
                            {
                                BGP4_RT_SET_FLAG (pBestRoute,
                                                  BGP4_RT_ADVT_NOTTO_EXTERNAL);
                            }
                            else if (((BGP4_RT_GET_FLAGS (pBestRoute)) &
                                      (BGP4_RT_IN_EXT_PEER_ADVT_LIST)) ==
                                     BGP4_RT_IN_EXT_PEER_ADVT_LIST)
                            {
                                /* Remove the route from that list. */
                                BGP4_RT_RESET_FLAG (pBestRoute,
                                                    (BGP4_RT_REPLACEMENT |
                                                     BGP4_RT_IN_EXT_PEER_ADVT_LIST));
                                Bgp4DshDelinkRouteFromList
                                    (BGP4_EXT_PEERS_ADVT_LIST (u4Context),
                                     pBestRoute, BGP4_EXT_PEER_LIST_INDEX);
                            }
                            if (((BGP4_RT_GET_FLAGS (pBestRoute)) &
                                 (BGP4_RT_IN_INT_PEER_ADVT_LIST)) ==
                                BGP4_RT_IN_INT_PEER_ADVT_LIST)
                            {
                                /* Remove the route from that list. */
                                BGP4_RT_RESET_FLAG (pBestRoute,
                                                    (BGP4_RT_REPLACEMENT |
                                                     BGP4_RT_IN_INT_PEER_ADVT_LIST));
                                Bgp4DshDelinkRouteFromList
                                    (BGP4_INT_PEERS_ADVT_LIST (u4Context),
                                     pBestRoute, BGP4_INT_PEER_LIST_INDEX);
                            }
                            Bgp4AggrAddAggrRouteToAdvtList (u4Index,
                                                            BGP4_RT_REPLACEMENT);
                        }
                    }
                }
                /* Update the status of the aggregation entry. */
                BGP4_AGGR_RESET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                        BGP4_AGGR_CREATE_IN_PROGRESS);
                BGP4_AGGR_SET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                      BGP4_AGGR_NO_REAGGREGATE);
            }
            else
            {
                /* This entry has more processing left to be performed. */
                i4Status = BGP4_TRUE;
            }
        }
        else if ((BGP4_AGGR_GET_STATUS (pAggrEntry) &
                  BGP4_AGGR_DELETE_IN_PROGRESS) == BGP4_AGGR_DELETE_IN_PROGRESS)
        {
            /* Process Aggregate entry Admin Status UP. */
            i4RetVal = Bgp4AggrProcessAdminDown (u4Index);
            if (i4RetVal == BGP4_INIT_COMPLETE)
            {
                BGP4_AGGR_RESET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                        BGP4_AGGR_DELETE_IN_PROGRESS);

                if ((BGP4_AGGRENTRY_AGGR_STATUS (u4Index) &
                     BGP4_AGGR_PENDING_ADVT_CHG) == BGP4_AGGR_PENDING_ADVT_CHG)
                {
                    BGP4_AGGR_RESET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                            BGP4_AGGR_PENDING_ADVT_CHG);
                    /* Ulta the current advertise status. */
                    if (BGP4_AGGRENTRY_ADV_TYPE (u4Index) == BGP4_SUMMARY)
                    {
                        BGP4_AGGRENTRY_ADV_TYPE (u4Index) = BGP4_ALL;
                    }
                    else
                    {
                        BGP4_AGGRENTRY_ADV_TYPE (u4Index) = BGP4_SUMMARY;
                    }
                }

                if ((BGP4_AGGRENTRY_AGGR_STATUS (u4Index) &
                     BGP4_AGGR_PENDING_CREATE) == BGP4_AGGR_PENDING_CREATE)
                {
                    BGP4_AGGR_RESET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                            BGP4_AGGR_PENDING_CREATE);

                    BGP4_AGGR_SET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                          BGP4_AGGR_CREATE_IN_PROGRESS);
                }
                else
                {
                    /* Free the aggregate route. */
#ifdef L3VPN
                    /* release the aggregate label */
                    if (BGP4_RT_LABEL_CNT (BGP4_AGGRENTRY_AGGR_ROUTE (u4Index))
                        > 0)
                    {
                        Bgp4DelBgp4Label (BGP4_RT_LABEL_INFO
                                          (BGP4_AGGRENTRY_AGGR_ROUTE
                                           (u4Index)));
                    }
#endif
                    Bgp4DshReleaseRtInfo (BGP4_AGGRENTRY_AGGR_ROUTE (u4Index));
                    BGP4_AGGRENTRY_AGGR_ROUTE (u4Index) = NULL;
                    BGP4_AGGR_RESET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                            BGP4_AGGR_ALL_FLAG);
                    Bgp4InitAggrEntry (&(BGP4_AGGRENTRY[u4Index]));

                }
            }
            else
            {
                /* This entry has more processing left to be performed. */
                i4Status = BGP4_TRUE;
            }
        }
        else if ((BGP4_AGGR_GET_STATUS (pAggrEntry) & BGP4_AGGR_REAGGREGATE)
                 == BGP4_AGGR_REAGGREGATE)
        {
            /* Reaggregation flag is set. Means this entry has gone 
             * through some changes. So need to re-aggregate this
             * entry and do appropriate action. */
            Bgp4AggrProcessAggregation (u4Index);
        }
        else if ((BGP4_AGGR_GET_STATUS (pAggrEntry) & BGP4_AGGR_ADVERTISE)
                 == BGP4_AGGR_ADVERTISE)
        {
            /* Need to advertise this aggregated route. */
            Bgp4AggrAddAggrRouteToAdvtList (u4Index,
                                            (UINT4) (~(BGP4_RT_WITHDRAWN)));
            BGP4_AGGR_RESET_STATUS (pAggrEntry, BGP4_AGGR_ADVERTISE);
        }
        else if (BGP4_AGGRENTRY_AGGR_ROUTE (u4Index) != NULL)
        {
            /* Non-BGP route will be the best route. */
            u1CanAggrRtBeAdvt = Bgp4AggrCanAggrRouteBeAdvt
                (BGP4_AGGRENTRY_AGGR_ROUTE (u4Index), &pBestRoute);
            pAggrRoute = BGP4_AGGRENTRY_AGGR_ROUTE (u4Index);
            if (pAggrRoute == NULL)
            {
                return (BGP4_FAILURE);
            }
            /* when Non-BGP route is the best route then reset the aggregate route */
            if ((u1CanAggrRtBeAdvt == BGP4_FALSE) &&
                (BGP4_RT_GET_FLAGS (pAggrRoute) & BGP4_RT_BEST))
            {
                Bgp4AggrAddAggrRouteToAdvtList (u4Index, BGP4_RT_WITHDRAWN);
            }
        }
    }
    return (i4Status);
}

/*****************************************************************************/
/* Function Name : Bgp4AggrProcessAdminUp                                    */
/* Description   : This routine handles the admin up status for the given    */
/*                 aggregate table entry.                                    */
/* Input(s)      : Index of Aggregate Table entry (u4Index)                  */
/* Output(s)     : None.                                                     */
/* RETURNS       : BGP4_SUCCESS/BGP4_FAILURE/BGP4_INIT_COMPLETE              */
/*****************************************************************************/
INT4
Bgp4AggrProcessAdminUp (UINT4 u4Index)
{
    tRouteProfile       RtProfile;
    tAddrPrefix         InvAddr;
    tAddrPrefix         AggrAddr;
    tBgp4AggrEntry     *pAggrEntry = NULL;
    tFilteringRMap      SuppressFilter;
    tFilteringRMap      AdvFilter;
    tFilteringRMap      AttFilter;
    tRouteProfile      *pAggrRoute = NULL;
    tRouteProfile      *pRtProfileList = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pNextRt = NULL;
    tBgp4PeerEntry     *pPeer = NULL;
    VOID               *pRibNode = NULL;
    UINT4               u4AggrLen = 0;
    UINT4               u4AddCurRoute = BGP4_TRUE;
    UINT4               u4RtCnt = 0;
    UINT4               u4Status = 0;
    UINT4               u4AsafiMask;
    INT4                i4Status = BGP4_SUCCESS;
    UINT1               u1AddrMatch = BGP4_FALSE;
    UINT4               u4Context = 0;
    UINT1               u1ExactAddrMatch = BGP4_FALSE;
    tRouteProfile      *pTempRtProfile = NULL;

    MEMSET (&SuppressFilter, 0, sizeof (tFilteringRMap));
    MEMSET (&AdvFilter, 0, sizeof (tFilteringRMap));
    MEMSET (&AttFilter, 0, sizeof (tFilteringRMap));

    u4Context = BGP4_AGGRENTRY_VRFID (u4Index);
    if (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_DOWN)
    {
        return BGP4_INIT_COMPLETE;
    }

    pAggrEntry = (&(BGP4_AGGRENTRY[u4Index]));
    pAggrRoute = BGP4_AGGRENTRY_AGGR_ROUTE (u4Index);

    u4AggrLen = BGP4_RT_PREFIXLEN (pAggrRoute);

    Bgp4InitAddrPrefixStruct (&(AggrAddr), BGP4_RT_AFI_INFO (pAggrRoute));
    Bgp4InitAddrPrefixStruct (&InvAddr, BGP4_RT_AFI_INFO (pAggrRoute));
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pAggrRoute),
                           BGP4_RT_SAFI_INFO (pAggrRoute), u4AsafiMask);
    /* Here we get the routemap status ie. whether route map is present
     * or not. It is done for all 3 maps, (advertise-map, suppress-map,
     * attribute-map)*/

    if (pAggrEntry->u1AttMapLen > 0)
    {
        MEMCPY (AttFilter.au1DistInOutFilterRMapName,
                pAggrEntry->au1AttMapName, pAggrEntry->u1AttMapLen);
#ifdef ROUTEMAP_WANTED
        u4Status = RMapGetInitialMapStatus (pAggrEntry->au1AttMapName);
#endif
        if (u4Status != 0)
        {
            AttFilter.u1Status = FILTERNIG_STAT_ENABLE;
        }
        else
        {
            AttFilter.u1Status = FILTERNIG_STAT_DISABLE;
        }
    }
    if (pAggrEntry->u1SuppressMapLen > 0)
    {
        MEMCPY (SuppressFilter.au1DistInOutFilterRMapName,
                pAggrEntry->au1SuppressMapName, pAggrEntry->u1SuppressMapLen);
#ifdef ROUTEMAP_WANTED
        u4Status = RMapGetInitialMapStatus (pAggrEntry->au1SuppressMapName);
#endif
        if (u4Status != 0)
        {
            SuppressFilter.u1Status = FILTERNIG_STAT_ENABLE;
        }
        else
        {
            SuppressFilter.u1Status = FILTERNIG_STAT_DISABLE;
        }
    }
    if (pAggrEntry->u1AdvMapLen > 0)
    {
        MEMCPY (AdvFilter.au1DistInOutFilterRMapName,
                pAggrEntry->au1AdvMapName, pAggrEntry->u1AdvMapLen);
#ifdef ROUTEMAP_WANTED
        u4Status = RMapGetInitialMapStatus (pAggrEntry->au1AdvMapName);
#endif
        if (u4Status != 0)
        {
            AdvFilter.u1Status = FILTERNIG_STAT_ENABLE;
        }
        else
        {
            AdvFilter.u1Status = FILTERNIG_STAT_DISABLE;
        }
    }
#ifdef L3VPN
    if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
    {
        if (BGP4_RT_GET_FLAGS (pAggrRoute) & BGP4_RT_CONVERTED_TO_VPNV4)
        {
            u4AsafiMask = CAP_MP_IPV4_UNICAST;
        }
    }
#endif
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
#endif
        {
            UINT4               u4Ipaddr;
            PTR_FETCH_4 (u4Ipaddr, BGP4_RT_IP_PREFIX (pAggrRoute));
            u4Ipaddr = ((u4Ipaddr) & (BGP4_MAX_PREFIX <<
                                      (BGP4_MAX_PREFIXLEN -
                                       BGP4_RT_PREFIXLEN (pAggrRoute))));
            PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AggrAddr),
                          u4Ipaddr);
        }
            break;

#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
        {
            tIp6Addr            IPAddr;
            tIp6Addr            IPAddr1;

            MEMCPY (&IPAddr1.u1_addr[0], BGP4_RT_IP_PREFIX (pAggrRoute),
                    BGP4_IPV6_PREFIX_LEN);
            Ip6CopyAddrBits (&IPAddr, &IPAddr1, BGP4_RT_PREFIXLEN (pAggrRoute));
            MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AggrAddr),
                    IPAddr.u1_addr, BGP4_IPV6_PREFIX_LEN);
        }
            break;
#endif
#ifdef L3VPN
        case CAP_MP_VPN4_UNICAST:
        {
            UINT4               u4Ipaddr;
            PTR_FETCH_4 (u4Ipaddr, BGP4_RT_IP_PREFIX (pAggrRoute));
            u4Ipaddr = ((u4Ipaddr) & (BGP4_MAX_PREFIX <<
                                      (BGP4_MAX_PREFIXLEN -
                                       BGP4_RT_PREFIXLEN (pAggrRoute))));
            PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AggrAddr) +
                          BGP4_VPN4_ROUTE_DISTING_SIZE, u4Ipaddr);
        }
            MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AggrAddr),
                    BGP4_RT_ROUTE_DISTING (pAggrRoute),
                    BGP4_VPN4_ROUTE_DISTING_SIZE);
            break;
#endif
        default:
            break;
    }

    /* Check if the Aggregate entry list hold any routes. If yes, but no
     * aggregated route exists, then first create the aggregated route. */
    if (TMO_SLL_Count (BGP4_AGGRENTRY_AGGR_LIST (u4Index)) > 0)
    {
        if ((BGP4_RT_GET_FLAGS (pAggrRoute) & BGP4_RT_AGGREGATED) !=
            BGP4_RT_AGGREGATED)
        {
            /* Though routes are present in list only dummy aggregate
             * route is present. This is possible if route which was
             * previously used to create to aggregated route was withdrawn or
             * replaced even before the creating aggregated route is complete.
             */
            Bgp4AggrReaggregate (pAggrRoute,
                                 BGP4_AGGRENTRY_AGGR_LIST (u4Index));
        }
    }

    /* Obtain the route-info for peer initialization */
    if (MEMCMP (&InvAddr, &(BGP4_AGGRENTRY_INIT_PREFIX_INFO (u4Index)),
                sizeof (tAddrPrefix)) == 0)
    {
        /* Get the first route overlapping route that can be
         * used for aggregation. */
        i4Status = Bgp4RibhGetFirstOverlapMoreSpecificRoute (pAggrRoute,
                                                             &pRtProfileList,
                                                             (VOID **)
                                                             &pRibNode);
        if (i4Status == BGP4_FAILURE)
        {
            /* No overlapping more-specific routes present in RIB */
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                      BGP4_FDB_TRC, BGP4_MOD_NAME,
                      "\tNo overlapping more-specific routes present in RIB.\n");
            return (BGP4_INIT_COMPLETE);
        }
    }
    else
    {
        /* Get the route corresponding to the route-info from the RIB */
        RtProfile.u1Ref = 0;
        RtProfile.u1Protocol = 0;
        RtProfile.p_RPNext = 0;
        RtProfile.u4Flags = 0;
        RtProfile.pRtInfo = 0;
        Bgp4CopyNetAddressStruct (&(RtProfile.NetAddress),
                                  gBgpNode.aBGP4Aggrtbl[u4Index].AggrInitAddr);

        i4Status = Bgp4RibhLookupRtEntry (&RtProfile, u4Context,
                                          BGP4_TREE_FIND_EXACT,
                                          &pRtProfileList, &pRibNode);
        if (i4Status == BGP4_FAILURE)
        {
            /* No routes present in RIB */
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                      BGP4_FDB_TRC, BGP4_MOD_NAME,
                      "\tNo matching route present in RIB for the aggregate route.\n");
            return (BGP4_INIT_COMPLETE);
        }
    }

    pRtProfile = Bgp4DshGetBestRouteFromProfileList (pRtProfileList, u4Context);

    for (;;)
    {
        if (pRtProfile != NULL)
        {
            u4AddCurRoute = BGP4_TRUE;
            /* Check if the current route can be aggregate or not. If yes,
             * aggregate it and process the next route. */

            switch (u4AsafiMask)
            {
                case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
                case CAP_MP_LABELLED_IPV4:
#endif
#ifdef BGP4_IPV6_WANTED
                case CAP_MP_IPV6_UNICAST:
#endif
                    u1AddrMatch = ((AddrMatch (AggrAddr,
                                               BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                               (BGP4_RT_NET_ADDRESS_INFO
                                                (pRtProfile)),
                                               u4AggrLen) == BGP4_TRUE)
                                   && (u4AggrLen <=
                                       BGP4_RT_PREFIXLEN (pRtProfile))) ?
                        BGP4_TRUE : BGP4_FALSE;
                    u1ExactAddrMatch = ((AddrMatch (AggrAddr,
                                                    BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                                    (BGP4_RT_NET_ADDRESS_INFO
                                                     (pRtProfile)),
                                                    u4AggrLen) == BGP4_TRUE)
                                        && (u4AggrLen ==
                                            BGP4_RT_PREFIXLEN (pRtProfile))) ?
                        BGP4_TRUE : BGP4_FALSE;

                    break;
#ifdef L3VPN
                case CAP_MP_VPN4_UNICAST:
                    u1AddrMatch = ((Vpnv4AddrMatch (AggrAddr, pRtProfile,
                                                    u4AggrLen) == BGP4_TRUE) &&
                                   (u4AggrLen < BGP4_RT_PREFIXLEN (pRtProfile)))
                        ? BGP4_TRUE : BGP4_FALSE;
                    break;
#endif
                default:
                    break;
            }

            if (u1AddrMatch == BGP4_TRUE)
            {
                /* Current route is overlapping more-specific route to the 
                 * aggregate route. It can be used for aggregation. 
                 * Check whether the route is from peer going through de-init
                 * If yes, then dont consider this route. This will be taken
                 * care in the normal path. */
                BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_FDB_TRC, BGP4_MOD_NAME,
                               "\tThe route %s mask %s is overlapping more-specific route. "
                               "It cna be used for aggregation if peer is not going through "
                               "de-init.\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               Bgp4PrintIpMask ((UINT1)
                                                BGP4_RT_PREFIXLEN (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)));
                if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
                {
                    pPeer = BGP4_RT_PEER_ENTRY (pRtProfile);
                    if (BGP4_GET_PEER_CURRENT_STATE (pPeer) ==
                        BGP4_PEER_DEINIT_INPROGRESS)
                    {
                        /* Peer going down. Get next route. */
                        u4AddCurRoute = BGP4_FALSE;
                    }
                }

/* Advertise-map Rule
 * 1. Entry-matched with route map + Route-map access is permit = Advertise
 * 2. Entry-matched with route-map + Route-map access is deny = Dont Advertise
 * 3. Entry not matched with route-map + Route-map access is permit = Dont
 *    Advertise
 * 4. Entry not matched with route-map + Route-map is deny = Advertise
 */
                if ((AdvFilter.u1Status == FILTERNIG_STAT_ENABLE) &&
                    (u4AddCurRoute == BGP4_TRUE))
                {
#ifdef ROUTEMAP_WANTED
                    if (Bgp4CheckAggrForAdvMap (pRtProfile,
                                                pAggrEntry->au1AdvMapName) ==
                        BGP4_TRUE)
                    {
                        u4AddCurRoute = BGP4_TRUE;
                    }
                    else
                    {
                        BGP4_RT_RESET_FLAG (pRtProfile,
                                            (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                             BGP4_RT_ADVT_NOTTO_EXTERNAL));
                        Bgp4AddFeasRouteToIntPeerAdvtList (pRtProfile, NULL);
                        Bgp4AddFeasRouteToExtPeerAdvtList (pRtProfile, NULL);
                        u4AddCurRoute = BGP4_FALSE;
                    }
#endif
                }

/* Though my route-matches with the suppress map, but to form attributes,
* we need to take those routes also
* so the attributes are copied and if the an advertise-map is defined
* and in advertise-map if the decision is to not advertise
* that routes are not taken for other operations
* */
                if (u4AddCurRoute == BGP4_TRUE)
                {
                    /* Add the new route to the Aggregate entry list and
                     * aggregate the current route. */
                    Bgp4AggrAggregateBgpInfos (pAggrRoute, pRtProfile,
                                               pAggrEntry->u1AdvType,
                                               pAggrEntry->u1AsSet);

/* Rules for suppress-map
* 1. Route-map Access is permit + route is matched with route-map = Route is
* suppressed not taken for aggregation
* 2. Route-map Access is permit + route is not matched with route-map = Route is
* not suppressed it is taken for aggregation
* 3. Route-map Access is deny + route is matched with route-map = Route is
* not suppressed it is taken for aggregation
* 4. Route-map Access is deny + route is not matched with route-map = Route is
* suppressed and it is not taken for aggregation
*/
                    if (SuppressFilter.u1Status == FILTERNIG_STAT_ENABLE)
                    {
#ifdef ROUTEMAP_WANTED
                        if (Bgp4CheckAggrForSuppMap (pRtProfile,
                                                     pAggrEntry->
                                                     au1SuppressMapName) ==
                            BGP4_FALSE)
                        {
                            u4AddCurRoute = BGP4_TRUE;
                        }
                        else
                        {
                            u4AddCurRoute = BGP4_FALSE;
                            BGP4_RT_RESET_FLAG (pRtProfile,
                                                (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                                 BGP4_RT_ADVT_NOTTO_EXTERNAL));
                            Bgp4AddFeasRouteToIntPeerAdvtList (pRtProfile,
                                                               NULL);
                            Bgp4AddFeasRouteToExtPeerAdvtList (pRtProfile,
                                                               NULL);
                        }
#endif
                    }
                    if (u4AddCurRoute == BGP4_TRUE)
                    {
                        Bgp4DshAddRouteToList (BGP4_AGGRENTRY_AGGR_LIST
                                               (u4Index), pRtProfile, 0);
                        BGP4_RT_SET_FLAG (pAggrRoute, BGP4_RT_AGGREGATED);
                        /* Check for the advertisement policy and decide whether
                         * to withdraw the current route or not. */
                        if (BGP4_AGGRENTRY_ADV_TYPE (u4Index) == BGP4_SUMMARY)
                        {
                            /* Previously advt. routes that are now used for Aggr.
                             * will now be advertised as withdrawn routes to both
                             * internal and external peers. 
                             */
                            Bgp4AggrWithdrawAdvtRoute (pRtProfile);
                            BgpSetRoutelistflag (BGP4_AGGRENTRY_AGGR_LIST
                                                 (u4Index), BGP4_RT_AGGREGATED);
                            if (u1ExactAddrMatch == BGP4_TRUE)
                            {
                                BGP4_RT_SET_FLAG (pRtProfile,
                                                  (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                                   BGP4_RT_ADVT_NOTTO_EXTERNAL |
                                                   BGP4_RT_AGGR_ROUTE |
                                                   BGP4_RT_WITHDRAWN));
                                Bgp4AddWithdRouteToFIBUpdList (pRtProfile);
                                pTempRtProfile = pRtProfile;
                                while (pTempRtProfile->pMultiPathRtNext != NULL)
                                {
                                    pTempRtProfile =
                                        pTempRtProfile->pMultiPathRtNext;
                                    BGP4_RT_SET_FLAG (pTempRtProfile,
                                                      (BGP4_RT_ADVT_NOTTO_INTERNAL
                                                       |
                                                       BGP4_RT_ADVT_NOTTO_EXTERNAL
                                                       | BGP4_RT_AGGR_ROUTE |
                                                       BGP4_RT_WITHDRAWN));
                                    Bgp4AddWithdRouteToFIBUpdList
                                        (pTempRtProfile);

                                }
                            }

                        }
                    }
                    /* Increase the processed route count. */
                    u4RtCnt++;
                }
            }
            else
            {
                /* The route is not a overlapping more-specific route to the
                 * aggregate route. So all routes following this route in the
                 * RIB, will also be non-overlapping. */
                /* Attribute-map
                 * All the attributes are overwritten by this attribute-map*/
                BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_FDB_TRC, BGP4_MOD_NAME,
                               "\tThe route %s mask %s is not overlapping more-specific route.\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               Bgp4PrintIpMask ((UINT1)
                                                BGP4_RT_PREFIXLEN (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)));
                if (AttFilter.u1Status == FILTERNIG_STAT_ENABLE)
                {
#ifdef ROUTEMAP_WANTED
                    Bgp4FillAggrForAttMap (pAggrRoute,
                                           pAggrEntry->au1AttMapName,
                                           pAggrRoute);
#endif
                }
                return (BGP4_INIT_COMPLETE);
            }
        }

        i4Status = Bgp4RibhGetNextEntry (pRtProfileList, u4Context,
                                         &pNextRt, &pRibNode, TRUE);
        if (i4Status == BGP4_FAILURE)
        {
            /* Attribute-map
             * All the attributes are overwritten by this attribute-map*/
            if (AttFilter.u1Status == FILTERNIG_STAT_ENABLE)
            {
#ifdef ROUTEMAP_WANTED
                Bgp4FillAggrForAttMap (pAggrRoute,
                                       pAggrEntry->au1AttMapName, pAggrRoute);
#endif
            }
            /* No routes present in RIB */
            return (BGP4_INIT_COMPLETE);
        }

        pRtProfileList = pNextRt;
        pNextRt = NULL;
        pRtProfile = Bgp4DshGetBestRouteFromProfileList (pRtProfileList,
                                                         u4Context);

        /* Check whether route count has reached the max count value. */
        if ((pRtProfile != NULL) && (u4RtCnt > BGP4_MAX_AGGR_UP_RT2PROCESS))
        {
            /* Store the next route information in the peerentry info  */
            Bgp4CopyNetAddressStruct (&
                                      (gBgpNode.aBGP4Aggrtbl[u4Index].
                                       AggrInitAddr), pRtProfile->NetAddress);
            /* Attribute-map
             * All the attributes are overwritten by this attribute-map*/
            if (AttFilter.u1Status == FILTERNIG_STAT_ENABLE)
            {
#ifdef ROUTEMAP_WANTED
                Bgp4FillAggrForAttMap (pAggrRoute,
                                       pAggrEntry->au1AttMapName, pAggrRoute);
#endif
            }
            break;
        }
    }
    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Bgp4AggrProcessAdminDown                                  */
/* Description   : This routine handles the admin down status for the given  */
/*                 aggregate table entry.                                    */
/* Input(s)      : Index of Aggregate Table entry (u4Index)                  */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4AggrProcessAdminDown (UINT4 u4Index)
{
    tLinkNode          *pLinkNode = NULL;
    tLinkNode          *pTempLinkNode = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tBgp4PeerEntry     *pPeer = NULL;
    UINT4               u4RtCnt = 0;
    tLinkNode          *pRtLink = NULL;
    tRouteProfile      *pTempRtProfile = NULL;
    INT4                i4AggrtblIndex = 0;
    BOOL1               bIsAggrIndexFound = BGP4_FALSE;
    tRouteProfile      *pRtProfileList = NULL;
    VOID               *pRibNode = NULL;
    UINT4               u4AggrLen = 0;
    tRouteProfile      *pAggrRoute = NULL;
    UINT1               u1ExactAddrMatch = BGP4_FALSE;
    UINT4               u4AsafiMask;
    tAddrPrefix         AggrAddr;
    tRouteProfile      *pMultiRtProfile = NULL;

    pAggrRoute = BGP4_AGGRENTRY_AGGR_ROUTE (u4Index);
    u4AggrLen = BGP4_RT_PREFIXLEN (pAggrRoute);
    Bgp4InitAddrPrefixStruct (&(AggrAddr), BGP4_RT_AFI_INFO (pAggrRoute));
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pAggrRoute),
                           BGP4_RT_SAFI_INFO (pAggrRoute), u4AsafiMask);
#ifdef L3VPN
    if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
    {
        if (BGP4_RT_GET_FLAGS (pAggrRoute) & BGP4_RT_CONVERTED_TO_VPNV4)
        {
            u4AsafiMask = CAP_MP_IPV4_UNICAST;
        }
    }
#endif
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
#endif
        {
            UINT4               u4Ipaddr;
            PTR_FETCH_4 (u4Ipaddr, BGP4_RT_IP_PREFIX (pAggrRoute));
            u4Ipaddr = ((u4Ipaddr) & (BGP4_MAX_PREFIX <<
                                      (BGP4_MAX_PREFIXLEN -
                                       BGP4_RT_PREFIXLEN (pAggrRoute))));
            PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AggrAddr),
                          u4Ipaddr);
        }
            break;

#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
        {
            tIp6Addr            IPAddr;
            tIp6Addr            IPAddr1;

            MEMCPY (&IPAddr1.u1_addr[0], BGP4_RT_IP_PREFIX (pAggrRoute),
                    BGP4_IPV6_PREFIX_LEN);
            Ip6CopyAddrBits (&IPAddr, &IPAddr1, BGP4_RT_PREFIXLEN (pAggrRoute));
            MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AggrAddr),
                    IPAddr.u1_addr, BGP4_IPV6_PREFIX_LEN);
        }
            break;
#endif
#ifdef L3VPN
        case CAP_MP_VPN4_UNICAST:
        {
            UINT4               u4Ipaddr;
            PTR_FETCH_4 (u4Ipaddr, BGP4_RT_IP_PREFIX (pAggrRoute));
            u4Ipaddr = ((u4Ipaddr) & (BGP4_MAX_PREFIX <<
                                      (BGP4_MAX_PREFIXLEN -
                                       BGP4_RT_PREFIXLEN (pAggrRoute))));
            PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AggrAddr) +
                          BGP4_VPN4_ROUTE_DISTING_SIZE, u4Ipaddr);
        }
            MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AggrAddr),
                    BGP4_RT_ROUTE_DISTING (pAggrRoute),
                    BGP4_VPN4_ROUTE_DISTING_SIZE);
            break;
#endif
        default:
            return (BGP4_SUCCESS);
    }

    /* Remove the routes from the Agggregate list and advertise to peer 
     * depanding upon the policy. */

    BGP_SLL_DYN_Scan (BGP4_AGGRENTRY_AGGR_LIST (u4Index), pLinkNode,
                      pTempLinkNode, tLinkNode *)
    {
        pRtProfile = pLinkNode->pRouteProfile;
        bIsAggrIndexFound = BGP4_FALSE;
        /* Check whether this route is part of any other Aggregate Entry */
        for (i4AggrtblIndex = 0; i4AggrtblIndex < BGP4_MAX_AGGR_ENTRIES;
             i4AggrtblIndex++)
        {
            if (u4Index != (UINT4) i4AggrtblIndex)
            {
                TMO_SLL_Scan (BGP4_AGGRENTRY_AGGR_LIST (i4AggrtblIndex),
                              pRtLink, tLinkNode *)
                {
                    pTempRtProfile = pRtLink->pRouteProfile;
                    if (pTempRtProfile == pRtProfile)
                    {
                        bIsAggrIndexFound = BGP4_TRUE;
                        break;
                    }
                }
                if (bIsAggrIndexFound == BGP4_TRUE)
                {
                    break;
                }
            }
        }

        if (BGP4_AGGRENTRY_ADV_TYPE (u4Index) == BGP4_SUMMARY)
        {
            /* These routes are suppressed earlier because of
             * the policy. But now since the aggregate entry is
             * set down, these routes needs to be advertised as
             * feasible. */

            /* Check whether the route is from peer going through
             * de-init. If yes, then dont consider this route. This
             * will be taken care in the normal path. */
            if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
            {
                pPeer = BGP4_RT_PEER_ENTRY (pRtProfile);
            }
            if ((pPeer != NULL) &&
                (BGP4_GET_PEER_CURRENT_STATE (pPeer) ==
                 BGP4_PEER_DEINIT_INPROGRESS))
            {
                /* Peer going down. No need for any process. */
            }
            else
            {
                BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_FDB_TRC, BGP4_MOD_NAME,
                               "\tThe route %s mask %s was suppressed because of aggregate "
                               "policy. Now since the aggregate entry is set down advertising "
                               "it as feasible route.\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               Bgp4PrintIpMask ((UINT1)
                                                BGP4_RT_PREFIXLEN (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)));
                if (bIsAggrIndexFound == BGP4_FALSE)
                {
                    u1ExactAddrMatch = ((AddrMatch (AggrAddr,
                                                    BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                                    (BGP4_RT_NET_ADDRESS_INFO
                                                     (pRtProfile)),
                                                    u4AggrLen) == BGP4_TRUE)
                                        && (u4AggrLen ==
                                            BGP4_RT_PREFIXLEN (pRtProfile))) ?
                        BGP4_TRUE : BGP4_FALSE;
                    if (u1ExactAddrMatch == BGP4_TRUE)
                    {
                        Bgp4AddFeasRouteToFIBUpdList (pRtProfile, NULL);
                        pMultiRtProfile = pRtProfile;
                        while (pMultiRtProfile->pMultiPathRtNext != NULL)
                        {
                            pMultiRtProfile = pMultiRtProfile->pMultiPathRtNext;
                            Bgp4AddFeasRouteToFIBUpdList (pMultiRtProfile,
                                                          NULL);
                        }
                    }
                    Bgp4AggrAdvtSuppressedRoute (pRtProfile);
                }
            }
        }
        if (Bgp4RibhLookupRtEntry (pRtProfile,
                                   BGP4_RT_CXT_ID (pRtProfile),
                                   BGP4_TREE_FIND_EXACT, &pRtProfileList,
                                   &pRibNode) == BGP4_SUCCESS)
        {
            if (bIsAggrIndexFound == BGP4_FALSE)
            {
                BGP4_RT_RESET_FLAG (pRtProfileList, BGP4_RT_AGGREGATED);
            }

        }
        /* Remove the route from the Aggregate list. */
        Bgp4DshRemoveNodeFromList (BGP4_AGGRENTRY_AGGR_LIST (u4Index),
                                   pLinkNode);

        u4RtCnt++;
        /* Check whether route count has reached the max count value. */
        if ((u4RtCnt > BGP4_MAX_AGGR_DOWN_RT2PROCESS) &&
            (BGP4_LOCAL_ADMIN_STATUS (BGP4_AGGRENTRY_VRFID (u4Index)) ==
             BGP4_ADMIN_UP))
        {
            return (BGP4_SUCCESS);
        }

    }

    return (BGP4_INIT_COMPLETE);
}

/*****************************************************************************/
/* Function Name : Bgp4AggrCanAggregationBeDone                              */
/* Description   : This routine take a route profile as input and check      */
/*                 whether a matching entry is present in aggregation table  */
/*                 or not.                                                   */
/* Input(s)      : Pointer to the route profile (pRtProfile)                 */
/* Output(s)     : Index of the matching entry in the aggregate entry else 0 */
/* Return(s)     : BGP4_SUCCESS - if entry is found.                         */
/*                 BGP4_FAILURE - if no entry is found.                      */
/*****************************************************************************/
INT4
Bgp4AggrCanAggregationBeDone (tRouteProfile * pRtProfile, UINT4 *pu4Index,
                              INT4 i4CurrIndex)
{
    *pu4Index = 0;

    if (pRtProfile == NULL)
    {
        return (BGP4_FAILURE);
    }

    /* Check whether a matching entry is present for the given route. */
    *pu4Index = Bgp4AggrFindAggrEntry (pRtProfile, i4CurrIndex);
    if (*pu4Index != 0)
    {
        *pu4Index = *pu4Index - BGP4_DECREMENT_BY_ONE;
        return (BGP4_SUCCESS);
    }
    else
    {
        /* No matching aggregate entry present. */
        return (BGP4_FAILURE);
    }
}

/*****************************************************************************/
/* Function Name : Bgp4AggrProcessFeasibleRoute                              */
/* Description   : This routine handles the application of the aggregation   */
/*                 policy over the newly received feasible route.            */
/* Input(s)      : Feasible route (pFeasRoute)                               */
/*               : Old Best Route (pOldBestRt)                               */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4AggrProcessFeasibleRoute (tRouteProfile * pFeasRoute,
                              tRouteProfile * pOldBestRt)
{
    tAddrPrefix         InvPrefix;
    tRouteProfile      *pAggrRoute = NULL;
    UINT4               u4Index = 0;
    INT4                i4RetVal = 0;
    INT4                i4CurrIndex = 0;
    tRouteProfile      *pTempFeasRoute = NULL;
#if defined (VPLSADS_WANTED) || defined (EVPN_WANTED)
    UINT4               u4AsafiMask;
#endif

    Bgp4InitAddrPrefixStruct (&InvPrefix,
                              BGP4_AFI_IN_ADDR_PREFIX_INFO (pFeasRoute->
                                                            NetAddress.
                                                            NetAddr));
#if defined (VPLSADS_WANTED) || defined (EVPN_WANTED)
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pFeasRoute),
                           BGP4_RT_SAFI_INFO (pFeasRoute), u4AsafiMask);

#endif
#ifdef VPLSADS_WANTED
    if (u4AsafiMask == CAP_MP_L2VPN_VPLS)
    {
        return BGP4_SUCCESS;
    }
#endif
#ifdef EVPN_WANTED
    if (u4AsafiMask == CAP_MP_L2VPN_EVPN)
    {

        return BGP4_SUCCESS;
    }
#endif
    for (i4CurrIndex = 0; i4CurrIndex < BGP4_MAX_AGGR_ENTRIES; i4CurrIndex++)
    {
        i4RetVal =
            Bgp4AggrCanAggregationBeDone (pFeasRoute, &u4Index, i4CurrIndex);
        if (i4RetVal != BGP4_SUCCESS)
        {
            /* New Feasible route cant be aggregated */
            return BGP4_FAILURE;
        }
        if (BGP4_RT_PREFIXLEN (pFeasRoute) ==
            BGP4_AGGRENTRY_PREFIXLEN (u4Index))
        {
            if ((BGP4_AGGR_GET_STATUS ((&(BGP4_AGGRENTRY[u4Index]))) &
                 BGP4_AGGR_CREATE_IN_PROGRESS) == BGP4_AGGR_CREATE_IN_PROGRESS)
            {
                /* Creation of Aggregate route is just going on. Since this new
                 * route happens to be the best route, just advertise the
                 * route. After aggregation is completed, the aggregated route
                 * shall be advertised.
                 */
            }
            else if ((BGP4_AGGR_GET_STATUS ((&(BGP4_AGGRENTRY[u4Index]))) &
                      BGP4_AGGR_DELETE_IN_PROGRESS) ==
                     BGP4_AGGR_DELETE_IN_PROGRESS)
            {
                /* Deletion of Aggregate route is in progress. Since the new
                 * route happens to be the best route, just advertise the
                 * route. Aggregation route no more exist.
                 */
            }
            else if (((BGP4_RT_GET_FLAGS (BGP4_AGGRENTRY_AGGR_ROUTE (u4Index)) &
                       BGP4_RT_AGGREGATED) == BGP4_RT_AGGREGATED) ||
                     ((BGP4_AGGR_GET_STATUS ((&(BGP4_AGGRENTRY[u4Index]))) &
                       BGP4_AGGR_REAGGREGATE) == BGP4_AGGR_REAGGREGATE))
            {
                /* Check whether the new route is selected as best route ahead
                 * of aggregated route. New feasible route is the best route in
                 * RIB and needs to be updated to FIB. But for advt. the
                 * preference order is Non-BGP routes > aggregated route >
                 * BGP route. So advt accordingly.
                 */
                if (BGP4_RT_PROTOCOL (pFeasRoute) == BGP_ID)
                {
                    /* Aggregated route is selected as best route for
                     * advertisement. New feasible route need not be
                     * advertised to the peers. */
                    BGP4_RT_SET_FLAG (pFeasRoute,
                                      (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                       BGP4_RT_ADVT_NOTTO_EXTERNAL |
                                       BGP4_RT_WITHDRAWN | BGP4_RT_AGGR_ROUTE));
                    Bgp4AddWithdRouteToFIBUpdList (pFeasRoute);
                    pTempFeasRoute = pFeasRoute;
                    while (pTempFeasRoute->pMultiPathRtNext != NULL)
                    {
                        pTempFeasRoute = pTempFeasRoute->pMultiPathRtNext;
                        BGP4_RT_SET_FLAG (pTempFeasRoute,
                                          (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                           BGP4_RT_ADVT_NOTTO_EXTERNAL |
                                           BGP4_RT_AGGR_ROUTE |
                                           BGP4_RT_WITHDRAWN));
                        Bgp4AddWithdRouteToFIBUpdList (pTempFeasRoute);

                    }

                    if (((BGP4_AGGR_GET_STATUS ((&(BGP4_AGGRENTRY[u4Index]))) &
                          BGP4_AGGR_REAGGREGATE) !=
                         BGP4_AGGR_REAGGREGATE) &&
                        (pOldBestRt != NULL) &&
                        (BGP4_RT_PROTOCOL (pOldBestRt) != BGP_ID))
                    {
                        /* Advertise the aggregated route. */
                        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC | BGP4_FDB_TRC,
                                       BGP4_MOD_NAME,
                                       "\tAggregated route %s mask %s is selected as best route "
                                       "for advertisement.\n",
                                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                        (BGP4_AGGRENTRY_AGGR_ROUTE
                                                         (u4Index)),
                                                        BGP4_RT_AFI_INFO
                                                        (BGP4_AGGRENTRY_AGGR_ROUTE
                                                         (u4Index))),
                                       Bgp4PrintIpMask ((UINT1)
                                                        BGP4_RT_PREFIXLEN
                                                        (BGP4_AGGRENTRY_AGGR_ROUTE
                                                         (u4Index)),
                                                        BGP4_RT_AFI_INFO
                                                        (BGP4_AGGRENTRY_AGGR_ROUTE
                                                         (u4Index))));
                        BGP4_AGGR_RESET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                                BGP4_AGGR_ALL_FLAG);
                        BGP4_AGGR_SET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                              BGP4_AGGR_ADVERTISE);
                    }
                }
                else
                {
                    /* New route is selected as best route ahead of
                     * aggregation route for advertisement. Reset the
                     * Aggrentry status if BGP4_AGGR_ADVERTISE flag is set.
                     */
                    if ((BGP4_AGGR_GET_STATUS ((&(BGP4_AGGRENTRY[u4Index]))) &
                         BGP4_AGGR_ADVERTISE) == BGP4_AGGR_ADVERTISE)
                    {
                        BGP4_AGGR_RESET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                                BGP4_AGGR_ALL_FLAG);
                        BGP4_AGGR_SET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                              BGP4_AGGR_NO_REAGGREGATE);
                    }
                }
            }
        }
        else
        {
            /* Route can be aggregated and u4Index points to the index of
             * corresponding configured aggregation policy. */
            if ((BGP4_AGGR_GET_STATUS ((&(BGP4_AGGRENTRY[u4Index]))) &
                 BGP4_AGGR_CREATE_IN_PROGRESS) == BGP4_AGGR_CREATE_IN_PROGRESS)
            {
                /* Check if the received route is less-specific to the stored
                 * Aggregate init route or not. If less-specific, then aggregate
                 * the received feasible route, add it to aggregate route list
                 * and control its advertisement according to the policy. Else
                 * if received route is more-specific or if stored init route is
                 * 0, then just continue. This route will be handled by
                 * Bgp4AggrProcessAdminUp function. */
                if (((PrefixMatch ((BGP4_AGGRENTRY_INIT_PREFIX_INFO (u4Index)),
                                   InvPrefix) == BGP4_TRUE) &&
                     (BGP4_AGGRENTRY_INIT_PREFIX_LEN (u4Index) ==
                      BGP4_MIN_PREFIXLEN)) ||
                    (AddrCompare
                     ((BGP4_RT_NET_ADDRESS_INFO (pFeasRoute).NetAddr),
                      BGP4_RT_PREFIXLEN (pFeasRoute),
                      (BGP4_AGGRENTRY_INIT_PREFIX_INFO (u4Index)),
                      BGP4_AGGRENTRY_INIT_PREFIX_LEN (u4Index)) > 0))
                {
                    /* Route is either more-specific to the stored init route or
                     * stored init route is 0. No need to perform any operation */
                    BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_FDB_TRC, BGP4_MOD_NAME,
                                   "\tThe Received new route %s mask %s is more-specific to the "
                                   "stored aggregate init route.\n",
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pFeasRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pFeasRoute)),
                                   Bgp4PrintIpMask ((UINT1)
                                                    BGP4_RT_PREFIXLEN
                                                    (pFeasRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pFeasRoute)));
                }
                else
                {
                    /* Route is less-specific to the stored init route. */
                    BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_FDB_TRC, BGP4_MOD_NAME,
                                   "\tThe Received new route %s mask %s is less-specific to the "
                                   "stored aggregate init route. Aggregating the route and adding "
                                   "it the aggregate entry list.\n",
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pFeasRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pFeasRoute)),
                                   Bgp4PrintIpMask ((UINT1)
                                                    BGP4_RT_PREFIXLEN
                                                    (pFeasRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pFeasRoute)));
                    if (pOldBestRt != NULL)
                    {
                        Bgp4DshRemoveRouteFromList
                            (BGP4_AGGRENTRY_AGGR_LIST (u4Index), pOldBestRt);
                    }
                    /* Add the new route to the aggregate entry list */
                    Bgp4DshAddRouteToList (BGP4_AGGRENTRY_AGGR_LIST (u4Index),
                                           pFeasRoute, 0);
                    if ((BGP4_RT_GET_FLAGS (BGP4_AGGRENTRY_AGGR_ROUTE (u4Index))
                         & BGP4_RT_AGGREGATED) == BGP4_RT_AGGREGATED)
                    {
                        if (pOldBestRt == NULL)
                        {
                            /* Just aggregate the new route and aggregated route. */
                            Bgp4AggrAggregateBgpInfos
                                (BGP4_AGGRENTRY_AGGR_ROUTE (u4Index),
                                 pFeasRoute, BGP4_AGGRENTRY_ADV_TYPE (u4Index),
                                 BGP4_AGGRENTRY_AS_SET (u4Index));
                        }
                        else
                        {
                            /* Need to reaggregate the entire aggregate route. So
                             * just make the aggregate route dummy and the 
                             * re-aggregation will be done by
                             * Bgp4AggrProcessAdminUp (). */
                            pAggrRoute = BGP4_AGGRENTRY_AGGR_ROUTE (u4Index);
                            if (BGP4_RT_BGP_INFO (pAggrRoute) != NULL)
                            {
                                /* Release BGP Info associated with the 
                                 * Aggregate route. */
                                Bgp4DshReleaseBgpInfo (BGP4_RT_BGP_INFO
                                                       (pAggrRoute));
                            }
                            BGP4_RT_RESET_FLAG (pAggrRoute, BGP4_RT_AGGREGATED);

                            /* Reset the Aggregated route into Dummy 
                             * aggregate route. */
                            pAggrRoute->u4MED = BGP4_INV_MEDLP;
                            BGP4_RT_PROTOCOL (pAggrRoute) = PROTO_INVALID;
                            BGP4_RT_SET_RT_IF_INDEX (pAggrRoute,
                                                     BGP4_INVALID_IFINDX);
                            BGP4_RT_SET_FLAG (pAggrRoute, BGP4_RT_AGGR_ROUTE);
                            BGP4_RT_BGP4_INF (pAggrRoute) = NULL;
                            if (BGP4_AGGRENTRY_ADV_TYPE (u4Index) ==
                                BGP4_SUMMARY)
                            {
                                /* No need to advertise this route. */
                                BGP4_RT_SET_FLAG (pFeasRoute,
                                                  (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                                   BGP4_RT_ADVT_NOTTO_EXTERNAL));
                            }
                        }
                    }
                }
            }
            else if ((BGP4_AGGR_GET_STATUS ((&(BGP4_AGGRENTRY[u4Index]))) &
                      BGP4_AGGR_DELETE_IN_PROGRESS) ==
                     BGP4_AGGR_DELETE_IN_PROGRESS)
            {
                /* Delete the old route from the aggregate route list and the new
                 * route should be advertised. */
                BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_FDB_TRC, BGP4_MOD_NAME,
                               "\tRemoving the aggregated route %s mask %s from aggregate route "
                               "list and the received new route %s mask %s is advertised.\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                (BGP4_AGGRENTRY_AGGR_ROUTE
                                                 (u4Index)),
                                                BGP4_RT_AFI_INFO
                                                (BGP4_AGGRENTRY_AGGR_ROUTE
                                                 (u4Index))),
                               Bgp4PrintIpMask ((UINT1)
                                                BGP4_RT_PREFIXLEN
                                                (BGP4_AGGRENTRY_AGGR_ROUTE
                                                 (u4Index)),
                                                BGP4_RT_AFI_INFO
                                                (BGP4_AGGRENTRY_AGGR_ROUTE
                                                 (u4Index))),
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pFeasRoute),
                                                BGP4_RT_AFI_INFO (pFeasRoute)),
                               Bgp4PrintIpMask ((UINT1)
                                                BGP4_RT_PREFIXLEN (pFeasRoute),
                                                BGP4_RT_AFI_INFO (pFeasRoute)));
                if (pOldBestRt != NULL)
                {
                    Bgp4DshRemoveRouteFromList (BGP4_AGGRENTRY_AGGR_LIST
                                                (u4Index), pOldBestRt);
                }
            }
            else
            {
                /* Remove the old route from aggregate entry list */
                BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_FDB_TRC, BGP4_MOD_NAME,
                               "\tRemoving the aggregated route %s mask %s from aggregate route "
                               "list and the received new route %s mask %s is added to the "
                               "aggregate entry list for re-aggregation.\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                (BGP4_AGGRENTRY_AGGR_ROUTE
                                                 (u4Index)),
                                                BGP4_RT_AFI_INFO
                                                (BGP4_AGGRENTRY_AGGR_ROUTE
                                                 (u4Index))),
                               Bgp4PrintIpMask ((UINT1)
                                                BGP4_RT_PREFIXLEN
                                                (BGP4_AGGRENTRY_AGGR_ROUTE
                                                 (u4Index)),
                                                BGP4_RT_AFI_INFO
                                                (BGP4_AGGRENTRY_AGGR_ROUTE
                                                 (u4Index))),
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pFeasRoute),
                                                BGP4_RT_AFI_INFO (pFeasRoute)),
                               Bgp4PrintIpMask ((UINT1)
                                                BGP4_RT_PREFIXLEN (pFeasRoute),
                                                BGP4_RT_AFI_INFO (pFeasRoute)));
                if (pOldBestRt != NULL)
                {
                    Bgp4DshRemoveRouteFromList (BGP4_AGGRENTRY_AGGR_LIST
                                                (u4Index), pOldBestRt);
                }
                /* Add the new route to the aggregate entry list */
                Bgp4DshAddRouteToList (BGP4_AGGRENTRY_AGGR_LIST (u4Index),
                                       pFeasRoute, 0);

                /* Update status for re-aggregation. */
                BGP4_AGGR_RESET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                        BGP4_AGGR_ALL_FLAG);
                BGP4_AGGR_SET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                      BGP4_AGGR_REAGGREGATE);
                if (BGP4_AGGRENTRY_ADV_TYPE (u4Index) == BGP4_SUMMARY)
                {
                    /* No need to advertise this route. */
                    BGP4_RT_SET_FLAG (pFeasRoute,
                                      (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                       BGP4_RT_ADVT_NOTTO_EXTERNAL));
                }
            }
        }
    }
    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Bgp4AggrProcessWithdrawnRoute                             */
/* Description   : This routine handles the application of the aggregation   */
/*                 policy over the withdrawn route.                          */
/* Input(s)      : Withdrawn route (pWithRoute)                              */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4AggrProcessWithdrawnRoute (tRouteProfile * pWithRoute)
{
    tAddrPrefix         InvPrefix;
    tRouteProfile      *pAggrRoute = NULL;
    tRouteProfile      *pOldBestRt = NULL;
    UINT4               u4Index = 0;
    INT4                i4RetVal = 0;
    INT4                i4CurrIndex = 0;

    Bgp4InitAddrPrefixStruct (&InvPrefix,
                              BGP4_AFI_IN_ADDR_PREFIX_INFO (pWithRoute->
                                                            NetAddress.
                                                            NetAddr));
    for (i4CurrIndex = 0; i4CurrIndex < BGP4_MAX_AGGR_ENTRIES; i4CurrIndex++)
    {
        i4RetVal =
            Bgp4AggrCanAggregationBeDone (pWithRoute, &u4Index, i4CurrIndex);
        if (i4RetVal != BGP4_SUCCESS)
        {
            /* New Feasible route cant be aggregated */
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tThe Received withdrawn route %s mask %s cannot be aggregated.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pWithRoute),
                                            BGP4_RT_AFI_INFO (pWithRoute)),
                           Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                                            (pWithRoute),
                                            BGP4_RT_AFI_INFO (pWithRoute)));
            return BGP4_SUCCESS;
        }
        if (BGP4_RT_PREFIXLEN (pWithRoute) ==
            BGP4_AGGRENTRY_PREFIXLEN (u4Index))
        {
            if ((BGP4_AGGR_GET_STATUS ((&(BGP4_AGGRENTRY[u4Index]))) &
                 BGP4_AGGR_CREATE_IN_PROGRESS) == BGP4_AGGR_CREATE_IN_PROGRESS)
            {
                /* Creation of Aggregate route is just going on. Since this
                 * route happens was the best route earlier, just advertise the
                 * withdrawn route. After aggregation is completed, the
                 * aggregated route shall be advertised.
                 */
            }
            else if ((BGP4_AGGR_GET_STATUS ((&(BGP4_AGGRENTRY[u4Index]))) &
                      BGP4_AGGR_DELETE_IN_PROGRESS) ==
                     BGP4_AGGR_DELETE_IN_PROGRESS)
            {
                /* Deletion of Aggregate route is in progress. Since this
                 * route was the best route earlier, just advertise the
                 * withdrawn route. Aggregation route no more exist.
                 */
            }
            else if (((BGP4_RT_GET_FLAGS (BGP4_AGGRENTRY_AGGR_ROUTE (u4Index)) &
                       BGP4_RT_AGGREGATED) == BGP4_RT_AGGREGATED) ||
                     ((BGP4_AGGR_GET_STATUS ((&(BGP4_AGGRENTRY[u4Index]))) &
                       BGP4_AGGR_REAGGREGATE) == BGP4_AGGR_REAGGREGATE))
            {
                if (BGP4_RT_PROTOCOL (pWithRoute) == BGP_ID)
                {
                    /* Withdrawn BGP route was the best route in RIB and
                     * needs to be updated to FIB. But aggregated route
                     * would have been advt earlier.So no need to advt this
                     * route. */
                    BGP4_RT_SET_FLAG (pWithRoute,
                                      (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                       BGP4_RT_ADVT_NOTTO_EXTERNAL));
                }
                else
                {
                    /* Withdrawn Non-BGP route was the best route in RIB and
                     * earlier only this route should have been advertised
                     * as feasible route to all the peers. So while withdrawning
                     * this route, if aggregate route is present advt the aggregate
                     * route as feasible. Else advt this route as withdrawn.
                     */
                    if ((BGP4_AGGRENTRY_AGGR_STATUS (u4Index) &
                         BGP4_AGGR_REAGGREGATE) != BGP4_AGGR_REAGGREGATE)
                    {
                        /* Update the status so that the aggregated route is
                         * just advertised. */
                        BGP4_AGGR_RESET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                                BGP4_AGGR_ALL_FLAG);
                        BGP4_AGGR_SET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                              BGP4_AGGR_ADVERTISE);
                    }
                    BGP4_RT_SET_FLAG (pWithRoute,
                                      (BGP4_RT_ADVT_NOTTO_EXTERNAL |
                                       BGP4_RT_ADVT_NOTTO_INTERNAL));
                }
            }
        }
        else
        {
            /* Route can be aggregated and u4Index points to the index of
             * corresponding configured aggregation policy. */
            if ((BGP4_AGGR_GET_STATUS ((&(BGP4_AGGRENTRY[u4Index]))) &
                 BGP4_AGGR_CREATE_IN_PROGRESS) == BGP4_AGGR_CREATE_IN_PROGRESS)
            {
                /* Check if the received route is less-specific to the stored
                 * Aggregate init route or not. If less-specific than aggregate
                 * the received feasible route, remove it to aggregate route list
                 * and control its advertisement according to the policy. Else
                 * if received route is more-specific or if stored init route is
                 * 0, then just continue.
                 */
                if (((PrefixMatch ((BGP4_AGGRENTRY_INIT_PREFIX_INFO (u4Index)),
                                   InvPrefix) == BGP4_TRUE) &&
                     (BGP4_AGGRENTRY_INIT_PREFIX_LEN (u4Index) ==
                      BGP4_MIN_PREFIXLEN)) ||
                    (AddrCompare
                     ((BGP4_RT_NET_ADDRESS_INFO (pWithRoute).NetAddr),
                      BGP4_RT_PREFIXLEN (pWithRoute),
                      (BGP4_AGGRENTRY_INIT_PREFIX_INFO (u4Index)),
                      BGP4_AGGRENTRY_INIT_PREFIX_LEN (u4Index)) > 0))
                {
                    /* Route is either more-specific to the stored init route or
                     * stored init route is 0. No need to perform any operation */
                    BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_FDB_TRC, BGP4_MOD_NAME,
                                   "\tThe Received withdrawn route %s mask %s is more-specific to the "
                                   "stored aggregate init route.\n",
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pWithRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pWithRoute)),
                                   Bgp4PrintIpMask ((UINT1)
                                                    BGP4_RT_PREFIXLEN
                                                    (pWithRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pWithRoute)));
                }
                else
                {
                    /* Route is less-specific to the stored init route.
                     * Remove the route from Aggregate route list. */
                    BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_FDB_TRC, BGP4_MOD_NAME,
                                   "\tThe Received withdrawn route %s mask %s is less-specific to the "
                                   "stored aggregate init route. Removing it from the aggregate entry list.\n",
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pWithRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pWithRoute)),
                                   Bgp4PrintIpMask ((UINT1)
                                                    BGP4_RT_PREFIXLEN
                                                    (pWithRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pWithRoute)));
                    pOldBestRt =
                        Bgp4DshRemoveRouteFromList (BGP4_AGGRENTRY_AGGR_LIST
                                                    (u4Index), pWithRoute);

                    if ((BGP4_RT_GET_FLAGS (BGP4_AGGRENTRY_AGGR_ROUTE (u4Index))
                         & BGP4_RT_AGGREGATED) == BGP4_RT_AGGREGATED)
                    {
                        if (pOldBestRt == NULL)
                        {
                            /* Old route not present in list. No need to modify
                             * the aggregated route. */
                        }
                        else
                        {
                            /* Need to reaggregate the entire aggregate route. So
                             * just make the aggregate route dummy and the 
                             * re-aggregation will be done by
                             * Bgp4AggrProcessAdminUp (). */
                            pAggrRoute = BGP4_AGGRENTRY_AGGR_ROUTE (u4Index);
                            if (BGP4_RT_BGP_INFO (pAggrRoute) != NULL)
                            {
                                /* Release BGP Info associated with the Aggregate
                                 * route. */
                                Bgp4DshReleaseBgpInfo
                                    (BGP4_RT_BGP_INFO (pAggrRoute));
                            }
                            BGP4_RT_RESET_FLAG (pAggrRoute, BGP4_RT_AGGREGATED);

                            /* Reset the Aggregated route into Dummy aggregate
                             * route. */
                            pAggrRoute->u4MED = BGP4_INV_MEDLP;
                            BGP4_RT_PROTOCOL (pAggrRoute) = PROTO_INVALID;
                            BGP4_RT_SET_RT_IF_INDEX (pAggrRoute,
                                                     BGP4_INVALID_IFINDX);
                            BGP4_RT_SET_FLAG (pAggrRoute, BGP4_RT_AGGR_ROUTE);
                            BGP4_RT_BGP4_INF (pAggrRoute) = NULL;
                            if (BGP4_AGGRENTRY_ADV_TYPE (u4Index) ==
                                BGP4_SUMMARY)
                            {
                                /* No need to advertise this route. */
                                BGP4_RT_SET_FLAG (pWithRoute,
                                                  (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                                   BGP4_RT_ADVT_NOTTO_EXTERNAL));
                            }
                        }
                    }
                }
            }
            else if ((BGP4_AGGR_GET_STATUS ((&(BGP4_AGGRENTRY[u4Index]))) &
                      BGP4_AGGR_DELETE_IN_PROGRESS) ==
                     BGP4_AGGR_DELETE_IN_PROGRESS)
            {
                Bgp4DshRemoveRouteFromList (BGP4_AGGRENTRY_AGGR_LIST (u4Index),
                                            pWithRoute);
            }
            else
            {
                /* Remove the route from aggregate entry list */
                pOldBestRt = Bgp4DshRemoveRouteFromList
                    (BGP4_AGGRENTRY_AGGR_LIST (u4Index), pWithRoute);

                if (pOldBestRt != NULL)
                {
                    /* Update status for re-aggregation. */
                    BGP4_AGGR_RESET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                            BGP4_AGGR_ALL_FLAG);
                    BGP4_AGGR_SET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                          BGP4_AGGR_REAGGREGATE);
                    if (BGP4_AGGRENTRY_ADV_TYPE (u4Index) == BGP4_SUMMARY)
                    {
                        /* No need to advertise this route. */
                        BGP4_RT_SET_FLAG (pWithRoute,
                                          (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                           BGP4_RT_ADVT_NOTTO_EXTERNAL));
                    }
                }
            }
        }
    }
    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Bgp4AggrProcessAggregation                                */
/* Description   : This routine is called whenever an entry in aggregation   */
/*                 table needs to be re-aggregated. This function            */
/*                 re-aggregates the entry and corresponding updates the FIB */
/*                 and other peers with the re-aggregated routes.            */
/* Input(s)      : Index to the aggregation table (u4AggrtblIndex)           */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS - if processed successfully                  */
/*                 BGP4_FAILURE - otherwise.                                 */
/*****************************************************************************/
INT4
Bgp4AggrProcessAggregation (UINT4 u4AggrtblIndex)
{
    tBgp4AggrEntry     *pAggrEntry = NULL;
    tTMO_SLL           *pAggregatedRouteList = NULL;
    tRouteProfile      *pAggrRoute = NULL;
    tRouteProfile      *pBestRoute = NULL;
    tRouteProfile       AggrRoute;
    UINT4               u4IsReAggregated = BGP4_FALSE;
    UINT1               u1CanAggrRtBeAdvt = BGP4_FALSE;
    UINT4               u4Context = 0;
    INT1                i1AdvtStatus = BGP4_FALSE;

    MEMSET (&AggrRoute, 0, sizeof (tRouteProfile));
    pAggrEntry = (&(BGP4_AGGRENTRY[u4AggrtblIndex]));
    u4Context = BGP4_AGGRENTRY_VRFID (u4AggrtblIndex);
    if (((BGP4_AGGR_GET_STATUS (pAggrEntry) & BGP4_AGGR_REAGGREGATE) !=
         BGP4_AGGR_REAGGREGATE)
        || (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_DOWN))
    {
        BGP4_AGGR_RESET_STATUS (pAggrEntry, BGP4_AGGR_REAGGREGATE);
        BGP4_AGGR_SET_STATUS (pAggrEntry, BGP4_AGGR_NO_REAGGREGATE);
        return (BGP4_SUCCESS);
    }

    /* Re-aggregation needs to be done. */
    Bgp4CopyNetAddressStruct (&(AggrRoute.NetAddress),
                              BGP4_AGGRENTRY_NET_ADDR_INFO (u4AggrtblIndex));
    BGP4_RT_PROTOCOL ((&AggrRoute)) = (UINT1) PROTO_INVALID;
    BGP4_RT_SET_FLAG ((&AggrRoute), BGP4_RT_AGGR_ROUTE);

    pAggrRoute = BGP4_AGGRENTRY_AGGR_ROUTE (u4AggrtblIndex);
    pAggregatedRouteList = BGP4_AGGRENTRY_AGGR_LIST (u4AggrtblIndex);

    BGP4_DBG1 (BGP4_DBG_CTRL_FLOW,
               "\tBgp4AggrProcessAggr() : Route Count that can be used "
               "for aggr. %d \n", TMO_SLL_Count (pAggregatedRouteList));

    if (BGP4_RT_GET_FLAGS (pAggrRoute) & BGP4_RT_AGGREGATED)
    {
        if (TMO_SLL_Count (pAggregatedRouteList) == 0)
        {
            /* Now need to de-aggregate the current route and send it
             * as withdrawn to all other peers. */
            BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                      "\tBgp4AggrProcessAggr() : Aggregated route to "
                      "be withdrawn \n");

            u1CanAggrRtBeAdvt =
                Bgp4AggrCanAggrRouteBeAdvt (pAggrRoute, &pBestRoute);
            if (u1CanAggrRtBeAdvt == BGP4_TRUE)
            {
                if (pBestRoute == NULL)
                {
                    /* No matching route present in RIB for this aggregated
                     * route. Advt the aggregated route as withdrawn. */
                    if (u4AggrtblIndex < BGP4_MAX_AGGR_ENTRIES)
                    {
                        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC | BGP4_FDB_TRC,
                                       BGP4_MOD_NAME,
                                       "\tNo matching route present in RIB for the aggregated "
                                       "route %s mask %s. Advertising the aggregated route as withdrawn.\n",
                                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                        (pAggrRoute),
                                                        BGP4_RT_AFI_INFO
                                                        (pAggrRoute)),
                                       Bgp4PrintIpMask ((UINT1)
                                                        BGP4_RT_PREFIXLEN
                                                        (pAggrRoute),
                                                        BGP4_RT_AFI_INFO
                                                        (pAggrRoute)));
                        Bgp4AggrAddAggrRouteToAdvtList (u4AggrtblIndex,
                                                        BGP4_RT_WITHDRAWN);
                    }
                }
                else
                {
                    /* BGP route is present in the RIB. Advt this route
                     * as the best route. */
                    BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_FDB_TRC, BGP4_MOD_NAME,
                                   "\tBGP route %s mask %s is present in the RIB for the "
                                   "aggregated route %s mask %s. Advertising the route as best route.\n",
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pBestRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pBestRoute)),
                                   Bgp4PrintIpMask ((UINT1)
                                                    BGP4_RT_PREFIXLEN
                                                    (pBestRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pBestRoute)),
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pAggrRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pAggrRoute)),
                                   Bgp4PrintIpMask ((UINT1)
                                                    BGP4_RT_PREFIXLEN
                                                    (pAggrRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pAggrRoute)));
                    BGP4_RT_RESET_FLAG (pBestRoute, BGP4_RT_WITHDRAWN);
                    BGP4_RT_SET_FLAG (pBestRoute, BGP4_RT_REPLACEMENT);
                    if ((GetAdvtStatus (pBestRoute, NULL, &i1AdvtStatus)
                         == BGP4_SUCCESS) && (i1AdvtStatus == BGP4_TRUE))
                    {
                        /* Aggregated route advertisement to external peers 
                         * is allowed by Community Attribute. */
                        Bgp4AddFeasRouteToExtPeerAdvtList (pBestRoute, NULL);
                    }
                    Bgp4AddFeasRouteToIntPeerAdvtList (pBestRoute, NULL);
                }
            }

            /* De-aggregate the current aggregated route. */
            Bgp4DshReleaseBgpInfo (BGP4_RT_BGP_INFO (pAggrRoute));
            pAggrRoute->u4MED = BGP4_INV_MEDLP;
            BGP4_RT_BGP4_INF (pAggrRoute) = NULL;
            BGP4_RT_RESET_FLAG (pAggrRoute, BGP4_RT_AGGREGATED);
            BGP4_RT_PROTOCOL (pAggrRoute) = PROTO_INVALID;
            BGP4_RT_SET_RT_IF_INDEX (pAggrRoute, BGP4_INVALID_IFINDX);

            /* Update the status of the aggregation entry. */
            BGP4_AGGR_RESET_STATUS (pAggrEntry, BGP4_AGGR_REAGGREGATE);
            BGP4_AGGR_SET_STATUS (pAggrEntry, BGP4_AGGR_NO_REAGGREGATE);
        }
        else
        {
            /* Route was already aggregated. */
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tThe route %s mask %s is already aggregated. Checking "
                           "for reaggregation.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pAggrRoute),
                                            BGP4_RT_AFI_INFO (pAggrRoute)),
                           Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                                            (pAggrRoute),
                                            BGP4_RT_AFI_INFO (pAggrRoute)));
            u4IsReAggregated =
                Bgp4AggrCheckForReAggregation (pAggrRoute, pAggregatedRouteList,
                                               pAggrEntry->u1AdvType,
                                               pAggrEntry->u1AsSet);
            if (u4IsReAggregated == BGP4_TRUE)
            {
                if (BGP4_RT_PROTOCOL (pAggrRoute) == PROTO_INVALID)
                {
                    /* After Reaggregation if none of the routes matches the aggregate entry. The previously advertised aggregate route is now advertised as withdrawn */
                    BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_FDB_TRC, BGP4_MOD_NAME,
                                   "\tThe Re-aggregated route %s mask %s matches none of the "
                                   "route. Hence advertising it as withdrawn.\n",
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pAggrRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pAggrRoute)),
                                   Bgp4PrintIpMask ((UINT1)
                                                    BGP4_RT_PREFIXLEN
                                                    (pAggrRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pAggrRoute)));

                    Bgp4AggrAddAggrRouteToAdvtList (u4AggrtblIndex,
                                                    BGP4_RT_WITHDRAWN);
                }
                else
                {

                    u1CanAggrRtBeAdvt =
                        Bgp4AggrCanAggrRouteBeAdvt (pAggrRoute, &pBestRoute);
                    if (u1CanAggrRtBeAdvt == BGP4_TRUE)
                    {
                        if (u4AggrtblIndex < BGP4_MAX_AGGR_ENTRIES)
                        {
                            Bgp4AggrAddAggrRouteToAdvtList (u4AggrtblIndex,
                                                            BGP4_RT_REPLACEMENT);
                        }
                    }
                }
            }

            /* Update the status of the aggregation entry. */
            BGP4_AGGR_RESET_STATUS (pAggrEntry, BGP4_AGGR_REAGGREGATE);
            BGP4_AGGR_SET_STATUS (pAggrEntry, BGP4_AGGR_NO_REAGGREGATE);
        }
    }
    else
    {
        /* Route previously not aggregated. Now can be aggregated. So
         * aggregate the current route. */
        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tThe route %s mask %s is not previously aggregated.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pAggrRoute),
                                        BGP4_RT_AFI_INFO (pAggrRoute)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                                        (pAggrRoute),
                                        BGP4_RT_AFI_INFO (pAggrRoute)));
        if (TMO_SLL_Count (pAggregatedRouteList) == 0)
        {
            /* No route present. Just reset the current flag. */
            BGP4_AGGR_RESET_STATUS (pAggrEntry, BGP4_AGGR_REAGGREGATE);
            BGP4_AGGR_SET_STATUS (pAggrEntry, BGP4_AGGR_NO_REAGGREGATE);
            return (BGP4_SUCCESS);
        }

        /* Try releasing Bgp Info if present. */
        Bgp4DshReleaseBgpInfo (BGP4_RT_BGP_INFO (pAggrRoute));
        BGP4_RT_BGP4_INF (pAggrRoute) = NULL;
        pAggrRoute->u4MED = BGP4_INV_MEDLP;

        BGP4_RT_PROTOCOL (pAggrRoute) = PROTO_INVALID;
        BGP4_RT_SET_RT_IF_INDEX (pAggrRoute, BGP4_INVALID_IFINDX);

        BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                  "\tBgp4AggrProcessAggr() : Reaggr. needs to be done \n");
        Bgp4AggrReaggregate (pAggrRoute, pAggregatedRouteList);

        /* Now check the new best route. */
        u1CanAggrRtBeAdvt =
            Bgp4AggrCanAggrRouteBeAdvt (pAggrRoute, &pBestRoute);
        if (u1CanAggrRtBeAdvt == BGP4_TRUE)
        {
            if (pBestRoute == NULL)
            {
                /* This route is the new best route. */
                if (pAggrRoute->pRtInfo == NULL)
                {
                    BGP4_RT_RESET_FLAG (pAggrRoute, BGP4_RT_AGGREGATED);
                }
                else if (u4AggrtblIndex < BGP4_MAX_AGGR_ENTRIES)
                {
                    Bgp4AggrAddAggrRouteToAdvtList (u4AggrtblIndex, 0);
                }
            }
            else
            {
                /* BGP route exist. Check for the status of this route
                 * and process accordingly */
                if (((BGP4_RT_GET_FLAGS (pBestRoute)) &
                     (BGP4_RT_IN_FIB_UPD_LIST)) == BGP4_RT_IN_FIB_UPD_LIST)
                {
                    BGP4_RT_SET_FLAG (pBestRoute, BGP4_RT_ADVT_NOTTO_EXTERNAL);
                }
                else if (((BGP4_RT_GET_FLAGS (pBestRoute)) &
                          (BGP4_RT_IN_EXT_PEER_ADVT_LIST)) ==
                         BGP4_RT_IN_EXT_PEER_ADVT_LIST)
                {
                    /* Remove the route from that list. */
                    BGP4_RT_RESET_FLAG (pBestRoute,
                                        (BGP4_RT_REPLACEMENT |
                                         BGP4_RT_IN_EXT_PEER_ADVT_LIST));
                    Bgp4DshDelinkRouteFromList (BGP4_EXT_PEERS_ADVT_LIST
                                                (u4Context), pBestRoute,
                                                BGP4_EXT_PEER_LIST_INDEX);
                }
                if (((BGP4_RT_GET_FLAGS (pBestRoute)) &
                     (BGP4_RT_IN_INT_PEER_ADVT_LIST)) ==
                    BGP4_RT_IN_INT_PEER_ADVT_LIST)
                {
                    /* Remove the route from that list. */
                    BGP4_RT_RESET_FLAG (pBestRoute,
                                        (BGP4_RT_REPLACEMENT |
                                         BGP4_RT_IN_INT_PEER_ADVT_LIST));
                    Bgp4DshDelinkRouteFromList (BGP4_INT_PEERS_ADVT_LIST
                                                (u4Context), pBestRoute,
                                                BGP4_INT_PEER_LIST_INDEX);
                }
                if (u4AggrtblIndex < BGP4_MAX_AGGR_ENTRIES)
                {
                    BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_FDB_TRC, BGP4_MOD_NAME,
                                   "\tMatching BGP route %s mask %s is present in the RIB for the "
                                   "aggregated route %s mask %s. Adding the route to advertisement list.\n",
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pBestRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pBestRoute)),
                                   Bgp4PrintIpMask ((UINT1)
                                                    BGP4_RT_PREFIXLEN
                                                    (pBestRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pBestRoute)),
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pAggrRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pAggrRoute)),
                                   Bgp4PrintIpMask ((UINT1)
                                                    BGP4_RT_PREFIXLEN
                                                    (pAggrRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pAggrRoute)));
                    Bgp4AggrAddAggrRouteToAdvtList (u4AggrtblIndex,
                                                    BGP4_RT_REPLACEMENT);
                }
            }
        }

        /* Update the status of the aggregation entry. */
        BGP4_AGGR_RESET_STATUS (pAggrEntry, BGP4_AGGR_REAGGREGATE);
        BGP4_AGGR_SET_STATUS (pAggrEntry, BGP4_AGGR_NO_REAGGREGATE);
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AggrFindAggrEntry                                     */
/* Description   : Checks for the presence of any Aggregate Entry            */
/*                 corresponding to this specific route profile in the       */
/*                 Aggregate Table. If any entry is present then the index   */
/*                 is returned.                                              */
/* Input(s)      : BGP Route Profile  (pSpecificRoute)                       */
/* Output(s)     : None                                                      */
/* Return(s)     : If a matching entry is found then the index to the        */
/*                 Aggregate table(1 to MAX_AGGR) is returned else 0 is      */
/*                 returned.                                                 */
/*****************************************************************************/
UINT4
Bgp4AggrFindAggrEntry (tRouteProfile * pSpecificRoute, INT4 i4Index)
{
    UINT4               u4AsafiMask;
    UINT4               u4AggrMask;
    INT4                i4AggrtblIndex;
    UINT1               u1Prefixlen;
    UINT1               u1AggrPrefixlen;
    INT4                i4MatchIndx = 0;
    UINT1               u1MatchFnd = BGP4_FALSE;

    u1Prefixlen = (UINT1) BGP4_RT_PREFIXLEN (pSpecificRoute);

    for (i4AggrtblIndex = i4Index; i4AggrtblIndex < BGP4_MAX_AGGR_ENTRIES;
         i4AggrtblIndex++)
    {
        if ((BGP4_AGGRENTRY_ADMIN (i4AggrtblIndex) == BGP4_INVALID) ||
            ((BGP4_AGGRENTRY_ADMIN (i4AggrtblIndex) == BGP4_ADMIN_DOWN) &&
             ((BGP4_AGGRENTRY_AGGR_STATUS (i4AggrtblIndex) &
               BGP4_AGGR_DELETE_IN_PROGRESS) != BGP4_AGGR_DELETE_IN_PROGRESS)))
        {
            continue;
        }

        u1AggrPrefixlen = (UINT1) BGP4_AGGRENTRY_PREFIXLEN (i4AggrtblIndex);
        if (u1Prefixlen < u1AggrPrefixlen)
        {
            continue;
        }

        BGP4_GET_AFISAFI_MASK (BGP4_AGGRENTRY_IPADDR_FAMILY (i4AggrtblIndex),
                               BGP4_AGGRENTRY_IPSUBADDR_FAMILY (i4AggrtblIndex),
                               u4AggrMask);
        BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pSpecificRoute),
                               BGP4_RT_SAFI_INFO (pSpecificRoute), u4AsafiMask);
#ifdef L3VPN
        if (u4AsafiMask == CAP_MP_LABELLED_IPV4)
        {
            u4AsafiMask = CAP_MP_IPV4_UNICAST;
        }
#endif
        if (u4AsafiMask != u4AggrMask)
        {
#ifdef L3VPN
            if (!((BGP4_RT_GET_FLAGS (pSpecificRoute) &
                   BGP4_RT_CONVERTED_TO_VPNV4) &&
                  u4AggrMask == CAP_MP_IPV4_UNICAST))
            {
                continue;        /* Prefix doesn't match */
            }
#else
            {
                continue;
            }
#endif
        }
        switch (u4AggrMask)
        {
            case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
            case CAP_MP_LABELLED_IPV4:
#endif
#ifdef BGP4_IPV6_WANTED
            case CAP_MP_IPV6_UNICAST:
#endif
                if ((AddrMatch
                     (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                      (BGP4_RT_NET_ADDRESS_INFO (pSpecificRoute)),
                      BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                      (BGP4_AGGRENTRY_NET_ADDR_INFO (i4AggrtblIndex)),
                      u1AggrPrefixlen)) != BGP4_TRUE)
                {
                    continue;    /* Prefix doesn't match */
                }
                break;
#ifdef L3VPN
            case CAP_MP_VPN4_UNICAST:
                if (BGP4_RT_GET_FLAGS (pSpecificRoute) &
                    BGP4_RT_CONVERTED_TO_VPNV4)
                {
                    /* this route is received from a CE peer */
                    if ((AddrMatch
                         (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                          (BGP4_RT_NET_ADDRESS_INFO (pSpecificRoute)),
                          BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                          (BGP4_AGGRENTRY_NET_ADDR_INFO (i4AggrtblIndex)),
                          u1AggrPrefixlen)) != BGP4_TRUE)
                    {
                        continue;    /* Prefix doesn't match */
                    }
                }
                else
                {
                    /* this route is received from a PE peer */
                    if ((Vpnv4AddrMatch
                         (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                          (BGP4_AGGRENTRY_NET_ADDR_INFO (i4AggrtblIndex)),
                          pSpecificRoute, u1AggrPrefixlen)) != BGP4_TRUE)
                    {
                        continue;    /* Prefix doesn't match */
                    }
                }
                break;
#endif
            default:
                continue;
        }

#ifdef L3VPN
        if ((u4AsafiMask == CAP_MP_VPN4_UNICAST) &&
            (BGP4_RT_PROTOCOL (pSpecificRoute) == BGP_ID))
        {
            if (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pSpecificRoute))
                == BGP4_VPN4_CE_PEER)
            {
                /* Received from a CE peer */
                if (BGP4_AGGRENTRY_VRFID (i4AggrtblIndex) !=
                    BGP4_RT_CXT_ID (pSpecificRoute))
                {
                    continue;    /* VRF ids do not match */
                }
            }
        }
#endif
        BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                  "\tBgp4AggrFindAggrEntry() :  Matching Entry FOUND \n");

        i4MatchIndx = i4AggrtblIndex;
        u1MatchFnd = BGP4_TRUE;
        if (Bgp4CheckAggrRestrict (i4AggrtblIndex, pSpecificRoute)
            == BGP4_RESTRICT)
        {
            return (0);
        }

        if (u1MatchFnd == BGP4_TRUE)
        {
            return ((UINT4) (i4MatchIndx + BGP4_INCREMENT_BY_ONE));
        }
    }
    return (0);
}

/*****************************************************************************/
/* Function Name : Bgp4AggrReaggregate                                       */
/* Description   : Forms the Aggregate BGP Route using the list of BGP routes*/
/* Input(s)      : List of BGP routes which can be aggregated                */
/*                 (pTobeAggrList)                                           */
/* Output(s)     : Aggregated BGP Route                                      */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4AggrReaggregate (tRouteProfile * pAggrRoute, tTMO_SLL * pTobeAggrList)
{
    tLinkNode          *pRtLink = NULL;
    tLinkNode          *pNextRtLink = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tFilteringRMap      SuppressFilter;
    tFilteringRMap      AdvFilter;
    tFilteringRMap      AttFilter;
    tBgp4AggrEntry     *pAggrEntry = NULL;
    UINT4               u4Status = 0;
    UINT4               u4Index = 0;
    UINT4               u4AdvStatus = FILTERNIG_STAT_DISABLE;
    UINT4               u4SuppStatus = FILTERNIG_STAT_DISABLE;
    UINT4               u4AttrStatus = FILTERNIG_STAT_DISABLE;

    MEMSET (&SuppressFilter, 0, sizeof (tFilteringRMap));
    MEMSET (&AdvFilter, 0, sizeof (tFilteringRMap));
    MEMSET (&AttFilter, 0, sizeof (tFilteringRMap));

    if (pTobeAggrList == NULL)
    {
        return BGP4_SUCCESS;
    }

    if (TMO_SLL_Count (pTobeAggrList) == 0)
    {
        return BGP4_SUCCESS;
    }

    TMO_DYN_SLL_Scan (pTobeAggrList, pRtLink, pNextRtLink, tLinkNode *)
    {
        pRtProfile = pRtLink->pRouteProfile;
        u4Index = Bgp4AggrFindAggrEntry (pRtProfile, u4Index);
        if (u4Index == 0)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tInvalid Index Value\n ");
            continue;
        }
        pAggrEntry = (&(BGP4_AGGRENTRY[u4Index - BGP4_DECREMENT_BY_ONE]));
        break;
    }

    if (pAggrEntry != NULL)
    {
        /* Here we get the routemap status ie. whether route map is present
         * or not. It is done for all 3 maps, (advertise-map, suppress-map,
         * attribute-map)*/
        if (pAggrEntry->u1AdvMapLen > 0)
        {
            MEMCPY (AdvFilter.au1DistInOutFilterRMapName,
                    pAggrEntry->au1AdvMapName, pAggrEntry->u1AdvMapLen);
#ifdef ROUTEMAP_WANTED
            u4Status = RMapGetInitialMapStatus (pAggrEntry->au1AdvMapName);
#endif
            if (u4Status != 0)
            {
                u4AdvStatus = FILTERNIG_STAT_ENABLE;
            }
        }
        if (pAggrEntry->u1SuppressMapLen > 0)
        {
            MEMCPY (SuppressFilter.au1DistInOutFilterRMapName,
                    pAggrEntry->au1SuppressMapName,
                    pAggrEntry->u1SuppressMapLen);
#ifdef ROUTEMAP_WANTED
            u4Status = RMapGetInitialMapStatus (pAggrEntry->au1SuppressMapName);
#endif
            if (u4Status != 0)
            {
                u4SuppStatus = FILTERNIG_STAT_ENABLE;
            }
        }
        if (pAggrEntry->u1AttMapLen > 0)
        {
            MEMCPY (AttFilter.au1DistInOutFilterRMapName,
                    pAggrEntry->au1AttMapName, pAggrEntry->u1AttMapLen);
#ifdef ROUTEMAP_WANTED
            u4Status = RMapGetInitialMapStatus (pAggrEntry->au1AttMapName);
#endif
            if (u4Status != 0)
            {
                u4AttrStatus = FILTERNIG_STAT_ENABLE;
            }
        }
    }

    /* Set the AGGREGATED flag. */
    BGP4_RT_SET_FLAG (pAggrRoute, BGP4_RT_AGGREGATED);

    TMO_DYN_SLL_Scan (pTobeAggrList, pRtLink, pNextRtLink, tLinkNode *)
    {
        pRtProfile = pRtLink->pRouteProfile;
        if (pAggrEntry != NULL)
        {
            if (u4AdvStatus == FILTERNIG_STAT_ENABLE)
            {
#ifdef ROUTEMAP_WANTED
                if (Bgp4CheckAggrForAdvMap (pRtProfile,
                                            pAggrEntry->au1AdvMapName) ==
                    BGP4_FALSE)
                {
                    BGP4_RT_RESET_FLAG (pRtProfile,
                                        (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                         BGP4_RT_ADVT_NOTTO_EXTERNAL));
                    Bgp4AddFeasRouteToIntPeerAdvtList (pRtProfile, NULL);
                    Bgp4AddFeasRouteToExtPeerAdvtList (pRtProfile, NULL);
                    TMO_SLL_Delete (pTobeAggrList, &(pRtLink->TSNext));
                    Bgp4MemReleaseLinkNode (pRtLink);
                    continue;
                }
#endif
            }
            Bgp4AggrAggregateBgpInfos (pAggrRoute, pRtProfile,
                                       pAggrEntry->u1AdvType,
                                       pAggrEntry->u1AsSet);

            if (u4SuppStatus == FILTERNIG_STAT_ENABLE)
            {
#ifdef ROUTEMAP_WANTED
                if (Bgp4CheckAggrForSuppMap (pRtProfile,
                                             pAggrEntry->au1SuppressMapName) ==
                    BGP4_TRUE)
                {
                    BGP4_RT_RESET_FLAG (pRtProfile,
                                        (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                         BGP4_RT_ADVT_NOTTO_EXTERNAL));
                    Bgp4AddFeasRouteToIntPeerAdvtList (pRtProfile, NULL);
                    Bgp4AddFeasRouteToExtPeerAdvtList (pRtProfile, NULL);
                    TMO_SLL_Delete (pTobeAggrList, &(pRtLink->TSNext));
                    Bgp4MemReleaseLinkNode (pRtLink);
                }
#endif
            }
        }
    }
    /* Attribute-map
     * All the attributes are overwritten by this attribute-map*/
    if (u4AttrStatus == FILTERNIG_STAT_ENABLE)
    {
#ifdef ROUTEMAP_WANTED
        if (pAggrEntry != NULL)
        {
            Bgp4FillAggrForAttMap (pAggrRoute,
                                   pAggrEntry->au1AttMapName, pAggrRoute);
        }
#endif
    }
    BgpSetRoutelistflag (pTobeAggrList, BGP4_RT_AGGREGATED);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AggrAggregateBgpInfos                                 */
/* Description   : Forms an aggregated route by combining some of the        */
/*                 attributes to the specific routes.                        */
/* Input(s)      : Aggregated or Non-Aggregated route(pAggrRoute),           */
/*                 Non-Aggregated route whose info. needs to be accumulated  */
/*                 into the aggregated route(pSpecificRoute)                 */
/* Output(s)     : Updated Aggregated BGP Route (pAggrRoute)                 */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4AggrAggregateBgpInfos (tRouteProfile * pAggrRoute,
                           tRouteProfile * pSpecificRoute, UINT1 u1AdvType,
                           UINT1 u1AsSet)
{
    tBgp4Info          *pAggrBgpInfo = NULL;
    tBgp4Info          *pSpecBgpInfo = NULL;
    UINT4               u4AggrFlag = BGP4_FALSE;
    UINT4               u4Index = 0;
    tAsPath            *pAspath = NULL;
    tAsPath            *pAsSetNode = NULL;
    tAsPath            *pAsSetNextNode = NULL;
    tAsPath            *pPrevAsSetNode = NULL;

    pAggrBgpInfo = BGP4_RT_BGP_INFO (pAggrRoute);
    pSpecBgpInfo = BGP4_RT_BGP_INFO (pSpecificRoute);

    if (BGP4_RT_PROTOCOL (pAggrRoute) == PROTO_INVALID)
    {
        /* This is for the first time the aggregated route is 
         * getting filled up. Protocol of the aggregated route
         * should be BGP4_OTHERS_ID */
        BGP4_RT_PROTOCOL (pAggrRoute) = BGP4_OTHERS_ID;
    }

    if (pAggrBgpInfo == NULL)
    {
        BGP4_RT_BGP4_INF (pAggrRoute) =
            Bgp4MemAllocateBgp4info (sizeof (tBgp4Info));
        pAggrBgpInfo = BGP4_RT_BGP_INFO (pAggrRoute);
        if (pAggrBgpInfo == NULL)
        {
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                           BGP4_MOD_NAME,
                           "\tMemory Allocation for advertising Aggregated "
                           "Route Info \n\t- Dest %s, Mask %s FAILED!!!\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pAggrRoute),
                                            BGP4_RT_AFI_INFO (pAggrRoute)),
                           Bgp4PrintIpMask ((UINT1)
                                            BGP4_RT_PREFIXLEN (pAggrRoute),
                                            BGP4_RT_AFI_INFO (pAggrRoute)));
            gu4BgpDebugCnt[MAX_BGP_ROUTE_INFO_ENTRIES_SIZING_ID]++;
            return BGP4_FAILURE;
        }
        Bgp4AggrCopyBgp4infos (pAggrRoute, pSpecificRoute, u1AdvType, u1AsSet);
        BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pAggrRoute)) |=
            BGP4_ATTR_MED_MASK;
        BGP4_INFO_REF_COUNT (pAggrBgpInfo)++;
        if (pAggrRoute->u4MED == BGP4_INV_MEDLP)
        {
            pAggrRoute->u4MED =
                BGP4_DEFAULT_IGP_METRIC (BGP4_RT_CXT_ID (pAggrRoute));
        }
        BGP4_INFO_WEIGHT (BGP4_RT_BGP_INFO (pAggrRoute)) =
            BGP4_WEIGHT_DEFAULT_VALUE;
        return BGP4_SUCCESS;
    }
    pAggrRoute->u4MED = BGP4_DEFAULT_IGP_METRIC (BGP4_RT_CXT_ID (pAggrRoute));
    /* Aggregation of these two BGP infos is done. */
    /* Filling up the ORIGIN attribute   */
    if ((BGP4_INFO_ORIGIN (pAggrBgpInfo) == BGP4_ATTR_ORIGIN_INCOMPLETE)
        || (BGP4_INFO_ORIGIN (pSpecBgpInfo) == BGP4_ATTR_ORIGIN_INCOMPLETE))
    {
        BGP4_INFO_ORIGIN (pAggrBgpInfo) = BGP4_ATTR_ORIGIN_INCOMPLETE;
    }
    else if ((BGP4_INFO_ORIGIN (pAggrBgpInfo) == BGP4_ATTR_ORIGIN_EGP)
             || (BGP4_INFO_ORIGIN (pSpecBgpInfo) == BGP4_ATTR_ORIGIN_EGP))
    {
        BGP4_INFO_ORIGIN (pAggrBgpInfo) = BGP4_ATTR_ORIGIN_EGP;
    }
    else
    {
        BGP4_INFO_ORIGIN (pAggrBgpInfo) = BGP4_ATTR_ORIGIN_IGP;
    }

    /* If the PATH attributes are identical or if the new route to be 
     * aggregated is non-BGP route, then no need to do any 
     * updation of the AS PATH.
     */
    if ((BGP4_RT_PROTOCOL (pSpecificRoute) == BGP_ID) &&
        (Bgp4AttrIsAspathIdentical (pSpecBgpInfo, pAggrBgpInfo) != TRUE))
    {
        if ((BGP4_RT_GET_FLAGS (pAggrRoute) & BGP4_RT_AGGREGATED) ==
            BGP4_RT_AGGREGATED)
        {
            u4AggrFlag = BGP4_TRUE;
            u4Index = Bgp4AggrFindAggrEntry (pAggrRoute, u4Index);
            if (u4Index == 0)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tInvalid Index Value\n ");
                return BGP4_FAILURE;
            }
        }
        /* Else branch added as Coverity fix */
        else
        {
            /* Since u4Index = 0, u4Index - BGP4_DECREMENT_BY_ONE will
             * be an integer overflow, hence returning failure here */
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tInvalid Index Value\n ");
            return BGP4_FAILURE;
        }

        Bgp4AggrUpdateAspath (pAggrBgpInfo, pSpecBgpInfo, u4AggrFlag);
        if (BGP4_AGGRENTRY_AS_SET (u4Index - BGP4_DECREMENT_BY_ONE) ==
            AS_SET_DISABLE)
        {
            BGP_SLL_DYN_Scan (BGP4_INFO_ASPATH (pAggrBgpInfo), pAsSetNode,
                              pAsSetNextNode, tAsPath *)
            {
                if ((pAsSetNode->u1Type == BGP4_ATTR_PATH_SET) ||
                    (pAsSetNode->u1Type == BGP4_ATTR_PATH_CONFED_SET))
                {
                    pPrevAsSetNode =
                        (tAsPath *)
                        TMO_SLL_Previous (BGP4_INFO_ASPATH (pAggrBgpInfo),
                                          (tTMO_SLL_NODE *) pAsSetNode);
                    TMO_SLL_Delete (BGP4_INFO_ASPATH (pAggrBgpInfo),
                                    (tTMO_SLL_NODE *) pAsSetNode);
                    pAsSetNode = pPrevAsSetNode;
                    BGP4_AGGRENTRY_AS_ATOMIC_AGGR (u4Index -
                                                   BGP4_DECREMENT_BY_ONE) =
                        BGP4_TRUE;
                }
            }
        }

        pAspath = (tAsPath *) TMO_SLL_First (BGP4_INFO_ASPATH (pSpecBgpInfo));
        if ((pAspath != NULL)
            && (BGP4_ASPATH_TYPE (pAspath) == BGP4_ATTR_PATH_SET))
        {
            BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pAggrRoute)) &=
                ~(BGP4_ATTR_MED_MASK);
        }

    }

    /* Filling up the ATOMIC_AGGREGATE  attribute */
    if ((BGP4_INFO_ATTR_FLAG (pSpecBgpInfo) & BGP4_ATTR_ATOMIC_AGGR_MASK)
        == BGP4_ATTR_ATOMIC_AGGR_MASK)
    {
        BGP4_INFO_ATTR_FLAG (pAggrBgpInfo) |= BGP4_ATTR_ATOMIC_AGGR_MASK;
    }

    /* If the aggregated route does not carry the ATOMIC_AGGREGATE
     * attribute, then the resulting aggregate route should have 
     * a community path attribute which contains all communities
     * from all the routes used for aggregation. */
    if ((BGP4_INFO_ATTR_FLAG (pAggrBgpInfo) & BGP4_ATTR_ATOMIC_AGGR_MASK)
        == BGP4_ATTR_ATOMIC_AGGR_MASK)
    {
        /* ATOMIC_AGGREGATE attribute present in aggregate route.
         * Aggregate route should not contain any community attribute.*/
        if (BGP4_INFO_COMM_ATTR (pAggrBgpInfo) != NULL)
        {
            /* Community attribute is present in the aggregated route.
             * Remove the community attributes. */
            ATTRIBUTE_NODE_FREE (BGP4_INFO_COMM_ATTR_VAL (pAggrBgpInfo));
            COMMUNITY_NODE_FREE (BGP4_INFO_COMM_ATTR (pAggrBgpInfo));
            BGP4_INFO_COMM_ATTR (pAggrBgpInfo) = NULL;
            BGP4_INFO_ATTR_FLAG (pAggrBgpInfo) &= (~(BGP4_ATTR_COMM_MASK));
        }
    }
    else
    {
        /* Need to aggregate community attributes. */
        CommUpdateAggrComm (pAggrRoute, pSpecificRoute);
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AggrCheckForReAggregation                             */
/* Description   : Checks whether the given aggregated route can be          */
/*                 reaggregated or not. If possible, then the route is       */
/*                 reaggregated.                                             */
/* Input(s)      : (pAggrRoute) - Aggregated Route                           */
/*                 (pAggrRtList) - List of routes used for aggregation       */
/* Output(s)     : (pAggrRoute) - Re-Aggregated Route                        */
/* Return(s)     : BGP4_TRUE    - if route Re-Aggregated                     */
/*                 BGP4_FALSE   - if route is not Re-Aggregated              */
/*****************************************************************************/
UINT4
Bgp4AggrCheckForReAggregation (tRouteProfile * pAggrRoute,
                               tTMO_SLL * pAggrRtList, UINT1 u1AdvType,
                               UINT1 u1AsSet)
{
    tRouteProfile       TsOldAggrRtProfile;
    tBgp4Info           TsBgpRtInfo;
    UINT4               u4IsRoutesIdentical = BGP4_FALSE;

    Bgp4InitRtProfile (&TsOldAggrRtProfile);
    Bgp4InitBgp4info (&TsBgpRtInfo);
    BGP4_RT_BGP4_INF ((&TsOldAggrRtProfile)) = (&TsBgpRtInfo);

    /* Copy the given aggregated route */
    Bgp4AggrCopyRoutes (&TsOldAggrRtProfile, pAggrRoute, u1AdvType, u1AsSet);

    Bgp4DshReleaseBgpInfo (BGP4_RT_BGP_INFO (pAggrRoute));
    BGP4_RT_BGP4_INF (pAggrRoute) = NULL;
    pAggrRoute->u4MED = BGP4_INV_MEDLP;

    BGP4_RT_PROTOCOL (pAggrRoute) = PROTO_INVALID;
    BGP4_RT_SET_RT_IF_INDEX (pAggrRoute, BGP4_INVALID_IFINDX);

    Bgp4AggrReaggregate (pAggrRoute, pAggrRtList);

    /* Check if the new Aggregated route and previous aggregated route are 
     * identical */
    u4IsRoutesIdentical = Bgp4IsRtsIdentical (pAggrRoute, &TsOldAggrRtProfile);

    /* Release the memory allocated for ASPath and ASPath-no */
    Bgp4DshReleaseAsPathList (BGP4_INFO_ASPATH ((&TsBgpRtInfo)));

    if (BGP4_INFO_COMM_ATTR ((&TsBgpRtInfo)) != NULL)
    {
        ATTRIBUTE_NODE_FREE (BGP4_INFO_COMM_ATTR_VAL ((&TsBgpRtInfo)));
        COMMUNITY_NODE_FREE (BGP4_INFO_COMM_ATTR ((&TsBgpRtInfo)));
        BGP4_INFO_COMM_ATTR ((&TsBgpRtInfo)) = NULL;
        BGP4_INFO_ATTR_FLAG ((&TsBgpRtInfo)) &= (~(BGP4_ATTR_COMM_MASK));
    }

    if (u4IsRoutesIdentical == BGP4_TRUE)
    {
        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tThe aggregated route %s mask %s is not re-aggregated.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pAggrRoute),
                                        BGP4_RT_AFI_INFO (pAggrRoute)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                                        (pAggrRoute),
                                        BGP4_RT_AFI_INFO (pAggrRoute)));
        return BGP4_FALSE;        /* Route not re-aggregated */
    }
    else
    {
        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tThe aggregated route %s mask %s is re-aggregated.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pAggrRoute),
                                        BGP4_RT_AFI_INFO (pAggrRoute)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                                        (pAggrRoute),
                                        BGP4_RT_AFI_INFO (pAggrRoute)));
        return BGP4_TRUE;        /* Route re-aggregated */
    }
}

/*****************************************************************************/
/* Function Name : Bgp4AggrReleaseAggrList                                   */
/* Description   : While an aggregate entry is configured, list of routes    */
/*                 which can be used for aggregation are formed and stored   */
/*                 in the corresponding Aggregate Table entry. This function */
/*                 frees all such list of routes from the Aggregate Table.   */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4AggrReleaseAggrList (UINT4 u4Context)
{
    tRouteProfile      *pAggrRoute = NULL;
    INT4                i4AggrtblIndex;

    for (i4AggrtblIndex = 0; i4AggrtblIndex < BGP4_MAX_AGGR_ENTRIES;
         i4AggrtblIndex++)
    {
        if (BGP4_AGGRENTRY_VRFID (i4AggrtblIndex) != u4Context)
        {
            continue;
        }
        if (BGP4_AGGRENTRY_ADMIN (i4AggrtblIndex) == BGP4_ADMIN_UP)
        {
            if (TMO_SLL_Count (BGP4_AGGRENTRY_AGGR_LIST (i4AggrtblIndex)) > 0)
            {
                Bgp4DshReleaseList (BGP4_AGGRENTRY_AGGR_LIST (i4AggrtblIndex),
                                    0);
            }
            /* De-aggregate the aggregate route and reset the operational
             * status. */
            pAggrRoute = BGP4_AGGRENTRY_AGGR_ROUTE (i4AggrtblIndex);
            if ((BGP4_RT_GET_FLAGS (pAggrRoute) & BGP4_RT_AGGREGATED) ==
                BGP4_RT_AGGREGATED)
            {
                Bgp4DshReleaseBgpInfo (BGP4_RT_BGP_INFO (pAggrRoute));
                BGP4_RT_BGP4_INF (pAggrRoute) = NULL;
                pAggrRoute->u4MED = BGP4_INV_MEDLP;
                BGP4_RT_RESET_FLAG (pAggrRoute, BGP4_RT_AGGREGATED);
                BGP4_RT_PROTOCOL (pAggrRoute) = PROTO_INVALID;
                BGP4_RT_SET_RT_IF_INDEX (pAggrRoute, BGP4_INVALID_IFINDX);
            }
            BGP4_AGGR_RESET_STATUS ((&(BGP4_AGGRENTRY[i4AggrtblIndex])),
                                    BGP4_AGGR_ALL_FLAG);
        }
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AggrUpdateAspath                                      */
/* Description   : While forming the aggregate route, the AS_PATH attribute  */
/*                 present in the BGP update needs to be combined as defined */
/*                 in the RFC. This function updates the AS_PATH attribute of*/
/*                 aggregated route using the two input BGP infos.           */
/* Input(s)      : BGP Info. of Route R1 (This info can be of the Aggregated */
/*                 route or Non-Aggregated route - pAggrBgpInfo),            */
/*                 BGP Info. of Route R2 (pBgpInfo)                          */
/*                 u4AggrFlag - Flag indicating whether aggregated route     */
/*                 exists or not                                             */
/* Output(s)     : BGP information with the Aggregated AS_PATH               */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4AggrUpdateAspath (tBgp4Info * pAggrBgpInfo, tBgp4Info * pBgpInfo,
                      UINT4 u4AggrFlag)
{
    tTMO_SLL            TsNewAggrAspath;
    tAsPath            *pAggrAspath = NULL;
    tAsPath            *pAspath = NULL;
    tAsPath            *pNewAspath = NULL;
    UINT4              *pu4AggrAsnos = NULL;
    UINT4              *pu4SpecAsnos = NULL;
    UINT2               u2AssetSize = 0;
    UINT2               u2SetArrayIndex = 0;
    UINT1               u1MatchingAsseq = 0;
    UINT1               u1Flag = BGP4_FALSE;
    UINT2               u2CopyArrayIndex = 0;
    UINT4               au4AssetArray[BGP4_MAX_TEMP_ASARRAY_SIZE];
    UINT4               u4AssegLen = 0;

    /* The  size of this array needs to be fixed. */
    /* 2*FF = 510, since the aspath len is 
     * one byte in len, if both the tuples 
     * mismatches at the max. it can take 
     * upto this many ASnos. 
     */

    BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\tBgp4AggrUpdateAspath() ....... \n");

    TMO_SLL_Init (&TsNewAggrAspath);
    MEMSET ((UINT1 *) au4AssetArray, 0,
            sizeof (UINT4) * BGP4_MAX_TEMP_ASARRAY_SIZE);

    pAggrAspath = (tAsPath *) TMO_SLL_First (BGP4_INFO_ASPATH (pAggrBgpInfo));
    pAspath = (tAsPath *) TMO_SLL_First (BGP4_INFO_ASPATH (pBgpInfo));

    if ((pAspath == NULL) && (pAggrAspath == NULL))
    {
        return BGP4_SUCCESS;
    }

    if ((pAggrAspath == NULL) && (pAspath != NULL))
    {
        TMO_SLL_Scan (BGP4_INFO_ASPATH (pBgpInfo), pAspath, tAsPath *)
        {
            pAggrAspath = Bgp4MemGetASNode (sizeof (tAsPath));
            if (pAggrAspath == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                          "\tMemory Allocation for Aggregate Route's AS Node "
                          "FAILED!!!\n");
                gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
                return BGP4_FAILURE;
            }
            /* if this is the first candidate route then Aspath info can be 
             * copied as it is. otherwise copy as a corresponding set. 
             */
            if (u4AggrFlag == BGP4_FALSE)
            {
                BGP4_ASPATH_TYPE (pAggrAspath) = BGP4_ASPATH_TYPE (pAspath);
            }
            else
            {
                if ((BGP4_ASPATH_TYPE (pAspath)
                     == BGP4_ATTR_PATH_CONFED_SEQUENCE)
                    || (BGP4_ASPATH_TYPE (pAspath)
                        == BGP4_ATTR_PATH_CONFED_SET))
                {
                    BGP4_ASPATH_TYPE (pAggrAspath) = BGP4_ATTR_PATH_CONFED_SET;
                }
                else if ((BGP4_ASPATH_TYPE (pAspath)
                          == BGP4_ATTR_PATH_SEQUENCE)
                         || (BGP4_ASPATH_TYPE (pAspath) == BGP4_ATTR_PATH_SET))
                {
                    BGP4_ASPATH_TYPE (pAggrAspath) = BGP4_ATTR_PATH_SET;
                }
                else
                {
                    /* invalid Aspath segment type */
                    Bgp4MemReleaseASNode (pAggrAspath);
                    return BGP4_FAILURE;
                }
            }
            BGP4_ASPATH_LEN (pAggrAspath) = BGP4_ASPATH_LEN (pAspath);
            if (BGP4_AS4_SEG_LEN <
                (BGP4_ASPATH_LEN (pAggrAspath) * BGP4_AS4_LENGTH))
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                          BGP4_MOD_NAME,
                          "\tSize of au1ASSegs is less,increase the value of BGP4_AS4_SEG_LEN  "
                          "FAILED!!!\n");
                Bgp4MemReleaseASNode (pAggrAspath);
                return BGP4_FAILURE;
            }
            MEMSET (BGP4_ASPATH_NOS (pAggrAspath), 0, BGP4_AS4_SEG_LEN);
            MEMCPY (BGP4_ASPATH_NOS (pAggrAspath), BGP4_ASPATH_NOS (pAspath),
                    (BGP4_ASPATH_LEN (pAspath) * BGP4_AS4_LENGTH));

            TMO_SLL_Add (BGP4_INFO_ASPATH (pAggrBgpInfo),
                         &pAggrAspath->sllNode);
        }
        return BGP4_SUCCESS;
    }

    /* if the new Aspath is NULL, just convert SEQs to corresponding SETs */
    if ((pAggrAspath != NULL) && (pAspath == NULL))
    {
        TMO_SLL_Scan (BGP4_INFO_ASPATH (pAggrBgpInfo), pAggrAspath, tAsPath *)
        {
            if ((BGP4_ASPATH_TYPE (pAggrAspath)
                 == BGP4_ATTR_PATH_CONFED_SEQUENCE))
            {
                BGP4_ASPATH_TYPE (pAggrAspath) =
                    (UINT1) BGP4_ATTR_PATH_CONFED_SET;
            }
            else if ((BGP4_ASPATH_TYPE (pAggrAspath)
                      == BGP4_ATTR_PATH_SEQUENCE))
            {
                BGP4_ASPATH_TYPE (pAggrAspath) = (UINT1) BGP4_ATTR_PATH_SET;
            }
        }
        return BGP4_SUCCESS;
    }

    /* Aggregate Confed Path segments */
    /* If the first AS path tuple is of type Confed sequence in both the routes
     * then that tuple alone is aggregated first.
     */
    while ((BGP4_ASPATH_TYPE (pAspath) == BGP4_ASPATH_TYPE (pAggrAspath)) &&
           (BGP4_ASPATH_TYPE (pAspath) == BGP4_ATTR_PATH_CONFED_SEQUENCE))
    {
        u1MatchingAsseq = 0;
        pu4SpecAsnos = (UINT4 *) (VOID *) BGP4_ASPATH_NOS (pAspath);
        pu4AggrAsnos = (UINT4 *) (VOID *) BGP4_ASPATH_NOS (pAggrAspath);

        /* Find upto which point both the sequences are identical. */
        while ((u1MatchingAsseq < BGP4_ASPATH_LEN (pAspath)) &&
               (u1MatchingAsseq < BGP4_ASPATH_LEN (pAggrAspath)))
        {
            if (pu4SpecAsnos[u1MatchingAsseq] != pu4AggrAsnos[u1MatchingAsseq])
            {
                break;
            }
            u1MatchingAsseq++;
        }

        /* Upto the point where the ASnos are matching copy that
         * portion alone as a sequence and update the new aspath list. 
         */
        if (u1MatchingAsseq != 0)
        {
            u1Flag = BGP4_TRUE;
            pNewAspath = Bgp4MemGetASNode (sizeof (tAsPath));
            if (pNewAspath == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                          "\tMemory Allocation for Aggregate Route's AS Node "
                          "FAILED!!!\n");
                Bgp4DshReleaseAsPathList (&TsNewAggrAspath);
                gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
                return BGP4_FAILURE;
            }

            BGP4_ASPATH_TYPE (pNewAspath) =
                (UINT1) BGP4_ATTR_PATH_CONFED_SEQUENCE;
            BGP4_ASPATH_LEN (pNewAspath) = u1MatchingAsseq;
            if (BGP4_AS4_SEG_LEN < (u1MatchingAsseq * BGP4_AS4_LENGTH))
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                          BGP4_MOD_NAME,
                          "\tSize of au1ASSegs is less,increase the value of BGP4_AS4_SEG_LEN"
                          "FAILED!!!\n");
                Bgp4DshReleaseAsPathList (&TsNewAggrAspath);
                Bgp4MemReleaseASNode (pNewAspath);
                return BGP4_FAILURE;
            }
            MEMSET (BGP4_ASPATH_NOS (pNewAspath), 0, BGP4_AS4_SEG_LEN);
            MEMCPY (BGP4_ASPATH_NOS (pNewAspath), (UINT1 *) pu4AggrAsnos,
                    (u1MatchingAsseq * BGP4_AS4_LENGTH));

            TMO_SLL_Add (&TsNewAggrAspath, &pNewAspath->sllNode);
        }
        else
        {
            u1Flag = BGP4_FALSE;
        }
        /* Put the rest of the AS nos in the SEQ to the aggregated route
         * as CONFED_SET. Check for duplication too.
         */
        if ((BGP4_ASPATH_LEN (pAspath) != BGP4_ASPATH_LEN (pAggrAspath))
            || (u1MatchingAsseq != BGP4_ASPATH_LEN (pAspath))
            || (u1MatchingAsseq != BGP4_ASPATH_LEN (pAggrAspath)))
        {
            BGP4_DBG1 (BGP4_DBG_CTRL_FLOW,
                       "\tPending ASnos SEQ. matching=%d \n", u1MatchingAsseq);
            u2CopyArrayIndex = u1MatchingAsseq;
            /* Fill the set array with all the ASnos present in 
             * the old aggr. route. 
             */
            if ((u2CopyArrayIndex < BGP4_ASPATH_LEN (pAggrAspath))
                && (u2CopyArrayIndex < BGP4_AS4_SEG_LEN))
            {
                do
                {
                    if ((BGP4_MAX_TEMP_ASARRAY_SIZE) > u2SetArrayIndex)
                    {
                        au4AssetArray[u2SetArrayIndex]
                            = pu4AggrAsnos[u2CopyArrayIndex];
                        u2CopyArrayIndex++;
                        u2SetArrayIndex++;
                    }
                }
                while (u2CopyArrayIndex < BGP4_ASPATH_LEN (pAggrAspath));
            }

            /* Get the ASnos from the specific route and check whether
             * that ASno is already present in the aggr. route. If it
             * is not present then add that Asno also in the array. 
             */
            while ((u1MatchingAsseq < BGP4_ASPATH_LEN (pAspath))
                   && (u1MatchingAsseq < BGP4_AS4_SEG_LEN))
            {
                if ((Bgp4AggrFindAsnoInarray (au4AssetArray, u2SetArrayIndex,
                                              pu4SpecAsnos[u1MatchingAsseq]) ==
                     FALSE) &&
                    (Bgp4IsASnoFound
                     ((BGP4_INFO_ASPATH (pAggrBgpInfo)),
                      (UINT4) OSIX_NTOHL (pu4SpecAsnos[u1MatchingAsseq])) ==
                     FALSE))
                {
                    if ((BGP4_AS4_LENGTH * BGP4_MAX_TEMP_ASARRAY_SIZE) >
                        u2SetArrayIndex)
                    {
                        au4AssetArray[u2SetArrayIndex] =
                            pu4SpecAsnos[u1MatchingAsseq];
                        u2SetArrayIndex++;
                    }
                }
                u1MatchingAsseq++;
            }
        }
        pAspath = (tAsPath *) TMO_SLL_Next (BGP4_INFO_ASPATH (pBgpInfo),
                                            &pAspath->sllNode);
        pAggrAspath =
            (tAsPath *) TMO_SLL_Next (BGP4_INFO_ASPATH (pAggrBgpInfo),
                                      &pAggrAspath->sllNode);
        if (u1Flag == BGP4_TRUE)
        {
            /* No more matching AS-Confed Sequence. */
            break;
        }
        if ((pAspath == NULL) || (pAggrAspath == NULL))
        {
            break;
        }
    }

    /* Copy all the AS nos from the old aggregated route in to the SET array.
     *  Excluding the first ASPATH tuple.
     */
    while ((pAggrAspath) &&
           ((BGP4_ASPATH_TYPE (pAggrAspath) == BGP4_ATTR_PATH_CONFED_SEQUENCE)
            || (BGP4_ASPATH_TYPE (pAggrAspath) == BGP4_ATTR_PATH_CONFED_SET)))
    {
        u2AssetSize = BGP4_ASPATH_LEN (pAggrAspath);
        while ((u2AssetSize + u2SetArrayIndex) > BGP4_MAX_TEMP_ASARRAY_SIZE)
        {
            /* AS Path segment can hold only max of 255. So allocate 
             * a AS Confed Type and add to the new AS path list. */
            pNewAspath = Bgp4MemGetASNode (sizeof (tAsPath));
            if (pNewAspath == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                          "\tMemory Allocation for Aggregate Route's AS Node "
                          "FAILED!!!\n");
                Bgp4DshReleaseAsPathList (&TsNewAggrAspath);
                gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
                return BGP4_FAILURE;
            }

            BGP4_ASPATH_TYPE (pNewAspath) = BGP4_ATTR_PATH_CONFED_SET;
            BGP4_ASPATH_LEN (pNewAspath) = (UINT1) u2SetArrayIndex;
            if (BGP4_AS4_SEG_LEN <
                BGP4_ASPATH_LEN (pNewAspath) * BGP4_AS4_LENGTH)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                          BGP4_MOD_NAME,
                          "\tSize of au1ASSegs is less,increase the value of BGP4_AS4_SEG_LEN"
                          "FAILED!!!\n");
                Bgp4DshReleaseAsPathList (&TsNewAggrAspath);
                Bgp4MemReleaseASNode (pNewAspath);
                return BGP4_FAILURE;
            }
            MEMSET (BGP4_ASPATH_NOS (pNewAspath), 0, BGP4_AS4_SEG_LEN);
            MEMCPY (BGP4_ASPATH_NOS (pNewAspath), (UINT1 *) au4AssetArray,
                    (u2SetArrayIndex * BGP4_AS4_LENGTH));

            TMO_SLL_Add (&TsNewAggrAspath, &pNewAspath->sllNode);
            if (u2SetArrayIndex > BGP4_MAX_TEMP_ASARRAY_SIZE)
            {
                /* Move the remaining AS-SET array front */
                MEMCPY ((UINT1 *) au4AssetArray,
                        &au4AssetArray[BGP4_MAX_TEMP_ASARRAY_SIZE],
                        BGP4_MAX_TEMP_ASARRAY_SIZE * sizeof (UINT4));
                MEMSET ((UINT1 *) &au4AssetArray[BGP4_MAX_TEMP_ASARRAY_SIZE],
                        0, BGP4_MAX_TEMP_ASARRAY_SIZE * sizeof (UINT4));
                u2SetArrayIndex -= BGP4_MAX_TEMP_ASARRAY_SIZE;
            }
            else
            {
                MEMSET ((UINT1 *) au4AssetArray, 0,
                        sizeof (UINT4) * BGP4_AS4_LENGTH *
                        BGP4_MAX_TEMP_ASARRAY_SIZE);
                u2SetArrayIndex = 0;
            }
        }
        MEMCPY ((UINT1 *) (&(au4AssetArray[u2SetArrayIndex])),
                BGP4_ASPATH_NOS (pAggrAspath), (u2AssetSize * BGP4_AS4_LENGTH));
        u2SetArrayIndex += u2AssetSize;

        pAggrAspath =
            (tAsPath *) TMO_SLL_Next (BGP4_INFO_ASPATH (pAggrBgpInfo),
                                      &pAggrAspath->sllNode);
    }

    /*  Fill the set array with the AS nos. which are not present in the 
     *  old aggregated route. The ASnos present in the old aggregated routes
     *  are taken as such. 
     */
    while ((pAspath) &&
           ((BGP4_ASPATH_TYPE (pAspath) == BGP4_ATTR_PATH_CONFED_SEQUENCE) ||
            (BGP4_ASPATH_TYPE (pAspath) == BGP4_ATTR_PATH_CONFED_SET)))
    {
        u2CopyArrayIndex = 0;
        pu4SpecAsnos = (UINT4 *) (VOID *) BGP4_ASPATH_NOS (pAspath);

        while (u2SetArrayIndex > BGP4_MAX_TEMP_ASARRAY_SIZE)
        {
            /* AS Path segment can hold only max of 255. So allocate 
             * a AS Confed Type and add to the new AS path list. */
            pNewAspath = Bgp4MemGetASNode (sizeof (tAsPath));
            if (pNewAspath == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                          "\tMemory Allocation for Aggregate Route's AS Node "
                          "FAILED!!!\n");
                Bgp4DshReleaseAsPathList (&TsNewAggrAspath);
                gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
                return BGP4_FAILURE;
            }

            BGP4_ASPATH_TYPE (pNewAspath) = BGP4_ATTR_PATH_CONFED_SET;
            BGP4_ASPATH_LEN (pNewAspath) = (UINT1) u2SetArrayIndex;
            if (BGP4_AS4_SEG_LEN < (u2SetArrayIndex * BGP4_AS4_LENGTH))
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                          BGP4_MOD_NAME,
                          "\tSize of au1ASSegs is less,increase the value of BGP4_AS4_SEG_LEN"
                          "FAILED!!!\n");
                Bgp4DshReleaseAsPathList (&TsNewAggrAspath);
                Bgp4MemReleaseASNode (pNewAspath);
                return BGP4_FAILURE;
            }
            MEMSET (BGP4_ASPATH_NOS (pNewAspath), 0, BGP4_AS4_SEG_LEN);
            MEMCPY (BGP4_ASPATH_NOS (pNewAspath), (UINT1 *) au4AssetArray,
                    (u2SetArrayIndex * BGP4_AS4_LENGTH));

            TMO_SLL_Add (&TsNewAggrAspath, &pNewAspath->sllNode);
            if (u2SetArrayIndex > BGP4_MAX_TEMP_ASARRAY_SIZE)
            {
                /* Move the remaining AS-SET array front */
                MEMCPY ((UINT1 *) au4AssetArray,
                        &au4AssetArray[BGP4_MAX_TEMP_ASARRAY_SIZE],
                        BGP4_MAX_TEMP_ASARRAY_SIZE * sizeof (UINT4));
                MEMSET ((UINT1 *) &au4AssetArray[BGP4_MAX_TEMP_ASARRAY_SIZE],
                        0, BGP4_MAX_TEMP_ASARRAY_SIZE * sizeof (UINT4));
                u2SetArrayIndex -= BGP4_MAX_TEMP_ASARRAY_SIZE;
            }
        }
        /* Scan thro' all the AS nos present in the specific
         * route and check whether it is already present in the
         * old aggregated route. If it is not present then add
         * that AS no to the SET array. 
         */
        while (u2CopyArrayIndex < BGP4_ASPATH_LEN (pAspath))
        {
            if (Bgp4IsASnoFound ((BGP4_INFO_ASPATH (pAggrBgpInfo)),
                                 (UINT4)
                                 OSIX_NTOHL (pu4SpecAsnos[u2CopyArrayIndex])) ==
                FALSE)
            {
                if ((BGP4_AS4_LENGTH * BGP4_MAX_TEMP_ASARRAY_SIZE) >
                    u2SetArrayIndex)
                {
                    au4AssetArray[u2SetArrayIndex] =
                        pu4SpecAsnos[u2CopyArrayIndex];
                    u2SetArrayIndex++;
                }
            }
            u2CopyArrayIndex++;
        }
        pAspath = (tAsPath *) TMO_SLL_Next (BGP4_INFO_ASPATH (pBgpInfo),
                                            &pAspath->sllNode);
    }

    if (u2SetArrayIndex > 0)
    {
        u2CopyArrayIndex = 0;
        while (u2CopyArrayIndex < u2SetArrayIndex)
        {
            pNewAspath = Bgp4MemGetASNode (sizeof (tAsPath));
            if (pNewAspath == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                          "\tMemory Allocation for Aggregate Route's AS Node "
                          "FAILED!!!\n");
                Bgp4DshReleaseAsPathList (&TsNewAggrAspath);
                gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
                return BGP4_FAILURE;
            }
            BGP4_ASPATH_TYPE (pNewAspath) = BGP4_ATTR_PATH_CONFED_SET;

            if ((u2SetArrayIndex - u2CopyArrayIndex) <=
                BGP4_MAX_TEMP_ASARRAY_SIZE)
            {
                u2AssetSize = (UINT2) (u2SetArrayIndex - u2CopyArrayIndex);
            }
            else
            {
                u2AssetSize = BGP4_MAX_TEMP_ASARRAY_SIZE;
            }
            if (BGP4_AS4_SEG_LEN < (u2AssetSize * BGP4_AS4_LENGTH))
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                          BGP4_MOD_NAME,
                          "\tSize of au1ASSegs is less,increase the value of BGP4_AS4_SEG_LEN"
                          "FAILED!!!\n");
                Bgp4DshReleaseAsPathList (&TsNewAggrAspath);
                Bgp4MemReleaseASNode (pNewAspath);
                return BGP4_FAILURE;
            }

            MEMSET (BGP4_ASPATH_NOS (pNewAspath), 0, BGP4_AS4_SEG_LEN);
            if (((BGP4_AS4_LENGTH * BGP4_MAX_TEMP_ASARRAY_SIZE) >
                 u2AssetSize * BGP4_AS4_LENGTH) &&
                (u2CopyArrayIndex <
                 BGP4_AS4_LENGTH * BGP4_MAX_TEMP_ASARRAY_SIZE))
            {
                MEMCPY (BGP4_ASPATH_NOS (pNewAspath),
                        (UINT1 *) (&(au4AssetArray[u2CopyArrayIndex])),
                        (u2AssetSize * BGP4_AS4_LENGTH));
            }
            BGP4_ASPATH_LEN (pNewAspath) = (UINT1) u2AssetSize;
            u2CopyArrayIndex = (UINT1) (u2CopyArrayIndex + u2AssetSize);

            TMO_SLL_Add (&TsNewAggrAspath, &pNewAspath->sllNode);
        }
    }

    u1MatchingAsseq = 0;
    u2SetArrayIndex = 0;

    /* If the first AS path tuple is of type sequence in both the routes
     * then that tuple alone is aggregated first.
     */
    while ((pAspath != NULL) && (pAggrAspath != NULL) &&
           (BGP4_ASPATH_TYPE (pAspath) == BGP4_ASPATH_TYPE (pAggrAspath)) &&
           (BGP4_ASPATH_TYPE (pAspath) == BGP4_ATTR_PATH_SEQUENCE))
    {
        u1MatchingAsseq = 0;
        pu4SpecAsnos = (UINT4 *) (VOID *) BGP4_ASPATH_NOS (pAspath);
        pu4AggrAsnos = (UINT4 *) (VOID *) BGP4_ASPATH_NOS (pAggrAspath);

        /* Find upto which point both the sequences are identical. */
        while ((u1MatchingAsseq < BGP4_ASPATH_LEN (pAspath)) &&
               (u1MatchingAsseq < BGP4_ASPATH_LEN (pAggrAspath)))
        {
            if (pu4SpecAsnos[u1MatchingAsseq] != pu4AggrAsnos[u1MatchingAsseq])
            {
                break;
            }
            u1MatchingAsseq++;
        }

        /* Upto the point where the ASnos are matching copy that
         * portion alone as a sequence and update the new aspath list. 
         */
        if (u1MatchingAsseq != 0)
        {
            u1Flag = BGP4_TRUE;
            pNewAspath = Bgp4MemGetASNode (sizeof (tAsPath));
            if (pNewAspath == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                          "\tMemory Allocation for Aggregate Route's AS Node "
                          "FAILED!!!\n");
                Bgp4DshReleaseAsPathList (&TsNewAggrAspath);
                gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
                return BGP4_FAILURE;
            }

            BGP4_ASPATH_TYPE (pNewAspath) = BGP4_ATTR_PATH_SEQUENCE;
            BGP4_ASPATH_LEN (pNewAspath) = u1MatchingAsseq;
            if (BGP4_AS4_SEG_LEN < (u1MatchingAsseq * BGP4_AS4_LENGTH))
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                          BGP4_MOD_NAME,
                          "\tSize of au1ASSegs is less,increase the value of BGP4_AS4_SEG_LEN "
                          "FAILED!!!\n");
                Bgp4DshReleaseAsPathList (&TsNewAggrAspath);
                Bgp4MemReleaseASNode (pNewAspath);
                return BGP4_FAILURE;
            }

            MEMSET (BGP4_ASPATH_NOS (pNewAspath), 0, BGP4_AS4_SEG_LEN);
            MEMCPY (BGP4_ASPATH_NOS (pNewAspath), (UINT1 *) pu4AggrAsnos,
                    (u1MatchingAsseq * BGP4_AS4_LENGTH));

            TMO_SLL_Add (&TsNewAggrAspath, &pNewAspath->sllNode);
        }
        else
        {
            u1Flag = BGP4_FALSE;
        }
        /* Put the rest of the AS nos in the SEQ to the aggregated route
         * as SET. Check for duplication too.
         */
        if ((BGP4_ASPATH_LEN (pAspath) != BGP4_ASPATH_LEN (pAggrAspath))
            || (u1MatchingAsseq != BGP4_ASPATH_LEN (pAspath))
            || (u1MatchingAsseq != BGP4_ASPATH_LEN (pAggrAspath)))
        {
            BGP4_DBG1 (BGP4_DBG_CTRL_FLOW,
                       "\tPending ASnos SEQ. matching=%d \n", u1MatchingAsseq);
            u2CopyArrayIndex = u1MatchingAsseq;
            /* Fill the set array with all the ASnos present in 
             * the old aggr. route. 
             */
            if (u2CopyArrayIndex < BGP4_ASPATH_LEN (pAggrAspath))
            {
                do
                {
                    if ((BGP4_AS4_LENGTH * BGP4_MAX_TEMP_ASARRAY_SIZE) >
                        u2SetArrayIndex)
                    {
                        au4AssetArray[u2SetArrayIndex]
                            = pu4AggrAsnos[u2CopyArrayIndex];
                        u2CopyArrayIndex++;
                        u2SetArrayIndex++;
                    }
                }
                while (u2CopyArrayIndex < BGP4_ASPATH_LEN (pAggrAspath));
            }

            /* Get the ASnos from the specific route and check whether
             * that ASno is already present in the aggr. route. If it
             * is not present then add that Asno also in the array. 
             */
            while (u1MatchingAsseq < BGP4_ASPATH_LEN (pAspath))
            {
                if ((Bgp4AggrFindAsnoInarray (au4AssetArray, u2SetArrayIndex,
                                              pu4SpecAsnos[u1MatchingAsseq]) ==
                     FALSE) &&
                    (Bgp4IsASnoFound
                     ((BGP4_INFO_ASPATH (pAggrBgpInfo)),
                      (UINT4) OSIX_NTOHL (pu4SpecAsnos[u1MatchingAsseq])) ==
                     FALSE))
                {
                    if ((BGP4_AS4_LENGTH * BGP4_MAX_TEMP_ASARRAY_SIZE) >
                        u2SetArrayIndex)
                    {
                        au4AssetArray[u2SetArrayIndex] =
                            pu4SpecAsnos[u1MatchingAsseq];
                        u2SetArrayIndex++;
                    }
                }
                u1MatchingAsseq++;
            }
        }
        pAspath = (tAsPath *) TMO_SLL_Next (BGP4_INFO_ASPATH (pBgpInfo),
                                            &pAspath->sllNode);
        pAggrAspath =
            (tAsPath *) TMO_SLL_Next (BGP4_INFO_ASPATH (pAggrBgpInfo),
                                      &pAggrAspath->sllNode);
        if (u1Flag == BGP4_TRUE)
        {
            /* No more matching AS-Confed Sequence. */
            break;
        }
    }

    /* Copy all the AS nos from the old aggregated route in to the SET array.
     *  Excluding the first ASPATH tuple.
     */
    while (pAggrAspath != NULL)
    {
        u2AssetSize = BGP4_ASPATH_LEN (pAggrAspath);
        while ((u2AssetSize + u2SetArrayIndex) > BGP4_MAX_TEMP_ASARRAY_SIZE)
        {
            /* AS Path segment can hold only max of 255. So allocate 
             * a AS Confed Type and add to the new AS path list. */
            pNewAspath = Bgp4MemGetASNode (sizeof (tAsPath));
            if (pNewAspath == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                          "\tMemory Allocation for Aggregate Route's AS Node "
                          "FAILED!!!\n");
                Bgp4DshReleaseAsPathList (&TsNewAggrAspath);
                gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
                return BGP4_FAILURE;
            }

            BGP4_ASPATH_TYPE (pNewAspath) = BGP4_ATTR_PATH_SET;
            BGP4_ASPATH_LEN (pNewAspath) = (UINT1) u2SetArrayIndex;
            if (BGP4_AS4_SEG_LEN <
                BGP4_ASPATH_LEN (pNewAspath) * BGP4_AS4_LENGTH)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                          BGP4_MOD_NAME,
                          "\tSize of au1ASSegs is less,increase the value of BGP4_AS4_SEG_LEN "
                          "FAILED!!!\n");
                Bgp4DshReleaseAsPathList (&TsNewAggrAspath);
                Bgp4MemReleaseASNode (pNewAspath);
                return BGP4_FAILURE;
            }
            MEMSET (BGP4_ASPATH_NOS (pNewAspath), 0, BGP4_AS4_SEG_LEN);
            u4AssegLen =
                (UINT4) (((u2SetArrayIndex * BGP4_AS4_LENGTH) >
                          BGP4_AS4_SEG_LEN) ? BGP4_AS4_SEG_LEN
                         : (u2SetArrayIndex * BGP4_AS4_LENGTH));
            MEMCPY (BGP4_ASPATH_NOS (pNewAspath), (UINT1 *) au4AssetArray,
                    u4AssegLen);

            TMO_SLL_Add (&TsNewAggrAspath, &pNewAspath->sllNode);
            if (u2SetArrayIndex > BGP4_MAX_TEMP_ASARRAY_SIZE)
            {
                /* Move the remaining AS-SET array front */
                MEMCPY ((UINT1 *) au4AssetArray,
                        &au4AssetArray[BGP4_MAX_TEMP_ASARRAY_SIZE],
                        BGP4_MAX_TEMP_ASARRAY_SIZE * sizeof (UINT4));
                MEMSET ((UINT1 *) &au4AssetArray[BGP4_MAX_TEMP_ASARRAY_SIZE],
                        0, BGP4_MAX_TEMP_ASARRAY_SIZE * sizeof (UINT4));
                u2SetArrayIndex -= BGP4_MAX_TEMP_ASARRAY_SIZE;
            }
            else
            {
                MEMSET ((UINT1 *) au4AssetArray, 0,
                        sizeof (UINT4) * BGP4_AS4_LENGTH *
                        BGP4_MAX_TEMP_ASARRAY_SIZE);
                u2SetArrayIndex = 0;
            }
        }
        MEMCPY ((UINT1 *) (&(au4AssetArray[u2SetArrayIndex])),
                BGP4_ASPATH_NOS (pAggrAspath), (u2AssetSize * BGP4_AS4_LENGTH));
        u2SetArrayIndex += u2AssetSize;

        pAggrAspath =
            (tAsPath *) TMO_SLL_Next (BGP4_INFO_ASPATH (pAggrBgpInfo),
                                      &pAggrAspath->sllNode);
    }

    /*  Fill the set array with the AS nos. which are not present in the 
     *  old aggregated route. The ASnos present in the old aggregated routes
     *  are taken as such. 
     */
    while (pAspath != NULL)
    {
        u2CopyArrayIndex = 0;
        pu4SpecAsnos = (UINT4 *) (VOID *) BGP4_ASPATH_NOS (pAspath);

        while (u2SetArrayIndex > BGP4_MAX_TEMP_ASARRAY_SIZE)
        {
            /* AS Path segment can hold only max of 255. So allocate 
             * a AS Confed Type and add to the new AS path list. */
            pNewAspath = Bgp4MemGetASNode (sizeof (tAsPath));
            if (pNewAspath == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                          "\tMemory Allocation for Aggregate Route's AS Node "
                          "FAILED!!!\n");
                Bgp4DshReleaseAsPathList (&TsNewAggrAspath);
                gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
                return BGP4_FAILURE;
            }

            BGP4_ASPATH_TYPE (pNewAspath) = BGP4_ATTR_PATH_SET;
            BGP4_ASPATH_LEN (pNewAspath) = (UINT1) u2SetArrayIndex;
            if (BGP4_AS4_SEG_LEN <
                (BGP4_ASPATH_LEN (pNewAspath) * BGP4_AS4_LENGTH))
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                          BGP4_MOD_NAME,
                          "\tSize of au1ASSegs is less,increase the value of BGP4_AS4_SEG_LEN"
                          "FAILED!!!\n");
                Bgp4DshReleaseAsPathList (&TsNewAggrAspath);
                Bgp4MemReleaseASNode (pNewAspath);
                return BGP4_FAILURE;
            }
            MEMSET (BGP4_ASPATH_NOS (pNewAspath), 0, BGP4_AS4_SEG_LEN);
            MEMCPY (BGP4_ASPATH_NOS (pNewAspath), (UINT1 *) au4AssetArray,
                    (u2SetArrayIndex * BGP4_AS4_LENGTH));

            TMO_SLL_Add (&TsNewAggrAspath, &pNewAspath->sllNode);
            if (u2SetArrayIndex > BGP4_MAX_TEMP_ASARRAY_SIZE)
            {
                /* Move the remaining AS-SET array front */
                MEMCPY ((UINT1 *) au4AssetArray,
                        &au4AssetArray[BGP4_MAX_TEMP_ASARRAY_SIZE],
                        BGP4_MAX_TEMP_ASARRAY_SIZE * sizeof (UINT4));
                MEMSET ((UINT1 *) &au4AssetArray[BGP4_MAX_TEMP_ASARRAY_SIZE],
                        0, BGP4_MAX_TEMP_ASARRAY_SIZE * sizeof (UINT4));
                u2SetArrayIndex -= BGP4_MAX_TEMP_ASARRAY_SIZE;
            }
        }
        /* Scan thro' all the AS nos present in the specific
         * route and check whether it is already present in the
         * old aggregated route. If it is not present then add
         * that AS no to the SET array. 
         */
        while (u2CopyArrayIndex < BGP4_ASPATH_LEN (pAspath))
        {
            if (Bgp4IsASnoFound ((BGP4_INFO_ASPATH (pAggrBgpInfo)),
                                 (UINT4)
                                 OSIX_NTOHL (pu4SpecAsnos[u2CopyArrayIndex])) ==
                FALSE)
            {
                if ((BGP4_AS4_LENGTH * BGP4_MAX_TEMP_ASARRAY_SIZE) >
                    u2SetArrayIndex)
                {
                    au4AssetArray[u2SetArrayIndex] =
                        pu4SpecAsnos[u2CopyArrayIndex];
                    u2SetArrayIndex++;
                }
            }
            u2CopyArrayIndex++;
        }
        pAspath = (tAsPath *) TMO_SLL_Next (BGP4_INFO_ASPATH (pBgpInfo),
                                            &pAspath->sllNode);
    }

    if (u2SetArrayIndex > 0)
    {
        u2CopyArrayIndex = 0;
        while (u2CopyArrayIndex < u2SetArrayIndex)
        {
            pNewAspath = Bgp4MemGetASNode (sizeof (tAsPath));
            if (pNewAspath == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                          "\tMemory Allocation for Aggregate Route's AS Node "
                          "FAILED!!!\n");
                Bgp4DshReleaseAsPathList (&TsNewAggrAspath);
                gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
                return BGP4_FAILURE;
            }
            BGP4_ASPATH_TYPE (pNewAspath) = BGP4_ATTR_PATH_SET;

            if ((u2SetArrayIndex - u2CopyArrayIndex) <=
                BGP4_MAX_TEMP_ASARRAY_SIZE)
            {
                u2AssetSize = (UINT2) (u2SetArrayIndex - u2CopyArrayIndex);
            }
            else
            {
                u2AssetSize = BGP4_MAX_TEMP_ASARRAY_SIZE;
            }
            if (BGP4_AS4_SEG_LEN < (u2AssetSize * BGP4_AS4_LENGTH))
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                          BGP4_MOD_NAME,
                          "\tSize of au1ASSegs is less,increase the value of BGP4_AS4_SEG_LEN"
                          "FAILED!!!\n");
                Bgp4DshReleaseAsPathList (&TsNewAggrAspath);
                Bgp4MemReleaseASNode (pNewAspath);
                return BGP4_FAILURE;
            }
            MEMSET (BGP4_ASPATH_NOS (pNewAspath), 0, BGP4_AS4_SEG_LEN);
            MEMCPY (BGP4_ASPATH_NOS (pNewAspath),
                    (UINT1 *) (&(au4AssetArray[u2CopyArrayIndex])),
                    (u2AssetSize * BGP4_AS4_LENGTH));
            BGP4_ASPATH_LEN (pNewAspath) = (UINT1) u2AssetSize;
            u2CopyArrayIndex = (UINT2) (u2CopyArrayIndex + u2AssetSize);

            TMO_SLL_Add (&TsNewAggrAspath, &pNewAspath->sllNode);
        }
    }

    /*  Free the old Aggregated ASpath list */
    Bgp4DshReleaseAsPathList (BGP4_INFO_ASPATH (pAggrBgpInfo));

    /* Concatenate the newly created tmp. Aggregated route's ASpath to the
     * actual Aggregated route. 
     */
    TMO_SLL_Concat (BGP4_INFO_ASPATH (pAggrBgpInfo), &TsNewAggrAspath);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AggrCopyBgp4infos                                     */
/* Description   : Copies the BGP Information                                */
/* Input(s)      : Source BGP Route Info (pSrcRtProfile)                     */
/*                 Target BGP Route Info (pDestRtProfile)                    */
/* Output(s)     : Target BGP Route Information which contains the           */
/*                 copied information(pDestRtProfile).                       */
/* Return(s)     :BGP4_SUCCESS/BGP4_FAILURE                                  */
/*****************************************************************************/
INT4
Bgp4AggrCopyBgp4infos (tRouteProfile * pDestRtProfile,
                       tRouteProfile * pSrcRtProfile, UINT1 u1AdvType,
                       UINT1 u1AsSet)
{
    tBgp4Info          *pDestBgp4Info = NULL;
    tBgp4Info          *pSrcBgp4Info = NULL;
    tAsPath            *pDestAspath = NULL;
    tAsPath            *pSrcAspath = NULL;

    pDestBgp4Info = BGP4_RT_BGP_INFO (pDestRtProfile);
    pSrcBgp4Info = BGP4_RT_BGP_INFO (pSrcRtProfile);

    BGP4_INFO_ORIGIN (pDestBgp4Info) = BGP4_INFO_ORIGIN (pSrcBgp4Info);
    BGP4_INFO_ATTR_FLAG (pDestBgp4Info) |= BGP4_ATTR_ORIGIN_MASK;

    /* For aggregation if summary-only option is not given then we need to copy all th epath info */
    if (u1AdvType != BGP4_SUMMARY)
    {
        TMO_SLL_Scan (BGP4_INFO_ASPATH (pSrcBgp4Info), pSrcAspath, tAsPath *)
        {
            pDestAspath = Bgp4MemGetASNode (sizeof (tAsPath));
            if (pDestAspath == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                          "\tMemory Allocation for Aggregate Route's AS Node "
                          "FAILED!!!\n");
                gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
                return BGP4_FAILURE;
            }
            BGP4_ASPATH_TYPE (pDestAspath) = BGP4_ASPATH_TYPE (pSrcAspath);
            BGP4_ASPATH_LEN (pDestAspath) = BGP4_ASPATH_LEN (pSrcAspath);
            if (BGP4_AS4_SEG_LEN <
                (BGP4_ASPATH_LEN (pSrcAspath) * BGP4_AS4_LENGTH))
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tSize of au1ASSegs is less,increase the value of BGP4_AS4_SEG_LEN FAILED!!!\n");
                Bgp4MemReleaseASNode (pDestAspath);
                return BGP4_FAILURE;
            }
            MEMSET (BGP4_ASPATH_NOS (pDestAspath), 0, BGP4_AS4_SEG_LEN);
            MEMCPY (BGP4_ASPATH_NOS (pDestAspath), BGP4_ASPATH_NOS (pSrcAspath),
                    (BGP4_ASPATH_LEN (pSrcAspath) * BGP4_AS4_LENGTH));

            TMO_SLL_Add (BGP4_INFO_ASPATH (pDestBgp4Info),
                         &pDestAspath->sllNode);
            BGP4_INFO_ATTR_FLAG (pDestBgp4Info) |= BGP4_ATTR_PATH_MASK;
        }
    }
    else
    {
        /* if summary-only and as-set is given then we need to add the as-path information */
        if (u1AsSet == AS_SET_ENABLE)
        {

            TMO_SLL_Scan (BGP4_INFO_ASPATH (pSrcBgp4Info), pSrcAspath,
                          tAsPath *)
            {
                pDestAspath = Bgp4MemGetASNode (sizeof (tAsPath));
                if (pDestAspath == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                              BGP4_MOD_NAME,
                              "\tMemory Allocation for Aggregate Route's AS Node "
                              "FAILED!!!\n");
                    gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
                    return BGP4_FAILURE;
                }
                BGP4_ASPATH_TYPE (pDestAspath) = BGP4_ASPATH_TYPE (pSrcAspath);
                BGP4_ASPATH_LEN (pDestAspath) = BGP4_ASPATH_LEN (pSrcAspath);
                if (BGP4_AS4_SEG_LEN <
                    (BGP4_ASPATH_LEN (pSrcAspath) * BGP4_AS4_LENGTH))
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG,
                              BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                              BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                              "\tSize of au1ASSegs is less,increase the value of BGP4_AS4_SEG_LEN FAILED!!!\n");
                    Bgp4MemReleaseASNode (pDestAspath);
                    return BGP4_FAILURE;
                }
                MEMSET (BGP4_ASPATH_NOS (pDestAspath), 0, BGP4_AS4_SEG_LEN);
                MEMCPY (BGP4_ASPATH_NOS (pDestAspath),
                        BGP4_ASPATH_NOS (pSrcAspath),
                        (BGP4_ASPATH_LEN (pSrcAspath) * BGP4_AS4_LENGTH));

                TMO_SLL_Add (BGP4_INFO_ASPATH (pDestBgp4Info),
                             &pDestAspath->sllNode);
                BGP4_INFO_ATTR_FLAG (pDestBgp4Info) |= BGP4_ATTR_PATH_MASK;
            }
        }
    }

    /* Filling up the ATOMIC_AGGREGATE  attribute */
    if ((BGP4_INFO_ATTR_FLAG (pSrcBgp4Info) & BGP4_ATTR_ATOMIC_AGGR_MASK)
        == BGP4_ATTR_ATOMIC_AGGR_MASK)
    {
        BGP4_INFO_ATTR_FLAG (pDestBgp4Info) |= BGP4_ATTR_ATOMIC_AGGR_MASK;
    }
    else
    {
        /* Filling up the Community attribute. */
        CommUpdateAggrComm (pDestRtProfile, pSrcRtProfile);
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AggrCopyRoutes                                        */
/* Description   : Copies the given src route profile to the destination     */
/*                 route profile. However this routine copies only those     */
/*                 informations required for the aggregated route.           */
/* Input(s)      : (pSrcRtProfile) - Source Route Profile                    */
/* Output(s)     : (pDstRtProfile) - Destination Route Profile               */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4AggrCopyRoutes (tRouteProfile * pDstRtProfile,
                    tRouteProfile * pSrcRtProfile, UINT1 u1AdvType,
                    UINT1 u1AsSet)
{
#ifdef L3VPN
    tRtInstallVrfInfo  *pRtVrfNode = NULL;
    tRtInstallVrfInfo  *pTmpRtVrfNode = NULL;
    tRtInstallVrfInfo  *pDstRtVrfNode = NULL;
#endif
    UINT4               u4Flags = 0;
    UINT4               u4Index = 0;
    tAsPath            *pAspath = NULL;
    tBgp4Info          *pBgpInfo = NULL;

    BGP4_RT_PROTOCOL (pDstRtProfile) = BGP4_RT_PROTOCOL (pSrcRtProfile);
    Bgp4CopyNetAddressStruct (&(pDstRtProfile->NetAddress),
                              pSrcRtProfile->NetAddress);
    u4Flags = BGP4_RT_GET_FLAGS (pSrcRtProfile);
    BGP4_RT_SET_FLAG (pDstRtProfile, u4Flags);
    u4Index = BGP4_RT_GET_RT_IF_INDEX (pSrcRtProfile);
    BGP4_RT_SET_RT_IF_INDEX (pDstRtProfile, u4Index);
#ifdef L3VPN
    TMO_SLL_Scan ((BGP4_VPN4_RT_INSTALL_VRF_LIST (pSrcRtProfile)),
                  pRtVrfNode, tRtInstallVrfInfo *)
    {
        pDstRtVrfNode =
            Bgp4MemAllocateVpnRtInstallVrfNode (sizeof (tRtInstallVrfInfo));
        if (pDstRtVrfNode == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tVPN: Creating Installed Vrf Node failed\n");
            /* delete the installed vrf list info in dest profile */
            BGP_SLL_DYN_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pDstRtProfile),
                              pDstRtVrfNode, pTmpRtVrfNode, tRtInstallVrfInfo *)
            {
                TMO_SLL_Delete (BGP4_VPN4_RT_INSTALL_VRF_LIST
                                (pDstRtProfile),
                                (tTMO_SLL_NODE *) pDstRtVrfNode);
                Bgp4MemReleaseVpnRtInstallVrfNode (pDstRtVrfNode);
            }
            return BGP4_FAILURE;
        }
        MEMCPY (pDstRtVrfNode, pRtVrfNode, sizeof (tRtInstallVrfInfo));
        TMO_SLL_Add (BGP4_VPN4_RT_INSTALL_VRF_LIST (pDstRtProfile),
                     (tTMO_SLL_NODE *) pDstRtVrfNode);
    }

    BGP4_RT_LABEL_CNT (pDstRtProfile) = BGP4_RT_LABEL_CNT (pSrcRtProfile);
    BGP4_RT_LABEL_INFO (pDstRtProfile) = BGP4_RT_LABEL_INFO (pSrcRtProfile);
    MEMCPY (BGP4_RT_ROUTE_DISTING (pDstRtProfile),
            BGP4_RT_ROUTE_DISTING (pSrcRtProfile),
            BGP4_VPN4_ROUTE_DISTING_SIZE);
#endif
    BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pDstRtProfile)) |=
        BGP4_ATTR_MED_MASK;
    pDstRtProfile->u4MED = pSrcRtProfile->u4MED;
    Bgp4AggrCopyBgp4infos (pDstRtProfile, pSrcRtProfile, u1AdvType, u1AsSet);
    pBgpInfo = BGP4_RT_BGP_INFO (pSrcRtProfile);
    if (pBgpInfo != NULL)
    {
        pAspath = (tAsPath *) TMO_SLL_First (BGP4_INFO_ASPATH (pBgpInfo));
        if ((pAspath != NULL)
            && (BGP4_ASPATH_TYPE (pAspath) == BGP4_ATTR_PATH_SET))
        {
            BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pDstRtProfile)) &=
                ~(BGP4_ATTR_MED_MASK);
        }
    }
    pDstRtProfile->pBgpCxtNode = pSrcRtProfile->pBgpCxtNode;
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AggrFindAsnoInarray                                   */
/* Description   : Checks whether a particular AS number is present in the   */
/*                 AS number array.                                          */
/* Input(s)      : Array of AS numbers (pu2AssetArray),                      */
/*                 Maximum index of the array (u2ArrayMaxIndex),             */
/*                 AS number to be checked for (u2ASno)                      */
/* Output(s)     : None                                                      */
/* Return(s)     : TRUE  if AS number is found in the array,                 */
/*                 FALSE if it is not.                                       */
/*****************************************************************************/
BOOL1
Bgp4AggrFindAsnoInarray (UINT4 *pu4AssetArray,
                         UINT2 u2ArrayMaxIndex, UINT4 u4ASno)
{
    UINT2               u2ScanIndex = 0;

    while (u2ScanIndex < u2ArrayMaxIndex)
    {
        if (pu4AssetArray[u2ScanIndex] == u4ASno)
        {
            return TRUE;
        }

        u2ScanIndex++;
    }
    return FALSE;
}

/*****************************************************************************/
/* Function Name : Bgp4AggrCanAggrRouteBeAdvt                                */
/* Description   : This function checks whether the aggregated route can be  */
/*                 advt or not. If any non-BGP route learnt exists in LOCAL  */
/*                 RIB, then that route will be the best route else          */
/*                 aggregated route will be selected as the best route for   */
/*                 advertisement. So the order of preference is              */
/*                 non-BGP route -> aggregated route -> BGP route.           */
/* Input(s)      : Pointer to aggregated route profile (pAggrRoute)          */
/*                 Pointer to best route for the matching route in RIB       */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_TRUE -  if the aggregated route can be advt.         */
/*               : BGP4_FALSE - if the route can't be advt.                  */
/*****************************************************************************/
UINT1
Bgp4AggrCanAggrRouteBeAdvt (tRouteProfile * pAggrRoute,
                            tRouteProfile ** pBestRoute)
{
    tRouteProfile      *pRtProfileList = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4RtFound;
    tRouteProfile      *pBestRtprofile = NULL;
    UINT1               u1CanAggrRtBeAdvt = BGP4_FALSE;

    /* Check whether the aggregated route can be advt. */
    i4RtFound = Bgp4RibhLookupRtEntry (pAggrRoute, BGP4_RT_CXT_ID (pAggrRoute),
                                       BGP4_TREE_FIND_EXACT,
                                       &pRtProfileList, &pRibNode);
    if (i4RtFound == BGP4_FAILURE)
    {
        *pBestRoute = NULL;
        u1CanAggrRtBeAdvt = BGP4_TRUE;
    }
    else
    {
        pBestRtprofile = Bgp4DshGetBestRouteFromProfileList (pRtProfileList,
                                                             BGP4_RT_CXT_ID
                                                             (pAggrRoute));
        if (pBestRtprofile == NULL)
        {
            /* Only Invalid Routes are present in this Node. */
            *pBestRoute = NULL;
            u1CanAggrRtBeAdvt = BGP4_TRUE;
        }
        else
        {
            if (BGP4_RT_PROTOCOL (pBestRtprofile) == BGP_ID)
            {
                u1CanAggrRtBeAdvt = BGP4_TRUE;
                *pBestRoute = pBestRtprofile;
            }
            else
            {
                /* Non-BGP route this route will be the best route. */
                u1CanAggrRtBeAdvt = BGP4_FALSE;
                *pBestRoute = pBestRtprofile;
            }
        }
    }
    return (u1CanAggrRtBeAdvt);
}

/*****************************************************************************/
/* Function Name : Bgp4AggrAddAggrRouteToAdvtList                            */
/* Description   : This function takes the index to entry in the aggregate   */
/*                 table and add the aggregated route associated with that   */
/*                 table to the external and internal peers advt list.       */
/* Input(s)      : Index of the entry in aggregate table (u4Index)           */
/*                 Flag indicating whether the aggregate route is withdrawn  */
/*                 or replacement route or feasible route.                   */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS  if the operation is successful              */
/*                 BGP4_FAILURE  otherwise                                   */
/*****************************************************************************/
INT4
Bgp4AggrAddAggrRouteToAdvtList (UINT4 u4Index, UINT4 u4Flag)
{
    tRouteProfile      *pAggrRoute = NULL;
    tRouteProfile      *pCopiedAggrRoute = NULL;
    tBgp4Info          *pBgp4Info = NULL;
    INT1                i1AdvtStatus = BGP4_FALSE;

    if (BGP4_LOCAL_ADMIN_STATUS (BGP4_AGGRENTRY_VRFID (u4Index)) ==
        BGP4_ADMIN_DOWN)
    {
        return (BGP4_SUCCESS);
    }
    pAggrRoute = BGP4_AGGRENTRY_AGGR_ROUTE (u4Index);
    if (pAggrRoute == NULL)
    {
        return (BGP4_FAILURE);
    }
    if (u4Flag == BGP4_RT_WITHDRAWN)
    {
        BGP4_RT_RESET_FLAG (pAggrRoute, BGP4_RT_BEST);
    }
    else
    {
        BGP4_RT_SET_FLAG (pAggrRoute, BGP4_RT_BEST);
    }

    if ((BGP4_RT_GET_FLAGS (pAggrRoute) & BGP4_RT_AGGREGATED) !=
        BGP4_RT_AGGREGATED)
    {
        /* Dummy aggregate route. No need to advt. */
        return (BGP4_SUCCESS);
    }

    pCopiedAggrRoute = Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
    if (pCopiedAggrRoute == NULL)
    {
        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tMemory Allocation for advertising Aggregated "
                       "Route \n\t- Dest %s, Mask %s FAILED!!!\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pAggrRoute),
                                        BGP4_RT_AFI_INFO (pAggrRoute)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pAggrRoute),
                                        BGP4_RT_AFI_INFO (pAggrRoute)));
        gu4BgpDebugCnt[MAX_BGP_ROUTES_SIZING_ID]++;
        return (BGP4_FAILURE);
    }
    pBgp4Info = Bgp4MemAllocateBgp4info (sizeof (tBgp4Info));
    if (pBgp4Info == NULL)
    {
        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tMemory Allocation for advertising Aggregated Route "
                       "Info \n\t- Dest %s, Mask %s FAILED!!!\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pAggrRoute),
                                        BGP4_RT_AFI_INFO (pAggrRoute)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pAggrRoute),
                                        BGP4_RT_AFI_INFO (pAggrRoute)));
        Bgp4DshReleaseRtInfo (pCopiedAggrRoute);
        gu4BgpDebugCnt[MAX_BGP_ROUTE_INFO_ENTRIES_SIZING_ID]++;
        return (BGP4_FAILURE);
    }
    BGP4_LINK_INFO_TO_PROFILE (pBgp4Info, pCopiedAggrRoute);
#ifdef L3VPN
    if (Bgp4AggrCopyRoutes (pCopiedAggrRoute, pAggrRoute,
                            BGP4_AGGRENTRY_ADV_TYPE (u4Index),
                            BGP4_AGGRENTRY_AS_SET (u4Index)) == BGP4_FAILURE)
    {
        Bgp4DshReleaseRtInfo (pCopiedAggrRoute);
        return BGP4_FAILURE;
    }
#else
    Bgp4AggrCopyRoutes (pCopiedAggrRoute, pAggrRoute,
                        BGP4_AGGRENTRY_ADV_TYPE (u4Index),
                        BGP4_AGGRENTRY_AS_SET (u4Index));
#endif
    if (u4Flag == BGP4_RT_WITHDRAWN)
    {
        BGP4_RT_SET_FLAG (pCopiedAggrRoute, BGP4_RT_WITHDRAWN);
    }
    else if (u4Flag == BGP4_RT_REPLACEMENT)
    {
        BGP4_RT_SET_FLAG (pCopiedAggrRoute, BGP4_RT_REPLACEMENT);
    }
    else
    {
        /* The aggregate route is new feasible. */
    }

    /* Add the aggregated route to the external peer advt list and
     * internal peer advt list. */
    Bgp4AddFeasRouteToIntPeerAdvtList (pCopiedAggrRoute, NULL);
    if ((GetAdvtStatus (pCopiedAggrRoute, NULL, &i1AdvtStatus) == BGP4_FAILURE)
        || (i1AdvtStatus == BGP4_FALSE))
    {
        /* Aggregated route advertisement is suppressed by Community Attribute.
         * Donot add it only to the external peer advertisement list */
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "Route Not Advertised. Suppressed by Community Attribute.\n");
        if (u4Flag == BGP4_RT_REPLACEMENT)
        {
            /* If new aggregated route is a replacement for an old route advertised
             * and the replacement route is suppressed by the community attributes
             * send a withdrawal for the old route advertised */
            BGP4_RT_RESET_FLAG (pCopiedAggrRoute, BGP4_RT_REPLACEMENT);
            BGP4_RT_SET_FLAG (pCopiedAggrRoute, BGP4_RT_WITHDRAWN);
            Bgp4AddFeasRouteToExtPeerAdvtList (pCopiedAggrRoute, NULL);
        }
        return (BGP4_SUCCESS);
    }

    Bgp4AddFeasRouteToExtPeerAdvtList (pCopiedAggrRoute, NULL);

    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Bgp4AggrAdvtSuppressedRoute                               */
/* Description   : This function takes the route that has been suppressed    */
/*                 because of aggregation policy and add it to the internal  */
/*                 and external peer advt list.                              */
/* Input(s)      : Pointer to route profile of suppressed route(pRtProfile)  */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS  if the operation is successful              */
/*****************************************************************************/
INT4
Bgp4AggrAdvtSuppressedRoute (tRouteProfile * pRtProfile)
{
    /* These routes are suppressed earlier because of
     * the policy. But now since the aggregate entry is
     * set down, these routes needs to be advertised as
     * feasible. */

    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_WITHDRAWN)
        == BGP4_RT_WITHDRAWN)
    {
        /* Route is in process of getting removed. No need
         * to advt it as feasible. Is it possible to have
         * a route set as WITHDRAWN and still remain in tree.*/
    }
    else
    {
        if ((BGP4_RT_GET_FLAGS (pRtProfile) &
             BGP4_RT_IN_FIB_UPD_LIST) == BGP4_RT_IN_FIB_UPD_LIST)
        {
            /* Route is present in the FIB list pending
             * for FIB updation and advt to external peer
             * as feasible route. No need to do any processing
             * here for this route. This will be taken care
             * in normal sequence itself. */
        }
        else if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                  BGP4_RT_IN_EXT_PEER_ADVT_LIST) ==
                 BGP4_RT_IN_EXT_PEER_ADVT_LIST)
        {
            /* Route present in EXT peer advt list pending
             * advt for external peers. This can happen
             * only when the route was earlier added to
             * the external peer advt list with
             * BGP4_RT_AGGR_WITHDRAWN while an aggrentry is
             * created and pending for advt. So in that case
             * just remove the route from the list. */
            if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                 BGP4_RT_AGGR_WITHDRAWN) == BGP4_RT_AGGR_WITHDRAWN)
            {
                BGP4_RT_RESET_FLAG (pRtProfile,
                                    (BGP4_RT_AGGR_WITHDRAWN |
                                     BGP4_RT_IN_EXT_PEER_ADVT_LIST));
                /* Route was already advt. So no need to
                 * sent it again. */
                Bgp4DshDelinkRouteFromList (BGP4_EXT_PEERS_ADVT_LIST
                                            (BGP4_RT_CXT_ID (pRtProfile)),
                                            pRtProfile,
                                            BGP4_EXT_PEER_LIST_INDEX);
            }
        }
        else
        {
            /* Route was earlier suppressed. Need to advt
             * as feasible route. */
            BGP4_RT_RESET_FLAG (pRtProfile,
                                (BGP4_RT_AGGR_WITHDRAWN |
                                 BGP4_RT_ADVT_NOTTO_EXTERNAL));
            Bgp4AddFeasRouteToExtPeerAdvtList (pRtProfile, NULL);
        }

        if ((BGP4_RT_GET_FLAGS (pRtProfile) &
             BGP4_RT_IN_INT_PEER_ADVT_LIST) == BGP4_RT_IN_INT_PEER_ADVT_LIST)
        {
            /* Route present in INT peer advt list pending
             * advt for internal peers. This can happen
             * only when the route was earlier added to
             * the internal peer advt list with
             * BGP4_RT_AGGR_WITHDRAWN while an aggrentry is
             * created and pending for advt. So in that case
             * just remove the route from the list. */
            if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                 BGP4_RT_AGGR_WITHDRAWN) == BGP4_RT_AGGR_WITHDRAWN)
            {
                BGP4_RT_RESET_FLAG (pRtProfile,
                                    (BGP4_RT_AGGR_WITHDRAWN |
                                     BGP4_RT_IN_INT_PEER_ADVT_LIST));
                /* Route was already advt. So no need to
                 * sent it again. */
                Bgp4DshDelinkRouteFromList (BGP4_INT_PEERS_ADVT_LIST
                                            (BGP4_RT_CXT_ID (pRtProfile)),
                                            pRtProfile,
                                            BGP4_INT_PEER_LIST_INDEX);
            }
        }
        else
        {
            /* Route was earlier suppressed. Need to advt
             * as feasible route. */
            BGP4_RT_RESET_FLAG (pRtProfile,
                                (BGP4_RT_AGGR_WITHDRAWN |
                                 BGP4_RT_ADVT_NOTTO_INTERNAL));
            Bgp4AddFeasRouteToIntPeerAdvtList (pRtProfile, NULL);
        }
    }
    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Bgp4AggrWithdrawAdvtRoute                                 */
/* Description   : This function takes the route that is to be suppressed    */
/*                 because of aggregation policy and add it to the internal  */
/*                 and external peer advt list as withdrawn route.           */
/* Input(s)      : Route profile pointer of the route to be suppressed       */
/*                 (pRtProfile)                                              */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS  if the operation is successful              */
/*****************************************************************************/
INT4
Bgp4AggrWithdrawAdvtRoute (tRouteProfile * pRtProfile)
{
    /* Previously advt. routes that are now used for Aggr.
     * will now be advertised as withdrawn routes to both
     * internal and external peers. 
     */
    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_WITHDRAWN)
        != BGP4_RT_WITHDRAWN)
    {
        if ((BGP4_RT_GET_FLAGS (pRtProfile) &
             BGP4_RT_IN_FIB_UPD_LIST) == BGP4_RT_IN_FIB_UPD_LIST)
        {
            /* Route is present in the FIB list pending
             * for FIB updation and advt to external peer
             * as feasible route. To prevent route from advt to
             * external peer set BGP4_RT_ADVT_NOTTO_EXTERNAL */
            BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_ADVT_NOTTO_EXTERNAL);
        }
        else if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                  BGP4_RT_IN_EXT_PEER_ADVT_LIST) ==
                 BGP4_RT_IN_EXT_PEER_ADVT_LIST)
        {
            /* Route present in EXT peer advt list pending
             * advt for external peers. So just remove the
             * the route from the list. */
            BGP4_RT_RESET_FLAG (pRtProfile, (BGP4_RT_IN_EXT_PEER_ADVT_LIST |
                                             BGP4_RT_ADVT_NOTTO_INTERNAL |
                                             BGP4_RT_ADVT_NOTTO_EXTERNAL |
                                             BGP4_RT_REPLACEMENT));
            Bgp4DshDelinkRouteFromList (BGP4_EXT_PEERS_ADVT_LIST
                                        (BGP4_RT_CXT_ID (pRtProfile)),
                                        pRtProfile, BGP4_EXT_PEER_LIST_INDEX);
        }
        else
        {
            /* Route was earlier advt to external peer. Need
             * to send the withdrawn route. */
            if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGREGATED) !=
                BGP4_RT_AGGREGATED)
            {
                Bgp4AddFeasRouteToExtPeerAdvtList (pRtProfile, NULL);
                BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_AGGR_WITHDRAWN);
            }
        }

        if ((BGP4_RT_GET_FLAGS (pRtProfile) &
             BGP4_RT_IN_INT_PEER_ADVT_LIST) == BGP4_RT_IN_INT_PEER_ADVT_LIST)
        {
            /* Route is present in the internal peer advt
             * list pending advt for internal peers. So
             * just remove the route from the list. */
            BGP4_RT_RESET_FLAG (pRtProfile, (BGP4_RT_IN_INT_PEER_ADVT_LIST |
                                             BGP4_RT_ADVT_NOTTO_INTERNAL |
                                             BGP4_RT_ADVT_NOTTO_EXTERNAL |
                                             BGP4_RT_REPLACEMENT));
            Bgp4DshDelinkRouteFromList (BGP4_INT_PEERS_ADVT_LIST
                                        (BGP4_RT_CXT_ID (pRtProfile)),
                                        pRtProfile, BGP4_INT_PEER_LIST_INDEX);
        }
        else
        {
            /* Route was earlier advt to internal peer. Need
             * to send the withdrawn route. */
            if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGREGATED) !=
                BGP4_RT_AGGREGATED)
            {
                Bgp4AddFeasRouteToIntPeerAdvtList (pRtProfile, NULL);
                BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_AGGR_WITHDRAWN);
            }
        }
    }
    else
    {
        /* Route is already is in the process of withdrawal
         * So no need to send it again. Is it possible to
         * have a route set as WITHDRAWN and remain in
         * RIB ??? */
    }
    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Bgp4AggrProcessAdminChange                                */
/* Description   : Does the required operation whenever the Aggregate Entry's*/
/*                 admin changes.                                            */
/* Input(s)      : Index of the Aggregate Entry in the table(u4Index),       */
/*                 Admin State value which needs to be set (i4Admin)         */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS  if the operation is successful,             */
/*                 BGP4_FAILURE if it fails.                                 */
/*****************************************************************************/
INT4
Bgp4AggrProcessAdminChange (UINT4 u4Index, INT4 i4Admin)
{
    tRouteProfile      *pAggrRoute = NULL;
    tRouteProfile      *pBestRoute = NULL;
#ifdef L3VPN
    UINT4               u4Label;
    UINT4               u4AsafiMask;
    INT4                i4RetVal = 0;
#endif
    UINT1               u1CanAggrRtBeAdvt = BGP4_FALSE;
    INT1                i1AdvtStatus = BGP4_FALSE;

    if (u4Index >= BGP4_MAX_AGGR_ENTRIES)
    {
        return (BGP4_FAILURE);
    }
    if ((i4Admin != BGP4_ADMIN_DOWN) && (i4Admin != BGP4_ADMIN_UP)
        && (i4Admin != BGP4_ADMIN_INVALID))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                  "\nError Configuring Aggrgate entry - Invalid Admin Status");
        return (BGP4_FAILURE);
    }

    if ((i4Admin == BGP4_ADMIN_DOWN) &&
        (BGP4_AGGRENTRY_ADMIN (u4Index) == BGP4_INVALID))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                  BGP4_FDB_TRC, BGP4_MOD_NAME,
                  "\tChanging the admin status of the aggregate entry to down.\n");
        BGP4_AGGRENTRY_ADMIN (u4Index) = (UINT1) i4Admin;
        return (BGP4_SUCCESS);
    }

    if (BGP4_AGGRENTRY_ADMIN (u4Index) == i4Admin)
    {
        if (i4Admin == BGP4_ADMIN_UP)
        {
            return (BGP4_SUCCESS);
        }
        else
        {
            /* Aggregate entry Admin status is down. Check & clear if
             * create is pending. */
            if ((BGP4_AGGRENTRY_AGGR_STATUS (u4Index) &
                 BGP4_AGGR_PENDING_CREATE) == BGP4_AGGR_PENDING_CREATE)
            {
                BGP4_AGGR_RESET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                        BGP4_AGGR_PENDING_CREATE);
            }
        }
        return (BGP4_SUCCESS);
    }

    if (i4Admin == BGP4_ADMIN_UP)
    {
        /* If the Aggregate entry is going through delete process, set the
         * status as create_pend. */
        if ((BGP4_AGGRENTRY_AGGR_STATUS (u4Index) &
             BGP4_AGGR_DELETE_IN_PROGRESS) == BGP4_AGGR_DELETE_IN_PROGRESS)
        {
            BGP4_AGGR_SET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                  BGP4_AGGR_PENDING_CREATE);
            return (BGP4_SUCCESS);
        }

        /* Create the Dummy Aggregate route and Fill the Aggregate route
         * information */
        if (BGP4_AGGRENTRY_AGGR_ROUTE (u4Index) == NULL)
        {
            pAggrRoute = Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
            if (pAggrRoute == NULL)
            {
                BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                               BGP4_MOD_NAME,
                               "\tMemory Allocation for creating Aggregated "
                               " Route \n\t- Dest %s, Mask %s FAILED!!!\n",
                               Bgp4PrintIpAddr (BGP4_AGGRENTRY_IPADDR (u4Index),
                                                BGP4_AGGRENTRY_IPADDR_FAMILY
                                                (u4Index)),
                               Bgp4PrintIpMask ((UINT1)
                                                BGP4_AGGRENTRY_IPADDR_LEN
                                                (u4Index),
                                                BGP4_AGGRENTRY_IPADDR_FAMILY
                                                (u4Index)));
                gu4BgpDebugCnt[MAX_BGP_ROUTES_SIZING_ID]++;
                return (BGP4_FAILURE);
            }
            BGP4_AGGRENTRY_AGGR_ROUTE (u4Index) = pAggrRoute;
            /* As the aggregate route is newly created,
             * add the context associated with this route
             * to the routeprofile. */
            pAggrRoute->pBgpCxtNode =
                Bgp4GetContextEntry (BGP4_AGGRENTRY_VRFID (u4Index));
            BGP4_RT_REF_COUNT (pAggrRoute)++;
        }
        else
        {
            pAggrRoute = BGP4_AGGRENTRY_AGGR_ROUTE (u4Index);
            if (BGP4_RT_BGP_INFO (pAggrRoute) != NULL)
            {
                /* Release BGP Info associated with the Aggregate route. */
                Bgp4DshReleaseBgpInfo (BGP4_RT_BGP_INFO (pAggrRoute));
            }
        }
        pAggrRoute->u4MED = BGP4_INV_MEDLP;
        BGP4_RT_PROTOCOL (pAggrRoute) = PROTO_INVALID;
        BGP4_RT_SET_RT_IF_INDEX (pAggrRoute, BGP4_INVALID_IFINDX);
        Bgp4CopyNetAddressStruct ((&(BGP4_RT_NET_ADDRESS_INFO (pAggrRoute))),
                                  BGP4_AGGRENTRY_NET_ADDR_INFO (u4Index));
#ifdef L3VPN
        /* Make the aggregated route VPN converted route, if the configuration
         * is for a VRF that is attached to a CE peer
         */
        BGP4_GET_AFISAFI_MASK (BGP4_AGGRENTRY_IPADDR_FAMILY (u4Index),
                               BGP4_AGGRENTRY_IPSUBADDR_FAMILY (u4Index),
                               u4AsafiMask);

        if ((BGP4_VPNV4_AFI_FLAG == BGP4_TRUE) &&
            (BGP4_AGGRENTRY_VRFID (u4Index) != BGP4_DFLT_VRFID) &&
            (BGP4_VPN4_VRF_SPEC_CURRENT_STATE
             (BGP4_AGGRENTRY_VRFID (u4Index)) == BGP4_L3VPN_VRF_UP))
        {
            i4RetVal = Bgp4Vpn4ConvertRtToVpn4Route (pAggrRoute,
                                                     BGP4_AGGRENTRY_VRFID
                                                     (u4Index));

            /* Get the aggregate label for the route if it does not have already
             * one. This aggregate label will be updated for each peer with its 
             * incoming interface index when this route is advertised to them.
             */
            if (((u4AsafiMask == CAP_MP_IPV4_UNICAST) &&
                 (BGP4_AGGRENTRY_VRFID (u4Index) != BGP4_DFLT_VRFID)) ||
                (u4AsafiMask == CAP_MP_LABELLED_IPV4))
            {
                if ((BGP4_LABEL_ALLOC_POLICY (0) ==
                     BGP4_PERVRF_LABEL_ALLOC_POLICY) &&
                    (BGP4_VPNV4_LABEL (BGP4_AGGRENTRY_VRFID (u4Index)) != 0))
                {
                    u4Label = BGP4_VPNV4_LABEL (BGP4_AGGRENTRY_VRFID (u4Index));
                }
                else if (Bgp4GetBgp4Label (&u4Label) == BGP4_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                              "\tLabel Space in BGP is exhausted !!!");
                    Bgp4DshReleaseRtInfo (BGP4_AGGRENTRY_AGGR_ROUTE (u4Index));
                    return (BGP4_FAILURE);
                }
                BGP4_RT_LABEL_CNT (pAggrRoute)++;
                BGP4_RT_LABEL_INFO (pAggrRoute) = u4Label;
                if (BGP4_LABEL_ALLOC_POLICY (0) ==
                    BGP4_PERVRF_LABEL_ALLOC_POLICY)
                {
                    BGP4_VPNV4_LABEL (BGP4_AGGRENTRY_VRFID (u4Index)) = u4Label;
                }
            }
        }
        UNUSED_PARAM (i4RetVal);
#endif
        BGP4_RT_SET_FLAG (pAggrRoute, BGP4_RT_AGGR_ROUTE);
        BGP4_RT_BGP4_INF (pAggrRoute) = NULL;

        /* update the Aggregate entry Init variables. */
        Bgp4InitAddrPrefixStruct (&(BGP4_AGGRENTRY_INIT_PREFIX_INFO (u4Index)),
                                  BGP4_AGGRENTRY_IPADDR_FAMILY (u4Index));
        /* Update the admin status */
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                  BGP4_FDB_TRC, BGP4_MOD_NAME,
                  "\tUpdating the admin status of the aggregate entry to up.\n");
        BGP4_AGGRENTRY_ADMIN (u4Index) = (UINT1) i4Admin;

        if (BGP4_LOCAL_ADMIN_STATUS (BGP4_AGGRENTRY_VRFID (u4Index)) ==
            BGP4_ADMIN_UP)
        {
            /* Update the Aggregate entry status */
            BGP4_AGGR_SET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                  BGP4_AGGR_CREATE_IN_PROGRESS);
        }
    }
    else if ((i4Admin == BGP4_ADMIN_DOWN) ||
             ((i4Admin == BGP4_ADMIN_INVALID)
              && (BGP4_AGGRENTRY_ADMIN (u4Index) == BGP4_ADMIN_UP)))
    {
        /* Aggregate entry admin status is going DOWN. */
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                  BGP4_FDB_TRC, BGP4_MOD_NAME,
                  "\tUpdating the admin status of the aggregate entry to down.\n");
        BGP4_AGGRENTRY_ADMIN (u4Index) = (UINT1) i4Admin;
        pAggrRoute = BGP4_AGGRENTRY_AGGR_ROUTE (u4Index);

        /* Check whether aggregated route exist and advertise as 
         * withdrawn if necessary. */
        if ((BGP4_AGGRENTRY_AGGR_STATUS (u4Index) &
             BGP4_AGGR_CREATE_IN_PROGRESS) == BGP4_AGGR_CREATE_IN_PROGRESS)
        {
            /* Aggregate route not yet advertised to any peers. Clear
             * the flag and proceed. */
            BGP4_AGGR_RESET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                    BGP4_AGGR_CREATE_IN_PROGRESS);
        }
        else
        {
            /* If aggregated route exist, then advertise it as
             * withdrawn if possible. */
            if ((BGP4_RT_GET_FLAGS (pAggrRoute) & BGP4_RT_AGGREGATED) ==
                BGP4_RT_AGGREGATED)
            {
                u1CanAggrRtBeAdvt =
                    Bgp4AggrCanAggrRouteBeAdvt (pAggrRoute, &pBestRoute);
                if (u1CanAggrRtBeAdvt == BGP4_TRUE)
                {
                    /* No matching route present in RIB for this aggregated
                     * route. Advt the aggregated route as withdrawn. */
                    if (u4Index < BGP4_MAX_AGGR_ENTRIES)
                    {
                        Bgp4AggrAddAggrRouteToAdvtList (u4Index,
                                                        BGP4_RT_WITHDRAWN);
                    }
                    if (pBestRoute != NULL)
                    {
                        /* Alternate route is present in the RIB. Advt that route
                         * as the best route. */
                        BGP4_RT_RESET_FLAG (pBestRoute, BGP4_RT_WITHDRAWN);
                        if ((GetAdvtStatus (pBestRoute, NULL, &i1AdvtStatus)
                             == BGP4_SUCCESS) && (i1AdvtStatus == BGP4_TRUE))
                        {
                            /* Aggregated route advertisement to external peers 
                             * is allowed by Community Attribute. */
                            Bgp4AddFeasRouteToExtPeerAdvtList (pBestRoute,
                                                               NULL);
                        }
                        Bgp4AddFeasRouteToIntPeerAdvtList (pBestRoute, NULL);
                    }
                }
                else
                {
                    /* Aggregated route was not choosen as best route for advt.
                     * No need for any processing. */
                }
            }
        }

        if (BGP4_RT_BGP_INFO (pAggrRoute) != NULL)
        {
            /* Release BGP Info associated with the Aggregate route. */
            Bgp4DshReleaseBgpInfo (BGP4_RT_BGP_INFO (pAggrRoute));
        }
        BGP4_RT_RESET_FLAG (pAggrRoute, BGP4_RT_AGGREGATED);

        /* Reset the Aggregated route into Dummy aggregate route. */
        pAggrRoute->u4MED = BGP4_INV_MEDLP;
        BGP4_RT_PROTOCOL (pAggrRoute) = PROTO_INVALID;
        BGP4_RT_SET_RT_IF_INDEX (pAggrRoute, BGP4_INVALID_IFINDX);
        BGP4_RT_SET_FLAG (pAggrRoute, BGP4_RT_AGGR_ROUTE);
        BGP4_RT_BGP4_INF (pAggrRoute) = NULL;

        /* update the Aggregate entry Init variables. */
        if (u4Index < BGP4_MAX_AGGR_ENTRIES)
        {
            Bgp4InitAddrPrefixStruct (&
                                      (BGP4_AGGRENTRY_INIT_PREFIX_INFO
                                       (u4Index)),
                                      BGP4_AGGRENTRY_IPADDR_FAMILY (u4Index));
        }
        /* Update the Aggregate entry status. */
        BGP4_AGGR_RESET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                BGP4_AGGR_ALL_FLAG);
        if (BGP4_LOCAL_ADMIN_STATUS (BGP4_AGGRENTRY_VRFID (u4Index)) ==
            BGP4_ADMIN_UP)
        {
            BGP4_AGGR_SET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                  BGP4_AGGR_DELETE_IN_PROGRESS);
        }
        else
        {
            /* Global admin status is down. Free the aggregated route
             * and clear the aggregate list if any route is present.
             * Clear the list is only a precausious step, since while
             * global admin is made down any way the routes in the
             * list would have been removed automatically. */
            Bgp4DshReleaseList (BGP4_AGGRENTRY_AGGR_LIST (u4Index), 0);
            if (BGP4_AGGRENTRY_AGGR_ROUTE (u4Index) != NULL)
            {
#ifdef L3VPN
                /* release the aggregate label */
                if (BGP4_RT_LABEL_CNT (BGP4_AGGRENTRY_AGGR_ROUTE (u4Index)) > 0)
                {
                    Bgp4DelBgp4Label (BGP4_RT_LABEL_INFO
                                      (BGP4_AGGRENTRY_AGGR_ROUTE (u4Index)));
                }
#endif
                Bgp4DshReleaseRtInfo (BGP4_AGGRENTRY_AGGR_ROUTE (u4Index));
                BGP4_AGGRENTRY_AGGR_ROUTE (u4Index) = NULL;
                BGP4_AGGR_RESET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                        BGP4_AGGR_ALL_FLAG);
                Bgp4InitAggrEntry (&(BGP4_AGGRENTRY[u4Index]));

            }
        }
    }
    else
    {
        /* Previous Admin status is down. Free the aggregated route
         * and clear the aggregate list if any route is present. */
        Bgp4DshReleaseList (BGP4_AGGRENTRY_AGGR_LIST (u4Index), 0);
        if (BGP4_AGGRENTRY_AGGR_ROUTE (u4Index) != NULL)
        {
            Bgp4DshReleaseRtInfo (BGP4_AGGRENTRY_AGGR_ROUTE (u4Index));
            BGP4_AGGRENTRY_AGGR_ROUTE (u4Index) = NULL;
        }
        BGP4_AGGR_RESET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                BGP4_AGGR_ALL_FLAG);
        Bgp4InitAggrEntry (&(BGP4_AGGRENTRY[u4Index]));
    }

    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Bgp4AggrProcessAdvertiseChange                            */
/* Description   : Does the required operation whenever the Aggregate Entry's*/
/*                 Advertise Type (SUMMARY, ALL) changes.                   */
/* Input(s)      : Index of the Aggregate Entry in the table(u4Index),      */
/*                 Advertise Type value which needs to be set (i4AdvValue)  */
/* Output(s)     : None                                                     */
/* Return(s)     : BGP4_SUCCESS  if the operation is successful,            */
/*                 BGP4_FAILURE if it fails.                                */
/*****************************************************************************/
INT4
Bgp4AggrProcessAdvertiseChange (UINT4 u4Index, INT4 i4AdvValue)
{
    tTMO_SLL           *pAggrList = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tBgp4PeerEntry     *pPeer = NULL;
    tLinkNode          *pLinkNode = NULL;

    BGP4_DBG (BGP4_DBG_ENTRY, "\tBgp4AggrProcessAdvertiseChange() ....... \n");

    if (BGP4_AGGRENTRY_ADMIN (u4Index) == BGP4_ADMIN_UP)
    {
        if ((BGP4_AGGRENTRY_ADV_TYPE (u4Index) == BGP4_SUMMARY) &&
            (i4AdvValue == BGP4_ALL))
        {
            /* Advertise all the routes used for aggregation to
             * all the peers, as feasible routes.
             */
            pAggrList = (BGP4_AGGRENTRY_AGGR_LIST (u4Index));

            TMO_SLL_Scan (pAggrList, pLinkNode, tLinkNode *)
            {
                pRtProfile = pLinkNode->pRouteProfile;
                /* Check whether the route is from peer going through
                 * de-init. If yes, then dont consider this route.
                 * This will be taken care in the normal path. */
                if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
                {
                    pPeer = BGP4_RT_PEER_ENTRY (pRtProfile);
                    if (BGP4_GET_PEER_CURRENT_STATE (pPeer) ==
                        BGP4_PEER_DEINIT_INPROGRESS)
                    {
                        /* Peer going down. Get next route. */
                        continue;
                    }
                }
                Bgp4AggrAdvtSuppressedRoute (pRtProfile);
            }
            /* Update the aggregate entry advertisement status. */
            BGP4_AGGRENTRY_ADV_TYPE (u4Index) = BGP4_ALL;
        }
        else if ((BGP4_AGGRENTRY_ADV_TYPE (u4Index) == BGP4_ALL) &&
                 (i4AdvValue == BGP4_SUMMARY))
        {
            /* Advertise all the routes used for aggregation to
             * all the external peers, as withdrawn.
             */
            pAggrList = (BGP4_AGGRENTRY_AGGR_LIST (u4Index));
            TMO_SLL_Scan (pAggrList, pLinkNode, tLinkNode *)
            {
                pRtProfile = pLinkNode->pRouteProfile;
                /* Check whether the route is from peer going through
                 * de-init. If yes, then dont consider this route.
                 * This will be taken care in the normal path. */
                if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
                {
                    pPeer = BGP4_RT_PEER_ENTRY (pRtProfile);
                    if (BGP4_GET_PEER_CURRENT_STATE (pPeer) ==
                        BGP4_PEER_DEINIT_INPROGRESS)
                    {
                        /* Peer going down. Get next route. */
                        continue;
                    }
                }
                Bgp4AggrWithdrawAdvtRoute (pRtProfile);
            }
            /* Update the aggregate entry advertisement status. */
            BGP4_AGGRENTRY_ADV_TYPE (u4Index) = BGP4_SUMMARY;
        }
    }
    else
    {
        /* Aggrgate entry admin status is DOWN. Check whether
         * admin down operation is in progress or not. If admin down
         * is in progress, then we shall not update this advt status
         * immediately because it will affect the admin down process.
         * So set the advt change pend flag. If admin down is not
         * in progress, then just update the advt status. */
        if ((BGP4_AGGRENTRY_AGGR_STATUS (u4Index) &
             BGP4_AGGR_DELETE_IN_PROGRESS) == BGP4_AGGR_DELETE_IN_PROGRESS)
        {
            if ((BGP4_AGGRENTRY_AGGR_STATUS (u4Index) &
                 BGP4_AGGR_PENDING_ADVT_CHG) == BGP4_AGGR_PENDING_ADVT_CHG)
            {
                if (BGP4_AGGRENTRY_ADV_TYPE (u4Index) == i4AdvValue)
                {
                    /* Already advt policy change is pending. and the
                     * new policy is same as that of the existing policy.
                     * Means user have changed the policy again. So just
                     * reset the pending flag. */
                    BGP4_AGGR_RESET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                            BGP4_AGGR_PENDING_ADVT_CHG);
                }
                else
                {
                    /* Already status change is set as pending. No need
                     * to do any operation. */
                }
            }
            else
            {
                /* Set the status as advt change pend if the policy is
                 * changed. */
                if (BGP4_AGGRENTRY_ADV_TYPE (u4Index) != i4AdvValue)
                {
                    BGP4_AGGR_SET_STATUS ((&(BGP4_AGGRENTRY[u4Index])),
                                          BGP4_AGGR_PENDING_ADVT_CHG);
                }
            }
        }
        else
        {
            BGP4_AGGRENTRY_ADV_TYPE (u4Index) = (UINT1) i4AdvValue;
        }
    }
    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Bgp4AggrClearAggrgateTable                                */
/* Description   : This function disables all the aggregate entries  in the  */
/*               : aggregate table and re-initialize the aggregate table.    */
/* Input(s)      : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS  if the operation is successful,             */
/*                 BGP4_FAILURE if it fails.                                 */
/*****************************************************************************/
INT4
Bgp4AggrClearAggrgateTable (UINT4 u4Context)
{
    tBgp4AggrEntry     *pAggrEntry = NULL;
    UINT4               u4AggrTblIndex;

    for (u4AggrTblIndex = 0; u4AggrTblIndex < BGP4_MAX_AGGR_ENTRIES;
         u4AggrTblIndex++)
    {
        pAggrEntry = (&(BGP4_AGGRENTRY[u4AggrTblIndex]));
        if (pAggrEntry->u4VrfId == u4Context)
        {
            Bgp4AggrClearAggrgateTableEntry (u4AggrTblIndex);
        }
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AggrClearAggrgateTableEntry                           */
/* Description   : This function disables the given entry in the aggregate   */
/*               : table and re-initializes the aggregate table entry.       */
/* Input(s)      : Index of the Aggregate table entry that needs to be       */
/*               : cleared. (u4AggrTblIndex)                                 */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS  if the operation is successful,             */
/*                 BGP4_FAILURE if it fails.                                 */
/*****************************************************************************/
INT4
Bgp4AggrClearAggrgateTableEntry (UINT4 u4AggrTblIndex)
{
    if (BGP4_MAX_AGGR_ENTRIES > u4AggrTblIndex)
    {
        if (BGP4_AGGRENTRY_ADMIN (u4AggrTblIndex) == BGP4_ADMIN_UP)
        {
            Bgp4AggrProcessAdminChange (u4AggrTblIndex, BGP4_ADMIN_DOWN);
        }
        Bgp4InitAggrEntry (&(BGP4_AGGRENTRY[u4AggrTblIndex]));
        return BGP4_SUCCESS;
    }
    return BGP4_FAILURE;
}

/*****************************************************************************/
/* Function Name : Bgp4AggrAdvtAggrRouteToPeer                               */
/* Description   : This function advertises all the aggregated routes to the */
/*               : given peer.                                               */
/* Input(s)      : Pointer to the peer entry (pPeer)                         */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS  if the operation is successful,             */
/*                 BGP4_FAILURE if it fails.                                 */
/*****************************************************************************/
INT4
Bgp4AggrAdvtAggrRouteToPeer (tBgp4PeerEntry * pPeer)
{
    tTMO_SLL            TsAggrAdvtList;
    tTMO_SLL            TsWithdrawnList;
    tRouteProfile      *pAggrRoute = NULL;
    tRouteProfile      *pCopiedAggrRoute = NULL;
    tRouteProfile      *pBestRoute = NULL;
    tBgp4Info          *pBgp4Info = NULL;
    UINT4               u4Index = 0;
    UINT1               u1CanAggrRtBeAdvt = BGP4_FALSE;
    INT1                i1AdvtStatus = BGP4_FALSE;

    TMO_SLL_Init (&TsAggrAdvtList);
    TMO_SLL_Init (&TsWithdrawnList);

    for (u4Index = 0; u4Index < BGP4_MAX_AGGR_ENTRIES; u4Index++)
    {
        if (BGP4_AGGRENTRY_ADMIN (u4Index) != BGP4_ADMIN_UP)
        {
            continue;
        }
        pAggrRoute = BGP4_AGGRENTRY_AGGR_ROUTE (u4Index);
        if (((BGP4_RT_GET_FLAGS (pAggrRoute)) & BGP4_RT_AGGREGATED) ==
            BGP4_RT_AGGREGATED)
        {
            /* Aggregated route exists. Check whether this aggregated route
             * can be advertised or not. */
            u1CanAggrRtBeAdvt = Bgp4AggrCanAggrRouteBeAdvt (pAggrRoute,
                                                            &pBestRoute);
            if (u1CanAggrRtBeAdvt == BGP4_TRUE)
            {
                if ((GetAdvtStatus (pAggrRoute, pPeer, &i1AdvtStatus)
                     == BGP4_FAILURE) || (i1AdvtStatus == BGP4_FALSE))
                {
                    /* Aggregated route advertisement is suppressed by Community Attribute.
                     * Donot add it only to the external peer advertisement list */
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC,
                              BGP4_MOD_NAME,
                              "Route Not Advertised. Suppressed by Community Attribute.\n");
                    continue;
                }
                /* Aggregated route needs to be advertised. */
                pCopiedAggrRoute =
                    Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
                if (pCopiedAggrRoute != NULL)
                {
                    pBgp4Info = Bgp4MemAllocateBgp4info (sizeof (tBgp4Info));
                    if (pBgp4Info == NULL)
                    {
                        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                       BGP4_ALL_FAILURE_TRC | BGP4_EVENTS_TRC |
                                       BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                       "\tMemory Allocation for advertising "
                                       "Aggregated Route Info\n\t"
                                       "- Dest %s, Mask %s FAILED!!!\n",
                                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                        (pCopiedAggrRoute),
                                                        BGP4_RT_AFI_INFO
                                                        (pCopiedAggrRoute)),
                                       Bgp4PrintIpMask ((UINT1)
                                                        BGP4_RT_PREFIXLEN
                                                        (pCopiedAggrRoute),
                                                        BGP4_RT_AFI_INFO
                                                        (pCopiedAggrRoute)));
                        Bgp4DshReleaseRtInfo (pCopiedAggrRoute);
                        gu4BgpDebugCnt[MAX_BGP_ROUTE_INFO_ENTRIES_SIZING_ID]++;
                    }
                    else
                    {
                        BGP4_LINK_INFO_TO_PROFILE (pBgp4Info, pCopiedAggrRoute);
                        Bgp4AggrCopyRoutes (pCopiedAggrRoute, pAggrRoute,
                                            BGP4_AGGRENTRY_ADV_TYPE (u4Index),
                                            BGP4_AGGRENTRY_AS_SET (u4Index));
                        BGP4_RT_RESET_FLAG (pCopiedAggrRoute,
                                            BGP4_RT_WITHDRAWN);
                        Bgp4DshAddRouteToList (&TsAggrAdvtList,
                                               pCopiedAggrRoute, BGP4_TRUE);
                    }
                }
                else
                {
                    BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                                   BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                                   BGP4_MOD_NAME,
                                   "\tMemory Allocation for advertising "
                                   "Aggregated Route \n\t"
                                   "- Dest %s, Mask %s FAILED!!!\n",
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pAggrRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pAggrRoute)),
                                   Bgp4PrintIpMask ((UINT1)
                                                    BGP4_RT_PREFIXLEN
                                                    (pAggrRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pAggrRoute)));
                    gu4BgpDebugCnt[MAX_BGP_ROUTES_SIZING_ID]++;
                }
            }
        }
    }
    if (TMO_SLL_Count (&TsAggrAdvtList) > 0)
    {
        /* Add the route to the Peer's Transimission queue. */
        Bgp4PeerSendRoutesToPeerTxQ (pPeer, &TsAggrAdvtList, &TsWithdrawnList,
                                     BGP4_FALSE);
    }
    Bgp4DshReleaseList (&TsAggrAdvtList, 0);
    Bgp4DshReleaseList (&TsWithdrawnList, 0);
    return (BGP4_FALSE);
}

/*****************************************************************************/
/* Function Name : Bgp4CheckAggrRestrict                                     */
/* Description   : Checks if the given index has an aggregated route         */
/*                 that is restricted                                        */
/* Input(s)      : Aggregate table index                                     */
/* Output(s)     : Aggregated BGP Route                                      */
/* Return(s)     : RESTRICT/ALLOW                                            */
/*****************************************************************************/
INT4
Bgp4CheckAggrRestrict (INT4 i4AggrtblIndex, tRouteProfile * pRoute)
{
    UINT4               u4AsafiMask;
    INT4                i4MatchSts = BGP4_TRUE;

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRoute),
                           BGP4_RT_SAFI_INFO (pRoute), u4AsafiMask);
#ifdef L3VPN
    if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
    {
        if (BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_CONVERTED_TO_VPNV4)
        {
            u4AsafiMask = CAP_MP_IPV4_UNICAST;
        }
    }
#endif

    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
            if (BGP4_RT_PREFIXLEN (pRoute) !=
                BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (BGP4_AGGRENTRY_NET_ADDR_INFO
                                                    (i4AggrtblIndex)))
            {
                i4MatchSts = BGP4_FALSE;
                break;
            }

            if ((AddrMatch
                 (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                  (BGP4_RT_NET_ADDRESS_INFO (pRoute)),
                  BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                  (BGP4_AGGRENTRY_NET_ADDR_INFO (i4AggrtblIndex)),
                  BGP4_RT_PREFIXLEN (pRoute))) != BGP4_TRUE)
            {
                i4MatchSts = BGP4_FALSE;
                break;
            }
            break;
#endif
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
#endif
            i4MatchSts = NetAddrMatch (BGP4_RT_NET_ADDRESS_INFO (pRoute),
                                       BGP4_AGGRENTRY_NET_ADDR_INFO
                                       (i4AggrtblIndex));
            break;
#ifdef L3VPN
        case CAP_MP_VPN4_UNICAST:
            if ((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_CONVERTED_TO_VPNV4) &&
                (!((BGP4_AGGRENTRY_IPSUBADDR_FAMILY (i4AggrtblIndex) ==
                    BGP4_INET_SAFI_UNICAST) &&
                   (BGP4_AGGRENTRY_PREFIXLEN (i4AggrtblIndex) ==
                    BGP4_RT_PREFIXLEN (pRoute)) &&
                   ((AddrMatch
                     (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                      (BGP4_AGGRENTRY_NET_ADDR_INFO (i4AggrtblIndex)),
                      BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                      (BGP4_RT_NET_ADDRESS_INFO (pRoute)),
                      BGP4_RT_PREFIXLEN (pRoute)) == BGP4_TRUE)))))
            {
                i4MatchSts = BGP4_FALSE;
                break;
            }
            else if ((!
                      (BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_CONVERTED_TO_VPNV4))
                     &&
                     (!((BGP4_AGGRENTRY_IPSUBADDR_FAMILY (i4AggrtblIndex) ==
                         BGP4_INET_SAFI_VPNV4_UNICAST)
                        && (BGP4_AGGRENTRY_PREFIXLEN (i4AggrtblIndex) ==
                            BGP4_RT_PREFIXLEN (pRoute))
                        &&
                        ((Vpnv4AddrMatch
                          (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                           (BGP4_AGGRENTRY_NET_ADDR_INFO (i4AggrtblIndex)),
                           pRoute, BGP4_RT_PREFIXLEN (pRoute)) == BGP4_TRUE)))))
            {
                i4MatchSts = BGP4_FALSE;
                break;
            }
            i4MatchSts = BGP4_TRUE;
            break;
#endif
        default:
            i4MatchSts = BGP4_FALSE;
            break;
    }

    if ((i4MatchSts == BGP4_TRUE) &&
        (BGP4_AGGRENTRY_ADV_TYPE (i4AggrtblIndex) == BGP4_RESTRICT))
    {
        return BGP4_RESTRICT;
    }
    return BGP4_ALLOW;
}

/*****************************************************************************/
/* Function Name : Bgp4AggrUpdateAggrInitRoute                               */
/* Description   : This routine is called whenever a route is removed from   */
/*                 the LOCAL RIB. This function checks whether the removed   */
/*                 route and the route stored in the aggregate init route    */
/*                 variable are identical. If yes, then the next route       */
/*                 from the LOCAL RIB is obtained and stored in the aggregate*/
/*                 init route variable.                                      */
/* Input(s)      : Pointer to the current route profile (pRtProfile)         */
/*                 Pointer to the treenode in which the route is present     */
/*                          - pTreeNode                                      */
/*               : i4Flag - Flag passed to RIB for semaphore lock decision   */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS.                                             */
/*****************************************************************************/
INT4
Bgp4AggrUpdateAggrInitRoute (tRouteProfile * pRtProfile, VOID *pRibNode,
                             INT4 i4Flag)
{
    VOID               *pNode = pRibNode;
    tRouteProfile      *pNextRoute = NULL;
    UINT4               u4Index = 0;
    INT4                i4RetVal = BGP4_FAILURE;

    for (u4Index = 0; u4Index < BGP4_MAX_AGGR_ENTRIES; u4Index++)
    {
        if ((PrefixMatch (pRtProfile->NetAddress.NetAddr,
                          BGP4_AGGRENTRY_INIT_PREFIX_INFO (u4Index)) ==
             BGP4_TRUE)
            && (BGP4_RT_PREFIXLEN (pRtProfile) ==
                BGP4_AGGRENTRY_INIT_PREFIX_LEN (u4Index)))
        {
            i4RetVal = Bgp4RibhGetNextEntry (pRtProfile,
                                             BGP4_RT_CXT_ID (pRtProfile),
                                             &pNextRoute,
                                             (VOID *) &pNode, i4Flag);
            if (i4RetVal == BGP4_FAILURE)
            {
                /* No more route present in the RIB. Dont Reset init route.
                 * Aggregation handler module will take care of further
                 * action.*/
            }
            else
            {
                /* Reset the aggregate init route with the next route info. */
                Bgp4CopyNetAddressStruct (&(gBgpNode.aBGP4Aggrtbl[u4Index].
                                            AggrInitAddr),
                                          pNextRoute->NetAddress);
            }
        }
    }
    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : BgpSetRoutelistflag                                       */
/* Description   : To Set the Status flag of the Routes in RIB               */
/* Input(s)      : a) List of aggregated routes whose flag has to be set     */
/*                 b) Flag status                                            */
/* Output(s)     : Status of the operation                                   */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_FAILURE - if operation fails                         */
/*****************************************************************************/
INT4
BgpSetRoutelistflag (tTMO_SLL * pTobeAggrList, INT4 u4Flag)
{
    tLinkNode          *pRtLink = NULL;
    tRouteProfile      *pRtProfile = NULL;
    INT4                i4RetVal = 0;
    tRouteProfile      *pRtProfileList = NULL;
    tRouteProfile      *pTravProfileList = NULL;
    VOID               *pRibNode = NULL;
    if (pTobeAggrList == NULL)
    {
        return BGP4_SUCCESS;
    }
    if (TMO_SLL_Count (pTobeAggrList) == 0)
    {
        return BGP4_SUCCESS;
    }
    TMO_SLL_Scan (pTobeAggrList, pRtLink, tLinkNode *)
    {

        pRtProfile = pRtLink->pRouteProfile;
        i4RetVal = Bgp4RibhLookupRtEntry (pRtProfile,
                                          BGP4_RT_CXT_ID (pRtProfile),
                                          BGP4_TREE_FIND_EXACT, &pRtProfileList,
                                          &pRibNode);
        pTravProfileList = pRtProfile;
        if (i4RetVal == BGP4_SUCCESS)
        {
            while (pTravProfileList != NULL)
            {
                if (u4Flag == BGP4_RT_AGGREGATED)
                {
                    BGP4_RT_SET_FLAG (pTravProfileList, BGP4_RT_AGGREGATED);
                }
                else
                {
                    BGP4_RT_RESET_FLAG (pTravProfileList, BGP4_RT_AGGREGATED);
                }
                pTravProfileList = BGP4_RT_NEXT (pTravProfileList);
            }
        }
    }
    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Bgp4CheckAggrForSuppMap                                   */
/* Description   : To check whether the rotue is belong to suppress map      */
/*                 and the access of the route map is also matched           */
/* Input(s)      : a) The Input route                                        */
/*                 b) Suppress route map name                                */
/* Output(s)     : Status of the operation                                   */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_FAILURE - if operation fails                         */
/*****************************************************************************/
INT4
Bgp4CheckAggrForSuppMap (tRouteProfile * pRoute, UINT1 *pu1SuppRMapName)
{
    tRtMapInfo          RtInfoIn;
    UINT1               u1MatchFlag = RMAP_ROUTE_DENY;
    UINT1               u1Access = RMAP_ROUTE_DENY;

    MEMSET (&RtInfoIn, 0, sizeof (tRtMapInfo));

#ifdef BGP4_IPV6_WANTED
    if (pRoute->NetAddress.NetAddr.u2Afi == BGP4_INET_AFI_IPV6)
    {
        IPVX_ADDR_INIT_IPV6 (RtInfoIn.DstXAddr, IPVX_ADDR_FMLY_IPV6,
                             pRoute->NetAddress.NetAddr.au1Address);
    }
    else
#endif /* BGP4_IPV6_WANTED */
    {
        IPVX_ADDR_INIT_IPV4 (RtInfoIn.DstXAddr, IPVX_ADDR_FMLY_IPV4,
                             pRoute->NetAddress.NetAddr.au1Address);
    }
    RtInfoIn.u2DstPrefixLen = pRoute->NetAddress.u2PrefixLen;

#ifdef ROUTEMAP_WANTED
    u1MatchFlag = RMapApplyRule2 (&RtInfoIn, pu1SuppRMapName);
#else
    UNUSED_PARAM (pu1SuppRMapName);
#endif
    u1Access = RtInfoIn.u1RmapAccess;
    /* permit map */
    if (u1Access == RMAP_ROUTE_PERMIT)
    {
        if (u1MatchFlag == RMAP_ENTRY_MATCHED)
        {
            return BGP4_FALSE;
        }
        else
        {
            return BGP4_TRUE;
        }
    }
    /* Deny map */
    else if (u1Access == RMAP_ROUTE_DENY)
    {
        if (u1MatchFlag == RMAP_ENTRY_MATCHED)
        {
            return BGP4_TRUE;
        }
        else
        {
            return BGP4_FALSE;
        }
    }
    return BGP4_FALSE;
}

/*****************************************************************************/
/* Function Name : Bgp4CheckAggrForAdvMap                                    */
/* Description   : To check whether the rotue is belong to suppress map      */
/*                 and the access of the route map is also matched           */
/* Input(s)      : a) The Input route                                        */
/*                 b) Advertise route map name                               */
/* Output(s)     : Status of the operation                                   */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_FAILURE - if operation fails                         */
/*****************************************************************************/
INT4
Bgp4CheckAggrForAdvMap (tRouteProfile * pRoute, UINT1 *pu1AdvRMapName)
{
    tRtMapInfo          RtInfoIn;
    UINT1               u1MatchFlag = RMAP_ROUTE_DENY;
    UINT1               u1Access = RMAP_ROUTE_DENY;

    MEMSET (&RtInfoIn, 0, sizeof (tRtMapInfo));

#ifdef BGP4_IPV6_WANTED
    if (pRoute->NetAddress.NetAddr.u2Afi == BGP4_INET_AFI_IPV6)
    {
        IPVX_ADDR_INIT_IPV6 (RtInfoIn.DstXAddr, IPVX_ADDR_FMLY_IPV6,
                             pRoute->NetAddress.NetAddr.au1Address);
    }
    else
#endif /* BGP4_IPV6_WANTED */
    {
        IPVX_ADDR_INIT_IPV4 (RtInfoIn.DstXAddr, IPVX_ADDR_FMLY_IPV4,
                             pRoute->NetAddress.NetAddr.au1Address);
    }
    RtInfoIn.u2DstPrefixLen = pRoute->NetAddress.u2PrefixLen;

#ifdef ROUTEMAP_WANTED
    u1MatchFlag = RMapApplyRule2 (&RtInfoIn, pu1AdvRMapName);
#else
    UNUSED_PARAM (pu1AdvRMapName);
#endif
    u1Access = RtInfoIn.u1RmapAccess;

    if (u1Access == RMAP_ROUTE_PERMIT)
    {
        if (u1MatchFlag == RMAP_ENTRY_MATCHED)
        {
            return BGP4_TRUE;
        }
        else
        {
            return BGP4_FALSE;
        }
    }
    else if (u1Access == RMAP_ROUTE_DENY)
    {
        if (u1MatchFlag == RMAP_ENTRY_MATCHED)
        {
            return BGP4_FALSE;
        }
        else
        {
            return BGP4_TRUE;
        }
    }
    return BGP4_TRUE;
}

/*****************************************************************************/
/* Function Name : Bgp4FillAggrForAttMap                                     */
/* Description   : To check whether the rotue is belong to suppress map      */
/*                 and the access of the route map is also matched           */
/* Input(s)      : a) The Input route                                        */
/*                 b) Attribute route map name                               */
/* Output(s)     : Status of the operation                                   */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_FAILURE - if operation fails                         */
/*****************************************************************************/
INT4
Bgp4FillAggrForAttMap (tRouteProfile * pRoute, UINT1 *pu1AdvRMapName,
                       tRouteProfile * pAggrRoute)
{
    UINT1              *pu1CommVal = NULL;
    tRtMapInfo          RtInfoIn;
    tRtMapInfo          RtInfoDummy;
    tBgp4Info          *pAggrBgpInfo = NULL;

    MEMSET (&RtInfoIn, 0, sizeof (tRtMapInfo));
    MEMSET (&RtInfoDummy, 0, sizeof (tRtMapInfo));

    pAggrBgpInfo = BGP4_RT_BGP_INFO (pAggrRoute);

#ifdef BGP4_IPV6_WANTED
    if (pRoute->NetAddress.NetAddr.u2Afi == BGP4_INET_AFI_IPV6)
    {
        IPVX_ADDR_INIT_IPV6 (RtInfoIn.DstXAddr, IPVX_ADDR_FMLY_IPV6,
                             pRoute->NetAddress.NetAddr.au1Address);
    }
    else
#endif /* BGP4_IPV6_WANTED */
    {
        IPVX_ADDR_INIT_IPV4 (RtInfoIn.DstXAddr, IPVX_ADDR_FMLY_IPV4,
                             pRoute->NetAddress.NetAddr.au1Address);
    }
    RtInfoIn.u2DstPrefixLen = pRoute->NetAddress.u2PrefixLen;
#ifdef ROUTEMAP_WANTED
    if (RMAP_ROUTE_PERMIT == RMapApplyRule3 (&RtInfoIn, &RtInfoDummy,
                                             pu1AdvRMapName))
#else
    UNUSED_PARAM (pu1AdvRMapName);
#endif
    {
        /* Copy the attributes from attribute route map */
        if (RtInfoDummy.i4Metric != 0)
        {
            pAggrRoute->i4NextHopMetric = RtInfoDummy.i4Metric;
        }
        if (RtInfoDummy.i4Origin != 0)
        {
            BGP4_INFO_ORIGIN (pAggrBgpInfo) = (UINT1) RtInfoDummy.i4Origin;
        }
        if (RtInfoDummy.u4LocalPref != 0)
        {
            BGP4_INFO_RCVD_LOCAL_PREF (pAggrBgpInfo) = RtInfoDummy.u4LocalPref;
        }
        if (RtInfoDummy.u1CommunityCnt != 0)
        {
            if (BGP4_INFO_COMM_ATTR (pAggrBgpInfo) == NULL)
            {
                COMMUNITY_NODE_CREATE (BGP4_INFO_COMM_ATTR (pAggrBgpInfo));
                if (BGP4_INFO_COMM_ATTR (pAggrBgpInfo) == NULL)
                {
                    ATTRIBUTE_NODE_FREE (pu1CommVal);
                    return BGP4_FALSE;
                }
            }
            ATTRIBUTE_NODE_CREATE (pu1CommVal);
            if (pu1CommVal == NULL)
            {
                COMMUNITY_NODE_FREE (BGP4_INFO_COMM_ATTR (pAggrBgpInfo));
                return BGP4_FALSE;
            }
            MEMCPY (pu1CommVal, &(RtInfoDummy.au4Community),
                    COMM_VALUE_LEN * RtInfoDummy.u1CommunityCnt);
            BGP4_INFO_COMM_ATTR_VAL (pAggrBgpInfo) = pu1CommVal;
            BGP4_INFO_COMM_COUNT (pAggrBgpInfo) = 1;
            BGP4_INFO_COMM_ATTR_FLAG (pAggrBgpInfo) |= BGP4_PARTIAL_FLAG_MASK;
        }
    }
    return BGP4_TRUE;
}
#endif /* BGP4AGGR_C */
