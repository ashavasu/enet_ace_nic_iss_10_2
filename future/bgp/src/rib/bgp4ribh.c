/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4ribh.c,v 1.47 2017/09/15 06:19:55 siva Exp $
 *
 * Description: Contains the core of the RIB handler. Updates the
 *              Local RIB according to the received updates and   
 *              appropriately forms the update list which needs to 
 *              be advertised to other peers. 
 *
 *******************************************************************/
#ifndef BGP4RIBH_C
#define BGP4RIBH_C

#include "bgp4com.h"

/*****************************************************************************/
/* Function Name : Bgp4RibhStartConnection                                   */
/* Description   : This function is called from the SEM Handler whenever a   */
/*                 configured peer changes its state to ESTABLISHED. The best*/
/*                 routes from the RIB tree are collected and advertised to  */
/*                 the new peer.                                             */
/* Input(s)      : Information about the peer which is newly connected       */
/*                 (pPeer)                                                   */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS.                                             */
/*****************************************************************************/
INT4
Bgp4RibhStartConnection (tBgp4PeerEntry * pPeer)
{
    tPeerNode          *pPeerNode = NULL;

    switch (BGP4_GET_PEER_CURRENT_STATE (pPeer))
    {
        case BGP4_PEER_STALE_DEL_INPROGRESS:
        case BGP4_PEER_UNKNOWN_STATE:
        case BGP4_PEER_READY:
            if ((BGP4_GET_PEER_CURRENT_STATE (pPeer) == BGP4_PEER_UNKNOWN_STATE)
                && (pPeer->peerStatus.u1RestartMode != BGP4_RESTARTING_MODE))
            {
                return BGP4_SUCCESS;
            }

            BGP_PEER_NODE_CREATE (pPeerNode);
            if (pPeerNode == NULL)
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : Memory Allocation to handle "
                               "Peer Initialization FAILED!!!\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeer))));
                gu4BgpDebugCnt[MAX_BGP_PEER_NODE_SIZING_ID]++;
                return BGP4_FAILURE;
            }
            pPeerNode->pPeer = pPeer;
            /* Initialize init information based on negotiated capabilities.
             * <IPV4,UNICAST> is advertised by default to IPV4 peers. */
            switch (BGP4_AFI_IN_ADDR_PREFIX_INFO
                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeer)))
            {
                case BGP4_INET_AFI_IPV4:
                    Bgp4InitNetAddressStruct (&
                                              (BGP4_PEER_INIT_NETADDR_INFO
                                               (pPeer)), BGP4_INET_AFI_IPV4,
                                              BGP4_INET_SAFI_UNICAST);
                    if (BGP4_PEER_NEG_ASAFI_MASK (pPeer) == 0)
                    {
                        BGP4_PEER_INIT_AFI (pPeer) = BGP4_INET_AFI_IPV4;
                        BGP4_PEER_INIT_SAFI (pPeer) = BGP4_INET_SAFI_UNICAST;
                    }
                    else if (BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                             CAP_NEG_IPV4_UNI_MASK)
                    {
                        BGP4_PEER_INIT_AFI (pPeer) = BGP4_INET_AFI_IPV4;
                        BGP4_PEER_INIT_SAFI (pPeer) = BGP4_INET_SAFI_UNICAST;
                    }
#ifdef L3VPN
                    else if (BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                             CAP_NEG_LBL_IPV4_MASK)
                    {
                        /* carrying label routes are stored in IPV4 rib only.
                         * hence, initialize to <ipv4, unicast> only
                         */
                        BGP4_PEER_INIT_AFI (pPeer) = BGP4_INET_AFI_IPV4;
                        BGP4_PEER_INIT_SAFI (pPeer) = BGP4_INET_SAFI_UNICAST;
                    }
#endif
#ifdef BGP4_IPV6_WANTED
                    else if (BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                             CAP_NEG_IPV6_UNI_MASK)
                    {
                        BGP4_PEER_INIT_AFI (pPeer) = BGP4_INET_AFI_IPV6;
                        BGP4_PEER_INIT_SAFI (pPeer) = BGP4_INET_SAFI_UNICAST;
                    }
#endif
#ifdef L3VPN
                    else if (BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                             CAP_NEG_VPN4_UNI_MASK)
                    {
                        BGP4_PEER_INIT_AFI (pPeer) = BGP4_INET_AFI_IPV4;
                        BGP4_PEER_INIT_SAFI (pPeer) =
                            BGP4_INET_SAFI_VPNV4_UNICAST;
                        BGP4_PEER_INIT_PE_CONTEXT (pPeer) = BGP4_DFLT_VRFID;
                    }
#endif
#ifdef VPLSADS_WANTED
                    else if (BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                             CAP_NEG_L2VPN_VPLS_MASK)
                    {
                        BGP4_PEER_INIT_AFI (pPeer) = BGP4_INET_AFI_L2VPN;
                        BGP4_PEER_INIT_SAFI (pPeer) = BGP4_INET_SAFI_VPLS;
                    }
#endif
#ifdef EVPN_WANTED
                    else if (BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                             CAP_NEG_L2VPN_EVPN_MASK)
                    {
                        BGP4_PEER_INIT_AFI (pPeer) = BGP4_INET_AFI_L2VPN;
                        BGP4_PEER_INIT_SAFI (pPeer) = BGP4_INET_SAFI_EVPN;
                    }
#endif
#ifdef L3VPN
                    if (BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_CE_PEER)
                    {
                        /* If the peer is a CE peer, then we should not
                         * advertise the local service provider networks.
                         * Only VPN routes should be advertised. hence
                         * intialize with VPN routing
                         */
                        BGP4_PEER_INIT_AFI (pPeer) = BGP4_INET_AFI_IPV4;
                        BGP4_PEER_INIT_SAFI (pPeer) =
                            BGP4_INET_SAFI_VPNV4_UNICAST;
                    }
#endif
                    break;

#ifdef BGP4_IPV6_WANTED
                case BGP4_INET_AFI_IPV6:
                    if ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                         CAP_NEG_IPV4_UNI_MASK) == CAP_NEG_IPV4_UNI_MASK)
                    {
                        Bgp4InitNetAddressStruct (&
                                                  (BGP4_PEER_INIT_NETADDR_INFO
                                                   (pPeer)), BGP4_INET_AFI_IPV4,
                                                  BGP4_INET_SAFI_UNICAST);
                        BGP4_PEER_INIT_AFI (pPeer) = BGP4_INET_AFI_IPV4;
                        BGP4_PEER_INIT_SAFI (pPeer) = BGP4_INET_SAFI_UNICAST;
                    }
                    else if ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                              CAP_NEG_IPV6_UNI_MASK) == CAP_NEG_IPV6_UNI_MASK)
                    {
                        Bgp4InitNetAddressStruct (&
                                                  (BGP4_PEER_INIT_NETADDR_INFO
                                                   (pPeer)), BGP4_INET_AFI_IPV6,
                                                  BGP4_INET_SAFI_UNICAST);
                        BGP4_PEER_INIT_AFI (pPeer) = BGP4_INET_AFI_IPV6;
                        BGP4_PEER_INIT_SAFI (pPeer) = BGP4_INET_SAFI_UNICAST;
                    }
                    /* IPv6 peering is not supported for L3VPN and L2VPN case. */
                    else
                    {
                        Bgp4InitNetAddressStruct (&
                                                  (BGP4_PEER_INIT_NETADDR_INFO
                                                   (pPeer)), 0, 0);
                        BGP4_PEER_INIT_AFI (pPeer) = 0;
                        BGP4_PEER_INIT_SAFI (pPeer) = 0;
                    }
                    break;
#endif
                default:
                    Bgp4InitNetAddressStruct (&
                                              (BGP4_PEER_INIT_NETADDR_INFO
                                               (pPeer)), 0, 0);
                    BGP4_PEER_INIT_AFI (pPeer) = 0;
                    BGP4_PEER_INIT_SAFI (pPeer) = 0;
                    break;
            }

            TMO_SLL_Add (BGP4_PEER_INIT_LIST (BGP4_PEER_CXT_ID (pPeer)),
                         &pPeerNode->TSNext);
            BGP4_SET_PEER_CURRENT_STATE (pPeer, BGP4_PEER_INIT_INPROGRESS);
            BGP4_PEER_INIT_PA_ENTRY (pPeer) = NULL;
            BGP4_PEER_INIT_PA_HASHKEY (pPeer) = 0;
            break;

        case BGP4_PEER_DEINIT_INPROGRESS:
        case BGP4_PEER_INIT_INPROGRESS:
        case BGP4_PEER_SOFTCONFIG_INBOUND_INPROGRESS:
        case BGP4_PEER_SOFTCONFIG_OUTBOUND_INPROGRESS:
        case BGP4_PEER_REUSE_INPROGRESS:
        default:
            return BGP4_SUCCESS;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4RibhInitPeerDeInitInfo                                */
/* Description   : This function is called when the peer has to be deleted   */
/*                 and hence de-initialized. This function initializes the   */
/*                 de-init information references.                           */
/* Input(s)      : Information about the peer which is getting deinitialized */
/* Output(s)     : None.                                                     */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
Bgp4RibhInitPeerDeInitInfo (tBgp4PeerEntry * pPeer)
{
    BGP4_PEER_INIT_PA_ENTRY (pPeer) = NULL;
    BGP4_PEER_INIT_PA_HASHKEY (pPeer) = 0;

    /* Initialize de-init information based on negotiated capabilities.
     * <IPV4,UNICAST> is advertised by default to IPV4 peers. */
    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_PEER_REMOTE_ADDR_INFO (pPeer)))
    {
        case BGP4_INET_AFI_IPV4:
            Bgp4InitNetAddressStruct (&(BGP4_PEER_INIT_NETADDR_INFO (pPeer)),
                                      BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);
            break;

#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                 CAP_NEG_IPV4_UNI_MASK) == CAP_NEG_IPV4_UNI_MASK)
            {
                Bgp4InitNetAddressStruct (&
                                          (BGP4_PEER_INIT_NETADDR_INFO (pPeer)),
                                          BGP4_INET_AFI_IPV4,
                                          BGP4_INET_SAFI_UNICAST);
            }
            else if ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                      CAP_NEG_IPV6_UNI_MASK) == CAP_NEG_IPV6_UNI_MASK)
            {
                Bgp4InitNetAddressStruct (&
                                          (BGP4_PEER_INIT_NETADDR_INFO (pPeer)),
                                          BGP4_INET_AFI_IPV6,
                                          BGP4_INET_SAFI_UNICAST);
            }
            else
            {
                Bgp4InitNetAddressStruct (&
                                          (BGP4_PEER_INIT_NETADDR_INFO (pPeer)),
                                          0, 0);
            }
            break;
#endif
        default:
            Bgp4InitNetAddressStruct (&(BGP4_PEER_INIT_NETADDR_INFO (pPeer)), 0,
                                      0);
            break;
    }
}

/*****************************************************************************/
/* Function Name : Bgp4RibhEndConnection                                     */
/* Description   : This function is called from the SEM Handler whenever a   */
/*                 configured peer changes its state from ESTABLISHED to     */
/*                 IDLE. The routes learnt from that particular peer is      */
/*                 removed from the RIB tree, and the new UPDATES are sent   */
/*                 to the other peers.                                       */
/* Input(s)      : Information about the peer which is to be removed (pPeer) */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4RibhEndConnection (tBgp4PeerEntry * pPeer)
{
    tPeerNode          *pPeerNode = NULL;

    switch (BGP4_GET_PEER_CURRENT_STATE (pPeer))
    {
        case BGP4_PEER_READY:
            BGP4_RESET_PEER_PEND_FLAG (pPeer,
                                       (BGP4_PEER_INIT_PENDING |
                                        BGP4_PEER_SOFTCONFIG_INBOUND_PEND |
                                        BGP4_PEER_SOFTCONFIG_OUTBOUND_PEND));
            break;

        case BGP4_PEER_INIT_INPROGRESS:
            Bgp4DshRemovePeerFromList (BGP4_PEER_INIT_LIST
                                       (BGP4_PEER_CXT_ID (pPeer)), pPeer);
            break;

        case BGP4_PEER_SOFTCONFIG_INBOUND_INPROGRESS:
            Bgp4DshRemovePeerFromList (BGP4_RTREF_PEER_LIST
                                       (BGP4_PEER_CXT_ID (pPeer)), pPeer);
            BGP4_RESET_PEER_PEND_FLAG (pPeer,
                                       BGP4_PEER_SOFTCONFIG_OUTBOUND_PEND |
                                       BGP4_PEER_REUSE_PEND);
            break;

        case BGP4_PEER_SOFTCONFIG_OUTBOUND_INPROGRESS:
            Bgp4DshRemovePeerFromList (BGP4_SOFTCFG_OUTBOUND_LIST
                                       (BGP4_PEER_CXT_ID (pPeer)), pPeer);
            BGP4_RESET_PEER_PEND_FLAG (pPeer,
                                       BGP4_PEER_SOFTCONFIG_INBOUND_PEND |
                                       BGP4_PEER_REUSE_PEND);
            break;

        case BGP4_PEER_DEINIT_INPROGRESS:
            /* De-init is already in progress. */
            BGP4_RESET_PEER_PEND_FLAG (pPeer, BGP4_PEER_INIT_PENDING);
            return BGP4_SUCCESS;

#ifdef RFD_WANTED
        case BGP4_PEER_REUSE_INPROGRESS:
            Bgp4DshRemovePeerFromList (BGP4_SUP_PEER_REUSE_LIST
                                       (BGP4_PEER_CXT_ID (pPeer)), pPeer);
            BGP4_RESET_PEER_PEND_FLAG (pPeer,
                                       BGP4_PEER_SOFTCONFIG_INBOUND_PEND |
                                       BGP4_PEER_SOFTCONFIG_OUTBOUND_PEND);
            break;
#endif
        case BGP4_PEER_RST_CLOSE_IDENTIFIED:
            /* Do nothing */
            break;
        default:
            BGP4_RESET_PEER_PEND_FLAG (pPeer, BGP4_PEER_INIT_PENDING);
            break;
    }

    /* If global admin status is down, all peers will be
     * brought down, so no need for de-init
     */

    /* If the GR capable peer is identified to be in close identified state
     * then do not reset the state */
    if (BGP4_GET_PEER_CURRENT_STATE (pPeer) != BGP4_PEER_RST_CLOSE_IDENTIFIED)
    {
        BGP4_RESET_PEER_CURRENT_STATE (pPeer);
    }
    Bgp4RibhInitPeerDeInitInfo (pPeer);

    if (BGP4_LOCAL_ADMIN_STATUS (BGP4_PEER_CXT_ID (pPeer)) == BGP4_ADMIN_DOWN)
    {
        if (BGP4_GET_PEER_CURRENT_STATE (pPeer) !=
            BGP4_PEER_RST_CLOSE_IDENTIFIED)
        {
            BGP4_SET_PEER_CURRENT_STATE (pPeer, BGP4_PEER_READY);
        }
    }
    else
    {
        BGP_PEER_NODE_CREATE (pPeerNode);
        if (pPeerNode == NULL)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Memory Allocation to handle "
                           "Peer Shutdown FAILED!!!\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeer))));
            gu4BgpDebugCnt[MAX_BGP_PEER_NODE_SIZING_ID]++;
            return BGP4_FAILURE;
        }
        pPeerNode->pPeer = pPeer;
        TMO_SLL_Add (BGP4_PEER_DEINIT_LIST (BGP4_PEER_CXT_ID (pPeer)),
                     &pPeerNode->TSNext);
        if (BGP4_GET_PEER_CURRENT_STATE (pPeer) !=
            BGP4_PEER_RST_CLOSE_IDENTIFIED)
        {
            BGP4_SET_PEER_CURRENT_STATE (pPeer, BGP4_PEER_DEINIT_INPROGRESS);
        }
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4RibhAddFeasibleRouteToUpdLists                        */
/* Description   : This function takes the new feasible route added to tree  */
/*                 as input and add this route to the Forwarding information */
/*                 base update list and Internal peer update list as needed. */
/* Input(s)      : Pointer to the feasible route (pRtProfile)                */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4RibhAddFeasibleRouteToUpdLists (tRouteProfile * pRtProfile)
{

    /* Just a precasious operation. */
    BGP4_RT_RESET_FLAG (pRtProfile, (BGP4_RT_WITHDRAWN |
                                     BGP4_RT_ADVT_NOTTO_EXTERNAL |
                                     BGP4_RT_ADVT_NOTTO_INTERNAL));

    /* Check for the aggregation policy. */
    Bgp4AggrProcessFeasibleRoute (pRtProfile, NULL);

    /* Add the route to FIB update list */
    Bgp4AddFeasRouteToFIBUpdList (pRtProfile, NULL);

    /* Add the route to Internal Peer Advertisement list */
    Bgp4AddFeasRouteToIntPeerAdvtList (pRtProfile, NULL);

    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Bgp4RibhAddFeasibleRouteToVrfUpdLists                     */
/* Description   : This function takes the new feasible route added to tree  */
/*                 as input. If the route is a non VPN route, then add this  */
/*                 route to the corresponding Forwarding information base    */
/*                 update list and Internal peer update list as needed.      */
/*                 If the route is a VPN route, then it adds this route to   */
/*                 to the VPN feaswdraw gloal list for VPN route processing  */
/* Input(s)      : Pointer to the feasible route (pRtProfile)                */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4RibhAddFeasibleRouteToVrfUpdLists (tRouteProfile * pRtProfile)
{
#ifdef L3VPN
    UINT4               u4AfiSafiMask;
#endif

#ifdef L3VPN
    u4AfiSafiMask = (UINT4) ((BGP4_RT_AFI_INFO (pRtProfile) <<
                              BGP4_TWO_BYTE_BITS) |
                             (BGP4_RT_SAFI_INFO (pRtProfile)));
    if (u4AfiSafiMask != CAP_MP_VPN4_UNICAST)
    {
#endif
        /* Route is not a vpnv4 route, hence add into default VRF table
         * and internal peer advertisement list
         */
        Bgp4RibhAddFeasibleRouteToUpdLists (pRtProfile);
        return (BGP4_SUCCESS);
#ifdef L3VPN
    }
    else
    {
        Bgp4RibhAddFeasRtToVpnv4List (pRtProfile);
    }
    return (BGP4_SUCCESS);
#endif
}

#ifdef L3VPN
/*****************************************************************************/
/* Function Name : Bgp4RibhAddFeasRtToVpnv4List                              */
/* Description   : This function takes the new feasible route added to tree  */
/*                 as input. this function adds this route to the VPN        */
/*                 feaswdraw gloal list for VPN route processing             */
/* Input(s)      : Pointer to the feasible route (pRtProfile)                */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4RibhAddFeasRtToVpnv4List (tRouteProfile * pRtProfile)
{

    /* Just a precasious operation. */
    BGP4_RT_RESET_FLAG (pRtProfile, (BGP4_RT_WITHDRAWN |
                                     BGP4_RT_ADVT_NOTTO_EXTERNAL |
                                     BGP4_RT_ADVT_NOTTO_INTERNAL));
    if (((BGP4_RT_PEER_ENTRY (pRtProfile) != NULL) &&
         (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRtProfile))
          == BGP4_VPN4_CE_PEER)) || (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID))
    {
        Bgp4AddFeasRouteToIntPeerAdvtList (pRtProfile, NULL);

        /* If route leaking is enabled allow the routes to be installed 
         * VPN-RIB */
        if (BGP4_ROUTE_LEAK_FLAG != BGP4_CLI_AFI_ENABLED)
        {
            return BGP4_SUCCESS;
        }
    }

    if (!(BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_IN_VPNV4_FEAS_WDRAW_LIST))
    {
        /* Check for the aggregation policy. */
        Bgp4AggrProcessFeasibleRoute (pRtProfile, NULL);
        /* Route is installed in VPN-RIB and must be processed for VRFs */
        BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_IN_VPNV4_FEAS_WDRAW_LIST);
        Bgp4DshLinkRouteToList (BGP4_VPN4_FEAS_WDRAW_LIST, pRtProfile, 0,
                                BGP4_VPN4_FEAS_WDRAW_LIST_INDEX);
    }
    return BGP4_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name : Bgp4RibhAddReplaceRouteToUpdLists                         */
/* Description   : This function takes the replacing route and replace route */
/*                 as input and add this replacing route to the Forwarding   */
/*                 information base update list and Internal peer update     */
/*                 list as needed.                                           */
/* Input(s)      : Pointer to the replacing route (pNewRoute)                */
/*                 Pointer to the old replaced route (pOldRoute)             */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4RibhAddReplaceRouteToUpdLists (tRouteProfile * pNewRoute,
                                   tRouteProfile * pOldRoute)
{

    BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                   BGP4_FDB_TRC, BGP4_MOD_NAME,
                   "\tReceived replacement route %s for the old route %s\r\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pNewRoute),
                                    BGP4_RT_AFI_INFO (pNewRoute)),
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pOldRoute),
                                    BGP4_RT_AFI_INFO (pOldRoute)));

    if (pNewRoute->pRtInfo != NULL)
    {
        BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tReceived replacement route with nexthop %s immediate nexthop %s "
                       "route flags 0x%x extended flags 0x%x\r\n",
                       Bgp4PrintIpAddr ((pNewRoute->pRtInfo->NextHopInfo.
                                         au1Address),
                                        BGP4_RT_AFI_INFO (pNewRoute)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pNewRoute),
                                        BGP4_RT_AFI_INFO (pNewRoute)),
                       pNewRoute->u4Flags, pNewRoute->u4ExtFlags);
    }

    if (pOldRoute->pRtInfo != NULL)
    {
        BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tOld route is with nexthop %s immediate nexthop %s "
                       "route flags 0x%x extended flags 0x%x\r\n",
                       Bgp4PrintIpAddr ((pOldRoute->pRtInfo->NextHopInfo.
                                         au1Address),
                                        BGP4_RT_AFI_INFO (pOldRoute)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pOldRoute),
                                        BGP4_RT_AFI_INFO (pOldRoute)),
                       pOldRoute->u4Flags, pOldRoute->u4ExtFlags);
    }
    /* Just a precasious operation. */
    BGP4_RT_RESET_FLAG (pNewRoute, (BGP4_RT_WITHDRAWN |
                                    BGP4_RT_ADVT_NOTTO_EXTERNAL |
                                    BGP4_RT_ADVT_NOTTO_INTERNAL));

    /* Check for the aggregation policy. */
    Bgp4AggrProcessFeasibleRoute (pNewRoute, pOldRoute);

    /* Now the replacement route needs to be added to the FIB update list
     * and Internal peers update send list. Verify whether any operation is
     * pending for the Old Route. If so, then handle the operation
     * accordingly. Always update the FIB list first and then the Internal peer
     * advt list. */

    if (BGP4CanRepRouteBeAddedToFIB (pNewRoute, pOldRoute) == BGP4_FALSE)
    {
        /* Next hop not identical. */
        BGP4_RT_SET_FLAG (pOldRoute, (BGP4_RT_WITHDRAWN |
                                      BGP4_RT_ADVT_NOTTO_EXTERNAL));
        Bgp4AddWithdRouteToFIBUpdList (pOldRoute);
        BGP4_RT_SET_EXT_FLAG (pOldRoute, BGP4_ROUTE_BEST_ROUTE_UPDATE);
        Bgp4RedSyncRouteInfo (pOldRoute);
        BGP4_RT_SET_FLAG (pNewRoute, BGP4_RT_NEWUPDATE_TOIP);
        Bgp4AddFeasRouteToFIBUpdList (pNewRoute, NULL);
        Bgp4RedSyncRouteInfo (pNewRoute);
    }
    else
    {
        Bgp4AddFeasRouteToFIBUpdList (pNewRoute, pOldRoute);
    }

    Bgp4AddFeasRouteToIntPeerAdvtList (pNewRoute, pOldRoute);

    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Bgp4RibhAddReplaceRouteToVrfUpdLists                      */
/* Description   : This function takes the replacing route and replace route */
/*                 as input. If the route is a non VPN route, then add this  */
/*                 replacing route to the Forwarding information base update */
/*                 list and Internal peer update list as needed.             */
/*                 If the route is a VPN route, then it calls another func   */
/*                 which does the replacement route handling                 */
/* Input(s)      : Pointer to the replacing route (pNewRoute)                */
/*                 Pointer to the old replaced route (pOldRoute)             */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4RibhAddReplaceRouteToVrfUpdLists (tRouteProfile * pNewRoute,
                                      tRouteProfile * pOldRoute)
{
#ifdef L3VPN
    UINT4               u4AfiSafiMask;
#endif

    /* Check whether the Replacement flag is set in the New route. Else
     * return. This check is to ensure that caller calls this routine
     * with proper intension to handle the replacement route. */
    if ((BGP4_RT_GET_FLAGS (pNewRoute) & BGP4_RT_REPLACEMENT) !=
        BGP4_RT_REPLACEMENT)
    {
        /* Replacement flag not set. Route will not be processed. Just
         * return. */
        BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                  "\tBgp4RibhAddReplaceRouteToVrfUpdLists() : Replacement "
                  "flag is not set.\n");
        return BGP4_FAILURE;
    }

    BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                   BGP4_FDB_TRC, BGP4_MOD_NAME,
                   "\tReceived VRF replacement route %s for the old route %s\r\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pNewRoute),
                                    BGP4_RT_AFI_INFO (pNewRoute)),
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pOldRoute),
                                    BGP4_RT_AFI_INFO (pOldRoute)));

    if (pNewRoute->pRtInfo != NULL)
    {
        BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tReceived VRF replacement route with nexthop %s immediate nexthop %s "
                       "route flags 0x%x extended flags 0x%x\r\n",
                       Bgp4PrintIpAddr ((pNewRoute->pRtInfo->NextHopInfo.
                                         au1Address),
                                        BGP4_RT_AFI_INFO (pNewRoute)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pNewRoute),
                                        BGP4_RT_AFI_INFO (pNewRoute)),
                       pNewRoute->u4Flags, pNewRoute->u4ExtFlags);
    }

    if (pOldRoute->pRtInfo != NULL)
    {
        BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tOld VRF route is with nexthop %s immediate nexthop %s "
                       "route flags 0x%x extended flags 0x%x\r\n",
                       Bgp4PrintIpAddr ((pOldRoute->pRtInfo->NextHopInfo.
                                         au1Address),
                                        BGP4_RT_AFI_INFO (pOldRoute)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pOldRoute),
                                        BGP4_RT_AFI_INFO (pOldRoute)),
                       pOldRoute->u4Flags, pOldRoute->u4ExtFlags);
    }
#ifdef L3VPN
    u4AfiSafiMask = (UINT4) ((BGP4_RT_AFI_INFO (pNewRoute) <<
                              BGP4_TWO_BYTE_BITS) |
                             (BGP4_RT_SAFI_INFO (pNewRoute)));
    if (u4AfiSafiMask != CAP_MP_VPN4_UNICAST)
    {
#endif
        /* This route is not vpnv4 route, hence carry out the normal
         * processing.
         */
        Bgp4RibhAddReplaceRouteToUpdLists (pNewRoute, pOldRoute);
        return BGP4_SUCCESS;
#ifdef L3VPN
    }
    else
    {
        Bgp4RibhAddReplaceRtToVpnv4List (pNewRoute, pOldRoute);
    }
    return (BGP4_SUCCESS);
#endif
}

#ifdef L3VPN
/*****************************************************************************/
/* Function Name : Bgp4RibhAddReplaceRtToVpnv4List                           */
/* Description   : This function takes the replacing route and replace route */
/*                 as input. Checks for in which VRFs the route needs to     */
/*                 replaced, and sets the flags accordingly and adds route   */
/*                 into VPN feaswdraw global list for further processing.    */
/* Input(s)      : Pointer to the replacing route (pNewRoute)                */
/*                 Pointer to the old replaced route (pOldRoute)             */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4RibhAddReplaceRtToVpnv4List (tRouteProfile * pNewRoute,
                                 tRouteProfile * pOldRoute)
{
    tRtInstallVrfInfo  *pOldRtVrfInfo = NULL;
    tRtInstallVrfInfo  *pNewRtVrfInfo = NULL;
    UINT1               u1VrfFound = BGP4_FALSE;
    UINT1               u1WithdrawnRtInVrf = 0;

    BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                   BGP4_FDB_TRC, BGP4_MOD_NAME,
                   "\tReceived VPN replacement route %s for the old route %s\r\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pNewRoute),
                                    BGP4_RT_AFI_INFO (pNewRoute)),
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pOldRoute),
                                    BGP4_RT_AFI_INFO (pOldRoute)));

    if (pNewRoute->pRtInfo != NULL)
    {
        BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tReceived VPN replacement route with nexthop %s immediate nexthop %s "
                       "route flags 0x%x extended flags 0x%x\r\n",
                       Bgp4PrintIpAddr ((pNewRoute->pRtInfo->NextHopInfo.
                                         au1Address),
                                        BGP4_RT_AFI_INFO (pNewRoute)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pNewRoute),
                                        BGP4_RT_AFI_INFO (pNewRoute)),
                       pNewRoute->u4Flags, pNewRoute->u4ExtFlags);
    }

    if (pOldRoute->pRtInfo != NULL)
    {
        BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tOld VPN route is with nexthop %s immediate nexthop %s "
                       "route flags 0x%x extended flags 0x%x\r\n",
                       Bgp4PrintIpAddr ((pOldRoute->pRtInfo->NextHopInfo.
                                         au1Address),
                                        BGP4_RT_AFI_INFO (pOldRoute)),
                       Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP (pOldRoute),
                                        BGP4_RT_AFI_INFO (pOldRoute)),
                       pOldRoute->u4Flags, pOldRoute->u4ExtFlags);
    }

    if ((BGP4_RT_PEER_ENTRY (pNewRoute) != NULL) &&
        (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pNewRoute))
         == BGP4_VPN4_CE_PEER))
    {
        Bgp4AddFeasRouteToIntPeerAdvtList (pNewRoute, NULL);
        return BGP4_SUCCESS;
    }
    BGP4_RT_SET_FLAG (pOldRoute, BGP4_RT_WITHDRAWN);
    /* Scan the old routes VRF installed informations and set the
     * flags accordingly
     */
    if (((BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) > 0) &&
         (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pNewRoute)) == 0)) ||
        ((BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) > 0) &&
         (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pOldRoute)) == 0)))
    {
        /* Speaker is a Route Reflector and ahs no vrfs */
        return BGP4_SUCCESS;
    }

    TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pOldRoute),
                  pOldRtVrfInfo, tRtInstallVrfInfo *)
    {
        u1VrfFound = BGP4_FALSE;
        TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pNewRoute),
                      pNewRtVrfInfo, tRtInstallVrfInfo *)
        {
            if (BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pOldRtVrfInfo) ==
                BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pNewRtVrfInfo))
            {
                /* VRF info is present in both old and new routes.
                 * Hence set the replacement flag for this VRF.
                 */
                u1VrfFound = BGP4_TRUE;
                BGP4_VPN4_RT_VRF_SET_FLAG (pOldRtVrfInfo,
                                           BGP4_RT_VRF_WITHDRAWN);
                BGP4_VPN4_RT_VRF_SET_FLAG (pNewRtVrfInfo,
                                           BGP4_RT_VRF_REPLACEMENT);
                u1WithdrawnRtInVrf++;
                break;
            }
        }
        if (u1VrfFound == BGP4_FALSE)
        {
            /* Old route must be removed from the VRF info */
            BGP4_VPN4_RT_VRF_SET_FLAG (pOldRtVrfInfo, BGP4_RT_VRF_WITHDRAWN);
            u1WithdrawnRtInVrf++;
        }
    }
    /* Add the old route for deletion from BGP-VRFs if required */
    if ((BGP4_RT_PROTOCOL (pOldRoute) == BGP_ID) &&
        (BGP4_GET_PEER_CURRENT_STATE (BGP4_RT_PEER_ENTRY (pOldRoute)) ==
         BGP4_PEER_DEINIT_INPROGRESS))
    {
        /* Peer is going down, hence remove this route from all the
         * VRFs
         */
        if (u1WithdrawnRtInVrf > 0)
        {
            BGP4_TRC_ARG2 (NULL,
                           BGP4_TRC_FLAG, BGP4_DETAIL_TRC, BGP4_MOD_NAME,
                           "\tCalling Bgp4Vpnv4DelRtFromVrfs from Func[%s] Line[%d]\n",
                           __func__, __LINE__);
            Bgp4Vpnv4DelRtFromVrfs (pOldRoute);
            Bgp4AddWithdRouteToFIBUpdList (pOldRoute);
            Bgp4AddWithdRouteToIntPeerAdvtList (pOldRoute);
        }
    }
    else
    {
        if (u1WithdrawnRtInVrf > 0)
        {
            if (!(BGP4_RT_GET_FLAGS (pOldRoute) &
                  BGP4_RT_IN_VPNV4_FEAS_WDRAW_LIST))
            {
                BGP4_RT_SET_FLAG (pOldRoute, BGP4_RT_IN_VPNV4_FEAS_WDRAW_LIST);
                Bgp4DshLinkRouteToList (BGP4_VPN4_FEAS_WDRAW_LIST,
                                        pOldRoute, 0,
                                        BGP4_VPN4_FEAS_WDRAW_LIST_INDEX);
            }
        }
    }

    if (!(BGP4_RT_GET_FLAGS (pNewRoute) & BGP4_RT_IN_VPNV4_FEAS_WDRAW_LIST))
    {
        BGP4_RT_SET_FLAG (pNewRoute, BGP4_RT_IN_VPNV4_FEAS_WDRAW_LIST);
        Bgp4DshLinkRouteToList (BGP4_VPN4_FEAS_WDRAW_LIST,
                                pNewRoute, 0, BGP4_VPN4_FEAS_WDRAW_LIST_INDEX);
    }
    return BGP4_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name : Bgp4RibhAddWithdRouteToUpdLists                           */
/* Description   : This function takes the route removed from the tree as    */
/*                 input and add this route to the Forwarding information    */
/*                 base update list and Internal peer advt list as needed.   */
/* Input(s)      : Pointer to the withdrawn route (pRtProfile)               */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4RibhAddWithdRouteToUpdLists (tRouteProfile * pRtProfile)
{

#ifdef EVPN_WANTED
    INT4                i4RetVal = 0;
#endif

    /* Set the BGP4_RT_WITHDRAWN flag */
    BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);

    /* Check for the aggregation policy. */
    Bgp4AggrProcessWithdrawnRoute (pRtProfile);

#ifdef EVPN_WANTED
    /* Removes the MAC learnt from Peer with this ESI */
    i4RetVal = Bgp4ProcessMacAddressWithdrawRoute (pRtProfile);
#endif

    /* First update the FIB update list and then update the internal peer
     * advt list. Dont change this order. */
    /* Add the withdrawn route to the FIB update list */
    Bgp4AddWithdRouteToFIBUpdList (pRtProfile);

    /* Add the withdrawn route to the Internal peers advertisement list */
    Bgp4AddWithdRouteToIntPeerAdvtList (pRtProfile);

#ifdef EVPN_WANTED
    UNUSED_PARAM (i4RetVal);
#endif

    return (BGP4_SUCCESS);

}

/*****************************************************************************/
/* Function Name : Bgp4RibhAddWithdRouteToVrfUpdLists                        */
/* Description   : This function takes the route removed from the tree as    */
/*                 input.If the route is non VPN route, then add this route  */
/*                 to the Forwarding information base update list and        */
/*                 Internal peer advt list as needed. If the route is a VPN  */
/*                 route then it calls another fun which does the withdrwal  */
/*                 processing for VRFs                                       */
/* Input(s)      : Pointer to the withdrawn route (pRtProfile)               */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4RibhAddWithdRouteToVrfUpdLists (tRouteProfile * pRtProfile)
{
#ifdef L3VPN
    UINT4               u4AfiSafiMask;

    u4AfiSafiMask = (UINT4) ((BGP4_RT_AFI_INFO (pRtProfile) <<
                              BGP4_TWO_BYTE_BITS) |
                             (BGP4_RT_SAFI_INFO (pRtProfile)));
    if (u4AfiSafiMask != CAP_MP_VPN4_UNICAST)
#endif
    {
        /* Route is not a vpnv4 route, hence add into default VRF table
         * and internal peer advertisement list
         */
        Bgp4RibhAddWithdRouteToUpdLists (pRtProfile);
        return (BGP4_SUCCESS);
    }
#ifdef L3VPN
    else
    {
        Bgp4RibhAddWithdRtToVpnv4List (pRtProfile);
    }
    return (BGP4_SUCCESS);
#endif
}

#ifdef L3VPN
/*****************************************************************************/
/* Function Name : Bgp4RibhAddWithdRtToVpnv4List                             */
/* Description   : This function takes the route removed from the tree as    */
/*                 input. Checks for in which VRFs the route needs to be     */
/*                 withdrawn and sets the flags accordinly and adds the route*/
/*                 in VPN feaswdraw global list for further processing.      */
/* Input(s)      : Pointer to the withdrawn route (pRtProfile)               */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4RibhAddWithdRtToVpnv4List (tRouteProfile * pRtProfile)
{
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
    UINT2               u4VrfCount = 0;

    BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);
    if (((BGP4_RT_PEER_ENTRY (pRtProfile) != NULL) &&
         (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRtProfile))
          == BGP4_VPN4_CE_PEER)) || (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID))
    {
        Bgp4AddWithdRouteToIntPeerAdvtList (pRtProfile);

        /* If route leaking is enabled allow the routes to be withdrawn from
         * VPN-RIB */
        if (BGP4_ROUTE_LEAK_FLAG != BGP4_CLI_AFI_ENABLED)
        {
            return BGP4_SUCCESS;
        }
    }

    /* This route is removed from VPN-RIB, so set the withdrawn flag 
     * in all the VRFs
     */
    if (!((BGP4_RR_CLIENT_CNT (BGP4_RT_CXT_ID (pRtProfile)) > 0) &&
          (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) == 0)))
    {
        /* Check if the router is Route Reflector and it doesn't have Vrfs.
         * Else the router has Vrf and set the flags*/
        TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile),
                      pRtVrfInfo, tRtInstallVrfInfo *)
        {
            BGP4_VPN4_RT_VRF_SET_FLAG (pRtVrfInfo, BGP4_RT_VRF_WITHDRAWN);
            if ((BGP4_VPN4_RT_VRF_GET_FLAG (pRtVrfInfo) &
                 BGP4_RT_PRESENT_IN_VRF) == BGP4_RT_PRESENT_IN_VRF)
            {
                u4VrfCount++;
            }
        }
    }
    if ((BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID) &&
        (BGP4_RT_PEER_ENTRY (pRtProfile) != NULL) &&
        (BGP4_GET_PEER_CURRENT_STATE (BGP4_RT_PEER_ENTRY (pRtProfile)) ==
         BGP4_PEER_DEINIT_INPROGRESS))
    {
        /* Peer is going down, hence remove this route from all the
         * VRFs
         */
        /* Check for the aggregation policy. */
        BGP4_TRC_ARG2 (NULL,
                       BGP4_TRC_FLAG, BGP4_DETAIL_TRC, BGP4_MOD_NAME,
                       "\tCalling Bgp4Vpnv4DelRtFromVrfs from Func[%s] Line[%d]\n",
                       __func__, __LINE__);
        Bgp4AggrProcessWithdrawnRoute (pRtProfile);
        Bgp4Vpnv4DelRtFromVrfs (pRtProfile);
        Bgp4AddWithdRouteToFIBUpdList (pRtProfile);
        Bgp4AddWithdRouteToIntPeerAdvtList (pRtProfile);
    }
    else
    {
        /* Route is installed in VPN-RIB and must be processed for VRFs */
        if (((!(BGP4_RT_GET_FLAGS (pRtProfile) &
                BGP4_RT_IN_VPNV4_FEAS_WDRAW_LIST)) && (u4VrfCount)) ||
            (BGP4_RR_CLIENT_CNT (BGP4_RT_CXT_ID (pRtProfile)) > 0))
        {
            /* Check for the aggregation policy. */
            Bgp4AggrProcessWithdrawnRoute (pRtProfile);
            BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_IN_VPNV4_FEAS_WDRAW_LIST);
            Bgp4DshLinkRouteToList (BGP4_VPN4_FEAS_WDRAW_LIST, pRtProfile, 0,
                                    BGP4_VPN4_FEAS_WDRAW_LIST_INDEX);
        }
        else if (((BGP4_RT_GET_FLAGS (pRtProfile) &
                   BGP4_RT_IN_VPNV4_FEAS_WDRAW_LIST)) && (!u4VrfCount))
        {
            /* Route is not present in any of the Vrfs. So if route
             * is present in Global list, remove it*/
            /* This happens when, 
             * the route has come for processing, meanwhile the VRF has gone
             * down, and this route has been removed, in such a case, we dont
             * have to process this route
             */
            Bgp4DshDelinkRouteFromList (BGP4_VPN4_FEAS_WDRAW_LIST, pRtProfile,
                                        BGP4_VPN4_FEAS_WDRAW_LIST_INDEX);
            BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_IN_VPNV4_FEAS_WDRAW_LIST);
        }
    }
    return BGP4_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name : Bgp4RibhProcessWithdRoute                                 */
/* Description   : This function process the given withdrawn route. IF RFD   */
/*                 is enabled then damping is performed over this route and  */
/*                 if the route is suppressed, the route is cleared from     */
/*                 peer's input list and no other action is done. Else if not*/
/*                 suppressed or no damping is enabled, then the route is    */
/*                 removed from the Local RIB/Peer's Input List and if it    */
/*                 happened to be the best route will be added in the        */
/*                 internal peer change list and Forwarding Information base */
/*                 update list.                                              */
/* Input(s)      : Peer from which the route is learnt (pPeer)               */
/*                 Pointer to the route profile to be withdrawn (pRtProfile) */
/*                 Pointer to the treenode holding this route profile        */
/*                 (pRibNode). It can either take the treenode pointer or    */
/*                 NULL.                                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4RibhProcessWithdRoute (tBgp4PeerEntry * pPeer,
                           tRouteProfile * pRtProfile, VOID *pRibNode)
{
    tTMO_SLL            TsTempFeasibleRouteList;
    tTMO_SLL            PrevMpRouteList;
    tLinkNode          *pLinkNode = NULL;
    tRouteProfile      *pPeerRoute = NULL;
    tRouteProfile      *pBestRoute = NULL;
    tRouteProfile      *pNewBestRoute = NULL;
    tRouteProfile      *pRoute = NULL;
    tRouteProfile      *pFndRtProfileList = NULL;
    INT4                i4RtFound;
#ifdef RFD_WANTED
    INT4                i4RetVal = BGP4_FAILURE;
#endif
#ifdef L3VPN
    UINT4               u4AsafiMask;
#endif
    tRouteProfile      *pRtMultipathPrev = NULL;
    tRouteProfile      *pRtTempRoute = NULL;
    TMO_SLL_Init (&TsTempFeasibleRouteList);
    TMO_SLL_Init (&PrevMpRouteList);

    /* Try and get the route from the RIB. If the route is present in the
     * RIB, then process the route. Else this route is just duplicate and dont
     * process it. */

    i4RtFound = Bgp4RibhLookupRtEntry (pRtProfile, BGP4_PEER_CXT_ID (pPeer),
                                       BGP4_TREE_FIND_EXACT,
                                       &pFndRtProfileList, &pRibNode);
    if (i4RtFound == BGP4_FAILURE)
    {
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tPEER %s - Matching Route not present in RIB "
                       "for the withdrawn route %s received\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeer))),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_SUCCESS;
    }
#ifdef L3VPN

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
    if ((u4AsafiMask == CAP_MP_VPN4_UNICAST) && (BGP4_PEER_CXT_ID (pPeer) != 0))
    {
        pPeerRoute =
            Bgp4DshGetPeerRtFromVrfProfileList (pFndRtProfileList, pPeer,
                                                BGP4_PEER_CXT_ID (pPeer));
    }
    else
    {
#endif
        pPeerRoute = Bgp4DshGetPeerRtFromProfileList (pFndRtProfileList, pPeer);
#ifdef L3VPN
    }
#endif
    if ((pPeerRoute == NULL) ||
        ((BGP4_RT_GET_FLAGS (pPeerRoute) & BGP4_RT_HISTORY) == BGP4_RT_HISTORY))
    {
        /* No matching route present in the RIB for this destination or
         * only the HISTORY of this Route is present. In either case there
         * is no need to process this route as withdrawn.
         */
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Matching Peer Route not present in RIB"
                       " for the withdrawn route %s received\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeer))),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_SUCCESS;
    }

    /* Get the Best route from this Node. */
    pBestRoute = Bgp4DshGetBestRouteFromProfileList (pFndRtProfileList,
                                                     BGP4_PEER_CXT_ID (pPeer));

#ifdef RFD_WANTED
    /* Applying the Damping to this withdrawn route. */
    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_NO_RFD) == BGP4_RT_NO_RFD)
    {
        /* Need to bypass RFD. Just reset this flag and continue further
         * processing. */
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_DAMP_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Damping is bypassed for the withdrawn route %s "
                       "received since damping is not enabled for the route.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeer))),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_NO_RFD);
    }
    else if ((BGP4_RT_GET_FLAGS (pPeerRoute) & BGP4_RT_NO_RFD) ==
             BGP4_RT_NO_RFD)
    {
        /* Need to bypass RFD. Just reset this flag and continue further
         * processing. */
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_DAMP_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Damping is bypassed for the withdrawn route %s"
                       " received since damping is not enabled for the route.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeer))),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pPeerRoute),
                                        BGP4_RT_AFI_INFO (pPeerRoute)));
        BGP4_RT_RESET_FLAG (pPeerRoute, BGP4_RT_NO_RFD);
    }
    else
    {
        i4RetVal = RfdRoutesClassifier (pPeer, pPeerRoute, NULL, BGP4_FALSE,
                                        pRibNode);
        if (i4RetVal == BGP4_FAILURE)
        {
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_DAMP_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - Handling dampening for the withdrawn route %s "
                           "received failed.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeer))),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pPeerRoute),
                                            BGP4_RT_AFI_INFO (pPeerRoute)));
        }

        /* Check whether the route is suppressed. If yes, then Remove the
         * suppressed route from the RIB. */
        if ((BGP4_RT_GET_FLAGS (pPeerRoute) & BGP4_RT_DAMPED) == BGP4_RT_DAMPED)
        {
            /* Route is suppressed. */
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_DAMP_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - Received withdrawn route %s is suppressed.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeer))),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pPeerRoute),
                                            BGP4_RT_AFI_INFO (pPeerRoute)));
            Bgp4RibhDelRtEntry (pPeerRoute, pRibNode, BGP4_PEER_CXT_ID (pPeer),
                                BGP4_TRUE);
            return (BGP4_SUCCESS);
        }
    }
#endif

    if (BGP4_IS_ROUTE_VALID (pPeerRoute) == BGP4_FALSE)
    {
        /* Route is in RIB and is not a valid route. Just clear the route
         * from the RIB and if present in Overlap list then remove it from
         * Overlap list. 
         */
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tPEER %s - Withdrawn Route %s received is not a valid"
                       "route. Removing it from the RIB\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO (pPeer))),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pPeerRoute),
                                        BGP4_RT_AFI_INFO (pPeerRoute)));
        if ((BGP4_RT_GET_FLAGS (pPeerRoute) & BGP4_RT_OVERLAP) ==
            BGP4_RT_OVERLAP)
        {
            BGP4_RT_RESET_FLAG (pPeerRoute, BGP4_RT_OVERLAP);
            Bgp4DshRemoveRouteFromList (BGP4_OVERLAP_ROUTE_LIST
                                        (BGP4_PEER_CXT_ID (pPeer)), pPeerRoute);
        }
        /* Reset the Invalid Flags for the old route. */
        BGP4_RT_SET_FLAG (pPeerRoute, BGP4_RT_WITHDRAWN);
        Bgp4RibhDelRtEntry (pPeerRoute, pRibNode, BGP4_PEER_CXT_ID (pPeer),
                            BGP4_TRUE);
        BGP4_RT_RESET_FLAG (pPeerRoute, BGP4_RT_INVALID_ROUTE_FLAG);
        return (BGP4_SUCCESS);
    }

    /* The Route is Valid route and present in RIB. Now remove the route from
     * the RIB and either advertise it as withdrawn or if any replacement route
     * exists, then advertise the replacement route as withdrawn. Set the
     * RT_WITHDRAWN flag to indiacte it. */
    BGP4_RT_SET_FLAG (pPeerRoute, BGP4_RT_WITHDRAWN);
    if (BGP4_RT_NEXT (pFndRtProfileList) == NULL)
    {
        /* No alternative route is present in tree for this
         * withdrawn route. */
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tPEER %s - Withdrawn Route %s received is a valid route."
                       " Removing it from the RIB and advertising it as withdrawn route\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeer))),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pPeerRoute),
                                        BGP4_RT_AFI_INFO (pPeerRoute)));
        Bgp4RibhAddWithdRouteToVrfUpdLists (pPeerRoute);
        if (BGP4_OVERLAP_ROUTE_POLICY (BGP4_PEER_CXT_ID (pPeer)) !=
            BGP4_INSTALL_BOTH_RT)
        {
            /* Overlapping route policy is not INSTALL_BOTH. So removing
             * a route can cause some other routes to be selected as best
             * route. */

            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                           BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_FDB_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - Overlap policy is set for the peer."
                           " Hence getting routes from overlap list\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeer))));

            Bgp4DshGetDeniedRoutesFromOverlapList (&TsTempFeasibleRouteList,
                                                   pPeerRoute,
                                                   BGP4_OVERLAP_ROUTE_POLICY
                                                   (BGP4_PEER_CXT_ID (pPeer)));
        }
        Bgp4RibhDelRtEntry (pPeerRoute, pRibNode, BGP4_PEER_CXT_ID (pPeer),
                            BGP4_FALSE);
    }
    else
    {
        /*Route to be removed is best route */
        if (pPeerRoute == pBestRoute)
        {
            BgpFormPrevMultipathLists (pFndRtProfileList, &PrevMpRouteList);
        }

        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tPEER %s - Withdrawn Route %s to be removed from the RIB "
                       "is the best route\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO (pPeer))),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pPeerRoute),
                                        BGP4_RT_AFI_INFO (pPeerRoute)));

        BGP4_RT_REF_COUNT (pPeerRoute)++;
        Bgp4RibhDelRtEntry (pPeerRoute, pRibNode, BGP4_PEER_CXT_ID (pPeer),
                            BGP4_FALSE);

        if (pPeerRoute == pBestRoute)
        {
            /* The Peer route removed is the Best Route. Check whether
             * any alternate best route exists or not. If yes, the
             * process it. */
            pRibNode = NULL;
            i4RtFound =
                Bgp4RibhLookupRtEntry (pPeerRoute, BGP4_PEER_CXT_ID (pPeer),
                                       BGP4_TREE_FIND_EXACT, &pFndRtProfileList,
                                       &pRibNode);
            if (pFndRtProfileList != NULL)
            {
                pNewBestRoute =
                    Bgp4DshGetBestRouteFromProfileList (pFndRtProfileList,
                                                        BGP4_PEER_CXT_ID
                                                        (pPeer));
                if (pNewBestRoute == NULL)
                {
                    /* Only Invalid Routes are present in the RIB. No other
                     * valid alternate route is present for this route. So 
                     * process the route for withdrawal. */

                    BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                   BGP4_TRC_FLAG,
                                   BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC |
                                   BGP4_FDB_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s - No other valid alternative route is present "
                                   "for the best route %s. So route is processed for withdrawal\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeer),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeer))),
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pPeerRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pPeerRoute)));

                    Bgp4RibhAddWithdRouteToVrfUpdLists (pPeerRoute);
                    if (BGP4_OVERLAP_ROUTE_POLICY (BGP4_PEER_CXT_ID (pPeer)) !=
                        BGP4_INSTALL_BOTH_RT)
                    {
                        /* Overlapping route policy is not INSTALL_BOTH. So
                         * removing a route can cause some other routes to be
                         * selected as best route. */
                        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                       BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC | BGP4_FDB_TRC,
                                       BGP4_MOD_NAME,
                                       "\tPEER %s - Overlap policy is set for the peer."
                                       " Hence getting routes from overlap list\n",
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeer),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeer))));

                        Bgp4DshGetDeniedRoutesFromOverlapList
                            (&TsTempFeasibleRouteList,
                             pPeerRoute,
                             BGP4_OVERLAP_ROUTE_POLICY (BGP4_PEER_CXT_ID
                                                        (pPeer)));
                    }
                }
                else if (pBestRoute != pNewBestRoute)
                {
                    /* Peer route is best route and this is removed.
                     * Alternate route is present for this withdrawn route
                     * Need to send this routes as replacement route. */

                    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                   BGP4_TRC_FLAG,
                                   BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC |
                                   BGP4_FDB_TRC, BGP4_MOD_NAME,
                                   "\tPEER %s - Alternative route %s is present for the withdrawn route %s."
                                   " Processing that route as new best route\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeer),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeer))),
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pNewBestRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pNewBestRoute)),
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pBestRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pBestRoute)));

                    BGP4_RT_SET_FLAG (pNewBestRoute, BGP4_RT_REPLACEMENT);
                    if (Bgp4AhCanRouteBeAdvtWithdrawn (pNewBestRoute,
                                                       pBestRoute) == BGP4_TRUE)
                    {
                        BGP4_RT_SET_FLAG (pNewBestRoute,
                                          BGP4_RT_ADVT_WITHDRAWN);
                        /* Reset the old routes ADVT_WITHDRAWN flag. */
                        BGP4_RT_RESET_FLAG (pBestRoute, BGP4_RT_ADVT_WITHDRAWN);
                    }

                    Bgp4RibhAddReplaceRouteToVrfUpdLists (pNewBestRoute,
                                                          pPeerRoute);
                    /* Since the old non-best routes have become Best route,
                     * update the RCVD_PA_ENTRY accordingly. */
                    Bgp4RcvdPADBAddRoute (pNewBestRoute, BGP4_TRUE);

                    /* Run Multipath selection logic */
                    BgpFormMultipathLists (pNewBestRoute);
                    Bgp4RibhAddMultipathRouteToVrfUpdLists (pNewBestRoute,
                                                            &PrevMpRouteList);
                }
            }
            BgpReleasePrevMpList (&PrevMpRouteList);
        }
        else
        {
            /* If multipath bit is set, Remove that route from RTM, 
             * Set as withdrawn and add  to FIB List to withdraw from RTM */
            if (((BGP4_RT_GET_EXT_FLAGS (pPeerRoute) & BGP4_RT_MULTIPATH) ==
                 BGP4_RT_MULTIPATH)
                && ((BGP4_RT_GET_EXT_FLAGS (pPeerRoute) & BGP4_RT_IN_RTM) ==
                    BGP4_RT_IN_RTM))
            {
                BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                               BGP4_TRC_FLAG,
                               BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC |
                               BGP4_FDB_TRC, BGP4_MOD_NAME,
                               "\tPEER %s - Multipath bit is set for the withdrawn route %s. "
                               "Adding the route to FIB update list as withdrawn to withdraw from RTM.\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeer))),
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pPeerRoute),
                                                BGP4_RT_AFI_INFO (pPeerRoute)));

                /*Recalulate new multipath list */
                Bgp4AddWithdRouteToFIBUpdList (pPeerRoute);
                if (pBestRoute != NULL)
                {
                    pRtMultipathPrev = pBestRoute;
                    pRtTempRoute = pBestRoute->pMultiPathRtNext;
                    while ((pRtTempRoute != pPeerRoute)
                           && (pRtTempRoute != NULL))
                    {
                        pRtMultipathPrev = pRtTempRoute;
                        pRtTempRoute = pRtTempRoute->pMultiPathRtNext;
                    }

                    if (pRtTempRoute == pPeerRoute)
                    {
                        pRtMultipathPrev->pMultiPathRtNext =
                            pPeerRoute->pMultiPathRtNext;
                        BgpFormPrevMultipathLists (pBestRoute,
                                                   &PrevMpRouteList);
                        /* Resetting previous multipath */
                        pBestRoute->pMultiPathRtNext = NULL;
                        BgpFormMultipathLists (pBestRoute);
                        Bgp4RibhAddMultipathRouteToVrfUpdLists (pBestRoute,
                                                                &PrevMpRouteList);
                    }
                }
            }
#ifdef EVPN_WANTED
            else if (pRtProfile->EvpnNlriInfo.u1RouteType ==
                     EVPN_ETH_SEGMENT_ROUTE)
            {
                BGP4_RT_SET_EXT_FLAG (pRtProfile, BGP4_RT_IN_FIB_UPD_LIST);
                Bgp4AddWithdRouteToFIBUpdList (pPeerRoute);
            }
#endif
        }
        Bgp4DshReleaseRtInfo (pPeerRoute);
    }

    /* If TsTempFeasibleRouteList is not empty, then these routes needs
     * to be added to the peer rececived route list/Redistribution list.
     * So that they get advertised. Add these routes to the beginning of
     * the list, so that these routes are processed first. */
    TMO_SLL_Scan (&TsTempFeasibleRouteList, pLinkNode, tLinkNode *)
    {
        pRoute = pLinkNode->pRouteProfile;

        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC |
                       BGP4_PEER_CON_TRC, BGP4_MOD_NAME,
                       "\tPEER %s - Received feasible route list from Overlap route list "
                       "after route is withdrawn. Processing the feasible route %s\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO (pPeer))),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRoute),
                                        BGP4_RT_AFI_INFO (pRoute)));

        /* Reset the RT_WITHDRAWN flag to ensure that the route
         * are processed as feasible. */
        BGP4_RT_RESET_FLAG (pRoute, BGP4_RT_WITHDRAWN);
        Bgp4DshDelinkRouteFromChgList (pRoute);
        if (BGP4_RT_PROTOCOL (pRoute) == BGP_ID)
        {
#ifdef RFD_WANTED
            BGP4_RT_SET_FLAG (pRoute, BGP4_RT_NO_RFD);
#endif
            Bgp4RibhProcessFeasibleRoute (BGP4_RT_PEER_ENTRY (pRoute), pRoute);

        }
        else
        {
            Bgp4IgphProcessFeasibleImportRoute (pRoute);
        }
    }
    Bgp4DshReleaseList (&TsTempFeasibleRouteList, 0);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4RibhProcessFeasibleRoute                              */
/* Description   : This function process the given feasible route. IF RFD    */
/*                 is enabled then damping is performed over this route and  */
/*                 if the route is suppressed no action is done. Else if not */
/*                 suppressed or no damping is enabled, then the route is    */
/*                 processed and if not rejected will get installed in the   */
/*                 Local RIB. And if selected as the best route this route   */
/*                 will be added to the internal peer change list and        */
/*                 forwarding information base update list.                  */
/* Input(s)      : Peer from which the route is learnt (pPeer)               */
/*                 Pointer to the feasible route profile (pRtProfile)        */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4RibhProcessFeasibleRoute (tBgp4PeerEntry * pPeer,
                              tRouteProfile * pRtProfile)
{
    tTMO_SLL            TsRouteList;
    tTMO_SLL            TsWithdrawnRouteList;
    tTMO_SLL            PrevMpRouteList;
    tAddrPrefix         RemAddr;
    tNetAddress         LocalAddr;
    tRouteProfile      *pFndRtProfileList = NULL;
    tRouteProfile      *pTmpFndRtProfileList = NULL;
    VOID               *pRibNode = NULL;
    tLinkNode          *pLinkNode = NULL;
    tRouteProfile      *pPeerRoute = NULL;
    tRouteProfile      *pBestRoute = NULL;
    tRouteProfile      *pNewBestRoute = NULL;
    tRouteProfile      *pRoute = NULL;
    tRouteProfile      *pRtMultipathPrev = NULL;
    tRouteProfile      *pRtTempRoute = NULL;
    tBgp4Info          *pBgpInfo = NULL;
    tAddrPrefix         InvPrefix;
    tAddrPrefix         RtNexthop;
    UINT4               u4IfIndex = BGP4_INVALID_IFINDX;
    UINT4               u4NewRtPeerId = 0;
    UINT4               u4OldRtPeerId = 0;
    UINT4               u4IsOldRtValid = BGP4_FALSE;
#if defined (VPLSADS_WANTED) || defined (L3VPN) || defined (EVPN_WANTED)
    UINT4               u4AsafiMask;
#endif
    INT4                i4Metric = BGP4_INVALID_NH_METRIC;
    INT4                i4Status;
    INT4                i4NextHopResolvable = BGP4_FALSE;
    INT4                i4IsNextHopMetricChange = BGP4_FALSE;
    INT4                i4RtFound;
    INT4                i4Ret;
    UINT2               u2AddrFamily;
    BOOL1               bGrFlag = BGP4_FALSE;
    INT4                i4RetVal = 0;

    UNUSED_PARAM (i4RetVal);
#ifdef VPLSADS_WANTED
    /*The whole processing of L2VPN-VPLS route is done here */
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
#ifdef MPLS_WANTED
    if (u4AsafiMask == CAP_MP_L2VPN_VPLS)
    {
        if (Bgp4VplsIsRouteCanBeProcessed (pRtProfile) == BGP4_FAILURE)
        {
            return BGP4_FAILURE;
        }
    }

    BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                   BGP4_TRC_FLAG, BGP4_VPLS_TRC, BGP4_MOD_NAME,
                   "\tPEER %s :<L2VPN, VPLS> Route received"
                   "NLRI - [%s]\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                     (pPeer))),
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)));

#endif

#endif
#ifdef L3VPN
    /* Check whethe the route can be installed.
     * If the route is destined to some VRF which is in meantime deleted,
     * so there is no point in adding the route here
     */
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
    if ((u4AsafiMask == CAP_MP_VPN4_UNICAST) &&
        (Bgp4Vpnv4IsRouteCanBeProcessed (pRtProfile) == BGP4_FAILURE))
    {
        return (BGP4_SUCCESS);
    }
#endif
#ifdef EVPN_WANTED
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
    if (u4AsafiMask == CAP_MP_L2VPN_EVPN)
    {
        if (Bgp4IsEvpnRouteCanBeProcessed (pRtProfile) == BGP4_FAILURE)
        {
            return (BGP4_SUCCESS);
        }

        else
        {
            if (pRtProfile->EvpnNlriInfo.u1RouteType == EVPN_MAC_ROUTE)
            {
                if (Bgp4CheckandProcessMacDup (pRtProfile) == BGP4_FAILURE)
                {
                    return BGP4_SUCCESS;
                }
            }
        }
    }
#endif
    TMO_SLL_Init (&TsRouteList);
    TMO_SLL_Init (&TsWithdrawnRouteList);

#ifdef VPLSADS_WANTED
    /*InvPrefix is get checked against Next-hop, for VPLS ,AFI=IPv4 */
    if (u4AsafiMask == CAP_MP_L2VPN_VPLS)
    {
        u2AddrFamily = BGP4_INET_AFI_IPV4;
    }
    else
    {
#endif

        u2AddrFamily = BGP4_RT_AFI_INFO (pRtProfile);

#ifdef VPLSADS_WANTED
    }
#endif
    Bgp4InitAddrPrefixStruct (&(InvPrefix), u2AddrFamily);

    i4Ret = Bgp4GetPeerNetworkAddress (pPeer, BGP4_RT_AFI_INFO (pRtProfile),
                                       &RemAddr);
    if (i4Ret == BGP4_FAILURE)
    {
        /* Network Address in not available for the peer. */
        Bgp4CopyAddrPrefixStruct (&RemAddr, BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
    }

    /* When the local peer address is recieved as a BGP route ,  that route
     * should be skipped from processing. Hence at Standby , this check has
     * no effect since the active itself would have skipped this  route and
     * hence no sync message will be sent for that route to standby */
    if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
    {

        i4Ret = Bgp4GetLocalAddrForPeer (pPeer, RemAddr, &LocalAddr, BGP4_TRUE);
        if (i4Ret == BGP4_FAILURE)
        {
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tLocal Peer Address not available for the route %s."
                           " Skipping the route from proccessing.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            return BGP4_FAILURE;
        }

        if ((PrefixMatch
             (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
              (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
              LocalAddr.NetAddr)) == BGP4_TRUE)
        {
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC | BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                           "\tLocal Peer Address is received as a BGP route %s."
                           " Skipping the route from proccessing.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            return (BGP4_FAILURE);
        }
    }
    i4RtFound = Bgp4RibhLookupRtEntry (pRtProfile, BGP4_RT_CXT_ID (pRtProfile),
                                       BGP4_TREE_FIND_EXACT,
                                       &pFndRtProfileList, &pRibNode);
    if (i4RtFound == BGP4_SUCCESS)
    {
        pPeerRoute = Bgp4DshGetPeerRtFromProfileList (pFndRtProfileList, pPeer);
        if (pPeerRoute != NULL)
        {
            u4IsOldRtValid = BGP4_IS_ROUTE_VALID (pPeerRoute);
        }
    }

#ifdef RFD_WANTED
    /* Applying the Damping to this feasible route. If the
     * BGP4_RT_NO_RFD flag is set then the RFD processing should be
     * by-passed. Ensure that this flag is resetted. */
    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_NO_RFD) == BGP4_RT_NO_RFD)
    {
        /* Need to bypass RFD. Just reset this flag and continue further
         * processing. */
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                       BGP4_CONTROL_PATH_TRC | BGP4_DAMP_TRC, BGP4_MOD_NAME,
                       "\tDamping is not enable for the route %s. Hence bypassing dampening.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_NO_RFD);
    }
    else
    {
        i4Ret = RfdRoutesClassifier (pPeer, pRtProfile, pPeerRoute, BGP4_TRUE,
                                     pRibNode);
        if (i4Ret == BGP4_FAILURE)
        {
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_DAMP_TRC, BGP4_MOD_NAME,
                           "\tHandling of Feasible route %s failed in RFD \n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
        }

        /* Check whether the route is suppressed. */
        if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_DAMPED) == BGP4_RT_DAMPED)
        {
            /* Feasible route got suppressed, need to process this route
             * as withdrawn because the route may be a replacement route.
             */
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_DAMP_TRC, BGP4_MOD_NAME,
                           "\tReceived feasible route %s is suppressed.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            if (pPeerRoute != NULL)
            {
                /* Route is already present. Delete the existing route. */
                if ((BGP4_RT_GET_FLAGS (pPeerRoute) & BGP4_RT_HISTORY) ==
                    BGP4_RT_HISTORY)
                {
                    /* Route History is present in RIB. Just delete this 
                     * route from RIB. */
                    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_DAMP_TRC | BGP4_EVENTS_TRC,
                                   BGP4_MOD_NAME,
                                   "\tRoute history is already present in the RIB for the "
                                   "suppressed route %s. Deleting the existing route from RIB.\n",
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pRtProfile)));
                    BGP4_RT_RESET_FLAG (pPeerRoute, BGP4_RT_HISTORY);
                    Bgp4RibhDelRtEntry (pPeerRoute, pRibNode,
                                        BGP4_PEER_CXT_ID (pPeer), BGP4_HISTORY);
                }
                else
                {
                    BGP4_RT_SET_FLAG (pPeerRoute,
                                      (BGP4_RT_NO_RFD | BGP4_RT_WITHDRAWN));
                    Bgp4RibhProcessWithdRoute (pPeer, pPeerRoute, pRibNode);
                }
            }

            /* Add the suppressed Feasible route to RIB and return. */
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_DAMP_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                           "\tAdding the suppressed feasible route %s mask %s in the RIB.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                                            (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            Bgp4RibhAddRtEntry (pRtProfile, BGP4_PEER_CXT_ID (pPeer));
            return (BGP4_SUCCESS);
        }
    }
#endif

    /* Process the received Feasible route. */
    BGP4_DBG2 (BGP4_DBG_CTRL_FLOW,
               "\tBgp4RibhProcessFeasibleRoute() : Config Lp = %d,Med = %d\n",
               BGP4_RT_LOCAL_PREF (pRtProfile), BGP4_RT_MED (pRtProfile));

    pBgpInfo = BGP4_RT_BGP_INFO (pRtProfile);

    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_NH_METRIC_CHANGE) ==
        BGP4_RT_NH_METRIC_CHANGE)
    {
        i4IsNextHopMetricChange = BGP4_TRUE;
        BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_NH_METRIC_CHANGE);
    }

    /* Check for Nexthop reachability, and
     * Fill the immediate IGP nexthop to the NextHop BGP router */

    if ((BGP4_GET_NODE_STATUS == RM_STANDBY) &&
        ((BGP4_RT_GET_RT_IF_INDEX (pRtProfile) != BGP4_INVALID_IFINDX)
         || ((INT4) BGP4_RT_GET_RT_IF_INDEX (pRtProfile) != BGP4_INV_INDEX)))
    {
        /* In standby , explicit nexthop resolution need not be done
         * for the incoming routes from active, as the routes have
         * already been resolved for next-hop at Active. 
         * Hence retain the same Interface index and metric value as 
         * synced by the active card */
        i4NextHopResolvable = BGP4_TRUE;
        Bgp4InitAddrPrefixStruct (&(RtNexthop), u2AddrFamily);
        u4IfIndex = BGP4_RT_GET_RT_IF_INDEX (pRtProfile);
        i4Metric = BGP4_RT_NH_METRIC (pRtProfile);
    }
    else
    {
        i4NextHopResolvable =
            Bgp4ProcessNextHopForResolution (pRtProfile,
                                             &RtNexthop, &i4Metric, &u4IfIndex);
    }

    if ((i4NextHopResolvable == BGP4_FALSE) ||
        (i4NextHopResolvable == BGP4_ENTRYCREATE_FAILURE))
    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC | BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                       "\tNexthop is not resolved for the route %s. It is unknown.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_NEXT_HOP_UNKNOWN);
    }
    else if (i4NextHopResolvable == BGP4_TRUE)
    {
        BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_NEXT_HOP_UNKNOWN);

    }

    else
    {
        BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_NEXT_HOP_UNKNOWN);
    }

    /* Update the route profile with the received value. If the next hop
     * is unknown then these values will be resetted. */
    if ((PrefixMatch (RtNexthop, InvPrefix)) == BGP4_TRUE)
    {
        if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
        {
            /* Next hop is attached to the same subnet. */
            Bgp4CopyAddrPrefixStruct (&
                                      (BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                       (pRtProfile)),
                                      BGP4_INFO_NEXTHOP_INFO (pBgpInfo));
        }
        /* If the node is Standby and Immediate Next Hop information is Invalid
         * as from Active, only then update Immediate next Hop information 
         * at standby with Next hop from the RouteInfo*/
        else if ((PrefixMatch ((BGP4_RT_IMMEDIATE_NEXTHOP_INFO (pRtProfile)),
                               InvPrefix)) == BGP4_TRUE)
        {
            /* Next hop is attached to the same subnet. */
            Bgp4CopyAddrPrefixStruct (&
                                      (BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                       (pRtProfile)),
                                      BGP4_INFO_NEXTHOP_INFO (pBgpInfo));
        }
    }
    else
    {
        Bgp4CopyAddrPrefixStruct (&
                                  (BGP4_RT_IMMEDIATE_NEXTHOP_INFO (pRtProfile)),
                                  RtNexthop);
    }
    BGP4_RT_NH_METRIC (pRtProfile) = i4Metric;
    BGP4_RT_SET_RT_IF_INDEX (pRtProfile, u4IfIndex);

    if ((pPeerRoute != NULL) &&
        (BGP4_RT_GET_FLAGS (pPeerRoute) & BGP4_RT_HISTORY) == BGP4_RT_HISTORY)
    {
        /* HISTORY Route is present in RIB. Delete this route from RIB.
         * Also update the pFndRtProfileList & pRibNode */

        if (pFndRtProfileList == pPeerRoute)
        {
            /* Since the peer route is deleted, update the pFndRtProfileList
             * and pRibNode.
             * !!! CAUTION !!!: Currently deleting a route from RIB,
             * will cause the DECISION Process to be executed again. But
             * since the first route itself is INVALID any route following
             * it will also be INVALID and there will be no change in the
             * order of the route position. So we are assigning the next route
             * to pPeerRoute as pFndRtProfileList. If this assumption is
             * changed then this part of the code also should be updated.
             */
            pFndRtProfileList = BGP4_RT_NEXT (pPeerRoute);
        }
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tRoute history is already present in the RIB for the "
                       "route %s. Deleting the existing route from RIB.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        BGP4_RT_RESET_FLAG (pPeerRoute, BGP4_RT_HISTORY);
        Bgp4RibhDelRtEntry (pPeerRoute, pRibNode,
                            BGP4_RT_CXT_ID (pRtProfile), BGP4_HISTORY);
        /* Reset pPeerRoute as NULL, to indicate that the Peer route
         * is not present in RIB. */
        pPeerRoute = NULL;
        if (pFndRtProfileList == NULL)
        {
            pRibNode = NULL;
        }
    }

    if ((pPeerRoute != NULL) &&
        (u4IsOldRtValid == BGP4_FALSE) &&
        (BGP4_IS_ROUTE_VALID (pRtProfile) == BGP4_FALSE))
    {
        /* Both Old Route and New Route are invalid. So just delete
         * the old route from RIB and add the new route to the RIB. */
        BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                  "\tBgp4RibhProcessFeasibleRoute() : Route present in "
                  "FIB and is invalid. \n");
        /* Remove the old route found in the RIB & Overlap List */
        if ((BGP4_RT_GET_FLAGS (pPeerRoute) & BGP4_RT_OVERLAP)
            == BGP4_RT_OVERLAP)
        {
            BGP4_RT_RESET_FLAG (pPeerRoute, BGP4_RT_OVERLAP);
            Bgp4DshRemoveRouteFromList (BGP4_OVERLAP_ROUTE_LIST
                                        (BGP4_RT_CXT_ID (pRtProfile)),
                                        pPeerRoute);
        }

        /* Reset the Invalid Flags for the old route. */
        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tBoth the Old route %s and new feasible route %s are invalid. "
                       "Deleting the existing route from RIB.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pPeerRoute),
                                        BGP4_RT_AFI_INFO (pPeerRoute)),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        BGP4_RT_RESET_FLAG (pPeerRoute, BGP4_RT_INVALID_ROUTE_FLAG);
        Bgp4RibhDelRtEntry (pPeerRoute, pRibNode, BGP4_RT_CXT_ID (pRtProfile),
                            BGP4_TRUE);

        /* Received new feasible route is also invalid. Adding it to
         * the RIB. */
        if (i4NextHopResolvable != BGP4_ENTRYCREATE_FAILURE)
        {
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                           "\tAdding the new feasible route %s mask %s which is also invalid to RIB.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                                            (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            i4RetVal =
                Bgp4RibhAddRtEntry (pRtProfile, BGP4_RT_CXT_ID (pRtProfile));
            return (BGP4_SUCCESS);
        }
        else
        {
            /* Unable to add the New route to the RIB. */
#ifdef RFD_WANTED
            if (BGP4_RT_DAMP_HIST (pRtProfile) != NULL)
            {
                /* Remove the route from REUSE list if it is present. */
                RfdRemoveRtFromRtsReuseList (pRtProfile,
                                             BGP4_RT_DAMP_HIST (pRtProfile));
                Bgp4MemReleaseRfdDampHist (BGP4_RT_DAMP_HIST (pRtProfile));
                BGP4_RT_DAMP_HIST (pRtProfile) = NULL;
                Bgp4DshReleaseRtInfo (pRtProfile);
            }
#endif
            return (BGP4_FAILURE);
        }
    }

    /* Install the received route in the RIB based on the overlapping 
     * policy */
    if (BGP4_OVERLAP_ROUTE_POLICY (BGP4_RT_CXT_ID (pRtProfile)) !=
        BGP4_INSTALL_BOTH_RT)
    {
        /* Install more/less-specific routes */
        if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_DAMPED) != BGP4_RT_DAMPED)
        {
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tOverlap policy is configured for the feasible route %s.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            Bgp4RibhProcessOverlapFeasRoutes (pRtProfile,
                                              &pTmpFndRtProfileList,
                                              &TsWithdrawnRouteList);

            /* TsWithdrawnRouteList contains the list of best route that 
             * overlaps with the new feasible route and have become
             * invalid. These route needs to be withdrawn from FIB and 
             * sent to Peers as withdrawn routes. */
            TMO_SLL_Scan (&TsWithdrawnRouteList, pLinkNode, tLinkNode *)
            {
                pRoute = pLinkNode->pRouteProfile;
                Bgp4RibhAddWithdRouteToVrfUpdLists (pRoute);
                /* Since these routes were best route earlier and now
                 * they have become Non-Best route, update the RCVD_PA_ENTRY
                 * accordingly. */
                Bgp4RcvdPADBAddRoute (pRoute, BGP4_FALSE);
            }
            Bgp4DshReleaseList (&TsWithdrawnRouteList, 0);
        }
    }

    if ((pPeerRoute == NULL) &&
        (BGP4_IS_ROUTE_VALID (pRtProfile) == BGP4_FALSE))
    {
        /* Peer route does not exist earlier and the new route is also
         * invalid. So just add the new route to RIB and return. */
        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tThere is no any old route and the received feasible route %s "
                       "is also invalid. Hence just adding the new route to the RIB.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                                        (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        i4RetVal = Bgp4RibhAddRtEntry (pRtProfile, BGP4_RT_CXT_ID (pRtProfile));
        return (BGP4_SUCCESS);
    }
    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_OVERLAP) == BGP4_RT_OVERLAP)
    {
        /* The new route is suppressed because of the overlap policy. This
         * means old route if present must also have been suppressed and the
         * invalid old route if present has to been removed. 
         */
        if ((pPeerRoute != NULL) && (u4IsOldRtValid == BGP4_FALSE))
        {
            /* Remove the invalid old route found in the RIB & Overlap List */
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                           "\tThe old route %s mask %s is invalid. Hence ",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pPeerRoute),
                                            BGP4_RT_AFI_INFO (pPeerRoute)),
                           Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                                            (pPeerRoute),
                                            BGP4_RT_AFI_INFO (pPeerRoute)));
            if ((BGP4_RT_GET_FLAGS (pPeerRoute) & BGP4_RT_OVERLAP) ==
                BGP4_RT_OVERLAP)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                          BGP4_FDB_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                          "removing the route from overlap list and ");
                BGP4_RT_RESET_FLAG (pPeerRoute, BGP4_RT_OVERLAP);
                Bgp4DshRemoveRouteFromList (BGP4_OVERLAP_ROUTE_LIST
                                            (BGP4_RT_CXT_ID (pRtProfile)),
                                            pPeerRoute);
            }

            /* Reset the Invalid Flags for the old route. */
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                      BGP4_FDB_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "removing the route from RIB.\n");
            BGP4_RT_RESET_FLAG (pPeerRoute, BGP4_RT_INVALID_ROUTE_FLAG);
            Bgp4RibhDelRtEntry (pPeerRoute, pRibNode,
                                BGP4_RT_CXT_ID (pRtProfile), BGP4_TRUE);
        }

        /* Also add the new route to the RIB and overlap List. */
        if (i4NextHopResolvable != BGP4_ENTRYCREATE_FAILURE)
        {
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                           "\tThe New feasible route %s mask %s is suppressed and valid. "
                           "Hence adding the route to RIB and overlap list.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                                            (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            i4RetVal =
                Bgp4RibhAddRtEntry (pRtProfile, BGP4_RT_CXT_ID (pRtProfile));
            Bgp4DshAddRouteToOverlapList (pRtProfile);
            return (BGP4_SUCCESS);
        }
        else
        {
            /* Unable to add the route the RIB. */
#ifdef RFD_WANTED
            if (BGP4_RT_DAMP_HIST (pRtProfile) != NULL)
            {
                /* Remove the route from REUSE list if it is present. */
                BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_DAMP_TRC | BGP4_ALL_FAILURE_TRC,
                               BGP4_MOD_NAME,
                               "\tFailed to add the new feasible route %s to RIB."
                               " Hence removing the route from reuse list.\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)));
                RfdRemoveRtFromRtsReuseList (pRtProfile,
                                             BGP4_RT_DAMP_HIST (pRtProfile));
                Bgp4MemReleaseRfdDampHist (BGP4_RT_DAMP_HIST (pRtProfile));
                BGP4_RT_DAMP_HIST (pRtProfile) = NULL;
                Bgp4DshReleaseRtInfo (pRtProfile);
            }
#endif
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC | BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                           "\tFailed to add the new feasible route %s to RIB.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_OVERLAP);
            return (BGP4_FAILURE);
        }
    }

    if (pFndRtProfileList != NULL)
    {
        /* Matching route is already present in the RIB. */
        pBestRoute = Bgp4DshGetBestRouteFromProfileList (pFndRtProfileList,
                                                         BGP4_RT_CXT_ID
                                                         (pRtProfile));
        if (pBestRoute != NULL)
        {
            if (BGP4_RT_PROTOCOL (pBestRoute) == BGP_ID)
            {
                u4OldRtPeerId =
                    BGP4_PEER_BGP_ID (BGP4_RT_PEER_ENTRY (pBestRoute));
            }
            else
            {
                u4OldRtPeerId = 0;
            }
        }
        BGP4_DBG1 (BGP4_DBG_CTRL_FLOW,
                   "\rValue of Old Route peer Id is %d\n", u4OldRtPeerId);

        /* Check whether the peer route is present or not. */
        if (pPeerRoute != NULL)
        {
            /* Peer route is already present in the RIB */
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tThe Received route %s is a replacement route. "
                           "Matching route is already present in the RIB.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            if ((u4IsOldRtValid == (UINT4) BGP4_IS_ROUTE_VALID (pRtProfile)) &&
                (i4IsNextHopMetricChange == BGP4_FALSE) &&
                (BGP4_RT_MED (pRtProfile) == BGP4_RT_MED (pPeerRoute)) &&
                (BGP4_RT_LOCAL_PREF (pRtProfile) ==
                 BGP4_RT_LOCAL_PREF (pPeerRoute)) &&
#ifdef RFD_WANTED
                (BGP4_RT_DAMP_HIST (pRtProfile) == NULL) &&
#endif
                (Bgp4AttrIsBgpinfosIdentical (pBgpInfo,
                                              BGP4_RT_BGP_INFO
                                              (pPeerRoute)) == TRUE))
            {
#ifdef VPLSADS_WANTED
                /* If everything is same then dont add to Rib */
                if (u4AsafiMask == CAP_MP_L2VPN_VPLS)
                {
                    if (Bgp4IsSameVplsRoute (pPeer, pRtProfile,
                                             pPeerRoute) == TRUE)
                    {
                        if ((BGP4_RT_GET_FLAGS (pPeerRoute) &
                             BGP4_RT_STALE) == BGP4_RT_STALE)
                        {
                            BGP4_RT_RESET_FLAG (pPeerRoute, BGP4_RT_STALE);
                        }
                        else
                        {
                            return BGP4_SUCCESS;
                        }
                    }
                }
                else
                {
#endif
#ifdef EVPN_WANTED
                    if (u4AsafiMask == CAP_MP_L2VPN_EVPN)
                    {
                        if (Bgp4IsSameEvpnRoute (pRtProfile,
                                                 pPeerRoute) == TRUE)
                        {
                            if ((BGP4_RT_GET_FLAGS (pPeerRoute) &
                                 BGP4_RT_STALE) == BGP4_RT_STALE)
                            {
                                BGP4_RT_RESET_FLAG (pPeerRoute, BGP4_RT_STALE);
                            }
                            else
                            {
                                return BGP4_SUCCESS;
                            }
                        }
                    }
#endif
#ifdef L3VPN
                    /*Check if the Label is same */
                    if ((Bgp4IsSameBgpLabel (pRtProfile, pPeerRoute)) == TRUE)
#endif
                    {
#ifdef L3VPN
                        if ((u4AsafiMask == CAP_MP_VPN4_UNICAST) &&
                            (Bgp4Vpnv4IsSameVrfInfo (pRtProfile,
                                                     pPeerRoute) == TRUE))
#endif
                        {
                            /* Received new route is identical to the existing 
                             * route. RFD if enabled also stores the Old route 
                             * information. So no need for any action. 
                             */
                            if (Bgp4GRCheckRestartMode
                                (BGP4_PEER_CXT_ID (pPeer)) ==
                                BGP4_RECEIVING_MODE)
                            {
                                /* The peer is restarting. Hence revert the stale bit set for
                                 * the peer routes if it is already preserved */
                                if ((BGP4_RT_GET_FLAGS (pPeerRoute) &
                                     BGP4_RT_STALE) == BGP4_RT_STALE)
                                {
                                    if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
                                    {
                                        bGrFlag = BGP4_TRUE;
                                    }
                                    BGP4_RT_RESET_FLAG (pPeerRoute,
                                                        BGP4_RT_STALE);
                                    if (BGP4_PEER_CURRENT_PREFIX_LIMIT (pPeer) >
                                        0)
                                    {
                                        BGP4_PEER_CURRENT_PREFIX_LIMIT (pPeer)
                                            -= 1;
                                    }
                                }
                            }
                            else if (BGP4_GET_NODE_STATUS == RM_STANDBY)
                            {
                                /* Route information already synced. Ext. flags
                                 * modification can cause route to be synced again.
                                 * Hence, update extflag information alone */
                                BGP4_RT_GET_EXT_FLAGS (pPeerRoute) =
                                    BGP4_RT_GET_EXT_FLAGS (pRtProfile);
                            }
                            if (bGrFlag != BGP4_TRUE)
                            {
                                BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                               BGP4_CONTROL_PATH_TRC |
                                               BGP4_FDB_TRC, BGP4_MOD_NAME,
                                               "\tThe Replacement route %s is identical to existing route.\n",
                                               Bgp4PrintIpAddr
                                               (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)));
                                return (BGP4_SUCCESS);
                            }
                        }
                    }
#ifdef VPLSADS_WANTED
                }
#endif
            }

            BGP4_RT_REF_COUNT (pPeerRoute)++;
            if (BGP4_RT_NEXT (pFndRtProfileList) == NULL)
            {
                /* There is only one route present in the RIB matching the
                 * incoming route and this route is the old route from peer.
                 */
                if (BGP4_IS_ROUTE_VALID (pRtProfile) == BGP4_FALSE)
                {
                    /* New Route is not Valid and is not usable. So
                     * remove the old route from RIB and since this
                     * is the only route, and if this route is the
                     * best route then add it to the FIB update
                     * list and Internal Peers Advt list. */
                    if (u4IsOldRtValid == BGP4_TRUE)
                    {
                        Bgp4RibhAddWithdRouteToVrfUpdLists (pPeerRoute);
                        Bgp4RibhDelRtEntry (pPeerRoute, pRibNode,
                                            BGP4_RT_CXT_ID (pRtProfile),
                                            BGP4_FALSE);
                    }
                    else
                    {
                        BGP4_RT_RESET_FLAG (pPeerRoute,
                                            BGP4_RT_INVALID_ROUTE_FLAG);
                        Bgp4RibhDelRtEntry (pPeerRoute, pRibNode,
                                            BGP4_RT_CXT_ID (pRtProfile),
                                            BGP4_TRUE);
                    }

                    /* Also add the new route to RIB. */
                    BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_FDB_TRC | BGP4_EVENTS_TRC,
                                   BGP4_MOD_NAME,
                                   "\tThe New feasible route %s mask %s is invalid. Hence removed the "
                                   "old route from RIB and adding the new route to RIB.\n",
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pRtProfile)),
                                   Bgp4PrintIpMask ((UINT1)
                                                    BGP4_RT_PREFIXLEN
                                                    (pRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pRtProfile)));
                    i4RetVal =
                        Bgp4RibhAddRtEntry (pRtProfile,
                                            BGP4_RT_CXT_ID (pRtProfile));
                    Bgp4DshReleaseRtInfo (pPeerRoute);
                    return (BGP4_SUCCESS);
                }

                /* New route is a Valid route and should be treated as
                 * a replacement route. Add the replacement route to the
                 * FIB update list and Internal Peer Advertise list. Also
                 * delete the old route from the RIB. */
                BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_REPLACEMENT);
                BGP4_RT_REF_COUNT (pRtProfile)++;
                if (u4IsOldRtValid == BGP4_TRUE)
                {
                    Bgp4RibhDelRtEntry (pPeerRoute, pRibNode,
                                        BGP4_RT_CXT_ID (pRtProfile),
                                        BGP4_FALSE);
                }
                else
                {
                    BGP4_RT_RESET_FLAG (pPeerRoute, BGP4_RT_INVALID_ROUTE_FLAG);
                    Bgp4RibhDelRtEntry (pPeerRoute, pRibNode,
                                        BGP4_RT_CXT_ID (pRtProfile), BGP4_TRUE);
                }
                BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_FDB_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                               "\tThe New feasible route %s mask %s is valid. Hence removed the "
                               "old route from RIB and adding the new route to RIB.\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                                                (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)));
                i4Status =
                    Bgp4RibhAddRtEntry (pRtProfile,
                                        BGP4_RT_CXT_ID (pRtProfile));
                if (i4Status == BGP4_FAILURE)
                {
                    BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                                   BGP4_FDB_TRC | BGP4_CONTROL_PATH_TRC,
                                   BGP4_MOD_NAME,
                                   "\tAdding Route %s Mask %s to RIB failed\n",
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pRtProfile)),
                                   Bgp4PrintIpMask ((UINT1)
                                                    BGP4_RT_PREFIXLEN
                                                    (pRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pRtProfile)));
                    BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_REPLACEMENT);
                    if (u4IsOldRtValid == BGP4_TRUE)
                    {
                        Bgp4RibhAddWithdRouteToVrfUpdLists (pPeerRoute);
                    }
                    Bgp4DshReleaseRtInfo (pRtProfile);
                    Bgp4DshReleaseRtInfo (pPeerRoute);
                    return BGP4_FAILURE;
                }
                Bgp4RibhAddReplaceRouteToVrfUpdLists (pRtProfile, pPeerRoute);
                Bgp4DshReleaseRtInfo (pRtProfile);
                Bgp4DshReleaseRtInfo (pPeerRoute);
                return (BGP4_SUCCESS);
            }
            else
            {
                /* There are more routes present in the LOCAL RIB for
                 * the same destination. Remove the route from the
                 * Local RIB. */
                BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_FDB_TRC, BGP4_MOD_NAME,
                               "\tThere are more routes present in the LOCAL RIB for "
                               "the same destination %s\n.",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)));
                Bgp4DshAddRouteToList (&TsRouteList, pPeerRoute, 0);
                if (u4IsOldRtValid == BGP4_TRUE)
                {
                    Bgp4RibhDelRtEntry (pPeerRoute, pRibNode,
                                        BGP4_RT_CXT_ID (pRtProfile),
                                        BGP4_FALSE);
                }
                else
                {
                    /* Reset the Invalid Flags for the old route. */
                    BGP4_RT_RESET_FLAG (pPeerRoute, BGP4_RT_INVALID_ROUTE_FLAG);
                    Bgp4RibhDelRtEntry (pPeerRoute, pRibNode,
                                        BGP4_RT_CXT_ID (pRtProfile), BGP4_TRUE);
                }
                BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_FDB_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                               "\tRemoved the old route %s mask %s from RIB.\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pPeerRoute),
                                                BGP4_RT_AFI_INFO (pPeerRoute)),
                               Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                                                (pPeerRoute),
                                                BGP4_RT_AFI_INFO (pPeerRoute)));
                if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_NEXT_HOP_UNKNOWN)
                    == BGP4_RT_NEXT_HOP_UNKNOWN)
                {
                    if ((BGP4_RT_GET_EXT_FLAGS (pPeerRoute) & BGP4_RT_IN_RTM) ==
                        BGP4_RT_IN_RTM)
                    {
                        /* If the route is available in RTM, need to remove it from RTM */
                        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC | BGP4_FDB_TRC |
                                       BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                                       "\tThe route %s ia available in RTM. Hence removing the "
                                       "route from RTM.\n",
                                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                        (pPeerRoute),
                                                        BGP4_RT_AFI_INFO
                                                        (pPeerRoute)));
                        BGP4_RT_SET_FLAG (pPeerRoute, BGP4_RT_WITHDRAWN);
                        Bgp4IgphUpdateRouteInFIB (pPeerRoute);
                        BGP4_RT_RESET_FLAG (pPeerRoute, BGP4_RT_WITHDRAWN);
                    }
                    /* Also add the new route to RIB. */
                    BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_FDB_TRC | BGP4_EVENTS_TRC,
                                   BGP4_MOD_NAME,
                                   "\tAdding the new feasible route %s mask %s to RIB.\n",
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pRtProfile)),
                                   Bgp4PrintIpMask ((UINT1)
                                                    BGP4_RT_PREFIXLEN
                                                    (pRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pRtProfile)));
                    i4RetVal =
                        Bgp4RibhAddRtEntry (pRtProfile,
                                            BGP4_RT_CXT_ID (pRtProfile));
                }

            }
        }

        /* Now the pRtProfile is either a REPLACEMENT route or new feasible
         * route. If it is a replacement route then the old route has 
         * already been removed from the LOCAL RIB. So either pRtProfile
         * is a REPLACEMENT or new feasible route, the only operation left
         * to be done is adding this route to the LOCAL RIB and if selected
         * as best route, then this has to be added the FIB update list and
         * Internal peers update list. */
        if (BGP4_IS_ROUTE_VALID (pRtProfile) == BGP4_TRUE)
        {
            /* Next-Hop for the route is resolvable and the route is Valid */
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                           "\tThe Next hop is resolvable for the new feasible route and "
                           "it is valid. Adding the route %s mask %s to RIB.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                                            (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            i4Status =
                Bgp4RibhAddRtEntry (pRtProfile, BGP4_RT_CXT_ID (pRtProfile));
            if (i4Status == BGP4_FAILURE)
            {
                /* Adding to tree fails. */
                BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                               BGP4_FDB_TRC | BGP4_CONTROL_PATH_TRC,
                               BGP4_MOD_NAME,
                               "\tAdding Route %s Mask %s to RIB failed.\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               Bgp4PrintIpMask ((UINT1)
                                                BGP4_RT_PREFIXLEN (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)));
            }
        }

        pRibNode = NULL;
        i4RtFound =
            Bgp4RibhLookupRtEntry (pRtProfile, BGP4_RT_CXT_ID (pRtProfile),
                                   BGP4_TREE_FIND_EXACT, &pFndRtProfileList,
                                   &pRibNode);

        if ((i4RtFound == BGP4_FAILURE))
        {
            /* This condition is possible only if adding new route to RIB
             * have failed. Check whether the old route exists. If yes,
             * then process the old route as withdrawn route. */
            if (pPeerRoute != NULL)
            {
                if (u4IsOldRtValid == BGP4_TRUE)
                {
                    Bgp4RibhAddWithdRouteToVrfUpdLists (pPeerRoute);
                }

                Bgp4DshReleaseList (&TsRouteList, 0);
                Bgp4DshReleaseRtInfo (pPeerRoute);
            }
            return (BGP4_FAILURE);
        }

        pNewBestRoute = Bgp4DshGetBestRouteFromProfileList (pFndRtProfileList,
                                                            BGP4_RT_CXT_ID
                                                            (pRtProfile));
        if (((pBestRoute != pNewBestRoute) && (pNewBestRoute != NULL)) &&
            (Bgp4RibhIsRouteIdentical (pBestRoute, pNewBestRoute) !=
             BGP4_SUCCESS))
        {
            if (BGP4_RT_PROTOCOL (pNewBestRoute) == BGP_ID)
            {
                u4NewRtPeerId =
                    BGP4_PEER_BGP_ID (BGP4_RT_PEER_ENTRY (pNewBestRoute));
            }
            else
            {
                u4NewRtPeerId = 0;
            }
            BGP4_DBG1 (BGP4_DBG_CTRL_FLOW,
                       "\rValue of New Route peer Id is %d\n", u4NewRtPeerId);
            if (BGP4_RT_PROTOCOL (pNewBestRoute) == BGP_ID)
            {
                if (pBestRoute != NULL)
                {
                    if (Bgp4AhCanRouteBeAdvtWithdrawn (pNewBestRoute,
                                                       pBestRoute) == BGP4_TRUE)
                    {
                        BGP4_RT_SET_FLAG (pNewBestRoute,
                                          BGP4_RT_ADVT_WITHDRAWN);
                        /* Reset the old routes ADVT_WITHDRAWN flag. */
                        BGP4_RT_RESET_FLAG (pBestRoute, BGP4_RT_ADVT_WITHDRAWN);
                    }
                }
            }
            if (pBestRoute != NULL)
            {
                i4NextHopResolvable =
                    Bgp4ProcessNextHopForResolution (pRtProfile,
                                                     &RtNexthop, &i4Metric,
                                                     &u4IfIndex);
                if (i4NextHopResolvable == BGP4_TRUE)
                {
                    BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_FDB_TRC | BGP4_EVENTS_TRC,
                                   BGP4_MOD_NAME,
                                   "\tThe Best route has been changed. Updating the new best route "
                                   "%s mask %s to FIB update list.\n",
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pNewBestRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pNewBestRoute)),
                                   Bgp4PrintIpMask ((UINT1)
                                                    BGP4_RT_PREFIXLEN
                                                    (pNewBestRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pNewBestRoute)));
                    BGP4_RT_SET_FLAG (pNewBestRoute, BGP4_RT_REPLACEMENT);
                    Bgp4RibhAddReplaceRouteToVrfUpdLists (pNewBestRoute,
                                                          pBestRoute);
                    if (pBestRoute != pPeerRoute)
                    {
                        /* Since the old best routes have become Non-Best route
                         * but still present in RIB. So update the
                         * RCVD_PA_ENTRY accordingly. */
                        Bgp4RcvdPADBAddRoute (pBestRoute, BGP4_FALSE);
                    }
                    if (pNewBestRoute != pRtProfile)
                    {
                        /* Best Route has been changed. But the new route is not
                         * the Best route. Means on adding the new route to the
                         * RIB, the old Best route was removed and some other
                         * non-Best route has now become the best route. So
                         * update teh RCVD_PA_ENTRY for this new best route. */
                        Bgp4RcvdPADBAddRoute (pNewBestRoute, BGP4_TRUE);
                    }
                }
                /* Prepare previous Multipath list */
                BgpFormPrevMultipathLists (pBestRoute, &PrevMpRouteList);

                /* Run Multipath selection logic */
                BgpFormMultipathLists (pNewBestRoute);
                Bgp4RibhAddMultipathRouteToVrfUpdLists (pNewBestRoute,
                                                        &PrevMpRouteList);
            }
            else
            {
                /* pBestRoute is NULL. Means there is no best route
                 * avaiable to this destination earlier. So just
                 * add the pNewBestRoute to the FIB update list
                 * and Internal Peer update List */
                BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_FDB_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                               "\tNo best route available for this destination earlier. "
                               "Updating the new best route %s mask %s to FIB update list.\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                (pNewBestRoute),
                                                BGP4_RT_AFI_INFO
                                                (pNewBestRoute)),
                               Bgp4PrintIpMask ((UINT1)
                                                BGP4_RT_PREFIXLEN
                                                (pNewBestRoute),
                                                BGP4_RT_AFI_INFO
                                                (pNewBestRoute)));
                Bgp4RibhAddFeasibleRouteToVrfUpdLists (pNewBestRoute);
            }
        }

        if ((pBestRoute == pNewBestRoute) && (pBestRoute != NULL))
        {
            /*Best route is not changed. Check and set multipath bit */
            /* Add new route to Multipath list if necessary */
            if (BgpCheckForMultipath (pBestRoute, pRtProfile) == BGP4_SUCCESS)
            {
                pRtMultipathPrev = pBestRoute;
                pRtTempRoute = BGP4_RT_NEXT (pBestRoute);
                /*Scan to find previous multipath route */
                while ((pRtTempRoute != pRtProfile) && (pRtTempRoute != NULL))
                {
                    if ((BGP4_RT_GET_EXT_FLAGS (pRtTempRoute) &
                         BGP4_RT_MULTIPATH) == BGP4_RT_MULTIPATH)
                    {
                        pRtMultipathPrev = pRtTempRoute;
                    }
                    pRtTempRoute = BGP4_RT_NEXT (pRtTempRoute);
                }
                pRtProfile->pMultiPathRtNext =
                    pRtMultipathPrev->pMultiPathRtNext;
                pRtMultipathPrev->pMultiPathRtNext = pRtProfile;
                BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_FDB_TRC, BGP4_MOD_NAME,
                               "\tThe Received new route %s is added to the multipath list.\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)));
                BGP4_RT_SET_EXT_FLAG (pRtProfile, BGP4_RT_MULTIPATH);
                if ((BGP4_RT_GET_FLAGS (pBestRoute) & BGP4_RT_AGGREGATED) ==
                    BGP4_RT_AGGREGATED)
                {
                    BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_AGGR_ROUTE);
                }
                else
                {
                    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_FDB_TRC, BGP4_MOD_NAME,
                                   "\tThe Received new route %s is added to the FIB update list.\n",
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pRtProfile)));
                    Bgp4AddFeasRouteToFIBUpdList (pRtProfile, NULL);
                }
            }

#ifdef EVPN_WANTED
            else if ((pRtProfile->EvpnNlriInfo.u1RouteType ==
                      EVPN_ETH_SEGMENT_ROUTE)
                     && (BGP4_IS_ROUTE_VALID (pRtProfile) == BGP4_TRUE))
            {
                BGP4_RT_SET_EXT_FLAG (pRtProfile, BGP4_RT_IN_FIB_UPD_LIST);
                Bgp4AddFeasRouteToFIBUpdList (pRtProfile, NULL);
            }
#endif
            else
            {
                /* Check for the aggregation policy. */
                Bgp4AggrProcessFeasibleRoute (pRtProfile, NULL);
            }

        }
        if (pPeerRoute != NULL)
        {
            Bgp4DshReleaseRtInfo (pPeerRoute);
        }
    }
    else
    {
        /* Route for this destination does not exist earlier. So just
         * update the RIB with the new route and add the route to FIB/UPDATE
         * Lists if the route is valid. */
        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tRoute for this destination does not exist earlier. "
                       "So Adding the route %s mask %s to RIB.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)),
                       Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                                        (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        i4Status = Bgp4RibhAddRtEntry (pRtProfile, BGP4_RT_CXT_ID (pRtProfile));

        if (i4Status == BGP4_FAILURE)
        {
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                           BGP4_UPD_MSG_TRC | BGP4_CONTROL_PATH_TRC,
                           BGP4_MOD_NAME,
                           "\tAdding Route %s Mask %s to RIB failed.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpMask ((UINT1)
                                            BGP4_RT_PREFIXLEN (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            Bgp4DshReleaseList (&TsRouteList, 0);
            return BGP4_FAILURE;
        }
        if (BGP4_IS_ROUTE_VALID (pRtProfile) == BGP4_TRUE)
        {
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                           "\tUpdating the route %s mask %s to FIB update list.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                                            (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            Bgp4RibhAddFeasibleRouteToVrfUpdLists (pRtProfile);
        }
    }
    Bgp4DshReleaseList (&TsRouteList, 0);
    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Bgp4RibhProcessOverlapFeasRoutes                          */
/* Description   : This function does the following actions:                 */
/*                 Check for the presence of an Overlapping route(more/less  */
/*                 specific) to the given route. If found, then depanding on */
/*                 the configured overlap policy, decides whether the route  */
/*                 can the installed in the RIB or not. If received route    */
/*                 cannot be installed, then the overlap flag is set. Else   */
/*                 if the route can be installed, then the current route is  */
/*                 removed from the RIB and added to the overlap route list  */
/* Input(s)      : (pRtProfile) - The new feasible route                     */
/* Output(s)     : List of new withdrawn routes (pNewWithdrawnList)          */
/*                 (pRtProfileList)- The updated route profile pointing to   */
/*                 the route matching the input route. If not matching route */
/*                 exists, then this value will be NULL.                     */
/* Return        : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4RibhProcessOverlapFeasRoutes (tRouteProfile * pRtProfile,
                                  tRouteProfile ** pRtProfileList,
                                  tTMO_SLL * pNewWithdrawnList)
{
    tRouteProfile      *pFndProfileList = NULL;
    tRouteProfile      *pBestRoute = NULL;
    tRouteProfile      *pNextRtProfileList = NULL;
    tRouteProfile      *pCurRoute = NULL;
    UINT4               u4DeleteNode = BGP4_FALSE;
    VOID               *pRibNode = NULL;
    VOID               *pNextRibNode = NULL;
    UINT4               u4Index;
    INT4                i4Found = BGP4_FAILURE;
    INT4                i4NextFound = BGP4_FAILURE;
    UINT1               u1RefPrefixLen = 0;

    *pRtProfileList = NULL;

    i4Found = Bgp4RibhLookupRtEntry (pRtProfile, BGP4_RT_CXT_ID (pRtProfile),
                                     BGP4_TREE_FIND,
                                     &pFndProfileList, &pRibNode);
    if (i4Found == BGP4_FAILURE)
    {
        /* No overlapping route is available for the given route. */
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC | BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                       "\tNo overlap route is available for the given route %s.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_FAILURE;
    }
    pCurRoute = Bgp4DshGetFirstRouteFromProfileList (pFndProfileList);

    if (pCurRoute == NULL)
    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC | BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                       "\tNo overlap route is available for the given route %s.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_FAILURE;
    }

    if ((Bgp4GetAfiSafiIndex (BGP4_RT_AFI_INFO (pCurRoute),
                              BGP4_RT_SAFI_INFO (pCurRoute),
                              &u4Index)) == BGP4_FAILURE)
    {
        /* No <AFI, SAFI> support for this family. */
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                  BGP4_FDB_TRC | BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                  "\tNo address family support for the route found.\n");
        return BGP4_FAILURE;
    }

    for (;;)
    {
        u4DeleteNode = BGP4_FALSE;
        if ((pCurRoute != NULL) &&
            (*pRtProfileList == NULL) &&
            ((AddrMatch (pCurRoute->NetAddress.NetAddr,
                         pRtProfile->NetAddress.NetAddr,
                         BGP4_RT_PREFIXLEN (pRtProfile)) == BGP4_TRUE)))
        {
            /* Update the RouteProfileList Pointer */
            *pRtProfileList = pFndProfileList;
        }

        while (pCurRoute)
        {
            /* Update the NextRibNode pointer. */
            pNextRibNode = pRibNode;
            i4NextFound =
                Bgp4RibhGetNextEntry (pCurRoute, BGP4_RT_CXT_ID (pRtProfile),
                                      &pNextRtProfileList, &pNextRibNode, TRUE);

            u1RefPrefixLen = (BGP4_RT_PREFIXLEN (pRtProfile) <
                              BGP4_RT_PREFIXLEN (pCurRoute))
                ? (UINT1) BGP4_RT_PREFIXLEN (pRtProfile)
                : (UINT1) BGP4_RT_PREFIXLEN (pCurRoute);
            if ((AddrGreaterThan (pCurRoute->NetAddress.NetAddr,
                                  pRtProfile->NetAddress.NetAddr,
                                  u1RefPrefixLen) == BGP4_TRUE) ||
                (AddrGreaterThan (pRtProfile->NetAddress.NetAddr,
                                  pCurRoute->NetAddress.NetAddr,
                                  u1RefPrefixLen) == BGP4_TRUE))
            {
                /* No more overlapping route is present in RIB for the given
                 * route */
                pCurRoute = BGP4_RT_NEXT (pCurRoute);
                continue;
            }

            if ((BGP4_RT_GET_FLAGS (pCurRoute) & BGP4_RT_HISTORY) !=
                BGP4_RT_HISTORY)
            {
                /* Check for Overlap Policy. */
                if (BGP4_OVERLAP_ROUTE_POLICY (BGP4_RT_CXT_ID (pRtProfile)) ==
                    BGP4_INSTALL_LESSSPEC_RT)

                {
                    if (BGP4_RT_PREFIXLEN (pRtProfile) >
                        BGP4_RT_PREFIXLEN (pCurRoute))
                    {
                        BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC | BGP4_FDB_TRC,
                                       BGP4_MOD_NAME,
                                       "\tOverlap route found for the received route %s with prefix %s "
                                       " prefix length %d and overlap policy %s.\n",
                                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                        (pRtProfile),
                                                        BGP4_RT_AFI_INFO
                                                        (pRtProfile)),
                                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                        (pCurRoute),
                                                        BGP4_RT_AFI_INFO
                                                        (pCurRoute)),
                                       BGP4_RT_PREFIXLEN (pCurRoute),
                                       Bgp4PrintCodeName
                                       (BGP4_INSTALL_LESSSPEC_RT,
                                        BGP4_OVERLAP_POLICY_CODE_NAME));
                        BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_OVERLAP);
                        Bgp4DshAddRouteToOverlapList (pRtProfile);
                    }
                    if (BGP4_RT_PREFIXLEN (pRtProfile) <
                        BGP4_RT_PREFIXLEN (pCurRoute))
                    {
                        u4DeleteNode = BGP4_TRUE;
                    }
                }
                else if (BGP4_OVERLAP_ROUTE_POLICY (BGP4_RT_CXT_ID (pRtProfile))
                         == BGP4_INSTALL_MORESPEC_RT)
                {
                    if (BGP4_RT_PREFIXLEN (pRtProfile) <
                        BGP4_RT_PREFIXLEN (pCurRoute))
                    {
                        BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC | BGP4_FDB_TRC,
                                       BGP4_MOD_NAME,
                                       "\tOverlap route found for the received route %s with prefix %s "
                                       " prefix length %d and overlap policy %s.\n",
                                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                        (pRtProfile),
                                                        BGP4_RT_AFI_INFO
                                                        (pRtProfile)),
                                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                        (pCurRoute),
                                                        BGP4_RT_AFI_INFO
                                                        (pCurRoute)),
                                       BGP4_RT_PREFIXLEN (pCurRoute),
                                       Bgp4PrintCodeName
                                       (BGP4_INSTALL_MORESPEC_RT,
                                        BGP4_OVERLAP_POLICY_CODE_NAME));
                        BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_OVERLAP);
                        Bgp4DshAddRouteToOverlapList (pRtProfile);
                    }

                    if (BGP4_RT_PREFIXLEN (pRtProfile) >
                        BGP4_RT_PREFIXLEN (pCurRoute))
                    {
                        u4DeleteNode = BGP4_TRUE;
                    }
                }

                if ((u4DeleteNode == BGP4_TRUE) &&
                    ((BGP4_RT_GET_FLAGS (pCurRoute) & BGP4_RT_OVERLAP) !=
                     BGP4_RT_OVERLAP))
                {
                    /* Add the Best route to the new withdrawn list */
                    pBestRoute =
                        Bgp4DshGetBestRouteFromProfileList (pFndProfileList,
                                                            BGP4_RT_CXT_ID
                                                            (pRtProfile));
                    if (pBestRoute != NULL)
                    {
                        Bgp4DshAddRouteToList (pNewWithdrawnList, pBestRoute,
                                               0);
                    }

                    while (pCurRoute != NULL)
                    {
                        /* Overlap policy is not applied for Local routes */
                        if (BGP4_RT_PROTOCOL (pCurRoute) == BGP4_LOCAL_ID)
                        {
                            pCurRoute = BGP4_RT_NEXT (pCurRoute);
                            continue;
                        }

                        /* Add the route to the overlap list */
                        BGP4_TRC_ARG4 (NULL, BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC | BGP4_FDB_TRC,
                                       BGP4_MOD_NAME,
                                       "\tOverlap route found for the received route %s with prefix %s "
                                       " prefix length %d and overlap policy %s.\n",
                                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                        (pRtProfile),
                                                        BGP4_RT_AFI_INFO
                                                        (pRtProfile)),
                                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                        (pCurRoute),
                                                        BGP4_RT_AFI_INFO
                                                        (pCurRoute)),
                                       BGP4_RT_PREFIXLEN (pCurRoute),
                                       Bgp4PrintCodeName
                                       (BGP4_INSTALL_MORESPEC_RT,
                                        BGP4_OVERLAP_POLICY_CODE_NAME));
                        BGP4_RT_SET_FLAG (pCurRoute, BGP4_RT_OVERLAP);
                        Bgp4DshAddRouteToOverlapList (pCurRoute);
                        pCurRoute = BGP4_RT_NEXT (pCurRoute);
                    }
                }
                else if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_OVERLAP) ==
                         BGP4_RT_OVERLAP)
                {
                    /* Received route is suppressed because of Overlap policy.
                     * No need to process any further because the RIB is not
                     * going to be affected because of this route. */
                    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_FDB_TRC, BGP4_MOD_NAME,
                                   "\tReceived route %s is suppressed of overlap policy.\n",
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pRtProfile)));
                    return BGP4_SUCCESS;
                }
            }
            if (pCurRoute != NULL)
            {
                pCurRoute = BGP4_RT_NEXT (pCurRoute);
            }
        }

        /* Try and process the next Node in the RIB. */
        if (i4NextFound != BGP4_FAILURE)
        {
            pFndProfileList = pNextRtProfileList;
            pRibNode = pNextRibNode;
            pCurRoute =
                Bgp4DshGetFirstRouteFromProfileList (pNextRtProfileList);
            if (pCurRoute != NULL)
            {
                if ((Bgp4GetAfiSafiIndex (BGP4_RT_AFI_INFO (pCurRoute),
                                          BGP4_RT_SAFI_INFO (pCurRoute),
                                          &u4Index)) == BGP4_FAILURE)
                {
                    /* No <AFI, SAFI> support for this family. */
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                              BGP4_FDB_TRC | BGP4_ALL_FAILURE_TRC,
                              BGP4_MOD_NAME,
                              "\tNo address family support for the route found.\n");
                    return BGP4_SUCCESS;
                }
            }
            pNextRtProfileList = NULL;
            pNextRibNode = NULL;
        }
        else
        {
            return BGP4_SUCCESS;
        }
    }
}

/******************************************************************************/
/* Function Name : Bgp4RibhAddMultipathRouteToVrfUpdLists                     */
/* Description   : Function to Update FIB list based on Mulipathlists         */
/* Input(s)      : (pRtProfile) -Profile list                                 */
/*                  pPrevMPathList - Previous multipath list                  */
/* Output(s)     :  Updated FIB List                                          */
/* Return        : BGP4_SUCCESS/BGP4_FAILURE                                  */
/******************************************************************************/
INT4
Bgp4RibhAddMultipathRouteToVrfUpdLists (tRouteProfile * pRtProfile,
                                        tTMO_SLL * pPrevMPathList)
{
    tRouteProfile      *pRtTempProfile = NULL;
    tLinkNode          *pRtLink = NULL;
    tLinkNode          *pTempLinkNode = NULL;
    UINT4               u4VrfId = BGP4_RT_CXT_ID (pRtProfile);
    INT4                i4Found = 0;

    if (BGP4_MPATH_NOT_ENABLED (u4VrfId))
    {
        return BGP4_FAILURE;
    }

    /* Scan through new Multipath list
     *       Check present in old
     *              yes, Remove from od list
     *              no , Add to FIB
     * Scan throgh old, Remove from FIB */
    pRtTempProfile = pRtProfile;
    while (pRtTempProfile != NULL)
    {
        i4Found = 0;
        BGP_SLL_DYN_Scan (pPrevMPathList, pRtLink, pTempLinkNode, tLinkNode *)
        {
            if (pRtLink->pRouteProfile == pRtTempProfile)
            {
                if ((BGP4_RT_GET_FLAGS (pRtTempProfile) &
                     BGP4_RT_IN_FIB_UPD_LIST) == BGP4_RT_IN_FIB_UPD_LIST)
                {
                    BGP4_RT_RESET_FLAG (pRtTempProfile, (BGP4_RT_WITHDRAWN));
                }
                i4Found = BGP4_TRUE;
                TMO_SLL_Delete (pPrevMPathList, &pRtLink->TSNext);
                Bgp4MemReleaseLinkNode (pRtLink);
                break;
            }
        }
        if (i4Found != BGP4_TRUE)
        {
            /* Add to FIB (pRtTempProfile) */
            if ((BGP4_RT_GET_FLAGS (pRtTempProfile) & BGP4_RT_IN_FIB_UPD_LIST)
                == BGP4_RT_IN_FIB_UPD_LIST)
            {
                BGP4_RT_RESET_FLAG (pRtTempProfile, (BGP4_RT_WITHDRAWN));
            }
            else
            {
                Bgp4AddFeasRouteToFIBUpdList (pRtTempProfile, NULL);
            }
        }
        pRtTempProfile = pRtTempProfile->pMultiPathRtNext;
    }

    BGP_SLL_DYN_Scan (pPrevMPathList, pRtLink, pTempLinkNode, tLinkNode *)
    {
        /*Add to FIB as withdrawn (pRtLink->pRouteProfile); */
        if ((BGP4_RT_GET_EXT_FLAGS (pRtLink->pRouteProfile) & BGP4_RT_IN_RTM) ==
            BGP4_RT_IN_RTM)
        {
            if ((BGP4_RT_GET_FLAGS (pRtLink->pRouteProfile) &
                 BGP4_RT_IN_FIB_UPD_LIST) != BGP4_RT_IN_FIB_UPD_LIST)
            {
                BGP4_RT_SET_FLAG (pRtLink->pRouteProfile, (BGP4_RT_WITHDRAWN));
                Bgp4AddWithdRouteToFIBUpdList (pRtLink->pRouteProfile);
            }
        }
        else if ((BGP4_RT_GET_FLAGS (pRtLink->pRouteProfile) &
                  BGP4_RT_IN_FIB_UPD_LIST) == BGP4_RT_IN_FIB_UPD_LIST)
        {
            BGP4_RT_SET_FLAG (pRtLink->pRouteProfile, (BGP4_RT_WITHDRAWN));
            Bgp4AddWithdRouteToFIBUpdList (pRtLink->pRouteProfile);
        }
        TMO_SLL_Delete (pPrevMPathList, &pRtLink->TSNext);
        Bgp4MemReleaseLinkNode (pRtLink);
    }
    return BGP4_SUCCESS;
}

UINT4
Bgp4RibhIsRouteIdentical (tRouteProfile * pRtProfile1,
                          tRouteProfile * pRtProfile2)
{
    if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
    {
        return BGP4_FAILURE;
    }
    if ((pRtProfile1 == NULL) || (pRtProfile2 == NULL))
    {
        return BGP4_FAILURE;
    }
    if ((BGP4_RT_PEER_ENTRY (pRtProfile1) == NULL)
        || (BGP4_RT_PEER_ENTRY (pRtProfile1) == NULL))
    {
        return BGP4_FAILURE;
    }
    if (BGP4_RT_PEER_ENTRY (pRtProfile1) != BGP4_RT_PEER_ENTRY (pRtProfile1))
    {
        return BGP4_FAILURE;
    }
    if (PrefixMatch
        (pRtProfile1->ImmediateNextHopInfo,
         pRtProfile2->ImmediateNextHopInfo) != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }
    if ((PrefixMatch
         (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
          (BGP4_RT_NET_ADDRESS_INFO (pRtProfile1)),
          BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
          (BGP4_RT_NET_ADDRESS_INFO (pRtProfile2)))) != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}
#endif /* BGP4RIBH_C */
