/*  $Id: bgp4trie.c,v 1.20 2016/12/27 12:35:58 siva Exp $*/
#ifndef BGP4TRIE_C
#define BGP4TRIE_C

#include "bgp4com.h"

struct TrieAppFns   Bgp4TrieLibFns = {
    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID **ppAppPtr, VOID *pAppSpecInfo)) Bgp4TrieCbAddEntry,
    (INT4 (*)(tInputParams * pInputParams, VOID **ppAppPtr,
              VOID *pOutputParams, VOID *pAppPtr, tKey Key))
        Bgp4TrieCbDeleteEntry,
    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID *pAppPtr)) Bgp4TrieCbLookupExactEntry,
    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID *pAppPtr, UINT2, tKey)) NULL,
    (VOID *(*)(tInputParams *, VOID (*)(VOID *), VOID *)) NULL,
    (VOID (*)(VOID *)) NULL,
    (INT4 (*)(VOID *, VOID **, tKey)) NULL,
    (INT4 (*)(VOID *, VOID *, tKey)) NULL,
    (INT4 (*)(tInputParams *, VOID *, VOID **, VOID *, UINT4)) NULL,
    (VOID *(*)(tInputParams * pInputParams, VOID (*pAppSpecFunc) (VOID *),
               VOID *pOutputParams)) Bgp4TrieCbDeleteTrieInit,
    (VOID (*)(VOID *pDummy)) Bgp4TrieCbDeleteTrieDeInit,
    (INT4 (*)(VOID *pInputParams, VOID **ppAppPtr, tKey Key))
        Bgp4TrieDeleteAppData,
    (INT4 (*)(tInputParams *, VOID *)) NULL,
    (INT4 (*)(UINT2, tInputParams *, VOID *, VOID *, tKey)) NULL,
    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID *pAppPtr, tKey Key)) Bgp4TrieCbTraverseAndGetNextEntry,
    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID *pAppPtr, UINT2 u2Keysize, tKey Key))
        Bgp4TrieCbLookupOverlapEntry
};

/*****************************************************************************/
/* Function Name : Bgp4TrieInit                                              */
/* Description   : This function initializes TRIE and creates instances of   */
/*                 TRIE for the supported address families                   */
/* Input(s)      : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4TrieInit (UINT4 u4CxtId)
{
    INT4                i4CrStatus = BGP4_FAILURE;

    /* Create one TRIE instance for <ipv4, unicast>. */
    i4CrStatus = Bgp4TrieCreate (BGP4_ROUTE_TRIE, BGP4_IPV4_UNI_INDEX,
                                 u4CxtId, &(BGP4_IPV4_RIB_TREE (u4CxtId)));
    if (i4CrStatus == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }

#ifdef BGP4_IPV6_WANTED
    /* Create one TRIE instance for <ipv6, unicast>. */
    i4CrStatus = Bgp4TrieCreate (BGP4_ROUTE_TRIE, BGP4_IPV6_UNI_INDEX,
                                 u4CxtId, &(BGP4_IPV6_RIB_TREE (u4CxtId)));
    if (i4CrStatus == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }
#endif
#ifdef L3VPN
    /* Create one TRIE instance for <vpn4, unicast>. */
    i4CrStatus = Bgp4TrieCreate (BGP4_ROUTE_TRIE, BGP4_VPN4_UNI_INDEX,
                                 u4CxtId, &(BGP4_VPN4_RIB_TREE (u4CxtId)));
    if (i4CrStatus == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }
#endif
#ifdef VPLSADS_WANTED
    if (u4CxtId == BGP4_DFLT_VRFID)
    {
        /* Create one TRIE instance for <l2vpn, vpls> */
        i4CrStatus = Bgp4TrieCreate (BGP4_ROUTE_TRIE, BGP4_L2VPN_VPLS_INDEX,
                                     u4CxtId, &(BGP4_VPLS_RIB_TREE (u4CxtId)));
        if (i4CrStatus == BGP4_FAILURE)
        {
            return BGP4_FAILURE;
        }
    }
#endif
#ifdef EVPN_WANTED
    if (u4CxtId == BGP4_DFLT_VRFID)
    { 
       /* Create one TRIE instance for <L2VPN, EVPN>. */
       i4CrStatus = Bgp4TrieCreate (BGP4_ROUTE_TRIE, BGP4_L2VPN_EVPN_INDEX,
                                 u4CxtId, &(BGP4_EVPN_RIB_TREE (u4CxtId)));
       if (i4CrStatus == BGP4_FAILURE)
       {
           return BGP4_FAILURE;
       }
    }
#endif
    BGP4_RIB_ROUTES_COUNT (u4CxtId) = 0;
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4TrieCreate                                            */
/* Description   : This function calls TRIE specific API to create TRIE      */
/* Input(s)      : u4TrieType & u4Index - Identifies the specific TRIE that  */
/*                                        needs to be created.               */
/*                 u4VrfId - VRF identifier                                  */
/* Output(s)     : ppRoot - pointer to the address of cretaed TRIE           */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4TrieCreate (UINT4 u4TrieType, UINT4 u4Index, UINT4 u4VrfId, VOID **ppRoot)
{
    tTrieCrtParams      CreateParams;
    tBgpSystemSize      BgpSystemSize;
    INT4                i4Status = 0;

    UNUSED_PARAM (ppRoot);

    GetBgpSizingParams (&BgpSystemSize);
    switch (u4TrieType)
    {
        case BGP4_ROUTE_TRIE:
            switch (u4Index)
            {
                    /* IPV4 - UNICAST */
                case BGP4_IPV4_UNI_INDEX:
                    /* key is address + mask */
                    CreateParams.u2KeySize = sizeof (UINT2) * sizeof (UINT4);
                    CreateParams.u4Type =
                        (BGP4_IPV4UNICAST_RIB | (u4VrfId << 8));
                    CreateParams.u1AppId = BGP_ID;
                    CreateParams.AppFns = &(Bgp4TrieLibFns);
                    CreateParams.u4NumRadixNodes =
                        FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                        u4PreAllocatedUnits + 1;
                    CreateParams.u4NumLeafNodes =
                        FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                        u4PreAllocatedUnits + 1;
                    CreateParams.u4NoofRoutes =
                        FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                        u4PreAllocatedUnits + 1;
                    CreateParams.bPoolPerInst = OSIX_FALSE;
                    CreateParams.bSemPerInst = OSIX_FALSE;
                    CreateParams.bValidateType = OSIX_TRUE;
                    BGP4_IPV4_RIB_TREE (u4VrfId) =
                        TrieCreateInstance (&(CreateParams));
                    if (BGP4_IPV4_RIB_TREE (u4VrfId) != NULL)
                        i4Status = BGP4_SUCCESS;
                    else
                        i4Status = BGP4_FAILURE;
                    break;
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    /* key is address + mask */
                    CreateParams.u4NumRadixNodes =
                        FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                        u4PreAllocatedUnits + 1;
                    CreateParams.u4NumLeafNodes =
                        FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                        u4PreAllocatedUnits + 1;
                    CreateParams.u4NoofRoutes =
                        FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                        u4PreAllocatedUnits + 1;
                    CreateParams.u2KeySize =
                        sizeof (UINT2) * BGP4_IPV6_PREFIX_LEN;
                    CreateParams.u4Type =
                        (BGP4_IPV6UNICAST_RIB | (u4VrfId << 8));
                    CreateParams.u1AppId = BGP_ID;
                    CreateParams.AppFns = &(Bgp4TrieLibFns);
                    CreateParams.bPoolPerInst = OSIX_FALSE;
                    CreateParams.bSemPerInst = OSIX_FALSE;
                    CreateParams.bValidateType = OSIX_TRUE;
                    BGP4_IPV6_RIB_TREE (u4VrfId) =
                        TrieCreateInstance (&(CreateParams));

                    if (BGP4_IPV6_RIB_TREE (u4VrfId) != NULL)
                        i4Status = BGP4_SUCCESS;
                    else
                        i4Status = BGP4_FAILURE;
                    break;
#endif
#ifdef L3VPN
                case BGP4_VPN4_UNI_INDEX:
                    /* key is address + mask */
                    CreateParams.u4NumRadixNodes =
                        FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                        u4PreAllocatedUnits + 1;
                    CreateParams.u4NumLeafNodes =
                        FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                        u4PreAllocatedUnits + 1;
                    CreateParams.u4NoofRoutes =
                        FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                        u4PreAllocatedUnits + 1;
                    CreateParams.u1AppId = BGP_ID;
                    CreateParams.AppFns = &(Bgp4TrieLibFns);
                    CreateParams.u2KeySize =
                        sizeof (UINT2) * BGP4_VPN4_PREFIX_LEN;
                    CreateParams.u4Type =
                        (BGP4_VPN4UNICAST_RIB | (u4VrfId << 8));
                    CreateParams.bPoolPerInst = OSIX_FALSE;
                    CreateParams.bSemPerInst = OSIX_FALSE;
                    CreateParams.bValidateType = OSIX_TRUE;
                    BGP4_VPN4_RIB_TREE (u4VrfId) =
                        TrieCreateInstance (&(CreateParams));

                    if (BGP4_VPN4_RIB_TREE (u4VrfId) != NULL)
                        i4Status = BGP4_SUCCESS;
                    else
                        i4Status = BGP4_FAILURE;
                    break;
#endif
#ifdef VPLSADS_WANTED
                case BGP4_L2VPN_VPLS_INDEX:
                    /*For L2VPN_VPLS the key is RD+VEID+VBO */
                    CreateParams.u2KeySize = BGP4_VPLS_MAX_KEY_SIZE;
                    CreateParams.u4Type = BGP4_L2VPNVPLS_RIB;
                    CreateParams.u1AppId = BGP_ID;
                    CreateParams.AppFns = &(Bgp4TrieLibFns);
                    /*  Below 3 fields are taken form FsBGPSizingParam 
                       or BGPSystem Size */
                    CreateParams.u4NumRadixNodes =
                        FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                        u4PreAllocatedUnits + 1;
                    CreateParams.u4NumLeafNodes =
                        FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                        u4PreAllocatedUnits + 1;
                    CreateParams.u4NoofRoutes =
                        FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                        u4PreAllocatedUnits + 1;
                    CreateParams.bPoolPerInst = OSIX_FALSE;
                    CreateParams.bSemPerInst = OSIX_FALSE;
                    CreateParams.bValidateType = OSIX_TRUE;
                    BGP4_VPLS_RIB_TREE (u4VrfId) =
                        TrieCreateInstance (&(CreateParams));
                    if (BGP4_VPLS_RIB_TREE (u4VrfId) != NULL)
                        i4Status = BGP4_SUCCESS;
                    else
                        i4Status = BGP4_FAILURE;
                    break;
#endif
#ifdef EVPN_WANTED
                case BGP4_L2VPN_EVPN_INDEX:
                    /*For L2VPN_VPLS the key is RD+VNID+MACADDR+ETHSEGID */
                    CreateParams.u2KeySize = BGP4_EVPN_MAX_KEY_SIZE;
                    CreateParams.u4Type = BGP4_EVPN_RIB;
                    CreateParams.u1AppId = BGP_ID;
                    CreateParams.AppFns = &(Bgp4TrieLibFns);
                    /*  Below 3 fields are taken form FsBGPSizingParam
                     *  or BGPSystem Size */
                    CreateParams.u4NumRadixNodes =
                        FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                        u4PreAllocatedUnits + 1;
                    CreateParams.u4NumLeafNodes =
                        FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                        u4PreAllocatedUnits + 1;
                    CreateParams.u4NoofRoutes =
                        FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
                        u4PreAllocatedUnits + 1;
                    CreateParams.bPoolPerInst = OSIX_FALSE;
                    CreateParams.bSemPerInst = OSIX_FALSE;
                    CreateParams.bValidateType = OSIX_TRUE;
                    BGP4_EVPN_RIB_TREE (u4VrfId) =
                        TrieCreateInstance (&(CreateParams));
                    if (BGP4_EVPN_RIB_TREE (u4VrfId) != NULL)
                        i4Status = BGP4_SUCCESS;
                    else
                        i4Status = BGP4_FAILURE;
                    break;

#endif
                default:
                    i4Status = BGP4_FAILURE;
                    break;
            }
            break;

        default:
            return BGP4_FAILURE;
    }

    return i4Status;
}

/*****************************************************************************/
/* Function Name : Bgp4TrieDelete                                            */
/* Description   : This function calls TRIE specific API to delete TRIE      */
/* Input(s)      : u4TrieType & u4Index - Identifies the specific TRIE that  */
/*                                        needs to be created.               */
/*                 u4VrfId - VRF identifier                                  */
/*                 ppRoot - pointer to the address of corresponding TRIE     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4TrieDelete (UINT4 u4TrieType, UINT4 u4Index, UINT4 u4VrfId, VOID **ppRoot)
{
    tInputParams        InParams;
    tBgp4TrieOutParams  OutParams;
    INT4                i4Status = 0;

    UNUSED_PARAM (ppRoot);
    switch (u4TrieType)
    {
        case BGP4_ROUTE_TRIE:
            switch (u4Index)
            {
                    /* IPV4 - UNICAST */
                case BGP4_IPV4_UNI_INDEX:
                    InParams.pRoot = BGP4_IPV4_RIB_TREE (u4VrfId);
                    InParams.pLeafNode = NULL;
                    InParams.i1AppId = BGP_ID;
                    break;
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    InParams.pRoot = BGP4_IPV6_RIB_TREE (u4VrfId);
                    InParams.pLeafNode = NULL;
                    InParams.i1AppId = BGP_ID;
                    break;
#endif
#ifdef L3VPN
                case BGP4_VPN4_UNI_INDEX:
                    InParams.pLeafNode = NULL;
                    InParams.i1AppId = BGP_ID;
                    InParams.pRoot = BGP4_VPN4_RIB_TREE (u4VrfId);
                    break;
#endif
#ifdef VPLSADS_WANTED
                case BGP4_L2VPN_VPLS_INDEX:
                    InParams.pRoot = BGP4_VPLS_RIB_TREE (u4VrfId);
                    InParams.pLeafNode = NULL;
                    InParams.i1AppId = BGP_ID;
                    break;
#endif
#ifdef EVPN_WANTED
                case BGP4_L2VPN_EVPN_INDEX:
                    InParams.pLeafNode = NULL;
                    InParams.i1AppId = BGP_ID;
                    InParams.pRoot = BGP4_EVPN_RIB_TREE (u4VrfId);
                    break;
#endif

                default:
                    return BGP4_FAILURE;
            }
            break;

        default:
            return BGP4_FAILURE;
    }

    i4Status = TrieDel (&InParams, Bgp4TrieAppSpecDelete, &OutParams);
    if (i4Status >= TRIE_SUCCESS)
    {
        /* TRIE has been cleared successfully. Update the TRIE Root. */
        switch (u4TrieType)
        {
            case BGP4_ROUTE_TRIE:
                switch (u4Index)
                {
                        /* IPV4 - UNICAST */
                    case BGP4_IPV4_UNI_INDEX:
                        BGP4_IPV4_RIB_TREE (u4VrfId) = NULL;
                        break;
#ifdef BGP4_IPV6_WANTED
                    case BGP4_IPV6_UNI_INDEX:
                        BGP4_IPV6_RIB_TREE (u4VrfId) = NULL;
                        break;
#endif
#ifdef L3VPN
                    case BGP4_VPN4_UNI_INDEX:
                        BGP4_VPN4_RIB_TREE (u4VrfId) = NULL;
                        break;
#endif
#ifdef VPLSADS_WANTED
                    case BGP4_L2VPN_VPLS_INDEX:
                        BGP4_VPLS_RIB_TREE (u4VrfId) = NULL;
                        break;
#endif
#ifdef EVPN_WANTED
                    case BGP4_L2VPN_EVPN_INDEX:
                        BGP4_EVPN_RIB_TREE (u4VrfId) = NULL;
                        break;
#endif
		    default:
			break;
                }
                break;
        }
    }

    return ((i4Status >= TRIE_SUCCESS) ? (BGP4_SUCCESS) : (BGP4_FAILURE));
}

/*****************************************************************************/
/* Function Name : Bgp4TrieAppSpecDelete                                     */
/* Description   : This function calls TRIE specific API to delete TRIE      */
/* Input(s)      : u4TrieType & u4Index - Identifies the specific TRIE that  */
/*                                        needs to be created.               */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
VOID
Bgp4TrieAppSpecDelete (VOID *pDummy)
{
    UNUSED_PARAM (pDummy);
    return;
}

/*****************************************************************************/
/* Function Name : Bgp4TrieDeleteAppData                                     */
/* Description   : This function calls Application Specific clear routine.   */
/* Input(s)      : pOutputParams - Output Params Structure based on which    */
/*               :                 app specific information are deleted.     */
/*               : ppAppSpecPtr - Pointer to the application specific data.  */
/*               : Key - Key corresponding the Application specific data.    */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4TrieDeleteAppData (VOID *pInputParams, VOID **ppAppSpecPtr, tKey Key)
{
    /* Currently All TRIE nodes are cleared explicitly before TRIE is deleted.
     * So this is not implemented. */
    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (ppAppSpecPtr);
    UNUSED_PARAM (Key);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4TrieCbDeleteTrieInit                                  */
/* Description   : This function callback function for Bgp Trie Delete.      */
/* Input(s)      :                                                           */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
VOID               *
Bgp4TrieCbDeleteTrieInit (tInputParams * pInputParams,
                          VOID (AppSpec) (VOID *), VOID *pOutputParams)
{
    /* Currently All TRIE nodes are cleared explicitly before TRIE is deleted.
     * So this is not implemented. */
    AppSpec (pOutputParams);
    return ((VOID *) pInputParams);
}

/*****************************************************************************/
/* Function Name : Bgp4TrieCbDeleteTrieDeInit                                */
/* Description   : This function calls TRIE specific API to delete TRIE      */
/* Input(s)      : u4TrieType & u4Index - Identifies the specific TRIE that  */
/*                                        needs to be created.               */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
VOID
Bgp4TrieCbDeleteTrieDeInit (VOID *pDummy)
{
    UNUSED_PARAM (pDummy);
    return;
}

/*****************************************************************************/
/* Function Name : Bgp4TrieAddEntry                                          */
/* Description   : This function calls TRIE specific API to add a route      */
/*                 profile entry into RIB                                    */
/* Input(s)      : pNewAppSpecInfo - pointer to the new application specific */
/*                                 info that is going to be added            */
/*                 u4VrfId - VRF identifier in which route to be added       */
/* Output(s)     : pu1IsBestRoute - Incicates whether the route is selected  */
/*                                  as best route or not. BGP4_TRUE if best  */
/*                                  else BGP4_FALSE                          */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4TrieAddEntry (tRouteProfile * pNewAppSpecInfo, UINT4 u4VrfId,
                  UINT1 *pu1IsBestRoute)
{
    tInputParams        InParams;
    tBgp4TrieOutParams  OutParams;
#if ((defined BGP4_IPV6_WANTED) || (defined L3VPN))
    tAddrPrefix         Mask;
#endif
#if (defined BGP4_IPV6_WANTED)
    tAddrPrefix         Prefix;
#endif
    UINT4               au4Indx[BGP4_IPV4_KEY_ARR_SIZE];
    /* The Key of Trie is made-up 
     * of address and mask */
    UINT4               u4AsafiMask;
    UINT4               u4Prefix;
    UINT4               u4Mask;
    UINT1               au1ParamsKey[BGP4_INPUT_PARAMS_KEY_SIZE];
#ifdef VPLSADS_WANTED
    UINT1               au1VplsKey[BGP4_VPLS_MAX_KEY_SIZE];
    tVplsNlriInfo       NlriInfo;
#endif
      
#if (defined BGP4_IPV6_WANTED)
    MEMSET (&Prefix, 0, sizeof (tAddrPrefix));
#endif
    *pu1IsBestRoute = BGP4_FALSE;

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pNewAppSpecInfo),
                           BGP4_RT_SAFI_INFO (pNewAppSpecInfo), u4AsafiMask);
    MEMSET (au1ParamsKey, 0, BGP4_INPUT_PARAMS_KEY_SIZE);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
#endif
        {
            InParams.pRoot = BGP4_IPV4_RIB_TREE (u4VrfId);
            InParams.pLeafNode = NULL;
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.Key.pKey = (UINT1 *) &(au4Indx[BGP4_IPV4_KEY_ARR_INDX0]);

            PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pNewAppSpecInfo));
            u4Mask = Bgp4GetSubnetmask ((UINT1)
                                        BGP4_RT_PREFIXLEN (pNewAppSpecInfo));
            au4Indx[BGP4_IPV4_KEY_ARR_INDX0] = OSIX_HTONL (u4Prefix);
            au4Indx[BGP4_IPV4_KEY_ARR_INDX1] = OSIX_HTONL (u4Mask);
            InParams.u1PrefixLen = (UINT1) BGP4_RT_PREFIXLEN (pNewAppSpecInfo);
            break;
        }
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
        {
            InParams.pRoot = BGP4_IPV6_RIB_TREE (u4VrfId);
            InParams.pLeafNode = NULL;
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.Key.pKey = au1ParamsKey;
            MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Prefix),
                    BGP4_RT_IP_PREFIX (pNewAppSpecInfo), BGP4_IPV6_PREFIX_LEN);
            Bgp4GetIPV6Subnetmask (BGP4_RT_PREFIXLEN (pNewAppSpecInfo),
                                   &(Mask.au1Address[0]));
            MEMCPY (InParams.Key.pKey, Prefix.au1Address, BGP4_IPV6_PREFIX_LEN);
            Bgp4MaskIpv6Address (InParams.Key.pKey, Mask.au1Address);
            MEMCPY (InParams.Key.pKey + BGP4_IPV6_PREFIX_LEN,
                    Mask.au1Address, BGP4_IPV6_PREFIX_LEN);
            InParams.u1PrefixLen = (UINT1) BGP4_RT_PREFIXLEN (pNewAppSpecInfo);
            break;
        }
#endif
#ifdef L3VPN
        case CAP_MP_VPN4_UNICAST:
        {
            /* Route is to be added into VPNv4-RIB */
            InParams.pRoot = BGP4_VPN4_RIB_TREE (u4VrfId);
            InParams.pLeafNode = NULL;
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.Key.pKey = au1ParamsKey;
            PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pNewAppSpecInfo));
            u4Prefix = OSIX_HTONL (u4Prefix);
            Bgp4GetVpn4Subnetmask (BGP4_RT_PREFIXLEN (pNewAppSpecInfo),
                                   &(Mask.au1Address[0]));
            MEMCPY (InParams.Key.pKey,
                    BGP4_RT_ROUTE_DISTING (pNewAppSpecInfo),
                    BGP4_VPN4_ROUTE_DISTING_SIZE);
            MEMCPY (InParams.Key.pKey + BGP4_VPN4_ROUTE_DISTING_SIZE,
                    (UINT1 *) &u4Prefix, BGP4_IPV4_PREFIX_LEN);
            Bgp4MaskVpn4Address (InParams.Key.pKey, Mask.au1Address);
            MEMCPY (InParams.Key.pKey + BGP4_VPN4_PREFIX_LEN,
                    Mask.au1Address, BGP4_VPN4_PREFIX_LEN);
            InParams.u1PrefixLen =
                (UINT1) ((BGP4_VPN4_ROUTE_DISTING_SIZE *
                          BGP4_ONE_BYTE_BITS) +
                         BGP4_RT_PREFIXLEN (pNewAppSpecInfo));
            break;
        }
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
        {
            InParams.pRoot = BGP4_VPLS_RIB_TREE (u4VrfId);
            InParams.pLeafNode = NULL;
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            /*For L2VPN_VPLS, the Key is RD+VEID+VBO */
            InParams.Key.pKey = au1VplsKey;

            Bgp4VplsGetNlriInfoFromNetAddr (BGP4_RT_IP_PREFIX (pNewAppSpecInfo),
                                            &NlriInfo);
            MEMCPY (InParams.Key.pKey,
                    NlriInfo.au1RouteDistinguisher, MAX_LEN_RD_VALUE);
            MEMCPY (InParams.Key.pKey + MAX_LEN_RD_VALUE,
                    &NlriInfo.u2VeId, BGP4_VPLS_NLRI_VEID_LEN);
            MEMCPY (InParams.Key.pKey + MAX_LEN_RD_VALUE +
                    BGP4_VPLS_NLRI_VEID_LEN, &NlriInfo.u2VeBaseOffset,
                    BGP4_VPLS_NLRI_VBO_LEN);

            InParams.u1PrefixLen = BGP4_VPLS_MAX_KEY_SIZE;

            BGP4_TRC_ARG1 (NULL,
                           BGP4_TRC_FLAG, BGP4_VPLS_TRC, BGP4_MOD_NAME,
                           "\tBgp4TrieAddEntry():<L2VPN, VPLS> Route Add to RIB"
                           "\tNLRI - [%s]\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pNewAppSpecInfo),
                                            BGP4_RT_AFI_INFO
                                            (pNewAppSpecInfo)));
            break;
        }
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
        {
            InParams.pRoot = BGP4_EVPN_RIB_TREE (u4VrfId);
            InParams.pLeafNode = NULL;
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            /*For EVPN, the Key is RD+VNID+MAC Addr */
            InParams.Key.pKey = au1ParamsKey;

            MEMCPY (InParams.Key.pKey,
                    BGP4_EVPN_RD_DISTING (pNewAppSpecInfo),
                    BGP4_EVPN_ROUTE_DISTING_SIZE);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE,
                    &(BGP4_EVPN_VNID(pNewAppSpecInfo)),MAX_LEN_VNI_VALUE);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE + MAX_LEN_VNI_VALUE,
                   BGP4_EVPN_MACADDR(pNewAppSpecInfo), MAX_LEN_MAC_ADDR);

            InParams.u1PrefixLen = BGP4_EVPN_MAX_KEY_SIZE;
            break;
        }
#endif
        default:
            return BGP4_FAILURE;
    }
    if (TrieAdd (&InParams, pNewAppSpecInfo, &OutParams) == TRIE_FAILURE)
    {
        return BGP4_FAILURE;
    }
    /* Route has been successfully added to the Trie. */
    *pu1IsBestRoute = (UINT1) OutParams.u4Data;
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4TrieCbAddEntry                                        */
/* Description   : This is a call back function to add a route profile entry */
/*                 into RIB                                                  */
/*                 (Registered with TRIE library during TRIE creation)       */
/* Input(s)      : pInputParams - void pointer used by TRIE                  */
/*                 pOutputParams - void pointer (used by BGP)                */
/*                 ppAppSpecInfo - address of the Application (BGP) specific */
/*                                 info present in TRIE leaf node            */
/*                 pNewAppSpecInfo - pointer to the new application specific */
/*                                 info that is going to be added            */
/* Output(s)     : ppAppSpecInfo - address of the updated application        */
/*                                 specific info pointer                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4TrieCbAddEntry (tInputParams * pInputParams, VOID *pOutputParams,
                    VOID **ppAppSpecInfo, VOID *pNewAppSpecInfo)
{
    tBgp4TrieOutParams *pBgpOutputParams = NULL;
    tRouteProfile      *pAddRtProfile = (tRouteProfile *) pNewAppSpecInfo;
    tRouteProfile     **ppRtProfileList = (tRouteProfile **) ppAppSpecInfo;

    UNUSED_PARAM (pInputParams);
    pBgpOutputParams = (tBgp4TrieOutParams *) pOutputParams;

    Bgp4DshAddRouteToProfileList (ppRtProfileList, pAddRtProfile);
    if ((*ppRtProfileList == pAddRtProfile) &&
        (BGP4_IS_ROUTE_VALID (pAddRtProfile) == BGP4_TRUE))
    {
        /* Input route is the best route */
        pBgpOutputParams->u4Data = BGP4_TRUE;
    }
    else
    {
        pBgpOutputParams->u4Data = BGP4_FALSE;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4TrieDeleteEntry                                       */
/* Description   : This function calls TRIE specific API to delete a route   */
/*                 profile entry from RIB                                    */
/* Input(s)      : pNewAppSpecInfo - pointer to the new application specific */
/*                                 info that is going to be deleted          */
/*               : pRibNode - Pointer to the LeafNode where the route is     */
/*                            present.                                       */
/*               : u4VrfId - VRF identifier from which route is to be removed*/
/* Output(s)     : pu1IsBestRoute - Incicates whether the route to be deleted*/
/*                                  is best route or not. BGP4_TRUE if best  */
/*                                  else BGP4_FALSE                          */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4TrieDeleteEntry (tRouteProfile * pNewAppSpecInfo, VOID *pRibNode,
                     UINT4 u4VrfId, UINT1 *pu1IsBestRoute)
{
    tInputParams        InParams;
    tBgp4TrieOutParams  OutParams;
#if (defined BGP4_IPV6_WANTED)
    tAddrPrefix         Prefix;
#endif
#if ((defined BGP4_IPV6_WANTED) || (defined L3VPN))
    tAddrPrefix         Mask;
#endif
    UINT4               au4Indx[BGP4_IPV4_KEY_ARR_SIZE];
    /* The Key of Trie is made-up 
     * of address and mask */
    UINT4               u4Prefix;
    UINT4               u4Mask;
    UINT4               u4AsafiMask;
    UINT1               au1ParamsKey[BGP4_INPUT_PARAMS_KEY_SIZE];
#ifdef VPLSADS_WANTED
    UINT1               au1VplsKey[BGP4_VPLS_MAX_KEY_SIZE];
    tVplsNlriInfo       NlriInfo;
#endif

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pNewAppSpecInfo),
                           BGP4_RT_SAFI_INFO (pNewAppSpecInfo), u4AsafiMask);
    MEMSET (au1ParamsKey, 0, BGP4_INPUT_PARAMS_KEY_SIZE);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
#endif
        {
            InParams.pRoot = BGP4_IPV4_RIB_TREE (u4VrfId);
            InParams.pLeafNode = pRibNode;
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.Key.pKey = (UINT1 *) &(au4Indx[BGP4_IPV4_KEY_ARR_INDX0]);

            PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pNewAppSpecInfo));
            u4Mask = Bgp4GetSubnetmask ((UINT1)
                                        BGP4_RT_PREFIXLEN (pNewAppSpecInfo));
            au4Indx[BGP4_IPV4_KEY_ARR_INDX0] = OSIX_HTONL (u4Prefix);
            au4Indx[BGP4_IPV4_KEY_ARR_INDX1] = OSIX_HTONL (u4Mask);
            break;
        }
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
        {
            InParams.pRoot = BGP4_IPV6_RIB_TREE (u4VrfId);
            InParams.pLeafNode = pRibNode;
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.Key.pKey = au1ParamsKey;
            MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Prefix),
                    BGP4_RT_IP_PREFIX (pNewAppSpecInfo), BGP4_IPV6_PREFIX_LEN);
            Bgp4GetIPV6Subnetmask (BGP4_RT_PREFIXLEN (pNewAppSpecInfo),
                                   &(Mask.au1Address[0]));
            MEMCPY (InParams.Key.pKey, Prefix.au1Address, BGP4_IPV6_PREFIX_LEN);
            Bgp4MaskIpv6Address (InParams.Key.pKey, Mask.au1Address);
            MEMCPY (InParams.Key.pKey + BGP4_IPV6_PREFIX_LEN,
                    Mask.au1Address, BGP4_IPV6_PREFIX_LEN);
            break;
        }
#endif
#ifdef L3VPN
        case CAP_MP_VPN4_UNICAST:
        {
            /* Route is to be added into VPNv4-RIB */
            InParams.pRoot = BGP4_VPN4_RIB_TREE (u4VrfId);
            InParams.pLeafNode = NULL;
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.Key.pKey = au1ParamsKey;
            PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pNewAppSpecInfo));
            u4Prefix = OSIX_HTONL (u4Prefix);
            Bgp4GetVpn4Subnetmask (BGP4_RT_PREFIXLEN (pNewAppSpecInfo),
                                   &(Mask.au1Address[0]));
            MEMCPY (InParams.Key.pKey,
                    BGP4_RT_ROUTE_DISTING (pNewAppSpecInfo),
                    BGP4_VPN4_ROUTE_DISTING_SIZE);
            MEMCPY (InParams.Key.pKey + BGP4_VPN4_ROUTE_DISTING_SIZE,
                    (UINT1 *) &u4Prefix, BGP4_IPV4_PREFIX_LEN);
            Bgp4MaskVpn4Address (InParams.Key.pKey, Mask.au1Address);
            MEMCPY (InParams.Key.pKey + BGP4_VPN4_PREFIX_LEN,
                    Mask.au1Address, BGP4_VPN4_PREFIX_LEN);
            break;
        }
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
        {
            InParams.pRoot = BGP4_VPLS_RIB_TREE (u4VrfId);
            InParams.pLeafNode = pRibNode;
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            /*For L2VPN_VPLS, the Key is RD+VEID+VBO */
            InParams.Key.pKey = au1VplsKey;

            Bgp4VplsGetNlriInfoFromNetAddr (BGP4_RT_IP_PREFIX (pNewAppSpecInfo),
                                            &NlriInfo);
            MEMCPY (InParams.Key.pKey,
                    NlriInfo.au1RouteDistinguisher, MAX_LEN_RD_VALUE);
            MEMCPY (InParams.Key.pKey + MAX_LEN_RD_VALUE,
                    &NlriInfo.u2VeId, BGP4_VPLS_NLRI_VEID_LEN);
            MEMCPY (InParams.Key.pKey + MAX_LEN_RD_VALUE +
                    BGP4_VPLS_NLRI_VEID_LEN, &NlriInfo.u2VeBaseOffset,
                    BGP4_VPLS_NLRI_VBO_LEN);

            InParams.u1PrefixLen = BGP4_VPLS_MAX_KEY_SIZE;
            BGP4_TRC_ARG1 (NULL,
                           BGP4_TRC_FLAG, BGP4_VPLS_TRC, BGP4_MOD_NAME,
                           "\tBgp4TrieDeleteEntry():<L2VPN, VPLS> Route Add to RIB"
                           "NLRI - [%s]\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pNewAppSpecInfo),
                                            BGP4_RT_AFI_INFO
                                            (pNewAppSpecInfo)));
            break;
        }
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
        {
            InParams.pRoot = BGP4_EVPN_RIB_TREE (u4VrfId);
            InParams.pLeafNode = NULL;
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            /*For EVPN, the Key is RD+VNID+MAC+ETHSEGID Addr */
            InParams.Key.pKey = au1ParamsKey;

            MEMCPY (InParams.Key.pKey,
                    BGP4_EVPN_RD_DISTING (pNewAppSpecInfo),
                    BGP4_EVPN_ROUTE_DISTING_SIZE);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE,
                    &(BGP4_EVPN_VNID(pNewAppSpecInfo)),MAX_LEN_VNI_VALUE);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE + MAX_LEN_VNI_VALUE,
                   BGP4_EVPN_MACADDR(pNewAppSpecInfo), MAX_LEN_MAC_ADDR);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE + MAX_LEN_VNI_VALUE
                     + MAX_LEN_MAC_ADDR,BGP4_EVPN_ETHSEGID(pNewAppSpecInfo),
                         MAX_LEN_ETH_SEG_ID);
            InParams.u1PrefixLen = BGP4_EVPN_MAX_KEY_SIZE;
            break;
        }
#endif

        default:
            return BGP4_FAILURE;
    }

    if (TrieRemove (&InParams, &OutParams, pNewAppSpecInfo) == TRIE_FAILURE)
    {
        return BGP4_FAILURE;
    }
    /* Route has been successfully deleted from the Trie. */
    *pu1IsBestRoute = (UINT1) OutParams.u4Data;
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4TrieCbDeleteEntry                                     */
/* Description   : This is a call back function to delete a route profile    */
/*                 entry from RIB                                            */
/*                 (Registered with TRIE library during TRIE creation)       */
/* Input(s)      : pInputParams - Pointer used by TRIE                       */
/*                 pOutputParams - void pointer (used by BGP)                */
/*                 ppAppSpecInfoList - address of the Application specific   */
/*                                 info present in TRIE leaf node            */
/*                 pAppSpecInfo - pointer to the application specific info   */
/*                                 that is going to be deleted               */
/* Output(s)     : ppAppSpecInfoList - address of the updated application    */
/*                                 specific info pointer                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4TrieCbDeleteEntry (tInputParams * pInputParams, VOID **ppAppSpecInfoList,
                       VOID *pOutputParams, VOID *pAppSpecInfo, tKey Key)
{
    tBgp4TrieOutParams *pBgpOutputParams = NULL;
#ifdef L3VPN
    UINT4               u4VrfId = BGP4_DFLT_VRFID;
#endif
    tRouteProfile     **ppDelRtProfileList = NULL;
    tRouteProfile      *pDelRtProfile = NULL;

    UNUSED_PARAM (Key);
    UNUSED_PARAM (pInputParams);
    pBgpOutputParams = (tBgp4TrieOutParams *) pOutputParams;
    ppDelRtProfileList = (tRouteProfile **) ppAppSpecInfoList;
    pDelRtProfile = (tRouteProfile *) pAppSpecInfo;
#ifdef L3VPN
    BGP4_VPN4_GET_PROCESS_VRFID (pDelRtProfile, u4VrfId);
    if (u4VrfId != BGP4_DFLT_VRFID)
    {
        tRtInstallVrfInfo  *pRtVrfInfo = NULL;

        Bgp4Vpnv4GetInstallVrfInfo (pDelRtProfile, u4VrfId, &pRtVrfInfo);
        pBgpOutputParams->u4Data =
            ((*ppDelRtProfileList == pDelRtProfile) &&
             ((pRtVrfInfo != NULL) &&
              (BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo) &
               BGP4_RT_BGP_VRF_BEST))) ? BGP4_TRUE : BGP4_FALSE;
    }
    else
#endif
    {
        if ((*ppDelRtProfileList == pDelRtProfile) &&
            (BGP4_RT_GET_FLAGS (pDelRtProfile) & BGP4_RT_BEST))
        {
            /* Input route is the best route */
            pBgpOutputParams->u4Data = BGP4_TRUE;
        }
        else
        {
            pBgpOutputParams->u4Data = BGP4_FALSE;
        }

    }
    if ((Bgp4DshRemoveRouteFromProfileList
         (pDelRtProfile, ppDelRtProfileList)) == BGP4_SUCCESS)
    {
        return BGP4_SUCCESS;
    }
    return BGP4_FAILURE;
}

/*****************************************************************************/
/* Function Name : Bgp4TrieLookupExactEntry                                  */
/* Description   : This function searches for a route profile                */
/*                 entry in RIB (Exact Match)                                */
/* Description   : This function calls TRIE specific API to search a route   */
/*                 profile entry from RIB                                    */
/* Input(s)      : pNewAppSpecInfo - pointer to the new application specific */
/*                                 info that is going to be searched in RIB  */
/*                 u4VrfId - VRF identifier in which route is to be searched */
/* Output(s)     : ppRtProfileList - pointer to the address of route profile */
/*                 stored in RIB                                             */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4TrieLookupExactEntry (tRouteProfile * pNewAppSpecInfo,
                          UINT4 u4VrfId,
                          tRouteProfile ** ppRtProfileList, VOID **ppRibNode)
{
    tInputParams        InParams;
    tBgp4TrieOutParams  OutParams;
#if (defined BGP4_IPV6_WANTED)
    tAddrPrefix         Prefix;
#endif
#if ((defined BGP4_IPV6_WANTED) || (defined L3VPN))
    tAddrPrefix         Mask;
#endif
    UINT4               au4Indx[BGP4_IPV4_KEY_ARR_SIZE];
    /* The Key of Trie is made-up 
     * of address and mask */
    UINT4               u4Prefix;
    UINT4               u4Mask;
    UINT4               u4AsafiMask;
    UINT1               au1ParamsKey[BGP4_INPUT_PARAMS_KEY_SIZE];
#ifdef VPLSADS_WANTED
    UINT1               au1VplsKey[BGP4_VPLS_MAX_KEY_SIZE];
    tVplsNlriInfo       NlriInfo;
#endif
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pNewAppSpecInfo),
                           BGP4_RT_SAFI_INFO (pNewAppSpecInfo), u4AsafiMask);
    MEMSET (au1ParamsKey, 0, BGP4_INPUT_PARAMS_KEY_SIZE);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
#endif
        {
            InParams.pRoot = BGP4_IPV4_RIB_TREE (u4VrfId);
            InParams.pLeafNode = NULL;
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.Key.pKey = (UINT1 *) &(au4Indx[BGP4_IPV4_KEY_ARR_INDX0]);

            PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pNewAppSpecInfo));
            InParams.u1PrefixLen = (UINT1) BGP4_RT_PREFIXLEN (pNewAppSpecInfo);
            u4Mask = Bgp4GetSubnetmask ((UINT1)
                                        BGP4_RT_PREFIXLEN (pNewAppSpecInfo));
            au4Indx[BGP4_IPV4_KEY_ARR_INDX0] = OSIX_HTONL (u4Prefix);
            au4Indx[BGP4_IPV4_KEY_ARR_INDX1] = OSIX_HTONL (u4Mask);
            break;
        }
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
        {
            InParams.pRoot = BGP4_IPV6_RIB_TREE (u4VrfId);
            InParams.pLeafNode = NULL;
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.Key.pKey = au1ParamsKey;
            MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Prefix),
                    BGP4_RT_IP_PREFIX (pNewAppSpecInfo), BGP4_IPV6_PREFIX_LEN);
            InParams.u1PrefixLen = (UINT1) BGP4_RT_PREFIXLEN (pNewAppSpecInfo);
            Bgp4GetIPV6Subnetmask (BGP4_RT_PREFIXLEN (pNewAppSpecInfo),
                                   &(Mask.au1Address[0]));
            MEMCPY (InParams.Key.pKey, Prefix.au1Address, BGP4_IPV6_PREFIX_LEN);
            Bgp4MaskIpv6Address (InParams.Key.pKey, Mask.au1Address);
            MEMCPY (InParams.Key.pKey + BGP4_IPV6_PREFIX_LEN,
                    Mask.au1Address, BGP4_IPV6_PREFIX_LEN);
            break;
        }
#endif
#ifdef L3VPN
        case CAP_MP_VPN4_UNICAST:
        {
            InParams.pRoot = BGP4_VPN4_RIB_TREE (u4VrfId);
            InParams.pLeafNode = NULL;
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.Key.pKey = au1ParamsKey;
            PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pNewAppSpecInfo));
            u4Prefix = OSIX_HTONL (u4Prefix);
            Bgp4GetVpn4Subnetmask (BGP4_RT_PREFIXLEN (pNewAppSpecInfo),
                                   &(Mask.au1Address[0]));
            MEMCPY (InParams.Key.pKey,
                    BGP4_RT_ROUTE_DISTING (pNewAppSpecInfo),
                    BGP4_VPN4_ROUTE_DISTING_SIZE);
            MEMCPY (InParams.Key.pKey + BGP4_VPN4_ROUTE_DISTING_SIZE,
                    (UINT1 *) &u4Prefix, BGP4_IPV4_PREFIX_LEN);
            Bgp4MaskVpn4Address (InParams.Key.pKey, Mask.au1Address);
            MEMCPY (InParams.Key.pKey + BGP4_VPN4_PREFIX_LEN,
                    Mask.au1Address, BGP4_VPN4_PREFIX_LEN);
            InParams.u1PrefixLen =
                (UINT1) ((BGP4_VPN4_ROUTE_DISTING_SIZE *
                          BGP4_ONE_BYTE_BITS) +
                         BGP4_RT_PREFIXLEN (pNewAppSpecInfo));
            break;
        }
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
        {
            InParams.pRoot = BGP4_VPLS_RIB_TREE (u4VrfId);
            InParams.pLeafNode = NULL;
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            /*For L2VPN_VPLS, the Key is RD+VEID+VBO */
            InParams.Key.pKey = au1VplsKey;

            Bgp4VplsGetNlriInfoFromNetAddr (BGP4_RT_IP_PREFIX (pNewAppSpecInfo),
                                            &NlriInfo);
            MEMCPY (InParams.Key.pKey,
                    NlriInfo.au1RouteDistinguisher, MAX_LEN_RD_VALUE);
            MEMCPY (InParams.Key.pKey + MAX_LEN_RD_VALUE,
                    &NlriInfo.u2VeId, BGP4_VPLS_NLRI_VEID_LEN);
            MEMCPY (InParams.Key.pKey + MAX_LEN_RD_VALUE +
                    BGP4_VPLS_NLRI_VEID_LEN, &NlriInfo.u2VeBaseOffset,
                    BGP4_VPLS_NLRI_VBO_LEN);

            InParams.u1PrefixLen = BGP4_VPLS_MAX_KEY_SIZE;
            break;
        }
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
        {
            InParams.pRoot = BGP4_EVPN_RIB_TREE (u4VrfId);
            InParams.pLeafNode = NULL;
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            /*For EVPN, the Key is RD+VNID+MAC_ETHSEGID Addr */
            InParams.Key.pKey = au1ParamsKey;

            MEMCPY (InParams.Key.pKey,
                    BGP4_EVPN_RD_DISTING (pNewAppSpecInfo),
                    BGP4_EVPN_ROUTE_DISTING_SIZE);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE,
                    &(BGP4_EVPN_VNID(pNewAppSpecInfo)),MAX_LEN_VNI_VALUE);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE + MAX_LEN_VNI_VALUE,
                   BGP4_EVPN_MACADDR(pNewAppSpecInfo), MAX_LEN_MAC_ADDR);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE + MAX_LEN_VNI_VALUE
                     + MAX_LEN_MAC_ADDR,BGP4_EVPN_ETHSEGID(pNewAppSpecInfo),
                         MAX_LEN_ETH_SEG_ID);
            InParams.u1PrefixLen = BGP4_EVPN_MAX_KEY_SIZE;
            break;
        }
#endif

        default:
            *ppRibNode = NULL;
            return BGP4_FAILURE;
    }

    if (TrieSearch (&InParams, &OutParams, ppRibNode) == TRIE_FAILURE)
    {
        *ppRibNode = NULL;
        return BGP4_FAILURE;
    }
    *ppRtProfileList = OutParams.pRtProfile;
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4TrieQueryIndirectNextHOPInCxt                         */
/* Description   : This function is an API to query the indirect BGP NextHop
 *                    for given route.                                       */
/* Input(s)      : pPrefix - pointer to the Prefix of Destination            */
/*                 u2PrefixLen  - prefix length                              */
/*                 pu1NextHop - Immedeate next hop                           */
/* Output(s)     : NextHop - pointer to the address of IndirectNextHop       */
/*****************************************************************************/

INT4
Bgp4TrieQueryIndirectNextHOPInCxt (UINT4 u4VrfId,
                                   tSNMP_OCTET_STRING_TYPE * pPrefix,
                                   UINT2 u2PrefixLen, UINT1 *pu1NextHop)
{
    VOID               *pRibNode = NULL;
    tRouteProfile       RtProfile;
    tRouteProfile      *pOutRtProfile = NULL;
    UINT1               au1NetAddress[MAX_ADDR_LEN];
    tRouteProfile      *pTempRtProfile = NULL;

    MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }
    MEMSET (&RtProfile, 0, sizeof (tRouteProfile));
    MEMCPY (BGP4_RT_IP_PREFIX ((&RtProfile)), pPrefix->pu1_OctetList,
            pPrefix->i4_Length);
    BGP4_RT_PREFIXLEN ((&RtProfile)) = u2PrefixLen;
    if (pPrefix->i4_Length == BGP4_IPV4_PREFIX_LEN)
    {
        BGP4_RT_AFI_INFO ((&RtProfile)) = BGP4_INET_AFI_IPV4;
        BGP4_RT_SAFI_INFO ((&RtProfile)) = BGP4_INET_SAFI_UNICAST;

    }
#ifdef BGP4_IPV6_WANTED
    else
    {
        BGP4_RT_AFI_INFO ((&RtProfile)) = BGP4_INET_AFI_IPV6;
        BGP4_RT_SAFI_INFO ((&RtProfile)) = BGP4_INET_SAFI_UNICAST;
    }
#endif
    MEMCPY (au1NetAddress, pu1NextHop, pPrefix->i4_Length);

    BgpLock ();
    Bgp4TrieLookupExactEntry (&RtProfile, u4VrfId, &pOutRtProfile, &pRibNode);
    BgpUnLock ();
    if (pOutRtProfile == NULL)
    {
        MEMSET (pu1NextHop, 0, (size_t) pPrefix->i4_Length);
        return BGP4_FAILURE;
    }

    pTempRtProfile = pOutRtProfile;

    while (pTempRtProfile != NULL)
    {
        if ((MEMCMP
             (BGP4_RT_IMMEDIATE_NEXTHOP (pTempRtProfile), au1NetAddress,
              (size_t) pPrefix->i4_Length) == 0))
        {
            break;
        }
        pTempRtProfile = BGP4_RT_NEXT (pTempRtProfile);
    }
    if (pTempRtProfile == NULL)
    {
        MEMSET (pu1NextHop, 0, (size_t) pPrefix->i4_Length);
        return BGP4_FAILURE;
    }
    MEMCPY (pu1NextHop, pTempRtProfile->pRtInfo->NextHopInfo.au1Address,
            pPrefix->i4_Length);

    return BGP4_SUCCESS;

}

/*****************************************************************************/
/* Function Name : Bgp4TrieQueryIndirectNextHOP                              */
/* Description   : This function is an API to query the indirect BGP NextHop
 *                    for given route.                                       */
/* Input(s)      : pPrefix - pointer to the Prefix of Destination            */
/*                 u2PrefixLen  - prefix length                              */
/* Output(s)     : NextHop - pointer to the address of IndirectNextHop       */
/*****************************************************************************/

INT4
Bgp4TrieQueryIndirectNextHOP (tSNMP_OCTET_STRING_TYPE * pPrefix,
                              UINT2 u2PrefixLen, UINT1 *pu1NextHop)
{
    return Bgp4TrieQueryIndirectNextHOPInCxt (BGP4_DFLT_VRFID, pPrefix,
                                              u2PrefixLen, pu1NextHop);
}

/*****************************************************************************/
/* Function Name : Bgp4TrieCbLookupExactEntry                                */
/* Description   : This is a call back function to search for a route profile*/
/*                 entry in RIB (Exact Match)                                */
/*                 (Registered with TRIE library during TRIE creation)       */
/* Input(s)      : pInputParams - void pointer used by TRIE                  */
/*                 pOutputParams - void pointer (used by BGP)                */
/*                 pAppSpecInfo - address of the Application (BGP) specific  */
/*                                 info present in TRIE leaf node            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4TrieCbLookupExactEntry (tInputParams * pInputParams,
                            tBgp4TrieOutParams * pOutParams, VOID *pAppSpecInfo)
{
    UNUSED_PARAM (pInputParams);
    pOutParams->pRtProfile = (tRouteProfile *) pAppSpecInfo;
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4TrieLookupOverlapEntry                                */
/* Description   : This is a call back function to search for a route profile*/
/*                 entry in RIB (First Overlap Route [could be either        */
/*                                        less/more specific route]          */
/* Description   : This function calls TRIE specific API                     */
/* Input(s)      : pNewAppSpecInfo - pointer to the new application specific */
/*                                 info that is going to be searched in RIB  */
/*                 u4VrfId - VRF identifier in which route is to be searched */
/* Output(s)     : ppRtProfileList - pointer to the address of route profile */
/*                 stored in RIB                                             */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4TrieLookupOverlapEntry (tRouteProfile * pNewAppSpecInfo,
                            UINT4 u4VrfId,
                            tRouteProfile ** ppRtProfileList, VOID **ppRibNode)
{
    tInputParams        InParams;
    tBgp4TrieOutParams  OutParams;
#if (defined BGP4_IPV6_WANTED)
    tAddrPrefix         Prefix;
    UINT1               u1MaskFound = BGP4_FALSE;
    UINT1               u1Index = 0;
#endif
#if ((defined BGP4_IPV6_WANTED) || (defined L3VPN))
    tAddrPrefix         Mask;
#endif
    UINT4               au4Indx[BGP4_IPV4_KEY_ARR_SIZE];
    /* The Key of Trie is made-up 
     * of address and mask */
    UINT4               u4Prefix;
    UINT4               u4TmpPrefix;
    UINT4               u4TmpMask = 0;
    UINT4               u4Mask;
    UINT4               u4AsafiMask;
    UINT1               u1TmpPrefixLen = 0;
    UINT1               au1ParamsKey[BGP4_INPUT_PARAMS_KEY_SIZE];

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pNewAppSpecInfo),
                           BGP4_RT_SAFI_INFO (pNewAppSpecInfo), u4AsafiMask);
    MEMSET (au1ParamsKey, 0, BGP4_INPUT_PARAMS_KEY_SIZE);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
#endif
        {
            InParams.pRoot = BGP4_IPV4_RIB_TREE (u4VrfId);
            InParams.pLeafNode = NULL;
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.Key.pKey = (UINT1 *) &(au4Indx[BGP4_IPV4_KEY_ARR_INDX0]);

            PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pNewAppSpecInfo));
            u4TmpPrefix = u4Prefix;
            u4TmpMask = 0x80000000;
            u1TmpPrefixLen = 1;
            for (;;)
            {
                if (((u4TmpPrefix & u4TmpMask) == u4TmpMask) &&
                    (u1TmpPrefixLen != 1))
                {
                    break;
                }

                u4TmpMask = u4TmpMask >> BGP4_ONE_BYTE;
                u1TmpPrefixLen++;

                if (u1TmpPrefixLen == BGP4_MAX_PREFIXLEN)
                {
                    break;
                }
            }
            u4Mask = Bgp4GetSubnetmask (u1TmpPrefixLen);
            u4Prefix &= u4Mask;
            au4Indx[BGP4_IPV4_KEY_ARR_INDX0] = OSIX_HTONL (u4Prefix);
            au4Indx[BGP4_IPV4_KEY_ARR_INDX1] = OSIX_HTONL (u4Mask);
            InParams.u1PrefixLen = (UINT1) BGP4_RT_PREFIXLEN (pNewAppSpecInfo);
            break;
        }

#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
        {
            MEMSET (&Prefix, 0, sizeof (tAddrPrefix));

            InParams.pRoot = BGP4_IPV6_RIB_TREE (u4VrfId);
            InParams.pLeafNode = NULL;
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.Key.pKey = au1ParamsKey;
            MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Prefix),
                    BGP4_RT_IP_PREFIX (pNewAppSpecInfo), BGP4_IPV6_PREFIX_LEN);
            u1TmpPrefixLen = 1;
            for (;;)
            {
                PTR_FETCH_4 (u4TmpPrefix, (u1Index +
                                           BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                                           (Prefix)));
                u4TmpMask = 0x80000000;
                for (;;)
                {
                    if ((u4TmpPrefix & u4TmpMask) == u4TmpMask)
                    {
                        if (!((u1TmpPrefixLen == 1) && (u1Index == 0)))
                        {
                            u1MaskFound = BGP4_TRUE;
                            break;
                        }
                    }

                    u4TmpMask = u4TmpMask >> BGP4_ONE_BYTE;
                    u1TmpPrefixLen++;

                    if (u1TmpPrefixLen == BGP4_MAX_PREFIXLEN)
                    {
                        break;
                    }
                }
                if (u1MaskFound == BGP4_TRUE)
                {
                    break;
                }

                u1Index += 4;
                if (u1Index >= BGP4_IPV6_PREFIX_LEN)
                {
                    break;
                }
            }
            Bgp4GetIPV6Subnetmask (u1TmpPrefixLen, &(Mask.au1Address[0]));
            MEMCPY (InParams.Key.pKey, Prefix.au1Address, BGP4_IPV6_PREFIX_LEN);
            Bgp4MaskIpv6Address (InParams.Key.pKey, Mask.au1Address);
            MEMCPY (InParams.Key.pKey + BGP4_IPV6_PREFIX_LEN,
                    Mask.au1Address, BGP4_IPV6_PREFIX_LEN);
            InParams.u1PrefixLen = (UINT1) BGP4_RT_PREFIXLEN (pNewAppSpecInfo);
            break;
        }
#endif
#ifdef L3VPN
        case CAP_MP_VPN4_UNICAST:
        {
            InParams.pRoot = BGP4_VPN4_RIB_TREE (u4VrfId);
            InParams.pLeafNode = NULL;
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.Key.pKey = au1ParamsKey;
            PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pNewAppSpecInfo));
            u4TmpPrefix = u4Prefix;
            u4TmpMask = 0x80000000;
            u1TmpPrefixLen = 1;
            for (;;)
            {
                if (((u4TmpPrefix & u4TmpMask) == u4TmpMask) &&
                    (u1TmpPrefixLen != 1))
                {
                    break;
                }
                u4TmpMask = u4TmpMask >> BGP4_ONE_BYTE;
                u1TmpPrefixLen++;
                if (u1TmpPrefixLen == BGP4_MAX_PREFIXLEN)
                {
                    break;
                }
            }
            u4Prefix = OSIX_HTONL (u4Prefix);
            Bgp4GetVpn4Subnetmask (BGP4_RT_PREFIXLEN (pNewAppSpecInfo),
                                   &(Mask.au1Address[0]));
            MEMCPY (InParams.Key.pKey,
                    BGP4_RT_ROUTE_DISTING (pNewAppSpecInfo),
                    BGP4_VPN4_ROUTE_DISTING_SIZE);
            MEMCPY (InParams.Key.pKey + BGP4_VPN4_ROUTE_DISTING_SIZE,
                    (UINT1 *) &u4Prefix, BGP4_IPV4_PREFIX_LEN);
            Bgp4MaskVpn4Address (InParams.Key.pKey, Mask.au1Address);
            MEMCPY (InParams.Key.pKey + BGP4_VPN4_PREFIX_LEN,
                    Mask.au1Address, BGP4_VPN4_PREFIX_LEN);
            InParams.u1PrefixLen =
                (UINT1) ((BGP4_VPN4_ROUTE_DISTING_SIZE *
                          BGP4_ONE_BYTE_BITS) +
                         BGP4_RT_PREFIXLEN (pNewAppSpecInfo));
            break;
        }
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
        {
            InParams.pRoot = BGP4_EVPN_RIB_TREE (u4VrfId);
            InParams.pLeafNode = NULL;
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            /*For EVPN, the Key is RD+VNID+MAC+ETHSEGID Addr */
            InParams.Key.pKey = au1ParamsKey;

            MEMCPY (InParams.Key.pKey,
                    BGP4_EVPN_RD_DISTING (pNewAppSpecInfo),
                    BGP4_EVPN_ROUTE_DISTING_SIZE);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE,
                    &(BGP4_EVPN_VNID(pNewAppSpecInfo)),MAX_LEN_VNI_VALUE);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE + MAX_LEN_VNI_VALUE,
                   BGP4_EVPN_MACADDR(pNewAppSpecInfo), MAX_LEN_MAC_ADDR);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE + MAX_LEN_VNI_VALUE
                     + MAX_LEN_MAC_ADDR,BGP4_EVPN_ETHSEGID(pNewAppSpecInfo),
                         MAX_LEN_ETH_SEG_ID);
            InParams.u1PrefixLen = BGP4_EVPN_MAX_KEY_SIZE;
            break;
        }
#endif

        default:
            *ppRibNode = NULL;
            return BGP4_FAILURE;
    }

    if (TrieLookupOverlapLessMoreSpecific (&InParams, &OutParams, ppRibNode) ==
        TRIE_FAILURE)
    {
        *ppRibNode = NULL;
        return BGP4_FAILURE;
    }
    *ppRtProfileList = OutParams.pRtProfile;
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4TrieCbLookupOverlapEntry                              */
/* Description   : This is a call back function to search for a route profile*/
/*                 entry in RIB (First Overlap Route [could be either        */
/*                                        less/more specific route]          */
/*                 (Registered with TRIE library during TRIE creation)       */
/* Input(s)      : pInputParams - void pointer used by TRIE                  */
/*                 pOutputParams - void pointer (used by BGP)                */
/*                 ppAppSpecInfo - address of the Application (BGP) specific */
/*                                 info present in TRIE leaf node            */
/*                 u2KeySize - key size used by TRIE                         */
/*                 Key - key used by TRIE                                    */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4TrieCbLookupOverlapEntry (tInputParams * pInputParams,
                              tBgp4TrieOutParams * pOutParams,
                              VOID *pAppSpecInfo, UINT2 u2KeySize, tKey Key)
{
    UNUSED_PARAM (u2KeySize);
    UNUSED_PARAM (Key);
    UNUSED_PARAM (pInputParams);
    pOutParams->pRtProfile = (tRouteProfile *) pAppSpecInfo;
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4TrieGetFirstEntry                                     */
/* Description   : This function gets the first route entry in RIB           */
/* Input(s)      : u4AsafiMask - address, subaddress family identifier mask  */
/*                 pRibNode - for VPN, the RIB pointer must be the input     */
/*                 i4Flag - indicates whether the TRIE SEM to be taken or not*/
/* Output(s)     : ppAppSpecInfo - address of the Application (BGP) specific */
/*                                 info present in TRIE leaf node            */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4TrieGetFirstEntry (UINT4 u4AsafiMask,
                       tRouteProfile ** ppAppSpecInfo, VOID **pRibNode,
                       INT4 i4Flag)
{
    tInputParams        InParams;
    UINT4               au4Indx[BGP4_IPV4_KEY_ARR_SIZE];
    /* The Key of Trie is made-up 
     * of address and mask */
    UINT1               au1ParamsKey[BGP4_INPUT_PARAMS_KEY_SIZE];
#ifdef VPLSADS_WANTED
    UINT1               au1VplsKey[BGP4_VPLS_MAX_KEY_SIZE];
    MEMSET (au1VplsKey, 0, BGP4_VPLS_MAX_KEY_SIZE);
#endif

    MEMSET (au1ParamsKey, 0, BGP4_INPUT_PARAMS_KEY_SIZE);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
#endif
        {
            InParams.pRoot = (*pRibNode);
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.Key.pKey = (UINT1 *) &(au4Indx[BGP4_IPV4_KEY_ARR_INDX0]);
            break;
        }
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
        {
            InParams.pRoot = (*pRibNode);
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.Key.pKey = au1ParamsKey;
            MEMSET (InParams.Key.pKey, 0,
                    (sizeof (UINT2) * BGP4_IPV6_PREFIX_LEN));
            break;
        }
#endif
#ifdef L3VPN
        case CAP_MP_VPN4_UNICAST:
        {
            InParams.pRoot = (*pRibNode);
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            /* default vpn-rib */
            /* Application ID is always Protocol ID - 1 */
            InParams.Key.pKey = au1ParamsKey;
            MEMSET (InParams.Key.pKey, 0,
                    (sizeof (UINT2) * BGP4_VPN4_PREFIX_LEN));
            break;
        }
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
        {
            InParams.pRoot = (*pRibNode);
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.Key.pKey = au1VplsKey;
            MEMSET (InParams.Key.pKey, 0, BGP4_VPLS_MAX_KEY_SIZE);
            break;
        }
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
        {
            InParams.pRoot = (*pRibNode);
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            /* default Evpn-rib */
            /* Application ID is always Protocol ID - 1 */
            InParams.Key.pKey = au1ParamsKey;
            MEMSET (InParams.Key.pKey, 0,
                          BGP4_EVPN_MAX_KEY_SIZE);
            break;
        }
#endif

        default:
            return BGP4_FAILURE;
    }
    i4Flag = TrieGetFirstNode (&InParams, (VOID *) ppAppSpecInfo, pRibNode);
    if (i4Flag >= TRIE_SUCCESS)
    {
        return BGP4_SUCCESS;
    }
    *pRibNode = NULL;
    return BGP4_FAILURE;
}

/*****************************************************************************/
/* Function Name : Bgp4TrieGetNextEntry                                      */
/* Description   : This function gets the next route entry in RIB by giving  */
/*                 the first/previous route entry RIB node address           */
/* Input(s)      : pRibNode - first/previous route entry RIB node address    */
/*                 pRtProfile - route for which next entry is to be found    */
/*                 u4VrfId - VRF identifier in which operations are made.    */
/* Output(s)     : ppAppSpecInfo - address of the Application (BGP) specific */
/*                                 info present in TRIE leaf node            */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4TrieGetNextEntry (tRouteProfile * pRtProfile, UINT4 u4VrfId,
                      tRouteProfile ** ppAppSpecInfo,
                      VOID **pRibNode, INT4 i4Flag)
{
    tInputParams        InParams;
    UINT4               au4Indx[BGP4_IPV4_KEY_ARR_SIZE];
    /* The Key of Trie is made-up 
     * of address and mask */
    UINT4               u4Prefix;
    UINT4               u4Mask;
#if (defined BGP4_IPV6_WANTED)
    tAddrPrefix         Prefix;
#endif
#if ((defined BGP4_IPV6_WANTED) || (defined L3VPN))
    tAddrPrefix         Mask;
#endif
    UINT4               u4AsafiMask;
    UINT1               au1ParamsKey[BGP4_INPUT_PARAMS_KEY_SIZE];
    INT4                i4Ret;
#ifdef VPLSADS_WANTED
    UINT1               au1VplsKey[BGP4_VPLS_MAX_KEY_SIZE];
    tVplsNlriInfo       NlriInfo;
#endif
    UNUSED_PARAM (i4Flag);
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
    MEMSET (au1ParamsKey, 0, BGP4_INPUT_PARAMS_KEY_SIZE);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
#endif
        {
            InParams.pRoot = BGP4_IPV4_RIB_TREE (u4VrfId);
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.Key.pKey = (UINT1 *) &(au4Indx[BGP4_IPV4_KEY_ARR_INDX0]);
            PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pRtProfile));
            u4Mask = Bgp4GetSubnetmask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile));
            au4Indx[BGP4_IPV4_KEY_ARR_INDX0] = OSIX_HTONL (u4Prefix);
            au4Indx[BGP4_IPV4_KEY_ARR_INDX1] = OSIX_HTONL (u4Mask);
            break;
        }
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
        {
            InParams.pRoot = BGP4_IPV6_RIB_TREE (u4VrfId);
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.Key.pKey = au1ParamsKey;
            MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Prefix),
                    BGP4_RT_IP_PREFIX (pRtProfile), BGP4_IPV6_PREFIX_LEN);
            Bgp4GetIPV6Subnetmask (BGP4_RT_PREFIXLEN (pRtProfile),
                                   &(Mask.au1Address[0]));
            MEMCPY (InParams.Key.pKey, Prefix.au1Address, BGP4_IPV6_PREFIX_LEN);
            Bgp4MaskIpv6Address (InParams.Key.pKey, Mask.au1Address);
            MEMCPY (InParams.Key.pKey + BGP4_IPV6_PREFIX_LEN,
                    Mask.au1Address, BGP4_IPV6_PREFIX_LEN);
            break;
        }
#endif
#ifdef L3VPN
        case CAP_MP_VPN4_UNICAST:
        {
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.pRoot = BGP4_VPN4_RIB_TREE (u4VrfId);
            /* default vpn-rib */
            InParams.Key.pKey = au1ParamsKey;
            PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pRtProfile));
            u4Prefix = OSIX_HTONL (u4Prefix);
            Bgp4GetVpn4Subnetmask (BGP4_RT_PREFIXLEN (pRtProfile),
                                   &(Mask.au1Address[0]));
            MEMCPY (InParams.Key.pKey,
                    BGP4_RT_ROUTE_DISTING (pRtProfile),
                    BGP4_VPN4_ROUTE_DISTING_SIZE);
            MEMCPY (InParams.Key.pKey + BGP4_VPN4_ROUTE_DISTING_SIZE,
                    (UINT1 *) &u4Prefix, BGP4_IPV4_PREFIX_LEN);
            Bgp4MaskVpn4Address (InParams.Key.pKey, Mask.au1Address);
            MEMCPY (InParams.Key.pKey + BGP4_VPN4_PREFIX_LEN,
                    Mask.au1Address, BGP4_VPN4_PREFIX_LEN);
            break;
        }
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
        {
            InParams.pRoot = BGP4_VPLS_RIB_TREE (u4VrfId);
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            /*For L2VPN_VPLS, the Key is RD+VEID+VBO */
            InParams.Key.pKey = au1VplsKey;

            Bgp4VplsGetNlriInfoFromNetAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            &NlriInfo);
            MEMCPY (InParams.Key.pKey,
                    NlriInfo.au1RouteDistinguisher, MAX_LEN_RD_VALUE);
            MEMCPY (InParams.Key.pKey + MAX_LEN_RD_VALUE,
                    &NlriInfo.u2VeId, BGP4_VPLS_NLRI_VEID_LEN);
            MEMCPY (InParams.Key.pKey + MAX_LEN_RD_VALUE +
                    BGP4_VPLS_NLRI_VEID_LEN, &NlriInfo.u2VeBaseOffset,
                    BGP4_VPLS_NLRI_VBO_LEN);

            InParams.u1PrefixLen = BGP4_VPLS_MAX_KEY_SIZE;
            break;
        }
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
        {
            InParams.pRoot = BGP4_EVPN_RIB_TREE (u4VrfId);
            InParams.pLeafNode = NULL;
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            /*For EVPN, the Key is RD+VNID+MAC+ETHSEGID Addr */
            InParams.Key.pKey = au1ParamsKey;

            MEMCPY (InParams.Key.pKey,
                    BGP4_EVPN_RD_DISTING (pRtProfile),
                    BGP4_EVPN_ROUTE_DISTING_SIZE);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE,
                    &(BGP4_EVPN_VNID(pRtProfile)),MAX_LEN_VNI_VALUE);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE + MAX_LEN_VNI_VALUE,
                   BGP4_EVPN_MACADDR(pRtProfile), MAX_LEN_MAC_ADDR);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE + MAX_LEN_VNI_VALUE
                     + MAX_LEN_MAC_ADDR,BGP4_EVPN_ETHSEGID(pRtProfile),
                         MAX_LEN_ETH_SEG_ID);

            InParams.u1PrefixLen = BGP4_EVPN_MAX_KEY_SIZE;
            break;
        }
#endif

        default:
            *pRibNode = NULL;
            return BGP4_FAILURE;
    }

    i4Ret = TrieGetNextNode (&InParams, *pRibNode,
                             (VOID *) ppAppSpecInfo, pRibNode);
    if (i4Ret >= TRIE_SUCCESS)
    {
        return BGP4_SUCCESS;
    }
    *pRibNode = NULL;
    return BGP4_FAILURE;
}

/*****************************************************************************/
/* Function Name : Bgp4TrieGetPrevEntry                                      */
/* Description   : This function gets the prev route entry in RIB by giving  */
/*                 the previous/next route entry RIB node address            */
/* Input(s)      : pRibNode - previous/next route entry RIB node address     */
/*                 pRtProfile - route for which previous entry is to be found*/
/*                 u4VrfId - VRF identifier in which operations are made.    */
/* Output(s)     : ppAppSpecInfo - address of the Application (BGP) specific */
/*                                 info present in TRIE leaf node            */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4TrieGetPrevEntry (tRouteProfile * pRtProfile,
                      tRouteProfile ** ppAppSpecInfo, VOID **pRibNode)
{
    VOID               *pTrieCtx = NULL;
    tInputParams        InParams;
    UINT4               au4Indx[BGP4_IPV4_KEY_ARR_SIZE];
    /* The Key of Trie is made-up 
     * of address and mask */
    UINT4               u4Prefix;
    UINT4               u4Mask;
#if (defined BGP4_IPV6_WANTED)
    tAddrPrefix         Prefix;
#endif
#if ((defined BGP4_IPV6_WANTED) || (defined L3VPN))
    tAddrPrefix         Mask;
#endif
    UINT4               u4AsafiMask;
    UINT1               au1ParamsKey[BGP4_INPUT_PARAMS_KEY_SIZE];
    INT4                i4Flag;
#ifdef VPLSADS_WANTED
    UINT1               au1VplsKey[BGP4_VPLS_MAX_KEY_SIZE];
    tVplsNlriInfo       NlriInfo;
#endif

    if (*pRibNode == NULL)
    {
        /* This value MUST NOT be null */
        return BGP4_FAILURE;
    }
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);

    MEMSET (au1ParamsKey, 0, BGP4_INPUT_PARAMS_KEY_SIZE);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
#endif
        {
            InParams.pRoot = BGP4_IPV4_RIB_TREE (BGP4_RT_CXT_ID (pRtProfile));
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.Key.pKey = (UINT1 *) &(au4Indx[BGP4_IPV4_KEY_ARR_INDX0]);
            PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pRtProfile));
            u4Mask = Bgp4GetSubnetmask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile));
            au4Indx[BGP4_IPV4_KEY_ARR_INDX0] = OSIX_HTONL (u4Prefix);
            au4Indx[BGP4_IPV4_KEY_ARR_INDX1] = OSIX_HTONL (u4Mask);
            break;
        }
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
        {
            InParams.pRoot = BGP4_IPV6_RIB_TREE (BGP4_RT_CXT_ID (pRtProfile));
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.Key.pKey = au1ParamsKey;
            MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Prefix),
                    BGP4_RT_IP_PREFIX (pRtProfile), BGP4_IPV6_PREFIX_LEN);
            Bgp4GetIPV6Subnetmask (BGP4_RT_PREFIXLEN (pRtProfile),
                                   &(Mask.au1Address[0]));
            MEMCPY (InParams.Key.pKey, Prefix.au1Address, BGP4_IPV6_PREFIX_LEN);
            Bgp4MaskIpv6Address (InParams.Key.pKey, Mask.au1Address);
            MEMCPY (InParams.Key.pKey + BGP4_IPV6_PREFIX_LEN,
                    Mask.au1Address, BGP4_IPV6_PREFIX_LEN);
            break;
        }
#endif
#ifdef L3VPN
        case CAP_MP_VPN4_UNICAST:
        {
            InParams.pRoot = BGP4_VPN4_RIB_TREE (BGP4_RT_CXT_ID (pRtProfile));
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            /* default vpn-rib */
            InParams.Key.pKey = au1ParamsKey;
            PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pRtProfile));
            u4Prefix = OSIX_HTONL (u4Prefix);
            Bgp4GetVpn4Subnetmask (BGP4_RT_PREFIXLEN (pRtProfile),
                                   &(Mask.au1Address[0]));
            MEMCPY (InParams.Key.pKey,
                    BGP4_RT_ROUTE_DISTING (pRtProfile),
                    BGP4_VPN4_ROUTE_DISTING_SIZE);
            MEMCPY (InParams.Key.pKey + BGP4_VPN4_ROUTE_DISTING_SIZE,
                    (UINT1 *) &u4Prefix, BGP4_IPV4_PREFIX_LEN);
            Bgp4MaskVpn4Address (InParams.Key.pKey, Mask.au1Address);
            MEMCPY (InParams.Key.pKey + BGP4_VPN4_PREFIX_LEN,
                    Mask.au1Address, BGP4_VPN4_PREFIX_LEN);
            break;
        }
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
        {
            InParams.pRoot = BGP4_VPLS_RIB_TREE (BGP4_RT_CXT_ID (pRtProfile));
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            /*For L2VPN_VPLS, the Key is RD+VEID+VBO */
            InParams.Key.pKey = au1VplsKey;

            Bgp4VplsGetNlriInfoFromNetAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            &NlriInfo);
            MEMCPY (InParams.Key.pKey,
                    NlriInfo.au1RouteDistinguisher, MAX_LEN_RD_VALUE);
            MEMCPY (InParams.Key.pKey + MAX_LEN_RD_VALUE,
                    &NlriInfo.u2VeId, BGP4_VPLS_NLRI_VEID_LEN);
            MEMCPY (InParams.Key.pKey + MAX_LEN_RD_VALUE +
                    BGP4_VPLS_NLRI_VEID_LEN, &NlriInfo.u2VeBaseOffset,
                    BGP4_VPLS_NLRI_VBO_LEN);

            InParams.u1PrefixLen = BGP4_VPLS_MAX_KEY_SIZE;
            break;
        }
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
        {
            InParams.pRoot = BGP4_EVPN_RIB_TREE (BGP4_RT_CXT_ID (pRtProfile));
            InParams.pLeafNode = NULL;
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            /*For EVPN, the Key is RD+VNID+MAC Addr */
            InParams.Key.pKey = au1ParamsKey;

            MEMCPY (InParams.Key.pKey,
                    BGP4_EVPN_RD_DISTING (pRtProfile),
                    BGP4_EVPN_ROUTE_DISTING_SIZE);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE,
                    &(BGP4_EVPN_VNID(pRtProfile)),MAX_LEN_VNI_VALUE);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE + MAX_LEN_VNI_VALUE,
                   BGP4_EVPN_MACADDR(pRtProfile), MAX_LEN_MAC_ADDR);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE + MAX_LEN_VNI_VALUE
                     + MAX_LEN_MAC_ADDR,BGP4_EVPN_ETHSEGID(pRtProfile),
                         MAX_LEN_ETH_SEG_ID);
            InParams.u1PrefixLen = BGP4_EVPN_MAX_KEY_SIZE;
            break;
        }
#endif

        default:
            *pRibNode = NULL;
            return BGP4_FAILURE;
    }
    i4Flag = TrieGetPrevNode (&InParams, *pRibNode,
                              (VOID *) ppAppSpecInfo, &pTrieCtx);
    if (i4Flag >= TRIE_SUCCESS)
    {
        *pRibNode = pTrieCtx;
        return BGP4_SUCCESS;
    }
    *pRibNode = NULL;
    return BGP4_FAILURE;
}

/*****************************************************************************/
/* Function Name : Bgp4TrieTraverseAndGetNextEntry                           */
/* Description   : This function will give you the next route entry present  */
/*                 in RIB for a specified route entry by traversing the RIB  */
/*                 for specified route and then gives the next entry         */
/* Input(s)      : pRtProfile - route entry for which the next route info    */
/*                 is to be got from RIB.                                    */
/* Output(s)     : pRtProfileList - pointer to the next route information    */
/*                 in RIB corresponding to input pRtProfile                  */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4TrieTraverseAndGetNextEntry (tRouteProfile * pRtProfile,
                                 VOID *pRibNode,
                                 tRouteProfile ** ppRtProfileList)
{
    tInputParams        InParams;
    tBgp4TrieOutParams *RtProfile;
    VOID               *pArg;
#if (defined BGP4_IPV6_WANTED)
    tAddrPrefix         Prefix;
#endif
#if ((defined BGP4_IPV6_WANTED) || (defined L3VPN))
    tAddrPrefix         Mask;
#endif
    UINT4               au4Indx[BGP4_IPV4_KEY_ARR_SIZE];
    /* The Key of Trie is made-up 
     * of address and mask */
    UINT4               u4Prefix;
    UINT4               u4Mask;
    UINT4               u4AsafiMask;
    UINT1               au1ParamsKey[BGP4_INPUT_PARAMS_KEY_SIZE];
    tOutputParams       OutputParams;
#ifdef VPLSADS_WANTED
    UINT1               au1VplsKey[BGP4_VPLS_MAX_KEY_SIZE];
    tVplsNlriInfo       NlriInfo;
#endif

    UNUSED_PARAM (pRibNode);
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
    MEMSET (au1ParamsKey, 0, BGP4_INPUT_PARAMS_KEY_SIZE);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
#endif
        {
            InParams.pRoot = BGP4_IPV4_RIB_TREE (BGP4_RT_CXT_ID (pRtProfile));
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.pLeafNode = NULL;
            InParams.Key.pKey = (UINT1 *) &(au4Indx[BGP4_IPV4_KEY_ARR_INDX0]);

            PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pRtProfile));
            u4Mask = Bgp4GetSubnetmask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile));
            au4Indx[BGP4_IPV4_KEY_ARR_INDX0] = OSIX_HTONL (u4Prefix);
            au4Indx[BGP4_IPV4_KEY_ARR_INDX1] = OSIX_HTONL (u4Mask);
            break;
        }
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
        {
            InParams.pRoot = BGP4_IPV6_RIB_TREE (BGP4_RT_CXT_ID (pRtProfile));
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.pLeafNode = NULL;
            InParams.Key.pKey = au1ParamsKey;
            MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Prefix),
                    BGP4_RT_IP_PREFIX (pRtProfile), BGP4_IPV6_PREFIX_LEN);
            Bgp4GetIPV6Subnetmask (BGP4_RT_PREFIXLEN (pRtProfile),
                                   &(Mask.au1Address[0]));
            MEMCPY (InParams.Key.pKey, Prefix.au1Address, BGP4_IPV6_PREFIX_LEN);
            Bgp4MaskIpv6Address (InParams.Key.pKey, Mask.au1Address);
            MEMCPY (InParams.Key.pKey + BGP4_IPV6_PREFIX_LEN,
                    Mask.au1Address, BGP4_IPV6_PREFIX_LEN);
            break;
        }
#endif
#ifdef L3VPN
        case CAP_MP_VPN4_UNICAST:
        {
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.pLeafNode = NULL;
            InParams.pRoot = BGP4_VPN4_RIB_TREE (BGP4_RT_CXT_ID (pRtProfile));
            InParams.Key.pKey = au1ParamsKey;
            PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pRtProfile));
            u4Prefix = OSIX_HTONL (u4Prefix);
            Bgp4GetVpn4Subnetmask (BGP4_RT_PREFIXLEN (pRtProfile),
                                   &(Mask.au1Address[0]));
            MEMCPY (InParams.Key.pKey,
                    BGP4_RT_ROUTE_DISTING (pRtProfile),
                    BGP4_VPN4_ROUTE_DISTING_SIZE);
            MEMCPY (InParams.Key.pKey + BGP4_VPN4_ROUTE_DISTING_SIZE,
                    (UINT1 *) &u4Prefix, BGP4_IPV4_PREFIX_LEN);
            Bgp4MaskVpn4Address (InParams.Key.pKey, Mask.au1Address);
            MEMCPY (InParams.Key.pKey + BGP4_VPN4_PREFIX_LEN,
                    Mask.au1Address, BGP4_VPN4_PREFIX_LEN);
            break;
        }
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
        {
            InParams.pRoot = BGP4_VPLS_RIB_TREE (BGP4_RT_CXT_ID (pRtProfile));
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.pLeafNode = NULL;
            /*For L2VPN_VPLS, the Key is RD+VEID+VBO */
            InParams.Key.pKey = au1VplsKey;

            Bgp4VplsGetNlriInfoFromNetAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            &NlriInfo);
            MEMCPY (InParams.Key.pKey,
                    NlriInfo.au1RouteDistinguisher, MAX_LEN_RD_VALUE);
            MEMCPY (InParams.Key.pKey + MAX_LEN_RD_VALUE,
                    &NlriInfo.u2VeId, BGP4_VPLS_NLRI_VEID_LEN);
            MEMCPY (InParams.Key.pKey + MAX_LEN_RD_VALUE +
                    BGP4_VPLS_NLRI_VEID_LEN, &NlriInfo.u2VeBaseOffset,
                    BGP4_VPLS_NLRI_VBO_LEN);

            InParams.u1PrefixLen = BGP4_VPLS_MAX_KEY_SIZE;
            break;
        }
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
        {
            InParams.pRoot = BGP4_EVPN_RIB_TREE (BGP4_RT_CXT_ID (pRtProfile));
            InParams.pLeafNode = NULL;
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            /*For EVPN, the Key is RD+VNID+MAC+ETHSEGID Addr */
            InParams.Key.pKey = au1ParamsKey;

            MEMCPY (InParams.Key.pKey,
                    BGP4_EVPN_RD_DISTING (pRtProfile),
                    BGP4_EVPN_ROUTE_DISTING_SIZE);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE,
                    &(BGP4_EVPN_VNID(pRtProfile)),MAX_LEN_VNI_VALUE);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE + MAX_LEN_VNI_VALUE,
                   BGP4_EVPN_MACADDR(pRtProfile), MAX_LEN_MAC_ADDR);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE + MAX_LEN_VNI_VALUE
                     + MAX_LEN_MAC_ADDR,BGP4_EVPN_ETHSEGID(pRtProfile),
                         MAX_LEN_ETH_SEG_ID);
            InParams.u1PrefixLen = BGP4_EVPN_MAX_KEY_SIZE;
            break;
        }
#endif

        default:
            return BGP4_FAILURE;
    }

    pArg = &RtProfile;
    if (TrieGetNext (&InParams, (VOID *) &OutputParams, &pArg) == TRIE_FAILURE)
    {
        return BGP4_FAILURE;
    }
    *ppRtProfileList = RtProfile->pRtProfile;
    if (pRtProfile == (*ppRtProfileList))
    {
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4TrieCbTraverseAndGetNextEntry                         */
/* Description   : This function will give you the next route entry present  */
/*                 in RIB for a specified route entry by traversing the RIB  */
/*                 for specified route and then gives the next entry         */
/* Input(s)      : pRtProfile - route entry for which the next route info    */
/*                 is to be got from RIB.                                    */
/* Output(s)     : pRtProfileList - pointer to the next route information    */
/*                 in RIB corresponding to input pRtProfile                  */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4TrieCbTraverseAndGetNextEntry (tInputParams * pInputParams,
                                   tBgp4TrieOutParams * pOutParams,
                                   VOID *pAppSpecInfo, tKey Key)
{
    UNUSED_PARAM (Key);
    UNUSED_PARAM (pInputParams);
    pOutParams->pRtProfile = (tRouteProfile *) pAppSpecInfo;
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4TrieTraverseAndGetPrevEntry                           */
/* Description   : This function will give you the perv route entry present  */
/*                 in RIB for a specified route entry by traversing the RIB  */
/*                 for specified route and then gives the prev entry         */
/* Input(s)      : pRtProfile - route entry for which the prev route info    */
/*                 is to be got from RIB.                                    */
/* Output(s)     : pRtProfileList - pointer to the prev route information    */
/*                 in RIB corresponding to input pRtProfile                  */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4TrieTraverseAndGetPrevEntry (tRouteProfile * pRtProfile,
                                 VOID *pRibNode,
                                 tRouteProfile ** ppRtProfileList)
{
    VOID               *pTrieCtx = NULL;
    tInputParams        InParams;
#if (defined BGP4_IPV6_WANTED)
    tAddrPrefix         Prefix;
#endif
#if ((defined BGP4_IPV6_WANTED) || (defined L3VPN))
    tAddrPrefix         Mask;
#endif
    UINT4               au4Indx[BGP4_IPV4_KEY_ARR_SIZE];
    /* The Key of Trie is made-up 
     * of address and mask */
    UINT4               u4Prefix;
    UINT4               u4Mask;
    UINT4               u4AsafiMask;
    UINT1               au1ParamsKey[BGP4_INPUT_PARAMS_KEY_SIZE];
    INT4                i4Flag;
#ifdef VPLSADS_WANTED
    UINT1               au1VplsKey[BGP4_VPLS_MAX_KEY_SIZE];
    tVplsNlriInfo       NlriInfo;
#endif

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
    MEMSET (au1ParamsKey, 0, BGP4_INPUT_PARAMS_KEY_SIZE);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
#endif
        {
            InParams.pRoot = BGP4_IPV4_RIB_TREE (BGP4_RT_CXT_ID (pRtProfile));
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.pLeafNode = NULL;
            InParams.Key.pKey = (UINT1 *) &(au4Indx[BGP4_IPV4_KEY_ARR_INDX0]);

            PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pRtProfile));
            u4Mask = Bgp4GetSubnetmask ((UINT1) BGP4_RT_PREFIXLEN (pRtProfile));
            au4Indx[BGP4_IPV4_KEY_ARR_INDX0] = OSIX_HTONL (u4Prefix);
            au4Indx[BGP4_IPV4_KEY_ARR_INDX1] = OSIX_HTONL (u4Mask);
            break;
        }
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
        {
            InParams.pRoot = BGP4_IPV6_RIB_TREE (BGP4_RT_CXT_ID (pRtProfile));
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.pLeafNode = NULL;
            InParams.Key.pKey = au1ParamsKey;
            MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Prefix),
                    BGP4_RT_IP_PREFIX (pRtProfile), BGP4_IPV6_PREFIX_LEN);
            Bgp4GetIPV6Subnetmask (BGP4_RT_PREFIXLEN (pRtProfile),
                                   &(Mask.au1Address[0]));
            MEMCPY (InParams.Key.pKey, Prefix.au1Address, BGP4_IPV6_PREFIX_LEN);
            Bgp4MaskIpv6Address (InParams.Key.pKey, Mask.au1Address);
            MEMCPY (InParams.Key.pKey + BGP4_IPV6_PREFIX_LEN,
                    Mask.au1Address, BGP4_IPV6_PREFIX_LEN);
            break;
        }
#endif
#ifdef L3VPN
        case CAP_MP_VPN4_UNICAST:
        {
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.pLeafNode = NULL;
            InParams.pRoot = BGP4_VPN4_RIB_TREE (BGP4_RT_CXT_ID (pRtProfile));
            InParams.Key.pKey = au1ParamsKey;
            PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pRtProfile));
            u4Prefix = OSIX_HTONL (u4Prefix);
            Bgp4GetVpn4Subnetmask (BGP4_RT_PREFIXLEN (pRtProfile),
                                   &(Mask.au1Address[0]));
            MEMCPY (InParams.Key.pKey,
                    BGP4_RT_ROUTE_DISTING (pRtProfile),
                    BGP4_VPN4_ROUTE_DISTING_SIZE);
            MEMCPY (InParams.Key.pKey + BGP4_VPN4_ROUTE_DISTING_SIZE,
                    (UINT1 *) &u4Prefix, BGP4_IPV4_PREFIX_LEN);
            Bgp4MaskVpn4Address (InParams.Key.pKey, Mask.au1Address);
            MEMCPY (InParams.Key.pKey + BGP4_VPN4_PREFIX_LEN,
                    Mask.au1Address, BGP4_VPN4_PREFIX_LEN);
            break;
        }
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
        {
            InParams.pRoot = BGP4_VPLS_RIB_TREE (BGP4_RT_CXT_ID (pRtProfile));
            /* Application ID is always Protocol ID - 1 */
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            InParams.pLeafNode = NULL;
            /*For L2VPN_VPLS, the Key is RD+VEID+VBO */
            InParams.Key.pKey = au1VplsKey;

            Bgp4VplsGetNlriInfoFromNetAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            &NlriInfo);
            MEMCPY (InParams.Key.pKey,
                    NlriInfo.au1RouteDistinguisher, MAX_LEN_RD_VALUE);
            MEMCPY (InParams.Key.pKey + MAX_LEN_RD_VALUE,
                    &NlriInfo.u2VeId, BGP4_VPLS_NLRI_VEID_LEN);
            MEMCPY (InParams.Key.pKey + MAX_LEN_RD_VALUE +
                    BGP4_VPLS_NLRI_VEID_LEN, &NlriInfo.u2VeBaseOffset,
                    BGP4_VPLS_NLRI_VBO_LEN);

            InParams.u1PrefixLen = BGP4_VPLS_MAX_KEY_SIZE;
            break;
        }
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
        {
            InParams.pRoot = BGP4_EVPN_RIB_TREE (BGP4_RT_CXT_ID (pRtProfile));
            InParams.pLeafNode = NULL;
            InParams.i1AppId = BGP_ID - BGP4_DECREMENT_BY_ONE;
            /*For EVPN, the Key is RD+VNID+MAC+ETHSEGID Addr */
            InParams.Key.pKey = au1ParamsKey;

            MEMCPY (InParams.Key.pKey,
                    BGP4_EVPN_RD_DISTING (pRtProfile),
                    BGP4_EVPN_ROUTE_DISTING_SIZE);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE,
                    &(BGP4_EVPN_VNID(pRtProfile)),MAX_LEN_VNI_VALUE);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE + MAX_LEN_VNI_VALUE,
                   BGP4_EVPN_MACADDR(pRtProfile), MAX_LEN_MAC_ADDR);
            MEMCPY (InParams.Key.pKey + BGP4_EVPN_ROUTE_DISTING_SIZE + MAX_LEN_VNI_VALUE
                     + MAX_LEN_MAC_ADDR,BGP4_EVPN_ETHSEGID(pRtProfile),
                         MAX_LEN_ETH_SEG_ID);
            InParams.u1PrefixLen = BGP4_EVPN_MAX_KEY_SIZE;
            break;
        }
#endif

        default:
            return BGP4_FAILURE;
    }
    i4Flag = TrieGetPrevNode (&InParams, pRibNode,
                              (VOID *) ppRtProfileList, &pTrieCtx);
    if (i4Flag >= TRIE_SUCCESS)
    {
        pRibNode = pTrieCtx;
        return BGP4_SUCCESS;
    }
    *ppRtProfileList = NULL;
    return BGP4_FAILURE;
}

/*****************************************************************************/
/* Function Name : Bgp4GetRibNode                                            */
/* Description   : This function will retrieve the RIB node for the given    */
/*                 AFI in the given context.                                 */
/* Input(s)      : u4Context - Context Information                           */
/*                 u4AsafiMask - AFI mask for the trie                       */
/* Output(s)     : ppRibNode - Pointer to the RIB Head                       */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
Bgp4GetRibNode (UINT4 u4Context, UINT4 u4AsafiMask, VOID **ppRibNode)
{
    (*ppRibNode) = NULL;
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
            (*ppRibNode) = BGP4_IPV4_RIB_TREE (u4Context);
            break;
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
            (*ppRibNode) = BGP4_IPV6_RIB_TREE (u4Context);
            break;
#endif
#ifdef L3VPN
        case CAP_MP_VPN4_UNICAST:
            (*ppRibNode) = BGP4_VPN4_RIB_TREE (u4Context);
            break;
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
            (*ppRibNode) = BGP4_VPLS_RIB_TREE (u4Context);
            break;
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
            (*ppRibNode) = BGP4_EVPN_RIB_TREE (u4Context);
            break;
        
#endif
        default:
            break;
    }
    return;
}

#endif
