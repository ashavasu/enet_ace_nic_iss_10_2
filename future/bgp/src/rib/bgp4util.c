/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4util.c,v 1.63 2017/09/15 06:19:55 siva Exp $
 *
 * Description: Contains various stub routines which needs to be 
 *              replaced while porting.
 *
 *******************************************************************/
#ifndef BGP4UTIL_C
#define BGP4UTIL_C

#include "bgp4com.h"
#include "bgp4var.h"

#define  IP6_ADDR_CURR_POS             7

/*-------------------------------------------------------------------+
 * Function           : Bgp4DumpBuf
 *
 * Input(s)           : pu1Buf - Buffer to be printed 
 *                      u4Size - the size of the buffer
 *                      u4Flag - flag indicating whether buffer is received/sent
 * Output(s)          : None.
 *
 * Returns            : BGP4_SUCCESS 
 *
 * Description        : This function prints the contents of the input buffer
 *
+-------------------------------------------------------------------*/
INT4
Bgp4DumpBuf (UINT1 *pu1Buf, UINT4 u4Size, UINT1 u1Flag)
{
    /* This buffer has to be printed only when the Debug is On */
    UINT2               u2BufSize = 0;
    if (u1Flag == BGP4_INCOMING)
    {
        PRINTF ("\n%s: \tThe Buffer Received ...\n", BGP4_MOD_NAME);
    }
    else
    {
        PRINTF ("\n%s: \tThe Buffer Sent ...\n", BGP4_MOD_NAME);
    }
    PRINTF ("\t\t");
    while (u4Size > 0)
    {
        if ((u2BufSize % BGP4_DUMP_BUF_LEN) == 0)
        {
            PRINTF ("\n");
            PRINTF ("\t");
            PRINTF ("0x%2x ", *pu1Buf);
        }
        else
        {
            PRINTF ("0x%2x ", *pu1Buf);
        }
        u2BufSize++;
        pu1Buf++;
        u4Size--;
    }
    PRINTF ("\n\n");
    return BGP4_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : Bgp4GlobalTrace
 *
 * Input(s)           : pAddr - Address of the Peer
 *                      u4DbgVar - debug flag
 *                      u4mask - mask flag
 *                      pmodule - module name 
 *                      fmt - arguments for UtlTrcLog
 * Output(s)          : None.
 *
 * Returns            : BGP4_SUCCESS 
 *
 * Description        : This function for debug the code. 
 *
+-------------------------------------------------------------------*/
VOID
Bgp4GlobalTrace (tAddrPrefix * pAddr, UINT4 u4DbgVar, UINT4 u4mask,
                 const char *pmodule, const char *fmt, ...)
{
    va_list             ap;
    UINT4               u4TrcFlag = 0;
    UINT4               u4OffSet = 0;
    tAddrPrefix        *pAddrPrefix;
    tBgpCxtNode        *pBgpCxtNode = NULL;
    CHR1                Str[BGP4_TRACE_MAX_LEN];
    UINT4               u4Context;

    UNUSED_PARAM (u4DbgVar);
    u4Context = BgpGetContextId ();

    MEMSET (Str, 0, BGP4_TRACE_MAX_LEN);

    if (u4Context == BGP4_INVALID_CONTEXT_ID)
    {
        u4TrcFlag = BGP4_TRC_FLAG;
        pAddrPrefix = &gTraceAddr;
    }
    else
    {
        pBgpCxtNode = Bgp4GetContextEntry (u4Context);
        if (pBgpCxtNode != NULL)
        {
            u4TrcFlag = pBgpCxtNode->Bgp4DbgTrc.u4Bgp4Trc;
            pAddrPrefix = &(pBgpCxtNode->Bgp4TraceAddr);
            SPRINTF (Str, "Context Id : %d ", u4Context);
        }
        else
        {
            u4TrcFlag = BGP4_TRC_FLAG;
            pAddrPrefix = &gTraceAddr;
        }
    }

#ifdef VPLSADS_WANTED
    if (u4mask == BGP4_VPLS_CRI_TRC)
    {
        SPRINTF (Str, ":CRITICAL:");
    }
    else if (u4mask == BGP4_VPLS_INFO_TRC)
    {
        SPRINTF (Str, ":INFO_TRC:");
    }
    else if (u4mask == BGP4_VPLS_FUN_ENTRY)
    {
        SPRINTF (Str, ":FUN_ENTRY:");
    }
    else if (u4mask == BGP4_VPLS_FUN_EXIT)
    {
        SPRINTF (Str, ":FUN_EXIT:");
    }
#endif
    u4OffSet = STRLEN (Str);

    va_start (ap, fmt);
    vsprintf (&Str[u4OffSet], fmt, ap);
    va_end (ap);

    Bgp4TraceLog (pAddrPrefix, pAddr, u4TrcFlag, u4mask, pmodule, Str);

}

/*-------------------------------------------------------------------+
 * Function           : Bgp4TraceLog
 *
 * Input(s)           : pAddrPrefix - Address of the Peer
 *                      pAddr - Address of the peer
 *                      u4DbgVar - debug flag
 *                      u4mask - mask flag
 *                      pmodule - module name
 *                      pStr - Log message string
 * Output(s)          : None.
 *
 * Returns            : None
 *
 * Description        : This function for debug the code.
 *
+-------------------------------------------------------------------*/
VOID
Bgp4TraceLog (tAddrPrefix * pAddrPrefix, tAddrPrefix * pAddr, UINT4 u4DbgVar,
              UINT4 u4mask, const char *pmodule, CHR1 * pStr)
{
    tAddrPrefix         AddrPrefix;

    if (pAddrPrefix->u2Afi == 0)
    {
        UtlTrcLog (u4DbgVar, u4mask, pmodule, pStr);
    }
    else
    {
        if (pAddr == NULL)
        {
            return;
        }
        MEMSET (&AddrPrefix, 0, sizeof (tAddrPrefix));
        if (MEMCMP (pAddrPrefix->au1Address, AddrPrefix.au1Address,
                    BGP4_MAX_INET_ADDRESS_LEN) == 0)
        {

            if (pAddrPrefix->u2Afi == pAddr->u2Afi)
            {
                UtlTrcLog (u4DbgVar, u4mask, pmodule, pStr);
            }
        }
        else
        {
            if (PrefixMatch (*pAddrPrefix, *pAddr) == BGP4_TRUE)
            {
                UtlTrcLog (u4DbgVar, u4mask, pmodule, pStr);
            }
        }
    }
}

/*-------------------------------------------------------------------+
 * Function           : BgpDefaultNetMask
 *
 * Input(s)           : u4Addr
 *
 * Output(s)          : None.
 *
 * Returns            : Net mask
 *
 * Action :
 * Gets the default net mask for the address class (A, B or C).
 *
+-------------------------------------------------------------------*/
UINT4
BgpDefaultNetMask (UINT4 u4Addr)
{
    UINT4               u4RetAddr;

    if (IP_IS_ADDR_CLASS_A (u4Addr))
    {
        u4RetAddr = BGP_CLASS_A_DEF_MASK;
    }
    else if (IP_IS_ADDR_CLASS_B (u4Addr))
    {
        u4RetAddr = BGP_CLASS_B_DEF_MASK;
    }
    else if (IP_IS_ADDR_CLASS_C (u4Addr))
    {
        u4RetAddr = BGP_CLASS_C_DEF_MASK;
    }
    else
    {
        u4RetAddr = BGP_DEF_NET_MASK;
    }
    return u4RetAddr;
}

#ifdef BGP4_IPV6_WANTED
/*****************************************************************************/
/* Function Name : Bgp4GetIPV6Subnetmask                                     */
/* Description   : Given a prefix length this function gives the equivalent  */
/*                 IP subnet mask (128 bit).                                 */
/* Input(s)      : Prefix Length (u1Prefixlen)                               */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4GetIPV6Subnetmask (UINT2 u2Prefixlen, UINT1 *pu1Max)
{
    UINT4               u4Index = 0;
    UINT1               u1Len = (UINT1) u2Prefixlen;
    UINT4               u4Maskval = 0;
    UINT1               u1Remain = 0;

    while (u4Index < BGP4_IPV6_PREFIX_LEN)
    {
        pu1Max[u4Index] = 0x00;
        u4Index++;
    }

    u4Maskval = u1Len / BGP4_ONE_BYTE_BITS;
    u1Remain = (UINT1) (u1Len % BGP4_ONE_BYTE_BITS);

    for (u4Index = 0; u4Index < u4Maskval; u4Index++)
    {
        pu1Max[u4Index] = BGP4_ONE_BYTE_MAX_VAL;
    }

    if (u1Remain > 0 && u4Maskval < BGP4_IPV6_PREFIX_LEN)
    {
        pu1Max[u4Maskval] =
            (UINT1) (BGP4_ONE_BYTE_MAX_VAL << (BGP4_ONE_BYTE_BITS - u1Remain));
    }

    return BGP4_SUCCESS;
}
#endif

#ifdef L3VPN
/*****************************************************************************/
/* Function Name : Bgp4GetVpn4Subnetmask                                     */
/* Description   : Given a prefix length this function gives the equivalent  */
/*                 IP subnet mask (128 bit).                                 */
/* Input(s)      : Prefix Length (u1Prefixlen)                               */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4GetVpn4Subnetmask (UINT2 u2Prefixlen, UINT1 *pu1Max)
{
    UINT4               u4Index = 0;
    UINT1               u1Len = (UINT1) u2Prefixlen;
    UINT4               u4Maskval = 0;
    UINT1               u1Remain = 0;

    while (u4Index < BGP4_VPN4_PREFIX_LEN)
    {
        pu1Max[u4Index] = 0x00;
        u4Index++;
    }

    u4Maskval = u1Len / BGP4_ONE_BYTE_BITS;
    u1Remain = (UINT1) (u1Len % BGP4_ONE_BYTE_BITS);

    for (u4Index = 0; u4Index < BGP4_VPN4_ROUTE_DISTING_SIZE; u4Index++)
    {
        /* Mask for 8 bytes route-distinguiser. always FF...FF */
        pu1Max[u4Index] = BGP4_ONE_BYTE_MAX_VAL;
    }

    u4Maskval += BGP4_VPN4_ROUTE_DISTING_SIZE;
    for (u4Index = BGP4_VPN4_ROUTE_DISTING_SIZE; u4Index < u4Maskval; u4Index++)
    {
        /* Mask for the remaining 4 bytes IP address */
        pu1Max[u4Index] = BGP4_ONE_BYTE_MAX_VAL;
    }

    if (u1Remain > 0)
    {
        pu1Max[u4Maskval] =
            (UINT1) (BGP4_ONE_BYTE_MAX_VAL << (BGP4_ONE_BYTE_BITS - u1Remain));
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4MaskVpn4Address                                       */
/* Description   : For a given PREFIX and Mask, it applies the mask to prefix*/
/* Input(s)      : pAddr -   address prefix                                  */
/*                 pMask -   Mask                                            */
/* Output(s)     : pAddr - Masked prefix.                                    */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4MaskVpn4Address (UINT1 *pAddr, UINT1 *pMask)
{
    UINT4               u4Index;

    for (u4Index = 0; u4Index < BGP4_VPN4_PREFIX_LEN; u4Index++)
    {
        pAddr[u4Index] = pAddr[u4Index] & pMask[u4Index];
    }
    return BGP4_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name : Bgp4GetSubnetmask                                         */
/* Description   : Given a prefix length this function gives the equivalent  */
/*                 IP subnet mask (32 bit).                                  */
/* Input(s)      : Prefix Length (u1Prefixlen)                               */
/* Output(s)     : None.                                                     */
/* Return(s)     : IP Subnet Mask (32 bit)                                   */
/*****************************************************************************/
UINT4
Bgp4GetSubnetmask (UINT1 u1Prefixlen)
{
    UINT4               u4Subnetmask = 0;

    if (u1Prefixlen != 0)
    {
        u4Subnetmask = (BGP4_HOST_MASK << (BGP4_MAX_PREFIXLEN - u1Prefixlen));
    }
    return (u4Subnetmask);
}

/*****************************************************************************/
/* Function Name : Bgp4IsLocalroute                                          */
/* Description   : This function tells whether a given route is a BGP route  */
/*                 or some other IGP route(or local route).                  */
/* Input(s)      : Route Profile (pRtProfile)                                */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRUE if the route not a BGP route,                        */
/*                 FALSE if the route is BGP route.                          */
/*****************************************************************************/
BOOL1
Bgp4IsLocalroute (tRouteProfile * pRtProfile)
{
    if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
    {
        return FALSE;
    }

    return TRUE;
}

#ifdef L3VPN
/*****************************************************************************/
/* Function Name : Vpnv4AddrMatch                                            */
/* Description   : compare "prefix length" bits of an address                */
/* Input(s)      : IP Addresses (u4Addr1, u4Addr2), PrefixLength (u4PfxLen)  */
/* Output(s)     : None.                                                     */
/* Return(s)     : 0 - Prefix Does not Match                                 */
/*                 BGP4_TRUE - Prefix Matches                                */
/*****************************************************************************/
INT4
Vpnv4AddrMatch (tAddrPrefix AddrPrefix, tRouteProfile * pRtProfile,
                UINT4 u4PfxLen)
/*
 *  compare "prefix length" bits of an address
 *  BGP4_FALSE - Prefix Does not Match
 *  BGP4_TRUE - Prefix Matches
 */
{
    UINT4               u4Mask = 0;
    UINT4               u4Addr1;
    UINT4               u4Addr2;

    if (pRtProfile == NULL)
    {
        return (BGP4_FALSE);
    }
    if (BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix) !=
        BGP4_RT_AFI_INFO (pRtProfile))
    {
        return (BGP4_FALSE);
    }
    if (MEMCMP (BGP4_RT_ROUTE_DISTING (pRtProfile),
                BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix),
                BGP4_VPN4_ROUTE_DISTING_SIZE) != 0)
    {
        return (BGP4_FALSE);
    }
    PTR_FETCH_4 (u4Addr1,
                 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix)
                  + BGP4_VPN4_ROUTE_DISTING_SIZE));
    PTR_FETCH_4 (u4Addr2, BGP4_RT_IP_PREFIX (pRtProfile));

    if ((u4PfxLen == 0) || ((u4Addr1 == 0) && (u4Addr2 == 0)))
    {
        return (BGP4_TRUE);
    }

    u4Mask = Bgp4GetSubnetmask ((UINT1) u4PfxLen);
    /* Used to compare Route Prefixes */
    if ((u4Addr1 ^ u4Addr2) & u4Mask)
    {
        return (BGP4_FALSE);
    }
    return BGP4_TRUE;
}
#endif

/*****************************************************************************/
/* Function Name : AddrMatch                                                 */
/* Description   : compare "prefix length" bits of an address                */
/* Input(s)      : IP Addresses (u4Addr1, u4Addr2), PrefixLength (u4PfxLen)  */
/* Output(s)     : None.                                                     */
/* Return(s)     : 0 - Prefix Does not Match                                 */
/*                 BGP4_TRUE - Prefix Matches                                */
/*****************************************************************************/
INT4
AddrMatch (tAddrPrefix AddrPrefix1, tAddrPrefix AddrPrefix2, UINT4 u4PfxLen)
/*
 *  compare "prefix length" bits of an address
 *  BGP4_FALSE - Prefix Does not Match
 *  BGP4_TRUE - Prefix Matches
 */
{
    if (BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix1) !=
        BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix2))
    {
        return (BGP4_FALSE);
    }

    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix1))
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4Mask = 0;
            UINT4               u4Addr1;
            UINT4               u4Addr2;

            PTR_FETCH_4 (u4Addr1,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix1));
            PTR_FETCH_4 (u4Addr2,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix2));

            if ((u4PfxLen == 0) || ((u4Addr1 == 0) && (u4Addr2 == 0)))
            {
                return (BGP4_TRUE);
            }

            u4Mask = Bgp4GetSubnetmask ((UINT1) u4PfxLen);
            /* Used to compare Route Prefixes */
            if ((u4Addr1 ^ u4Addr2) & u4Mask)
            {
                return (BGP4_FALSE);
            }
        }
            break;
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
        {
            tIp6Addr            IPAddr1;
            tIp6Addr            IPAddr2;
            MEMCPY (&IPAddr1.u1_addr[0], AddrPrefix1.au1Address,
                    BGP4_IPV6_PREFIX_LEN);
            MEMCPY (&IPAddr2.u1_addr[0], AddrPrefix2.au1Address,
                    BGP4_IPV6_PREFIX_LEN);

            if (Ip6AddrMatch (&IPAddr1, &IPAddr2, (INT4) u4PfxLen) != TRUE)
            {
                return (BGP4_FALSE);
            }
            else
            {
                break;
            }
        }
#endif
        default:
        {
            UINT1               u1Index = 0;

            while ((u1Index < BGP4_MAX_INET_ADDRESS_LEN) &&
                   (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix1)[u1Index]
                    ==
                    BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix2)
                    [u1Index]))
            {
                u1Index++;
            }

            if (u1Index == BGP4_MAX_INET_ADDRESS_LEN)
            {
                return (BGP4_TRUE);
            }
            return (BGP4_FALSE);
        }
    }
    return (BGP4_TRUE);
}

/*****************************************************************************/
/* Function Name : AddrCompare                                               */
/* Description   : check if u4Addr1 is > u4Addr2                             */
/* Input(s)      : IP Addresses (u4Addr1, u4Addr2)                           */
/* Output(s)     : None.                                                     */
/* Return(s)     : > than 0, if u4Addr1 >  u4Addr2                           */
/*                 == 0, if u4Addr1 == u4Addr2                               */
/*                 < 0,  u4Addr1 < u4Addr2                                   */
/*****************************************************************************/
INT4
AddrMatchDefault (tAddrPrefix AddrPrefix1, tAddrPrefix AddrPrefix2,
                  UINT4 u4PfxLen, UINT4 u4AsafiMask)
/*
 *  compare "prefix length" bits of an address
 *  BGP4_FALSE - Prefix Does not Match
 *  BGP4_TRUE - Prefix Matches
 */
{
    UNUSED_PARAM (u4AsafiMask);
    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix1))
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4Addr1;
            UINT4               u4Addr2;

            PTR_FETCH_4 (u4Addr1,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix1));
            PTR_FETCH_4 (u4Addr2,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix2));

            if ((u4PfxLen == BGP4_MAX_PREFIXLEN) && (u4Addr2 == 0)
                && (u4Addr1 == 0))
            {
                return (BGP4_TRUE);
            }

        }
            break;
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
        {
            tIp6Addr            IPAddr1;
            tIp6Addr            IPAddr2;
            MEMCPY (&IPAddr1.u1_addr[0], AddrPrefix1.au1Address,
                    BGP4_IPV6_PREFIX_LEN);
            MEMCPY (&IPAddr2.u1_addr[0], AddrPrefix2.au1Address,
                    BGP4_IPV6_PREFIX_LEN);
            if (u4PfxLen == BGP4_MAX_IPV6_PREFIXLEN)
            {
                if (MEMCMP
                    (&IPAddr2.u1_addr[0], &IPAddr1.u1_addr[0],
                     BGP4_IPV6_PREFIX_LEN) == 0)
                {
                    return (BGP4_TRUE);
                }

            }
        }
            break;

#endif
        default:
        {

            return (BGP4_FALSE);
        }
    }
    return (BGP4_FALSE);
}

/*****************************************************************************/
/* Function Name : AddrCompare                                               */
/* Description   : check if u4Addr1 is > u4Addr2                             */
/* Input(s)      : IP Addresses (u4Addr1, u4Addr2)                           */
/* Output(s)     : None.                                                     */
/* Return(s)     : > than 0, if u4Addr1 >  u4Addr2                           */
/*                 == 0, if u4Addr1 == u4Addr2                               */
/*                 < 0,  u4Addr1 < u4Addr2                                   */
/*****************************************************************************/
INT4
AddrCompare (tAddrPrefix Addr1, UINT4 u4PfxLen1, tAddrPrefix Addr2,
             UINT4 u4PfxLen2)
{
#ifdef BGP4_IPV6_WANTED
    tIp6Addr            Address1;
    tIp6Addr            Address2;
    tIp6Addr            Address3;
    tIp6Addr            Address4;
    INT4                i4Ret;
    UINT1               u1CopyBytes = 0;
#endif /* BGP4_IPV6_WANTED */

    if (BGP4_AFI_IN_ADDR_PREFIX_INFO (Addr1) !=
        BGP4_AFI_IN_ADDR_PREFIX_INFO (Addr2))
    {
        return (BGP4_FAILURE);
    }

    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (Addr1))
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4Mask1 = 0;
            UINT4               u4Mask2 = 0;
            UINT4               u4Addr1;
            UINT4               u4Addr2;

            if (u4PfxLen1)
            {
                u4Mask1 = Bgp4GetSubnetmask ((UINT1) u4PfxLen1);
            }

            if (u4PfxLen2)
            {
                u4Mask2 = Bgp4GetSubnetmask ((UINT1) u4PfxLen2);
            }

            PTR_FETCH_4 (u4Addr1,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Addr1));
            PTR_FETCH_4 (u4Addr2,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Addr2));
            u4Addr1 = u4Addr1 & u4Mask1;
            u4Addr2 = u4Addr2 & u4Mask2;

            if (u4Addr1 > u4Addr2)
            {
                return (1);
            }
            else
            {
                if (u4Addr1 == u4Addr2)
                {
                    return 0;
                }
                else
                {
                    return (-1);
                }
            }
        }
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
        {
            u1CopyBytes = Bgp4RoundToBytes ((UINT2) u4PfxLen1);
            if (u1CopyBytes <= BGP4_MAX_INET_ADDRESS_LEN)
            {
                MEMCPY (Address3.u1_addr, Addr1.au1Address, u1CopyBytes);
            }

            u1CopyBytes = Bgp4RoundToBytes ((UINT2) u4PfxLen2);
            if (u1CopyBytes <= BGP4_MAX_INET_ADDRESS_LEN)
            {
                MEMCPY (Address4.u1_addr, Addr2.au1Address, u1CopyBytes);
            }

            Ip6CopyAddrBits (&Address1, &Address3, (INT4) u4PfxLen1);
            Ip6CopyAddrBits (&Address2, &Address4, (INT4) u4PfxLen2);

            i4Ret = MEMCMP (Address1.u1_addr, Address2.u1_addr,
                            BGP4_IPV6_PREFIX_LEN);

            return i4Ret;
        }
#endif
        default:
        {
            UINT1               u1Index = 0;

            while ((u1Index < (BGP4_MAX_INET_ADDRESS_LEN)) &&
                   (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Addr1)[u1Index] ==
                    BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Addr2)[u1Index]))
            {
                u1Index++;
            }
            if (u1Index == BGP4_MAX_INET_ADDRESS_LEN)
            {
                return (0);
            }

            if (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Addr1)[u1Index] >
                BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Addr2)[u1Index])
            {
                return (1);
            }
            else if (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Addr1)[u1Index] <
                     BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Addr2)[u1Index])
            {
                return (-1);
            }
            else
                return (0);
        }
    }
}

/*****************************************************************************/
/* Function Name : AddrGreaterThan                                           */
/* Description   : check if u4Addr1 is > u4Addr2                             */
/* Input(s)      : IP Addresses (u4Addr1, u4Addr2)                           */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE - u4Addr1 >  u4Addr2                            */
/*                 0 - u4Addr1 <= u4Addr2                                    */
/*****************************************************************************/
UINT1
AddrGreaterThan (tAddrPrefix Addr1, tAddrPrefix Addr2, UINT4 u4PfxLen)
{

#ifdef BGP4_IPV6_WANTED
    tIp6Addr            Address1;
    tIp6Addr            Address2;
    tIp6Addr            Address3;
    tIp6Addr            Address4;
    UINT1               u1CopyBytes = 0;
#endif /* BGP4_IPV6_WANTED */

    if (BGP4_AFI_IN_ADDR_PREFIX_INFO (Addr1) !=
        BGP4_AFI_IN_ADDR_PREFIX_INFO (Addr2))
    {
        return (0);
    }
    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (Addr1))
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4Mask = 0;
            UINT4               u4Addr1;
            UINT4               u4Addr2;

            if (u4PfxLen)
            {
                u4Mask = Bgp4GetSubnetmask ((UINT1) u4PfxLen);
            }

            PTR_FETCH_4 (u4Addr1,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Addr1));
            PTR_FETCH_4 (u4Addr2,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Addr2));
            u4Addr1 = u4Addr1 & u4Mask;
            u4Addr2 = u4Addr2 & u4Mask;

            if (u4Addr1 > u4Addr2)
            {
                return (1);
            }
            return (0);
        }
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
        {

            u1CopyBytes = Bgp4RoundToBytes ((UINT2) u4PfxLen);
            if (u1CopyBytes <= BGP4_MAX_INET_ADDRESS_LEN)
            {
                MEMCPY (Address3.u1_addr, Addr1.au1Address, u1CopyBytes);
                MEMCPY (Address4.u1_addr, Addr2.au1Address, u1CopyBytes);
            }
            Ip6CopyAddrBits (&Address1, &Address3, (INT4) u4PfxLen);
            Ip6CopyAddrBits (&Address2, &Address4, (INT4) u4PfxLen);
            if (MEMCMP (Address1.u1_addr, Address2.u1_addr,
                        BGP4_IPV6_PREFIX_LEN) > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
#endif
        default:
        {
            UINT1               u1Index = 0;

            while ((u1Index < (BGP4_MAX_INET_ADDRESS_LEN)) &&
                   (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Addr1)[u1Index] ==
                    BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Addr2)[u1Index]))
            {
                u1Index++;
            }
            if (u1Index == BGP4_MAX_INET_ADDRESS_LEN)
            {
                return (0);
            }

            if (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Addr1)[u1Index] >
                BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Addr2)[u1Index])
            {
                return (1);
            }
            return (0);
        }
    }
}

/*****************************************************************************/
/* Function Name : Bgp4IsRtsIdentical                                        */
/* Description   : Checks whether both the given route profiles are          */
/*                 identical or not                                          */
/* Input(s)      : (pRtProfile1) - First Route Profile                       */
/*                 (pRtProfile2) - Second Route Profile                      */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_TRUE  -  if routes are identical                     */
/*                 BGP4_FALSE -  if routes are not identical                 */
/*****************************************************************************/
UINT4
Bgp4IsRtsIdentical (tRouteProfile * pRtProfile1, tRouteProfile * pRtProfile2)
{
    tBgp4Info          *pBgpInfo1 = BGP4_RT_BGP_INFO (pRtProfile1);
    tBgp4Info          *pBgpInfo2 = BGP4_RT_BGP_INFO (pRtProfile2);
    INT4                i4ValidStatus;

    if (BGP4_RT_PROTOCOL (pRtProfile1) != BGP4_RT_PROTOCOL (pRtProfile2))
    {
        return BGP4_FALSE;
    }

    if (BGP4_RT_PREFIXLEN (pRtProfile1) != BGP4_RT_PREFIXLEN (pRtProfile2))
    {
        return BGP4_FALSE;
    }

    i4ValidStatus = NetAddrMatch (BGP4_RT_NET_ADDRESS_INFO (pRtProfile1),
                                  BGP4_RT_NET_ADDRESS_INFO (pRtProfile2));
    if (i4ValidStatus == BGP4_FALSE)
    {
        return BGP4_FALSE;
    }
    if (BGP4_RT_PROTOCOL (pRtProfile1) == BGP_ID)
    {
        i4ValidStatus =
            PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO
                         (BGP4_RT_PEER_ENTRY (pRtProfile1)),
                         BGP4_PEER_REMOTE_ADDR_INFO (BGP4_RT_PEER_ENTRY
                                                     (pRtProfile2)));
        if (i4ValidStatus == BGP4_FALSE)
        {
            return BGP4_FALSE;
        }
    }

    if (Bgp4AttrIsBgpinfosIdentical (pBgpInfo1, pBgpInfo2) == FALSE)
    {
        return BGP4_FALSE;
    }

    return BGP4_TRUE;
}

/*****************************************************************************/
/* Function Name : Bgp4GetSubnetmasklen                                      */
/* Description   : Given a destination mask this function gives the          */
/*                 equivalent IP subnet prefix-length.                       */
/* Input(s)      : Destination mask (u4Mask)                                 */
/* Output(s)     : None.                                                     */
/* Return(s)     : IP Prefixlength (32 bit)                                  */
/*****************************************************************************/

UINT1
Bgp4GetSubnetmasklen (UINT4 u4Mask)
{
    UINT1               u1PLen = 0;

    while (u4Mask > 0)
    {
        u4Mask = u4Mask << 1;
        u1PLen++;
    }
    return u1PLen;
}

#ifdef BGP4_IPV6_WANTED
/*****************************************************************************/
/* Function Name : Bgp4GetIpv6Subnetmasklen                                  */
/* Description   : Given a destination V6 mask this function gives the       */
/*                 equivalent IP subnet prefix-length.                       */
/* Input(s)      : Destination mask (pu1IpAddress)                           */
/* Output(s)     : None.                                                     */
/* Return(s)     : IP Prefixlength (32 bit)                                  */
/*****************************************************************************/

UINT1
Bgp4GetIpv6Subnetmasklen (UINT1 *pu1IpAddress)
{
    INT4                i4Index = 0;
    UINT1               u1TempLen = 8;
    UINT1               u1PLen = 0;
    UINT1               u1Mask = 0;

    for (i4Index = 0; i4Index < BGP4_IPV6_PREFIX_LEN; i4Index++)
    {
        u1Mask = *(pu1IpAddress + i4Index);
        u1TempLen = 8;
        while (u1Mask)
        {
            u1Mask = (UINT1) (u1Mask << 1);
            u1PLen++;
            u1TempLen--;
        }
        if (u1TempLen != 0)
        {
            break;
        }
    }
    return u1PLen;
}
#endif

/*****************************************************************************/
/* Function Name : Bgp4IsValidPrefixLength                                   */
/* Description   : Validates the given prefix length based on AFI            */
/* Input(s)      : AddrPrefix                                                */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE/BGP4_FALSE                                      */
/*****************************************************************************/
UINT1
Bgp4IsValidPrefixLength (tNetAddress RoutePrefix)
{

    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO
            (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (RoutePrefix)))
    {
        case BGP4_INET_AFI_IPV4:
            if (BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RoutePrefix) >
                BGP4_MAX_PREFIXLEN)
            {
                return BGP4_FALSE;
            }
            break;
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
            if (BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RoutePrefix) >
                BGP4_MAX_IPV6_PREFIXLEN)
            {
                return BGP4_FALSE;
            }
            break;
#endif
        default:
            return BGP4_FALSE;
    }
    return BGP4_TRUE;
}

/*****************************************************************************/
/* Function Name : Bgp4GetMaxPrefixLength                                    */
/* Description   : Gets the maximum prefixlength based on AFI                */
/* Input(s)      : Prefix                                                    */
/* Output(s)     : None.                                                     */
/* Return(s)     : max prefix length value                                   */
/*****************************************************************************/
UINT2
Bgp4GetMaxPrefixLength (tAddrPrefix Prefix)
{
    UINT2               u2PrefixLen = 0;
    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (Prefix))
    {
        case BGP4_INET_AFI_IPV4:
            u2PrefixLen = BGP4_MAX_PREFIXLEN;
            break;
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
            u2PrefixLen = BGP4_MAX_IPV6_PREFIXLEN;
            break;
#endif
        default:
            break;
    }
    return u2PrefixLen;
}

/*****************************************************************************/
/* Function Name : Bgp4ConfIsValidAddress                                    */
/* Description   : Validates the given prefix address based on AFI           */
/* Input(s)      : AddrPrefix                                                */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE/BGP4_FALSE                                      */
/*****************************************************************************/
UINT1
Bgp4ConfIsValidAddress (tAddrPrefix AddrPrefix)
{
    UINT1               u1ValidStatus = BGP4_FALSE;

    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix))
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4Ipaddr;

            PTR_FETCH_4 (u4Ipaddr,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix));
            u1ValidStatus = (UINT1) BGP4_IS_MCAST_LOOP_ADDR (u4Ipaddr);
            if (u1ValidStatus == 0)
            {
                return BGP4_FALSE;
            }
            u1ValidStatus = (UINT1) BGP4_IS_BROADCAST_ADDRESS (u4Ipaddr);
            if (u1ValidStatus == 1)
            {
                return BGP4_FALSE;
            }
            return BGP4_TRUE;
        }
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
        {
            INT4                i4ValidStatus = BGP4_FAILURE;

            i4ValidStatus = Bgp4Ipv6ConfIsValidAddr (&AddrPrefix);
            if (i4ValidStatus == BGP4_FAILURE)
            {
                return BGP4_FALSE;
            }
            return BGP4_TRUE;
        }
#endif
        default:
            return BGP4_FALSE;
    }
}

/*****************************************************************************/
/* Function Name : Bgp4IsValidRouteAddress                                    */
/* Description   : Validates the given prefix address based on AFI           */
/* Input(s)      : AddrPrefix                                                */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE/BGP4_FALSE                                      */
/*****************************************************************************/
UINT1
Bgp4IsValidRouteAddress (tNetAddress RtAddress)
{
    UINT4               u4AsafiMask;
    UINT1               u1ValidStatus = BGP4_FALSE;

    BGP4_GET_AFISAFI_MASK (BGP4_AFI_IN_ADDR_PREFIX_INFO
                           (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                            (RtAddress)),
                           BGP4_SAFI_IN_NET_ADDRESS_INFO (RtAddress),
                           u4AsafiMask);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
#endif
        {
            UINT4               u4Ipaddr;

            PTR_FETCH_4 (u4Ipaddr,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                         (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                          (RtAddress)));
            u1ValidStatus = (UINT1) BGP4_IS_MCAST_LOOP_ADDR (u4Ipaddr);
            if (u1ValidStatus == 0)
            {
                return BGP4_FALSE;
            }
            u1ValidStatus = (UINT1) BGP4_IS_BROADCAST_ADDRESS (u4Ipaddr);
            if (u1ValidStatus == 1)
            {
                return BGP4_TRUE;
            }
            return BGP4_TRUE;
        }
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
        {
            INT4                i4ValidStatus = BGP4_FAILURE;

            i4ValidStatus =
                Bgp4Ipv6ConfIsValidAddr (&
                                         (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                          (RtAddress)));
            if (i4ValidStatus == BGP4_FAILURE)
            {
                return BGP4_FALSE;
            }
            return BGP4_TRUE;
        }
#endif
#ifdef L3VPN
        case CAP_MP_VPN4_UNICAST:
        {
            UINT4               u4Ipaddr;
            BGP4_VPN4_IS_VALID_RD (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                                   (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                    (RtAddress)), u1ValidStatus);
            if (u1ValidStatus != BGP4_TRUE)
            {
                return BGP4_FALSE;
            }
            PTR_FETCH_4 (u4Ipaddr,
                         (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                          (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                           (RtAddress))) + BGP4_VPN4_ROUTE_DISTING_SIZE);
            u1ValidStatus = (UINT1) BGP4_IS_MCAST_LOOP_ADDR (u4Ipaddr);
            if (u1ValidStatus == 0)

            {
                return BGP4_FALSE;
            }
            u1ValidStatus = (UINT1) BGP4_IS_BROADCAST_ADDRESS (u4Ipaddr);
            if (u1ValidStatus == 1)
            {
                return BGP4_FALSE;
            }
            return BGP4_TRUE;
        }

#endif
        default:
            return BGP4_FALSE;
    }
}

/*****************************************************************************/
/* Function Name : Bgp4IsValidAddress                                        */
/* Description   : Validates the given prefix address based on AFI           */
/* Input(s)      : AddrPrefix                                                */
/*                 u1DefAddrStatus - Indicates whether default route is VALID*/
/*                                   or not. If BGP4_TRUE, then the default  */
/*                                   route is considered as VALID, else the  */
/*                                   default route is considered INVALID     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE/BGP4_FALSE                                      */
/*****************************************************************************/
UINT1
Bgp4IsValidAddress (tAddrPrefix AddrPrefix, UINT1 u1DefAddrStatus)
{
    UINT1               u1ValidStatus = BGP4_FALSE;

    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix))
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4Ipaddr;

            PTR_FETCH_4 (u4Ipaddr,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix));
            u1ValidStatus = (UINT1) BGP4_IS_ALL_BROADCAST_ADDRESS (u4Ipaddr);
            if (u1ValidStatus == 1)
            {
                CLI_SET_ERR (CLI_BGP4_INVALID_BROADCAST_MASK);
                return BGP4_FALSE;
            }
            if (u1DefAddrStatus == BGP4_TRUE)
            {
                /* Default route is valid */
                u1ValidStatus = (UINT1) BGP4_IS_MCAST_LOOP_ADDR (u4Ipaddr);
                if (u1ValidStatus == 0)
                {
                    CLI_SET_ERR (CLI_BGP4_INVALID_MULTICAST_MASK);
                    return BGP4_FALSE;
                }
            }
            else
            {
                /* Default route is invalid */
                u1ValidStatus = (UINT1) BGP4_IS_VALID_ADDRESS (u4Ipaddr);

                if (u1ValidStatus == 0)
                {
                    CLI_SET_ERR (CLI_BGP4_INVALID_MASK);
                    return BGP4_FALSE;
                }
            }
            return BGP4_TRUE;
        }
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
        {
            INT4                i4ValidStatus = BGP4_FAILURE;

            i4ValidStatus = Bgp4Ipv6IsValidAddr (&AddrPrefix, u1DefAddrStatus);
            if (i4ValidStatus == BGP4_FAILURE)
            {
                return BGP4_FALSE;
            }
            return BGP4_TRUE;
        }
#endif
        default:
            return BGP4_FALSE;
    }
}

#ifdef BGP4_IPV6_WANTED
/*****************************************************************************/
/* Function Name : BgpIP6ConfIsValidAddr                                     */
/* Description   : checks if the recieved address is a valid IPv6 address    */
/* Input(s)      : AddrPrefix                                                */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4Ipv6ConfIsValidAddr (tAddrPrefix * pAddr)
{
    INT4                i4Status;

    BGP4_IN6_IS_ADDR_LOOPBACK (pAddr->au1Address, i4Status);
    if (i4Status == BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }

    BGP4_IN6_IS_ADDR_MULTICAST (pAddr->au1Address, i4Status);
    if (i4Status == BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }

    return BGP4_SUCCESS;
}
#endif

#ifdef BGP4_IPV6_WANTED
/*****************************************************************************/
/* Function Name : BgpIpv6IsValidAddr                                        */
/* Description   : checks if the recieved address is a valid IPv6 address    */
/* Input(s)      : AddrPrefix                                                */
/*                 u1DefAddrStatus - Indicates whether default route is      */
/*                                   or not. If BGP4_TRUE, then the default  */
/*                                   route is considered as VALID, else the  */
/*                                   default route is considered INVALID     */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4Ipv6IsValidAddr (tAddrPrefix * pAddr, UINT1 u1DefAddrStatus)
{
    tAddrPrefix         AddrPrefix;
    UINT4               u4Addr;
    INT4                i4Status;
    CHR1                ac1Addr[50];
    tUtlIn6Addr         In6Addr;

    MEMCPY (In6Addr.u1addr, pAddr->au1Address, BGP4_MAX_INET_ADDRESS_LEN);
    STRCPY (ac1Addr, INET_NTOA6 (In6Addr));
    if (INET_ATON6 (ac1Addr, pAddr->au1Address) == 0)
    {
        return BGP4_FAILURE;
    }

    BGP4_IN6_IS_ADDR_LOOPBACK (pAddr->au1Address, i4Status);
    if (i4Status == BGP4_TRUE)
    {
        CLI_SET_ERR (CLI_BGP4_INVALID_LOOPBACK_MASK);
        return BGP4_FAILURE;
    }
    BGP4_IN6_IS_ADDR_BROADCAST (pAddr->au1Address, i4Status);
    if (i4Status == BGP4_TRUE)
    {
        CLI_SET_ERR (CLI_BGP4_INVALID_BROADCAST_MASK);
        return BGP4_FAILURE;
    }
    BGP4_IN6_IS_ADDR_MULTICAST (pAddr->au1Address, i4Status);
    if (i4Status == BGP4_TRUE)
    {
        CLI_SET_ERR (CLI_BGP4_INVALID_MULTICAST_MASK);
        return BGP4_FAILURE;
    }
    BGP4_IN6_IS_ADDR_LINKLOCAL (pAddr->au1Address, i4Status);
    if (i4Status == BGP4_TRUE)
    {
        CLI_SET_ERR (CLI_BGP4_INVALID_LINK_LOCAL_MASK);
        return BGP4_FAILURE;
    }
    BGP4_IN6_IS_ADDR_V4COMPATIBLE (pAddr->au1Address, i4Status);
    if (i4Status == BGP4_TRUE)
    {
        /* IPv4-IPv6 compatible route. Validate the IPv4 prefix */
        Bgp4InitAddrPrefixStruct (&AddrPrefix, BGP4_INET_AFI_IPV4);
        PTR_FETCH4 (u4Addr, &(pAddr->au1Address[BGP4_IPV6_PREFIX_LEN -
                                                BGP4_IPV4_PREFIX_LEN]));
        PTR_ASSIGN_4 (AddrPrefix.au1Address, u4Addr);
        i4Status = Bgp4IsValidAddress (AddrPrefix, u1DefAddrStatus);
        if (i4Status == BGP4_FALSE)
        {
            return BGP4_FAILURE;
        }
    }

    return BGP4_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name : Bgp4GetPrefixHashIndex                                    */
/* Description   : Gets the Hash index for a given Prefix in AddrPrefix      */
/* Input(s)      : AddrPrefix                                                */
/* Output(s)     : pu4HashIndex.                                             */
/* Return(s)     : BGP4_SUCCESS.                                             */
/*****************************************************************************/
INT4
Bgp4GetPrefixHashIndex (tAddrPrefix AddrPrefix, UINT4 *pu4HashIndex)
{
    UINT4               u4HashValue = 0;
    UINT1               u1Index;

    *pu4HashIndex = 0;
    switch (AddrPrefix.u2Afi)
    {
        case BGP4_INET_AFI_IPV4:
            /* first 4 bytes has IPV4 address */
            for (u1Index = 0; u1Index < BGP4_IPV4_PREFIX_LEN; u1Index++)
            {
                u4HashValue += AddrPrefix.au1Address[u1Index] %
                    BGP4_MAX_PREFIX_HASH_TBL_SIZE;
            }
            break;
#if defined (VPLSADS_WANTED)
        case BGP4_INET_AFI_L2VPN:
            for (u1Index = 0; u1Index < BGP4_VPLS_NLRI_PREFIX_LEN; u1Index++)
            {
                u4HashValue += AddrPrefix.au1Address[u1Index] %
                    BGP4_MAX_PREFIX_HASH_TBL_SIZE;
            }
            break;
#endif
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
            for (u1Index = 0; u1Index < BGP4_IPV6_PREFIX_LEN; u1Index++)
            {
                u4HashValue += AddrPrefix.au1Address[u1Index] %
                    BGP4_MAX_PREFIX_HASH_TBL_SIZE;
            }
            break;
#endif
        default:
            break;
    }

    *pu4HashIndex = u4HashValue % BGP4_MAX_PREFIX_HASH_TBL_SIZE;
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4GetRouteHashIndex                                     */
/* Description   : Gets the Hash index for a given Prefix and prefix length  */
/* Input(s)      : AddrPrefix                                                */
/*                 u4PrefixLen                                               */
/* Output(s)     : pu4HashIndex.                                             */
/* Return(s)     : BGP4_SUCCESS.                                             */
/*****************************************************************************/
INT4
Bgp4GetRouteHashIndex (tAddrPrefix AddrPrefix, UINT1 u1PrefixLen,
                       UINT4 *pu4HashIndex)
{
    UINT4               u4HashValue = 0;
    UINT2               u2Afi;
    UINT1               u1Index;

    *pu4HashIndex = 0;
    u2Afi = BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix);
    switch (u2Afi)
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4Addr = 0;
            UINT4               u4Mask = 0;

            u4Mask = Bgp4GetSubnetmask (u1PrefixLen);
            PTR_FETCH_4 (u4Addr, AddrPrefix.au1Address);
            u4Addr = u4Addr & u4Mask;
            PTR_ASSIGN_4 (AddrPrefix.au1Address, u4Addr);
            for (u1Index = 0; u1Index < BGP4_IPV4_PREFIX_LEN; u1Index++)
            {
                u4HashValue += AddrPrefix.au1Address[u1Index] %
                    BGP4_MAX_ROUTE_HASH_TBL_SIZE;
            }
            break;
        }
#ifdef VPLSADS_WANTED
            /*ADS-VPLS related processing */
        case BGP4_INET_AFI_L2VPN:
        {
            /* For VPLS case prefix is VPLS NLRI */
            for (u1Index = 0; u1Index < BGP4_VPLS_NLRI_PREFIX_LEN; u1Index++)
            {
                u4HashValue += (UINT4) (AddrPrefix.au1Address[u1Index] %
                                        BGP4_MAX_ROUTE_HASH_TBL_SIZE);
            }
            break;
        }

#endif

#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
        {
            tAddrPrefix         Mask;
            Bgp4GetIPV6Subnetmask (u1PrefixLen, Mask.au1Address);
            Bgp4MaskIpv6Address (AddrPrefix.au1Address, Mask.au1Address);
            for (u1Index = 0; u1Index < BGP4_IPV6_PREFIX_LEN; u1Index++)
            {
                u4HashValue += AddrPrefix.au1Address[u1Index] %
                    BGP4_MAX_ROUTE_HASH_TBL_SIZE;
            }
            break;
        }
#endif
        default:
            break;
    }

    *pu4HashIndex = u4HashValue % BGP4_MAX_ROUTE_HASH_TBL_SIZE;
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4GetContextEntry                                       */
/* Description   : Retruns the contextnodeentry for an input contexted       */
/* Input(s)      : u4ContextId - contextId                                   */
/* Output(s)     : None.                                                     */
/* Return(s)     : tBgp4CxtNode  pointer                                     */
/*****************************************************************************/
tBgpCxtNode        *
Bgp4GetContextEntry (UINT4 u4ContextId)
{
    if (u4ContextId >=
        FsBGPSizingParams[MAX_BGP_CXT_NODES_SIZING_ID].u4PreAllocatedUnits)
    {
        return NULL;
    }
    if (gBgpCxtNode == NULL)
    {
        return NULL;
    }
    return (gBgpCxtNode[u4ContextId]);
}

/*****************************************************************************/
/* Function Name : PrefixMatch                                               */
/* Description   : compare two given prefixes                                */
/* Input(s)      : AddrPrefix1 - first address prefix                        */
/*                 AddrPrefix2 - second address prefix                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_FALSE - Prefix Does not Match                        */
/*                 BGP4_TRUE  - Prefix Match                                 */
/*****************************************************************************/
INT4
PrefixMatch (tAddrPrefix AddrPrefix1, tAddrPrefix AddrPrefix2)
{
    if (BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix1) !=
        BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix2))
    {
        return (BGP4_FALSE);
    }

    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix1))
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4Addr1;
            UINT4               u4Addr2;

            PTR_FETCH_4 (u4Addr1,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix1));
            PTR_FETCH_4 (u4Addr2,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix2));

            if (u4Addr1 == u4Addr2)
            {
                return (BGP4_TRUE);
            }
            return (BGP4_FALSE);
        }
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
        {
            /* BGP_ST_UP_FLT_46 */
            if (MEMCMP (&AddrPrefix1.au1Address, &AddrPrefix2.au1Address,
                        BGP4_IPV6_PREFIX_LEN) == 0)
            {
                return (BGP4_TRUE);
            }
            else
            {
                return (BGP4_FALSE);
            }
        }
#endif
        default:
        {
            UINT1               u1Index = 0;

            while ((u1Index < BGP4_MAX_INET_ADDRESS_LEN) &&
                   (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix1)[u1Index]
                    ==
                    BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix2)
                    [u1Index]))
            {
                u1Index++;
            }

            if (u1Index == BGP4_MAX_INET_ADDRESS_LEN)
            {
                return (BGP4_TRUE);
            }
            return (BGP4_FALSE);
        }
    }
}

/*****************************************************************************/
/* Function Name : PrefixGreaterThan                                         */
/* Description   : compare two given prefixes                                */
/* Input(s)      : AddrPrefix1 - first address prefix                        */
/*                 AddrPrefix2 - second address prefix                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : 1         - AddrPrefix1 > AddrPrefix2                     */
/*                 0         - AddrPrefix1 <= AddrPrefix2                    */
/*****************************************************************************/
INT4
PrefixGreaterThan (tAddrPrefix AddrPrefix1, tAddrPrefix AddrPrefix2)
{
    if (BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix1) <
        BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix2))
    {
        return (0);
    }
    else if (BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix1) >
             BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix2))
    {
        return (1);
    }
    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix1))
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4Addr1;
            UINT4               u4Addr2;

            PTR_FETCH_4 (u4Addr1,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix1));
            PTR_FETCH_4 (u4Addr2,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix2));

            if (u4Addr1 > u4Addr2)
            {
                return (1);
            }
            return (0);
        }
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
        {
            /* BGP_ST_UP_FLT_46 */
            if (MEMCMP (&AddrPrefix1.au1Address, &AddrPrefix2.au1Address,
                        BGP4_IPV6_PREFIX_LEN) > 0)
            {
                return (1);
            }
            else
            {
                return (0);
            }
        }
#endif
        default:
        {
            UINT1               u1Index = 0;

            while ((u1Index < BGP4_MAX_INET_ADDRESS_LEN) &&
                   (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix1)[u1Index]
                    ==
                    BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix2)
                    [u1Index]))
            {
                u1Index++;
            }

            if (u1Index == BGP4_MAX_INET_ADDRESS_LEN)
            {
                return (0);
            }

            if (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix1)[u1Index]
                > BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix2)[u1Index])
            {
                return (1);
            }
            return (0);
        }
    }
}

/*****************************************************************************/
/* Function Name : PrefixLessThan                                            */
/* Description   : compare two given prefixes                                */
/* Input(s)      : AddrPrefix1 - first address prefix                        */
/*                 AddrPrefix2 - second address prefix                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : 1         - AddrPrefix1 < AddrPrefix2                     */
/*                 0         - AddrPrefix1 >= AddrPrefix2                    */
/*****************************************************************************/
INT4
PrefixLessThan (tAddrPrefix AddrPrefix1, tAddrPrefix AddrPrefix2)
{
    if (BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix1) !=
        BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix2))
    {
        return (0);
    }

    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix1))
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4Addr1;
            UINT4               u4Addr2;

            PTR_FETCH_4 (u4Addr1,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix1));
            PTR_FETCH_4 (u4Addr2,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix2));

            if (u4Addr1 < u4Addr2)
            {
                return (1);
            }
            return (0);
        }
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
        {
            /* BGP_ST_UP_FLT_46 */
            if (MEMCMP (&AddrPrefix1.au1Address, &AddrPrefix2.au1Address,
                        BGP4_IPV6_PREFIX_LEN) < 0)
            {
                return (1);
            }
            else
            {
                return (0);
            }
        }
#endif
        default:
        {
            UINT1               u1Index = 0;

            while ((u1Index < BGP4_MAX_INET_ADDRESS_LEN) &&
                   (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix1)[u1Index]
                    ==
                    BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix2)
                    [u1Index]))
            {
                u1Index++;
            }

            if (u1Index == BGP4_MAX_INET_ADDRESS_LEN)
            {
                return (0);
            }

            if (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix1)[u1Index]
                < BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix2)[u1Index])
            {
                return (1);
            }
            return (0);
        }
    }
}

/*****************************************************************************/
/* Function Name : Bgp4CopyPrefix                                            */
/* Description   : copies source prefix into destination prefix              */
/* Input(s)      : DstAddr - pointer to destination address prefix           */
/*                 SrcAddr - source address prefix                           */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4CopyPrefix (tAddrPrefix * DstAddr, tAddrPrefix SrcAddr)
{
    if (DstAddr->u2Afi != BGP4_AFI_IN_ADDR_PREFIX_INFO (SrcAddr))
    {
        return (BGP4_FAILURE);
    }

    /* Intialize with all zero's */
    MEMSET (DstAddr->au1Address, 0, BGP4_MAX_INET_ADDRESS_LEN);
    MEMCPY (DstAddr->au1Address,
            BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (SrcAddr),
            BGP4_MAX_INET_ADDRESS_LEN);
    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Bgp4CopyAddrPrefixStruct                                  */
/* Description   : copies source address prefix struct into destination addr */
/*                 prefix structure                                          */
/* Input(s)      : DstAddr - pointer to destination address prefix struct    */
/*                 SrcAddr - source address prefix struct                    */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4CopyAddrPrefixStruct (tAddrPrefix * DstAddr, tAddrPrefix SrcAddr)
{
    DstAddr->u2Afi = BGP4_AFI_IN_ADDR_PREFIX_INFO (SrcAddr);
    DstAddr->u2AddressLen = BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO (SrcAddr);
    Bgp4CopyPrefix (DstAddr, SrcAddr);
#ifdef VPLSADS_WANTED
    /*Coverty Fix */
    MEMSET (&(DstAddr->au1Pad), 0, sizeof (DstAddr->au1Pad));
#endif
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4CopyNetAddressStruct                                  */
/* Description   : copies source net address struct into destination net     */
/*                 address structure                                         */
/* Input(s)      : SrcAddr - destination net address struct                  */
/*                 DstAddr - source net address struct                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4CopyNetAddressStruct (tNetAddress * DstAddr, tNetAddress SrcAddr)
{
    DstAddr->u2Safi = BGP4_SAFI_IN_NET_ADDRESS_INFO (SrcAddr);
    DstAddr->u2PrefixLen = BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (SrcAddr);
    Bgp4CopyAddrPrefixStruct (&(DstAddr->NetAddr),
                              BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                              (SrcAddr));
    return BGP4_SUCCESS;
}

#ifdef L3VPN
/*****************************************************************************/
/* Function Name : Bgp4CopyVpn4NetAddressFromRt                            */
/* Description   : copies source vpnv4 net address struct into destination  */
/*                   net  address structure                                 */
/* Input(s)      : pRtProfile    -Vpnv4 Route                               */
/*                 DstAddr - source net address struct                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4CopyVpn4NetAddressFromRt (tNetAddress * DstAddr, tRouteProfile * pRtProfile)
{
    UINT4               u4Addr;
    DstAddr->u2Safi = BGP4_RT_SAFI_INFO (pRtProfile);
    DstAddr->u2PrefixLen = BGP4_RT_PREFIXLEN (pRtProfile);
    DstAddr->NetAddr.u2Afi = BGP4_RT_AFI_INFO (pRtProfile);
    DstAddr->NetAddr.u2AddressLen = BGP4_RT_PREFIXLEN (pRtProfile);
    MEMCPY (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (DstAddr->NetAddr),
            BGP4_RT_ROUTE_DISTING (pRtProfile), BGP4_VPN4_ROUTE_DISTING_SIZE);
    PTR_FETCH_4 (u4Addr, BGP4_RT_IP_PREFIX (pRtProfile));
    PTR_ASSIGN_4 (((BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (DstAddr->NetAddr)) +
                   BGP4_VPN4_ROUTE_DISTING_SIZE), u4Addr);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4CopyVpn4NetAddressToRt                               */
/* Description   : copies source vpnv4 net address struct into destination  */
/*                   net  address structure                                 */
/* Input(s)      : pRtProfile    -Vpnv4 Route                               */
/*                 SrcAddr - source net address struct                       */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4CopyVpn4NetAddressToRt (tRouteProfile * pRtProfile, tNetAddress SrcAddr)
{
    UINT4               u4Addr;
    BGP4_RT_SAFI_INFO (pRtProfile) = BGP4_SAFI_IN_NET_ADDRESS_INFO (SrcAddr);
    BGP4_RT_PREFIXLEN (pRtProfile) =
        BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (SrcAddr);
    BGP4_RT_AFI_INFO (pRtProfile) =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (SrcAddr.NetAddr);
    MEMCPY (BGP4_RT_ROUTE_DISTING (pRtProfile),
            BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (SrcAddr.NetAddr),
            BGP4_VPN4_ROUTE_DISTING_SIZE);
    PTR_FETCH_4 (u4Addr,
                 ((BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (SrcAddr.NetAddr)) +
                  BGP4_VPN4_ROUTE_DISTING_SIZE));
    PTR_ASSIGN_4 (BGP4_RT_IP_PREFIX (pRtProfile), u4Addr);
    return BGP4_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name : Bgp4PrintAddress                                          */
/* Description   : Prints the prefix present in AddrPrefix structure         */
/* Input(s)      : u4Flag - configured Debug/Trace Flag value                */
/*                 u4Value - Debug/Trace level value                         */
/*                 AddrPrefix - address prefix structure                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4PrintAddress (UINT4 u4Flag, UINT4 u4Value,
                  tAddrPrefix AddrPrefix, UINT1 u1PrintStatus)
{
    char                ai1LogMsgBuf[256];

    /* If both DEBUG and TRACE is disabled don't print anything */
    if (!(u4Flag & u4Value))
    {
        return BGP4_SUCCESS;
    }

    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix))
    {
        case BGP4_INET_AFI_IPV4:
        {
            /* The address will be in host order */
            if (u1PrintStatus == BGP4_PRINT_NEW_LINE)
            {
                SPRINTF (ai1LogMsgBuf, "%d.%d.%d.%d\n",
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix)[3],
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix)[2],
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix)[1],
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix)[0]);
            }
            else
            {
                SPRINTF (ai1LogMsgBuf, "%d.%d.%d.%d",
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix)[3],
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix)[2],
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix)[1],
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix)[0]);
            }
        }
            break;
        default:
            return BGP4_SUCCESS;
    }
    UtlTrcPrint (ai1LogMsgBuf);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4GetAfiSafiMask                                        */
/* Description   : Given an update buffer having either MP_REACH_NLRI or     */
/*                 MP_UNREACH_NLRI attribute value, this gives the           */
/*                 <AFI, SAFI> mask                                          */
/* Input(s)      : update buffer                                             */
/* Output(s)     : None                                                      */
/* Return(s)     : <AFI, SAFI> mask                                          */
/*****************************************************************************/
UINT4
Bgp4GetAfiSafiMask (UINT1 *pu1Buf)
{
    UINT4               u4AsafiMask = 0;
    UINT2               u2RcvdAfi = 0;
    UINT1               u1RcvdSafi = 0;
    UINT1               u1Flag = 0;

    u1Flag = *pu1Buf;

    if (u1Flag & BGP4_EXT_LEN_FLAG_MASK)
    {
        PTR_FETCH2 (u2RcvdAfi,
                    (pu1Buf + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                     BGP4_EXTENDED_ATTR_LEN_SIZE));
        u1RcvdSafi = *(pu1Buf + BGP4_ATYPE_FLAGS_LEN +
                       BGP4_ATYPE_CODE_LEN + BGP4_EXTENDED_ATTR_LEN_SIZE +
                       BGP4_MPE_ADDRESS_FAMILY_LEN);
    }
    else
    {
        PTR_FETCH2 (u2RcvdAfi,
                    (pu1Buf + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                     BGP4_NORMAL_ATTR_LEN_SIZE));
        u1RcvdSafi = *(pu1Buf + BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                       BGP4_NORMAL_ATTR_LEN_SIZE + BGP4_MPE_ADDRESS_FAMILY_LEN);
    }

    u4AsafiMask = ((UINT4) u2RcvdAfi << BGP4_TWO_BYTE_BITS) | u1RcvdSafi;

    return u4AsafiMask;
}

/*****************************************************************************/
/* Function Name : Bgp4GetAfiSafiIndex                                       */
/* Description   : Given address family and sub-address family, this function*/
/*                 gives the index correspond to that <AFI, SAFI>            */
/* Input(s)      : u2Afi - address family                                    */
/*                 u2Safi - subsequent address family                        */
/* Output(s)     : Index correspond to <AFI, SAFI>                           */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE - index found/unfound           */
/*****************************************************************************/

INT4
Bgp4GetAfiSafiIndex (UINT2 u2Afi, UINT2 u2Safi, UINT4 *u4Index)
{
    UINT4               u4AsafiMask = 0;

    u4AsafiMask = ((UINT4) u2Afi << BGP4_TWO_BYTE_BITS) | u2Safi;
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
            *u4Index = BGP4_IPV4_UNI_INDEX;
            break;
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
            /* Carrying Label Information - RFC 3107 */
            *u4Index = BGP4_IPV4_LBLD_INDEX;
            break;
#endif
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
            *u4Index = BGP4_IPV6_UNI_INDEX;
            break;
#endif
#ifdef L3VPN
        case CAP_MP_VPN4_UNICAST:
            *u4Index = BGP4_VPN4_UNI_INDEX;
            break;
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
            *u4Index = BGP4_L2VPN_VPLS_INDEX;
            break;
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
            *u4Index = BGP4_L2VPN_EVPN_INDEX;
            break;
#endif
        default:
            return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4GetAfiSafiMaskFromIndex                               */
/* Description   : Given Index to the <AFI,SAFI>, this function return the   */
/*                 mask associated with that <AFI,SAFI>                      */
/* Input(s)      : Index correspond to <AFI, SAFI>                           */
/* output(s)     : pu4AsafiMask - Mask for the Index.                        */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE - index found/unfound           */
/*****************************************************************************/

INT4
Bgp4GetAfiSafiMaskFromIndex (UINT4 u4Index, UINT4 *pu4AsafiMask)
{

    switch (u4Index)
    {
        case BGP4_IPV4_UNI_INDEX:
            *pu4AsafiMask = CAP_MP_IPV4_UNICAST;
            break;
#ifdef L3VPN
        case BGP4_IPV4_LBLD_INDEX:
            /* Carrying Label Information - RFC 3107 */
            *pu4AsafiMask = CAP_MP_LABELLED_IPV4;
            break;
#endif
#ifdef BGP4_IPV6_WANTED
        case BGP4_IPV6_UNI_INDEX:
            *pu4AsafiMask = CAP_MP_IPV6_UNICAST;
            break;
#endif
#ifdef L3VPN
        case BGP4_VPN4_UNI_INDEX:
            *pu4AsafiMask = CAP_MP_VPN4_UNICAST;
            break;
#endif
        default:
            return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4GetAfiSafiFromIndex                                   */
/* Description   : Given address family and sub-address family, this function*/
/*                 gives the index correspond to that <AFI, SAFI>            */
/* Input(s)      : Index correspond to <AFI, SAFI>                           */
/* output(s)      : u2Afi - address family                                   */
/*                 u2Safi - subsequent address family                        */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE - index found/unfound           */
/*****************************************************************************/

INT4
Bgp4GetAfiSafiFromIndex (UINT4 u4Index, UINT2 *pu2Afi, UINT2 *pu2Safi)
{
    UINT4               u4AsafiMask;

    switch (u4Index)
    {
        case BGP4_IPV4_UNI_INDEX:
            u4AsafiMask = CAP_MP_IPV4_UNICAST;
            break;
#ifdef L3VPN
            /* Carrying Label Information - RFC 3107 */
        case BGP4_IPV4_LBLD_INDEX:
            u4AsafiMask = CAP_MP_LABELLED_IPV4;
            break;
        case BGP4_VPN4_UNI_INDEX:
            u4AsafiMask = CAP_MP_VPN4_UNICAST;
            break;
#endif
#ifdef BGP4_IPV6_WANTED
        case BGP4_IPV6_UNI_INDEX:
            u4AsafiMask = CAP_MP_IPV6_UNICAST;
            break;
#endif
#ifdef VPLSADS_WANTED
        case BGP4_L2VPN_VPLS_INDEX:
            u4AsafiMask = CAP_MP_L2VPN_VPLS;
            break;
#endif
#ifdef EVPN_WANTED
        case BGP4_L2VPN_EVPN_INDEX:
            u4AsafiMask = CAP_MP_L2VPN_EVPN;
            break;
#endif
        default:
            return BGP4_FAILURE;
    }
    Bgp4GetAfiSafiFromMask (u4AsafiMask, pu2Afi, pu2Safi);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4GetAfiSafiFromMask*/
/* Description   : Given Afi and Safi mask, this function gives the afi      */
/*                 and safi                                                  */
/*  Input(s)     : u4AfiSafiMask                                             */
/*  Output(s)    : u2Afi - address family                                    */
/*                 u2Safi - subsequent address family                        */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4GetAfiSafiFromMask (UINT4 u4AfiSafiMask, UINT2 *pu2Afi, UINT2 *pu2Safi)
{

    *pu2Safi = (UINT2) (u4AfiSafiMask & BGP4_ONE_BYTE_MAX_VAL);
    *pu2Afi = (UINT2) ((u4AfiSafiMask >> BGP4_TWO_BYTE_BITS) &
                       BGP4_INET_MAX_AFI);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4CopyInfoToMpeInfo                                     */
/* Description   : Copies the BGP Information except nexthop and immediate   */
/*                 nexthop information                                       */
/* Input(s)      : Source BGP Route's Info (pSrcBgp4Info)                    */
/*                 Target BGP Route's Info (pDestBgp4Info)                   */
/* Output(s)     : Target BGP Route's Information which contains the         */
/*                 copied information(pDestBgp4Info).                        */
/* Return(s)     : None                                                      */
/*****************************************************************************/
INT4
Bgp4CopyInfoToMpeInfo (tBgp4Info * pDestBgp4Info, tBgp4Info * pSrcBgp4Info)
{
    tAsPath            *pDestAspath = NULL;
    tAsPath            *pSrcAspath = NULL;

    if ((pDestBgp4Info == NULL) || (pSrcBgp4Info == NULL))
    {
        return BGP4_FAILURE;
    }

    /* TSASPath */
    TMO_SLL_Scan (BGP4_INFO_ASPATH (pSrcBgp4Info), pSrcAspath, tAsPath *)
    {
        pDestAspath = Bgp4MemGetASNode (sizeof (tAsPath));
        if (pDestAspath == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                      BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tBgp4AggrCopyBgp4infos() : GET_ASNode failed !!! \n");
            gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
            return BGP4_FAILURE;
        }
        BGP4_ASPATH_TYPE (pDestAspath) = BGP4_ASPATH_TYPE (pSrcAspath);
        BGP4_ASPATH_LEN (pDestAspath) = BGP4_ASPATH_LEN (pSrcAspath);
        if (BGP4_AS4_SEG_LEN <
            (BGP4_ASPATH_LEN (pDestAspath) * BGP4_AS4_LENGTH))
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                      BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tBgp4AggrCopyBgp4infos: Size of au1ASSegs is less,increase the value of BGP4_AS_SEG_LEN \n");
            Bgp4MemReleaseASNode (pDestAspath);
            return BGP4_FAILURE;
        }
        MEMSET (BGP4_ASPATH_NOS (pDestAspath), 0, BGP4_AS4_SEG_LEN);
        MEMCPY (BGP4_ASPATH_NOS (pDestAspath), BGP4_ASPATH_NOS (pSrcAspath),
                (BGP4_ASPATH_LEN (pSrcAspath) * BGP4_AS4_LENGTH));

        TMO_SLL_Add (BGP4_INFO_ASPATH (pDestBgp4Info), &pDestAspath->sllNode);
    }

    /* tAggregator */
    if (BGP4_INFO_AGGREGATOR (pSrcBgp4Info) != NULL)
    {
        AGGR_NODE_CREATE (BGP4_INFO_AGGREGATOR (pDestBgp4Info));

        if (BGP4_INFO_AGGREGATOR (pDestBgp4Info) == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                      BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for MPE Aggregator Attribute "
                      "FAILED!!!\n");
            gu4BgpDebugCnt[MAX_BGP_AGGR_NODE_SIZING_ID]++;
            return BGP4_FAILURE;
        }

        BGP4_INFO_AGGREGATOR_AS (pDestBgp4Info) =
            BGP4_INFO_AGGREGATOR_AS (pSrcBgp4Info);
        BGP4_INFO_AGGREGATOR_NODE (pDestBgp4Info) =
            BGP4_INFO_AGGREGATOR_NODE (pSrcBgp4Info);
        BGP4_INFO_AGGREGATOR_FLAG (pDestBgp4Info) =
            BGP4_INFO_AGGREGATOR_FLAG (pSrcBgp4Info);
    }

    /* Originator Identifier Attribute. */
    BGP4_INFO_ORIG_ID (pDestBgp4Info) = BGP4_INFO_ORIG_ID (pSrcBgp4Info);

    /* ClusterList Attribute. */
    if (BGP4_INFO_CLUS_LIST_ATTR (pSrcBgp4Info) != NULL)
    {
        CLUSTER_LIST_CREATE (BGP4_INFO_CLUS_LIST_ATTR (pDestBgp4Info));
        if (BGP4_INFO_CLUS_LIST_ATTR (pDestBgp4Info) == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                      BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for MPE Cluster-List Attribute "
                      "FAILED!!!\n");
            AGGR_NODE_FREE (BGP4_INFO_AGGREGATOR (pDestBgp4Info));
            gu4BgpDebugCnt[MAX_BGP_CLUSTER_LIST_SIZING_ID]++;
            return BGP4_FAILURE;
        }
        ATTRIBUTE_NODE_CREATE (BGP4_INFO_CLUS_LIST_ATTR_VAL (pDestBgp4Info));
        if (BGP4_INFO_CLUS_LIST_ATTR_VAL (pDestBgp4Info) == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                      BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for MPE Cluster-List Attribute "
                      "FAILED!!!\n");
            CLUSTER_LIST_FREE (BGP4_INFO_CLUS_LIST_ATTR (pDestBgp4Info));
            AGGR_NODE_FREE (BGP4_INFO_AGGREGATOR (pDestBgp4Info));
            gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
            return BGP4_FAILURE;
        }
        BGP4_INFO_CLUS_LIST_ATTR_FLAG (pDestBgp4Info) =
            BGP4_INFO_CLUS_LIST_ATTR_FLAG (pSrcBgp4Info);
        BGP4_INFO_CLUS_LIST_COUNT (pDestBgp4Info) =
            BGP4_INFO_CLUS_LIST_COUNT (pSrcBgp4Info);
        MEMCPY (BGP4_INFO_CLUS_LIST_ATTR_VAL (pDestBgp4Info),
                BGP4_INFO_CLUS_LIST_ATTR_VAL (pSrcBgp4Info),
                (BGP4_INFO_CLUS_LIST_COUNT (pSrcBgp4Info) * CLUSTER_ID_LENGTH));
    }

    /* Community Attribute. */
    if (BGP4_INFO_COMM_ATTR (pSrcBgp4Info) != NULL)
    {
        COMMUNITY_NODE_CREATE (BGP4_INFO_COMM_ATTR (pDestBgp4Info));

        if (BGP4_INFO_COMM_ATTR (pDestBgp4Info) == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                      BGP4_MOD_NAME,
                      "\tMemory Allocation for MPE Community Attribute "
                      "FAILED!!!\n");
            CLUSTER_LIST_FREE (BGP4_INFO_CLUS_LIST_ATTR (pDestBgp4Info));
            AGGR_NODE_FREE (BGP4_INFO_AGGREGATOR (pDestBgp4Info));
            gu4BgpDebugCnt[MAX_BGP_COMMUNITY_NODE_SIZING_ID]++;
            return BGP4_FAILURE;
        }
        ATTRIBUTE_NODE_CREATE (BGP4_INFO_COMM_ATTR_VAL (pDestBgp4Info));
        if (BGP4_INFO_COMM_ATTR_VAL (pDestBgp4Info) == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                      BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for MPE Community Attribute "
                      "FAILED!!!\n");
            COMMUNITY_NODE_FREE (BGP4_INFO_COMM_ATTR (pDestBgp4Info));
            CLUSTER_LIST_FREE (BGP4_INFO_CLUS_LIST_ATTR (pDestBgp4Info));
            AGGR_NODE_FREE (BGP4_INFO_AGGREGATOR (pDestBgp4Info));
            gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
            return BGP4_FAILURE;
        }
        BGP4_INFO_COMM_ATTR_FLAG (pDestBgp4Info) =
            BGP4_INFO_COMM_ATTR_FLAG (pSrcBgp4Info);
        BGP4_INFO_COMM_COUNT (pDestBgp4Info) =
            BGP4_INFO_COMM_COUNT (pSrcBgp4Info);
        MEMCPY (BGP4_INFO_COMM_ATTR_VAL (pDestBgp4Info),
                BGP4_INFO_COMM_ATTR_VAL (pSrcBgp4Info),
                (BGP4_INFO_COMM_COUNT (pSrcBgp4Info) * COMM_VALUE_LEN));
    }

    /* Ext-Community Attribute. */
    if (BGP4_INFO_ECOMM_ATTR (pSrcBgp4Info) != NULL)
    {
        EXT_COMMUNITY_NODE_CREATE (BGP4_INFO_ECOMM_ATTR (pDestBgp4Info));
        if (BGP4_INFO_ECOMM_ATTR (pDestBgp4Info) == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                      BGP4_MOD_NAME,
                      "\tMemory Allocation for MPE Ext-Community Attribute "
                      "FAILED!!!\n");
            COMMUNITY_NODE_FREE (BGP4_INFO_COMM_ATTR (pDestBgp4Info));
            CLUSTER_LIST_FREE (BGP4_INFO_CLUS_LIST_ATTR (pDestBgp4Info));
            AGGR_NODE_FREE (BGP4_INFO_AGGREGATOR (pDestBgp4Info));
            gu4BgpDebugCnt[MAX_BGP_EXT_COMM_NODE_SIZING_ID]++;
            return BGP4_FAILURE;
        }
        ATTRIBUTE_NODE_CREATE (BGP4_INFO_ECOMM_ATTR_VAL (pDestBgp4Info));
        if (BGP4_INFO_ECOMM_ATTR_VAL (pDestBgp4Info) == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                      BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for MPE Ext-Community Attribute "
                      "FAILED!!!\n");
            EXT_COMMUNITY_NODE_FREE (BGP4_INFO_ECOMM_ATTR (pDestBgp4Info));
            COMMUNITY_NODE_FREE (BGP4_INFO_COMM_ATTR (pDestBgp4Info));
            CLUSTER_LIST_FREE (BGP4_INFO_CLUS_LIST_ATTR (pDestBgp4Info));
            AGGR_NODE_FREE (BGP4_INFO_AGGREGATOR (pDestBgp4Info));
            gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
            return BGP4_FAILURE;
        }
        BGP4_INFO_ECOMM_ATTR_FLAG (pDestBgp4Info) =
            BGP4_INFO_ECOMM_ATTR_FLAG (pSrcBgp4Info);
        BGP4_INFO_ECOMM_COUNT (pDestBgp4Info) =
            BGP4_INFO_ECOMM_COUNT (pSrcBgp4Info);
        MEMCPY (BGP4_INFO_ECOMM_ATTR_VAL (pDestBgp4Info),
                BGP4_INFO_ECOMM_ATTR_VAL (pSrcBgp4Info),
                (BGP4_INFO_ECOMM_COUNT (pSrcBgp4Info) * EXT_COMM_VALUE_LEN));
    }

    /* pu1UnknownAttr */
    if (BGP4_INFO_UNKNOWN_ATTR_LEN (pSrcBgp4Info) > 0)
    {
        ATTRIBUTE_NODE_CREATE (BGP4_INFO_UNKNOWN_ATTR (pDestBgp4Info));
        if (BGP4_INFO_UNKNOWN_ATTR (pDestBgp4Info) == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                      BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for MPE Unknown Attribute "
                      "FAILED!!!\n");
            EXT_COMMUNITY_NODE_FREE (BGP4_INFO_ECOMM_ATTR (pDestBgp4Info));
            COMMUNITY_NODE_FREE (BGP4_INFO_COMM_ATTR (pDestBgp4Info));
            CLUSTER_LIST_FREE (BGP4_INFO_CLUS_LIST_ATTR (pDestBgp4Info));
            AGGR_NODE_FREE (BGP4_INFO_AGGREGATOR (pDestBgp4Info));
            gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
            return BGP4_FAILURE;
        }
        MEMCPY ((UINT1 *) BGP4_INFO_UNKNOWN_ATTR (pDestBgp4Info),
                BGP4_INFO_UNKNOWN_ATTR (pSrcBgp4Info),
                BGP4_INFO_UNKNOWN_ATTR_LEN (pSrcBgp4Info));
    }
    /* u4UnknownAttrLen */
    BGP4_INFO_UNKNOWN_ATTR_LEN (pDestBgp4Info) =
        BGP4_INFO_UNKNOWN_ATTR_LEN (pSrcBgp4Info);

    /* u4RecdMED */
    BGP4_INFO_RCVD_MED (pDestBgp4Info) = BGP4_INFO_RCVD_MED (pSrcBgp4Info);
    /* u4RecdLocalPref */
    BGP4_INFO_RCVD_LOCAL_PREF (pDestBgp4Info) =
        BGP4_INFO_RCVD_LOCAL_PREF (pSrcBgp4Info);

    /* u2AttrFlag */
    BGP4_INFO_ATTR_FLAG (pDestBgp4Info) |= BGP4_INFO_ATTR_FLAG (pSrcBgp4Info);

    /* u1Origin */
    BGP4_INFO_ORIGIN (pDestBgp4Info) = BGP4_INFO_ORIGIN (pSrcBgp4Info);
    /* u2Weight */
    BGP4_INFO_WEIGHT (pDestBgp4Info) = BGP4_INFO_WEIGHT (pSrcBgp4Info);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : NetAddrMatch                                              */
/* Description   : compare "prefix length" bits of an address                */
/* Input(s)      : AddrPrefix1 - first address prefix                        */
/*                 AddrPrefix2 - second address prefix                       */
/*                 u4PfxLen    - prefix length                               */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_FALSE - NetAddresses Does not Match                  */
/*                 BGP4_TRUE  - NetAddresses Match                           */
/*****************************************************************************/
INT4
NetAddrMatch (tNetAddress NetAddr1, tNetAddress NetAddr2)
{
    if (BGP4_SAFI_IN_NET_ADDRESS_INFO (NetAddr1) !=
        BGP4_SAFI_IN_NET_ADDRESS_INFO (NetAddr2))
    {
        return (BGP4_FALSE);
    }

    if (BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddr1) !=
        BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddr2))
    {
        return (BGP4_FALSE);
    }

    if ((AddrMatch (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddr1),
                    BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (NetAddr2),
                    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (NetAddr1)))
        != BGP4_TRUE)
    {
        return (BGP4_FALSE);
    }

    return (BGP4_TRUE);
}

#define MAX_INCR(x,y)    ((x == (y-1)) ? x = 0 : ++x)

/*****************************************************************************/
/* Function Name : Bgp4PrintIpAddr                                           */
/* Description   : This routine takes the IP address in the decimal form and */
/*                 returns in the dotted notation.                           */
/* Input(s)      : Ip Address (pu1IpAddr)                                    */
/* Output(s)     : None.                                                     */
/* Return(s)     : IP Address in dotted notation (pu1IpAddr)                 */
/*****************************************************************************/
UINT1              *
Bgp4PrintIpAddr (UINT1 *pu1IpAddr, UINT2 u2Afi)
{
    PRIVATE UINT1       au1IpAddrBuffer[5][BGP4_PRINT_BUF_SIZE];
    PRIVATE UINT1       u1Index = 0;
    MAX_INCR (u1Index, 5);

    switch (u2Afi)
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4RemAddr = 0;
            tAddrPrefix         PeerRemAddr;

            PTR_FETCH4 (u4RemAddr, pu1IpAddr);
            Bgp4InitAddrPrefixStruct (&PeerRemAddr, BGP4_INET_AFI_IPV4);
            MEMCPY (PeerRemAddr.au1Address, &u4RemAddr, sizeof (UINT4));
            pu1IpAddr = PeerRemAddr.au1Address;
            SPRINTF ((char *) au1IpAddrBuffer[u1Index], "%d.%d.%d.%d",
                     *(pu1IpAddr), *(pu1IpAddr + 1), *(pu1IpAddr + 2),
                     *(pu1IpAddr + 3));
        }
            break;
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
        {
            tIp6Addr            Addr;
            UINT1              *pTmpBuf = NULL;

            MEMCPY (&Addr.u1_addr[0], pu1IpAddr, BGP4_IPV6_PREFIX_LEN);
            if ((pTmpBuf = Ip6PrintNtop (&Addr)) == NULL)
            {
                return (UINT1 *) &au1IpAddrBuffer[u1Index];
            }
            STRCPY (au1IpAddrBuffer[u1Index], pTmpBuf);
        }
            break;
#endif
#ifdef L3VPN
            /* Make this function to work for ALL families  :( :( */
#endif
#ifdef VPLSADS_WANTED
        case BGP4_INET_AFI_L2VPN:
        {
            tVplsNlriInfo       NlriInfo;
            Bgp4VplsGetNlriInfoFromNetAddr (pu1IpAddr, &NlriInfo);

            SPRINTF ((char *) au1IpAddrBuffer[u1Index],
                     "Length = %d, VE-ID = %d, VBO = %d, VBS = %d LB = %d",
                     NlriInfo.u2Length,
                     /*NlriInfo., */ NlriInfo.u2VeId, NlriInfo.u2VeBaseOffset,
                     NlriInfo.u2VeBaseSize, NlriInfo.u4labelBase);
        }
            break;

#endif
        default:
            break;

    }
    return (UINT1 *) &au1IpAddrBuffer[u1Index];
}

#ifdef BGP4_IPV6_WANTED

#define BGP4_IP6_ADDR_ACCESS_IN_SHORT_INT   8
static UINT1        gau1Hexc[] = "0123456789abcdef";

/*****************************************************************************/
/* Function Name : Bgp4PrintIp6Addr                                          */
/* Description   : This routine takes the IPv6 address in a linear buffer    */
/*                 returns in the dotted notation.                           */
/* Input(s)      : Ip Address (pu1IpAddr)                                    */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4PrintIp6Addr (UINT1 *pu1IpAddr, UINT1 *pu1IpAddrBuffer)
{
    UINT1              *pu1CP = NULL;
    UINT1              *pu1Tmp = NULL;
    INT4                start = -1, count = 0, i = 0, value = 0;
    INT4                i4Status, i4Position = -1, i4PreCount = 0;

    if (pu1IpAddr == NULL)
    {
        return BGP4_FAILURE;
    }

    MEMSET (pu1IpAddrBuffer, 0, BGP4_PRINT_BUF_SIZE);

    pu1CP = pu1IpAddrBuffer;

    pu1Tmp = pu1CP;

    /* weird special cases for IPv4 address */
    BGP4_IN6_IS_ADDR_V4COMPATIBLE (pu1IpAddr, i4Status);
    if (i4Status == BGP4_TRUE)
    {
        SPRINTF ((char *) pu1CP, "::%x%x:%d.%d.%d.%d", pu1IpAddr[10],
                 pu1IpAddr[11], pu1IpAddr[12], pu1IpAddr[13], pu1IpAddr[14],
                 pu1IpAddr[15]);
        return BGP4_SUCCESS;
    }

    if ((pu1IpAddr[0] == 0) && (pu1IpAddr[1] == 0))
    {
        if (pu1IpAddr[2] == 0)
        {
            if (pu1IpAddr[3] == 0)
            {
                STRCPY (pu1CP, "::");
            }
            else if (OSIX_NTOHL (pu1IpAddr[3]))
            {
                SPRINTF ((char *) pu1CP, "::%x", pu1IpAddr[15]);
            }
            return BGP4_SUCCESS;
        }
    }

    /*
     * Find the first string of consecutive zeros
     */
    for (i = 0; i < BGP4_IP6_ADDR_ACCESS_IN_SHORT_INT; i++)
    {
        if ((pu1IpAddr[2 * i] == 0) && (pu1IpAddr[2 * i + 1] == 0))
        {
            if (start > -1)
            {
                count++;
            }
            else
            {
                start = i;
                count = 1;
            }
        }
        else
        {
            if (count > 1)
            {
                if ((i < BGP4_IP6_ADDR_ACCESS_IN_SHORT_INT)
                    && (i4PreCount == 0))
                {
                    i4Position = start;
                    i4PreCount = count;
                    start = -1;
                    count = 0;
                }
                else
                {
                    break;
                }
            }
            else
            {
                start = -1;
                count = 0;
            }
        }
    }
    if (i4PreCount >= count)
    {
        count = i4PreCount;
        start = i4Position;
    }

    if (start == 0)
    {
        *pu1Tmp++ = ':';
    }

    for (i = 0; i < BGP4_IP6_ADDR_ACCESS_IN_SHORT_INT; i++)
    {
        if ((i == start) && (IP6_ADDR_CURR_POS != start))
        {
            *pu1Tmp++ = ':';
            i += count - 1;
        }
        else
        {
            value = (pu1IpAddr[2 * i] << 8) | pu1IpAddr[2 * i + 1];
            if (value >> 12)
            {
                *pu1Tmp++ = gau1Hexc[(value >> 12) & 0xf];
            }

            if (value >> 8)
            {
                *pu1Tmp++ = gau1Hexc[(value >> 8) & 0xf];
            }
            if (value >> 4)
            {
                *pu1Tmp++ = gau1Hexc[(value >> 4) & 0xf];
            }

            *pu1Tmp++ = gau1Hexc[value & 0xf];
            if (i != 7)
            {
                *pu1Tmp++ = ':';
            }
        }
    }

    *pu1Tmp = '\0';
    return BGP4_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name : Bgp4PrintIpMask                                           */
/* Description   : This routine takes the Prefix Len value, converts and     */
/*                 returns in the dotted notation.                           */
/* Input(s)      : Ip Mask (u4IpMask)                                        */
/* Output(s)     : None.                                                     */
/* Return(s)     : IP Mask in dotted notation (u1IpMaskBuffer)               */
/*****************************************************************************/
const INT1         *
Bgp4PrintIpMask (UINT1 u1PrefixLen, UINT2 u2Afi)
{
    PRIVATE INT1        i1IpMaskBuffer[BGP4_PRINT_BUF_SIZE] = { 0 };
    UINT1               au1IpMask[16] = { 0 };
    UINT4               u4Mask;

    switch (u2Afi)
    {
        case BGP4_INET_AFI_IPV4:
            u4Mask = Bgp4GetSubnetmask (u1PrefixLen);
            PTR_ASSIGN4 (au1IpMask, u4Mask);
            SPRINTF ((CHR1 *) & i1IpMaskBuffer, "%d.%d.%d.%d",
                     *au1IpMask, *(au1IpMask + 1), *(au1IpMask + 2),
                     *(au1IpMask + 3));
            break;

#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
            Bgp4GetIPV6Subnetmask (u1PrefixLen, au1IpMask);

            SPRINTF ((CHR1 *) & i1IpMaskBuffer,
                     "%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:%d",
                     *(au1IpMask + 0), *(au1IpMask + 1), *(au1IpMask + 2),
                     *(au1IpMask + 3), *(au1IpMask + 4), *(au1IpMask + 5),
                     *(au1IpMask + 6), *(au1IpMask + 7), *(au1IpMask + 8),
                     *(au1IpMask + 9), *(au1IpMask + 10), *(au1IpMask + 11),
                     *(au1IpMask + 12), *(au1IpMask + 13), *(au1IpMask + 14),
                     *(au1IpMask + 15));

            break;
#endif
#ifdef VPLSADS_WANTED
        case BGP4_INET_AFI_L2VPN:
            SPRINTF ((CHR1 *) & i1IpMaskBuffer, "Length = %d", u1PrefixLen);
#endif

        default:
            break;

    }

    return ((const INT1 *) &i1IpMaskBuffer);
}

/*****************************************************************************/
/* Function Name : Bgp4PrintCodeName                                         */
/* Description   : For a given code value and code it gives the code name    */
/* Input(s)      : u1CodeVal - Code Value                                    */
/*                 u1Code -   The code to which the code value belongs       */
/* Output(s)     : None                                                      */
/* Return(s)     : pu1CodeName - The code name of the given code value and   */
/*                               code                                        */
/*****************************************************************************/
UINT1              *
Bgp4PrintCodeName (UINT1 u1CodeVal, UINT1 u1Code)
{
    PRIVATE UINT1       au1CodeName[MAX_CODE_NAME];

    MEMSET (au1CodeName, 0, MAX_CODE_NAME);

    switch (u1Code)
    {
        case BGP4_SUP_CAP_CODE_NAME:
            switch (u1CodeVal)
            {
                case CAP_CODE_MP_EXTN:
                    MEMCPY (au1CodeName, "MP_EXTENSION",
                            STRLEN ("MP_EXTENSION"));
                    break;
                case CAP_CODE_ROUTE_REFRESH:
                    MEMCPY (au1CodeName, "ROUTE_REFRESH",
                            STRLEN ("ROUTE_REFRESH"));
                    break;
                case CAP_CODE_MULTIPLE_ROUTES:
                    MEMCPY (au1CodeName, "MULTIPLE_ROUTES",
                            STRLEN ("MULTIPLE_ROUTES"));
                    break;
                case CAP_CODE_GRACEFUL_RESTART:
                    MEMCPY (au1CodeName, "GRACEFUL_RESTART",
                            STRLEN ("GRACEFUL_RESTART"));
                    break;
                case CAP_CODE_4_OCTET_ASNO:
                    MEMCPY (au1CodeName, "4_OCTET_ASNO",
                            STRLEN ("4_OCTET_ASNO"));
                    break;
                case CAP_CODE_DYNAMIC_CAP:
                    MEMCPY (au1CodeName, "DYNAMIC", STRLEN ("DYNAMIC"));
                    break;
                case CAP_CODE_VENDOR_SPECIFIC_MIN:
                    MEMCPY (au1CodeName, "VENDOR_SPECIFIC_MIN",
                            STRLEN ("VENDOR_SPECIFIC_MIN"));
                    break;
                case CAP_CODE_VENDOR_SPECIFIC_MAX:
                    MEMCPY (au1CodeName, "VENDOR_SPECIFIC_MAX",
                            STRLEN ("VENDOR_SPECIFIC_MAX"));
                    break;
                default:
                    break;
            }
            break;
        case BGP4_PATH_ATTR_CODE_NAME:
            switch (u1CodeVal)
            {
                case BGP4_ATTR_ORIGIN:
                    MEMCPY (au1CodeName, "ORIGIN", STRLEN ("ORIGIN"));
                    break;
                case BGP4_ATTR_PATH:
                    MEMCPY (au1CodeName, "ASPATH", STRLEN ("ASPATH"));
                    break;
                case BGP4_ATTR_NEXT_HOP:
                    MEMCPY (au1CodeName, "NEXTHOP", STRLEN ("NEXTHOP"));
                    break;
                case BGP4_ATTR_MED:
                    MEMCPY (au1CodeName, "METRIC", STRLEN ("METRIC"));
                    break;
                case BGP4_ATTR_LOCAL_PREF:
                    MEMCPY (au1CodeName, "LOCAL_PREFERENCE",
                            STRLEN ("LOCAL_PREFERENCE"));
                    break;
                case BGP4_ATTR_ATOMIC_AGGR:
                    MEMCPY (au1CodeName, "ATOMIC_AGGREGATOR",
                            STRLEN ("ATOMIC_AGGREGATOR"));
                    break;
                case BGP4_ATTR_AGGREGATOR:
                    MEMCPY (au1CodeName, "AGGREGATOR", STRLEN ("AGGREGATOR"));
                    break;
                case BGP4_ATTR_COMM:
                    MEMCPY (au1CodeName, "COMMUNITY", STRLEN ("COMMUNITY"));
                    break;
                case BGP4_ATTR_ORIG_ID:
                    MEMCPY (au1CodeName, "ORIGINATORID",
                            STRLEN ("ORIGINATORID"));
                    break;
                case BGP4_ATTR_CLUS_LIST:
                    MEMCPY (au1CodeName, "CLUSTER_LIST",
                            STRLEN ("CLUSTER_LIST"));
                    break;
                case BGP4_ATTR_MP_REACH_NLRI:
                    MEMCPY (au1CodeName, "MP_REACH_NLRI",
                            STRLEN ("MP_REACH_NLRI"));
                    break;
                case BGP4_ATTR_MP_UNREACH_NLRI:
                    MEMCPY (au1CodeName, "MP_UNREACH_NLRI",
                            STRLEN ("MP_UNREACH_NLRI"));
                    break;
                case BGP4_ATTR_ECOMM:
                    MEMCPY (au1CodeName, "EXTENDED_COMMUNITY",
                            STRLEN ("EXTENDED_COMMUNITY"));
                    break;
                case BGP4_ATTR_PATH_FOUR:
                    MEMCPY (au1CodeName, "4_BYTE_ASPATH",
                            STRLEN ("4_BYTE_ASPATH"));
                    break;
                case BGP4_ATTR_AGGREGATOR_FOUR:
                    MEMCPY (au1CodeName, "4_BYTE_AGGREGATOR_ASPATH",
                            STRLEN ("4_BYTE_AGGREGATOR_ASPATH"));
                    break;
                default:
                    break;
            }
            break;
        case BGP4_OVERLAP_POLICY_CODE_NAME:
            switch (u1CodeVal)
            {
                case BGP4_INSTALL_LESSSPEC_RT:
                    MEMCPY (au1CodeName, "LESS_SPECIFIC",
                            STRLEN ("LESS_SPECIFIC"));
                    break;
                case BGP4_INSTALL_MORESPEC_RT:
                    MEMCPY (au1CodeName, "MORE_SPECIFIC",
                            STRLEN ("MORE_SPECIFIC"));
                    break;
                default:
                    break;
            }
            break;
        case BGP4_ORIGIN_CODE_NAME:
            switch (u1CodeVal)
            {
                case BGP4_ATTR_ORIGIN_IGP:
                    MEMCPY (au1CodeName, "i", STRLEN ("i"));
                    break;
                case BGP4_ATTR_ORIGIN_EGP:
                    MEMCPY (au1CodeName, "e", STRLEN ("e"));
                    break;
                case BGP4_ATTR_ORIGIN_INCOMPLETE:
                    MEMCPY (au1CodeName, "?", STRLEN ("?"));
                    break;
                default:
                    break;
            }
            break;
        case BGP4_RETURN_STATUS:
            switch (u1CodeVal)
            {
                case BGP4_SUCCESS:
                    MEMCPY (au1CodeName, "SUCCESS", STRLEN ("SUCCESS"));
                    break;
                default:
                    MEMCPY (au1CodeName, "FAILURE", STRLEN ("FAILURE"));
                    break;
            }
            break;
        default:
            break;
    }
    return (UINT1 *) &au1CodeName;
}

#ifdef BGP4_IPV6_WANTED
/*****************************************************************************/
/* Function Name : Bgp4MaskIpv6Address                                       */
/* Description   : For a given PREFIX and Mask, it applies the mask to prefix*/
/* Input(s)      : pAddr -   address prefix                                  */
/*                 pMask -   Mask                                            */
/* Output(s)     : pAddr - Masked prefix.                                    */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4MaskIpv6Address (UINT1 *pAddr, UINT1 *pMask)
{
    UINT4               u4Index;

    for (u4Index = 0; u4Index < BGP4_IPV6_PREFIX_LEN; u4Index++)
    {
        pAddr[u4Index] = pAddr[u4Index] & pMask[u4Index];
    }
    return BGP4_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name : Bgp4GetPeerNetworkAddress                                 */
/* Description   : This function gets the peer network address corresponding */
/*                 to the input address family.                              */
/* Input(s)      : pPeerEntry - pointer to the peer entry information        */
/*                 u2Afi - AFI info                                          */
/* Output(s)     : pPeerNetworkAddr - pointer to the peer network address    */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4GetPeerNetworkAddress (tBgp4PeerEntry * pPeerEntry, UINT2 u2Afi,
                           tAddrPrefix * pPeerNetworkAddr)
{
    tAddrPrefix        *pPeerAddr = NULL;

    pPeerAddr = &(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));

    if (pPeerAddr->u2Afi != u2Afi)
    {
        pPeerAddr = &(BGP4_PEER_NETWORK_ADDR_INFO (pPeerEntry));
    }

    if (u2Afi != pPeerAddr->u2Afi)
    {
        return BGP4_FAILURE;
    }
    pPeerNetworkAddr->u2Afi = u2Afi;
    MEMCPY (pPeerNetworkAddr, pPeerAddr, sizeof (tAddrPrefix));

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4GetPeerGatewayAddress                                 */
/* Description   : This function gets the peer gateway address corresponding */
/*                 to the input address family and peer entry.               */
/* Input(s)      : pPeerEntry - pointer to the peer entry information        */
/*                 u2Afi - AFI info                                          */
/* Output(s)     : pPeerNetworkAddr - pointer to the peer network address    */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4GetPeerGatewayAddress (tBgp4PeerEntry * pPeerEntry, UINT2 u2Afi,
                           tAddrPrefix * pPeerGatewayAddr)
{
    tAddrPrefix        *pPeerAddr = NULL;

    pPeerAddr = &(pPeerEntry->peerConfig.GatewayAddrInfo);

    if (pPeerAddr->u2Afi != u2Afi)
    {
        return BGP4_FAILURE;
    }
    pPeerGatewayAddr->u2Afi = u2Afi;
    MEMCPY (pPeerGatewayAddr, pPeerAddr, sizeof (tAddrPrefix));

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4GetPrefixForPrefixLen                                */
/* Description   : Masks the Prefix with PrefixLen                          */
/* Input(s)      : NetAddress.                                              */
/*                 PrefixLen                                                */
/* Output(s)     : None.                                                     */
/* Return(s)     : Prefix amsked with PrefixLen                              */
/*****************************************************************************/
INT4
Bgp4GetPrefixForPrefixLen (tNetAddress * pRoutePrefix, UINT2 u2PrefixLen)
{
    UINT4               u4AsafiMask;

    BGP4_GET_AFISAFI_MASK (pRoutePrefix->NetAddr.u2Afi, pRoutePrefix->u2Safi,
                           u4AsafiMask);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
        {
            UINT4               u4Mask;
            UINT4               u4Addr;

            u4Mask = Bgp4GetSubnetmask ((UINT1) u2PrefixLen);
            PTR_FETCH_4 (u4Addr, pRoutePrefix->NetAddr.au1Address);
            u4Addr = u4Addr & u4Mask;
            PTR_ASSIGN_4 (pRoutePrefix->NetAddr.au1Address, u4Addr);
            break;
        }
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
        {
            tAddrPrefix         Mask;
            Bgp4GetIPV6Subnetmask (u2PrefixLen, Mask.au1Address);
            Bgp4MaskIpv6Address (pRoutePrefix->NetAddr.au1Address,
                                 Mask.au1Address);
            break;
        }
#endif
#ifdef L3VPN
        case CAP_MP_VPN4_UNICAST:
        {
            tAddrPrefix         Mask;
            Bgp4GetVpn4Subnetmask (u2PrefixLen, Mask.au1Address);
            Bgp4MaskVpn4Address (pRoutePrefix->NetAddr.au1Address,
                                 Mask.au1Address);
            break;
        }
#endif
        default:
            return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4GetBytesForPrefix                                     */
/* Description   : Gets the total number of bytes required to fill prefix    */
/*                 alone in the update message.                              */
/* Input(s)      : Route Profile information.                                */
/*                 Peer Information                                          */
/* Output(s)     : None.                                                     */
/* Return(s)     : Bytes required to fill the prefix alone.                  */
/*****************************************************************************/
UINT4
Bgp4GetBytesForPrefix (tRouteProfile * pRtProfile, tBgp4PeerEntry * pPeerInfo)
{
    UINT4               u4AsafiMask = 0;
    UINT4               u4PrefBytes = 0;

    Bgp4GetPeerAdvtAfiSafi (pRtProfile, pPeerInfo, &u4AsafiMask);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
            /* route is of ipv4-unicast. we'll send in normal NLRI */
            u4PrefBytes = Bgp4RoundToBytes (BGP4_RT_PREFIXLEN (pRtProfile));
            break;
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
            /* route is of ipv6-unicast. */
            u4PrefBytes = Bgp4RoundToBytes (BGP4_RT_PREFIXLEN (pRtProfile));
            break;
#endif
#ifdef L3VPN
            /* Carrying Label Information - RFC 3107 */
        case CAP_MP_LABELLED_IPV4:
            /* Label, IPv4 route */
            u4PrefBytes += BGP4_ONE_BYTE;    /* for length in bits */
            u4PrefBytes += BGP4_VPN4_LABEL_SIZE;
            u4PrefBytes += Bgp4RoundToBytes (BGP4_RT_PREFIXLEN (pRtProfile));
            break;

        case CAP_MP_VPN4_UNICAST:
            /* Label, VPNv4 route */
            u4PrefBytes += BGP4_ONE_BYTE;    /* for length in bits */
            u4PrefBytes += BGP4_VPN4_LABEL_SIZE;
            u4PrefBytes += BGP4_VPN4_ROUTE_DISTING_SIZE;
            u4PrefBytes += Bgp4RoundToBytes (BGP4_RT_PREFIXLEN (pRtProfile));
            break;
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
            u4PrefBytes = BGP4_RT_PREFIXLEN (pRtProfile);    /* This should be 17 bytes as perVPLS NLRI */
            break;
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
            u4PrefBytes = BGP4_EVPN_MAC_NLRI_PREFIX_LEN;
            break;
#endif

        default:
            return 0;
    }

    return u4PrefBytes;
}

/*****************************************************************************/
/* Function Name : Bgp4GetPeerNegAfiSafi                                    */
/* Description   : Gets the Afi and Safi to be advertised to the peer        */
/* Input(s)      : Route Profile information.                                */
/*                 Peer Information                                          */
/* Output(s)     :  Afi and Safi                                             */
/* Return(s)     : BGP4_SUCCESS                           .                  */
/*****************************************************************************/
INT4
Bgp4GetNegAfiSafi (UINT4 u4AsafiMask, UINT4 *pu4NegAsafiMask)
{

    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
            *pu4NegAsafiMask = CAP_NEG_IPV4_UNI_MASK;
            break;
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
            *pu4NegAsafiMask = CAP_NEG_IPV6_UNI_MASK;
            break;
#endif
#ifdef L3VPN
            /* Carrying Label Information - RFC 3107 */
        case CAP_MP_LABELLED_IPV4:
            *pu4NegAsafiMask = CAP_NEG_LBL_IPV4_MASK;
            break;

        case CAP_MP_VPN4_UNICAST:
            *pu4NegAsafiMask = CAP_NEG_VPN4_UNI_MASK;
            break;
#endif
        default:
            *pu4NegAsafiMask = 0;
            return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4GetPeerAdvtAfiSafi                                    */
/* Description   : Gets the Afi and Safi to be advertised to the peer        */
/* Input(s)      : Route Profile information.                                */
/*                 Peer Information                                          */
/* Output(s)     :  Afi and Safi                                             */
/* Return(s)     : BGP4_SUCCESS                           .                  */
/*****************************************************************************/
INT4
Bgp4GetPeerAdvtAfiSafi (tRouteProfile * pRtProfile,
                        tBgp4PeerEntry * pPeerInfo, UINT4 *pu4AfiSafi)
{
    UINT4               u4AsafiMask;

#ifndef L3VPN
    UNUSED_PARAM (pPeerInfo);
#endif
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) &
                 CAP_NEG_LBL_IPV4_MASK) == CAP_NEG_LBL_IPV4_MASK)
            {
                *pu4AfiSafi = CAP_MP_LABELLED_IPV4;
            }
            else
#endif
            {
                *pu4AfiSafi = u4AsafiMask;
            }
            break;
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
            *pu4AfiSafi = u4AsafiMask;
            break;
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
            *pu4AfiSafi = u4AsafiMask;
            break;
#endif
#ifdef VPLSADS_WANTED
            /*ADS-VPLS Related Processing */
        case CAP_MP_L2VPN_VPLS:
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) &
                 CAP_NEG_L2VPN_VPLS_MASK) == CAP_NEG_L2VPN_VPLS_MASK)
            {
                *pu4AfiSafi = u4AsafiMask;
            }

            break;

#endif

#ifdef L3VPN
            /* Carrying Label Information - RFC 3107 */
        case CAP_MP_LABELLED_IPV4:
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) &
                 CAP_NEG_LBL_IPV4_MASK) == CAP_NEG_LBL_IPV4_MASK)
            {
                *pu4AfiSafi = u4AsafiMask;
            }
            else
            {
                *pu4AfiSafi = CAP_MP_IPV4_UNICAST;
            }
            break;

        case CAP_MP_VPN4_UNICAST:
            /* route is of vpn4-unicast. check for the labels also */
            if (BGP4_VPN4_PEER_ROLE (pPeerInfo) == BGP4_VPN4_CE_PEER)
            {
                if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) &
                     CAP_NEG_LBL_IPV4_MASK) == CAP_NEG_LBL_IPV4_MASK)
                {
                    *pu4AfiSafi = CAP_MP_LABELLED_IPV4;
                }
                else
                {
                    *pu4AfiSafi = CAP_MP_IPV4_UNICAST;
                }
            }
            else if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) &
                      CAP_NEG_VPN4_UNI_MASK) == CAP_NEG_VPN4_UNI_MASK)
            {
                *pu4AfiSafi = u4AsafiMask;
            }
            break;
#endif
        default:
            break;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4DuplicateRouteProfile                                 */
/* Description   : Duplicates the given route profile information and returns*/
/*                 the new duplicated route profile pointer.                 */
/* Input(s)      : Route Profile information.                                */
/* Output(s)     : None.                                                     */
/* Return(s)     : Duplicated route profile pointer.      .                  */
/*****************************************************************************/
tRouteProfile      *
Bgp4DuplicateRouteProfile (tRouteProfile * pRtProfile)
{
    tRouteProfile      *pAddRtProfile = NULL;
#ifdef L3VPN
    tRtInstallVrfInfo  *pRtAddVrfInfo = NULL;
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
#endif

    pAddRtProfile = Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
    if (pAddRtProfile == NULL)
    {
        /* Allocation FAILS. We are not able to handle the
         * status change for this route.
         */
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tPEER : Allocate Route Profile FAILS!!! \n");
        gu4BgpDebugCnt[MAX_BGP_ROUTES_SIZING_ID]++;
        return NULL;
    }
#ifdef L3VPN
    /* copy installed vrf information */
    TMO_SLL_Init (BGP4_VPN4_RT_INSTALL_VRF_LIST (pAddRtProfile));
    TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile),
                  pRtVrfInfo, tRtInstallVrfInfo *)
    {
        pRtAddVrfInfo =
            Bgp4MemAllocateVpnRtInstallVrfNode (sizeof (tRtInstallVrfInfo));
        if (pRtAddVrfInfo == NULL)
        {
            Bgp4DshReleaseRtInfo (pAddRtProfile);
            return NULL;
        }
        BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtAddVrfInfo)
            = BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrfInfo);
        BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtAddVrfInfo)
            = BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo);
        TMO_SLL_Add (BGP4_VPN4_RT_INSTALL_VRF_LIST (pAddRtProfile),
                     (tTMO_SLL_NODE *) pRtAddVrfInfo);
    }
    if (BGP4_RT_LABEL_CNT (pRtProfile))
    {
        BGP4_RT_LABEL_CNT (pAddRtProfile) = BGP4_RT_LABEL_CNT (pRtProfile);
        BGP4_RT_LABEL_INFO (pAddRtProfile) = BGP4_RT_LABEL_INFO (pRtProfile);
    }
    MEMCPY (BGP4_RT_ROUTE_DISTING (pAddRtProfile),
            BGP4_RT_ROUTE_DISTING (pRtProfile), BGP4_VPN4_ROUTE_DISTING_SIZE);
#endif
    /* copy the flags */
    BGP4_RT_GET_FLAGS (pAddRtProfile) =
        BGP4_RT_GET_FLAGS (pRtProfile) & (BGP4_RT_SYNC_FLAG);
    /* Update the new route profile with the existing one. */
    BGP4_LINK_INFO_TO_PROFILE (BGP4_RT_BGP_INFO (pRtProfile), pAddRtProfile);
    BGP4_RT_PEER_ENTRY (pAddRtProfile) = BGP4_RT_PEER_ENTRY (pRtProfile);
    BGP4_RT_PROTOCOL (pAddRtProfile) = BGP4_RT_PROTOCOL (pRtProfile);
    Bgp4CopyNetAddressStruct (&(BGP4_RT_NET_ADDRESS_INFO (pAddRtProfile)),
                              (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)));
    /* Update the Local_Pref and MED */
    BGP4_RT_LOCAL_PREF (pAddRtProfile) = BGP4_RT_LOCAL_PREF (pRtProfile);
    BGP4_RT_MED (pAddRtProfile) = BGP4_RT_MED (pRtProfile);
    BGP4_RT_NEW_MED (pAddRtProfile) = BGP4_RT_NEW_MED (pRtProfile);
    pAddRtProfile->pBgpCxtNode = pRtProfile->pBgpCxtNode;
    return pAddRtProfile;
}

/*******************************************************
*
* Function Name : BgpSetContext
*
* Input         : u4FsMIContextId
*
* Output        : NONE
*
*********************************************************/

INT4
BgpSetContext (INT4 u4SetValFsMIContextId)
{
    INT4                i4MaxCxtValue;

    i4MaxCxtValue =
        FsBGPSizingParams[MAX_BGP_CXT_NODES_SIZING_ID].u4PreAllocatedUnits;

    if (u4SetValFsMIContextId >= i4MaxCxtValue)
    {
        gpBgpCurrCxtNode = NULL;
        return SNMP_FAILURE;
    }
    if (gBgpCxtNode == NULL)
    {
        gpBgpCurrCxtNode = NULL;
        return SNMP_FAILURE;
    }
    if (NULL == gBgpCxtNode[u4SetValFsMIContextId])
    {
        gpBgpCurrCxtNode = NULL;
        return SNMP_FAILURE;
    }

    gpBgpCurrCxtNode = gBgpCxtNode[u4SetValFsMIContextId];
    BgpSetContextId (u4SetValFsMIContextId);
    return SNMP_SUCCESS;
}

/**********************************************************
* Function Name : BgpReleaseContext
*
* Input         : u4FsMIContextId
*
* Output        : NONE
*
***********************************************************/

VOID
BgpReleaseContext (VOID)
{
    gpBgpCurrCxtNode = NULL;
}

/**********************************************************
* Function Name : BgpResetContext
*
* Description   : Resets the Global Context pointer in web pages
*
* Input         : NONE
*
* Output        : NONE
*
* Returns       : SNMP_SUCCESS
***********************************************************/

INT4
BgpResetContext (VOID)
{
    BgpReleaseContext ();
    return SNMP_SUCCESS;
}

/*****************************************************************************
 * FUNCTION    :  Gets the first active context ID
 *
 * INPUTS      : None
 *
 * OUTPUTS     : pu4RetValContextID - Context ID
 *
 * RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
 *
 ******************************************************************************/

INT4
BgpGetFirstActiveContextID (UINT4 *pu4RetValContextID)
{
    UINT4               i4TmpContext = 0;

    UINT4               u4MaxCxtValue;

    u4MaxCxtValue =
        FsBGPSizingParams[MAX_BGP_CXT_NODES_SIZING_ID].u4PreAllocatedUnits;

    if (gBgpCxtNode == NULL)
    {
        return SNMP_FAILURE;
    }

    for (i4TmpContext = 0; i4TmpContext < u4MaxCxtValue; i4TmpContext++)
    {
        if ((NULL != gBgpCxtNode[i4TmpContext])
            && (BGP4_STATUS (i4TmpContext) == BGP4_TRUE))
        {
            *pu4RetValContextID = i4TmpContext;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/******************************************************************************
 * FUNCTION : Gets the next active context ID
 *
 * INPUTS      : u4ContextID - Context ID
 *
 * OUTPUTS     : pu4NextContextID - Next active Context ID
 *
 * RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
 *
 ******************************************************************************/

INT4
BgpGetNextActiveContextID (UINT4 u4ContextID, UINT4 *pu4NextContextID)
{
    UINT4               i4TmpContext = 0;

    UINT4               u4MaxCxtValue;

    u4MaxCxtValue =
        FsBGPSizingParams[MAX_BGP_CXT_NODES_SIZING_ID].u4PreAllocatedUnits;

    if (gBgpCxtNode == NULL)
    {
        return SNMP_FAILURE;
    }

    for (i4TmpContext = u4ContextID + 1; i4TmpContext < u4MaxCxtValue;
         i4TmpContext++)
    {
        if ((NULL != gBgpCxtNode[i4TmpContext])
            && (BGP4_STATUS (i4TmpContext) == BGP4_TRUE))
        {
            *pu4NextContextID = i4TmpContext;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/***************************************************************************/
/*  Function Name  : Bgp4UtilClearPeerGroup                                */
/*  Description    : This function clears the peers in the given peer group*/
/*  Input(s)       : pu1PeerGroup     -  Peer Group name                   */
/*                   u1PolicyDir      -  Clear policy direction            */
/*                   u1SoftFlag       -  Soft clear flag                   */
/*                   u1OrfRqst        -  Orf request flag                  */
/*  Output(s)      : None.                                                 */
/*  Return Values  : BGP4_CLEAR_PEER_FAILURE when the PeerGroup is invalid */
/*                   BGP4_CLEAR_SET_FAILURE  when the there is any failure */
/*                   in memmory allocation or any nmhset fails             */
/*                   BGP4_SUCCESS/BGP4_FAILURE.                            */
/***************************************************************************/
INT4
Bgp4UtilClearPeerGroup (UINT1 *pu1PeerGrp, UINT1 u1SoftFlag, UINT1 u1PolicyDir,
                        UINT1 u1OrfRqst)
{
    tBgp4QMsg          *pQMsg = NULL;
    tBgpPeerGroupEntry *pPeerGroupEntry = NULL;
    UINT4               u4Context = gpBgpCurrCxtNode->u4ContextId;

    if ((pPeerGroupEntry =
         Bgp4GetPeerGroupEntry (u4Context, pu1PeerGrp)) == NULL)
    {
        return BGP4_CLEAR_PEER_FAILURE;
    }
    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        return BGP4_CLEAR_SET_FAILURE;
    }

    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;

    MEMSET (BGP4_INPUTQ_PEER_GROUP_NAME (pQMsg), 0, BGP_MAX_PEER_GROUP_NAME);

    if (u1SoftFlag != BGP4_CLEAR_SOFT)
    {
        BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_PEER_GROUP_CLEAR_EVENT;
        MEMCPY (BGP4_INPUTQ_PEER_GROUP_NAME (pQMsg), pu1PeerGrp,
                BGP_MAX_PEER_GROUP_NAME);
        BGP4_INPUTQ_CXT (pQMsg) = u4Context;
        /* Send the peer entry to the bgp-task */
        if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
        {
            return BGP4_FAILURE;
        }
    }
    else
    {
        if (u1PolicyDir == BGP4_CLEAR_SOFT_IN)
        {
            if (u1OrfRqst == BGP4_TRUE)
            {
                pPeerGroupEntry->u1OrfRqst = BGP4_ORF_REQ_SET;
                BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_PEER_GROUP_ORF_EVENT;
            }
            else
            {
                BGP4_INPUTQ_MSGTYPE (pQMsg) =
                    BGP4_PEER_GROUP_SOFT_CLEAR_IN_EVENT;
            }
            MEMCPY (BGP4_INPUTQ_PEER_GROUP_NAME (pQMsg), pu1PeerGrp,
                    BGP_MAX_PEER_GROUP_NAME);
            BGP4_INPUTQ_CXT (pQMsg) = u4Context;
            /* Send the peer entry to the bgp-task */
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                return BGP4_FAILURE;
            }
        }
        else if (u1PolicyDir == BGP4_CLEAR_SOFT_OUT)
        {
            BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_PEER_GROUP_SOFT_CLEAR_OUT_EVENT;
            MEMCPY (BGP4_INPUTQ_PEER_GROUP_NAME (pQMsg), pu1PeerGrp,
                    BGP_MAX_PEER_GROUP_NAME);
            BGP4_INPUTQ_CXT (pQMsg) = u4Context;
            /* Send the peer entry to the bgp-task */
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                return BGP4_FAILURE;
            }
        }
        else if (u1PolicyDir == BGP4_CLEAR_SOFT_BOTH)
        {
            BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_PEER_GROUP_SOFT_CLEAR_BOTH_EVENT;
            MEMCPY (BGP4_INPUTQ_PEER_GROUP_NAME (pQMsg), pu1PeerGrp,
                    BGP_MAX_PEER_GROUP_NAME);
            BGP4_INPUTQ_CXT (pQMsg) = u4Context;
            /* Send the peer entry to the bgp-task */
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                return BGP4_FAILURE;
            }
        }
        else
        {
            BGP4_QMSG_FREE (pQMsg);
        }
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4UtilNeighborClearIpBgp                                */
/* CLI Command   : clear ip bgp {*|ip-address}                               */
/*                             [soft[in|out]]                                */
/* Description   : This function is used to reset the peer session and bring */
/*                 it back dynamically. This function needs to be called     */
/*                 when (clear ip bgp ip-address                             */
/* Input(s)      : pu4IpAddress - Pointer to the BGP peer's IP Address for   */
/*                 which seession needs to be reset.                         */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLEAR_PEER_FAILURE -                                 */
/*                      if Peer is not valid                                 */
/*               : BGP4_CLEAR_FAILURE                                        */
/*                      if there is any failure while restarting the peers   */
/*               : BGP4_FAILURE -  if operation failed due to any            */
/*                               other reason than mentioned above           */
/*****************************************************************************/
INT4
Bgp4UtilNeighborClearIpBgp (tNetAddress * pIpAddress)
{
    UINT1               au1Address[BGP4_MAX_INET_ADDRESS_LEN];
    UINT4               u4Context = gpBgpCurrCxtNode->u4ContextId;

    MEMSET (au1Address, 0, BGP4_MAX_INET_ADDRESS_LEN);

    if (Bgp4SnmphGetPeerEntry (u4Context, pIpAddress->NetAddr) == NULL)
    {
        return BGP4_CLEAR_PEER_FAILURE;
    }
    if (Bgp4PeerHandleClearIpBgp (u4Context, pIpAddress->NetAddr) !=
        BGP4_SUCCESS)
    {
        return BGP4_CLEAR_FAILURE;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************
 * Function Name : Bgp4UtilClearRouteDampening                               *
 * CLI Command   : clear ip bgp dampening [ Prefix PrefixLen]                *
 * Description   : clear the Dampened route                                  *
 * Input(s)      : RtNetAddr - RtNetAddr                                     *
 *                 u1RtFlag - clear for only the RtNetAddr Route             *
 * Output(s)     : None.                                                     *
 * Return(s)     : BGP4_CLEAR_RIB_FAILURE - if there is failure in rib       *
 *                 BGP4_CLEAR_RFD_DIS_FAILURE - if RFD is diabled            *
 *                 BGP4_SUCCESS    - if operation is success                 *
 *                 BGP4_FAILURE    - operation failes                        *
*****************************************************************************/
INT4
Bgp4UtilClearRouteDampening (tNetAddress RtNetAddr, UINT1 u1RtFlag)
{
#ifdef RFD_WANTED
    tRouteProfile       Route;
    tRouteProfile      *pFndRtProfileList = NULL;
    VOID               *pRibNode = NULL;
    tRouteProfile      *pRoute = NULL;
    tRouteProfile      *pTmpRoute = NULL;
    INT4                i4Found = BGP4_FAILURE;
    INT4                i4Retval = RFD_FAILURE;
    tRtDampHist        *pRtDampHist = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tBgp4PeerEntry     *pPeerEntry;
    UINT4               u4Context = gpBgpCurrCxtNode->u4ContextId;
    if (u1RtFlag == 0)
    {
        i4Retval = RfdDisable (u4Context);
        if (i4Retval == RFD_FAILURE)
        {
            return BGP4_CLEAR_ADMIN_FAIL;
        }
        i4Retval = RfdEnable (u4Context);
        if (i4Retval == RFD_FAILURE)
        {
            return BGP4_CLEAR_ADMIN_FAIL;
        }
        return BGP4_SUCCESS;
    }
    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return BGP4_CLEAR_RIB_LOCK_FAILURE;
    }
    MEMSET (&Route, 0, sizeof (tRouteProfile));
    Bgp4CopyNetAddressStruct (&(Route.NetAddress), RtNetAddr);
    i4Found =
        Bgp4RibhLookupRtEntry (&Route, u4Context, BGP4_TREE_FIND_EXACT,
                               &pFndRtProfileList, &pRibNode);
    if (i4Found == BGP4_FAILURE)
    {
        /* Unable to find the current route in the RIB. */
        bgp4RibUnlock ();
        return BGP4_CLEAR_RIB_FAILURE;
    }
    pRoute = pFndRtProfileList;

    while (pRoute != NULL)
    {
        if ((NetAddrMatch (RtNetAddr, pRoute->NetAddress) == BGP4_TRUE) &&
            (pRoute->pRtDampHist != NULL))
        {
            Bgp4TmrhStopTimer (BGP4_ROUTE_REUSE_TIMER,
                               (VOID *) pRoute->pRtDampHist);
            pRtDampHist = pRoute->pRtDampHist;
            if (pRtDampHist != NULL)
            {
                pRtDampHist->pReusePrev = NULL;
                pRtDampHist->pReuseNext = NULL;
                pRtDampHist->pRtProfile->pRtDampHist = NULL;
                if ((BGP4_RT_REF_COUNT (pRtDampHist->pRtProfile) > 1)
                    && (pRtDampHist->u1RtFlag == RFD_FEASIBLE_ROUTE))
                {
                    pRtProfile = pRtDampHist->pRtProfile;
                    pPeerEntry = BGP4_RT_PEER_ENTRY (pRtProfile);

                    /* Route needs to be advertised. Add the route to
                     * the peer's received route list. Alse Delete the
                     * existing route from the RIB. By Pass RFD for this
                     * process. */
                    Bgp4DshDelinkRouteFromChgList (pRtProfile);
                    Bgp4DshAddRouteToList (BGP4_PEER_RCVD_ROUTES (pPeerEntry),
                                           pRtProfile, 0);

                    BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_NO_RFD);
                    if (BGP4_RT_NEXT (pRoute) != NULL)
                    {
                        pTmpRoute = BGP4_RT_NEXT (pRoute);
                    }
                    Bgp4RibhDelRtEntry (pRtDampHist->pRtProfile, NULL,
                                        u4Context, BGP4_TRUE);
                    /* Set NO_RFD flag to by-pass RFD when the route is
                     * processed as feasible route. */
                    BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_NO_RFD);
                    BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_DAMPED);
                }
                else
                {
                    Bgp4DshReleaseRtInfo (pRtDampHist->pRtProfile);
                    if (pRtDampHist->u1RtFlag != RFD_FEASIBLE_ROUTE)
                    {
                        BGP4_RT_RESET_FLAG (pRtDampHist->pRtProfile,
                                            BGP4_RT_HISTORY);
                        Bgp4RibhDelRtEntry (pRtDampHist->pRtProfile, NULL,
                                            u4Context, BGP4_HISTORY);
                    }
                }
                Bgp4MemReleaseRfdDampHist (pRtDampHist);
            }
        }
        if (pTmpRoute != NULL)
        {
            pRoute = pTmpRoute;
            pTmpRoute = NULL;
        }
        else
        {
            pRoute = BGP4_RT_NEXT (pRoute);
        }
    }
    bgp4RibUnlock ();

    return BGP4_SUCCESS;
#else
    UNUSED_PARAM (RtNetAddr);
    UNUSED_PARAM (u1RtFlag);
    return BGP4_CLEAR_RFD_DIS_FAILURE;
#endif
}

/*****************************************************************************
* Function Name : Bgp4UtilClearRouteFlapStats                                *
* CLI Command   : clear ip bgp flap-stat [ Prefix PrefixLen]                 *
* Description   : clear the route flapstatistics                             *
* Input(s)      : RtNetAddr - RtNetAddr                                      *
*                 u1RtFlag - clear for only the RtNetAddr Rout  e            *
* Output(s)     : None.                                                      *
* Return(s)     : BGP4_CLEAR_RIB_FAILURE - if there is failure in rib        *
*                 BGP4_CLEAR_RFD_DIS_FAILURE - if RFD is diabled             *
*                 BGP4_SUCCESS    - if operation is success                  *
*                 BGP4_FAILURE    - operation failes                         *
*****************************************************************************/
INT4
Bgp4UtilClearRouteFlapStats (tNetAddress RtNetAddr, UINT1 u1RtFlag)
{
#ifdef RFD_WANTED
    tRouteProfile       Route;
    tRouteProfile      *pFndRtProfileList = NULL;
    VOID               *pRibNode = NULL;
    tRouteProfile      *pRoute = NULL;
    INT4                i4Found = BGP4_FAILURE;
    UINT4               u4Context = gpBgpCurrCxtNode->u4ContextId;
    UINT4               u4CurrTime = 0;
    INT4                i4Status = 0;

    if (u1RtFlag == 0)
    {                            /* clear all the flapped routes.  */
        if (bgp4RibLock () != OSIX_SUCCESS)
        {
            return BGP4_CLEAR_RIB_LOCK_FAILURE;
        }

        Bgp4GetRibNode (u4Context, CAP_MP_IPV4_UNICAST, &pRibNode);
        if (pRibNode != NULL)
        {

            i4Status = Bgp4RibhGetFirstEntry (CAP_MP_IPV4_UNICAST,
                                              &pFndRtProfileList, &pRibNode,
                                              TRUE);
            if (i4Status == BGP4_SUCCESS)
            {
                for (;;)
                {
                    pRoute = pFndRtProfileList;
                    u4Context = BGP4_RT_CXT_ID (pRoute);
                    while (pRoute != NULL)
                    {
                        if (pRoute->pRtDampHist != NULL)
                        {
                            pRoute->pRtDampHist->u4RtFlapCount = 0;
                            RFD_GET_SYS_TIME (&u4CurrTime);
                            u4CurrTime =
                                (u4CurrTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
                            pRoute->pRtDampHist->u4RtFlapTime = u4CurrTime;
                        }
                        pRoute = BGP4_RT_NEXT (pRoute);
                    }
                    i4Status = Bgp4RibhGetNextEntry (pFndRtProfileList,
                                                     u4Context,
                                                     &pFndRtProfileList,
                                                     &pRibNode, TRUE);
                    if (i4Status == BGP4_FAILURE)
                    {
                        break;
                    }
                }
            }
        }
#ifdef BGP4_IPV6_WANTED
        Bgp4GetRibNode (u4Context, CAP_MP_IPV6_UNICAST, &pRibNode);
        if (pRibNode != NULL)
        {

            i4Status = Bgp4RibhGetFirstEntry (CAP_MP_IPV6_UNICAST,
                                              &pFndRtProfileList, &pRibNode,
                                              TRUE);
            if (i4Status == BGP4_SUCCESS)
            {
                for (;;)
                {
                    pRoute = pFndRtProfileList;
                    u4Context = BGP4_RT_CXT_ID (pRoute);
                    while (pRoute != NULL)
                    {
                        if (pRoute->pRtDampHist != NULL)
                        {
                            pRoute->pRtDampHist->u4RtFlapCount = 0;
                            RFD_GET_SYS_TIME (&u4CurrTime);
                            u4CurrTime =
                                (u4CurrTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
                            pRoute->pRtDampHist->u4RtFlapTime = u4CurrTime;
                        }
                        pRoute = BGP4_RT_NEXT (pRoute);
                    }
                    i4Status = Bgp4RibhGetNextEntry (pFndRtProfileList,
                                                     u4Context,
                                                     &pFndRtProfileList,
                                                     &pRibNode, TRUE);
                    if (i4Status == BGP4_FAILURE)
                    {
                        break;
                    }
                }
            }
        }
#endif
        bgp4RibUnlock ();
        return BGP4_SUCCESS;
    }

    /* Clear the Perticular route  */
    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return BGP4_CLEAR_RIB_LOCK_FAILURE;
    }
    MEMSET (&Route, 0, sizeof (tRouteProfile));
    Bgp4CopyNetAddressStruct (&(Route.NetAddress), RtNetAddr);
    i4Found = Bgp4RibhLookupRtEntry (&Route, u4Context,
                                     BGP4_TREE_FIND_EXACT,
                                     &pFndRtProfileList, &pRibNode);
    if (i4Found == BGP4_FAILURE)
    {
        /* Unable to find the current route in the RIB. */
        bgp4RibUnlock ();
        return BGP4_CLEAR_RIB_FAILURE;
    }
    pRoute = pFndRtProfileList;
    while (pRoute != NULL)
    {
        /* Should clear this flap route for all the peers */
        if ((NetAddrMatch (RtNetAddr, pRoute->NetAddress) == BGP4_TRUE) &&
            (pRoute->pRtDampHist != NULL))
        {
            pRoute->pRtDampHist->u4RtFlapCount = 0;
            RFD_GET_SYS_TIME (&u4CurrTime);
            u4CurrTime = (u4CurrTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
            pRoute->pRtDampHist->u4RtFlapTime = u4CurrTime;
        }
        pRoute = BGP4_RT_NEXT (pRoute);
    }
    bgp4RibUnlock ();
    return BGP4_SUCCESS;
#else
    UNUSED_PARAM (RtNetAddr);
    UNUSED_PARAM (u1RtFlag);
    return BGP4_CLEAR_RFD_DIS_FAILURE;
#endif
}

/*****************************************************************************/
/* Function Name : Bgp4UtilNeighborClearIpBgpSoftAll                         */
/* CLI Command   : clear ip bgp                                              */
/* Description   : Soft reconfig the Ipv4/Ipv6 peers based on u1afi          */
/* Input(s)      : u1ASCheck - flag to check AS number                       */
/*                 u4InputAS - AS Number of the peers to soft clear          */
/*                 u1afi     - Address family of the peers IPv4/Ipv6         */
/*                 u1Dir     - Soft in/Soft Out                              */
/*                 u1OrfFlag - ORf request flag                              */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*                 BGP4_FAILURE    - operation failes                        */
/*****************************************************************************/
INT4
Bgp4UtilNeighborClearIpBgpSoftAll (UINT1 u1ASCheck, UINT4 u4InputAS,
                                   UINT1 u1afi, UINT1 u1Dir, UINT1 u1OrfFlag)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         CurPeerAddr;
    tAddrPrefix         NextPeerAddr;
    tNetAddress         IpAddr;
    INT4                i4Sts = BGP4_FAILURE;
    UINT4               u4LocalAS = 0;
    UINT4               u4PeerAddress = 0;
    UINT4               u4Count = 0;
    UINT4               u4Context = gpBgpCurrCxtNode->u4ContextId;

    /* Need to reset all the active peers in the Autonomous System. 
     * Dont use the for loop
     * with BGP4_PEERENTRY_HEAD, because some peers may have duplicate
     * entries in the Peer list. Such peers need to be processed only
     * once. */
    switch (u1afi)
    {
        case BGP4_INET_AFI_IPV4:
            i4Sts = Bgp4Ipv4GetFirstIndexBgpPeerTable (u4Context, &CurPeerAddr);
            break;

#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
            i4Sts = Bgp4Ipv6GetFirstIndexBgpPeerTable (u4Context, &CurPeerAddr);
            break;
#endif
        default:
            return BGP4_FAILURE;
    }
    if (i4Sts == BGP4_FAILURE)
    {
        /* No peer exists for this Address Family. */
        return BGP4_FAILURE;
    }

    BGP4_MPATH_OPER_EBGP_COUNT (u4Context) = BGP4_MPATH_EBGP_COUNT (u4Context);
    BGP4_MPATH_OPER_IBGP_COUNT (u4Context) = BGP4_MPATH_IBGP_COUNT (u4Context);
    BGP4_MPATH_OPER_EIBGP_COUNT (u4Context) =
        BGP4_MPATH_EIBGP_COUNT (u4Context);

    Bgp4RedSyncMpathOperCnt (u4Context,
                             BGP4_MPATH_OPER_IBGP_COUNT (u4Context),
                             BGP4_MPATH_OPER_EBGP_COUNT (u4Context),
                             BGP4_MPATH_OPER_EIBGP_COUNT (u4Context));

    nmhGetBgpLocalAs ((INT4 *) &u4LocalAS);
    do
    {
        pPeer = Bgp4SnmphGetPeerEntry (u4Context, CurPeerAddr);
        if (pPeer == NULL)
        {
            break;
        }
        if ((u1ASCheck == BGP4_AS_CHECK))
        {
            if (u4InputAS == BGP4_PEER_ASNO (pPeer))
            {
                /* clear ip bgp AS-NUM soft  */
                Bgp4CopyAddrPrefixStruct (&(IpAddr.NetAddr), CurPeerAddr);
                if (u1afi == BGP4_INET_FAMILY_IPV4)
                {
                    PTR_FETCH4 (u4PeerAddress, CurPeerAddr.au1Address);
                    PTR_ASSIGN_4 ((IpAddr.NetAddr.au1Address), u4PeerAddress);
                }
                IpAddr.u2Safi = BGP4_INET_SAFI_UNICAST;

#ifdef L3VPN
                if (BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_PE_PEER)
                {
                    IpAddr.u2Safi = BGP4_INET_SAFI_VPNV4_UNICAST;
                }
#endif

                i4Sts = Bgp4UtilNeighborSoftReconfigClearIpBgp (u1Dir,
                                                                &IpAddr, u1afi,
                                                                u1OrfFlag);
                if (i4Sts != BGP4_SUCCESS)
                {
                    return i4Sts;
                }
                u4Count++;
            }
            if (u4InputAS == 0 && u4LocalAS != BGP4_PEER_ASNO (pPeer))
            {
                /* clear ip bgp external soft - Extenal peers to be clear */
                Bgp4CopyAddrPrefixStruct (&(IpAddr.NetAddr), CurPeerAddr);
                if (u1afi == BGP4_INET_FAMILY_IPV4)
                {
                    PTR_FETCH4 (u4PeerAddress, CurPeerAddr.au1Address);
                    PTR_ASSIGN_4 ((IpAddr.NetAddr.au1Address), u4PeerAddress);
                }
                IpAddr.u2Safi = BGP4_INET_SAFI_UNICAST;
#ifdef L3VPN
                if (BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_PE_PEER)
                {
                    IpAddr.u2Safi = BGP4_INET_SAFI_VPNV4_UNICAST;
                }
#endif
                i4Sts =
                    Bgp4UtilNeighborSoftReconfigClearIpBgp (u1Dir, &IpAddr,
                                                            u1afi, u1OrfFlag);
                if (i4Sts != BGP4_SUCCESS)
                {
                    return i4Sts;
                }
                u4Count++;
            }
        }
        else
        {
            Bgp4CopyAddrPrefixStruct (&(IpAddr.NetAddr), CurPeerAddr);
            if (u1afi == BGP4_INET_FAMILY_IPV4)
            {
                PTR_FETCH4 (u4PeerAddress, CurPeerAddr.au1Address);
                PTR_ASSIGN_4 ((IpAddr.NetAddr.au1Address), u4PeerAddress);
            }
            IpAddr.u2Safi = BGP4_INET_SAFI_UNICAST;
#ifdef L3VPN
            if (BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_PE_PEER)
            {
                IpAddr.u2Safi = BGP4_INET_SAFI_VPNV4_UNICAST;
            }
#endif
            i4Sts =
                Bgp4UtilNeighborSoftReconfigClearIpBgp (u1Dir, &IpAddr, u1afi,
                                                        u1OrfFlag);
            if (i4Sts != BGP4_SUCCESS)
            {
                return i4Sts;
            }
            u4Count++;
        }
        i4Sts =
            Bgp4GetNextIndexBgpPeerTable (u4Context, CurPeerAddr,
                                          &NextPeerAddr);
        if (i4Sts == BGP4_FAILURE)
        {
            break;
        }
        if (BGP4_AFI_IN_ADDR_PREFIX_INFO (CurPeerAddr) !=
            BGP4_AFI_IN_ADDR_PREFIX_INFO (NextPeerAddr))
        {
            break;
        }
        Bgp4CopyAddrPrefixStruct (&CurPeerAddr, NextPeerAddr);
    }
    while (pPeer != NULL);
    if (u4Count == 0)
    {
        return BGP4_FAILURE;
    }
    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Bgp4UtilNeighborSoftReconfigClearIpBgp                    */
/* CLI Command   : clear ip bgp                                              */
/* Description   : This function is used to trigger inbound and outbound     */
/*                  routing table updates dynamically.                       */
/* Input(s)      : u1Dir - Indicates in/out bound soft-reconfig needs to be  */
/*                 done                                                      */
/*               : pu4IpAddress - Pointer to the BGP peer's IP Address to    */
/*                 whom the route-refresh message needs to be sent           */
/*                 i4PeerType - Address family of the peer                   */
/*                 u1OrfFlag - ORF flag                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS    - if operation is success                 */
/*               : BGP4_CLEAR_PEER_FAILURE  -                                */
/*                      if failure occurs in memmory allocation              */
/*               : BGP4_CLEAR_SET_FAILURE -                                  */
/*                      if failure in any nmhset funtion                     */
/*****************************************************************************/
INT4
Bgp4UtilNeighborSoftReconfigClearIpBgp (UINT1 u1Dir,
                                        tNetAddress * pIpAddress,
                                        INT4 i4PeerType, UINT1 u1OrfFlag)
{
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    UINT4               u4ErrorCode;
    INT1                i1Status;

    BGP_CLI_MEM_BLK_ALLOC (PeerAddr.pu1_OctetList, UINT1)
        if (PeerAddr.pu1_OctetList == NULL)
    {
        return BGP4_CLEAR_PEER_FAILURE;
    }
    if (u1Dir == BGP4_CLEAR_SOFT_IN || u1Dir == BGP4_CLEAR_SOFT_BOTH)
    {
        PeerAddr.i4_Length = pIpAddress->NetAddr.u2AddressLen;
        MEMCPY (PeerAddr.pu1_OctetList, pIpAddress->NetAddr.au1Address,
                PeerAddr.i4_Length);

        if (u1OrfFlag != BGP4_TRUE)
        {
            i1Status = nmhTestv2Fsbgp4mpeRtRefreshInboundRequest
                (&u4ErrorCode, i4PeerType, &PeerAddr,
                 pIpAddress->NetAddr.u2Afi, pIpAddress->u2Safi,
                 BGP4_RTREF_REQ_SET);
            if (i1Status == SNMP_FAILURE)
            {
                BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
                return BGP4_FAILURE;
            }
            i1Status =
                nmhSetFsbgp4mpeRtRefreshInboundRequest (i4PeerType, &PeerAddr,
                                                        pIpAddress->NetAddr.
                                                        u2Afi,
                                                        pIpAddress->u2Safi,
                                                        BGP4_RTREF_REQ_SET);
        }
        else
        {
            i1Status = nmhTestv2Fsbgp4mpeRtRefreshInboundPrefixFilter
                (&u4ErrorCode, i4PeerType, &PeerAddr,
                 pIpAddress->NetAddr.u2Afi, pIpAddress->u2Safi,
                 BGP4_ORF_REQ_SET);
            if (i1Status == SNMP_FAILURE)
            {
                BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
                return BGP4_FAILURE;
            }
            i1Status =
                nmhSetFsbgp4mpeRtRefreshInboundPrefixFilter (i4PeerType,
                                                             &PeerAddr,
                                                             pIpAddress->
                                                             NetAddr.u2Afi,
                                                             pIpAddress->u2Safi,
                                                             BGP4_ORF_REQ_SET);

        }
        if (i1Status == SNMP_FAILURE)
        {
            BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
            return BGP4_CLEAR_SET_FAILURE;
        }
        MEMSET (PeerAddr.pu1_OctetList, 0, MAX_BGP_CLI_MSG_BUF_LEN);
    }

    if (u1Dir == BGP4_CLEAR_SOFT_OUT || u1Dir == BGP4_CLEAR_SOFT_BOTH)
    {
        PeerAddr.i4_Length = pIpAddress->NetAddr.u2AddressLen;
        MEMCPY (PeerAddr.pu1_OctetList, pIpAddress->NetAddr.au1Address,
                PeerAddr.i4_Length);

        i1Status = nmhTestv2Fsbgp4mpeSoftReconfigOutboundRequest
            (&u4ErrorCode, i4PeerType, &PeerAddr,
             pIpAddress->NetAddr.u2Afi, pIpAddress->u2Safi, BGP4_RTREF_REQ_SET);
        if (i1Status == SNMP_FAILURE)
        {
            BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
            return BGP4_CLEAR_SET_FAILURE;
        }
        i1Status = nmhSetFsbgp4mpeSoftReconfigOutboundRequest (i4PeerType,
                                                               &PeerAddr,
                                                               pIpAddress->
                                                               NetAddr.u2Afi,
                                                               pIpAddress->
                                                               u2Safi,
                                                               BGP4_RTREF_REQ_SET);
        if (i1Status == SNMP_FAILURE)
        {
            BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
            return BGP4_CLEAR_SET_FAILURE;
        }
    }
    BGP_CLI_MEM_BLK_FREE (PeerAddr.pu1_OctetList);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4ConfigClearIpBgp                                      */
/* CLI Command   : clear ip bgp                                              */
/* Description   : Resets the bgp connection for Ipv4/Ipv6 peers based on u1afi*/
/* Input(s)      : u1ASCheck - flag to check AS number 
 *                 u4InputAS - AS Number of the peers to clear 
 *                 u1afi     - Address family of the peers IPv4/Ipv6         */
/*                 u1Group   - clear Group Number                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS    - if operation is success 
 *                 BGP4_FAILURE    - operation failes                        */
/*****************************************************************************/
INT4
Bgp4ConfigClearIpBgp (UINT1 u1ASCheck, UINT4 u4InputAS, UINT1 u1afi,
                      UINT1 u1Group)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         CurPeerAddr;
    tAddrPrefix         NextPeerAddr;
    INT4                i4Sts = BGP4_FAILURE;
    UINT4               u4LocalAS = 0;
    UINT4               u4Count = 0;
    UINT4               u4Context = gpBgpCurrCxtNode->u4ContextId;
    UINT4               u4RRDProtoMask = 0;
    INT4                i4Index = 0;

    /* Need to reset all the active peers in the Autonomous System. 
     * Dont use the for loop
     * with BGP4_PEERENTRY_HEAD, because some peers may have duplicate
     * entries in the Peer list. Such peers need to be processed only
     * once. */
    switch (u1afi)
    {
        case BGP4_INET_AFI_IPV4:
            i4Sts = Bgp4Ipv4GetFirstIndexBgpPeerTable (u4Context, &CurPeerAddr);
            break;

#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
            i4Sts = Bgp4Ipv6GetFirstIndexBgpPeerTable (u4Context, &CurPeerAddr);
            break;
#endif
        default:
            return BGP4_FAILURE;
    }

    if (i4Sts == BGP4_FAILURE)
    {
        /* No peer exists for this Address Family. */
        return BGP4_FAILURE;
    }
    BGP4_MPATH_OPER_EBGP_COUNT (u4Context) = BGP4_MPATH_EBGP_COUNT (u4Context);
    BGP4_MPATH_OPER_IBGP_COUNT (u4Context) = BGP4_MPATH_IBGP_COUNT (u4Context);
    BGP4_MPATH_OPER_EIBGP_COUNT (u4Context) =
        BGP4_MPATH_EIBGP_COUNT (u4Context);
    if (BGP4_LOCAL_BGP_ID_CONFIG_TYPE (u4Context) == BGP4_LOCAL_BGP_ID_DYNAMIC)
    {
        Bgp4GetDefaultBgpIdentifier (u4Context,
                                     &(BGP4_LOCAL_BGP_ID (u4Context)));
        Bgp4RedSyncBgpId (u4Context, BGP4_LOCAL_BGP_ID (u4Context));
    }
    Bgp4RedSyncMpathOperCnt (u4Context,
                             BGP4_MPATH_OPER_IBGP_COUNT (u4Context),
                             BGP4_MPATH_OPER_EBGP_COUNT (u4Context),
                             BGP4_MPATH_OPER_EIBGP_COUNT (u4Context));

    nmhGetBgpLocalAs ((INT4 *) &u4LocalAS);
    do
    {
        pPeer = Bgp4SnmphGetPeerEntry (u4Context, CurPeerAddr);
        if (pPeer == NULL)
        {
            break;
        }
        /* Process the pPeer pointer. */
        if ((u1ASCheck == BGP4_AS_CHECK))
        {
            if (u1Group == BGP4_CLEAR_GROUP3)
            {
                if (u4InputAS != 0 && u4InputAS == BGP4_PEER_ASNO (pPeer))
                {                /* clear ip bgp AS-NUM */
                    if ((BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_START) ||
                        (BGP4_PEER_ADMIN_STATUS (pPeer) ==
                         BGP4_PEER_AUTO_START))
                    {
                        Bgp4DisablePeer (pPeer);
                        Bgp4EnablePeer (pPeer);
                    }
                    u4Count++;
                }
            }
            else
            {                    /* for CLEAR_GROUP1 */
                if (u4LocalAS != BGP4_PEER_ASNO (pPeer))
                {                /* clear ip bgp external - Extenal peers to be clear */
                    if ((BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_START) ||
                        (BGP4_PEER_ADMIN_STATUS (pPeer) ==
                         BGP4_PEER_AUTO_START))
                    {
                        Bgp4DisablePeer (pPeer);
                        Bgp4EnablePeer (pPeer);
                    }
                    u4Count++;
                }
            }
        }
        else
        {
            if ((BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_START) ||
                (BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_AUTO_START))
            {
                for (i4Index = MAX_BGP_ADVT_LINK_NODES_SIZING_ID;
                     i4Index < BGP_MAX_SIZING_ID; i4Index++)
                {
                    gu4BgpDebugCnt[i4Index] = 0;
                }
                Bgp4DisablePeer (pPeer);
                /* When clearing bgp sessions disable redistribution and
                   enable it again to apply the routemap configurations */
                u4RRDProtoMask = BGP4_RRD_PROTO_MASK (u4Context);
                if (Bgp4DisableRRDProtoMask (u4Context, u4RRDProtoMask) !=
                    SNMP_SUCCESS)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG,
                              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                              BGP4_MOD_NAME,
                              "\t Unable to Disable Redistribution\n");
                    return SNMP_FAILURE;
                }
                if (Bgp4EnableRRDProtoMask (u4Context, u4RRDProtoMask) !=
                    SNMP_SUCCESS)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG,
                              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                              BGP4_MOD_NAME,
                              "\t Unable to enable Redistribution\n");
                    return SNMP_FAILURE;
                }
                Bgp4EnablePeer (pPeer);
            }
            u4Count++;
        }
        i4Sts =
            Bgp4GetNextIndexBgpPeerTable (u4Context, CurPeerAddr,
                                          &NextPeerAddr);
        if (i4Sts == BGP4_FAILURE)
        {
            break;
        }
        if (BGP4_AFI_IN_ADDR_PREFIX_INFO (CurPeerAddr) !=
            BGP4_AFI_IN_ADDR_PREFIX_INFO (NextPeerAddr))
        {
            break;
        }
        Bgp4CopyAddrPrefixStruct (&CurPeerAddr, NextPeerAddr);
    }
    while (pPeer != NULL);
    if (u4Count == 0)
    {
        return (BGP4_FAILURE);
    }
    /*Delete the entries from the IGP metric table */
    Bgp4DeleteIgpMetricTable (u4Context);
    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Bgp4GetPeerGroupLocalAsCfgStatus                          */
/* Description   : Obtains the local AS configuration status for             */
/*                 given peer Group                                          */
/* Input(s)      : u4Context - context identifier                            */
/*                 pPeerGroupName - Peer Group name                          */
/* Output(s)     : pu1AsCfgFlag - BGP4_TRUE - when Local AS is static        */
/*                                            configuration                  */
/*                                BGP4_FALSE - when local AS is dynamically  */
/*                                             learnt from global localAs    */
/* Return(s)     : BGP4_SUCCESS /BGP4_FAILURE                                */
/*****************************************************************************/
INT4
Bgp4GetPeerGroupLocalAsCfgStatus (UINT4 u4Context,
                                  tSNMP_OCTET_STRING_TYPE * pPeerGroupName,
                                  UINT1 *pu1AsCfgFlag)
{
    tBgpPeerGroupEntry *pPeerGroup = NULL;
    pPeerGroupName->pu1_OctetList[pPeerGroupName->i4_Length] = '\0';
    pPeerGroup =
        Bgp4GetPeerGroupEntry (u4Context, pPeerGroupName->pu1_OctetList);
    if (pPeerGroup == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Group Name.\n");
        return SNMP_FAILURE;
    }

    *pu1AsCfgFlag = pPeerGroup->u1LocalASConfig;
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4GetPeerLocalAsCfgStatus                               */
/* Description   : Obtains the peer local AS configuration status for the    */
/*                  given peer                                               */
/* Input(s)      : u4Context - context identifier                            */
/*                 i4PeerType - Peer AFI                                     */
/*                 pIpAddress - Peer Ip address                              */
/* Output(s)     : pu1AsCfgFlag - BGP4_TRUE - when Local AS is static        */
/*                                            configuration                  */
/*                                BGP4_FALSE - when local AS is dynamically  */
/*                                             learnt from global localAs    */
/* Return(s)     : BGP4_SUCCESS /BGP4_FAILURE                                */
/*****************************************************************************/
INT4
Bgp4GetPeerLocalAsCfgStatus (UINT4 u4Context, INT4 i4PeerType,
                             tSNMP_OCTET_STRING_TYPE * pIpAddress,
                             UINT1 *pu1AsCfgFlag)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddress;
    INT4                i4RetValue = BGP4_FAILURE;

    i4RetValue = Bgp4LowGetIpAddress (i4PeerType, pIpAddress, &PeerAddress);
    if (i4RetValue == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }
    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeer == NULL)
    {
        return BGP4_FAILURE;
    }
    *pu1AsCfgFlag = BGP4_PEER_LOCAL_AS_CFG (pPeer);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4GetLocalAsCfgStatus                                   */
/* Description   : Obtains the local AS configuration status for the         */
/*                 given peer                                                */
/* Input(s)      : u4Context - context identifier                            */
/* Output(s)     : pu1AsCfgFlag - BGP4_TRUE - when Local AS is configured    */
/*                                            specifically for a VR          */
/*                                BGP4_FALSE - when local AS is copied from  */
/*                                               global AS                   */
/* Return(s)     : BGP4_SUCCESS /BGP4_FAILURE                                */
/*****************************************************************************/
INT4
Bgp4GetLocalAsCfgStatus (UINT4 u4Context, UINT1 *pu1AsCfgFlag)
{
    tBgpCxtNode        *pBgpNode = NULL;

    pBgpNode = gBgpCxtNode[u4Context];
    if (pBgpNode == NULL)
    {
        return BGP4_FAILURE;
    }

    *pu1AsCfgFlag = BGP4_LOCAL_AS_CFG (u4Context);

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4GetBgpIdConfigType                                    */
/* Description   : Obtains the BGP identifier configuration type for the     */
/*                  given context                                            */
/* Input(s)      : u4Context - context identifier                            */
/* Output(s)     : pu1Type - BGP4_TRUE - when Bgp ID is static               */
/*                                       configuration                       */
/*                           BGP4_FALSE - when Bgp Id is dynamically learnt  */
/* Return(s)     : BGP4_SUCCESS /BGP4_FAILURE                                */
/*****************************************************************************/
INT4
Bgp4GetBgpIdConfigType (UINT4 u4Context, UINT1 *pu1Type)
{
    tBgpCxtNode        *pBgpNode = NULL;

    pBgpNode = gBgpCxtNode[u4Context];
    if (pBgpNode == NULL)
    {
        return BGP4_FAILURE;
    }
    *pu1Type = BGP4_LOCAL_BGP_ID_CONFIG_TYPE (u4Context);
    return BGP4_SUCCESS;
}

 /*****************************************************************************/
/* Function Name : BgpCheckBgpClusterId                                      */
/* Description   : Compares the BGP identifier with the cluster ID for the   */
/*                  given context                                            */
/* Input(s)      : u4Context - context identifier                            */
/* Output(s)     : pClusterId - Cluster ID                                   */
/* Return(s)     : BGP4_SUCCESS /BGP4_FAILURE                                */
/*****************************************************************************/

INT4
BgpCheckBgpClusterId (UINT4 u4Context, tSNMP_OCTET_STRING_TYPE * pClusterId)
{
    tBgpCxtNode        *pBgpNode = NULL;
    UINT4               au4CompVal[BGP4_IPV4_PREFIX_LEN];

    MEMSET (au4CompVal, 0, (sizeof (au4CompVal)));

    pBgpNode = gBgpCxtNode[u4Context];
    if (pBgpNode == NULL)
    {
        return BGP4_FAILURE;
    }

    PTR_ASSIGN4 ((au4CompVal), BGP4_LOCAL_BGP_ID (u4Context));
    if (MEMCMP (pClusterId->pu1_OctetList, au4CompVal, BGP4_IPV4_PREFIX_LEN) ==
        0)
    {
        return BGP4_SUCCESS;
    }
    return BGP4_FAILURE;
}

/*****************************************************************************/
/* Function Name : BgpSetContextId                                           */
/* Description   : Assigns u4ContextId to global variable                    */
/* Input(s)      : u4ContextId - context identifier                          */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS /BGP4_FAILURE                                */
/*****************************************************************************/

INT4
BgpSetContextId (INT4 u4ContextId)
{
    gu4BgpCurrContextId = u4ContextId;

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : BgpGetContextId                                           */
/* Description   : gets global variable containing ContextId                 */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

UINT4
BgpGetContextId (VOID)
{
    return gu4BgpCurrContextId;
}

/*****************************************************************************/
/* Function Name : BgpGetTableVersion                                        */
/* Description   : gets table version containing ContextId                   */
/* Input(s)      : Context Id                                                */
/* Output(s)     : table version number                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

UINT4
BgpGetTableVersion (UINT4 u4Context)
{
    return (BGP4_TABLE_VERSION (u4Context));
}

/*****************************************************************************/
/* Function Name : BgpGetContextId                                           */
/* Description   : gets the MD5 password for a peer. Caller : MsrValidateBgp4PwdSet*/
/* Input(s)      : u4Context - context id                                   
 *                 i4Fsbgp4TCPMD5AuthPeerType - Peer type 
 *                 pFsbgp4TCPMD5AuthPeerAddr - Addr of the Peer             */
/* Output(s)     : pRetValFsbgp4TCPMD5AuthPassword - password for the peer.  */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4GetNeighborPasswd (UINT4 u4Context, INT4 i4Fsbgp4TCPMD5AuthPeerType,
                       tSNMP_OCTET_STRING_TYPE * pFsbgp4TCPMD5AuthPeerAddr,
                       tSNMP_OCTET_STRING_TYPE *
                       pRetValFsbgp4TCPMD5AuthPassword)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tAddrPrefix         PeerAddress;
    INT4                i4Status = 0;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return BGP4_FAILURE;
    }
    i4Status = Bgp4LowGetIpAddress (i4Fsbgp4TCPMD5AuthPeerType,
                                    pFsbgp4TCPMD5AuthPeerAddr, &PeerAddress);
    if (i4Status == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }

    pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeerEntry == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No matching peer exists.\n");
        return BGP4_FAILURE;
    }

    if (BGP4_PEER_TCPMD5_PTR (pPeerEntry) != NULL)
    {
        MEMCPY (pRetValFsbgp4TCPMD5AuthPassword->pu1_OctetList,
                BGP4_PEER_TCPMD5_PASSWD (pPeerEntry),
                BGP4_PEER_TCPMD5_PASSWD_LEN (pPeerEntry));
        pRetValFsbgp4TCPMD5AuthPassword->i4_Length =
            BGP4_PEER_TCPMD5_PASSWD_LEN (pPeerEntry);
    }
    else
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - TCP MD5 Password option not enabled.\n");
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4CheckSyncedRoutes                                     */
/* Description   : When moving from Standby state to Active state, the       */
/*                  routes received from peer is verified if it is already   */
/*                  present in RIB. If yes, the route is removed from        */
/*                  the new route list.                                      */
/* Input(s)      : u4Context - Context in which the routes is to be verified */
/*                 pTsNewroutes - New routes learnt list                     */
/* Output(s)     : pTsNewroutes - Updated routes learnt list                 */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4CheckSyncedRoutes (tBgp4PeerEntry * pPeer, tTMO_SLL * pTsNewRoutes)
{
    tLinkNode          *pLkNode = NULL;
    tLinkNode          *pTmpLkNode = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pPeerRoute = NULL;
    tRouteProfile      *pFndRtProfileList = NULL;
    VOID               *pRibNode = NULL;
    UINT4               u4Context = 0;
    INT4                i4RtFound = BGP4_FAILURE;

    u4Context = BGP4_PEER_CXT_ID (pPeer);

    /* Verify if the route is already present in RIB when the 
     * Route selection flag is false and the restart reason 
     * is software upgrade to denote the BGP instance is moving
     * from Standby to Active state */
    if ((BGP4_ROUTE_SELECTION_FLAG (u4Context) == BGP4_TRUE)
        || (BGP4_RESTART_REASON (u4Context) != BGP4_GR_REASON_UPGRADE))
    {
        return BGP4_SUCCESS;
    }

    TMO_DYN_SLL_Scan (pTsNewRoutes, pLkNode, pTmpLkNode, tLinkNode *)
    {
        pPeerRoute = NULL;
        pRtProfile = pLkNode->pRouteProfile;
        i4RtFound = Bgp4RibhLookupRtEntry (pRtProfile, u4Context,
                                           BGP4_TREE_FIND_EXACT,
                                           &pFndRtProfileList, &pRibNode);
        if (i4RtFound == BGP4_FAILURE)
        {
            /* No matching route is present in RIB move to next route */
            continue;
        }
        pPeerRoute = Bgp4DshGetPeerRtFromProfileList (pFndRtProfileList, pPeer);
        if ((pPeerRoute == NULL)
            || ((INT4) BGP4_RT_GET_RT_IF_INDEX (pPeerRoute) == BGP4_INV_INDEX)
            || ((BGP4_RT_GET_RT_IF_INDEX (pPeerRoute) == BGP4_INVALID_IFINDX)))
        {
            /* Matching route is not advertised by this peer.
             * Move to next route */
            continue;
        }
        if ((BGP4_RT_MED (pRtProfile) == BGP4_RT_MED (pPeerRoute))
            && (BGP4_RT_LOCAL_PREF (pRtProfile) ==
                BGP4_RT_LOCAL_PREF (pPeerRoute))
            && (Bgp4AttrIsBgpinfosIdentical (BGP4_RT_BGP_INFO (pRtProfile),
                                             BGP4_RT_BGP_INFO (pPeerRoute))
                == TRUE))
        {
            /* Both the incoming route and the route present in RIB 
             * have the same attributes. Remove the Stale mark for the 
             * route in RIB and remove the route from the NewRoutes list */

            /* If it history route skip the process */

            if ((BGP4_RT_GET_FLAGS (pPeerRoute) & BGP4_RT_HISTORY) !=
                BGP4_RT_HISTORY)
            {
                BGP4_RT_RESET_FLAG (pPeerRoute, BGP4_RT_STALE);
                Bgp4RedSyncRouteUpdToFIB (pPeerRoute);
                /* if the route was added to RTM. inform RTM of route addition 
                 * to remove the Stale mark */
                if ((BGP4_RT_GET_EXT_FLAGS (pPeerRoute) & BGP4_RT_IN_RTM)
                    == BGP4_RT_IN_RTM)
                {
                    BGP4_TRC_ARG2 (NULL,
                                   BGP4_TRC_FLAG, BGP4_DETAIL_TRC,
                                   BGP4_MOD_NAME,
                                   "\tCalling  Bgp4IgphUpdateRouteInFIB    from Func[%s] Line[%d]\n",
                                   __func__, __LINE__);
                    BGP4_RT_SET_FLAG (pPeerRoute,
                                      (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                       BGP4_RT_ADVT_NOTTO_EXTERNAL));
                    Bgp4IgphUpdateRouteInFIB (pPeerRoute);
                    BGP4_RT_RESET_FLAG (pPeerRoute,
                                        (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                         BGP4_RT_ADVT_NOTTO_EXTERNAL));
                }

                TMO_SLL_Delete (pTsNewRoutes, &(pLkNode->TSNext));
                Bgp4DshReleaseLinkNode (pLkNode);
            }
        }
    }
    return BGP4_SUCCESS;
}

/************************************************************************/
/* Function Name      : Bgp4ModuleDisable                                */
/* Description        : This function deletes all the configurations in */
/*                      BGP4 and deletes all its data structures.       */
/* Input(s)           : None                                            */
/* Output(s)          : None                                            */
/* Returns            : None                                            */
/************************************************************************/
PUBLIC VOID
Bgp4Disable (VOID)
{
    UINT4               u4Context = 0;
    INT4                i4RetVal = SNMP_FAILURE;

    i4RetVal = BgpGetFirstActiveContextID (&u4Context);
    while (i4RetVal == SNMP_SUCCESS)
    {
        Bgp4GlobalAdminStatusHandler (u4Context, BGP4_ADMIN_DOWN);
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
    }
    return;
}

/************************************************************************/
/* Function Name      : Bgp4ModuleEnable                                 */
/* Description        : This function initializes the context data      */
/*                      structures                                      */
/* Input(s)           : None                                            */
/* Output(s)          : None                                            */
/* Returns            : OSIX_SUCCESS/OSIX_FAILURE                       */
/************************************************************************/
PUBLIC INT4
Bgp4Enable (VOID)
{
    UINT4               u4Context = 0;
    INT4                i4RetVal = SNMP_FAILURE;

    i4RetVal = BgpGetFirstActiveContextID (&u4Context);
    while (i4RetVal == SNMP_SUCCESS)
    {
        if (Bgp4GlobalAdminStatusHandler (u4Context, BGP4_ADMIN_UP) ==
            BGP4_FAILURE)
        {
            return OSIX_FAILURE;
        }
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Bgp4ApiModuleDisable                            */
/* Description        : This function disables Bgp in all the contexts  */
/* Input(s)           : None                                            */
/* Output(s)          : None                                            */
/* Returns            : None                                            */
/************************************************************************/
PUBLIC VOID
Bgp4ApiModuleDisable (VOID)
{
    BgpLock ();
    Bgp4Disable ();
    BgpUnLock ();
    return;
}

/************************************************************************/
/* Function Name      : Bgp4ApiModuleEnable                             */
/* Description        : This function enables Bgp in all the contexts   */
/* Input(s)           : None                                            */
/* Output(s)          : None                                            */
/* Returns            : OSIX_SUCCESS/OSIX_FAILURE                       */
/************************************************************************/
PUBLIC INT4
Bgp4ApiModuleEnable (VOID)
{
    INT4                i4RetVal;
    BgpLock ();
    i4RetVal = Bgp4Enable ();
    BgpUnLock ();
    return i4RetVal;
}

/****************************************************************************/
/* Function Name : Bgp4GetTcpAoMKTPasswd                                          */
/* Description   : Used to get TCP MKT password to store in MSR             */
/* Input(s)      : u4Context - context id                                   
 *                 i4Fsbgp4TCPMKTId - MKT id  
 *                              */
/* Output(s)     :  pRetValFsbgp4TCPAOMktAuthKey - password for the MKT  */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4GetTcpAoMKTPasswd (UINT4 u4Context, INT4 i4Fsbgp4TCPMKTId,
                       tSNMP_OCTET_STRING_TYPE * pRetValFsbgp4TCPAOMktAuthKey)
{
    tTcpAoAuthMKT      *pMkt = NULL;

    pMkt = BgpFindTCPAOMktInContext (u4Context, i4Fsbgp4TCPMKTId);
    if (pMkt == NULL)
    {
        return BGP4_FAILURE;
    }

    /*Copy the key */
    MEMCPY (pRetValFsbgp4TCPAOMktAuthKey->pu1_OctetList,
            pMkt->au1TcpAOMasterKey, pMkt->u1TcpAOPasswdLength);
    pRetValFsbgp4TCPAOMktAuthKey->i4_Length = pMkt->u1TcpAOPasswdLength;

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : BgpFormMultipathLists                                     */
/* Description   : Function to keep prepare multipath list                    */
/* Input(s)      : pRtProfile - Route profile                                */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS / BGP4_FAILURE                               */
/*****************************************************************************/
INT4
BgpFormMultipathLists (tRouteProfile * pRtProfile)
{

    tRouteProfile      *pRtNextRoute = NULL;
    tRouteProfile      *pRtCurRoute = NULL;
    UINT4               u4VrfId = BGP4_RT_CXT_ID (pRtProfile);

    if (BGP4_MPATH_NOT_ENABLED (u4VrfId))
    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_ALL_FAILURE_TRC | BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tMultipath is not enabled for this context %d.\n",
                       u4VrfId);
        return BGP4_FAILURE;
    }
    /* Prepare new Multipath list */
    pRtCurRoute = pRtProfile;
    pRtProfile->pMultiPathRtNext = NULL;
    pRtNextRoute = BGP4_RT_NEXT (pRtCurRoute);
    BGP4_RT_RESET_EXT_FLAG (pRtCurRoute, BGP4_RT_MULTIPATH);
    while (pRtNextRoute != NULL)
    {
        if ((BGP4_IS_ROUTE_VALID (pRtNextRoute) != BGP4_FALSE) &&
            (BGP4_RT_GET_FLAGS (pRtNextRoute) & BGP4_RT_HISTORY) !=
            BGP4_RT_HISTORY)
        {
            if (BgpCheckForMultipath (pRtProfile, pRtNextRoute) == BGP4_SUCCESS)
            {
                BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_FDB_TRC, BGP4_MOD_NAME,
                               "\tMultipath route %s exist for the route %s with nexthop %s.\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                (pRtNextRoute),
                                                BGP4_RT_AFI_INFO
                                                (pRtNextRoute)),
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               Bgp4PrintIpAddr (BGP4_RT_IMMEDIATE_NEXTHOP
                                                (pRtNextRoute),
                                                BGP4_RT_IMMEDIATE_NEXTHOP_AFI_INFO
                                                (pRtNextRoute)));

                BGP4_RT_SET_EXT_FLAG (pRtNextRoute, BGP4_RT_MULTIPATH);
                pRtCurRoute->pMultiPathRtNext = pRtNextRoute;
                pRtNextRoute->pMultiPathRtNext = NULL;
                pRtCurRoute = pRtNextRoute;
            }
            else if ((BGP4_RT_GET_EXT_FLAGS (pRtNextRoute) & BGP4_RT_MULTIPATH)
                     == BGP4_RT_MULTIPATH)
            {
                BGP4_RT_RESET_EXT_FLAG (pRtNextRoute, BGP4_RT_MULTIPATH);
            }

        }
        pRtNextRoute = BGP4_RT_NEXT (pRtNextRoute);
    }
    return BGP4_SUCCESS;
}

/*******************************************************************************/
/* Function Name :  BgpCheckForMultipath                                       */
/* Description   :  Function to check a route is multipath , which should have */
/*                   below characteristic same as Best path                    */
/*             Common: Weight, Local preference , AS-PATH length , Origin, MED */
/*              i-bgp: igp metric , path is internal,  neighboruing AS         */
/*              e-bgp: igp metric, path is external , neighboruing AS          */
/*             ei-bgp: as path                                                 */
/*               Note: Only one of e-bgp or i-bgp or ei-bgp can be enaled      */
/*                     at a time per context.                                  */
/*                                                                             */
/* Input(s)      :  pRtBestRoute - Best Route                                  */
/*                  pRtNewRoute  - Route to be checked for multipath           */
/* Return(s)     : BGP4_SUCCESs / BGP4_FAILURE                                 */
/*******************************************************************************/
INT4
BgpCheckForMultipath (tRouteProfile * pRtBestRoute, tRouteProfile * pRtNewRoute)
{
    tBgp4Info          *pBestPathBgpInfo = NULL;
    tBgp4Info          *pRouteBgpInfo = NULL;
    tRouteProfile      *pNextMpRouteProfile = NULL;
    UINT4               u4VrfId = BGP4_RT_CXT_ID (pRtNewRoute);
    UINT4               u4LocalAsNo = 0;
    UINT4               u4BestPathAsNo = 0;
    UINT4               u4RouteAsNo = 0;
    UINT4               u4IsIbgpRoute = 0;
    UINT4               u4MpCount = 0;

    if (BGP4_MPATH_NOT_ENABLED (u4VrfId))
    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_ALL_FAILURE_TRC | BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tMultipath is not enabled for this context %d.\n",
                       u4VrfId);
        return BGP4_FAILURE;
    }

    /*Multipath should be calculated only when Best path is a BGP Route */
    if (BGP4_RT_PROTOCOL (pRtBestRoute) != BGP_ID)
    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_ALL_FAILURE_TRC | BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tThe Best route %s is a not a BGP route. Multipath "
                       "will not be checked for non BGP route.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtBestRoute),
                                        BGP4_RT_AFI_INFO (pRtBestRoute)));
        return BGP4_FAILURE;
    }

    pNextMpRouteProfile = pRtBestRoute->pMultiPathRtNext;

    u4MpCount++;                /*Incrementing to count  best path */
    while (pNextMpRouteProfile != NULL)
    {
        pNextMpRouteProfile = pNextMpRouteProfile->pMultiPathRtNext;
        u4MpCount++;
    }

    u4LocalAsNo = BGP4_LOCAL_AS_NO (u4VrfId);
    u4BestPathAsNo = BGP4_PEER_ASNO (BGP4_RT_PEER_ENTRY (pRtBestRoute));
    u4RouteAsNo = BGP4_PEER_ASNO (BGP4_RT_PEER_ENTRY (pRtNewRoute));
    pBestPathBgpInfo = BGP4_RT_BGP_INFO (pRtBestRoute);
    pRouteBgpInfo = BGP4_RT_BGP_INFO (pRtNewRoute);

    if (u4LocalAsNo == u4RouteAsNo)
    {
        u4IsIbgpRoute = BGP4_TRUE;
    }

    /*eibgp OR (ibgp or egbp) will be applicable at same time */
    if (BGP4_MPATH_OPER_EIBGP_COUNT (u4VrfId) > BGP_DEFAULT_MAXPATH)
    {
        if (u4MpCount == BGP4_MPATH_OPER_EIBGP_COUNT (u4VrfId))
        {
            return BGP4_FAILURE;
        }
    }
    else
    {
        if (u4IsIbgpRoute != BGP4_TRUE)    /*EBGP Route */
        {
            if (BGP4_MPATH_OPER_EBGP_COUNT (u4VrfId) == BGP_DEFAULT_MAXPATH)
            {
                return BGP4_FAILURE;
            }
            if (u4MpCount == BGP4_MPATH_OPER_EBGP_COUNT (u4VrfId))
            {
                return BGP4_FAILURE;
            }

            /*Checking for ebgp specifc parameteres - Igp metric */
            if ((BGP4_RT_NH_METRIC (pRtBestRoute) !=
                 BGP4_RT_NH_METRIC (pRtNewRoute)))
            {
                return BGP4_FAILURE;
            }
        }
        else                    /* IBGP */
        {
            if (BGP4_MPATH_OPER_IBGP_COUNT (u4VrfId) == BGP_DEFAULT_MAXPATH)
            {
                return BGP4_FAILURE;
            }
            if (u4MpCount == BGP4_MPATH_OPER_IBGP_COUNT (u4VrfId))
            {
                return BGP4_FAILURE;
            }
            /*Checking for ibgp specifc parameteres */
            if ((BGP4_RT_NH_METRIC (pRtBestRoute) !=
                 BGP4_RT_NH_METRIC (pRtNewRoute)))
            {
                return BGP4_FAILURE;
            }
            /* Best path As should not be external */
            if (u4BestPathAsNo != u4LocalAsNo)
            {
                return BGP4_FAILURE;
            }
        }
    }

    /*Checking AS_PATH for all */
    if (Bgp4AttrIsAspathIdentical (pBestPathBgpInfo, pRouteBgpInfo) == FALSE)
    {
        return BGP4_FAILURE;
    }
    /* Multipath if Best path and new route has
     * same Weight, Local preference , AS-PATH length , Origin, MED */
    if ((BGP4_INFO_WEIGHT (pBestPathBgpInfo) ==
         BGP4_INFO_WEIGHT (pRouteBgpInfo))
        && (BGP4_RT_LOCAL_PREF (pRtBestRoute) ==
            BGP4_RT_LOCAL_PREF (pRtNewRoute))
        && (Bgp4AttrAspathLength (BGP4_INFO_ASPATH (pBestPathBgpInfo)) ==
            Bgp4AttrAspathLength (BGP4_INFO_ASPATH (pRouteBgpInfo)))
        && (BGP4_INFO_ORIGIN (pBestPathBgpInfo) ==
            BGP4_INFO_ORIGIN (pRouteBgpInfo)))
    {
        if (BGP4_RT_MED (pRtBestRoute) == BGP4_RT_MED (pRtNewRoute))
        {
            return BGP4_SUCCESS;
        }
    }
    return BGP4_FAILURE;
}

/*****************************************************************************/
/* Function Name : BgpFormPrevMultipathLists                                 */
/* Description   : Function to prepare previous multipath list                */
/* Input(s)      : pPrevMPathList - SLL head to point new prev multipath list*/
/*                 pRtProfile - Route profile                                */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS / BGP4_FAILURE                               */
/*****************************************************************************/
INT4
BgpFormPrevMultipathLists (tRouteProfile * pRtProfile,
                           tTMO_SLL * pPrevMPathList)
{

    tRouteProfile      *pNextMpRouteProfile = NULL;
    tLinkNode          *pRtLink = NULL;
    UINT4               u4VrfId = BGP4_RT_CXT_ID (pRtProfile);

    TMO_SLL_Init (pPrevMPathList);

    if (BGP4_MPATH_NOT_ENABLED (u4VrfId))
    {
        return BGP4_FAILURE;
    }
    /* Prepare previous Multipath list */
    pNextMpRouteProfile = pRtProfile;
    do
    {
        pRtLink = Bgp4MemAllocateLinkNode (sizeof (tLinkNode));
        if (pRtLink == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC |
                      BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                      "\tBgpFormMultipathLists() : Unable to allocate memory "
                      "for Linknode entry.\n");
            gu4BgpDebugCnt[MAX_BGP_LINK_NOCDES_SIZING_ID]++;
            return BGP4_RSRC_ALLOC_FAIL;
        }
        BGP4_LINK_PROFILE_TO_NODE (pNextMpRouteProfile, pRtLink);
        TMO_SLL_Add (pPrevMPathList, &pRtLink->TSNext);
        pNextMpRouteProfile = pNextMpRouteProfile->pMultiPathRtNext;
    }
    while (pNextMpRouteProfile != NULL);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : BgpReleasePrevMpList                                      */
/* Description   : Releases the given list of link nodes                     */
/* Input(s)      : Link node list  - pPrevMPathList                          */
/* Output(s)     : None.                                                     */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
VOID
BgpReleasePrevMpList (tTMO_SLL * pPrevMPathList)
{
    tLinkNode          *pRtLink = NULL;
    tLinkNode          *pTempLinkNode = NULL;

    if (TMO_SLL_Count (pPrevMPathList) == 0)
    {
        return;
    }
    BGP_SLL_DYN_Scan (pPrevMPathList, pRtLink, pTempLinkNode, tLinkNode *)
    {
        TMO_SLL_Delete (pPrevMPathList, &pRtLink->TSNext);
        Bgp4MemReleaseLinkNode (pRtLink);
    }
    return;
}

/*****************************************************************************/
/* Function Name : Bgp4ValidateAsNumber                                      */
/* Description   : This routine validates the given AS no. string and        */
/*                 converts it to an AS number                               */
/* Input(s)      : *pAsNo - Pointer to the AS number string                  */
/* Output(s)     : *pu4AsNo - Computed AS Number                               */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4ValidateAsNumber (CONST CHR1 * pAsNo, UINT4 *pu4AsNo)
{
    UINT4               u4Context;
    UINT4               u4DigitOne = 0, u4DigitTwo = 0;
    INT4                i4Index = 0;
    INT1                i1DotFound = 0;
    INT1                i1DigitOne[BGP4_MAX_AS_LEN_WITH_DOT],
        i1DigitTwo[BGP4_MAX_AS_DIGITS_LEN_IN_STR];
    INT1                i1IsAsNoValid = BGP4_FALSE;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    MEMSET (i1DigitOne, 0, sizeof (i1DigitOne));
    MEMSET (i1DigitTwo, 0, sizeof (i1DigitTwo));

    /* If the AS string is in A.B (asdot) format - DigitOne = A, DigitTwo = B
     * If the AS string is in asplain format - DigitOne = A */
    while (*pAsNo != '\0')
    {
        if (isdigit (*pAsNo))
        {
            if (i1DotFound == 0)
            {
                MEMCPY (i1DigitOne + i4Index, pAsNo, sizeof (UINT1));
                if (i4Index == BGP4_MAX_AS_LEN)
                    break;
            }
            else
            {
                MEMCPY (i1DigitTwo + i4Index, pAsNo, sizeof (UINT1));
                if (i4Index == BGP4_MAX_AS_DIGITS_LEN)
                    break;
            }
            i4Index++;
            pAsNo++;
            if ((*pAsNo == '.') && (i1DotFound == 0))
            {
                if (*(pAsNo + 1) == '\0')
                    break;
                i1DotFound = 1;
                i4Index = 0;
                pAsNo++;
            }
        }
        else
        {
            return BGP4_FAILURE;
        }
    }

    /* Computing the AS number */
    if (*pAsNo == '\0')
    {
        if (i1DotFound == 1)
        {
            u4DigitOne = (UINT4) ATOI (i1DigitOne);
            u4DigitTwo = (UINT4) ATOI (i1DigitTwo);
            if ((u4DigitOne <= BGP4_MAX_TWO_BYTE_AS) &&
                (u4DigitTwo <= BGP4_MAX_TWO_BYTE_AS))
            {
                *pu4AsNo = (u4DigitOne * (BGP4_MAX_TWO_BYTE_AS + 1)) +
                    u4DigitTwo;
                i1IsAsNoValid = BGP4_TRUE;
            }
        }
        else
        {
            if ((STRLEN (i1DigitOne) >= BGP4_MAX_AS_LEN) &&
                (STRCMP (i1DigitOne, "4294967295") > 0))
            {
                return BGP4_FAILURE;
            }
            *pu4AsNo = (UINT4) ATOI (i1DigitOne);
            i1IsAsNoValid = BGP4_TRUE;
        }
    }
    /* Validating the AS number */
    if (i1IsAsNoValid == BGP4_TRUE)
    {
        if ((BGP4_FOUR_BYTE_ASN_SUPPORT (u4Context) ==
             BGP4_ENABLE_4BYTE_ASN_SUPPORT) && (*pu4AsNo <= BGP4_MAX_AS))
        {
            return BGP4_SUCCESS;
        }
        else if ((BGP4_FOUR_BYTE_ASN_SUPPORT (u4Context) ==
                  BGP4_DISABLE_4BYTE_ASN_SUPPORT) &&
                 (*pu4AsNo <= BGP4_MAX_TWO_BYTE_AS))
        {
            return BGP4_SUCCESS;
        }
    }

    return BGP4_FAILURE;
}

/*****************************************************************************/
/* Function Name : Bgp4GetSpeakerPeer4ByteAsnCapability                      */
/* Description   : This routine finds the 4 byte ASN capability support      */
/*                 of the Speaker and the Peer.                              */
/* Input(s)      : *pPeerentry - Peer for which the Speaker and Peer         */
/*                               support status are to be found              */
/* Output(s)     : None                                                      */
/* Return(s)     : Speaker-Peer Capability                                   */
/*                 BGP4_4BYTE_ASN_SPEAKER_ONLY => SPEAKER Supports,          */
/*                                                PEER Doesn't               */
/*                 BGP4_4BYTE_ASN_SPEAKER_AND_PEER => SPEAKER & PEER Supports*/
/*                 BGP4_4BYTE_ASN_NO_SPEAKER => SPEAKER Doesn't              */
/*****************************************************************************/
INT4
Bgp4GetSpeakerPeer4ByteAsnCapability (tBgp4PeerEntry * pPeerentry)
{
    tSupCapsInfo       *pPeerCapRcvd = NULL;
    UINT4               u4Context;
    INT4                i4NegotiatedStatus;
    UINT1               au1FourByteAsnCapOctetList[CAP_4_BYTE_ASN_LENGTH];
    tSNMP_OCTET_STRING_TYPE FourByteAsnCap;
    u4Context = BGP4_PEER_CXT_ID (pPeerentry);

    FourByteAsnCap.pu1_OctetList = au1FourByteAsnCapOctetList;
    MEMSET (au1FourByteAsnCapOctetList, 0, CAP_4_BYTE_ASN_LENGTH);
    FourByteAsnCap.i4_Length = CAP_4_BYTE_ASN_LENGTH;

    /* Checking for the 4-byte ASN support of the SPEAKER */
    if (BGP4_FOUR_BYTE_ASN_SUPPORT (u4Context) == BGP4_ENABLE_4BYTE_ASN_SUPPORT)
    {

        /* Checking for the 4-byte ASN support of the PEER  - Looking for the
           Negotiated Status in Capabilities List
           1. If Negotiated Status is FALSE - The PEER is OLD BGP Speaker
           2. If Negotiated Status is TRUE  - The PEER is NEW BGP Speaker */

        GetFindSpkrSupCap (pPeerentry,
                           CAP_CODE_4_OCTET_ASNO,
                           CAP_4_BYTE_ASN_LENGTH,
                           &FourByteAsnCap, CAPS_RCVD, &pPeerCapRcvd);
        if (pPeerCapRcvd == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tUnable to get Speaker Supported Capability.\n");
            return BGP4_4BYTE_ASN_SPEAKER_ONLY;
        }

        i4NegotiatedStatus =
            BGP4_PEER_RCVD_CAPS_NEGOTIATED_STATUS (pPeerCapRcvd);

        if (i4NegotiatedStatus == BGP4_TRUE)
        {
            return BGP4_4BYTE_ASN_SPEAKER_AND_PEER;
        }

    }

    return BGP4_4BYTE_ASN_NO_SPEAKER;
}

/*****************************************************************************/
/* Function Name : Bgp4VerifyGlobalState                                     */
/* Description   : This function verifies if the BGP is enabled in all the   */
/*                 VRs.                                                      */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_TRUE - When BGP is enabled in all VRs                */
/*                 BGP4_FALSE - When BGP is not enabled in all VRs           */
/*****************************************************************************/
INT1
Bgp4VerifyGlobalState ()
{
    UINT4               u4Context = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    INT1                i1Return = BGP4_TRUE;

    i4RetVal = BgpGetFirstActiveContextID (&u4Context);
    while (i4RetVal == SNMP_SUCCESS)
    {
        if (BGP4_GLOBAL_STATE (u4Context) == BGP4_GLOBAL_ADMIN_DOWN_INPROGRESS)
        {
            i1Return = BGP4_FALSE;
            break;
        }
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
    }
    return i1Return;
}

/*****************************************************************************/
/* Function Name : Bgp4GetPeerLocalAddrCfgStatus                             */
/* Description   : This function return the local address configuration      */
/*                 status for the given peer address.                        */
/* Input(s)      : u4context - Context Id                                    */
/*                 PeerAddress - Peer Ip Address                             */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_TRUE - When local address is statically configured   */
/*                 BGP4_FALSE - When local address is dynamically learnt     */
/*****************************************************************************/
INT4
Bgp4GetPeerLocalAddrCfgStatus (UINT4 u4Context, tAddrPrefix PeerAddress)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;

    pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if ((pPeerEntry != NULL)
        && (BGP4_PEER_LOCAL_ADDR_CONFIGURED (pPeerEntry) == BGP4_TRUE))
    {
        return BGP4_TRUE;
    }
    return BGP4_FALSE;

}

/*****************************************************************************/
/* Function Name : Bgp4DuplicateBgpInfos                                     */
/* Description   : Duplicates the given BgpInfo information and returns      */
/*                 the new duplicated Bgp4Info pointer.                      */
/* Input(s)      : Bgp4Info information.                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : Duplicated Bgp4Info pointer.                              */
/*****************************************************************************/
tBgp4Info          *
Bgp4DuplicateBgp4Info (tBgp4Info * pBgp4Info)
{
    tBgp4Info          *pDestBgp4Info = NULL;
    tAsPath            *pDestAspath = NULL;
    tAsPath            *pSrcAspath = NULL;
    tCommunity         *pComm = NULL;
    tExtCommunity      *pEcomm;
    tClusterList       *pClusList = NULL;
    UINT1              *pu1ClusterList = NULL;
    UINT1              *pu1CommStr = NULL;
    UINT1              *pu1EcommStr = NULL;
    UINT1              *pu1UnknownAttr = NULL;

    pDestBgp4Info = Bgp4MemAllocateBgp4info (sizeof (tBgp4Info));
    if (pDestBgp4Info == NULL)
    {
        /* Allocation FAILS. */
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "BGP4: Allocate Bgp4Info FAILS!!! \n");
        gu4BgpDebugCnt[MAX_BGP_ROUTE_INFO_ENTRIES_SIZING_ID]++;
        return NULL;
    }
    BGP4_INFO_ATTR_FLAG (pDestBgp4Info) = BGP4_INFO_ATTR_FLAG (pBgp4Info);
    TMO_SLL_Scan (BGP4_INFO_ASPATH (pBgp4Info), pSrcAspath, tAsPath *)
    {
        pDestAspath = Bgp4MemGetASNode (sizeof (tAsPath));
        if (pDestAspath == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "Memory Allocation for Route's AS Node " "FAILED!!!\n");
            Bgp4DshReleaseBgpInfo (pDestBgp4Info);
            gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
            return NULL;
        }
        BGP4_ASPATH_TYPE (pDestAspath) = BGP4_ASPATH_TYPE (pSrcAspath);
        BGP4_ASPATH_LEN (pDestAspath) = BGP4_ASPATH_LEN (pSrcAspath);
        if (BGP4_AS4_SEG_LEN < (BGP4_ASPATH_LEN (pSrcAspath) * BGP4_AS4_LENGTH))
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                      BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tSize of au1ASSegs is less,increase the value of BGP4_AS4_SEG_LEN FAILED!!!\n");
            Bgp4MemReleaseASNode (pDestAspath);
            Bgp4DshReleaseBgpInfo (pDestBgp4Info);
            return NULL;
        }
        MEMSET (BGP4_ASPATH_NOS (pDestAspath), 0, BGP4_AS4_SEG_LEN);
        MEMCPY (BGP4_ASPATH_NOS (pDestAspath), BGP4_ASPATH_NOS (pSrcAspath),
                (BGP4_ASPATH_LEN (pSrcAspath) * BGP4_AS4_LENGTH));

        TMO_SLL_Add (BGP4_INFO_ASPATH (pDestBgp4Info), &pDestAspath->sllNode);
    }
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_AGGREGATOR_MASK)
        == BGP4_ATTR_AGGREGATOR_MASK)
    {
        AGGR_NODE_CREATE (BGP4_INFO_AGGREGATOR (pDestBgp4Info));
        if (BGP4_INFO_AGGREGATOR (pDestBgp4Info) == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "Memory Allocation for Aggregator Attribute FAILED\n");
            Bgp4DshReleaseBgpInfo (pDestBgp4Info);
            gu4BgpDebugCnt[MAX_BGP_AGGR_NODE_SIZING_ID]++;
            return NULL;
        }
        MEMCPY (BGP4_INFO_AGGREGATOR (pDestBgp4Info),
                BGP4_INFO_AGGREGATOR (pBgp4Info), sizeof (tAggregator));
    }
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_COMM_MASK)
        == BGP4_ATTR_COMM_MASK)
    {
        COMMUNITY_NODE_CREATE (pComm);
        if (pComm == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "Memory Allocation for Community Attribute FAILED\n");
            Bgp4DshReleaseBgpInfo (pDestBgp4Info);
            gu4BgpDebugCnt[MAX_BGP_COMMUNITY_NODE_SIZING_ID]++;
            return NULL;
        }
        ATTRIBUTE_NODE_CREATE (pu1CommStr);
        if (pu1CommStr == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "Memory Allocation for Community Attribute string FAILED\n");
            COMMUNITY_NODE_FREE (pComm);
            Bgp4DshReleaseBgpInfo (pDestBgp4Info);
            gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
            return NULL;
        }
        BGP4_INFO_COMM_ATTR (pDestBgp4Info) = pComm;
        BGP4_INFO_COMM_ATTR_VAL (pDestBgp4Info) = pu1CommStr;
        BGP4_INFO_COMM_COUNT (pDestBgp4Info) = BGP4_INFO_COMM_COUNT (pBgp4Info);
        BGP4_INFO_COMM_ATTR_FLAG (pDestBgp4Info) =
            BGP4_INFO_COMM_ATTR_FLAG (pBgp4Info);
        MEMCPY (pu1CommStr, BGP4_INFO_COMM_ATTR_VAL (pBgp4Info),
                (UINT4) ((BGP4_INFO_COMM_COUNT (pBgp4Info)) * COMM_VALUE_LEN));
    }
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_ECOMM_MASK)
        == BGP4_ATTR_ECOMM_MASK)
    {
        EXT_COMMUNITY_NODE_CREATE (pEcomm);
        if (pEcomm == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "Memory Allocation for Ext Community Attribute FAILED\n");
            Bgp4DshReleaseBgpInfo (pDestBgp4Info);
            gu4BgpDebugCnt[MAX_BGP_EXT_COMM_NODE_SIZING_ID]++;
            return NULL;
        }
        BGP4_INFO_ECOMM_ATTR (pDestBgp4Info) = pEcomm;
        BGP4_INFO_ECOMM_COUNT (pDestBgp4Info) =
            BGP4_INFO_ECOMM_COUNT (pBgp4Info);
        BGP4_INFO_ECOMM_ATTR_FLAG (pDestBgp4Info) =
            BGP4_INFO_ECOMM_ATTR_FLAG (pBgp4Info);
        BGP4_INFO_ECOMM_ATTR_COST_FLAG (pDestBgp4Info) =
            BGP4_INFO_ECOMM_ATTR_COST_FLAG (pBgp4Info);
        if (BGP4_INFO_ECOMM_ATTR_COST_FLAG (pBgp4Info) == BGP4_TRUE)
        {
            MEMCPY (&(BGP4_INFO_ECOMM_ATTR (pDestBgp4Info)->ExtCommCost),
                    &(BGP4_INFO_ECOMM_ATTR (pBgp4Info)->ExtCommCost),
                    sizeof (tExtCommCost));
        }
        else
        {
            ATTRIBUTE_NODE_CREATE (pu1EcommStr);
            if (pu1EcommStr == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                          "Memory Allocation for Ext Community Attribute FAILED\n");
                Bgp4DshReleaseBgpInfo (pDestBgp4Info);
                gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
                return NULL;
            }
            BGP4_INFO_ECOMM_ATTR_VAL (pDestBgp4Info) = pu1EcommStr;
            MEMCPY (pu1EcommStr, BGP4_INFO_ECOMM_ATTR_VAL (pBgp4Info),
                    (UINT4) ((BGP4_INFO_ECOMM_COUNT (pBgp4Info)) *
                             EXT_COMM_VALUE_LEN));
        }
    }
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_CLUS_LIST_MASK)
        == BGP4_ATTR_CLUS_LIST_MASK)
    {
        CLUSTER_LIST_CREATE (pClusList);
        if (pClusList == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "Memory Allocation for Cluster list Attribute FAILED\n");
            Bgp4DshReleaseBgpInfo (pDestBgp4Info);
            gu4BgpDebugCnt[MAX_BGP_CLUSTER_LIST_SIZING_ID]++;
            return NULL;
        }
        ATTRIBUTE_NODE_CREATE (pu1ClusterList);
        if (pu1ClusterList == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "Memory Allocation for Cluster list Attribute FAILED\n");
            CLUSTER_LIST_FREE (pClusList);
            Bgp4DshReleaseBgpInfo (pDestBgp4Info);
            gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
            return NULL;
        }
        BGP4_INFO_CLUS_LIST_ATTR (pDestBgp4Info) = pClusList;
        BGP4_INFO_CLUS_LIST_ATTR_VAL (pDestBgp4Info) = pu1ClusterList;
        BGP4_INFO_CLUS_LIST_COUNT (pDestBgp4Info)
            = BGP4_INFO_CLUS_LIST_COUNT (pBgp4Info);
        BGP4_INFO_CLUS_LIST_ATTR_FLAG (pDestBgp4Info)
            = BGP4_INFO_CLUS_LIST_ATTR_FLAG (pBgp4Info);
        MEMCPY (BGP4_INFO_CLUS_LIST_ATTR_VAL (pDestBgp4Info),
                BGP4_INFO_CLUS_LIST_ATTR_VAL (pBgp4Info),
                (UINT4) ((BGP4_INFO_CLUS_LIST_COUNT (pBgp4Info)) *
                         CLUSTER_ID_LENGTH));
    }
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) &
         BGP4_ATTR_NEXT_HOP_MASK) == BGP4_ATTR_NEXT_HOP_MASK)
    {
        MEMCPY (&(BGP4_INFO_NEXTHOP_INFO (pDestBgp4Info)),
                &(BGP4_INFO_NEXTHOP_INFO (pBgp4Info)), sizeof (tAddrPrefix));
    }
#ifdef BGP4_IPV6_WANTED
    BGP4_INFO_LINK_LOCAL_PRESENT (pDestBgp4Info)
        = BGP4_INFO_LINK_LOCAL_PRESENT (pBgp4Info);
    if (BGP4_INFO_LINK_LOCAL_PRESENT (pBgp4Info) == BGP4_TRUE)
    {
        MEMCPY (&(BGP4_INFO_LINK_LOCAL_ADDR_INFO (pDestBgp4Info)),
                &(BGP4_INFO_LINK_LOCAL_ADDR_INFO (pBgp4Info)),
                sizeof (tAddrPrefix));
    }
#endif
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_UNKNOWN_MASK)
        == BGP4_ATTR_UNKNOWN_MASK)
    {
        ATTRIBUTE_NODE_CREATE (pu1UnknownAttr);
        if (pu1UnknownAttr == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "Memory Allocation for Unknown Attribute FAILED\n");
            Bgp4DshReleaseBgpInfo (pDestBgp4Info);
            gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
            return NULL;
        }
        BGP4_INFO_UNKNOWN_ATTR (pDestBgp4Info) = pu1UnknownAttr;
        BGP4_INFO_UNKNOWN_ATTR_LEN (pDestBgp4Info)
            = BGP4_INFO_UNKNOWN_ATTR_LEN (pBgp4Info);
        MEMCPY (BGP4_INFO_UNKNOWN_ATTR (pDestBgp4Info),
                BGP4_INFO_UNKNOWN_ATTR (pBgp4Info),
                BGP4_INFO_UNKNOWN_ATTR_LEN (pBgp4Info));
    }
    BGP4_INFO_ORIG_ID (pDestBgp4Info) = BGP4_INFO_ORIG_ID (pBgp4Info);
    BGP4_INFO_RCVD_MED (pDestBgp4Info) = BGP4_INFO_RCVD_MED (pBgp4Info);
    BGP4_INFO_RCVD_LOCAL_PREF (pDestBgp4Info)
        = BGP4_INFO_RCVD_LOCAL_PREF (pBgp4Info);
    BGP4_INFO_ORIGIN (pDestBgp4Info) = BGP4_INFO_ORIGIN (pBgp4Info);
    BGP4_INFO_IS_RMAP_SET (pDestBgp4Info) = BGP4_INFO_IS_RMAP_SET (pBgp4Info);
    BGP4_INFO_WEIGHT (pDestBgp4Info) = BGP4_INFO_WEIGHT (pBgp4Info);
    BGP4_MPE_BGP_SAFI_INFO (pDestBgp4Info) = BGP4_MPE_BGP_SAFI_INFO (pBgp4Info);
    return pDestBgp4Info;
}

/*****************************************************************************/
/* Function Name : Bgp4DupCompleteRtProfile                                  */
/* Description   : Duplicates the complete route profile information         */
/*                  including the route Bgp information and returns          */
/*                  the new duplicated route profile pointer.                */
/* Input(s)      : Route Profile information.                                */
/* Output(s)     : None.                                                     */
/* Return(s)     : Duplicated route profile pointer.      .                  */
/*****************************************************************************/
tRouteProfile      *
Bgp4DupCompleteRtProfile (tRouteProfile * pRtProfile)
{
    tRouteProfile      *pDstRtProfile = NULL;
    tBgp4Info          *pDstBgp4Info = NULL;

    pDstRtProfile = Bgp4DuplicateRouteProfile (pRtProfile);
    if (pDstRtProfile == NULL)
    {
        return NULL;
    }
    pDstBgp4Info = Bgp4DuplicateBgp4Info (BGP4_RT_BGP_INFO (pRtProfile));
    if (pDstBgp4Info == NULL)
    {
        Bgp4DshReleaseRtInfo (pDstRtProfile);
        return NULL;
    }
    /* Decrement the BgpInfo of Src Rt here as it will be
     * incremented in the Bgp4DuplicateRouteProfile */
    Bgp4DshReleaseBgpInfo (BGP4_RT_BGP_INFO (pRtProfile));
    BGP4_LINK_INFO_TO_PROFILE (pDstBgp4Info, pDstRtProfile);
    return pDstRtProfile;
}

#ifdef VPLSADS_WANTED
/*****************************************************************************/
/* Function Name : Bgp4VplsGetNlriInfoFromNetAddr                            */
/* Description   : This Function get the NLRI info from NetAddr->au1Address  */
/* Input(s)      : pu1NetAddress pRtProfile->NetAddress).NetAddr).au1Address */
/*                 pNlriInfo  NLRI Info                                      */
/* Output(s)     : BGP4_SUCCESS                                              */
/* Return(s)     : None                                                      */
/*****************************************************************************/
INT4
Bgp4VplsGetNlriInfoFromNetAddr (UINT1 *pu1NetAddress, tVplsNlriInfo * pNlriInfo)
{

    /*UINT1 u1LabelBase[4] = {0}; */

    MEMSET (pNlriInfo, 0, sizeof (tVplsNlriInfo));
    /*Length- */
    /*
       PTR_FETCH2(pNlriInfo->u2Length, pu1NetAddress);
       pu1NetAddress += BGP4_VPLS_NLRI_PREFIX_FIELD_LEN;
     */
     /*RD*/
        MEMCPY (pNlriInfo->au1RouteDistinguisher, pu1NetAddress,
                MAX_LEN_RD_VALUE);
    pu1NetAddress += MAX_LEN_RD_VALUE;

    /*VE-ID */
    PTR_FETCH2 (pNlriInfo->u2VeId, pu1NetAddress);
    /*pNlriInfo->u2VeId = *(UINT2*)(pu1NetAddress); */
    pu1NetAddress += BGP4_VPLS_NLRI_VEID_LEN;

     /*VBO*/ PTR_FETCH2 (pNlriInfo->u2VeBaseOffset, pu1NetAddress);
    /* pNlriInfo->u2VeBaseOffset = *(UINT2*)(pu1NetAddress); */
    pu1NetAddress += BGP4_VPLS_NLRI_VBO_LEN;

     /*VBS*/ PTR_FETCH2 (pNlriInfo->u2VeBaseSize, pu1NetAddress);
    /*pNlriInfo->u2VeBaseSize = *(UINT2*)(pu1NetAddress); */
    pu1NetAddress += BGP4_VPLS_NLRI_VBS_LEN;

     /*LB*/
        /*
           MEMCPY(&u1LabelBase[1], pu1NetAddress, BGP4_VPLS_NLRI_LB_LEN);
           PTR_FETCH4(pNlriInfo->u4labelBase, u1LabelBase);
         */
        MEMCPY (&(((UINT1 *) (&(pNlriInfo->u4labelBase)))[1]), pu1NetAddress,
                BGP4_VPLS_NLRI_LB_LEN);
    pNlriInfo->u4labelBase = OSIX_HTONL (pNlriInfo->u4labelBase);

    return BGP4_SUCCESS;
}

#endif
INT4
Bgp4FindDupCommunity (UINT4 *au4Community, UINT2 *u2CommunityCnt)
{
    UINT2               u2Count = 0;
    UINT2               u2Cnt = 0;
    UINT1               u1CommCount = 0;
    for (u1CommCount = 0; u1CommCount < *u2CommunityCnt; u1CommCount++)
    {
        for (u2Count = u1CommCount + 1; u2Count < *u2CommunityCnt;)
        {
            if (au4Community[u2Count] == au4Community[u1CommCount])
            {
                for (u2Cnt = u2Count; u2Cnt < *u2CommunityCnt; u2Cnt++)
                {
                    au4Community[u2Cnt] = au4Community[u2Cnt + 1];
                }
                *u2CommunityCnt = *u2CommunityCnt - 1;
            }
            else
            {
                u2Count++;
            }
        }
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4PrintIntermediateAs                                   */
/* Description   : This Function gives the intermediate As list in the dot   */
/*           notation or usual format.                                 */
/* Input(s)      : pInterAs - Intermediate As list                 */
/* Output(s)     : pOutIntAs - Intermediate in the AS DOT or usual fmt       */
/*           BGP4_SUCCESS                                              */
/* Return(s)     : None                                                      */
/*****************************************************************************/
INT4
Bgp4PrintIntermediateAs (CONST CHR1 * pInterAs, CHR1 * pOutIntAs)
{
    CHR1                au1Temp[BGP4_MAX_TEMP_ASARRAY_SIZE] = { 0 };
    UINT1               u1Count = 0;
    INT4                i4NotationType = 0;
    UINT4               u4ExtractAs = 0;
    /* Intermediate As is empty */
    if (*pInterAs == '\0')
    {
        return BGP4_SUCCESS;
    }

    for (;;)
    {
        u1Count = 0;
        while ((*pInterAs != ',') && (*pInterAs != '\0'))
        {
            au1Temp[u1Count] = *pInterAs;
            pInterAs++;
            u1Count++;
            if (u1Count >= (BGP4_MAX_TEMP_ASARRAY_SIZE - 1))
            {
                break;
            }
        }

        au1Temp[u1Count] = '\0';
        u4ExtractAs = (UINT4) ATOI ((const char *) au1Temp);
        nmhGetFsbgp4FourByteASNotationType (&i4NotationType);
        BGP4_ASN_CONVERT_TO_STRING (i4NotationType, u4ExtractAs, pOutIntAs);
        if (*pInterAs == '\0')
        {
            break;
        }

        pOutIntAs[STRLEN (pOutIntAs)] = ',';
        pOutIntAs = pOutIntAs + STRLEN (pOutIntAs);
        pInterAs++;

    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4IsValidNextHopAddress                                 */
/* Description   : Validates the given prefix address based on AFI           */
/* Input(s)      : AddrPrefix                                                */
/*                 u1DefAddrStatus - Indicates whether default route is VALID*/
/*                                   or not. If BGP4_TRUE, then the default  */
/*                                   route is considered as VALID, else the  */
/*                                   default route is considered INVALID     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE/BGP4_FALSE                                      */
/*****************************************************************************/
UINT1
Bgp4IsValidNextHopAddress (tAddrPrefix AddrPrefix, UINT1 u1DefAddrStatus)
{
    UINT1               u1ValidStatus = BGP4_FALSE;

    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (AddrPrefix))
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4Ipaddr;

            PTR_FETCH_4 (u4Ipaddr,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (AddrPrefix));
            if (u1DefAddrStatus == BGP4_TRUE)
            {
                /* Default route is valid */
                u1ValidStatus = (UINT1) BGP4_IS_MCAST_LOOP_ADDR (u4Ipaddr);
            }
            else
            {
                /* Default route is invalid */
                u1ValidStatus = (UINT1) BGP4_IS_VALID_ADDRESS (u4Ipaddr);
            }
            if (u1ValidStatus == 0)
            {
                return BGP4_FALSE;
            }
            u1ValidStatus = (UINT1) BGP4_IS_ALL_BROADCAST_ADDRESS (u4Ipaddr);
            if (u1ValidStatus == 1)
            {
                return BGP4_FALSE;
            }
            return BGP4_TRUE;
        }
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
        {
            INT4                i4ValidStatus = BGP4_FAILURE;

            i4ValidStatus = Bgp4Ipv6IsValidAddr (&AddrPrefix, u1DefAddrStatus);
            if (i4ValidStatus == BGP4_FAILURE)
            {
                return BGP4_FALSE;
            }
            return BGP4_TRUE;
        }
#endif
        default:
            return BGP4_FALSE;
    }
}

#endif /* BGP4UTIL_C */
