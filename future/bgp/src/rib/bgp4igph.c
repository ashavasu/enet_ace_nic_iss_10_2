/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4igph.c,v 1.59 2017/09/15 06:19:55 siva Exp $
 *
 * Description: This file contains the submodules which deals with 
 *              IGP interaction.
 *
 *******************************************************************/
#ifndef BGP4IGPH_C
#define BGP4IGPH_C

#include "bgp4com.h"
extern INT1         gi1SyncStatus;
/*****************************************************************************/
/* Function Name : Bgp4ImportRoute                                           */
/* Description   : Imports a non-BGP route, and updates the BGP RIB Tree     */
/*                 accordingly. After updating the RIB tree, if there is any */
/*                 change in the best route selected then proper action will */
/*                 be taken accordingly.                                     */
/* Input(s)      : Prefix of the route to be imported (u4Prefix),            */
/*                 Prefix length of the route (u1Prefixlen),                 */
/*                 Protocol from which imported (u4Protocol),                */
/*                 Whether it is ADDition or DELetion (u4Action)             */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS if the operation is successful,              */
/*                 BGP4_FAILURE if it fails.                                 */
/*****************************************************************************/
INT4
Bgp4ImportRoute (UINT4 u4VrfId, tNetAddress ImportAddress,
                 UINT4 u4Protocol, tAddrPrefix NextHopInfo, UINT4 u4Action,
                 UINT4 u4Tag, INT4 i4Metric, UINT4 u4RtIfIndx,
                 UINT1 u1MetricType, UINT1 u1Community, UINT4 *pulCommunity,
                 UINT4 u4RmapMetricSet)
{
    tTMO_SLL            FeasRouteList;
    tTMO_SLL            WithdrawnRouteList;
    tAddrPrefix         InvPrefix;
    INT4                i4RetVal = BGP4_SUCCESS;
    UINT4               u4RRDProtoMask;
    UINT2               u2AddrFamily = 0;
    UINT1               u1AdvFlag = 0;
    tRouteProfile      *pRouteProfile = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tLinkNode          *pScanLinkNode = NULL;
    UINT4               u4Bgp4PrefixLen = 0;

    TMO_SLL_Init (&FeasRouteList);
    TMO_SLL_Init (&WithdrawnRouteList);
    u2AddrFamily =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                      (ImportAddress));
    Bgp4InitAddrPrefixStruct (&(InvPrefix), u2AddrFamily);

    if (BGP4_RRD_ADMIN_STATE (u4VrfId) != BGP4_RRD_ENABLE)
    {
        return (BGP4_FAILURE);
    }
    u4RRDProtoMask = BGP4_RRD_PROTO_MASK (u4VrfId);

    switch (u4Protocol)
    {
        case BGP4_LOCAL_ID:
            if (!(u4RRDProtoMask & BGP4_IMPORT_DIRECT))
            {
                return (BGP4_FAILURE);
            }
            break;

        case BGP4_STATIC_ID:
            if (!(u4RRDProtoMask & BGP4_IMPORT_STATIC))
            {
                return (BGP4_FAILURE);
            }
            break;

        case BGP4_RIP_ID:
            if (!(u4RRDProtoMask & BGP4_IMPORT_RIP))
            {
                return (BGP4_FAILURE);
            }

            break;

        case BGP4_OSPF_ID:
            if (!(u4RRDProtoMask & BGP4_IMPORT_OSPF))
            {
                return (BGP4_FAILURE);
            }
            break;

        case BGP4_ISIS_ID:
            if (!(u4RRDProtoMask & BGP4_IMPORT_ISISL1L2))
            {
                return (BGP4_FAILURE);
            }
            break;
        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tBgp4ImportRoute() : INVALID PROTOCOL TYPE\n");
            return (BGP4_FAILURE);
    }

    BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\tBgp4ImportRoute() : ORIG MASK is SET\n");

    /* In case of default route, the default route will be redistributed only
     * if the default route originate status is enabled. Check this with the
     * changed status value, because it is the latest status. */
    if (((PrefixMatch (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                       (ImportAddress), InvPrefix)) == BGP4_TRUE) &&
        (BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (ImportAddress) == 0) &&
        (BGP4_DEF_ROUTE_ORIG_NEW_STATUS (u4VrfId) ==
         BGP4_DEF_ROUTE_ORIG_DISABLE))
    {
        /* Receive redistributed default route. But the default route
         * origination status is disabled. */
        return (BGP4_FAILURE);
    }

    u1AdvFlag = BGP4_REDIST_ADV_ROUTE;
    pRtProfile = Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
    if (pRtProfile == NULL)
    {
        BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tMemory Allocation for Redistributed Route "
                       "\n\t- Dest %s, Mask %s Protocol %d FAILED!!!\n",
                       Bgp4PrintIpAddr (ImportAddress.NetAddr.au1Address,
                                        ImportAddress.NetAddr.u2Afi),
                       Bgp4PrintIpMask ((UINT1) ImportAddress.u2PrefixLen,
                                        ImportAddress.NetAddr.u2Afi),
                       u4Protocol);
        gu4BgpDebugCnt[MAX_BGP_ROUTES_SIZING_ID]++;
        return (BGP4_FAILURE);
    }

    switch (u2AddrFamily)
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4Mask;
            UINT4               u4Addr;
            UINT4               u4NextHop;

            u4Mask = Bgp4GetSubnetmask
                ((UINT1) BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (ImportAddress));
            PTR_FETCH_4 (u4Addr,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                         (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                          (ImportAddress)));
            u4Addr = u4Addr & u4Mask;
            PTR_FETCH_4 (u4NextHop,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (NextHopInfo));
            /* Reject route with local address as nexthop */
#ifndef RRD_WANTED
            if (Bgp4IsLocalAddr (u4NextHop) == BGP4_TRUE)
            {
                return (BGP4_FAILURE);
            }
#endif

            /* Assign the <AFI, SAFI> and prefix/length */
            BGP4_AFI_IN_ADDR_PREFIX_INFO
                (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                 (BGP4_RT_NET_ADDRESS_INFO (pRtProfile))) = u2AddrFamily;
            BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO
                (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                 (BGP4_RT_NET_ADDRESS_INFO (pRtProfile))) =
                BGP4_IPV4_PREFIX_LEN;
            BGP4_SAFI_IN_NET_ADDRESS_INFO (BGP4_RT_NET_ADDRESS_INFO
                                           (pRtProfile)) =
                BGP4_SAFI_IN_NET_ADDRESS_INFO (ImportAddress);
            PTR_ASSIGN_4 (BGP4_RT_IP_PREFIX (pRtProfile), u4Addr);
            BGP4_RT_PREFIXLEN (pRtProfile) =
                BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (ImportAddress);
            if ((u4Protocol == BGP4_LOCAL_ID) || (u4Protocol == BGP4_RIP_ID) ||
                (u4Protocol == BGP4_OSPF_ID) || (u4Protocol == BGP4_ISIS_ID) ||
                (u4Protocol == BGP4_STATIC_ID))
            {
                Bgp4CopyAddrPrefixStruct (&
                                          (BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                           (pRtProfile)), NextHopInfo);
            }
            u4Bgp4PrefixLen = BGP4_IPV4_PREFIX_LEN;
            break;
        }
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
        {
            tIp6Addr            IPAddr;
            tIp6Addr            IPAddr1;
#ifndef RRD_WANTED
            if (u4Protocol != BGP4_LOCAL_ID)
            {
                /* Reject route with local address as nexthop */
                if (Ip6IsLocalAddr
                    (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (NextHopInfo),
                     &u2Port) == IP_SUCCESS)
                {
                    Bgp4DshReleaseRtInfo (pRtProfile);
                    return (BGP4_FAILURE);
                }
            }
#endif
            MEMCPY (IPAddr1.u1_addr,
                    BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                    (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (ImportAddress)),
                    BGP4_IPV6_PREFIX_LEN);
            Ip6CopyAddrBits (&IPAddr, &IPAddr1,
                             BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO
                             (ImportAddress));

            /* Assign the <AFI, SAFI> and prefix/length */
            BGP4_AFI_IN_ADDR_PREFIX_INFO
                (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                 (BGP4_RT_NET_ADDRESS_INFO (pRtProfile))) = u2AddrFamily;
            BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO
                (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                 (BGP4_RT_NET_ADDRESS_INFO (pRtProfile))) =
                BGP4_IPV6_PREFIX_LEN;
            BGP4_SAFI_IN_NET_ADDRESS_INFO (BGP4_RT_NET_ADDRESS_INFO
                                           (pRtProfile)) =
                BGP4_SAFI_IN_NET_ADDRESS_INFO (ImportAddress);
            MEMCPY (BGP4_RT_IP_PREFIX (pRtProfile), IPAddr.u1_addr,
                    BGP4_IPV6_PREFIX_LEN);
            BGP4_RT_PREFIXLEN (pRtProfile) =
                BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (ImportAddress);
            u4Bgp4PrefixLen = BGP4_IPV6_PREFIX_LEN;
            break;
        }
#endif
        default:
        {
            /* rightnow there is no support for any other address
             * family, so return FAILURE
             */
            Bgp4DshReleaseRtInfo (pRtProfile);
            return (BGP4_FAILURE);
        }
    }
    pRtProfile->pBgpCxtNode = Bgp4GetContextEntry (u4VrfId);
    BGP4_RT_PROTOCOL (pRtProfile) = (UINT1) u4Protocol;
    BGP4_RT_METRIC_TYPE (pRtProfile) = u1MetricType;
    BGP4_RT_GET_RT_IF_INDEX (pRtProfile) = u4RtIfIndx;

    TMO_SLL_Scan (BGP4_REDIST_LIST (u4VrfId), pScanLinkNode, tLinkNode *)
    {
        pRouteProfile = pScanLinkNode->pRouteProfile;
        if ((BGP4_RT_GET_ADV_FLAG (pRouteProfile) & BGP4_REDIST_ADV_ROUTE)
            == BGP4_REDIST_ADV_ROUTE)
        {
            if ((MEMCMP (pRouteProfile->NetAddress.NetAddr.au1Address,
                         pRtProfile->NetAddress.NetAddr.au1Address,
                         u4Bgp4PrefixLen) == 0) &&
                (pRouteProfile->NetAddress.u2PrefixLen ==
                 pRtProfile->NetAddress.u2PrefixLen) &&
                (pRtProfile->NetAddress.NetAddr.u2Afi ==
                 pRouteProfile->NetAddress.NetAddr.u2Afi) &&
                (BGP4_RT_GET_RT_IF_INDEX (pRouteProfile) ==
                 BGP4_RT_GET_RT_IF_INDEX (pRtProfile)) &&
                (MEMCMP (&(BGP4_RT_IMMEDIATE_NEXTHOP_INFO (pRouteProfile)),
                         &(BGP4_RT_IMMEDIATE_NEXTHOP_INFO (pRtProfile)),
                         sizeof (tAddrPrefix)) == 0) &&
                (BGP4_RT_MED (pRouteProfile) == (UINT4) i4Metric))
            {
                if (u4Action == BGP4_IMPORT_RT_ADD)
                {
                    if ((BGP4_RT_GET_FLAGS (pRouteProfile) & BGP4_RT_WITHDRAWN)
                        == BGP4_RT_WITHDRAWN)
                    {
                        BGP4_RT_RESET_FLAG (pRouteProfile, BGP4_RT_WITHDRAWN);
                    }
                    Bgp4DshReleaseRtInfo (pRtProfile);
                    return BGP4_SUCCESS;
                }
                else
                {
                    if ((BGP4_RT_GET_FLAGS (pRouteProfile) & BGP4_RT_WITHDRAWN)
                        != BGP4_RT_WITHDRAWN)
                    {
                        BGP4_RT_SET_FLAG (pRouteProfile, BGP4_RT_WITHDRAWN);
                    }
                    Bgp4DshReleaseRtInfo (pRtProfile);
                    return BGP4_SUCCESS;
                }

            }
        }
    }
    Bgp4DshReleaseRtInfo (pRtProfile);
    Bgp4RedistLock ();
    if (u4Action == BGP4_IMPORT_RT_ADD)
    {
        i4RetVal = Bgp4ImportRouteAdd (u4VrfId, ImportAddress, u4Protocol,
                                       NextHopInfo, u4Tag, i4Metric, u4RtIfIndx,
                                       u1MetricType, u1Community, pulCommunity,
                                       u1AdvFlag, u4RmapMetricSet);
    }
    else
    {
        i4RetVal = Bgp4ImportRouteDelete (u4VrfId, ImportAddress, u4Protocol,
                                          NextHopInfo, u4RtIfIndx, u1AdvFlag);
    }
    Bgp4RedistUnLock ();
    return (i4RetVal);
}

/*****************************************************************************/
/* Function Name : Bgp4IgphAddFeasibleRouteToVrfUpdLists                     */
/* Description   : This function takes the new feasible non-BGP route added  */
/*                 to tree as input and add this route to the FIB update     */
/*                 list/External peers update list  and Internal peer        */
/*                 update list as needed.                                    */
/* Input(s)      : Pointer to the new feasible route (pNewRoute)             */
/*                 Pointer to the old best route (pOldRoute) -               */
/*                    - if pOldRoute is NULL, then the pNewRoute will be     */
/*                      considered as the new feasible route.                */
/*                    - if pOldRoute is not NULL, then the pNewRoute will    */
/*                      be treated as replacement to the pOldRoute           */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4IgphAddFeasibleRouteToVrfUpdLists (tRouteProfile * pNewRoute,
                                       tRouteProfile * pOldRoute)
{
#ifdef L3VPN
    UINT4               u4AsafiMask;

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pNewRoute),
                           BGP4_RT_SAFI_INFO (pNewRoute), u4AsafiMask);
    if (u4AsafiMask != CAP_MP_VPN4_UNICAST)
#endif
    {
        Bgp4IgphAddFeasibleRouteToUpdLists (pNewRoute, pOldRoute);
        return BGP4_SUCCESS;
    }
#ifdef L3VPN
    else
    {
        if (pOldRoute != NULL)
        {
            Bgp4RibhAddReplaceRtToVpnv4List (pNewRoute, pOldRoute);
        }
        else
        {
            Bgp4RibhAddFeasRtToVpnv4List (pNewRoute);
        }
    }
    return (BGP4_SUCCESS);
#endif
}

/*****************************************************************************/
/* Function Name : Bgp4IgphAddFeasibleRouteToUpdLists                        */
/* Description   : This function takes the new feasible non-BGP route added  */
/*                 to tree as input and add this route to the FIB update     */
/*                 list/External peers update list  and Internal peer        */
/*                 update list as needed.                                    */
/* Input(s)      : Pointer to the new feasible route (pNewRoute)             */
/*                 Pointer to the old best route (pOldRoute) -               */
/*                    - if pOldRoute is NULL, then the pNewRoute will be     */
/*                      considered as the new feasible route.                */
/*                    - if pOldRoute is not NULL, then the pNewRoute will    */
/*                      be treated as replacement to the pOldRoute           */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4IgphAddFeasibleRouteToUpdLists (tRouteProfile * pNewRoute,
                                    tRouteProfile * pOldRoute)
{
    UINT4               u4AdvtPolicy;
    tRouteProfile      *pTempRtProfile = NULL;

#ifdef VPLSADS_WANTED
    UINT4               u4AsafiMask;

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pNewRoute),
                           BGP4_RT_SAFI_INFO (pNewRoute), u4AsafiMask);
#endif

    /* Just a precautionary measure */
    BGP4_RT_RESET_FLAG (pNewRoute, BGP4_RT_WITHDRAWN);

    /* Check whether the Replacement flag is set in the pNewRoute
     * if the pOldRoute is not NULL. If flag not set in pNewRoute, then
     * return. This check is to ensure that caller calls this routine
     * with proper intension to handle the replacement route. */
    if ((pOldRoute != NULL) &&
        ((BGP4_RT_GET_FLAGS (pNewRoute) & BGP4_RT_REPLACEMENT) !=
         BGP4_RT_REPLACEMENT))
    {
        /* Replacement flag not set. Route will not be processed. Just
         * return. */
        BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                  "\tBgp4IgphAddFeasibleRouteToUpdLists() : Replacement "
                  "flag is not set.\n");
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_ALL_FAILURE_TRC | BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tReplacement flag is not set for the new route %s. "
                       "Hence the route will not be added to FIB update list.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pNewRoute),
                                        BGP4_RT_AFI_INFO (pNewRoute)));
        return BGP4_FAILURE;
    }

    Bgp4AggrProcessFeasibleRoute (pNewRoute, pOldRoute);

#ifndef RRD_WANTED
    /* If the old route is STATIC, then this route is added via BGP and
     * this route should be removed explicityly. So add this route
     * to the FIB update list. But set the BGP4_RT_ADVT_NOTTO_EXTERNAL
     * so that the route is not advertised to the external peer. */
    if ((pOldRoute != NULL) && (BGP4_RT_PROTOCOL (pOldRoute) == BGP4_STATIC_ID))
    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_EVENTS_TRC | BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tThe old route %s was added via BGP. Hence it is added to "
                       "FIB update list to remove explicitly and will not be advertised "
                       "to the external peer.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pOldRoute),
                                        BGP4_RT_AFI_INFO (pOldRoute)));
        BGP4_RT_SET_FLAG (pOldRoute, (BGP4_RT_WITHDRAWN |
                                      BGP4_RT_ADVT_NOTTO_EXTERNAL));
        Bgp4AddWithdRouteToFIBUpdList (pOldRoute);

        BGP4_RT_SET_FLAG (pNewRoute, BGP4_RT_NEWUPDATE_TOIP);
    }
#endif

    /* The pNewRoute is selected as the best route. This route
     * should be advertised to the peer. If the protocol is STATIC
     * and if RRD_WANTED switch is disabled, then STATIC routes
     * can be added via BGP. So in that case, STATIC routes needs
     * to be added to FIB. So in that case add the route to the
     * FIB update list. Else if RRD_WANTED is enabled, then no route
     * should be given to FIB for updation. So directly add those
     * route as to the External Peers advertisement list. Irrespective
     * of RRD_WANTED switch is enabled or disabled, the routes should 
     * be added to the Internal Peer update list depanding upon the
     * non-BGP routes import policy. */
    if (BGP4_RT_PROTOCOL (pNewRoute) == BGP4_STATIC_ID)
    {
#ifdef RRD_WANTED
        /* Add the new route to External peer Advertisement list. */
        if ((BGP4_RT_GET_FLAGS (pNewRoute) & BGP4_RT_REPLACEMENT)
            == BGP4_RT_REPLACEMENT)
        {
            /* pNewRoute has replaced the pOldRoute. If old route is learnt
             * from BGP peer, needs to remove it from FIB. No need to advt
             * the old route. */
            if (pOldRoute != NULL)
            {
                if (BGP4_RT_PROTOCOL (pOldRoute) == BGP_ID)
                {
                    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_EVENTS_TRC | BGP4_FDB_TRC,
                                   BGP4_MOD_NAME,
                                   "\tThe old route %s was added via BGP. Hence it is added to "
                                   "FIB update list to remove explicitly and will not be advertised "
                                   "to the external peer.\n",
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pOldRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pOldRoute)));
                    BGP4_RT_SET_FLAG (pOldRoute,
                                      (BGP4_RT_WITHDRAWN |
                                       BGP4_RT_ADVT_NOTTO_EXTERNAL));
                    Bgp4AddWithdRouteToFIBUpdList (pOldRoute);

                    pTempRtProfile = pOldRoute->pMultiPathRtNext;
                    while (pTempRtProfile != NULL)
                    {
                        BGP4_RT_RESET_EXT_FLAG (pTempRtProfile,
                                                BGP4_RT_MULTIPATH);
                        BGP4_RT_SET_FLAG (pTempRtProfile,
                                          (BGP4_RT_WITHDRAWN |
                                           BGP4_RT_ADVT_NOTTO_EXTERNAL));
                        Bgp4AddWithdRouteToFIBUpdList (pTempRtProfile);
                        pTempRtProfile = pTempRtProfile->pMultiPathRtNext;
                    }
                }
            }
            if (pOldRoute != NULL)
            {
                BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_EVENTS_TRC | BGP4_FDB_TRC, BGP4_MOD_NAME,
                               "\tThe route %s is the replacement route for the old route %s "
                               "and it is added to the external peer advertisement list.\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pNewRoute),
                                                BGP4_RT_AFI_INFO (pNewRoute)),
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pOldRoute),
                                                BGP4_RT_AFI_INFO (pOldRoute)));
            }
            Bgp4AddFeasRouteToExtPeerAdvtList (pNewRoute, pOldRoute);
        }
        else
        {
            /* pNewRoute is new feasible route. */
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tThe route %s is the new feasible route and it is "
                           "added to the external peer advertisement list.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pNewRoute),
                                            BGP4_RT_AFI_INFO (pNewRoute)));
            Bgp4AddFeasRouteToExtPeerAdvtList (pNewRoute, NULL);
        }
#else
        /* Adding the new best route to FIB update list */
        if ((BGP4_RT_GET_FLAGS (pNewRoute) & BGP4_RT_REPLACEMENT)
            == BGP4_RT_REPLACEMENT)
        {
            /* pNewRoute has replaced the pOldRoute. */
            if ((BGP4CanRepRouteBeAddedToFIB (pNewRoute, pOldRoute) ==
                 BGP4_FALSE) && (BGP4_RT_PROTOCOL (pOldRoute) == BGP_ID))
            {
                /* Next hop not identical and the old route is learnt via
                 * BGP. So explicitly remove the old route from FIB. */
                BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_EVENTS_TRC | BGP4_FDB_TRC, BGP4_MOD_NAME,
                               "\tThe old route %s was added via BGP. Hence it is added to "
                               "FIB update list to remove explicitly and will not be advertised "
                               "to the external peer.\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pOldRoute),
                                                BGP4_RT_AFI_INFO (pOldRoute)));
                BGP4_RT_SET_FLAG (pOldRoute, (BGP4_RT_WITHDRAWN |
                                              BGP4_RT_ADVT_NOTTO_EXTERNAL));
                Bgp4AddWithdRouteToFIBUpdList (pOldRoute);
                BGP4_RT_SET_FLAG (pNewRoute, BGP4_RT_NEWUPDATE_TOIP);
                Bgp4AddFeasRouteToFIBUpdList (pNewRoute, NULL);
            }
            else
            {
                if (BGP4_RT_PROTOCOL (pOldRoute) == BGP4_STATIC_ID)
                {
                    Bgp4AddFeasRouteToFIBUpdList (pNewRoute, NULL);
                }
                else
                {
                    Bgp4AddFeasRouteToFIBUpdList (pNewRoute, pOldRoute);
                }
            }
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tThe new route %s is the new feasible route and it is "
                           "added to the FIB update list.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pNewRoute),
                                            BGP4_RT_AFI_INFO (pNewRoute)));
        }
        else
        {
            /* pNewRoute is new feasible route. */
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tThe new route %s is the new feasible route and it is "
                           "added to the FIB update list.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pNewRoute),
                                            BGP4_RT_AFI_INFO (pNewRoute)));
            Bgp4AddFeasRouteToFIBUpdList (pNewRoute, NULL);
        }
#endif
    }
    else if (BGP4_RT_PROTOCOL (pNewRoute) != BGP_ID)
    {
#ifdef VPLSADS_WANTED
        /*non-BGP vpls routes should not be added in EXT_PEER_ADVT_LIST */
        if (u4AsafiMask != CAP_MP_L2VPN_VPLS)
#endif
        {
            /* New route is a non-BGP route other than the static route.
             * Add the new route to External peer Advertisement list. */
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tThe new route %s is a non BGP route.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pNewRoute),
                                            BGP4_RT_AFI_INFO (pNewRoute)));
            if ((BGP4_RT_GET_FLAGS (pNewRoute) & BGP4_RT_REPLACEMENT)
                == BGP4_RT_REPLACEMENT)
            {
                if (pOldRoute != NULL)
                {
                    /* pNewRoute has replaced the pOldRoute. If the old route is
                     * BGP route then this needs to be removed from the FIB table. */
                    if (BGP4_RT_PROTOCOL (pOldRoute) == BGP_ID)
                    {
                        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC | BGP4_EVENTS_TRC |
                                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                                       "\tThe old route %s was added via BGP. Hence it is added to "
                                       "FIB update list to remove explicitly and will not be advertised "
                                       "to the external peer.\n",
                                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                        (pOldRoute),
                                                        BGP4_RT_AFI_INFO
                                                        (pOldRoute)));
                        BGP4_RT_SET_FLAG (pOldRoute,
                                          (BGP4_RT_WITHDRAWN |
                                           BGP4_RT_ADVT_NOTTO_EXTERNAL));
                        Bgp4AddWithdRouteToFIBUpdList (pOldRoute);

                        pTempRtProfile = pOldRoute->pMultiPathRtNext;
                        while (pTempRtProfile != NULL)
                        {
                            BGP4_RT_RESET_EXT_FLAG (pTempRtProfile,
                                                    BGP4_RT_MULTIPATH);
                            BGP4_RT_SET_FLAG (pTempRtProfile,
                                              (BGP4_RT_WITHDRAWN |
                                               BGP4_RT_ADVT_NOTTO_EXTERNAL));
                            Bgp4AddWithdRouteToFIBUpdList (pTempRtProfile);
                            pTempRtProfile = pTempRtProfile->pMultiPathRtNext;
                        }
                    }
                }
                if (pOldRoute != NULL)
                {
                    BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_EVENTS_TRC | BGP4_FDB_TRC,
                                   BGP4_MOD_NAME,
                                   "\tThe route %s is the replacement route for the old route %s "
                                   "and it is added to the external peer advertisement list.\n",
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pNewRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pNewRoute)),
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pOldRoute),
                                                    BGP4_RT_AFI_INFO
                                                    (pOldRoute)));
                }
                Bgp4AddFeasRouteToExtPeerAdvtList (pNewRoute, pOldRoute);
            }
            else
            {
                /* pNewRoute is new feasible route. */
                BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_EVENTS_TRC | BGP4_FDB_TRC, BGP4_MOD_NAME,
                               "\tThe route %s is the new feasible route and it is "
                               "added to the external peer advertisement list.\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pNewRoute),
                                                BGP4_RT_AFI_INFO (pNewRoute)));
                Bgp4AddFeasRouteToExtPeerAdvtList (pNewRoute, NULL);
            }
        }
    }
    else
    {
        /* New route is BGP route. Needs to be updated in the FIB. Add it
         * to the FIB update list. */
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tThe Old route %s is a non BGP route. Hence it is not updated "
                       " in FIB advertisement list\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pOldRoute),
                                        BGP4_RT_AFI_INFO (pOldRoute)));
        if ((BGP4_RT_GET_FLAGS (pNewRoute) & BGP4_RT_REPLACEMENT)
            == BGP4_RT_REPLACEMENT)
        {
#ifdef RRD_WANTED
            /* pNewRoute has replaced the pOldRoute. Since new route is
             * learnt from BGP peer, needs to add this route to the FIB.
             * Since old route is a non-BGP route no need to update FIB/advt
             * this route. */
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tThe new route %s is the new feasible route and it is "
                           "added to the FIB update list.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pNewRoute),
                                            BGP4_RT_AFI_INFO (pNewRoute)));
            Bgp4AddFeasRouteToFIBUpdList (pNewRoute, NULL);
            /* New Best route is BGP so check for Multipath */
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tChecking multipath for the new best route %s.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pNewRoute),
                                            BGP4_RT_AFI_INFO (pNewRoute)));
            BgpFormMultipathLists (pNewRoute);
            pTempRtProfile = pNewRoute->pMultiPathRtNext;
            while (pTempRtProfile)
            {
                Bgp4AddFeasRouteToFIBUpdList (pTempRtProfile, NULL);
                pTempRtProfile = pTempRtProfile->pMultiPathRtNext;
            }

#else
            if (BGP4_RT_PROTOCOL (pOldRoute) == BGP4_STATIC_ID)
            {
                Bgp4AddFeasRouteToFIBUpdList (pNewRoute, NULL);
            }
            else
            {
                Bgp4AddFeasRouteToFIBUpdList (pNewRoute, pOldRoute);
            }
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tThe new route %s is the new feasible route and it is "
                           "added to the FIB update list.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pNewRoute),
                                            BGP4_RT_AFI_INFO (pNewRoute)));
#endif
        }
        else
        {
            /* pNewRoute is new feasible route. */
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                           BGP4_EVENTS_TRC | BGP4_FDB_TRC, BGP4_MOD_NAME,
                           "\tThe new route %s is the new feasible route and it is "
                           "added to the FIB update list.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pNewRoute),
                                            BGP4_RT_AFI_INFO (pNewRoute)));
            Bgp4AddFeasRouteToFIBUpdList (pNewRoute, NULL);
        }
    }

    /* Check for the Non-BGP routes advertisement policy. If the
     * policy is set as BGP4_EXT_OR_INT_PEER, then add the route to the
     * internal peers advertisement list, else if the policy is
     * BGP4_EXTERNAL_PEER then dont add the routes to internal peers
     * advertisement list. */

    if (BGP4_NON_BGP_EVENT_RCVD (BGP4_RT_CXT_ID (pNewRoute)) != 0)
    {
        u4AdvtPolicy = BGP4_NON_BGP_EVENT_RCVD (BGP4_RT_CXT_ID (pNewRoute));
    }
    else
    {
        u4AdvtPolicy =
            BGP4_NON_BGP_RT_EXPORT_POLICY (BGP4_RT_CXT_ID (pNewRoute));
    }

    if (u4AdvtPolicy == BGP4_EXT_OR_INT_PEER)
    {
        /* Add the new route to Internal peer Advertisement list. */
        if ((BGP4_RT_GET_FLAGS (pNewRoute) & BGP4_RT_REPLACEMENT)
            == BGP4_RT_REPLACEMENT)
        {
            /* pNewRoute has replaced the pOldRoute. */
            Bgp4AddFeasRouteToIntPeerAdvtList (pNewRoute, pOldRoute);
        }
        else
        {
            /* pNewRoute is new feasible route. */
            Bgp4AddFeasRouteToIntPeerAdvtList (pNewRoute, NULL);
        }
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                       BGP4_EVENTS_TRC | BGP4_FDB_TRC, BGP4_MOD_NAME,
                       "\tThe new route %s is the new feasible route and it is "
                       "added to the internal peer advertisement list.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pNewRoute),
                                        BGP4_RT_AFI_INFO (pNewRoute)));
    }
    return (BGP4_SUCCESS);
}

#ifdef L3VPN
/*****************************************************************************/
/* Function Name : Bgp4IgphCheckFeasibleRouteForVrfUpdt                     */
/* Description   : This function takes the new feasible non-BGP route added  */
/*                 to tree as input and add this route to the FIB update     */
/*                 list/External peers update list  and Internal peer        */
/*                 update list as needed.                                    */
/* Input(s)      : Pointer to the new feasible route (pNewRoute)             */
/*                 Pointer to the old best route (pOldRoute) -               */
/*                    - if pOldRoute is NULL, then the pNewRoute will be     */
/*                      considered as the new feasible route.                */
/*                    - if pOldRoute is not NULL, then the pNewRoute will    */
/*                      be treated as replacement to the pOldRoute           */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4IgphCheckFeasibleRouteForVrfUpdt (tRouteProfile * pNewRoute,
                                      tRouteProfile * pOldRoute, UINT4 u4VrfId,
                                      tTMO_SLL * pFeasFIBList,
                                      tTMO_SLL * pWithFIBList)
{
    UINT4               u4VrfFlags = 0;

    /* Just a precautionary measure */
    BGP4_RT_RESET_FLAG (pNewRoute, BGP4_RT_WITHDRAWN);

    if (BGP4_RT_PROTOCOL (pNewRoute) != BGP_ID)
    {
        /* New Best route is non-BGP route. Set the Best route flag. */
        Bgp4Vpnv4SetVrfFlags (pNewRoute, u4VrfId, BGP4_RT_BGP_VRF_BEST);
    }

    /* Check whether the Replacement flag is set in the pNewRoute
     * if the pOldRoute is not NULL. If flag not set in pNewRoute, then
     * return. This check is to ensure that caller calls this routine
     * with proper intension to handle the replacement route. */
    Bgp4Vpnv4GetVrfFlags (pNewRoute, u4VrfId, &u4VrfFlags);
    if ((pOldRoute != NULL) && (!(u4VrfFlags & BGP4_RT_VRF_REPLACEMENT)))
    {
        /* Replacement flag not set. Route will not be processed. Just
         * return. */
        BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                  "\tBgp4IgphAddFeasibleRouteToUpdLists() : Replacement "
                  "flag is not set.\n");
        return BGP4_FAILURE;
    }

#ifndef RRD_WANTED
    /* If the old route is STATIC, then this route is added via BGP and
     * this route should be removed explicityly. So add this route
     * to the FIB update list. But set the BGP4_RT_ADVT_NOTTO_EXTERNAL
     * so that the route is not advertised to the external peer. */
    if ((pOldRoute != NULL) && (BGP4_RT_PROTOCOL (pOldRoute) == BGP4_STATIC_ID))
    {
        Bgp4Vpnv4ResetVrfFlags (pOldRoute, u4VrfId, BGP4_RT_VRF_NEWUPDATE_TOIP);
        Bgp4Vpnv4SetVrfFlags (pOldRoute, u4VrfId, (BGP4_RT_VRF_WITHDRAWN |
                                                   BGP4_RT_VRF_ADVT_NOTTO_EXTERNAL));
        Bgp4Vpnv4SetVrfFlags (pNewRoute, u4VrfId, BGP4_RT_VRF_NEWUPDATE_TOIP);
        if (Bgp4DshGetRouteFromList (pWithFIBList, pOldRoute) == NULL)
        {
            Bgp4DshAddRouteToList (pWithFIBList, pOldRoute, 0);
        }
    }
#endif

    /* The pNewRoute is selected as the best route. This route
     * should be advertised to the peer. If the protocol is STATIC
     * and if RRD_WANTED switch is disabled, then STATIC routes
     * can be added via BGP. So in that case, STATIC routes needs
     * to be added to FIB. So in that case add the route to the
     * FIB update list. Else if RRD_WANTED is enabled, then no route
     * should be given to FIB for updation. So directly add those
     * route as to the External Peers advertisement list. Irrespective
     * of RRD_WANTED switch is enabled or disabled, the routes should 
     * be added to the Internal Peer update list depanding upon the
     * non-BGP routes import policy. */
    if (BGP4_RT_PROTOCOL (pNewRoute) == BGP4_STATIC_ID)
    {
#ifdef RRD_WANTED
        /* Add the new route to External peer Advertisement list. */
        if ((u4VrfFlags & BGP4_RT_VRF_REPLACEMENT) && (pOldRoute != NULL))
        {
            /* pNewRoute has replaced the pOldRoute. If old route is learnt
             * from BGP peer, needs to remove it from FIB. No need to advt
             * the old route. */
            if (BGP4_RT_PROTOCOL (pOldRoute) == BGP_ID)
            {
                Bgp4Vpnv4SetVrfFlags (pOldRoute, u4VrfId,
                                      (BGP4_RT_VRF_WITHDRAWN |
                                       BGP4_RT_VRF_ADVT_NOTTO_EXTERNAL));
                Bgp4Vpnv4ResetVrfFlags (pOldRoute, u4VrfId,
                                        BGP4_RT_VRF_NEWUPDATE_TOIP);

                if (Bgp4DshGetRouteFromList (pWithFIBList, pOldRoute) == NULL)
                {
                    Bgp4DshAddRouteToList (pWithFIBList, pOldRoute, 0);
                }
            }
            else
            {
                /* Reset the Best route flag. */
                Bgp4Vpnv4ResetVrfFlags (pOldRoute, u4VrfId,
                                        BGP4_RT_BGP_VRF_BEST);
            }
        }
        else
        {
            /* pNewRoute is new feasible route. */
            /* If route leaking is enabled . Update the new route to IP */
            if (BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED)
            {
                Bgp4Vpnv4SetVrfFlags (pNewRoute, u4VrfId,
                                      BGP4_RT_VRF_NEWUPDATE_TOIP);

                if (Bgp4DshGetRouteFromList (pFeasFIBList, pNewRoute) == NULL)
                {
                    Bgp4DshAddRouteToList (pFeasFIBList, pNewRoute, 0);
                }
            }

        }
#else
        /* Adding the new best route to FIB update list */
        if ((u4VrfFlags & BGP4_RT_VRF_REPLACEMENT) && (pOldRoute != NULL))
        {
            /* pNewRoute has replaced the pOldRoute. */
            if ((BGP4CanRepRouteBeAddedToFIB (pNewRoute, pOldRoute) ==
                 BGP4_FALSE) && (BGP4_RT_PROTOCOL (pOldRoute) == BGP_ID))
            {
                /* Next hop not identical and the old route is learnt via
                 * BGP. So explicitly remove the old route from FIB. */
                Bgp4Vpnv4SetVrfFlags (pOldRoute, u4VrfId,
                                      (BGP4_RT_VRF_WITHDRAWN |
                                       BGP4_RT_VRF_ADVT_NOTTO_EXTERNAL));
                Bgp4Vpnv4ResetVrfFlags (pOldRoute, u4VrfId,
                                        BGP4_RT_VRF_NEWUPDATE_TOIP);
                if (Bgp4DshGetRouteFromList (pWithFIBList, pOldRoute) == NULL)
                {
                    Bgp4DshAddRouteToList (pWithFIBList, pOldRoute, 0);
                }
                Bgp4Vpnv4SetVrfFlags (pNewRoute, u4VrfId,
                                      BGP4_RT_VRF_NEWUPDATE_TOIP);
                if (Bgp4DshGetRouteFromList (pFeasFIBList, pNewRoute) == NULL)
                {
                    Bgp4DshAddRouteToList (pFeasFIBList, pNewRoute, 0);
                }
            }
            else
            {
                if (BGP4_RT_PROTOCOL (pOldRoute) == BGP4_STATIC_ID)
                {
                    Bgp4Vpnv4SetVrfFlags (pNewRoute, u4VrfId,
                                          BGP4_RT_VRF_NEWUPDATE_TOIP);
                    if (Bgp4DshGetRouteFromList (pFeasFIBList, pNewRoute)
                        == NULL)
                    {
                        Bgp4DshAddRouteToList (pFeasFIBList, pNewRoute, 0);
                    }
                }
                else
                {
                    Bgp4Vpnv4ResetVrfFlags (pOldRoute, u4VrfId,
                                            BGP4_RT_VRF_NEWUPDATE_TOIP);
                    if (Bgp4DshGetRouteFromList (pFeasFIBList, pOldRoute)
                        == NULL)
                    {
                        Bgp4DshAddRouteToList (pFeasFIBList, pOldRoute, 0);
                    }
                    Bgp4Vpnv4SetVrfFlags (pNewRoute, u4VrfId,
                                          BGP4_RT_VRF_NEWUPDATE_TOIP);
                    if (Bgp4DshGetRouteFromList (pFeasFIBList, pNewRoute)
                        == NULL)
                    {
                        Bgp4DshAddRouteToList (pFeasFIBList, pNewRoute, 0);
                    }
                }
            }
        }
        else
        {
            Bgp4Vpnv4SetVrfFlags (pNewRoute, u4VrfId,
                                  BGP4_RT_VRF_NEWUPDATE_TOIP);
            /* pNewRoute is new feasible route. */
            if (Bgp4DshGetRouteFromList (pFeasFIBList, pNewRoute) == NULL)
            {
                Bgp4DshAddRouteToList (pFeasFIBList, pNewRoute, 0);
            }
        }
#endif
    }
    else if (BGP4_RT_PROTOCOL (pNewRoute) != BGP_ID)
    {
        /* New route is a non-BGP route other than the static route.
         * Add the new route to External peer Advertisement list. */
        if ((u4VrfFlags & BGP4_RT_VRF_REPLACEMENT) && (pOldRoute != NULL))
        {
            /* pNewRoute has replaced the pOldRoute. If the old route is
             * BGP route then this needs to be removed from the FIB table. */
            if (BGP4_RT_PROTOCOL (pOldRoute) == BGP_ID)
            {
                Bgp4Vpnv4SetVrfFlags (pOldRoute, u4VrfId,
                                      (BGP4_RT_VRF_WITHDRAWN |
                                       BGP4_RT_VRF_ADVT_NOTTO_EXTERNAL));
                if (Bgp4DshGetRouteFromList (pWithFIBList, pOldRoute) == NULL)
                {
                    Bgp4DshAddRouteToList (pWithFIBList, pOldRoute, 0);
                }
            }
            else
            {
                /* Reset the Best route flag. */
                Bgp4Vpnv4ResetVrfFlags (pOldRoute, u4VrfId,
                                        BGP4_RT_BGP_VRF_BEST);
            }
        }
        else
        {
            /* pNewRoute is new feasible route. */
            if (BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED)
            {
                Bgp4Vpnv4ResetVrfFlags (pNewRoute, u4VrfId,
                                        BGP4_RT_VRF_WITHDRAWN);
                /* If route leaking is enabled . Update the new route to IP */
                Bgp4Vpnv4SetVrfFlags (pNewRoute, u4VrfId,
                                      BGP4_RT_VRF_NEWUPDATE_TOIP);

                if (Bgp4DshGetRouteFromList (pFeasFIBList, pNewRoute) == NULL)
                {
                    Bgp4DshAddRouteToList (pFeasFIBList, pNewRoute, 0);
                }
            }
        }
    }
    else
    {
        /* New route is BGP route. Needs to be updated in the FIB. Add it
         * to the FIB update list. */
        if ((u4VrfFlags & BGP4_RT_VRF_REPLACEMENT) && (pOldRoute != NULL))
        {
#ifdef RRD_WANTED
            /* pNewRoute has replaced the pOldRoute. Since new route is
             * learnt from BGP peer, needs to add this route to the FIB.
             * Since old route is a non-BGP route no need to update FIB/advt
             * this route. */
            Bgp4DshAddRouteToList (pFeasFIBList, pNewRoute, 0);
#else
            if (BGP4_RT_PROTOCOL (pOldRoute) == BGP4_STATIC_ID)
            {
                Bgp4Vpnv4SetVrfFlags (pNewRoute, u4VrfId,
                                      BGP4_RT_VRF_NEWUPDATE_TOIP);
                if (Bgp4DshGetRouteFromList (pFeasFIBList, pNewRoute) == NULL)
                {
                    Bgp4DshAddRouteToList (pFeasFIBList, pNewRoute, 0);
                }

            }
            else
            {
                Bgp4Vpnv4SetVrfFlags (pNewRoute, u4VrfId,
                                      BGP4_RT_VRF_NEWUPDATE_TOIP);
                if (Bgp4DshGetRouteFromList (pFeasFIBList, pNewRoute) == NULL)
                {
                    Bgp4DshAddRouteToList (pFeasFIBList, pNewRoute, 0);
                }
                Bgp4Vpnv4ResetVrfFlags (pOldRoute, u4VrfId,
                                        BGP4_RT_VRF_NEWUPDATE_TOIP);
                if (Bgp4DshGetRouteFromList (pFeasFIBList, pOldRoute) == NULL)
                {
                    Bgp4DshAddRouteToList (pFeasFIBList, pOldRoute, 0);
                }
            }
#endif
        }
        else
        {
            Bgp4Vpnv4SetVrfFlags (pNewRoute, u4VrfId,
                                  BGP4_RT_VRF_NEWUPDATE_TOIP);
            if (Bgp4DshGetRouteFromList (pFeasFIBList, pNewRoute) == NULL)
            {
                /* pNewRoute is new feasible route. */
                Bgp4DshAddRouteToList (pFeasFIBList, pNewRoute, 0);
            }
        }
    }

    return (BGP4_SUCCESS);
}
#endif

/*****************************************************************************/
/* Function Name : Bgp4IgphAddWithdRouteToUpdLists                           */
/* Description   : This function takes the route removed from the tree as    */
/*                 input and add this route to the FIB update list/external  */
/*                 peer advt list and Internal peer advt list as needed.     */
/* Input(s)      : Pointer to the withdrawn route (pRtProfile)               */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4IgphAddWithdRouteToVrfUpdLists (tRouteProfile * pRtProfile)
{
#ifdef L3VPN
    UINT4               u4AsafiMask;

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
    if (u4AsafiMask != CAP_MP_VPN4_UNICAST)
#endif
    {
        Bgp4IgphAddWithdRouteToUpdLists (pRtProfile);
        return BGP4_SUCCESS;
    }

#ifdef L3VPN
    Bgp4RibhAddWithdRtToVpnv4List (pRtProfile);
    return (BGP4_SUCCESS);
#endif
}

/*****************************************************************************/
/* Function Name : Bgp4IgphAddWithdRouteToUpdLists                           */
/* Description   : This function takes the route removed from the tree as    */
/*                 input and add this route to the FIB update list/external  */
/*                 peer advt list and Internal peer advt list as needed.     */
/* Input(s)      : Pointer to the withdrawn route (pRtProfile)               */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4IgphAddWithdRouteToUpdLists (tRouteProfile * pRtProfile)
{

    UINT4               u4AdvtPolicy;
#ifdef VPLSADS_WANTED
    UINT4               u4AsafiMask;

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
#endif

    /* Set the RT_WITHDRAWN flag */
    BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);

    /* Check for the aggregation policy. */
    Bgp4AggrProcessWithdrawnRoute (pRtProfile);

    /* The pRtProfile is removed from the Tree. This route
     * should be advertised to the peer. If the protocol is STATIC
     * and if RRD_WANTED switch is disabled, then STATIC routes
     * can be removed via BGP. So in that case, STATIC routes needs
     * to be removed from FIB. So in that case add the route to the
     * FIB update list. Else if RRD_WANTED is enabled, then no route
     * should be given to FIB for updation. So directly add those
     * route as to the External Peers advertisement list. Irrespective
     * of RRD_WANTED switch is enabled or disabled, the routes should 
     * be added to the Internal Peer update list depanding upon the
     * non-BGP routes import policy. */
    if (BGP4_RT_PROTOCOL (pRtProfile) == BGP4_STATIC_ID)
    {
#ifdef RRD_WANTED
        /* Add the withdrawn route to the Ext-Peer Advt list */
        Bgp4AddWithdRouteToExtPeerAdvtList (pRtProfile);
#else
        /* Add the withdrawn route to the FIB update list */
        Bgp4AddWithdRouteToFIBUpdList (pRtProfile);
#endif
    }
    else
    {

#ifdef VPLSADS_WANTED
        if (u4AsafiMask != CAP_MP_L2VPN_VPLS)
#endif
        {
            /* Add the withdrawn route to the Ext-Peer Advt list */
            Bgp4AddWithdRouteToExtPeerAdvtList (pRtProfile);
            if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
            {
                Bgp4AddWithdRouteToFIBUpdList (pRtProfile);
            }
        }
    }

    /* Check for the Non-BGP routes advertisement policy. If the
     * policy is set as BGP4_EXT_OR_INT_PEER, then add the route to the
     * internal peers advertisement list, else if the policy is
     * BGP4_EXTERNAL_PEER then dont add the routes to internal peers
     * advertisement list. */

    if (BGP4_NON_BGP_EVENT_RCVD (BGP4_RT_CXT_ID (pRtProfile)) != 0)
    {
        u4AdvtPolicy = BGP4_NON_BGP_EVENT_RCVD (BGP4_RT_CXT_ID (pRtProfile));
    }
    else
    {
        u4AdvtPolicy =
            BGP4_NON_BGP_RT_EXPORT_POLICY (BGP4_RT_CXT_ID (pRtProfile));
    }
    if (u4AdvtPolicy == BGP4_EXT_OR_INT_PEER)
    {
        /* Add the withdrawn route to the Internal peers advertisement list */
        Bgp4AddWithdRouteToIntPeerAdvtList (pRtProfile);
    }

    return (BGP4_SUCCESS);

}

/*****************************************************************************/
/* Function Name : Bgp4ImportRouteAdd                                        */
/* Description   : Imports a non-BGP route, and allocate the buffer and      */
/*                 adds the route to Redistribution update list.             */
/* Input(s)      : Prefix of the route to be imported (u4Prefix),            */
/*                 Prefix length of the route (u1Prefixlen),                 */
/*                 Protocol from which imported (u4Protocol),                */
/*                 Nexthop, Tag-info, Metric, Route Interface Index          */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS if the operation is successful,              */
/*                 BGP4_FAILURE if it fails.                                 */
/*****************************************************************************/
INT4
Bgp4ImportRouteAdd (UINT4 u4VrfId, tNetAddress ImportAddress,
                    UINT4 u4Protocol, tAddrPrefix NextHopInfo,
                    UINT4 u4Tag, INT4 i4Metric, UINT4 u4RtIfIndx,
                    UINT1 u1MetricType, UINT1 u1Community,
                    UINT4 *pulCommunity, UINT1 u1AdvFlag, UINT4 u4RmapMetricSet)
{
    tRouteProfile      *pRtProfile = NULL;
    tBgp4Info          *pBGP4Info = NULL;
    UINT4               u4RtProfileCnt = 0;
#ifndef RRD_WANTED
    UINT2               u2Port = 0;
#endif
    UINT2               u2AddrFamily;
#ifdef L3VPN
    INT4                i4RetVal;
    tRouteProfile      *pVpnRtProfile = NULL;
    UINT4               u4Label = 0;
    INT4                i4RetStatus;
#endif
    tCommunity         *pComm = NULL;
    UINT4               u4TmpTag = 0;

    tAddrPrefix         InvPrefix;

    u2AddrFamily =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                      (ImportAddress));
    Bgp4InitAddrPrefixStruct (&(InvPrefix), u2AddrFamily);
    /*Routes in the RIB + Routes in NonSync List + Current Route */
    u4RtProfileCnt =
        BGP4_RIB_ROUTE_CNT (u4VrfId) + (BGP4_NONSYNC_LIST (u4VrfId))->u4_Count +
        1;
    if (u4RtProfileCnt >=
        FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].u4PreAllocatedUnits)
    {
        /* BGP is handling MAX_CONFIGURED route. Need not process this
         * route. */
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4ImportRouteAdd() : BGP HANDLING MAX CONFIGURED "
                  "ROUTES. Route not processed. !!! \n");
        return (BGP4_FAILURE);
    }

    /* In case of default route, the default route will be redistributed only
     * if the default route originate status is enabled. Check this with the
     * changed status value, because it is the latest status. */
    if (((PrefixMatch (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                       (ImportAddress), InvPrefix)) == BGP4_TRUE) &&
        (BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (ImportAddress) == 0) &&
        (BGP4_DEF_ROUTE_ORIG_NEW_STATUS (u4VrfId) ==
         BGP4_DEF_ROUTE_ORIG_DISABLE))
    {

        /* Receive redistributed default route. But the default route
         * origination status is disabled. */
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                  BGP4_FDB_TRC, BGP4_MOD_NAME,
                  "\tRedistribution of default route is received."
                  " But redistribution of default route is disabled\n");
        return (BGP4_SUCCESS);
    }

    pRtProfile = Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
    if (pRtProfile == NULL)
    {
        BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "\tMemory Allocation for Redistributed Route "
                       "\n\t- Dest %s, Mask %s Protocol %d FAILED!!!\n",
                       Bgp4PrintIpAddr (ImportAddress.NetAddr.au1Address,
                                        ImportAddress.NetAddr.u2Afi),
                       Bgp4PrintIpMask ((UINT1) ImportAddress.u2PrefixLen,
                                        ImportAddress.NetAddr.u2Afi),
                       u4Protocol);
        gu4BgpDebugCnt[MAX_BGP_ROUTES_SIZING_ID]++;
        return (BGP4_FAILURE);
    }

    switch (u2AddrFamily)
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4Mask;
            UINT4               u4Addr;
            UINT4               u4NextHop;

            u4Mask = Bgp4GetSubnetmask
                ((UINT1) BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (ImportAddress));
            PTR_FETCH_4 (u4Addr,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                         (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                          (ImportAddress)));
            u4Addr = u4Addr & u4Mask;
            PTR_FETCH_4 (u4NextHop,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (NextHopInfo));
            /* Reject route with local address as nexthop */
#ifndef RRD_WANTED
            if (Bgp4IsLocalAddr (u4NextHop) == BGP4_TRUE)
            {
                BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                               BGP4_FDB_TRC, BGP4_MOD_NAME,
                               "\tRoute %s is not redistributed since nexthop %s "
                               "is a local address\n", u4Addr, u4NextHop);
                return (BGP4_FAILURE);
            }
#endif

            /* Assign the <AFI, SAFI> and prefix/length */
            BGP4_AFI_IN_ADDR_PREFIX_INFO
                (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                 (BGP4_RT_NET_ADDRESS_INFO (pRtProfile))) = u2AddrFamily;
            BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO
                (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                 (BGP4_RT_NET_ADDRESS_INFO (pRtProfile))) =
                BGP4_IPV4_PREFIX_LEN;
            BGP4_SAFI_IN_NET_ADDRESS_INFO (BGP4_RT_NET_ADDRESS_INFO
                                           (pRtProfile)) =
                BGP4_SAFI_IN_NET_ADDRESS_INFO (ImportAddress);
            PTR_ASSIGN_4 (BGP4_RT_IP_PREFIX (pRtProfile), u4Addr);
            BGP4_RT_PREFIXLEN (pRtProfile) =
                BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (ImportAddress);
            if ((u4Protocol == BGP4_LOCAL_ID) || (u4Protocol == BGP4_RIP_ID) ||
                (u4Protocol == BGP4_OSPF_ID) || (u4Protocol == BGP4_ISIS_ID) ||
                (u4Protocol == BGP4_STATIC_ID))
            {
                Bgp4CopyAddrPrefixStruct (&
                                          (BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                           (pRtProfile)), NextHopInfo);
            }
            break;
        }
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
        {
            tIp6Addr            IPAddr;
            tIp6Addr            IPAddr1;
#ifndef RRD_WANTED
            if (u4Protocol != BGP4_LOCAL_ID)
            {
                /* Reject route with local address as nexthop */
                if (Ip6IsLocalAddr
                    (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (NextHopInfo),
                     &u2Port) == IP_SUCCESS)
                {
                BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC |
                                   BGP4_FDB_TRC, BGP4_MOD_NAME,
                                   "\tRoute %s is not redistributed since nexthop %s "
                                   "is a local address\n",
                                   Bgp4PrintIpAddr
                                   (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                                        (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                             (ImportAddress)), u2AddrFamily,
                                        Bgp4PrintIpAddr
                                        (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                                             (NextHopInfo),
                                             BGP4_AFI_IN_ADDR_PREFIX_INFO
                                             (NextHopInfo)));
                                   Bgp4DshReleaseRtInfo (pRtProfile);
                                   return (BGP4_FAILURE);}
                               }
#endif
                               MEMCPY (IPAddr1.u1_addr,
                                       BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                                       (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                        (ImportAddress)), BGP4_IPV6_PREFIX_LEN);
                               Ip6CopyAddrBits (&IPAddr, &IPAddr1,
                                                BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO
                                                (ImportAddress));
                               /* Assign the <AFI, SAFI> and prefix/length */
                               BGP4_AFI_IN_ADDR_PREFIX_INFO
                               (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                (BGP4_RT_NET_ADDRESS_INFO (pRtProfile))) =
                               u2AddrFamily;
                               BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO
                               (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                (BGP4_RT_NET_ADDRESS_INFO (pRtProfile))) =
                               BGP4_IPV6_PREFIX_LEN;
                               BGP4_SAFI_IN_NET_ADDRESS_INFO
                               (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)) =
                               BGP4_SAFI_IN_NET_ADDRESS_INFO (ImportAddress);
                               MEMCPY (BGP4_RT_IP_PREFIX (pRtProfile),
                                       IPAddr.u1_addr, BGP4_IPV6_PREFIX_LEN);
                               BGP4_RT_PREFIXLEN (pRtProfile) =
                               BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO
                               (ImportAddress); break;}
#endif
        default:
                               {
                               /* rightnow there is no support for any other address
                                * family, so return FAILURE
                                */
                               Bgp4DshReleaseRtInfo (pRtProfile);
                               return (BGP4_FAILURE);}
                               }
                               if (NULL != Bgp4GetContextEntry (u4VrfId))
                               {
                               pRtProfile->pBgpCxtNode =
                               Bgp4GetContextEntry (u4VrfId);}
                               BGP4_RT_PROTOCOL (pRtProfile) =
                               (UINT1) u4Protocol;
                               BGP4_RT_METRIC_TYPE (pRtProfile) = u1MetricType;
                               BGP4_RT_GET_RT_IF_INDEX (pRtProfile) =
                               u4RtIfIndx;
                               BGP4_RT_SET_ADV_FLAG (pRtProfile, u1AdvFlag);
                               pBGP4Info =
                               Bgp4MemAllocateBgp4info (sizeof (tBgp4Info));
                               if (pBGP4Info == NULL)
                               {
                               BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG,
                                              BGP4_ALL_FAILURE_TRC |
                                              BGP4_OS_RESOURCE_TRC |
                                              BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                                              "\tMemory Allocation for Redistributed Route Info"
                                              "\n\t- Dest %s, Mask %s Protocol %d FAILED!!!\n",
                                              Bgp4PrintIpAddr (ImportAddress.
                                                               NetAddr.
                                                               au1Address,
                                                               ImportAddress.
                                                               NetAddr.u2Afi),
                                              Bgp4PrintIpMask ((UINT1)
                                                               ImportAddress.
                                                               u2PrefixLen,
                                                               ImportAddress.
                                                               NetAddr.u2Afi),
                                              u4Protocol);
                               Bgp4DshReleaseRtInfo (pRtProfile);
                               gu4BgpDebugCnt
                               [MAX_BGP_ROUTE_INFO_ENTRIES_SIZING_ID]++;
                               return (BGP4_FAILURE);}

                               /* Assign the address family here */
                               BGP4_AFI_IN_ADDR_PREFIX_INFO
                               (BGP4_INFO_NEXTHOP_INFO (pBGP4Info)) =
                               u2AddrFamily;
                               BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO
                               (BGP4_INFO_NEXTHOP_INFO (pBGP4Info)) =
                               BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO (NextHopInfo);
                               BGP4_AFI_IN_ADDR_PREFIX_INFO
                               (BGP4_RT_IMMEDIATE_NEXTHOP_INFO (pRtProfile)) =
                               u2AddrFamily;
                               BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO
                               (BGP4_RT_IMMEDIATE_NEXTHOP_INFO (pRtProfile)) =
                               BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO (NextHopInfo);
                               /* Set Origin Attribute */
                               switch (u4Protocol)
                               {
case BGP4_OSPF_ID:
case BGP4_ISIS_ID:
                               /* Currently set the origin as IGP. If any external AS no is
                                * present in the tag, then the ORIGIN is changed to EGP */
BGP4_INFO_ORIGIN (pBGP4Info) = BGP4_ATTR_ORIGIN_IGP; break; case BGP4_STATIC_ID:
case BGP4_LOCAL_ID:
case BGP4_RIP_ID:
BGP4_INFO_ORIGIN (pBGP4Info) = BGP4_ATTR_ORIGIN_INCOMPLETE; break; default:
                               break;}

                               if ((u4Protocol == BGP4_OSPF_ID)
                                   || (u4Protocol == BGP4_RIP_ID)
                                   || (u4Protocol == BGP4_STATIC_ID)
                                   || (u4Protocol == BGP4_LOCAL_ID))
                               {
                               if (u4Tag > 0)
                               {
                               u4TmpTag = u4Tag;
                               if ((u4TmpTag & RMAP_BGP_AUTO_TAG) ==
                                   RMAP_BGP_AUTO_TAG)
                               {
                               if (Bgp4ExtractInfoFromTag
                                   (u4VrfId, pBGP4Info, u4Tag,
                                    pRtProfile) == BGP4_FAILURE)
                               {
                               BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_ALL_FAILURE_TRC |
                                              BGP4_FDB_TRC, BGP4_MOD_NAME,
                                              "\tFailed in extracting AS information from the tag "
                                              "for the route %s\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               Bgp4DshReleaseBgpInfo (pBGP4Info);
                               Bgp4DshReleaseRtInfo (pRtProfile);
                               return (BGP4_FAILURE);}
                               }
                               }
                               }

                               /* Set Next-Hop Attribute */
                               Bgp4CopyPrefix (&
                                               (BGP4_INFO_NEXTHOP_INFO
                                                (pBGP4Info)), NextHopInfo);
                               /* Update the Received MED field with the received metric value */
                               BGP4_INFO_ATTR_FLAG (pBGP4Info) |=
                               BGP4_ATTR_MED_MASK;
                               BGP4_INFO_ATTR_FLAG (pBGP4Info) |=
                               BGP4_ATTR_NEXT_HOP_MASK;
                               BGP4_INFO_ATTR_FLAG (pBGP4Info) |=
                               BGP4_ATTR_ORIGIN_MASK;
                               BGP4_INFO_RCVD_MED (pBGP4Info) = i4Metric;
                               if (u4RmapMetricSet == METRIC_SET_RMAP)
                               {
                               BGP4_INFO_IS_RMAP_SET (pBGP4Info) = BGP4_TRUE;}
                               BGP4_INFO_WEIGHT (pBGP4Info) =
                               BGP4_WEIGHT_DEFAULT_VALUE;
                               BGP4_LINK_INFO_TO_PROFILE (pBGP4Info,
                                                          pRtProfile);
                               /* Needs to add the route to REDISTRIBUTION update list. */
                               BGP4_RT_SET_FLAG (pRtProfile,
                                                 BGP4_RT_IN_REDIST_LIST);
                               if (BGP4_INFO_COMM_ATTR
                                   (BGP4_RT_BGP_INFO (pRtProfile)) != NULL)
                               {
                               if (BGP4_INFO_COMM_ATTR_VAL
                                   (BGP4_RT_BGP_INFO (pRtProfile)) != NULL)
                               {
                               ATTRIBUTE_NODE_FREE (BGP4_INFO_COMM_ATTR_VAL
                                                    (BGP4_RT_BGP_INFO
                                                     (pRtProfile)));}
                               COMMUNITY_NODE_FREE (BGP4_INFO_COMM_ATTR
                                                    (BGP4_RT_BGP_INFO
                                                     (pRtProfile)));
                               BGP4_INFO_COMM_ATTR_VAL (BGP4_RT_BGP_INFO
                                                        (pRtProfile)) = NULL;}
                               if (u1Community != 0)
                               {

                               COMMUNITY_NODE_CREATE (BGP4_INFO_COMM_ATTR
                                                      (BGP4_RT_BGP_INFO
                                                       (pRtProfile)));
                               pComm =
                               BGP4_INFO_COMM_ATTR (BGP4_RT_BGP_INFO
                                                    (pRtProfile));
                               if (pComm == NULL)
                               {
                               BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                         BGP4_ALL_FAILURE_TRC |
                                         BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                                         BGP4_MOD_NAME,
                                         "\tMemory Allocation for Community Attribute "
                                         "FAILED!!!\n");
                               gu4BgpDebugCnt
                               [MAX_BGP_COMMUNITY_NODE_SIZING_ID]++;
                               return (BGP4_FAILURE);}
                               ATTRIBUTE_NODE_CREATE (pComm->pu1CommVal);
                               if (pComm->pu1CommVal == NULL)
                               {
                               BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                         BGP4_ALL_FAILURE_TRC |
                                         BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                                         BGP4_MOD_NAME,
                                         "\tMemory Allocation for Community Attribute "
                                         "FAILED!!!\n");
                               gu4BgpDebugCnt
                               [MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
                               return (BGP4_FAILURE);}
                               MEMCPY (pComm->pu1CommVal, pulCommunity,
                                       u1Community * 4); pComm->u1Flag = 0;
                               pComm->u2CommCnt = (UINT2) u1Community;
                               BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO
                                                    (pRtProfile)) |=
                               BGP4_ATTR_COMM_MASK;}
                               Bgp4DshAddRouteToList (BGP4_REDIST_LIST
                                                      (u4VrfId), pRtProfile, 0);
                               KW_FALSEPOSITIVE_FIX1 (pRtProfile);
#ifdef L3VPN
                               if (BGP4_VPNV4_AFI_FLAG != BGP4_TRUE)
                               {
                               return (BGP4_SUCCESS);}
                               if ((u4VrfId != BGP4_DFLT_VRFID)
                                   &&
                                   (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId)
                                    == BGP4_L3VPN_VRF_UP))
                               {
                               pVpnRtProfile =
                               Bgp4DupCompleteRtProfile (pRtProfile);
                               if (pVpnRtProfile == NULL)
                               {
                               return (BGP4_FAILURE);}

                               i4RetVal =
                               Bgp4Vpn4ConvertRtToVpn4Route (pVpnRtProfile,
                                                             u4VrfId);
                               if (i4RetVal != BGP4_SUCCESS)
                               {
                               Bgp4DshReleaseBgpInfo (pBGP4Info);
                               Bgp4DshReleaseRtInfo (pVpnRtProfile);
                               return (BGP4_FAILURE);}
                               if ((BGP4_LABEL_ALLOC_POLICY (0) ==
                                    BGP4_PERVRF_LABEL_ALLOC_POLICY) &&
                                   (BGP4_VPNV4_LABEL (u4VrfId) != 0))
                               {
                               u4Label = BGP4_VPNV4_LABEL (u4VrfId);}
                               else
                               if (Bgp4GetBgp4Label (&u4Label) == BGP4_FAILURE)
                               {
                               Bgp4DshReleaseBgpInfo (pBGP4Info);
                               Bgp4DshReleaseRtInfo (pVpnRtProfile);
                               return (BGP4_FAILURE);}
                               BGP4_RT_LABEL_CNT (pVpnRtProfile)++;
                               BGP4_RT_LABEL_INFO (pVpnRtProfile) = u4Label;
                               if (BGP4_LABEL_ALLOC_POLICY (0) ==
                                   BGP4_PERVRF_LABEL_ALLOC_POLICY)
                               {
                               BGP4_VPNV4_LABEL (u4VrfId) = u4Label;}
                               BGP4_RT_GET_RT_IF_INDEX (pVpnRtProfile) =
                               u4RtIfIndx;
                               if (BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED)
                               {
                               /* Scan for other non-Default VRFs present and add the VPNv4
                                * route to the other non-Defualt VRFs Present*/

                               i4RetStatus =
                               Bgp4Vpnv4VrftoVrfRouteLeak (pVpnRtProfile);
                               if (i4RetStatus == BGP4_FAILURE)
                               {
                               Bgp4DshReleaseBgpInfo (pBGP4Info);
                               Bgp4DshReleaseRtInfo (pVpnRtProfile);
                               return BGP4_FAILURE;}
                               }
                               /* Needs to add the route to REDISTRIBUTION update list. */
                               BGP4_RT_SET_FLAG (pVpnRtProfile,
                                                 BGP4_RT_IN_REDIST_LIST);
                               Bgp4DshAddRouteToList (BGP4_REDIST_LIST
                                                      (u4VrfId), pVpnRtProfile,
                                                      0);
                               KW_FALSEPOSITIVE_FIX1 (pVpnRtProfile);}
#endif
                               BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC, BGP4_MOD_NAME,
                                              "\tThe route %s is successfully added to the redistriution list\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               return (BGP4_SUCCESS);}

/*****************************************************************************/
/* Function Name : Bgp4ImportRouteDelete                                     */
/* Description   : Checks whether the route is present in the Route list. If */
/*                 yes, then add the route to Redistribution list as         */
/*                 withdrawn route.                                          */
/* Input(s)      : Prefix of the route to be deleted (u4Prefix),             */
/*                 Prefix length of the route (u1Prefixlen),                 */
/*                 Protocol from which imported (u4Protocol),                */
/*                 Nexthop, Route Interface Index  associated with           */
/*                 the route.                                                */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS if the operation is successful,              */
/*                 BGP4_FAILURE if it fails.                                 */
/*****************************************************************************/
                               INT4
                               Bgp4ImportRouteDelete (UINT4 u4VrfId,
                                                      tNetAddress ImportAddress,
                                                      UINT4 u4Protocol,
                                                      tAddrPrefix NextHopInfo,
                                                      UINT4 u4RtIfIndx,
                                                      UINT1 u1AdvFlag)
                               {
                               tRouteProfile * pRtProfile = NULL;
                               tRouteProfile TempRtProfile;
#ifdef L3VPN
                               INT4 i4RetVal = BGP4_FAILURE;
                               tRouteProfile * pVpnRtProfile = NULL;
                               INT4 i4RetStatus;
#endif
                               UNUSED_PARAM (u4RtIfIndx);
                               MEMSET (&(TempRtProfile), 0,
                                       sizeof (tRouteProfile));
                               /* Static RouteProfile is used for deletion of the route , when
                                * delete is called from RTM.The same RouteProfile is overwritten
                                * whenever delete call is invoked, instead of MEM allocation from
                                * the pool. */
                               pRtProfile = &TempRtProfile;
                               BGP4_RT_SET_ADV_FLAG (pRtProfile, u1AdvFlag);
                               BGP4_RT_PROTOCOL (pRtProfile) =
                               (UINT1) u4Protocol;
                               Bgp4CopyNetAddressStruct (&
                                                         (BGP4_RT_NET_ADDRESS_INFO
                                                          (pRtProfile)),
                                                         ImportAddress);
                               if ((u4Protocol == BGP4_LOCAL_ID)
                                   || (u4Protocol == BGP4_RIP_ID)
                                   || (u4Protocol == BGP4_OSPF_ID)
                                   || (u4Protocol == BGP4_ISIS_ID)
                                   || (u4Protocol == BGP4_STATIC_ID))
                               {
                               Bgp4CopyAddrPrefixStruct (&
                                                         (BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                                          (pRtProfile)),
                                                         NextHopInfo);}
                               BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);
                               BGP4_RT_SET_FLAG (pRtProfile,
                                                 BGP4_RT_IN_REDIST_LIST);
                               pRtProfile->pBgpCxtNode =
                               Bgp4GetContextEntry (u4VrfId);
                               Bgp4IgphProcessWithdrawnImportRoute (pRtProfile);
                               KW_FALSEPOSITIVE_FIX (pRtProfile);
#ifdef L3VPN
                               if (BGP4_VPNV4_AFI_FLAG != BGP4_TRUE)
                               {
                               return (BGP4_SUCCESS);}
                               if ((u4VrfId != BGP4_DFLT_VRFID)
                                   &&
                                   (BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId)
                                    == BGP4_L3VPN_VRF_UP))
                               {
                               pVpnRtProfile =
                               Bgp4MemAllocateRouteProfile (sizeof
                                                            (tRouteProfile));
                               if (pVpnRtProfile == NULL)
                               {
                               BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                         BGP4_ALL_FAILURE_TRC |
                                         BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                         "\tAllocate L3VPN Route Profile for Unfeasible Redistribute "
                                         "route FAILS!!!\n");
                               gu4BgpDebugCnt[MAX_BGP_ROUTES_SIZING_ID]++;
                               return (BGP4_FAILURE);}
                               BGP4_RT_PROTOCOL (pVpnRtProfile) =
                               (UINT1) u4Protocol;
                               Bgp4CopyNetAddressStruct (&
                                                         (BGP4_RT_NET_ADDRESS_INFO
                                                          (pVpnRtProfile)),
                                                         ImportAddress);
                               BGP4_RT_SET_FLAG (pVpnRtProfile,
                                                 BGP4_RT_WITHDRAWN);
                               BGP4_RT_SET_FLAG (pVpnRtProfile,
                                                 BGP4_RT_IN_REDIST_LIST);
                               i4RetVal =
                               Bgp4Vpn4ConvertRtToVpn4Route (pVpnRtProfile,
                                                             u4VrfId);
                               if (i4RetVal != BGP4_SUCCESS)
                               {
                               Bgp4DshReleaseRtInfo (pVpnRtProfile);
                               BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                         BGP4_ALL_FAILURE_TRC |
                                         BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                         "\tConverting Vpnv4 Redistribute route to Vpnv4 "
                                         "route FAILS!!!\n");
                               return (BGP4_FAILURE);}

                               /* If VPN route leaking is enabled */
                               if (BGP4_ROUTE_LEAK_FLAG == BGP4_CLI_AFI_ENABLED)
                               {
                               /* Scan for Non-Default VRFs present and delete the VPNv4
                                * Routes leaked to Deleted */
                               i4RetStatus =
                               Bgp4Vpnv4VrftoVrfRouteLeak (pVpnRtProfile);
                               if (i4RetStatus == BGP4_FAILURE)
                               {
                               BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                         BGP4_ALL_FAILURE_TRC |
                                         BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                         "\tDeleting the Leaked route-FAILS\n");
                               return BGP4_FAILURE;}
                               }
                               /* Needs to add the route to REDISTRIBUTION update list. */
                               Bgp4DshAddRouteToList (BGP4_REDIST_LIST
                                                      (u4VrfId), pVpnRtProfile,
                                                      0);
                               KW_FALSEPOSITIVE_FIX1 (pVpnRtProfile);}
#endif
                               return (BGP4_SUCCESS);}

/*****************************************************************************/
/* Function Name : Bgp4IgphProcessFeasibleImportRoute                        */
/* Description   : This routine process the feasible import routes. Adds the */
/*                 feasible route to the tree. If this route is selected as  */
/*                 best route then this route will be added to the internal  */
/*                 and external peers advertisement list accordingly.        */
/* Input(s)      : pointer to the feasible Route (pRtProfile)                */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS if the operation is successful,              */
/*                 BGP4_FAILURE if it fails.                                 */
/*****************************************************************************/
                               INT4
                               Bgp4IgphProcessFeasibleImportRoute (tRouteProfile
                                                                   * pRtProfile)
                               {
                               tRouteProfile * pBestRoute = NULL;
                               tRouteProfile * pNewBestRoute = NULL;
                               tRouteProfile * pRtList = NULL;
                               tRouteProfile * pRoute = NULL;
                               tRouteProfile * pNewRtProfile = NULL;
                               tBgp4Info * pBGP4Info = NULL;
                               tTMO_SLL TsWithdrawnRouteList;
                               tLinkNode * pLinkNode = NULL;
                               VOID *pRibNode = NULL;
                               tRouteProfile * pFndRtProfileList = NULL;
                               tRouteProfile * pTmpFndRtProfileList = NULL;
                               UINT4 u4OldMED = 0;
#if defined(L3VPN) || defined (VPLSADS_WANTED)
                               UINT4 u4AsafiMask;
#endif
                               INT4 i4RtFound = BGP4_FAILURE;
                               INT4 i4ProtocolId = 0;
                               BOOL1 bIsIdenticalRoute = BGP4_FALSE;
                               BOOL1 bIsDftMEDChg = BGP4_FALSE;
                               INT4 i4RetVal = 0;
                               TMO_SLL_Init (&TsWithdrawnRouteList);
                               pBGP4Info = BGP4_RT_BGP_INFO (pRtProfile);
#if defined(L3VPN) || defined (VPLSADS_WANTED)
                               BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO
                                                      (pRtProfile),
                                                      BGP4_RT_SAFI_INFO
                                                      (pRtProfile),
                                                      u4AsafiMask);
#endif
#ifdef L3VPN
                               /* Check whethe the route can be installed.
                                * If the route is destined to some VRF which is in meantime deleted,
                                * so there is no point in adding the route here
                                */
                               if ((u4AsafiMask == CAP_MP_VPN4_UNICAST) &&
                                   (BGP4_RT_CXT_ID (pRtProfile) != 0) &&
                                   (BGP4_VPN4_VRF_SPEC_CURRENT_STATE
                                    (BGP4_RT_CXT_ID (pRtProfile)) !=
                                    BGP4_L3VPN_VRF_UP))
                               {
                               return BGP4_SUCCESS;}
                               /* These are redistributed routes. So donot verify processing rules here */
#endif
                               /* If the Default IGP metric is configured, then set the MED value
                                * as the configured value. Else fill the MED value as the received
                                * metric value. For Direct routes, always the received Metric will
                                * be used. Also check whether the route needs to be handled for the
                                * change in the Default metric value. */

                               if ((BGP4_RT_PROTOCOL (pRtProfile) !=
                                    BGP4_LOCAL_ID)
                                   || (BGP4_RT_BGP_MED_CHANGE (pRtProfile) ==
                                       BGP4_TRUE))
                               {
                               if (BGP4_DEFAULT_IGP_METRIC
                                   (BGP4_RT_CXT_ID (pRtProfile)) ==
                                   BGP4_DEF_IGP_METRIC_VAL)
                               {
                               /* Default MED not configured. Check if the BGP4_RT_MED have
                                * value other than default value or received METRIC value.
                                * If yes, then the route needs to be processed for change
                                * in Default MED. */
                               if ((BGP4_RT_MED (pRtProfile) != BGP4_INV_MEDLP)
                                   && (BGP4_RT_MED (pRtProfile) !=
                                       BGP4_INFO_RCVD_MED (BGP4_RT_BGP_INFO
                                                           (pRtProfile)))
                                   &&
                                   (BGP4_RT_BGP_STATIC_MED_CHANGE (pRtProfile)
                                    != BGP4_TRUE))
                               {
                               bIsDftMEDChg = BGP4_TRUE;
                               u4OldMED = BGP4_RT_MED (pRtProfile);}
                               }
                               else
                               {
                               /* Default MED configured. Check if the BGP4_RT_MED have
                                * value other than default MED value. If yes, then the route needs
                                * to be processed for change in Default MED. */
                               if ((BGP4_RT_MED (pRtProfile) !=
                                    BGP4_DEFAULT_IGP_METRIC (BGP4_RT_CXT_ID
                                                             (pRtProfile)))
                                   &&
                                   (BGP4_RT_BGP_STATIC_MED_CHANGE (pRtProfile)
                                    != BGP4_TRUE))
                               {
                               bIsDftMEDChg = BGP4_TRUE;
                               u4OldMED = BGP4_RT_MED (pRtProfile);}
                               }
                               }
                               else
                               {
                               if (BGP4_GET_NODE_STATUS == RM_STANDBY)
                               {
                               /* In Standby Node for direct routes metric value change also
                                  needs to be handled */
                               if (BGP4_DEFAULT_IGP_METRIC
                                   (BGP4_RT_CXT_ID (pRtProfile)) ==
                                   BGP4_DEF_IGP_METRIC_VAL)
                               {
                               /* Default MED not configured. Check if the BGP4_RT_MED have
                                * value other than default value or received METRIC value.
                                * If yes, then the route needs to be processed for change
                                * in Default MED. */
                               if ((BGP4_RT_MED (pRtProfile) != BGP4_INV_MEDLP)
                                   && (BGP4_RT_MED (pRtProfile) !=
                                       BGP4_INFO_RCVD_MED (BGP4_RT_BGP_INFO
                                                           (pRtProfile))))
                               {
                               bIsDftMEDChg = BGP4_TRUE;
                               u4OldMED = BGP4_RT_MED (pRtProfile);}
                               }
                               else
                               {
                               /* Default MED configured. Check if the BGP4_RT_MED have
                                * value other than default MED value. If yes, then the route needs
                                * to be processed for change in Default MED. */
                               if ((BGP4_RT_MED (pRtProfile) !=
                                    BGP4_DEFAULT_IGP_METRIC (BGP4_RT_CXT_ID
                                                             (pRtProfile))))
                               {
                               bIsDftMEDChg = BGP4_TRUE;
                               u4OldMED = BGP4_RT_MED (pRtProfile);}
                               }
                               }
                               }
                               BGP4_DBG1 (BGP4_DBG_CTRL_FLOW,
                                          "\rValue of OldMED is %d\n",
                                          u4OldMED);
                               if (BGP4_RT_PROTOCOL (pRtProfile) ==
                                   BGP4_STATIC_ID)
                               {
                               i4ProtocolId = BGP4_STATIC_METRIC;}
                               else
                               if (BGP4_RT_PROTOCOL (pRtProfile) ==
                                   BGP4_LOCAL_ID)
                               {
                               i4ProtocolId = BGP4_DIRECT_METRIC;}
                               else
                               if (BGP4_RT_PROTOCOL (pRtProfile) == BGP4_RIP_ID)
                               {
                               i4ProtocolId = BGP4_RIP_METRIC;}
                               else
                               if (BGP4_RT_PROTOCOL (pRtProfile) ==
                                   BGP4_OSPF_ID)
                               {
                               i4ProtocolId = BGP4_OSPF_METRIC;}
                               else
                               if (BGP4_RT_PROTOCOL (pRtProfile) ==
                                   BGP4_ISIS_ID)
                               {
                               i4ProtocolId = BGP4_ISIS_METRIC;}
                               if (BGP4_GET_NODE_STATUS != RM_STANDBY)
                               {
                               if ((BGP4_RT_PROTOCOL (pRtProfile) !=
                                    BGP4_LOCAL_ID)
                                   &&
                                   ((BGP4_DEFAULT_IGP_METRIC
                                     (BGP4_RT_CXT_ID (pRtProfile)) !=
                                     BGP4_DEF_IGP_METRIC_VAL)
                                    &&
                                    ((BGP4_RRD_METRIC_MASK
                                      (BGP4_RT_CXT_ID (pRtProfile)) &
                                      i4ProtocolId) != i4ProtocolId))
                                   && (BGP4_INFO_IS_RMAP_SET (pBGP4Info) !=
                                       BGP4_TRUE))
                               {
                               BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC, BGP4_MOD_NAME,
                                              "\tDefault MED has configured for the route %s."
                                              " Updating the default metric value %d as MED value of route\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)),
                                              BGP4_DEFAULT_IGP_METRIC
                                              (BGP4_RT_CXT_ID (pRtProfile)));
                               BGP4_RT_MED (pRtProfile) =
                               BGP4_DEFAULT_IGP_METRIC (BGP4_RT_CXT_ID
                                                        (pRtProfile));}
                               else
                               {
                               BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC, BGP4_MOD_NAME,
                                              "\tDefault MED not configured for the route %s."
                                              " Updating the received MED value %d as MED value of route\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)),
                                              BGP4_INFO_RCVD_MED (pBGP4Info));
                               BGP4_RT_MED (pRtProfile) =
                               BGP4_INFO_RCVD_MED (pBGP4Info);}
                               }
                               if ((BGP4_RT_GET_ADV_FLAG (pRtProfile) &
                                    BGP4_NETWORK_ADV_ROUTE) ==
                                   BGP4_NETWORK_ADV_ROUTE)
                               {
                               BGP4_INFO_ORIGIN (pBGP4Info) =
                               BGP4_ATTR_ORIGIN_IGP;}

    /* Update the immediate next hop value. */
#ifdef VPLSADS_WANTED
    /* next hop is not known */
                               if ((u4AsafiMask != CAP_MP_L2VPN_VPLS))
#endif
                               {
                               Bgp4CopyAddrPrefixStruct (&
                                                         (BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                                          (pRtProfile)),
                                                         BGP4_INFO_NEXTHOP_INFO
                                                         (pBGP4Info));}

                               i4RtFound =
                               Bgp4RibhLookupRtEntry (pRtProfile,
                                                      BGP4_RT_CXT_ID
                                                      (pRtProfile),
                                                      BGP4_TREE_FIND_EXACT,
                                                      &pFndRtProfileList,
                                                      &pRibNode);
                               if (i4RtFound == BGP4_SUCCESS)
                               {
                               pBestRoute =
                               Bgp4DshGetBestRouteFromProfileList
                               (pFndRtProfileList, BGP4_RT_CXT_ID (pRtProfile));
                               if (pBestRoute != NULL)
                               {
                               BGP4_RT_REF_COUNT (pBestRoute)++;}

        /* Check for replacement route should be done. */
                               pRtList = pFndRtProfileList;
                               while (pRtList != NULL)
                               {
            /* Is it the replacement route learnt via redistribution. */
                               if ((BGP4_RT_PROTOCOL (pRtList) != BGP_ID) &&
                                   (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID))
                               {
                /* Check if route is not identical to old-route */
                               if ((bIsDftMEDChg == BGP4_TRUE)
                                   && (pRtList->u4MED != u4OldMED))
                               {
                               /* Need not verify whether route is identical
                                * to old-route as only metric value is changed */
                               break;}

                /* Check if route is not identical to old-route */
                               if ((Bgp4AttrIsBgpinfosIdenticalImport
                                    (pBGP4Info, BGP4_RT_BGP_INFO (pRtList),
                                     BGP4_RT_GET_ADV_FLAG (pRtList),
                                     BGP4_RT_GET_ADV_FLAG (pRtProfile)) !=
                                    TRUE))
                               {
                               /* Need Not Break the loop. Checking for any other
                                * Identical route */
                               break;}
                               else
                               {
                               /* Both new and old routes are identical */
                               bIsIdenticalRoute = BGP4_TRUE; break;}
                               }
                               pRtList = BGP4_RT_NEXT (pRtList);}
                               }

                               if ((i4RtFound == BGP4_SUCCESS)
                                   && (pRtList != NULL))
                               {
                               if ((BGP4_RT_GET_ADV_FLAG (pRtProfile) &
                                    BGP4_NETWORK_ADV_ROUTE) ==
                                   BGP4_NETWORK_ADV_ROUTE)
                               {
                               if ((BGP4_RT_GET_ADV_FLAG (pRtList) &
                                    BGP4_REDIST_ADV_ROUTE) ==
                                   BGP4_REDIST_ADV_ROUTE)
                               {
                               Bgp4RibhDelRtEntry (pRtList, pRibNode,
                                                   BGP4_RT_CXT_ID (pRtProfile),
                                                   BGP4_FALSE);
                               BGP4_RT_SET_ADV_FLAG (pRtProfile,
                                                     (BGP4_NETWORK_ADV_ROUTE |
                                                      BGP4_REDIST_ADV_ROUTE));
                               i4RetVal =
                               Bgp4RibhAddRtEntry (pRtProfile,
                                                   BGP4_RT_CXT_ID (pRtProfile));
                               Bgp4IgphAddFeasibleRouteToVrfUpdLists
                               (pRtProfile, NULL); if (pBestRoute != NULL)
                               {
                               Bgp4DshReleaseRtInfo (pBestRoute);}
                               UNUSED_PARAM (i4RetVal); return BGP4_SUCCESS;}
                               }
                               else
                               if ((BGP4_RT_GET_ADV_FLAG (pRtProfile) &
                                    BGP4_REDIST_ADV_ROUTE) ==
                                   BGP4_REDIST_ADV_ROUTE)
                               {
                               if ((BGP4_RT_GET_ADV_FLAG (pRtList) &
                                    BGP4_NETWORK_ADV_ROUTE) ==
                                   BGP4_NETWORK_ADV_ROUTE)
                               {
                               BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC, BGP4_MOD_NAME,
                                              "\tThe route %s is advertised via network command. "
                                              " Nothing is done with route.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               BGP4_RT_SET_ADV_FLAG (pRtList,
                                                     BGP4_REDIST_ADV_ROUTE);
                               /* if already the same route is advertised via netwrok command
                                  then do nothing */
                               return BGP4_SUCCESS;}

                               }
                               }

                               if (BGP4_OVERLAP_ROUTE_POLICY
                                   (BGP4_RT_CXT_ID (pRtProfile)) !=
                                   BGP4_INSTALL_BOTH_RT)
                               {
                               if ((bIsIdenticalRoute == BGP4_TRUE)
                                   || (bIsDftMEDChg == BGP4_TRUE))
                               {
            /* Either New Route is Identical to Old Route or the Default metric
             * configuration has been changed. Either case no need for
             * processing the Overlap Route. */
                               }
                               else
                               {
                               if (BGP4_RT_PROTOCOL (pRtProfile) !=
                                   BGP4_LOCAL_ID)
                               {
                               Bgp4RibhProcessOverlapFeasRoutes (pRtProfile,
                                                                 &pTmpFndRtProfileList,
                                                                 &TsWithdrawnRouteList);
                               /* TsWithdrawnRouteList contains the list of best route that
                                * are removed from the tree because of the new feasible
                                * route. These route needs to be withdrawn from FIB and
                                * sent to Peers as withdrawn routes. */
                               TMO_SLL_Scan (&TsWithdrawnRouteList, pLinkNode,
                                             tLinkNode *)
                               {
                               pRoute = pLinkNode->pRouteProfile;
                               Bgp4IgphAddWithdRouteToVrfUpdLists (pRoute);
                               BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC | BGP4_EVENTS_TRC,
                                              BGP4_MOD_NAME,
                                              "\tThe route %s is removed from the tree, need to be "
                                              " withdrawn from FIB and sent to Peers as withdrawn route.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRoute),
                                                               BGP4_RT_AFI_INFO
                                                               (pRoute)));
                               /* Since the old best routes have become Non-Best route,
                                * update the RCVD_PA_ENTRY accordingly. */
                               Bgp4RcvdPADBAddRoute (pRoute, BGP4_FALSE);}
                               Bgp4DshReleaseList (&TsWithdrawnRouteList, 0);}

                               if ((pRtProfile->u4Flags & BGP4_RT_OVERLAP) ==
                                   BGP4_RT_OVERLAP)
                               {
                               BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC, BGP4_MOD_NAME,
                                              "\tOverlap policy is applied for the received new "
                                              "route %s mask %s.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)),
                                              Bgp4PrintIpMask ((UINT1)
                                                               BGP4_RT_PREFIXLEN
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               if (pFndRtProfileList != NULL)
                               {
                               /* Some route with same prefix exists in Tree. Check
                                * whether the route from same protocol exists or not.
                                * If yes, then delete the old route. */
                               pRtList = pFndRtProfileList;
                               while (pRtList != NULL)
                               {
                               if ((BGP4_RT_PROTOCOL (pRtList) != BGP_ID) &&
                                   (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID))
                               {
                               /* Replacement route learnt via Redistribution.
                                * Delete the old route. */
                               if ((BGP4_RT_GET_FLAGS (pRtList) &
                                    BGP4_RT_OVERLAP) == BGP4_RT_OVERLAP)
                               {
                               Bgp4RibhDelRtEntry (pRtList, pRibNode,
                                                   BGP4_RT_CXT_ID (pRtProfile),
                                                   BGP4_TRUE);
                               BGP4_RT_RESET_FLAG (pRtList, BGP4_RT_OVERLAP);
                               Bgp4DshRemoveRouteFromList
                               (BGP4_OVERLAP_ROUTE_LIST
                                (BGP4_RT_CXT_ID (pRtProfile)), pRtList);}
                               else
                               {
                               Bgp4RibhDelRtEntry (pRtList, pRibNode,
                                                   BGP4_RT_CXT_ID (pRtProfile),
                                                   BGP4_FALSE);}
                               BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC | BGP4_EVENTS_TRC,
                                              BGP4_MOD_NAME,
                                              "\tReplacement route received via redistribution. Hence "
                                              "deleting the existing route %s mask %s from RIB.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtList),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtList)),
                                              Bgp4PrintIpMask ((UINT1)
                                                               BGP4_RT_PREFIXLEN
                                                               (pRtList),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtList)));
                               break;}
                               else
                               {
                               pRtList = BGP4_RT_NEXT (pRtList);}
                               }
                               }

                /* Add the new route to the RIB and Overlap route list. */
                               BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC | BGP4_EVENTS_TRC,
                                              BGP4_MOD_NAME,
                                              "\tAdding the received new route %s mask %s to RIB and "
                                              "overlap list.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)),
                                              Bgp4PrintIpMask ((UINT1)
                                                               BGP4_RT_PREFIXLEN
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               Bgp4RibhAddRtEntry (pRtProfile,
                                                   BGP4_RT_CXT_ID (pRtProfile));
                               Bgp4DshAddRouteToOverlapList (pRtProfile);
                               if (pBestRoute != NULL)
                               {
                               Bgp4DshReleaseRtInfo (pBestRoute);}
                               return BGP4_SUCCESS;}
                               }
                               }

                               if (pRtList != NULL)
                               {
                               if ((BGP4_RT_PROTOCOL (pRtProfile) ==
                                    BGP4_LOCAL_ID))
                               {
                    /* For direct routes if Metric is changed for an existing route
                     * it needs to be updated in the PeerAdvList */
                               if (BGP4_RT_BGP_MED_CHANGE (pRtProfile) ==
                                   BGP4_TRUE)
                               {
                               BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC, BGP4_MOD_NAME,
                                              "\tMetric is changed for the local route %s. Updating "
                                              " metric change %d by adding to peer advertisement list.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)),
                                              pRtProfile->u4MED);
                               BGP4_RT_MED (pRtList) = pRtProfile->u4MED;
                               BGP4_RT_BGP_MED_CHANGE (pRtProfile) = ZERO;
                               Bgp4AddFeasRouteToExtPeerAdvtList (pRtList,
                                                                  NULL);
                               Bgp4AddFeasRouteToIntPeerAdvtList (pRtList,
                                                                  NULL);
                               return BGP4_SUCCESS;}
                               else
                               {
                               return BGP4_SUCCESS;}
                               }
                               if (((bIsDftMEDChg == BGP4_TRUE)
                                    && (pRtProfile == pRtList))
                                   || ((bIsDftMEDChg == BGP4_TRUE)
                                       && (pRtProfile->u4MED != u4OldMED)))
                               {
            /* Both New Best Route and Old Best Route are same. The Default
             * Metric has changed. Need to advertise this route as new
             * feasible route. Metric value is changed so update the 
             * metric in the pRtList before updating to PeerAdvList */
                               BGP4_RT_MED (pRtList) = pRtProfile->u4MED;
                               if (BGP4_RT_BGP_MED_CHANGE (pRtProfile) ==
                                   BGP4_TRUE)
                               {
                               BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC, BGP4_MOD_NAME,
                                              "\tMetric value %d is changed for the received new feasible route %s. "
                                              "Updating the metric change by adding to peer advertisement list.\n",
                                              pRtProfile->u4MED,
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               BGP4_RT_BGP_MED_CHANGE (pRtProfile) = ZERO;}

                               Bgp4RedSyncRouteInfo (pRtProfile);
                               if (BGP4_IS_ROUTE_VALID (pRtList) == BGP4_TRUE)
                               {
#ifdef RRD_WANTED
                               if ((BGP4_RT_GET_FLAGS (pRtList) &
                                    BGP4_RT_IN_EXT_PEER_ADVT_LIST) !=
                                   BGP4_RT_IN_EXT_PEER_ADVT_LIST)
                               {
#ifdef L3VPN
                               if (u4AsafiMask != CAP_MP_VPN4_UNICAST)
#endif
#ifdef VPLSADS_WANTED
                               if (u4AsafiMask != CAP_MP_L2VPN_VPLS)
#endif
                               {
                               Bgp4AddFeasRouteToExtPeerAdvtList (pRtList,
                                                                  NULL);}
                               }
#ifdef L3VPN
                               else
                               {
                               BGP4_RT_RESET_FLAG (pRtList,
                                                   BGP4_RT_IN_EXT_PEER_ADVT_LIST);
                               Bgp4DshDelinkRouteFromList
                               (BGP4_EXT_PEERS_ADVT_LIST (BGP4_DFLT_VRFID),
                                pRtList, BGP4_EXT_PEER_LIST_INDEX);}
#endif
#else
#ifdef VPLSADS_WANTED
                               if (u4AsafiMask != CAP_MP_L2VPN_VPLS)
#endif
                               {
                               if (((BGP4_RT_GET_FLAGS (pRtList) &
                                     BGP4_RT_IN_FIB_UPD_LIST) !=
                                    BGP4_RT_IN_FIB_UPD_LIST))
                               {
                               UINT4 u4NewMED = 0;
                               /* Route is not in FIB list. Means FDB has already been
                                * updated with this route. Need to remove the old route
                                * from FDB and add this route again. */
                               u4NewMED = BGP4_RT_MED (pRtList);
                               BGP4_RT_MED (pRtList) = pRtProfile->u4MED;
                               BGP4_RT_SET_FLAG (pRtList, (BGP4_RT_WITHDRAWN |
                                                           BGP4_RT_ADVT_NOTTO_EXTERNAL));
#ifdef L3VPN
                               if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
                               {
                               Bgp4Vpnv4SetFlagsForAllVrf (pRtList,
                                                           (BGP4_RT_VRF_WITHDRAWN
                                                            |
                                                            BGP4_RT_VRF_ADVT_NOTTO_EXTERNAL));}
#endif
                               BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC | BGP4_EVENTS_TRC,
                                              BGP4_MOD_NAME,
                                              "\tAdding the new feasible route %s mask %s to FIB "
                                              "with MED change.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtList),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtList)),
                                              Bgp4PrintIpMask ((UINT1)
                                                               BGP4_RT_PREFIXLEN
                                                               (pRtList),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtList)));
                               Bgp4IgphUpdateRouteInFIB (pRtList);
#ifdef L3VPN
                               if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
                               {
                               Bgp4Vpnv4ResetFlagsForAllVrf (pRtList,
                                                             (BGP4_RT_VRF_WITHDRAWN
                                                              |
                                                              BGP4_RT_VRF_ADVT_NOTTO_EXTERNAL));}
#endif
                               BGP4_RT_RESET_FLAG (pRtList, (BGP4_RT_WITHDRAWN |
                                                             BGP4_RT_ADVT_NOTTO_EXTERNAL));
                               BGP4_RT_MED (pRtList) = u4NewMED;
                               /* Check whether the route is present in Ext-Peer Advt list.
                                * If yes, then remove that route. */
                               if ((BGP4_RT_GET_FLAGS (pRtList) &
                                    BGP4_RT_IN_EXT_PEER_ADVT_LIST) ==
                                   BGP4_RT_IN_EXT_PEER_ADVT_LIST)
                               {
                               BGP4_RT_RESET_FLAG (pRtList,
                                                   BGP4_RT_IN_EXT_PEER_ADVT_LIST);
                               Bgp4DshDelinkRouteFromList
                               (BGP4_EXT_PEERS_ADVT_LIST
                                (BGP4_RT_CXT_ID (pRtProfile)), pRtList,
                                BGP4_EXT_PEER_LIST_INDEX);}

                               /* Add the Route to the FIB update list. */
#ifdef L3VPN
                               if (u4AsafiMask != CAP_MP_VPN4_UNICAST)
#endif
                               {
                               BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC | BGP4_EVENTS_TRC,
                                              BGP4_MOD_NAME,
                                              "\tAdding the new feasible route %s mask %s to FIB "
                                              "update list with MED change.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtList),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtList)),
                                              Bgp4PrintIpMask ((UINT1)
                                                               BGP4_RT_PREFIXLEN
                                                               (pRtList),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtList)));
                               Bgp4AddFeasRouteToFIBUpdList (pRtList, NULL);}
                               }
                               }
#endif
                               if (BGP4_EXT_OR_INT_PEER ==
                                   ((BGP4_NON_BGP_EVENT_RCVD
                                     (BGP4_RT_CXT_ID (pRtProfile)) !=
                                     0) ?
                                    BGP4_NON_BGP_EVENT_RCVD (BGP4_RT_CXT_ID
                                                             (pRtProfile)) :
                                    BGP4_NON_BGP_RT_EXPORT_POLICY
                                    (BGP4_RT_CXT_ID (pRtProfile))))
                               {

                               if ((BGP4_RT_GET_FLAGS (pRtList) &
                                    BGP4_RT_IN_INT_PEER_ADVT_LIST) !=
                                   BGP4_RT_IN_INT_PEER_ADVT_LIST)
                               {
#ifdef L3VPN
                               if (u4AsafiMask != CAP_MP_VPN4_UNICAST)
#endif
                               {
                               /* NON-BGP route can be advertised to Internal
                                * Peer */
                               BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC, BGP4_MOD_NAME,
                                              "\tThe Received new route is a NON-BGP rotue %s. "
                                              "Adding the route to internal peer advertisement list.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtList),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtList)));
                               Bgp4AddFeasRouteToIntPeerAdvtList (pRtList,
                                                                  NULL);}
                               }
                               else
                               {
                               BGP4_RT_RESET_FLAG (pRtList,
                                                   BGP4_RT_IN_INT_PEER_ADVT_LIST);
                               Bgp4DshDelinkRouteFromList
                               (BGP4_INT_PEERS_ADVT_LIST
                                (BGP4_RT_CXT_ID (pRtProfile)), pRtList,
                                BGP4_INT_PEER_LIST_INDEX);}
                               }
#ifdef L3VPN
                               if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
                               {
                               Bgp4IgphAddFeasibleRouteToVrfUpdLists (pRtList,
                                                                      0);}
#endif
                               }
                               }
       /* It is a replacement route/Identical route.  */
                               else
                               if (bIsIdenticalRoute != BGP4_TRUE)
                               {
            /* Replacement route received. Remove the old route and
             * add the new route
             */
                               BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC | BGP4_EVENTS_TRC,
                                              BGP4_MOD_NAME,
                                              "\tReplacement route is received. Removing the old route"
                                              "from RIB and adding the new route %s mask %s to RIB.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)),
                                              Bgp4PrintIpMask ((UINT1)
                                                               BGP4_RT_PREFIXLEN
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               Bgp4RibhDelRtEntry (pRtList, pRibNode,
                                                   BGP4_RT_CXT_ID (pRtProfile),
                                                   BGP4_FALSE);
                               if ((Bgp4RibhAddRtEntry
                                    (pRtProfile,
                                     BGP4_RT_CXT_ID (pRtProfile))) ==
                                   BGP4_FAILURE)
                               {
                /* Adding route to Tree Fails. Dont return. May be we need
                 * to send the Old route as Withdrawn. */
                               BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC | BGP4_EVENTS_TRC |
                                              BGP4_ALL_FAILURE_TRC,
                                              BGP4_MOD_NAME,
                                              "\tAdding the route %s mask %s to RIB failed.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)),
                                              Bgp4PrintIpMask ((UINT1)
                                                               BGP4_RT_PREFIXLEN
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));}
                               }
                               else
                               if ((bIsIdenticalRoute == BGP4_TRUE) &&
                                   (BGP4_RT_BGP_STATIC_MED_CHANGE (pRtProfile)
                                    == BGP4_TRUE)
                                   &&
                                   (!((BGP4_RT_GET_ADV_FLAG (pRtList) &
                                       BGP4_NETWORK_ADV_ROUTE) ==
                                      BGP4_NETWORK_ADV_ROUTE)))
                               {
            /* Replacement route received. Remove the old route and
             * add the new route
             */
                               if (BGP4_INFO_IS_RMAP_SET
                                   (BGP4_RT_BGP_INFO (pRtProfile)) != BGP4_TRUE)
                               {
                               pNewRtProfile =
                               Bgp4DupCompleteRtProfile (pRtProfile);
                               BGP4_INFO_RCVD_MED (BGP4_RT_BGP_INFO
                                                   (pNewRtProfile)) =
                               BGP4_RT_NEW_MED (pNewRtProfile);
                               BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC | BGP4_EVENTS_TRC,
                                              BGP4_MOD_NAME,
                                              "\tReplacement route is received. Removing the old route"
                                              "from RIB and adding the new route %s mask %s to RIB.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pNewRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pNewRtProfile)),
                                              Bgp4PrintIpMask ((UINT1)
                                                               BGP4_RT_PREFIXLEN
                                                               (pNewRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pNewRtProfile)));
                               Bgp4RibhDelRtEntry (pRtList, pRibNode,
                                                   BGP4_RT_CXT_ID (pRtProfile),
                                                   BGP4_FALSE);
                               BGP4_RT_SET_ADV_FLAG (pNewRtProfile,
                                                     BGP4_REDIST_ADV_ROUTE);
                               BGP4_RT_MED (pNewRtProfile) =
                               BGP4_INFO_RCVD_MED (BGP4_RT_BGP_INFO
                                                   (pNewRtProfile));
                               if ((Bgp4RibhAddRtEntry
                                    (pNewRtProfile,
                                     BGP4_RT_CXT_ID (pNewRtProfile))) ==
                                   BGP4_FAILURE)
                               {
                    /* Adding route to Tree Fails. Dont return. May be we need
                     * to send the Old route as Withdrawn. */
                               BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC | BGP4_EVENTS_TRC |
                                              BGP4_ALL_FAILURE_TRC,
                                              BGP4_MOD_NAME,
                                              "\tAdding the route %s mask %s to RIB failed.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pNewRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pNewRtProfile)),
                                              Bgp4PrintIpMask ((UINT1)
                                                               BGP4_RT_PREFIXLEN
                                                               (pNewRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pNewRtProfile)));}
                               }
                               }

                               else
                               {
                               Bgp4RedSyncRouteInfo (pRtProfile);
                               /* Identical route */
                               if (pBestRoute != NULL)
                               {
                               Bgp4DshReleaseRtInfo (pBestRoute);}
                               return BGP4_SUCCESS;}
                               }
                               else
                               {
                               if (bIsIdenticalRoute != BGP4_TRUE)
                               {
        /* New route originated */
                               BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC | BGP4_EVENTS_TRC,
                                              BGP4_MOD_NAME,
                                              "\tAdding the new route %s mask %s to RIB.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)),
                                              Bgp4PrintIpMask ((UINT1)
                                                               BGP4_RT_PREFIXLEN
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               if ((Bgp4RibhAddRtEntry
                                    (pRtProfile,
                                     BGP4_RT_CXT_ID (pRtProfile))) ==
                                   BGP4_FAILURE)
                               {
            /* Adding of New route FAILS. RIB is unaffected. So no need
             * for any further processing. */
                               BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC | BGP4_EVENTS_TRC |
                                              BGP4_ALL_FAILURE_TRC,
                                              BGP4_MOD_NAME,
                                              "\tAdding the route %s mask %s to RIB failed.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)),
                                              Bgp4PrintIpMask ((UINT1)
                                                               BGP4_RT_PREFIXLEN
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               if (pBestRoute != NULL)
                               {
                               Bgp4DshReleaseRtInfo (pBestRoute);}
                               return (BGP4_FAILURE);}
                               }
                               }

                               i4RtFound =
                               Bgp4RibhLookupRtEntry (pRtProfile,
                                                      BGP4_RT_CXT_ID
                                                      (pRtProfile),
                                                      BGP4_TREE_FIND_EXACT,
                                                      &pFndRtProfileList,
                                                      &pRibNode);
                               if (i4RtFound == BGP4_FAILURE)
                               {
        /* Adding new route to RIB should have failed. */
                               if ((pBestRoute != NULL)
                                   && (pBestRoute == pRtList))
                               {
            /* The removed route was the best route earlier. Now
             * we need to remove the old best route from FIB and 
             * advertise it as withdrawn. */
                               BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC |
                                              BGP4_ALL_FAILURE_TRC,
                                              BGP4_MOD_NAME,
                                              "\tSince adding the route %s mask %s to RIB failed and "
                                              "it was the best route. Hence removing it from FIB and "
                                              "advertise it as withdrawn\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)),
                                              Bgp4PrintIpMask ((UINT1)
                                                               BGP4_RT_PREFIXLEN
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               Bgp4IgphAddWithdRouteToVrfUpdLists (pBestRoute);}
                               if (pBestRoute != NULL)
                               {
                               Bgp4DshReleaseRtInfo (pBestRoute);}
                               return (BGP4_FAILURE);}

                               pNewBestRoute =
                               Bgp4DshGetBestRouteFromProfileList
                               (pFndRtProfileList, BGP4_RT_CXT_ID (pRtProfile));
                               if (pBestRoute != pNewBestRoute)
                               {
                               if (pNewBestRoute != NULL)
                               {
            /* If the newly selected route is the replacement route, 
             * set the REPLACEMENT flag in it. 
             */
                               if (pBestRoute != NULL)
                               {
                               BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                                         "\tBgp4ImportRoute() : Advt. REPLACEMENT ROUTE\n");
                               BGP4_RT_SET_FLAG (pNewBestRoute,
                                                 BGP4_RT_REPLACEMENT);
                               if (pBestRoute != pRtList)
                               {
                               /* Since the old best routes have become Non-Best route
                                * but still present in RIB. So update the
                                * RCVD_PA_ENTRY accordingly. */
                               Bgp4RcvdPADBAddRoute (pBestRoute, BGP4_FALSE);}
                               if (pNewBestRoute != pRtProfile)
                               {
                               /* Best Route has been changed. But the new route is not
                                * the Best route. Means on adding the new route to the
                                * RIB, the old Best route was removed and some other
                                * non-Best route has now become the best route. So
                                * update teh RCVD_PA_ENTRY for this new best route. */
                               Bgp4RcvdPADBAddRoute (pNewBestRoute, BGP4_TRUE);}
                               }

            /* Update the FIB update/peer advt list */
                               BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC | BGP4_EVENTS_TRC,
                                              BGP4_MOD_NAME,
                                              "\tThe Newly selected best route is %s mask %s. Adding "
                                              "the new best route to FIB and peer advertisemnet list.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pNewBestRoute),
                                                               BGP4_RT_AFI_INFO
                                                               (pNewBestRoute)),
                                              Bgp4PrintIpMask ((UINT1)
                                                               BGP4_RT_PREFIXLEN
                                                               (pNewBestRoute),
                                                               BGP4_RT_AFI_INFO
                                                               (pNewBestRoute)));
                               Bgp4IgphAddFeasibleRouteToVrfUpdLists
                               (pNewBestRoute, pBestRoute);}
                               }
                               if (pBestRoute != NULL)
                               {
                               Bgp4DshReleaseRtInfo (pBestRoute);}
                               return BGP4_SUCCESS;}

/*****************************************************************************/
/* Function Name : Bgp4IgphProcessWithdrawnImportRoute                       */
/* Description   : This routine process the withdrawn import routes. Removes */
/*                 the route from the tree. If this some route was selected  */
/*                 as best route then that route will be added to the        */
/*                 internal and external peers advertisement list as new     */
/*                 feasible route. Else this route if best route earlier     */
/*                 will be advertised as withdrawn to the peers.             */
/* Input(s)      : pointer to the Withdrawn Route (pRtProfile)               */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS if the operation is successful,              */
/*                 BGP4_FAILURE if it fails.                                 */
/*****************************************************************************/
                               INT4
                               Bgp4IgphProcessWithdrawnImportRoute
                               (tRouteProfile * pRtProfile)
                               {
                               VOID *pRibNode = NULL;
                               VOID *pNextNode = NULL;
                               tRouteProfile * pFndRtProfileList = NULL;
                               tRouteProfile * pBestRoute = NULL;
                               tRouteProfile * pNewBestRoute = NULL;
                               tRouteProfile * pTmpRtProfile = NULL;
                               tRouteProfile * pRtList = NULL;
                               tRouteProfile * pNextRt = NULL;
                               tLinkNode * pLinkNode = NULL;
                               tBgp4PeerEntry * pPeer = NULL;
                               tTMO_SLL TsFeasibleRouteList;
                               tAddrPrefix InvPrefix;
                               tAddrPrefix FndIpRouteInfo;
#ifdef ROUTEMAP_WANTED
                               tFilteringRMap TempFilterRMap;
#endif /* ROUTEMAP_WANTED */
                               UINT4 u4RtIfIndx = 0;
                               INT4 i4RtFound = 0;
                               INT4 i4RedisMask = 0;
                               INT4 i4LkupStatus = 0;
                               INT4 i4Metric = 0;
                               INT4 i4RetStatus = 0;
                               INT1 i1Protocol = BGP4_ALL_APPIDS;
                               Bgp4InitAddrPrefixStruct (&(InvPrefix),
                                                         BGP4_RT_AFI_INFO
                                                         (pRtProfile));
                               TMO_SLL_Init (&TsFeasibleRouteList);
                               i4RtFound =
                               Bgp4RibhLookupRtEntry (pRtProfile,
                                                      BGP4_RT_CXT_ID
                                                      (pRtProfile),
                                                      BGP4_TREE_FIND_EXACT,
                                                      &pFndRtProfileList,
                                                      &pRibNode);
                               if (i4RtFound == BGP4_FAILURE)
                               {
                               /* Route is not present in RIB. */
                               BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_ALL_FAILURE_TRC |
                                              BGP4_FDB_TRC, BGP4_MOD_NAME,
                                              "\tThe route %s is not present in the RIB. "
                                              "Deletion of the route not processed\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               return (BGP4_FAILURE);}
#ifdef ROUTEMAP_WANTED
                               MEMSET (&TempFilterRMap, 0,
                                       sizeof (tFilteringRMap));
#endif
                               pRtList = pFndRtProfileList;
                               while (pRtList != NULL)
                               {
                               if ((BGP4_RT_PROTOCOL (pRtList) ==
                                    BGP4_RT_PROTOCOL (pRtProfile))
                                   && (BGP4_RT_PROTOCOL (pRtList) == BGP_ID))
                               {
                               break;}
                               else
                               if ((BGP4_RT_PROTOCOL (pRtList) ==
                                    BGP4_RT_PROTOCOL (pRtProfile))
                                   && (BGP4_RT_PROTOCOL (pRtList) != BGP_ID))
                               {
                               if (MEMCMP
                                   (&
                                    (BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                     (pRtProfile)),
                                    &(BGP4_RT_IMMEDIATE_NEXTHOP_INFO (pRtList)),
                                    sizeof (tAddrPrefix)) == 0)
                               {
                               if ((BGP4_RT_GET_ADV_FLAG (pRtProfile) &
                                    BGP4_NETWORK_ADV_ROUTE) ==
                                   BGP4_NETWORK_ADV_ROUTE)
                               {
                               if ((BGP4_RT_GET_ADV_FLAG (pRtList) &
                                    BGP4_NETWORK_ADV_ROUTE) ==
                                   BGP4_NETWORK_ADV_ROUTE)
                               {
                               break;}
                               }
                               else
                               {
                               if ((BGP4_RT_GET_ADV_FLAG (pRtList) &
                                    BGP4_NETWORK_ADV_ROUTE) !=
                                   BGP4_NETWORK_ADV_ROUTE)
                               {
                               break;}

                               }

                               }

                               }
                               pRtList = BGP4_RT_NEXT (pRtList);}
                               if (pRtList == NULL)
                               {
                               /* No matching route exists in RIB. */
                               BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_ALL_FAILURE_TRC |
                                              BGP4_FDB_TRC, BGP4_MOD_NAME,
                                              "\tNo matching route present in the RIB. "
                                              "Route %s is not withdrawn.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               return (BGP4_FAILURE);}

                               if ((BGP4_RT_GET_ADV_FLAG (pRtProfile) &
                                    BGP4_REDIST_ADV_ROUTE) ==
                                   BGP4_REDIST_ADV_ROUTE)
                               {
                               if ((BGP4_RT_GET_ADV_FLAG (pRtList) &
                                    BGP4_NETWORK_ADV_ROUTE) ==
                                   BGP4_NETWORK_ADV_ROUTE)
                               {
                               /* do not delete as route is advertised via network command */
                               BGP4_RT_RESET_ADV_FLAG (pRtList,
                                                       BGP4_REDIST_ADV_ROUTE);
                               BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC, BGP4_MOD_NAME,
                                              "\tRoute %s was advertised using network command. "
                                              "Deletion of the route cannot be proccessed.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               return BGP4_SUCCESS;}
                               }
                               if ((BGP4_RT_GET_ADV_FLAG (pRtProfile) &
                                    BGP4_NETWORK_ADV_ROUTE) ==
                                   BGP4_NETWORK_ADV_ROUTE)
                               {
                               if ((BGP4_RT_GET_ADV_FLAG (pRtList) &
                                    BGP4_REDIST_ADV_ROUTE) ==
                                   BGP4_REDIST_ADV_ROUTE)
                               {
                               BGP4_RT_RESET_ADV_FLAG (pRtList,
                                                       BGP4_NETWORK_ADV_ROUTE);
                               /* to handle in case if route-map is associated earlier with
                                  redistribute command */
                               if (BGP4_RT_PROTOCOL (pRtList) == STATIC_ID)
                               {
                               i4RedisMask = BGP4_IMPORT_STATIC;}
                               else
                               if (BGP4_RT_PROTOCOL (pRtList) == LOCAL_ID)
                               {
                               i4RedisMask = BGP4_IMPORT_DIRECT;}
                               else
                               if (BGP4_RT_PROTOCOL (pRtList) == RIP_ID)
                               {
                               i4RedisMask = BGP4_IMPORT_RIP;}
                               else
                               if (BGP4_RT_PROTOCOL (pRtList) == OSPF_ID)
                               {
                               i4RedisMask = BGP4_IMPORT_OSPF;}
                               else
                               if (BGP4_RT_PROTOCOL (pRtList) == ISIS_ID)
                               {
                               i4RedisMask = BGP4_IMPORT_ISISL1L2;}
                               if (((UINT4) i4RedisMask &
                                    (BGP4_RRD_PROTO_MASK
                                     (BGP4_RT_CXT_ID (pRtList)))) ==
                                   (UINT4) i4RedisMask)
                               {

                               i4LkupStatus =
                               BgpIpRtLookup (BGP4_RT_CXT_ID (pRtProfile),
                                              &
                                              (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                               (BGP4_RT_NET_ADDRESS_INFO
                                                (pRtProfile))),
                                              BGP4_RT_PREFIXLEN (pRtProfile),
                                              &FndIpRouteInfo, &i4Metric,
                                              &u4RtIfIndx, i1Protocol);
                               if (i4LkupStatus == BGP4_SUCCESS)
                               {
                               BGP4_RT_MED (pRtProfile) = i4Metric;
                               BGP4_RT_SET_RT_IF_INDEX (pRtProfile, u4RtIfIndx);
#ifdef ROUTEMAP_WANTED
                               if (STRLEN
                                   (BGP4_RRD_RMAP_NAME
                                    (BGP4_RT_CXT_ID (pRtProfile))) != 0)
                               {
                               MEMCPY (TempFilterRMap.
                                       au1DistInOutFilterRMapName,
                                       BGP4_RRD_RMAP_NAME (BGP4_RT_CXT_ID
                                                           (pRtProfile)),
                                       (RMAP_MAX_NAME_LEN + 4));
                               TempFilterRMap.u1Status = FILTERNIG_STAT_ENABLE;
                               BGP4_DBG (BGP4_DBG_ENTRY,
                                         "\tBgp4ApplyInOutFilter() : .......\n ");
                               if (BGP4_FAILURE ==
                                   Bgp4ApplyInOutFilter (&TempFilterRMap,
                                                         pRtProfile,
                                                         BGP4_INCOMING))
                               {
                               /* Stop processing this route. */
                               BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                                         "\tBgp4ApplyInOutFilter() : Filter Entry FOUND. \n");
                               /*Filter entry found and it is applied as deny. So we are deleting the route in RIB */
                               BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_EVENTS_TRC | BGP4_FDB_TRC,
                                              BGP4_MOD_NAME,
                                              "\tFilter entry found for the route %s and it is applied as deny. "
                                              "Hence deleting the route from RIB.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               Bgp4RibhDelRtEntry (pRtList, pRibNode,
                                                   BGP4_RT_CXT_ID (pRtProfile),
                                                   BGP4_FALSE);
                               return BGP4_SUCCESS;}
                               }
#endif
                               BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_EVENTS_TRC | BGP4_FDB_TRC,
                                              BGP4_MOD_NAME,
                                              "\tRoute %s was added in the RIB using redistribution. "
                                              "Hence deleting the route from RIB.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               Bgp4RibhDelRtEntry (pRtList, pRibNode,
                                                   BGP4_RT_CXT_ID (pRtProfile),
                                                   BGP4_FALSE);
                               i4RetStatus =
                               Bgp4ImportRouteAdd (BGP4_RT_CXT_ID (pRtProfile),
                                                   BGP4_RT_NET_ADDRESS_INFO
                                                   (pRtProfile),
                                                   BGP4_RT_PROTOCOL
                                                   (pRtProfile),
                                                   BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                                   (pRtProfile),
                                                   BGP4_INVALID_ROUTE_TAG,
                                                   BGP4_RT_MED (pRtProfile),
                                                   BGP4_RT_GET_RT_IF_INDEX
                                                   (pRtProfile), 0, 0, NULL,
                                                   BGP4_REDIST_ADV_ROUTE,
                                                   BGP4_FALSE);
                               if (i4RetStatus == BGP4_FAILURE)
                               {
                               BGP4_DBG (BGP4_DBG_ALL,
                                         "\tBgp4ImportRouteAdd() :  Network Route Addition FAILED !!! \n");
                               BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_EVENTS_TRC |
                                              BGP4_ALL_FAILURE_TRC |
                                              BGP4_FDB_TRC, BGP4_MOD_NAME,
                                              "\tAdding the route %s again in RIB using network command failed.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               return BGP4_FAILURE;}

                               }
                               }
                               BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_EVENTS_TRC | BGP4_FDB_TRC,
                                              BGP4_MOD_NAME,
                                              "\tSuccessfully added the route %s again in RIB using network command.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               return BGP4_SUCCESS;}
                               }

                               if (BGP4_IS_ROUTE_VALID (pRtList) != BGP4_TRUE)
                               {
                               /* Route is Invalid. Also remove the route from the RIB. Since this
                                * route is invalid removing of this route need not be followed by any
                                * advertisement. So just return
                                */
                               if ((BGP4_RT_GET_FLAGS (pRtList) &
                                    BGP4_RT_OVERLAP) == BGP4_RT_OVERLAP)
                               {
                               /* Route present in overlap list.Remove it from the Overlap List. */
                               BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_EVENTS_TRC | BGP4_FDB_TRC,
                                              BGP4_MOD_NAME,
                                              "\tRoute %s is invalid and it is present in overlap policy list. "
                                              "Hence deleting the route from RIB and overlap list.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               Bgp4RibhDelRtEntry (pRtList, pRibNode,
                                                   BGP4_RT_CXT_ID (pRtProfile),
                                                   BGP4_TRUE);
                               BGP4_RT_RESET_FLAG (pRtList, BGP4_RT_OVERLAP);
                               Bgp4DshRemoveRouteFromList
                               (BGP4_OVERLAP_ROUTE_LIST
                                (BGP4_RT_CXT_ID (pRtProfile)), pRtList);}
                               else
                               {
                               BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_EVENTS_TRC | BGP4_FDB_TRC,
                                              BGP4_MOD_NAME,
                                              "\tRoute %s is invalid. Hence deleting the route from RIB.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               Bgp4RibhDelRtEntry (pRtList, pRibNode,
                                                   BGP4_RT_CXT_ID (pRtProfile),
                                                   BGP4_FALSE);}
                               return BGP4_SUCCESS;}

                               /* Route is a Valid Route. Delete the route from RIB and if necessary 
                                * advertise to the Peers. . */
                               if ((BGP4_NON_BGP_EVENT_RCVD
                                    (BGP4_RT_CXT_ID (pRtProfile)) != 0)
                                   &&
                                   (PrefixMatch
                                    (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                     (BGP4_RT_NET_ADDRESS_INFO (pRtList)),
                                     InvPrefix) != BGP4_TRUE)
                                   && (BGP4_RT_PREFIXLEN (pRtList) != 0)
                                   &&
                                   (PrefixMatch
                                    (BGP4_NON_BGP_INIT_PREFIX_INFO
                                     (BGP4_RT_CXT_ID (pRtProfile)),
                                     pFndRtProfileList->NetAddress.NetAddr) ==
                                    BGP4_TRUE))
                               {
#ifdef L3VPN
                               /*NonBgpRoute VrfId has to be checked */
#endif
                               /* Route present in the Non-BGP init info is to be removed.
                                * Get the next non-BGP route from the RIB and update this field
                                * If next route is not present, then clear this init info and
                                * update the non-BGP route Advt policy. */
                               pNextRt = NULL;
                               pNextNode = pRibNode;
                               pNextRt =
                               Bgp4RibhGetNextImportRtEntry (pFndRtProfileList,
                                                             (VOID **)
                                                             &pNextNode);
                               if (pNextRt == NULL)
                               {
                               /* No more NON-BGP route present in RIB for non-BGP Advt
                                * policy change handling. */
                               BGP4_NON_BGP_RT_EXPORT_POLICY (BGP4_RT_CXT_ID
                                                              (pRtProfile)) =
                               BGP4_NON_BGP_EVENT_RCVD (BGP4_RT_CXT_ID
                                                        (pRtProfile));
                               BGP4_NON_BGP_EVENT_RCVD (BGP4_RT_CXT_ID
                                                        (pRtProfile)) = 0;
                               Bgp4InitAddrPrefixStruct (&
                                                         (BGP4_NON_BGP_INIT_PREFIX_INFO
                                                          (BGP4_RT_CXT_ID
                                                           (pRtProfile))), 0);}
                               else
                               {
                               Bgp4CopyAddrPrefixStruct (&
                                                         (BGP4_NON_BGP_INIT_PREFIX_INFO
                                                          (BGP4_RT_CXT_ID
                                                           (pRtProfile))),
                                                         pNextRt->NetAddress.
                                                         NetAddr);}
                               }

                               pBestRoute =
                               Bgp4DshGetBestRouteFromProfileList
                               (pFndRtProfileList, BGP4_RT_CXT_ID (pRtProfile));
                               /* Delete the route from the RIB. */
                               BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_EVENTS_TRC | BGP4_FDB_TRC,
                                              BGP4_MOD_NAME,
                                              "\tRoute %s is valid. Hence deleting the route from RIB.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               BGP4_RT_REF_COUNT (pRtList)++;
                               Bgp4RibhDelRtEntry (pRtList, pRibNode,
                                                   BGP4_RT_CXT_ID (pRtProfile),
                                                   BGP4_FALSE);
                               /* Try and get the New Best Route. */
                               pFndRtProfileList = NULL;
                               pRibNode = NULL;
                               pNewBestRoute = NULL;
                               i4RtFound =
                               Bgp4RibhLookupRtEntry (pRtProfile,
                                                      BGP4_RT_CXT_ID
                                                      (pRtProfile),
                                                      BGP4_TREE_FIND_EXACT,
                                                      &pFndRtProfileList,
                                                      &pRibNode);
                               if (i4RtFound == BGP4_SUCCESS)
                               {
                               pNewBestRoute =
                               Bgp4DshGetBestRouteFromProfileList
                               (pFndRtProfileList,
                                BGP4_RT_CXT_ID (pRtProfile));}

                               if (pBestRoute == pRtList)
                               {
                               /* Best route is removed from the RIB. */
                               BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_FDB_TRC, BGP4_MOD_NAME,
                                              "\tThe deleted route %s is the best route. And it has been withdrawn.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));
                               if ((pNewBestRoute != NULL)
                                   &&
                                   ((BGP4_RT_GET_FLAGS (pNewBestRoute) &
                                     BGP4_RT_BEST) == BGP4_RT_BEST))
                               {
                               /* If the newly selected route is the replacement route, 
                                * set the REPLACEMENT flag in it. 
                                */
                               if (pBestRoute != NULL)
                               {
                               BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                                         "\tBgp4ImportRouteDelete() : Advt. "
                                         "REPLACEMENT ROUTE\n");
                               BGP4_RT_SET_FLAG (pNewBestRoute,
                                                 BGP4_RT_REPLACEMENT);
                               if (BGP4_RT_PROTOCOL (pNewBestRoute) == BGP_ID)
                               {
                               if (Bgp4AhCanRouteBeAdvtWithdrawn (pNewBestRoute,
                                                                  pBestRoute) ==
                                   BGP4_TRUE)
                               {
                               BGP4_RT_SET_FLAG (pNewBestRoute,
                                                 BGP4_RT_ADVT_WITHDRAWN);
                               /* Reset the old routes ADVT_WITHDRAWN flag. */
                               BGP4_RT_RESET_FLAG (pBestRoute,
                                                   BGP4_RT_ADVT_WITHDRAWN);}
                               }

                               if (BGP4_RT_PROTOCOL (pBestRoute) ==
                                   BGP4_STATIC_ID)
                               {
                               BGP4_RT_SET_FLAG (pNewBestRoute,
                                                 BGP4_RT_NEWUPDATE_TOIP);}
                               BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_EVENTS_TRC | BGP4_FDB_TRC,
                                              BGP4_MOD_NAME,
                                              "\tThe new best route is %s since the old best route is deleted."
                                              " Updating the new best route in feasible route list.\n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pNewBestRoute),
                                                               BGP4_RT_AFI_INFO
                                                               (pNewBestRoute)));
                               Bgp4IgphAddFeasibleRouteToVrfUpdLists
                               (pNewBestRoute, pBestRoute);
                               /* Since the old non-best routes have become Best route,
                                * update the RCVD_PA_ENTRY accordingly. */
                               Bgp4RcvdPADBAddRoute (pNewBestRoute, BGP4_TRUE);}
                               else
                               {
                               /* There is no possibility for this else condition to be 
                                * reached. */
                               }
                               }
                               else
                               {
                               /* pRtList  was previously selected as the best route and 
                                * advertised to other peers, but now it has been withdrawn.
                                */
                               Bgp4IgphAddWithdRouteToVrfUpdLists (pBestRoute);
                               if (BGP4_OVERLAP_ROUTE_POLICY
                                   (BGP4_RT_CXT_ID (pRtProfile)) !=
                                   BGP4_INSTALL_BOTH_RT)
                               {
                               Bgp4DshGetDeniedRoutesFromOverlapList
                               (&TsFeasibleRouteList, pBestRoute,
                                BGP4_OVERLAP_ROUTE_POLICY (BGP4_RT_CXT_ID
                                                           (pRtProfile)));}
                               }

                               /* Add the overlapping feasible routes to the RIB */

                               if (TMO_SLL_Count (&TsFeasibleRouteList) > 0)
                               {
                               TMO_SLL_Scan (&TsFeasibleRouteList, pLinkNode,
                                             tLinkNode *)
                               {
                               pTmpRtProfile = pLinkNode->pRouteProfile;
                               BGP4_RT_RESET_FLAG (pTmpRtProfile,
                                                   BGP4_RT_WITHDRAWN);
                               Bgp4DshDelinkRouteFromChgList (pTmpRtProfile);
                               if (BGP4_RT_PROTOCOL (pTmpRtProfile) == BGP_ID)
                               {
                               /* Route is learnt via BGP. Add the route to the
                                * received route list of the peer from which the
                                * route was learnt. */
                               pPeer = BGP4_RT_PEER_ENTRY (pTmpRtProfile);
                               Bgp4RibhProcessFeasibleRoute (pPeer,
                                                             pTmpRtProfile);}
                               else
                               {
                               /* Route learnt via redistribution. Add the route to
                                * the Redistribution list. */
                               if ((BGP4_RT_GET_FLAGS (pTmpRtProfile) &
                                    BGP4_RT_IN_REDIST_LIST) !=
                                   BGP4_RT_IN_REDIST_LIST)
                               {
                               Bgp4IgphProcessFeasibleImportRoute
                               (pTmpRtProfile);}
                               }
                               }
                               Bgp4DshReleaseList (&TsFeasibleRouteList, 0);}
                               }

                               /* Release the pRtList Structure. */
                               Bgp4DshReleaseRtInfo (pRtList);
                               return BGP4_SUCCESS;}

/****************************************************************************/
/* FUNCTION NAME : Bgp4IgphUpdateRouteInFIB                                 */
/* DESCRIPTION   : This function handles the route-updation in the Forward  */
/*               : Information Base table.                                  */
/* INPUTS        : pRtProfile - The route profile information               */
/* OUTPUTS       : None                                                     */
/* RETURNS       : BGP4_SUCCESS/BGP4_FAILURE                                */
/****************************************************************************/
                               INT4
                               Bgp4IgphUpdateRouteInFIB (tRouteProfile *
                                                         pRtProfile)
                               {
                               tBgp4PeerEntry * pPeer = NULL;
                               INT4 i4RetVal = BGP4_SUCCESS;
                               UINT4 u4Context = 0;
#if defined (VPLSADS_WANTED) || defined (EVPN_WANTED)
                               UINT4 u4AsafiMask;
                               BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO
                                                      (pRtProfile),
                                                      BGP4_RT_SAFI_INFO
                                                      (pRtProfile),
                                                      u4AsafiMask);
#endif
#ifdef L3VPN
                               if ((BGP4_RT_AFI_INFO (pRtProfile) ==
                                    BGP4_INET_AFI_IPV4)
                                   && (BGP4_RT_SAFI_INFO (pRtProfile) ==
                                       BGP4_INET_SAFI_VPNV4_UNICAST))
                               {
                               i4RetVal =
                               Bgp4Vpnv4UpdtRouteInIPVRF (pRtProfile);
                               return (i4RetVal);}
#endif
                               u4Context = BGP4_RT_CXT_ID (pRtProfile);
                               /* Check whether the route is feasible or withdrawn. */
                               if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                                    BGP4_RT_WITHDRAWN) == BGP4_RT_WITHDRAWN)
                               {

#ifdef VPLSADS_WANTED
#ifdef MPLS_WANTED
                               /*If route is Withdrawn route for VPLS, send it to the L2VPN 
                                *as withdrawn message*/
                               if (u4AsafiMask == CAP_MP_L2VPN_VPLS)
                               {
                               if (pRtProfile->pVplsSpecInfo != NULL)
                               {
                               i4RetVal =
                               Bgp4VplsCreateWithdMsgforL2VPN (pRtProfile);}
                               }
#endif
#endif
#ifdef EVPN_WANTED
                               /* The Route is withdrawn route, send it to VXLAN */
                               if (u4AsafiMask == CAP_MP_L2VPN_EVPN)
                               {
                               i4RetVal =
                               Bgp4EvpnCreateWithdMsgforVXLAN (pRtProfile);}
#endif

                               if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
                               {
                               pPeer = BGP4_RT_PEER_ENTRY (pRtProfile);}
                               if (gi1SyncStatus != SYNC_IN_PROGRESS)
                               {
                               if ((BGP4_RT_GET_EXT_FLAGS (pRtProfile) &
                                    BGP4_RT_IN_RTM) == BGP4_RT_IN_RTM)
                               {
                               BGP4_TRC_ARG2 (NULL,
                                              BGP4_TRC_FLAG, BGP4_DETAIL_TRC,
                                              BGP4_MOD_NAME,
                                              "\tCalling BGP4RTDeleteRouteInCommIpRtTbl from Func[%s] Line[%d]\n",
                                              __func__, __LINE__);
                               BGP4RTDeleteRouteInCommIpRtTbl (pRtProfile);}
                               else
                               {
                               /* RM 233626    
                                  while ( pRtProfile->pMultiPathRtNext != NULL)
                                  {
                                  pRtProfile= pRtProfile->pMultiPathRtNext;
                                  BGP4RTDeleteRouteInCommIpRtTbl (pRtProfile);
                                  }
                                */
                               }
                               }
                               /* Route is a withdrawn route. */
                               /* Updating to FIB is done. Check whether the
                                * peer from which the route is learnt is going through deinit.
                                * If yes, then dont do any process. Else Check whether
                                * the route needs to be sent to the external peer or not.
                                * If required to send, then add the route to the external
                                * peer advt list. */

                               if ((pPeer != NULL) &&
                                   (BGP4_GET_PEER_CURRENT_STATE (pPeer) ==
                                    BGP4_PEER_DEINIT_INPROGRESS))
                               {
                               /* No need to do any operation. */
                               }
                               else
                               {
                               if (((BGP4_RT_GET_FLAGS (pRtProfile) &
                                     BGP4_RT_ADVT_NOTTO_EXTERNAL) == 0) &&
                                   (BGP4_LOCAL_ADMIN_STATUS (u4Context) ==
                                    BGP4_ADMIN_UP)
                                   &&
                                   ((BGP4_RT_GET_EXT_FLAGS (pRtProfile) &
                                     BGP4_RT_MULTIPATH) != BGP4_RT_MULTIPATH))
                               {
                               /* Route can be advertised to external peer. */
                               Bgp4DshLinkRouteToList (BGP4_EXT_PEERS_ADVT_LIST
                                                       (u4Context), pRtProfile,
                                                       0,
                                                       BGP4_EXT_PEER_LIST_INDEX);
                               BGP4_RT_SET_FLAG (pRtProfile,
                                                 BGP4_RT_IN_EXT_PEER_ADVT_LIST);}
                               else
                               {
                               /* Route should not be advertised to the external peer. */
                               BGP4_RT_RESET_FLAG (pRtProfile,
                                                   BGP4_RT_ADVT_NOTTO_EXTERNAL);
                               if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                                    BGP4_RT_IN_INT_PEER_ADVT_LIST) !=
                                   (BGP4_RT_IN_INT_PEER_ADVT_LIST))
                               {
                               BGP4_RT_RESET_FLAG (pRtProfile,
                                                   (BGP4_RT_WITHDRAWN |
                                                    BGP4_RT_REPLACEMENT));}
                               }
                               }

                               BGP4_RT_RESET_EXT_FLAG (pRtProfile,
                                                       BGP4_RT_IN_RTM);
                               if ((BGP4_RT_GET_EXT_FLAGS (pRtProfile) &
                                    BGP4_RT_MULTIPATH) == BGP4_RT_MULTIPATH)
                               {
                               BGP4_RT_RESET_EXT_FLAG (pRtProfile,
                                                       BGP4_RT_MULTIPATH);}
#ifdef VPLSADS_WANTED
                               if (u4AsafiMask == CAP_MP_L2VPN_VPLS)
                               {
                               BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_VPLS_TRC, BGP4_MOD_NAME,
                                              "\tBgp4IgphUpdateRouteInFIB(): < L2VPN,VPLS > Updating FIB:WithDraw to L2vpn "
                                              " NLRI - [%s] \n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));}

#endif
                               }
                               else
                               {

                               /* If the global admin status is down, then no need to process the
                                * route for updating the FIB and adding to external peer advt list.
                                * Just remove the route from FIB list. */
                               if (BGP4_LOCAL_ADMIN_STATUS (u4Context) ==
                                   BGP4_ADMIN_DOWN)
                               {
                               return (BGP4_SUCCESS);}
                               /* Route is a feasible route. */

                               BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                         BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                                         "\tBgp4IgphUpdateRouteInFIB() : Updating FIB... \n");
#ifdef VPLSADS_WANTED
                               if (u4AsafiMask == CAP_MP_L2VPN_VPLS)
                               {
                               BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_VPLS_TRC, BGP4_MOD_NAME,
                                              "\tBgp4IgphUpdateRouteInFIB(): < L2VPN,VPLS > Updating FIB:Advt to L2vpn "
                                              " NLRI - [%s] \n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));}

#endif

                               /* Call the routine to Add the route entry to the Trie Table */
                               if (((BGP4_RT_GET_FLAGS (pRtProfile) &
                                     BGP4_RT_REPLACEMENT) ==
                                    BGP4_RT_REPLACEMENT)
                                   &&
                                   ((BGP4_RT_GET_FLAGS (pRtProfile) &
                                     BGP4_RT_NEWUPDATE_TOIP) !=
                                    BGP4_RT_NEWUPDATE_TOIP))
                               {
#ifdef VPLSADS_WANTED
                               if (u4AsafiMask != CAP_MP_L2VPN_VPLS)
#endif
                               {
                               BGP4_TRC_ARG2 (NULL,
                                              BGP4_TRC_FLAG, BGP4_DETAIL_TRC,
                                              BGP4_MOD_NAME,
                                              "\tCalling BGP4RTModifyRouteInCommIpRtTbl from Func[%s] Line[%d]\n",
                                              __func__, __LINE__);
                               i4RetVal =
                               BGP4RTModifyRouteInCommIpRtTbl (pRtProfile);}
#ifdef VPLSADS_WANTED
#ifdef MPLS_WANTED
                               else
                               {
                               if (pRtProfile->pVplsSpecInfo != NULL)
                               {
                               /*This is not valid case for VPLS,precaution add it */
                               /*If route is feasible route for VPLS, send it to L2VPN
                                *as Advt message*/
                               i4RetVal =
                               Bgp4VplsCreateAdvtMsgforL2VPN (pRtProfile);}
                               }
#endif
#endif
#ifdef EVPN_WANTED
                               if (u4AsafiMask == CAP_MP_L2VPN_EVPN)
                               {
                               i4RetVal =
                               Bgp4EvpnCreateAdvtMsgforVXLAN (pRtProfile);}
#endif
                               }
                               else
                               {
#ifdef VPLSADS_WANTED
                               if (u4AsafiMask != CAP_MP_L2VPN_VPLS)
#endif
                               {
                               BGP4_TRC_ARG2 (NULL,
                                              BGP4_TRC_FLAG, BGP4_DETAIL_TRC,
                                              BGP4_MOD_NAME,
                                              "\tCalling BGP4RTAddRouteToCommIpRtTbl from Func[%s] Line[%d]\n",
                                              __func__, __LINE__);
                               i4RetVal =
                               BGP4RTAddRouteToCommIpRtTbl (pRtProfile);}
#ifdef VPLSADS_WANTED
#ifdef MPLS_WANTED
                               else
                               {
                               if (pRtProfile->pVplsSpecInfo != NULL)
                               {
                               /*If route is feasible route for VPLS, send it to L2VPN
                                *as Advt message*/
                               i4RetVal =
                               Bgp4VplsCreateAdvtMsgforL2VPN (pRtProfile);}
                               }
#endif
#endif
#ifdef EVPN_WANTED
                               if (u4AsafiMask == CAP_MP_L2VPN_EVPN)
                               {
                               i4RetVal =
                               Bgp4EvpnCreateAdvtMsgforVXLAN (pRtProfile);}
#endif

                               }

                               if (i4RetVal == BGP4_SUCCESS)
                               {
                               /* Updating to FIB is done successfully. Now Check whether
                                * the route needs to be sent to the external peer or not.
                                * If required to send, then add the route to the external
                                * peer advt list. */
                               /* Set the BEST flag to indicate the route is selected as
                                * best route and add to FIB. */

                               if (((BGP4_RT_GET_FLAGS (pRtProfile) &
                                     BGP4_RT_ADVT_NOTTO_EXTERNAL) == 0)
                                   &&
                                   ((BGP4_RT_GET_EXT_FLAGS (pRtProfile) &
                                     BGP4_RT_MULTIPATH) != BGP4_RT_MULTIPATH))
                               {
                               /* Route can be advertised to external peer. */
                               Bgp4DshLinkRouteToList (BGP4_EXT_PEERS_ADVT_LIST
                                                       (u4Context), pRtProfile,
                                                       0,
                                                       BGP4_EXT_PEER_LIST_INDEX);
                               BGP4_RT_SET_FLAG (pRtProfile,
                                                 BGP4_RT_IN_EXT_PEER_ADVT_LIST);}
                               else
                               {
                               /* Route should not be advertised to the external peer.
                                * Reset the notto_external flag. */
                               BGP4_RT_RESET_FLAG (pRtProfile,
                                                   BGP4_RT_ADVT_NOTTO_EXTERNAL |
                                                   BGP4_RT_REPLACEMENT);}
                               BGP4_RT_RESET_FLAG (pRtProfile,
                                                   BGP4_RT_NEWUPDATE_TOIP);
                               BGP4_RT_SET_EXT_FLAG (pRtProfile,
                                                     BGP4_RT_IN_RTM);
                               Bgp4RedSyncRouteUpdToFIB (pRtProfile);
#ifdef VPLSADS_WANTED
                               BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                         BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                                         "\tBgp4IgphUpdateRouteInFIB():< L2VPN,VPLS > Updating FIB:Advt to L2VPN SUCCESS ... \n");
#endif
                               }
                               else
                               {
                               /* Adding to FIB fails. Reset the flags. */
                               if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                                    BGP4_RT_IN_INT_PEER_ADVT_LIST) ==
                                   BGP4_RT_IN_INT_PEER_ADVT_LIST)
                               {
                               /* Route not yet advt to internal peers. Just remove it. */
                               BGP4_RT_RESET_FLAG (pRtProfile,
                                                   BGP4_RT_IN_INT_PEER_ADVT_LIST);
                               Bgp4DshDelinkRouteFromList
                               (BGP4_INT_PEERS_ADVT_LIST (u4Context),
                                pRtProfile, BGP4_INT_PEER_LIST_INDEX);}
                               else
                               {
                               /* Route might have been advt to internal peers.
                                * Need to advt this as withdrawn. Using
                                * BGP4_RT_AGGR_WITHDRAWN flag because this route needs to
                                * be sent as withdrawn but still will be present in RIB */
                               Bgp4DshLinkRouteToList (BGP4_INT_PEERS_ADVT_LIST
                                                       (u4Context), pRtProfile,
                                                       BGP4_FALSE,
                                                       BGP4_INT_PEER_LIST_INDEX);
                               BGP4_RT_SET_FLAG (pRtProfile,
                                                 (BGP4_RT_IN_INT_PEER_ADVT_LIST
                                                  | BGP4_RT_AGGR_WITHDRAWN));}
                               BGP4_RT_RESET_FLAG (pRtProfile,
                                                   (BGP4_RT_ADVT_NOTTO_EXTERNAL
                                                    | BGP4_RT_REPLACEMENT |
                                                    BGP4_RT_NEWUPDATE_TOIP));
                               BGP4_RT_RESET_EXT_FLAG (pRtProfile,
                                                       BGP4_RT_IN_RTM);
#ifdef VPLSADS_WANTED
                               if (u4AsafiMask != CAP_MP_L2VPN_VPLS)
#endif
                               {
                               Bgp4SnmpSendRTMRouteAdditionFailureTrap
                               (pRtProfile, BGP4_RTM_ROUTE_FAILURE_TRAP);}
#ifdef VPLSADS_WANTED
                               else
                               {
                               BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                                              BGP4_CONTROL_PATH_TRC |
                                              BGP4_VPLS_TRC |
                                              BGP4_ALL_FAILURE_TRC,
                                              BGP4_MOD_NAME,
                                              "\tBgp4IgphUpdateRouteInFIB(): < L2VPN,VPLS > Updating FIB Failure:Advt to L2vpn "
                                              " NLRI - [%s] \n",
                                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                               (pRtProfile),
                                                               BGP4_RT_AFI_INFO
                                                               (pRtProfile)));}
#endif
                               }
                               }
                               return i4RetVal;}

/****************************************************************************/
/* Function    :  Bgp4ExtractInfoFromTag                                    */
/* Description :  This function will extract information from the tag.      */
/*             :  Only the AS information (last 2 bytes) from the tag is    */
/*             :  processed. If a valid AS is present, then the route       */
/*             :  actually belongs to an external AS and should have been   */
/*             :  learnt via some EGP (may be EBGP). So in such case, the   */
/*             :  origin is set as  EGP. Else if the AS present in tag is   */
/*             :  local AS or 0, the route is considered to be learnt from  */
/*             :  the same AS and the origin will be IGP.                   */
/* Input       :  Routes BGP Info - pBGP4Info                               */
/*             :  Tags info received from IGP                               */
/* Output      :  None.                                                     */
/* Returns     :  BGP4_SUCCESS or BGP4_FAILURE                              */
/****************************************************************************/

                               INT1
                               Bgp4ExtractInfoFromTag (UINT4 u4CxtId,
                                                       tBgp4Info * pBGP4Info,
                                                       UINT4 u4Tag,
                                                       tRouteProfile *
                                                       pRtProfile)
                               {
                               UINT4 u4PathAS;
                               UINT4 u4TmpTag = u4Tag;
                               UNUSED_PARAM (u4CxtId); if (pBGP4Info == NULL)
                               {
                               return BGP4_FAILURE;}

                               u4TmpTag = u4TmpTag >> BGP4_ONE_BYTE_BITS;
                               /* Accept all manual tags */
                               if ((u4TmpTag & BGP4_TAG_AUTO_MASK) != 0)
                               {
                               /* For automatic tags, the following process to be followed */
                               switch (u4TmpTag & BGP4_TAG_FOR_DECISION)
                               {
case BGP4_TAG_1000:
BGP4_INFO_ORIGIN (pBGP4Info) = BGP4_ATTR_ORIGIN_EGP; break; case BGP4_TAG_1001:
BGP4_INFO_ORIGIN (pBGP4Info) = BGP4_ATTR_ORIGIN_EGP; break; case BGP4_TAG_1100:
BGP4_INFO_ORIGIN (pBGP4Info) = BGP4_ATTR_ORIGIN_IGP; break; case BGP4_TAG_1101:
BGP4_INFO_ORIGIN (pBGP4Info) = BGP4_ATTR_ORIGIN_IGP; break; default:
                               return BGP4_FAILURE;}
                               }
                               /* Extract the AS number value from Tag */
                               u4PathAS = (UINT4) (u4Tag & BGP4_TAG_AS_FLAG);
                               if (u4PathAS == BGP4_LOCAL_AS_NO (u4CxtId))
                               {
                               /* Tag carrying the LOCAL AS no. */
                               return (BGP4_SUCCESS);}
                               else
                               if (u4PathAS == BGP4_INVALID_ROUTE_TAG)
                               {
                               /* Tag dont have any value. Probably route belongs to the same AS */
                               return (BGP4_SUCCESS);}

                               if (Bgp4ConstructAsPath (u4PathAS, pBGP4Info) !=
                                   BGP4_SUCCESS)
                               {
                               BGP4_RT_RESET_EXT_FLAG (pRtProfile,
                                                       BGP4_RT_AS_PATHSET_TAG);
                               return BGP4_FAILURE;}
                               BGP4_RT_SET_EXT_FLAG (pRtProfile,
                                                     BGP4_RT_AS_PATHSET_TAG);
                               return (BGP4_SUCCESS);}

/*****************************************************************************/
/* Function Name : Bgp4SetTaginfo                                            */
/* Description   : Set the tag info appropriately for re-distribution to IGPs*/
/* Input(s)      : Bgp Info of the route (pBGP4Info)                         */
/*                 Peer Type (u4PeerType)                                    */
/* Output(s)     : None.                                                     */
/* Return(s)     : Constructed Tag                                           */
/*****************************************************************************/
                               UINT4
                               Bgp4SetTaginfo (tBgp4Info * pBGP4Info,
                                               UINT4 u4PeerType)
                               {
                               tAsPath * pASPath = NULL;
                               UINT4 u4TagValue = BGP4_INVALID_ROUTE_TAG;
                               UINT4 u4AsNo;
                               UINT1 u1AsCnt = 0;
                               if ((pBGP4Info != NULL)
                                   &&
                                   (TMO_SLL_Count (BGP4_INFO_ASPATH (pBGP4Info))
                                    > 0))
                               {
                               if (u4PeerType == BGP4_EXTERNAL_PEER)
                               {
                               pASPath =
                               (tAsPath *)
                               TMO_SLL_First (BGP4_INFO_ASPATH (pBGP4Info));
                               u1AsCnt = BGP4_ASPATH_LEN (pASPath);
                               if (u1AsCnt == BGP4_MIN_AS_CNT)
                               {
                               if (BGP4_INFO_ORIGIN (pBGP4Info) ==
                                   BGP4_ATTR_ORIGIN_INCOMPLETE)
                               {
                               PTR_FETCH4 (u4AsNo, BGP4_ASPATH_NOS (pASPath));
                               u4TagValue = BGP4_TAG1_1001 | u4AsNo;}
                               else
                               {
                               if ((BGP4_INFO_ORIGIN (pBGP4Info) ==
                                    BGP4_ATTR_ORIGIN_IGP)
                                   || (BGP4_INFO_ORIGIN (pBGP4Info) ==
                                       BGP4_ATTR_ORIGIN_EGP))
                               {
                               PTR_FETCH4 (u4AsNo, BGP4_ASPATH_NOS (pASPath));
                               u4TagValue = BGP4_TAG1_1101 | u4AsNo;}
                               }
                               }
                               else
                               {
                               PTR_FETCH4 (u4AsNo, BGP4_ASPATH_NOS (pASPath));
                               u4TagValue = BGP4_TAG1_1010 | u4AsNo;}
                               }
                               else
                               {
                               u4TagValue = BGP4_INVALID_ROUTE_TAG;}
                               }
                               return (u4TagValue);}

/*****************************************************************************/
/* Function Name : Bgp4ConstructAsPath                                       */
/* Description   : This function adds the given as number to the ASPath list */
/*                 in the bgpInfo of the route.                              */
/* Input(s)      : u2AsNo,pASPath                                            */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,             */
/*                 BGP4_FAILURE if it fails.                                 */
/******************************************************************************/
                               INT4
                               Bgp4ConstructAsPath (UINT4 u4AsNo,
                                                    tBgp4Info * pBGP4Info)
                               {
                               tAsPath * pASPath = NULL;
                               UINT1 *pu1AsNos = NULL;
                               pASPath = Bgp4MemGetASNode (sizeof (tAsPath));
                               if (pASPath == NULL)
                               {
                               BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                         BGP4_ALL_FAILURE_TRC |
                                         BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC,
                                         BGP4_MOD_NAME,
                                         "\tMemory Allocation to Construct AS Node FAILED!!!\n");
                               gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
                               return (BGP4_FAILURE);}

                               BGP4_ASPATH_TYPE (pASPath) = BGP4_ATTR_PATH_SEQUENCE; BGP4_ASPATH_LEN (pASPath) = BGP4_MIN_AS_CNT;    /* As Number is a 
                                                                                                                                     * 2-octet field */
                               if (BGP4_AS4_SEG_LEN <
                                   BGP4_ASPATH_LEN (pASPath) * BGP4_AS4_LENGTH)
                               {
                               BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                         BGP4_ALL_FAILURE_TRC |
                                         BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                                         "\tSize of au1ASSegs is less,increase the value of BGP4_AS4_SEG_LEN\n");
                               Bgp4MemReleaseASNode (pASPath);
                               return (BGP4_FAILURE);}
                               MEMSET (BGP4_ASPATH_NOS (pASPath), 0,
                                       BGP4_AS4_SEG_LEN);
                               pu1AsNos = BGP4_ASPATH_NOS (pASPath);
                               PTR_ASSIGN4 (pu1AsNos, ((u4AsNo)));
                               TMO_SLL_Add (BGP4_INFO_ASPATH (pBGP4Info),
                                            &pASPath->sllNode);
                               BGP4_INFO_ATTR_FLAG (pBGP4Info) |=
                               BGP4_ATTR_PATH_MASK; return (BGP4_SUCCESS);}

/*****************************************************************************/
/* Function Name : Bgp4RouteLeak                                             */
/* Description   : Imports a non-BGP route, and updates the BGP RIB Tree     */
/*                 accordingly. After updating the RIB tree, if there is any */
/*                 change in the best route selected then proper action will */
/*                 be taken accordingly.                                     */
/* Input(s)      : Prefix of the route to be imported (u4Prefix),            */
/*                 Prefix length of the route (u1Prefixlen),                 */
/*                 Protocol from which imported (u4Protocol),                */
/*                 NextHop for the route (u4NextHop)                         */
/*                 Route Metrics (i4Metric)                                  */
/*                 Tags associated with the route (u4Tag)                    */
/*                 Interface Index (u4RtIfIndx)                              */
/*                 Whether it is ADDition or DELetion (u4Action)             */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS if the operation is successful,              */
/*                 BGP4_FAILURE if it fails.                                 */
/*****************************************************************************/
                               INT4
                               Bgp4RouteLeak  (UINT4 u4VrfId,
                                               tNetAddress RtLeakAddress,
                                               UINT4 u4Protocol,
                                               tAddrPrefix NextHopInfo,
                                               UINT4 u4Action, UINT4 u4Tag,
                                               INT4 i4Metric, UINT4 u4RtIfIndx,
                                               UINT1 u1MetricType,
                                               UINT1 u1Community,
                                               UINT4 *pulCommunity,
                                               UINT4 u4RmapMetricSet)
                               {
                               UINT1 u1ValidStatus;
                               /* Validate the input */
                               u1ValidStatus =
                               Bgp4IsValidPrefixLength (RtLeakAddress);
                               if (u1ValidStatus == BGP4_FALSE)
                               {
                               BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                         BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                                         "\tRoute Leak : Invalid Prefix Length\n");
                               return BGP4_FAILURE;}

                               if (Bgp4IsValidAddress
                                   (RtLeakAddress.NetAddr,
                                    BGP4_TRUE) != BGP4_TRUE)
                               {
                               BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                         BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                                         "\tRoute Leak : Invalid Prefix\n");
                               return BGP4_FAILURE;}

                               if ((u4Protocol != BGP4_STATIC_ID) &&
                                   (u4Protocol != BGP4_LOCAL_ID) &&
                                   (u4Protocol != BGP4_RIP_ID)
                                   && (u4Protocol != BGP4_OSPF_ID)
                                   && (u4Protocol != BGP4_ISIS_ID))
                               {
                               BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                         BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                                         "\tRoute Leak : Invalid Protocol\n");
                               return BGP4_FAILURE;}

                               if ((u4Action != BGP4_IMPORT_RT_ADD)
                                   && (u4Action != BGP4_IMPORT_RT_DELETE))
                               {
                               BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                         BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                                         "\tRoute Leak : Invalid Action\n");
                               return BGP4_FAILURE;}

                               /* Update the local rib for the next-hop */
                               if (Bgp4ImportRoute
                                   (u4VrfId, RtLeakAddress, u4Protocol,
                                    NextHopInfo, u4Action, u4Tag, i4Metric,
                                    u4RtIfIndx, u1MetricType, u1Community,
                                    pulCommunity,
                                    u4RmapMetricSet) == BGP4_FAILURE)
                               {
                               return BGP4_FAILURE;}
                               return BGP4_SUCCESS;}

/****************************************************************************
 Function    :  Bgp4StoreImportList
 Description :  This function will store all the route learned by particular
                protocol
 Input       :  RouteProfile to be added to the list (pRtProfile)
 Output      :  Updated route List (pImportList)
 Returns     :  BGP4_SUCCESS/BGP4_FAILURE
****************************************************************************/
                               INT1
                               Bgp4StoreImportList (tTMO_HASH_TABLE *
                                                    pImportList,
                                                    tRouteProfile * pRtProfile)
                               {
                               if ((pRtProfile == NULL)
                                   || (pImportList == NULL))
                               {
                               return BGP4_FAILURE;}

                               Bgp4HashTblDeleteEntry (pImportList, pRtProfile,
                                                       BGP4_FALSE);
                               Bgp4HashTblAddEntry (pImportList, pRtProfile, 0);
                               return BGP4_SUCCESS;}

/*****************************************************************************/
/* Function        : Bgp4ProcessImportList                                   */
/* Description     : This procedure sends the BGP4 Update messages to        */
/*                   BGP4 peers using import list as a feasible/withdrawn    */
/*                   list depanding upon the input mask.                     */
/* Input           : Mask describing the action to be performed.             */
/* Output          : None                                                    */
/* Returns         : BGP4_TRUE, if processing is complete                    */
/*                   BGP4_FALSE, otherwise                                   */
/*****************************************************************************/
                               INT4
                               Bgp4ProcessImportList (UINT4 u4RedisMask,
                                                      UINT4 u4VrfId)
                               {
                               /* For IGP Metric set event, the routes present in the Static, RIP and OSPF
                                * import list should be processed again with the new Metric value. For
                                * RRD_DISABLE event, the routes in all Import List should be removed. For
                                * Specific Protocol Disable Event, the routes in the corresponding
                                * protocol Import List needs to be removed. */
                               if (u4RedisMask == BGP4_IGP_METRIC_SET_EVENT)
                               {
                               Bgp4DshCopyDLLToSLL (BGP4_REDIST_LIST (u4VrfId),
                                                    BGP4_STATIC_IMPORT_LIST
                                                    (u4VrfId),
                                                    BGP4_PROTOCOL_LIST_INDEX,
                                                    BGP4_RT_IN_REDIST_LIST,
                                                    BGP4_PREPEND, u4VrfId, 0);
                               Bgp4DshCopyDLLToSLL (BGP4_REDIST_LIST (u4VrfId),
                                                    BGP4_RIP_IMPORT_LIST
                                                    (u4VrfId),
                                                    BGP4_PROTOCOL_LIST_INDEX,
                                                    BGP4_RT_IN_REDIST_LIST,
                                                    BGP4_PREPEND, u4VrfId, 0);
                               Bgp4DshCopyDLLToSLL (BGP4_REDIST_LIST (u4VrfId),
                                                    BGP4_OSPF_IMPORT_LIST
                                                    (u4VrfId),
                                                    BGP4_PROTOCOL_LIST_INDEX,
                                                    BGP4_RT_IN_REDIST_LIST,
                                                    BGP4_PREPEND, u4VrfId, 0);
                               Bgp4DshCopyDLLToSLL (BGP4_REDIST_LIST (u4VrfId),
                                                    BGP4_ISISL1_IMPORT_LIST
                                                    (u4VrfId),
                                                    BGP4_PROTOCOL_LIST_INDEX,
                                                    BGP4_RT_IN_REDIST_LIST,
                                                    BGP4_PREPEND, u4VrfId, 0);
                               Bgp4DshCopyDLLToSLL (BGP4_REDIST_LIST (u4VrfId),
                                                    BGP4_ISISL2_IMPORT_LIST
                                                    (u4VrfId),
                                                    BGP4_PROTOCOL_LIST_INDEX,
                                                    BGP4_RT_IN_REDIST_LIST,
                                                    BGP4_PREPEND, u4VrfId, 0);
                               return BGP4_TRUE;}

                               if (u4RedisMask == BGP4_RRD_DISABLE)
                               {
                               Bgp4DshCopyDLLToSLL (BGP4_REDIST_LIST (u4VrfId),
                                                    BGP4_STATIC_IMPORT_LIST
                                                    (u4VrfId),
                                                    BGP4_PROTOCOL_LIST_INDEX,
                                                    (BGP4_RT_IN_REDIST_LIST |
                                                     BGP4_RT_WITHDRAWN), 0,
                                                    u4VrfId, 0);
                               Bgp4DshCopyDLLToSLL (BGP4_REDIST_LIST (u4VrfId),
                                                    BGP4_RIP_IMPORT_LIST
                                                    (u4VrfId),
                                                    BGP4_PROTOCOL_LIST_INDEX,
                                                    (BGP4_RT_IN_REDIST_LIST |
                                                     BGP4_RT_WITHDRAWN), 0,
                                                    u4VrfId, 0);
                               Bgp4DshCopyDLLToSLL (BGP4_REDIST_LIST (u4VrfId),
                                                    BGP4_OSPF_IMPORT_LIST
                                                    (u4VrfId),
                                                    BGP4_PROTOCOL_LIST_INDEX,
                                                    (BGP4_RT_IN_REDIST_LIST |
                                                     BGP4_RT_WITHDRAWN), 0,
                                                    u4VrfId, 0);
                               Bgp4DshCopyDLLToSLL (BGP4_REDIST_LIST (u4VrfId),
                                                    BGP4_DIRECT_IMPORT_LIST
                                                    (u4VrfId),
                                                    BGP4_PROTOCOL_LIST_INDEX,
                                                    (BGP4_RT_IN_REDIST_LIST |
                                                     BGP4_RT_WITHDRAWN), 0,
                                                    u4VrfId, 0);
                               Bgp4DshCopyDLLToSLL (BGP4_REDIST_LIST (u4VrfId),
                                                    BGP4_ISISL1_IMPORT_LIST
                                                    (u4VrfId),
                                                    BGP4_PROTOCOL_LIST_INDEX,
                                                    (BGP4_RT_IN_REDIST_LIST |
                                                     BGP4_RT_WITHDRAWN), 0,
                                                    u4VrfId, 0);
                               Bgp4DshCopyDLLToSLL (BGP4_REDIST_LIST (u4VrfId),
                                                    BGP4_ISISL2_IMPORT_LIST
                                                    (u4VrfId),
                                                    BGP4_PROTOCOL_LIST_INDEX,
                                                    (BGP4_RT_IN_REDIST_LIST |
                                                     BGP4_RT_WITHDRAWN), 0,
                                                    u4VrfId, 0);}
                               else
                               {
                               if ((u4RedisMask & BGP4_IMPORT_STATIC) ==
                                   BGP4_IMPORT_STATIC)
                               {
                               Bgp4DshCopyDLLToSLL (BGP4_REDIST_LIST (u4VrfId),
                                                    BGP4_STATIC_IMPORT_LIST
                                                    (u4VrfId),
                                                    BGP4_PROTOCOL_LIST_INDEX,
                                                    (BGP4_RT_IN_REDIST_LIST |
                                                     BGP4_RT_WITHDRAWN), 0,
                                                    u4VrfId, 0);}
                               if ((u4RedisMask & BGP4_IMPORT_OSPF) ==
                                   BGP4_IMPORT_OSPF)
                               {
                               Bgp4DshCopyDLLToSLL (BGP4_REDIST_LIST (u4VrfId),
                                                    BGP4_OSPF_IMPORT_LIST
                                                    (u4VrfId),
                                                    BGP4_PROTOCOL_LIST_INDEX,
                                                    (BGP4_RT_IN_REDIST_LIST |
                                                     BGP4_RT_WITHDRAWN), 0,
                                                    u4VrfId, 0);}
                               else
                               if ((u4RedisMask & BGP4_MATCH_ALL))
                               {
                               Bgp4DshCopyDLLToSLL (BGP4_REDIST_LIST (u4VrfId),
                                                    BGP4_OSPF_IMPORT_LIST
                                                    (u4VrfId),
                                                    BGP4_PROTOCOL_LIST_INDEX,
                                                    (BGP4_RT_IN_REDIST_LIST |
                                                     BGP4_RT_WITHDRAWN), 0,
                                                    u4VrfId, u4RedisMask);}
                               if ((u4RedisMask & BGP4_IMPORT_RIP) ==
                                   BGP4_IMPORT_RIP)
                               {
                               Bgp4DshCopyDLLToSLL (BGP4_REDIST_LIST (u4VrfId),
                                                    BGP4_RIP_IMPORT_LIST
                                                    (u4VrfId),
                                                    BGP4_PROTOCOL_LIST_INDEX,
                                                    (BGP4_RT_IN_REDIST_LIST |
                                                     BGP4_RT_WITHDRAWN), 0,
                                                    u4VrfId, 0);}
                               if ((u4RedisMask & BGP4_IMPORT_DIRECT) ==
                                   BGP4_IMPORT_DIRECT)
                               {
                               Bgp4DshCopyDLLToSLL (BGP4_REDIST_LIST (u4VrfId),
                                                    BGP4_DIRECT_IMPORT_LIST
                                                    (u4VrfId),
                                                    BGP4_PROTOCOL_LIST_INDEX,
                                                    (BGP4_RT_IN_REDIST_LIST |
                                                     BGP4_RT_WITHDRAWN), 0,
                                                    u4VrfId, 0);}
                               if ((u4RedisMask & BGP4_IMPORT_ISISL1) ==
                                   BGP4_IMPORT_ISISL1)
                               {
                               Bgp4DshCopyDLLToSLL (BGP4_REDIST_LIST (u4VrfId),
                                                    BGP4_ISISL1_IMPORT_LIST
                                                    (u4VrfId),
                                                    BGP4_PROTOCOL_LIST_INDEX,
                                                    (BGP4_RT_IN_REDIST_LIST |
                                                     BGP4_RT_WITHDRAWN), 0,
                                                    u4VrfId, 0);}
                               if ((u4RedisMask & BGP4_IMPORT_ISISL2) ==
                                   BGP4_IMPORT_ISISL2)
                               {
                               Bgp4DshCopyDLLToSLL (BGP4_REDIST_LIST (u4VrfId),
                                                    BGP4_ISISL2_IMPORT_LIST
                                                    (u4VrfId),
                                                    BGP4_PROTOCOL_LIST_INDEX,
                                                    (BGP4_RT_IN_REDIST_LIST |
                                                     BGP4_RT_WITHDRAWN), 0,
                                                    u4VrfId, 0);}
                               }
                               return BGP4_TRUE;}

/******************************************************************************/
/* Function Name : Bgp4CreateIgpMetricTable                                   */
/* Description   : This function create the IGP metric Hash table.            */
/* Input(s)      : None                                                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/

                               INT4 Bgp4CreateIgpMetricTable (UINT4 u4CxtId)
                               {
                               /* HASH TBL creation for Igp Metric Table */
                               BGP4_NEXTHOP_METRIC_TBL (u4CxtId) =
                               TMO_HASH_Create_Table
                               (BGP4_MAX_PREFIX_HASH_TBL_SIZE, NULL, FALSE);
                               if (BGP4_NEXTHOP_METRIC_TBL (u4CxtId) == NULL)
                               {
                               BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                         BGP4_ALL_FAILURE_TRC |
                                         BGP4_OS_RESOURCE_TRC |
                                         BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                                         "\tBgp4CreateIgpMetricTable() : CREATING HASH TABLE For"
                                         "IgpMetricEntry FAILED !!!\n");
                               return (BGP4_FAILURE);}
                               return (BGP4_SUCCESS);}

/******************************************************************************/
/* Function Name : Bgp4DeleteIgpMetricTable                                   */
/* Description   : This function delete the IGP metric Hash table.            */
/* Input(s)      : None                                                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/

                               INT4 Bgp4DeleteIgpMetricTable (UINT4 u4Context)
                               {
                               UINT4 u4HashKey;
                               tBgp4IgpMetricEntry * pIgpMetricEntry = NULL;
                               tBgp4IgpMetricEntry * pTmpIgpMetricEntry = NULL;
                               /* Delete the hash table Bgp4NexthopMetricTbl */
                               if (BGP4_NEXTHOP_METRIC_TBL (u4Context) != NULL)
                               {
                               TMO_HASH_Scan_Table (BGP4_NEXTHOP_METRIC_TBL
                                                    (u4Context), u4HashKey)
                               {
                               BGP4_HASH_DYN_Scan_Bucket
                               (BGP4_NEXTHOP_METRIC_TBL (u4Context), u4HashKey,
                                pIgpMetricEntry, pTmpIgpMetricEntry,
                                tBgp4IgpMetricEntry *)
                               {
                               TMO_HASH_Delete_Node (BGP4_NEXTHOP_METRIC_TBL
                                                     (u4Context),
                                                     &pIgpMetricEntry->
                                                     NextIgpMetricEntry,
                                                     u4HashKey);
                               Bgp4ReleaseIgpMetricEntry (pIgpMetricEntry);}
                               }
                               }

                               return (BGP4_SUCCESS);}

/****************************************************************************/
/* Function    :  Bgp4IgpMetricTblAddEntry                                  */
/* Description :  This function adds a new entry to the given link next hop */
/*                in the Nexthop metrics table.                             */
/* Input       :  u4VrfId - VRF identifier                                  */
/*                LinkNexthop - Link Nexthop Address                        */
/* Output      :  None                                                      */
/* Returns     :  Pointer to the new entry added for this next hop.         */
/*                NULL - if any failure.                                    */
/****************************************************************************/
                               tBgp4IgpMetricEntry *
                               Bgp4IgpMetricTblAddEntry (UINT4 u4VrfId,
                                                         tAddrPrefix
                                                         LinkNexthop)
                               {
                               tBgp4IgpMetricEntry * pIgpMetricEntry = NULL;
                               UINT4 u4HashKey = 0;
                               INT4 i4CreateNodeStatus;
                               BGP4_GET_NH_HASH_KEY (u4VrfId, LinkNexthop,
                                                     u4HashKey);
                               i4CreateNodeStatus =
                               Bgp4AllocateIgpMetricEntry (&pIgpMetricEntry);
                               if (i4CreateNodeStatus == BGP4_FAILURE)
                               {
                               return ((tBgp4IgpMetricEntry *) NULL);}

                               TMO_HASH_Add_Node (BGP4_NEXTHOP_METRIC_TBL
                                                  (u4VrfId),
                                                  &pIgpMetricEntry->
                                                  NextIgpMetricEntry, u4HashKey,
                                                  NULL);
                               Bgp4CopyAddrPrefixStruct (&
                                                         (BGP4_NH_METRIC_NEXTHOP_INFO
                                                          (pIgpMetricEntry)),
                                                         LinkNexthop);
                               BGP4_NH_METRIC_VRFID (pIgpMetricEntry) = u4VrfId;
                               return (pIgpMetricEntry);}

/****************************************************************************/
/* Function    :  Bgp4IgpMetricTblDeleteEntry                               */
/* Description :  This function deletes the existint entry to the given     */
/*                link next hop from the Nexthop metrics table.             */
/* Input       :  u4VrfId - VRF identifier                                  */
/*                LinkNexthop - Link Nexthop Address                        */
/* Output      :  None                                                      */
/* Returns     :  BGP4_SUCCESS -  if entry removed. Else returns.           */
/*                BGP4_FAILURE -  other wise.                               */
/****************************************************************************/
                               INT4
                               Bgp4IgpMetricTblDeleteEntry (UINT4 u4VrfId,
                                                            tAddrPrefix
                                                            LinkNexthop)
                               {
                               tBgp4IgpMetricEntry * pDelIgpMetricEntry = NULL;
                               tBgp4IgpMetricEntry * pTmpIgpMetricEntry = NULL;
                               UINT4 u4HashKey = 0;
                               BGP4_GET_NH_HASH_KEY (u4VrfId, LinkNexthop,
                                                     u4HashKey);
                               BGP4_HASH_DYN_Scan_Bucket
                               (BGP4_NEXTHOP_METRIC_TBL (u4VrfId), u4HashKey,
                                pDelIgpMetricEntry, pTmpIgpMetricEntry,
                                tBgp4IgpMetricEntry *)
                               {
                               if (BGP4_NH_METRIC_VRFID (pDelIgpMetricEntry) !=
                                   u4VrfId)
                               {
                               continue;}
                               if ((PrefixMatch
                                    (BGP4_NH_METRIC_NEXTHOP_INFO
                                     (pDelIgpMetricEntry),
                                     LinkNexthop)) == BGP4_TRUE)
                               {
                               TMO_HASH_Delete_Node (BGP4_NEXTHOP_METRIC_TBL
                                                     (u4VrfId),
                                                     &pDelIgpMetricEntry->
                                                     NextIgpMetricEntry,
                                                     u4HashKey);
                               Bgp4ReleaseIgpMetricEntry (pDelIgpMetricEntry);}
                               }

                               return (BGP4_SUCCESS);}

/****************************************************************************/
/* Function    :  Bgp4IgpMetricTblGetEntry                                  */
/* Description :  This function searches for an entry in the Peer Reflector */
/*                table correspond to the given input                       */
/* Input       :  u4VrfId - VRF identifier                                  */
/*                LinkNexthop - Link Nexthop Address                        */
/* Output      :  pIgpMetricEntry -  the entry in IGP Metric Table          */
/* Returns     :  BGP4_TRUE - if the entry is present.                      */
/*                BGP4_FALSE - if the entry is not present.                 */
/****************************************************************************/
                               UINT1
                               Bgp4IgpMetricTblGetEntry (UINT4 u4VrfId,
                                                         tAddrPrefix
                                                         LinkNexthop,
                                                         tBgp4IgpMetricEntry **
                                                         ppIgpMetricEntry)
                               {
                               UINT4 u4HashKey = 0x0;
                               UINT1 u1EntryFound = BGP4_FALSE;
                               tBgp4IgpMetricEntry * pTmpIgpMetricEntry = NULL;
                               BGP4_GET_NH_HASH_KEY (u4VrfId, LinkNexthop,
                                                     u4HashKey);
                               TMO_HASH_Scan_Bucket (BGP4_NEXTHOP_METRIC_TBL
                                                     (u4VrfId), u4HashKey,
                                                     pTmpIgpMetricEntry,
                                                     tBgp4IgpMetricEntry *)
                               {
                               if (BGP4_NH_METRIC_VRFID (pTmpIgpMetricEntry) !=
                                   u4VrfId)
                               {
                               continue;}
                               if ((PrefixMatch
                                    (BGP4_NH_METRIC_NEXTHOP_INFO
                                     (pTmpIgpMetricEntry),
                                     LinkNexthop)) == BGP4_TRUE)
                               {
                               u1EntryFound = BGP4_TRUE; break;}
                               }
                               *ppIgpMetricEntry = pTmpIgpMetricEntry;
                               return u1EntryFound;}

/****************************************************************************/
/* Function    :  Bgp4IgpMetricTblGetNextEntry                              */
/* Description :  This function searches for an entry in the Peer Reflector */
/*                table correspond to the given input and return the next   */
/*                entry if present.                                         */
/* Input       :  pIgpMetricEntry - Pointer to the IgpMetric Entry          */
/* Output      :  ppNextIgpMetricEntry -  Pointer to next IgpMetric entry.  */
/* Returns     :  BGP4_TRUE - if the entry is present.                      */
/*                BGP4_FALSE - if the entry is not present.                 */
/****************************************************************************/
                               UINT1
                               Bgp4IgpMetricTblGetNextEntry (tBgp4IgpMetricEntry
                                                             * pIgpMetricEntry,
                                                             tBgp4IgpMetricEntry
                                                             **
                                                             ppNextIgpMetricEntry)
                               {
                               tBgp4IgpMetricEntry * pTmpIgpMetricEntry = NULL;
                               tAddrPrefix LinkNexthop;
                               UINT4 u4HashKey = 0x0;
                               UINT1 u1EntryFound = BGP4_FALSE;
                               LinkNexthop =
                               BGP4_NH_METRIC_NEXTHOP_INFO (pIgpMetricEntry);
                               BGP4_GET_NH_HASH_KEY (BGP4_NH_METRIC_VRFID
                                                     (pIgpMetricEntry),
                                                     LinkNexthop, u4HashKey);
                               TMO_HASH_Scan_Bucket (BGP4_NEXTHOP_METRIC_TBL
                                                     (pIgpMetricEntry->u4VrfId),
                                                     u4HashKey,
                                                     pTmpIgpMetricEntry,
                                                     tBgp4IgpMetricEntry *)
                               {
                               if (u1EntryFound == BGP4_FALSE)
                               {
                               if (BGP4_NH_METRIC_VRFID (pTmpIgpMetricEntry) !=
                                   BGP4_NH_METRIC_VRFID (pIgpMetricEntry))
                               {
                               continue;}
                               if ((PrefixMatch
                                    (BGP4_NH_METRIC_NEXTHOP_INFO
                                     (pTmpIgpMetricEntry),
                                     LinkNexthop)) == BGP4_TRUE)
                               {
                               u1EntryFound = BGP4_TRUE; continue;}
                               }
                               else
                               {
                               *ppNextIgpMetricEntry = pTmpIgpMetricEntry;
                               return BGP4_TRUE;}
                               }

                               if (u1EntryFound == BGP4_TRUE)
                               {
                               /* Given input route is present in the corresponding bucket. But next
                                * route is not present in this bucket. Get the first entry from the
                                * remaining buckets */
                               BGP4_HASH_Scan_Table (BGP4_NEXTHOP_METRIC_TBL
                                                     (pIgpMetricEntry->u4VrfId),
                                                     u4HashKey,
                                                     (u4HashKey +
                                                      BGP4_ONE_BYTE))
                               {
                               pTmpIgpMetricEntry = (tBgp4IgpMetricEntry *)
                               TMO_HASH_Get_First_Bucket_Node
                               (BGP4_NEXTHOP_METRIC_TBL
                                (pIgpMetricEntry->u4VrfId), u4HashKey);
                               if (pTmpIgpMetricEntry != NULL)
                               {
                               *ppNextIgpMetricEntry = pTmpIgpMetricEntry;
                               return BGP4_TRUE;}
                               }
                               }

                               *ppNextIgpMetricEntry = NULL;
                               return (BGP4_FALSE);}

/*****************************************************************************/
/* Function Name : Bgp4AddRouteToIdentNextHopList                            */
/* Description   : This routine adds the given route to the next hop entries */
/*                 route list.                                               */
/* Input(s)      : Pointer to the Route profile that needs to be added       */
/*                 (pRtProfile)                                              */
/*                 u4VrfId - VRF identifier                                  */
/*                 Next Hop address of the route to be added (u4NextHop)     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS - if route added                             */
/*                 BGP4_FAILURE - otherwise                                  */
/*****************************************************************************/
                               INT4
                               Bgp4AddRouteToIdentNextHopList (tRouteProfile *
                                                               pRtProfile,
                                                               UINT4 u4VrfId,
                                                               tAddrPrefix
                                                               NextHop)
                               {
                               tBgp4IgpMetricEntry * pIgpMetricEntry = NULL;
                               INT4 i4RetVal = BGP4_FAILURE;
                               Bgp4IgpMetricTblGetEntry (u4VrfId, NextHop,
                                                         &pIgpMetricEntry);
                               if (pIgpMetricEntry != NULL)
                               {
                               /* While adding the route to the Next-Hop list ensure that the route
                                * is always added to the beginning of the list. This will ensure that
                                * these routes need not be processed again during Next-Hop Resolution
                                * scan. */
                               i4RetVal =
                               Bgp4DshLinkRouteToList (BGP4_NH_METRIC_ROUTES
                                                       (pIgpMetricEntry),
                                                       pRtProfile, BGP4_PREPEND,
                                                       BGP4_NEXTHOP_LIST_INDEX);}
                               return i4RetVal;}

/*****************************************************************************/
/* Function Name : Bgp4RemoveRouteFromIdentNextHopList                       */
/* Description   : This routine finds a matching route from the list of      */
/*                 identical nexthop routes and removes that route from the  */
/*                 list.                                                     */
/* Input(s)      : Pointer to the Route profile that needs to be removed.    */
/*                 (pRtProfile)                                              */
/*                 u4VrfId - VRF identifier                                  */
/*                 Next Hop address of the route to be removed (NextHop)     */
/* Output(s)     : None.                                                     */
/* Return(s)     : Removed route profile if match occurs,                    */
/*                 NULL if matching entry is not found.                      */
/*****************************************************************************/
                               tRouteProfile *
                               Bgp4RemoveRouteFromIdentNextHopList
                               (tRouteProfile * pRtProfile, UINT4 u4VrfId,
                                tAddrPrefix NextHop)
                               {
                               tBgp4IgpMetricEntry * pIgpMetricEntry = NULL;
                               tRouteProfile * pDelRoute = NULL;
                               Bgp4IgpMetricTblGetEntry (u4VrfId, NextHop,
                                                         &pIgpMetricEntry);
                               if (pIgpMetricEntry != NULL)
                               {
                               pDelRoute =
                               Bgp4DshDelinkRouteFromList (BGP4_NH_METRIC_ROUTES
                                                           (pIgpMetricEntry),
                                                           pRtProfile,
                                                           BGP4_NEXTHOP_LIST_INDEX);}

                               return pDelRoute;}

/******************************************************************************/
/* Function Name : Bgp4CreateHashTable                                        */
/* Description   : This function create the Hash Table.                       */
/* Input(s)      : size of hash table                                         */
/* Output(s)     : None.                                                      */
/* Return(s)     : pointer to hash table created                              */
/*                 NULL, if it fails.                                         */
/******************************************************************************/
                               tTMO_HASH_TABLE *
                               Bgp4CreateHashTable (UINT4 u4HashSize)
                               {
                               tTMO_HASH_TABLE * pList = NULL;
                               /* HASH TBL creation for Igp import Table */
                               pList =
                               TMO_HASH_Create_Table (u4HashSize, NULL, FALSE);
                               if (pList == NULL)
                               {
                               return (NULL);}
                               return (pList);}

/******************************************************************************/
/* Function Name : Bgp4ClearHashTable                                         */
/* Description   : This function clears the given input Hash Table.           */
/* Input(s)      : Hash Table that needs to be cleared.                       */
/*               : u4NodeCnt - Number of nodes to be removed. If 0, then      */
/*               :             entire HASH Table needs to be cleared.         */
/*               : u4Flag - Flag to identify the Peer Advertisement hash table*/
/*               :          BGP4_FALSE - Peer Advertised Feasible/ Withdrawn  */
/*               :                      hash table                            */
/*               :          BGP4_TRUE - other peer hash table                 */
/* Output(s)     : None.                                                      */
/* Return(s)     : Number of nodes removed.                                   */
/******************************************************************************/
                               UINT4
                               Bgp4ClearHashTable (tTMO_HASH_TABLE * pHashTbl,
                                                   UINT4 u4NodeCnt,
                                                   UINT4 u4Flag)
                               {
                               tLinkNode * pLinkNode = NULL;
                               tLinkNode * pTmpLinkNode = NULL;
                               UINT4 u4HashKey; UINT4 u4Count = 0;
                               /* Clear the hash table */
                               if (pHashTbl != NULL)
                               {
                               TMO_HASH_Scan_Table (pHashTbl, u4HashKey)
                               {
                               BGP4_HASH_DYN_Scan_Bucket (pHashTbl, u4HashKey,
                                                          pLinkNode,
                                                          pTmpLinkNode,
                                                          tLinkNode *)
                               {
                               TMO_HASH_Delete_Node (pHashTbl,
                                                     &pLinkNode->TSNext,
                                                     u4HashKey);
                               if (u4Flag == BGP4_TRUE)
                               {
                               Bgp4DshReleaseLinkNode (pLinkNode);}
                               else
                               {
                               Bgp4DshReleaseAdvRtLinkNode ((tAdvRtLinkNode *)
                                                            pLinkNode);}
                               u4Count++;
                               if ((u4NodeCnt > 0) && (u4Count >= u4NodeCnt))
                               {
                               if (TMO_SLL_Count
                                   (&((pHashTbl)->HashList[u4HashKey])) == 0)
                               {
                               TMO_SLL_Init (&
                                             ((pHashTbl)->
                                              HashList[u4HashKey]));}
                               return (u4Count);}
                               }
                               TMO_SLL_Init (&
                                             ((pHashTbl)->
                                              HashList[u4HashKey]));}
                               }
                               return (u4Count);}

/****************************************************************************/
/* Function    :  Bgp4HashTblAddEntry                                       */
/* Description :  This function adds a new entry for the given route        */
/*                in the given hash table.                                  */
/* Input       :  pList - HASH TABLE pointer                                */
/*                pRtProfile - pointer to the route-profile                 */
/*                u4Flags - Flag indicating how to add the route.           */
/*                Depending upon the flag, it adds the route into the       */
/*                list by grouping routes with identical Routing            */
/*                informations, or append the route to the end of the       */
/*                list, or add the route to the start of the list           */
/*                u4Flag == 1, group and add the route to list.             */
/*                u4Flag == 2, add the route as the first node in the list  */
/*                else append the route to the end of the list.             */
/* Output      :  None                                                      */
/* Returns     :  BGP4_SUCCESS - if operation is a success                  */
/*                BGP4_FAILURE - if any failure.                            */
/****************************************************************************/
                               INT4
                               Bgp4HashTblAddEntry (tTMO_HASH_TABLE * pHashTab,
                                                    tRouteProfile * pRtProfile,
                                                    UINT4 u4Flag)
                               {
                               UINT4 u4HashKey = 0;
                               INT4 i4RetVal;
                               Bgp4GetRouteHashIndex
                               (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
                                (UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                &u4HashKey);
                               i4RetVal =
                               Bgp4DshAddRouteToList (&pHashTab->
                                                      HashList[u4HashKey],
                                                      pRtProfile, u4Flag);
                               return (i4RetVal);}

/****************************************************************************/
/* Function    :  Bgp4PeerRtAdvHashTblAddEntry                              */
/* Description :  This function adds a new entry for the given route        */
/*                in the given Peer Route Advertisement hash table.         */
/* Input       :  pList - HASH TABLE pointer                                */
/*                pRtProfile - pointer to the route-profile                 */
/*                u4Flag - Flag indicating how to add the route.            */
/*                Depending upon the flag, it adds the route into the       */
/*                list by grouping routes with identical Routing            */
/*                informations, or append the route to the end of the       */
/*                list, or add the route to the start of the list           */
/*                u4Flag == BGP4_TIMESTAMP (3), increasing order of update  */
/*                          sent time; group and add the route to list.     */
/*                       For any other value, append to the end of the list */
/*                u4UpdSentTime - the timestamp at which the route was      */
/*                                advertised                                */
/* Output      :  None                                                      */
/* Returns     :  BGP4_SUCCESS - if operation is a success                  */
/*                BGP4_FAILURE - if any failure.                            */
/****************************************************************************/
                               INT4
                               Bgp4PeerRtAdvHashTblAddEntry (tTMO_HASH_TABLE *
                                                             pHashTab,
                                                             tRouteProfile *
                                                             pRtProfile,
                                                             UINT4 u4Flag,
                                                             UINT4
                                                             u4UpdSentTime)
                               {
                               UINT4 u4HashKey = 0;
                               INT4 i4RetVal;
                               Bgp4GetRouteHashIndex
                               (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
                                (UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                &u4HashKey);
                               i4RetVal =
                               Bgp4DshAddRouteToPeerAdvtList (&pHashTab->
                                                              HashList
                                                              [u4HashKey],
                                                              pRtProfile,
                                                              u4Flag,
                                                              u4UpdSentTime);
                               return (i4RetVal);}

/****************************************************************************/
/* Function    :  Bgp4HashTblDeleteEntry                                    */
/* Description :  This function deletes the existing entry to the given     */
/*                route from the given hash table.                          */
/* Input       :  pHashTbl - HASH TABLE pointer                             */
/*                pRtProfile - pointer to the route-profile                 */
/*                u4Flags - if == BGP4_TRUE, then Hash table is peer table  */
/*                        - else the Hash table is general hash table.      */
/* Output      :  None                                                      */
/* Return(s)     : Removed route profile if match occurs,                   */
/*                 NULL if matching entry is not found.                     */
/****************************************************************************/
                               tRouteProfile *
                               Bgp4HashTblDeleteEntry (tTMO_HASH_TABLE *
                                                       pHashTbl,
                                                       tRouteProfile *
                                                       pRtProfile,
                                                       UINT4 u4Flags)
                               {
                               tRouteProfile * pRouteInfo = NULL;
                               UINT4 u4HashKey = 0;
                               Bgp4GetRouteHashIndex
                               (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
                                (UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                &u4HashKey);
                               /* Remove the route from the Hash Table List corresponding to Hash Key */
                               if (u4Flags == BGP4_TRUE)
                               {
                               pRouteInfo = Bgp4RibhRemoveRouteFromPeerList
                               (&pHashTbl->HashList[u4HashKey], pRtProfile);}
                               else
                               {
                               pRouteInfo =
                               Bgp4DshRemoveRouteFromList (&pHashTbl->
                                                           HashList[u4HashKey],
                                                           pRtProfile);}
                               return (pRouteInfo);}

/****************************************************************************/
/* Function    :  Bgp4PeerRtAdvHashTblDeleteEntry                           */
/* Description :  This function deletes the existing entry to the given     */
/*                route from the given hash table.                          */
/* Input       :  pHashTbl - HASH TABLE pointer                             */
/*                pRtProfile - pointer to the route-profile                 */
/* Output      :  pu4UpdSentTime - Route update sent time                   */
/* Return(s)     : Removed route profile if match occurs,                   */
/*                 NULL if matching entry is not found.                     */
/****************************************************************************/
                               tRouteProfile *
                               Bgp4PeerRtAdvHashTblDeleteEntry (tTMO_HASH_TABLE
                                                                * pHashTbl,
                                                                tRouteProfile *
                                                                pRtProfile,
                                                                UINT4
                                                                *pu4UpdSentTime)
                               {
                               tRouteProfile * pRouteInfo = NULL;
                               UINT4 u4HashKey = 0;
                               Bgp4GetRouteHashIndex
                               (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
                                (UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                &u4HashKey);
                               /* Remove the route from the Hash Table List corresponding to Hash Key */
                               pRouteInfo = Bgp4RibhRemoveRtFromPeerAdvtList
                               (&pHashTbl->HashList[u4HashKey], pRtProfile,
                                pu4UpdSentTime); return (pRouteInfo);}

/****************************************************************************/
/* Function    :  Bgp4PeerRtAdvHashTblGetEntry                              */
/* Description :  This function searches for an entry in the given Hash     */
/*                table for the particular route                            */
/* Input       :  pHashTbl - HASH TABLE pointer                             */
/*                pRtProfile - pointer to the route-profile                 */
/*                u4Flags - if == BGP4_TRUE, then Hash table is peer table  */
/*                        - else the Hash table is general hash table.      */
/* Output      :  u4UpdSentTime - update sent time                          */
/* Returns     :  BGP4_TRUE - if the entry is present.                      */
/*                BGP4_FALSE - if the entry is not present.                 */
/****************************************************************************/
                               tRouteProfile *
                               Bgp4PeerRtAdvHashTblGetEntry (tTMO_HASH_TABLE *
                                                             pHashTbl,
                                                             tRouteProfile *
                                                             pRtProfile,
                                                             UINT4
                                                             *pu4UpdSentTime)
                               {
                               UINT4 u4HashKey = 0x0;
                               tRouteProfile * pRouteInfo = NULL;
                               Bgp4GetRouteHashIndex
                               (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
                                (UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                &u4HashKey);
                               /* Get the route from the Hash Table List corresponding to Hash Key */
                               pRouteInfo = Bgp4RibhGetRtFromPeerAdvRtList
                               (&pHashTbl->HashList[u4HashKey], pRtProfile,
                                pu4UpdSentTime); return (pRouteInfo);}

/****************************************************************************/
/* Function    :  Bgp4HashTblGetEntry                                       */
/* Description :  This function searches for an entry in the given Hash     */
/*                table for the particular route                            */
/* Input       :  pHashTbl - HASH TABLE pointer                             */
/*                pRtProfile - pointer to the route-profile                 */
/*                u4Flags - if == BGP4_TRUE, then Hash table is peer table  */
/*                        - else the Hash table is general hash table.      */
/* Output      :  NONE                                                      */
/* Returns     :  BGP4_TRUE - if the entry is present.                      */
/*                BGP4_FALSE - if the entry is not present.                 */
/****************************************************************************/
                               tRouteProfile *
                               Bgp4HashTblGetEntry (tTMO_HASH_TABLE * pHashTbl,
                                                    tRouteProfile * pRtProfile,
                                                    UINT4 u4Flags)
                               {
                               UINT4 u4HashKey = 0x0;
                               tRouteProfile * pRouteInfo = NULL;
                               Bgp4GetRouteHashIndex
                               (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
                                (UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                &u4HashKey);
                               /* Get the route from the Hash Table List corresponding to Hash Key */
                               if (u4Flags == BGP4_TRUE)
                               {
                               pRouteInfo = Bgp4RibhGetRouteFromPeerList
                               (&pHashTbl->HashList[u4HashKey], pRtProfile);}
                               else
                               {
                               pRouteInfo =
                               Bgp4DshGetRouteFromList (&pHashTbl->
                                                        HashList[u4HashKey],
                                                        pRtProfile);}
                               return (pRouteInfo);}

/****************************************************************************/
/* Function    :  Bgp4HashTblGetNextEntry                                   */
/* Description :  This function searches for the next entry in the given    */
/*                hash table for the particular route                       */
/* Input       :  pHashTbl - HASH TABLE pointer                             */
/*                pRtProfile - pointer to the route-profile.                */
/*                           - if Null, then return the first entry.        */
/* Output      :  pu1Flag - if BGP4_TRUE means input route is present       */
/*                        - if BGP4_FALSE means input route is not present. */
/* Returns     :  BGP4_TRUE - if the entry is present.                      */
/*                BGP4_FALSE - if the entry is not present.                 */
/****************************************************************************/
                               tRouteProfile *
                               Bgp4HashTblGetNextEntry (tTMO_HASH_TABLE *
                                                        pHashTbl,
                                                        tRouteProfile *
                                                        pRtProfile,
                                                        UINT1 *pu1Flag)
                               {
                               UINT4 u4HashKey = 0x0;
                               tLinkNode * pLinkNode = NULL;
                               tLinkNode * pFoundNode = NULL;
                               tRouteProfile * pRouteInfo = NULL;
                               /* Set the flag to indicate that the input route is not present and update
                                * it once the route is found. */
                               *pu1Flag = BGP4_FALSE; if (pRtProfile == NULL)
                               {
                               TMO_HASH_Scan_Table (pHashTbl, u4HashKey)
                               {
                               pLinkNode = (tLinkNode *)
                               TMO_HASH_Get_First_Bucket_Node (pHashTbl,
                                                               u4HashKey);
                               if (pLinkNode != NULL)
                               {
                               return (pLinkNode->pRouteProfile);}
                               }
                               return (NULL);}

                               Bgp4GetRouteHashIndex
                               (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
                                (UINT1) BGP4_RT_PREFIXLEN (pRtProfile),
                                &u4HashKey);
                               TMO_HASH_Scan_Bucket (pHashTbl, u4HashKey,
                                                     pLinkNode, tLinkNode *)
                               {
                               pRouteInfo = pLinkNode->pRouteProfile;
                               if (*pu1Flag == BGP4_FALSE)
                               {
                               if (((PrefixMatch
                                     (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                      (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
                                      BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                      (BGP4_RT_NET_ADDRESS_INFO (pRouteInfo))))
                                    == BGP4_TRUE)
                                   && (BGP4_RT_PREFIXLEN (pRtProfile) ==
                                       BGP4_RT_PREFIXLEN (pRouteInfo)))
                               {
                               *pu1Flag = BGP4_TRUE;
                               pFoundNode = pLinkNode; continue;}
                               }
                               if ((*pu1Flag == BGP4_TRUE)
                                   && (pFoundNode != pLinkNode))
                               {
                               return (pRouteInfo);}
                               }

                               if (*pu1Flag == BGP4_TRUE)
                               {
                               /* Given input route is present in the corresponding bucket. But next
                                * route is not present in this bucket. Get the first route from the
                                * remaining buckets */
                               BGP4_HASH_Scan_Table (pHashTbl, u4HashKey,
                                                     (u4HashKey +
                                                      BGP4_ONE_BYTE))
                               {
                               pLinkNode = (tLinkNode *)
                               TMO_HASH_Get_First_Bucket_Node (pHashTbl,
                                                               u4HashKey);
                               if (pLinkNode != NULL)
                               {
                               return (pLinkNode->pRouteProfile);}
                               }
                               }
                               return (NULL);}

/******************************************************************************/
/* Function Name : Bgp4DshConcatHashTableToSLL                                */
/* Description   : This function concatenates all buckets to the input SLL    */
/* Input(s)      : None                                                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,              */
/*                 BGP4_FAILURE if it fails.                                  */
/******************************************************************************/
                               INT4
                               Bgp4DshConcatHashTableToSLL (tTMO_SLL * pList,
                                                            tTMO_HASH_TABLE *
                                                            pHashTbl)
                               {
                               UINT4 u4HashKey;
                               if ((pList != NULL) && (pHashTbl != NULL))
                               {
                               TMO_HASH_Scan_Table (pHashTbl, u4HashKey)
                               {
                               if (TMO_SLL_Count
                                   (&((pHashTbl)->HashList[(u4HashKey)])) > 0)
                               {
                               TMO_SLL_Concat (pList,
                                               &((pHashTbl)->
                                                 HashList[(u4HashKey)]));}
                               }
                               }
                               return (BGP4_SUCCESS);}

/*****************************************************************************/
/* Function Name : Bgp4DshCopyDLLToSLL                                       */
/* Description   : This function copy all routes in DLL to the input SLL     */
/* Input(s)      : pDLList - DLL holding the route.                          */
/*               : u4DLLInfo - Info about the DLL in the Route Profile.      */
/*                          BGP4_FIB_UPD_LIST_INDEX  - 1                     */
/*                          BGP4_INT_PEER_LIST_INDEX - 2                     */
/*                          BGP4_EXT_PEER_LIST_INDEX - 3                     */
/*                          BGP4_NEXTHOP_LIST_INDEX  - 4                     */
/*                          BGP4_PROTOCOL_LIST_INDEX - 5                     */
/*                 The FIB/INT/EXT List used the CHANGE_LIST_DLL             */
/*               : u4Flag - Indicate whether any flag need to be set to the  */
/*               : route while adding to the list.                           */
/*               : u4Position - Indiacate whether the route needs to copied  */
/*               : at the beginning or end of the list.                      */
/* Output(s)     : pList - List where all the routes should be copied.       */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,             */
/*                 BGP4_FAILURE if it fails.                                 */
/*****************************************************************************/
                               INT4
                               Bgp4DshCopyDLLToSLL (tTMO_SLL * pList,
                                                    tBGP4_DLL * pDLList,
                                                    UINT4 u4DLLInfo,
                                                    UINT4 u4Flag,
                                                    UINT4 u4Position,
                                                    UINT4 u4VrfId,
                                                    UINT4 u4MatchMask)
                               {
                               tRouteProfile * pRtProfile = NULL;
#ifdef L3VPN
                               tRtInstallVrfInfo * pRtVrfInfo = NULL;
#endif
#ifndef L3VPN
                               UNUSED_PARAM (u4VrfId);
#endif
                               if (u4DLLInfo != BGP4_PROTOCOL_LIST_INDEX)
                               {
                               /* Currently only it has been implemented for PROTO-DLL in Route
                                * Profile entry. If require for other DLL, then update this
                                * function. */
                               return BGP4_FAILURE;}

                               for (pRtProfile = BGP4_DLL_FIRST_ROUTE (pDLList);
                                    pRtProfile != NULL;
                                    pRtProfile =
                                    BGP4_RT_PROTO_LINK_NEXT (pRtProfile))
                               {
#ifdef L3VPN
                               if (u4VrfId == BGP4_DFLT_VRFID)
                               {
                               if ((BGP4_RT_AFI_INFO (pRtProfile) ==
                                    BGP4_INET_AFI_IPV4)
                                   && (BGP4_RT_SAFI_INFO (pRtProfile) ==
                                       BGP4_INET_SAFI_VPNV4_UNICAST))
                               {
                               /* The VRF redistributed routes should be removed by
                                * using no redist in VRF context
                                */
                               continue;}
                               }
                               else
                               {
                               Bgp4Vpnv4GetInstallVrfInfo (pRtProfile, u4VrfId,
                                                           &pRtVrfInfo);
                               if (pRtVrfInfo == NULL)
                               {
                               /* The route is not installed in this particular VRF */
                               continue;}
                               }
#endif
                               if ((u4Flag & BGP4_RT_IN_REDIST_LIST) ==
                                   BGP4_RT_IN_REDIST_LIST)
                               {
                               if (Bgp4CheckForMatchType
                                   (u4MatchMask,
                                    BGP4_RT_METRIC_TYPE (pRtProfile)) ==
                                   BGP4_SUCCESS)
                               {
                               if ((u4Flag & BGP4_RT_WITHDRAWN) ==
                                   BGP4_RT_WITHDRAWN)
                               {
                               BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);}
                               if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                                    BGP4_RT_IN_REDIST_LIST) !=
                                   BGP4_RT_IN_REDIST_LIST)
                               {
                               BGP4_RT_SET_FLAG (pRtProfile,
                                                 BGP4_RT_IN_REDIST_LIST);
                               Bgp4DshDelinkRouteFromChgList (pRtProfile);
                               Bgp4DshAddRouteToList (pList, pRtProfile,
                                                      u4Position);}
                               }
                               }
                               else
                               {
                               BGP4_RT_SET_FLAG (pRtProfile, u4Flag);
                               Bgp4DshAddRouteToList (pList, pRtProfile,
                                                      u4Position);}
                               }
                               return (BGP4_SUCCESS);}

/******************************************************************************/
/* Function Name : Bgp4DshGetHashTableCnt                                     */
/* Description   : This function gets the total no. of elements in the hash   */
/*                 table                                                      */
/* Input(s)      : None                                                       */
/* Output(s)     : None.                                                      */
/* Return(s)     : No. of elements in the hash table                          */
/******************************************************************************/
                               UINT4
                               Bgp4DshGetHashTableCnt (tTMO_HASH_TABLE *
                                                       pHashTbl)
                               {
                               UINT4 u4HashKey; UINT4 u4Cnt = 0;
                               /* Delete the hash table */
                               if (pHashTbl != NULL)
                               {
                               TMO_HASH_Scan_Table (pHashTbl, u4HashKey)
                               {
                               u4Cnt +=
                               TMO_SLL_Count (&
                                              ((pHashTbl)->
                                               HashList[(u4HashKey)]));}
                               }
                               return (u4Cnt);}

/*****************************************************************************/
/* Function Name : Bgp4CheckForMatchType                                     */
/* Description   : This function checks for the ospf metric type falls in 
                   given u4ProtoMask or not                                  */
/* Input(s)      : ProtoMask  -                                              
                   u1MetricType - ospf metric type of the route              */
/* Output(s)     :                                                           */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,             */
/*                 BGP4_FAILURE if it fails.                                 */
/*****************************************************************************/

                               INT4
                               Bgp4CheckForMatchType (UINT4 u4ProtoMask,
                                                      UINT1 u1MetricType)
                               {
                               switch (u1MetricType)
                               {
case BGP4_OSPF_INTRA_AREA:
case BGP4_OSPF_INTER_AREA:

                               if ((u4ProtoMask & BGP4_MATCH_INT) ==
                                   BGP4_MATCH_INT)
                               {
                               return BGP4_SUCCESS;}
break; case BGP4_OSPF_TYPE_1_EXT:
case BGP4_OSPF_TYPE_2_EXT:

                               if ((u4ProtoMask & BGP4_MATCH_EXT) ==
                                   BGP4_MATCH_EXT)
                               {
                               return BGP4_SUCCESS;}
break; case BGP4_OSPF_NSSA_1_EXT:
case BGP4_OSPF_NSSA_2_EXT:

                               if ((u4ProtoMask & BGP4_MATCH_NSSA) ==
                                   BGP4_MATCH_NSSA)
                               {
                               return BGP4_SUCCESS;}
break; default:
                               if (u1MetricType == 0)
                               {
                               /* For NON OSPF types i1MetricType will be zero
                                * So Match need not be done */
                               return BGP4_SUCCESS;}
                               break;}
                               return BGP4_FAILURE;}

/******************************************************************************/
/* Function Name : BgpNetworkIpRtLookup                                       */
/* Description   : This routine adds the specific  route configured via       */
/*                 network command to the redistribution list                 */
/* Input(s)      : Network Address                                            */
/*                 Prefix Len                                                 */
/*                 Set action                                                 */
/* Output(s)     : None                                                       */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                  */
/******************************************************************************/
                               INT4
                               BgpNetworkIpRtLookup (UINT4 u4VrfId,
                                                     tAddrPrefix * pNetworkAddr,
                                                     UINT2 u2PrefixLen,
                                                     INT4 i4Action,
                                                     INT1 i1AppId,
                                                     UINT1 u1Community,
                                                     UINT4 *pulCommunity)
                               {
                               UINT4 u4Mask = 0;
                               INT4 i4RetStatus = 0;
                               UINT4 u4Ipaddr = 0;
                               tNetIpv4RtInfo NetIpRtInfo;
                               tRtInfoQueryMsg RtQuery;
                               tAddrPrefix NextHopInfo;
                               tNetAddress IpAddr; UINT1 u1AdvFlag = 0;
#ifdef BGP4_IPV6_WANTED
                               tNetIpv6RtInfo NetIpv6RtInfo;
                               tIp6Addr IpPrefix;
                               MEMSET (&NetIpv6RtInfo, 0,
                                       sizeof (tNetIpv6RtInfo));
                               MEMSET (&IpPrefix, 0, sizeof (tIp6Addr));
#endif
                               MEMSET (&NextHopInfo, 0, sizeof (tAddrPrefix));
                               MEMSET (&IpAddr, 0, sizeof (tNetAddress));
                               MEMSET (&NetIpRtInfo, 0,
                                       sizeof (tNetIpv4RtInfo));
                               MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
                               switch (pNetworkAddr->u2Afi)
                               {
case BGP4_INET_AFI_IPV4:
                               {

                               u4Mask = Bgp4GetSubnetmask ((UINT1) u2PrefixLen);
                               PTR_FETCH4 (u4Ipaddr, pNetworkAddr->au1Address);
                               RtQuery.u4DestinationIpAddress = u4Ipaddr;
                               RtQuery.u4DestinationSubnetMask = u4Mask;
                               if (i1AppId == BGP4_ALL_APPIDS)
                               {
                               RtQuery.u2AppIds = 0;
                               RtQuery.u1QueryFlag = QUERY_FOR_NEXTHOP;}
                               else
                               if (i1AppId == BGP4_LOCAL_ID)
                               {
                               RtQuery.u2AppIds = RTM_DIRECT_MASK;
                               RtQuery.u1QueryFlag = QUERY_FOR_NEXTHOP;}
                               else
                               {
                               RtQuery.u2AppIds = RTM_RIP_MASK | RTM_OSPF_MASK;
                               RtQuery.u1QueryFlag = QUERY_FOR_DESTINATION;}
                               RtQuery.u4ContextId = u4VrfId;
                               if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) ==
                                   NETIPV4_FAILURE)
                               {
                               BGP4_DBG (BGP4_DBG_ALL,
                                         "\tNetIpv4GetRoute() :  Ip Route Lookup FAILED !!! \n");
                               return BGP4_SUCCESS;}

                               if (NetIpRtInfo.u2RtProto == BGP_ID)
                               {
                               BGP4_DBG (BGP4_DBG_ALL,
                                         "\tThe obtained route from RTM is BGP. So we can ignore it !!! \n");
                               return BGP4_SUCCESS;}

                               PTR_ASSIGN_4
                               (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                                (NextHopInfo), NetIpRtInfo.u4NextHop);
                               NextHopInfo.u2AddressLen = BGP4_IPV4_PREFIX_LEN;
                               NextHopInfo.u2Afi = BGP4_INET_AFI_IPV4;
                               u4Ipaddr = u4Ipaddr & u4Mask;
                               PTR_ASSIGN_4 ((IpAddr.NetAddr.au1Address),
                                             u4Ipaddr);
                               IpAddr.NetAddr.u2Afi = BGP4_INET_FAMILY_IPV4;
                               IpAddr.NetAddr.u2AddressLen =
                               pNetworkAddr->u2AddressLen;
                               IPV4_MASK_TO_MASKLEN (IpAddr.u2PrefixLen,
                                                     NetIpRtInfo.u4DestMask);
                               IpAddr.u2Safi = BGP4_INET_SAFI_UNICAST;}
                               break;
#ifdef BGP4_IPV6_WANTED
case BGP4_INET_AFI_IPV6:
                               {
                               MEMCPY (IpPrefix.u1_addr,
                                       pNetworkAddr->au1Address,
                                       BGP4_IPV6_PREFIX_LEN);
                               i4RetStatus =
                               NetIpv6GetFwdTableRouteEntryInCxt (u4VrfId,
                                                                  &IpPrefix,
                                                                  (UINT1)
                                                                  u2PrefixLen,
                                                                  &NetIpv6RtInfo);
                               if (i4RetStatus == NETIPV6_FAILURE)
                               {
                               return BGP4_SUCCESS;}
                               else
                               {
                               if (NetIpv6RtInfo.i1Proto == IP6_BGP_PROTOID)
                               {
                               BGP4_DBG (BGP4_DBG_ALL,
                                         "\tThe obtained route from RTM is BGP. So we can ignore it !!! \n");
                               return BGP4_SUCCESS;}
                               MEMCPY (NextHopInfo.au1Address,
                                       &(NetIpv6RtInfo.NextHop),
                                       BGP4_IPV6_PREFIX_LEN);
                               NextHopInfo.u2AddressLen = BGP4_IPV6_PREFIX_LEN;
                               NextHopInfo.u2Afi = BGP4_INET_AFI_IPV6;
                               MEMCPY (IpAddr.NetAddr.au1Address,
                                       pNetworkAddr->au1Address,
                                       BGP4_IPV6_PREFIX_LEN);
                               IpAddr.NetAddr.u2Afi = BGP4_INET_AFI_IPV6;
                               IpAddr.NetAddr.u2AddressLen =
                               BGP4_IPV6_PREFIX_LEN;
                               IpAddr.u2PrefixLen = NetIpv6RtInfo.u1Prefixlen;
                               IpAddr.u2Safi = BGP4_INET_SAFI_UNICAST;}
                               }
                               break;
#endif
default:
                               break;}

                               Bgp4RedistLock ();
                               if (IpAddr.u2PrefixLen == u2PrefixLen)
                               {
                               u1AdvFlag = BGP4_NETWORK_ADV_ROUTE;
                               if (i4Action == BGP4_NETWORK_ROUTE_ADD)
                               {
                               if (pNetworkAddr->u2Afi == BGP4_INET_AFI_IPV4)
                               {

                               i4RetStatus =
                               Bgp4ImportRouteAdd (u4VrfId, IpAddr,
                                                   NetIpRtInfo.u2RtProto,
                                                   NextHopInfo,
                                                   BGP4_INVALID_ROUTE_TAG,
                                                   NetIpRtInfo.i4Metric1,
                                                   NetIpRtInfo.u4RtIfIndx, 0, 0,
                                                   NULL, u1AdvFlag,
                                                   NetIpRtInfo.u4SetFlag);}
#ifdef BGP4_IPV6_WANTED
                               else
                               {

                               i4RetStatus =
                               Bgp4ImportRouteAdd (u4VrfId, IpAddr,
                                                   NetIpv6RtInfo.i1Proto,
                                                   NextHopInfo,
                                                   BGP4_INVALID_ROUTE_TAG,
                                                   NetIpv6RtInfo.u4Metric,
                                                   NetIpv6RtInfo.u4Index, 0, 0,
                                                   NULL, u1AdvFlag,
                                                   NetIpv6RtInfo.u4SetFlag);}
#endif

                               if (i4RetStatus == BGP4_FAILURE)
                               {
                               BGP4_DBG (BGP4_DBG_ALL,
                                         "\tBgp4ImportRouteAdd() :  Network Route Addition FAILED !!! \n");
                               Bgp4RedistUnLock (); return BGP4_FAILURE;}
                               }
                               else
                               {
                               if (pNetworkAddr->u2Afi == BGP4_INET_AFI_IPV4)
                               {
                               i4RetStatus =
                               Bgp4ImportRouteDelete (u4VrfId, IpAddr,
                                                      NetIpRtInfo.u2RtProto,
                                                      NextHopInfo,
                                                      NetIpRtInfo.u4RtIfIndx,
                                                      u1AdvFlag);}
#ifdef BGP4_IPV6_WANTED
                               else
                               {
                               i4RetStatus =
                               Bgp4ImportRouteDelete (u4VrfId, IpAddr,
                                                      NetIpv6RtInfo.i1Proto,
                                                      NextHopInfo,
                                                      NetIpv6RtInfo.u4Index,
                                                      u1AdvFlag);}
#endif
                               if (i4RetStatus == BGP4_FAILURE)
                               {
                               BGP4_DBG (BGP4_DBG_ALL,
                                         "\tBgp4ImportRouteDelete() :  Network Route Deletion FAILED !!! \n");
                               Bgp4RedistUnLock (); return BGP4_FAILURE;}
                               }
                               }
                               UNUSED_PARAM (pulCommunity);
                               UNUSED_PARAM (u1Community);
                               Bgp4RedistUnLock (); return BGP4_SUCCESS;}

/*****************************************************************************/
/* Function        : Bgp4UpdateMetricInRoutes                                */
/* Description     : This procedure sends the BGP4 Update messages with      */
/*                   updated metric value to BGP4 peers using import list    */
/*                   as a feasible/withdrawn list                            */
/* Input           : u4VrfId                                                 */
/*                   u4Metric metric value to  be updated                    */
/* Output          : None                                                    */
/* Returns         : BGP4_TRUE, if processing is complete                    */
/*                   BGP4_FALSE, otherwise                                   */
/*****************************************************************************/
                               INT4
                               Bgp4UpdateMetricInRoutes (UINT4 u4VrfId,
                                                         UINT4 u4Metric,
                                                         INT4 i4RedisProto)
                               {
                               tLinkNode * pScanLinkNode = NULL;
                               tRouteProfile * pRouteProfile = NULL;
                               INT4 i4ProtocolId = 0;
                               UINT1 u1RtProfileProtocol = 0;
                               /* The routes present in the Static, RIP and OSPF
                                * import list and redistribution list should be processed again 
                                * with the new Metric value */
                               switch (i4RedisProto)
                               {
case BGP4_IMPORT_STATIC:
u1RtProfileProtocol = STATIC_ID; i4ProtocolId = BGP4_STATIC_METRIC; break; case BGP4_IMPORT_DIRECT:
u1RtProfileProtocol = LOCAL_ID; i4ProtocolId = BGP4_DIRECT_METRIC; break; case BGP4_IMPORT_RIP:
u1RtProfileProtocol = RIP_ID; i4ProtocolId = BGP4_RIP_METRIC; break; case BGP4_IMPORT_OSPF:
u1RtProfileProtocol = OSPF_ID; i4ProtocolId = BGP4_OSPF_METRIC; break; case BGP4_IMPORT_ISISL1L2:
u1RtProfileProtocol = ISIS_ID; i4ProtocolId = BGP4_ISIS_METRIC; break; case BGP4_IMPORT_ALL:
u1RtProfileProtocol = BGP4_ALL_METRIC; i4ProtocolId = BGP4_ALL_METRIC; break; default:
                               u1RtProfileProtocol = 0; i4ProtocolId = 0;}

                               TMO_SLL_Scan (BGP4_REDIST_LIST (u4VrfId),
                                             pScanLinkNode, tLinkNode *)
                               {
                               pRouteProfile = pScanLinkNode->pRouteProfile;
                               if (u1RtProfileProtocol == BGP4_ALL_METRIC)
                               {
                               BGP4_RT_MED (pScanLinkNode->pRouteProfile) =
                               u4Metric;
                               BGP4_RT_NEW_MED (pRouteProfile) = u4Metric;
                               if (((BGP4_RRD_METRIC_MASK (u4VrfId)) &
                                    i4ProtocolId) == i4ProtocolId)
                               {
                               BGP4_RT_BGP_STATIC_MED_CHANGE (pRouteProfile) =
                               BGP4_TRUE;}
                               if ((BGP4_RT_BGP_INFO
                                    (pScanLinkNode->pRouteProfile)) != NULL)
                               {
                               BGP4_INFO_RCVD_MED (BGP4_RT_BGP_INFO
                                                   (pScanLinkNode->
                                                    pRouteProfile)) = u4Metric;}
                               }
                               else
                               {
                               if (pRouteProfile->u1Protocol ==
                                   u1RtProfileProtocol)
                               {
                               BGP4_RT_MED (pScanLinkNode->pRouteProfile) =
                               u4Metric;
                               BGP4_RT_NEW_MED (pRouteProfile) = u4Metric;
                               if (((BGP4_RRD_METRIC_MASK (u4VrfId)) &
                                    i4ProtocolId) == i4ProtocolId)
                               {
                               BGP4_RT_BGP_STATIC_MED_CHANGE (pRouteProfile) =
                               BGP4_TRUE;}
                               if ((BGP4_RT_BGP_INFO
                                    (pScanLinkNode->pRouteProfile)) != NULL)
                               {
                               BGP4_INFO_RCVD_MED
                               (BGP4_RT_BGP_INFO (pScanLinkNode->pRouteProfile))
                               = u4Metric;}
                               }
                               }
                               }
                               if (i4RedisProto == BGP4_IMPORT_ALL)
                               {
                               Bgp4DshUpdateMetricInSLL (BGP4_REDIST_LIST
                                                         (u4VrfId),
                                                         BGP4_STATIC_IMPORT_LIST
                                                         (u4VrfId),
                                                         BGP4_PROTOCOL_LIST_INDEX,
                                                         BGP4_RT_IN_REDIST_LIST,
                                                         0, u4VrfId, u4Metric,
                                                         i4ProtocolId);
                               Bgp4DshUpdateMetricInSLL (BGP4_REDIST_LIST
                                                         (u4VrfId),
                                                         BGP4_DIRECT_IMPORT_LIST
                                                         (u4VrfId),
                                                         BGP4_PROTOCOL_LIST_INDEX,
                                                         BGP4_RT_IN_REDIST_LIST,
                                                         0, u4VrfId, u4Metric,
                                                         i4ProtocolId);
                               Bgp4DshUpdateMetricInSLL (BGP4_REDIST_LIST
                                                         (u4VrfId),
                                                         BGP4_RIP_IMPORT_LIST
                                                         (u4VrfId),
                                                         BGP4_PROTOCOL_LIST_INDEX,
                                                         BGP4_RT_IN_REDIST_LIST,
                                                         0, u4VrfId, u4Metric,
                                                         i4ProtocolId);
                               Bgp4DshUpdateMetricInSLL (BGP4_REDIST_LIST
                                                         (u4VrfId),
                                                         BGP4_OSPF_IMPORT_LIST
                                                         (u4VrfId),
                                                         BGP4_PROTOCOL_LIST_INDEX,
                                                         BGP4_RT_IN_REDIST_LIST,
                                                         0, u4VrfId, u4Metric,
                                                         i4ProtocolId);
                               Bgp4DshUpdateMetricInSLL (BGP4_REDIST_LIST
                                                         (u4VrfId),
                                                         BGP4_ISISL1_IMPORT_LIST
                                                         (u4VrfId),
                                                         BGP4_PROTOCOL_LIST_INDEX,
                                                         BGP4_RT_IN_REDIST_LIST,
                                                         0, u4VrfId, u4Metric,
                                                         i4ProtocolId);
                               Bgp4DshUpdateMetricInSLL (BGP4_REDIST_LIST
                                                         (u4VrfId),
                                                         BGP4_ISISL2_IMPORT_LIST
                                                         (u4VrfId),
                                                         BGP4_PROTOCOL_LIST_INDEX,
                                                         BGP4_RT_IN_REDIST_LIST,
                                                         0, u4VrfId, u4Metric,
                                                         i4ProtocolId);}
                               else
                               if ((i4RedisProto & BGP4_IMPORT_STATIC) ==
                                   BGP4_IMPORT_STATIC)
                               {
                               Bgp4DshUpdateMetricInSLL (BGP4_REDIST_LIST
                                                         (u4VrfId),
                                                         BGP4_STATIC_IMPORT_LIST
                                                         (u4VrfId),
                                                         BGP4_PROTOCOL_LIST_INDEX,
                                                         BGP4_RT_IN_REDIST_LIST,
                                                         0, u4VrfId, u4Metric,
                                                         i4ProtocolId);}
                               else
                               if ((i4RedisProto & BGP4_IMPORT_DIRECT) ==
                                   BGP4_IMPORT_DIRECT)
                               {
                               Bgp4DshUpdateMetricInSLL (BGP4_REDIST_LIST
                                                         (u4VrfId),
                                                         BGP4_DIRECT_IMPORT_LIST
                                                         (u4VrfId),
                                                         BGP4_PROTOCOL_LIST_INDEX,
                                                         BGP4_RT_IN_REDIST_LIST,
                                                         0, u4VrfId, u4Metric,
                                                         i4ProtocolId);}
                               else
                               if ((i4RedisProto & BGP4_IMPORT_RIP) ==
                                   BGP4_IMPORT_RIP)
                               {
                               Bgp4DshUpdateMetricInSLL (BGP4_REDIST_LIST
                                                         (u4VrfId),
                                                         BGP4_RIP_IMPORT_LIST
                                                         (u4VrfId),
                                                         BGP4_PROTOCOL_LIST_INDEX,
                                                         BGP4_RT_IN_REDIST_LIST,
                                                         0, u4VrfId, u4Metric,
                                                         i4ProtocolId);}
                               else
                               if ((i4RedisProto & BGP4_IMPORT_OSPF) ==
                                   BGP4_IMPORT_OSPF)
                               {
                               Bgp4DshUpdateMetricInSLL (BGP4_REDIST_LIST
                                                         (u4VrfId),
                                                         BGP4_OSPF_IMPORT_LIST
                                                         (u4VrfId),
                                                         BGP4_PROTOCOL_LIST_INDEX,
                                                         BGP4_RT_IN_REDIST_LIST,
                                                         0, u4VrfId, u4Metric,
                                                         i4ProtocolId);}
                               else
                               if ((i4RedisProto & BGP4_IMPORT_ISISL1) ==
                                   BGP4_IMPORT_ISISL1)
                               {
                               Bgp4DshUpdateMetricInSLL (BGP4_REDIST_LIST
                                                         (u4VrfId),
                                                         BGP4_ISISL1_IMPORT_LIST
                                                         (u4VrfId),
                                                         BGP4_PROTOCOL_LIST_INDEX,
                                                         BGP4_RT_IN_REDIST_LIST,
                                                         0, u4VrfId, u4Metric,
                                                         i4ProtocolId);}
                               else
                               if ((i4RedisProto & BGP4_IMPORT_ISISL1) ==
                                   BGP4_IMPORT_ISISL1)
                               {
                               Bgp4DshUpdateMetricInSLL (BGP4_REDIST_LIST
                                                         (u4VrfId),
                                                         BGP4_ISISL2_IMPORT_LIST
                                                         (u4VrfId),
                                                         BGP4_PROTOCOL_LIST_INDEX,
                                                         BGP4_RT_IN_REDIST_LIST,
                                                         0, u4VrfId, u4Metric,
                                                         i4ProtocolId);}
                               else
                               {
                               return BGP4_FALSE;}
                               return BGP4_TRUE;}

/*****************************************************************************/
/* Function Name : Bgp4DshUpdateMetricInSLL                                  */
/* Description   : This function copy all routes in DLL to the input SLL     */
/*                 and update the metric value set                           */
/* Input(s)      : pDLList - DLL holding the route.                          */
/*               : u4DLLInfo - Info about the DLL in the Route Profile.      */
/*                          BGP4_FIB_UPD_LIST_INDEX  - 1                     */
/*                          BGP4_INT_PEER_LIST_INDEX - 2                     */
/*                          BGP4_EXT_PEER_LIST_INDEX - 3                     */
/*                          BGP4_NEXTHOP_LIST_INDEX  - 4                     */
/*                          BGP4_PROTOCOL_LIST_INDEX - 5                     */
/*                 The FIB/INT/EXT List used the CHANGE_LIST_DLL             */
/*               : u4Flag - Indicate whether any flag need to be set to the  */
/*               : route while adding to the list.                           */
/*               : u4Position - Indiacate whether the route needs to copied  */
/*               : at the beginning or end of the list.                      */
/* Output(s)     : pList - List where all the routes should be copied.       */
/* Return(s)     : BGP4_SUCCESS if the operation was successful,             */
/*                 BGP4_FAILURE if it fails.                                 */
/*****************************************************************************/
                               INT4
                               Bgp4DshUpdateMetricInSLL (tTMO_SLL * pList,
                                                         tBGP4_DLL * pDLList,
                                                         UINT4 u4DLLInfo,
                                                         UINT4 u4Flag,
                                                         UINT4 u4Position,
                                                         UINT4 u4VrfId,
                                                         UINT4 u4Metric,
                                                         INT4 i4ProtocolId)
                               {
                               tRouteProfile * pRtProfile = NULL;
#ifdef L3VPN
                               tRtInstallVrfInfo * pRtVrfInfo = NULL;
#endif
#ifndef L3VPN
                               UNUSED_PARAM (u4VrfId);
#endif
                               if (u4DLLInfo != BGP4_PROTOCOL_LIST_INDEX)
                               {
                               /* Currently only it has been implemented for PROTO-DLL in Route
                                * Profile entry. If require for other DLL, then update this
                                * function. */
                               return BGP4_FAILURE;}

                               for (pRtProfile = BGP4_DLL_FIRST_ROUTE (pDLList);
                                    pRtProfile != NULL;
                                    pRtProfile =
                                    BGP4_RT_PROTO_LINK_NEXT (pRtProfile))
                               {
#ifdef L3VPN
                               if (u4VrfId == BGP4_DFLT_VRFID)
                               {
                               if ((BGP4_RT_AFI_INFO (pRtProfile) ==
                                    BGP4_INET_AFI_IPV4)
                                   && (BGP4_RT_SAFI_INFO (pRtProfile) ==
                                       BGP4_INET_SAFI_VPNV4_UNICAST))
                               {
                               /* The VRF redistributed routes should be removed by
                                * using no redist in VRF context
                                */
                               continue;}
                               }
                               else
                               {
                               Bgp4Vpnv4GetInstallVrfInfo (pRtProfile, u4VrfId,
                                                           &pRtVrfInfo);
                               if (pRtVrfInfo == NULL)
                               {
                               /* The route is not installed in this particular VRF */
                               continue;}
                               }
#endif
                               if ((u4Flag & BGP4_RT_IN_REDIST_LIST) ==
                                   BGP4_RT_IN_REDIST_LIST)
                               {
                               if (pRtProfile->u1Protocol == LOCAL_ID)
                               {
                               BGP4_RT_BGP_MED_CHANGE (pRtProfile) = BGP4_TRUE;}
                               BGP4_RT_MED (pRtProfile) = u4Metric;
                               BGP4_RT_NEW_MED (pRtProfile) = u4Metric;
                               if (((BGP4_RRD_METRIC_MASK (u4VrfId)) &
                                    i4ProtocolId) == i4ProtocolId)
                               {
                               BGP4_RT_BGP_STATIC_MED_CHANGE (pRtProfile) =
                               BGP4_TRUE;}
                               if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                                    BGP4_RT_IN_REDIST_LIST) !=
                                   BGP4_RT_IN_REDIST_LIST)
                               {
                               BGP4_RT_SET_FLAG (pRtProfile,
                                                 BGP4_RT_IN_REDIST_LIST);
                               Bgp4DshDelinkRouteFromChgList (pRtProfile);
                               if (Bgp4DshAddRouteToList
                                   (pList, pRtProfile,
                                    u4Position) == BGP4_FAILURE)
                               {
                               BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                         BGP4_ALL_FAILURE_TRC |
                                         BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                                         "\tBgp4DshUpdateMetricInSLL : Failed to add route to list\n");}
                               Bgp4RedSyncRouteInfo (pRtProfile);}
                               }
                               else
                               {
                               if (pRtProfile->u1Protocol == LOCAL_ID)
                               {
                               BGP4_RT_BGP_MED_CHANGE (pRtProfile) = BGP4_TRUE;}
                               BGP4_RT_MED (pRtProfile) = u4Metric;
                               BGP4_RT_NEW_MED (pRtProfile) = u4Metric;
                               if (((BGP4_RRD_METRIC_MASK (u4VrfId)) &
                                    i4ProtocolId) == i4ProtocolId)
                               {
                               BGP4_RT_BGP_STATIC_MED_CHANGE (pRtProfile) =
                               BGP4_TRUE;}
                               BGP4_RT_SET_FLAG (pRtProfile, u4Flag);
                               if (Bgp4DshAddRouteToList
                                   (pList, pRtProfile,
                                    u4Position) == BGP4_FAILURE)
                               {
                               BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                         BGP4_ALL_FAILURE_TRC |
                                         BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                                         "\tBgp4DshUpdateMetricInSLL : Failed to add route to list\n");}
                               Bgp4RedSyncRouteInfo (pRtProfile);}
                               }
                               return (BGP4_SUCCESS);}
#endif /* BGP4IGPH_C */
