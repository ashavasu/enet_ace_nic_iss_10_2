/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4sync.c,v 1.17 2016/12/27 12:35:58 siva Exp $
 *
 * Description: This file contains modules which will handle the 
 *              synchronization part of the advertisement hanlder.
 *
 *******************************************************************/

#ifndef   BGP4ASYNC_C
#define   BGP4ASYNC_C

#include "bgp4com.h"

/*****************************************************************************/
/* Function Name : Bgp4SyncCheckForSync                                      */
/* Description   : This routine takes the route profile for which synchroni- */
/*                 sation needs to be applied as input. If the route is      */
/*                 withdrawn then checks whether the route is present in     */
/*                 NON-SYNC list. If yes, then removes the route from that   */
/*                 list. If the route is feasible route, then checks whether */
/*                 the route is synchronised or not. If not, then adds the   */
/*                 route to the NON-SYNC list.                               */
/* Input(s)      : pRtProfile - Pointer to the route profile.                */
/*                 pPeerInfo - Pointer to the peer information               */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS - if the route is not suppressed because of  */
/*                                synchronisation.                           */
/*                 BGP4_FAILURE - otherwise.                                 */
/*****************************************************************************/
INT4
Bgp4SyncCheckForSync (tRouteProfile * pRtProfile, tBgp4PeerEntry * pPeerInfo)
{
    tRouteProfile      *pNonSyncRoute = NULL;
    UINT4               u4RtIfIndx = BGP4_INVALID_IFINDX;
    INT4                i4Metric = BGP4_INVALID_NH_METRIC;
    INT4                i4RetVal = BGP4_FAILURE;
    tAddrPrefix         FndIpRouteInfo;
    INT4                i4LkupStatus;
    UINT1               u1Protocol = 0;    /* 0 - IGP */
#ifdef L3VPN
    UINT4               u4AsafiMask;
#endif

    BGP4_DBG (BGP4_DBG_ENTRY, " Bgp4SyncCheckForSync \n");

    UNUSED_PARAM (pPeerInfo);
#ifdef L3VPN
    /*The VPN routes will not be leaked in to the Backbone IGP.
     * So need  not to apply Synchoronisation*/
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
    if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
    {
        return BGP4_SUCCESS;
    }
#endif

    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_WITHDRAWN) ==
        BGP4_RT_WITHDRAWN)
    {
        /* NON_SYNC list updation is done here for the withdrawn routes */
        pNonSyncRoute =
            Bgp4DshRemoveRouteFromList (BGP4_NONSYNC_LIST
                                        (BGP4_RT_CXT_ID (pRtProfile)),
                                        pRtProfile);
        if (pNonSyncRoute != NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tBgp4SyncCheckForSync() : Route removed from NON_SYNC "
                      "list \n");
            i4RetVal = BGP4_FAILURE;
        }
        else
        {
            /* Route not present in non-sync list. Should be present in RIB */
            i4RetVal = BGP4_SUCCESS;
        }
    }
    else
    {
        pNonSyncRoute =
            Bgp4DshRemoveRouteFromList (BGP4_NONSYNC_LIST
                                        (BGP4_RT_CXT_ID (pRtProfile)),
                                        pRtProfile);
        if (pNonSyncRoute != NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tBgp4SyncCheckForSync() : Route removed from NON_SYNC list\n");
        }

        Bgp4InitAddrPrefixStruct (&FndIpRouteInfo,
                                  BGP4_RT_AFI_INFO (pRtProfile));
        /* synchronisation is not appliable for VPNv4 routes. so always
         * check in default VRF table.
         */
        i4LkupStatus =
            BgpIpRtLookup (BGP4_RT_CXT_ID (pRtProfile),
                           &(BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                             (BGP4_RT_NET_ADDRESS_INFO (pRtProfile))),
                           BGP4_RT_PREFIXLEN(pRtProfile), &FndIpRouteInfo, &i4Metric, &u4RtIfIndx,
                           u1Protocol);
        if (i4LkupStatus == BGP4_FAILURE)
        {
            /* IGP route lookup fails, hence add this route
             * to the NON_SYNC_list. 
             */

            BGP4_DBG (BGP4_DBG_ERROR,
                      " Bgp4SyncCheckForSync: BgpIpRtLookup failed \n");

            Bgp4DshAddRouteToList (BGP4_NONSYNC_LIST
                                   (BGP4_RT_CXT_ID (pRtProfile)), pRtProfile,
                                   0);

            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tBgp4SyncCheckForSync() : Route added to NON SYNC "
                      "list \n");
            i4RetVal = BGP4_FAILURE;
        }
        else
        {
            BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                      "\tBgp4SyncCheckForSync() : BgpIpRtLookup Success\n");
            i4RetVal = BGP4_SUCCESS;
        }

        return (i4RetVal);
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4SyncProcessSynchronisation                            */
/* Description   : This routine takes care of synchronisation process.       */
/*                 This routine should be called periodically at the         */
/*                 expiration of BGP4_SYNC_PRCS_INTERVAL. This routine checks*/
/*                 whether the routes present in the NON-SYNC list have      */
/*                 been synchronized or not. If synchronised then the route  */
/*                 will be processed as feasible route. If all routes in the */
/*                 NON-SYNC list is processed, then the routes present in    */
/*                 the LOCAL RIB will be checked for synchronisation. If     */
/*                 any route have become non-synchronised then those routes  */
/*                 will be removed from LOCAL RIB and processed as withdrawn */
/*                 routes. This process is throttled for handling            */
/*                 MAX_SYNC_ROUTES in a single iteration.                    */
/* Input(s)      : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return        : BGP4_TRUE  - if more routes available for processing.     */
/*               : BGP4_FALSE - if no more routes available for processing.  */
/*                 BGP4_RELINQUISH - if external events requires CPU         */
/*****************************************************************************/
INT4
Bgp4SyncProcessSynchronisation (UINT4 u4Context)
{
    tLinkNode          *pLinkNode = NULL;
    tLinkNode          *pTempLinkNode = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pNextRt = NULL;
    tRouteProfile       RtProfile;
    tBgp4PeerEntry     *pPeer = NULL;
    VOID               *pRibNode = NULL;
    VOID               *pNextRibNode = NULL;
    tAddrPrefix         InvPrefix;
    tAddrPrefix         FndIpRouteInfo;
    INT4                i4LkupStatus;
    UINT1               u1Protocol = 0;    /* 0 - IGP */
    INT4                i4Metric = 0;
    UINT4               u4RtIfIndx = 0;
    UINT4               u4RtCnt = 0;
    INT4                i4RetVal = BGP4_FAILURE;

    MEMSET (&RtProfile, 0, sizeof (tRouteProfile));
    if ((BGP4_SYNC_INTERVAL (u4Context) < BGP4_SYNC_PRCS_INTERVAL) ||
        (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_DOWN))
    {
        return (BGP4_FALSE);
    }
    Bgp4InitAddrPrefixStruct (&(InvPrefix),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO
                              (BGP4_SYNC_INIT_ROUTE_PREFIX_INFO (u4Context)));
    if ((MEMCMP
         (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (InvPrefix),
          BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
          (BGP4_SYNC_INIT_ROUTE_PREFIX_INFO (u4Context)),
          BGP4_MAX_INET_ADDRESS_LEN)) == 0)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tInitiating Scanning for BGP Synchronisation.\n");
        /* Start with the routes present in the NON-SYNC list. All the
         * routes in the SYNC list will be processed. */
        BGP_SLL_DYN_Scan (BGP4_NONSYNC_LIST (u4Context), pLinkNode,
                          pTempLinkNode, tLinkNode *)
        {
            pRtProfile = pLinkNode->pRouteProfile;
            /* Process the route present in the NON-SYNC list. If the route
             * is synchronised now, then sent the route as feasible route. */
            Bgp4InitAddrPrefixStruct (&(FndIpRouteInfo),
                                      BGP4_RT_AFI_INFO (pRtProfile));
            /* synchronisation is not appliable for VPNv4 routes. so always
             * check in default VRF table.
             */
            i4LkupStatus = BgpIpRtLookup (u4Context,
                                          &BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                          (BGP4_RT_NET_ADDRESS_INFO
                                           (pRtProfile)), BGP4_RT_PREFIXLEN(pRtProfile),
                                          &FndIpRouteInfo, &i4Metric,
                                          &u4RtIfIndx, u1Protocol);
            if (i4LkupStatus == BGP4_SUCCESS)
            {
                /* IGP route lookup success, hence remove this route
                 * from the NON_SYNC_list and advertise this route
                 * feasible route to other peers. 
                 */
                BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                          "\tBgp4SyncProcessSynchronisation() : "
                          "BgpIpRtLookup successful \n");
                pPeer = BGP4_RT_PEER_ENTRY (pRtProfile);

                /* First check whether the route is present in FIB/Peer Advt
                 * list. If present, then remove the route from those list.
                 * The idea is since the route is in NON_SYNC list, it cant
                 * be in LOCAL-RIB and any other list. If present, then this
                 * is because this route become Non-Sync and removed from RIB
                 * but before processed for FIB/advertisement, have become
                 * sync again. */
                if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_IN_FIB_UPD_LIST)
                    == BGP4_RT_IN_FIB_UPD_LIST)
                {
                    BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_IN_FIB_UPD_LIST);
                    Bgp4DshDelinkRouteFromList (BGP4_FIB_UPD_LIST (u4Context),
                                                pRtProfile,
                                                BGP4_FIB_UPD_LIST_INDEX);
                }
                else if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                          BGP4_RT_IN_EXT_PEER_ADVT_LIST) ==
                         BGP4_RT_IN_EXT_PEER_ADVT_LIST)
                {
                    BGP4_RT_RESET_FLAG (pRtProfile,
                                        BGP4_RT_IN_EXT_PEER_ADVT_LIST);
                    Bgp4DshDelinkRouteFromList (BGP4_EXT_PEERS_ADVT_LIST
                                                (u4Context), pRtProfile,
                                                BGP4_EXT_PEER_LIST_INDEX);
                }
                else if ((BGP4_RT_GET_FLAGS (pRtProfile) &
                          BGP4_RT_IN_INT_PEER_ADVT_LIST) ==
                         BGP4_RT_IN_INT_PEER_ADVT_LIST)
                {
                    BGP4_RT_RESET_FLAG (pRtProfile,
                                        BGP4_RT_IN_INT_PEER_ADVT_LIST);
                    Bgp4DshDelinkRouteFromList (BGP4_INT_PEERS_ADVT_LIST
                                                (u4Context), pRtProfile,
                                                BGP4_INT_PEER_LIST_INDEX);
                }

                /* Process the route as feasible route. */
                BGP4_RT_RESET_FLAG (pRtProfile,
                                    (BGP4_RT_WITHDRAWN | BGP4_RT_REPLACEMENT));
                Bgp4RibhProcessFeasibleRoute (pPeer, pRtProfile);

                /* Clear the route from the non-sync list. */
                Bgp4DshRemoveNodeFromList (BGP4_NONSYNC_LIST (u4Context),
                                           pLinkNode);
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC,
                          BGP4_MOD_NAME,
                          "\tBgp4SyncProcessSynchronisation() : Route removed"
                          "from NON_SYNC list \n");
            }
            u4RtCnt++;
        }
        /* Has completed processing all the routes in the NON-SYNC list. Start
         * processing the route in the RIB. */
        pRtProfile = NULL;
        pRibNode = NULL;
        /* Try to get the first entry from IPV4 RIB. Incase there are no routes
         * in IPV4 RIB, then move to IPV6 RIB. Similarly when all the routes
         * in IPV4 RIB are processed then move to IPV6 RIB
         */
        Bgp4GetRibNode (u4Context, CAP_MP_IPV4_UNICAST, &pRibNode);
        if (pRibNode != NULL)
        {
            i4RetVal = Bgp4RibhGetFirstEntry (CAP_MP_IPV4_UNICAST,
                                              &pRtProfile, &pRibNode, TRUE);
        }
#ifdef BGP4_IPV6_WANTED
        if (i4RetVal == BGP4_FAILURE)
        {
            /* No route present in IPV4 RIB. */
            /* Get the first entry from IPV6 RIB */
            Bgp4GetRibNode (u4Context, CAP_MP_IPV6_UNICAST, &pRibNode);
            if (pRibNode != NULL)
            {
                i4RetVal = Bgp4RibhGetFirstEntry (CAP_MP_IPV6_UNICAST,
                                                  &pRtProfile, &pRibNode, TRUE);
            }
        }
#endif
        if ((i4RetVal == BGP4_FAILURE) || (pRtProfile == NULL))
        {
            /* No route is present in IPV4/IPV6 RIB. */
            Bgp4InitNetAddressStruct (&
                                      (BGP4_SYNC_INIT_ROUTE_NETADDR_INFO
                                       (u4Context)), BGP4_INET_AFI_IPV4,
                                      BGP4_INET_SAFI_UNICAST);
            BGP4_SYNC_INTERVAL (u4Context) = 0;
            return (BGP4_FALSE);
        }
        else
        {
            if (pRtProfile != NULL)
            {
                Bgp4CopyNetAddressStruct (&
                                          (BGP4_SYNC_INIT_ROUTE_NETADDR_INFO
                                           (u4Context)),
                                          BGP4_RT_NET_ADDRESS_INFO
                                          (pRtProfile));
            }
        }

        if (u4RtCnt >= BGP4_MAX_SYNC_RTS2PRCS)
        {
            /* Has processed max routes for synchronisation. */
            if (Bgp4IsCpuNeeded () == BGP4_TRUE)
            {
                /* External events requires CPU. Relinquish CPU. */
                return (BGP4_RELINQUISH);
            }
            else
            {
                return (BGP4_TRUE);
            }
        }
    }

    Bgp4InitAddrPrefixStruct (&(InvPrefix),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO
                              (BGP4_SYNC_INIT_ROUTE_PREFIX_INFO (u4Context)));

    if ((PrefixMatch (BGP4_SYNC_INIT_ROUTE_PREFIX_INFO (u4Context), InvPrefix))
        != BGP4_TRUE)
    {
        /* Need to start processing from this route. */
        if (pRtProfile == NULL)
        {
            Bgp4CopyNetAddressStruct (&(RtProfile.NetAddress),
                                      BGP4_SYNC_INIT_ROUTE_NETADDR_INFO
                                      (u4Context));
            RtProfile.u1Ref = 0;
            RtProfile.u1Protocol = 0;
            RtProfile.p_RPNext = 0;
            RtProfile.u4Flags = 0;
            RtProfile.pRtInfo = 0;
            i4RetVal = Bgp4RibhLookupRtEntry (&RtProfile, u4Context,
                                              BGP4_TREE_FIND_EXACT,
                                              &pRtProfile, &pRibNode);
            if (i4RetVal == BGP4_FAILURE)
            {
                switch (BGP4_RT_AFI_INFO ((&RtProfile)))
                {
                    case BGP4_INET_AFI_IPV4:
#ifdef BGP4_IPV6_WANTED
                        i4RetVal = BGP4_FAILURE;
                        Bgp4GetRibNode (u4Context, CAP_MP_IPV6_UNICAST,
                                        &pRibNode);
                        if (pRibNode != NULL)
                        {
                            i4RetVal =
                                Bgp4RibhGetFirstEntry (CAP_MP_IPV6_UNICAST,
                                                       &pRtProfile, &pRibNode,
                                                       TRUE);
                        }
                        break;
                    case BGP4_INET_AFI_IPV6:
                        break;
#endif
                    default:
                        break;
                }
                if (i4RetVal == BGP4_FAILURE)
                {
                    /* No routes present in RIB */
                    Bgp4InitNetAddressStruct (&
                                              (BGP4_SYNC_INIT_ROUTE_NETADDR_INFO
                                               (u4Context)), BGP4_INET_AFI_IPV4,
                                              BGP4_INET_SAFI_UNICAST);
                    BGP4_SYNC_INTERVAL (u4Context) = 0;
                    return (BGP4_FALSE);
                }
            }
        }

        Bgp4CopyNetAddressStruct (&
                                  (BGP4_SYNC_INIT_ROUTE_NETADDR_INFO
                                   (u4Context)),
                                  BGP4_RT_NET_ADDRESS_INFO (pRtProfile));
        for (;;)
        {
            /* Get the next route profile and store it. */
            pNextRibNode = pRibNode;
            i4RetVal = Bgp4RibhGetNextEntry (pRtProfile, u4Context,
                                             &pNextRt, &pNextRibNode, TRUE);
            if ((BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID) &&
                (!(((BGP4_GET_PEER_TYPE
                     (u4Context,
                      BGP4_RT_PEER_ENTRY (pRtProfile))) == BGP4_EXTERNAL_PEER)
                   && (BGP4_CONFED_PEER_STATUS (BGP4_RT_PEER_ENTRY (pRtProfile))
                       == BGP4_FALSE))))
            {
                /* Route is learnt from BGP peer and peer is not
                 * external peer. check in IGPs only */
                i4LkupStatus = BgpIpRtLookup (u4Context,
                                              &BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                              (BGP4_RT_NET_ADDRESS_INFO
                                               (pRtProfile)),BGP4_RT_PREFIXLEN(pRtProfile) ,
                                              &FndIpRouteInfo, &i4Metric,
                                              &u4RtIfIndx,
                                              0 /* check in IGPs only */ );
                if (i4LkupStatus == BGP4_FAILURE)
                {
                    /* IGP route lookup fails, hence this route should
                     * be processed as withdrawn route. */
                    BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                              "\tBgp4SyncProcessSynchronisation() : "
                              "BgpIpRtLookup failed \n");
#ifdef RFD_WANTED
                    BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_NO_RFD);
#endif
                    BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_WITHDRAWN);
                    BGP4_RT_REF_COUNT (pRtProfile)++;
                    Bgp4RibhProcessWithdRoute (BGP4_RT_PEER_ENTRY (pRtProfile),
                                               pRtProfile, pRibNode);

                    /* Update the NON-SYNC list with the current route. */
                    Bgp4DshRemoveRouteFromList (BGP4_NONSYNC_LIST (u4Context),
                                                pRtProfile);
                    Bgp4DshAddRouteToList (BGP4_NONSYNC_LIST (u4Context),
                                           pRtProfile, 0);
                    Bgp4DshReleaseRtInfo (pRtProfile);
                }
            }

            u4RtCnt++;
            if (i4RetVal == BGP4_FAILURE)
            {
                /* No more routes present in RIB */
                switch (BGP4_RT_AFI_INFO (pRtProfile))
                {
                    case BGP4_INET_AFI_IPV4:
#ifdef BGP4_IPV6_WANTED
                        /* Get the first entry from IPV6 RIB */
                        pNextRt = NULL;
                        pNextRibNode = NULL;
                        Bgp4GetRibNode (u4Context, CAP_MP_IPV6_UNICAST,
                                        &pNextRibNode);
                        if (pNextRibNode != NULL)
                        {
                            i4RetVal =
                                Bgp4RibhGetFirstEntry (CAP_MP_IPV6_UNICAST,
                                                       &pNextRt, &pNextRibNode,
                                                       TRUE);
                        }
                        break;
                    case BGP4_INET_AFI_IPV6:
                        break;
#endif
                    default:
                        break;
                }
                if (i4RetVal == BGP4_FAILURE)
                {
                    Bgp4InitNetAddressStruct (&
                                              (BGP4_SYNC_INIT_ROUTE_NETADDR_INFO
                                               (u4Context)), BGP4_INET_AFI_IPV4,
                                              BGP4_INET_SAFI_UNICAST);
                    BGP4_SYNC_INTERVAL (u4Context) = 0;
                    return (BGP4_FALSE);
                }
            }

            pRtProfile = pNextRt;
            pRibNode = pNextRibNode;
            pNextRt = NULL;
            pNextRibNode = NULL;

            Bgp4CopyNetAddressStruct (&
                                      (BGP4_SYNC_INIT_ROUTE_NETADDR_INFO
                                       (u4Context)),
                                      BGP4_RT_NET_ADDRESS_INFO (pRtProfile));

            if (u4RtCnt >= BGP4_MAX_SYNC_RTS2PRCS)
            {
                /* Has processed max routes for synchronisation. */
                if (Bgp4IsCpuNeeded () == BGP4_TRUE)
                {
                    /* External events requires CPU. Relinquish CPU. */
                    return (BGP4_RELINQUISH);
                }
                else
                {
                    return (BGP4_TRUE);
                }
            }

        }
    }

    /* No more routes to be processed. */
    Bgp4InitNetAddressStruct (&(BGP4_SYNC_INIT_ROUTE_NETADDR_INFO (u4Context)),
                              BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
    BGP4_SYNC_INTERVAL (u4Context) = 0;
    return (BGP4_FALSE);
}

/*****************************************************************************/
/* Function Name : Bgp4SyncUpdateSyncInitRoute                               */
/* Description   : This routine is called whenever a route is removed from   */
/*                 the LOCAL RIB. This function checks whether the removed   */
/*                 route and the route stored in the sync init route variable*/
/*                 are identical. If yes, then the next route from the LOCAL */
/*                 RIB is obtained and stored in the sync init route variable*/
/*                 If next route is not present, then sync init route is set */
/*                 as 0.                                                     */
/* Input(s)      : Pointer to the current route profile (pRtProfile)         */
/*                 Pointer to the treenode in which the route is present     */
/*                          - pTreeNode                                      */
/*               : i4Flag - Flag passed to RIB for semaphore lock decision   */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS.                                             */
/*****************************************************************************/
INT4
Bgp4SyncUpdateSyncInitRoute (tRouteProfile * pRtProfile, VOID *pTreeNode,
                             INT4 i4Flag)
{
    VOID               *pNode = NULL;
    tRouteProfile      *pNextRoute = NULL;
    INT4                i4RetVal = BGP4_FAILURE;

    pNode = pTreeNode;
    if ((NetAddrMatch (BGP4_RT_NET_ADDRESS_INFO (pRtProfile),
                       BGP4_SYNC_INIT_ROUTE_NETADDR_INFO (BGP4_RT_CXT_ID
                                                          (pRtProfile)))) ==
        BGP4_TRUE)
    {
        i4RetVal =
            Bgp4RibhGetNextEntry (pRtProfile, BGP4_RT_CXT_ID (pRtProfile),
                                  &pNextRoute, (VOID *) &pNode, i4Flag);
        if (i4RetVal == BGP4_FAILURE)
        {
            if(BGP4_RT_AFI_INFO (pRtProfile) == BGP4_INET_AFI_IPV4)
            {
                    /* Get the first entry from IPV6 RIB */
#ifdef BGP4_IPV6_WANTED
                    Bgp4GetRibNode (BGP4_RT_CXT_ID (pRtProfile),
                                    CAP_MP_IPV6_UNICAST, &pNode);
                    if (pNode != NULL)
                    {
                        i4RetVal = Bgp4RibhGetFirstEntry (CAP_MP_IPV6_UNICAST,
                                                          &pNextRoute, &pNode,
                                                          i4Flag);
                    }
#endif
            }

            if (pNextRoute == NULL)
            {
                Bgp4InitNetAddressStruct (&
                                          (BGP4_SYNC_INIT_ROUTE_NETADDR_INFO
                                           (BGP4_RT_CXT_ID (pRtProfile))),
                                          BGP4_INET_AFI_IPV4,
                                          BGP4_INET_SAFI_UNICAST);
            }
            else
            {
                Bgp4CopyNetAddressStruct (&
                                          (BGP4_SYNC_INIT_ROUTE_NETADDR_INFO
                                           (BGP4_RT_CXT_ID (pRtProfile))),
                                          BGP4_RT_NET_ADDRESS_INFO
                                          (pNextRoute));
            }
        }
        else
        {
            Bgp4CopyNetAddressStruct (&
                                      (BGP4_SYNC_INIT_ROUTE_NETADDR_INFO
                                       (BGP4_RT_CXT_ID (pRtProfile))),
                                      BGP4_RT_NET_ADDRESS_INFO (pNextRoute));
        }
    }
    return (BGP4_SUCCESS);
}
#endif /* BGP4SYNC_C */
