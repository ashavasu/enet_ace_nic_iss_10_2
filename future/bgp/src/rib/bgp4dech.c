/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4dech.c,v 1.22 2016/12/27 12:35:58 siva Exp $
 *
 * Description: This module adds the newly received route in its 
 *              appropriate place in the list of routes to the same  
 *              destination, as per the decision algorithm.
 *
 *******************************************************************/
#ifndef   BGP4DECH_C
#define   BGP4DECH_C

#include  "bgp4com.h"

/*****************************************************************************/
/* Function Name : Bgp4DechAddRouteSorted                                    */
/* Description   : Adds a newly arrived feasible route into the specified    */
/*                 leaf in sorted order of route preference                  */
/* Input(s)      : Leaf which contains list of routes to a destination       */
/*                 (pLeaf),                                                  */
/*                 Route Profile which needs to be inserted into the above   */
/*                 stated list (pRoute)                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4DechAddRouteSorted (tRouteProfile ** ppRtProfileList,
                        tRouteProfile * pRoute)
{
    tRouteProfile      *pRtList = NULL;
    tRouteProfile      *pRtNext = NULL;
    tRouteProfile      *pPrevRoute = NULL;
    tRouteProfile      *pHeadNode = NULL;
    tBgp4Info          *pBgpInfo = BGP4_RT_BGP_INFO (pRoute);
    tTMO_SLL           *pNewRtASPath = NULL;
    tTMO_SLL           *pPresentRtASPath = NULL;
    tBgp4PeerEntry     *pSrcPeerEntry = NULL;
    tBgp4PeerEntry     *pScanPeerEntry = NULL;
    INT4                i4NexthopMetric;
    UINT4               u4MED;
    UINT4               u4LocalPref = 0;
    UINT4               u4LocalPrefRtList = 0;
    UINT4               u4BgpId = BGP4_INVALID_ROUTE;
    UINT4               u4ScanBgpId;
    UINT4               u4VrfId = BGP4_RT_CXT_ID (pRoute);
#ifdef L3VPN
    UINT4               u4AsafiMask;
#endif
    UINT2               u2SrcPeerType = 0;
    UINT2               u2ScanPeerType = 0;
    UINT2               u2ASPathLen = 0;
    UINT1               u1Origin;
    UINT1               u1IsAggr;
    UINT4               u4OrigId;
    UINT2               u2ClusCnt;
    UINT2               u2ClusCntnewRt;
    UINT4               u4ExtCommCostVal = 0;
    UINT2               u2ExtCommCostCommId = 0;
    UINT4               u4ExtCommCostValList = 0;
    UINT2               u2ExtCommCostCommIdList = 0;
    UINT2               u2Weight;

    BGP4_DBG (BGP4_DBG_ENTRY, "\tBgp4DechAddRouteToSorted() ....... \n");

#ifdef L3VPN
    BGP4_VPN4_GET_PROCESS_VRFID (pRoute, u4VrfId);
#else
    BGP4_GET_PROCESS_VRFID (pRoute, u4VrfId);
#endif

    pRtList = (*ppRtProfileList);
    pHeadNode = pRtList;

    /* Reset the Best flag */
    BGP4_RT_RESET_FLAG (pRoute, BGP4_RT_BEST);
#ifdef L3VPN
    if (u4VrfId != BGP4_DFLT_VRFID)
    {
        Bgp4Vpnv4ResetVrfFlags (pRoute, u4VrfId, BGP4_RT_BGP_VRF_BEST);
    }
#endif

    /* If the route is invalid/History, then add the route to the end
     * of the list. */
     /* If Route is Stale then do not consider
        for best route add it to the end of the list*/
    if ((BGP4_IS_ROUTE_VALID (pRoute) == BGP4_FALSE) ||
        ((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_WITHDRAWN) == BGP4_RT_WITHDRAWN) ||
        (((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_HISTORY) == BGP4_RT_HISTORY)) || 
        (((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_STALE) == BGP4_RT_STALE) && 
         ((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_WITHDRAWN) == BGP4_RT_WITHDRAWN)))
    {
        /* Route is not Valid. */
        BGP4_RT_SET_NEXT (pRoute, NULL, u4VrfId);
        BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtNext);
        while (pRtNext != NULL)
        {
            pRtList = pRtNext;
            BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtNext);
        }
        BGP4_RT_SET_NEXT (pRtList, pRoute, u4VrfId);
        return (BGP4_SUCCESS);
    }

    /* If the first route present in the list is invalid/History, then add
     * the new route as the first route. */
    if ((BGP4_IS_ROUTE_VALID (pHeadNode) == BGP4_FALSE) ||
        (((BGP4_RT_GET_FLAGS (pHeadNode) & BGP4_RT_HISTORY) ==
          BGP4_RT_HISTORY)))
    {
        BGP4_RT_SET_NEXT (pRoute, pHeadNode, u4VrfId);
        (*ppRtProfileList) = pRoute;
        /* Set the Best flag */
        BGP4_RT_SET_FLAG (pRoute, BGP4_RT_BEST);
        return BGP4_SUCCESS;
    }

    /* Local Routes will be placed as the first route in the list.  
     */
        /* If incoming Route is Local Route and HeadNode is
         *  ON-LOCAL route , directly place the incoming route
         *  as First Node.
         * If incoming route is Local Route and HeadNode both
         *  are redistributed route
         *  AND
         *  incoming route is Network advertised route and the
         *  head node is Redistributed route, then place the
         *  incoming route as first node in profilelist */

    if ((BGP4_RT_PROTOCOL (pRoute) == BGP4_LOCAL_ID) &&
           ((BGP4_RT_PROTOCOL (pHeadNode) != BGP4_LOCAL_ID) ||
           ((BGP4_RT_PROTOCOL (pHeadNode) == BGP4_LOCAL_ID) &&
             ((BGP4_RT_GET_ADV_FLAG (pHeadNode) == BGP4_REDIST_ADV_ROUTE))
           && (BGP4_RT_GET_ADV_FLAG (pRoute) == BGP4_NETWORK_ADV_ROUTE))))
    {
        /* Reset the Best flag for the old route. */
        BGP4_RT_RESET_FLAG (pHeadNode, BGP4_RT_BEST);
#ifdef L3VPN
        if (u4VrfId != BGP4_DFLT_VRFID)
        {
            Bgp4Vpnv4ResetVrfFlags (pHeadNode, u4VrfId, BGP4_RT_BGP_VRF_BEST);
        }
#endif
        BGP4_RT_SET_NEXT (pRoute, pHeadNode, u4VrfId);
        (*ppRtProfileList) = pRoute;
        /* Set the Best flag */
        BGP4_RT_SET_FLAG (pRoute, BGP4_RT_BEST);
#ifdef L3VPN
        if (u4VrfId != BGP4_DFLT_VRFID)
        {
            Bgp4Vpnv4SetVrfFlags (pRoute, u4VrfId, BGP4_RT_BGP_VRF_BEST);
        }
#endif
        return BGP4_SUCCESS;
    }

    u1Origin = BGP4_INFO_ORIGIN (pBgpInfo);
    u2ASPathLen = Bgp4AttrAspathLength (BGP4_INFO_ASPATH (pBgpInfo));
    pNewRtASPath = BGP4_INFO_ASPATH (pBgpInfo);
    u4MED = BGP4_RT_MED (pRoute);
    u4LocalPref = BGP4_RT_LOCAL_PREF (pRoute);
    u2Weight = BGP4_INFO_WEIGHT (pBgpInfo);
    u1IsAggr = (UINT1) (BGP4_INFO_ATTR_FLAG (pBgpInfo) &
                        BGP4_ATTR_ATOMIC_AGGR_MASK);
    i4NexthopMetric = BGP4_RT_NH_METRIC (pRoute);
    u4OrigId = BGP4_INFO_ORIG_ID (pBgpInfo);
    u2ClusCnt = ((BGP4_INFO_CLUS_LIST_ATTR (pBgpInfo) != NULL) ?
                 BGP4_INFO_CLUS_LIST_COUNT (pBgpInfo) : 0);

    if ((BGP4_INFO_ECOMM_ATTR (pRoute->pRtInfo) != NULL) &&
        (BGP4_INFO_ECOMM_ATTR_COST_COMM_FLAG (pRoute) == BGP4_TRUE))
    {
        u4ExtCommCostVal = BGP4_INFO_ECOMM_ATTR_COST_COMM_VAL (pRoute);
        u2ExtCommCostCommId = BGP4_INFO_ECOMM_ATTR_COST_COMM_ID (pRoute);
    }
    else
    {
        u4ExtCommCostVal = BGP4_ECOMM_COST_DEFAULT;
    }

#ifdef L3VPN
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRoute),
                           BGP4_RT_SAFI_INFO (pRoute), u4AsafiMask);
#endif
    if (BGP4_RT_PROTOCOL (pRoute) == BGP_ID)
    {
        pSrcPeerEntry = BGP4_RT_PEER_ENTRY (pRoute);
        u4BgpId = BGP4_PEER_BGP_ID (pSrcPeerEntry);
        u2SrcPeerType =
            BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pSrcPeerEntry),
                                pSrcPeerEntry);
    }
    else
    {
        u4BgpId = BGP4_LOCAL_BGP_ID (BGP4_RT_CXT_ID (pRoute));
    }

    BGP4_DBG2 (BGP4_DBG_CTRL_FLOW,
               "\t--- MED, LP of the new rt = %d,%d\n", u4MED, u4LocalPref);

    while (pRtList != NULL)
    {
        if ((BGP4_RT_PROTOCOL (pRtList) == BGP4_LOCAL_ID) &&
           ((BGP4_RT_PROTOCOL (pRoute) != BGP4_LOCAL_ID) ||
           ((BGP4_RT_PROTOCOL (pRoute) == BGP4_LOCAL_ID) &&
             ((BGP4_RT_GET_ADV_FLAG (pRoute) == BGP4_REDIST_ADV_ROUTE))
             && (BGP4_RT_GET_ADV_FLAG (pRtList) == BGP4_NETWORK_ADV_ROUTE))))
        {
            /* Local routes are skipped from the Decision process. */
            /* Handling done to always elect Network advertised
             * route as best route out of Network advertised and
             * redistrisbuted route */
            pPrevRoute = pRtList;
            BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtList);
            continue;
        }

        /* If the current route is not valid, then add the new route
         * before the current route. */
        if ((BGP4_IS_ROUTE_VALID (pRtList) == BGP4_FALSE) ||
            ((BGP4_RT_GET_FLAGS (pRtList) & BGP4_RT_HISTORY) ==
             BGP4_RT_HISTORY))
        {
            if (BGP4_RT_GET_FLAGS (pRtList) & BGP4_RT_HISTORY)
            {
                break;
            }
        }

        pBgpInfo = BGP4_RT_BGP_INFO (pRtList);

        BGP4_DBG2 (BGP4_DBG_CTRL_FLOW,
                   "\t--- MED, LP of scan rt = %d,%d \n",
                   BGP4_RT_MED (pRtList), BGP4_RT_LOCAL_PREF (pRtList));

        /* Select the route learnt via redistribution path over the route
         * learnt from BGP Peers */
        if ((BGP4_RT_PROTOCOL (pRtList) == BGP_ID) &&
            (BGP4_RT_PROTOCOL (pRoute) != BGP_ID))
        {
            /* New route is non-BGP route and old route is BGP route */
            BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\t--- Non-BGP Route Selected\n");
            break;
        }
        else if ((BGP4_RT_PROTOCOL (pRtList) != BGP_ID) &&
                 (BGP4_RT_PROTOCOL (pRoute) == BGP_ID))
        {
            /* New route is BGP route and old route is Non-BGP route */
            pPrevRoute = pRtList;
            BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtList);
            continue;
        }
        else if ((BGP4_RT_PROTOCOL (pRtList) != BGP_ID) &&
                 (BGP4_RT_PROTOCOL (pRoute) != BGP_ID))
        {
            /* Both routes are Non-BGP routes. Select the new route as
             * the best route. 
	       If the med value populated and the new one is having less MED 
	       we can choose that as a best route, otherwise we can check
	       the remaining attributes	*/
	       if(!((BGP4_RT_MED(pRtList) == BGP4_RT_MED(pRoute))
		   ))
		{
               if(BGP4_RT_MED(pRtList) < BGP4_RT_MED(pRoute))
               {
                pPrevRoute = pRtList;
            BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtList);
                continue;
                }
                else
                {
            break;
        }
		}
		
        }

#ifdef L3VPN
        if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
        {
            if ((!(BGP4_RT_GET_FLAGS (pRtList) & BGP4_RT_CONVERTED_TO_VPNV4))
                && (BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_CONVERTED_TO_VPNV4))
            {
                /* CE routes must be chosen over PE routes */
                break;
            }
        }
#endif
        /* Select the route which have got the highest weight */
        if (u2Weight > BGP4_INFO_WEIGHT (pBgpInfo))
        {
            BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\t--- Higher Weight \n");
            break;
        }
        if (u2Weight < BGP4_INFO_WEIGHT (pBgpInfo))
        {
            BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\t--- Lower Weight \n");
            pPrevRoute = pRtList;
            BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtList);
            continue;
        }

        /* Select the route which have got the highest local Preference */
        if (BGP4_RT_PEER_ENTRY (pRoute) != NULL)
        {
	    if(u4LocalPref == 0)
	    {
            	if (BGP4_GET_PEER_TYPE (u4VrfId, BGP4_RT_PEER_ENTRY (pRoute)) == BGP4_EXTERNAL_PEER)
            	{
                	u4LocalPref = BGP4_DEFAULT_LOCAL_PREF (u4VrfId);
            	}
	    }
        }
        u4LocalPrefRtList = BGP4_RT_LOCAL_PREF (pRtList);
        if (BGP4_RT_PEER_ENTRY (pRtList) != NULL)
        {
            if(u4LocalPrefRtList == 0)
            {
               if (BGP4_GET_PEER_TYPE (u4VrfId, BGP4_RT_PEER_ENTRY (pRtList)) == BGP4_EXTERNAL_PEER)
               {
                  u4LocalPref = BGP4_DEFAULT_LOCAL_PREF (u4VrfId);
               }
            }

        }
        if (u4LocalPref > u4LocalPrefRtList)
        {
            BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\t--- Higher LOCAL_PREF \n");
            break;
        }
        if (u4LocalPref < u4LocalPrefRtList)
        {
            BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\t--- Lower LOCAL_PREF \n");
            pPrevRoute = pRtList;
            BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtList);
            continue;
        }

        /* If Local Preference are same then compare the AS Path length 
         * and select the rout with smaller Path length.  
         */
        if (u2ASPathLen < Bgp4AttrAspathLength (BGP4_INFO_ASPATH (pBgpInfo)))
        {
            BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\t--- ASpath len. shorter\n");
            break;
        }
        if (u2ASPathLen > Bgp4AttrAspathLength (BGP4_INFO_ASPATH (pBgpInfo)))
        {
            BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\t--- ASpath len. higher\n");
            pPrevRoute = pRtList;
            BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtList);
            continue;
        }

        /* Select the route received from BGP speaker having the lowest
           CLUSTER length value */
        u2ClusCntnewRt = ((BGP4_INFO_CLUS_LIST_ATTR (pBgpInfo) != NULL) ?
                          BGP4_INFO_CLUS_LIST_COUNT (pBgpInfo) : 0);

        if (u2ClusCnt < u2ClusCntnewRt)
        {
            BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\t--- Cluster length lower\n");
            break;
        }

        if (u2ClusCnt > u2ClusCntnewRt)
        {
            pPrevRoute = pRtList;
            BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtList);
            BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\t--- Cluster length higher\n");
            continue;
        }

        /* If CLUSTER length values are identical, then select the route with the
         * the lowest origin. IGP -> EGP -> INCOMPLETE */
        if (u1Origin < BGP4_INFO_ORIGIN (pBgpInfo))
        {
            BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\t--- Origin lower\n");
            break;
        }
        if (u1Origin > BGP4_INFO_ORIGIN (pBgpInfo))
        {
            pPrevRoute = pRtList;
            BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtList);
            BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\t--- Origin higher\n");
            continue;
        }

        pPresentRtASPath = BGP4_INFO_ASPATH (pBgpInfo);
        /* decide MED value shud be compared or not */
        if (Bgp4DechCompareMED (u4VrfId, pPresentRtASPath, pNewRtASPath) ==
            BGP4_TRUE)
        {
            if (u4MED < BGP4_RT_MED (pRtList))
            {
                BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\t--- MED lower\n");
                break;
            }
            if (u4MED > BGP4_RT_MED (pRtList))
            {
                BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\t--- MED higher\n");
                pPrevRoute = pRtList;
                BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtList);
                continue;
            }
        }

        /* if the existing route is received from an internal peer
         *  and the newly received route is from an external peer
         *  then choose the route from the external peer 
         */
        pScanPeerEntry = BGP4_RT_PEER_ENTRY (pRtList);
        if (pScanPeerEntry != NULL)
        {
            u2ScanPeerType =
                BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pScanPeerEntry),
                                    pScanPeerEntry);
        }
        /* treat confed peers as internal peers */

        /* If ScanPeer is external (outside the confed), and 
         * SrcPeer is Internal, or external but within the confed
         */
        if ((u2ScanPeerType == BGP4_EXTERNAL_PEER) &&
            ((pScanPeerEntry != NULL)
             && (BGP4_CONFED_PEER_STATUS (pScanPeerEntry) == BGP4_FALSE))
            && ((u2SrcPeerType == BGP4_INTERNAL_PEER)
                || ((pScanPeerEntry != NULL)
                    && (BGP4_CONFED_PEER_STATUS (pSrcPeerEntry) == BGP4_TRUE))))
        {
            /* compare with next route */
            pPrevRoute = pRtList;
            BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtList);
            continue;
        }

        /* If SrcPeer is external (outside the confed), and 
         * ScanPeer is Internal, or external but within the confed
         */
        if ((u2SrcPeerType == BGP4_EXTERNAL_PEER) &&
            ((pScanPeerEntry != NULL) &&
             (BGP4_CONFED_PEER_STATUS (pSrcPeerEntry) == BGP4_FALSE)) &&
            ((u2ScanPeerType == BGP4_INTERNAL_PEER) ||
             ((pScanPeerEntry != NULL) &&
              (BGP4_CONFED_PEER_STATUS (pScanPeerEntry) == BGP4_TRUE))))
        {
            BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                      "\t-- Route Recvd from external peer\n");
            /* keep the new route here */
            break;
        }

        /* Select the route with lower interior cost */
        if (i4NexthopMetric < BGP4_RT_NH_METRIC (pRtList))
        {
            BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\t--- Nexthop metric lower\n");
            break;
        }
        if (i4NexthopMetric > BGP4_RT_NH_METRIC (pRtList))
        {
            pPrevRoute = pRtList;
            BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtList);
            continue;
        }
        /* if the POI value 129 the extended community attributes are considered for 
         * the best route selection process after the Metric attribute. */
        /*If the Ext Community Attributes are present those attributes are
         *considered */
        if ((BGP4_INFO_ECOMM_ATTR (pRtList->pRtInfo) != NULL) &&
            (BGP4_INFO_ECOMM_ATTR_COST_COMM_FLAG (pRtList) == BGP4_TRUE))
        {
            u4ExtCommCostValList = BGP4_INFO_ECOMM_ATTR_COST_COMM_VAL (pRtList);
            u2ExtCommCostCommIdList =
                BGP4_INFO_ECOMM_ATTR_COST_COMM_ID (pRtList);
        }
        else
        {
            u4ExtCommCostValList = BGP4_ECOMM_COST_DEFAULT;
        }

        /* Lower Ext community cost value is preferred in choosing the Best Route */
        if (u4ExtCommCostVal < u4ExtCommCostValList)
        {
            break;
        }
        else if (u4ExtCommCostVal > u4ExtCommCostValList)
        {
            pPrevRoute = pRtList;
            BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtList);
            continue;
        }
        /* If the Ext community cost vlaue is same for two routes then 
         * the lower ExtCommunity Id is considered in the descision process */
        if (u2ExtCommCostCommId < u2ExtCommCostCommIdList)
        {
            break;
        }
        else if (u2ExtCommCostCommId > u2ExtCommCostCommIdList)
        {
            pPrevRoute = pRtList;
            BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtList);
            continue;
        }

        /* Prefer the non-Aggregated route over the Aggregated route */

        if ((u1IsAggr != BGP4_ATTR_ATOMIC_AGGR_MASK)
            && ((BGP4_INFO_ATTR_FLAG (pBgpInfo) & BGP4_ATTR_ATOMIC_AGGR_MASK)
                == BGP4_ATTR_ATOMIC_AGGR_MASK))
        {
            BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\t--- Differs in Aggr. flag\n");
            break;
        }
        if ((u1IsAggr == BGP4_ATTR_ATOMIC_AGGR_MASK)
            && ((BGP4_INFO_ATTR_FLAG (pBgpInfo) & BGP4_ATTR_ATOMIC_AGGR_MASK)
                != BGP4_ATTR_ATOMIC_AGGR_MASK))
        {
            pPrevRoute = pRtList;
            BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtList);
            continue;
        }

        /* Select the route received from BGP speaker having the lowest
           ORIGINATOR id value */
        if (u4OrigId < BGP4_INFO_ORIG_ID (pBgpInfo))
        {
            BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\t--- Originator id lower\n");
            break;
        }

        if (u4OrigId > BGP4_INFO_ORIG_ID (pBgpInfo))
        {
            pPrevRoute = pRtList;
            BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtList);
            BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\t--- Originator id higher\n");
            continue;
        }

        /* Select the route received from BGP speaker having the lowest
         * Identifier value */
        if (BGP4_RT_PEER_ENTRY (pRtList) != NULL)
        {
            u4ScanBgpId = BGP4_PEER_BGP_ID (BGP4_RT_PEER_ENTRY (pRtList));
        }
        else
        {
            u4ScanBgpId = BGP4_LOCAL_BGP_ID (BGP4_RT_CXT_ID (pRtList));
        }
        if (u4BgpId < u4ScanBgpId)
        {
            BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\t--- BGP Id lower \n");
            break;
        }

        if (u4BgpId > u4ScanBgpId)
        {
            pPrevRoute = pRtList;
            BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtList);
            BGP4_DBG (BGP4_DBG_CTRL_FLOW, "\t--- BGP id higher\n");
            continue;
        }

        if ((BGP4_RT_PEER_ENTRY (pRoute) != NULL)
            && (BGP4_RT_PEER_ENTRY (pRtList) != NULL))
        {
            if ((PrefixLessThan
                 (BGP4_PEER_REMOTE_ADDR_INFO (BGP4_RT_PEER_ENTRY (pRoute)),
                  BGP4_PEER_REMOTE_ADDR_INFO (BGP4_RT_PEER_ENTRY (pRtList)))) ==
                BGP4_TRUE)
            {
                BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                          "\t--- Neighbor Address lower \n");
                break;
            }
        }
        pPrevRoute = pRtList;
        BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtNext);
        if (pRtNext == NULL)
        {
            /* There are no more routes left for processing. The new route is
             * not preferred as best route over the current existing route.
             * So just add the new route to the end of the list and leave
             * the current best route undistrubed.
             */
            break;
        }
        BGP4_RT_GET_NEXT (pRtList, u4VrfId, pRtList);
    }

    if (pPrevRoute == NULL)
    {
        /* The new node should become the head node */
        BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                  "\t--- NEW ROUTE SELECTED AS THE  BEST ROUTE\n");
        /* Reset the Best flag for the old route */
        BGP4_RT_RESET_FLAG (pHeadNode, BGP4_RT_BEST);
#ifdef L3VPN
        if (u4VrfId != BGP4_DFLT_VRFID)
        {
            Bgp4Vpnv4ResetVrfFlags (pHeadNode, u4VrfId, BGP4_RT_BGP_VRF_BEST);
        }
#endif
        BGP4_RT_SET_NEXT (pRoute, pHeadNode, u4VrfId);
        (*ppRtProfileList) = pRoute;
        /* Set the Best flag */
        BGP4_RT_SET_FLAG (pRoute, BGP4_RT_BEST);
#ifdef L3VPN
        if (u4VrfId != BGP4_DFLT_VRFID)
        {
            Bgp4Vpnv4SetVrfFlags (pRoute, u4VrfId, BGP4_RT_BGP_VRF_BEST);
        }
#endif
    }
    else
    {
        BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                  "\t--- NEW ROUTE IS NOT THE  BEST ROUTE\n");
        BGP4_RT_GET_NEXT (pPrevRoute, u4VrfId, pRtNext);
        BGP4_RT_SET_NEXT (pRoute, pRtNext, u4VrfId);
        BGP4_RT_SET_NEXT (pPrevRoute, pRoute, u4VrfId);
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4DechReRunDecProcess                                   */
/* Description   : This routine re-run the decision process for all the route*/
/*                 present in the given leaf.                                */
/* Input(s)      :  which contains list of routes to a destination       */
/*                 (pLeaf)                                                   */
/*                  u4VrfId - VRF identifier in which decision process is to */
/*                  be rerun                                                 */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4DechReRunDecProcess (tRouteProfile ** pRtProfileList, UINT4 u4VrfId)
{
    tTMO_SLL            TsRtList;
    tRouteProfile      *pRoute = NULL;
    tRouteProfile      *pLastRoute = NULL;
    tRouteProfile      *pNextRoute = NULL;
    tLinkNode          *pLinkNode = NULL;
    UINT4               u4RouteCount = 0;
    UINT1               u1Flag = BGP4_FALSE;

    if (pRtProfileList == NULL)
    {
        return BGP4_FAILURE;
    }
    UNUSED_PARAM (u4VrfId);
    TMO_SLL_Init (&TsRtList);
    pNextRoute = (*pRtProfileList);

    /* Collect all the routes from the leaf. */
    while (pNextRoute)
    {
        pRoute = pNextRoute;
        Bgp4DshAddRouteToList (&TsRtList, pRoute, 0);
        BGP4_RT_GET_NEXT (pRoute, u4VrfId, pNextRoute);
        u4RouteCount++;
    }

    /* Check whether the Leaf route count and route count in list
     * are equal. If not equal then decision process need not be
     * re-executed. This condition is possible if the addition of
     * route to list fails because of mem allocate failure. */
    if (u4RouteCount != TMO_SLL_Count (&TsRtList))
    {
        if ((BGP4_IS_ROUTE_VALID ((*pRtProfileList)) == BGP4_TRUE) &&
            ((BGP4_RT_GET_FLAGS ((*pRtProfileList)) & BGP4_RT_HISTORY) !=
             BGP4_RT_HISTORY) &&
            ((BGP4_RT_PROTOCOL ((*pRtProfileList)) != BGP_ID) ||
             (BGP4_PEER_STATE (BGP4_RT_PEER_ENTRY ((*pRtProfileList))) ==
              BGP4_ESTABLISHED_STATE)))
        {
            /* Set the first route as the best route. */
            BGP4_RT_SET_FLAG ((*pRtProfileList), BGP4_RT_BEST);
#ifdef L3VPN
            if (u4VrfId != BGP4_DFLT_VRFID)
            {
                Bgp4Vpnv4SetVrfFlags ((*pRtProfileList), u4VrfId,
                                      BGP4_RT_BGP_VRF_BEST);
            }
#endif
        }
        Bgp4DshReleaseList (&TsRtList, 0);
        return BGP4_SUCCESS;
    }

    pRoute = NULL;
    (*pRtProfileList) = NULL;

    TMO_SLL_Scan (&TsRtList, pLinkNode, tLinkNode *)
    {
        pRoute = pLinkNode->pRouteProfile;
        /* Reset the route's Next Pointer and the Best Flag. */
        BGP4_RT_RESET_FLAG (pRoute, BGP4_RT_BEST);
#ifdef L3VPN
        if (u4VrfId != BGP4_DFLT_VRFID)
        {
            Bgp4Vpnv4ResetVrfFlags (pRoute, u4VrfId, BGP4_RT_BGP_VRF_BEST);
        }
#endif
        /* Reset the route's Next Pointer. */
        BGP4_RT_SET_NEXT (pRoute, NULL, u4VrfId);

        if ((BGP4_IS_ROUTE_VALID (pRoute) == BGP4_FALSE) ||
            ((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_WITHDRAWN) == BGP4_RT_WITHDRAWN) ||
            ((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_HISTORY) == BGP4_RT_HISTORY) || 
            (((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_STALE) == BGP4_RT_STALE) && 
             ((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_WITHDRAWN) == BGP4_RT_WITHDRAWN)))
        {
            /* Route is not valid. Add it to the end of the list. */
            if ((*pRtProfileList) == NULL)
            {
                (*pRtProfileList) = pRoute;
            }
            else
            {
                BGP4_RT_NEXT (pLastRoute) = pRoute;
            }
        }
        else if ((BGP4_RT_PROTOCOL (pRoute) == BGP_ID) &&
                 (BGP4_GET_PEER_CURRENT_STATE (BGP4_RT_PEER_ENTRY (pRoute)) ==
                  BGP4_PEER_DEINIT_INPROGRESS))
        {
            /* Peer is going through DeInit. */
            if ((*pRtProfileList) == NULL)
            {
                /* Add the route as the First route. But ensure that the
                 * next valid route replaces this route as the best route.
                 * Else we will be processing this route as new best route.
                 */
                (*pRtProfileList) = pRoute;
                u1Flag = BGP4_TRUE;
            }
            else
            {
                /* Place this route at the end of the list. */
                BGP4_RT_SET_NEXT (pLastRoute, pRoute, u4VrfId);
            }
        }
        else
        {
            if ((*pRtProfileList) == NULL)
            {
                /* Add the first route in the list to the leaf */
                (*pRtProfileList) = pRoute;
                BGP4_RT_SET_FLAG (pRoute, BGP4_RT_BEST);
#ifdef L3VPN
                if (u4VrfId != BGP4_DFLT_VRFID)
                {
                    Bgp4Vpnv4SetVrfFlags (pRoute, u4VrfId,
                                          BGP4_RT_BGP_VRF_BEST);
                }
#endif
            }
            else
            {
                if (u1Flag == BGP4_TRUE)
                {
                    /* Route is VALID and DeInit Peer's Route is the
                     * current best route. Just add this route as
                     * the best route. */
                    u1Flag = BGP4_FALSE;
                    BGP4_RT_SET_NEXT (pRoute, (*pRtProfileList), u4VrfId);
                    (*pRtProfileList) = pRoute;
                    BGP4_RT_SET_FLAG (pRoute, BGP4_RT_BEST);
#ifdef L3VPN
                    if (u4VrfId != BGP4_DFLT_VRFID)
                    {
                        Bgp4Vpnv4SetVrfFlags (pRoute, u4VrfId,
                                              BGP4_RT_BGP_VRF_BEST);
                    }
#endif
                    /* No need to update the Las route, since the current route
                     * is added as the First Route. */
                    continue;
                }
                else
                {
#ifdef L3VPN
                    Bgp4Vpnv4SetVrfFlags (pRoute, u4VrfId,
                                          BGP4_RT_PROCESS_VRF_RIB);
#endif
                    /* Run the decision process. */
                    Bgp4DechAddRouteSorted (pRtProfileList, pRoute);
#ifdef L3VPN
                    Bgp4Vpnv4ResetVrfFlags (pRoute, u4VrfId,
                                            BGP4_RT_PROCESS_VRF_RIB);
#endif
                    /* Update the last route. */
                    BGP4_RT_GET_NEXT (pRoute, u4VrfId, pNextRoute);
                    if (pNextRoute != NULL)
                    {
                        /* This route is not the last route. */
                        continue;
                    }
                }
            }
        }
        pLastRoute = pRoute;
    }
    Bgp4DshReleaseList (&TsRtList, 0);
    return BGP4_SUCCESS;
}
#endif /* BGP4DECH_C */
