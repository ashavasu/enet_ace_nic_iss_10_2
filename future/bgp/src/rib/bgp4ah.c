/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4ah.c,v 1.56 2017/09/15 06:19:54 siva Exp $
 *
 * Description: This file contains modules which are used when the 
 *              update needs to be sent to other known peers.
 *
 *******************************************************************/

#ifndef   BGP4ADH_C
#define   BGP4ADH_C

#include "bgp4com.h"

/*****************************************************************************/
/* Function Name : Bgp4AhUpdateFeasibleRouteList                             */
/* Description   : This routine updates the feasible route list or the       */
/*                 withdrawn route list by checking whether that particular  */
/*                 feasible route can be send to that peer or not.           */
/* Input(s)      : Peer to which this route is supposed to be advertised     */
/*                 (pPeer),                                                  */
/*                 Feasible route which is going to be advertised            */
/*                 (pFeasRoute)                                              */
/* Output(s)     : Updated feasible route list (pFeasRouteList),             */
/*                 Updated withdrawn route list (pWithdrawnRouteList)        */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4AhUpdateFeasibleRouteList (tBgp4PeerEntry * pPeer,
                               tRouteProfile * pFeasRoute,
                               tTMO_SLL * pFeasRouteList,
                               tTMO_SLL * pWithdrawnRouteList)
{

    INT4                i4RetVal = BGP4_SUCCESS;

    if (BGP4_RT_PROTOCOL (pFeasRoute) == BGP_ID)
    {
        if ((BGP4_RT_PEER_ENTRY (pFeasRoute) == pPeer)
            && ((BGP4_RT_GET_FLAGS (pFeasRoute) & BGP4_RT_ADVT_WITHDRAWN) ==
                BGP4_RT_ADVT_WITHDRAWN))
        {
            BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                      "\tBgp4AhUpdateFeasibleRouteList() : Feas. "
                      "specific WITHDRAWN\n");
            Bgp4AhUpdateWithdrawnRouteList (pPeer, pFeasRoute,
                                            pWithdrawnRouteList);
            BGP4_RT_RESET_FLAG (pFeasRoute, BGP4_RT_ADVT_WITHDRAWN);
            return BGP4_SUCCESS;
        }
    }
    i4RetVal = Bgp4AhAddFeasibleRouteToPeerlist (pPeer, pFeasRoute,
                                                 pFeasRouteList,
                                                 pWithdrawnRouteList);

    if (i4RetVal == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4AhUpdateFeasibleRouteList () : Add_Feasible_Route_to_PeerList"
                  "returning Failure\n");
        return i4RetVal;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AhUpdateWithdrawnRouteList                            */
/* Description   : This routine updates the withdrawn route list by checking */
/*                 whether this particular withdrawn route can be send to    */
/*                 that peer or not.                                         */
/* Input(s)      : Peer to which this route is supposed to be advertised     */
/*                 (pPeer),                                                  */
/*                 Unfeasible route which is going to be advertised          */
/*                 (pWithdrawnRoute)                                         */
/* Output(s)     : Updated feasible route list (pFeasRouteList),             */
/*                 Updated withdrawn route list (pWithdrawnRouteList)        */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4AhUpdateWithdrawnRouteList (tBgp4PeerEntry * pPeer,
                                tRouteProfile * pWithdrawnRoute,
                                tTMO_SLL * pWithdrawnRouteList)
{
    tRouteProfile      *pRtProfile = NULL;
    UINT4               u4UpdSentTime = 0;
    UINT4               u1MinRAFlag = BGP4_FALSE;
    UINT4               u1MinOrigFlag = BGP4_FALSE;

    pRtProfile = Bgp4DshRemoveRouteFromPeerList (pPeer, BGP4_PEER_OUTPUT_LIST,
                                                 pWithdrawnRoute);
    if (pRtProfile == NULL)
    {
        pRtProfile = Bgp4DshRemoveRtFrmPeerAdvtList (pPeer,
                                                     BGP4_PEER_NEW_LIST,
                                                     pWithdrawnRoute,
                                                     &u4UpdSentTime);
        if (pRtProfile != NULL)
        {
            if (((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_WITHDRAWN)
                 == BGP4_RT_WITHDRAWN) ||
                ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGR_WITHDRAWN)
                 == BGP4_RT_AGGR_WITHDRAWN))
            {
                u1MinRAFlag = BGP4_TRUE;
            }

        }

        if (pRtProfile == NULL || u1MinRAFlag == BGP4_TRUE)
        {
            pRtProfile =
                Bgp4DshRemoveRouteFromPeerList (pPeer,
                                                BGP4_PEER_NEW_LOCAL_LIST,
                                                pWithdrawnRoute);

            if (pRtProfile != NULL)
            {
                if (((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_WITHDRAWN)
                     == BGP4_RT_WITHDRAWN) ||
                    ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_AGGR_WITHDRAWN)
                     == BGP4_RT_AGGR_WITHDRAWN))
                {
                    u1MinOrigFlag = BGP4_TRUE;
                }
            }

            if (pRtProfile == NULL || u1MinOrigFlag == BGP4_TRUE)
            {
                Bgp4DshAddRouteToList (pWithdrawnRouteList, pWithdrawnRoute, 0);
            }
        }
    }
    /* Remove the old feasible route, if present in the ADVT feasible list */
    if (pRtProfile == NULL)
    {
        Bgp4DshRemoveRtFrmPeerAdvtList (pPeer, BGP4_PEER_ADVT_FEAS_LIST,
                                        pWithdrawnRoute, &u4UpdSentTime);
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AhAddFeasibleRouteToPeerlist                          */
/* Description   : This routine applies the Filter Table entries on the route*/
/*                 If the route needs to be filtered, then it will be added  */
/*                 to the peers input list.  Else, if the route can be       */
/*                 advertised then it checks whether this feasible route can */
/*                 be advertised now or to be advertised after certain period*/
/*                 of time. If it needs to be advertised later then it is    */
/*                 appended in the appropriate list.                         */
/* Input(s)      : Peer to which this route is supposed to be advertised     */
/*                 (pPeer),                                                  */
/*                 Feasible route which is going to be advertised            */
/*                 (pFeasRoute)                                              */
/* Output(s)     : Updated feasible route list (pFeasRouteList),             */
/*                 Updated withdrawn route list (pWithdrawnRouteList)        */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4AhAddFeasibleRouteToPeerlist (tBgp4PeerEntry * pPeer,
                                  tRouteProfile * pFeasRoute,
                                  tTMO_SLL * pFeasRouteList,
                                  tTMO_SLL * pWithdrawnRouteList)
{
    tTMO_SLL           *pPeerRtlist = NULL;
    tRouteProfile      *pOldFeasRoute = NULL;
    tRouteProfile      *pRtProfile = NULL;
    BOOL1               bIsRtLocal = FALSE;
    UINT4               u4TimerValue = 0;
    UINT4               u4RtCnt = 0;
    UINT4               u4HashKey = 0x0;
    UINT4               u4AsafiMask;
    UINT4               u4UpdSentTime = 0;
    UINT4               u4UpdToSendTime = 0;
    UINT4               u4CurTime = 0;
    INT4                i4RetVal = BGP4_SUCCESS;

    UNUSED_PARAM (pWithdrawnRouteList);

    bIsRtLocal = Bgp4IsLocalroute (pFeasRoute);

    /* Check whether it is a replacement route for a 
     * feasible route which is yet to be advertised. 
     */
    pRtProfile = Bgp4DshRemoveRtFrmPeerAdvtList (pPeer, BGP4_PEER_NEW_LIST,
                                                 pFeasRoute, &u4UpdSentTime);
    if (pRtProfile == NULL)
    {
        pRtProfile =
            Bgp4DshRemoveRouteFromPeerList (pPeer, BGP4_PEER_NEW_LOCAL_LIST,
                                            pFeasRoute);
        if (pRtProfile == NULL)
        {
            Bgp4DshRemoveRouteFromPeerList (pPeer, BGP4_PEER_OUTPUT_LIST,
                                            pFeasRoute);
        }
    }

    Bgp4GetRouteHashIndex (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                           (BGP4_RT_NET_ADDRESS_INFO (pFeasRoute)),
                           (UINT1) BGP4_RT_PREFIXLEN (pFeasRoute), &u4HashKey);

    Bgp4GetPeerAdvtAfiSafi (pFeasRoute, pPeer, &u4AsafiMask);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
            if (bIsRtLocal == FALSE)
            {
                pPeerRtlist =
                    &(BGP4_PEER_IPV4_NEW_ROUTES (pPeer))->HashList[u4HashKey];
                u4RtCnt =
                    Bgp4DshGetHashTableCnt (BGP4_PEER_IPV4_NEW_ROUTES (pPeer));
            }
            else
            {
                pPeerRtlist =
                    &(BGP4_PEER_IPV4_NEW_LOCAL_ROUTES (pPeer))->
                    HashList[u4HashKey];
                u4RtCnt = Bgp4DshGetHashTableCnt
                    (BGP4_PEER_IPV4_NEW_LOCAL_ROUTES (pPeer));
            }
            break;
#ifdef VPLSADS_WANTED
            /*ADS-VPLS related processing */
        case CAP_MP_L2VPN_VPLS:
            if (bIsRtLocal == FALSE)
            {
                pPeerRtlist =
                    &(BGP4_PEER_VPLS_NEW_ROUTES (pPeer))->HashList[u4HashKey];
                u4RtCnt =
                    Bgp4DshGetHashTableCnt (BGP4_PEER_VPLS_NEW_ROUTES (pPeer));
            }
            else
            {
                pPeerRtlist =
                    &(BGP4_PEER_VPLS_NEW_LOCAL_ROUTES (pPeer))->
                    HashList[u4HashKey];
                u4RtCnt = Bgp4DshGetHashTableCnt
                    (BGP4_PEER_VPLS_NEW_LOCAL_ROUTES (pPeer));
            }
            break;
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
            if (bIsRtLocal == FALSE)
            {
                pPeerRtlist =
                    &(BGP4_PEER_EVPN_NEW_ROUTES (pPeer))->HashList[u4HashKey];
                u4RtCnt =
                    Bgp4DshGetHashTableCnt (BGP4_PEER_EVPN_NEW_ROUTES (pPeer));
            }
            else
            {
                pPeerRtlist =
                    &(BGP4_PEER_EVPN_NEW_LOCAL_ROUTES (pPeer))->
                    HashList[u4HashKey];
                u4RtCnt = Bgp4DshGetHashTableCnt
                    (BGP4_PEER_EVPN_NEW_LOCAL_ROUTES (pPeer));
            }
            break;
#endif
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
            if (bIsRtLocal == FALSE)
            {
                pPeerRtlist =
                    &(BGP4_PEER_IPV6_NEW_ROUTES (pPeer))->HashList[u4HashKey];
                u4RtCnt =
                    Bgp4DshGetHashTableCnt (BGP4_PEER_IPV6_NEW_ROUTES (pPeer));
            }
            else
            {
                pPeerRtlist =
                    &(BGP4_PEER_IPV6_NEW_LOCAL_ROUTES (pPeer))->
                    HashList[u4HashKey];
                u4RtCnt = Bgp4DshGetHashTableCnt
                    (BGP4_PEER_IPV6_NEW_LOCAL_ROUTES (pPeer));
            }
            break;
#endif
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
            if (bIsRtLocal == FALSE)
            {
                pPeerRtlist =
                    &(BGP4_PEER_IPV4_LBLD_NEW_ROUTES (pPeer))->
                    HashList[u4HashKey];
                u4RtCnt =
                    Bgp4DshGetHashTableCnt (BGP4_PEER_IPV4_LBLD_NEW_ROUTES
                                            (pPeer));
            }
            else
            {
                pPeerRtlist =
                    &(BGP4_PEER_IPV4_LBLD_NEW_LOCAL_ROUTES (pPeer))->
                    HashList[u4HashKey];
                u4RtCnt = Bgp4DshGetHashTableCnt
                    (BGP4_PEER_IPV4_LBLD_NEW_LOCAL_ROUTES (pPeer));
            }
            break;
        case CAP_MP_VPN4_UNICAST:
            if (BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_CE_PEER)
            {
                if (bIsRtLocal == FALSE)
                {
                    pPeerRtlist =
                        &(BGP4_PEER_IPV4_NEW_ROUTES (pPeer))->
                        HashList[u4HashKey];
                    u4RtCnt =
                        Bgp4DshGetHashTableCnt (BGP4_PEER_IPV4_NEW_ROUTES
                                                (pPeer));
                }
                else
                {
                    pPeerRtlist =
                        &(BGP4_PEER_IPV4_NEW_LOCAL_ROUTES (pPeer))->
                        HashList[u4HashKey];
                    u4RtCnt =
                        Bgp4DshGetHashTableCnt (BGP4_PEER_IPV4_NEW_LOCAL_ROUTES
                                                (pPeer));
                }
            }
            else
            {
                if (bIsRtLocal == FALSE)
                {
                    pPeerRtlist =
                        &(BGP4_PEER_VPN4_NEW_ROUTES (pPeer))->
                        HashList[u4HashKey];
                    u4RtCnt =
                        Bgp4DshGetHashTableCnt (BGP4_PEER_VPN4_NEW_ROUTES
                                                (pPeer));
                }
                else
                {
                    pPeerRtlist =
                        &(BGP4_PEER_VPN4_NEW_LOCAL_ROUTES (pPeer))->
                        HashList[u4HashKey];
                    u4RtCnt =
                        Bgp4DshGetHashTableCnt (BGP4_PEER_VPN4_NEW_LOCAL_ROUTES
                                                (pPeer));
                }
            }
            break;
#endif
        default:
            return BGP4_SUCCESS;
    }

    if (bIsRtLocal == FALSE)
    {
        /* If route is received from internal peer, advertise immediately */
        if (BGP4_GET_PEER_TYPE
            (BGP4_RT_CXT_ID (pFeasRoute),
             BGP4_RT_PEER_ENTRY (pFeasRoute)) == BGP4_INTERNAL_PEER)
        {
            /* Route is received from Internal Peer */
            Bgp4DshAddRouteToList (pFeasRouteList, pFeasRoute, 0);
            return BGP4_SUCCESS;
        }
    }
    else
    {
        /* Route is Originated within the same AS. If the peer is
         * going through Init/soft-Outbound/RFD Reuse, then add the
         * route to the FeasRouteList. Else add the route to the Peer's
         * NEW_LOCAL ROUTE List.
         */
        if ((BGP4_GET_PEER_CURRENT_STATE (pPeer) ==
             BGP4_PEER_INIT_INPROGRESS) ||
            (BGP4_GET_PEER_CURRENT_STATE (pPeer) ==
             BGP4_PEER_SOFTCONFIG_OUTBOUND_INPROGRESS) ||
            (BGP4_GET_PEER_CURRENT_STATE (pPeer) == BGP4_PEER_REUSE_INPROGRESS))
        {
            /* Add the route to the the Feasible list. */
            Bgp4DshAddRouteToList (pFeasRouteList, pFeasRoute, 0);
        }
        else
        {
            /* Adding the route to the Peer's NEW_LOCAL ROUTE LIST. */
            Bgp4DshAddRouteToList (pPeerRtlist, pFeasRoute, 0);
        }
        return BGP4_SUCCESS;
    }

    /* If new route (not previously advertised), send immediately */
    if ((BGP4_RT_GET_FLAGS (pFeasRoute) & BGP4_RT_REPLACEMENT) !=
        BGP4_RT_REPLACEMENT)
    {
        pOldFeasRoute =
            Bgp4DshGetRouteFromPeerAdvList (pPeer, BGP4_PEER_ADVT_WITH_LIST,
                                            pFeasRoute, &u4UpdSentTime);
        if (pOldFeasRoute == NULL)
        {
            Bgp4DshAddRouteToList (pFeasRouteList, pFeasRoute, 0);
            return BGP4_SUCCESS;
        }
        else
        {
            /* Old route is present in Peer's Advt Withdrawn list. Means we
             * need to wait. Update the Time stamp and add it to the
             * corresponding timer list. */
        }
    }

    /* If (re-advertised|replacement) route
     *       Check If the route can be advertised now.
     *       If YES,
     *          Advertise the route.
     *       Else
     *          Update the Time Stamp for the route.
     *          If timer is not started (list cnt == 0, means no timer running), 
     *             Start the corresponding Timer.
     *          Add the route to peer's new (local) list.
     */

    if ((BGP4_RT_GET_FLAGS (pFeasRoute) & BGP4_RT_REPLACEMENT) ==
        BGP4_RT_REPLACEMENT)
    {
        pOldFeasRoute =
            Bgp4DshGetRouteFromPeerAdvList (pPeer, BGP4_PEER_ADVT_FEAS_LIST,
                                            pFeasRoute, &u4UpdSentTime);
    }

    u4CurTime = Bgp4ElapTime ();
    if (pOldFeasRoute != NULL)
    {
        if ((Bgp4DiffTime (u4CurTime, u4UpdSentTime))
            >= BGP4_PEER_MIN_ADV_TIME (pPeer))
        {
            /* New route can be advertised. The removal of Old route will
             * be carried out by one-second timer expiry handler. */
            Bgp4DshAddRouteToList (pFeasRouteList, pFeasRoute, 0);
            return BGP4_SUCCESS;
        }
        else
        {
            /* Route needs to be deferred from advertising. Update the 
             * routes time stamp value and start the corresponding peer
             * advertisement timer if required. */
            u4UpdToSendTime = BGP4_PEER_MIN_ADV_TIME (pPeer) + u4UpdSentTime;
            if (u4RtCnt == 0)
            {
                u4TimerValue
                    = BGP4_PEER_MIN_ADV_TIME (pPeer) -
                    (Bgp4DiffTime (u4CurTime, u4UpdSentTime));

                Bgp4TmrhStartTimer (BGP4_MINROUTEADV_TIMER,
                                    (VOID *) pPeer, u4TimerValue);
            }
            /* Add the route to the corresponding peer list. */
            i4RetVal = Bgp4DshAddRouteToPeerAdvtList (pPeerRtlist, pFeasRoute,
                                                      BGP4_TIMESTAMP,
                                                      u4UpdToSendTime);

            if (i4RetVal == BGP4_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                          "\tBgp4AhAddFeasibleRouteToPeerlist () : Add_Route_to_Peer_AdvList"
                          "returning Failure\n");
                return i4RetVal;
            }

            return BGP4_SUCCESS;
        }
    }
    /* The following code would prevent the external replacement routes getting 
     * advertised for a Min Rt Adv period after Peer initialization */
    else if (pPeer->peerLocal.tRouteAdvTmr.u4Flag == BGP4_DORMANT)
    {
        i4RetVal = Bgp4DshAddRouteToPeerAdvtList (pPeerRtlist, pFeasRoute,
                                                  BGP4_TIMESTAMP, u4CurTime);

        if (i4RetVal == BGP4_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                      "\tBgp4AhAddFeasibleRouteToPeerlist () : Add_Route_to_Peer_AdvList"
                      "returning Failure\n");
            return i4RetVal;
        }
        return BGP4_SUCCESS;
    }

    Bgp4DshAddRouteToList (pFeasRouteList, pFeasRoute, 0);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AhIsRouteCanbeAdvt                                    */
/* Description   : This routine checks whether the route can be advertised to*/
/*                 the peer specified.                                       */
/* Input(s)      : Peer to which this route is supposed to be advertised     */
/*                 (pPeer), Route which is going to be advertised (pRoute)   */
/* Output(s)     : None                                                      */
/* Return(s)     : TRUE if the route can be advertised,                      */
/*                 FALSE if it cannot be advertised.                         */
/*****************************************************************************/
BOOL1
Bgp4AhIsRouteCanbeAdvt (tBgp4PeerEntry * pPeer, tRouteProfile * pRoute,
                        UINT1 u1IsFeasRoute)
{
    tAsPath            *pASSeg = NULL;
    tTMO_SLL           *pASList = NULL;
    tAddrPrefix         InvPrefix;
#ifdef L3VPN
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
#endif
    UINT4               u4AsafiMask;
    UINT4              *pu4ASNo = NULL;
    UINT4               u4PeerType = 0;
    UINT4               u4ExpPolicy = 0;
    UINT4               u4AdvtAsafiMask;
    INT1                i1AdvtStatus = 0;

    BGP4_DBG3 (BGP4_DBG_ALL,
               "\tBgp4AhIsRouteCanbeAdvt() : Checking if Route %s, Mask  %s "
               "can be advertised to Peer= %s\n",
               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRoute),
                                BGP4_RT_AFI_INFO (pRoute)),
               Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN (pRoute),
                                BGP4_RT_AFI_INFO (pRoute)),
               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                (BGP4_PEER_REMOTE_ADDR_INFO (pPeer))));
    Bgp4InitAddrPrefixStruct (&(InvPrefix), BGP4_AFI_IN_ADDR_PREFIX_INFO
                              (BGP4_PEER_REMOTE_ADDR_INFO (pPeer)));

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRoute),
                           BGP4_RT_SAFI_INFO (pRoute), u4AsafiMask);

#ifdef L3VPN
    if (BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_CE_PEER)
    {
        if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
        {
            Bgp4Vpnv4GetInstallVrfInfo (pRoute, BGP4_PEER_CXT_ID (pPeer),
                                        &pRtVrfInfo);
            /*The route is not seleted for the vrf to which
               this CE is associated */
            if (pRtVrfInfo == NULL)
            {
                return FALSE;
            }
            if (u1IsFeasRoute == BGP4_TRUE)
            {
                /*If the route is feasible and seleted as best route in BGP vrf,
                 * the route will be advertised to CE peer*/
                if ((BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo) &
                     BGP4_RT_VRF_WITHDRAWN) ||
                    (!(BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo) &
                       BGP4_RT_BGP_VRF_BEST)))
                {
                    return FALSE;
                }
            }
            else
            {
                if ((!(BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo) &
                       BGP4_RT_VRF_WITHDRAWN)) ||
                    (!(BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo) &
                       BGP4_RT_BGP_VRF_BEST)))
                {
                    return FALSE;
                }
            }
        }
        else
        {
            return FALSE;
        }
    }
    else if (BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_PE_PEER)
    {
        if (u4AsafiMask != CAP_MP_VPN4_UNICAST)
        {
            return FALSE;
        }
    }
#else
    UNUSED_PARAM (u1IsFeasRoute);
#endif
    /* 
     * Check if <AFI,SAFI> in the received route is negotiated 
     * with  the peer 
     */
    Bgp4GetPeerAdvtAfiSafi (pRoute, pPeer, &u4AdvtAsafiMask);
    switch (u4AdvtAsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) != 0) &&
                ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) & CAP_NEG_IPV4_UNI_MASK)
                 != CAP_NEG_IPV4_UNI_MASK))
            {
                return FALSE;
            }
            break;

#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) & CAP_NEG_L2VPN_VPLS_MASK)
                != CAP_NEG_L2VPN_VPLS_MASK)
            {
                return FALSE;
            }
            break;
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) & CAP_NEG_L2VPN_EVPN_MASK)
                != CAP_NEG_L2VPN_EVPN_MASK)
            {
                return FALSE;
            }
            break;
#endif
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
            break;

        case CAP_MP_VPN4_UNICAST:
            /* The route is going to be advertised to a PE peer */
            if (BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_PE_PEER)
            {
                if ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) & CAP_NEG_VPN4_UNI_MASK)
                    != CAP_NEG_VPN4_UNI_MASK)
                {
                    /* vpn4 capability is not negotiated with the peer */
                    return FALSE;
                }
                if (u1IsFeasRoute != BGP4_TRUE)
                {
                    break;
                }
                /* If the export route targets are configured for the installed
                 * vrf, (explicit NULL export target) then do not send this 
                 * route;
                 * the route is either received from a CE peer or is 
                 * originated at PE.
                 */
                if (BGP4_RR_CLIENT_CNT (BGP4_PEER_CXT_ID (pPeer)) > 0)
                {
                    break;
                }
                if (TMO_SLL_Count (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRoute)) == 0)
                {
                    return FALSE;
                }
                pRtVrfInfo =
                    (tRtInstallVrfInfo *)
                    TMO_SLL_First (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRoute));
                if (pRtVrfInfo == NULL)
                {
                    /* NOTE: This must not be NULL, if the route is received
                     * from CE peer, its ensured, but if the route is
                     * redistributed from VRF, then we should ensure that
                     * this contains non NULL information.
                     */
                    return FALSE;
                }
                if (BGP4_VPN4_VRF_SPEC_CURRENT_STATE
                    (BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrfInfo))
                    == BGP4_L3VPN_VRF_DELETE)
                {
                    /* VRF would have been deleted, so retrun true */
                    return BGP4_TRUE;
                }
                if (((!(BGP4_VPN4_VRF_SPEC_GET_FLAG
                        (BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrfInfo)) &
                        BGP4_VRF_EXP_TGT_CHG_INPROGRESS)) &&
                     (TMO_SLL_Count
                      (BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS
                       (BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrfInfo))) ==
                      0)))
                {
                    return FALSE;
                }
            }
            break;

#endif

#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) & CAP_NEG_IPV6_UNI_MASK)
                != CAP_NEG_IPV6_UNI_MASK)
            {
                return FALSE;
            }
            break;
#endif
        default:
            return FALSE;
    }

    /* Check for the Advertisement of Default route. If the default route
     * is originated via Peer Default Orignate command, then the procotol
     * id will be BGP4_OTHERS_ID and for this route, no need to check for
     * the default route advertisement policy. */
    if ((PrefixMatch
         (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
          (BGP4_RT_NET_ADDRESS_INFO (pRoute)), InvPrefix) == BGP4_TRUE)
        && (BGP4_RT_PROTOCOL (pRoute) != BGP4_OTHERS_ID))
    {
        /* Default route is to be advertised. */
        if (BGP4_PEER_DEF_ROUTE_ORIG_STATUS (pPeer) ==
            BGP4_DEF_ROUTE_ORIG_ENABLE)
        {
            /* Peer based advertisement for Default route is enabled. So
             * only the automatic default route shall be sent to peer.
             * No need to send this route. */
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tRoute %s Will Not be Advertised to peer %s. Default "
                           "route advertisement for peer is enabled.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRoute),
                                            BGP4_RT_AFI_INFO (pRoute)),
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeer))));
            return FALSE;
        }

        if (BGP4_DEF_ROUTE_ORIG_NEW_STATUS (BGP4_PEER_CXT_ID (pPeer)) ==
            BGP4_DEF_ROUTE_ORIG_DISABLE)
        {
            if (BGP4_DEF_ROUTE_ORIG_STATUS (BGP4_PEER_CXT_ID (pPeer)) ==
                BGP4_DEF_ROUTE_ORIG_DISABLE)
            {
                /* Default route advertisement is disabled. */
                return FALSE;
            }
            /* Change in default route advertisement policy. Advertisement
             * support for Default route is enabled earlier and now disabled.
             * Check whether the route is to advertised as feasible route
             * or not. If it is to be advertised as feasible then reject
             * it. Permit the advertisement of withdrawn route. */
#ifdef L3VPN
            /*Default route is not supported currently of Vpnv4 
             * and Labelled Ipv4 address family*/
            if ((u4AsafiMask == CAP_MP_LABELLED_IPV4) ||
                (u4AsafiMask == CAP_MP_VPN4_UNICAST))
            {
                return FALSE;
            }
#endif
            if (((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_WITHDRAWN) == 0) &&
                ((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_AGGR_WITHDRAWN) == 0))
            {
                return FALSE;
            }

        }
    }
    if (BGP4_RT_PROTOCOL (pRoute) == BGP_ID)
    {
        /* Route learned from the same peer should not be advertised
         *          * to the same peer.
         *                   */
        if (BGP4_RT_PEER_ENTRY (pRoute) == pPeer)
        {
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tRoute %s Will Not be Advertised to peer %s. Route "
                           "learnt from same peer.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRoute),
                                            BGP4_RT_AFI_INFO (pRoute)),
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeer))));
            return FALSE;
        }

    }

    if (BGP4_RT_PROTOCOL (pRoute) == BGP_ID)
    {
        if (BGP4_RT_PEER_ENTRY (pRoute) == pPeer)
        {
#ifdef L3VPN
            if ((u4AsafiMask != CAP_MP_VPN4_UNICAST) ||
                ((u4AsafiMask == CAP_MP_VPN4_UNICAST) &&
                 (BGP4_VPN4_PEER_ROLE (pPeer) != BGP4_VPN4_CE_PEER)))
#endif
            {
                if (BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_ADVT_WITHDRAWN)
                {
                    /* BGP4_RT_ADVT_WITHDRAWN flag is set. Then this route
                     * should be advertised to the same peer as Withdrawn
                     * route. So return TRUE. */
                    return TRUE;
                }
            }
#ifdef L3VPN
            else
            {
                if ((u4AsafiMask == CAP_MP_VPN4_UNICAST) &&
                    (BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo) &
                     BGP4_RT_VRF_ADVT_WITHDRAWN))
                {
                    return TRUE;
                }
            }
#endif
        }
    }

    u4PeerType = BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeer), pPeer);

#ifdef L3VPN
    if ((u4AsafiMask == CAP_MP_VPN4_UNICAST) &&
        (BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_CE_PEER) &&
        (BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo) &
         BGP4_RT_VRF_ADVT_NOTTO_EXTERNAL))
    {
        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                  BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tRoute Not Advertised. NOTTO_EXTERNAL flag is set.\n");
        return FALSE;
    }
    else
#endif
    {
        if ((u4PeerType == BGP4_EXTERNAL_PEER) &&
            (BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_ADVT_NOTTO_EXTERNAL))
        {
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tRoute %s Will Not be Advertised to peer %s. Since external "
                           "peer advertisement is disabled for the route.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRoute),
                                            BGP4_RT_AFI_INFO (pRoute)),
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeer))));
            return FALSE;
        }
    }

    if ((u4PeerType == BGP4_INTERNAL_PEER) &&
        ((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_ADVT_NOTTO_INTERNAL) ==
         BGP4_RT_ADVT_NOTTO_INTERNAL))
    {
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tRoute %s Will Not be Advertised to peer %s. Since internal "
                       "peer advertisement is disabled for the route.\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRoute),
                                        BGP4_RT_AFI_INFO (pRoute)),
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO (pPeer))));
        return FALSE;
    }

#ifdef VPLSADS_WANTED
    if (u4AsafiMask == CAP_MP_L2VPN_VPLS)
    {
        if (u4PeerType == BGP4_EXTERNAL_PEER)
        {
            BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                      BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                      BGP4_MOD_NAME,
                      "\tVPLS Route Not Advertised. VPLS route should not to be. "
                      "advertised to External Peer.\n");
            return FALSE;
        }
        if (u4PeerType == BGP4_INTERNAL_PEER)
        {
            if (BGP4_RT_PROTOCOL (pRoute) == BGP_ID)
            {
                if (BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) > 0)
                {
                    BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                              BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC,
                              BGP4_MOD_NAME,
                              "\tVPLS Route is Advertised. BGP learned VPLS route should be "
                              "advertised to Internal Peer in case of \n");
                }
                else
                {
                    BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                              BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC,
                              BGP4_MOD_NAME,
                              "\tVPLS Route Not Advertised. BGP learned VPLS route should not to be "
                              "advertised to Internal Peer.\n");
                    return FALSE;
                }

            }
        }
    }

#endif

    /*  Imported or Non-BGP learned routes by default will be advertised
     *  based on the configured Import_Aggr flag.
     */
    if ((BGP4_RT_PROTOCOL (pRoute) != BGP_ID) &&
        (((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_AGGR_ROUTE) !=
          BGP4_RT_AGGR_ROUTE)) && (BGP4_RT_PROTOCOL (pRoute) != BGP4_OTHERS_ID))
    {
        if (BGP4_NON_BGP_EVENT_RCVD (BGP4_PEER_CXT_ID (pPeer)) != 0)
        {
            /* Non BGP Route Advt policy change in progress. Check whether
             * the received route is < the stored Init Info. If yes, then
             * basically this route has already been advt as per the new
             * policy. So use the new policy to advt the route. Else advt
             * the route as per the old policy and the non-BGP Advt handler
             * will take of the advt as per the new policy. */
            if (AddrGreaterThan
                (BGP4_NON_BGP_INIT_PREFIX_INFO (BGP4_PEER_CXT_ID (pPeer)),
                 pRoute->NetAddress.NetAddr,
                 BGP4_NON_BGP_INIT_PREFIXLEN (BGP4_PEER_CXT_ID (pPeer))) ==
                BGP4_TRUE)
            {
                u4ExpPolicy =
                    BGP4_NON_BGP_EVENT_RCVD (BGP4_PEER_CXT_ID (pPeer));
            }
            else
            {
                /* If the route is withdrawn route, then it should have been
                 * advertised as per the old policy. So in this case, use the
                 * old export policy. Else if the route is feasible, then use
                 * the new export policy. */
#ifdef L3VPN
                /*To be discussed and mofified */
#endif
                if (((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_AGGR_WITHDRAWN) ==
                     BGP4_RT_AGGR_WITHDRAWN) ||
                    ((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_WITHDRAWN) ==
                     BGP4_RT_WITHDRAWN))
                {
                    u4ExpPolicy =
                        BGP4_NON_BGP_RT_EXPORT_POLICY (BGP4_PEER_CXT_ID
                                                       (pPeer));
                }
                else
                {
                    u4ExpPolicy =
                        BGP4_NON_BGP_EVENT_RCVD (BGP4_PEER_CXT_ID (pPeer));
                }
            }
        }
        else
        {
            u4ExpPolicy =
                BGP4_NON_BGP_RT_EXPORT_POLICY (BGP4_PEER_CXT_ID (pPeer));
        }
        if (u4ExpPolicy == BGP4_EXTERNAL_PEER)
        {
            if (u4PeerType == BGP4_INTERNAL_PEER)
            {
                BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                               BGP4_TRC_FLAG,
                               BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                               BGP4_MOD_NAME,
                               "\tRoute %s Will Not be Advertised to peer %s. Non BGP route "
                               "will not be advertised to Internal peer."
                               " Since export policy is external\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRoute),
                                                BGP4_RT_AFI_INFO (pRoute)),
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeer))));
                return FALSE;
            }
        }

        if ((u4PeerType == BGP4_INTERNAL_PEER) &&
            ((pRoute->u4ExtFlags & BGP4_RT_AS_PATHSET_TAG) !=
             BGP4_RT_AS_PATHSET_TAG))
        {
            /* While advertising to internal peers, check whether the
             * AS no in the AS path holds any value other than LOCAL AS.
             * If so this route should have been received from external AS
             * and imported into this AS. So dont advertise such routes. */
            pASList = &(((tBgp4Info *) (pRoute->pRtInfo))->TSASPath);
            if (pASList != NULL)
            {
                TMO_SLL_Scan (pASList, pASSeg, tAsPath *)
                {
                    pu4ASNo = (UINT4 *) (VOID *) pASSeg->au1ASSegs;
                    if (OSIX_NTOHL (*(pu4ASNo)) !=
                        BGP4_LOCAL_AS_NO (BGP4_PEER_CXT_ID (pPeer)))
                    {
                        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                       BGP4_TRC_FLAG,
                                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                                       BGP4_MOD_NAME,
                                       "\tRoute %s Will Not be Advertised to peer %s. Non BGP route "
                                       "belongs to external AS but peer is internal peer\n.",
                                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                        (pRoute),
                                                        BGP4_RT_AFI_INFO
                                                        (pRoute)),
                                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                        (pPeer),
                                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                                         (pPeer))));
                        return (FALSE);
                    }
                }
            }
        }
        /* Check whether the Community attributes affects the advertisement
         * of this route to the peer. */
        if ((GetAdvtStatus (pRoute, pPeer, &i1AdvtStatus) == BGP4_FAILURE) ||
            (i1AdvtStatus == BGP4_FALSE))
        {
            /* Rotue advertised suppressed by Community Attribute */
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tRoute %s Will Not be Advertised to peer %s. It is suppressed "
                           "by community or extended community attribute\n.",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRoute),
                                            BGP4_RT_AFI_INFO (pRoute)),
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeer))));
            return FALSE;
        }
        return TRUE;
    }

    if (BGP4_RT_PROTOCOL (pRoute) == BGP_ID)
    {
        /* Route learned from the same peer should not be advertised
         * to the same peer.
         */
        if (BGP4_RT_PEER_ENTRY (pRoute) == pPeer)
        {
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tRoute %s Will Not be Advertised to peer %s. Route "
                           "learnt from same peer.\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRoute),
                                            BGP4_RT_AFI_INFO (pRoute)),
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeer))));
            return FALSE;
        }

        if ((BGP4_PEER_ASNO (BGP4_RT_PEER_ENTRY (pRoute)) ==
             BGP4_LOCAL_AS_NO (BGP4_PEER_CXT_ID (pPeer)))
            && (BGP4_PEER_ASNO (pPeer) ==
                BGP4_LOCAL_AS_NO (BGP4_PEER_CXT_ID (pPeer))))
        {
            /* Route is to reflected from internal peer to another internal
             * peer. Check whether the route can be reflected to the peer.*/
            i1AdvtStatus = (INT1) RflCheckForAdvertisement (pPeer, pRoute);
            if (i1AdvtStatus == FALSE)
            {
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                          BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tRoute Not reflected from INTERNAL <-> INTERNAL "
                          "peer.\n");
                return FALSE;
            }
        }

#ifdef L3VPN
        if ((u4AsafiMask == CAP_MP_VPN4_UNICAST) &&
            (BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_CE_PEER))
        {
            if (Bgp4Vpn4CheckAdvtToCEPeer (pRoute, pPeer) == BGP4_FALSE)
            {
                return FALSE;
            }
        }
#endif
        /* Check whether the Community attributes affects the advertisement
         * of this route to the peer. */
        if ((GetAdvtStatus (pRoute, pPeer, &i1AdvtStatus) == BGP4_FAILURE) ||
            (i1AdvtStatus == BGP4_FALSE))
        {
            /* Rotue advertised suppressed by Community Attribute */
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tRoute %s Will Not be Advertised to peer %s. It is suppressed "
                           "by community or extended community attribute\n.",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRoute),
                                            BGP4_RT_AFI_INFO (pRoute)),
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeer))));
            return FALSE;
        }
    }
    else
    {
        /* Check community for redist routes and locally generated routes */
        if ((GetAdvtStatus (pRoute, pPeer, &i1AdvtStatus) == BGP4_FAILURE) ||
            (i1AdvtStatus == BGP4_FALSE))
        {
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tRoute %s Will Not be Advertised to peer %s. It is suppressed "
                           "by community or extended community attribute\n.",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRoute),
                                            BGP4_RT_AFI_INFO (pRoute)),
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeer))));
            return FALSE;
        }

    }
    /* If this is the self_aggregated route, but Aggregation is
     * not done yet, then this is just a dummy route created by
     * us. Hence, don't advertise this route.
     */
    if (((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_AGGR_ROUTE) ==
         BGP4_RT_AGGR_ROUTE)
        && (((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_AGGREGATED) !=
             BGP4_RT_AGGREGATED)))
    {
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tRoute %s Will Not be Advertised to peer %s. It is a dummy "
                       "aggregate route created\n.",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRoute),
                                        BGP4_RT_AFI_INFO (pRoute)),
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeer),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO (pPeer))));
        return FALSE;
    }
    return TRUE;
}

/****************************************************************************/
/* FUNCTION NAME : Bgp4AhCanRouteBeAdvtWithdrawn                            */
/* DESCRIPTION   : This function checks whether the new route has to be     */
/*               : advertised as withdrawn to the peer from which this      */
/*               : route is received or not. This check needs to be         */
/*               : performed only when the new route is received from a peer*/
/* INPUTS        : pointer to the new route (pNewRoute)                     */
/*               : pointer to the old route (pOldRoute)                     */
/* OUTPUTS       : None                                                     */
/* RETURNS       : BGP4_TRUE - if the route needs to be withdrawn           */
/*               : BGP4_FALSE - if the route need not be withdrawn          */
/****************************************************************************/
INT4
Bgp4AhCanRouteBeAdvtWithdrawn (tRouteProfile * pNewRoute,
                               tRouteProfile * pOldRoute)
{
    tBgp4PeerEntry     *pNewRtPeer = NULL;
    tBgp4PeerEntry     *pOldRtPeer = NULL;
    UINT4               u4IsOldRtAdvt = BGP4_TRUE;
    UINT4               u4HashKey = 0;
    UINT4               u4RtAsafiIndex = 0;
    UINT4               u4PeerInitAsafiIndex = 0;

    pNewRtPeer = BGP4_RT_PEER_ENTRY (pNewRoute);
    /* pNewRoute is received from Peer. */
    if (BGP4_RT_PROTOCOL (pOldRoute) != BGP_ID)
    {
        pOldRtPeer = NULL;
    }
    else
    {
        pOldRtPeer = BGP4_RT_PEER_ENTRY (pOldRoute);
    }

    /* If the peer is going through init and if the received route's PA Hash
     * Key is greater than that stored in the PeerInit structure, then the
     * route has not been advertised earlier and there is no need to advertise
     * that route. Else advertise the route.
     */
    if (BGP4_GET_PEER_CURRENT_STATE (pNewRtPeer) == BGP4_PEER_INIT_INPROGRESS)
    {
        Bgp4GetAfiSafiIndex (BGP4_RT_AFI_INFO (pNewRoute),
                             BGP4_RT_SAFI_INFO (pNewRoute), &u4RtAsafiIndex);
        Bgp4GetAfiSafiIndex (BGP4_PEER_INIT_AFI (pNewRtPeer),
                             BGP4_PEER_INIT_SAFI (pNewRtPeer),
                             &u4PeerInitAsafiIndex);
#ifdef L3VPN
        /* Carrying Label Information - RFC 3107 */
        if (u4RtAsafiIndex == BGP4_IPV4_LBLD_INDEX)
        {
            u4RtAsafiIndex = BGP4_IPV4_UNI_INDEX;
        }
#endif

        Bgp4PAFormHashKey (BGP4_RT_BGP_INFO (pNewRoute), &u4HashKey,
                           BGP4_MAX_RCVD_PA_HASH_INDEX);
        if ((u4RtAsafiIndex > u4PeerInitAsafiIndex) ||
            ((u4RtAsafiIndex == u4PeerInitAsafiIndex) &&
             (u4HashKey > BGP4_PEER_INIT_PA_HASHKEY (pNewRtPeer))))
        {
            return BGP4_FALSE;
        }
    }

    if (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pNewRtPeer), pNewRtPeer) ==
        BGP4_INTERNAL_PEER)
    {
        /* New peer is an internal peer. Check whether the old route is
         * present in the internal peer advt list or not. If present, then
         * the old route has not been advertised to the peer earlier. In
         * that case no need to advt the new route as withdrawn to the peer.
         */
        if ((BGP4_RT_GET_FLAGS (pOldRoute) & BGP4_RT_IN_INT_PEER_ADVT_LIST)
            == BGP4_RT_IN_INT_PEER_ADVT_LIST)
        {
            u4IsOldRtAdvt = BGP4_FALSE;
        }
    }
    else
    {
        /* New peer is an external peer. Check whether the old route is
         * present in the external peer advt list or FIB List. If present, then
         * the old route has not been advertised to the peer earlier. In
         * that case no need to advt the new route as withdrawn to the peer.
         */
        if (((BGP4_RT_GET_FLAGS (pOldRoute) & BGP4_RT_IN_FIB_UPD_LIST)
             == BGP4_RT_IN_FIB_UPD_LIST) ||
            ((BGP4_RT_GET_FLAGS (pOldRoute) & BGP4_RT_IN_EXT_PEER_ADVT_LIST)
             == BGP4_RT_IN_EXT_PEER_ADVT_LIST))
        {
            u4IsOldRtAdvt = BGP4_FALSE;
        }
    }

    if (u4IsOldRtAdvt == BGP4_FALSE)
    {
        /* Old route has not been advertised. Check whether the old route
         * is also received from the same peer and if the
         * BGP4_RT_ADVT_WITHDRAWN flag is set of not. If yes, then
         * need to set this flag. Else no need to set the flag. */
        if ((pOldRtPeer != NULL) && (pOldRtPeer == pNewRtPeer) &&
            ((BGP4_RT_GET_FLAGS (pOldRoute) & BGP4_RT_ADVT_WITHDRAWN) ==
             BGP4_RT_ADVT_WITHDRAWN))
        {
            return BGP4_TRUE;
        }
        /* No need to advt this route as withdrawn. */
        return BGP4_FALSE;
    }

    /* The old route has already been advertised. */
    if (BGP4_RT_PROTOCOL (pOldRoute) != BGP_ID)
    {
        /* Should advertise the new route as withdrawn */
        return BGP4_TRUE;
    }

    if ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pOldRtPeer), pOldRtPeer) ==
         BGP4_INTERNAL_PEER)
        && (BGP4_PEER_RFL_CLIENT (pOldRtPeer) == NON_CLIENT)
        && (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pNewRtPeer), pNewRtPeer) ==
            BGP4_INTERNAL_PEER)
        && (BGP4_PEER_RFL_CLIENT (pNewRtPeer) == NON_CLIENT))
    {
        /* Old route since learnt from an internal peer would
         * not have been advertised to internal peers. So no
         * need to send these route as withdrawn. */
        return BGP4_FALSE;
    }

    /* For all other cases, need to advt as withdrawn. */
    return BGP4_TRUE;

}

/*****************************************************************************/
/* Function Name : Bgp4AhProcessMinASTimerExpiryEvent                        */
/* Description   : This is the Timer expiry routine for handling the MinAS-  */
/*                 Origination timer expiry event. This function is called   */
/*                 when this Timer is expired and there are more routes left */
/*                 in Peer's NEW_LOCAL_ROUTE List.                           */
/* Input(s)      : Peer whose MINAS-ORIGINATION timer has expired.           */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4AhProcessMinASTimerExpiryEvent (tBgp4PeerEntry * pPeerEntry)
{
    tTMO_HASH_TABLE    *pHashTab = NULL;
    tTMO_SLL            EmptyWithdrawnList;
    tTMO_SLL            NewFeasList;
    tTMO_SLL           *pScanList = NULL;
    tLinkNode          *pLinkNode = NULL;
    tLinkNode          *pTempLinkNode = NULL;
    tRouteProfile      *pRtProfile = NULL;
    UINT4               u4RtCnt = 0;
    UINT4               u4IsMaxRtPrc = BGP4_FALSE;
    UINT4               u4Index = 0;
    UINT4               u4HashKey = 0;

    for (u4Index = 0; u4Index < (BGP4_MPE_ADDR_FAMILY_MAX_INDEX); u4Index++)
    {
        /* Initialize the lists for each <AFI, SAFI> */
        TMO_SLL_Init (&EmptyWithdrawnList);
        TMO_SLL_Init (&NewFeasList);

        if (u4Index == BGP4_IPV4_UNI_INDEX)
        {
            pHashTab = BGP4_PEER_IPV4_NEW_LOCAL_ROUTES (pPeerEntry);
        }
#ifdef VPLSADS_WANTED
        /*ADS-VPLS related processing */
        else if (u4Index == BGP4_L2VPN_VPLS_INDEX)
        {
            if (BGP4_PEER_VPLS_AFISAFI_INSTANCE (pPeerEntry) != NULL)
            {
                pHashTab = BGP4_PEER_VPLS_NEW_LOCAL_ROUTES (pPeerEntry);
            }
            else
            {
                pHashTab = NULL;
            }
        }
#endif

#ifdef L3VPN
        else if (u4Index == BGP4_IPV4_LBLD_INDEX)
        {
            if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeerEntry) != NULL)
            {
                pHashTab = BGP4_PEER_IPV4_LBLD_NEW_LOCAL_ROUTES (pPeerEntry);
            }
            else
            {
                pHashTab = NULL;
            }
        }
        else if (u4Index == BGP4_VPN4_UNI_INDEX)
        {
            if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeerEntry) != NULL)
            {
                pHashTab = BGP4_PEER_VPN4_NEW_LOCAL_ROUTES (pPeerEntry);
            }
            else
            {
                pHashTab = NULL;
            }
        }
#endif
#ifdef BGP4_IPV6_WANTED
        else if (u4Index == BGP4_IPV6_UNI_INDEX)
        {
            if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeerEntry) != NULL)
            {
                pHashTab = BGP4_PEER_IPV6_NEW_LOCAL_ROUTES (pPeerEntry);
            }
            else
            {
                pHashTab = NULL;
            }
        }
#endif
#ifdef EVPN_WANTED
        /* EVPN related processing */
        else if (u4Index == BGP4_L2VPN_EVPN_INDEX)
        {
            if (BGP4_PEER_EVPN_AFISAFI_INSTANCE (pPeerEntry) != NULL)
            {
                pHashTab = BGP4_PEER_EVPN_NEW_LOCAL_ROUTES (pPeerEntry);
            }
            else
            {
                pHashTab = NULL;
            }
        }
#endif
        else
        {
            Bgp4DshReleaseList (&EmptyWithdrawnList, 0);
            Bgp4DshReleaseList (&NewFeasList, 0);
            return (BGP4_SUCCESS);
        }

        if (pHashTab != NULL)
        {
            TMO_HASH_Scan_Table (pHashTab, u4HashKey)
            {
                pScanList = &pHashTab->HashList[u4HashKey];
                BGP_SLL_DYN_Scan (pScanList, pLinkNode, pTempLinkNode,
                                  tLinkNode *)
                {
                    pRtProfile = pLinkNode->pRouteProfile;
                    pRtProfile->pOutRtInfo = pLinkNode->pRtInfo;
                    pLinkNode->pRtInfo = NULL;
                    /* Add the route to the FeasibleList and advertise. */
                    Bgp4DshAddRouteToList (&NewFeasList, pRtProfile, BGP4_TRUE);
                    Bgp4DshRemoveNodeFromList (pScanList, pLinkNode);

                    u4RtCnt++;
                    if (u4RtCnt >= BGP4_MAX_PEERRTS2PROCESS)
                    {
                        /* Has processed expected number of routes. */
                        u4IsMaxRtPrc = BGP4_TRUE;
                        break;
                    }
                }
                if (u4IsMaxRtPrc == BGP4_TRUE)
                {
                    break;
                }
            }
            if (TMO_SLL_Count (&NewFeasList) > 0)
            {
                Bgp4AhSendUpdatesToPeer (pPeerEntry, &NewFeasList,
                                         &EmptyWithdrawnList);
                /*  The flags NOTTO_EXTERNAL, NOTTO_INTERNAL, REPLACEMENT needs
                 *  to be reset now. */
                BGP_SLL_DYN_Scan (&NewFeasList, pLinkNode, pTempLinkNode,
                                  tLinkNode *)
                {
                    pRtProfile = pLinkNode->pRouteProfile;
                    BGP4_RT_RESET_FLAG (pRtProfile,
                                        (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                         BGP4_RT_ADVT_NOTTO_EXTERNAL |
                                         BGP4_RT_REPLACEMENT));
                    Bgp4DshRemoveNodeFromList (&NewFeasList, pLinkNode);
                }
            }
            Bgp4DshReleaseList (&EmptyWithdrawnList, 0);
        }
        if (u4IsMaxRtPrc == BGP4_TRUE)
        {
            break;
        }
    }

    /* Reaches here if all routes have been processed or MAX routes processed.
     * In case if all routes have been processed restart the MINAS Timer. */
    if ((u4IsMaxRtPrc == BGP4_FALSE) && (u4RtCnt > 0))
    {
        /* Has completed processing the NEW_LOCAL_ROUTES. Now restart the
         * MINAS-ORIGINATION Timer. */
        Bgp4TmrhStartTimer (BGP4_MINASORIG_TIMER, (VOID *) pPeerEntry,
                            BGP4_PEER_MIN_AS_ORIG_TIME (pPeerEntry));
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AhProcessTimer                                        */
/* Description   : This is the Timer expiry routine. When a particular timer */
/*                 expires depending on the Timer ID, the list of routes     */
/*                 which needs to be advertised to any particular peer (which*/
/*                 been identified using the reference parameter) is         */
/*                 advertised, to the peer specified.                        */
/* Input(s)      : Timer ID (u4TimerId),                                     */
/*                 Reference to the BGP Peer entry (u4Tmr)                   */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4AhProcessTimer (UINT4 u4TimerId, FS_ULONG u4Tmr)
{
    tTMO_HASH_TABLE    *pHashTab = NULL;
    tTMO_SLL            EmptyWithdrawnList;
    tTMO_SLL            NewFeasList;
    tTMO_SLL           *pScanList = NULL;
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tBgp4PeerEntry     *pPeer = NULL;
    tLinkNode          *pLinkNode = NULL;
    tLinkNode          *pScankNode = NULL;
    tAdvRtLinkNode     *pAdvRtLinkNode = NULL;
    tAdvRtLinkNode     *pTempLinkNode = NULL;
    tRouteProfile      *pRtProfile = NULL;
    UINT4               u4Flag = BGP4_FALSE;
    UINT4               u4TimeStamp = 0;
    UINT4               u4TimePeriod = 0;
    UINT4               u4Index = 0;
    UINT4               u4TimerValue = 0;
    UINT4               u4HashKey = 0;
    UINT4               u4Context;

    pPeerEntry = ((tBgp4PeerEntry *) u4Tmr);

    u4Context = BGP4_PEER_CXT_ID (pPeerEntry);

    BgpSetContextId ((INT4) u4Context);

    /* Process event for a valid peerentry */
    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (BGP4_PEER_CXT_ID (pPeerEntry)), pPeer,
                  tBgp4PeerEntry *)
    {
        if (pPeer == pPeerEntry)
        {
            break;
        }
    }

    if (pPeer == NULL)
    {
        return BGP4_SUCCESS;
    }

    /*  NEW routes and the LOCAL NEW routes needs to be 
     *  advertised, depending on the Timer ID.
     */
    if (u4TimerId == BGP4_MINASORIG_TIMER)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : MinAS Origination Advertisement Timer "
                       "Expired.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerEntry))));
        pPeerEntry->peerLocal.tOrigTmr.u4Flag = BGP4_INACTIVE;
        Bgp4AhProcessMinASTimerExpiryEvent (pPeerEntry);
        return BGP4_SUCCESS;
    }
    else
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                       BGP4_TRC_FLAG, BGP4_KEEP_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : MinRoute Advertisement Timer Expired.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerEntry))));
        pPeerEntry->peerLocal.tRouteAdvTmr.u4Flag = BGP4_INACTIVE;
        u4TimerValue = BGP4_PEER_MIN_ADV_TIME (pPeerEntry);
    }

    for (u4Index = 0; u4Index < (BGP4_MPE_ADDR_FAMILY_MAX_INDEX); u4Index++)
    {
        /* Initialize the lists for each <AFI, SAFI> */
        TMO_SLL_Init (&EmptyWithdrawnList);
        TMO_SLL_Init (&NewFeasList);
        if (u4Index == BGP4_IPV4_UNI_INDEX)
        {
            /* this includes the routes that are received from CE peers
             * also, if any
             */
            pHashTab = BGP4_PEER_IPV4_NEW_ROUTES (pPeerEntry);
        }
#ifdef VPLSADS_WANTED
        else if (u4Index == BGP4_L2VPN_VPLS_INDEX)
        {
            if (BGP4_PEER_VPLS_AFISAFI_INSTANCE (pPeerEntry) != NULL)
            {
                pHashTab = BGP4_PEER_VPLS_NEW_ROUTES (pPeerEntry);
            }
            else
            {
                pHashTab = NULL;
            }
        }
#endif

#ifdef L3VPN
        else if (u4Index == BGP4_IPV4_LBLD_INDEX)
        {
            if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeerEntry) != NULL)
            {
                pHashTab = BGP4_PEER_IPV4_LBLD_NEW_ROUTES (pPeerEntry);
            }
            else
            {
                pHashTab = NULL;
            }
        }
        else if (u4Index == BGP4_VPN4_UNI_INDEX)
        {
            if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeerEntry) != NULL)
            {
                pHashTab = BGP4_PEER_VPN4_NEW_ROUTES (pPeerEntry);
            }
            else
            {
                pHashTab = NULL;
            }
        }
#endif
#ifdef BGP4_IPV6_WANTED
        else if (u4Index == BGP4_IPV6_UNI_INDEX)
        {
            if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeerEntry) != NULL)
            {
                pHashTab = BGP4_PEER_IPV6_NEW_ROUTES (pPeerEntry);
            }
            else
            {
                pHashTab = NULL;
            }
        }
#endif
#ifdef EVPN_WANTED
        else if (u4Index == BGP4_L2VPN_EVPN_INDEX)
        {
            if (BGP4_PEER_EVPN_AFISAFI_INSTANCE (pPeerEntry) != NULL)
            {
                pHashTab = BGP4_PEER_EVPN_NEW_ROUTES (pPeerEntry);
            }
            else
            {
                pHashTab = NULL;
            }
        }
#endif
        else
        {
            Bgp4DshReleaseList (&EmptyWithdrawnList, 0);
            Bgp4DshReleaseList (&NewFeasList, 0);
            return (BGP4_SUCCESS);
        }

        if (pHashTab != NULL)
        {
            u4TimeStamp = Bgp4ElapTime ();
            TMO_HASH_Scan_Table (pHashTab, u4HashKey)
            {
                pScanList = &pHashTab->HashList[u4HashKey];
                BGP_SLL_DYN_Scan (pScanList, pAdvRtLinkNode, pTempLinkNode,
                                  tAdvRtLinkNode *)
                {
                    pRtProfile = pAdvRtLinkNode->pRouteProfile;
                    /* Check whether the route can be advertised now or
                     * not. The timer value might have Wrapped round. Need
                     * to take care of this situation also. */
                    if (Bgp4DiffTime (u4TimeStamp, u4TimerValue) >=
                        Bgp4DiffTime (BGP4_RT_UPDATE_SENT_TIME (pAdvRtLinkNode),
                                      u4TimerValue))
                    {
                        /* Current Time is >= the stored Time Stamp.
                         * Means the route can now be advertised. */
                        Bgp4DshAddRouteToList (&NewFeasList, pRtProfile,
                                               BGP4_GROUP);
                        Bgp4DshRelRtFromPeerRtAdvList (pScanList,
                                                       pAdvRtLinkNode);
                    }
                    else
                    {
                        /* Route's Time Stamp is greater than the current
                         * time. Means the route should not be advertised
                         * now. */
                        if (u4Flag == BGP4_FALSE)
                        {
                            /* Calculate the Time interval for starting the
                             * timer */
                            u4TimePeriod =
                                Bgp4DiffTime (BGP4_RT_UPDATE_SENT_TIME
                                              (pAdvRtLinkNode), u4TimeStamp);
                            u4Flag = BGP4_TRUE;
                        }
                        else
                        {
                            /* Update the time interval. */
                            if (u4TimePeriod >
                                Bgp4DiffTime
                                (BGP4_RT_UPDATE_SENT_TIME (pAdvRtLinkNode),
                                 u4TimeStamp))
                            {
                                u4TimePeriod =
                                    Bgp4DiffTime (BGP4_RT_UPDATE_SENT_TIME
                                                  (pAdvRtLinkNode),
                                                  u4TimeStamp);
                            }
                        }
                        /* Since while adding to the Hash-Table list, the route
                         * are added according to the time-stamp value, all
                         * other routes following this route should be
                         * advertise only after a while. So break now and
                         * processing the remaining bucket. */
                        break;
                    }
                }
            }
        }
        Bgp4AhSendUpdatesToPeer (pPeerEntry, &NewFeasList, &EmptyWithdrawnList);

        if ((u4Flag == BGP4_TRUE) && (u4TimePeriod > 0))
        {
            Bgp4TmrhStartTimer (BGP4_MINROUTEADV_TIMER,
                                (VOID *) pPeerEntry, u4TimePeriod);
        }

        /*  The flags NOTTO_EXTERNAL, NOTTO_INTERNAL, REPLACEMENT needs to
         *  be reset now. Also clear the New Feasible list.  */
        BGP_SLL_DYN_Scan (&NewFeasList, pLinkNode, pScankNode, tLinkNode *)
        {
            pRtProfile = pLinkNode->pRouteProfile;
            BGP4_RT_RESET_FLAG (pRtProfile, (BGP4_RT_ADVT_NOTTO_INTERNAL |
                                             BGP4_RT_ADVT_NOTTO_EXTERNAL |
                                             BGP4_RT_REPLACEMENT));
            Bgp4DshRemoveNodeFromList (&NewFeasList, pLinkNode);
        }
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AhProcessAdvtList                                     */
/* Description   : This routine process the Peer's advertised route list and */
/*                 check whether any route is present in the list for time   */
/*                 longer than the MinRouteAdvtTimeout/MinAsOrigTimeout      */
/*                 value. If present then those routes are removed from the  */
/*                 peer's advt. list. This routine needs to be called        */
/*                 periodically to refresh the peer's advt. route list       */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4AhProcessAdvtList ()
{
    tTMO_HASH_TABLE    *pFeasHashTab = NULL;
    tTMO_HASH_TABLE    *pWithHashTab = NULL;
    tBgp4PeerEntry     *pPeer = NULL;
    tAdvRtLinkNode     *pLinkNode = NULL;
    tAdvRtLinkNode     *pTempLinkNode = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tTMO_SLL           *pAdvtWithList = NULL;
    tTMO_SLL           *pAdvtFeasList = NULL;
    BOOL1               b1IsRtLocal;
    UINT4               u4TimePeriod;
    UINT4               u4Index = 0;
    UINT4               u4HashKey = 0;
    UINT4               u4Context = 0;
    INT4                i4RetStatus = SNMP_FAILURE;

    i4RetStatus = BgpGetFirstActiveContextID (&u4Context);
    while (i4RetStatus == SNMP_SUCCESS)
    {
        TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
        {
            for (u4Index = 0; u4Index < (BGP4_MPE_ADDR_FAMILY_MAX_INDEX);
                 u4Index++)
            {
                switch (u4Index)
                {
                    case BGP4_IPV4_UNI_INDEX:
                        pWithHashTab = BGP4_PEER_IPV4_ADVT_WITH_ROUTES (pPeer);
                        pFeasHashTab = BGP4_PEER_IPV4_ADVT_FEAS_ROUTES (pPeer);
                        break;
#ifdef VPLSADS_WANTED
                        /*ADS-VPLS related processing */
                    case BGP4_L2VPN_VPLS_INDEX:
                        if (BGP4_PEER_L2VPN_VPLS_AFISAFI_INSTANCE (pPeer) !=
                            NULL)
                        {
                            pWithHashTab =
                                BGP4_PEER_VPLS_ADVT_WITH_ROUTES (pPeer);
                            pFeasHashTab =
                                BGP4_PEER_VPLS_ADVT_FEAS_ROUTES (pPeer);
                        }
                        else
                        {
                            pWithHashTab = NULL;
                            pFeasHashTab = NULL;
                        }
                        break;
#endif
#ifdef EVPN_WANTED
                    case BGP4_L2VPN_EVPN_INDEX:
                        if (BGP4_PEER_EVPN_AFISAFI_INSTANCE (pPeer) != NULL)
                        {
                            pWithHashTab =
                                BGP4_PEER_EVPN_ADVT_WITH_ROUTES (pPeer);
                            pFeasHashTab =
                                BGP4_PEER_EVPN_ADVT_FEAS_ROUTES (pPeer);
                        }
                        else
                        {
                            pWithHashTab = NULL;
                            pFeasHashTab = NULL;
                        }
                        break;

#endif
#ifdef L3VPN
                    case BGP4_IPV4_LBLD_INDEX:
                        if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) !=
                            NULL)
                        {
                            pWithHashTab =
                                BGP4_PEER_IPV4_LBLD_ADVT_WITH_ROUTES (pPeer);
                            pFeasHashTab =
                                BGP4_PEER_IPV4_LBLD_ADVT_FEAS_ROUTES (pPeer);
                        }
                        else
                        {
                            pWithHashTab = NULL;
                            pFeasHashTab = NULL;
                        }
                        break;
                    case BGP4_VPN4_UNI_INDEX:
                        if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
                        {
                            pWithHashTab =
                                BGP4_PEER_VPN4_ADVT_WITH_ROUTES (pPeer);
                            pFeasHashTab =
                                BGP4_PEER_VPN4_ADVT_FEAS_ROUTES (pPeer);
                        }
                        else
                        {
                            pWithHashTab = NULL;
                            pFeasHashTab = NULL;
                        }
                        break;
#endif
#ifdef BGP4_IPV6_WANTED
                    case BGP4_IPV6_UNI_INDEX:
                        if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
                        {
                            pWithHashTab =
                                BGP4_PEER_IPV6_ADVT_WITH_ROUTES (pPeer);
                            pFeasHashTab =
                                BGP4_PEER_IPV6_ADVT_FEAS_ROUTES (pPeer);
                        }
                        else
                        {
                            pWithHashTab = NULL;
                            pFeasHashTab = NULL;
                        }
                        break;
#endif
                    default:
                        return (BGP4_SUCCESS);
                }
                if (pWithHashTab != NULL)
                {
                    TMO_HASH_Scan_Table (pWithHashTab, u4HashKey)
                    {
                        pAdvtWithList = &pWithHashTab->HashList[u4HashKey];
                        BGP_SLL_DYN_Scan (pAdvtWithList, pLinkNode,
                                          pTempLinkNode, tAdvRtLinkNode *)
                        {
                            pRtProfile = pLinkNode->pRouteProfile;
                            b1IsRtLocal = Bgp4IsLocalroute (pRtProfile);
                            if (b1IsRtLocal == TRUE)
                            {
                                u4TimePeriod =
                                    BGP4_PEER_MIN_AS_ORIG_TIME (pPeer);
                            }
                            else
                            {
                                u4TimePeriod = BGP4_PEER_MIN_ADV_TIME (pPeer);
                            }
                            if (u4TimePeriod <=
                                Bgp4DiffTime (Bgp4ElapTime (),
                                              BGP4_RT_UPDATE_SENT_TIME
                                              (pLinkNode)))
                            {
                                Bgp4DshRelRtFromPeerRtAdvList (pAdvtWithList,
                                                               pLinkNode);
                            }
                            else
                            {
                                /* Diff time is less than configured Timer value. Since
                                 * while adding to the list, the routes are added at the
                                 * end, if this route cant be removed, all other routes
                                 * also should not be removed. So no need for further
                                 * processing. */
                                break;
                            }
                        }
                    }
                }

                if (pFeasHashTab != NULL)
                {
                    TMO_HASH_Scan_Table (pFeasHashTab, u4HashKey)
                    {
                        pAdvtFeasList = &pFeasHashTab->HashList[u4HashKey];
                        BGP_SLL_DYN_Scan (pAdvtFeasList, pLinkNode,
                                          pTempLinkNode, tAdvRtLinkNode *)
                        {
                            pRtProfile = pLinkNode->pRouteProfile;
                            b1IsRtLocal = Bgp4IsLocalroute (pRtProfile);
                            if (b1IsRtLocal == TRUE)
                            {
                                u4TimePeriod =
                                    BGP4_PEER_MIN_AS_ORIG_TIME (pPeer);
                            }
                            else
                            {
                                u4TimePeriod = BGP4_PEER_MIN_ADV_TIME (pPeer);
                            }
                            if (u4TimePeriod <=
                                Bgp4DiffTime (Bgp4ElapTime (),
                                              BGP4_RT_UPDATE_SENT_TIME
                                              (pLinkNode)))
                            {
                                Bgp4DshRelRtFromPeerRtAdvList (pAdvtFeasList,
                                                               pLinkNode);
                            }
                            else
                            {
                                /* Diff time is less than configured Timer value. Since
                                 * while adding to the list, the routes are added at the
                                 * end, if this route cant be removed, all other routes
                                 * also should not be removed. So no need for further
                                 * processing. */
                                break;
                            }
                        }
                    }
                }
            }
        }
        i4RetStatus = BgpGetNextActiveContextID (u4Context, &u4Context);
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AhHandleNonBgpRtAdvtPolicyChange                      */
/* Description   : This routine handles the change in the Advertisement      */
/*               : policy for the Non-BGP routes. If the policy is made from */
/*               : EXTERNAL to BOTH, then the non-BGP best routes are        */
/*               : advertised to the internal peers. If the policy changes   */
/*               : from BOTH to EXTERNAL, then the previously advertised     */
/*               : non-BGP best routes are withdrawn for internal peers      */
/* Input(s)      : New Non Bgp Routes Advt policy (u4Bgp4NewAdvtPolicy)      */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS - if opertion is success                     */
/*               : BGP4_FAILURE - if operation fails                         */
/*****************************************************************************/
INT4
Bgp4AhHandleNonBgpRtAdvtPolicyChange (UINT4 u4Context,
                                      UINT4 u4Bgp4NewAdvtPolicy)
{
    tTMO_SLL            TsAdvtRouteList;
    tTMO_SLL            TsDummyRouteList;
    tRouteProfile       RtProfile;
    tAddrPrefix         InvPrefix;
    tLinkNode          *pLinkNode = NULL;
    tLinkNode          *pTempLinkNode = NULL;
    tBgp4PeerEntry     *pPeer = NULL;
    tRouteProfile      *pGetNextRtProfileList = NULL;
    tRouteProfile      *pRtProfileList = NULL;
    VOID               *pRibNode = NULL;
    VOID               *pNextRibNode = NULL;
    UINT4               u4RtCnt = 0;
    UINT4               u4AggrIndex = 0;
    INT4                i4Status = BGP4_FAILURE;
    UINT1               u1CanRtAdvt = BGP4_TRUE;

    TMO_SLL_Init (&TsAdvtRouteList);
    TMO_SLL_Init (&TsDummyRouteList);

    Bgp4InitAddrPrefixStruct (&(InvPrefix),
                              BGP4_AFI_IN_ADDR_PREFIX_INFO
                              (BGP4_NON_BGP_INIT_PREFIX_INFO (u4Context)));

    /* Obtain the route-info for Non-BGP Route Advertisement Policy Change */
    if ((PrefixMatch (BGP4_NON_BGP_INIT_PREFIX_INFO (u4Context), InvPrefix)) ==
        BGP4_TRUE)
    {
        /* Get First route from RIB */
        pRtProfileList = Bgp4RibhGetFirstImportRtEntry (u4Context, &pRibNode);
        if (pRtProfileList == NULL)
        {
            /* No routes present in RIB */
            BGP4_NON_BGP_RT_EXPORT_POLICY (u4Context) = u4Bgp4NewAdvtPolicy;
            BGP4_NON_BGP_EVENT_RCVD (u4Context) = 0;
            Bgp4InitAddrPrefixStruct (&
                                      (BGP4_NON_BGP_INIT_PREFIX_INFO
                                       (u4Context)), 0);
            return BGP4_SUCCESS;
        }
    }
    else
    {
        /* Get the route corresponding to the route-info from the RIB */
        RtProfile.u1Ref = 0;
        RtProfile.u1Protocol = 0;
        RtProfile.p_RPNext = 0;
        RtProfile.u4Flags = 0;
        RtProfile.pRtInfo = 0;
        Bgp4CopyNetAddressStruct (&(RtProfile.NetAddress),
                                  BGP4_NON_BGP_INIT_ROUTE_INFO (u4Context));

        i4Status = Bgp4RibhLookupRtEntry (&RtProfile, u4Context,
                                          BGP4_TREE_FIND_EXACT,
                                          &pRtProfileList, &pRibNode);
        if (i4Status == BGP4_FAILURE)
        {
            /* No routes present in RIB */
            BGP4_NON_BGP_RT_EXPORT_POLICY (u4Context) = u4Bgp4NewAdvtPolicy;
            BGP4_NON_BGP_EVENT_RCVD (u4Context) = 0;
            Bgp4InitAddrPrefixStruct (&BGP4_NON_BGP_INIT_PREFIX_INFO
                                      (u4Context), 0);
            return BGP4_SUCCESS;
        }
    }

    for (;;)
    {
        pGetNextRtProfileList = NULL;
        /* Initialize the pNextRibNode with pRibNode, because the
         * Bgp4RibhGetNextImportRtEntry takes the curretn Routes node as
         * input and returns the next route node as the output */
        pNextRibNode = pRibNode;
        pGetNextRtProfileList = Bgp4RibhGetNextImportRtEntry (pRtProfileList,
                                                              &pNextRibNode);

        /* pRtProfileList points to the current route list that needs to
         * be processed and pGetNextRtProfileList contains the info whether
         * the next route is present or not. If next route exists
         * then pGetNextRtProfileList holds the pointer to the next
         * route list. */

        if (BGP4_RT_PROTOCOL (pRtProfileList) == BGP_ID)
        {
            /* Non route is not present for this destination. */
        }
        else
        {
            /* Non BGP route is present for this destination. */
            /* Check for the aggregation policy. */
            i4Status = Bgp4AggrCanAggregationBeDone (pRtProfileList,
                                                     &u4AggrIndex, u4AggrIndex);
            if (i4Status == BGP4_SUCCESS)
            {
                if (BGP4_RT_PREFIXLEN (pRtProfileList) ==
                    BGP4_AGGRENTRY_PREFIXLEN (u4AggrIndex))
                {
                    /* Non-BGP is the best route. So it needs to be advt. */
                    u1CanRtAdvt = BGP4_TRUE;
                }
                else
                {
                    if ((BGP4_AGGR_GET_STATUS
                         ((&(BGP4_AGGRENTRY[u4AggrIndex]))) &
                         BGP4_AGGR_CREATE_IN_PROGRESS) ==
                        BGP4_AGGR_CREATE_IN_PROGRESS)
                    {
                        if ((PrefixMatch
                             (BGP4_AGGRENTRY_INIT_PREFIX_INFO (u4AggrIndex),
                              InvPrefix) == BGP4_TRUE)
                            ||
                            (AddrCompare
                             (pRtProfileList->NetAddress.NetAddr,
                              BGP4_RT_PREFIXLEN (pRtProfileList),
                              BGP4_AGGRENTRY_INIT_PREFIX_INFO (u4AggrIndex),
                              BGP4_AGGRENTRY_INIT_PREFIX_LEN (u4AggrIndex)) >
                             0))
                        {
                            /* Route is either more-specific to the
                             * stored init route or stored init route
                             * is 0. Advertise the route to peer. */
                            u1CanRtAdvt = BGP4_TRUE;
                        }
                        else
                        {
                            if (BGP4_AGGRENTRY_ADV_TYPE (u4AggrIndex) ==
                                BGP4_SUMMARY)
                            {
                                /* No need to advertise this route.
                                 * Aggregate route will be advertised
                                 * once creat is completed. */
                                u1CanRtAdvt = BGP4_FALSE;
                            }
                            else
                            {
                                /* Need to advt this route. */
                                u1CanRtAdvt = BGP4_TRUE;
                            }
                        }
                    }
                    else if ((BGP4_AGGR_GET_STATUS
                              ((&(BGP4_AGGRENTRY[u4AggrIndex]))) &
                              BGP4_AGGR_DELETE_IN_PROGRESS) ==
                             BGP4_AGGR_DELETE_IN_PROGRESS)
                    {
                        /* Need to advt this route. */
                        u1CanRtAdvt = BGP4_TRUE;
                    }
                    else
                    {
                        /* Check for the advertisement policy and
                         * either advt or withdraw the route. */
                        if (BGP4_AGGRENTRY_ADV_TYPE (u4AggrIndex) ==
                            BGP4_SUMMARY)
                        {
                            /* No need to advertise this route. */
                            u1CanRtAdvt = BGP4_FALSE;
                        }
                    }
                }
            }
            if (u1CanRtAdvt == BGP4_TRUE)
            {
                /* Route is best route and should have been processed for
                 * advt earlier. So depanding on the policy add the route to
                 * the internal peer advt list. */
                if (u4Bgp4NewAdvtPolicy == BGP4_EXTERNAL_PEER)
                {
                    /* Current policy is External Peer only. So need to
                     * advt as withdrawn to the internal peers. */
                    if ((BGP4_RT_GET_FLAGS (pRtProfileList) &
                         BGP4_RT_IN_INT_PEER_ADVT_LIST) ==
                        BGP4_RT_IN_INT_PEER_ADVT_LIST)
                    {
                        /* Route in internal peer advt list waiting for
                         * advt. so just remove it. */
                        BGP4_RT_RESET_FLAG (pRtProfileList,
                                            BGP4_RT_IN_INT_PEER_ADVT_LIST);
                        Bgp4DshDelinkRouteFromList
                            (BGP4_INT_PEERS_ADVT_LIST (u4Context),
                             pRtProfileList, BGP4_INT_PEER_LIST_INDEX);
                    }
                    else
                    {
                        BGP4_RT_SET_FLAG (pRtProfileList,
                                          BGP4_RT_AGGR_WITHDRAWN);
                        Bgp4DshAddRouteToList (&TsAdvtRouteList,
                                               pRtProfileList, 0);
                    }
                }
                else
                {
                    /* New policy is advt to both internal and external 
                     * peers.
                     * So route should be advt to the internal peers. */
                    if ((BGP4_RT_GET_FLAGS (pRtProfileList) &
                         BGP4_RT_IN_INT_PEER_ADVT_LIST) ==
                        BGP4_RT_IN_INT_PEER_ADVT_LIST)
                    {
                        /* Route in internal peer advt list waiting for 
                         * advt. This should never happen */
                    }
                    else
                    {
                        Bgp4DshAddRouteToList (&TsAdvtRouteList,
                                               pRtProfileList, 0);
                    }
                }

                /* Increment the processed route count */
                u4RtCnt++;
            }
        }

        /* Now process the next Non-BGP route in the RIB. */
        if (pGetNextRtProfileList == NULL)
        {
            /* No more Non BGP Routes in RIB */
            break;
        }

        if (u4RtCnt >= BGP4_MAX_REDIS_UPD2PROCESS)
        {
            break;
        }

        /* Reinitialise the pRtProfileList with pGetNextRtProfileList */
        pRtProfileList = pGetNextRtProfileList;
        pRibNode = pNextRibNode;

    }                            /* while-loop */

    if (TMO_SLL_Count (&TsAdvtRouteList) > 0)
    {
        switch (u4Bgp4NewAdvtPolicy)
        {
            case BGP4_EXT_OR_INT_PEER:
                /* Need to advertise all the non-BGP best routes to 
                 * Internal Peers as feasible Routes */
                TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer,
                              tBgp4PeerEntry *)
            {
                if ((BGP4_GET_PEER_TYPE (u4Context, pPeer) ==
                     BGP4_EXTERNAL_PEER)
                    && (BGP4_CONFED_PEER_STATUS (pPeer) == BGP4_FALSE))
                {
                    continue;
                }

                /* Peer is INTERNAL. So advt the route as feasible */
                Bgp4PeerSendRoutesToPeerTxQ (pPeer, &TsAdvtRouteList,
                                             &TsDummyRouteList, BGP4_FALSE);
            }
                break;

            case BGP4_EXTERNAL_PEER:
                /* Need to advertise all the non-BGP best routes to 
                 * Internal Peers as Withdrawn Routes */
                TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer,
                              tBgp4PeerEntry *)
            {
                if ((BGP4_GET_PEER_TYPE (u4Context, pPeer) ==
                     BGP4_EXTERNAL_PEER)
                    && (BGP4_CONFED_PEER_STATUS (pPeer) == BGP4_FALSE))
                {
                    continue;
                }

                /* Peer is INTERNAL. */
                Bgp4PeerSendRoutesToPeerTxQ (pPeer, &TsDummyRouteList,
                                             &TsAdvtRouteList, BGP4_FALSE);
            }
                break;
            default:
                break;
        }
        /* Reset the the BGP4_RT_AGGR_WITHDRAWN. */
        BGP_SLL_DYN_Scan (&TsAdvtRouteList, pLinkNode, pTempLinkNode,
                          tLinkNode *)
        {
            BGP4_RT_RESET_FLAG (pLinkNode->pRouteProfile,
                                BGP4_RT_AGGR_WITHDRAWN);
            Bgp4DshRemoveNodeFromList (&TsAdvtRouteList, pLinkNode);
        }
    }

    if (pGetNextRtProfileList == NULL)
    {
        /* Have completed processing all the NON-BGP routes in the RIB. */
        BGP4_NON_BGP_RT_EXPORT_POLICY (u4Context) = u4Bgp4NewAdvtPolicy;
        BGP4_NON_BGP_EVENT_RCVD (u4Context) = 0;
        Bgp4InitAddrPrefixStruct (&(BGP4_NON_BGP_INIT_PREFIX_INFO (u4Context)),
                                  0);
        return BGP4_SUCCESS;
    }

    /* Still more Non-BGP routes are present in the RIB.
     * Update the NON-BGP Init info */
    Bgp4CopyAddrPrefixStruct (&(BGP4_NON_BGP_INIT_PREFIX_INFO (u4Context)),
                              pGetNextRtProfileList->NetAddress.NetAddr);
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name   : Bgp4AhSendUpdatesToPeer                                */
/* Description     : Sends updates to the peer with given                   */
/*                   lists of feasible and withdrawn route profiles.        */
/* Input(s)        : pPeerEntry- pointer to the peer entry for which the    */
/*                               advertisement is to be done.               */
/*                   pSelectedAdvtFeasRtList- pointer to list of selected   */
/*                               best route profiles for advertisement.     */
/*                   pSelectedAdvtWithdrawnRtList - pointer to the list of  */
/*                               selected withdrawn route profiles for      */
/*                               advertisement.                             */
/* Output(s)       :  None.                                                 */
/* Returns         :  BGP4_SUCCESS or BGP4_FAILURE                          */
/****************************************************************************/
INT4
Bgp4AhSendUpdatesToPeer (tBgp4PeerEntry * pPeerEntry,
                         tTMO_SLL * pSelectedAdvtFeasRtList,
                         tTMO_SLL * pSelectedAdvtWithdrawnRtList)
{
    UINT1              *pu1UpdateMessageToSend = NULL;
    INT4                i4Status = BGP4_FAILURE;
    UINT4               u4NegOrfCapMask = 0;
    UINT4               u4AfiSafiMask = 0;
    UINT2               u2BufLen = BGP4_MAX_OUTBUF_LEN;
    UINT2               u2UnfeasRtLen = 0;
    UINT2               u2PattrLen = 0;

    /* If ORF receive mode is advertised for this peer and if it has not 
     * received any ROUTE-REFRESH message or ORF message, it should not send any update*/

    BGP4_GET_AFISAFI_MASK (BGP4_PEER_REMOTE_ADDR_TYPE (pPeerEntry),
                           (UINT2) BGP4_INET_SAFI_UNICAST, u4AfiSafiMask);

    /* Fetch the ORF Capability negotiated mask for the given AFI,SAFI,ORF receive mode */
    if (Bgp4OrfGetOrfCapMask (u4AfiSafiMask, BGP4_CLI_ORF_MODE_RECEIVE,
                              &u4NegOrfCapMask) == BGP4_SUCCESS)
    {
        /* Check whether ORF receive Capability is negotiated with
         * the peer for this AFI,SAFI or not. */
        if ((BGP4_PEER_NEG_CAP_MASK (pPeerEntry) & u4NegOrfCapMask) ==
            u4NegOrfCapMask)
        {
            if (BGP4_PEER_REMOTE_ADDR_TYPE (pPeerEntry) == BGP4_INET_AFI_IPV4)
            {
                if (BGP4_RTREF_MSG_RCVD_CTR (pPeerEntry, BGP4_IPV4_UNI_INDEX) ==
                    0)
                {
                    return BGP4_SUCCESS;
                }
            }
#ifdef BGP4_IPV6_WANTED
            else if (BGP4_PEER_REMOTE_ADDR_TYPE (pPeerEntry) ==
                     BGP4_INET_AFI_IPV6)
            {
                if (BGP4_RTREF_MSG_RCVD_CTR (pPeerEntry, BGP4_IPV6_UNI_INDEX) ==
                    0)
                {
                    return BGP4_SUCCESS;
                }
            }
#endif
        }
    }

    /* If there is any entry present in either feasible routes list
     * or withdrawn routes list then we can process the route profiles.
     * If not return success, since there is no route profile to process.
     */
    if ((TMO_SLL_Count (pSelectedAdvtFeasRtList) == 0) &&
        (TMO_SLL_Count (pSelectedAdvtWithdrawnRtList) == 0))
    {
        return BGP4_SUCCESS;
    }

    pu1UpdateMessageToSend = Bgp4FormBgp4Hdr (BGP4_UPDATE_MSG);

    if (pu1UpdateMessageToSend == NULL)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                       BGP4_TRC_FLAG,
                       BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC |
                       BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Error forming BGP Header. Routes not advertised.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerEntry))));
        return BGP4_FAILURE;
    }

    u2BufLen =
        (UINT2) (u2BufLen -
                 (BGP4_MSG_COMMON_HDR_LEN + BGP4_WITHDRAWN_LEN_FIELD_SIZE +
                  BGP4_ATTR_LEN_FIELD_SIZE));
    if (TMO_SLL_Count (pSelectedAdvtWithdrawnRtList) > 0)
    {
        i4Status = Bgp4ProcessWithdrawnRoutesList (pPeerEntry,
                                                   pSelectedAdvtWithdrawnRtList,
                                                   &pu1UpdateMessageToSend,
                                                   &u2BufLen, &u2UnfeasRtLen);
        if (i4Status == BGP4_FAILURE)
        {
            BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                      BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                      BGP4_MOD_NAME,
                      "Failure in Processing the Withdrawn Route List!!!.\n");
            return BGP4_FAILURE;
        }
    }

    if (TMO_SLL_Count (pSelectedAdvtFeasRtList) > 0)
    {
        i4Status = Bgp4AhSendMpeRoutes (pPeerEntry,
                                        pSelectedAdvtFeasRtList,
                                        &pu1UpdateMessageToSend,
                                        &u2BufLen, &u2UnfeasRtLen);
    }
    else
    {
        /* No feasible route is present to be advertised. */
        if (u2UnfeasRtLen > 0)
        {
            i4Status = Bgp4AhSendConstructUpdMsg (pPeerEntry,
                                                  &pu1UpdateMessageToSend,
                                                  &u2UnfeasRtLen,
                                                  &u2PattrLen,
                                                  &u2BufLen, BGP4_SEND_UPD);
        }
        else
        {
            /* No message to advertise. Free allocated message. */
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                pu1UpdateMessageToSend);
        }
    }
    return i4Status;
}

/****************************************************************************/
/* Function Name   : Bgp4AhAddRouteToAdvtPAList                             */
/* Description     : This routine will extract the Prefix info from the     */
/*                   route profile structure and will append it to the      */
/*                   list. The maximum size of data a node in this list can */
/*                   hold is BGP4_MAX_MSG_LEN - Path Attr Size. If the size */
/*                   of the data is  greater than this value or adding new  */
/*                   Prefix info cause the size to become greater than this */
/*                   value, then add the data in new node.                  */
/* Input(s)        : pAdvtPA   - pointer to the Advertised Path Attribute   */
/*                               node where this prefix needs to be added.  */
/*                   pRouteProfile - Pointer to the route profile structure */
/*                                   that is to be added.                   */
/* Output(s)       :  None.                                                 */
/* Returns         :  BGP4_SUCCESS or BGP4_FAILURE                          */
/****************************************************************************/
INT4
Bgp4AhAddRouteToAdvtPAList (tBgp4AdvtPathAttr * pAdvtPA,
                            tRouteProfile * pRtProfile)
{
    tBufNode           *pBufNode = NULL;
    tBgp4PeerEntry     *pPeerEntry = NULL;
    UINT4               u4AsafiMask = 0;
    INT4                i4Ret;
    UINT2               u2MaxMsgSize = 0;
    UINT1               u1MaxNlriLen = 0;
    UINT1               u1BufAlloc = BGP4_FALSE;

    /* Get the Max Message size that can be stored in a Node. */
    u2MaxMsgSize = (UINT2) (BGP4_MAX_MSG_LEN - (BGP4_MSG_COMMON_HDR_LEN +
                                                BGP4_WITHDRAWN_LEN_FIELD_SIZE +
                                                BGP4_ATTR_LEN_FIELD_SIZE) -
                            BGP4_ADVT_PA_SIZE (pAdvtPA));

    pPeerEntry = BGP4_ADVT_PA_PEER (pAdvtPA);
    Bgp4GetPeerAdvtAfiSafi (pRtProfile, pPeerEntry, &u4AsafiMask);

    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
            u1MaxNlriLen = BGP4_IPV4_PREFIX_LEN + 1;
            break;
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
            u1MaxNlriLen = BGP4_IPV6_PREFIX_LEN + 1;
            break;
#endif

#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
            u1MaxNlriLen = BGP4_VPLS_NLRI_PREFIX_LEN;
            break;
#endif

#ifdef L3VPN
            /* Carrying Label Informatin - RFC 3107 */
        case CAP_MP_LABELLED_IPV4:
            u1MaxNlriLen = (UINT1) ((BGP4_RT_LABEL_CNT (pRtProfile) > 0) ?
                                    ((BGP4_RT_LABEL_CNT (pRtProfile) *
                                      BGP4_VPN4_LABEL_SIZE) +
                                     BGP4_IPV4_PREFIX_LEN +
                                     1) : BGP4_VPN4_LABEL_SIZE +
                                    BGP4_IPV4_PREFIX_LEN + 1);
            break;
        case CAP_MP_VPN4_UNICAST:
            /* Here it is assumed that only one label will be carried in
             * update message */
            u1MaxNlriLen = (UINT1) ((BGP4_RT_LABEL_CNT (pRtProfile) > 0) ?
                                    ((BGP4_RT_LABEL_CNT (pRtProfile) *
                                      BGP4_VPN4_LABEL_SIZE) +
                                     BGP4_VPN4_PREFIX_LEN +
                                     1) : BGP4_VPN4_LABEL_SIZE +
                                    BGP4_VPN4_PREFIX_LEN + 1);
            break;
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
            if ((pRtProfile->EvpnNlriInfo.u1RouteType == EVPN_MAC_ROUTE) ||
                (pRtProfile->EvpnNlriInfo.u1RouteType ==
                 EVPN_ETH_SEGMENT_ROUTE))
            {
                if (pRtProfile->EvpnNlriInfo.u1IpAddrLen == 0)
                {
                    if (pRtProfile->EvpnNlriInfo.u1RouteType == EVPN_MAC_ROUTE)
                    {
                        u1MaxNlriLen = BGP4_EVPN_MAC_NLRI_PREFIX_LEN;
                    }
                    else
                    {
                        u1MaxNlriLen = BGP4_EVPN_ETH_SEG_NLRI_PREFIX_LEN;
                    }
                }
                else if (pRtProfile->EvpnNlriInfo.u1IpAddrLen ==
                         BGP4_EVPN_IP_ADDR_SIZE)
                {
                    if (pRtProfile->EvpnNlriInfo.u1RouteType == EVPN_MAC_ROUTE)
                    {
                        u1MaxNlriLen = BGP4_EVPN_MAC_NLRI_PREFIX_LEN +
                            BGP4_EVPN_IP_ADDR_SIZE;
                    }
                    else
                    {
                        u1MaxNlriLen = BGP4_EVPN_ETH_SEG_NLRI_PREFIX_LEN +
                            BGP4_EVPN_IP_ADDR_SIZE;
                    }
                }
                else if (pRtProfile->EvpnNlriInfo.u1IpAddrLen ==
                         BGP4_EVPN_IP6_ADDR_SIZE)
                {
                    if (pRtProfile->EvpnNlriInfo.u1RouteType == EVPN_MAC_ROUTE)
                    {
                        u1MaxNlriLen = BGP4_EVPN_MAC_NLRI_PREFIX_LEN +
                            BGP4_EVPN_IP6_ADDR_SIZE;
                    }
                    else
                    {
                        u1MaxNlriLen = BGP4_EVPN_ETH_SEG_NLRI_PREFIX_LEN +
                            BGP4_EVPN_IP6_ADDR_SIZE;
                    }
                }
            }
            else if (pRtProfile->EvpnNlriInfo.u1RouteType == EVPN_AD_ROUTE)
            {
                u1MaxNlriLen = BGP4_EVPN_ETH_AD_ROUTE_NLRI_PREFIX_LEN;
            }

            break;
#endif
        default:
            /* This address Family not supported. */
            return BGP4_FAILURE;
    }

    /* Get the last Node in the ADVT-LIST */
    pBufNode = (tBufNode *) TMO_SLL_Last (BGP4_ADVT_PA_ROUTE_LIST (pAdvtPA));
    if ((pBufNode == NULL) ||
        ((BGP4_ADVT_PA_SIZE (pAdvtPA) + pBufNode->u4MsgSize + u1MaxNlriLen) >
         u2MaxMsgSize))
    {
        /* This node is already full or no node exist. Need to add the
         * Route Prefix to a new node. */
        pBufNode = (tBufNode *) MemAllocMemBlk (gBgpNode.Bgp4BufNodesPoolId);
        if (pBufNode == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                      BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation to add Node to ADVT_PA_DB FAILED!!!\n");
            gu4BgpDebugCnt[MAX_BGP_BUF_NODES_SIZING_ID]++;
            return BGP4_FAILURE;
        }
        TMO_SLL_Init_Node (&pBufNode->TSNext);

        /* Allocate Buffer to Store the Prefix. Currently we use MALLOC to
         * allocate Buf Size of u2MaxMsgSize, for storing the Prefix. This is
         * always an unnecesary usage of additional memory. To optimise, if
         * the environment supports REALLOC, then this function can be used
         * for more efficient memory utilisation. */
        pBufNode->pu1Msg = MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);
        if (pBufNode->pu1Msg == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                      BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation to add Prefix to ADVT_PA_DB FAILED!!!\n");
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodesPoolId,
                                (UINT1 *) pBufNode);
            gu4BgpDebugCnt[MAX_BGP_BUF_NODES_SIZING_ID]++;
            return BGP4_FAILURE;
        }
        pBufNode->u4MsgSize = 0;
        pBufNode->u4MsgOffset = 0;
        u1BufAlloc = BGP4_TRUE;
    }

    /* Add the Prefix to the List. */
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
#endif
            i4Ret = Bgp4AttrFillPrefix ((pBufNode->pu1Msg +
                                         pBufNode->u4MsgSize), pRtProfile);
            pBufNode->u4MsgSize += (UINT4) i4Ret;
            break;

#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
            i4Ret = Bgp4AttrFillVplsPrefix ((pBufNode->pu1Msg +
                                             pBufNode->u4MsgSize), pRtProfile,
                                            pPeerEntry);
            pBufNode->u4MsgSize += i4Ret;
            break;
#endif

#ifdef L3VPN
            /* Carrying Label Information - RFC 3107 */
        case CAP_MP_LABELLED_IPV4:
            i4Ret = Bgp4MpeFillIpLbldPrefix ((pBufNode->pu1Msg +
                                              pBufNode->u4MsgSize), pRtProfile,
                                             pAdvtPA, pPeerEntry);

            if (i4Ret == BGP4_FAILURE)
            {
                if (u1BufAlloc == BGP4_TRUE)
                {
                    MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                        (UINT1 *) pBufNode->pu1Msg);
                    MemReleaseMemBlock (gBgpNode.Bgp4BufNodesPoolId,
                                        (UINT1 *) pBufNode);
                    return BGP4_FAILURE;
                }
            }
            pBufNode->u4MsgSize = (pBufNode->u4MsgSize + (UINT4) i4Ret);
            break;

        case CAP_MP_VPN4_UNICAST:
            i4Ret = Bgp4MpeFillIpLbldPrefix ((pBufNode->pu1Msg +
                                              pBufNode->u4MsgSize), pRtProfile,
                                             pAdvtPA, pPeerEntry);
            if (i4Ret == BGP4_FAILURE)
            {
                if (u1BufAlloc == BGP4_TRUE)
                {
                    MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                        (UINT1 *) pBufNode->pu1Msg);
                    MemReleaseMemBlock (gBgpNode.Bgp4BufNodesPoolId,
                                        (UINT1 *) pBufNode);
                    return BGP4_FAILURE;
                }
            }
            pBufNode->u4MsgSize = (pBufNode->u4MsgSize + (UINT4) i4Ret);
            break;
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
            i4Ret = Bgp4MpeFillEvpnPrefix ((pBufNode->pu1Msg +
                                            pBufNode->u4MsgSize), pRtProfile,
                                           pPeerEntry);
            pBufNode->u4MsgSize = (pBufNode->u4MsgSize + (UINT4) i4Ret);
            break;
#endif
        default:
            break;
    }
    if (u1BufAlloc == BGP4_TRUE)
    {
        TMO_SLL_Add (BGP4_ADVT_PA_ROUTE_LIST (pAdvtPA), &pBufNode->TSNext);
    }
    return BGP4_SUCCESS;
}

/****************************************************************************/
/* Function Name   : Bgp4AhSendMpeRoutes                                    */
/* Description     : Sends updates to the peer with given                   */
/*                   lists of reachable and unreachable route               */
/*                   profiles. If the speaker and peer is negotiated for    */
/*                   <AFI, SAFI> then routes will be sent in MP_REACH_NLRI/ */
/*                   MP_UNREACH_NLRI path attributes, otherwise only        */
/*                   <IPV4, IPV6, unicast> routes will be filled in normal  */
/*                   NLRI                                                   */
/*                   field of UPDATE message (In this case, the assumption  */
/*                   is list of unreachble routes should be empty)          */
/* Input(s)        : pPeerEntry- pointer to the peer entry for which the    */
/*                               advertisement is to be done.               */
/*                   pAdvtReachRts - pointer to list of selected            */
/*                               reachable route profiles for advertisement.*/
/*                   ppu1MsgBuf - pointer to the already constructed UPDATE */
/*                               message.                                   */
/*                   pu2BufLen - pointer to the buffer length left in UPDATE*/
/*                   u2UnfeasRtLen - Length of unfeasible route in the      */
/*                                   update message.                        */
/* Output(s)       :  None.                                                 */
/* Returns         :  BGP4_SUCCESS or BGP4_FAILURE                          */
/****************************************************************************/
INT4
Bgp4AhSendMpeRoutes (tBgp4PeerEntry * pPeerEntry,
                     tTMO_SLL * pAdvtReachRts,
                     UINT1 **ppu1MsgBuf,
                     UINT2 *pu2BufLen, UINT2 *pu2UnfeasRtLen)
{
    tLinkNode          *pRtLink = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tBgp4Info          *pAdvtBgpInfo = NULL;
    tBgp4AdvtPathAttr  *pAdvtPA = NULL;
    tBgp4AdvtPathAttr  *pAdvtPANode = NULL;
    tBgp4AdvtPathAttr  *pTempAdvtPANode = NULL;
    UINT1              *pu1MsgBuf = NULL;
    UINT4               u4HashKey = 0;
    UINT4               u4ExtRt = BGP4_FALSE;
    UINT4               u4AsafiMask = 0;
    UINT4               u4AfiIndex = 0;
    INT4                i4Status = BGP4_FAILURE;
    UINT2               u2BufLen = 0;
    UINT2               u2TotalPALen = 0;
    UINT2               u2UnfeasRtLen = 0;
    UINT2               u2Afi = 0;
    UINT2               u2Safi = 0;
    UINT4               u4UpdSentTime = 0;

    /* We have list of feasible route list. Now process each route,
     * create the OUTBOUND BGP4-INFO to be advertised for that route
     * and Pack all the routes with identical OUTBOUND BGP4-INFO.
     * Once all the routes in the list are packed together, advertise
     * the routes. */

    pu1MsgBuf = *ppu1MsgBuf;
    u2BufLen = *pu2BufLen;
    u2UnfeasRtLen = *pu2UnfeasRtLen;

    TMO_SLL_Scan (pAdvtReachRts, pRtLink, tLinkNode *)
    {
        pRtProfile = pRtLink->pRouteProfile;
        Bgp4GetPeerAdvtAfiSafi (pRtProfile, pPeerEntry, &u4AsafiMask);
        switch (u4AsafiMask)
        {
            case CAP_MP_IPV4_UNICAST:
                u4AfiIndex = BGP4_IPV4_UNI_INDEX;
                break;
#ifdef BGP4_IPV6_WANTED
            case CAP_MP_IPV6_UNICAST:
                u4AfiIndex = BGP4_IPV6_UNI_INDEX;
                break;
#endif
#ifdef L3VPN
                /* Carrying Label Information - RFC 3107 */
            case CAP_MP_LABELLED_IPV4:
                u4AfiIndex = BGP4_IPV4_LBLD_INDEX;
                break;
            case CAP_MP_VPN4_UNICAST:
                u4AfiIndex = BGP4_VPN4_UNI_INDEX;
                break;
#endif

#ifdef VPLSADS_WANTED
                /*ADS-VPLS Related Processing */
            case CAP_MP_L2VPN_VPLS:
                u4AfiIndex = BGP4_L2VPN_VPLS_INDEX;
                break;
#endif
#ifdef EVPN_WANTED
            case CAP_MP_L2VPN_EVPN:
                u4AfiIndex = BGP4_L2VPN_EVPN_INDEX;
                break;
#endif
            default:
                continue;
        }

        pAdvtBgpInfo = Bgp4MemAllocateBgp4info (sizeof (tBgp4Info));
        if (pAdvtBgpInfo == NULL)
        {
            /* Memory allocation fails. Now process the routes that
             * are so far updated in the ADVT_DB. */
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for Advertise BGPINFO FAILED!!!\n");
            gu4BgpDebugCnt[MAX_BGP_ROUTE_INFO_ENTRIES_SIZING_ID]++;
            if (u2UnfeasRtLen > 0)
            {
                /* Buffer is having withdrawn routes to be advertised.
                 * Advertise withdrawn routes. Try to advertise all the
                 * route that are being processed so far. */
                i4Status = Bgp4AhSendConstructUpdMsg (pPeerEntry,
                                                      &pu1MsgBuf,
                                                      &u2UnfeasRtLen,
                                                      &u2TotalPALen,
                                                      &u2BufLen, BGP4_SEND_UPD);
                if ((i4Status == BGP4_FAILURE) || (pu1MsgBuf == NULL))
                {
                    Bgp4AdvtPADBClearAdvtDB (BGP4_PEER_CXT_ID (pPeerEntry));
                    return BGP4_FAILURE;
                }
            }
            break;
        }
        BGP4_INFO_REF_COUNT (pAdvtBgpInfo)++;

        i4Status = Bgp4AttrFillPathAttributes (pPeerEntry, pRtProfile,
                                               pAdvtBgpInfo, pRtLink->pRtInfo);
        if (i4Status == BGP4_FAILURE)
        {
            /* Failure processing this route. Release the BGP4-INFO. */
            Bgp4DshReleaseBgpInfo (pAdvtBgpInfo);
            continue;
        }

        /* Now Pack this ADVT BGP4-INFO with other available info's. */
        Bgp4GetAfiSafiFromMask (u4AsafiMask, &u2Afi, &u2Safi);
        pAdvtPA = Bgp4AdvtPADBAddEntry (pPeerEntry, pAdvtBgpInfo,
                                        u2Afi, u2Safi);

        /* Release the ADVT BGP4-INFO structure. */
        Bgp4DshReleaseBgpInfo (pAdvtBgpInfo);
        if (pAdvtPA == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for AdvtPathAttrib FAILED!!!\n");
            Bgp4AdvtPADBClearAdvtDB (BGP4_PEER_CXT_ID (pPeerEntry));
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1MsgBuf);
            return (BGP4_FAILURE);
        }

        /* Add the route to the Advt-PA entry. */
        i4Status = Bgp4AhAddRouteToAdvtPAList (pAdvtPA, pRtProfile);
        if (i4Status == BGP4_FAILURE)
        {
            Bgp4AdvtPADBClearAdvtDB (BGP4_PEER_CXT_ID (pPeerEntry));
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1MsgBuf);
            return (BGP4_FAILURE);
        }
        if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
        {
            if ((BGP4_GET_PEER_TYPE
                 (BGP4_PEER_CXT_ID (pPeerEntry),
                  BGP4_RT_PEER_ENTRY (pRtProfile)) == BGP4_EXTERNAL_PEER)
                && (BGP4_CONFED_PEER_STATUS (BGP4_RT_PEER_ENTRY (pRtProfile)) ==
                    BGP4_FALSE))
            {
                u4ExtRt = BGP4_TRUE;
            }
        }
        if ((u4ExtRt == BGP4_TRUE) &&
            ((BGP4_GET_PEER_CURRENT_STATE (pPeerEntry) &
              BGP4_PEER_INIT_INPROGRESS) == 0))

        {

            /* Remove the Old route from the peer Advt list */
            Bgp4DshRemoveRtFrmPeerAdvtList (pPeerEntry,
                                            BGP4_PEER_ADVT_FEAS_LIST,
                                            pRtProfile, &u4UpdSentTime);
            Bgp4DshAddRouteToPeerAdvList (pPeerEntry, BGP4_PEER_ADVT_FEAS_LIST,
                                          pRtProfile, 0, Bgp4ElapTime ());
        }
        u4ExtRt = BGP4_FALSE;
        BGP4_PEER_PREFIX_SENT_CNT (pPeerEntry, u4AfiIndex)++;
        if (BGP4_PEER_HOLD_ADVT_ROUTES (pPeerEntry) == BGP4_HOLD_ROUTES_ENABLE)
        {
            /* Add the route to the Advt routes RBTree */
            Bgp4AddAdvtRouteToList (pPeerEntry, pRtProfile);
        }

    }

    /* Process the route added to ADVT_PA_DB. */
    TMO_HASH_Scan_Table (BGP4_ADVT_PA_DB (BGP4_PEER_CXT_ID (pPeerEntry)),
                         u4HashKey)
    {
        BGP4_HASH_DYN_Scan_Bucket (BGP4_ADVT_PA_DB
                                   (BGP4_PEER_CXT_ID (pPeerEntry)), u4HashKey,
                                   pAdvtPANode, pTempAdvtPANode,
                                   tBgp4AdvtPathAttr *)
        {
            /* Process all the route in this node. */
            Bgp4ProcessFeasibleRouteList
                (pPeerEntry, pAdvtPANode,
                 BGP4_ADVT_PA_AFI (pAdvtPANode),
                 BGP4_ADVT_PA_SAFI (pAdvtPANode),
                 pu1MsgBuf, u2BufLen, u2UnfeasRtLen);
            Bgp4AdvtPADBClearAdvtEntry (&pAdvtPANode->NextNonIdentPA);

            /* Previously allocate Allocate new buffer and use it. */
            pu1MsgBuf = Bgp4FormBgp4Hdr (BGP4_UPDATE_MSG);
            if (pu1MsgBuf == NULL)
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                               BGP4_TRC_FLAG,
                               BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC |
                               BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                               "\tPEER %s : Error forming BGP Header. Routes not advertised.\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerEntry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerEntry))));
                /* clear ADVT_PA_DB */
                Bgp4AdvtPADBClearAdvtDB (BGP4_PEER_CXT_ID (pPeerEntry));
                return (BGP4_FAILURE);
            }

            u2BufLen = BGP4_MAX_OUTBUF_LEN - (BGP4_MSG_COMMON_HDR_LEN +
                                              BGP4_WITHDRAWN_LEN_FIELD_SIZE +
                                              BGP4_ATTR_LEN_FIELD_SIZE);
            u2UnfeasRtLen = 0;
        }
    }

    /* Release the Update message. */
    MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1MsgBuf);
    return (BGP4_SUCCESS);
}

/****************************************************************************/
/* Function Name   : Bgp4AhAppendMpeAttrBuffToUpdMsg                        */
/* Description     : Appends the MPE path attribute information (Could be   */
/*                   either MP_REACH_NLRI or MP_UNREACH_NLRI) to the UDPATE */
/*                   message.                                               */
/* Input(s)        : pu1UpdBufToSend - pointer to the UPDATE message        */
/*                   pu1MpeAttrBuf - pointer to the MPE path attrib buffer  */
/*                   u2MpeBufLen - length of the MPE path attrib buffer     */
/*                   u1MpeAttrType - Type of MPE path attribute             */
/*                   u2Afi - Address family of the update message           */
/*                   u1Szfi - Sub-Address family of the update message      */
/* Output(s)       : None.                                                  */
/* Returns         : Number of bytes appended                               */
/****************************************************************************/
UINT2
Bgp4AhAppendMpeAttrBuffToUpdMsg (UINT1 *pu1UpdBufToSend,
                                 UINT1 *pu1MpeAttrBuf,
                                 UINT2 u2MpeBufLen,
                                 UINT1 u1MpeAttrType, UINT2 u2Afi, UINT1 u1Safi)
{
    UINT2               u2BytesFilled = 0;
    UINT2               u2TotalMpeLen = 0;

    /*  if (u2MpeBufLen == 0)
       {
       return 0;
       } */

    u2TotalMpeLen = (UINT2) (u2MpeBufLen + BGP4_MPE_ADDRESS_FAMILY_LEN +
                             BGP4_MPE_SUB_ADDRESS_FAMILY_LEN);
    /* Check whether attribute length exceeds one byte? If it exceeds
     * we should set Extended Bit
     */
    if (u2TotalMpeLen > BGP4_ONE_BYTE_MAX_VAL)
    {
        *(pu1UpdBufToSend) = BGP4_OPTIONAL_FLAG_MASK | BGP4_EXT_LEN_FLAG_MASK;
        pu1UpdBufToSend++;
        *(pu1UpdBufToSend) = u1MpeAttrType;
        pu1UpdBufToSend++;
        PTR_ASSIGN2 (pu1UpdBufToSend, (u2TotalMpeLen));
        pu1UpdBufToSend += BGP4_EXTENDED_ATTR_LEN_SIZE;
        u2BytesFilled =
            (UINT2) (u2BytesFilled +
                     (BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                      BGP4_EXTENDED_ATTR_LEN_SIZE));
    }
    else
    {
        *(pu1UpdBufToSend) = BGP4_OPTIONAL_FLAG_MASK;
        pu1UpdBufToSend++;
        *(pu1UpdBufToSend) = u1MpeAttrType;
        pu1UpdBufToSend++;
        *(pu1UpdBufToSend) = (UINT1) u2TotalMpeLen;
        pu1UpdBufToSend += BGP4_NORMAL_ATTR_LEN_SIZE;
        u2BytesFilled =
            (UINT2) (u2BytesFilled +
                     (BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                      BGP4_NORMAL_ATTR_LEN_SIZE));
    }

    PTR_ASSIGN2 (pu1UpdBufToSend, u2Afi);
    u2BytesFilled = (UINT2) (u2BytesFilled + BGP4_MPE_ADDRESS_FAMILY_LEN);
    pu1UpdBufToSend += BGP4_MPE_ADDRESS_FAMILY_LEN;
    *(pu1UpdBufToSend) = u1Safi;
    u2BytesFilled = (UINT2) (u2BytesFilled + BGP4_MPE_SUB_ADDRESS_FAMILY_LEN);
    pu1UpdBufToSend += BGP4_MPE_SUB_ADDRESS_FAMILY_LEN;
    if (u2MpeBufLen != 0)
    {
        MEMCPY (pu1UpdBufToSend, pu1MpeAttrBuf, u2MpeBufLen);
    }
    u2BytesFilled = (UINT2) (u2BytesFilled + u2MpeBufLen);
    return (u2BytesFilled);
}

/****************************************************************************/
/* Function Name   : Bgp4AhSendConstructUpdMsg                              */
/* Description     : Sends the UPDATE message to the specified peer.        */
/*                   This is responsible for filling Withdrawn Routes field */
/*                   Path Attributes Length field and Total Length field of */
/*                   UPDATE message. If requested, this will also constuct  */
/*                   a new empty UPDATE  after the input UPDATE mesg is sent*/
/* Input(s)        : pPeerEntry - pointer to the peer entry to which the    */
/*                           UPDATE message is being sent                   */
/*                   ppu1UpdBufToSend - double pointer to the UPDATE message*/
/*                   pu2UnfeasRtLen - pointer to the Withdrawn Routes field */
/*                               Length                                     */
/*                   pu2PattrLen - pointer to the Path Attributes field len */
/*                   pu2BufLen - pointer to the buffer length left unfilled */
/*                   u1SendType - request type (SEND/SEND_CONSTRUCT)        */
/* Output(s)       : ppu1UpdBuffToSend - pointer to the newly constrcted    */
/*                               UPDATE message if the request is to        */
/*                               construct a new one after sending          */
/*                   pu2UnfeasRtLen - pointer to the initialized Withdrawn  */
/*                               Routes Length                              */
/*                   pu2PattrLen - pointer to the initialized Path Attribute*/
/*                               length                                     */
/*                   pu2BufLen - pointer to the new buffer length left      */
/*                               unfilled in the newly constructed UPDATE   */
/* Returns         : BGP4_SUCCESS or BGP4_FAILURE                           */
/****************************************************************************/
INT4
Bgp4AhSendConstructUpdMsg (tBgp4PeerEntry * pPeerEntry,
                           UINT1 **ppu1UpdBufToSend,
                           UINT2 *pu2UnfeasRtLen,
                           UINT2 *pu2PattrLen,
                           UINT2 *pu2BufLen, UINT1 u1SendType)
{
    UINT1              *pu1MsgBuf = NULL;
    /*  UINT1              *pu1MsgBuf2 = NULL; */
    UINT2               u2TotSize = 0;
    INT4                i4Status = BGP4_FAILURE;

    pu1MsgBuf = *ppu1UpdBufToSend;
    u2TotSize = (UINT2) (BGP4_MAX_OUTBUF_LEN - (*pu2BufLen));
    /* Fill the Total Length, Withdrawn Routes Length field and Path
     * Attributes Length field of UPDATE message
     */
    PTR_ASSIGN2 ((pu1MsgBuf + BGP4_MARKER_LEN), (u2TotSize));
    PTR_ASSIGN2 ((pu1MsgBuf + BGP4_MSG_COMMON_HDR_LEN), ((*(pu2UnfeasRtLen))));
    PTR_ASSIGN2 ((pu1MsgBuf + BGP4_MSG_COMMON_HDR_LEN +
                  BGP4_WITHDRAWN_LEN_FIELD_SIZE + (*pu2UnfeasRtLen)),
                 ((*(pu2PattrLen))));
    i4Status = Bgp4PeerAddMsgToTxQ (pPeerEntry, pu1MsgBuf,
                                    u2TotSize, BGP4_TRUE);
    if (i4Status != BGP4_SUCCESS)
    {
        /* Do appropriate action */
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_TXQ_TRC |
                       BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                       "\tPEER %s : Failure in Queueing message"
                       "to transmission queue.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerEntry))));
        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1MsgBuf);
        return BGP4_FAILURE;
    }

    BGP4_PEER_OUT_UPDATES (pPeerEntry)++;

    /* If the request is for constructing a new UPDATE buffer also
     * then construct a new one, initialize all the outputs and return
     */
    if (u1SendType == BGP4_SEND_CONSTRUCT_UPD)
    {
        pu1MsgBuf = Bgp4FormBgp4Hdr (BGP4_UPDATE_MSG);
        if (pu1MsgBuf == NULL)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tPEER %s : Error in forming BGP Header. Update message "
                           "not sent. Routes not advertised\n.",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerEntry))));
            return BGP4_FAILURE;
        }
        *ppu1UpdBufToSend = pu1MsgBuf;
        *pu2BufLen = BGP4_MAX_OUTBUF_LEN;
    }
    else
    {
        /* Update the Buffer to prevent the usage of the old buffer. */
        *ppu1UpdBufToSend = NULL;
        *pu2BufLen = 0;
    }

    *pu2UnfeasRtLen = 0;
    *pu2PattrLen = 0;

    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                   BGP4_TRC_FLAG, BGP4_TXQ_TRC |
                   BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                   "\tPEER %s : Update message is sent successfully to "
                   "peer transmission queue.\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry))));
    return BGP4_SUCCESS;
}

/**************************************************************************
 Function Name   : Bgp4ProcessWithdrawnRoutesList
 Description     : Process with drawn route list and send to the peer.
 Input(s)        : pSelectedAdvtWithdrawnRtList - pointer to the list 
                                                  of withdrawn route.
                   pPeerEntry - pointer to the peer to which update message
                                to be sent.
                   pu2BufLen- pointer to the available buffer length
                   ppu1MsgBuf - pointer to the begining of update message 
                                            to be sent.
 Output(s)       : ppu1MsgBuf - pointer to the begining of update message 
                                            to be sent.
                   pu2UnfeasRtLen - pointer to the length of unfeasible
                                    routes
 Global Variables Referred :
 Global variables Modified :
 Exceptions or Operating System Error Handling :
 Use of Recursion :
 Returns          : BGP4_SUCCESS or BGP4_FAILURE
**************************************************************************/
INT4
Bgp4ProcessWithdrawnRoutesList (tBgp4PeerEntry * pPeerEntry,
                                tTMO_SLL * pSelectedAdvtWithdrawnRtList,
                                UINT1 **ppu1MsgBuf,
                                UINT2 *pu2BufLen, UINT2 *pu2UnfeasRtLen)
{
    UINT1              *pau1NlriBuf[BGP4_MPE_ADDR_FAMILY_MAX_INDEX];
    /* Currently this is for IPV4_UNICAST
     * and IPV6_UNICAST. This needs to be
     * extended when family is supported.
     * Index 0 for IPv4_UNICAST 
     * Index 1 for IPV6_UNICAST
     */
    UINT2               au2NlriSize[BGP4_MPE_ADDR_FAMILY_MAX_INDEX];
    /* Currently this is for IPV4_UNICAST
     * and IPV6_UNICAST. This needs to be
     * extended when family is supported.
     * Index 0 for IPv4_UNICAST 
     * Index 1 for IPV6_UNICAST
     */
    tLinkNode          *pRtLink = NULL;
    tRouteProfile      *pWithdrawnRouteToBeFilled = NULL;
    UINT4               u4NlriIndex = 0;
    UINT4               u4AsafiMask = 0;
    INT4                i4UpdateMessageSentStatus = BGP4_SUCCESS;
    INT4                i4RetVal;
    UINT2               u2BufLen = 0;
    UINT2               u2RemBufLen = 0;
    UINT2               u2UnfeasRtLen = 0;
    UINT2               u2TotalPALen = 0;
    UINT1               u1PrefixLenBytes = 0;
    UINT1              *pu1UpdateMessageToSend = NULL;

    u2BufLen = *pu2BufLen;
    *pu2UnfeasRtLen = 0;

    pau1NlriBuf[BGP4_IPV4_UNI_INDEX] = NULL;
    au2NlriSize[BGP4_IPV4_UNI_INDEX] = 0;
#ifdef BGP4_IPV6_WANTED
    pau1NlriBuf[BGP4_IPV6_UNI_INDEX] = NULL;
    au2NlriSize[BGP4_IPV6_UNI_INDEX] = 0;
#endif
#ifdef L3VPN
    /* Carrying Label Information - RFC 3107 */
    pau1NlriBuf[BGP4_IPV4_LBLD_INDEX] = NULL;
    au2NlriSize[BGP4_IPV4_LBLD_INDEX] = 0;
    pau1NlriBuf[BGP4_VPN4_UNI_INDEX] = NULL;
    au2NlriSize[BGP4_VPN4_UNI_INDEX] = 0;
#endif

#ifdef VPLSADS_WANTED
    /* ADS-VPLS Related Processing */
    pau1NlriBuf[BGP4_L2VPN_VPLS_INDEX] = NULL;
    au2NlriSize[BGP4_L2VPN_VPLS_INDEX] = 0;
#endif
#ifdef EVPN_WANTED
    pau1NlriBuf[BGP4_L2VPN_EVPN_INDEX] = NULL;
    au2NlriSize[BGP4_L2VPN_EVPN_INDEX] = 0;
#endif
    /* Start processing the withdrawn routes list */
    if ((pSelectedAdvtWithdrawnRtList != NULL) &&
        (TMO_SLL_Count (pSelectedAdvtWithdrawnRtList)))
    {
        /* For updating the message to be send buffer */
        pu1UpdateMessageToSend = *ppu1MsgBuf;

        TMO_SLL_Scan (pSelectedAdvtWithdrawnRtList, pRtLink, tLinkNode *)
        {
            pWithdrawnRouteToBeFilled = pRtLink->pRouteProfile;
            /*Delete the withdrawn route fron the RBTree */
            Bgp4DelAdvtRoute (pPeerEntry, pWithdrawnRouteToBeFilled);
            BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                           BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                           "\tRoute %s Mask %s advertised as unfeasible to "
                           "PEER %s\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                            (pWithdrawnRouteToBeFilled),
                                            BGP4_RT_AFI_INFO
                                            (pWithdrawnRouteToBeFilled)),
                           Bgp4PrintIpMask ((UINT1) BGP4_RT_PREFIXLEN
                                            (pWithdrawnRouteToBeFilled),
                                            BGP4_RT_AFI_INFO
                                            (pWithdrawnRouteToBeFilled)),
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerEntry))));
            Bgp4GetPeerAdvtAfiSafi (pWithdrawnRouteToBeFilled, pPeerEntry,
                                    &u4AsafiMask);
            switch (u4AsafiMask)
            {
                case CAP_MP_IPV4_UNICAST:
                    u4NlriIndex = BGP4_IPV4_UNI_INDEX;
                    u2TotalPALen = au2NlriSize[BGP4_IPV4_UNI_INDEX];
                    break;
#ifdef BGP4_IPV6_WANTED
                case CAP_MP_IPV6_UNICAST:
                    u4NlriIndex = BGP4_IPV6_UNI_INDEX;
                    u2TotalPALen = BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN;
                    u2TotalPALen +=
                        ((au2NlriSize[BGP4_IPV6_UNI_INDEX] >
                          BGP4_ONE_BYTE_MAX_VAL) ?
                         BGP4_EXTENDED_ATTR_LEN_SIZE :
                         BGP4_NORMAL_ATTR_LEN_SIZE);
                    u2TotalPALen += BGP4_MPE_ADDRESS_FAMILY_LEN +
                        BGP4_MPE_SUB_ADDRESS_FAMILY_LEN +
                        au2NlriSize[BGP4_IPV6_UNI_INDEX];
                    break;
#endif
#ifdef L3VPN
                    /* Carrying Label Information - RFC 3107 */
                case CAP_MP_LABELLED_IPV4:
                    u4NlriIndex = BGP4_IPV4_LBLD_INDEX;
                    u2TotalPALen = BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN;
                    u2TotalPALen = (UINT2) (u2TotalPALen +
                                            ((au2NlriSize[BGP4_IPV4_LBLD_INDEX]
                                              >
                                              BGP4_ONE_BYTE_MAX_VAL) ?
                                             BGP4_EXTENDED_ATTR_LEN_SIZE :
                                             BGP4_NORMAL_ATTR_LEN_SIZE));
                    u2TotalPALen =
                        (UINT2) (u2TotalPALen +
                                 (BGP4_MPE_ADDRESS_FAMILY_LEN +
                                  BGP4_MPE_SUB_ADDRESS_FAMILY_LEN +
                                  au2NlriSize[BGP4_IPV4_LBLD_INDEX]));
                    break;

                case CAP_MP_VPN4_UNICAST:
                    u4NlriIndex = BGP4_VPN4_UNI_INDEX;
                    u2TotalPALen = BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN;
                    u2TotalPALen = (UINT2) (u2TotalPALen +
                                            ((au2NlriSize[BGP4_VPN4_UNI_INDEX] >
                                              BGP4_ONE_BYTE_MAX_VAL) ?
                                             BGP4_EXTENDED_ATTR_LEN_SIZE :
                                             BGP4_NORMAL_ATTR_LEN_SIZE));
                    u2TotalPALen =
                        (UINT2) (u2TotalPALen +
                                 (BGP4_MPE_ADDRESS_FAMILY_LEN +
                                  BGP4_MPE_SUB_ADDRESS_FAMILY_LEN +
                                  au2NlriSize[BGP4_VPN4_UNI_INDEX]));
                    break;
#endif
#ifdef EVPN_WANTED
                case CAP_MP_L2VPN_EVPN:
                    u4NlriIndex = BGP4_L2VPN_EVPN_INDEX;
                    u2TotalPALen = BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN;
                    u2TotalPALen = (UINT2) (u2TotalPALen +
                                            ((au2NlriSize[BGP4_L2VPN_EVPN_INDEX]
                                              >
                                              BGP4_ONE_BYTE_MAX_VAL) ?
                                             BGP4_EXTENDED_ATTR_LEN_SIZE :
                                             BGP4_NORMAL_ATTR_LEN_SIZE));
                    u2TotalPALen =
                        (UINT2) (u2TotalPALen +
                                 (BGP4_MPE_ADDRESS_FAMILY_LEN +
                                  BGP4_MPE_SUB_ADDRESS_FAMILY_LEN +
                                  au2NlriSize[BGP4_L2VPN_EVPN_INDEX]));
                    break;

#endif
#ifdef VPLSADS_WANTED
                    /*ADS-VPLS Related Processing */
                case CAP_MP_L2VPN_VPLS:
                    u4NlriIndex = BGP4_L2VPN_VPLS_INDEX;
                    u2TotalPALen = BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN;
                    u2TotalPALen += ((au2NlriSize[BGP4_L2VPN_VPLS_INDEX] >
                                      BGP4_ONE_BYTE_MAX_VAL) ?
                                     BGP4_EXTENDED_ATTR_LEN_SIZE :
                                     BGP4_NORMAL_ATTR_LEN_SIZE);
                    u2TotalPALen += BGP4_MPE_ADDRESS_FAMILY_LEN +
                        BGP4_MPE_SUB_ADDRESS_FAMILY_LEN +
                        au2NlriSize[BGP4_L2VPN_VPLS_INDEX];
                    break;

#endif
                default:
                    /* Route Family not supported. Skip this route. */
                    continue;
            }

            if (pau1NlriBuf[u4NlriIndex] == NULL)
            {
                /* Allocate Buffer for storing the UNFEASIBLE Route. */
                pau1NlriBuf[u4NlriIndex] =
                    MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);
                if (pau1NlriBuf[u4NlriIndex] == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG,
                              BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                              BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                              "\tMemory Allocation to form UnFeasible Route "
                              "FAILED!!!\n");
                    gu4BgpDebugCnt[MAX_BGP_BUF_NODES_SIZING_ID]++;
                    if (pau1NlriBuf[BGP4_IPV4_UNI_INDEX] != NULL)
                    {
                        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                            pau1NlriBuf[BGP4_IPV4_UNI_INDEX]);
                    }
#ifdef BGP4_IPV6_WANTED
                    if (pau1NlriBuf[BGP4_IPV6_UNI_INDEX] != NULL)
                    {
                        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                            pau1NlriBuf[BGP4_IPV6_UNI_INDEX]);
                    }
#endif
#ifdef VPLSADS_WANTED
                    if (pau1NlriBuf[BGP4_L2VPN_VPLS_INDEX] != NULL)
                    {
                        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                            pau1NlriBuf[BGP4_L2VPN_VPLS_INDEX]);
                    }
#endif
#ifdef EVPN_WANTED
                    if (pau1NlriBuf[BGP4_L2VPN_EVPN_INDEX] != NULL)
                    {
                        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                            pau1NlriBuf[BGP4_L2VPN_EVPN_INDEX]);
                    }
#endif
#ifdef L3VPN
                    if (pau1NlriBuf[BGP4_IPV4_LBLD_INDEX] != NULL)
                    {
                        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                            pau1NlriBuf[BGP4_IPV4_LBLD_INDEX]);
                    }
                    if (pau1NlriBuf[BGP4_VPN4_UNI_INDEX] != NULL)
                    {
                        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                            pau1NlriBuf[BGP4_VPN4_UNI_INDEX]);
                    }
#endif
                    MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                        pu1UpdateMessageToSend);
                    return BGP4_FAILURE;
                }
            }

            if ((BGP4_RT_PROTOCOL (pWithdrawnRouteToBeFilled) == BGP_ID) &&
                (BGP4_GET_PEER_TYPE
                 (BGP4_PEER_CXT_ID (pPeerEntry),
                  BGP4_RT_PEER_ENTRY (pWithdrawnRouteToBeFilled)) ==
                 BGP4_EXTERNAL_PEER)
                &&
                (BGP4_CONFED_PEER_STATUS
                 (BGP4_RT_PEER_ENTRY (pWithdrawnRouteToBeFilled)) ==
                 BGP4_FALSE))
            {
                /* Add the withdrawn route to the ADVT_WITHLIST only if it is
                 * learnt from external Peer. */
                Bgp4DshAddRouteToPeerAdvList (pPeerEntry,
                                              BGP4_PEER_ADVT_WITH_LIST,
                                              pWithdrawnRouteToBeFilled, 0,
                                              Bgp4ElapTime ());
            }
            u1PrefixLenBytes =
                (UINT1) Bgp4GetBytesForPrefix (pWithdrawnRouteToBeFilled,
                                               pPeerEntry);
            if (u2BufLen <
                (u2TotalPALen + u1PrefixLenBytes + RFL_BYTES_FOR_PREFIX_LEN))
            {
                /* Buffer is full, so try to send the UPDATE message
                 * till now formed by filling the rest of the information
                 * needed.
                 */
                u2RemBufLen = (UINT2) (u2BufLen - u2TotalPALen);

                switch (u4AsafiMask)
                {
                    case CAP_MP_IPV4_UNICAST:
                        u2UnfeasRtLen = au2NlriSize[BGP4_IPV4_UNI_INDEX];
                        u2TotalPALen = 0;
                        MEMCPY (pu1UpdateMessageToSend +
                                BGP4_MSG_COMMON_HDR_LEN +
                                BGP4_WITHDRAWN_LEN_FIELD_SIZE,
                                pau1NlriBuf[BGP4_IPV4_UNI_INDEX],
                                u2UnfeasRtLen);
                        MEMSET (pau1NlriBuf[BGP4_IPV4_UNI_INDEX], 0,
                                BGP4_MAX_MSG_LEN);
                        au2NlriSize[BGP4_IPV4_UNI_INDEX] = 0;
                        break;

#ifdef VPLSADS_WANTED
                        /*ADS-VPLS Related Processing */
                    case CAP_MP_L2VPN_VPLS:
                        u2UnfeasRtLen = 0;
                        u2TotalPALen =
                            Bgp4AhAppendMpeAttrBuffToUpdMsg
                            (pu1UpdateMessageToSend + BGP4_MSG_COMMON_HDR_LEN +
                             BGP4_WITHDRAWN_LEN_FIELD_SIZE +
                             BGP4_ATTR_LEN_FIELD_SIZE,
                             pau1NlriBuf[BGP4_L2VPN_VPLS_INDEX],
                             au2NlriSize[BGP4_L2VPN_VPLS_INDEX],
                             BGP4_ATTR_MP_UNREACH_NLRI,
                             BGP4_RT_AFI_INFO (pWithdrawnRouteToBeFilled),
                             (UINT1)
                             BGP4_RT_SAFI_INFO (pWithdrawnRouteToBeFilled));
                        MEMSET (pau1NlriBuf[BGP4_L2VPN_VPLS_INDEX], 0,
                                BGP4_MAX_MSG_LEN);
                        au2NlriSize[BGP4_L2VPN_VPLS_INDEX] = 0;
                        break;
#endif
#ifdef BGP4_IPV6_WANTED
                    case CAP_MP_IPV6_UNICAST:
                        u2UnfeasRtLen = 0;
                        u2TotalPALen =
                            Bgp4AhAppendMpeAttrBuffToUpdMsg
                            (pu1UpdateMessageToSend + BGP4_MSG_COMMON_HDR_LEN +
                             BGP4_WITHDRAWN_LEN_FIELD_SIZE +
                             BGP4_ATTR_LEN_FIELD_SIZE,
                             pau1NlriBuf[BGP4_IPV6_UNI_INDEX],
                             au2NlriSize[BGP4_IPV6_UNI_INDEX],
                             BGP4_ATTR_MP_UNREACH_NLRI,
                             BGP4_RT_AFI_INFO (pWithdrawnRouteToBeFilled),
                             (UINT1)
                             BGP4_RT_SAFI_INFO (pWithdrawnRouteToBeFilled));
                        MEMSET (pau1NlriBuf[BGP4_IPV6_UNI_INDEX], 0,
                                BGP4_MAX_MSG_LEN);
                        au2NlriSize[BGP4_IPV6_UNI_INDEX] = 0;
                        break;
#endif
#ifdef L3VPN
                    case CAP_MP_LABELLED_IPV4:
                        u2UnfeasRtLen = 0;
                        if (pu1UpdateMessageToSend != NULL)
                        {
                            u2TotalPALen =
                                Bgp4AhAppendMpeAttrBuffToUpdMsg
                                (pu1UpdateMessageToSend +
                                 BGP4_MSG_COMMON_HDR_LEN +
                                 BGP4_WITHDRAWN_LEN_FIELD_SIZE +
                                 BGP4_ATTR_LEN_FIELD_SIZE,
                                 pau1NlriBuf[BGP4_IPV4_LBLD_INDEX],
                                 au2NlriSize[BGP4_IPV4_LBLD_INDEX],
                                 BGP4_ATTR_MP_UNREACH_NLRI, BGP4_INET_AFI_IPV4,
                                 BGP4_INET_SAFI_LABEL);
                        }
                        MEMSET (pau1NlriBuf[BGP4_IPV4_LBLD_INDEX], 0,
                                BGP4_MAX_MSG_LEN);
                        au2NlriSize[BGP4_IPV4_LBLD_INDEX] = 0;
                        break;

                    case CAP_MP_VPN4_UNICAST:
                        u2UnfeasRtLen = 0;
                        u2TotalPALen =
                            Bgp4AhAppendMpeAttrBuffToUpdMsg
                            (pu1UpdateMessageToSend + BGP4_MSG_COMMON_HDR_LEN +
                             BGP4_WITHDRAWN_LEN_FIELD_SIZE +
                             BGP4_ATTR_LEN_FIELD_SIZE,
                             pau1NlriBuf[BGP4_VPN4_UNI_INDEX],
                             au2NlriSize[BGP4_VPN4_UNI_INDEX],
                             BGP4_ATTR_MP_UNREACH_NLRI, BGP4_INET_AFI_IPV4,
                             BGP4_INET_SAFI_VPNV4_UNICAST);
                        MEMSET (pau1NlriBuf[BGP4_VPN4_UNI_INDEX], 0,
                                BGP4_MAX_MSG_LEN);
                        au2NlriSize[BGP4_VPN4_UNI_INDEX] = 0;
                        break;
#endif
#ifdef EVPN_WANTED
                    case CAP_MP_L2VPN_EVPN:
                        u2UnfeasRtLen = 0;
                        u2TotalPALen =
                            Bgp4AhAppendMpeAttrBuffToUpdMsg
                            (pu1UpdateMessageToSend + BGP4_MSG_COMMON_HDR_LEN +
                             BGP4_WITHDRAWN_LEN_FIELD_SIZE +
                             BGP4_ATTR_LEN_FIELD_SIZE,
                             pau1NlriBuf[BGP4_L2VPN_EVPN_INDEX],
                             au2NlriSize[BGP4_L2VPN_EVPN_INDEX],
                             BGP4_ATTR_MP_UNREACH_NLRI, BGP4_INET_AFI_L2VPN,
                             BGP4_INET_SAFI_EVPN);
                        MEMSET (pau1NlriBuf[BGP4_L2VPN_EVPN_INDEX], 0,
                                BGP4_MAX_MSG_LEN);
                        au2NlriSize[BGP4_L2VPN_EVPN_INDEX] = 0;
                        break;
#endif
                    default:
                        break;
                }

                i4UpdateMessageSentStatus =
                    Bgp4AhSendConstructUpdMsg (pPeerEntry,
                                               &pu1UpdateMessageToSend,
                                               &u2UnfeasRtLen, &u2TotalPALen,
                                               &u2RemBufLen,
                                               BGP4_SEND_CONSTRUCT_UPD);
                if (i4UpdateMessageSentStatus == BGP4_FAILURE)
                {
                    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                                   BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC |
                                   BGP4_ALL_FAILURE_TRC | BGP4_TXQ_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : Failure in sending update message"
                                   "to transmission queue.\n",
                                   Bgp4PrintIpAddr
                                   (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry))));
                    if (pau1NlriBuf[BGP4_IPV4_UNI_INDEX] != NULL)
                    {
                        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                            pau1NlriBuf[BGP4_IPV4_UNI_INDEX]);
                    }
#ifdef BGP4_IPV6_WANTED
                    if (pau1NlriBuf[BGP4_IPV6_UNI_INDEX] != NULL)
                    {
                        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                            pau1NlriBuf[BGP4_IPV6_UNI_INDEX]);
                    }
#endif
#ifdef VPLSADS_WANTED
                    if (pau1NlriBuf[BGP4_L2VPN_VPLS_INDEX] != NULL)
                    {
                        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                            pau1NlriBuf[BGP4_L2VPN_VPLS_INDEX]);
                    }
#endif
#ifdef EVPN_WANTED
                    if (pau1NlriBuf[BGP4_L2VPN_EVPN_INDEX] != NULL)
                    {
                        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                            pau1NlriBuf[BGP4_L2VPN_EVPN_INDEX]);
                    }

#endif
#ifdef L3VPN
                    if (pau1NlriBuf[BGP4_IPV4_LBLD_INDEX] != NULL)
                    {
                        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                            pau1NlriBuf[BGP4_IPV4_LBLD_INDEX]);
                    }
                    if (pau1NlriBuf[BGP4_VPN4_UNI_INDEX] != NULL)
                    {
                        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                            pau1NlriBuf[BGP4_VPN4_UNI_INDEX]);
                    }
#endif
                    return BGP4_FAILURE;
                }

                u2UnfeasRtLen = 0;
                u2TotalPALen = 0;
                u2BufLen = u2RemBufLen;
                u2BufLen = (UINT2) (u2BufLen - (BGP4_MSG_COMMON_HDR_LEN +
                                                BGP4_WITHDRAWN_LEN_FIELD_SIZE +
                                                BGP4_ATTR_LEN_FIELD_SIZE));
                /* Updated the begining of processing buffer */
                *ppu1MsgBuf = pu1UpdateMessageToSend;
            }

            /* Increment withdraws sent counter here */
            i4RetVal = BGP4_SUCCESS;
            switch (u4AsafiMask)
            {
                case CAP_MP_IPV4_UNICAST:
                    au2NlriSize[u4NlriIndex] = (UINT2)
                        (au2NlriSize[u4NlriIndex] +
                         (Bgp4AttrFillPrefix (pau1NlriBuf[u4NlriIndex] +
                                              au2NlriSize[u4NlriIndex],
                                              pWithdrawnRouteToBeFilled)));
                    BGP4_PEER_IPV4_WITHDRAWS_SENT_CNT (pPeerEntry)++;
                    break;

#ifdef VPLSADS_WANTED
                case CAP_MP_L2VPN_VPLS:
                    au2NlriSize[u4NlriIndex] +=
                        Bgp4AttrFillVplsPrefix (pau1NlriBuf[u4NlriIndex] +
                                                au2NlriSize[u4NlriIndex],
                                                pWithdrawnRouteToBeFilled,
                                                pPeerEntry);
                    BGP4_PEER_VPLS_WITHDRAWS_SENT_CNT (pPeerEntry)++;
                    break;
#endif

#ifdef BGP4_IPV6_WANTED
                case CAP_MP_IPV6_UNICAST:
                    au2NlriSize[u4NlriIndex] +=
                        Bgp4AttrFillPrefix (pau1NlriBuf[u4NlriIndex] +
                                            au2NlriSize[u4NlriIndex],
                                            pWithdrawnRouteToBeFilled);
                    BGP4_PEER_IPV6_WITHDRAWS_SENT_CNT (pPeerEntry)++;
                    break;

#endif
#ifdef L3VPN
                    /* Carrying Label Informatin - RFC 3107 */
                case CAP_MP_LABELLED_IPV4:
                {
                    tBgp4AdvtPathAttr  *pAdvtPA = NULL;
                    i4RetVal =
                        Bgp4MpeFillIpLbldPrefix (pau1NlriBuf[u4NlriIndex] +
                                                 au2NlriSize[u4NlriIndex],
                                                 pWithdrawnRouteToBeFilled,
                                                 pAdvtPA, pPeerEntry);
                    if (i4RetVal == BGP4_FAILURE)
                    {
                        break;
                    }
                    au2NlriSize[u4NlriIndex] =
                        (UINT2) ((au2NlriSize[u4NlriIndex]) + i4RetVal);
                    BGP4_PEER_IPV4_LBLD_WITHDRAWS_SENT_CNT (pPeerEntry)++;
                    break;
                }
                case CAP_MP_VPN4_UNICAST:
                {
                    tBgp4AdvtPathAttr  *pAdvtPA = NULL;
                    i4RetVal =
                        Bgp4MpeFillIpLbldPrefix (pau1NlriBuf[u4NlriIndex] +
                                                 au2NlriSize[u4NlriIndex],
                                                 pWithdrawnRouteToBeFilled,
                                                 pAdvtPA, pPeerEntry);
                    if (i4RetVal == BGP4_FAILURE)
                    {
                        break;
                    }
                    au2NlriSize[u4NlriIndex] =
                        (UINT2) (i4RetVal + au2NlriSize[u4NlriIndex]);
                    BGP4_PEER_VPN4_WITHDRAWS_SENT_CNT (pPeerEntry)++;
                    break;
                }
#endif
#ifdef EVPN_WANTED
                case CAP_MP_L2VPN_EVPN:
                {
                    i4RetVal =
                        Bgp4MpeFillEvpnPrefix (pau1NlriBuf[u4NlriIndex] +
                                               au2NlriSize[u4NlriIndex],
                                               pWithdrawnRouteToBeFilled,
                                               pPeerEntry);
                    if (i4RetVal == BGP4_FAILURE)
                    {
                        break;
                    }
                    au2NlriSize[u4NlriIndex] =
                        (UINT2) (i4RetVal + au2NlriSize[u4NlriIndex]);
                    break;
                }

#endif
                default:
                    break;
            }
#ifdef L3VPN
            if (i4RetVal == BGP4_FAILURE)
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                               BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC |
                               BGP4_ALL_FAILURE_TRC | BGP4_TXQ_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : Failure in filling Prefix",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerEntry),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerEntry))));
                if (pau1NlriBuf[BGP4_IPV4_UNI_INDEX] != NULL)
                {
                    MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                        pau1NlriBuf[BGP4_IPV4_UNI_INDEX]);
                }
#ifdef BGP4_IPV6_WANTED
                if (pau1NlriBuf[BGP4_IPV6_UNI_INDEX] != NULL)
                {
                    MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                        pau1NlriBuf[BGP4_IPV6_UNI_INDEX]);
                }
#endif
                if (pau1NlriBuf[BGP4_IPV4_LBLD_INDEX] != NULL)
                {
                    MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                        pau1NlriBuf[BGP4_IPV4_LBLD_INDEX]);
                }
                if (pau1NlriBuf[BGP4_VPN4_UNI_INDEX] != NULL)
                {
                    MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                        pau1NlriBuf[BGP4_VPN4_UNI_INDEX]);
                }
                return BGP4_FAILURE;
            }
#endif
        }
    }

    u2TotalPALen = 0;
    u2UnfeasRtLen = 0;
    u2RemBufLen = u2BufLen;

    /* Now fill the UNFEASIBLE-NLRI and advertise the update message. */
    u4NlriIndex = BGP4_MPE_ADDR_FAMILY_MAX_INDEX - 1;
    for (;;)
    {
        if (u4NlriIndex == BGP4_IPV4_UNI_INDEX)
        {
            u2TotalPALen = au2NlriSize[BGP4_IPV4_UNI_INDEX];
            if (pau1NlriBuf[BGP4_IPV4_UNI_INDEX] != NULL)
            {
                if (pu1UpdateMessageToSend != NULL)
                {
                    MEMCPY (pu1UpdateMessageToSend +
                            BGP4_MSG_COMMON_HDR_LEN +
                            BGP4_WITHDRAWN_LEN_FIELD_SIZE,
                            pau1NlriBuf[BGP4_IPV4_UNI_INDEX],
                            au2NlriSize[BGP4_IPV4_UNI_INDEX]);
                }
                u2RemBufLen = (UINT2) (u2RemBufLen - u2TotalPALen);
                u2TotalPALen = 0;
                u2UnfeasRtLen = au2NlriSize[BGP4_IPV4_UNI_INDEX];
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                    pau1NlriBuf[BGP4_IPV4_UNI_INDEX]);
                pau1NlriBuf[BGP4_IPV4_UNI_INDEX] = NULL;
                au2NlriSize[BGP4_IPV4_UNI_INDEX] = 0;
            }
            /* Only IPV4_UNICAST Unfeasible routes are filled in the buffer.
             * Dont sent the packet immediately. Check if any feasible route
             * also needs to be sent.
             */
            break;
        }
#ifdef BGP4_IPV6_WANTED
        else if (pau1NlriBuf[BGP4_IPV6_UNI_INDEX] != NULL)
        {
            u2TotalPALen =
                Bgp4AhAppendMpeAttrBuffToUpdMsg (pu1UpdateMessageToSend +
                                                 BGP4_MSG_COMMON_HDR_LEN +
                                                 BGP4_WITHDRAWN_LEN_FIELD_SIZE +
                                                 BGP4_ATTR_LEN_FIELD_SIZE,
                                                 pau1NlriBuf
                                                 [BGP4_IPV6_UNI_INDEX],
                                                 au2NlriSize
                                                 [BGP4_IPV6_UNI_INDEX],
                                                 BGP4_ATTR_MP_UNREACH_NLRI,
                                                 BGP4_INET_AFI_IPV6,
                                                 BGP4_INET_SAFI_UNICAST);
            u2RemBufLen -= u2TotalPALen;
            u2UnfeasRtLen = 0;
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                pau1NlriBuf[BGP4_IPV6_UNI_INDEX]);
            pau1NlriBuf[BGP4_IPV6_UNI_INDEX] = NULL;
            au2NlriSize[BGP4_IPV6_UNI_INDEX] = 0;
        }
#endif
#ifdef VPLSADS_WANTED
        else if (pau1NlriBuf[BGP4_L2VPN_VPLS_INDEX] != NULL)
        {
            u2TotalPALen =
                Bgp4AhAppendMpeAttrBuffToUpdMsg (pu1UpdateMessageToSend +
                                                 BGP4_MSG_COMMON_HDR_LEN +
                                                 BGP4_WITHDRAWN_LEN_FIELD_SIZE +
                                                 BGP4_ATTR_LEN_FIELD_SIZE,
                                                 pau1NlriBuf
                                                 [BGP4_L2VPN_VPLS_INDEX],
                                                 au2NlriSize
                                                 [BGP4_L2VPN_VPLS_INDEX],
                                                 BGP4_ATTR_MP_UNREACH_NLRI,
                                                 BGP4_INET_AFI_L2VPN,
                                                 BGP4_INET_SAFI_VPLS);
            u2RemBufLen -= u2TotalPALen;
            u2UnfeasRtLen = 0;
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                pau1NlriBuf[BGP4_L2VPN_VPLS_INDEX]);
            pau1NlriBuf[BGP4_L2VPN_VPLS_INDEX] = NULL;
            au2NlriSize[BGP4_L2VPN_VPLS_INDEX] = 0;
        }
#endif
#ifdef EVPN_WANTED
        else if (pau1NlriBuf[BGP4_L2VPN_EVPN_INDEX] != NULL)
        {
            if (pu1UpdateMessageToSend != NULL)
            {
                u2TotalPALen =
                    Bgp4AhAppendMpeAttrBuffToUpdMsg (pu1UpdateMessageToSend +
                                                     BGP4_MSG_COMMON_HDR_LEN +
                                                     BGP4_WITHDRAWN_LEN_FIELD_SIZE
                                                     + BGP4_ATTR_LEN_FIELD_SIZE,
                                                     pau1NlriBuf
                                                     [BGP4_L2VPN_EVPN_INDEX],
                                                     au2NlriSize
                                                     [BGP4_L2VPN_EVPN_INDEX],
                                                     BGP4_ATTR_MP_UNREACH_NLRI,
                                                     BGP4_INET_AFI_L2VPN,
                                                     BGP4_INET_SAFI_EVPN);
                u2RemBufLen -= u2TotalPALen;
                u2UnfeasRtLen = 0;
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                    pau1NlriBuf[BGP4_L2VPN_EVPN_INDEX]);
                pau1NlriBuf[BGP4_L2VPN_EVPN_INDEX] = NULL;
                au2NlriSize[BGP4_L2VPN_EVPN_INDEX] = 0;
            }
        }

#endif
#ifdef L3VPN
        /* Carrying Label Information - RFC 3107 */
        else if (pau1NlriBuf[BGP4_IPV4_LBLD_INDEX] != NULL)
        {
            if (pu1UpdateMessageToSend != NULL)
            {
                u2TotalPALen =
                    Bgp4AhAppendMpeAttrBuffToUpdMsg (pu1UpdateMessageToSend +
                                                     BGP4_MSG_COMMON_HDR_LEN +
                                                     BGP4_WITHDRAWN_LEN_FIELD_SIZE
                                                     + BGP4_ATTR_LEN_FIELD_SIZE,
                                                     pau1NlriBuf
                                                     [BGP4_IPV4_LBLD_INDEX],
                                                     au2NlriSize
                                                     [BGP4_IPV4_LBLD_INDEX],
                                                     BGP4_ATTR_MP_UNREACH_NLRI,
                                                     BGP4_INET_AFI_IPV4,
                                                     BGP4_INET_SAFI_LABEL);
            }
            u2RemBufLen = (UINT2) (u2RemBufLen - u2TotalPALen);
            u2UnfeasRtLen = 0;
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                pau1NlriBuf[BGP4_IPV4_LBLD_INDEX]);
            pau1NlriBuf[BGP4_IPV4_LBLD_INDEX] = NULL;
            au2NlriSize[BGP4_IPV4_LBLD_INDEX] = 0;
        }
        else if (pau1NlriBuf[BGP4_VPN4_UNI_INDEX] != NULL)
        {
            if (pu1UpdateMessageToSend != NULL)
            {
                u2TotalPALen =
                    Bgp4AhAppendMpeAttrBuffToUpdMsg (pu1UpdateMessageToSend +
                                                     BGP4_MSG_COMMON_HDR_LEN +
                                                     BGP4_WITHDRAWN_LEN_FIELD_SIZE
                                                     + BGP4_ATTR_LEN_FIELD_SIZE,
                                                     pau1NlriBuf
                                                     [BGP4_VPN4_UNI_INDEX],
                                                     au2NlriSize
                                                     [BGP4_VPN4_UNI_INDEX],
                                                     BGP4_ATTR_MP_UNREACH_NLRI,
                                                     BGP4_INET_AFI_IPV4,
                                                     BGP4_INET_SAFI_VPNV4_UNICAST);
            }
            u2RemBufLen = (UINT2) (u2RemBufLen - u2TotalPALen);
            u2UnfeasRtLen = 0;
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                pau1NlriBuf[BGP4_VPN4_UNI_INDEX]);
            pau1NlriBuf[BGP4_VPN4_UNI_INDEX] = NULL;
            au2NlriSize[BGP4_VPN4_UNI_INDEX] = 0;
        }
#endif
        else
        {
            /* This address family not supported. */
            u4NlriIndex--;
            continue;
        }

#if defined (BGP4_IPV6_WANTED) || defined (L3VPN ) || defined (EVPN_WANTED)
        /* For all Address Family other than IPV4_UNICAST advertise
         * immediately. */
        i4UpdateMessageSentStatus =
            Bgp4AhSendConstructUpdMsg (pPeerEntry,
                                       &pu1UpdateMessageToSend,
                                       &u2UnfeasRtLen, &u2TotalPALen,
                                       &u2RemBufLen, BGP4_SEND_CONSTRUCT_UPD);
        if (i4UpdateMessageSentStatus == BGP4_FAILURE)
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                           BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC |
                           BGP4_ALL_FAILURE_TRC | BGP4_TXQ_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Failure in sending update message"
                           "to transmission queue.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerEntry))));
            if (pau1NlriBuf[BGP4_IPV4_UNI_INDEX] != NULL)
            {
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                    pau1NlriBuf[BGP4_IPV4_UNI_INDEX]);
            }
#ifdef VPLSADS_WANTED
            if (pau1NlriBuf[BGP4_L2VPN_VPLS_INDEX] != NULL)
            {
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                    pau1NlriBuf[BGP4_L2VPN_VPLS_INDEX]);
            }
#endif
#ifdef EVPN_WANTED
            if (pau1NlriBuf[BGP4_L2VPN_EVPN_INDEX] != NULL)
            {
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                    pau1NlriBuf[BGP4_L2VPN_EVPN_INDEX]);
            }
#endif
#ifdef BGP4_IPV6_WANTED
            if (pau1NlriBuf[BGP4_IPV6_UNI_INDEX] != NULL)
            {
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                    pau1NlriBuf[BGP4_IPV6_UNI_INDEX]);
            }
#endif
#ifdef EVPN_WANTED
            if (pau1NlriBuf[BGP4_L2VPN_EVPN_INDEX] != NULL)
            {
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                    pau1NlriBuf[BGP4_L2VPN_EVPN_INDEX]);
            }
#endif
#ifdef L3VPN
            /* Carrying Label Information - RFC 3107 */
            if (pau1NlriBuf[BGP4_IPV4_LBLD_INDEX] != NULL)
            {
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                    pau1NlriBuf[BGP4_IPV4_LBLD_INDEX]);
            }
            if (pau1NlriBuf[BGP4_VPN4_UNI_INDEX] != NULL)
            {
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId,
                                    pau1NlriBuf[BGP4_VPN4_UNI_INDEX]);
            }
#endif
            return BGP4_FAILURE;
        }
        u2UnfeasRtLen = 0;
        u2TotalPALen = 0;
        u2RemBufLen = (UINT2) (u2RemBufLen - (BGP4_MSG_COMMON_HDR_LEN +
                                              BGP4_WITHDRAWN_LEN_FIELD_SIZE +
                                              BGP4_ATTR_LEN_FIELD_SIZE));
        u2BufLen = u2RemBufLen;
        /* Updated the begining of processing buffer */
        *ppu1MsgBuf = pu1UpdateMessageToSend;
        u4NlriIndex--;
#endif
    }
    BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                   BGP4_TRC_FLAG, BGP4_TXQ_TRC |
                   BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                   "\tPEER %s : Withdrawn route list is successfully processed "
                   "and send to the peer queue.\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry))));
    *pu2UnfeasRtLen = u2UnfeasRtLen;
    *pu2BufLen = u2RemBufLen;
    return BGP4_SUCCESS;
}

/**************************************************************************
 Function Name   : Bgp4ProcessFeasibleRouteList
 Description     : Process feasible route list and send update message to
                   the peer.
 Input(s)        : pSelectedAdvtFeasibleRtList - pointer to the list of
                                                 feasible route. It is
                                                 expected all the routes
                                                 in this list falls under
                                                 the same AFI-SAFI.
                   pAdvtBgpInfo - Pointer to the Path Attribute info to be
                                  advertised.
                   u2Afi    -   Address Family to which these routes belong
                   u2Safi   -   Sub-Address Family to which these routes belong
                   pPeerEntry - pointer to the peer to which update message
                                to be sent.
                   pu1MsgBuf - pointer to the begining of update message 
                                            to be sent.
                   u2BufLen - available buffer length
                   u2UnfeasRtLen - the length of unfeasible routes
 Output(s)       : ppu1MsgBuf - pointer to the begining of update message 
                                            to be sent.
 Global Variables Referred :
 Global variables Modified :
 Exceptions or Operating System Error Handling :
 Use of Recursion :
 Returns          : BGP4_SUCCESS or BGP4_FAILURE
**************************************************************************/
INT4
Bgp4ProcessFeasibleRouteList (tBgp4PeerEntry * pPeerEntry,
                              tBgp4AdvtPathAttr * pAdvtPA,
                              UINT2 u2Afi,
                              UINT2 u2Safi,
                              UINT1 *pu1MsgBuf,
                              UINT2 u2BufLen, UINT2 u2UnfeasRtLen)
{
    tBgp4Info          *pAdvtBgpInfo = NULL;
    tBufNode           *pBufNode = NULL;
    tBufNode           *pNextBufNode = NULL;
    UINT1              *pu1UpdateMessageToSend = NULL;
    UINT1              *pu1UpdMsgOffset = NULL;
    UINT4               u4AsafiMask = 0;
    UINT4               u4NextHop = 0;
    INT4                i4UpdateMessageSentStatus = BGP4_FAILURE;
    INT4                i4SpeakerPeerCap;
    INT4                i4AggrLen;
    INT4                i4AggrAsLen;
    UINT2               u2BytesFilled = 0;
    UINT2               u2TotalPALen = 0;
    UINT2               u2RemBufLen = 0;
    UINT2               u2PathAttrSize = 0;
    UINT2               u2MpeReachOffset = 0;
    UINT2               u2MpeReachPALen = 0;
    UINT2               u2TotalNlriLen = 0;
    UINT2               u2NlriLen = 0;
    UINT1               u1IsExtLenBitSet = BGP4_FALSE;
    UINT1               u1As4Flag = BGP4_FALSE;

    pu1UpdateMessageToSend = pu1UpdMsgOffset = pu1MsgBuf;
    pAdvtBgpInfo = BGP4_ADVT_PA_BGP4INFO (pAdvtPA);
    pBufNode = (tBufNode *) TMO_SLL_First (BGP4_ADVT_PA_ROUTE_LIST (pAdvtPA));

    if (NULL == pBufNode)
    {
        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)), BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_MGMT_TRC | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "[ERROR ]: BGP4_ADVT_PA_ROUTE_LIST is NULL in "
                  "Bgp4ProcessFeasibleRouteList function \r\n");
        return BGP4_FAILURE;
    }
    pu1UpdMsgOffset += BGP4_MSG_COMMON_HDR_LEN +
        BGP4_WITHDRAWN_LEN_FIELD_SIZE +
        u2UnfeasRtLen + BGP4_ATTR_LEN_FIELD_SIZE;

    u4AsafiMask = ((UINT4) u2Afi << BGP4_TWO_BYTE_BITS) | u2Safi;
    if (u2UnfeasRtLen > 0)
    {
        /* Whenever there is withdrawn route message available to be
         * advertised, calculate the Buffer size required for advertised path
         * attribute. In other case we assume that since only the feasible
         * route is to be advertised, always we have enough buffer left in the
         * update message for storing the Path Attributes. */
        u2BytesFilled = (UINT2) (BGP4_ADVT_PA_SIZE (pAdvtPA) +
                                 pBufNode->u4MsgSize);
        if (u2BytesFilled > u2BufLen)
        {
            /* The update message contains withdrawn message. Advertise the
             * withdrawn message. */
            u2RemBufLen = u2BufLen;
            i4UpdateMessageSentStatus =
                Bgp4AhSendConstructUpdMsg (pPeerEntry,
                                           &pu1UpdateMessageToSend,
                                           &u2UnfeasRtLen, &u2TotalPALen,
                                           &u2RemBufLen,
                                           BGP4_SEND_CONSTRUCT_UPD);
            if (i4UpdateMessageSentStatus == BGP4_FAILURE)
            {
                return BGP4_FAILURE;
            }
            u2BufLen = u2RemBufLen;
            u2RemBufLen = 0;
            pu1MsgBuf = pu1UpdMsgOffset = pu1UpdateMessageToSend;
            pu1UpdMsgOffset += (BGP4_MSG_COMMON_HDR_LEN +
                                BGP4_WITHDRAWN_LEN_FIELD_SIZE +
                                BGP4_ATTR_LEN_FIELD_SIZE);
        }
        u2BytesFilled = 0;
    }

    for (;;)
    {
        /* Form the update Path Attribute information. */
        u2BytesFilled = 0;

        /* Attr 1 : ORIGIN */
        *pu1UpdMsgOffset = (UINT1) BGP4_TRANSITIVE_FLAG_MASK;
        pu1UpdMsgOffset++;
        *pu1UpdMsgOffset = (UINT1) BGP4_ATTR_ORIGIN;
        pu1UpdMsgOffset++;
        *pu1UpdMsgOffset = (UINT1) BGP4_ATTR_ORIGIN_LEN;
        pu1UpdMsgOffset++;
        *pu1UpdMsgOffset = BGP4_INFO_ORIGIN (pAdvtBgpInfo);
        pu1UpdMsgOffset++;
        u2BytesFilled =
            (UINT2) (u2BytesFilled +
                     (BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                      BGP4_NORMAL_ATTR_LEN_SIZE + BGP4_ATTR_ORIGIN_LEN));

        /* Attr 2 & 17: AS-PATH  & AS4-PATH */
        u2PathAttrSize = Bgp4AddPathAttributeToUpdate (pAdvtBgpInfo,
                                                       pPeerEntry,
                                                       pu1UpdMsgOffset);
        pu1UpdMsgOffset += u2PathAttrSize;
        u2BytesFilled = (UINT2) (u2BytesFilled + u2PathAttrSize);

        /* ATTR 3 : IPV4-UNICAST NEXT-HOP */
#ifdef L3VPN
        /* If the route is of IPv4 family and the peer is of IPv4 type
         * OR if the route is of VPNv4 family and the peer is CE peer,
         * then we need to send the prefix in normal NLRI field of 
         * update message, hence fill the nexthop
         */
        if (((u2Afi == BGP4_INET_AFI_IPV4) &&
             (u2Safi == BGP4_INET_SAFI_UNICAST) &&
             (BGP4_AFI_IN_ADDR_PREFIX_INFO
              (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)) ==
              BGP4_INET_AFI_IPV4)) ||
            ((u2Afi == BGP4_INET_AFI_IPV4) &&
             (u2Safi == BGP4_INET_SAFI_VPNV4_UNICAST) &&
             (BGP4_VPN4_PEER_ROLE (pPeerEntry) == BGP4_VPN4_CE_PEER)))
#else

        if ((u2Afi == BGP4_INET_AFI_IPV4) && (u2Safi == BGP4_INET_SAFI_UNICAST))
#endif
        {
            /* Fill the next-hop field. */
            *(pu1UpdMsgOffset) = (UINT1) BGP4_TRANSITIVE_FLAG_MASK;
            pu1UpdMsgOffset++;
            *(pu1UpdMsgOffset) = (UINT1) BGP4_ATTR_NEXT_HOP;
            pu1UpdMsgOffset++;
            *(pu1UpdMsgOffset) = (UINT1) BGP4_ATTR_NEXTHOP_LEN;
            pu1UpdMsgOffset++;
            PTR_FETCH_4 (u4NextHop, BGP4_INFO_NEXTHOP (pAdvtBgpInfo));
            PTR_ASSIGN4 (pu1UpdMsgOffset, u4NextHop);
            pu1UpdMsgOffset += BGP4_ATTR_NEXTHOP_LEN;
            u2BytesFilled =
                (UINT2) (u2BytesFilled +
                         (BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                          BGP4_AL_NEXTHOP_SIZE + BGP4_ATTR_NEXTHOP_LEN));
        }

        /* ATTR 4 : MED */
        if ((BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) & BGP4_ATTR_MED_MASK) ==
            BGP4_ATTR_MED_MASK)
        {
            *(pu1UpdMsgOffset) = (UINT1) BGP4_OPTIONAL_FLAG_MASK;
            pu1UpdMsgOffset++;
            *(pu1UpdMsgOffset) = (UINT1) BGP4_ATTR_MED;
            pu1UpdMsgOffset++;
            *(pu1UpdMsgOffset) = (UINT1) BGP4_ATTR_MED_LEN;
            pu1UpdMsgOffset++;
            PTR_ASSIGN4 (pu1UpdMsgOffset, BGP4_INFO_RCVD_MED (pAdvtBgpInfo));
            pu1UpdMsgOffset += BGP4_ATTR_MED_LEN;

            u2BytesFilled =
                (UINT2) (u2BytesFilled + BGP4_ATYPE_FLAGS_LEN +
                         BGP4_ATYPE_CODE_LEN + BGP4_NORMAL_ATTR_LEN_SIZE +
                         BGP4_ATTR_MED_LEN);
        }

        /* ATTR 5 : LOCAL-PREF */
        if ((BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) &
             BGP4_ATTR_LOCAL_PREF_MASK) == BGP4_ATTR_LOCAL_PREF_MASK)
        {
            *(pu1UpdMsgOffset) = (UINT1) BGP4_TRANSITIVE_FLAG_MASK;
            pu1UpdMsgOffset++;
            *(pu1UpdMsgOffset) = (UINT1) BGP4_ATTR_LOCAL_PREF;
            pu1UpdMsgOffset++;
            *(pu1UpdMsgOffset) = (UINT1) BGP4_ATTR_LOCAL_PREF_LEN;
            pu1UpdMsgOffset++;
            PTR_ASSIGN4 (pu1UpdMsgOffset,
                         BGP4_INFO_RCVD_LOCAL_PREF (pAdvtBgpInfo));
            pu1UpdMsgOffset += BGP4_ATTR_LOCAL_PREF_LEN;

            u2BytesFilled =
                (UINT2) (u2BytesFilled +
                         (BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                          BGP4_NORMAL_ATTR_LEN_SIZE +
                          BGP4_ATTR_LOCAL_PREF_LEN));
        }

        /* ATTR 6 : ATOMIC-AGGREGATOR */
        if ((BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) &
             BGP4_ATTR_ATOMIC_AGGR_MASK) == BGP4_ATTR_ATOMIC_AGGR_MASK)
        {
            *(pu1UpdMsgOffset) = (UINT1) BGP4_TRANSITIVE_FLAG_MASK;
            pu1UpdMsgOffset++;
            *(pu1UpdMsgOffset) = (UINT1) BGP4_ATTR_ATOMIC_AGGR;
            pu1UpdMsgOffset++;
            *(pu1UpdMsgOffset) = (UINT1) BGP4_ATTR_ATOMIC_AGGR_LEN;
            pu1UpdMsgOffset++;
            u2BytesFilled =
                (UINT2) (u2BytesFilled +
                         (BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                          BGP4_NORMAL_ATTR_LEN_SIZE));
        }

        /* ATTR 7 : AGGREGATOR */
        if ((BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) & BGP4_ATTR_AGGREGATOR_MASK)
            == BGP4_ATTR_AGGREGATOR_MASK)
        {
            /* When the Speaker-Peer is,
             * NEW-NEW - Only AS_AGGREGATOR ===> AS is a 4 byte ASN
             * All other cases,
             *                AS_AGGREGATOR ===> AS is a 2 byte ASN
             *                AS4_AGGREGATOR ==> AS is a 4 byte ASN */
            i4SpeakerPeerCap =
                Bgp4GetSpeakerPeer4ByteAsnCapability (pPeerEntry);
            if (i4SpeakerPeerCap == BGP4_4BYTE_ASN_SPEAKER_AND_PEER)
            {
                i4AggrLen = BGP4_ATTR_AGGREGATOR_FOUR_LEN;
                i4AggrAsLen = BGP4_ATTR_AGGR_AS4LEN;
            }
            else
            {
                i4AggrLen = BGP4_ATTR_AGGREGATOR_LEN;
                i4AggrAsLen = BGP4_ATTR_AGGR_ASLEN;
            }

            *(pu1UpdMsgOffset) = (UINT1) (BGP4_TRANSITIVE_FLAG_MASK |
                                          BGP4_OPTIONAL_FLAG_MASK);
            if ((BGP4_INFO_AGGREGATOR_FLAG (pAdvtBgpInfo) &
                 BGP4_PARTIAL_FLAG_MASK) == BGP4_PARTIAL_FLAG_MASK)
            {
                *(pu1UpdMsgOffset) |= (UINT1) BGP4_PARTIAL_FLAG_MASK;
            }
            pu1UpdMsgOffset++;
            *(pu1UpdMsgOffset) = (UINT1) BGP4_ATTR_AGGREGATOR;
            pu1UpdMsgOffset++;
            *(pu1UpdMsgOffset) = (UINT1) i4AggrLen;
            pu1UpdMsgOffset++;
            if (i4AggrAsLen == BGP4_ATTR_AGGR_AS4LEN)
            {
                PTR_ASSIGN4 (pu1UpdMsgOffset,
                             (BGP4_INFO_AGGREGATOR_AS (pAdvtBgpInfo)));
            }
            else
            {
                if (BGP4_INFO_AGGREGATOR_AS (pAdvtBgpInfo) >
                    BGP4_MAX_TWO_BYTE_AS)
                {
                    PTR_ASSIGN2 (pu1UpdMsgOffset, BGP4_AS_TRANS);
                    u1As4Flag = BGP4_TRUE;
                }
                else
                {
                    PTR_ASSIGN2 (pu1UpdMsgOffset,
                                 (UINT2) (BGP4_INFO_AGGREGATOR_AS
                                          (pAdvtBgpInfo)));
                }
            }
            pu1UpdMsgOffset += (UINT1) i4AggrAsLen;

            PTR_ASSIGN4 (pu1UpdMsgOffset,
                         (BGP4_INFO_AGGREGATOR_NODE (pAdvtBgpInfo)));
            pu1UpdMsgOffset += BGP4_ATTR_AGGR_BGPID_LEN;

            u2BytesFilled =
                (UINT2) (u2BytesFilled +
                         (BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                          BGP4_NORMAL_ATTR_LEN_SIZE + (UINT2) i4AggrLen));
        }

        /* ATTR 18: AS4_AGGREGATOR */
        if (u1As4Flag == BGP4_TRUE)
        {
            *(pu1UpdMsgOffset) = (UINT1) (BGP4_TRANSITIVE_FLAG_MASK |
                                          BGP4_OPTIONAL_FLAG_MASK);
            if ((BGP4_INFO_AGGREGATOR_FLAG (pAdvtBgpInfo) &
                 BGP4_PARTIAL_FLAG_MASK) == BGP4_PARTIAL_FLAG_MASK)
            {
                *(pu1UpdMsgOffset) |= (UINT1) BGP4_PARTIAL_FLAG_MASK;
            }
            pu1UpdMsgOffset++;
            *(pu1UpdMsgOffset) = (UINT1) BGP4_ATTR_AGGREGATOR_FOUR;
            pu1UpdMsgOffset++;

            *(pu1UpdMsgOffset) = (UINT1) BGP4_ATTR_AGGREGATOR_FOUR_LEN;
            pu1UpdMsgOffset++;
            PTR_ASSIGN4 (pu1UpdMsgOffset,
                         (BGP4_INFO_AGGREGATOR_AS (pAdvtBgpInfo)));
            pu1UpdMsgOffset += BGP4_ATTR_AGGR_AS4LEN;

            PTR_ASSIGN4 (pu1UpdMsgOffset,
                         (BGP4_INFO_AGGREGATOR_NODE (pAdvtBgpInfo)));
            pu1UpdMsgOffset += BGP4_ATTR_AGGR_BGPID_LEN;
            u2BytesFilled =
                (UINT2) (u2BytesFilled +
                         (BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                          BGP4_NORMAL_ATTR_LEN_SIZE +
                          BGP4_ATTR_AGGREGATOR_FOUR_LEN));
        }

        /* ATTR 8 : COMMUNITY */
        if ((BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) & BGP4_ATTR_COMM_MASK) ==
            BGP4_ATTR_COMM_MASK)
        {
            u2PathAttrSize = FillCalcCommInUpdMesg (pu1UpdMsgOffset,
                                                    pAdvtBgpInfo);
            pu1UpdMsgOffset += u2PathAttrSize;
            u2BytesFilled = (UINT2) (u2BytesFilled + u2PathAttrSize);
        }

        /* ATTR 9 : ORIGNATOR-ID. */
        if ((BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) & BGP4_ATTR_ORIG_ID_MASK)
            == BGP4_ATTR_ORIG_ID_MASK)
        {
            /* Fill the Originator id  Flag */
            *(pu1UpdMsgOffset) = BGP4_OPTIONAL_FLAG_MASK;
            pu1UpdMsgOffset += BGP4_ATYPE_FLAGS_LEN;

            /* Fill the orignator id Code */
            *(pu1UpdMsgOffset) = BGP4_ATTR_ORIG_ID;
            pu1UpdMsgOffset += BGP4_ATYPE_CODE_LEN;

            /* Fill the originator length */
            *(pu1UpdMsgOffset) = (UINT1) ORIGINATOR_ID_LENGTH;
            pu1UpdMsgOffset += BGP4_NORMAL_ATTR_LEN_SIZE;

            /* Fill originator Id. */
            PTR_ASSIGN4 (pu1UpdMsgOffset, BGP4_INFO_ORIG_ID (pAdvtBgpInfo));
            pu1UpdMsgOffset += ORIGINATOR_ID_LENGTH;
            u2BytesFilled =
                (UINT2) (u2BytesFilled +
                         (BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
                          BGP4_NORMAL_ATTR_LEN_SIZE + ORIGINATOR_ID_LENGTH));
        }

        /* ATTR 10 : CLUSTER-LIST */
        if ((BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) & BGP4_ATTR_CLUS_LIST_MASK)
            == BGP4_ATTR_CLUS_LIST_MASK)
        {
            u2PathAttrSize = (UINT2) (BGP4_INFO_CLUS_LIST_COUNT (pAdvtBgpInfo) *
                                      CLUSTER_ID_LENGTH);
            u1IsExtLenBitSet = (u2PathAttrSize > BGP4_ONE_BYTE_MAX_VAL) ?
                BGP4_TRUE : BGP4_FALSE;
            /* Fill the cluster-list  Flag */
            *(pu1UpdMsgOffset) = BGP4_OPTIONAL_FLAG_MASK;
            if (u1IsExtLenBitSet == BGP4_TRUE)
            {
                *(pu1UpdMsgOffset) |= BGP4_EXT_LEN_FLAG_MASK;
            }
            pu1UpdMsgOffset += BGP4_ATYPE_FLAGS_LEN;
            u2BytesFilled++;

            /* Fill the cluster-list Code */
            *pu1UpdMsgOffset = BGP4_ATTR_CLUS_LIST;
            pu1UpdMsgOffset += BGP4_ATYPE_CODE_LEN;
            u2BytesFilled++;

            /* Fill the cluster-list Len */
            if (u1IsExtLenBitSet == BGP4_FALSE)
            {
                *pu1UpdMsgOffset = (UINT1) u2PathAttrSize;
                pu1UpdMsgOffset++;
                u2BytesFilled++;
            }
            else
            {
                PTR_ASSIGN2 (pu1UpdMsgOffset, u2PathAttrSize);
                pu1UpdMsgOffset += BGP4_EXTENDED_ATTR_LEN_SIZE;
                u2BytesFilled =
                    (UINT2) (u2BytesFilled + BGP4_EXTENDED_ATTR_LEN_SIZE);
            }

            /* Fill Cluster-List Value. */
            MEMCPY (pu1UpdMsgOffset,
                    BGP4_INFO_CLUS_LIST_ATTR_VAL (pAdvtBgpInfo),
                    u2PathAttrSize);
            pu1UpdMsgOffset += u2PathAttrSize;
            u2BytesFilled = (UINT2) (u2BytesFilled + u2PathAttrSize);
        }

        /* MP_REACH and MP_UNREACH Attribute. MP_UNREACH attribute
         * advertisement are taken care by Bgp4ProcessWithdrawnRoutesList.
         * So ATTR 15 (MP_UNREACH) is not filled here. For MP-REACH
         * attribute, Update the info about AFI/SAFI, NEXT-HOP, SNPA 
         * and since all the routes needs to be packed together, just
         * add all the routes in the list to the update message.
         * But however still few more attributes are available to
         * be filled (ECOMM, unknown etc.). Calculate the length required
         * to fill these attributes and in the remaining Space fill the
         * MP_REACH NLRI information. If the buffer is full, before
         * all routes are updated, then advertise the current buffer
         * and form a new update. */

#if defined(L3VPN)                /* ||  defined (L2VPN_VPLS) */

        if (!(((u2Afi == BGP4_INET_AFI_IPV4) &&
               (u2Safi == BGP4_INET_SAFI_UNICAST) &&
               (BGP4_AFI_IN_ADDR_PREFIX_INFO
                (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)) ==
                BGP4_INET_AFI_IPV4)) ||
              ((u2Afi == BGP4_INET_AFI_IPV4) &&
               (u2Safi == BGP4_INET_SAFI_VPNV4_UNICAST) &&
               (BGP4_VPN4_PEER_ROLE (pPeerEntry) == BGP4_VPN4_CE_PEER))))

#else
        if (!((u2Afi == BGP4_INET_AFI_IPV4) &&
              (u2Safi == BGP4_INET_SAFI_UNICAST) &&
              (BGP4_AFI_IN_ADDR_PREFIX_INFO
               (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)) ==
               BGP4_INET_AFI_IPV4)))
#endif
        {
            /* Need to Send the route in MP_REACH NLRI Field. Fill
             * the MP_REACH attributes except for NLRI Field. For
             * MP_REACH attribute the extended Bit is always set. This
             * is to avoid, calculation overhead. */
            u2MpeReachOffset = u2BytesFilled;
            u2MpeReachPALen = 0;
            *(pu1UpdMsgOffset) = BGP4_OPTIONAL_FLAG_MASK |
                BGP4_EXT_LEN_FLAG_MASK;
            pu1UpdMsgOffset++;
            u2BytesFilled++;

            *(pu1UpdMsgOffset) = BGP4_ATTR_MP_REACH_NLRI;
            pu1UpdMsgOffset++;
            u2BytesFilled++;

            /* Leave space for updating the Length field later. */
            pu1UpdMsgOffset += BGP4_EXTENDED_ATTR_LEN_SIZE;
            u2BytesFilled =
                (UINT2) (u2BytesFilled + BGP4_EXTENDED_ATTR_LEN_SIZE);

            /* Fill the AFI/SAFI field. */
            PTR_ASSIGN2 (pu1UpdMsgOffset, u2Afi);
            pu1UpdMsgOffset += BGP4_MPE_ADDRESS_FAMILY_LEN;
            u2BytesFilled =
                (UINT2) (u2BytesFilled + BGP4_MPE_ADDRESS_FAMILY_LEN);
            u2MpeReachPALen =
                (UINT2) (u2MpeReachPALen + BGP4_MPE_ADDRESS_FAMILY_LEN);

            *(pu1UpdMsgOffset) = (UINT1) u2Safi;
            pu1UpdMsgOffset += BGP4_MPE_SUB_ADDRESS_FAMILY_LEN;
            u2BytesFilled =
                (UINT2) (u2BytesFilled + BGP4_MPE_SUB_ADDRESS_FAMILY_LEN);
            u2MpeReachPALen =
                (UINT2) (u2MpeReachPALen + BGP4_MPE_SUB_ADDRESS_FAMILY_LEN);

            /* Fill Next-Hop Field Length. */
            switch (u4AsafiMask)
            {
#ifdef VPLSADS_WANTED
                    /*for <afi,safi> = <l2vpn,vpls>, the next hop is IPv4 or IPv6 */
                case CAP_MP_L2VPN_VPLS:

                    if (BGP4_INFO_AFI (pAdvtBgpInfo) == BGP4_INET_AFI_IPV4)
                    {
                        *(pu1UpdMsgOffset) = BGP4_IPV4_PREFIX_LEN;
                        pu1UpdMsgOffset++;
                        u2BytesFilled++;
                        u2MpeReachPALen++;
                        PTR_FETCH_4 (u4NextHop,
                                     BGP4_INFO_NEXTHOP (pAdvtBgpInfo));
                        PTR_ASSIGN4 (pu1UpdMsgOffset, u4NextHop);
                        pu1UpdMsgOffset += BGP4_IPV4_PREFIX_LEN;
                        u2BytesFilled =
                            (UINT2) (u2BytesFilled + BGP4_IPV4_PREFIX_LEN);
                        u2MpeReachPALen =
                            (UINT2) (u2MpeReachPALen + BGP4_IPV4_PREFIX_LEN);
                    }
#ifdef BGP4_IPV6_WANTED
                    else
                    {
                        if (BGP4_INFO_LINK_LOCAL_PRESENT (pAdvtBgpInfo) ==
                            BGP4_TRUE)
                        {
                            *(pu1UpdMsgOffset) = BGP4_IPV6_PREFIX_LEN * 2;
                        }
                        else
                        {
                            *(pu1UpdMsgOffset) = BGP4_IPV6_PREFIX_LEN;
                        }
                        pu1UpdMsgOffset++;
                        u2BytesFilled++;
                        u2MpeReachPALen++;
                        MEMCPY (pu1UpdMsgOffset,
                                BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                                BGP4_IPV6_PREFIX_LEN);
                        pu1UpdMsgOffset += BGP4_IPV6_PREFIX_LEN;
                        u2BytesFilled += BGP4_IPV6_PREFIX_LEN;
                        u2MpeReachPALen += BGP4_IPV6_PREFIX_LEN;
                        if (BGP4_INFO_LINK_LOCAL_PRESENT (pAdvtBgpInfo) ==
                            BGP4_TRUE)
                        {
                            MEMCPY (pu1UpdMsgOffset,
                                    BGP4_INFO_LINK_LOCAL_ADDR (pAdvtBgpInfo),
                                    BGP4_IPV6_PREFIX_LEN);
                            pu1UpdMsgOffset += BGP4_IPV6_PREFIX_LEN;
                            u2BytesFilled += BGP4_IPV6_PREFIX_LEN;
                            u2MpeReachPALen += BGP4_IPV6_PREFIX_LEN;
                        }
                    }
#endif
                    break;
#endif
                case CAP_MP_IPV4_UNICAST:
                    *(pu1UpdMsgOffset) = BGP4_IPV4_PREFIX_LEN;
                    pu1UpdMsgOffset++;
                    u2BytesFilled++;
                    u2MpeReachPALen++;
                    PTR_FETCH_4 (u4NextHop, BGP4_INFO_NEXTHOP (pAdvtBgpInfo));
                    PTR_ASSIGN4 (pu1UpdMsgOffset, u4NextHop);
                    pu1UpdMsgOffset += BGP4_IPV4_PREFIX_LEN;
                    u2BytesFilled =
                        (UINT2) (u2BytesFilled + BGP4_IPV4_PREFIX_LEN);
                    u2MpeReachPALen =
                        (UINT2) (u2MpeReachPALen + BGP4_IPV4_PREFIX_LEN);
                    break;

#ifdef BGP4_IPV6_WANTED
                case CAP_MP_IPV6_UNICAST:
                    if (BGP4_INFO_LINK_LOCAL_PRESENT (pAdvtBgpInfo) ==
                        BGP4_TRUE)
                    {
                        *(pu1UpdMsgOffset) = BGP4_IPV6_PREFIX_LEN * 2;
                    }
                    else
                    {
                        *(pu1UpdMsgOffset) = BGP4_IPV6_PREFIX_LEN;
                    }
                    pu1UpdMsgOffset++;
                    u2BytesFilled++;
                    u2MpeReachPALen++;
                    MEMCPY (pu1UpdMsgOffset,
                            BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                            BGP4_IPV6_PREFIX_LEN);
                    pu1UpdMsgOffset += BGP4_IPV6_PREFIX_LEN;
                    u2BytesFilled += BGP4_IPV6_PREFIX_LEN;
                    u2MpeReachPALen += BGP4_IPV6_PREFIX_LEN;
                    if (BGP4_INFO_LINK_LOCAL_PRESENT (pAdvtBgpInfo) ==
                        BGP4_TRUE)
                    {
                        MEMCPY (pu1UpdMsgOffset,
                                BGP4_INFO_LINK_LOCAL_ADDR (pAdvtBgpInfo),
                                BGP4_IPV6_PREFIX_LEN);
                        pu1UpdMsgOffset += BGP4_IPV6_PREFIX_LEN;
                        u2BytesFilled += BGP4_IPV6_PREFIX_LEN;
                        u2MpeReachPALen += BGP4_IPV6_PREFIX_LEN;
                    }
                    break;
#endif
#ifdef L3VPN
                case CAP_MP_LABELLED_IPV4:
                {
                    *(pu1UpdMsgOffset) = BGP4_IPV4_PREFIX_LEN;
                    pu1UpdMsgOffset++;
                    u2BytesFilled++;
                    u2MpeReachPALen++;
                    /* Fill Nexthop value */
                    PTR_FETCH_4 (u4NextHop, BGP4_INFO_NEXTHOP (pAdvtBgpInfo));
                    PTR_ASSIGN4 (pu1UpdMsgOffset, u4NextHop);
                    pu1UpdMsgOffset += BGP4_IPV4_PREFIX_LEN;
                    u2BytesFilled =
                        (UINT2) (u2BytesFilled + BGP4_IPV4_PREFIX_LEN);
                    u2MpeReachPALen =
                        (UINT2) (u2MpeReachPALen + BGP4_IPV4_PREFIX_LEN);
                    break;
                }

                case CAP_MP_VPN4_UNICAST:
                {
                    const UINT1
                         
                         
                         
                         
                        au1InvalidRD[BGP4_VPN4_ROUTE_DISTING_SIZE] =
                        { 0x00, 0x00, 0x00, 0x00,
                        0x00, 0x00, 0x00, 0x00
                    };

                    *(pu1UpdMsgOffset) = BGP4_VPN4_PREFIX_LEN;
                    pu1UpdMsgOffset++;
                    u2BytesFilled++;
                    u2MpeReachPALen++;
                    /* Fill RD of zero value */
                    MEMCPY (pu1UpdMsgOffset, au1InvalidRD,
                            BGP4_VPN4_ROUTE_DISTING_SIZE);
                    pu1UpdMsgOffset += BGP4_VPN4_ROUTE_DISTING_SIZE;
                    u2BytesFilled =
                        (UINT2) (u2BytesFilled + BGP4_VPN4_ROUTE_DISTING_SIZE);
                    u2MpeReachPALen =
                        (UINT2) (u2MpeReachPALen +
                                 BGP4_VPN4_ROUTE_DISTING_SIZE);
                    PTR_FETCH_4 (u4NextHop, BGP4_INFO_NEXTHOP (pAdvtBgpInfo));
                    PTR_ASSIGN4 (pu1UpdMsgOffset, u4NextHop);
                    pu1UpdMsgOffset += BGP4_IPV4_PREFIX_LEN;
                    u2BytesFilled =
                        (UINT2) (u2BytesFilled + BGP4_IPV4_PREFIX_LEN);
                    u2MpeReachPALen =
                        (UINT2) (u2MpeReachPALen + BGP4_IPV4_PREFIX_LEN);
                    break;
                }
#endif
#ifdef EVPN_WANTED
                case CAP_MP_L2VPN_EVPN:
                {
                    if (BGP4_INFO_AFI (pAdvtBgpInfo) == BGP4_INET_AFI_IPV4)
                    {
                        *(pu1UpdMsgOffset) = BGP4_IPV4_PREFIX_LEN;
                        pu1UpdMsgOffset++;
                        u2BytesFilled++;
                        u2MpeReachPALen++;
                        PTR_FETCH_4 (u4NextHop,
                                     BGP4_INFO_NEXTHOP (pAdvtBgpInfo));
                        PTR_ASSIGN4 (pu1UpdMsgOffset, u4NextHop);
                        pu1UpdMsgOffset += BGP4_IPV4_PREFIX_LEN;
                        u2BytesFilled =
                            (UINT2) (u2BytesFilled + BGP4_IPV4_PREFIX_LEN);
                        u2MpeReachPALen =
                            (UINT2) (u2MpeReachPALen + BGP4_IPV4_PREFIX_LEN);
                    }
#ifdef BGP4_IPV6_WANTED
                    else
                    {
                        if (BGP4_INFO_LINK_LOCAL_PRESENT (pAdvtBgpInfo) ==
                            BGP4_TRUE)
                        {
                            *(pu1UpdMsgOffset) = BGP4_IPV6_PREFIX_LEN * 2;
                        }
                        else
                        {
                            *(pu1UpdMsgOffset) = BGP4_IPV6_PREFIX_LEN;
                        }
                        pu1UpdMsgOffset++;
                        u2BytesFilled++;
                        u2MpeReachPALen++;
                        MEMCPY (pu1UpdMsgOffset,
                                BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                                BGP4_IPV6_PREFIX_LEN);
                        pu1UpdMsgOffset += BGP4_IPV6_PREFIX_LEN;
                        u2BytesFilled += BGP4_IPV6_PREFIX_LEN;
                        u2MpeReachPALen += BGP4_IPV6_PREFIX_LEN;
                        if (BGP4_INFO_LINK_LOCAL_PRESENT (pAdvtBgpInfo) ==
                            BGP4_TRUE)
                        {
                            MEMCPY (pu1UpdMsgOffset,
                                    BGP4_INFO_LINK_LOCAL_ADDR (pAdvtBgpInfo),
                                    BGP4_IPV6_PREFIX_LEN);
                            pu1UpdMsgOffset += BGP4_IPV6_PREFIX_LEN;
                            u2BytesFilled += BGP4_IPV6_PREFIX_LEN;
                            u2MpeReachPALen += BGP4_IPV6_PREFIX_LEN;
                        }
                    }
#endif

                    break;
                }
#endif
                default:
                    break;
            }

            /* Fill SNPA Field. Currently this field is
             * zero. Update it when required. OR this can be reserved field for RFC 4760 */
            *(pu1UpdMsgOffset) = 0;
            pu1UpdMsgOffset++;
            u2BytesFilled++;
            u2MpeReachPALen++;

            /* Fill the MP_REACH_NLRI Information. Once the buffer can't
             * take any more NLRI information advertise the buffer and form 
             * new update message. */
            u2NlriLen = (UINT2) pBufNode->u4MsgSize;
            MEMCPY (pu1UpdMsgOffset, pBufNode->pu1Msg, pBufNode->u4MsgSize);
            /* u2TotalNlriLen variable is used only for IPV4_UNICAST, since
             * only there NLRI are send seperately. For other address-family
             * NLRI are send along with the path attributes. So update
             * the u2BytesFilled value. */
            u2TotalNlriLen = 0;
            pu1UpdMsgOffset += u2NlriLen;
            u2MpeReachPALen = (UINT2) (u2MpeReachPALen + u2NlriLen);
            u2BytesFilled = (UINT2) (u2BytesFilled + u2NlriLen);

            /* Update MPE Path Attribute Length. */
            PTR_ASSIGN2 ((pu1UpdateMessageToSend +
                          (u2MpeReachOffset + BGP4_MSG_COMMON_HDR_LEN +
                           BGP4_WITHDRAWN_LEN_FIELD_SIZE +
                           BGP4_ATTR_LEN_FIELD_SIZE + u2UnfeasRtLen +
                           BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN)),
                         u2MpeReachPALen);
        }

        /* ATTR : 16 - EXT-COMMUNITY */
        if ((BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) & BGP4_ATTR_ECOMM_MASK) ==
            BGP4_ATTR_ECOMM_MASK)
        {
            u2PathAttrSize = FillCalcExtCommInUpdMesg (pu1UpdMsgOffset,
                                                       pAdvtBgpInfo);
            u2BytesFilled = (UINT2) (u2BytesFilled + u2PathAttrSize);
            pu1UpdMsgOffset += u2PathAttrSize;
        }

        /* Unknown Attributes. */
        if ((BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) & BGP4_ATTR_UNKNOWN_MASK) ==
            BGP4_ATTR_UNKNOWN_MASK)
        {
            MEMCPY (pu1UpdMsgOffset, BGP4_INFO_UNKNOWN_ATTR (pAdvtBgpInfo),
                    BGP4_INFO_UNKNOWN_ATTR_LEN (pAdvtBgpInfo));
            u2BytesFilled =
                (UINT2) (u2BytesFilled +
                         BGP4_INFO_UNKNOWN_ATTR_LEN (pAdvtBgpInfo));
            pu1UpdMsgOffset += BGP4_INFO_UNKNOWN_ATTR_LEN (pAdvtBgpInfo);
        }

#ifdef L3VPN
        if (((u2Afi == BGP4_INET_AFI_IPV4) &&
             (u2Safi == BGP4_INET_SAFI_UNICAST) &&
             (BGP4_AFI_IN_ADDR_PREFIX_INFO
              (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)) ==
              BGP4_INET_AFI_IPV4)) ||
            ((u2Afi == BGP4_INET_AFI_IPV4) &&
             (u2Safi == BGP4_INET_SAFI_VPNV4_UNICAST) &&
             (BGP4_VPN4_PEER_ROLE (pPeerEntry) == BGP4_VPN4_CE_PEER)))

#else

        if (((u2Afi == BGP4_INET_AFI_IPV4) &&
             (u2Safi == BGP4_INET_SAFI_UNICAST) &&
             (BGP4_AFI_IN_ADDR_PREFIX_INFO
              (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)) == BGP4_INET_AFI_IPV4)))
#endif
        {
            /* Fill the NLRI Information still, there is enough buffer
             * available . Once the buffer can't take any more NLRI information
             * advertise the buffer and form new update message. */
            u2NlriLen = (UINT2) pBufNode->u4MsgSize;
            MEMCPY (pu1UpdMsgOffset, pBufNode->pu1Msg, pBufNode->u4MsgSize);
            u2TotalNlriLen = (UINT2) (u2TotalNlriLen + u2NlriLen);
            pu1UpdMsgOffset += u2NlriLen;
        }

        u2RemBufLen = (UINT2) (u2BufLen - (u2BytesFilled + u2TotalNlriLen));
        pNextBufNode =
            (tBufNode *) TMO_SLL_Next (BGP4_ADVT_PA_ROUTE_LIST (pAdvtPA),
                                       &pBufNode->TSNext);
        if (pNextBufNode == NULL)
        {
            /* No more message is present. Just send the update and return. */
            Bgp4AhSendConstructUpdMsg (pPeerEntry, &pu1UpdateMessageToSend,
                                       &u2UnfeasRtLen, &u2BytesFilled,
                                       &u2RemBufLen, BGP4_SEND_UPD);
            break;
        }
        else
        {
            /* More message is present. Just send the update and process
             * the remaining update message. */
            i4UpdateMessageSentStatus =
                Bgp4AhSendConstructUpdMsg (pPeerEntry,
                                           &pu1UpdateMessageToSend,
                                           &u2UnfeasRtLen,
                                           &u2BytesFilled,
                                           &u2RemBufLen,
                                           BGP4_SEND_CONSTRUCT_UPD);
        }
        if (i4UpdateMessageSentStatus == BGP4_FAILURE)
        {
            return BGP4_FAILURE;
        }
        pBufNode = pNextBufNode;
        pNextBufNode = NULL;
        /* Route has been advertised. Reset all the values used for
         * computation and start forming a new update message. */
        u2BufLen = u2RemBufLen;
        u2RemBufLen = 0;
        pu1MsgBuf = pu1UpdMsgOffset = pu1UpdateMessageToSend;
        pu1UpdMsgOffset += (BGP4_MSG_COMMON_HDR_LEN +
                            BGP4_WITHDRAWN_LEN_FIELD_SIZE +
                            BGP4_ATTR_LEN_FIELD_SIZE);
        u2BufLen = (UINT2) (u2BufLen - (BGP4_MSG_COMMON_HDR_LEN +
                                        BGP4_WITHDRAWN_LEN_FIELD_SIZE +
                                        BGP4_ATTR_LEN_FIELD_SIZE));
        u2BytesFilled = 0;
        u2MpeReachOffset = 0;
        u2MpeReachPALen = 0;
        u2TotalNlriLen = 0;
    }

    return BGP4_SUCCESS;
}

/**************************************************************************
 Function Name   : Bgp4IsCurrentDiffToPrevBgp4Attr
 Description     : Compare the BGP information in the route profile 
                   information pPrevInfo and pFeasibleRouteToBeFilled.
 Input(s)        : pFeasibleRouteToBeFilled - pointer to route profile 
                   from which comparison has to be done
                   pPrevInfo - pointer to route profile information 
                               to be compared
 Output(s)       : None
 Global Variables Referred : None
 Global variables Modified : None
 Exceptions or Operating System Error Handling : None
 Use of Recursion : None
 Returns          : BGP4_TRUE or BGP4_FALSE
**************************************************************************/
INT4
Bgp4IsCurrentDiffToPrevBgp4Attr (tRouteProfile * pFeasibleRouteToBeFilled,
                                 tRouteProfile * pPrevInfo)
{
    if (pPrevInfo == NULL)
    {
        return BGP4_FALSE;
    }
    if (pFeasibleRouteToBeFilled == NULL)
    {
        return BGP4_FALSE;
    }
    if (RFL_BGP_INFO_CMP (BGP4_RT_BGP_INFO (pPrevInfo),
                          BGP4_RT_BGP_INFO (pFeasibleRouteToBeFilled))
        != BGP4_TRUE)
    {
        return BGP4_FALSE;
    }
    else
    {
        return BGP4_TRUE;
    }
}

/**************************************************************************
 Function Name   : Bgp4AddPathAttributeToUpdate
 Description     : This function adds the Path Attribute to the Update
                   message.
 Input(s)        :
                   pAdvtBgpInfo - Pointer to the Path Attribute info to be
                                  advertised.
                   pPeerEntry - pointer to the peer to which update message
                                to be sent.
                   pu1UpdMsgOffset - pointer to the update message
                                     to be sent.
 Output(s)       : None
 Global Variables Referred : None
 Global variables Modified : None
 Exceptions or Operating System Error Handling : None
 Use of Recursion : None
 Returns          : u2BytesFilled - No. of bytes filled in UPDATE message
**************************************************************************/
UINT2
Bgp4AddPathAttributeToUpdate (tBgp4Info * pAdvtBgpInfo,
                              tBgp4PeerEntry * pPeerEntry,
                              UINT1 *pu1UpdMsgOffset)
{
    tAsPath            *pTsAspath = NULL;
    UINT4               u4AsNo;
    UINT4              *pu4AsList;
    INT4                i4Index = 0;
    INT4                i4SpeakerPeerCap;
    UINT2               u2BytesFilled = 0;
    UINT2               u2AsPathSize = 0;
    UINT2               u2As4PathSize = 0;
    INT2                i2FourByteAsnCount = 0;
    INT2                i2AsSetCount = 0;
    INT2                i2AsSeqCount = 0;
    UINT1               u1IsExtLenBitSet;
    INT1                i1FourByteAsnFound = BGP4_FALSE;

    /* Get the Speaker-Peer's 4-byte ASN Capbility. If
       Case 1: NEW-NEW - AS_PATH  ===> Fully composed of 4 byte ASNs
       Case 2: NEW-OLD - AS_PATH  ===> Fully composed of 2 byte ASNs
       AS4_PATH ===> Fully composed of 4 byte ASNs
       Case 3: OLD-OLD
       OLD-NEW - AS_PATH  ===> Fully composed of 2 byte ASNs
       AS4_PATH ===> Fully composed of 4 byte ASNs */

    /* Getting the 4-byte ASN capability of the SPEAKER and PEER */
    i4SpeakerPeerCap = Bgp4GetSpeakerPeer4ByteAsnCapability (pPeerEntry);

    /* Finding the size of AS_PATH */
    u2AsPathSize = Bgp4AttrAsPathSize (pAdvtBgpInfo, i4SpeakerPeerCap);
    /* Finding the size of AS4_PATH */
    u2As4PathSize = Bgp4AttrAs4PathSize (pAdvtBgpInfo, i4SpeakerPeerCap);

    /* Filling AS-PATH */

    u1IsExtLenBitSet = (u2AsPathSize > BGP4_MAX_BYTE_VALUE) ?
        BGP4_TRUE : BGP4_FALSE;

    *pu1UpdMsgOffset = (UINT1) BGP4_TRANSITIVE_FLAG_MASK;
    if (u1IsExtLenBitSet == BGP4_TRUE)
    {
        *pu1UpdMsgOffset |= (UINT1) BGP4_EXT_LEN_FLAG_MASK;
    }
    pu1UpdMsgOffset++;
    *pu1UpdMsgOffset = (UINT1) BGP4_ATTR_PATH;
    pu1UpdMsgOffset++;

    if (u1IsExtLenBitSet == BGP4_TRUE)
    {
        PTR_ASSIGN2 (pu1UpdMsgOffset, (u2AsPathSize));
        pu1UpdMsgOffset += BGP4_EXTENDED_ATTR_LEN_SIZE;
        u2BytesFilled = (UINT2) (u2BytesFilled + (BGP4_ATYPE_FLAGS_LEN +
                                                  BGP4_ATYPE_CODE_LEN +
                                                  BGP4_EXTENDED_ATTR_LEN_SIZE));
    }
    else
    {
        *pu1UpdMsgOffset = (UINT1) u2AsPathSize;
        pu1UpdMsgOffset++;
        u2BytesFilled = (UINT2) (u2BytesFilled + BGP4_ATYPE_FLAGS_LEN +
                                 BGP4_ATYPE_CODE_LEN +
                                 BGP4_NORMAL_ATTR_LEN_SIZE);
    }

    TMO_SLL_Scan (BGP4_INFO_ASPATH (pAdvtBgpInfo), pTsAspath, tAsPath *)
    {
        *pu1UpdMsgOffset = BGP4_ASPATH_TYPE (pTsAspath);
        pu1UpdMsgOffset++;
        u2BytesFilled++;

        *pu1UpdMsgOffset = BGP4_ASPATH_LEN (pTsAspath);
        pu1UpdMsgOffset++;
        u2BytesFilled++;

        if (i4SpeakerPeerCap == BGP4_4BYTE_ASN_SPEAKER_AND_PEER)
        {
            if (BGP4_PEER_OVERRIDE_CAPABILITY (pPeerEntry) ==
                BGP4_ENABLE_OVERRIDE_CAPABILITY)
            {
                for (i4Index = 0; i4Index < BGP4_ASPATH_LEN (pTsAspath);
                     i4Index++)
                {
                    PTR_ASSIGN4 (pu1UpdMsgOffset, (UINT4) BGP4_GLB_LOCAL_AS_NO);
                    pu1UpdMsgOffset += BGP4_AS4_LENGTH;
                    u2BytesFilled = (UINT2) (u2BytesFilled + BGP4_AS4_LENGTH);
                }
            }
            else
            {
                /* Copy the Path List as it is - since AS-PATH should be 
                   fully composed of 4 bytes */
                MEMCPY (pu1UpdMsgOffset, BGP4_ASPATH_NOS (pTsAspath),
                        (BGP4_ASPATH_LEN (pTsAspath) * BGP4_AS4_LENGTH));
                pu1UpdMsgOffset +=
                    (BGP4_ASPATH_LEN (pTsAspath) * BGP4_AS4_LENGTH);
                u2BytesFilled =
                    (UINT2) (u2BytesFilled +
                             (BGP4_ASPATH_LEN (pTsAspath) * BGP4_AS4_LENGTH));
            }
        }
        else
        {
            pu4AsList = (UINT4 *) (VOID *) BGP4_ASPATH_NOS (pTsAspath);
            for (i4Index = 0; i4Index < BGP4_ASPATH_LEN (pTsAspath); i4Index++)
            {
                /* Replace the non-mappable 4-byte ASNs in path list with
                 * AS_TRANS */
                PTR_FETCH4 (u4AsNo, pu4AsList);
                if (u4AsNo > BGP4_MAX_TWO_BYTE_AS)
                {
                    u4AsNo = BGP4_AS_TRANS;
                }
                i2FourByteAsnCount++;
                if (BGP4_PEER_OVERRIDE_CAPABILITY (pPeerEntry) ==
                    BGP4_ENABLE_OVERRIDE_CAPABILITY)
                {
                    /* Assign the current node's AS number */
                    PTR_ASSIGN2 (pu1UpdMsgOffset, (UINT2) BGP4_GLB_LOCAL_AS_NO);
                }
                else
                {
                    PTR_ASSIGN2 (pu1UpdMsgOffset, (UINT2) u4AsNo);
                }

                pu1UpdMsgOffset += BGP4_AS_LENGTH;
                u2BytesFilled = (UINT2) (u2BytesFilled + BGP4_AS_LENGTH);
                pu4AsList++;
            }

            BGP4_FOUR_BYTE_ASN_SET_SEQ_COUNT (BGP4_ASPATH_TYPE (pTsAspath),
                                              i2FourByteAsnCount,
                                              i2AsSetCount, i2AsSeqCount);
            i2FourByteAsnCount = 0;
        }

    }

    if ((i4SpeakerPeerCap == BGP4_4BYTE_ASN_SPEAKER_AND_PEER) ||
        ((i2AsSetCount == 0) && (i2AsSeqCount == 0)))
    {
        /* Case 1: NEW-NEW - Only AS-PATH to be sent.
         * Case 2: Other cases - If the AS-PATH is composed of mappable ASNs 
         *                       i.e. 4 byte ASN between (0 and 65535). Hence 
         *                       AS4-PATH shouldn't be sent */
        return (u2BytesFilled);
    }
    /* Filling AS-PATH ends */

    /* Filling AS4-PATH */
    u1IsExtLenBitSet = (u2As4PathSize > BGP4_MAX_BYTE_VALUE) ?
        BGP4_TRUE : BGP4_FALSE;

    *pu1UpdMsgOffset = (UINT1) (BGP4_OPTIONAL_FLAG_MASK |
                                BGP4_TRANSITIVE_FLAG_MASK);
    if (u1IsExtLenBitSet == BGP4_TRUE)
    {
        *pu1UpdMsgOffset |= (UINT1) BGP4_EXT_LEN_FLAG_MASK;
    }
    pu1UpdMsgOffset++;
    *pu1UpdMsgOffset = (UINT1) BGP4_ATTR_PATH_FOUR;
    pu1UpdMsgOffset++;

    if (u1IsExtLenBitSet == BGP4_TRUE)
    {
        PTR_ASSIGN2 (pu1UpdMsgOffset, (u2As4PathSize));
        pu1UpdMsgOffset += BGP4_EXTENDED_ATTR_LEN_SIZE;
        u2BytesFilled = (UINT2) (u2BytesFilled +
                                 BGP4_ATYPE_FLAGS_LEN +
                                 BGP4_ATYPE_CODE_LEN +
                                 BGP4_EXTENDED_ATTR_LEN_SIZE);
    }
    else
    {
        *pu1UpdMsgOffset = (UINT1) u2As4PathSize;
        pu1UpdMsgOffset++;
        u2BytesFilled = (UINT2) (u2BytesFilled + BGP4_ATYPE_FLAGS_LEN +
                                 BGP4_ATYPE_CODE_LEN +
                                 BGP4_NORMAL_ATTR_LEN_SIZE);
    }

    TMO_SLL_Scan (BGP4_INFO_ASPATH (pAdvtBgpInfo), pTsAspath, tAsPath *)
    {
        if ((BGP4_ASPATH_TYPE (pTsAspath) != BGP4_ATTR_PATH_CONFED_SET) &&
            (BGP4_ASPATH_TYPE (pTsAspath) != BGP4_ATTR_PATH_CONFED_SEQUENCE))
        {
            *pu1UpdMsgOffset = BGP4_ASPATH_TYPE (pTsAspath);
            pu1UpdMsgOffset++;
            u2BytesFilled++;
            u2As4PathSize--;

            BGP4_FOUR_BYTE_ASN_COUNT
                (BGP4_ASPATH_TYPE (pTsAspath),
                 i2AsSetCount, i2AsSeqCount, i2FourByteAsnCount);

            if (i2FourByteAsnCount > 0)
            {
                *pu1UpdMsgOffset = (UINT1) i2FourByteAsnCount;
                pu1UpdMsgOffset++;
                u2BytesFilled++;
                u2As4PathSize--;

                pu4AsList = (UINT4 *) (VOID *) BGP4_ASPATH_NOS (pTsAspath);
                for (i4Index = 0;
                     ((i4Index < BGP4_ASPATH_LEN (pTsAspath)) &&
                      (u2As4PathSize >= BGP4_AS4_LENGTH)); i4Index++)
                {

                    PTR_FETCH4 (u4AsNo, pu4AsList);
                    i1FourByteAsnFound = BGP4_TRUE;
                    i2FourByteAsnCount--;

                    if ((i1FourByteAsnFound == BGP4_TRUE) &&
                        (i2FourByteAsnCount >= 0))
                    {
                        if (BGP4_PEER_OVERRIDE_CAPABILITY (pPeerEntry) ==
                            BGP4_ENABLE_OVERRIDE_CAPABILITY)
                        {
                            PTR_ASSIGN4 (pu1UpdMsgOffset, BGP4_GLB_LOCAL_AS_NO);
                        }
                        else
                        {
                            PTR_ASSIGN4 (pu1UpdMsgOffset, u4AsNo);
                        }
                        pu1UpdMsgOffset += BGP4_AS4_LENGTH;
                        u2BytesFilled =
                            (UINT2) (u2BytesFilled + BGP4_AS4_LENGTH);
                        u2As4PathSize =
                            (UINT2) (u2As4PathSize - BGP4_AS4_LENGTH);
                    }
                    pu4AsList++;
                }
            }
            else                /* If there had been no 4-byte ASNs in this AS_PATH_TYPE */
            {
                pu1UpdMsgOffset -= 1;
                u2BytesFilled = (UINT2) (u2BytesFilled - 1);
                u2As4PathSize = (UINT2) (u2As4PathSize + 1);
            }
        }
        i2FourByteAsnCount = 0;

    }                            /* Filling AS4-PATH ends */

    return (u2BytesFilled);
}
#endif /* BGP4ADH_C */
