/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4dsh.c,v 1.40 2017/09/15 06:19:55 siva Exp $
 *
 * Description: Contains various routines which are used for the 
 *              manipulation of various data structure used by the
 *              RIB Handler.
 *
 *******************************************************************/
#ifndef BGP4DSH_C
#define BGP4DSH_C

#include "bgp4com.h"

/*****************************************************************************/
/* Function Name : Bgp4DshGetRouteFromList                                   */
/* Description   : Returns the matching route profile from the list of       */
/*                 Route Profiles.                                           */
/* Input(s)      : List of Route profiles (pList),                           */
/*                 Route Profile which needs to be searched for (pRoute)     */
/* Output(s)     : None                                                      */
/* Return(s)     : Route profile if a matching route is present in the list, */
/*                 NULL if matching entry is not found.                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4DshGetRouteFromList (tTMO_SLL * pList, tRouteProfile * pRoute)
{
    tLinkNode          *pRtLinkNode = NULL;
    tBgp4PeerEntry     *pPeer = NULL;
    UINT1               u1Protocol;

    if (pList == NULL)
    {
        return ((tRouteProfile *) NULL);
    }

    u1Protocol = BGP4_RT_PROTOCOL (pRoute);

    if (u1Protocol == BGP_ID)
    {
        pPeer = BGP4_RT_PEER_ENTRY (pRoute);
    }

    TMO_SLL_Scan (pList, pRtLinkNode, tLinkNode *)
    {
        if ((NetAddrMatch (BGP4_RT_NET_ADDRESS_INFO (pRoute),
                           BGP4_RT_NET_ADDRESS_INFO
                           (pRtLinkNode->pRouteProfile))) == BGP4_TRUE)
        {
#ifdef L3VPN
            if ((BGP4_RT_AFI_INFO (pRoute) == BGP4_INET_AFI_IPV4) &&
                (BGP4_RT_SAFI_INFO (pRoute) == BGP4_INET_SAFI_VPNV4_UNICAST))
            {
                if (MEMCMP (BGP4_RT_ROUTE_DISTING (pRoute),
                            BGP4_RT_ROUTE_DISTING (pRtLinkNode->pRouteProfile),
                            BGP4_VPN4_ROUTE_DISTING_SIZE) != 0)
                {
                    continue;
                }
            }
#endif
            if (BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_AGGR_ROUTE)
            {
                if (BGP4_RT_GET_FLAGS (pRtLinkNode->pRouteProfile) &
                    BGP4_RT_AGGR_ROUTE)
                {
                    break;        /* Found a hit */
                }
            }
            else
            {
                if (u1Protocol == BGP_ID)
                {
                    if ((BGP4_RT_PROTOCOL (pRtLinkNode->pRouteProfile) ==
                         BGP_ID) &&
                        (pPeer ==
                         BGP4_RT_PEER_ENTRY (pRtLinkNode->pRouteProfile)))
                    {
                        break;    /* Found an hit */
                    }
                }
                else
                {                /* Non- BGP route. */
                    if (BGP4_RT_PROTOCOL (pRtLinkNode->pRouteProfile)
                        == u1Protocol)
                    {
                        break;    /* Found an hit */
                    }
                }
            }
        }
    }
    if (pRtLinkNode != NULL)
    {
        return (pRtLinkNode->pRouteProfile);
    }
    else
    {
        return (tRouteProfile *) NULL;
    }
}

/*****************************************************************************/
/* Function Name : Bgp4DshGetPeerRouteFromList                               */
/* Description   : Returns the matching peer's route profile from the list of*/
/*                 Route Profiles.                                           */
/* Input(s)      : List of Route profiles (pList),                           */
/*                 Route Profile which needs to be searched for (pRoute),    */
/*                 Pointer to the Peer Information (pPeerInfo)               */
/* Output(s)     : None                                                      */
/* Return(s)     : Route profile if a matching route is present in the list, */
/*                 NULL if matching entry is not found.                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4DshGetPeerRouteFromList (tTMO_SLL * pList, tRouteProfile * pRoute,
                             tBgp4PeerEntry * pPeerInfo)
{
    tLinkNode          *pRtLinkNode = NULL;

    if (pList == NULL)
    {
        return ((tRouteProfile *) NULL);
    }

    TMO_SLL_Scan (pList, pRtLinkNode, tLinkNode *)
    {
        if ((NetAddrMatch (BGP4_RT_NET_ADDRESS_INFO (pRoute),
                           BGP4_RT_NET_ADDRESS_INFO
                           (pRtLinkNode->pRouteProfile))) == BGP4_TRUE)
        {
#ifdef L3VPN
            if ((BGP4_RT_AFI_INFO (pRoute) == BGP4_INET_AFI_IPV4) &&
                (BGP4_RT_SAFI_INFO (pRoute) == BGP4_INET_SAFI_VPNV4_UNICAST))
            {
                if (MEMCMP (BGP4_RT_ROUTE_DISTING (pRoute),
                            BGP4_RT_ROUTE_DISTING (pRtLinkNode->pRouteProfile),
                            BGP4_VPN4_ROUTE_DISTING_SIZE) != 0)
                {
                    continue;
                }
            }
#endif
            if ((BGP4_RT_PROTOCOL (pRtLinkNode->pRouteProfile) == BGP_ID) &&
                (pPeerInfo == BGP4_RT_PEER_ENTRY (pRtLinkNode->pRouteProfile)))
            {
                break;            /* Found an hit */
            }
        }
    }
    if (pRtLinkNode != NULL)
    {
        return (pRtLinkNode->pRouteProfile);
    }
    else
    {
        return (tRouteProfile *) NULL;
    }
}

/*****************************************************************************/
/* Function Name : Bgp4RibhGetRtLinkNodeFromPeerList                         */
/* Description   : Returns the matching route profile from the list of       */
/*                 Route Profiles.                                           */
/* Input(s)      : List of Route profiles (pList),                           */
/*                 Route Profile which needs to be searched for (pRoute)     */
/* Output(s)     : None                                                      */
/* Return(s)     : Route profile if a matching route is present in the list, */
/*                 NULL if matching entry is not found.                      */
/*****************************************************************************/
PRIVATE tLinkNode  *
Bgp4RibhGetRtLinkNodeFromPeerList (tTMO_SLL * pList, tRouteProfile * pRoute)
{
    tLinkNode          *pRtLinkNode = NULL;
    UINT1               u1Protocol;

    if (pList == NULL)
    {
        return ((tLinkNode *) NULL);
    }

    u1Protocol = BGP4_RT_PROTOCOL (pRoute);

    TMO_SLL_Scan (pList, pRtLinkNode, tLinkNode *)
    {
        if ((NetAddrMatch (BGP4_RT_NET_ADDRESS_INFO (pRoute),
                           BGP4_RT_NET_ADDRESS_INFO
                           (pRtLinkNode->pRouteProfile))) == BGP4_TRUE)
        {
#ifdef L3VPN
            if ((BGP4_RT_AFI_INFO (pRoute) == BGP4_INET_AFI_IPV4) &&
                (BGP4_RT_SAFI_INFO (pRoute) == BGP4_INET_SAFI_VPNV4_UNICAST))
            {
                if (MEMCMP (BGP4_RT_ROUTE_DISTING (pRoute),
                            BGP4_RT_ROUTE_DISTING (pRtLinkNode->pRouteProfile),
                            BGP4_VPN4_ROUTE_DISTING_SIZE) != 0)
                {
                    continue;
                }
            }
#endif
            if (BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_AGGR_ROUTE)
            {
                if (BGP4_RT_GET_FLAGS (pRtLinkNode->pRouteProfile) &
                    BGP4_RT_AGGR_ROUTE)
                {
                    break;        /* Found a hit */
                }
            }
            else
            {
                if (BGP4_RT_PROTOCOL (pRtLinkNode->pRouteProfile) == u1Protocol)
                {
                    break;        /* Found an hit */
                }
            }
        }
    }
    return (pRtLinkNode);
}

/*****************************************************************************/
/* Function Name : Bgp4DshAddRouteToPeerAdvtList                             */
/* Description   : Depending on the u4Group flag, it adds the route into the */
/*                 list, if u4Group = BGP4_TIMESTAMP(3), add the route in    */
/*                 ascending order of time stamp value. This used while      */
/*                 adding the routes to the  New Local Peer route list.      */
/*                 else append the route to the end of the list.             */
/* Input(s)      : List of Route profiles (pList),                           */
/*                 Route Profile which needs to be added to the list(pRoute) */
/*                 Flag which says whether grouping is needed or not         */
/*                 (u4Group)                                                 */
/* Output(s)     : Updated list of route profiles (pList)                    */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4DshAddRouteToPeerAdvtList (tTMO_SLL * pList, tRouteProfile * pRoute,
                               UINT4 u4Group, UINT4 u4UpdSentTime)
{
    tAdvRtLinkNode     *pAdvRtLinkNode = NULL;
    tAdvRtLinkNode     *pScanLinkNode = NULL;

    pAdvRtLinkNode = Bgp4DshAllocateAdvtRtLinkNode ();
    if (pAdvRtLinkNode == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tBgp4DshAddRouteToPeerAdvtList () :Alloc_Adv_rt_link_node "
                  "returing NULL \n");
        return BGP4_FAILURE;

    }

    /*TMO_SLL_Init_Node ((tTMO_SLL_NODE *) pAdvRtLinkNode); */

    /* Increment the reference counts appropriately */
    BGP4_RT_REF_COUNT (pRoute)++;

    pAdvRtLinkNode->pRouteProfile = pRoute;
    pAdvRtLinkNode->u4UpdSentTime = u4UpdSentTime;
    pAdvRtLinkNode->pRtInfo = pRoute->pOutRtInfo;
    pRoute->pOutRtInfo = NULL;

    if (u4Group == BGP4_TIMESTAMP)
    {
        pAdvRtLinkNode->u4UpdSentTime = u4UpdSentTime;
        /* order the advertised routes on the basic of the 
         * update-sent time value. */
        TMO_SLL_Scan (pList, pScanLinkNode, tAdvRtLinkNode *)
        {
            if (BGP4_RT_UPDATE_SENT_TIME (pScanLinkNode) ==
                BGP4_RT_UPDATE_SENT_TIME (pAdvRtLinkNode))
            {
                /* Route with matching Time-Stamp found. Add the new
                 * route next to this matching route. */
                TMO_SLL_Insert (pList, &pScanLinkNode->TSNext,
                                &pAdvRtLinkNode->TSNext);
                break;
            }
            else if (BGP4_RT_UPDATE_SENT_TIME (pAdvRtLinkNode) <
                     BGP4_RT_UPDATE_SENT_TIME (pScanLinkNode))
            {
                /* New route time stamp is less than that of the route
                 * in the list. Add the new route before this route. */
                TMO_SLL_Insert (pList, &pScanLinkNode->TSNext,
                                &pAdvRtLinkNode->TSNext);
                break;
            }
        }
        if (pScanLinkNode == NULL)
        {
            /* Route is not yet added. Add it to the end of list. */
            TMO_SLL_Add (pList, &pAdvRtLinkNode->TSNext);
        }
    }
    else
    {
        /* Append the given route to the end of the list */
        TMO_SLL_Add (pList, &pAdvRtLinkNode->TSNext);
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4RibhGetRouteFromPeerList                              */
/* Description   : Returns the matching route profile from the list of       */
/*                 Route Profiles.                                           */
/* Input(s)      : List of Route profiles (pList),                           */
/*                 Route Profile which needs to be searched for (pRoute)     */
/* Output(s)     : None                                                      */
/* Return(s)     : Route profile if a matching route is present in the list, */
/*                 NULL if matching entry is not found.                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4RibhGetRouteFromPeerList (tTMO_SLL * pList, tRouteProfile * pRoute)
{
    tLinkNode          *pRtLinkNode = NULL;

    pRtLinkNode = Bgp4RibhGetRtLinkNodeFromPeerList (pList, pRoute);
    if (pRtLinkNode != NULL)
    {
        return (pRtLinkNode->pRouteProfile);
    }
    return (tRouteProfile *) NULL;
}

/*****************************************************************************/
/* Function Name : Bgp4RibhGetRtFromPeerAdvRtList                            */
/* Description   : Returns the matching route profile from the list of       */
/*                 Route Profiles.                                           */
/* Input(s)      : List of Route profiles (pList),                           */
/*                 Route Profile which needs to be searched for (pRoute)     */
/* Output(s)     : UpdSentTime - time stamp as which the update was sent     */
/* Return(s)     : Route profile if a matching route is present in the list, */
/*                 NULL if matching entry is not found.                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4RibhGetRtFromPeerAdvRtList (tTMO_SLL * pList, tRouteProfile * pRoute,
                                UINT4 *pu4UpdSentTime)
{
    tAdvRtLinkNode     *pRtLinkNode = NULL;

    pRtLinkNode = (tAdvRtLinkNode *)
        Bgp4RibhGetRtLinkNodeFromPeerList (pList, pRoute);
    if (pRtLinkNode != NULL)
    {
        *pu4UpdSentTime = pRtLinkNode->u4UpdSentTime;
        return (pRtLinkNode->pRouteProfile);
    }
    return (tRouteProfile *) NULL;
}

/*****************************************************************************/
/* Function Name : Bgp4DshAddRouteToList                                     */
/* Description   : Depending on the u4Group flag, it adds the route into the*/
/*                 list by grouping routes with identical Routing            */
/*                 informations, or append the route to the end of the       */
/*                 list, or add the route to the start of the list           */
/*                 u4Group = 1, group and add the route to list.             */
/*                 u4Group = 2, add the route as the first node in the list  */
/*                 u4Group = BGP4_TIMESTAMP(3), add the route in ascending   */
/*                 order of time stamp value. This used while adding the     */
/*                 routes to the Local or New Local Peer route list.         */
/*                 else append the route to the end of the list.             */
/* Input(s)      : List of Route profiles (pList),                           */
/*                 Route Profile which needs to be added to the list(pRoute) */
/*                 Flag which says whether grouping is needed or not         */
/*                 (u4Group)                                                 */
/* Output(s)     : Updated list of route profiles (pList)                    */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4DshAddRouteToList (tTMO_SLL * pList, tRouteProfile * pRoute, UINT4 u4Group)
{
    tLinkNode          *pLinkNode = NULL;
    tLinkNode          *pScanLinkNode = NULL;
/*    tLinkNode          *pPrevScanLinkNode = NULL;*/
    tRouteProfile      *pRtProfile = NULL;

    pLinkNode = Bgp4DshAllocateLinkNode ();
    if (pLinkNode == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tBgp4DshAddRouteToList() :Alloc_link_node "
                  "returing NULL \n");
        return BGP4_FAILURE;

    }

    TMO_SLL_Init_Node (&pLinkNode->TSNext);

    /* Increment the reference counts appropriately */
    BGP4_RT_REF_COUNT (pRoute)++;

    /* BGP4_RT_NEXT (pRoute) = NULL; */

    pLinkNode->pRouteProfile = pRoute;
    pLinkNode->pRtInfo = pRoute->pOutRtInfo;
    pRoute->pOutRtInfo = NULL;
    if (u4Group == BGP4_GROUP)
    {
        TMO_SLL_Scan (pList, pScanLinkNode, tLinkNode *)
        {
            pRtProfile = pScanLinkNode->pRouteProfile;
            /* Only if protocol matches, then compare the Info pointers. */
            if (BGP4_RT_PROTOCOL (pRtProfile) != BGP4_RT_PROTOCOL (pRoute))
            {
                continue;
            }
            if ((BGP4_RT_BGP_INFO (pRtProfile) == BGP4_RT_BGP_INFO (pRoute)) ||
                (((Bgp4AttrIsBgpinfosIdentical (BGP4_RT_BGP_INFO (pRtProfile),
                                                BGP4_RT_BGP_INFO (pRoute)))
                  == TRUE) &&
                 (BGP4_RT_MED (pRtProfile) == BGP4_RT_MED (pRoute)) &&
                 (BGP4_RT_LOCAL_PREF (pRtProfile) ==
                  BGP4_RT_LOCAL_PREF (pRoute))))
            {
                TMO_SLL_Insert (pList, &pScanLinkNode->TSNext,
                                &pLinkNode->TSNext);
                return BGP4_SUCCESS;
            }
        }
        /* If there is no matching BGP info in the list, then
         * the control will reach here. Hence just append the
         * new route at the end of the existing list.
         */
        TMO_SLL_Add (pList, &pLinkNode->TSNext);
    }
    else if (u4Group == BGP4_PREPEND)
    {
        /* If u4Group = 2, then add the route as the first node */
        TMO_SLL_Insert (pList, (tTMO_SLL_NODE *) NULL, &pLinkNode->TSNext);
    }
    else
    {
        /* Append the given route to the end of the list */
        TMO_SLL_Add (pList, &pLinkNode->TSNext);
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4AddAdvtRouteToList                                    */
/* Description   : Adds the route profile to the advertised route RBTree in  */
/*                 the peer entry                         */
/* Input(s)      : PeerEntry (pPeerEntry)                     */
/*           Route Profile to be added (pRoute)                        */
/* Output(s)     : None                                 */
/* Returns     : BGP4_SUCCESS or BGP4_FAILURE                     */
/*****************************************************************************/

INT4
Bgp4AddAdvtRouteToList (tBgp4PeerEntry * pPeerEntry, tRouteProfile * pRoute)
{
    tAdvRoute          *pRtProfile = NULL;
    tAdvRoute           RouteProfile;
    UINT4               u4Ret = 0;
    UINT1               u1MatchFnd = BGP4_FALSE;

    RouteProfile.pRouteProfile = pRoute;
    pRtProfile = BGP4_RB_TREE_GET (BGP4_PEER_SENT_ROUTES (pPeerEntry),
                                   (tBgp4RBElem *) (&RouteProfile));
    if (pRtProfile != NULL)
    {
        u1MatchFnd = BGP4_TRUE;
        u4Ret = BGP4_RB_TREE_REMOVE (BGP4_PEER_SENT_ROUTES (pPeerEntry),
                                     (tBgp4RBElem *) pRtProfile);
    }
    if (u1MatchFnd == BGP4_FALSE)
    {
        pRtProfile = (tAdvRoute *) MemAllocMemBlk (BGP4_ADV_ROUTES_MEM_POOL_ID);
        if (pRtProfile == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                      BGP4_CONTROL_PATH_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for BGP4_ADV_ROUTES failed\n");
            gu4BgpDebugCnt[MAX_BGP_ADV_ROUTES_INFOS_SIZING_ID]++;
            return BGP4_FAILURE;
        }
    }
    pRtProfile->pRouteProfile = pRoute;
    u4Ret =
        BGP4_RB_TREE_ADD (BGP4_PEER_SENT_ROUTES (pPeerEntry),
                          (tBgp4RBElem *) pRtProfile);
    if (u4Ret == RB_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tBgp4AddAdvtRouteToList () :RBTree Addition failed\n");
        MemReleaseMemBlock (BGP4_ADV_ROUTES_MEM_POOL_ID, (UINT1 *) pRtProfile);
        return BGP4_FAILURE;
    }
    BGP4_RT_REF_COUNT (pRoute)++;
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4DelAdvtRoute                                          */
                                                                                                                                                                                                                                                                                                                                                                                                                                                                  /* Description   : Deletes the route profile to the advertised route RBTree  *//*                 the peer entry                                            */
/* Input(s)      : PeerEntry (pPeerEntry)                                    */
/*                 Route Profile to be added (pRoute)                        */
/* Output(s)     : None                                                      */
/* Returns       : BGP4_SUCCESS or BGP4_FAILURE                              */
/*****************************************************************************/

INT4
Bgp4DelAdvtRoute (tBgp4PeerEntry * pPeerEntry, tRouteProfile * pRoute)
{
    tAdvRoute          *pFindRtProfile = NULL;
    tAdvRoute           RouteProfile;
    UINT4               u4Ret = 0;
    UINT1               u1MatchFnd = BGP4_FALSE;
    RouteProfile.pRouteProfile = pRoute;
    pFindRtProfile = BGP4_RB_TREE_GET (BGP4_PEER_SENT_ROUTES (pPeerEntry),
                                       (tBgp4RBElem *) (&RouteProfile));
    if (pFindRtProfile != NULL)
    {
        u1MatchFnd = BGP4_TRUE;
    }
    if (u1MatchFnd == BGP4_TRUE)
    {

        u4Ret = BGP4_RB_TREE_REMOVE (BGP4_PEER_SENT_ROUTES (pPeerEntry),
                                     (tBgp4RBElem *) pFindRtProfile);
        MemReleaseMemBlock (BGP4_ADV_ROUTES_MEM_POOL_ID,
                            (UINT1 *) pFindRtProfile);
        if (u4Ret == RB_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                      BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\t Bgp4DelAdvtRoute () :RBTree deletion failed\n");
            return BGP4_FAILURE;
        }
        Bgp4DshReleaseRtInfo (pRoute);
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4DshRelRtFromPeerRtAdvList                             */
/* Description   : This routine removes the link node from the list and also */
/*               : frees the route profile associated with that node         */
/* Input(s)      : List of Route profiles (pList),                           */
/*                 Link Node which needs to be removed (pRoute)              */
/* Output(s)     : Updated list of route profiles (pList)                    */
/* Return(s)     : Removed route profile                                     */
/*****************************************************************************/
VOID
Bgp4DshRelRtFromPeerRtAdvList (tTMO_SLL * pList, tAdvRtLinkNode * pLinkNode)
{
    if ((pLinkNode == NULL) || (pList == NULL))
    {
        return;
    }

    TMO_SLL_Delete (pList, &pLinkNode->TSNext);
    Bgp4DshReleaseAdvRtLinkNode (pLinkNode);

    return;
}

/*****************************************************************************/
/* Function Name : Bgp4DshRemoveNodeFromList                                 */
/* Description   : This routine removes the link node from the list and also */
/*               : frees the route profile associated with that node         */
/* Input(s)      : List of Route profiles (pList),                           */
/*                 Link Node which needs to be removed (pRoute)              */
/* Output(s)     : Updated list of route profiles (pList)                    */
/* Return(s)     : Removed route profile                                     */
/*****************************************************************************/
tRouteProfile      *
Bgp4DshRemoveNodeFromList (tTMO_SLL * pList, tLinkNode * pLinkNode)
{
    tRouteProfile      *pRtProfile = NULL;
    if ((pLinkNode == NULL) || (pList == NULL))
    {
        return (NULL);
    }

    pRtProfile = pLinkNode->pRouteProfile;

    TMO_SLL_Delete (pList, &pLinkNode->TSNext);
    Bgp4DshReleaseLinkNode (pLinkNode);

    return (pRtProfile);
}

/*****************************************************************************/
/* Function Name : Bgp4DshRemoveRouteFromList                                */
/* Description   : This routine finds a matching route from the list of      */
/*                 routes and removes that route from the list.              */
/* Input(s)      : List of Route profiles (pList),                           */
/*                 Route Profile which needs to be removed (pRoute)          */
/* Output(s)     : Updated list of route profiles (pList)                    */
/* Return(s)     : Removed route profile if match occurs,                    */
/*                 NULL if matching entry is not found.                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4DshRemoveRouteFromList (tTMO_SLL * pList, tRouteProfile * pRoute)
{
    tLinkNode          *pMatchingRtNode = NULL;
    tLinkNode          *pPrevNode = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tBgp4PeerEntry     *pPeer = NULL;
    tBgp4PeerEntry     *pScanPeer = NULL;
    UINT4               u4MED;
    UINT4               u4IfIndex;
    UINT1               u1Protocol = 0;
    UINT1               u1ScanProtocol = 0;

    if (pList == NULL)
    {
        return ((tRouteProfile *) NULL);
    }
    if (pRoute == NULL)
    {
        return ((tRouteProfile *) NULL);
    }

    u1Protocol = BGP4_RT_PROTOCOL (pRoute);
    u4MED = BGP4_RT_MED (pRoute);
    u4IfIndex = BGP4_RT_GET_RT_IF_INDEX (pRoute);

    if (u1Protocol == BGP_ID)
    {
        pPeer = BGP4_RT_PEER_ENTRY (pRoute);
    }
    pPrevNode = NULL;
    TMO_SLL_Scan (pList, pMatchingRtNode, tLinkNode *)
    {
        u1ScanProtocol = BGP4_RT_PROTOCOL (pMatchingRtNode->pRouteProfile);

        if (u1ScanProtocol == BGP_ID)
        {
            pScanPeer = BGP4_RT_PEER_ENTRY (pMatchingRtNode->pRouteProfile);
        }

        if (((NetAddrMatch (BGP4_RT_NET_ADDRESS_INFO (pRoute),
                            BGP4_RT_NET_ADDRESS_INFO
                            (pMatchingRtNode->pRouteProfile))) == BGP4_TRUE)
            && (u1ScanProtocol == u1Protocol))
        {
#ifdef L3VPN
            if ((BGP4_RT_AFI_INFO (pRoute) == BGP4_INET_AFI_IPV4) &&
                (BGP4_RT_SAFI_INFO (pRoute) == BGP4_INET_SAFI_VPNV4_UNICAST))
            {
                if (MEMCMP (BGP4_RT_ROUTE_DISTING (pRoute),
                            BGP4_RT_ROUTE_DISTING
                            (pMatchingRtNode->pRouteProfile),
                            BGP4_VPN4_ROUTE_DISTING_SIZE) != 0)
                {
                    pPrevNode = pMatchingRtNode;
                    continue;
                }
            }
#endif
#ifdef EVPN_WANTED
            if ((BGP4_RT_AFI_INFO (pRoute) == BGP4_INET_AFI_L2VPN) &&
                (BGP4_RT_SAFI_INFO (pRoute) == BGP4_INET_SAFI_EVPN))
            {
                if (pRoute->EvpnNlriInfo.u1RouteType == EVPN_ETH_SEGMENT_ROUTE)
                {
                    if (MEMCMP (BGP4_EVPN_ETHSEGID (pRoute), BGP4_EVPN_ETHSEGID
                                (pMatchingRtNode->pRouteProfile),
                                BGP4_EVPN_ETH_SEG_ID_SIZE) != 0)
                    {
                        pPrevNode = pMatchingRtNode;
                        continue;
                    }
                }
                else if ((MEMCMP (BGP4_EVPN_MACADDR (pRoute), BGP4_EVPN_MACADDR
                                  (pMatchingRtNode->pRouteProfile),
                                  sizeof (tMacAddr)) != 0) ||
                         ((MEMCMP (BGP4_EVPN_MACADDR (pRoute), BGP4_EVPN_MACADDR
                                   (pMatchingRtNode->pRouteProfile),
                                   sizeof (tMacAddr)) == 0) &&
                          (BGP4_EVPN_VNID (pRoute) !=
                           BGP4_EVPN_VNID (pMatchingRtNode->pRouteProfile))))
                {
                    pPrevNode = pMatchingRtNode;
                    continue;
                }
            }
#endif

            if ((u1Protocol == BGP_ID) && (pPeer != pScanPeer))
            {
                pPrevNode = pMatchingRtNode;
                continue;
            }
            if (u1Protocol != BGP_ID)
            {
                /* Added for FIB update list - the interface, metric also
                 * MUST be checked
                 */
                if ((BGP4_RT_MED (pMatchingRtNode->pRouteProfile) != u4MED) ||
                    (BGP4_RT_GET_RT_IF_INDEX (pMatchingRtNode->pRouteProfile)
                     != u4IfIndex))
                {
                    pPrevNode = pMatchingRtNode;
                    continue;
                }
            }
            break;                /* Found an hit */
        }
        pPrevNode = pMatchingRtNode;
    }

    if (pMatchingRtNode != NULL)
    {
        pRtProfile = pMatchingRtNode->pRouteProfile;
        if (pPrevNode == NULL)
        {
            TMO_SLL_Delete (pList, &pMatchingRtNode->TSNext);
        }
        else
        {
            TMO_SLL_Delete_In_Middle (pList, &pPrevNode->TSNext,
                                      &pMatchingRtNode->TSNext,
                                      pMatchingRtNode->TSNext.pNext);
        }

        Bgp4DshReleaseLinkNode (pMatchingRtNode);
        pMatchingRtNode = NULL;

        return (pRtProfile);
    }

    return (tRouteProfile *) NULL;
}

/*****************************************************************************/
/* Function Name : Bgp4RibhDetachLinkNodeFromPeerList                        */
/* Description   : This routine finds a matching route from the list of      */
/*                 routes and removes Link node from the list.               */
/* Input(s)      : List of Route profiles (pList),                           */
/*                 Route Profile which needs to be removed (pRoute)          */
/* Output(s)     : Updated list of route profiles (pList)                    */
/* Return(s)     : Removed Link node if match occurs,                        */
/*                 NULL if matching entry is not found.                      */
/*****************************************************************************/
PRIVATE tLinkNode  *
Bgp4RibhDetachLinkNodeFromPeerList (tTMO_SLL * pList, tRouteProfile * pRoute)
{
    tLinkNode          *pMatchingRtNode = NULL;
    tLinkNode          *pPrevNode = NULL;

    if (pList == NULL)
    {
        return ((tLinkNode *) NULL);
    }
    if (pRoute == NULL)
    {
        return ((tLinkNode *) NULL);
    }

    pPrevNode = NULL;

    TMO_SLL_Scan (pList, pMatchingRtNode, tLinkNode *)
    {

        if ((NetAddrMatch (BGP4_RT_NET_ADDRESS_INFO (pRoute),
                           BGP4_RT_NET_ADDRESS_INFO
                           (pMatchingRtNode->pRouteProfile))) == BGP4_TRUE)
        {
#ifdef L3VPN
            if ((BGP4_RT_AFI_INFO (pRoute) == BGP4_INET_AFI_IPV4) &&
                (BGP4_RT_SAFI_INFO (pRoute) == BGP4_INET_SAFI_VPNV4_UNICAST))
            {
                if (MEMCMP (BGP4_RT_ROUTE_DISTING (pRoute),
                            BGP4_RT_ROUTE_DISTING
                            (pMatchingRtNode->pRouteProfile),
                            BGP4_VPN4_ROUTE_DISTING_SIZE) != 0)
                {
                    pPrevNode = pMatchingRtNode;
                    continue;
                }
            }
#endif
#ifdef EVPN_WANTED
            if ((BGP4_RT_AFI_INFO (pRoute) == BGP4_INET_AFI_L2VPN) &&
                (BGP4_RT_SAFI_INFO (pRoute) == BGP4_INET_SAFI_EVPN))
            {
                if (pRoute->EvpnNlriInfo.u1RouteType == EVPN_ETH_SEGMENT_ROUTE)
                {
                    if (MEMCMP (BGP4_EVPN_ETHSEGID (pRoute), BGP4_EVPN_ETHSEGID
                                (pMatchingRtNode->pRouteProfile),
                                BGP4_EVPN_ETH_SEG_ID_SIZE) != 0)
                    {
                        pPrevNode = pMatchingRtNode;
                        continue;
                    }
                }
                else if ((MEMCMP (BGP4_EVPN_MACADDR (pRoute), BGP4_EVPN_MACADDR
                                  (pMatchingRtNode->pRouteProfile),
                                  sizeof (tMacAddr)) != 0) ||
                         ((MEMCMP (BGP4_EVPN_MACADDR (pRoute), BGP4_EVPN_MACADDR
                                   (pMatchingRtNode->pRouteProfile),
                                   sizeof (tMacAddr)) == 0) &&
                          (BGP4_EVPN_VNID (pRoute) !=
                           BGP4_EVPN_VNID (pMatchingRtNode->pRouteProfile))))
                {
                    pPrevNode = pMatchingRtNode;
                    continue;
                }

            }
#endif

            break;                /* Found an hit */
        }
        pPrevNode = pMatchingRtNode;
    }

    if (pMatchingRtNode != NULL)
    {
        if (pPrevNode == NULL)
        {
            TMO_SLL_Delete (pList, &pMatchingRtNode->TSNext);
        }
        else
        {
            TMO_SLL_Delete_In_Middle (pList, &pPrevNode->TSNext,
                                      &pMatchingRtNode->TSNext,
                                      pMatchingRtNode->TSNext.pNext);
        }
        return (pMatchingRtNode);
    }
    return (tLinkNode *) NULL;
}

/*****************************************************************************/
/* Function Name : Bgp4RibhRemoveRouteFromPeerList                           */
/* Description   : This routine finds a matching route from the list of      */
/*                 routes and removes that route from the list.              */
/* Input(s)      : List of Route profiles (pList),                           */
/*                 Route Profile which needs to be removed (pRoute)          */
/* Output(s)     : Updated list of route profiles (pList)                    */
/* Return(s)     : Removed route profile if match occurs,                    */
/*                 NULL if matching entry is not found.                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4RibhRemoveRouteFromPeerList (tTMO_SLL * pList, tRouteProfile * pRoute)
{
    tLinkNode          *pLinkNode = NULL;
    tRouteProfile      *pRtProfile = NULL;

    pLinkNode = Bgp4RibhDetachLinkNodeFromPeerList (pList, pRoute);
    if (pLinkNode != NULL)
    {
        pRtProfile = pLinkNode->pRouteProfile;
        Bgp4DshReleaseLinkNode (pLinkNode);
        pLinkNode = NULL;
    }
    return (pRtProfile);
}

/*****************************************************************************/
/* Function Name : Bgp4RibhRemoveRtFromPeerAdvtList                          */
/* Description   : This routine finds a matching route from the list of      */
/*                 routes and removes that route from the list.              */
/* Input(s)      : List of Route profiles (pList),                           */
/*                 Route Profile which needs to be removed (pRoute)          */
/* Output(s)     : pList  - Updated list of route profiles                   */
/*                 pu4UpdSentTime - Route update sent time                   */
/* Return(s)     : Removed route profile if match occurs,                    */
/*                 NULL if matching entry is not found.                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4RibhRemoveRtFromPeerAdvtList (tTMO_SLL * pList, tRouteProfile * pRoute,
                                  UINT4 *pu4UpdSentTime)
{
    tAdvRtLinkNode     *pAdvLinkNode = NULL;
    tRouteProfile      *pRtProfile = NULL;

    pAdvLinkNode = (tAdvRtLinkNode *)
        Bgp4RibhDetachLinkNodeFromPeerList (pList, pRoute);
    if (pAdvLinkNode != NULL)
    {
        pRtProfile = pAdvLinkNode->pRouteProfile;
        *pu4UpdSentTime = pAdvLinkNode->u4UpdSentTime;
        Bgp4DshReleaseAdvRtLinkNode (pAdvLinkNode);
    }
    return (pRtProfile);
}

/*****************************************************************************/
/* Function Name : Bgp4DshRemovePeerRoutesFromList                           */
/* Description   : This routine finds the given peer's route present in the  */
/*                 list and removes those routes.                            */
/* Input(s)      : List of Route profiles (pList),                           */
/*                 Peer whose routes need to be removed.   (pPeer)           */
/* Output(s)     : Updated list of route profiles (pList)                    */
/* Return(s)     : BGP4_SUCCESS - if operation is successful                 */
/*                 BGP4_FAILURE - if operation fails.                        */
/*****************************************************************************/
INT4
Bgp4DshRemovePeerRoutesFromList (tTMO_SLL * pList, tBgp4PeerEntry * pPeer)
{
    tLinkNode          *pMatchingRtNode = NULL;
    tLinkNode          *pTmpNode = NULL;
    tRouteProfile      *pRtProfile = NULL;

    if ((pList == NULL) || (pPeer == NULL))
    {
        return (BGP4_FAILURE);
    }

    BGP_SLL_DYN_Scan (pList, pMatchingRtNode, pTmpNode, tLinkNode *)
    {
        pRtProfile = pMatchingRtNode->pRouteProfile;
        if ((BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID) &&
            ((PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO
                           (BGP4_RT_PEER_ENTRY (pRtProfile)),
                           BGP4_PEER_REMOTE_ADDR_INFO (pPeer))) == BGP4_TRUE))
        {
            TMO_SLL_Delete (pList, &pMatchingRtNode->TSNext);
            Bgp4DshReleaseLinkNode (pMatchingRtNode);
        }
    }
    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Bgp4DshGetNonBgpMatchRouteFromProfileList                 */
/* Description   : This function returns the Identical Non-Bgp route from   */
/*                      the list for the given Non-Bgp route.               */
/*                      If Identical rotue is not present, this gives       */
/*                      the Non-Bgp rotue.                                  */
/* Input(s)      : Tree Node which contains a list of routes to a destination*/
/*                 (pLeaf)                                                   */
/* Output(s)     : None.                                                     */
/* Return(s)     : Route profile if best route is found,                     */
/*                 NULL if not.                                              */
/*****************************************************************************/
tRouteProfile      *
Bgp4DshGetNonBgpRtFromProfileList (tRouteProfile * pRtProfileList,
                                   tRouteProfile * pRoute,
                                   BOOL1 * pu1IsIdentical)
{
    tRouteProfile      *pRtList = NULL;
    tBgp4Info          *pBGP4Info = NULL;

    pRtList = pRtProfileList;
    pBGP4Info = BGP4_RT_BGP_INFO (pRoute);
    while (pRtList != NULL)
    {
        /* Is it the replacement route learnt via redistribution. */
        if ((BGP4_RT_PROTOCOL (pRtList) != BGP_ID) &&
            (BGP4_RT_PROTOCOL (pRoute) != BGP_ID))
        {
            /* Check if route is not identical to old-route */
            if ((Bgp4AttrIsBgpinfosIdentical
                 (pBGP4Info, BGP4_RT_BGP_INFO (pRtList)) != TRUE) ||
                (BGP4_RT_MED (pRoute) != BGP4_RT_MED (pRtList)) ||
                (BGP4_RT_GET_RT_IF_INDEX (pRtList) !=
                 BGP4_RT_GET_RT_IF_INDEX (pRoute)))
            {
                break;
            }
            else
            {
                /* Both new and old routes are identical */
                *pu1IsIdentical = BGP4_TRUE;
                break;
            }
        }
        pRtList = BGP4_RT_NEXT (pRtList);
    }
    return pRtList;
}

/*****************************************************************************/
/* Function Name : Bgp4DshGetBestRouteFromProfileList                        */
/* Description   : This function returns the best route from the sorted      */
/*                 list of route profiles which is present in the            */
/*                 Tree Node of a particular destination.                    */
/* Input(s)      : Tree Node which contains a list of routes to a destination*/
/*                 (pLeaf)                                                   */
/* Output(s)     : None.                                                     */
/* Return(s)     : Route profile if best route is found,                     */
/*                 NULL if not.                                              */
/*****************************************************************************/
tRouteProfile      *
Bgp4DshGetBestRouteFromProfileList (tRouteProfile * pRtProfileList,
                                    UINT4 u4VrfId)
{
    tRouteProfile      *pHeadRtProfile = NULL;

    UNUSED_PARAM (u4VrfId);
    if (pRtProfileList == NULL)
    {
        return (tRouteProfile *) NULL;
    }

    pHeadRtProfile = pRtProfileList;

    if (pHeadRtProfile != NULL)
    {
        if ((BGP4_RT_GET_FLAGS (pHeadRtProfile) & BGP4_RT_BEST) == BGP4_RT_BEST)
        {
            return (pHeadRtProfile);    /* First and Best route */
        }
#ifdef L3VPN
        else
        {
            tRtInstallVrfInfo  *pRtVrfInfo = NULL;

            Bgp4Vpnv4GetInstallVrfInfo (pHeadRtProfile, u4VrfId, &pRtVrfInfo);
            if ((pRtVrfInfo != NULL) &&
                (BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS (pRtVrfInfo) &
                 BGP4_RT_BGP_VRF_BEST))
            {
                return (pHeadRtProfile);    /* First and Best route */
            }
        }
#endif
    }
    return ((tRouteProfile *) NULL);
}

/*****************************************************************************/
/* Function Name : Bgp4DshGetFirstRouteFromProfileList                       */
/* Description   : This function returns the first route from the sorted     */
/*                 list of route profiles which is present in the            */
/*                 Tree Node of a particular destination.                    */
/* Input(s)      : Tree Node which contains a list of routes to a destination*/
/*                 (pLeaf)                                                   */
/* Output(s)     : None.                                                     */
/* Return(s)     : Route profile if best route is found,                     */
/*                 NULL if not.                                              */
/*****************************************************************************/
tRouteProfile      *
Bgp4DshGetFirstRouteFromProfileList (tRouteProfile * pRtProfileList)
{
    tRouteProfile      *pHeadRtProfile = NULL;

    if (pRtProfileList == NULL)
    {
        return (tRouteProfile *) NULL;
    }

    pHeadRtProfile = pRtProfileList;

    return (pHeadRtProfile);    /* First route */
}

#ifdef L3VPN
/*****************************************************************************/
/* Function Name : Bgp4DshGetPeerRtFromVrfProfileList                       */
/* Description   : Returns the route profile which was advertised by a       */
/*                 specified peer from the list of Route Profiles.           */
/* Input(s)      : List of routes to a destination                           */
/*                 (pRouteList)                                              */
/*                 Peer Information (pPeerInfo)                              */
/* Output(s)     : None.                                                     */
/* Return(s)     : Route profile if a matching route is present in the list, */
/*                 NULL if matching entry is not found.                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4DshGetPeerRtFromVrfProfileList (tRouteProfile * pRouteList,
                                    tBgp4PeerEntry * pPeerInfo, UINT4 u4VrfId)
{
    tBgp4PeerEntry     *pScanPeer = NULL;
    tRouteProfile      *pHeadRtProfile = NULL;

    pHeadRtProfile = pRouteList;

    while (pHeadRtProfile != NULL)
    {
        if (BGP4_RT_PROTOCOL (pHeadRtProfile) == BGP_ID)
        {
            pScanPeer = BGP4_RT_PEER_ENTRY (pHeadRtProfile);
#ifdef L3VPN
            if ((PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pScanPeer),
                              BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)))
                == BGP4_TRUE)
#else
            if (pScanPeer == pPeerInfo)
#endif
            {
                break;            /* Found an hit */
            }
        }
        BGP4_RT_GET_NEXT (pHeadRtProfile, u4VrfId, pHeadRtProfile);
    }
    return (pHeadRtProfile);
}

/*****************************************************************************/
/* Function Name : Bgp4DshGetPeerVrfRtFromProfileList                        */
/* Description   : Returns the route profile which was advertised by a       */
/*                 specified peer from the list of Route Profiles.           */
/*                 Here, the RD of the input route profile is also considered*/
/* Input(s)      : List of routes to a destination                           */
/*                 (pRouteList)                                              */
/*                 Peer Information (pPeerInfo)                              */
/*                 pRtProfile - input route information                      */
/* Output(s)     : None.                                                     */
/* Return(s)     : Route profile if a matching route is present in the list, */
/*                 NULL if matching entry is not found.                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4DshGetPeerVrfRtFromProfileList (tRouteProfile * pRouteList,
                                    tBgp4PeerEntry * pPeerInfo,
                                    tRouteProfile * pRtProfile)
{
    tBgp4PeerEntry     *pScanPeer = NULL;
    tRouteProfile      *pHeadRtProfile = NULL;

    pHeadRtProfile = pRouteList;

    while (pHeadRtProfile != NULL)
    {
        if (BGP4_RT_PROTOCOL (pHeadRtProfile) == BGP_ID)
        {
            pScanPeer = BGP4_RT_PEER_ENTRY (pHeadRtProfile);
            if (((PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pScanPeer),
                               BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)))
                 == BGP4_TRUE) &&
                (MEMCMP (BGP4_RT_ROUTE_DISTING (pHeadRtProfile),
                         BGP4_RT_ROUTE_DISTING (pRtProfile),
                         BGP4_VPN4_ROUTE_DISTING_SIZE) == 0))
            {
                break;            /* Found an hit */
            }
        }
        pHeadRtProfile = BGP4_RT_NEXT (pHeadRtProfile);
    }
    return (pHeadRtProfile);
}
#endif

/*****************************************************************************/
/* Function Name : Bgp4DshGetPeerRtFromProfileList                           */
/* Description   : Returns the route profile which was advertised by a       */
/*                 specified peer from the list of Route Profiles.           */
/* Input(s)      : List of routes to a destination                           */
/*                 (pRouteList)                                              */
/*                 Peer Information (pPeerInfo)                              */
/* Output(s)     : None.                                                     */
/* Return(s)     : Route profile if a matching route is present in the list, */
/*                 NULL if matching entry is not found.                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4DshGetPeerRtFromProfileList (tRouteProfile * pRouteList,
                                 tBgp4PeerEntry * pPeerInfo)
{
    tBgp4PeerEntry     *pScanPeer = NULL;
    tRouteProfile      *pHeadRtProfile = NULL;

    pHeadRtProfile = pRouteList;

    while (pHeadRtProfile != NULL)
    {
        if (BGP4_RT_PROTOCOL (pHeadRtProfile) == BGP_ID)
        {
            pScanPeer = BGP4_RT_PEER_ENTRY (pHeadRtProfile);

            if ((pScanPeer != NULL)
                &&
                (PrefixMatch
                 (BGP4_PEER_REMOTE_ADDR_INFO (pScanPeer),
                  BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo))) == BGP4_TRUE)
            {
                break;            /* Found an hit */
            }
        }
        pHeadRtProfile = BGP4_RT_NEXT (pHeadRtProfile);
    }
    return (pHeadRtProfile);
}

/*****************************************************************************/
/* Function Name : Bgp4DshGetRtForNextHopFromProfileList                     */
/* Description   : Returns the route profile for a nextho                    */
/* Input(s)      : List of routes to a destination                           */
/*                 (pRouteList)                                              */
/*                 Nexthop Information (NextHopAddr)                        */
/* Output(s)     : None.                                                     */
/* Return(s)     : Route profile if a matching route is present in the list, */
/*                 NULL if matching entry is not found.                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4DshGetRtForNextHopFromProfileList (tRouteProfile * pRouteList,
                                       tAddrPrefix NextHop, UINT4 u4VrfId)
{
    tRouteProfile      *pHeadRtProfile = NULL;

    pHeadRtProfile = pRouteList;
    while (pHeadRtProfile != NULL)
    {
        if (PrefixMatch
            (BGP4_INFO_NEXTHOP_INFO (BGP4_RT_BGP_INFO (pHeadRtProfile)),
             NextHop) == BGP4_TRUE)
        {
            return pHeadRtProfile;
        }
#ifdef L3VPN
        BGP4_RT_GET_NEXT (pHeadRtProfile, u4VrfId, pHeadRtProfile);
#else
        UNUSED_PARAM (u4VrfId);
        pHeadRtProfile = BGP4_RT_NEXT (pHeadRtProfile);
#endif
    }
    return (pHeadRtProfile);
}

/*****************************************************************************/
/* Function Name : Bgp4DshAddRouteToProfileList                              */
/* Description   : This routine adds a particular route into the route list  */
/*                 present in the Tree node for that particular destination  */
/*                 after applying the decision algorithm.                    */
/* Input(s)      : Route Profile which needs to be added(pRoute)             */
/* Output(s)     : (ppRtProfileList) -Updated list of route profiles in the  */
/*                 RIB node.                                                 */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4DshAddRouteToProfileList (tRouteProfile ** ppRtProfileList,
                              tRouteProfile * pRoute)
{
    UINT4               u4VrfId = BGP4_RT_CXT_ID (pRoute);
    UINT2               u2AddrFamily;
#ifdef L3VPN
    UINT4               u4AsafiMask;
#endif

#ifdef L3VPN
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRoute),
                           BGP4_RT_SAFI_INFO (pRoute), u4AsafiMask);
#endif

    u2AddrFamily =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                      (BGP4_RT_NET_ADDRESS_INFO (pRoute)));
    switch (u2AddrFamily)
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4Mask;
            UINT4               u4Ipaddr;

            PTR_FETCH_4 (u4Ipaddr, BGP4_RT_IP_PREFIX (pRoute));
            u4Mask = Bgp4GetSubnetmask ((UINT1) BGP4_RT_PREFIXLEN (pRoute));
            u4Ipaddr = u4Ipaddr & u4Mask;
            PTR_ASSIGN_4 (BGP4_RT_IP_PREFIX (pRoute), u4Ipaddr);
        }
            break;
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
        {
            tIp6Addr            IPAddr;
            tIp6Addr            IPAddr1;

            MEMCPY (&IPAddr1.u1_addr[0], BGP4_RT_IP_PREFIX (pRoute),
                    BGP4_IPV6_PREFIX_LEN);
            Ip6CopyAddrBits (&IPAddr, &IPAddr1, BGP4_RT_PREFIXLEN (pRoute));
            MEMCPY (BGP4_RT_IP_PREFIX (pRoute), IPAddr.u1_addr,
                    BGP4_IPV6_PREFIX_LEN);
        }
            break;
#endif
        default:
            break;
    }

    BGP4_RT_REF_COUNT (pRoute)++;
#ifdef L3VPN
    BGP4_VPN4_GET_PROCESS_VRFID (pRoute, u4VrfId);
#else
    BGP4_GET_PROCESS_VRFID (pRoute, u4VrfId);
#endif

    /* When there is no route profile list for this destination
     * then just add this route and return success
     */
    if ((*ppRtProfileList) == NULL)
    {
#ifdef L3VPN
        if ((u4VrfId == BGP4_DFLT_VRFID) ||
            (u4AsafiMask != CAP_MP_VPN4_UNICAST))
        {
#endif
            BGP4_RT_NEXT (pRoute) = NULL;
            (*ppRtProfileList) = pRoute;
            if ((BGP4_IS_ROUTE_VALID (pRoute) == BGP4_TRUE) &&
                ((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_HISTORY) !=
                 BGP4_RT_HISTORY))
            {
                /* Set the Best Flag */
                BGP4_RT_SET_FLAG (pRoute, BGP4_RT_BEST);
            }
            BGP4_RIB_ROUTE_CNT (u4VrfId)++;
            if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
            {
                BGP4_TABLE_VERSION (u4VrfId)++;
                Bgp4RedSyncTableVersion (u4VrfId, BGP4_TABLE_VERSION (u4VrfId));
            }
            return (BGP4_SUCCESS);
#ifdef L3VPN
        }
        else
        {
            (*ppRtProfileList) = pRoute;
            BGP4_VPN4_RT_INSTALL_VRFINFO_SET_RTNEXT (pRoute, NULL, u4VrfId);
            Bgp4Vpnv4SetVrfFlags (pRoute, u4VrfId,
                                  (BGP4_RT_PRESENT_IN_VRF |
                                   BGP4_RT_BGP_VRF_BEST));
            BGP4_RIB_ROUTE_CNT (u4VrfId)++;
            if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
            {
                BGP4_TABLE_VERSION (u4VrfId)++;
                Bgp4RedSyncTableVersion (u4VrfId, BGP4_TABLE_VERSION (u4VrfId));
            }
            return (BGP4_SUCCESS);
        }
#endif
    }
    /* Present in the Decision Handler SubModule */
    Bgp4DechAddRouteSorted (ppRtProfileList, pRoute);
    BGP4_RIB_ROUTE_CNT (u4VrfId)++;
    if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
    {
        BGP4_TABLE_VERSION (u4VrfId)++;
        Bgp4RedSyncTableVersion (u4VrfId, BGP4_TABLE_VERSION (u4VrfId));
    }
#ifdef L3VPN
    if (u4VrfId != BGP4_DFLT_VRFID)
    {
        Bgp4Vpnv4SetVrfFlags (pRoute, u4VrfId, BGP4_RT_PRESENT_IN_VRF);
    }
#endif
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4DshRemoveRouteFromProfileList                         */
/* Description   : Removes the route profile which was advertised by a       */
/*                 specified peer from the list of Route Profiles.           */
/* Input(s)      : Tree Node which contains a list of routes to a destination*/
/*                 (pTreeNode)                                               */
/*                 Peer Information (pPeerInfo)                              */
/* Output(s)     : Tree Node containing the updated list of routes.          */
/* Return(s)     : Removed route profile if a matching route is present,     */
/*                 NULL if matching entry is not found.                      */
/*****************************************************************************/
INT4
Bgp4DshRemoveRouteFromProfileList (tRouteProfile * pRtProfile,
                                   tRouteProfile ** pRtProfileList)
{
    tRouteProfile      *pHeadRtProfile = NULL;
#ifdef L3VPN
    tRouteProfile      *pRtNext = NULL;
#endif
    tRouteProfile      *pPrevRtProfile = NULL;
    VOID               *pTreeNode = NULL;
    tAddrPrefix         InvPrefix;
    UINT4               u4VrfId = BGP4_RT_CXT_ID (pRtProfile);
    UINT1               u1ReRunDech = BGP4_FALSE;

    Bgp4InitAddrPrefixStruct (&(InvPrefix), BGP4_RT_AFI_INFO (pRtProfile));

#ifdef L3VPN
    BGP4_VPN4_GET_PROCESS_VRFID (pRtProfile, u4VrfId);
#else
    BGP4_GET_PROCESS_VRFID (pRtProfile, u4VrfId);
#endif
    pHeadRtProfile = (*pRtProfileList);

    /* Decide whether the decision process needs to be done again
     * because of this route's removal. The decision process is
     * executed again if there are more than one route in this
     * list and the route to be deleted is learnt from a BGP Peer
     * and the route to be deleted is the best route. In all other
     * case Decision Handler is not executed again.
     */
#ifdef L3VPN
    BGP4_RT_GET_NEXT (pHeadRtProfile, u4VrfId, pRtNext);
    if ((pRtNext != NULL) &&
        (pHeadRtProfile == pRtProfile) &&
        (BGP4_LOCAL_ADMIN_STATUS (u4VrfId) == BGP4_ADMIN_UP))
#else
    if ((BGP4_RT_NEXT (pHeadRtProfile) != NULL) &&
        (pHeadRtProfile == pRtProfile) &&
        (BGP4_LOCAL_ADMIN_STATUS (u4VrfId) == BGP4_ADMIN_UP))
#endif
    {
        u1ReRunDech = BGP4_TRUE;
    }

    while (pHeadRtProfile != NULL)
    {
        if (((NetAddrMatch (BGP4_RT_NET_ADDRESS_INFO (pHeadRtProfile),
                            BGP4_RT_NET_ADDRESS_INFO (pRtProfile)))
             == BGP4_TRUE) &&
            (BGP4_RT_GET_RT_IF_INDEX (pHeadRtProfile) ==
             BGP4_RT_GET_RT_IF_INDEX (pRtProfile)) &&
            (BGP4_RT_PROTOCOL (pHeadRtProfile) ==
             BGP4_RT_PROTOCOL (pRtProfile)))
        {
            if (BGP4_RT_PROTOCOL (pHeadRtProfile) == BGP_ID)
            {
                if ((BGP4_RT_PEER_ENTRY (pHeadRtProfile) ==
                     BGP4_RT_PEER_ENTRY (pRtProfile)) &&
                    (BGP4_RT_GET_FLAGS (pHeadRtProfile) ==
                     BGP4_RT_GET_FLAGS (pRtProfile)))
                {
                    break;
                }
                pPrevRtProfile = pHeadRtProfile;
#ifdef L3VPN
                BGP4_RT_GET_NEXT (pHeadRtProfile, u4VrfId, pHeadRtProfile);
#else
                pHeadRtProfile = BGP4_RT_NEXT (pHeadRtProfile);
#endif
                continue;
            }
            else
            {
                /* If the route is from NON-BGP Protocol; search for exact
                 * match by comparing the next hop */
                if ((BGP4_RT_GET_ADV_FLAG (pRtProfile)) ==
                    (BGP4_RT_GET_ADV_FLAG (pHeadRtProfile)))
                {
                    if (MEMCMP (&(BGP4_RT_IMMEDIATE_NEXTHOP_INFO (pRtProfile)),
                                &(BGP4_RT_IMMEDIATE_NEXTHOP_INFO
                                  (pHeadRtProfile)), sizeof (tAddrPrefix)) == 0)
                    {
                        break;
                    }
                }
                pPrevRtProfile = pHeadRtProfile;
                pHeadRtProfile = BGP4_RT_NEXT (pHeadRtProfile);
                continue;
            }
            break;
        }
        pPrevRtProfile = pHeadRtProfile;
#ifdef L3VPN
        BGP4_RT_GET_NEXT (pHeadRtProfile, u4VrfId, pHeadRtProfile);
#else
        pHeadRtProfile = BGP4_RT_NEXT (pHeadRtProfile);
#endif
    }

    if (pHeadRtProfile == NULL)
    {
        /* Matching route not present in RIB. */
        return BGP4_FAILURE;
    }

    /* Reset the Best Route Flag */
    BGP4_RT_RESET_FLAG (pHeadRtProfile, BGP4_RT_BEST);

    if ((BGP4_RT_NEXT ((*pRtProfileList)) == NULL) &&
        (BGP4_GLOBAL_STATE (u4VrfId) != BGP4_GLOBAL_ADMIN_DOWN_INPROGRESS) &&
        (PrefixMatch
         (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
          (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)), InvPrefix) != BGP4_TRUE)
        && (BGP4_RT_PREFIXLEN (pRtProfile) != 0))
    {
        /* pHeadRtProfile points to the route that needs to be removed from the
         * RIB. Before removing this route from the RIB check whether the route
         * is stored in the peer's init route info and do appropriate action.
         */
        Bgp4PeerUpdPeerInitRtInfo (pHeadRtProfile, pTreeNode, FALSE);

        /* Update the Sync Init list if synchronisation is enabled. */
        if (BGP4_LOCAL_SYNC_STATUS (u4VrfId) == BGP4_ENABLE_SYNC)
        {
            Bgp4SyncUpdateSyncInitRoute (pHeadRtProfile, pTreeNode, FALSE);
        }

        /* Update the Next Hop change Init list */
        Bgp4UpdateNHChgInitRoute (pHeadRtProfile, u4VrfId, pTreeNode, FALSE);
        /* Update the Aggregate entry init route */
        Bgp4AggrUpdateAggrInitRoute (pHeadRtProfile, pTreeNode, FALSE);
    }
#ifdef L3VPN
    if ((BGP4_RT_NEXT ((*pRtProfileList)) == NULL) &&
        (u4VrfId == BGP4_DFLT_VRFID) &&
        (BGP4_GLOBAL_STATE (BGP4_DFLT_VRFID) !=
         BGP4_GLOBAL_ADMIN_DOWN_INPROGRESS)
        && (BGP4_RT_PREFIXLEN (pRtProfile) != 0))
    {
        /* update the vrf init info */
        Bgp4UpdateVrfChgInitInfo (pHeadRtProfile);
    }
#endif

#ifdef L3VPN
    if (u4VrfId != BGP4_DFLT_VRFID)
    {
        Bgp4Vpnv4ResetVrfFlags (pHeadRtProfile, u4VrfId,
                                BGP4_RT_VRF_NEWUPDATE_TOIP);
        Bgp4Vpnv4UpdPeerInitRtInfo (pHeadRtProfile, u4VrfId, FALSE);
    }
#endif
    Bgp4UpdateNHChgInitRoute (pHeadRtProfile, u4VrfId, pTreeNode, FALSE);

    if (pPrevRtProfile == NULL)
    {
        /* The first route profile is the matching one.
         * Hence change pTreeNode appropriately.
         */
#ifdef L3VPN
        BGP4_RT_GET_NEXT (pHeadRtProfile, u4VrfId, (*pRtProfileList));
#else
        (*pRtProfileList) = BGP4_RT_NEXT (pHeadRtProfile);
#endif
    }
    else
    {
#ifdef L3VPN
        BGP4_RT_GET_NEXT (pHeadRtProfile, u4VrfId, pRtNext);
        BGP4_RT_SET_NEXT (pPrevRtProfile, pRtNext, u4VrfId);
#else
        BGP4_RT_NEXT (pPrevRtProfile) = BGP4_RT_NEXT (pHeadRtProfile);
#endif
    }
#ifdef L3VPN
    BGP4_RT_SET_NEXT (pHeadRtProfile, NULL, u4VrfId);
#else
    BGP4_RT_NEXT (pHeadRtProfile) = NULL;
#endif
    if (u1ReRunDech == BGP4_TRUE)
    {
        /* Re-run the Dech process. */
        Bgp4DechReRunDecProcess (pRtProfileList, u4VrfId);
    }
    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_HISTORY) == BGP4_RT_HISTORY)
    {
        /* Route needs to be maintained in RIB as HISTORY route.
         * So re-add the route at the end of the list. */
        if ((*pRtProfileList) == NULL)
        {
            /* This is the only route in the NODE */
            (*pRtProfileList) = pRtProfile;
        }
        else
        {
            pHeadRtProfile = (*pRtProfileList);
#ifdef L3VPN
            BGP4_RT_GET_NEXT (pHeadRtProfile, u4VrfId, pRtNext);
            while (pRtNext != NULL)
            {
                pHeadRtProfile = pRtNext;
                BGP4_RT_GET_NEXT (pHeadRtProfile, u4VrfId, pRtNext);
            }
            BGP4_RT_SET_NEXT (pHeadRtProfile, pRtProfile, u4VrfId);
#else
            while (BGP4_RT_NEXT (pHeadRtProfile) != NULL)
            {
                pHeadRtProfile = BGP4_RT_NEXT (pHeadRtProfile);
            }
            BGP4_RT_NEXT (pHeadRtProfile) = pRtProfile;
#endif
        }
    }
    else
    {
#ifdef RFD_WANTED
        /* Route is not History route. It will be removed from the RIB. So
         * if any Damp History is present for this Route, then clear it. */
        if (BGP4_RT_DAMP_HIST (pRtProfile) != NULL)
        {
            Bgp4TmrhStopTimer (BGP4_ROUTE_REUSE_TIMER,
                               (VOID *) pRtProfile->pRtDampHist);
            Bgp4MemReleaseRfdDampHist (BGP4_RT_DAMP_HIST (pRtProfile));
            BGP4_RT_DAMP_HIST (pRtProfile) = NULL;
            Bgp4DshReleaseRtInfo (pRtProfile);
        }
#endif
        /* Release the Route Profile and update RIB route count. */
        BGP4_RIB_ROUTES_COUNT (u4VrfId)--;
        Bgp4DshReleaseRtInfo (pRtProfile);
        BGP4_RIB_ROUTE_CNT (u4VrfId)--;
        if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
        {
            BGP4_TABLE_VERSION (u4VrfId)++;
            Bgp4RedSyncTableVersion (u4VrfId, BGP4_TABLE_VERSION (u4VrfId));
        }
#ifdef L3VPN
        if (u4VrfId != BGP4_DFLT_VRFID)
        {
            Bgp4Vpnv4ResetVrfFlags (pRtProfile, u4VrfId,
                                    BGP4_RT_PRESENT_IN_VRF);
        }
#endif
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4DshReleaseAsPathList                                  */
/* Description   : This function is called to release all AS nodes in the AS */
/*                 Path List.                                                */
/* Input(s)      : List of AS nodes (pAsList)                                */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4DshReleaseAsPathList (tTMO_SLL * pAsList)
{
    tAsPath            *pAspath = NULL;

    if ((pAsList == NULL) || (TMO_SLL_Count (pAsList) == 0))
    {
        return BGP4_SUCCESS;
    }

    for (;;)
    {
        pAspath = (tAsPath *) TMO_SLL_Last (pAsList);

        if (pAspath == NULL)
        {
            break;
        }

        TMO_SLL_Delete (pAsList, &pAspath->sllNode);
        Bgp4MemReleaseASNode (pAspath);
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4DshReleaseBgpInfo                                     */
/* Description   : Frees the BGP Information sturcture specified. Reference  */
/*                 count of the BGP Info. is decremented and if it reaches   */
/*                 zero then the associated AS_PATH and the unknown fields   */
/*                 are freed followed by freeing of the BGP Information.     */
/* Input(s)      : BGP Information which needs to be freed (pBgpInfo).       */
/* Output(s)     : None.                                                     */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
VOID
Bgp4DshReleaseBgpInfo (tBgp4Info * pBgpInfo)
{
    tAsPath            *pAspath = NULL;
    tSnpaInfoNode      *pSnpaInfoNode = NULL;

    if (pBgpInfo != NULL)
    {
        if (BGP4_INFO_REF_COUNT (pBgpInfo) > 0)
        {
            BGP4_INFO_REF_COUNT (pBgpInfo)--;
        }

        if (BGP4_INFO_REF_COUNT (pBgpInfo) == 0)
        {
            for (;;)
            {
                pAspath =
                    (tAsPath *) TMO_SLL_Last (BGP4_INFO_ASPATH (pBgpInfo));
                if (pAspath == NULL)
                {
                    break;
                }
                TMO_SLL_Delete (BGP4_INFO_ASPATH (pBgpInfo), &pAspath->sllNode);
                Bgp4MemReleaseASNode (pAspath);
            }

            for (; BGP4_MPE_SNPA_INFO_LIST (pBgpInfo) != NULL;)
            {
                /* Clear all the SNPA-Nodes in the list. */
                pSnpaInfoNode =
                    (tSnpaInfoNode *)
                    TMO_SLL_Last (BGP4_MPE_SNPA_INFO_LIST (pBgpInfo));
                if (pSnpaInfoNode == NULL)
                {
                    break;
                }
                TMO_SLL_Delete (BGP4_MPE_SNPA_INFO_LIST (pBgpInfo),
                                &pSnpaInfoNode->TSNextSNPA);
                Bgp4MpeReleaseSnpaNode (pSnpaInfoNode);
            }
            TMO_SLL_Init (BGP4_MPE_SNPA_INFO_LIST (pBgpInfo));
            /* Free Aggregator Attribute. */
            if (BGP4_INFO_AGGREGATOR (pBgpInfo) != NULL)
            {
                AGGR_NODE_FREE (BGP4_INFO_AGGREGATOR (pBgpInfo));
                BGP4_INFO_AGGREGATOR (pBgpInfo) = NULL;
            }

            /* Free Community Attribute. */
            if (BGP4_INFO_COMM_ATTR (pBgpInfo) != NULL)
            {
                ATTRIBUTE_NODE_FREE (BGP4_INFO_COMM_ATTR_VAL (pBgpInfo));
                COMMUNITY_NODE_FREE (BGP4_INFO_COMM_ATTR (pBgpInfo));
                BGP4_INFO_COMM_ATTR (pBgpInfo) = NULL;
            }

            /* Free Extended Community Attribute. */
            if (BGP4_INFO_ECOMM_ATTR (pBgpInfo) != NULL)
            {
                if (BGP4_INFO_ECOMM_ATTR_VAL (pBgpInfo) != NULL)
                {
                    ATTRIBUTE_NODE_FREE (BGP4_INFO_ECOMM_ATTR_VAL (pBgpInfo));
                }
                EXT_COMMUNITY_NODE_FREE (BGP4_INFO_ECOMM_ATTR (pBgpInfo));
                BGP4_INFO_ECOMM_ATTR (pBgpInfo) = NULL;
            }

            /* Free Cluster List Attribute. */
            if (BGP4_INFO_CLUS_LIST_ATTR (pBgpInfo) != NULL)
            {
                ATTRIBUTE_NODE_FREE (BGP4_INFO_CLUS_LIST_ATTR_VAL (pBgpInfo));
                CLUSTER_LIST_FREE (BGP4_INFO_CLUS_LIST_ATTR (pBgpInfo));
                BGP4_INFO_CLUS_LIST_ATTR (pBgpInfo) = NULL;
            }

            if (BGP4_INFO_UNKNOWN_ATTR (pBgpInfo) != NULL)
            {
                ATTRIBUTE_NODE_FREE (BGP4_INFO_UNKNOWN_ATTR (pBgpInfo));
                BGP4_INFO_UNKNOWN_ATTR (pBgpInfo) = NULL;
                BGP4_INFO_UNKNOWN_ATTR_LEN (pBgpInfo) = 0;
            }
            BGP4_INFO_ATTR_FLAG (pBgpInfo) = 0;
            BGP4_INFO_RCVD_PA_ENTRY (pBgpInfo) = NULL;
            Bgp4MemReleaseBgp4info (pBgpInfo);
        }
    }
}

/*****************************************************************************/
/* Function Name : Bgp4DshReleaseRtInfo                                      */
/* Description   : Frees the Route Profile sturcture specified. Reference    */
/*                 count of the Route Profile  is decremented and if it      */
/*                 reaches zero then the associated  BGP Info. and this route*/
/*                 profiles are freed.                                       */
/* Input(s)      : Route Profile which needs to be freed (pRtProfile).       */
/* Output(s)     : None.                                                     */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
VOID
Bgp4DshReleaseRtInfo (tRouteProfile * pRtProfile)
{
#ifdef L3VPN
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
    UINT4               u4VrfId = 0;
    UINT1               u1FlagDel = BGP4_FALSE;
#endif

    if (pRtProfile != NULL)
    {
        if (BGP4_RT_REF_COUNT (pRtProfile) > 0)
        {
            BGP4_RT_REF_COUNT (pRtProfile)--;
        }

        if (BGP4_RT_REF_COUNT (pRtProfile) == 0)
        {
            Bgp4DshReleaseBgpInfo (BGP4_RT_BGP_INFO (pRtProfile));
#ifdef L3VPN
            while (TMO_SLL_Count
                   (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile)) > 0)
            {
                /* Clear all the installed vrf nodes in the list. */
                pRtVrfInfo =
                    (tRtInstallVrfInfo *)
                    TMO_SLL_Last (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile));
                if (pRtVrfInfo == NULL)
                {
                    break;
                }
                TMO_SLL_Delete (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile),
                                (tTMO_SLL_NODE *) pRtVrfInfo);
                Bgp4MemReleaseVpnRtInstallVrfNode (pRtVrfInfo);
            }
            TMO_SLL_Init (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile));

            if (BGP4_RT_LABEL_CNT (pRtProfile) > 0)
            {
                if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
                {
                    if (BGP4_VPN4_PEER_ROLE (BGP4_RT_PEER_ENTRY (pRtProfile))
                        == BGP4_VPN4_CE_PEER)
                    {
                        u4VrfId =
                            BGP4_PEER_CXT_ID (BGP4_RT_PEER_ENTRY (pRtProfile));
                        u1FlagDel = BGP4_TRUE;
                    }
                    /* For routes learnt from PE peers, the label was advertised by
                     * the remote peer. so no need to release it here. */
                }
                else
                {
                    u4VrfId = BGP4_RT_CXT_ID (pRtProfile);
                    u1FlagDel = BGP4_TRUE;
                }
                if (u1FlagDel == BGP4_TRUE)
                {
                    if ((u4VrfId != 0) && (BGP4_VPNV4_LABEL (u4VrfId) == 0))
                    {
                        Bgp4DelBgp4Label (BGP4_RT_LABEL_INFO (pRtProfile));
                    }
                }
            }
#endif
            Bgp4MemReleaseRouteProfile (pRtProfile);
        }
    }
}

/*****************************************************************************/
/* Function Name : Bgp4DshAllocateLinkNode                                   */
/* Description   : Allocates memory for the LINKNODE data sturcture. It uses */
/*                 Memory handler module to get this data sturcture.         */
/* Input(s)      : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : Pointer to tLinkNode sturcture.                           */
/*****************************************************************************/
tLinkNode          *
Bgp4DshAllocateLinkNode ()
{
    tLinkNode          *pLinkNd = NULL;

    pLinkNd = (Bgp4MemAllocateLinkNode (sizeof (tLinkNode)));

    if (pLinkNd == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for Link Node FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_LINK_NOCDES_SIZING_ID]++;
    }
    return pLinkNd;
}

/*****************************************************************************/
/* Function Name : Bgp4DshAllocateAdvtRtLinkNode                             */
/* Description   : Allocates memory for the tAdvRtLinkNode data structure.   */
/* Input(s)      : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : Pointer to tAdvRtLinkNode structure.                      */
/*****************************************************************************/
tAdvRtLinkNode     *
Bgp4DshAllocateAdvtRtLinkNode ()
{
    tAdvRtLinkNode     *pAdvRtLinkNd = NULL;

    pAdvRtLinkNd = (Bgp4MemAllocateAdvRtLinkNode (sizeof (tAdvRtLinkNode)));

    if (pAdvRtLinkNd == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Alloc for Advertised Route Link Node FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_ADVT_LINK_NODES_SIZING_ID]++;
    }
    return pAdvRtLinkNd;
}

/*****************************************************************************/
/* Function Name : Bgp4DshReleaseLinkNode                                    */
/* Description   : Releases the memory allocated for the link node as well as*/
/*                 the Route profile associated with that link node.         */
/* Input(s)      : Link Node which needs to be freed (pLinkNode).            */
/* Output(s)     : None.                                                     */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
VOID
Bgp4DshReleaseLinkNode (tLinkNode * pLinkNode)
{
    if (pLinkNode != NULL)
    {
        Bgp4DshReleaseRtInfo (pLinkNode->pRouteProfile);
        if (pLinkNode->pRtInfo != NULL)
        {
            Bgp4DshReleaseBgpInfo (pLinkNode->pRtInfo);
            pLinkNode->pRtInfo = NULL;
        }
        Bgp4MemReleaseLinkNode (pLinkNode);
        /* pLinkNode = NULL; */
    }
}

/*****************************************************************************/
/* Function Name : Bgp4DshReleaseAdvRtLinkNode                               */
/* Description   : Releases the memory allocated for the link node as well as*/
/*                 the Route profile associated with that link node.         */
/* Input(s)      : Adv Link Node which needs to be freed (pAdvLinkNode).     */
/* Output(s)     : None.                                                     */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
VOID
Bgp4DshReleaseAdvRtLinkNode (tAdvRtLinkNode * pAdvLinkNode)
{
    if (pAdvLinkNode != NULL)
    {
        Bgp4DshReleaseRtInfo (pAdvLinkNode->pRouteProfile);
        if (pAdvLinkNode->pRtInfo != NULL)
        {
            Bgp4DshReleaseBgpInfo (pAdvLinkNode->pRtInfo);
            pAdvLinkNode->pRtInfo = NULL;
        }
        Bgp4MemReleaseAdvtLinkNode (pAdvLinkNode);
    }
}

/*****************************************************************************/
/* Function Name : Bgp4DshReleaseList                                        */
/* Description   : Releases the given list of route profiles.                */
/* Input(s)      : List of route profiles (pTsList)                          */
/*               : No of route to be deleted (u4RtCnt). If this value is 0,  */
/*               : then all the routes in the list will be freed. Else the   */
/*               : the exact number of nodes specified will be freed.        */
/* Output(s)     : None.                                                     */
/* Return(s)     : Number of Nodes removed.                                  */
/*****************************************************************************/
UINT4
Bgp4DshReleaseList (tTMO_SLL * pTsList, UINT4 u4RtCnt)
{
    tLinkNode          *pLinkNode = NULL;
    UINT4               u4Count = 0;

    if (TMO_SLL_Count (pTsList) == 0)
    {
        return u4Count;
    }

    for (u4Count = 0; ((u4RtCnt == 0) || (u4Count < u4RtCnt)); u4Count++)
    {
        pLinkNode = (tLinkNode *) TMO_SLL_First (pTsList);

        if (pLinkNode != NULL)
        {
            TMO_SLL_Delete (pTsList, &pLinkNode->TSNext);
            Bgp4DshReleaseLinkNode (pLinkNode);
            /* pLinkNode = NULL; */
        }
        else
        {
            break;
        }
    }
    if (TMO_SLL_Count (pTsList) == 0)
    {
        TMO_SLL_Init (pTsList);
    }
    return u4Count;
}

/*****************************************************************************/
/* Function Name : Bgp4DshReleaseNumRtsFromList                              */
/* Description   : Releases the given list of route profiles for the specified*/
/*                 number of routes.                                         */
/* Input(s)      : List of route profiles (pTsList)                          */
/* Output(s)     : None.                                                     */
/* Return(s)     : Num routes released from list                             */
/*****************************************************************************/
INT4
Bgp4DshReleaseNumRtsFromList (tTMO_SLL * pTsList, UINT4 u4RtCnt)
{
    tLinkNode          *pLinkNode = NULL;
    UINT4               u4Cnt = 0;

    if (TMO_SLL_Count (pTsList) == 0)
    {
        return 0;
    }

    for (;;)
    {
        pLinkNode = (tLinkNode *) TMO_SLL_First (pTsList);

        if (pLinkNode != NULL)
        {
            TMO_SLL_Delete (pTsList, &pLinkNode->TSNext);
            Bgp4DshReleaseLinkNode (pLinkNode);
            u4Cnt++;
            if (u4Cnt >= u4RtCnt)
            {
                return ((INT4) u4Cnt);
            }
            /* pLinkNode = NULL; */
        }
        else
        {
            break;
        }
    }
    TMO_SLL_Init (pTsList);
    return ((INT4) u4Cnt);
}

/*****************************************************************************/
/* Function Name : Bgp4DshGetDeniedRoutesFromOverlapList                     */
/* Description   : This routine depanding upon the overlap route policy      */
/*                 will find the more or less specific route for the given   */
/*                 Route Profile and update the given route list(pList)      */
/* Input(s)      : Route Profile (pRtProfile)                                */
/*                 Overlap Route Policy (u4Policy)                           */
/* Output(s)     : Updated List of route profiles (pList)                    */
/* Return(s)     : BGP4_SUCCESS.                                             */
/*****************************************************************************/
INT4
Bgp4DshGetDeniedRoutesFromOverlapList (tTMO_SLL * pList,
                                       tRouteProfile * pRtProfile,
                                       UINT4 u4Policy)
{
    if (u4Policy == BGP4_INSTALL_MORESPEC_RT)
    {
        Bgp4DshGetDeniedMoreSpecRoutesFromOverlapList (pList, pRtProfile);
    }
    else if (u4Policy == BGP4_INSTALL_LESSSPEC_RT)
    {
        Bgp4DshGetDeniedLessSpecRoutesFromOverlapList (pList, pRtProfile);
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4DshGetDeniedLessSpecRoutesFromOverlapList             */
/* Description   : This routine will find the less specific route for given  */
/*                 Route Profile and update the given Feasible route list.   */
/* Input(s)      : List of route profiles (pList)                            */
/*                 Route Profile (pRtProfile)                                */
/* Output(s)     : Updated List of route profiles (pList)                    */
/* Return(s)     : BGP4_SUCCESS                                              */
/* Notes         : Current policy is INSTALL LESS SPECIFIC routes. So right  */
/*                 now only less specific routes will be installed in the    */
/*                 and the routes in the overlap list will be more specific  */
/*                 to the routes in the tree. So on deletion of the given    */
/*                 route profile from tree will return the list of routes    */
/*                 which are less specific among the routes in the overlap   */
/*                 list that are more specific to the deleted route          */
/*****************************************************************************/
INT4
Bgp4DshGetDeniedLessSpecRoutesFromOverlapList (tTMO_SLL * pList,
                                               tRouteProfile * pRtProfile)
{
    tLinkNode          *pScanLinkNode = NULL;
    tRouteProfile      *pRouteProfile = NULL;
    tAddrPrefix         InputPrefix;
    tAddrPrefix         ScanPrefix;
    tAddrPrefix         RefPrefixInfo;
    UINT4               u4Context = 0;
    UINT1               u1PrefixLen = 0;
    UINT1               u1RefPrefixLen = 0;
    UINT1               u1IsLessSpecRouteFound = BGP4_FALSE;

    if ((pList == NULL) || (pRtProfile == NULL))
    {
        return BGP4_SUCCESS;
    }

    u4Context = BGP4_RT_CXT_ID (pRtProfile);
    /* Get the scan prefix information */
    Bgp4CopyAddrPrefixStruct (&InputPrefix,
                              BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                              (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)));
    u1PrefixLen = (UINT1) BGP4_RT_PREFIXLEN (pRtProfile);

    TMO_SLL_Scan (BGP4_OVERLAP_ROUTE_LIST (u4Context), pScanLinkNode,
                  tLinkNode *)
    {
        pRouteProfile = pScanLinkNode->pRouteProfile;
        Bgp4CopyAddrPrefixStruct (&ScanPrefix,
                                  BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                  (BGP4_RT_NET_ADDRESS_INFO (pRouteProfile)));
        switch (BGP4_RT_AFI_INFO (pRouteProfile))
        {
            case BGP4_INET_AFI_IPV4:
            {
                UINT4               u4Prefix;
                UINT4               u4Mask;

                PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pRtProfile));
                u4Mask = Bgp4GetSubnetmask (u1PrefixLen);
                u4Prefix = u4Prefix & u4Mask;
                PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                              (ScanPrefix), u4Prefix);
            }
                break;
#ifdef BGP4_IPV6_WANTED
            case BGP4_INET_AFI_IPV6:
            {
                tIp6Addr            IPAddr;
                tIp6Addr            IPAddr1;

                MEMCPY (&IPAddr1.u1_addr[0], BGP4_RT_IP_PREFIX (pRtProfile),
                        BGP4_IPV6_PREFIX_LEN);
                Ip6CopyAddrBits (&IPAddr, &IPAddr1, u1PrefixLen);
                MEMCPY (ScanPrefix.au1Address, &IPAddr.u1_addr[0],
                        BGP4_IPV6_PREFIX_LEN);
            }
                break;
#endif
            default:
                return BGP4_SUCCESS;
        }

        if (PrefixGreaterThan (InputPrefix, ScanPrefix) == BGP4_TRUE)
        {
            continue;
        }
        else if (PrefixMatch (InputPrefix, ScanPrefix) == BGP4_TRUE)
        {
            if (u1PrefixLen > BGP4_RT_PREFIXLEN (pRouteProfile))
            {
                /* In this case, the given deleted route is more specific than 
                 * route present in the overlap list. This happens only in 
                 * case of policy change from more specific to less specific.
                 * This route should not be added to the given list. 
                 */
                continue;
            }
            if (u1IsLessSpecRouteFound == BGP4_FALSE)
            {
                Bgp4CopyAddrPrefixStruct (&RefPrefixInfo,
                                          BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                          (BGP4_RT_NET_ADDRESS_INFO
                                           (pRouteProfile)));
                u1RefPrefixLen = (UINT1) BGP4_RT_PREFIXLEN (pRouteProfile);
                u1IsLessSpecRouteFound = BGP4_TRUE;
            }
            if (u1IsLessSpecRouteFound == BGP4_TRUE)
            {
                if (((PrefixMatch (RefPrefixInfo,
                                   BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                   (BGP4_RT_NET_ADDRESS_INFO (pRouteProfile))))
                     == BGP4_TRUE))
                {
                    BGP4_RT_RESET_FLAG (pRouteProfile, BGP4_RT_OVERLAP);
                    Bgp4DshAddRouteToList (pList, pRouteProfile, 0);
                }
                else
                {
                    tAddrPrefix         RtPrefix;

                    Bgp4CopyAddrPrefixStruct (&RtPrefix,
                                              BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                              (BGP4_RT_NET_ADDRESS_INFO
                                               (pRouteProfile)));
                    switch (BGP4_RT_AFI_INFO (pRouteProfile))
                    {
                        case BGP4_INET_AFI_IPV4:
                        {
                            UINT4               u4RtPrefix;
                            UINT4               u4RefMask;

                            u4RefMask = Bgp4GetSubnetmask (u1RefPrefixLen);
                            PTR_FETCH_4 (u4RtPrefix,
                                         BGP4_RT_IP_PREFIX (pRouteProfile));
                            u4RtPrefix = u4RtPrefix & u4RefMask;
                            PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                                          (RtPrefix), u4RtPrefix);
                        }
                            break;
#ifdef BGP4_IPV6_WANTED
                        case BGP4_INET_AFI_IPV6:
                        {
                            tIp6Addr            IPAddr;
                            tIp6Addr            IPAddr1;

                            MEMCPY (&IPAddr1.u1_addr[0],
                                    BGP4_RT_IP_PREFIX (pRouteProfile),
                                    BGP4_IPV6_PREFIX_LEN);
                            Ip6CopyAddrBits (&IPAddr, &IPAddr1, u1RefPrefixLen);
                            MEMCPY (RtPrefix.au1Address,
                                    &IPAddr.u1_addr[0], BGP4_IPV6_PREFIX_LEN);
                        }
                            break;
#endif
                        default:
                            return BGP4_SUCCESS;
                    }

                    if ((PrefixMatch (RefPrefixInfo, RtPrefix)) != BGP4_TRUE)
                    {
                        BGP4_RT_RESET_FLAG (pRouteProfile, BGP4_RT_OVERLAP);
                        Bgp4DshAddRouteToList (pList, pRouteProfile, 0);
                        Bgp4CopyAddrPrefixStruct (&RefPrefixInfo,
                                                  BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                                  (BGP4_RT_NET_ADDRESS_INFO
                                                   (pRouteProfile)));
                        u1RefPrefixLen =
                            (UINT1) BGP4_RT_PREFIXLEN (pRouteProfile);
                    }
                }
            }
        }
        else if (PrefixLessThan (InputPrefix, ScanPrefix) == BGP4_TRUE)
        {
            break;
        }
    }
    TMO_SLL_Scan (pList, pScanLinkNode, tLinkNode *)
    {
        pRouteProfile = pScanLinkNode->pRouteProfile;
        Bgp4DshRemoveRouteFromList (BGP4_OVERLAP_ROUTE_LIST (u4Context),
                                    pRouteProfile);
        /* Delete the Old invalid route from RIB also. */
        Bgp4RibhDelRtEntry (pRouteProfile, NULL, u4Context, BGP4_TRUE);
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4DshGetDeniedMoreSpecRoutesFromOverlapList             */
/* Description   : This routine will find the more specific route for given  */
/*                 Route Profile and update the given Feasible route list.   */
/* Input(s)      : Route Profile (pRtProfile)                                */
/* Output(s)     : Updated List of route profiles (pList)                    */
/* Return(s)     : BGP4_SUCCESS.                                             */
/* Notes         : Current policy is INSTALL More SPECIFIC routes. So right  */
/*                 now only more specific routes will be installed in the    */
/*                 and the routes in the overlap list will be less specific  */
/*                 to the routes in the tree. So on deletion of the given    */
/*                 route profile from tree will return the list of routes    */
/*                 which are more specific among the routes in the overlap   */
/*                 list that are less specific to the deleted route          */

/*****************************************************************************/
INT4
Bgp4DshGetDeniedMoreSpecRoutesFromOverlapList (tTMO_SLL * pList,
                                               tRouteProfile * pRtProfile)
{
    tTMO_SLL            TsTempList;
    tLinkNode          *pScanLinkNode = NULL;
    tRouteProfile      *pRouteProfile = NULL;
    tAddrPrefix         InputPrefixInfo;
    tAddrPrefix         ScanPrefix;
    UINT4               u4Context = 0;
    UINT1               u1PrefixLen = 0;
    UINT1               u1IsMoreSpecRouteFound = BGP4_FALSE;

    if ((pList == NULL) || (pRtProfile == NULL))
    {
        return BGP4_SUCCESS;
    }

    Bgp4CopyAddrPrefixStruct (&InputPrefixInfo,
                              BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                              (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)));

    TMO_SLL_Init (&TsTempList);

    u4Context = BGP4_RT_CXT_ID (pRtProfile);
    TMO_SLL_Scan (BGP4_OVERLAP_ROUTE_LIST (u4Context), pScanLinkNode,
                  tLinkNode *)
    {
        pRouteProfile = pScanLinkNode->pRouteProfile;
        u1PrefixLen = (UINT1) BGP4_RT_PREFIXLEN (pRouteProfile);
        Bgp4CopyAddrPrefixStruct (&ScanPrefix,
                                  BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                  (BGP4_RT_NET_ADDRESS_INFO (pRouteProfile)));
        switch (BGP4_RT_AFI_INFO (pRouteProfile))
        {
            case BGP4_INET_AFI_IPV4:
            {
                UINT4               u4Prefix;
                UINT4               u4InputPrefix;
                UINT4               u4Mask;

                PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pRouteProfile));
                u4Mask = Bgp4GetSubnetmask (u1PrefixLen);
                u4Prefix = u4Prefix & u4Mask;
                PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                              (ScanPrefix), u4Prefix);
                PTR_FETCH_4 (u4InputPrefix, BGP4_RT_IP_PREFIX (pRtProfile));
                u4InputPrefix = u4InputPrefix & u4Mask;
                PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                              (InputPrefixInfo), u4InputPrefix);
            }
                break;
#ifdef BGP4_IPV6_WANTED
            case BGP4_INET_AFI_IPV6:
            {
                tIp6Addr            IPAddr;
                tIp6Addr            IPAddr1;
                tIp6Addr            IPInputAddr;
                tIp6Addr            IPInputAddr1;

                MEMCPY (&IPAddr1.u1_addr[0], BGP4_RT_IP_PREFIX (pRouteProfile),
                        BGP4_IPV6_PREFIX_LEN);
                Ip6CopyAddrBits (&IPAddr, &IPAddr1, u1PrefixLen);
                MEMCPY (ScanPrefix.au1Address, &IPAddr.u1_addr[0],
                        BGP4_IPV6_PREFIX_LEN);
                MEMCPY (&IPInputAddr1.u1_addr[0],
                        BGP4_RT_IP_PREFIX (pRtProfile), BGP4_IPV6_PREFIX_LEN);
                Ip6CopyAddrBits (&IPInputAddr, &IPAddr1, u1PrefixLen);
                MEMCPY (InputPrefixInfo.au1Address, &IPInputAddr.u1_addr[0],
                        BGP4_IPV6_PREFIX_LEN);
            }
                break;
#endif
            default:
                Bgp4DshReleaseList (&TsTempList, 0);
                return BGP4_SUCCESS;
        }

        if (PrefixGreaterThan (InputPrefixInfo, ScanPrefix) == BGP4_TRUE)
        {
            /* Non overlapping route */
            continue;
        }
        else if (PrefixMatch (InputPrefixInfo, ScanPrefix) == BGP4_TRUE)
        {
            if (u1PrefixLen > BGP4_RT_PREFIXLEN (pRtProfile))
            {
                /* In this case, the given deleted route is less specific than 
                 * route present in the overlap list. This happens only in 
                 * case of policy change from less specific to more specific.
                 * This route should not be added to the given list. 
                 */
                continue;
            }
            Bgp4DshAddRouteToList (&TsTempList, pRouteProfile, BGP4_PREPEND);
            continue;
        }
        else if (PrefixLessThan (InputPrefixInfo, ScanPrefix) == BGP4_TRUE)
        {
            break;
        }
    }

    TMO_SLL_Scan (&TsTempList, pScanLinkNode, tLinkNode *)
    {
        pRouteProfile = pScanLinkNode->pRouteProfile;
        if (u1IsMoreSpecRouteFound == BGP4_FALSE)
        {
            Bgp4CopyAddrPrefixStruct (&ScanPrefix,
                                      BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                      (BGP4_RT_NET_ADDRESS_INFO
                                       (pRouteProfile)));
            u1PrefixLen = (UINT1) BGP4_RT_PREFIXLEN (pRouteProfile);
            u1IsMoreSpecRouteFound = BGP4_TRUE;
        }
        if (u1IsMoreSpecRouteFound == BGP4_TRUE)
        {
            if (u1PrefixLen == BGP4_RT_PREFIXLEN (pRouteProfile))
            {
                BGP4_RT_RESET_FLAG (pRouteProfile, BGP4_RT_OVERLAP);
                Bgp4DshAddRouteToList (pList, pRouteProfile, 0);
            }
            else if (u1PrefixLen > BGP4_RT_PREFIXLEN (pRouteProfile))
            {
                break;
            }
        }
    }
    Bgp4DshReleaseList (&TsTempList, 0);
    TMO_SLL_Scan (pList, pScanLinkNode, tLinkNode *)
    {
        pRouteProfile = pScanLinkNode->pRouteProfile;
        Bgp4DshRemoveRouteFromList (BGP4_OVERLAP_ROUTE_LIST (u4Context),
                                    pRouteProfile);
        /* Delete the Old invalid route from RIB also. */
        Bgp4RibhDelRtEntry (pRouteProfile, NULL, u4Context, BGP4_TRUE);
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4DshGetLessSpecRoutesFromOverlapList                   */
/* Description   : This routine will find the less specific route for given  */
/*                 more specific Route Profile and update the given Feasible */
/*                 route list and returns less specific overlapping route    */
/* Input(s)      : Route Profile (pRtProfile)                                */
/* Output(s)     : Updated List of route profiles (pList)                    */
/* Return(s)     : Less specific overlapping Route                           */
/*****************************************************************************/
tRouteProfile      *
Bgp4DshGetLessSpecRoutesFromOverlapList (tTMO_SLL * pList,
                                         tRouteProfile * pRtProfile)
{
    tLinkNode          *pScanLinkNode = NULL;
    tRouteProfile      *pRouteProfile = NULL;
    tRouteProfile      *pLessSpecRtProfile = NULL;
    tAddrPrefix         InputPrefixInfo;
    tAddrPrefix         RefPrefixInfo;
    tAddrPrefix         ScanPrefix;
    UINT4               u4Context = 0;
    UINT1               u1PrefixLen = 0;
    UINT1               u1RefPrefixLen = 0;
    UINT1               u1IsLessSpecRouteFound = BGP4_FALSE;

    if ((pList == NULL) || (pRtProfile == NULL))
    {
        return ((tRouteProfile *) NULL);
    }

    Bgp4CopyAddrPrefixStruct (&InputPrefixInfo,
                              BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                              (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)));

    u4Context = BGP4_RT_CXT_ID (pRtProfile);
    TMO_SLL_Scan (BGP4_OVERLAP_ROUTE_LIST (u4Context), pScanLinkNode,
                  tLinkNode *)
    {
        pRouteProfile = pScanLinkNode->pRouteProfile;
        u1PrefixLen = (UINT1) BGP4_RT_PREFIXLEN (pRouteProfile);
        Bgp4CopyAddrPrefixStruct (&ScanPrefix,
                                  BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                  (BGP4_RT_NET_ADDRESS_INFO (pRouteProfile)));
        switch (BGP4_RT_AFI_INFO (pRouteProfile))
        {
            case BGP4_INET_AFI_IPV4:
            {
                UINT4               u4Prefix;
                UINT4               u4Mask;

                PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pRtProfile));
                u4Mask = Bgp4GetSubnetmask (u1PrefixLen);
                u4Prefix = u4Prefix & u4Mask;
                PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                              (InputPrefixInfo), u4Prefix);
            }
                break;
#ifdef BGP4_IPV6_WANTED
            case BGP4_INET_AFI_IPV6:
            {
                tIp6Addr            IPAddr;
                tIp6Addr            IPAddr1;

                MEMCPY (&IPAddr1.u1_addr[0], BGP4_RT_IP_PREFIX (pRouteProfile),
                        BGP4_IPV6_PREFIX_LEN);
                Ip6CopyAddrBits (&IPAddr, &IPAddr1,
                                 BGP4_RT_PREFIXLEN (pRouteProfile));
                MEMCPY (BGP4_RT_IP_PREFIX (pRouteProfile), &IPAddr.u1_addr[0],
                        BGP4_IPV6_PREFIX_LEN);
            }
                break;
#endif
            default:
                return BGP4_SUCCESS;
        }

        if ((PrefixGreaterThan (InputPrefixInfo,
                                BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                (BGP4_RT_NET_ADDRESS_INFO (pRouteProfile)))) ==
            BGP4_TRUE)
        {
            continue;
        }
        else if ((PrefixMatch (InputPrefixInfo,
                               BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                               (BGP4_RT_NET_ADDRESS_INFO (pRouteProfile)))) ==
                 BGP4_TRUE)
        {
            if (u1IsLessSpecRouteFound == BGP4_FALSE)
            {
                Bgp4CopyAddrPrefixStruct (&RefPrefixInfo,
                                          BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                          (BGP4_RT_NET_ADDRESS_INFO
                                           (pRouteProfile)));
                u1RefPrefixLen = (UINT1) BGP4_RT_PREFIXLEN (pRouteProfile);
                /* Fill the less specific route for return value */
                pLessSpecRtProfile = pRouteProfile;
                u1IsLessSpecRouteFound = BGP4_TRUE;
            }
            if (u1IsLessSpecRouteFound == BGP4_TRUE)
            {
                if ((PrefixMatch (RefPrefixInfo,
                                  BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                  (BGP4_RT_NET_ADDRESS_INFO (pRouteProfile))))
                    == BGP4_TRUE)
                {
                    BGP4_RT_RESET_FLAG (pRouteProfile, BGP4_RT_OVERLAP);
                    Bgp4DshAddRouteToList (pList, pRouteProfile, 0);
                }
                else
                {
                    tAddrPrefix         RtPrefix;

                    Bgp4CopyAddrPrefixStruct (&RtPrefix,
                                              BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                              (BGP4_RT_NET_ADDRESS_INFO
                                               (pRouteProfile)));
                    switch (BGP4_RT_AFI_INFO (pRouteProfile))
                    {
                        case BGP4_INET_AFI_IPV4:
                        {
                            UINT4               u4RtPrefix;
                            UINT4               u4RefMask;

                            u4RefMask = Bgp4GetSubnetmask (u1RefPrefixLen);
                            PTR_FETCH_4 (u4RtPrefix,
                                         BGP4_RT_IP_PREFIX (pRouteProfile));
                            u4RtPrefix = u4RtPrefix & u4RefMask;
                            PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                                          (RtPrefix), u4RtPrefix);
                        }
                            break;
#ifdef BGP4_IPV6_WANTED
                        case BGP4_INET_AFI_IPV6:
                        {
                            tIp6Addr            IPAddr;
                            tIp6Addr            IPAddr1;

                            MEMCPY (&IPAddr1.u1_addr[0],
                                    BGP4_RT_IP_PREFIX (pRouteProfile),
                                    BGP4_IPV6_PREFIX_LEN);
                            Ip6CopyAddrBits (&IPAddr, &IPAddr1,
                                             BGP4_RT_PREFIXLEN (pRouteProfile));
                            MEMCPY (BGP4_RT_IP_PREFIX (pRouteProfile),
                                    &IPAddr.u1_addr[0], BGP4_IPV6_PREFIX_LEN);
                        }
                            break;
#endif
                        default:
                            return (BGP4_SUCCESS);
                    }
                    if ((PrefixMatch (RefPrefixInfo, RtPrefix)) == BGP4_TRUE)
                    {
                        BGP4_RT_RESET_FLAG (pRouteProfile, BGP4_RT_OVERLAP);
                        Bgp4DshAddRouteToList (pList, pRouteProfile, 0);
                        Bgp4CopyAddrPrefixStruct (&RefPrefixInfo,
                                                  BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                                  (BGP4_RT_NET_ADDRESS_INFO
                                                   (pRouteProfile)));
                        u1RefPrefixLen =
                            (UINT1) BGP4_RT_PREFIXLEN (pRouteProfile);
                    }
                }
            }
        }
        else if ((PrefixLessThan (ScanPrefix,
                                  BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                  (BGP4_RT_NET_ADDRESS_INFO (pRouteProfile))))
                 == BGP4_TRUE)
        {
            break;
        }
    }
    TMO_SLL_Scan (pList, pScanLinkNode, tLinkNode *)
    {
        pRouteProfile = pScanLinkNode->pRouteProfile;
        Bgp4DshRemoveRouteFromList (BGP4_OVERLAP_ROUTE_LIST (u4Context),
                                    pRouteProfile);
    }
    return (pLessSpecRtProfile);
}

/*****************************************************************************/
/* Function Name : Bgp4DshGetMoreSpecRoutesFromOverlapList                   */
/* Description   : This routine will find the more specific route for given  */
/*                 less specifice Route Profile and update the given         */
/*                 Feasible route list. Returns the more specific route      */
/* Input(s)      : Route Profile (pRtProfile)                                */
/* Output(s)     : Updated List of route profiles (pList)                    */
/* Return(s)     : More Specific overlapping route                           */
/*****************************************************************************/
tRouteProfile      *
Bgp4DshGetMoreSpecRoutesFromOverlapList (tTMO_SLL * pList,
                                         tRouteProfile * pRtProfile)
{
    tTMO_SLL            TsTempList;
    tLinkNode          *pScanLinkNode = NULL;
    tRouteProfile      *pRouteProfile = NULL;
    tRouteProfile      *pMoreSpecRtProfile = NULL;
    tAddrPrefix         ScanPrefix;
    UINT4               u4Context = 0;
    UINT1               u1PrefixLen;
    UINT1               u1IsMoreSpecRouteFound = BGP4_FALSE;

    if ((pList == NULL) || (pRtProfile == NULL))
    {
        return ((tRouteProfile *) NULL);
    }

    u1PrefixLen = (UINT1) BGP4_RT_PREFIXLEN (pRtProfile);

    TMO_SLL_Init (&TsTempList);

    u4Context = BGP4_RT_CXT_ID (pRtProfile);
    TMO_SLL_Scan (BGP4_OVERLAP_ROUTE_LIST (u4Context), pScanLinkNode,
                  tLinkNode *)
    {
        pRouteProfile = pScanLinkNode->pRouteProfile;
        Bgp4CopyAddrPrefixStruct (&ScanPrefix,
                                  BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                  (BGP4_RT_NET_ADDRESS_INFO (pRouteProfile)));
        switch (BGP4_RT_AFI_INFO (pRouteProfile))
        {
            case BGP4_INET_AFI_IPV4:
            {
                UINT4               u4Prefix;
                UINT4               u4Mask;

                PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pRouteProfile));
                u4Mask = Bgp4GetSubnetmask (u1PrefixLen);
                u4Prefix = u4Prefix & u4Mask;
                PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                              (ScanPrefix), u4Prefix);
            }
                break;
#ifdef BGP4_IPV6_WANTED
            case BGP4_INET_AFI_IPV6:
            {
                tIp6Addr            IPAddr;
                tIp6Addr            IPAddr1;

                MEMCPY (&IPAddr1.u1_addr[0], BGP4_RT_IP_PREFIX (pRouteProfile),
                        BGP4_IPV6_PREFIX_LEN);
                Ip6CopyAddrBits (&IPAddr, &IPAddr1,
                                 BGP4_RT_PREFIXLEN (pRouteProfile));
                MEMCPY (BGP4_RT_IP_PREFIX (pRouteProfile), &IPAddr.u1_addr[0],
                        BGP4_IPV6_PREFIX_LEN);
            }
                break;
#endif
            default:
                Bgp4DshReleaseList (&TsTempList, 0);
                return BGP4_SUCCESS;
        }

        if ((PrefixGreaterThan
             (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
              (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
              ScanPrefix)) == BGP4_TRUE)
        {
            continue;
        }
        else if ((PrefixMatch
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                   (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
                   ScanPrefix)) == BGP4_TRUE)
        {
            Bgp4DshAddRouteToList (&TsTempList, pRouteProfile, BGP4_PREPEND);
            continue;
        }
        else if ((PrefixLessThan
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                   (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
                   ScanPrefix)) == BGP4_TRUE)
        {
            break;
        }
    }

    TMO_SLL_Scan (&TsTempList, pScanLinkNode, tLinkNode *)
    {
        pRouteProfile = pScanLinkNode->pRouteProfile;
        if (u1IsMoreSpecRouteFound == BGP4_FALSE)
        {
            Bgp4CopyAddrPrefixStruct (&ScanPrefix,
                                      BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                      (BGP4_RT_NET_ADDRESS_INFO
                                       (pRouteProfile)));
            u1PrefixLen = (UINT1) BGP4_RT_PREFIXLEN (pRouteProfile);
            /* Fill the more specific route for returning */
            pMoreSpecRtProfile = pRouteProfile;
            u1IsMoreSpecRouteFound = BGP4_TRUE;
        }
        if (u1IsMoreSpecRouteFound == BGP4_TRUE)
        {
            if ((AddrMatch (ScanPrefix,
                            BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                            (BGP4_RT_NET_ADDRESS_INFO ((pRouteProfile))),
                            u1PrefixLen)) == BGP4_TRUE)
            {
                BGP4_RT_RESET_FLAG (pRouteProfile, BGP4_RT_OVERLAP);
                Bgp4DshAddRouteToList (pList, pRouteProfile, 0);
            }
            else
            {
                /* Check if the current route is overlapping with ref. route
                 * or not. If not, then add that route to list and make
                 * that route as reference */
                if ((AddrMatch (ScanPrefix,
                                BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                (BGP4_RT_NET_ADDRESS_INFO ((pRouteProfile))),
                                BGP4_RT_PREFIXLEN (pRouteProfile))) ==
                    BGP4_TRUE)
                {
                    continue;
                }
                BGP4_RT_RESET_FLAG (pRouteProfile, BGP4_RT_OVERLAP);
                Bgp4DshAddRouteToList (pList, pRouteProfile, 0);
                Bgp4CopyAddrPrefixStruct (&ScanPrefix,
                                          BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                          (BGP4_RT_NET_ADDRESS_INFO
                                           (pRouteProfile)));
                u1PrefixLen = (UINT1) BGP4_RT_PREFIXLEN (pRouteProfile);
            }
        }
    }
    Bgp4DshReleaseList (&TsTempList, 0);
    TMO_SLL_Scan (pList, pScanLinkNode, tLinkNode *)
    {
        pRouteProfile = pScanLinkNode->pRouteProfile;
        Bgp4DshRemoveRouteFromList (BGP4_OVERLAP_ROUTE_LIST (u4Context),
                                    pRouteProfile);
    }
    return (pMoreSpecRtProfile);
}

/*****************************************************************************/
/* Function Name : Bgp4DshCheckForMoreSpecRoutesInOverlapList                */
/* Description   : This routine will find for given less specifice Route     */
/*                 Profile, whether a more specific route is present in      */
/*                 the Overlap Route list or not.                            */
/* Input(s)      : Route Profile (pRtProfile)                                */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_TRUE  - if More specific route is found              */
/*                 BGP4_FALSE - if no more specific route is found           */
/*****************************************************************************/
UINT4
Bgp4DshCheckForMoreSpecRoutesInOverlapList (tRouteProfile * pRtProfile)
{
    tLinkNode          *pScanLinkNode = NULL;
    tRouteProfile      *pRouteProfile = NULL;
    tAddrPrefix         ScanPrefix;
    UINT4               u4Context = 0;
    UINT1               u1PrefixLen;

    if (pRtProfile == NULL)
    {
        return BGP4_FALSE;
    }

    u1PrefixLen = (UINT1) BGP4_RT_PREFIXLEN (pRtProfile);
    u4Context = BGP4_RT_CXT_ID (pRtProfile);

    TMO_SLL_Scan (BGP4_OVERLAP_ROUTE_LIST (u4Context), pScanLinkNode,
                  tLinkNode *)
    {
        pRouteProfile = pScanLinkNode->pRouteProfile;
        Bgp4CopyAddrPrefixStruct (&ScanPrefix,
                                  BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                  (BGP4_RT_NET_ADDRESS_INFO (pRouteProfile)));
        switch (BGP4_RT_AFI_INFO (pRouteProfile))
        {
            case BGP4_INET_AFI_IPV4:
            {
                UINT4               u4Prefix;
                UINT4               u4Mask;

                PTR_FETCH_4 (u4Prefix, BGP4_RT_IP_PREFIX (pRouteProfile));
                u4Mask = Bgp4GetSubnetmask (u1PrefixLen);
                u4Prefix = u4Prefix & u4Mask;
                PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                              (ScanPrefix), u4Prefix);
            }
                break;
#ifdef BGP4_IPV6_WANTED
            case BGP4_INET_AFI_IPV6:
            {
                tIp6Addr            IPAddr;
                tIp6Addr            IPAddr1;

                MEMCPY (&IPAddr1.u1_addr[0], BGP4_RT_IP_PREFIX (pRouteProfile),
                        BGP4_IPV6_PREFIX_LEN);
                Ip6CopyAddrBits (&IPAddr, &IPAddr1,
                                 BGP4_RT_PREFIXLEN (pRouteProfile));
                MEMCPY (BGP4_RT_IP_PREFIX (pRouteProfile), &IPAddr.u1_addr[0],
                        BGP4_IPV6_PREFIX_LEN);
            }
                break;
#endif
            default:
                return BGP4_SUCCESS;
        }
        if ((PrefixGreaterThan
             (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
              (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
              ScanPrefix)) == BGP4_TRUE)
        {
            continue;
        }
        else if ((PrefixMatch
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                   (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
                   ScanPrefix)) == BGP4_TRUE)
        {
            if (u1PrefixLen < BGP4_RT_PREFIXLEN (pRouteProfile))
            {
                /* More specific route is found */
                return BGP4_TRUE;
            }
            continue;
        }
        else if ((PrefixLessThan
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                   (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
                   ScanPrefix)) == BGP4_TRUE)
        {
            break;
        }
    }

    return BGP4_FALSE;
}

/*****************************************************************************/
/* Function Name : Bgp4DshAddRouteToOverlapList                              */
/* Description   : This routine adds the route into the overlap route list.  */
/*                 The route profiles are sorted in the ascending order      */
/*                 based on Prefix and Prefix Length values                  */
/* Input(s)      : Route Profile which needs to be added to the list(pRoute) */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/* Note          : The calling function has to ensure that the overlap flag  */
/*                 has to be set, before calling this function, inorder to   */
/*                 add the route to the overlap list                         */
/*****************************************************************************/
INT4
Bgp4DshAddRouteToOverlapList (tRouteProfile * pRoute)
{
    tLinkNode          *pLinkNode = NULL;
    tLinkNode          *pScanLinkNode = NULL;
    tLinkNode          *pTempLinkNode = NULL;
    tLinkNode          *pPrevLinkNode = NULL;
    tLinkNode          *pNextLinkNode = NULL;
    tLinkNode          *pAddLinkNode = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pNextRtProfile = NULL;
    UINT4               u4Context = 0;

    if (pRoute == NULL)
    {
        return BGP4_SUCCESS;
    }
    u4Context = BGP4_RT_CXT_ID (pRoute);
    if ((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_OVERLAP) != BGP4_RT_OVERLAP)
    {
        return BGP4_SUCCESS;
    }

    pLinkNode = Bgp4DshAllocateLinkNode ();
    if (pLinkNode == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tBgp4DshAddRouteToList() :Alloc_link_node "
                  "returing NULL \n");
        return BGP4_FAILURE;
    }

    TMO_SLL_Init_Node (&pLinkNode->TSNext);

    /* Increment the reference counts appropriately */
    BGP4_RT_REF_COUNT (pRoute)++;

    pLinkNode->pRouteProfile = pRoute;

    /* Reset the best route flag */
    BGP4_RT_RESET_FLAG (pRoute, BGP4_RT_BEST);

    if (TMO_SLL_Count (BGP4_OVERLAP_ROUTE_LIST (u4Context)) == 0)
    {
        TMO_SLL_Insert (BGP4_OVERLAP_ROUTE_LIST (u4Context),
                        (tTMO_SLL_NODE *) NULL, &pLinkNode->TSNext);
        return BGP4_SUCCESS;
    }

    BGP_SLL_DYN_Scan (BGP4_OVERLAP_ROUTE_LIST (u4Context), pScanLinkNode,
                      pTempLinkNode, tLinkNode *)
    {
        pRtProfile = pScanLinkNode->pRouteProfile;
        pNextLinkNode =
            (tLinkNode *) TMO_SLL_Next (BGP4_OVERLAP_ROUTE_LIST (u4Context),
                                        &pScanLinkNode->TSNext);
        if ((PrefixLessThan
             (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
              (BGP4_RT_NET_ADDRESS_INFO (pRoute)),
              BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
              (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)))) == BGP4_TRUE)
        {
            pAddLinkNode = pPrevLinkNode;
            break;
        }
        else if (pNextLinkNode)
        {
            pNextRtProfile = pNextLinkNode->pRouteProfile;
            if (((PrefixGreaterThan
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                   (BGP4_RT_NET_ADDRESS_INFO (pRoute)),
                   BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                   (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)))) == BGP4_TRUE)
                &&
                ((PrefixLessThan
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                   (BGP4_RT_NET_ADDRESS_INFO (pRoute)),
                   BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                   (BGP4_RT_NET_ADDRESS_INFO (pNextRtProfile)))) == BGP4_TRUE))
            {
                pAddLinkNode = pScanLinkNode;
                break;
            }
            else if ((PrefixMatch
                      (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                       (BGP4_RT_NET_ADDRESS_INFO (pRoute)),
                       BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                       (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)))) == BGP4_TRUE)
            {
                if (BGP4_RT_PREFIXLEN (pRoute) < BGP4_RT_PREFIXLEN (pRtProfile))
                {
                    pAddLinkNode = pPrevLinkNode;
                    break;
                }
                if (BGP4_RT_PREFIXLEN (pRoute) ==
                    BGP4_RT_PREFIXLEN (pRtProfile))
                {
                    if (BGP4_RT_PROTOCOL (pRoute) ==
                        BGP4_RT_PROTOCOL (pRtProfile))
                    {
                        if (((BGP4_RT_PROTOCOL (pRoute) == BGP_ID) &&
                             ((BGP4_RT_PEER_ENTRY (pRoute)) ==
                              (BGP4_RT_PEER_ENTRY (pRtProfile)))) ||
                            (BGP4_RT_PROTOCOL (pRoute) != BGP_ID))
                        {
                            /* Received route is a replacement route. Remove
                             * the current route from the Overlap list and
                             * add the new route to the list
                             */
                            TMO_SLL_Delete (BGP4_OVERLAP_ROUTE_LIST (u4Context),
                                            &pScanLinkNode->TSNext);
                            Bgp4DshReleaseLinkNode (pScanLinkNode);
                            pAddLinkNode = pPrevLinkNode;
                            break;
                        }
                    }
                }
                if ((PrefixMatch
                     (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                      (BGP4_RT_NET_ADDRESS_INFO (pRoute)),
                      BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                      (BGP4_RT_NET_ADDRESS_INFO (pNextRtProfile)))) !=
                    BGP4_TRUE)
                {
                    pAddLinkNode = pScanLinkNode;
                    break;
                }
            }
        }
        else if (pNextLinkNode == NULL)
        {
            if ((PrefixGreaterThan
                 (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                  (BGP4_RT_NET_ADDRESS_INFO (pRoute)),
                  BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                  (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)))) == BGP4_TRUE)
            {
                pAddLinkNode = pScanLinkNode;
                break;
            }
            if ((PrefixMatch
                 (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                  (BGP4_RT_NET_ADDRESS_INFO (pRoute)),
                  BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                  (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)))) == BGP4_TRUE)
            {
                if (BGP4_RT_PREFIXLEN (pRoute) < BGP4_RT_PREFIXLEN (pRtProfile))
                {
                    pAddLinkNode = pPrevLinkNode;
                }
                if (BGP4_RT_PREFIXLEN (pRoute) ==
                    BGP4_RT_PREFIXLEN (pRtProfile))
                {
                    if (BGP4_RT_PROTOCOL (pRoute) ==
                        BGP4_RT_PROTOCOL (pRtProfile))
                    {
                        if (((BGP4_RT_PROTOCOL (pRoute) == BGP_ID) &&
                             ((BGP4_RT_PEER_ENTRY (pRoute)) ==
                              (BGP4_RT_PEER_ENTRY (pRtProfile)))) ||
                            (BGP4_RT_PROTOCOL (pRoute) != BGP_ID))
                        {
                            /* Received route is a replacement route. Remove
                             * the current route from the Overlap list and
                             * add the new route to the list
                             */
                            TMO_SLL_Delete (BGP4_OVERLAP_ROUTE_LIST (u4Context),
                                            &pScanLinkNode->TSNext);
                            Bgp4DshReleaseLinkNode (pScanLinkNode);
                            pAddLinkNode = pPrevLinkNode;
                            break;
                        }
                    }
                }
                pAddLinkNode = pScanLinkNode;
                break;
            }
        }
        pPrevLinkNode = pScanLinkNode;
    }

    TMO_SLL_Insert (BGP4_OVERLAP_ROUTE_LIST (u4Context), &pAddLinkNode->TSNext,
                    &pLinkNode->TSNext);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4DshRemovePeerRoutesFromOverlapList                    */
/* Description   : This routine removes the route advertised by the given    */
/*                 peer from the Overlap route List.                         */
/* Input(s)      : Peer whose routes has to be deleted (pPeer)               */
/* Output(s)     : None                                                      */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4DshRemovePeerRoutesFromOverlapList (tBgp4PeerEntry * pPeer)
{
    tLinkNode          *pScanLinkNode = NULL;
    tLinkNode          *pTempLinkNode = NULL;
    tRouteProfile      *pRouteProfile = NULL;

    BGP_SLL_DYN_Scan (BGP4_OVERLAP_ROUTE_LIST (BGP4_PEER_CXT_ID (pPeer)),
                      pScanLinkNode, pTempLinkNode, tLinkNode *)
    {
        pRouteProfile = pScanLinkNode->pRouteProfile;
        if (BGP4_RT_PROTOCOL (pRouteProfile) != BGP_ID)
        {
            /* This route is not learnt from BGP peer */
            continue;
        }
        if ((PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                          BGP4_PEER_REMOTE_ADDR_INFO (BGP4_RT_PEER_ENTRY
                                                      (pRouteProfile)))) ==
            BGP4_TRUE)
        {
            /* While removing this route dont reset the OVERLAP flag. The flag
             * will be resetted once it is removed from the RIB. */
            Bgp4DshRemoveNodeFromList (BGP4_OVERLAP_ROUTE_LIST
                                       (BGP4_PEER_CXT_ID (pPeer)),
                                       pScanLinkNode);
        }
        continue;
    }
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4DshRemovePeerFromList                                 */
/* Description   : This routine finds a matching peerentry from the list of  */
/*                 peer-nodes and  removes it from the list                  */
/* Input(s)      : List of peer-nodes (pList),                               */
/*                 PeerEntry which needs to be removed (pPeer)               */
/* Output(s)     : Updated list of peer-nodes (pList)                        */
/* Return(s)     : BGP4_SUCCESS, if match occurs,                            */
/*                 BGP4_FAILURE, if matching entry is not found.             */
/*****************************************************************************/
INT4
Bgp4DshRemovePeerFromList (tTMO_SLL * pList, tBgp4PeerEntry * pPeerEntry)
{
    /*Variable Declarations */
    tPeerNode          *pPeerNode = NULL;
    tBgp4PeerEntry     *pPeer = NULL;

    TMO_SLL_Scan (pList, pPeerNode, tPeerNode *)
    {
        pPeer = pPeerNode->pPeer;

        if (pPeer == pPeerEntry)
        {
            break;
        }
    }

    if (pPeerNode != NULL)
    {
        TMO_SLL_Delete (pList, &pPeerNode->TSNext);
        BGP_PEER_NODE_FREE (pPeerNode);
        return BGP4_SUCCESS;
    }

    if (TMO_SLL_Count (pList) == 0)
    {
        TMO_SLL_Init (pList);
    }
    return BGP4_FAILURE;
}

/*****************************************************************************/
/* Function Name : Bgp4DshAddRouteToPeerAdvList                              */
/* Description   : Depending on the <AFI, SAFI> and Listindex type this      */
/*                 routine adds the route                                    */
/*                 into corresponding Route Advertisemnet list of the peer   */
/* Input(s)      : pPeer - peer information                                  */
/*                 u4ListIndex - list type                                   */
/*                               3 - NEW LIST                                */
/*                               5 - ADVT FEAS LIST                          */
/*                               6 - ADVT WITH LIST                          */
/*                 pRoute - Route Profile which needs to be added to the     */
/*                          peer input list                                  */
/*                 u4Group - Flag which says if grouping is needed or not    */
/*                 u4UpdSentTime - The timestamp at which the route was      */
/*                                  advertised                               */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4DshAddRouteToPeerAdvList (tBgp4PeerEntry * pPeer, UINT4 u4ListIndex,
                              tRouteProfile * pRoute, UINT4 u4Group,
                              UINT4 u4UpdSentTime)
{
    UINT4               u4Index;

    if (pRoute == NULL)
    {
        return BGP4_SUCCESS;
    }

    if ((Bgp4GetAfiSafiIndex (BGP4_RT_AFI_INFO (pRoute),
                              BGP4_RT_SAFI_INFO (pRoute),
                              &u4Index)) == BGP4_FAILURE)
    {
        /* This <AFI, SAFI> support is not available */
        return (BGP4_SUCCESS);
    }

#ifdef L3VPN
    if ((BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_CE_PEER) &&
        (u4Index == BGP4_VPN4_UNI_INDEX))
    {
        u4Index = BGP4_IPV4_UNI_INDEX;
    }
#endif
    switch (u4ListIndex)
    {
        case BGP4_PEER_NEW_LIST:
            switch (u4Index)
            {
                case BGP4_IPV4_UNI_INDEX:
                    Bgp4PeerRtAdvHashTblAddEntry
                        (BGP4_PEER_IPV4_NEW_ROUTES (pPeer), pRoute, u4Group,
                         u4UpdSentTime);
                    break;
#ifdef VPLSADS_WANTED
                    /*ADS-VPLS related processing */
                case BGP4_L2VPN_VPLS_INDEX:
                    Bgp4PeerRtAdvHashTblAddEntry
                        (BGP4_PEER_VPLS_NEW_ROUTES (pPeer), pRoute, u4Group,
                         u4UpdSentTime);
                    break;
#endif
#ifdef EVPN_WANTED
                case BGP4_L2VPN_EVPN_INDEX:
                    Bgp4PeerRtAdvHashTblAddEntry
                        (BGP4_PEER_EVPN_NEW_ROUTES (pPeer), pRoute, u4Group,
                         u4UpdSentTime);
                    break;

#endif
#ifdef L3VPN
                case BGP4_IPV4_LBLD_INDEX:
                    /* Carrying Label Information - RFC 3107 */
                    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        Bgp4PeerRtAdvHashTblAddEntry
                            (BGP4_PEER_IPV4_LBLD_NEW_ROUTES (pPeer),
                             pRoute, u4Group, u4UpdSentTime);
                    }
                    break;
#endif
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        Bgp4PeerRtAdvHashTblAddEntry
                            (BGP4_PEER_IPV6_NEW_ROUTES (pPeer),
                             pRoute, u4Group, u4UpdSentTime);
                    }
                    break;
#endif
#ifdef L3VPN
                case BGP4_VPN4_UNI_INDEX:
                    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        Bgp4PeerRtAdvHashTblAddEntry
                            (BGP4_PEER_VPN4_NEW_ROUTES (pPeer), pRoute, u4Group,
                             u4UpdSentTime);
                    }
                    break;
#endif
                default:
                    return (BGP4_SUCCESS);
            }
            break;
        case BGP4_PEER_ADVT_FEAS_LIST:
            switch (u4Index)
            {
                case BGP4_IPV4_UNI_INDEX:
                    Bgp4PeerRtAdvHashTblAddEntry
                        (BGP4_PEER_IPV4_ADVT_FEAS_ROUTES (pPeer), pRoute,
                         u4Group, u4UpdSentTime);
                    break;
#ifdef VPLSADS_WANTED
                    /*ADS-VPLS related processing */
                case BGP4_L2VPN_VPLS_INDEX:
                    Bgp4PeerRtAdvHashTblAddEntry
                        (BGP4_PEER_VPLS_ADVT_FEAS_ROUTES (pPeer), pRoute,
                         u4Group, u4UpdSentTime);
                    break;
#endif
#ifdef EVPN_WANTED
                case BGP4_L2VPN_EVPN_INDEX:
                    Bgp4PeerRtAdvHashTblAddEntry
                        (BGP4_PEER_EVPN_ADVT_FEAS_ROUTES (pPeer), pRoute,
                         u4Group, u4UpdSentTime);
                    break;
#endif
#ifdef L3VPN
                case BGP4_IPV4_LBLD_INDEX:
                    /* Carrying Label Information - RFC 3107 */
                    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        Bgp4PeerRtAdvHashTblAddEntry
                            (BGP4_PEER_IPV4_LBLD_ADVT_FEAS_ROUTES (pPeer),
                             pRoute, u4Group, u4UpdSentTime);
                    }
                    break;
#endif
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        Bgp4PeerRtAdvHashTblAddEntry
                            (BGP4_PEER_IPV6_ADVT_FEAS_ROUTES (pPeer), pRoute,
                             u4Group, u4UpdSentTime);
                    }
                    break;
#endif
#ifdef L3VPN
                case BGP4_VPN4_UNI_INDEX:
                    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        Bgp4PeerRtAdvHashTblAddEntry
                            (BGP4_PEER_VPN4_ADVT_FEAS_ROUTES (pPeer), pRoute,
                             u4Group, u4UpdSentTime);
                    }
                    break;
#endif
                default:
                    return (BGP4_SUCCESS);
            }
            break;
        case BGP4_PEER_ADVT_WITH_LIST:
            switch (u4Index)
            {
                case BGP4_IPV4_UNI_INDEX:
                    Bgp4PeerRtAdvHashTblAddEntry
                        (BGP4_PEER_IPV4_ADVT_WITH_ROUTES (pPeer), pRoute,
                         u4Group, u4UpdSentTime);
                    break;
#ifdef VPLSADS_WANTED
                    /*ADS-VPLS related processing */
                case BGP4_L2VPN_VPLS_INDEX:
                    Bgp4PeerRtAdvHashTblAddEntry
                        (BGP4_PEER_VPLS_ADVT_WITH_ROUTES (pPeer), pRoute,
                         u4Group, u4UpdSentTime);
                    break;
#endif
#ifdef EVPN_WANTED
                case BGP4_L2VPN_EVPN_INDEX:
                    Bgp4PeerRtAdvHashTblAddEntry
                        (BGP4_PEER_EVPN_ADVT_WITH_ROUTES (pPeer), pRoute,
                         u4Group, u4UpdSentTime);
                    break;
#endif
#ifdef L3VPN
                case BGP4_IPV4_LBLD_INDEX:
                    /* Carrying Label Information - RFC 3107 */
                    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        Bgp4PeerRtAdvHashTblAddEntry
                            (BGP4_PEER_IPV4_LBLD_ADVT_WITH_ROUTES (pPeer),
                             pRoute, u4Group, u4UpdSentTime);
                    }
                    break;
#endif
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        Bgp4PeerRtAdvHashTblAddEntry
                            (BGP4_PEER_IPV6_ADVT_WITH_ROUTES (pPeer), pRoute,
                             u4Group, u4UpdSentTime);
                    }
                    break;
#endif
#ifdef L3VPN
                case BGP4_VPN4_UNI_INDEX:
                    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        Bgp4PeerRtAdvHashTblAddEntry
                            (BGP4_PEER_VPN4_ADVT_WITH_ROUTES (pPeer), pRoute,
                             u4Group, u4UpdSentTime);
                    }
                    break;
#endif
                default:
                    return (BGP4_SUCCESS);
            }
            break;
        default:
            return (BGP4_SUCCESS);
    }
    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Bgp4DshAddRouteToPeerList                                 */
/* Description   : Depending on the <AFI, SAFI> and Listindex type this      */
/*                 routine adds the route                                    */
/*                 into corresponding list of the peer                       */
/* Input(s)      : pPeer - peer information                                  */
/*                 u4ListIndex - list type                                   */
/*                  1 - INPUT LIST                                           */
/*                  2 - OUTPUT LIST                                          */
/*                  4 - NEW LOCAL LIST                                       */
/*                  7 - RCVD ROUTE LIST                                      */
/*                  8 - DEINIT RT_LIST                                       */
/*                 Route Profile which needs to be added to the peer input   */
/*                 list(pRoute) */
/*                 Flag which says whether grouping is needed or not         */
/*                 (u4Group)                                                 */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4DshAddRouteToPeerList (tBgp4PeerEntry * pPeer, UINT4 u4ListIndex,
                           tRouteProfile * pRoute, UINT4 u4Group)
{
    UINT4               u4Index;

    if (pRoute == NULL)
    {
        return BGP4_SUCCESS;
    }

    if ((Bgp4GetAfiSafiIndex (BGP4_RT_AFI_INFO (pRoute),
                              BGP4_RT_SAFI_INFO (pRoute),
                              &u4Index)) == BGP4_FAILURE)
    {
        /* This <AFI, SAFI> support is not available */
        return (BGP4_SUCCESS);
    }

#ifdef L3VPN
    if ((BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_CE_PEER) &&
        (u4Index == BGP4_VPN4_UNI_INDEX))
    {
        u4Index = BGP4_IPV4_UNI_INDEX;
    }
#endif
    switch (u4ListIndex)
    {
        case BGP4_PEER_OUTPUT_LIST:
            switch (u4Index)
            {
                case BGP4_IPV4_UNI_INDEX:
                    Bgp4HashTblAddEntry (BGP4_PEER_IPV4_OUTPUT_ROUTES (pPeer),
                                         pRoute, u4Group);
                    break;
#ifdef VPLSADS_WANTED
                    /*ADS-VPLS related processing */
                case BGP4_L2VPN_VPLS_INDEX:
                    Bgp4HashTblAddEntry (BGP4_PEER_VPLS_OUTPUT_ROUTES (pPeer),
                                         pRoute, u4Group);
                    break;
#endif
#ifdef L3VPN
                case BGP4_IPV4_LBLD_INDEX:
                    /* Carrying Label Information - RFC 3107 */
                    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        Bgp4HashTblAddEntry (BGP4_PEER_IPV4_LBLD_OUTPUT_ROUTES
                                             (pPeer), pRoute, u4Group);
                    }
                    break;
#endif
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        Bgp4HashTblAddEntry (BGP4_PEER_IPV6_OUTPUT_ROUTES
                                             (pPeer), pRoute, u4Group);
                    }
                    break;
#endif
#ifdef L3VPN
                case BGP4_VPN4_UNI_INDEX:
                    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        Bgp4HashTblAddEntry (BGP4_PEER_VPN4_OUTPUT_ROUTES
                                             (pPeer), pRoute, u4Group);
                    }
                    break;
#endif
#ifdef EVPN_WANTED
                case BGP4_L2VPN_EVPN_INDEX:
                    if (BGP4_PEER_EVPN_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        Bgp4HashTblAddEntry (BGP4_PEER_EVPN_OUTPUT_ROUTES
                                             (pPeer), pRoute, u4Group);
                    }
                    break;
#endif

                default:
                    return (BGP4_SUCCESS);
            }
            break;
        case BGP4_PEER_NEW_LOCAL_LIST:
            switch (u4Index)
            {
                case BGP4_IPV4_UNI_INDEX:
                    Bgp4HashTblAddEntry (BGP4_PEER_IPV4_NEW_LOCAL_ROUTES
                                         (pPeer), pRoute, u4Group);
                    break;
#ifdef VPLSADS_WANTED
                    /*ADS-VPLS related processing */
                case BGP4_L2VPN_VPLS_INDEX:
                    Bgp4HashTblAddEntry (BGP4_PEER_VPLS_NEW_LOCAL_ROUTES
                                         (pPeer), pRoute, u4Group);
                    break;
#endif
#ifdef EVPN_WANTED
                case BGP4_L2VPN_EVPN_INDEX:
                    Bgp4HashTblAddEntry (BGP4_PEER_EVPN_NEW_LOCAL_ROUTES
                                         (pPeer), pRoute, u4Group);
                    break;
#endif
#ifdef L3VPN
                case BGP4_IPV4_LBLD_INDEX:
                    /* Carrying Label Information - RFC 3107 */
                    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        Bgp4HashTblAddEntry
                            (BGP4_PEER_IPV4_LBLD_NEW_LOCAL_ROUTES (pPeer),
                             pRoute, u4Group);
                    }
                    break;
#endif
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        Bgp4HashTblAddEntry (BGP4_PEER_IPV6_NEW_LOCAL_ROUTES
                                             (pPeer), pRoute, u4Group);
                    }
                    break;
#endif
#ifdef L3VPN
                case BGP4_VPN4_UNI_INDEX:
                    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        Bgp4HashTblAddEntry (BGP4_PEER_VPN4_NEW_LOCAL_ROUTES
                                             (pPeer), pRoute, u4Group);
                    }
                    break;
#endif
                default:
                    return (BGP4_SUCCESS);
            }
            break;

        case BGP4_PEER_NEW_LIST:
        case BGP4_PEER_ADVT_FEAS_LIST:
        case BGP4_PEER_ADVT_WITH_LIST:
            break;

        case BGP4_PEER_DEINIT_ROUTE_LIST:
            switch (u4Index)
            {
                case BGP4_IPV4_UNI_INDEX:
                    Bgp4DshAddRouteToList (BGP4_PEER_IPV4_DEINIT_RT_LIST
                                           (pPeer), pRoute, u4Group);
                    break;
#ifdef VPLSADS_WANTED
                    /*ADS-VPLS related processing */
                case BGP4_L2VPN_VPLS_INDEX:
                    Bgp4DshAddRouteToList (BGP4_PEER_VPLS_DEINIT_RT_LIST
                                           (pPeer), pRoute, u4Group);
                    break;
#endif
#ifdef EVPN_WANTED
                case BGP4_L2VPN_EVPN_INDEX:
                    Bgp4DshAddRouteToList (BGP4_PEER_EVPN_DEINIT_RT_LIST
                                           (pPeer), pRoute, u4Group);
                    break;
#endif
#ifdef L3VPN
                case BGP4_IPV4_LBLD_INDEX:
                    /* Carrying Label Information - RFC 3107 */
                    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        Bgp4DshAddRouteToList
                            (BGP4_PEER_IPV4_LBLD_DEINIT_RT_LIST (pPeer), pRoute,
                             u4Group);
                    }
                    break;
#endif
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        Bgp4DshAddRouteToList (BGP4_PEER_IPV6_DEINIT_RT_LIST
                                               (pPeer), pRoute, u4Group);
                    }
                    break;
#endif
#ifdef L3VPN
                case BGP4_VPN4_UNI_INDEX:
                    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        Bgp4DshAddRouteToList (BGP4_PEER_VPN4_DEINIT_RT_LIST
                                               (pPeer), pRoute, u4Group);
                    }
                    break;
#endif
                default:
                    return (BGP4_SUCCESS);
            }
            break;
        case BGP4_PEER_RCVD_ROUTE_LIST:
            Bgp4DshDelinkRouteFromChgList (pRoute);
            Bgp4DshAddRouteToList (BGP4_PEER_RCVD_ROUTES (pPeer),
                                   pRoute, u4Group);
            break;
        default:
            return (BGP4_SUCCESS);
    }
    return (BGP4_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Bgp4DshGetRouteFromPeerList                               */
/* Description   : Depending on the <AFI, SAFI> and Listindex type this      */
/*                 routine gets the route                                    */
/*                 from corresponding list of the peer                       */
/* Input(s)      : pPeer - peer information                                  */
/*                 u4ListIndex - list type                                   */
/*                  3 - NEW LIST                                             */
/*                  5 - ADVT FEAS LIST                                       */
/*                  6 - ADVT WITH LIST                                       */
/*                 Route Profile which needs to be searched for (pRoute)     */
/* Output(s)     : None.                                                     */
/* Return(s)     : Searched route profile if match occurs,                   */
/*                 NULL if matching entry is not found.                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4DshGetRouteFromPeerAdvList (tBgp4PeerEntry * pPeer, UINT4 u4ListIndex,
                                tRouteProfile * pRoute, UINT4 *pu4UpdSentTime)
{
    UINT4               u4Index;
    tRouteProfile      *pRtProfile = NULL;

    if (pRoute == NULL)
    {
        return (NULL);
    }

    if ((Bgp4GetAfiSafiIndex (BGP4_RT_AFI_INFO (pRoute),
                              BGP4_RT_SAFI_INFO (pRoute),
                              &u4Index)) == BGP4_FAILURE)
    {
        /* This <AFI, SAFI> support is not available */
        return (NULL);
    }

#ifdef L3VPN
    if ((BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_CE_PEER) &&
        (u4Index == BGP4_VPN4_UNI_INDEX))
    {
        u4Index = BGP4_IPV4_UNI_INDEX;
    }
#endif

    switch (u4ListIndex)
    {
        case BGP4_PEER_NEW_LIST:
            switch (u4Index)
            {
                case BGP4_IPV4_UNI_INDEX:
                    pRtProfile = Bgp4PeerRtAdvHashTblGetEntry
                        (BGP4_PEER_IPV4_NEW_ROUTES (pPeer),
                         pRoute, pu4UpdSentTime);
                    break;
#ifdef VPLSADS_WANTED
                    /* ADS-VPLS related processing */
                case BGP4_L2VPN_VPLS_INDEX:
                    pRtProfile = Bgp4PeerRtAdvHashTblGetEntry
                        (BGP4_PEER_VPLS_NEW_ROUTES (pPeer),
                         pRoute, pu4UpdSentTime);
                    break;
#endif
#ifdef EVPN_WANTED
                case BGP4_L2VPN_EVPN_INDEX:
                    pRtProfile = Bgp4PeerRtAdvHashTblGetEntry
                        (BGP4_PEER_EVPN_NEW_ROUTES (pPeer),
                         pRoute, pu4UpdSentTime);
                    break;

#endif
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile = Bgp4PeerRtAdvHashTblGetEntry
                            (BGP4_PEER_IPV6_NEW_ROUTES (pPeer),
                             pRoute, pu4UpdSentTime);
                    }
                    else
                    {
                        return (NULL);
                    }
                    break;
#endif
#ifdef L3VPN
                    /* Carrying Label Information - RFC 3107 */
                case BGP4_IPV4_LBLD_INDEX:
                    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile = Bgp4PeerRtAdvHashTblGetEntry
                            (BGP4_PEER_IPV4_LBLD_NEW_ROUTES (pPeer),
                             pRoute, pu4UpdSentTime);
                    }
                    else
                    {
                        return (NULL);
                    }
                    break;

                case BGP4_VPN4_UNI_INDEX:
                    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile = Bgp4PeerRtAdvHashTblGetEntry
                            (BGP4_PEER_VPN4_NEW_ROUTES (pPeer),
                             pRoute, pu4UpdSentTime);
                    }
                    else
                    {
                        return (NULL);
                    }
                    break;
#endif
                default:
                    return (NULL);
            }
            break;
        case BGP4_PEER_ADVT_FEAS_LIST:
            switch (u4Index)
            {
                case BGP4_IPV4_UNI_INDEX:
                    pRtProfile = Bgp4PeerRtAdvHashTblGetEntry
                        (BGP4_PEER_IPV4_ADVT_FEAS_ROUTES (pPeer),
                         pRoute, pu4UpdSentTime);
                    break;

#ifdef VPLSADS_WANTED
                    /* ADS-VPLS related processing */
                case BGP4_L2VPN_VPLS_INDEX:
                    pRtProfile = Bgp4PeerRtAdvHashTblGetEntry
                        (BGP4_PEER_VPLS_ADVT_FEAS_ROUTES (pPeer),
                         pRoute, pu4UpdSentTime);
                    break;
#endif
#ifdef EVPN_WANTED
                case BGP4_L2VPN_EVPN_INDEX:
                    pRtProfile = Bgp4PeerRtAdvHashTblGetEntry
                        (BGP4_PEER_EVPN_ADVT_FEAS_ROUTES (pPeer),
                         pRoute, pu4UpdSentTime);
                    break;
#endif
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile = Bgp4PeerRtAdvHashTblGetEntry
                            (BGP4_PEER_IPV6_ADVT_FEAS_ROUTES
                             (pPeer), pRoute, pu4UpdSentTime);
                    }
                    else
                    {
                        return (NULL);
                    }
                    break;
#endif
#ifdef L3VPN
                    /* Carrying Label Information - RFC 3107 */
                case BGP4_IPV4_LBLD_INDEX:
                    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile = Bgp4PeerRtAdvHashTblGetEntry
                            (BGP4_PEER_IPV4_LBLD_ADVT_FEAS_ROUTES (pPeer),
                             pRoute, pu4UpdSentTime);
                    }
                    else
                    {
                        return (NULL);
                    }
                    break;

                case BGP4_VPN4_UNI_INDEX:
                    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4PeerRtAdvHashTblGetEntry
                            (BGP4_PEER_VPN4_ADVT_FEAS_ROUTES (pPeer),
                             pRoute, pu4UpdSentTime);
                    }
                    else
                    {
                        return (NULL);
                    }
                    break;
#endif
                default:
                    return (NULL);
            }
            break;
        case BGP4_PEER_ADVT_WITH_LIST:
            switch (u4Index)
            {
                case BGP4_IPV4_UNI_INDEX:
                    pRtProfile = Bgp4PeerRtAdvHashTblGetEntry
                        (BGP4_PEER_IPV4_ADVT_WITH_ROUTES (pPeer),
                         pRoute, pu4UpdSentTime);
                    break;
#ifdef VPLSADS_WANTED
                    /* ADS-VPLS related processing */
                case BGP4_L2VPN_VPLS_INDEX:
                    pRtProfile = Bgp4PeerRtAdvHashTblGetEntry
                        (BGP4_PEER_VPLS_ADVT_WITH_ROUTES (pPeer),
                         pRoute, pu4UpdSentTime);
                    break;
#endif
#ifdef EVPN_WANTED
                case BGP4_L2VPN_EVPN_INDEX:
                    pRtProfile = Bgp4PeerRtAdvHashTblGetEntry
                        (BGP4_PEER_EVPN_ADVT_WITH_ROUTES (pPeer),
                         pRoute, pu4UpdSentTime);
                    break;
#endif
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile = Bgp4PeerRtAdvHashTblGetEntry
                            (BGP4_PEER_IPV6_ADVT_WITH_ROUTES
                             (pPeer), pRoute, pu4UpdSentTime);
                    }
                    else
                    {
                        return (NULL);
                    }
                    break;
#endif
#ifdef L3VPN
                    /* Carrying Label Information - RFC 3107 */
                case BGP4_IPV4_LBLD_INDEX:
                    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile = Bgp4PeerRtAdvHashTblGetEntry
                            (BGP4_PEER_IPV4_LBLD_ADVT_WITH_ROUTES (pPeer),
                             pRoute, pu4UpdSentTime);
                    }
                    else
                    {
                        return (NULL);
                    }
                    break;

                case BGP4_VPN4_UNI_INDEX:
                    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile = Bgp4PeerRtAdvHashTblGetEntry
                            (BGP4_PEER_VPN4_ADVT_WITH_ROUTES (pPeer),
                             pRoute, pu4UpdSentTime);
                    }
                    else
                    {
                        return (NULL);
                    }
                    break;
#endif
                default:
                    return (NULL);
            }
            break;
        default:
            return (NULL);
    }
    return (pRtProfile);
}

/*****************************************************************************/
/* Function Name : Bgp4DshGetRouteFromPeerList                               */
/* Description   : Depending on the <AFI, SAFI> and Listindex type this      */
/*                 routine gets the route                                    */
/*                 from corresponding list of the peer                       */
/* Input(s)      : pPeer - peer information                                  */
/*                 u4ListIndex - list type                                   */
/*                  1 - INPUT LIST                                           */
/*                  2 - OUTPUT LIST                                          */
/*                  4 - NEW LOCAL LIST                                       */
/*                  7 - RCVD ROUTE LIST                                      */
/*                  8 - DEINIT RT_LIST                                       */
/*                 Route Profile which needs to be searched for (pRoute)     */
/* Output(s)     : None.                                                     */
/* Return(s)     : Searched route profile if match occurs,                   */
/*                 NULL if matching entry is not found.                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4DshGetRouteFromPeerList (tBgp4PeerEntry * pPeer, UINT4 u4ListIndex,
                             tRouteProfile * pRoute)
{
    UINT4               u4Index;
    tRouteProfile      *pRtProfile = NULL;

    if (pRoute == NULL)
    {
        return (NULL);
    }

    if ((Bgp4GetAfiSafiIndex (BGP4_RT_AFI_INFO (pRoute),
                              BGP4_RT_SAFI_INFO (pRoute),
                              &u4Index)) == BGP4_FAILURE)
    {
        /* This <AFI, SAFI> support is not available */
        return (NULL);
    }

#ifdef L3VPN
    if ((BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_CE_PEER) &&
        (u4Index == BGP4_VPN4_UNI_INDEX))
    {
        u4Index = BGP4_IPV4_UNI_INDEX;
    }
#endif

    switch (u4ListIndex)
    {
        case BGP4_PEER_OUTPUT_LIST:
            switch (u4Index)
            {
                case BGP4_IPV4_UNI_INDEX:
                    pRtProfile = Bgp4HashTblGetEntry
                        (BGP4_PEER_IPV4_OUTPUT_ROUTES (pPeer),
                         pRoute, BGP4_TRUE);
                    break;
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile = Bgp4HashTblGetEntry
                            (BGP4_PEER_IPV6_OUTPUT_ROUTES (pPeer),
                             pRoute, BGP4_TRUE);
                    }
                    else
                    {
                        return (NULL);
                    }
                    break;
#endif
#ifdef L3VPN
                    /* Carrying Label Information - RFC 3107 */
                case BGP4_IPV4_LBLD_INDEX:
                    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile = Bgp4HashTblGetEntry
                            (BGP4_PEER_IPV4_LBLD_OUTPUT_ROUTES (pPeer),
                             pRoute, BGP4_TRUE);
                    }
                    else
                    {
                        return (NULL);
                    }
                    break;

                case BGP4_VPN4_UNI_INDEX:
                    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile = Bgp4HashTblGetEntry
                            (BGP4_PEER_VPN4_OUTPUT_ROUTES (pPeer),
                             pRoute, BGP4_TRUE);
                    }
                    else
                    {
                        return (NULL);
                    }
                    break;
#endif
                default:
                    return (NULL);
            }
            break;
        case BGP4_PEER_NEW_LOCAL_LIST:
            switch (u4Index)
            {
                case BGP4_IPV4_UNI_INDEX:
                    pRtProfile = Bgp4HashTblGetEntry
                        (BGP4_PEER_IPV4_NEW_LOCAL_ROUTES (pPeer),
                         pRoute, BGP4_TRUE);
                    break;
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4HashTblGetEntry (BGP4_PEER_IPV6_NEW_LOCAL_ROUTES
                                                 (pPeer), pRoute, BGP4_TRUE);
                    }
                    else
                    {
                        return (NULL);
                    }
                    break;
#endif
#ifdef L3VPN
                    /* Carrying Label Information - RFC 3107 */
                case BGP4_IPV4_LBLD_INDEX:
                    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile = Bgp4HashTblGetEntry
                            (BGP4_PEER_IPV4_LBLD_NEW_LOCAL_ROUTES (pPeer),
                             pRoute, BGP4_TRUE);
                    }
                    else
                    {
                        return (NULL);
                    }
                    break;

                case BGP4_VPN4_UNI_INDEX:
                    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4HashTblGetEntry (BGP4_PEER_VPN4_NEW_LOCAL_ROUTES
                                                 (pPeer), pRoute, BGP4_TRUE);
                    }
                    else
                    {
                        return (NULL);
                    }
                    break;
#endif
                default:
                    return (NULL);
            }
            break;
        case BGP4_PEER_DEINIT_ROUTE_LIST:
            switch (u4Index)
            {
                case BGP4_IPV4_UNI_INDEX:
                    pRtProfile =
                        Bgp4RibhGetRouteFromPeerList
                        (BGP4_PEER_IPV4_DEINIT_RT_LIST (pPeer), pRoute);
                    break;
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4RibhGetRouteFromPeerList
                            (BGP4_PEER_IPV6_DEINIT_RT_LIST (pPeer), pRoute);
                    }
                    else
                    {
                        return (NULL);
                    }
                    break;
#endif
#ifdef L3VPN
                    /* Carrying Label Information - RFC 3107 */
                case BGP4_IPV4_LBLD_INDEX:
                    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4RibhGetRouteFromPeerList
                            (BGP4_PEER_IPV4_LBLD_DEINIT_RT_LIST (pPeer),
                             pRoute);
                    }
                    else
                    {
                        return (NULL);
                    }
                    break;

                case BGP4_VPN4_UNI_INDEX:
                    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4RibhGetRouteFromPeerList
                            (BGP4_PEER_VPN4_DEINIT_RT_LIST (pPeer), pRoute);
                    }
                    else
                    {
                        return (NULL);
                    }
                    break;
#endif
                default:
                    return (NULL);
            }
            break;
        case BGP4_PEER_RCVD_ROUTE_LIST:
            pRtProfile =
                Bgp4RibhGetRouteFromPeerList (BGP4_PEER_RCVD_ROUTES (pPeer),
                                              pRoute);
            break;
        default:
            return (NULL);
    }
    return (pRtProfile);
}

/*****************************************************************************/
/* Function Name : Bgp4DshRemoveRtFrmPeerAdvtList                            */
/* Description   : Depending on the <AFI, SAFI> and Listindex type this      */
/*                 routine removes the route                                 */
/*                 from corresponding advertisement list of the peer         */
/* Input(s)      : pPeer - peer information                                  */
/*                 u4ListIndex - list type                                   */
/*                  3 - NEW LIST                                             */
/*                  5 - ADVT FEAS LIST                                       */
/*                  6 - ADVT WITH LIST                                       */
/*                 Route Profile which needs to be removed (pRoute)          */
/* Output(s)     : None.                                                     */
/* Return(s)     : Removed route profile if match occurs,                    */
/*                 NULL if matching entry is not found.                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4DshRemoveRtFrmPeerAdvtList (tBgp4PeerEntry * pPeer, UINT4 u4ListIndex,
                                tRouteProfile * pRoute, UINT4 *pu4UpdSentTime)
{
    UINT4               u4Index;
    tRouteProfile      *pRtProfile = NULL;

    if (pRoute == NULL)
    {
        return NULL;
    }

    if ((Bgp4GetAfiSafiIndex (BGP4_RT_AFI_INFO (pRoute),
                              BGP4_RT_SAFI_INFO (pRoute),
                              &u4Index)) == BGP4_FAILURE)
    {
        /* This <AFI, SAFI> support is not available */
        return NULL;
    }

#ifdef L3VPN
    if ((BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_CE_PEER) &&
        (u4Index == BGP4_VPN4_UNI_INDEX))
    {
        u4Index = BGP4_IPV4_UNI_INDEX;
    }
#endif

    switch (u4ListIndex)
    {
        case BGP4_PEER_NEW_LIST:
            switch (u4Index)
            {
                case BGP4_IPV4_UNI_INDEX:
                    pRtProfile = Bgp4PeerRtAdvHashTblDeleteEntry
                        (BGP4_PEER_IPV4_NEW_ROUTES (pPeer),
                         pRoute, pu4UpdSentTime);
                    break;

#ifdef VPLSADS_WANTED
                    /* ADS-VPLS related processing */
                case BGP4_L2VPN_VPLS_INDEX:
                    pRtProfile = Bgp4PeerRtAdvHashTblDeleteEntry
                        (BGP4_PEER_VPLS_NEW_ROUTES (pPeer),
                         pRoute, pu4UpdSentTime);
                    break;
#endif
#ifdef EVPN_WANTED
                case BGP4_L2VPN_EVPN_INDEX:
                    pRtProfile = Bgp4PeerRtAdvHashTblDeleteEntry
                        (BGP4_PEER_EVPN_NEW_ROUTES (pPeer),
                         pRoute, pu4UpdSentTime);
                    break;
#endif
#ifdef L3VPN
                    /* Carrying Label Information - RFC 3107 */
                case BGP4_IPV4_LBLD_INDEX:
                    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4PeerRtAdvHashTblDeleteEntry
                            (BGP4_PEER_IPV4_LBLD_NEW_ROUTES (pPeer), pRoute,
                             pu4UpdSentTime);
                    }
                    break;
                case BGP4_VPN4_UNI_INDEX:
                    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile = Bgp4PeerRtAdvHashTblDeleteEntry
                            (BGP4_PEER_VPN4_NEW_ROUTES (pPeer),
                             pRoute, pu4UpdSentTime);
                    }
                    break;
#endif
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile = Bgp4PeerRtAdvHashTblDeleteEntry
                            (BGP4_PEER_IPV6_NEW_ROUTES (pPeer),
                             pRoute, pu4UpdSentTime);
                    }
                    break;
#endif
                default:
                    return (NULL);
            }
            break;
        case BGP4_PEER_ADVT_FEAS_LIST:
            switch (u4Index)
            {
                case BGP4_IPV4_UNI_INDEX:
                    pRtProfile =
                        Bgp4PeerRtAdvHashTblDeleteEntry
                        (BGP4_PEER_IPV4_ADVT_FEAS_ROUTES (pPeer),
                         pRoute, pu4UpdSentTime);
                    break;
#ifdef VPLSADS_WANTED
                    /* ADS-VPLS related processing */
                case BGP4_L2VPN_VPLS_INDEX:
                    pRtProfile =
                        Bgp4PeerRtAdvHashTblDeleteEntry
                        (BGP4_PEER_VPLS_ADVT_FEAS_ROUTES (pPeer),
                         pRoute, pu4UpdSentTime);
                    break;
#endif
#ifdef EVPN_WANTED
                case BGP4_L2VPN_EVPN_INDEX:
                    pRtProfile =
                        Bgp4PeerRtAdvHashTblDeleteEntry
                        (BGP4_PEER_EVPN_ADVT_FEAS_ROUTES (pPeer),
                         pRoute, pu4UpdSentTime);
                    break;
#endif
#ifdef L3VPN
                    /* Carrying Label Information - RFC 3107 */
                case BGP4_IPV4_LBLD_INDEX:
                    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4PeerRtAdvHashTblDeleteEntry
                            (BGP4_PEER_IPV4_LBLD_ADVT_FEAS_ROUTES (pPeer),
                             pRoute, pu4UpdSentTime);
                    }
                    break;
                case BGP4_VPN4_UNI_INDEX:
                    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4PeerRtAdvHashTblDeleteEntry
                            (BGP4_PEER_VPN4_ADVT_FEAS_ROUTES (pPeer), pRoute,
                             pu4UpdSentTime);
                    }
                    break;
#endif
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4PeerRtAdvHashTblDeleteEntry
                            (BGP4_PEER_IPV6_ADVT_FEAS_ROUTES (pPeer), pRoute,
                             pu4UpdSentTime);
                    }
                    break;
#endif
                default:
                    return (NULL);
            }
            break;
        case BGP4_PEER_ADVT_WITH_LIST:
            switch (u4Index)
            {
                case BGP4_IPV4_UNI_INDEX:
                    pRtProfile =
                        Bgp4PeerRtAdvHashTblDeleteEntry
                        (BGP4_PEER_IPV4_ADVT_WITH_ROUTES
                         (pPeer), pRoute, pu4UpdSentTime);
                    break;
#ifdef VPLSADS_WANTED
                    /* ADS-VPLS related processing */
                case BGP4_L2VPN_VPLS_INDEX:
                    pRtProfile =
                        Bgp4PeerRtAdvHashTblDeleteEntry
                        (BGP4_PEER_VPLS_ADVT_WITH_ROUTES
                         (pPeer), pRoute, pu4UpdSentTime);
                    break;
#endif
#ifdef EVPN_WANTED
                case BGP4_L2VPN_EVPN_INDEX:
                    pRtProfile =
                        Bgp4PeerRtAdvHashTblDeleteEntry
                        (BGP4_PEER_EVPN_ADVT_WITH_ROUTES
                         (pPeer), pRoute, pu4UpdSentTime);
                    break;

#endif
#ifdef L3VPN
                    /* Carrying Label Information - RFC 3107 */
                case BGP4_IPV4_LBLD_INDEX:
                    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4PeerRtAdvHashTblDeleteEntry
                            (BGP4_PEER_IPV4_LBLD_ADVT_WITH_ROUTES (pPeer),
                             pRoute, pu4UpdSentTime);
                    }
                    break;
                case BGP4_VPN4_UNI_INDEX:
                    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4PeerRtAdvHashTblDeleteEntry
                            (BGP4_PEER_VPN4_ADVT_WITH_ROUTES (pPeer), pRoute,
                             pu4UpdSentTime);
                    }
                    break;
#endif
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4PeerRtAdvHashTblDeleteEntry
                            (BGP4_PEER_IPV6_ADVT_WITH_ROUTES (pPeer), pRoute,
                             pu4UpdSentTime);
                    }
                    break;
#endif
                default:
                    return (NULL);
            }
            break;
        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC |
                      BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tBgp4DshRemoveRtFrmPeerAdvtList () : Invalid Peer "
                      "Route List Handle\n");
            return (NULL);
    }
    return (pRtProfile);
}

/*****************************************************************************/
/* Function Name : Bgp4DshRemoveRouteFromPeerList                            */
/* Description   : Depending on the <AFI, SAFI> and Listindex type this      */
/*                 routine removes the route                                 */
/*                 from corresponding list of the peer                       */
/* Input(s)      : pPeer - peer information                                  */
/*                 u4ListIndex - list type                                   */
/*                  1 - INPUT LIST                                           */
/*                  2 - OUTPUT LIST                                          */
/*                  4 - NEW LOCAL LIST                                       */
/*                  7 - DEINIT RT_LIST                                       */
/*                  8 - RCVD ROUTE LIST                                      */
/*                 Route Profile which needs to be removed (pRoute)          */
/* Output(s)     : None.                                                     */
/* Return(s)     : Removed route profile if match occurs,                    */
/*                 NULL if matching entry is not found.                      */
/*****************************************************************************/
tRouteProfile      *
Bgp4DshRemoveRouteFromPeerList (tBgp4PeerEntry * pPeer, UINT4 u4ListIndex,
                                tRouteProfile * pRoute)
{
    UINT4               u4Index;
    tRouteProfile      *pRtProfile = NULL;

    if (pRoute == NULL)
    {
        return NULL;
    }

    if ((Bgp4GetAfiSafiIndex (BGP4_RT_AFI_INFO (pRoute),
                              BGP4_RT_SAFI_INFO (pRoute),
                              &u4Index)) == BGP4_FAILURE)
    {
        /* This <AFI, SAFI> support is not available */
        return NULL;
    }

#ifdef L3VPN
    if ((BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_CE_PEER) &&
        (u4Index == BGP4_VPN4_UNI_INDEX))
    {
        u4Index = BGP4_IPV4_UNI_INDEX;
    }
#endif

    switch (u4ListIndex)
    {
        case BGP4_PEER_OUTPUT_LIST:
            switch (u4Index)
            {
                case BGP4_IPV4_UNI_INDEX:
                    pRtProfile =
                        Bgp4HashTblDeleteEntry (BGP4_PEER_IPV4_OUTPUT_ROUTES
                                                (pPeer), pRoute, BGP4_TRUE);
                    break;
#ifdef VPLSADS_WANTED
                    /*ADS-VPLS related processing */
                case BGP4_L2VPN_VPLS_INDEX:
                    pRtProfile =
                        Bgp4HashTblDeleteEntry (BGP4_PEER_VPLS_OUTPUT_ROUTES
                                                (pPeer), pRoute, BGP4_TRUE);
                    break;
#endif
#ifdef L3VPN
                    /* Carrying Label Information - RFC 3107 */
                case BGP4_IPV4_LBLD_INDEX:
                    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4HashTblDeleteEntry
                            (BGP4_PEER_IPV4_LBLD_OUTPUT_ROUTES (pPeer), pRoute,
                             BGP4_TRUE);
                    }
                    break;
                case BGP4_VPN4_UNI_INDEX:
                    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4HashTblDeleteEntry (BGP4_PEER_VPN4_OUTPUT_ROUTES
                                                    (pPeer), pRoute, BGP4_TRUE);
                    }
                    break;
#endif
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4HashTblDeleteEntry (BGP4_PEER_IPV6_OUTPUT_ROUTES
                                                    (pPeer), pRoute, BGP4_TRUE);
                    }
                    break;
#endif
#ifdef EVPN_WANTED
                case BGP4_L2VPN_EVPN_INDEX:
                    if (BGP4_PEER_EVPN_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4HashTblDeleteEntry (BGP4_PEER_EVPN_OUTPUT_ROUTES
                                                    (pPeer), pRoute, BGP4_TRUE);
                    }
                    break;
#endif
                default:
                    return (NULL);
            }
            break;
        case BGP4_PEER_NEW_LOCAL_LIST:
            switch (u4Index)
            {
                case BGP4_IPV4_UNI_INDEX:
                    pRtProfile =
                        Bgp4HashTblDeleteEntry (BGP4_PEER_IPV4_NEW_LOCAL_ROUTES
                                                (pPeer), pRoute, BGP4_TRUE);
                    break;
#ifdef VPLSADS_WANTED
                    /*ADS-VPLS related processing */
                case BGP4_L2VPN_VPLS_INDEX:
                    pRtProfile =
                        Bgp4HashTblDeleteEntry (BGP4_PEER_VPLS_NEW_LOCAL_ROUTES
                                                (pPeer), pRoute, BGP4_TRUE);
                    break;
#endif
#ifdef EVPN_WANTED
                case BGP4_L2VPN_EVPN_INDEX:
                    pRtProfile =
                        Bgp4HashTblDeleteEntry (BGP4_PEER_EVPN_NEW_LOCAL_ROUTES
                                                (pPeer), pRoute, BGP4_TRUE);
                    break;
#endif
#ifdef L3VPN
                    /* Carrying Label Information - RFC 3107 */
                case BGP4_IPV4_LBLD_INDEX:
                    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4HashTblDeleteEntry
                            (BGP4_PEER_IPV4_LBLD_NEW_LOCAL_ROUTES (pPeer),
                             pRoute, BGP4_TRUE);
                    }
                    break;
                case BGP4_VPN4_UNI_INDEX:
                    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4HashTblDeleteEntry
                            (BGP4_PEER_VPN4_NEW_LOCAL_ROUTES (pPeer), pRoute,
                             BGP4_TRUE);
                    }
                    break;
#endif
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4HashTblDeleteEntry
                            (BGP4_PEER_IPV6_NEW_LOCAL_ROUTES (pPeer), pRoute,
                             BGP4_TRUE);
                    }
                    break;
#endif
                default:
                    return (NULL);
            }
            break;
        case BGP4_PEER_ADVT_FEAS_LIST:
        case BGP4_PEER_ADVT_WITH_LIST:
        case BGP4_PEER_NEW_LIST:
            /* These lists have been handled separately in 
               Bgp4DshRemoveRtFrmPeerAdvtList */
            BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)), BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC |
                      BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tBgp4DshRemoveRouteFromPeerList () : Invalid Peer "
                      "Advertisement List Handle\n");
            break;
        case BGP4_PEER_DEINIT_ROUTE_LIST:
            switch (u4Index)
            {
                case BGP4_IPV4_UNI_INDEX:
                    pRtProfile =
                        Bgp4RibhRemoveRouteFromPeerList
                        (BGP4_PEER_IPV4_DEINIT_RT_LIST (pPeer), pRoute);
                    break;
#ifdef VPLSADS_WANTED
                    /*ADS-VPLS related processing */
                case BGP4_L2VPN_VPLS_INDEX:
                    pRtProfile =
                        Bgp4RibhRemoveRouteFromPeerList
                        (BGP4_PEER_VPLS_DEINIT_RT_LIST (pPeer), pRoute);
                    break;
#endif
#ifdef EVPN_WANTED
                case BGP4_L2VPN_EVPN_INDEX:
                    pRtProfile =
                        Bgp4RibhRemoveRouteFromPeerList
                        (BGP4_PEER_EVPN_DEINIT_RT_LIST (pPeer), pRoute);
                    break;
#endif
#ifdef L3VPN
                    /* Carrying Label Information - RFC 3107 */
                case BGP4_IPV4_LBLD_INDEX:
                    if (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4RibhRemoveRouteFromPeerList
                            (BGP4_PEER_IPV4_LBLD_DEINIT_RT_LIST (pPeer),
                             pRoute);
                    }
                    break;
                case BGP4_VPN4_UNI_INDEX:
                    if (BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4RibhRemoveRouteFromPeerList
                            (BGP4_PEER_VPN4_DEINIT_RT_LIST (pPeer), pRoute);
                    }
                    break;
#endif
#ifdef BGP4_IPV6_WANTED
                case BGP4_IPV6_UNI_INDEX:
                    if (BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeer) != NULL)
                    {
                        pRtProfile =
                            Bgp4RibhRemoveRouteFromPeerList
                            (BGP4_PEER_IPV6_DEINIT_RT_LIST (pPeer), pRoute);
                    }
                    break;
#endif
                default:
                    return (NULL);
            }
            break;
        case BGP4_PEER_RCVD_ROUTE_LIST:
            pRtProfile =
                Bgp4RibhRemoveRouteFromPeerList (BGP4_PEER_RCVD_ROUTES (pPeer),
                                                 pRoute);
            break;
        default:
            return (NULL);
    }
    return (pRtProfile);
}

/*****************************************************************************/
/* Function Name : Bgp4DshLinkRouteToList                                    */
/* Description   : Depending on the u4Group flag, it adds the route into the */
/*                 list either append the route to the end of the list or    */
/*                 add the route to the start of the list                    */
/*                 u4Group = BGP4_PREPEND, add the route as the first node   */
/*                           in the list                                     */
/*                 else add the route to the end of the list.                */
/* Input(s)      : List of Route profiles (pList),                           */
/*                 Route Profile which needs to be added to the list(pRoute) */
/*                 Flag indicating how the route needs to be added (u4Group) */
/*                 This flag will not be applied to FIB/INT-Peer/Ext-Peer    */
/*                 list. For this list the route are always added at the end.*/
/*                 u1List - Refers to the list to which the route is added.  */
/*                          Currently takes value as                         */
/*                          BGP4_FIB_UPD_LIST_INDEX  - 1                     */
/*                          BGP4_INT_PEER_LIST_INDEX - 2                     */
/*                          BGP4_EXT_PEER_LIST_INDEX - 3                     */
/*                          BGP4_NEXTHOP_LIST_INDEX  - 4                     */
/*                          BGP4_PROTOCOL_LIST_INDEX - 5                     */
/* Output(s)     : Updated list of route profiles (pList)                    */
/* Return(s)     : BGP4_SUCCESS/BGP4_FAILURE.                                */
/*****************************************************************************/
INT4
Bgp4DshLinkRouteToList (tBGP4_DLL * pList, tRouteProfile * pRoute,
                        UINT4 u4Group, UINT1 u1List)
{
    tRouteProfile      *pPrevRoute = NULL;
    tRouteProfile      *pNextRoute = NULL;
    tRouteProfile      *pTempRoute = NULL;
    UINT4               u4Context = 0;
    UINT1               u1UpdateChgList = BGP4_FALSE;
#ifdef L3VPN
    UINT4               u4AsafiMask = 0;
#endif

    /* Increment the reference counts appropriately */
    BGP4_RT_REF_COUNT (pRoute)++;
    u4Context = BGP4_RT_CXT_ID (pRoute);

#ifdef L3VPN
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRoute),
                           BGP4_RT_SAFI_INFO (pRoute), u4AsafiMask);
    if ((u4AsafiMask == CAP_MP_VPN4_UNICAST) &&
        (u1List == BGP4_EXT_PEER_LIST_INDEX))
    {
        u4Context = pRoute->u4ExtListCxtId;
        pRoute->u4ExtListCxtId = 0;
    }
#endif

    if (BGP4_DLL_FIRST_ROUTE (pList) == NULL)
    {
        /* List is empty. So just add the route to the list */
        BGP4_DLL_FIRST_ROUTE (pList) = pRoute;
        BGP4_DLL_LAST_ROUTE (pList) = pRoute;
        BGP4_DLL_COUNT (pList) = 1;

        /* Also update the Route's Link information. */
        switch (u1List)
        {
            case BGP4_FIB_UPD_LIST_INDEX:
            case BGP4_INT_PEER_LIST_INDEX:
            case BGP4_EXT_PEER_LIST_INDEX:
                if ((BGP4_CHG_LIST_FIRST_ROUTE (u4Context) != pRoute) &&
                    (BGP4_CHG_LIST_LAST_ROUTE (u4Context) != pRoute) &&
                    (BGP4_RT_CHG_LIST_LINK_PREV (pRoute) == NULL) &&
                    (BGP4_RT_CHG_LIST_LINK_NEXT (pRoute) == NULL))
                {
                    /* Route is not present in change list. Need to update the
                     * change list. */
                    u1UpdateChgList = BGP4_TRUE;
                }
                else
                {
                    /* Route is already present in the change list. */
                    u1UpdateChgList = BGP4_FALSE;
                }
                if (u1UpdateChgList == BGP4_TRUE)
                {
                    if ((BGP4_CHG_LIST_FIRST_ROUTE (u4Context) == NULL) ||
                        (BGP4_CHG_LIST_LAST_ROUTE (u4Context) == NULL))
                    {
                        BGP4_CHG_LIST_FIRST_ROUTE (u4Context) = pRoute;
                        BGP4_CHG_LIST_LAST_ROUTE (u4Context) = pRoute;
                        BGP4_CHG_LIST_COUNT (u4Context) = 1;
                        BGP4_RT_CHG_LIST_LINK_PREV (pRoute) = NULL;
                        BGP4_RT_CHG_LIST_LINK_NEXT (pRoute) = NULL;
                    }
                    else
                    {
                        BGP4_RT_CHG_LIST_LINK_NEXT (BGP4_CHG_LIST_LAST_ROUTE
                                                    (u4Context)) = pRoute;
                        BGP4_RT_CHG_LIST_LINK_PREV (pRoute) =
                            BGP4_CHG_LIST_LAST_ROUTE (u4Context);
                        BGP4_RT_CHG_LIST_LINK_NEXT (pRoute) = NULL;
                        BGP4_CHG_LIST_LAST_ROUTE (u4Context) = pRoute;
                        BGP4_CHG_LIST_COUNT (u4Context)++;
                    }
                }
                break;

            case BGP4_NEXTHOP_LIST_INDEX:
                BGP4_RT_NEXTHOP_LINK_PREV (pRoute) = NULL;
                BGP4_RT_NEXTHOP_LINK_NEXT (pRoute) = NULL;
                break;

            case BGP4_PROTOCOL_LIST_INDEX:
                BGP4_RT_PROTO_LINK_PREV (pRoute) = NULL;
                BGP4_RT_PROTO_LINK_NEXT (pRoute) = NULL;
                break;
#ifdef L3VPN
            case BGP4_VPN4_FEAS_WDRAW_LIST_INDEX:
                BGP4_RT_VPN4_LINK_PREV (pRoute) = NULL;
                BGP4_RT_VPN4_LINK_NEXT (pRoute) = NULL;
                break;
#endif
            default:
                break;
        }
        return BGP4_SUCCESS;
    }
    if ((u1List == BGP4_FIB_UPD_LIST_INDEX) ||
        (u1List == BGP4_INT_PEER_LIST_INDEX) ||
        (u1List == BGP4_EXT_PEER_LIST_INDEX))
    {
        /* Add the route to the end of the input list. */
        BGP4_DLL_LAST_ROUTE (pList) = pRoute;
        BGP4_DLL_COUNT (pList)++;

        /* Check whether the route is already present in the change
         * list or not. If present, then move the route from the current
         * location to the end of the change list because if the corresponding
         * First route pointer of list is somewhere after this route's current
         * location in change_list, we may never process this route.
         */
        if ((BGP4_CHG_LIST_LAST_ROUTE (u4Context) != pRoute) &&
            ((BGP4_CHG_LIST_FIRST_ROUTE (u4Context) == pRoute) ||
             (BGP4_RT_CHG_LIST_LINK_PREV (pRoute) != NULL) ||
             (BGP4_RT_CHG_LIST_LINK_NEXT (pRoute) != NULL)))
        {
            /* Route is already present in the Chg list. Move it to
             * the end of the Chg list and update individual list
             * accordingly.
             */
            pPrevRoute = BGP4_RT_CHG_LIST_LINK_PREV (pRoute);
            pNextRoute = BGP4_RT_CHG_LIST_LINK_NEXT (pRoute);

            if (BGP4_CHG_LIST_FIRST_ROUTE (u4Context) == pRoute)
            {
                BGP4_CHG_LIST_FIRST_ROUTE (u4Context) = pNextRoute;
            }
            else
            {
                if (NULL != pPrevRoute)
                {
                    BGP4_RT_CHG_LIST_LINK_NEXT (pPrevRoute) = pNextRoute;
                }
            }
            if (pNextRoute != NULL)
            {
                BGP4_RT_CHG_LIST_LINK_PREV (pNextRoute) = pPrevRoute;
            }

            BGP4_RT_CHG_LIST_LINK_NEXT (BGP4_CHG_LIST_LAST_ROUTE (u4Context)) =
                pRoute;
            BGP4_RT_CHG_LIST_LINK_PREV (pRoute) =
                BGP4_CHG_LIST_LAST_ROUTE (u4Context);
            BGP4_RT_CHG_LIST_LINK_NEXT (pRoute) = NULL;
            BGP4_CHG_LIST_LAST_ROUTE (u4Context) = pRoute;

            if (u1List == BGP4_FIB_UPD_LIST_INDEX)
            {
                if ((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_IN_INT_PEER_ADVT_LIST)
                    == BGP4_RT_IN_INT_PEER_ADVT_LIST)
                {
                    /* Update the INT_PEER_ADVT List */
                    if ((BGP4_INT_PEER_LIST_FIRST_ROUTE (u4Context) == pRoute)
                        && (BGP4_INT_PEER_LIST_LAST_ROUTE (u4Context) !=
                            pRoute))
                    {
                        /* Update the First route of INT_PEER_ADVT List */
                        for (pTempRoute = pNextRoute; pTempRoute != NULL;
                             pTempRoute =
                             BGP4_RT_CHG_LIST_LINK_NEXT (pTempRoute))
                        {
                            if ((BGP4_RT_GET_FLAGS (pTempRoute) &
                                 BGP4_RT_IN_INT_PEER_ADVT_LIST) ==
                                BGP4_RT_IN_INT_PEER_ADVT_LIST)
                            {
                                break;
                            }
                        }
                        BGP4_INT_PEER_LIST_FIRST_ROUTE (u4Context) = pTempRoute;
                    }
                    /* Update the Last node of INT_PEER_ADVT List */
                    BGP4_INT_PEER_LIST_LAST_ROUTE (u4Context) = pRoute;
                }
                if ((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_IN_EXT_PEER_ADVT_LIST)
                    == BGP4_RT_IN_EXT_PEER_ADVT_LIST)
                {
                    /* Update the EXT_PEER_ADVT List */
                    if ((BGP4_EXT_PEER_LIST_FIRST_ROUTE (u4Context) == pRoute)
                        && (BGP4_EXT_PEER_LIST_LAST_ROUTE (u4Context) !=
                            pRoute))
                    {
                        /* Update the First route of EXT_PEER_ADVT List */
                        for (pTempRoute = pNextRoute; pTempRoute != NULL;
                             pTempRoute =
                             BGP4_RT_CHG_LIST_LINK_NEXT (pTempRoute))
                        {
                            if ((BGP4_RT_GET_FLAGS (pTempRoute) &
                                 BGP4_RT_IN_EXT_PEER_ADVT_LIST) ==
                                BGP4_RT_IN_EXT_PEER_ADVT_LIST)
                            {
                                break;
                            }
                        }
                        BGP4_EXT_PEER_LIST_FIRST_ROUTE (u4Context) = pTempRoute;
                    }
                    /* Update the Last node of EXT_PEER_ADVT List */
                    BGP4_EXT_PEER_LIST_LAST_ROUTE (u4Context) = pRoute;
                }
            }

            if (u1List == BGP4_EXT_PEER_LIST_INDEX)
            {
                if ((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_IN_FIB_UPD_LIST) ==
                    BGP4_RT_IN_FIB_UPD_LIST)
                {
                    /* Update the FIB_UPDATE List */
                    if ((BGP4_FIB_LIST_FIRST_ROUTE (u4Context) == pRoute) &&
                        (BGP4_FIB_LIST_LAST_ROUTE (u4Context) != pRoute))
                    {
                        /* Update the First route of FIB_UPDATE List */
                        for (pTempRoute = pNextRoute; pTempRoute != NULL;
                             pTempRoute =
                             BGP4_RT_CHG_LIST_LINK_NEXT (pTempRoute))
                        {
                            if ((BGP4_RT_GET_FLAGS (pTempRoute) &
                                 BGP4_RT_IN_FIB_UPD_LIST) ==
                                BGP4_RT_IN_FIB_UPD_LIST)
                            {
                                break;
                            }
                        }
                        BGP4_FIB_LIST_FIRST_ROUTE (u4Context) = pTempRoute;
                    }
                    /* Update the Last node of FIB_UPDATE List */
                    BGP4_FIB_LIST_LAST_ROUTE (u4Context) = pRoute;
                }
                if ((BGP4_RT_GET_FLAGS (pRoute) &
                     BGP4_RT_IN_INT_PEER_ADVT_LIST) ==
                    BGP4_RT_IN_INT_PEER_ADVT_LIST)
                {
                    /* Update the INT_PEER_ADVT List */
                    if ((BGP4_INT_PEER_LIST_FIRST_ROUTE (u4Context) == pRoute)
                        && (BGP4_INT_PEER_LIST_LAST_ROUTE (u4Context) !=
                            pRoute))
                    {
                        /* Update the First route of INT_PEER_ADVT List */
                        for (pTempRoute = pNextRoute; pTempRoute != NULL;
                             pTempRoute =
                             BGP4_RT_CHG_LIST_LINK_NEXT (pTempRoute))
                        {
                            if ((BGP4_RT_GET_FLAGS (pTempRoute) &
                                 BGP4_RT_IN_INT_PEER_ADVT_LIST) ==
                                BGP4_RT_IN_INT_PEER_ADVT_LIST)
                            {
                                break;
                            }
                        }
                        BGP4_INT_PEER_LIST_FIRST_ROUTE (u4Context) = pTempRoute;
                    }
                    /* Update the Last node of INT_PEER_ADVT List */
                    BGP4_INT_PEER_LIST_LAST_ROUTE (u4Context) = pRoute;
                }
            }

            if (u1List == BGP4_INT_PEER_LIST_INDEX)
            {
                if ((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_IN_FIB_UPD_LIST) ==
                    BGP4_RT_IN_FIB_UPD_LIST)
                {
                    /* Update the FIB_UPDATE List */
                    if ((BGP4_FIB_LIST_FIRST_ROUTE (u4Context) == pRoute) &&
                        (BGP4_FIB_LIST_LAST_ROUTE (u4Context) != pRoute))
                    {
                        /* Update the First route of FIB_UPDATE List */
                        for (pTempRoute = pNextRoute; pTempRoute != NULL;
                             pTempRoute =
                             BGP4_RT_CHG_LIST_LINK_NEXT (pTempRoute))
                        {
                            if ((BGP4_RT_GET_FLAGS (pTempRoute) &
                                 BGP4_RT_IN_FIB_UPD_LIST) ==
                                BGP4_RT_IN_FIB_UPD_LIST)
                            {
                                break;
                            }
                        }
                        BGP4_FIB_LIST_FIRST_ROUTE (u4Context) = pTempRoute;
                    }
                    /* Update the Last node of FIB_UPDATE List */
                    BGP4_FIB_LIST_LAST_ROUTE (u4Context) = pRoute;
                }
                if ((BGP4_RT_GET_FLAGS (pRoute) &
                     BGP4_RT_IN_EXT_PEER_ADVT_LIST) ==
                    BGP4_RT_IN_EXT_PEER_ADVT_LIST)
                {
                    /* Update the EXT_PEER_ADVT List */
                    if ((BGP4_EXT_PEER_LIST_FIRST_ROUTE (u4Context) == pRoute)
                        && (BGP4_EXT_PEER_LIST_LAST_ROUTE (u4Context) !=
                            pRoute))
                    {
                        /* Update the First route of EXT_PEER_ADVT List */
                        for (pTempRoute = pNextRoute; pTempRoute != NULL;
                             pTempRoute =
                             BGP4_RT_CHG_LIST_LINK_NEXT (pTempRoute))
                        {
                            if ((BGP4_RT_GET_FLAGS (pTempRoute) &
                                 BGP4_RT_IN_EXT_PEER_ADVT_LIST) ==
                                BGP4_RT_IN_EXT_PEER_ADVT_LIST)
                            {
                                break;
                            }
                        }
                        BGP4_EXT_PEER_LIST_FIRST_ROUTE (u4Context) = pTempRoute;
                    }
                    /* Update the Last node of EXT_PEER_ADVT List */
                    BGP4_EXT_PEER_LIST_LAST_ROUTE (u4Context) = pRoute;
                }
            }
        }
        else
        {
            /* If the route is not present in the change list then add the
             * route at the end of the list.
             */
            if (BGP4_CHG_LIST_LAST_ROUTE (u4Context) != pRoute)
            {
                /* Route is not present in the change list. */
                BGP4_RT_CHG_LIST_LINK_NEXT (BGP4_CHG_LIST_LAST_ROUTE
                                            (u4Context)) = pRoute;
                BGP4_RT_CHG_LIST_LINK_PREV (pRoute) =
                    BGP4_CHG_LIST_LAST_ROUTE (u4Context);
                BGP4_RT_CHG_LIST_LINK_NEXT (pRoute) = NULL;
                BGP4_CHG_LIST_LAST_ROUTE (u4Context) = pRoute;
                BGP4_CHG_LIST_COUNT (u4Context)++;
            }
        }
    }
    else
    {
        /* List is not empty. Add the route according to the Group Flag. */
        if (u4Group == BGP4_PREPEND)
        {
            /* If u4Group = BGP4_PREPEND, then add the route as the first node */
            pPrevRoute = NULL;
            pNextRoute = BGP4_DLL_FIRST_ROUTE (pList);
            BGP4_DLL_FIRST_ROUTE (pList) = pRoute;
            BGP4_DLL_COUNT (pList)++;
        }
        else
        {
            /* Add the given route to the list */
            pPrevRoute = BGP4_DLL_LAST_ROUTE (pList);
            pNextRoute = NULL;
            BGP4_DLL_LAST_ROUTE (pList) = pRoute;
            BGP4_DLL_COUNT (pList)++;
        }
    }
    /* Update the route's link information */
    switch (u1List)
    {
        case BGP4_FIB_UPD_LIST_INDEX:
        case BGP4_INT_PEER_LIST_INDEX:
        case BGP4_EXT_PEER_LIST_INDEX:
            break;

        case BGP4_NEXTHOP_LIST_INDEX:
            BGP4_RT_NEXTHOP_LINK_PREV (pRoute) = pPrevRoute;
            BGP4_RT_NEXTHOP_LINK_NEXT (pRoute) = pNextRoute;
            if (u4Group == BGP4_PREPEND)
            {
                BGP4_RT_NEXTHOP_LINK_PREV (pNextRoute) = pRoute;
            }
            else
            {
                BGP4_RT_NEXTHOP_LINK_NEXT (pPrevRoute) = pRoute;
            }
            break;

        case BGP4_PROTOCOL_LIST_INDEX:
            BGP4_RT_PROTO_LINK_PREV (pRoute) = pPrevRoute;
            BGP4_RT_PROTO_LINK_NEXT (pRoute) = pNextRoute;
            if (u4Group == BGP4_PREPEND)
            {
                BGP4_RT_PROTO_LINK_PREV (pNextRoute) = pRoute;
            }
            else
            {
                BGP4_RT_PROTO_LINK_NEXT (pPrevRoute) = pRoute;
            }
            break;
#ifdef L3VPN
        case BGP4_VPN4_FEAS_WDRAW_LIST_INDEX:
            BGP4_RT_VPN4_LINK_PREV (pRoute) = pPrevRoute;
            BGP4_RT_VPN4_LINK_NEXT (pRoute) = pNextRoute;
            if (u4Group == BGP4_PREPEND)
            {
                BGP4_RT_VPN4_LINK_PREV (pNextRoute) = pRoute;
            }
            else
            {
                BGP4_RT_VPN4_LINK_NEXT (pPrevRoute) = pRoute;
            }
            break;
#endif

        default:
            break;
    }

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4DshDelinkRouteFromList                                */
/* Description   : This routine removes the route from the list and also     */
/*               : frees the route profile if necessary.                     */
/* Input(s)      : List of Route profiles (pList),                           */
/*                 Route which needs to be removed (pRoute)                  */
/*                 u1List - Refers to the list to which the route is added.  */
/*                          Currently takes value as                         */
/*                          BGP4_FIB_UPD_LIST_INDEX  - 1                     */
/*                          BGP4_INT_PEER_LIST_INDEX - 2                     */
/*                          BGP4_EXT_PEER_LIST_INDEX - 3                     */
/*                          BGP4_NEXTHOP_LIST_INDEX  - 4                     */
/*                          BGP4_PROTOCOL_LIST_INDEX - 5                     */
/* Output(s)     : Updated list of route profiles (pList)                    */
/* Return(s)     : Removed route profile                                     */
/*****************************************************************************/
tRouteProfile      *
Bgp4DshDelinkRouteFromList (tBGP4_DLL * pList, tRouteProfile * pRoute,
                            UINT1 u1List)
{
    tRouteProfile      *pPrevRoute = NULL;
    tRouteProfile      *pNextRoute = NULL;
    tRouteProfile      *pListRoute = NULL;
    UINT4               u4Context = 0;
    UINT1               u1CanRtUnlinked = BGP4_TRUE;
#ifdef L3VPN
    UINT4               u4AsafiMask = 0;
#endif

    /* Unlink the Route from the list. */
    u4Context = BGP4_RT_CXT_ID (pRoute);

#ifdef L3VPN
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRoute),
                           BGP4_RT_SAFI_INFO (pRoute), u4AsafiMask);
    if ((u4AsafiMask == CAP_MP_VPN4_UNICAST) &&
        (u1List == BGP4_EXT_PEER_LIST_INDEX))
    {
        u4Context = pRoute->u4ExtListCxtId;
        pRoute->u4ExtListCxtId = 0;
    }
#endif
    switch (u1List)
    {
        case BGP4_FIB_UPD_LIST_INDEX:
            if (((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_IN_INT_PEER_ADVT_LIST) ==
                 BGP4_RT_IN_INT_PEER_ADVT_LIST) ||
                ((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_IN_EXT_PEER_ADVT_LIST) ==
                 BGP4_RT_IN_EXT_PEER_ADVT_LIST))
            {
                /* Route is present in INT/EXT Peer Advt list. No need to
                 * remove the route from the chg list. But however update the
                 * FIB-list pointer. */
                u1CanRtUnlinked = BGP4_FALSE;
            }
            else
            {
                u1CanRtUnlinked = BGP4_TRUE;
            }

            pPrevRoute = BGP4_RT_CHG_LIST_LINK_PREV (pRoute);
            pNextRoute = BGP4_RT_CHG_LIST_LINK_NEXT (pRoute);

            /* Update the List */
            if ((pPrevRoute == NULL) && (pNextRoute == NULL))
            {
                /* Only one route in change List. So clear the input list
                 * and the change list. */
                BGP4_FIB_LIST_FIRST_ROUTE (u4Context) = NULL;
                BGP4_FIB_LIST_LAST_ROUTE (u4Context) = NULL;
                BGP4_FIB_LIST_COUNT (u4Context) = 0;
                if (u1CanRtUnlinked == BGP4_TRUE)
                {
                    BGP4_CHG_LIST_FIRST_ROUTE (u4Context) = NULL;
                    BGP4_CHG_LIST_LAST_ROUTE (u4Context) = NULL;
                }
            }
            else
            {
                if (u1CanRtUnlinked == BGP4_TRUE)
                {
                    /* Update Previous Route's next value. */
                    if (pPrevRoute != NULL)
                    {
                        BGP4_RT_CHG_LIST_LINK_NEXT (pPrevRoute) = pNextRoute;
                    }
                    else
                    {
                        /* Route should be first route in Change list. */
                        BGP4_CHG_LIST_FIRST_ROUTE (u4Context) = pNextRoute;
                    }

                    /* Update Next route's previous value. */
                    if (pNextRoute != NULL)
                    {
                        BGP4_RT_CHG_LIST_LINK_PREV (pNextRoute) = pPrevRoute;
                    }
                    else
                    {
                        /* Route should be last route in Change list. */
                        BGP4_CHG_LIST_LAST_ROUTE (u4Context) = pPrevRoute;
                    }
                }

                /* Update FIRST Node in List */
                if (BGP4_FIB_LIST_FIRST_ROUTE (u4Context) == pRoute)
                {
                    /* First route in list is removed. */
                    pListRoute = BGP4_RT_CHG_LIST_LINK_NEXT (pRoute);
                    for (;;)
                    {
                        if ((pListRoute == NULL) ||
                            ((BGP4_RT_GET_FLAGS (pListRoute) &
                              BGP4_RT_IN_FIB_UPD_LIST) ==
                             BGP4_RT_IN_FIB_UPD_LIST))
                        {
                            break;
                        }
                        pListRoute = BGP4_RT_CHG_LIST_LINK_NEXT (pListRoute);
                    }
                    BGP4_FIB_LIST_FIRST_ROUTE (u4Context) = pListRoute;
                }

                /* Update LAST Node in List */
                if (BGP4_FIB_LIST_LAST_ROUTE (u4Context) == pRoute)
                {
                    /* Last route in the List is deleted. */
                    pListRoute = BGP4_RT_CHG_LIST_LINK_PREV (pRoute);
                    for (;;)
                    {
                        if ((pListRoute == NULL) ||
                            ((BGP4_RT_GET_FLAGS (pListRoute) &
                              BGP4_RT_IN_FIB_UPD_LIST) ==
                             BGP4_RT_IN_FIB_UPD_LIST))
                        {
                            break;
                        }
                        pListRoute = BGP4_RT_CHG_LIST_LINK_PREV (pListRoute);
                    }
                    BGP4_FIB_LIST_LAST_ROUTE (u4Context) = pListRoute;
                }
                BGP4_FIB_LIST_COUNT (u4Context)--;
            }

            if (u1CanRtUnlinked == BGP4_TRUE)
            {
                /* Clear the current route. */
                BGP4_RT_CHG_LIST_LINK_PREV (pRoute) = NULL;
                BGP4_RT_CHG_LIST_LINK_NEXT (pRoute) = NULL;
                BGP4_CHG_LIST_COUNT (u4Context)--;
            }
            break;

        case BGP4_INT_PEER_LIST_INDEX:
            if (((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_IN_FIB_UPD_LIST) ==
                 BGP4_RT_IN_FIB_UPD_LIST) ||
                ((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_IN_EXT_PEER_ADVT_LIST) ==
                 BGP4_RT_IN_EXT_PEER_ADVT_LIST))
            {
                /* Route is present in FIB update/EXT Peer Advt list. No need to
                 * remove the route from the chg list. But however update the
                 * INT-Peer-Advt list pointer. */
                u1CanRtUnlinked = BGP4_FALSE;
            }
            else
            {
                u1CanRtUnlinked = BGP4_TRUE;
            }
            pPrevRoute = BGP4_RT_CHG_LIST_LINK_PREV (pRoute);
            pNextRoute = BGP4_RT_CHG_LIST_LINK_NEXT (pRoute);

            /* Update the List */
            if ((pPrevRoute == NULL) && (pNextRoute == NULL))
            {
                /* Only one route in List. So clear the input list
                 * and change list. */
                BGP4_INT_PEER_LIST_FIRST_ROUTE (u4Context) = NULL;
                BGP4_INT_PEER_LIST_LAST_ROUTE (u4Context) = NULL;
                BGP4_INT_PEER_LIST_COUNT (u4Context) = 0;
                if (u1CanRtUnlinked == BGP4_TRUE)
                {
                    BGP4_CHG_LIST_FIRST_ROUTE (u4Context) = NULL;
                    BGP4_CHG_LIST_LAST_ROUTE (u4Context) = NULL;
                }
            }
            else
            {
                if (u1CanRtUnlinked == BGP4_TRUE)
                {
                    /* Update Previous routes next value. */
                    if (pPrevRoute != NULL)
                    {
                        BGP4_RT_CHG_LIST_LINK_NEXT (pPrevRoute) = pNextRoute;
                    }
                    else
                    {
                        /* Route should be first route in Change list. */
                        BGP4_CHG_LIST_FIRST_ROUTE (u4Context) = pNextRoute;
                    }

                    /* Update Next route's previous value. */
                    if (pNextRoute != NULL)
                    {
                        BGP4_RT_CHG_LIST_LINK_PREV (pNextRoute) = pPrevRoute;
                    }
                    else
                    {
                        /* Route should be last route in Change list. */
                        BGP4_CHG_LIST_LAST_ROUTE (u4Context) = pPrevRoute;
                    }
                }

                /* Update FIRST Node in List */
                if (BGP4_INT_PEER_LIST_FIRST_ROUTE (u4Context) == pRoute)
                {
                    /* First route in list is removed. */
                    pListRoute = BGP4_RT_CHG_LIST_LINK_NEXT (pRoute);
                    for (;;)
                    {
                        if ((pListRoute == NULL) ||
                            ((BGP4_RT_GET_FLAGS (pListRoute) &
                              BGP4_RT_IN_INT_PEER_ADVT_LIST) ==
                             BGP4_RT_IN_INT_PEER_ADVT_LIST))
                        {
                            break;
                        }
                        pListRoute = BGP4_RT_CHG_LIST_LINK_NEXT (pListRoute);
                    }
                    BGP4_INT_PEER_LIST_FIRST_ROUTE (u4Context) = pListRoute;
                }

                /* Update LAST Node in List */
                if (BGP4_INT_PEER_LIST_LAST_ROUTE (u4Context) == pRoute)
                {
                    /* Last route in the List is deleted. */
                    pListRoute = BGP4_RT_CHG_LIST_LINK_PREV (pRoute);
                    for (;;)
                    {
                        if ((pListRoute == NULL) ||
                            ((BGP4_RT_GET_FLAGS (pListRoute) &
                              BGP4_RT_IN_INT_PEER_ADVT_LIST) ==
                             BGP4_RT_IN_INT_PEER_ADVT_LIST))
                        {
                            break;
                        }
                        pListRoute = BGP4_RT_CHG_LIST_LINK_PREV (pListRoute);
                    }
                    BGP4_INT_PEER_LIST_LAST_ROUTE (u4Context) = pListRoute;
                }
                BGP4_INT_PEER_LIST_COUNT (u4Context)--;
            }

            if (u1CanRtUnlinked == BGP4_TRUE)
            {
                /* Clear the current route. */
                BGP4_RT_CHG_LIST_LINK_PREV (pRoute) = NULL;
                BGP4_RT_CHG_LIST_LINK_NEXT (pRoute) = NULL;
                BGP4_CHG_LIST_COUNT (u4Context)--;
            }
            break;

        case BGP4_EXT_PEER_LIST_INDEX:
            if (((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_IN_FIB_UPD_LIST) ==
                 BGP4_RT_IN_FIB_UPD_LIST) ||
                ((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_IN_INT_PEER_ADVT_LIST) ==
                 BGP4_RT_IN_INT_PEER_ADVT_LIST))
            {
                /* Route is present in FIB update/INT Peer Advt list. No need to
                 * remove the route from the chg list. But however update the
                 * EXT-Peer-Advt list pointer. */
                u1CanRtUnlinked = BGP4_FALSE;
            }
            else
            {
                u1CanRtUnlinked = BGP4_TRUE;
            }

            pPrevRoute = BGP4_RT_CHG_LIST_LINK_PREV (pRoute);
            pNextRoute = BGP4_RT_CHG_LIST_LINK_NEXT (pRoute);

            /* Update the List */
            if ((pPrevRoute == NULL) && (pNextRoute == NULL))
            {
                /* Only one route in List. So clear the iput list
                 * and change list. */
                BGP4_EXT_PEER_LIST_FIRST_ROUTE (u4Context) = NULL;
                BGP4_EXT_PEER_LIST_LAST_ROUTE (u4Context) = NULL;
                BGP4_EXT_PEER_LIST_COUNT (u4Context) = 0;
                if (u1CanRtUnlinked == BGP4_TRUE)
                {
                    BGP4_CHG_LIST_FIRST_ROUTE (u4Context) = NULL;
                    BGP4_CHG_LIST_LAST_ROUTE (u4Context) = NULL;
                }
            }
            else
            {
                if (u1CanRtUnlinked == BGP4_TRUE)
                {
                    /* Update previous routes next value. */
                    if (pPrevRoute != NULL)
                    {
                        BGP4_RT_CHG_LIST_LINK_NEXT (pPrevRoute) = pNextRoute;
                    }
                    else
                    {
                        /* Route should be first route in Change list. */
                        BGP4_CHG_LIST_FIRST_ROUTE (u4Context) = pNextRoute;
                    }

                    /* Update Next routes previous value. */
                    if (pNextRoute != NULL)
                    {
                        BGP4_RT_CHG_LIST_LINK_PREV (pNextRoute) = pPrevRoute;
                    }
                    else
                    {
                        /* Route should be last route in Change list. */
                        BGP4_CHG_LIST_LAST_ROUTE (u4Context) = pPrevRoute;
                    }
                }

                /* Updated FIRST Node in List */
                if (BGP4_EXT_PEER_LIST_FIRST_ROUTE (u4Context) == pRoute)
                {
                    /* First route in list is removed. */
                    pListRoute = BGP4_RT_CHG_LIST_LINK_NEXT (pRoute);
                    for (;;)
                    {
                        if ((pListRoute == NULL) ||
                            ((BGP4_RT_GET_FLAGS (pListRoute) &
                              BGP4_RT_IN_EXT_PEER_ADVT_LIST) ==
                             BGP4_RT_IN_EXT_PEER_ADVT_LIST))
                        {
                            break;
                        }
                        pListRoute = BGP4_RT_CHG_LIST_LINK_NEXT (pListRoute);
                    }
                    BGP4_EXT_PEER_LIST_FIRST_ROUTE (u4Context) = pListRoute;
                }

                /* Update LAST Node in List */
                if (BGP4_EXT_PEER_LIST_LAST_ROUTE (u4Context) == pRoute)
                {
                    /* Last route in the List is deleted. */
                    pListRoute = BGP4_RT_CHG_LIST_LINK_PREV (pRoute);
                    for (;;)
                    {
                        if ((pListRoute == NULL) ||
                            ((BGP4_RT_GET_FLAGS (pListRoute) &
                              BGP4_RT_IN_EXT_PEER_ADVT_LIST) ==
                             BGP4_RT_IN_EXT_PEER_ADVT_LIST))
                        {
                            break;
                        }
                        pListRoute = BGP4_RT_CHG_LIST_LINK_PREV (pListRoute);
                    }
                    BGP4_EXT_PEER_LIST_LAST_ROUTE (u4Context) = pListRoute;
                }
                BGP4_EXT_PEER_LIST_COUNT (u4Context)--;
            }

            if (u1CanRtUnlinked == BGP4_TRUE)
            {
                /* Clear the current route. */
                BGP4_RT_CHG_LIST_LINK_PREV (pRoute) = NULL;
                BGP4_RT_CHG_LIST_LINK_NEXT (pRoute) = NULL;
                BGP4_CHG_LIST_COUNT (u4Context)--;
            }
            break;

        case BGP4_NEXTHOP_LIST_INDEX:
            pPrevRoute = BGP4_RT_NEXTHOP_LINK_PREV (pRoute);
            pNextRoute = BGP4_RT_NEXTHOP_LINK_NEXT (pRoute);

            /* Update the List */
            if ((pPrevRoute == NULL) && (pNextRoute == NULL))
            {
                /* In Case of NEXT-HOP list, the route may or may
                 * not be present in this list, even though it is present
                 * in the RIB. This is true for BGP4_INVALID_ROUTE other than
                 * the NEXT-HOP unreacheable route. So under this case
                 * check whether the route present in Next-HOP List matches
                 * the input route or not and then delete it. */
                if (BGP4_DLL_FIRST_ROUTE (pList) == pRoute)
                {
                    /* Only one route in List. So clear the list. */
                    BGP4_DLL_FIRST_ROUTE (pList) = NULL;
                    BGP4_DLL_LAST_ROUTE (pList) = NULL;
                    BGP4_DLL_COUNT (pList) = 0;
                }
                else
                {
                    /* Route is not present in Next-Hop List. Route should have
                     * been an Invalid Route earlier. */
                    return (pRoute);
                }
            }
            else
            {
                if (pPrevRoute != NULL)
                {
                    BGP4_RT_NEXTHOP_LINK_NEXT (pPrevRoute) = pNextRoute;
                }
                else
                {
                    /* First route in list is removed. */
                    BGP4_DLL_FIRST_ROUTE (pList) = pNextRoute;
                }
                if (pNextRoute != NULL)
                {
                    BGP4_RT_NEXTHOP_LINK_PREV (pNextRoute) = pPrevRoute;
                }
                else
                {
                    /* Last route in the List is deleted. */
                    BGP4_DLL_LAST_ROUTE (pList) = pPrevRoute;
                }
                BGP4_DLL_COUNT (pList)--;
            }

            /* Clear the current route. */
            BGP4_RT_NEXTHOP_LINK_PREV (pRoute) = NULL;
            BGP4_RT_NEXTHOP_LINK_NEXT (pRoute) = NULL;
            break;

        case BGP4_PROTOCOL_LIST_INDEX:
            pPrevRoute = BGP4_RT_PROTO_LINK_PREV (pRoute);
            pNextRoute = BGP4_RT_PROTO_LINK_NEXT (pRoute);

            /* Update the List */
            if ((pPrevRoute == NULL) && (pNextRoute == NULL))
            {
                /* Either only one route is present in the List or the
                 * the given route is not present in the List. If present
                 * unlink the route else just return.
                 */
                if ((BGP4_DLL_FIRST_ROUTE (pList) != pRoute) &&
                    (BGP4_DLL_LAST_ROUTE (pList) != pRoute))
                {
                    /* Route is not present in List */
                    return pRoute;
                }
                else
                {
                    /* Only one route in List. So clear the list. */
                    BGP4_DLL_FIRST_ROUTE (pList) = NULL;
                    BGP4_DLL_LAST_ROUTE (pList) = NULL;
                    BGP4_DLL_COUNT (pList) = 0;
                }
            }
            else
            {
                if (pPrevRoute != NULL)
                {
                    BGP4_RT_PROTO_LINK_NEXT (pPrevRoute) = pNextRoute;
                }
                else
                {
                    /* First route in list is removed. */
                    BGP4_DLL_FIRST_ROUTE (pList) = pNextRoute;
                }
                if (pNextRoute != NULL)
                {
                    BGP4_RT_PROTO_LINK_PREV (pNextRoute) = pPrevRoute;
                }
                else
                {
                    /* Last route in the List is deleted. */
                    BGP4_DLL_LAST_ROUTE (pList) = pPrevRoute;
                }
                BGP4_DLL_COUNT (pList)--;
            }

            /* Clear the current route. */
            BGP4_RT_PROTO_LINK_PREV (pRoute) = NULL;
            BGP4_RT_PROTO_LINK_NEXT (pRoute) = NULL;
            break;
#ifdef L3VPN
        case BGP4_VPN4_FEAS_WDRAW_LIST_INDEX:
            pPrevRoute = BGP4_RT_VPN4_LINK_PREV (pRoute);
            pNextRoute = BGP4_RT_VPN4_LINK_NEXT (pRoute);

            /* Update the List */
            if ((pPrevRoute == NULL) && (pNextRoute == NULL))
            {
                BGP4_ASSERT (BGP4_DLL_FIRST_ROUTE (pList) == pRoute);
                /* Only one route in List. So clear the list. */
                BGP4_DLL_FIRST_ROUTE (pList) = NULL;
                BGP4_DLL_LAST_ROUTE (pList) = NULL;
                BGP4_DLL_COUNT (pList) = 0;
            }
            else
            {
                if (pPrevRoute != NULL)
                {
                    BGP4_RT_VPN4_LINK_NEXT (pPrevRoute) = pNextRoute;
                }
                else
                {
                    /* First route in list is removed. */
                    BGP4_DLL_FIRST_ROUTE (pList) = pNextRoute;
                }
                if (pNextRoute != NULL)
                {
                    BGP4_RT_VPN4_LINK_PREV (pNextRoute) = pPrevRoute;
                }
                else
                {
                    /* Last route in the List is deleted. */
                    BGP4_DLL_LAST_ROUTE (pList) = pPrevRoute;
                }
                BGP4_DLL_COUNT (pList)--;
            }

            /* Clear the current route. */
            BGP4_RT_VPN4_LINK_PREV (pRoute) = NULL;
            BGP4_RT_VPN4_LINK_NEXT (pRoute) = NULL;
            break;
#endif

        default:
            break;
    }

    /* Release the Route Profile structure. */
    Bgp4DshReleaseRtInfo (pRoute);

    return (pRoute);
}

/*****************************************************************************/
/* Function Name : Bgp4DshClearLinkList                                      */
/* Description   : Releases the given list of route profiles.                */
/* Input(s)      : List of route profiles (pTsList)                          */
/*                 u1List - Refers to the list to which the route is added.  */
/*                          Currently takes value as                         */
/*                          BGP4_FIB_UPD_LIST_INDEX  - 1                     */
/*                          BGP4_INT_PEER_LIST_INDEX - 2                     */
/*                          BGP4_EXT_PEER_LIST_INDEX - 3                     */
/*                          BGP4_NEXTHOP_LIST_INDEX  - 4                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4DshClearLinkList (UINT4 u4Context, tBGP4_DLL * pTsList, UINT1 u1List)
{
    tRouteProfile      *pRoute = NULL;
    tRouteProfile      *pListNextRoute = NULL;
    tRouteProfile      *pPrevRoute = NULL;
    tRouteProfile      *pNextRoute = NULL;

    if (pTsList->u4Count == 0)
    {
        /* List is empty. */
        return BGP4_SUCCESS;
    }

    switch (u1List)
    {
            /* Initialise the route to be processed. */
        case BGP4_FIB_UPD_LIST_INDEX:
            pRoute = BGP4_FIB_LIST_FIRST_ROUTE (u4Context);
            break;

        case BGP4_INT_PEER_LIST_INDEX:
            pRoute = BGP4_INT_PEER_LIST_FIRST_ROUTE (u4Context);
            break;

        case BGP4_EXT_PEER_LIST_INDEX:
            pRoute = BGP4_EXT_PEER_LIST_FIRST_ROUTE (u4Context);
            break;

        case BGP4_NEXTHOP_LIST_INDEX:
            pRoute = BGP4_DLL_FIRST_ROUTE (pTsList);
            break;

        default:
            return BGP4_SUCCESS;
    }

    for (;;)
    {
        if (pRoute == NULL)
        {
            /* No more route present in List */
            break;
        }
        switch (u1List)
        {
            case BGP4_FIB_UPD_LIST_INDEX:
                pListNextRoute = BGP4_RT_CHG_LIST_LINK_NEXT (pRoute);
                for (;;)
                {
                    if ((pListNextRoute == NULL) ||
                        ((BGP4_RT_GET_FLAGS (pListNextRoute) &
                          BGP4_RT_IN_FIB_UPD_LIST) == BGP4_RT_IN_FIB_UPD_LIST))
                    {
                        break;
                    }
                    pListNextRoute =
                        BGP4_RT_CHG_LIST_LINK_NEXT (pListNextRoute);
                }

                if (((BGP4_RT_GET_FLAGS (pRoute) &
                      BGP4_RT_IN_INT_PEER_ADVT_LIST) ==
                     BGP4_RT_IN_INT_PEER_ADVT_LIST) ||
                    ((BGP4_RT_GET_FLAGS (pRoute) &
                      BGP4_RT_IN_EXT_PEER_ADVT_LIST) ==
                     BGP4_RT_IN_EXT_PEER_ADVT_LIST))
                {
                    /* Just reset the FIB flag */
                    BGP4_RT_RESET_FLAG (pRoute, BGP4_RT_IN_FIB_UPD_LIST);
                }
                else
                {
                    /* Clear the current route. */
                    BGP4_RT_RESET_FLAG (pRoute, BGP4_RT_IN_FIB_UPD_LIST);

                    /* Update the prev and next route pointer. */
                    pPrevRoute = BGP4_RT_CHG_LIST_LINK_PREV (pRoute);
                    pNextRoute = BGP4_RT_CHG_LIST_LINK_NEXT (pRoute);

                    if (pPrevRoute != NULL)
                    {
                        BGP4_RT_CHG_LIST_LINK_NEXT (pPrevRoute) = pNextRoute;
                    }

                    if (pNextRoute != NULL)
                    {
                        BGP4_RT_CHG_LIST_LINK_PREV (pNextRoute) = pPrevRoute;
                    }

                    BGP4_RT_CHG_LIST_LINK_PREV (pRoute) = NULL;
                    BGP4_RT_CHG_LIST_LINK_NEXT (pRoute) = NULL;
                }
                break;

            case BGP4_INT_PEER_LIST_INDEX:
                pListNextRoute = BGP4_RT_CHG_LIST_LINK_NEXT (pRoute);
                for (;;)
                {
                    if ((pListNextRoute == NULL) ||
                        ((BGP4_RT_GET_FLAGS (pListNextRoute) &
                          BGP4_RT_IN_INT_PEER_ADVT_LIST) ==
                         BGP4_RT_IN_INT_PEER_ADVT_LIST))
                    {
                        break;
                    }
                    pListNextRoute =
                        BGP4_RT_CHG_LIST_LINK_NEXT (pListNextRoute);
                }

                if (((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_IN_FIB_UPD_LIST) ==
                     BGP4_RT_IN_FIB_UPD_LIST) ||
                    ((BGP4_RT_GET_FLAGS (pRoute) &
                      BGP4_RT_IN_EXT_PEER_ADVT_LIST) ==
                     BGP4_RT_IN_EXT_PEER_ADVT_LIST))
                {
                    /* Just reset the FIB flag */
                    BGP4_RT_RESET_FLAG (pRoute, BGP4_RT_IN_INT_PEER_ADVT_LIST);
                }
                else
                {
                    /* Clear the current route. */
                    BGP4_RT_RESET_FLAG (pRoute, BGP4_RT_IN_INT_PEER_ADVT_LIST);

                    /* Update the prev and next route pointer. */
                    pPrevRoute = BGP4_RT_CHG_LIST_LINK_PREV (pRoute);
                    pNextRoute = BGP4_RT_CHG_LIST_LINK_NEXT (pRoute);

                    if (pPrevRoute != NULL)
                    {
                        BGP4_RT_CHG_LIST_LINK_NEXT (pPrevRoute) = pNextRoute;
                    }

                    if (pNextRoute != NULL)
                    {
                        BGP4_RT_CHG_LIST_LINK_PREV (pNextRoute) = pPrevRoute;
                    }

                    BGP4_RT_CHG_LIST_LINK_PREV (pRoute) = NULL;
                    BGP4_RT_CHG_LIST_LINK_NEXT (pRoute) = NULL;
                }
                break;

            case BGP4_EXT_PEER_LIST_INDEX:
                pListNextRoute = BGP4_RT_CHG_LIST_LINK_NEXT (pRoute);
                for (;;)
                {
                    if ((pListNextRoute == NULL) ||
                        ((BGP4_RT_GET_FLAGS (pListNextRoute) &
                          BGP4_RT_IN_EXT_PEER_ADVT_LIST) ==
                         BGP4_RT_IN_EXT_PEER_ADVT_LIST))
                    {
                        break;
                    }
                    pListNextRoute =
                        BGP4_RT_CHG_LIST_LINK_NEXT (pListNextRoute);
                }

                if (((BGP4_RT_GET_FLAGS (pRoute) & BGP4_RT_IN_FIB_UPD_LIST) ==
                     BGP4_RT_IN_FIB_UPD_LIST) ||
                    ((BGP4_RT_GET_FLAGS (pRoute) &
                      BGP4_RT_IN_INT_PEER_ADVT_LIST) ==
                     BGP4_RT_IN_INT_PEER_ADVT_LIST))
                {
                    /* Just reset the FIB flag */
                    BGP4_RT_RESET_FLAG (pRoute, BGP4_RT_IN_EXT_PEER_ADVT_LIST);
                }
                else
                {
                    /* Clear the current route. */
                    BGP4_RT_RESET_FLAG (pRoute, BGP4_RT_IN_EXT_PEER_ADVT_LIST);

                    /* Update the prev and next route pointer. */
                    pPrevRoute = BGP4_RT_CHG_LIST_LINK_PREV (pRoute);
                    pNextRoute = BGP4_RT_CHG_LIST_LINK_NEXT (pRoute);

                    if (pPrevRoute != NULL)
                    {
                        BGP4_RT_CHG_LIST_LINK_NEXT (pPrevRoute) = pNextRoute;
                    }

                    if (pNextRoute != NULL)
                    {
                        BGP4_RT_CHG_LIST_LINK_PREV (pNextRoute) = pPrevRoute;
                    }

                    BGP4_RT_CHG_LIST_LINK_PREV (pRoute) = NULL;
                    BGP4_RT_CHG_LIST_LINK_NEXT (pRoute) = NULL;
                }
                break;

            case BGP4_NEXTHOP_LIST_INDEX:
                pListNextRoute = BGP4_RT_NEXTHOP_LINK_NEXT (pRoute);

                /* Clear the current route. */
                BGP4_RT_NEXTHOP_LINK_PREV (pRoute) = NULL;
                BGP4_RT_NEXTHOP_LINK_NEXT (pRoute) = NULL;
                break;
            default:
                break;

        }

        /* Release the Route Profile structure. */
        Bgp4DshReleaseRtInfo (pRoute);
        pRoute = pListNextRoute;

    }

    /* Reinitialise the list. */
    pTsList->pFirst = NULL;
    pTsList->pLast = NULL;
    pTsList->u4Count = 0;

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Bgp4DshDelinkRouteFromChgList                             */
/* Description   : Removes the given route if present in the Change list.    */
/* Input(s)      : Route to be removed. (pRtProfile)                         */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_SUCCESS                                              */
/*****************************************************************************/
INT4
Bgp4DshDelinkRouteFromChgList (tRouteProfile * pRtProfile)
{
    UINT4               u4Context = 0;
    u4Context = BGP4_RT_CXT_ID (pRtProfile);
    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_IN_FIB_UPD_LIST) ==
        BGP4_RT_IN_FIB_UPD_LIST)
    {
        BGP4_RT_RESET_FLAG (pRtProfile, BGP4_RT_IN_FIB_UPD_LIST);
        Bgp4DshDelinkRouteFromList (BGP4_FIB_UPD_LIST (u4Context), pRtProfile,
                                    BGP4_FIB_UPD_LIST_INDEX);
    }
    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_IN_INT_PEER_ADVT_LIST) ==
        BGP4_RT_IN_INT_PEER_ADVT_LIST)
    {
        BGP4_RT_RESET_FLAG (pRtProfile, (BGP4_RT_IN_INT_PEER_ADVT_LIST |
                                         BGP4_RT_ADVT_NOTTO_INTERNAL));
        Bgp4DshDelinkRouteFromList (BGP4_INT_PEERS_ADVT_LIST (u4Context),
                                    pRtProfile, BGP4_INT_PEER_LIST_INDEX);
    }
    if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_IN_EXT_PEER_ADVT_LIST) ==
        BGP4_RT_IN_EXT_PEER_ADVT_LIST)
    {
        BGP4_RT_RESET_FLAG (pRtProfile, (BGP4_RT_IN_EXT_PEER_ADVT_LIST |
                                         BGP4_RT_ADVT_NOTTO_EXTERNAL));
        Bgp4DshDelinkRouteFromList (BGP4_EXT_PEERS_ADVT_LIST (u4Context),
                                    pRtProfile, BGP4_EXT_PEER_LIST_INDEX);
    }
    return BGP4_SUCCESS;
}
#endif /* BGP4DSH_C */
