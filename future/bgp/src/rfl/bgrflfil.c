/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgrflfil.c,v 1.13 2017/09/15 06:19:54 siva Exp $
 *
 * Description: This file contains functions for filling the
 *              path attributes for sending the update mesage.
 *
 *******************************************************************/
#ifndef _BGRFLFIL_C
#define _BGRFLFIL_C
#include "bgp4com.h"

/**************************************************************************
 Function Name   : RflFillRflPathAttrInfo
 Description     : Fill the Route reflector path attribute information 
                   like originator and cluster information
 Input(s)        : pFeasibleRouteToBeFilled - pointer to the route profile
                                information from which the originator and
                                cluster list path attribute information is
                                to extracted and filled in the BGP4-INFO
                                to be advertised.
 Output(s)       : pAdvtBgpInfo - Updated BGP4-INFO to be advertised. 
 Global Variables Referred :None
 Global variables Modified :None
 Exceptions or Operating System Error Handling : None
 Use of Recursion          : None.
 Returns                   :  RFL_SUCCESS or RFL_FAILURE
**************************************************************************/
INT4
RflFillRflPathAttrInfo (tRouteProfile * pFeasibleRouteToBeFilled,
                        tBgp4Info * pAdvtBgpInfo)
{
    INT4                i4RflStatus = RFL_SUCCESS;

    /* Fill originator and cluster path attribute information */
    RflFillOrigPathAttrInfo (pFeasibleRouteToBeFilled, pAdvtBgpInfo);

    i4RflStatus = RflFillClusterPathAttrInfo (pFeasibleRouteToBeFilled,
                                              pAdvtBgpInfo);
    return i4RflStatus;
}

/**************************************************************************
 Function Name   : RflFillOrigPathAttrInfo
 Description     : Fill the originator id path attribute information
 Input(s)        : pFeasibleRouteToBeFilled - pointer to the route profile
                                information from which the originator id
                                path attribute information is to be filled
                   ppu1MsgBuf - pointer to the message buffer
                   pu2BufLen- pointer to the available buffer length
 Output(s)       : pAdvtBgpInfo - Updated BGP4-INFO to be advertised. 
 Global Variables Referred :
 Global variables Modified :
 Exceptions or Operating System Error Handling : None
 Use of Recursion          : None.
 Returns                   :  RFL_SUCCESS or RFL_FAILURE
**************************************************************************/
INT4
RflFillOrigPathAttrInfo (tRouteProfile * pFeasibleRouteToBeFilled,
                         tBgp4Info * pAdvtBgpInfo)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    UINT4               u4RcvdOrigId = NO_ORIG_ID;
    INT4                i4RflStatus = RFL_SUCCESS;

    i4RflStatus = RflGetOriginatorId (pFeasibleRouteToBeFilled, &u4RcvdOrigId);
    if (i4RflStatus == RFL_FAILURE)
    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                       "\tORIGINATOR ID is not Present for the route %s\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                        (pFeasibleRouteToBeFilled),
                                        BGP4_RT_AFI_INFO
                                        (pFeasibleRouteToBeFilled)));
        return RFL_SUCCESS;
    }

    /* Fill the Originator id  Flag */
    if (u4RcvdOrigId == NO_ORIG_ID)
    {
        /* If received route does not contain originator id then 
         * local originator id has to be filled */

        pPeerEntry = BGP4_RT_PEER_ENTRY (pFeasibleRouteToBeFilled);

        if (pPeerEntry == NULL)
        {
            /* No need to Fill the Originator Id */
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPeer entry is NULL. No need to fill ORIGINATOR ID for the route %s\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                            (pFeasibleRouteToBeFilled),
                                            BGP4_RT_AFI_INFO
                                            (pFeasibleRouteToBeFilled)));
            return RFL_SUCCESS;
        }
        else
        {
            BGP4_INFO_ORIG_ID (pAdvtBgpInfo) = BGP4_PEER_BGP_ID (pPeerEntry);
        }
    }
    else
    {
        /* If rcvd route contains originator id, that should 
         * not be modified */
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                       "\tThe Received route %s already contains originator id, "
                       " no need to modify\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                        (pFeasibleRouteToBeFilled),
                                        BGP4_RT_AFI_INFO
                                        (pFeasibleRouteToBeFilled)));
        BGP4_INFO_ORIG_ID (pAdvtBgpInfo) = u4RcvdOrigId;
    }
    BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_ORIG_ID_MASK;
    BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                   BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                   "\tOriginator ID %d attribute is successfully filled for the route %s\n",
                   BGP4_INFO_ORIG_ID (pAdvtBgpInfo),
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                    (pFeasibleRouteToBeFilled),
                                    BGP4_RT_AFI_INFO
                                    (pFeasibleRouteToBeFilled)));
    return RFL_SUCCESS;
}

/**************************************************************************
 Function Name   : RflFillClusterPathAttrInfo
Description     : Fill the ClusterList path attribute information
Input(s)        : pFeasibleRouteToBeFilled - pointer to the route profile
information from which the Cluster List 
                                path attribute information is to be filled
                   ppu1MsgBuf - pointer to the message buffer
                   pu2BufLen- pointer to the available buffer length
 Output(s)       : pAdvtBgpInfo - Updated BGP4-INFO to be advertised. 
 Global Variables Referred :None
 Global variables Modified :None
 Exceptions or Operating System Error Handling : None
 Use of Recursion          : None.
 Returns                   :  RFL_SUCCESS or RFL_FAILURE
**************************************************************************/
INT4
RflFillClusterPathAttrInfo (tRouteProfile * pFeasibleRouteToBeFilled,
                            tBgp4Info * pAdvtBgpInfo)
{
    tClusterList       *pClusterList = NULL;
    UINT4               u4FillClusterId = 0;
    INT4                i4RflStatus = RFL_SUCCESS;
    UINT2               u2CidPattrLen = 0;
    UINT2               u2ClusterCount = 0;
    UINT2               u2CidIndx = 0;
    UINT1              *pu1ClusterInfo = NULL;
    UINT1              *pu1ClusList = NULL;

    i4RflStatus = RflGetClusterList (pFeasibleRouteToBeFilled, &pu1ClusterInfo,
                                     &u2ClusterCount);
    if (i4RflStatus == RFL_FAILURE)
    {
        /* No Cluster List is present. */
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                       "\tCluster list is not Present for the route %s\n",
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                        (pFeasibleRouteToBeFilled),
                                        BGP4_RT_AFI_INFO
                                        (pFeasibleRouteToBeFilled)));
        return RFL_SUCCESS;
    }

    if (u2ClusterCount > (MAX_BGP_ATTR_LEN / CLUSTER_ID_LENGTH) - 1)
    {
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_OS_RESOURCE_TRC,
                       BGP4_MOD_NAME,
                       "\t Tuncating the cluster list to maximum"
                       "length %d for cluster node\n",
                       ((MAX_BGP_ATTR_LEN / CLUSTER_ID_LENGTH) - 1));
        u2ClusterCount = (MAX_BGP_ATTR_LEN / CLUSTER_ID_LENGTH) - 1;
    }
    u2CidPattrLen = (UINT2) (u2ClusterCount * CLUSTER_ID_LENGTH);

    /* Update the u2CidPattrLen  value by CLUSTER_ID_LENGTH because
     * we need to add our Cluster-id to the list. */
    u2CidPattrLen += CLUSTER_ID_LENGTH;

    /* Allocate Buffer for storing the CLUSTER and CLUSTER_LIST */
    CLUSTER_LIST_CREATE (pClusterList);
    if (pClusterList == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for Advertise BgpInfo's Cluster"
                  " List FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_CLUSTER_LIST_SIZING_ID]++;
        return RFL_FAILURE;
    }

    ATTRIBUTE_NODE_CREATE (pu1ClusList);
    if (pu1ClusList == NULL)
    {
        CLUSTER_LIST_FREE (pClusterList);
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for Advertise BgpInfo's Cluster"
                  " List FAILED!!!\n");
        gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
        return RFL_FAILURE;
    }

    BGP4_INFO_CLUS_LIST_ATTR (pAdvtBgpInfo) = pClusterList;
    BGP4_INFO_CLUS_LIST_ATTR_VAL (pAdvtBgpInfo) = pu1ClusList;

    /* Fill the cluster Id values
     * Prepend the local cluster Id */
    if (RFL_CLUSTER_ID (BGP4_RT_CXT_ID (pFeasibleRouteToBeFilled)) ==
        NO_CLUSTER_ID)
    {
        PTR_ASSIGN4 (pu1ClusList,
                     (BGP4_LOCAL_BGP_ID
                      (BGP4_RT_CXT_ID (pFeasibleRouteToBeFilled))));
    }
    else
    {
        PTR_ASSIGN4 (pu1ClusList,
                     (RFL_CLUSTER_ID
                      (BGP4_RT_CXT_ID (pFeasibleRouteToBeFilled))));
    }
    pu1ClusList += CLUSTER_ID_LENGTH;
    /* Fill the cluster list rcvd in the route profile - my cluster id has been filled
     * so allow till maximumlength  - 1 */

    for (u2CidIndx = 0; u2CidIndx < u2ClusterCount; u2CidIndx++)
    {
        PTR_FETCH_4 (u4FillClusterId, (pu1ClusterInfo +
                                       (u2CidIndx * CLUSTER_ID_LENGTH)));
        PTR_ASSIGN_4 (pu1ClusList, u4FillClusterId);
        pu1ClusList += CLUSTER_ID_LENGTH;
    }

    BGP4_INFO_CLUS_LIST_COUNT (pAdvtBgpInfo) = (UINT2) (u2ClusterCount + 1);
    if (BGP4_INFO_CLUS_LIST_ATTR (BGP4_RT_BGP_INFO (pFeasibleRouteToBeFilled))
        != NULL)
    {
        BGP4_INFO_CLUS_LIST_ATTR_FLAG (pAdvtBgpInfo) =
            BGP4_INFO_CLUS_LIST_ATTR_FLAG (BGP4_RT_BGP_INFO
                                           (pFeasibleRouteToBeFilled));
    }
    BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_CLUS_LIST_MASK;
    BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                   BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                   "\tCluster list attribute is successfully filled for the route %s\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                    (pFeasibleRouteToBeFilled),
                                    BGP4_RT_AFI_INFO
                                    (pFeasibleRouteToBeFilled)));
    return RFL_SUCCESS;
}

#endif /*  _BGRFLFIL_C */

/* ******************* END ******************************** */
