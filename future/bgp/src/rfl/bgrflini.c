/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgrflini.c,v 1.8 2013/02/07 12:19:01 siva Exp $
 *
 * Description: This file contains functions for handling
 *              initialization and shutdown of Route Reflector.
 *
 *******************************************************************/
#ifndef _BGRFLINI_C
#define _BGRFLINI_C
#include "bgp4com.h"

/**************************************************************************
 Function Name   : RflInitHandler
 Description     : Rfl Initialization
 Input(s)        : None
 Output(s)       : None
 Global Variables Referred : RFL_GLOBAL_PARAMS(BGP4_DFLT_VRFID)
 Global variables Modified : RFL_GLOBAL_PARAMS(BGP4_DFLT_VRFID)
 Exceptions or Operating System Error Handling : None
 Use of Recursion          : None.
 Returns                   : None
**************************************************************************/
VOID
RflInitHandler (UINT4 u4CxtId)
{
    /* setting the cluster list, originator_id, and advt error statistics */
    RFL_CLUSTER_LIST_ERROR (u4CxtId) = 0;
    RFL_ORIGINATOR_ID_ERROR (u4CxtId) = 0;

    /* Initilaize the global variables to default values */
    RFL_CLIENT_SUPPORT (u4CxtId) = CLIENT_SUPPORT;

    /* All the initialization of Route Reflector module is completed */
    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_OS_RESOURCE_TRC | BGP4_CONTROL_PATH_TRC,
              BGP4_MOD_NAME,
              "\tRflInitHandler() : RFL INITIALIZATION SUCCESSFULL !!!\n");
    return;
}

/**************************************************************************
 Function Name   : RflShutDown
 Description     : Rfl Shutting down
 Input(s)        : None
 Output(s)       : None
 Global Variables Referred : RFL_GLOBAL_PARAMS(BGP4_DFLT_VRFID)
 Global variables Modified : RFL_GLOBAL_PARAMS(BGP4_DFLT_VRFID)
 Exceptions or Operating System Error Handling : None
 Use of Recursion          : None.
 Returns                   :  RFL_SUCCESS or RFL_FAILURE
**************************************************************************/
INT4
RflShutDown (UINT4 u4Context)
{
    /* Reset the error statistics, RrErrorStats */
    RFL_CLUSTER_LIST_ERROR (u4Context) = 0;
    RFL_ORIGINATOR_ID_ERROR (u4Context) = 0;

    /* Shut down successfully done  */
    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_OS_RESOURCE_TRC | BGP4_CONTROL_PATH_TRC,
              BGP4_MOD_NAME, "\tRflShutDown() : RFL SHUTDOWN SUCCESSFUL !!!\n");
    return RFL_SUCCESS;
}

#endif /* ifndef __BGRFLINI_C */

/* **************************** END *******************************************/
