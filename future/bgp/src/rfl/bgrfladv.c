/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgrfladv.c,v 1.7 2013/02/07 12:19:00 siva Exp $
 *
 * Description: This file contains functions for controlling
 *              advertisement to peers.
 *
 *******************************************************************/
#ifndef _BGRFLADV_C
#define _BGRFLADV_C

#include "bgp4com.h"

/*************************************************************************
 Function Name   : RflCheckForAdvertisement
 Description     : Checks whether a route profile can be advertised to a 
                   peer or not by this Route Reflector.
 Input(s)        : pPeerEntry - pointer to the peer entry to which 
                                pRouteProfileToBeAdvt is supposed to be sent.
                   pRouteProfileToBeAdvt - pointer to a route profile for 
                                which the advertisement status check to 
                                be done.
 Output(s)       : None
 Global Variables Referred : None 
 Global variables Modified : None
 Exceptions or Operating System Error Handling : None
 Use of Recursion          : None.
 Returns                   : TRUE or FALSE 
*************************************************************************/
INT4
RflCheckForAdvertisement (tBgp4PeerEntry * pPeerEntry,
                          tRouteProfile * pRouteProfileToBeAdvt)
{
    tBgp4PeerEntry     *pSrcPeer = NULL;

    pSrcPeer = BGP4_RT_PEER_ENTRY (pRouteProfileToBeAdvt);
    if (pSrcPeer != NULL)
    {
        /* pSrcPeer exists */
        if ((PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pSrcPeer),
                          BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)))
            == BGP4_TRUE)
        {
            return FALSE;
        }

        if ((BGP4_PEER_RFL_CLIENT (pSrcPeer) == CLIENT) &&
            (BGP4_PEER_RFL_CLIENT (pPeerEntry) == CLIENT))
        {
            /* Src -> Client, Dst -> Client */
            if (RFL_CLIENT_SUPPORT (BGP4_PEER_CXT_ID (pPeerEntry)) ==
                CLIENT_SUPPORT)
            {
                /* Client - Client Support Enabled. */
                return TRUE;
            }
            else
            {
                /* Client - Client Support Disabled. */
                return FALSE;
            }
        }
        else if ((BGP4_PEER_RFL_CLIENT (pSrcPeer) == CLIENT) &&
                 (BGP4_PEER_RFL_CLIENT (pPeerEntry) == NON_CLIENT))
        {
            /* Src -> Client, Dst -> Non Client */
            return TRUE;
        }
        else if ((BGP4_PEER_RFL_CLIENT (pSrcPeer) == NON_CLIENT) &&
                 (BGP4_PEER_RFL_CLIENT (pPeerEntry) == CLIENT))
        {
            /* Src -> Non client, Dst -> client */
            return TRUE;
        }
        else if ((BGP4_PEER_RFL_CLIENT (pSrcPeer) == NON_CLIENT) &&
                 (BGP4_PEER_RFL_CLIENT (pPeerEntry) == NON_CLIENT))
        {
            /* Src -> Non client, Dst -> Non client */
            return FALSE;
        }
    }
    return TRUE;
}

#endif /* _BGRFLADV_C */
/* **************************** END *******************************************/
