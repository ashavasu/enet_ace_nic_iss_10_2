/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgrflrts.c,v 1.8 2017/02/03 13:04:10 siva Exp $
 *
 * Description: This file contains functions for handling
 *              routes.
 *
 *******************************************************************/
#ifndef _BGRFLRTS_C
#define _BGRFLRTS_C

#include "bgp4com.h"

/**************************************************************************
 Function Name   : Bgp4GetBgpId 
 Description     : To get the BGP Router ID
Input(s)        :  None
Output(s)       : pu4RtrOrigId -  pointer to the router Id 
 Exceptions or Operating System Error Handling : None
 Use of Recursion          : None.
 Returns                   : None 
**************************************************************************/

VOID
Bgp4GetBgpId (UINT4 u4CxtId, UINT4 *pu4RtrOrigId)
{
    *pu4RtrOrigId = BGP4_LOCAL_BGP_ID (u4CxtId);
}

#endif /* ifndef _BGRFLRTS_C */
/* **************************** END *******************************************/
