/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgrflinp.c,v 1.9 2013/02/07 12:19:01 siva Exp $
 *
 * Description: This file contains functions for validating
 *              Route Reflector Path attributes.
 *
 *******************************************************************/
#ifndef _BGRFLINP_C
#define _BGRFLINP_C
#include "bgp4com.h"

/**************************************************************************
 Function Name   : RflOrigIdValidation
 Description     : On receiving route profile, this module compares the
                   ORIGINATOR_ID of this router. If they are different,
                   this module returns SUCCESS.Otherwise it returns
                   FAILURE.
 Input(s)        : pRouteForValidation - pointer to a route profile
                                for which the validation of ORIGINATOR_ID
                                path attribute has to be done.
 Outputs(s)      : None
 Global Variables Referred :
 Global variables Modified :
 Exceptions or Operating System Error Handling : None
 Use of Recursion          : None.
 Returns                   :  RFL_SUCCESS or RFL_FAILURE
*************************************************************************/
INT4
RflOrigIdValidation (tRouteProfile * pRouteForValidation)
{
    UINT4               u4OrigIdInRcvdRoute;
    UINT4               u4RtrOrigId;
    INT4                i4FnRflStatus = RFL_SUCCESS;

    /* Extract OriginatorId from pRouteForValidation */
    i4FnRflStatus =
        RflGetOriginatorId (pRouteForValidation, &u4OrigIdInRcvdRoute);
    if (i4FnRflStatus == RFL_SUCCESS)
    {
        /* If Rcvd and router's id are different then return success 
         * else return failure */
        Bgp4GetBgpId (BGP4_RT_CXT_ID (pRouteForValidation), &u4RtrOrigId);
        if (u4OrigIdInRcvdRoute != u4RtrOrigId)
        {
            return RFL_SUCCESS;
        }
        else
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tRflOrigIdValidation() : Received "
                      "ORIGINATOR_ID VALIDATION FAILED !!!\n");
            return RFL_FAILURE;
        }
    }
    else
    {
        /* Received no origination Id. */
        return RFL_SUCCESS;
    }
}

/**************************************************************************
 Function Name   : RflClusterListValidation
 Description     : On receiving the pointer to cluster list, this module
                   checks whether the CLUSTER_LIST path attribute 
                   contains the configured CLUSTER_ID of this RR.
 Input(s)        : pu1ClusterInfo - pointer to the Cluster list that 
                                    needs to be validated.
                   u2ClusIdCnt  - Number of Cluster-id present in the
                                  cluster list.
 Outputs(s)      : None
 Global Variables Referred : None
 Global variables Modified : None
 Exceptions or Operating System Error Handling : None
 Use of Recursion          : None.
 Returns                   :  RFL_SUCCESS or RFL_FAILURE
*************************************************************************/
INT4
RflClusterListValidation (UINT4 u4CxtId, UINT1 *pu1ClusterInfo,
                          UINT2 u2ClusIdCnt)
{
    UINT4               u4TempBgpClusterId;
    UINT4               u4BgpClusterId;
    UINT2               u2CidValLpCntr = 0;

    if (RFL_CLUSTER_ID (u4CxtId) == NO_CLUSTER_ID)
    {
        u4BgpClusterId = (BGP4_LOCAL_BGP_ID (u4CxtId));
    }
    else
    {
        u4BgpClusterId = (RFL_CLUSTER_ID (u4CxtId));
    }

    /* Get the cluster-id from the given cluster information and
     * Check whether there is atleast one cluster id in cluster list 
     * matches the local cluster-id or not. */
    while (u2CidValLpCntr < u2ClusIdCnt)
    {
        /* If local ClusterId is present in rcvd ClusterList 
         * then return failure */
        u4TempBgpClusterId = 0;
        PTR_FETCH4 (u4TempBgpClusterId,
                    (pu1ClusterInfo + (u2CidValLpCntr * CLUSTER_ID_LENGTH)));
        if (u4TempBgpClusterId == u4BgpClusterId)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tRflClusterListValidation():"
                      "CLUSTERID VALIDATION FAILURE\n");
            return RFL_FAILURE;
        }
        /* Increment the cluster count, since the cluster id 
         * traveresed is a valid id and is not equal to Local Cluster_Id */
        u2CidValLpCntr++;
    }
    /* Local ClusterId is not present in rcvd ClusterList
     * then return success
     */
    return RFL_SUCCESS;
}

/**************************************************************************
 Function Name   : RflGetClusterList
 Description     : Extract the cluster list received in a route profile
 Input(s)        : pRouteForValidation - pointer to a route profile
                                for which the CLUSTER_LIST
                                path attribute has to be extracted.
 Outputs(s)      : pu1ClusterInfo - pointer to the output cluster information
                   pu2ClusIdCnt   - Pointer to the number of cluster-id in
                                    the output cluster information.
 Global Variables Referred : None
 Global variables Modified : None
 Exceptions or Operating System Error Handling : None
 Use of Recursion          : None.
 Returns                   :  RFL_SUCCESS or RFL_FAILURE
*************************************************************************/
INT4
RflGetClusterList (const tRouteProfile * pRouteForValidation,
                   UINT1 **pu1ClusterInfo, UINT2 *pu2ClusIdCnt)
{
    tBgp4Info          *pRtInfo = NULL;

    /*    
     *   CID - Cluster ID each of 4 byte length
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |Attr. Flags(1) |Attr.Type Code (1)|Length(1) | CID1 CID2 CID3 ...
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     */
    /* Check whether the protocol of the route profile is BGP */
    if (BGP4_RT_PROTOCOL (pRouteForValidation) == BGP_ID)
    {
        pRtInfo = BGP4_RT_BGP_INFO (pRouteForValidation);
        /* This information presents in pRtInfo field */
        if (BGP4_INFO_CLUS_LIST_ATTR (pRtInfo) != NULL)
        {
            *pu1ClusterInfo = BGP4_INFO_CLUS_LIST_ATTR_VAL (pRtInfo);
            *pu2ClusIdCnt = BGP4_INFO_CLUS_LIST_COUNT (pRtInfo);
            return RFL_SUCCESS;
        }
        else
        {
            /* No cluster Id is present. Return */
            *pu1ClusterInfo = NULL;
            *pu2ClusIdCnt = 0;
            return RFL_SUCCESS;
        }
    }
    *pu1ClusterInfo = NULL;
    *pu2ClusIdCnt = 0;
    return RFL_FAILURE;
}

/**************************************************************************
 Function Name   : RflGetOriginatorId
 Description     : Extract the Originator Id received in a route profile
 Input(s)        : pRouteForValidation - pointer to a route profile
                                for which the ORIGINATOR_ID
                                path attribute has to be extracted.
 Outputs(s)      : pu4RcvdOrigId - pointer to the originator Id received
                                in the input route profile
 Global Variables Referred : None
 Global variables Modified : None
 Exceptions or Operating System Error Handling : None
 Use of Recursion          : None.
 Returns                   :  RFL_SUCCESS or RFL_FAILURE
*************************************************************************/
INT4
RflGetOriginatorId (const tRouteProfile * pRouteForValidation,
                    UINT4 *pu4RcvdOrigId)
{
    tBgp4Info          *pRtInfo = NULL;

    /* To begin initialize NO_ORIG_ID into pu4RcvdOrigId */
    *pu4RcvdOrigId = NO_ORIG_ID;

    /* whether the protocol of route profile is BGP */
    if ((BGP4_RT_PROTOCOL (pRouteForValidation)) == BGP_ID)
    {
        pRtInfo = BGP4_RT_BGP_INFO (pRouteForValidation);
        /* Now get this BGP information presents in pRtInfo field */
        if ((BGP4_INFO_ATTR_FLAG (pRtInfo) & (BGP4_ATTR_ORIG_ID_MASK)) ==
            BGP4_ATTR_ORIG_ID_MASK)
        {
            *pu4RcvdOrigId = BGP4_INFO_ORIG_ID (pRtInfo);
            return RFL_SUCCESS;
        }
        else
        {
            /*  Copy NO_ORIG_ID into pu4RcvdOrigId */
            *pu4RcvdOrigId = NO_ORIG_ID;
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                      "\tRflGetOriginatorId() : ORIGINATOR ID not present\n");
            return RFL_SUCCESS;
        }
    }
    return RFL_FAILURE;
}

#endif /* ifndef _BGRFLINP_C */

/* ******************* END ******************************** */
