/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgcaplow.c,v 1.27 2016/12/27 12:35:54 siva Exp $
 *
 * Description: Contains BGP Capability feature low level routines
 *
 *******************************************************************/
# include   "bgp4com.h"
# include  "include.h"
# include  "fsbgpcon.h"
# include  "fsbgpogi.h"
# include  "midconst.h"
# include  "fsmpbgcli.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4CapabilitySupportAvailable
 Input       :  The Indices

                The Object 
                retValFsbgp4CapabilitySupportAvailable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4CapabilitySupportAvailable (INT4
                                        *pi4RetValFsbgp4CapabilitySupportAvailable)
{
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4CapabilitySupportAvailable = CAPS_ADVT_SUP (u4Context);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4MaxCapsPerPeer
 Input       :  The Indices

                The Object 
                retValFsbgp4MaxCapsPerPeer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4MaxCapsPerPeer (INT4 *pi4RetValFsbgp4MaxCapsPerPeer)
{

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4MaxCapsPerPeer =
        FsBGPSizingParams[MAX_BGP_SUPP_CAP_INFOS_SIZING_ID].
        u4PreAllocatedUnits /
        FsBGPSizingParams[MAX_BGP_PEERS_SIZING_ID].u4PreAllocatedUnits;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4MaxInstancesPerCap
 Input       :  The Indices

                The Object 
                retValFsbgp4MaxInstancesPerCap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4MaxInstancesPerCap (INT4 *pi4RetValFsbgp4MaxInstancesPerCap)
{

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4MaxInstancesPerCap = FsBGPSizingParams
        [MAX_BGP_SUPP_CAP_INFOS_SIZING_ID].
        u4PreAllocatedUnits /
        FsBGPSizingParams[MAX_BGP_PEERS_SIZING_ID].u4PreAllocatedUnits;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4MaxCapDataSize
 Input       :  The Indices

                The Object 
                retValFsbgp4MaxCapDataSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4MaxCapDataSize (INT4 *pi4RetValFsbgp4MaxCapDataSize)
{

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4MaxCapDataSize = 0;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4CapabilitySupportAvailable
 Input       :  The Indices

                The Object 
                setValFsbgp4CapabilitySupportAvailable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4CapabilitySupportAvailable (INT4
                                        i4SetValFsbgp4CapabilitySupportAvailable)
{
    INT4                i4RetSts;
    UINT4               u4Context;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    if (CAPS_ADVT_SUP (u4Context) == i4SetValFsbgp4CapabilitySupportAvailable)
    {
        return SNMP_SUCCESS;
    }

    switch (i4SetValFsbgp4CapabilitySupportAvailable)
    {
        case CAPS_TRUE:
            CAPS_ADVT_SUP (u4Context) =
                (UINT1) i4SetValFsbgp4CapabilitySupportAvailable;
            break;
        case CAPS_FALSE:
            i4RetSts = CapsDeletePeerCaps (u4Context);
            if (i4RetSts != CAPS_SUCCESS)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to Delete Peer Capabilities.\n");
                return SNMP_FAILURE;
            }
            i4RetSts = CapsDeleteSpeakerCaps (u4Context);
            if (i4RetSts != CAPS_SUCCESS)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to Delete Speaker Capabilities.\n");
                return SNMP_FAILURE;
            }

            CAPS_ADVT_SUP (u4Context) =
                (UINT1) i4SetValFsbgp4CapabilitySupportAvailable;
            break;

        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Capability Advertisement Action.\n");
            return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4CapabilitySupportAvailable,
                          u4SeqNum, FALSE, BgpLock, BgpUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gpBgpCurrCxtNode->u4ContextId,
                      i4SetValFsbgp4CapabilitySupportAvailable));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsbgp4MaxCapsPerPeer
 Input       :  The Indices

                The Object 
                setValFsbgp4MaxCapsPerPeer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4MaxCapsPerPeer (INT4 i4SetValFsbgp4MaxCapsPerPeer)
{
    tBgpSystemSize      BgpSystemSize;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    GetBgpSizingParams (&BgpSystemSize);
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4MaxCapsPerPeer, u4SeqNum,
                          FALSE, BgpLock, BgpUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gpBgpCurrCxtNode->u4ContextId,
                      i4SetValFsbgp4MaxCapsPerPeer));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsbgp4MaxInstancesPerCap
 Input       :  The Indices

                The Object 
                setValFsbgp4MaxInstancesPerCap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4MaxInstancesPerCap (INT4 i4SetValFsbgp4MaxInstancesPerCap)
{
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4MaxInstancesPerCap, u4SeqNum,
                          FALSE, BgpLock, BgpUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gpBgpCurrCxtNode->u4ContextId,
                      i4SetValFsbgp4MaxInstancesPerCap));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsbgp4MaxCapDataSize
 Input       :  The Indices

                The Object 
                setValFsbgp4MaxCapDataSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4MaxCapDataSize (INT4 i4SetValFsbgp4MaxCapDataSize)
{
    tBgpSystemSize      BgpSystemSize;
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    GetBgpSizingParams (&BgpSystemSize);

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIBgp4MaxCapDataSize, u4SeqNum,
                          FALSE, BgpLock, BgpUnLock, 1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", gpBgpCurrCxtNode->u4ContextId,
                      i4SetValFsbgp4MaxCapDataSize));

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4CapabilitySupportAvailable
 Input       :  The Indices

                The Object 
                testValFsbgp4CapabilitySupportAvailable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4CapabilitySupportAvailable (UINT4 *pu4ErrorCode,
                                           INT4
                                           i4TestValFsbgp4CapabilitySupportAvailable)
{

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Local AS not configured.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_UP)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Local Admin Status Active.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4CapabilitySupportAvailable == CAPS_TRUE) ||
        (i4TestValFsbgp4CapabilitySupportAvailable == CAPS_FALSE))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Capability Support Enable Action.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4MaxCapsPerPeer
 Input       :  The Indices

                The Object 
                testValFsbgp4MaxCapsPerPeer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4MaxCapsPerPeer (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsbgp4MaxCapsPerPeer)
{
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4MaxCapsPerPeer > 0) &&
        (i4TestValFsbgp4MaxCapsPerPeer <= CAP_MAX_CAPS_PER_PEER))
    {
        return SNMP_SUCCESS;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - Invalid Max Capabilities per Peer Value.\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4MaxInstancesPerCap
 Input       :  The Indices

                The Object 
                testValFsbgp4MaxInstancesPerCap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4MaxInstancesPerCap (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsbgp4MaxInstancesPerCap)
{
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4MaxInstancesPerCap > 0)
        && (i4TestValFsbgp4MaxInstancesPerCap <= CAP_MAX_INSTANCES_PER_CAP))

    {
        return SNMP_SUCCESS;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - Invalid Maximum Instatances per Capability value.\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4MaxCapDataSize
 Input       :  The Indices

                The Object 
                testValFsbgp4MaxCapDataSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4MaxCapDataSize (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsbgp4MaxCapDataSize)
{
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4MaxCapDataSize < CAP_MIN_CAP_MAX_DATA_SIZE) ||
        (i4TestValFsbgp4MaxCapDataSize > CAP_MAX_CAP_MAX_DATA_SIZE))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Maximum Capability Data Size.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsbgp4MaxCapDataSize % CAP_MIN_CAP_MAX_DATA_SIZE) != 0)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Maximum Capability Data Size.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4CapabilitySupportAvailable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4CapabilitySupportAvailable (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4MaxCapsPerPeer
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4MaxCapsPerPeer (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4MaxInstancesPerCap
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4MaxInstancesPerCap (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4MaxCapDataSize
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4MaxCapDataSize (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4MpeCapSupportedCapsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4MpeCapSupportedCapsTable
 Input       :  The Indices
                Fsbgp4CapPeerType
                Fsbgp4CapPeerRemoteIpAddr
                Fsbgp4SupportedCapabilityCode
                Fsbgp4SupportedCapabilityLength
                Fsbgp4SupportedCapabilityValue
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4MpeCapSupportedCapsTable (INT4
                                                        i4Fsbgp4CapPeerType,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pFsbgp4CapPeerRemoteIpAddr,
                                                        INT4
                                                        i4Fsbgp4SupportedCapabilityCode,
                                                        INT4
                                                        i4Fsbgp4SupportedCapabilityLength,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pFsbgp4SupportedCapabilityValue)
{
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    UNUSED_PARAM (pFsbgp4SupportedCapabilityValue);
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4CapPeerType,
                                 pFsbgp4CapPeerRemoteIpAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i4Sts = Bgp4ValidateBgpPeerTableIndex (PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        return SNMP_FAILURE;
    }

    if ((IS_VALID_CAPABILITY_CODE (i4Fsbgp4SupportedCapabilityCode)) == 0)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Capability Code.\n");
        return SNMP_FAILURE;
    }
    if ((i4Fsbgp4SupportedCapabilityLength > (INT4) MAX_CAP_LENGTH) ||
        (i4Fsbgp4SupportedCapabilityLength < 0))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Capability Length.\n");
        return SNMP_FAILURE;
    }

    pPeerInfo = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Valid Peer does not exist.\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4MpeCapSupportedCapsTable
 Input       :  The Indices
                Fsbgp4CapPeerType
                Fsbgp4CapPeerRemoteIpAddr
                Fsbgp4SupportedCapabilityCode
                Fsbgp4SupportedCapabilityLength
                Fsbgp4SupportedCapabilityValue
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4MpeCapSupportedCapsTable (INT4 *pi4Fsbgp4CapPeerType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsbgp4CapPeerRemoteIpAddr,
                                                INT4
                                                *pi4Fsbgp4SupportedCapabilityCode,
                                                INT4
                                                *pi4Fsbgp4SupportedCapabilityLength,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsbgp4SupportedCapabilityValue)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tSupCapsInfo       *pSpkrSupCap = NULL;
    UINT4               u4PeerAddr;
    tTMO_SLL           *pCapsList = NULL;
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        return SNMP_FAILURE;
    }

    pPeer = (tBgp4PeerEntry *) TMO_SLL_First (BGP4_PEERENTRY_HEAD (u4Context));
    while (pPeer != NULL)
    {
        pCapsList = BGP4_PEER_SUP_CAPS_LIST (pPeer);
        pSpkrSupCap = (tSupCapsInfo *) (TMO_SLL_First (pCapsList));
        if (pSpkrSupCap == NULL)
        {
            pPeer =
                (tBgp4PeerEntry *)
                TMO_SLL_Next (BGP4_PEERENTRY_HEAD (u4Context),
                              &pPeer->TsnNextpeer);
            continue;
        }
        switch (BGP4_AFI_IN_ADDR_PREFIX_INFO
                (BGP4_PEER_REMOTE_ADDR_INFO (pPeer)))
        {
            case BGP4_INET_AFI_IPV4:
            {
                PTR_FETCH_4 (u4PeerAddr, BGP4_PEER_REMOTE_ADDR (pPeer));
                PTR_ASSIGN4 (pFsbgp4CapPeerRemoteIpAddr->pu1_OctetList,
                             (u4PeerAddr));
                pFsbgp4CapPeerRemoteIpAddr->i4_Length = BGP4_IPV4_PREFIX_LEN;
                *pi4Fsbgp4CapPeerType = BGP4_INET_AFI_IPV4;
            }
                break;
#ifdef BGP4_IPV6_WANTED
            case BGP4_INET_AFI_IPV6:
                MEMCPY (pFsbgp4CapPeerRemoteIpAddr->pu1_OctetList,
                        BGP4_PEER_REMOTE_ADDR (pPeer), BGP4_IPV6_PREFIX_LEN);
                pFsbgp4CapPeerRemoteIpAddr->i4_Length = BGP4_IPV6_PREFIX_LEN;
                *pi4Fsbgp4CapPeerType = BGP4_INET_AFI_IPV6;
                break;
#endif
            default:
                return SNMP_FAILURE;
        }
        *pi4Fsbgp4SupportedCapabilityCode =
            pSpkrSupCap->SupCapability.u1CapCode;
        *pi4Fsbgp4SupportedCapabilityLength =
            pSpkrSupCap->SupCapability.u1CapLength;
        if (pSpkrSupCap->SupCapability.u1CapLength > 0)
        {
            if ((pSpkrSupCap->SupCapability.u1CapLength) > 0 &&
                (pSpkrSupCap->SupCapability.u1CapLength <= MAX_CAP_LENGTH))
            {
                MEMCPY (pFsbgp4SupportedCapabilityValue->
                        pu1_OctetList,
                        pSpkrSupCap->SupCapability.au1CapValue,
                        pSpkrSupCap->SupCapability.u1CapLength);
                pFsbgp4SupportedCapabilityValue->i4_Length =
                    pSpkrSupCap->SupCapability.u1CapLength;
            }
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4MpeCapSupportedCapsTable
 Input       :  The Indices
                Fsbgp4CapPeerType
                nextFsbgp4CapPeerType
                Fsbgp4CapPeerRemoteIpAddr
                nextFsbgp4CapPeerRemoteIpAddr
                Fsbgp4SupportedCapabilityCode
                nextFsbgp4SupportedCapabilityCode
                Fsbgp4SupportedCapabilityLength
                nextFsbgp4SupportedCapabilityLength
                Fsbgp4SupportedCapabilityValue
                nextFsbgp4SupportedCapabilityValue
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4MpeCapSupportedCapsTable (INT4 i4Fsbgp4CapPeerType,
                                               INT4 *pi4NextFsbgp4CapPeerType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsbgp4CapPeerRemoteIpAddr,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pNextFsbgp4CapPeerRemoteIpAddr,
                                               INT4
                                               i4Fsbgp4SupportedCapabilityCode,
                                               INT4
                                               *pi4NextFsbgp4SupportedCapabilityCode,
                                               INT4
                                               i4Fsbgp4SupportedCapabilityLength,
                                               INT4
                                               *pi4NextFsbgp4SupportedCapabilityLength,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsbgp4SupportedCapabilityValue,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pNextFsbgp4SupportedCapabilityValue)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tSupCapsInfo       *pSpkrSupCap = NULL;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;
    UINT1               u1MatchFnd = CAPS_FALSE;
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4CapPeerType,
                                 pFsbgp4CapPeerRemoteIpAddr, &PeerAddress);

    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
    {
        if ((u1MatchFnd == CAPS_FALSE) &&
            (PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                          PeerAddress) != CAPS_TRUE))
        {
            continue;
        }

        if ((u1MatchFnd == CAPS_TRUE) &&
            (PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                          PeerAddress) == CAPS_TRUE))

        {
            continue;
        }

        TMO_SLL_Scan (BGP4_PEER_SUP_CAPS_LIST (pPeer), pSpkrSupCap,
                      tSupCapsInfo *)
        {
            if (u1MatchFnd == CAPS_TRUE)
            {
                switch (BGP4_AFI_IN_ADDR_PREFIX_INFO
                        (BGP4_PEER_REMOTE_ADDR_INFO (pPeer)))
                {
                    case BGP4_INET_AFI_IPV4:
                    {
                        UINT4               u4PeerAddr;
                        PTR_FETCH_4 (u4PeerAddr, BGP4_PEER_REMOTE_ADDR (pPeer));
                        PTR_ASSIGN4 (pNextFsbgp4CapPeerRemoteIpAddr->
                                     pu1_OctetList, u4PeerAddr);
                        pNextFsbgp4CapPeerRemoteIpAddr->i4_Length =
                            BGP4_IPV4_PREFIX_LEN;
                        *pi4NextFsbgp4CapPeerType = BGP4_INET_AFI_IPV4;
                    }
                        break;
#ifdef BGP4_IPV6_WANTED
                    case BGP4_INET_AFI_IPV6:
                        MEMCPY (pNextFsbgp4CapPeerRemoteIpAddr->
                                pu1_OctetList,
                                BGP4_PEER_REMOTE_ADDR (pPeer),
                                BGP4_IPV6_PREFIX_LEN);
                        pNextFsbgp4CapPeerRemoteIpAddr->i4_Length =
                            BGP4_IPV6_PREFIX_LEN;
                        *pi4NextFsbgp4CapPeerType = BGP4_INET_AFI_IPV6;
                        break;
#endif
                    default:
                        return SNMP_FAILURE;
                }
                *pi4NextFsbgp4SupportedCapabilityCode =
                    pSpkrSupCap->SupCapability.u1CapCode;
                *pi4NextFsbgp4SupportedCapabilityLength =
                    pSpkrSupCap->SupCapability.u1CapLength;
                if (pSpkrSupCap->SupCapability.u1CapLength > 0 &&
                    pSpkrSupCap->SupCapability.u1CapLength <= MAX_CAP_LENGTH)
                {
                    MEMCPY (pNextFsbgp4SupportedCapabilityValue->pu1_OctetList,
                            pSpkrSupCap->SupCapability.au1CapValue,
                            pSpkrSupCap->SupCapability.u1CapLength);
                }
                pNextFsbgp4SupportedCapabilityValue->i4_Length =
                    pSpkrSupCap->SupCapability.u1CapLength;
                return SNMP_SUCCESS;
            }
            if (pSpkrSupCap->SupCapability.u1CapCode <
                (UINT1) i4Fsbgp4SupportedCapabilityCode)
            {
                continue;
            }
            if (pSpkrSupCap->SupCapability.u1CapLength <
                (UINT1) i4Fsbgp4SupportedCapabilityLength)
            {
                continue;
            }
            if ((pSpkrSupCap->SupCapability.u1CapLength > 0) &&
                (pSpkrSupCap->SupCapability.u1CapLength <= MAX_CAP_LENGTH))
            {
                if (MEMCMP (pSpkrSupCap->SupCapability.au1CapValue,
                            pFsbgp4SupportedCapabilityValue->pu1_OctetList,
                            pSpkrSupCap->SupCapability.u1CapLength) != 0)
                {
                    continue;
                }
            }
            u1MatchFnd = BGP4_TRUE;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeCapSupportedCapsRowStatus
 Input       :  The Indices
                Fsbgp4CapPeerType
                Fsbgp4CapPeerRemoteIpAddr
                Fsbgp4SupportedCapabilityCode
                Fsbgp4SupportedCapabilityLength
                Fsbgp4SupportedCapabilityValue

                The Object 
                retValFsbgp4CapSupportedCapsRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeCapSupportedCapsRowStatus (INT4 i4Fsbgp4CapPeerType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsbgp4CapPeerRemoteIpAddr,
                                          INT4 i4Fsbgp4SupportedCapabilityCode,
                                          INT4
                                          i4Fsbgp4SupportedCapabilityLength,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsbgp4SupportedCapabilityValue,
                                          INT4
                                          *pi4RetValFsbgp4CapSupportedCapsRowStatus)
{
    tSupCapsInfo       *pSpkrSupCap = NULL;
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tAddrPrefix         PeerAddress;
    INT4                i4RetSts;
    UINT4               u4Context = 0;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    if (CAPS_ADVT_SUP (u4Context) != CAPS_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Capability Support not enabled.\n");
        return SNMP_FAILURE;
    }
    i4RetSts = Bgp4LowGetIpAddress (i4Fsbgp4CapPeerType,
                                    pFsbgp4CapPeerRemoteIpAddr, &PeerAddress);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pPeerInfo = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to find Peer Entry.\n");
        return SNMP_FAILURE;
    }

    i4RetSts = GetFindSpkrSupCap (pPeerInfo,
                                  (UINT1) i4Fsbgp4SupportedCapabilityCode,
                                  (UINT1) i4Fsbgp4SupportedCapabilityLength,
                                  pFsbgp4SupportedCapabilityValue,
                                  CAPS_SUP, &pSpkrSupCap);
    if ((i4RetSts == CAPS_FAILURE) || (pSpkrSupCap == NULL))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to get Speaker Supported Capability.\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4CapSupportedCapsRowStatus = pSpkrSupCap->u1RowStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeCapAnnouncedStatus
 Input       :  The Indices
                Fsbgp4CapPeerType
                Fsbgp4CapPeerRemoteIpAddr
                Fsbgp4SupportedCapabilityCode
                Fsbgp4SupportedCapabilityLength
                Fsbgp4SupportedCapabilityValue

                The Object 
                retValFsbgp4CapAnnouncedStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeCapAnnouncedStatus (INT4 i4Fsbgp4CapPeerType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsbgp4CapPeerRemoteIpAddr,
                                   INT4 i4Fsbgp4SupportedCapabilityCode,
                                   INT4 i4Fsbgp4SupportedCapabilityLength,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsbgp4SupportedCapabilityValue,
                                   INT4 *pi4RetValFsbgp4CapAnnouncedStatus)
{
    tSupCapsInfo       *pSpkrSupCap = NULL;
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;
    INT4                i4RetSts;
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    if (CAPS_ADVT_SUP (u4Context) != CAPS_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Capability Support not enabled.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4CapPeerType,
                                 pFsbgp4CapPeerRemoteIpAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pPeerInfo = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to find Peer Entry.\n");
        return SNMP_FAILURE;
    }

    i4RetSts = GetFindSpkrSupCap (pPeerInfo,
                                  (UINT1) i4Fsbgp4SupportedCapabilityCode,
                                  (UINT1) i4Fsbgp4SupportedCapabilityLength,
                                  pFsbgp4SupportedCapabilityValue,
                                  CAPS_SUP, &pSpkrSupCap);
    if ((i4RetSts == CAPS_FAILURE) || (pSpkrSupCap == NULL))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to get Speaker Supported Capability.\n");
        *pi4RetValFsbgp4CapAnnouncedStatus = CAPS_FALSE;
        return SNMP_FAILURE;
    }
    if (i4Fsbgp4SupportedCapabilityCode == CAP_CODE_ORF)
    {
        if ((pFsbgp4SupportedCapabilityValue->
             pu1_OctetList[BGP_CAP_ORF_MODE_OFFSET] & pSpkrSupCap->
             SupCapability.au1CapValue[BGP_CAP_ORF_MODE_OFFSET]) ==
            pFsbgp4SupportedCapabilityValue->
            pu1_OctetList[BGP_CAP_ORF_MODE_OFFSET])
        {
            *pi4RetValFsbgp4CapAnnouncedStatus = CAPS_TRUE;
        }
        else
        {
            *pi4RetValFsbgp4CapAnnouncedStatus = CAPS_FALSE;
        }
    }
    else
    {
        *pi4RetValFsbgp4CapAnnouncedStatus =
            (INT4) BGP4_PEER_SUP_CAPS_ANCD_STATUS (pSpkrSupCap);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeCapReceivedStatus
 Input       :  The Indices
                Fsbgp4CapPeerType
                Fsbgp4CapPeerRemoteIpAddr
                Fsbgp4SupportedCapabilityCode
                Fsbgp4SupportedCapabilityLength
                Fsbgp4SupportedCapabilityValue

                The Object 
                retValFsbgp4CapReceivedStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeCapReceivedStatus (INT4 i4Fsbgp4CapPeerType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsbgp4CapPeerRemoteIpAddr,
                                  INT4 i4Fsbgp4SupportedCapabilityCode,
                                  INT4 i4Fsbgp4SupportedCapabilityLength,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsbgp4SupportedCapabilityValue,
                                  INT4 *pi4RetValFsbgp4CapReceivedStatus)
{
    tSupCapsInfo       *pPeerCapRcvd = NULL;
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    if (CAPS_ADVT_SUP (u4Context) != CAPS_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Capability Support Not Enabled.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4CapPeerType,
                                 pFsbgp4CapPeerRemoteIpAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pPeerInfo = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to find Peer Entry.\n");
        return SNMP_FAILURE;
    }

    GetFindSpkrSupCap (pPeerInfo, (UINT1) i4Fsbgp4SupportedCapabilityCode,
                       (UINT1) i4Fsbgp4SupportedCapabilityLength,
                       pFsbgp4SupportedCapabilityValue,
                       CAPS_RCVD, &pPeerCapRcvd);
    if (pPeerCapRcvd == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tUnable to get Speaker Supported Capability.\n");
        *pi4RetValFsbgp4CapReceivedStatus = CAPS_FALSE;
        return SNMP_SUCCESS;
    }
    if (i4Fsbgp4SupportedCapabilityCode == CAP_CODE_ORF)
    {
        if ((pFsbgp4SupportedCapabilityValue->
             pu1_OctetList[BGP_CAP_ORF_MODE_OFFSET] & pPeerCapRcvd->
             SupCapability.au1CapValue[BGP_CAP_ORF_MODE_OFFSET]) ==
            pFsbgp4SupportedCapabilityValue->
            pu1_OctetList[BGP_CAP_ORF_MODE_OFFSET])
        {
            *pi4RetValFsbgp4CapReceivedStatus = CAPS_TRUE;
        }
        else
        {
            *pi4RetValFsbgp4CapReceivedStatus = CAPS_FALSE;
        }
    }
    else
    {
        *pi4RetValFsbgp4CapReceivedStatus = CAPS_TRUE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeCapNegotiatedStatus
 Input       :  The Indices
                Fsbgp4CapPeerType
                Fsbgp4CapPeerRemoteIpAddr
                Fsbgp4SupportedCapabilityCode
                Fsbgp4SupportedCapabilityLength
                Fsbgp4SupportedCapabilityValue

                The Object 
                retValFsbgp4CapNegotiatedStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeCapNegotiatedStatus (INT4 i4Fsbgp4CapPeerType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsbgp4CapPeerRemoteIpAddr,
                                    INT4 i4Fsbgp4SupportedCapabilityCode,
                                    INT4 i4Fsbgp4SupportedCapabilityLength,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsbgp4SupportedCapabilityValue,
                                    INT4 *pi4RetValFsbgp4CapNegotiatedStatus)
{
    tSupCapsInfo       *pPeerCapRcvd = NULL;
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;
    UINT4               u4NegOrfCapMask = 0;
    UINT4               u4AfiSafiMask = 0;
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    if (CAPS_ADVT_SUP (u4Context) != CAPS_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Capability Support Not Enabled.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4CapPeerType,
                                 pFsbgp4CapPeerRemoteIpAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pPeerInfo = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to find Peer Entry.\n");
        return SNMP_FAILURE;
    }

    GetFindSpkrSupCap (pPeerInfo, (UINT1) i4Fsbgp4SupportedCapabilityCode,
                       (UINT1) i4Fsbgp4SupportedCapabilityLength,
                       pFsbgp4SupportedCapabilityValue,
                       CAPS_RCVD, &pPeerCapRcvd);
    if (pPeerCapRcvd == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tUnable to get Speaker Supported Capability.\n");
        *pi4RetValFsbgp4CapNegotiatedStatus = CAPS_FALSE;
        return SNMP_SUCCESS;
    }
    if (i4Fsbgp4SupportedCapabilityCode == CAP_CODE_ORF)
    {
        /* For ORF capability, first 2 bytes will carry the AFI, 3rd is reserved and
         * 4th byte will carry Safi*/
        u4AfiSafiMask =
            ((UINT4) pFsbgp4SupportedCapabilityValue->
             pu1_OctetList[0] << BGP4_TWO_BYTE_BITS) |
            pFsbgp4SupportedCapabilityValue->pu1_OctetList[3];

        /* Fetch the ORF Capability negotiated mask for the given AFI,SAFI,ORF mode */
        Bgp4OrfGetOrfCapMask (u4AfiSafiMask,
                              pFsbgp4SupportedCapabilityValue->pu1_OctetList
                              [BGP_CAP_ORF_MODE_OFFSET], &u4NegOrfCapMask);

        if ((pPeerInfo->u4NegCapMask & u4NegOrfCapMask) == u4NegOrfCapMask)
        {
            *pi4RetValFsbgp4CapNegotiatedStatus = CAPS_TRUE;
        }
        else
        {
            *pi4RetValFsbgp4CapNegotiatedStatus = CAPS_FALSE;
        }
    }
    else
    {
        *pi4RetValFsbgp4CapNegotiatedStatus =
            BGP4_PEER_RCVD_CAPS_NEGOTIATED_STATUS (pPeerCapRcvd);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsbgp4mpeCapConfiguredStatus
 Input       :  The Indices
                Fsbgp4mpeCapPeerType
                Fsbgp4mpeCapPeerRemoteIpAddr
                Fsbgp4mpeSupportedCapabilityCode
                Fsbgp4mpeSupportedCapabilityLength
                Fsbgp4mpeSupportedCapabilityValue

                The Object
                retValFsbgp4mpeCapConfiguredStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4mpeCapConfiguredStatus (INT4 i4Fsbgp4CapPeerType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsbgp4CapPeerRemoteIpAddr,
                                    INT4 i4Fsbgp4SupportedCapabilityCode,
                                    INT4 i4Fsbgp4SupportedCapabilityLength,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsbgp4SupportedCapabilityValue,
                                    INT4 *pi4RetValFsbgp4CapConfiguredStatus)
{
    tSupCapsInfo       *pSpkrSupCap = NULL;
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tAddrPrefix         PeerAddress;
    INT4                i4Sts;
    INT4                i4RetSts;
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    if (CAPS_ADVT_SUP (u4Context) != CAPS_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Capability Support not enabled.\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4CapPeerType,
                                 pFsbgp4CapPeerRemoteIpAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pPeerInfo = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to find Peer Entry.\n");
        return SNMP_FAILURE;
    }

    i4RetSts = GetFindSpkrSupCap (pPeerInfo,
                                  (UINT1) i4Fsbgp4SupportedCapabilityCode,
                                  (UINT1) i4Fsbgp4SupportedCapabilityLength,
                                  pFsbgp4SupportedCapabilityValue,
                                  CAPS_SUP, &pSpkrSupCap);
    if ((i4RetSts == CAPS_FAILURE) || (pSpkrSupCap == NULL))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to get Speaker Supported Capability.\n");
        *pi4RetValFsbgp4CapConfiguredStatus = BGP4_FALSE;
        return SNMP_FAILURE;
    }

    *pi4RetValFsbgp4CapConfiguredStatus =
        (BGP4_PEER_SUP_CAPS_CONFIGURED_STATUS (pSpkrSupCap) == BGP4_TRUE) ?
        BGP4_TRUE : BGP4_FALSE;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4mpeCapSupportedCapsRowStatus
 Input       :  The Indices
                Fsbgp4CapPeerType
                Fsbgp4CapPeerRemoteIpAddr
                Fsbgp4SupportedCapabilityCode
                Fsbgp4SupportedCapabilityLength
                Fsbgp4SupportedCapabilityValue

                The Object 
                setValFsbgp4CapSupportedCapsRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4mpeCapSupportedCapsRowStatus (INT4 i4Fsbgp4CapPeerType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsbgp4CapPeerRemoteIpAddr,
                                          INT4 i4Fsbgp4SupportedCapabilityCode,
                                          INT4
                                          i4Fsbgp4SupportedCapabilityLength,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsbgp4SupportedCapabilityValue,
                                          INT4
                                          i4SetValFsbgp4CapSupportedCapsRowStatus)
{
    tAddrPrefix         PeerAddress;
    tBgp4PeerEntry     *pPeerEntry;
    INT4                i4Sts;

    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4CapPeerType,
                                 pFsbgp4CapPeerRemoteIpAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4SetCapSupportedRowStatus (u4Context, PeerAddress,
                                          i4Fsbgp4SupportedCapabilityCode,
                                          i4Fsbgp4SupportedCapabilityLength,
                                          pFsbgp4SupportedCapabilityValue,
                                          i4SetValFsbgp4CapSupportedCapsRowStatus);
    if (i4Sts == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if ( pPeerEntry != NULL && pPeerEntry->pPeerGrp == NULL )
    {
            if ( ((i4Fsbgp4SupportedCapabilityCode & CAP_NEG_ROUTE_REFRESH_MASK) == CAP_NEG_ROUTE_REFRESH_MASK )
                            && (i4SetValFsbgp4CapSupportedCapsRowStatus == CREATE_AND_GO ||
                                i4SetValFsbgp4CapSupportedCapsRowStatus == ACTIVE))
            {
                    BGP4_PEER_SHADOW_MP_CAP_MASK (pPeerEntry) |= CAP_NEG_ROUTE_REFRESH_MASK;
            }
            if (i4Fsbgp4SupportedCapabilityCode == CAP_CODE_ORF)
            {
                    BGP4_PEER_SHADOW_ORF_MODE (pPeerEntry) =
                          pFsbgp4SupportedCapabilityValue->pu1_OctetList[BGP_CAP_ORF_MODE_OFFSET];
            }

    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2Fsbgp4mpeCapSupportedCapsRowStatus
 Input       :  The Indices
                Fsbgp4CapPeerType
                Fsbgp4CapPeerRemoteIpAddr
                Fsbgp4SupportedCapabilityCode
                Fsbgp4SupportedCapabilityLength
                Fsbgp4SupportedCapabilityValue

                The Object 
                testValFsbgp4CapSupportedCapsRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4mpeCapSupportedCapsRowStatus (UINT4 *pu4ErrorCode,
                                             INT4 i4Fsbgp4CapPeerType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsbgp4CapPeerRemoteIpAddr,
                                             INT4
                                             i4Fsbgp4SupportedCapabilityCode,
                                             INT4
                                             i4Fsbgp4SupportedCapabilityLength,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsbgp4SupportedCapabilityValue,
                                             INT4
                                             i4TestValFsbgp4CapSupportedCapsRowStatus)
{
    tSupCapsInfo       *pSpkrSupCap = NULL;
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tAddrPrefix         PeerAddress;
#ifdef L3VPN
    tSNMP_OCTET_STRING_TYPE CapValue;
    UINT1               au1Value[CAP_MP_CAP_LENGTH];
    UINT4               u4AsafiFamily = 0;
#endif
    INT4                i4Sts;
    UINT4               u4Context;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4LowGetIpAddress (i4Fsbgp4CapPeerType,
                                 pFsbgp4CapPeerRemoteIpAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i4Sts = Bgp4ValidateBgpPeerTableIndex (PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Peer Address.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((IS_VENDOR_SPECIFIC_CAPABILITY (i4Fsbgp4SupportedCapabilityCode)) == 1)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Capability Code.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((IS_VALID_CAPABILITY_CODE (i4Fsbgp4SupportedCapabilityCode)) == 0)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Capability Code.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4Fsbgp4SupportedCapabilityLength < 0) ||
        (i4Fsbgp4SupportedCapabilityLength > MAX_CAP_LENGTH))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Capability Length.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (CAPS_ADVT_SUP (u4Context) != CAPS_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Capability Advertisement not Enabled.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    pPeerInfo = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeerInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to find Peer Entry.\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

#ifdef L3VPN
    if ((i4Fsbgp4SupportedCapabilityCode == CAP_CODE_MP_EXTN) &&
        (i4Fsbgp4SupportedCapabilityLength == CAP_MP_CAP_LENGTH))
    {
        PTR_FETCH4 (u4AsafiFamily,
                    pFsbgp4SupportedCapabilityValue->pu1_OctetList);
    }

    /* For CE peer, in this release, only <ipv4, unicast> 
     * config is allowed
     */
    if ((BGP4_VPN4_PEER_ROLE (pPeerInfo) == BGP4_VPN4_CE_PEER) &&
        (i4Fsbgp4SupportedCapabilityCode == CAP_CODE_MP_EXTN) &&
        (i4Fsbgp4SupportedCapabilityLength == CAP_MP_CAP_LENGTH))
    {
        if (!((u4AsafiFamily == CAP_MP_IPV4_UNICAST) ||
              (u4AsafiFamily == CAP_MP_LABELLED_IPV4)))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (i4Fsbgp4SupportedCapabilityCode == CAP_CODE_MP_EXTN)
    {
        switch (u4AsafiFamily)
        {
            case CAP_MP_LABELLED_IPV4:
                CapValue.pu1_OctetList = au1Value;
                PTR_ASSIGN4 (CapValue.pu1_OctetList,
                             (UINT4) CAP_MP_IPV4_UNICAST);
                CapValue.i4_Length = CAP_MP_CAP_LENGTH;
                /*Both Ipv4 and Labelled Ipv4 will not  be supported at 
                 * the same time*/
                GetFindSpkrSupCap (pPeerInfo, CAP_CODE_MP_EXTN,
                                   CAP_MP_CAP_LENGTH,
                                   &CapValue, CAPS_SUP, &pSpkrSupCap);
                if (pSpkrSupCap != NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                break;

            case CAP_MP_IPV4_UNICAST:
                CapValue.pu1_OctetList = au1Value;
                PTR_ASSIGN4 (CapValue.pu1_OctetList,
                             (UINT4) CAP_MP_LABELLED_IPV4);
                CapValue.i4_Length = CAP_MP_CAP_LENGTH;
                /*Both Ipv4 and Labelled Ipv4 will not  be supported at 
                 * the same time*/
                GetFindSpkrSupCap (pPeerInfo, CAP_CODE_MP_EXTN,
                                   CAP_MP_CAP_LENGTH,
                                   &CapValue, CAPS_SUP, &pSpkrSupCap);
                if (pSpkrSupCap != NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                break;

            case CAP_MP_VPN4_UNICAST:
                break;
#ifdef BGP4_IPV6_WANTED
            case CAP_MP_IPV6_UNICAST:
                break;
#endif
            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
        }
    }
#endif

    switch (i4TestValFsbgp4CapSupportedCapsRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            GetFindSpkrSupCap (pPeerInfo,
                               (UINT1) i4Fsbgp4SupportedCapabilityCode,
                               (UINT1) i4Fsbgp4SupportedCapabilityLength,
                               pFsbgp4SupportedCapabilityValue,
                               CAPS_SUP, &pSpkrSupCap);
            if (pSpkrSupCap != NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to find Capability entry.\n");
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
        case NOT_IN_SERVICE:
            GetFindSpkrSupCap (pPeerInfo,
                               (UINT1) i4Fsbgp4SupportedCapabilityCode,
                               (UINT1) i4Fsbgp4SupportedCapabilityLength,
                               pFsbgp4SupportedCapabilityValue,
                               CAPS_SUP, &pSpkrSupCap);
            if (pSpkrSupCap == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to find Capability entry.\n");
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            if ((pSpkrSupCap->u1RowStatus == ACTIVE) ||
                (pSpkrSupCap->u1RowStatus == NOT_IN_SERVICE) ||
                (pSpkrSupCap->u1RowStatus == NOT_READY))
            {
                return SNMP_SUCCESS;
            }
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Capability Row Status.\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;

        case DESTROY:
            GetFindSpkrSupCap (pPeerInfo,
                               (UINT1) i4Fsbgp4SupportedCapabilityCode,
                               (UINT1) i4Fsbgp4SupportedCapabilityLength,
                               pFsbgp4SupportedCapabilityValue,
                               CAPS_SUP, &pSpkrSupCap);
            if (pSpkrSupCap == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - Unable to find Capability Entry.\n");
                return SNMP_SUCCESS;
            }
            return SNMP_SUCCESS;
        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - Invalid Row Status Value.\n");
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4MpeCapSupportedCapsTable
 Input       :  The Indices
                Fsbgp4CapPeerType
                Fsbgp4CapPeerRemoteIpAddr
                Fsbgp4SupportedCapabilityCode
                Fsbgp4SupportedCapabilityLength
                Fsbgp4SupportedCapabilityValue
                Fsbgp4SupportedCapabilityInstance
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4MpeCapSupportedCapsTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
