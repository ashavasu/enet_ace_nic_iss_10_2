/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgpsnif4.c,v 1.59 2017/02/09 14:07:40 siva Exp $
 *
 * Description: Contains BGP ipv4 support routines
 *
 *******************************************************************/
#ifndef BGPSNIF4_C
#define BGPSNIF4_C

# include  "snmccons.h"
# include  "snmcdefn.h"
# include  "bgp4com.h"
# include  "utilcli.h"

PRIVATE UINT4       gau4AsafiMask[] = {
    CAP_MP_IPV4_UNICAST,
#ifdef L3VPN
    CAP_MP_LABELLED_IPV4,
#endif
#ifdef VPLSADS_WANTED
    CAP_MP_L2VPN_VPLS,
#endif

#ifdef BGP4_IPV6_WANTED
    CAP_MP_IPV6_UNICAST,
#endif
#ifdef L3VPN
    CAP_MP_VPN4_UNICAST
#endif
};

INT4
Bgp4ValidateBgpPeerTableIndex (tAddrPrefix PeerAddress)
{
    UINT1               u1ValidStatus = BGP4_FALSE;

    u1ValidStatus = Bgp4IsValidAddress (PeerAddress, BGP4_FALSE);
    if (u1ValidStatus != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

INT4
Bgp4Ipv4GetFirstIndexBgpPeerTable (UINT4 u4Context, tAddrPrefix * pPeerAddress)
{
    tBgp4PeerEntry     *pPeer = NULL;
    UINT2               u2Afi = 0;

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
    {
        u2Afi =
            BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
        if (u2Afi == BGP4_INET_AFI_IPV4)
        {
            if ((Bgp4IsValidAddress (BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                                     BGP4_FALSE)) == BGP4_TRUE)
            {
                Bgp4CopyAddrPrefixStruct (pPeerAddress,
                                          BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
                return BGP4_SUCCESS;
            }
        }
    }
    return BGP4_FAILURE;
}

INT4
Bgp4GetFirstIndexBgpPeerTable (UINT4 u4Context, tAddrPrefix * pPeerAddress)
{
    tBgp4PeerEntry     *pPeer = NULL;

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
    {
        if ((Bgp4IsValidAddress (BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                                 BGP4_FALSE)) == BGP4_TRUE)
        {
            Bgp4CopyAddrPrefixStruct (pPeerAddress,
                                      BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
            return BGP4_SUCCESS;
        }
    }
    return BGP4_FAILURE;
}

INT4
Bgp4GetNextIndexBgpPeerTable (UINT4 u4Context, tAddrPrefix PeerAddress,
                              tAddrPrefix * pNextPeerAddress)
{
    tBgp4PeerEntry     *pPeerEntry = NULL;
    UINT1               u1PeerFound = BGP4_FALSE;

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeerEntry, tBgp4PeerEntry *)
    {
        if (u1PeerFound == BGP4_TRUE)
        {
            if (PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry),
                             PeerAddress) != BGP4_TRUE)
            {
                break;
            }
        }
        if (PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry),
                         PeerAddress) == BGP4_TRUE)
        {
            u1PeerFound = BGP4_TRUE;
        }
    }

    if (pPeerEntry == NULL)
    {
        /* Next entry not present */
        return BGP4_FAILURE;
    }

    Bgp4CopyAddrPrefixStruct (pNextPeerAddress,
                              BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));
    return BGP4_SUCCESS;
}

/****************************************************************************
 Function    :  Bgp4GetFirstTCPAOKeyId
 Description :  This function returns the first TCP-AO key id associated with the 
         BGP peer. Currently only one MKT can be associated with the peer.
        But the mib routines are designed for multiple mkt. 
 Input       :  u4Context - context Id
         PeerAddress - BGP peer address
 Output      :  *pi4TCPAOKeyId - MKT id associated with peer
 Returns     :  BGP4_SUCCESS/BGP4_FAILURE   
****************************************************************************/

INT4
Bgp4GetFirstTCPAOKeyId (UINT4 u4Context, tAddrPrefix PeerAddress,
                        INT4 *pi4TCPAOKeyId)
{

    tBgp4PeerEntry     *pPeerEntry = NULL;

    pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);

    if (pPeerEntry == NULL)
    {
        return BGP4_FAILURE;

    }
    else
    {

        if (pPeerEntry->pTcpAOAuthMKT == NULL)
        {
            return BGP4_FAILURE;
        }
        else
        {
            *pi4TCPAOKeyId = (INT4) pPeerEntry->pTcpAOAuthMKT->u1SendKeyId;
            return BGP4_SUCCESS;
        }
    }

    return BGP4_FAILURE;
}

/****************************************************************************
 Function    :  Bgp4GetNextTCPAOKeyId
 Description :  This function returns the next TCP-AO key id associated with the 
         BGP peer. Currently only one MKT can be associated with the peer.
        But the mib routines are designed for multiple mkt. 
 Input       :  u4Context - context Id
         PeerAddress - BGP peer address
        i4CurrentTCPAOKeyId - incoming MKT id
 Output      :  *pi4NextTCPAOKeyId - MKT id associated with peer
 Returns     :  BGP4_SUCCESS/BGP4_FAILURE   
****************************************************************************/
INT4
Bgp4GetNextTCPAOKeyId (UINT4 u4Context, tAddrPrefix PeerAddress,
                       INT4 i4CurrentTCPAOKeyId, INT4 *pi4NextTCPAOKeyId)
{
    /*Currently only one MKT can be associated. If multiple MKT support is implemented later, then this routine has to be completed. */
    /*This routine is implemented for returning the first key id if the incoming keyid is less than 0 */
    tBgp4PeerEntry     *pPeerEntry = NULL;

    pPeerEntry = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);

    if (pPeerEntry == NULL)
    {
        return BGP4_FAILURE;

    }
    else
    {

        if (pPeerEntry->pTcpAOAuthMKT == NULL)
        {
            return BGP4_FAILURE;
        }
        else
        {
            if (i4CurrentTCPAOKeyId <
                (INT4) (pPeerEntry->pTcpAOAuthMKT->u1SendKeyId))
            {
                *pi4NextTCPAOKeyId =
                    (INT4) pPeerEntry->pTcpAOAuthMKT->u1SendKeyId;
                return BGP4_SUCCESS;
            }
        }
    }

    return BGP4_FAILURE;

}

INT4
Bgp4GetBgpPeerTableIntegerObject (UINT4 u4Context, tAddrPrefix PeerAddress,
                                  INT4 i4ObjectType, INT4 *pi4RetVal)
{
    tBgp4PeerEntry     *pPeer = NULL;

    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeer == NULL)
    {
        return BGP4_FAILURE;
    }
    switch (i4ObjectType)
    {
        case BGP4_PEER_STATE_OBJECT:
            *pi4RetVal = (INT4) BGP4_PEER_STATE (pPeer);
            break;
        case BGP4_PEER_ADMIN_STATUS_OBJECT:
            switch (BGP4_PEER_ADMIN_STATUS (pPeer))
            {
                case BGP4_PEER_START:
                    *pi4RetVal = BGP4_PEER_START;
                    break;
                case BGP4_PEER_STOP:
                    if ((BGP4_GET_PEER_PEND_FLAG (pPeer) &
                         BGP4_PEER_MULTIHOP_PEND_START) ==
                        BGP4_PEER_MULTIHOP_PEND_START)
                    {
                        *pi4RetVal = BGP4_PEER_START;
                    }
                    else
                    {
                        *pi4RetVal = BGP4_PEER_STOP;
                    }
                    break;
                    /* RFC 4271 Update */
                case BGP4_PEER_AUTO_START:
                    *pi4RetVal = BGP4_PEER_AUTO_START;
                    break;
                default:
                    *pi4RetVal = BGP4_INVALID;
                    break;
            }
            break;
        case BGP4_PEER_NEG_VERSION_OBJECT:
            *pi4RetVal = (INT4) BGP4_PEER_NEG_VER (pPeer);
            break;
        case BGP4_PEER_LOCAL_PORT_OBJECT:
            *pi4RetVal = (INT4) BGP4_PEER_LOCAL_PORT (pPeer);
            break;
        case BGP4_PEER_REMOTE_PORT_OBJECT:
            *pi4RetVal = (INT4) BGP4_PEER_REMOTE_PORT (pPeer);
            break;
        case BGP4_PEER_REMOTE_AS_OBJECT:
           if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) && 
#endif
               (BGP4_PEER_SHADOW_REMOTE_AS_NO (pPeer) != BGP4_INV_AS))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_REMOTE_AS_NO (pPeer);
           }
           else
           {
            *pi4RetVal = (INT4) BGP4_PEER_ASNO (pPeer);
           }
            break;
        case BGP4_PEER_CONNECT_RETRY_TIME_OBJECT:
	   if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) && 
#endif
               (BGP4_PEER_SHADOW_CONNECT_RETRY_INTERVAL (pPeer) != BGP4_DEF_CONNRETRYINTERVAL))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_CONNECT_RETRY_INTERVAL (pPeer);
           }
           else
           {
            *pi4RetVal = (INT4) BGP4_PEER_CONN_RETRY_TIME (pPeer);
           }
            break;
        case BGP4_PEER_HOLD_TIME_OBJECT:
            *pi4RetVal = (INT4) BGP4_PEER_NEG_HOLD_INT (pPeer);
            break;
        case BGP4_PEER_KEEPALIVE_OBJECT:
            *pi4RetVal = (INT4) BGP4_PEER_NEG_KEEP_ALIVE (pPeer);
            break;
        case BGP4_PEER_HOLD_TIME_CONF_OBJECT:
	   if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
               (BGP4_PEER_SHADOW_HOLD_INTERVAL (pPeer) != BGP4_DEF_HOLDINTERVAL))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_HOLD_INTERVAL (pPeer);
           }
           else
           {
            *pi4RetVal = (INT4) BGP4_PEER_HOLD_TIME (pPeer);
           }
            break;
        case BGP4_PEER_KEEPALIVE_CONF_OBJECT:
	   if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
               (BGP4_PEER_SHADOW_KEEP_ALIVE_INTERVAL (pPeer) != BGP4_DEF_KEEPALIVEINTERVAL))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_KEEP_ALIVE_INTERVAL (pPeer);
           }
           else
           {
            *pi4RetVal = (INT4) BGP4_PEER_KEEP_ALIVE_TIME (pPeer);
           }
            break;
        case BGP4_PEER_MINAS_ORIG_OBJECT:
	   if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
               (BGP4_PEER_SHADOW_MIN_AS_ORIG_INTERVAL (pPeer) != BGP4_DEF_MINASORIGINTERVAL))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_MIN_AS_ORIG_INTERVAL (pPeer);
           }
           else
           {
            *pi4RetVal = (INT4) BGP4_PEER_MIN_AS_ORIG_TIME (pPeer);
           }
            break;
        case BGP4_PEER_MINROUTE_ADVT_OBJECT:
            if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
               (((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeer), pPeer) == BGP4_EXTERNAL_PEER) &&
                 (BGP4_PEER_SHADOW_MIN_ROUTE_ADV_INTERVAL (pPeer) != BGP4_DEF_EBGP_MINROUTEADVINTERVAL)) ||
                ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeer), pPeer) == BGP4_INTERNAL_PEER) &&
                 (BGP4_PEER_SHADOW_MIN_ROUTE_ADV_INTERVAL (pPeer) != BGP4_DEF_IBGP_MINROUTEADVINTERVAL))))

           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_MIN_ROUTE_ADV_INTERVAL (pPeer);
           }
           else
           {
            *pi4RetVal = (INT4) BGP4_PEER_MIN_ADV_TIME (pPeer);
           }
            break;
        case BGP4_PEER_EOR_RECVD_OBJECT:
            *pi4RetVal = (INT4) BGP4_PEER_RECVD_EOR_MARKER (pPeer);
            break;
        case BGP4_PEER_EOR_SENT_OBJECT:
            *pi4RetVal = (INT4) BGP4_PEER_SENT_EOR_MARKER (pPeer);
            break;
        case BGP4_PEER_RESTART_MODE_OBJECT:
            *pi4RetVal = (INT4) BGP4_PEER_RESTART_MODE (pPeer);
            break;
        case BGP4_PEER_RST_TIME_OBJECT:
            *pi4RetVal = (INT4) BGP4_PEER_RESTART_INTERVAL (pPeer);
            break;
        case BGP4_PEER_ALLOW_AUTOMATIC_START_OBJECT:
	   if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
               (BGP4_PEER_SHADOW_AUTO_START (pPeer) != BGP4_PEER_AUTOMATICSTART_DISABLE))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_AUTO_START (pPeer);
           }
           else
           {
            *pi4RetVal = (INT4) BGP4_PEER_ALLOW_AUTOMATIC_START (pPeer);
           }
            break;
        case BGP4_PEER_ALLOW_AUTOMATIC_STOP_OBJECT:
	   if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
               (BGP4_PEER_SHADOW_AUTO_STOP (pPeer) != BGP4_PEER_AUTOMATICSTOP_DISABLE))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_AUTO_STOP (pPeer);
           }
           else
           {
            *pi4RetVal = (INT4) BGP4_PEER_ALLOW_AUTOMATIC_STOP (pPeer);
           }
            break;
        case BGP4_PEER_IDLE_HOLD_TIME_CONF_OBJECT:
	   if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
               (BGP4_PEER_SHADOW_IDLE_HOLDTIME_INTERVAL (pPeer) != BGP4_DEF_IDLEHOLDINTERVAL))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_IDLE_HOLDTIME_INTERVAL (pPeer);
           }
           else
           {
            *pi4RetVal = (INT4) BGP4_PEER_IDLE_HOLD_TIME (pPeer);
           }
            break;
        case BGP4_DAMP_PEER_OSCILLATIONS_OBJECT:
	    if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
               (BGP4_PEER_SHADOW_DAMP_PEER_OSC_STATUS (pPeer) != BGP4_DAMP_PEER_OSCILLATIONS_DISABLE)) 
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_DAMP_PEER_OSC_STATUS (pPeer);
           }
           else
           {
            *pi4RetVal = (INT4) BGP4_DAMP_PEER_OSCILLATIONS (pPeer);
           }
            break;
        case BGP4_PEER_DELAY_OPEN_OBJECT:
            if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
               (BGP4_PEER_SHADOW_DELAY_OPEN_STATUS (pPeer) != BGP4_PEER_DELAY_OPEN_DISABLE))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_DELAY_OPEN_STATUS (pPeer);
           }
           else
           {
            *pi4RetVal = (INT4) BGP4_PEER_DELAY_OPEN (pPeer);
           }
            break;
        case BGP4_PEER_DELAY_OPEN_TIME_CONF_OBJECT:
	   if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
               (BGP4_PEER_SHADOW_DELAY_OPEN_TIME_INTERVAL (pPeer) != BGP4_DEF_DELAYOPENINTERVAL))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_DELAY_OPEN_TIME_INTERVAL (pPeer);
           }
           else
           {
            *pi4RetVal = (INT4) BGP4_PEER_DELAY_OPEN_TIME (pPeer);
           }
            break;
        case BGP4_PEER_PREFIX_UPPER_LIMIT_OBJECT:
	    if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif

               (BGP4_PEER_SHADOW_PEER_PREFIX_UPPER_LIMIT (pPeer) != BGP4_DEF_MAX_PEER_PREFIX_LIMIT))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_PEER_PREFIX_UPPER_LIMIT (pPeer);
           }
           else
           {
            *pi4RetVal = (INT4) BGP4_PEER_PREFIX_UPPER_LIMIT (pPeer);
           }
            break;
        case BGP4_PEER_TCP_CONNECT_RETRY_COUNT_OBJECT:
	   if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) && 
#endif
               (BGP4_PEER_SHADOW_CONNECT_COUNT (pPeer) != BGP4_DEF_CONNECT_RETRY_COUNT))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_CONNECT_COUNT (pPeer);
           }
           else
           {
               *pi4RetVal = (INT4) BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT (pPeer);
           }
            break;
        case BGP4_PEER_TCP_CURRENT_CONNECT_RETRY_COUNT_OBJECT:
            *pi4RetVal = (INT4) BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT (pPeer);
            break;
        case BGP4_PEER_DAMPED_STATUS_OBJECT:
            *pi4RetVal = (INT4) BGP4_PEER_DAMPED_STATUS (pPeer);
            break;
        case BGP4_PEER_LOCAL_AS_OBJECT:
	   if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
               (BGP4_PEER_SHADOW_LOCAL_AS_NO (pPeer) != BGP4_INV_AS))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_LOCAL_AS_NO (pPeer);
           }
           else
           {
            *pi4RetVal = (INT4) BGP4_PEER_LOCAL_AS (pPeer);
           }
            break;

        case BGP4_PEER_TCPAO_ICMP_ACCEPT_OBJECT:
            *pi4RetVal = BGP4_PEER_TCPAO_ICMP_ACCEPT_STATUS (pPeer);
            break;
        case BGP4_PEER_TCPAO_PKT_DISCARD_OBJECT:
            *pi4RetVal = BGP4_PEER_TCPAO_NOMKT_PKTDISC_STATUS (pPeer);
            break;
        case BGP4_PEER_TCPAO_MKT_ASSOCIATE_OBJECT:
            *pi4RetVal = BGP4_PEER_GET_TCPAO_ASSOCIATED_MKT_STATUS (pPeer);
            break;
        case BGP4_PEER_TCPAO_PEERKEYID_GET:
            *pi4RetVal = BGP4_PEER_GET_TCPAO_ASSOCIATED_MKT_ID (pPeer);
            break;
       case BGP4_PEER_BFD_STATUS_CONF_OBJECT:
	   if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
               (BGP4_PEER_SHADOW_BFD_STATUS (pPeer) != BGP4_BFD_DISABLE))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_BFD_STATUS (pPeer);
           }
           else
           {
               *pi4RetVal = (INT4) pPeer->u1BfdStatus;
           }
           break;
        default:
            return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

INT4
Bgp4GetBgpPeerTableUnsignIntObject (UINT4 u4Context, tAddrPrefix PeerAddress,
                                    INT4 i4ObjectType, UINT4 *pu4RetVal)
{
    tBgp4PeerEntry     *pPeer = NULL;

    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeer == NULL)
    {
        return BGP4_FAILURE;
    }
    switch (i4ObjectType)
    {
        case BGP4_PEER_INUPDATES_OBJECT:
            *pu4RetVal = BGP4_PEER_IN_UPDATES (pPeer);
            break;
        case BGP4_PEER_OUTUPDATES_OBJECT:
            *pu4RetVal = BGP4_PEER_OUT_UPDATES (pPeer);
            break;
        case BGP4_PEER_IN_TOTAL_MSG_OBJECT:
            *pu4RetVal = BGP4_PEER_IN_MSGS (pPeer);
            break;
        case BGP4_PEER_OUT_TOTAL_MSG_OBJECT:
            *pu4RetVal = BGP4_PEER_OUT_MSGS (pPeer);
            break;
        case BGP4_PEER_FSM_TRANS_OBJECT:
            *pu4RetVal = BGP4_PEER_FSM_TRANS (pPeer);
            break;
        case BGP4_PEER_ESTAB_TIME_OBJECT:
            if (BGP4_PEER_ESTAB_TIME (pPeer) == BGP4_INIT_TIME)
            {
                *pu4RetVal = 0;
            }
            else
            {
                *pu4RetVal = ((UINT4) Bgp4ElapTime () -
                              BGP4_PEER_ESTAB_TIME (pPeer));
            }
            break;
        case BGP4_PEER_IN_UPDT_ELAPSE_OBJECT:
            if (BGP4_PEER_IN_UPDATE_ELAP_TIME (pPeer) == BGP4_INIT_TIME)
            {
                *pu4RetVal = 0;
            }
            else
            {
                *pu4RetVal = ((UINT4) Bgp4ElapTime () -
                              BGP4_PEER_IN_UPDATE_ELAP_TIME (pPeer));
            }
            break;
        default:
            return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

INT4
Bgp4SetBgpPeerTableIntegerObject (UINT4 u4Context, tAddrPrefix PeerAddress,
                                  INT4 i4ObjectType, INT4 i4SetVal)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tBgp4QMsg          *pQMsg = NULL;
    tSupCapsInfo       *pSpkrSupCap = NULL;
    INT4                i4Sts;
    UINT1               u1SupportStatus;

    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeer == NULL)
    {
        return BGP4_FAILURE;
    }

    switch (i4ObjectType)
    {
        case BGP4_PEER_ADMIN_STATUS_OBJECT:
            /* Peer Admin status can't be changed before configuring the
             * peer's AS number */
            if (BGP4_PEER_ASNO (pPeer) == BGP4_INV_AS)
            {
                return BGP4_FAILURE;
            }
            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                return BGP4_FAILURE;
            }
            if ((i4SetVal == BGP4_PEER_START) ||
                (i4SetVal == BGP4_PEER_AUTO_START))
            {
                BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_SNMP_START_EVENT;
#ifdef ISS_WANTED
                if (MsrGetRestorationStatus () != ISS_TRUE)
                {
#endif
                    /* If 4-byte ASN Support status is enabled, 
                     * then add 4-byte ASN capability to
                     * BGP4_PEER_SUP_CAPS_LIST */
                    if (BGP4_FOUR_BYTE_ASN_SUPPORT (u4Context) ==
                        BGP4_ENABLE_4BYTE_ASN_SUPPORT)
                    {
                        i4Sts = Bgp4Snmph4ByteAsnCapabilityEnable
                            (u4Context, PeerAddress);
                        if (i4Sts == SNMP_FAILURE)
                        {
                            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                                      BGP4_MOD_NAME,
                                      "\tERROR - BGP 4-byte ASN capability "
                                      "  can't be enabled.\n");
                            return SNMP_FAILURE;
                        }
                    }
#ifdef ISS_WANTED
                }
#endif
            }
            else
            {
                BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_SNMP_STOP_EVENT;
            }
            BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
            BGP4_INPUTQ_CXT (pQMsg) = u4Context;

            Bgp4CopyAddrPrefixStruct (&(BGP4_INPUTQ_PEER_ADDR_INFO (pQMsg)),
                                      BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
            /* Send the peer entry to the bgp-task */
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                return BGP4_FAILURE;
            }
            break;
        case BGP4_PEER_CONNECT_RETRY_TIME_OBJECT:
            BGP4_PEER_CONN_RETRY_TIME (pPeer) = (UINT4) i4SetVal;
            BGP4_PEER_SHADOW_CONNECT_RETRY_INTERVAL(pPeer) = BGP4_PEER_CONN_RETRY_TIME(pPeer);
	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u4ConnectRetryInterval != BGP4_DEF_CONNRETRYINTERVAL) && 
                  (pPeer->pPeerGrp->u4ConnectRetryInterval == BGP4_PEER_SHADOW_CONNECT_RETRY_INTERVAL(pPeer)))
               {
                  BGP4_PEER_SHADOW_CONNECT_RETRY_INTERVAL(pPeer) = BGP4_DEF_CONNRETRYINTERVAL;
               }
            }
            break;
        case BGP4_PEER_HOLD_TIME_CONF_OBJECT:
            BGP4_PEER_HOLD_TIME (pPeer) = (UINT4) i4SetVal;
            BGP4_PEER_SHADOW_HOLD_INTERVAL(pPeer) = BGP4_PEER_HOLD_TIME(pPeer);
	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                  (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u4HoldInterval != BGP4_DEF_HOLDINTERVAL) && 
                  (pPeer->pPeerGrp->u4HoldInterval == BGP4_PEER_SHADOW_HOLD_INTERVAL(pPeer)))
               {
                  BGP4_PEER_SHADOW_HOLD_INTERVAL(pPeer) = BGP4_DEF_HOLDINTERVAL;
               }
            }
            break;
        case BGP4_PEER_KEEPALIVE_CONF_OBJECT:
            BGP4_PEER_KEEP_ALIVE_TIME (pPeer) = (UINT4) i4SetVal;
            BGP4_PEER_SHADOW_KEEP_ALIVE_INTERVAL(pPeer) = BGP4_PEER_KEEP_ALIVE_TIME(pPeer);
	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
              (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u4KeepAliveInterval != BGP4_DEF_KEEPALIVEINTERVAL) && 
                  (pPeer->pPeerGrp->u4KeepAliveInterval == BGP4_PEER_SHADOW_KEEP_ALIVE_INTERVAL(pPeer)))
               {
                  BGP4_PEER_SHADOW_KEEP_ALIVE_INTERVAL(pPeer) = BGP4_DEF_KEEPALIVEINTERVAL;
               }
            }
            break;
        case BGP4_PEER_MINAS_ORIG_OBJECT:
            BGP4_PEER_MIN_AS_ORIG_TIME (pPeer) = (UINT4) i4SetVal;
            BGP4_PEER_SHADOW_MIN_AS_ORIG_INTERVAL(pPeer) = BGP4_PEER_MIN_AS_ORIG_TIME(pPeer);
	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u4MinASOrigInterval != BGP4_DEF_MINASORIGINTERVAL) && 
                  (pPeer->pPeerGrp->u4MinASOrigInterval == BGP4_PEER_SHADOW_MIN_AS_ORIG_INTERVAL(pPeer)))
               {
                  BGP4_PEER_SHADOW_MIN_AS_ORIG_INTERVAL(pPeer) = BGP4_DEF_MINASORIGINTERVAL;
               }
            }
            break;
        case BGP4_PEER_MINROUTE_ADVT_OBJECT:
            BGP4_PEER_MIN_ADV_TIME (pPeer) = (UINT4) i4SetVal;
            BGP4_PEER_SHADOW_MIN_ROUTE_ADV_INTERVAL(pPeer) = BGP4_PEER_MIN_ADV_TIME(pPeer);
	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                 (pPeer->pPeerGrp != NULL))
            {
               if(pPeer->pPeerGrp->u4MinRouteAdvInterval == BGP4_PEER_SHADOW_MIN_ROUTE_ADV_INTERVAL(pPeer))
               {
		  if ((pPeer->pPeerGrp->u1PeerGrpType == BGP4_EXTERNAL_PEER) &&
		      (pPeer->pPeerGrp->u4MinRouteAdvInterval != BGP4_DEF_EBGP_MINROUTEADVINTERVAL))
		  {
                     BGP4_PEER_SHADOW_MIN_ROUTE_ADV_INTERVAL(pPeer) = BGP4_DEF_EBGP_MINROUTEADVINTERVAL;
		  }
		  else if((pPeer->pPeerGrp->u1PeerGrpType == BGP4_INTERNAL_PEER) &&
			  (pPeer->pPeerGrp->u4MinRouteAdvInterval != BGP4_DEF_IBGP_MINROUTEADVINTERVAL))
		  {
		     BGP4_PEER_SHADOW_MIN_ROUTE_ADV_INTERVAL(pPeer) = BGP4_DEF_IBGP_MINROUTEADVINTERVAL;
		  }
               }
            }
            break;

        case BGP4_PEER_ALLOW_AUTOMATIC_START_OBJECT:
            BGP4_PEER_ALLOW_AUTOMATIC_START (pPeer) = (UINT1) i4SetVal;
            BGP4_PEER_SHADOW_AUTO_START(pPeer) = BGP4_PEER_ALLOW_AUTOMATIC_START(pPeer);
	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) && 
#endif
                 (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u1AutomaticStart != BGP4_PEER_AUTOMATICSTART_DISABLE) && 
                  (pPeer->pPeerGrp->u1AutomaticStart == BGP4_PEER_SHADOW_AUTO_START(pPeer)))
               {
                  BGP4_PEER_SHADOW_AUTO_START(pPeer) = BGP4_PEER_AUTOMATICSTART_DISABLE;
               }
            }
            break;
        case BGP4_PEER_ALLOW_AUTOMATIC_STOP_OBJECT:
            BGP4_PEER_ALLOW_AUTOMATIC_STOP (pPeer) = (UINT1) i4SetVal;
            BGP4_PEER_SHADOW_AUTO_STOP(pPeer) = BGP4_PEER_ALLOW_AUTOMATIC_STOP(pPeer);
	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                 (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u1AutomaticStop != BGP4_PEER_AUTOMATICSTOP_DISABLE) && 
                  (pPeer->pPeerGrp->u1AutomaticStop == BGP4_PEER_SHADOW_AUTO_STOP(pPeer)))
               {
                  BGP4_PEER_SHADOW_AUTO_STOP(pPeer) = BGP4_PEER_AUTOMATICSTOP_DISABLE;
               }
            }
            break;
        case BGP4_PEER_IDLE_HOLD_TIME_CONF_OBJECT:
            BGP4_PEER_IDLE_HOLD_TIME (pPeer) = (UINT4) i4SetVal;
            BGP4_PEER_SHADOW_IDLE_HOLDTIME_INTERVAL(pPeer) = BGP4_PEER_IDLE_HOLD_TIME(pPeer);
	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) && 
#endif
                  (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u4IdleHoldTimeInterval != BGP4_DEF_IDLEHOLDINTERVAL) && 
                  (pPeer->pPeerGrp->u4IdleHoldTimeInterval == BGP4_PEER_SHADOW_IDLE_HOLDTIME_INTERVAL(pPeer)))
               {
                  BGP4_PEER_SHADOW_IDLE_HOLDTIME_INTERVAL(pPeer) = BGP4_DEF_IDLEHOLDINTERVAL;
               }
            }
            break;
        case BGP4_DAMP_PEER_OSCILLATIONS_OBJECT:
            BGP4_DAMP_PEER_OSCILLATIONS (pPeer) = (UINT1) i4SetVal;
            BGP4_PEER_SHADOW_DAMP_PEER_OSC_STATUS(pPeer) = BGP4_DAMP_PEER_OSCILLATIONS(pPeer);
	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                  (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u1DampPeerOscillationStatus != BGP4_DAMP_PEER_OSCILLATIONS_DISABLE) && 
                  (pPeer->pPeerGrp->u1DampPeerOscillationStatus == BGP4_PEER_SHADOW_DAMP_PEER_OSC_STATUS(pPeer)))
               {
                  BGP4_PEER_SHADOW_DAMP_PEER_OSC_STATUS(pPeer) = BGP4_DAMP_PEER_OSCILLATIONS_DISABLE;
               }
            }
            break;
        case BGP4_PEER_DELAY_OPEN_OBJECT:
            BGP4_PEER_DELAY_OPEN (pPeer) = (UINT1) i4SetVal;
            BGP4_PEER_SHADOW_DELAY_OPEN_STATUS(pPeer) = BGP4_PEER_DELAY_OPEN(pPeer);
	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                  (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u1DelayOpenStatus != BGP4_PEER_DELAY_OPEN_DISABLE) && 
                  (pPeer->pPeerGrp->u1DelayOpenStatus == BGP4_PEER_SHADOW_DELAY_OPEN_STATUS(pPeer)))
               {
                  BGP4_PEER_SHADOW_DELAY_OPEN_STATUS(pPeer) = BGP4_PEER_DELAY_OPEN_DISABLE;
               }
            }
            break;
        case BGP4_PEER_DELAY_OPEN_TIME_CONF_OBJECT:
            BGP4_PEER_DELAY_OPEN_TIME (pPeer) = (UINT4) i4SetVal;
            BGP4_PEER_SHADOW_DELAY_OPEN_TIME_INTERVAL(pPeer) = BGP4_PEER_DELAY_OPEN_TIME(pPeer);
	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                  (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u4DelayOpenTimeInterval != BGP4_DEF_DELAYOPENINTERVAL) && 
                  (pPeer->pPeerGrp->u4DelayOpenTimeInterval == BGP4_PEER_SHADOW_DELAY_OPEN_TIME_INTERVAL(pPeer)))
               {
                  BGP4_PEER_SHADOW_DELAY_OPEN_TIME_INTERVAL(pPeer) = BGP4_DEF_DELAYOPENINTERVAL;
               }
            }
            break;
        case BGP4_PEER_PREFIX_UPPER_LIMIT_OBJECT:
            BGP4_PEER_PREFIX_UPPER_LIMIT (pPeer) = (UINT4) i4SetVal;
            BGP4_PEER_SHADOW_PEER_PREFIX_UPPER_LIMIT(pPeer) = BGP4_PEER_PREFIX_UPPER_LIMIT(pPeer);
	    if ((
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                  (pPeer->pPeerGrp != NULL))
                || (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u4PeerPrefixUpperLimit != BGP4_DEF_MAX_PEER_PREFIX_LIMIT) && 
                  (pPeer->pPeerGrp->u4PeerPrefixUpperLimit == BGP4_PEER_SHADOW_PEER_PREFIX_UPPER_LIMIT(pPeer)))
               {
                  BGP4_PEER_SHADOW_PEER_PREFIX_UPPER_LIMIT(pPeer) = BGP4_DEF_MAX_PEER_PREFIX_LIMIT;
               }
            }
            break;
        case BGP4_PEER_TCP_CONNECT_RETRY_COUNT_OBJECT:
            BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT (pPeer) =
                (UINT1) i4SetVal;
            BGP4_PEER_SHADOW_CONNECT_COUNT(pPeer) = BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT(pPeer);
	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                  (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u1ConnectRetryCount != BGP4_DEF_CONNECT_RETRY_COUNT) && 
                  (pPeer->pPeerGrp->u1ConnectRetryCount == BGP4_PEER_SHADOW_CONNECT_COUNT(pPeer)))
               {
                  BGP4_PEER_SHADOW_CONNECT_COUNT(pPeer) = BGP4_DEF_CONNECT_RETRY_COUNT;
               }
            }
            break;
        case BGP4_PEER_LOCAL_AS_OBJECT:
            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                return BGP4_FAILURE;
            }
            BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_LOCAL_AS_CFG_EVENT;
            if (i4SetVal != 0)
            {
                pQMsg->QMsg.Bgp4PeerMsg.u4LocalAs = (UINT4) i4SetVal;
                BGP4_PEER_LOCAL_AS_CFG (pPeer) = BGP4_TRUE;
            }
            else
            {
                pQMsg->QMsg.Bgp4PeerMsg.u4LocalAs =
                    BGP4_LOCAL_AS_NO (u4Context);
                BGP4_PEER_LOCAL_AS_CFG (pPeer) = BGP4_FALSE;
            }
            BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
            BGP4_INPUTQ_CXT (pQMsg) = u4Context;

            Bgp4CopyAddrPrefixStruct (&(pQMsg->QMsg.Bgp4PeerMsg.PeerAddress),
                                      BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
            /* Send the peer entry to the bgp-task */
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                return BGP4_FAILURE;
            }
            BGP4_PEER_SHADOW_LOCAL_AS_NO(pPeer) = (UINT4) i4SetVal;
            BGP4_PEER_SHADOW_LOCAL_AS_CONFIG(pPeer) = BGP4_PEER_LOCAL_AS_CFG(pPeer);
            u1SupportStatus =
                Bgp4SnmphGet4ByteAsnCapSupportStatus (PeerAddress);
            if (u1SupportStatus == BGP4_TRUE)
            {
                TMO_SLL_Scan (BGP4_PEER_SUP_CAPS_LIST (pPeer), pSpkrSupCap,
                              tSupCapsInfo *)
                {
                    /* Search only 4-byte ASN CAPABILITY Support */
                    if (pSpkrSupCap->SupCapability.u1CapCode ==
                        CAP_CODE_4_OCTET_ASNO)
                    {
                        if (i4SetVal == 0)
                        {
                            PTR_ASSIGN4 (pSpkrSupCap->SupCapability.au1CapValue,
                                         BGP4_LOCAL_AS_NO (u4Context));
                        }
                        else
                        {
                            PTR_ASSIGN4 (pSpkrSupCap->SupCapability.au1CapValue,
                                         (UINT4) i4SetVal);
                        }
                    }
                }
            }
	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                  (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u4LocalASNo != BGP4_INV_AS) && 
                  (pPeer->pPeerGrp->u4LocalASNo == BGP4_PEER_SHADOW_LOCAL_AS_NO(pPeer)))
               {
                  BGP4_PEER_SHADOW_LOCAL_AS_NO(pPeer) = BGP4_INV_AS;
               }
            }
            if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                  (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u1LocalASConfig != BGP4_FALSE) && 
                  (pPeer->pPeerGrp->u1LocalASConfig  == BGP4_PEER_SHADOW_LOCAL_AS_CONFIG(pPeer)))
               {
                   BGP4_PEER_SHADOW_LOCAL_AS_CONFIG(pPeer) = BGP4_FALSE;
               }
            }
            break;
        case BGP4_PEER_TCPAO_ICMP_ACCEPT_OBJECT:

            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                return BGP4_FAILURE;
            }

            BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_TCPAO_ICMPACCEPT_CFG_EVENT;
            BGP4_INPUTQ_CXT (pQMsg) = u4Context;
            /*TODO To check if wait for response flag needs to be set */

            /*Use u4Data in the Q Msg */
            pQMsg->QMsg.Bgp4PeerMsg.u4Data = (UINT4) i4SetVal;

            Bgp4CopyAddrPrefixStruct (&(pQMsg->QMsg.Bgp4PeerMsg.PeerAddress),
                                      BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                return BGP4_FAILURE;
            }
            break;
        case BGP4_PEER_TCPAO_PKT_DISCARD_OBJECT:

            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                return BGP4_FAILURE;
            }

            BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
            BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_TCPAO_NOMKT_PKTDISC_CFG_EVENT;
            BGP4_INPUTQ_CXT (pQMsg) = u4Context;
            /*TODO To check if wait for response flag needs to be set */
            /*Use u4Data in the Q Msg */
            pQMsg->QMsg.Bgp4PeerMsg.u4Data = (UINT4) i4SetVal;

            Bgp4CopyAddrPrefixStruct (&(pQMsg->QMsg.Bgp4PeerMsg.PeerAddress),
                                      BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                return BGP4_FAILURE;
            }
            break;
        case BGP4_PEER_TCPAO_MKT_ASSOCIATE_OBJECT:
            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                return BGP4_FAILURE;
            }

            BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_TCPAO_MKT_ASSOCIATE_CFG_EVENT;

            BGP4_INPUTQ_CXT (pQMsg) = u4Context;
            /*TODO To check if wait for response flag needs to be set */
            /*Use u4Data in the Q Msg */
            pQMsg->QMsg.Bgp4PeerMsg.u4Data = (UINT4) i4SetVal;

            Bgp4CopyAddrPrefixStruct (&(pQMsg->QMsg.Bgp4PeerMsg.PeerAddress),
                                      BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                return BGP4_FAILURE;
            }

            break;

        case BGP4_PEER_TCPAO_MKT_NO_ASSOCIATE_OBJECT:
            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                return BGP4_FAILURE;
            }

            BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_TCPAO_MKT_NO_ASSOCIATE_CFG_EVENT;

            BGP4_INPUTQ_CXT (pQMsg) = u4Context;
            /*TODO To check if wait for response flag needs to be set */
            /*Use u4Data in the Q Msg */
            pQMsg->QMsg.Bgp4PeerMsg.u4Data = (UINT4) i4SetVal;

            Bgp4CopyAddrPrefixStruct (&(pQMsg->QMsg.Bgp4PeerMsg.PeerAddress),
                                      BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                return BGP4_FAILURE;
            }

            break;
        case BGP4_PEER_HOLD_ADVT_ROUTES_OBJECT:
            BGP4_PEER_HOLD_ADVT_ROUTES (pPeer) = (UINT1) i4SetVal;
            break;
        case BGP4_PEER_BFD_STATUS_CONF_OBJECT:
             pPeer->u1BfdStatus = (UINT1) i4SetVal;
            BGP4_PEER_SHADOW_BFD_STATUS(pPeer) = pPeer->u1BfdStatus;
	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                  (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u1BfdStatus != BGP4_BFD_DISABLE) && 
                  (pPeer->pPeerGrp->u1BfdStatus == BGP4_PEER_SHADOW_BFD_STATUS(pPeer)))
               {
                  BGP4_PEER_SHADOW_BFD_STATUS(pPeer) = BGP4_BFD_DISABLE;
               }
            }
            break;

        default:
            return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

INT4
Bgp4TestBgpPeerTableIntegerObject (UINT4 u4Context, tAddrPrefix PeerAddress,
                                   INT4 i4ObjectType,
                                   INT4 i4TestVal, UINT4 *pu4ErrorCode)
{
    tBgp4PeerEntry     *pPeer = NULL;
    UINT4               u4ContextId = u4Context;
    UINT2               u2Port = 0;
    UINT1               u1ValidStatus;

    /* Validate the Peer Remote Address */
    u1ValidStatus = Bgp4IsValidAddress (PeerAddress, BGP4_FALSE);
    if (u1ValidStatus != BGP4_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return BGP4_FAILURE;
    }

    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeer == NULL)
    {
        CLI_SET_ERR (CLI_BGP4_PEER_NOT_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return BGP4_FAILURE;
    }
    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress))
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4BgpPeerRemoteAddr;

            PTR_FETCH_4 (u4BgpPeerRemoteAddr,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddress));
            if (u4BgpPeerRemoteAddr ==
                BGP4_LOCAL_BGP_ID (BGP4_PEER_CXT_ID (pPeer)))
            {
                CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return BGP4_FAILURE;
            }
            /* Reject Peer Remote Address, if same as local address */
            if (IpIsLocalAddrInCxt (u4ContextId,
                                    u4BgpPeerRemoteAddr, &u2Port) == IP_SUCCESS)
            {
                if (BgpGetIpIfAddr (u2Port) == u4BgpPeerRemoteAddr)
                {
                    CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (BGP4_FAILURE);
                }
            }
        }
            break;
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
        {
            /* Reject Peer Remote Address, if same as local address */
            if (Ip6IsLocalAddr
                (u4Context, BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddress),
                 &u2Port) == IP_SUCCESS)
            {
                CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (BGP4_FAILURE);
            }
        }
            break;
#endif
        default:
            return (BGP4_FAILURE);
    }

    switch (i4ObjectType)
    {
        case BGP4_PEER_ADMIN_STATUS_OBJECT:
            if ((i4TestVal == BGP4_PEER_START) || (i4TestVal == BGP4_PEER_STOP))
            {
                return BGP4_SUCCESS;
            }
            CLI_SET_ERR (CLI_BGP4_INVALID_ADMIN_STATUS_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return BGP4_FAILURE;
        case BGP4_PEER_CONNECT_RETRY_TIME_OBJECT:
            if ((i4TestVal >= BGP4_MIN_CONNRETRY_INTERVAL) &&
                (i4TestVal <= BGP4_MAX_CONNRETRY_INTERVAL))
            {
                return BGP4_SUCCESS;
            }
            CLI_SET_ERR (CLI_BGP4_PEER_CONNECT_RETRY_TIME_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return BGP4_FAILURE;
        case BGP4_PEER_HOLD_TIME_CONF_OBJECT:
            pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
            if (pPeer == NULL)
            {
                CLI_SET_ERR (CLI_BGP4_NO_PEER_ENTRY);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return BGP4_FAILURE;
            }

            if((i4TestVal == 0) && ((INT4) BGP4_PEER_KEEP_ALIVE_TIME (pPeer) == 0 ))
            {
               /* Both Keep alive and Hold time is Zero*/
               return BGP4_SUCCESS;
            } 
            if (i4TestVal <= (INT4) BGP4_PEER_KEEP_ALIVE_TIME (pPeer))
            {
                CLI_SET_ERR
                    (CLI_BGP4_KEEPALIVE_TIME_GREATER_THAN_HOLD_TIME_ERR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return BGP4_FAILURE;
            }
            if ((i4TestVal == 0) || ((i4TestVal >= BGP4_MIN_HOLD_INTERVAL) &&
                                     (i4TestVal <= BGP4_MAX_HOLD_INTERVAL)))
            {
                return BGP4_SUCCESS;
            }
            CLI_SET_ERR (CLI_BGP4_INVALID_HOLD_TIME_VALUE_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return BGP4_FAILURE;
        case BGP4_PEER_KEEPALIVE_CONF_OBJECT:
            pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
            if (pPeer == NULL)
            {
                CLI_SET_ERR (CLI_BGP4_NO_PEER_ENTRY);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return BGP4_FAILURE;
            }
            if (i4TestVal >= (INT4) BGP4_PEER_HOLD_TIME (pPeer))
            {
                CLI_SET_ERR
                    (CLI_BGP4_KEEPALIVE_TIME_GREATER_THAN_HOLD_TIME_ERR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return BGP4_FAILURE;
            }
            if ((i4TestVal == 0) || ((i4TestVal >= BGP4_MIN_KEEP_INTERVAL) &&
                                     (i4TestVal <= BGP4_MAX_KEEP_INTERVAL)))
            {
                return BGP4_SUCCESS;
            }
            CLI_SET_ERR (CLI_BGP4_INVALID_KEEP_ALIVE_VALUE_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return BGP4_FAILURE;
        case BGP4_PEER_MINAS_ORIG_OBJECT:
            if ((i4TestVal >= BGP4_MIN_ASORIG_INTERVAL) &&
                (i4TestVal <= BGP4_MAX_ASORIG_INTERVAL))
            {
                return BGP4_SUCCESS;
            }
            CLI_SET_ERR (CLI_BGP4_INVALID_AS_ORIG_INT_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return BGP4_FAILURE;
        case BGP4_PEER_MINROUTE_ADVT_OBJECT:
            if ((i4TestVal >= BGP4_MIN_RA_INTERVAL) &&
                (i4TestVal <= BGP4_MAX_RA_INTERVAL))
            {
                return BGP4_SUCCESS;
            }
            CLI_SET_ERR (CLI_BGP4_INVALID_RT_ADV_INT_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return BGP4_FAILURE;

            /* RFC 4271 Changes */
        case BGP4_PEER_ALLOW_AUTOMATIC_START_OBJECT:
            if ((i4TestVal == BGP4_PEER_AUTOMATICSTART_ENABLE)
                || (i4TestVal == BGP4_PEER_AUTOMATICSTART_DISABLE))
            {
                return BGP4_SUCCESS;
            }
            CLI_SET_ERR (CLI_BGP4_PEER_AUTOMATIC_START_STOP_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return BGP4_FAILURE;

        case BGP4_PEER_ALLOW_AUTOMATIC_STOP_OBJECT:
            if ((i4TestVal == BGP4_PEER_AUTOMATICSTOP_ENABLE)
                || (i4TestVal == BGP4_PEER_AUTOMATICSTOP_DISABLE))
            {
                return BGP4_SUCCESS;
            }
            CLI_SET_ERR (CLI_BGP4_PEER_AUTOMATIC_START_STOP_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return BGP4_FAILURE;

        case BGP4_PEER_IDLE_HOLD_TIME_CONF_OBJECT:
            pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
            if (pPeer == NULL)
            {
                CLI_SET_ERR (CLI_BGP4_NO_PEER_ENTRY);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return BGP4_FAILURE;
            }

            if ((BGP4_PEER_ALLOW_AUTOMATIC_START (pPeer) ==
                 BGP4_PEER_AUTOMATICSTART_ENABLE)
                || (BGP4_DAMP_PEER_OSCILLATIONS (pPeer) ==
                    BGP4_DAMP_PEER_OSCILLATIONS_ENABLE))
            {
                if ((i4TestVal == 0)
                    || ((i4TestVal >= BGP4_MIN_IDLE_HOLD_INTERVAL)
                        && (i4TestVal <= BGP4_MAX_IDLE_HOLD_INTERVAL)))
                {
                    return BGP4_SUCCESS;
                }
            }
            CLI_SET_ERR (CLI_BGP4_INVALID_HOLD_TIME_VALUE_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return BGP4_FAILURE;

        case BGP4_DAMP_PEER_OSCILLATIONS_OBJECT:
            if ((i4TestVal == BGP4_DAMP_PEER_OSCILLATIONS_ENABLE) ||
                (i4TestVal == BGP4_DAMP_PEER_OSCILLATIONS_DISABLE))
            {
                return BGP4_SUCCESS;
            }
            CLI_SET_ERR (CLI_BGP4_DAMP_PEER_OSCILLATIONS_STATUS_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return BGP4_FAILURE;

        case BGP4_PEER_DELAY_OPEN_OBJECT:
            if ((i4TestVal == BGP4_PEER_DELAY_OPEN_ENABLE) ||
                (i4TestVal == BGP4_PEER_DELAY_OPEN_DISABLE))
            {
                return BGP4_SUCCESS;
            }
            CLI_SET_ERR (CLI_BGP4_PEER_DELAY_OPEN_INTERVAL_STATUS_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return BGP4_FAILURE;

        case BGP4_PEER_DELAY_OPEN_TIME_CONF_OBJECT:
            pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
            if (pPeer == NULL)
            {
                CLI_SET_ERR (CLI_BGP4_NO_PEER_ENTRY);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return BGP4_FAILURE;
            }

            if ((i4TestVal == 0)
                || ((i4TestVal >= BGP4_MIN_DELAY_OPEN_INTERVAL)
                    && (i4TestVal <= BGP4_MAX_DELAY_OPEN_INTERVAL)))
            {
                return BGP4_SUCCESS;
            }
            CLI_SET_ERR (CLI_BGP4_DELAY_OPEN_INTERVAL_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return BGP4_FAILURE;

        case BGP4_PEER_PREFIX_UPPER_LIMIT_OBJECT:
            if (i4TestVal > 0)
            {
                return BGP4_SUCCESS;
            }
            CLI_SET_ERR (CLI_BGP4_PREFIX_VALUE_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return BGP4_FAILURE;

        case BGP4_PEER_TCP_CONNECT_RETRY_COUNT_OBJECT:
            if ((i4TestVal > 0) && (i4TestVal <= BGP4_MAX_CONNECTRETRY_COUNT))
            {
                return BGP4_SUCCESS;
            }
            CLI_SET_ERR (CLI_BGP4_PEER_TCP_RETRY_COUNT_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return BGP4_FAILURE;
        case BGP4_PEER_LOCAL_AS_OBJECT:
            pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
            if (pPeer == NULL)
            {
                CLI_SET_ERR (CLI_BGP4_NO_PEER_ENTRY);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return BGP4_FAILURE;
            }
            if ((BGP4_PEER_ASNO (pPeer) == (UINT4) i4TestVal) ||
                (BGP4_GLB_LOCAL_AS_NO == (UINT4) i4TestVal))
            {
                CLI_SET_ERR (CLI_BGP4_INVALID_LOCAL_AS);
                return BGP4_FAILURE;
            }
            return BGP4_SUCCESS;

        case BGP4_PEER_TCPAO_ICMP_ACCEPT_OBJECT:
            pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
            if (pPeer == NULL)
            {
                CLI_SET_ERR (CLI_BGP4_NO_PEER_ENTRY);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return BGP4_FAILURE;
            }
            if ((i4TestVal == BGP4_TCPAO_PEER_ICMPACCEPT_TRUE) ||
                (i4TestVal == BGP4_TCPAO_PEER_ICMPACCEPT_FALSE))
            {
                return BGP4_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return BGP4_FAILURE;
            }
            break;
        case BGP4_PEER_TCPAO_PKT_DISCARD_OBJECT:
            pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
            if (pPeer == NULL)
            {
                CLI_SET_ERR (CLI_BGP4_NO_PEER_ENTRY);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return BGP4_FAILURE;
            }
            if ((i4TestVal == BGP4_TCPAO_PEER_MKTDISCARD_TRUE) ||
                (i4TestVal == BGP4_TCPAO_PEER_MKTDISCARD_FALSE))
            {
                return BGP4_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return BGP4_FAILURE;
            }
            break;
        case BGP4_PEER_TCPAO_MKT_ASSOCIATE_OBJECT:
            pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
            if (pPeer == NULL)
            {
                CLI_SET_ERR (CLI_BGP4_NO_PEER_ENTRY);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return BGP4_FAILURE;
            }

            /*What do we test when MKT is given */
            /*Check the MKT in the global MKT table and decide */
            {
                INT4                i4TempRowStatus;
                nmhGetFsbgp4TCPMKTAuthRowStatus (i4TestVal, &i4TempRowStatus);
                if (i4TempRowStatus == ACTIVE)
                {
                    if (BGP4_PEER_TCPMD5_PTR (pPeer) == NULL)
                    {
                        return BGP4_SUCCESS;
                    }
                    else
                    {
                        CLI_SET_ERR (CLI_BGP4_MD5_AUTH_ENABLED);
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return BGP4_FAILURE;
                    }
                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return BGP4_FAILURE;

                }
            }
            break;
        default:
            return BGP4_FAILURE;
    }
    return SNMP_SUCCESS;
}

INT4
Bgp4ValidatePathAttrTableIndex (tNetAddress RoutePrefix,
                                tAddrPrefix PeerAddress)
{
    UINT1               u1ValidStatus;

    /* Validate the prefix length  */
    u1ValidStatus = Bgp4IsValidPrefixLength (RoutePrefix);
    if (u1ValidStatus == BGP4_FALSE)
    {
        return BGP4_FAILURE;
    }

    /* Validate the prefix */
    u1ValidStatus = Bgp4IsValidRouteAddress (RoutePrefix);
    if (u1ValidStatus != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }

    /* Validate the peer remote address here */
    u1ValidStatus = Bgp4IsValidAddress (PeerAddress, BGP4_FALSE);
    if (u1ValidStatus != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

INT4
Bgp4ValidateFsbgp4RflRouteReflectTableIndex (tNetAddress RoutePrefix,
                                             tAddrPrefix PeerAddress)
{
    UINT1               u1ValidStatus;

    /* Validate the SAFI */
    u1ValidStatus =
        Bgp4MpeValidateSubAddressType (BGP4_SAFI_IN_NET_ADDRESS_INFO
                                       (RoutePrefix));
    if (u1ValidStatus != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }

    /* Validate the prefix length  */
    u1ValidStatus = Bgp4IsValidPrefixLength (RoutePrefix);
    if (u1ValidStatus == BGP4_FALSE)
    {
        return BGP4_FAILURE;
    }

    if ((Bgp4IsValidAddress
         (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (RoutePrefix),
          BGP4_TRUE)) != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }

    if ((Bgp4IsValidAddress (PeerAddress, BGP4_FALSE)) != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

INT4
Bgp4GetFirstPathAttrTableIndex (UINT4 u4Context, tNetAddress * pRoutePrefix,
                                tAddrPrefix * pPeerAddress)
{
    INT4                i4RetVal = BGP4_FAILURE;
    UINT4               u4AsafiMask;
    UINT2               u2Index;

    /* Get the index corresponding to this mask */
    for (u2Index = 0; u2Index < (BGP4_MPE_ADDR_FAMILY_MAX_INDEX - 1); u2Index++)
    {
        u4AsafiMask = gau4AsafiMask[u2Index];
#ifdef L3VPN
        /* Since Both Carrying Label and IIpv4 are in the same RIB,
         * GetFirstEntry should be called only once.i. 
         * If they are put in different RIB, 
         * pls remove this check*/
        if (u4AsafiMask == CAP_MP_LABELLED_IPV4)
        {
            continue;
        }
#endif
        switch (u4AsafiMask)
        {
            case CAP_MP_IPV4_UNICAST:
                /* Get the first route from IPV4-Unicast routing table */
                i4RetVal = Bgp4MpGetFirstPathAttrTableIndex
                    (u4Context, CAP_MP_IPV4_UNICAST, pRoutePrefix,
                     pPeerAddress);
                break;
#ifdef BGP4_IPV6_WANTED
            case CAP_MP_IPV6_UNICAST:
                /* Get the first route from IPV6-Unicast routing table */
                i4RetVal = Bgp4MpGetFirstPathAttrTableIndex
                    (u4Context, CAP_MP_IPV6_UNICAST, pRoutePrefix,
                     pPeerAddress);
                break;
#endif
#ifdef L3VPN
            case CAP_MP_VPN4_UNICAST:
                /* Get the first route from IPV4-Unicast routing table */
                i4RetVal =
                    Bgp4MpGetFirstPathAttrTableIndex (u4Context,
                                                      CAP_MP_VPN4_UNICAST,
                                                      pRoutePrefix,
                                                      pPeerAddress);
                break;
#endif
            default:
                i4RetVal = BGP4_FAILURE;
                break;
        }
        if (i4RetVal == BGP4_SUCCESS)
        {
            return BGP4_SUCCESS;
        }
    }
    return i4RetVal;
}

INT4
Bgp4GetNextPathAttrTableIndex (UINT4 u4Context, tNetAddress RoutePrefix,
                               tAddrPrefix PeerAddress,
                               tNetAddress * pNextRoutePrefix,
                               tAddrPrefix * pNextPeerAddress)
{
    tBgp4PeerEntry     *pPeer = NULL;
    INT4                i4RetVal = BGP4_FAILURE;
    INT1                i1Found = BGP4_FALSE;
    UINT2               u2Index;
    UINT2               u2NextAfi = BGP4_FALSE;
    UINT4               u4AsafiMask;

    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeer == NULL)
    {
        /* Peer is not found. */
        return BGP4_FAILURE;
    }
    BGP4_GET_AFISAFI_MASK (RoutePrefix.NetAddr.u2Afi, RoutePrefix.u2Safi,
                           u4AsafiMask);

    for (u2Index = 0; u2Index < (BGP4_MPE_ADDR_FAMILY_MAX_INDEX - 1); u2Index++)
    {
        if (gau4AsafiMask[u2Index] == u4AsafiMask)
        {
            i1Found = BGP4_TRUE;
            break;
        }
    }
    if (i1Found == BGP4_FALSE)
    {
        return BGP4_FAILURE;
    }

    /* Get the index corresponding to this mask */
    for (; u2Index < (BGP4_MPE_ADDR_FAMILY_MAX_INDEX - 1); u2Index++)
    {
        u4AsafiMask = gau4AsafiMask[u2Index];
        switch (u4AsafiMask)
        {
            case CAP_MP_IPV4_UNICAST:
                /* Get the first route from IPV4-Unicast routing table */
                i4RetVal =
                    Bgp4MpGetNextPathAttrTableIndex (u4Context, RoutePrefix,
                                                     PeerAddress,
                                                     pNextRoutePrefix,
                                                     pNextPeerAddress);
                if (i4RetVal == BGP4_SUCCESS)
                {
                    /* Got the Next IPV4 - UNICAST entry */
                    return BGP4_SUCCESS;
                }
                else
                {
                    /* No more route left in IPV4 - Unicast. Try and
                     * get the route from the next Afi. */
                    u2NextAfi = BGP4_TRUE;
                }
                break;
#ifdef L3VPN
            case CAP_MP_LABELLED_IPV4:
                if (u2NextAfi == BGP4_TRUE)
                {
                    /* Since Ipv4 and Labelled Ipv4 are stored in same RIB,
                     * Getting First again will cause loop
                     */
                }
                else
                {
                    /* Get the next route to the given route. */
                    i4RetVal = Bgp4MpGetNextPathAttrTableIndex
                        (u4Context, RoutePrefix, PeerAddress, pNextRoutePrefix,
                         pNextPeerAddress);
                    if (i4RetVal == BGP4_SUCCESS)
                    {
                        /* Got the Next Labelled - UNICAST entry */
                        return BGP4_SUCCESS;
                    }
                }
                /* No more route left in Labelled IPV4 - Unicast. Try and
                 * get the route from the next Afi. */
                u2NextAfi = BGP4_TRUE;
                break;
#endif
#ifdef BGP4_IPV6_WANTED
            case CAP_MP_IPV6_UNICAST:
                if (u2NextAfi == BGP4_TRUE)
                {
                    /* Get the first route from IPV6-Unicast routing table */
                    u2NextAfi = BGP4_FALSE;
                    i4RetVal = Bgp4MpGetFirstPathAttrTableIndex
                        (u4Context, CAP_MP_IPV6_UNICAST, pNextRoutePrefix,
                         pNextPeerAddress);
                    if (i4RetVal == BGP4_SUCCESS)
                    {
                        /* Got the First IPV6 - UNICAST entry */
                        return BGP4_SUCCESS;
                    }
                }
                else
                {
                    /* Get the next route to the given route. */
                    i4RetVal = Bgp4MpGetNextPathAttrTableIndex
                        (u4Context, RoutePrefix, PeerAddress, pNextRoutePrefix,
                         pNextPeerAddress);
                    if (i4RetVal == BGP4_SUCCESS)
                    {
                        /* Got the Next IPV6 - UNICAST entry */
                        return BGP4_SUCCESS;
                    }
                }
                /* No more route left in Ipv6 - Unicast. Try and
                 * get the route from the next Afi. */
                u2NextAfi = BGP4_TRUE;
                break;
#endif
#ifdef L3VPN
            case CAP_MP_VPN4_UNICAST:
                if (u2NextAfi == BGP4_TRUE)
                {
                    /* Get the first route from IPV6-Unicast routing table */
                    i4RetVal = Bgp4MpGetFirstPathAttrTableIndex
                        (u4Context, CAP_MP_VPN4_UNICAST, pNextRoutePrefix,
                         pNextPeerAddress);
                    if (i4RetVal == BGP4_SUCCESS)
                    {
                        /* Got the First Vpnv4- UNICAST entry */
                        return BGP4_SUCCESS;
                    }
                }
                else
                {
                    /* Get the next route to the given route. */
                    i4RetVal = Bgp4MpGetNextPathAttrTableIndex
                        (u4Context, RoutePrefix, PeerAddress, pNextRoutePrefix,
                         pNextPeerAddress);
                    if (i4RetVal == BGP4_SUCCESS)
                    {
                        /* Got the Next Vpnv4- UNICAST entry */
                        return BGP4_SUCCESS;
                    }
                }
                /* No more route left in Vpnv4 - Unicast. Try and
                 * get the route from the next Afi. */
                u2NextAfi = BGP4_TRUE;
                break;
#endif
            default:
                i4RetVal = BGP4_FAILURE;
                break;
        }
    }
    return i4RetVal;
}

INT1
Bgp4GetNextRouteProfile (tNetAddress * pNextRoutePrefix,
                         tAddrPrefix * pNextPeerAddress, tBgp4PeerEntry * pPeer)
{
    tBgp4PeerEntry     *pNextPeer = NULL;
    INT1                i1RetVal;
    INT4                i4RetVal;
    UINT4               u4Context = 0;

    if (pPeer == NULL)
    {
        return BGP4_FALSE;
    }
    u4Context = BGP4_PEER_CXT_ID (pPeer);
    /* First get the IPV4 route */
    i4RetVal = Bgp4Ipv4GetFirstPeerPathAttrTableIndex (pNextRoutePrefix,
                                                       pNextPeerAddress, pPeer);
    if (i4RetVal == BGP4_SUCCESS)
    {
        return BGP4_TRUE;
    }

#ifdef BGP4_IPV6_WANTED
    i4RetVal = Bgp4Ipv6GetFirstPeerPathAttrTableIndex (pNextRoutePrefix,
                                                       pNextPeerAddress, pPeer);
    if (i4RetVal == BGP4_SUCCESS)
    {
        return BGP4_TRUE;
    }
#endif
#ifdef L3VPN
    i4RetVal = Bgp4Vpnv4GetFirstPeerPathAttrTableIndex (pNextRoutePrefix,
                                                        pNextPeerAddress,
                                                        pPeer);
    if (i4RetVal == BGP4_SUCCESS)
    {
        return BGP4_TRUE;
    }
#endif

    /* Get the next peer from peer list */
    pNextPeer =
        (tBgp4PeerEntry *) TMO_SLL_Next (BGP4_PEERENTRY_HEAD (u4Context),
                                         &pPeer->TsnNextpeer);
    i1RetVal =
        Bgp4GetNextRouteProfile (pNextRoutePrefix, pNextPeerAddress, pNextPeer);
    return (i1RetVal);
}

INT1
Bgp4GetNextIpv4RouteProfile (tNetAddress * pNextRoutePrefix,
                             tAddrPrefix * pNextPeerAddress,
                             tBgp4PeerEntry * pPeer)
{
    tBgp4PeerEntry     *pNextPeer = NULL;
    INT1                i1RetVal;
    INT4                i4RetVal;
    UINT4               u4Context = 0;

    if (pPeer == NULL)
    {
        return BGP4_FALSE;
    }
    u4Context = BGP4_PEER_CXT_ID (pPeer);
    /* First get the IPV4 route */
    i4RetVal = Bgp4Ipv4GetFirstPeerPathAttrTableIndex (pNextRoutePrefix,
                                                       pNextPeerAddress, pPeer);
    if (i4RetVal == BGP4_SUCCESS)
    {
        return BGP4_TRUE;
    }

    /* Get the next peer from peer list */
    pNextPeer =
        (tBgp4PeerEntry *) TMO_SLL_Next (BGP4_PEERENTRY_HEAD (u4Context),
                                         &pPeer->TsnNextpeer);
    i1RetVal =
        Bgp4GetNextIpv4RouteProfile (pNextRoutePrefix, pNextPeerAddress,
                                     pNextPeer);
    return (i1RetVal);
}

#ifdef BGP4_IPV6_WANTED
INT1
Bgp4GetNextIpv6RouteProfile (tNetAddress * pNextRoutePrefix,
                             tAddrPrefix * pNextPeerAddress,
                             tBgp4PeerEntry * pPeer)
{
    tBgp4PeerEntry     *pNextPeer = NULL;
    INT4                i4RetVal;
    INT1                i1RetVal;
    UINT4               u4Context = 0;
    if (pPeer == NULL)
    {
        return BGP4_FALSE;
    }
    u4Context = BGP4_PEER_CXT_ID (pPeer);
    i4RetVal = Bgp4Ipv6GetFirstPeerPathAttrTableIndex (pNextRoutePrefix,
                                                       pNextPeerAddress, pPeer);
    if (i4RetVal == BGP4_SUCCESS)
    {
        return BGP4_TRUE;
    }

    /* Get the next peer from peer list */
    pNextPeer =
        (tBgp4PeerEntry *) TMO_SLL_Next (BGP4_PEERENTRY_HEAD (u4Context),
                                         &pPeer->TsnNextpeer);
    i1RetVal =
        Bgp4GetNextIpv6RouteProfile (pNextRoutePrefix, pNextPeerAddress,
                                     pNextPeer);
    return (i1RetVal);
}
#endif

INT4
Bgp4GetPathAttrTableIntegerObject (UINT4 u4Context, tNetAddress RoutePrefix,
                                   tAddrPrefix PeerAddress,
                                   INT4 i4ObjectType, INT4 *pi4RetVal)
{
    tRouteProfile      *pMatchRoute = NULL;
    tBgp4Info          *pBgp4Info = NULL;

    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return BGP4_FAILURE;
    }

    pMatchRoute = Bgp4GetRoute (u4Context, RoutePrefix, PeerAddress, NULL);
    if ((pMatchRoute == NULL) ||
        ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_HISTORY) ==
         BGP4_RT_HISTORY))
    {
        *pi4RetVal = 0;
        bgp4RibUnlock ();
        return BGP4_FAILURE;
    }

    switch (i4ObjectType)
    {
        case BGP4_ATTR_ORIGIN:
            *pi4RetVal =
                (INT4) (BGP4_INFO_ORIGIN (BGP4_RT_BGP_INFO (pMatchRoute))) +
                BGP4_INCREMENT_BY_ONE;
            break;
        case BGP4_ATTR_MED:
            pBgp4Info = BGP4_RT_BGP_INFO (pMatchRoute);
            if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_MED_MASK)
                == BGP4_ATTR_MED_MASK)
            {
                *pi4RetVal = (INT4) (BGP4_INFO_RCVD_MED (pBgp4Info));
            }
            else
            {
                *pi4RetVal = BGP4_INVALID_METRIC;
            }
            break;
        case BGP4_ATTR_LOCAL_PREF:
            pBgp4Info = BGP4_RT_BGP_INFO (pMatchRoute);
            if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_LOCAL_PREF_MASK)
                == BGP4_ATTR_LOCAL_PREF_MASK)
            {
                *pi4RetVal = (INT4) (BGP4_INFO_RCVD_LOCAL_PREF (pBgp4Info));
            }
            else
            {
                *pi4RetVal = BGP4_INVALID_METRIC;
            }
            break;
        case BGP4_ATTR_ATOMIC_AGGR:
            if ((BGP4_INFO_ATTR_FLAG (BGP4_RT_BGP_INFO (pMatchRoute))
                 & BGP4_ATTR_ATOMIC_AGGR_MASK) == BGP4_ATTR_ATOMIC_AGGR_MASK)
            {
                *pi4RetVal = BGP4_LESS_SPECIFIC_ROUTE_SELECTED;
            }
            else
            {
                *pi4RetVal = BGP4_LESS_SPECIFIC_ROUTE_NOT_SELECTED;
            }
            break;
        case BGP4_PATHATTR_TBL_AGGRAS_OBJECT:
            pBgp4Info = BGP4_RT_BGP_INFO (pMatchRoute);
            if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_AGGREGATOR_MASK)
                == BGP4_ATTR_AGGREGATOR_MASK)
            {
                *pi4RetVal = (INT4) (BGP4_INFO_AGGREGATOR_AS (pBgp4Info));
            }
            else
            {
                *pi4RetVal = 0;
            }
            break;
        case BGP4_PATHATTR_TBL_CALC_LP_OBJECT:
            pBgp4Info = BGP4_RT_BGP_INFO (pMatchRoute);
            *pi4RetVal = (INT4) (BGP4_RT_LOCAL_PREF (pMatchRoute));
            break;
        case BGP4_PATHATTR_BEST_OBJECT:
            if ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_STALE)
                == BGP4_RT_STALE)
            {
                *pi4RetVal = BGP4_STALE_ROUTE;
            }
            else if ((BGP4_RT_GET_FLAGS (pMatchRoute) & BGP4_RT_BEST)
                     == BGP4_RT_BEST)
            {
                *pi4RetVal = BGP4_BEST_ROUTE;
            }
            else
            {
                *pi4RetVal = BGP4_NOT_BEST_ROUTE;
            }
            break;
        default:
            *pi4RetVal = 0;
            bgp4RibUnlock ();
            return BGP4_FAILURE;
    }

    bgp4RibUnlock ();
    return BGP4_SUCCESS;
}

INT4
Bgp4MpGetFirstPathAttrTableIndex (UINT4 u4Context, UINT4 u4AfiSafi,
                                  tNetAddress * pRoutePrefix,
                                  tAddrPrefix * pPeerAddress)
{
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pNextRtProfile = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4RetVal = BGP4_FAILURE;

    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return BGP4_FAILURE;
    }
#ifdef L3VPN
    if (u4AfiSafi == CAP_MP_VPN4_UNICAST)
    {
        pRibNode = BGP4_VPN4_RIB_TREE (u4Context);
    }
#endif
    if (u4AfiSafi == CAP_MP_IPV4_UNICAST)
    {
        pRibNode = BGP4_IPV4_RIB_TREE (u4Context);
    }
#ifdef BGP4_IPV6_WANTED
    else if (u4AfiSafi == CAP_MP_IPV6_UNICAST)
    {
        pRibNode = BGP4_IPV6_RIB_TREE (u4Context);
    }
#endif
    i4RetVal = Bgp4RibhGetFirstEntry (u4AfiSafi, &pRtProfile, &pRibNode, TRUE);
    if (i4RetVal == BGP4_FAILURE)
    {
        /* No u4AfiSafi route. */
        bgp4RibUnlock ();
        return BGP4_FAILURE;
    }

    for (;;)
    {
        if ((BGP4_RT_PEER_ENTRY (pRtProfile) != NULL) &&
            ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_HISTORY) !=
             BGP4_RT_HISTORY))
        {
            break;
        }

        if (BGP4_RT_PEER_ENTRY (pRtProfile) == NULL)
        {
            /* Non - BGP Route. Get the BGP Route. */
            pNextRtProfile = BGP4_RT_NEXT (pRtProfile);
            if (pNextRtProfile != NULL)
            {
                pRtProfile = pNextRtProfile;
                pNextRtProfile = NULL;
                continue;
            }
        }

        pNextRtProfile = NULL;
        /* Route is a History/Last route. History routes can never be used
         * for Index and any routes following this route in this RIB node
         * will also be history. So get the next route entry from the RIB */
        i4RetVal = Bgp4RibhGetNextEntry (pRtProfile, u4Context,
                                         &pNextRtProfile, &pRibNode, TRUE);
        if (i4RetVal == BGP4_FAILURE)
        {
            pRtProfile = NULL;
            break;
        }
        pRtProfile = pNextRtProfile;
        pNextRtProfile = NULL;
    }

    if (pRtProfile != NULL)
    {
        Bgp4SnmphCopyNetAddr (pRtProfile, BGP4_RT_PEER_ENTRY (pRtProfile),
                              pRoutePrefix);
        Bgp4CopyAddrPrefixStruct (pPeerAddress,
                                  BGP4_PEER_REMOTE_ADDR_INFO (BGP4_RT_PEER_ENTRY
                                                              (pRtProfile)));
        bgp4RibUnlock ();
        return BGP4_SUCCESS;
    }

    bgp4RibUnlock ();
    return BGP4_FAILURE;
}

INT4
Bgp4Ipv4GetFirstPeerPathAttrTableIndex (tNetAddress * pRoutePrefix,
                                        tAddrPrefix * pPeerAddress,
                                        tBgp4PeerEntry * pPeer)
{
    tRouteProfile      *pRtProfile = NULL;
    tBgp4Info          *pRtInfo = NULL;

    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return BGP4_FAILURE;
    }

    /* Search for the IPV4 routes in RIB */
    pRtProfile = Bgp4RibhGetFirstAsafiPeerRtEntry (pPeer, CAP_MP_IPV4_UNICAST);
    for (;;)
    {
        if ((pRtProfile == NULL) ||
            ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_HISTORY) !=
             BGP4_RT_HISTORY))
        {
            break;
        }

        /* Route is a History route. This can never be a Index. Get the
         * next route from the peer */
        pRtProfile = Bgp4RibhGetNextPeerRtEntry (pRtProfile, pPeer);
    }

    if (pRtProfile != NULL)
    {
        pRtInfo = BGP4_RT_BGP_INFO (pRtProfile);
        if (pRtInfo != NULL)
        {
            Bgp4CopyNetAddressStruct (pRoutePrefix,
                                      BGP4_RT_NET_ADDRESS_INFO (pRtProfile));
            Bgp4CopyAddrPrefixStruct (pPeerAddress,
                                      BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
            bgp4RibUnlock ();
            return BGP4_SUCCESS;
        }
    }
    bgp4RibUnlock ();
    return BGP4_FAILURE;
}

INT4
Bgp4MpGetNextPathAttrTableIndex (UINT4 u4Context, tNetAddress RoutePrefix,
                                 tAddrPrefix PeerAddress,
                                 tNetAddress * pNextRoutePrefix,
                                 tAddrPrefix * pNextPeerAddress)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tRouteProfile      *pFndRtProfile = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile       RtProfile;
    tRouteProfile      *pNextRtProfile = NULL;
    tBgp4Info           RtInfo;
    VOID               *pRibNode = NULL;
    INT4                i4RetVal = BGP4_FAILURE;
#ifdef L3VPN
    UINT4               u4AsafiMask;
#endif
    UINT1               u1PeerFound = BGP4_FALSE;

    Bgp4InitBgp4info (&RtInfo);

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
    {
        if ((PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pPeer), PeerAddress))
            == BGP4_TRUE)
        {
            u1PeerFound = BGP4_TRUE;
            break;
        }
    }
    if (u1PeerFound == BGP4_FALSE)
    {
        /* No matching Peer. */
        return BGP4_FAILURE;
    }
#ifdef L3VPN
    BGP4_GET_AFISAFI_MASK (RoutePrefix.NetAddr.u2Afi, RoutePrefix.u2Safi,
                           u4AsafiMask);
    if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
    {
        Bgp4CopyVpn4NetAddressToRt (&RtProfile, RoutePrefix);
    }
    else
#endif
    {
        Bgp4CopyNetAddressStruct (&(RtProfile.NetAddress), RoutePrefix);
    }
    BGP4_RT_PEER_ENTRY ((&RtProfile)) = pPeer;
    BGP4_RT_BGP4_INF ((&RtProfile)) = &RtInfo;

    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return BGP4_FAILURE;
    }

    i4RetVal = Bgp4RibhLookupRtEntry (&RtProfile, u4Context,
                                      BGP4_TREE_FIND_EXACT,
                                      &pFndRtProfile, &pRibNode);
    if (i4RetVal == BGP4_FAILURE)
    {
        /* No matching route. */
        bgp4RibUnlock ();
        return BGP4_FAILURE;
    }

    pRtProfile = Bgp4DshGetPeerRtFromProfileList (pFndRtProfile, pPeer);
    if (pRtProfile != NULL)
    {
        /* Peer route is found. Get the next route. */
        pRtProfile = BGP4_RT_NEXT (pRtProfile);
    }

    for (;;)
    {
        if (pRtProfile != NULL)
        {
            if ((BGP4_RT_PEER_ENTRY (pRtProfile) != NULL) &&
                ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_HISTORY) !=
                 BGP4_RT_HISTORY))
            {
                break;
            }

            if (BGP4_RT_PEER_ENTRY (pRtProfile) == NULL)
            {
                /* Non - BGP Route. Get the BGP Route. */
                pNextRtProfile = BGP4_RT_NEXT (pRtProfile);
                if (pNextRtProfile != NULL)
                {
                    pRtProfile = pNextRtProfile;
                    pNextRtProfile = NULL;
                    continue;
                }
            }
        }

        pNextRtProfile = NULL;
        /* Route is a History/Last route. History routes can never be used
         * for Index and any routes following this route in this RIB node
         * will also be history. So get the next route entry from the RIB */
        i4RetVal = Bgp4RibhGetNextEntry (pFndRtProfile, u4Context,
                                         &pNextRtProfile, &pRibNode, TRUE);
        if (i4RetVal == BGP4_FAILURE)
        {
            pRtProfile = NULL;
            break;
        }
        pFndRtProfile = pNextRtProfile;
        pRtProfile = pNextRtProfile;
        pNextRtProfile = NULL;
    }

    if (pRtProfile != NULL)
    {
        /* Next route found. */
        Bgp4SnmphCopyNetAddr (pRtProfile, BGP4_RT_PEER_ENTRY (pRtProfile),
                              pNextRoutePrefix);
        Bgp4CopyAddrPrefixStruct (pNextPeerAddress,
                                  BGP4_PEER_REMOTE_ADDR_INFO (BGP4_RT_PEER_ENTRY
                                                              (pRtProfile)));
        bgp4RibUnlock ();
        return BGP4_SUCCESS;
    }

    bgp4RibUnlock ();
    return BGP4_FAILURE;
}

INT4
Bgp4Ipv4GetNextIpv4PathAttrTableIndex (UINT4 u4Context, tNetAddress RoutePrefix,
                                       tAddrPrefix PeerAddress,
                                       tNetAddress * pNextRoutePrefix,
                                       tAddrPrefix * pNextPeerAddress)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tBgp4PeerEntry     *pTmpPeer = NULL;
    tRouteProfile      *pFndRtProfile = NULL;
    tRouteProfile       RtProfile;
    tBgp4Info           RtInfo;
    UINT1               u1MatchFound = BGP4_FALSE;

    Bgp4InitBgp4info (&RtInfo);

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
    {
        if ((PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pPeer), PeerAddress))
            == BGP4_TRUE)
        {
            break;
        }
    }
    if (pPeer == NULL)
    {
        return BGP4_FAILURE;
    }

    Bgp4CopyNetAddressStruct (&(RtProfile.NetAddress), RoutePrefix);
    BGP4_RT_PEER_ENTRY ((&RtProfile)) = pPeer;
    BGP4_RT_BGP4_INF ((&RtProfile)) = &RtInfo;

    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return BGP4_FAILURE;
    }
    pFndRtProfile = Bgp4RibhGetNextIpv4PeerRtEntry (&RtProfile, pPeer);
    bgp4RibUnlock ();
    if (pFndRtProfile == NULL)
    {
        /* Get the next peer from peer list */
        pTmpPeer =
            (tBgp4PeerEntry *) TMO_SLL_Next (BGP4_PEERENTRY_HEAD (u4Context),
                                             &pPeer->TsnNextpeer);
        u1MatchFound =
          (UINT1)Bgp4GetNextIpv4RouteProfile (pNextRoutePrefix, pNextPeerAddress,
                                         pTmpPeer);
        if (u1MatchFound == BGP4_TRUE)
        {
            return BGP4_SUCCESS;
        }
    }
    else
    {
        Bgp4CopyNetAddressStruct (pNextRoutePrefix,
                                  BGP4_RT_NET_ADDRESS_INFO (pFndRtProfile));
        Bgp4CopyAddrPrefixStruct (pNextPeerAddress,
                                  BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
        return BGP4_SUCCESS;
    }
    return BGP4_FAILURE;
}

INT4
Bgp4ValidateFsbgp4PeerExtTable (tAddrPrefix PeerAddress)
{
    INT4                i4RetVal;

    i4RetVal = Bgp4ValidateBgpPeerTableIndex (PeerAddress);
    return i4RetVal;
}

INT4
Bgp4GetFirstIndexFsbgp4PeerExtTable (UINT4 u4Context,
                                     tAddrPrefix * pPeerAddress)
{
    INT4                i4RetVal;

    i4RetVal = Bgp4GetFirstIndexBgpPeerTable (u4Context, pPeerAddress);
    return i4RetVal;
}

INT4
Bgp4GetNextIndexFsbgp4PeerExtTable (UINT4 u4Context, tAddrPrefix PeerAddress,
                                    tAddrPrefix * pNextPeerAddress)
{
    INT4                i4RetVal;

    i4RetVal =
        Bgp4GetNextIndexBgpPeerTable (u4Context, PeerAddress, pNextPeerAddress);
    return i4RetVal;
}

INT4
Bgp4GetFsBgp4PeerExtTableIntegerObject (UINT4 u4Context,
                                        tAddrPrefix PeerAddress,
                                        INT4 i4ObjectType, INT4 *pi4RetVal)
{
    tBgp4PeerEntry     *pPeer = NULL;

    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeer == NULL)
    {
        return BGP4_FAILURE;
    }
    switch (i4ObjectType)
    {
        case BGP4_PEER_EXT_CONF_OBJECT:
            *pi4RetVal = BGP4_PEER_CREATE;
            break;
        case BGP4_PEER_EXT_REMOTE_AS_OBJECT:
           if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                   (BGP4_PEER_SHADOW_REMOTE_AS_NO (pPeer) != BGP4_INV_AS))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_REMOTE_AS_NO (pPeer);
           }
           else
           {
            *pi4RetVal = (INT4) BGP4_PEER_ASNO (pPeer);
           }
            break;
        case BGP4_PEER_EXT_MULTIHOP_OBJECT:
           if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) && 
#endif
               (BGP4_PEER_SHADOW_EBGP_MULTIHOP (pPeer) != BGP4_EBGP_MULTI_HOP_DISABLE))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_EBGP_MULTIHOP (pPeer);
           }
           else
           {
            *pi4RetVal = BGP4_PEER_EBGP_MULTIHOP (pPeer);
           }
            break;
        case BGP4_PEER_EXT_NEXTHOP_SELF_OBJECT:
           if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) && 
#endif
               (BGP4_PEER_SHADOW_SELF_NEXTHOP (pPeer) != BGP4_DISABLE_NEXTHOP_SELF))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_SELF_NEXTHOP (pPeer);
           }
           else
           {
            *pi4RetVal = BGP4_PEER_SELF_NEXTHOP (pPeer);
           }
            break;
        case BGP4_PEER_EXT_HOPLIMIT_OBJECT:
	    if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
               (BGP4_PEER_SHADOW_HOP_LIMIT (pPeer) != BGP4_DEFAULT_HOPLIMIT))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_HOP_LIMIT (pPeer);
           }
           else
           {
            *pi4RetVal = BGP4_PEER_HOPLIMIT (pPeer);
           }
            break;
        case BGP4_PEER_EXT_TCP_SENDBUF_OBJECT:
	   if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
               (BGP4_PEER_SHADOW_SEND_BUF (pPeer) != BGP4_DEFAULT_SENDBUF))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_SEND_BUF (pPeer);
           }
           else
           {
            *pi4RetVal = (INT4) BGP4_PEER_SENDBUF (pPeer);
           }
            break;
        case BGP4_PEER_EXT_TCP_RECVBUF_OBJECT:
           if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
               (BGP4_PEER_SHADOW_RECV_BUF (pPeer) != BGP4_DEFAULT_RECVBUF))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_RECV_BUF (pPeer);
           }
           else
           {
            *pi4RetVal = (INT4) BGP4_PEER_RECVBUF (pPeer);
           }
            break;
        case BGP4_PEER_EXT_RFL_CLIENT_OBJECT:
	   if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
               (BGP4_PEER_SHADOW_RFL_CLIENT (pPeer) != NON_CLIENT))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_RFL_CLIENT (pPeer);
           }
           else
           {
            *pi4RetVal = BGP4_PEER_RFL_CLIENT (pPeer);
           }
            break;
        case BGP4_PEER_EXT_COMM_SEND_STATUS_OBJECT:
           if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
               (BGP4_PEER_SHADOW_COMM_SEND_STATUS (pPeer) != BGP4_TRUE))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_COMM_SEND_STATUS (pPeer) + BGP4_INCREMENT_BY_ONE;
           }
           else
           {
               *pi4RetVal = BGP4_PEER_COMM_SEND_STATUS (pPeer) + BGP4_INCREMENT_BY_ONE;
           }
            break;
        case BGP4_PEER_EXT_ECOMM_SEND_STATUS_OBJECT:
	    if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
               (BGP4_PEER_SHADOW_ECOMM_SEND_STATUS (pPeer) != BGP4_TRUE))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_ECOMM_SEND_STATUS (pPeer) + BGP4_INCREMENT_BY_ONE;
           }
           else
           {
               *pi4RetVal = BGP4_PEER_ECOMM_SEND_STATUS (pPeer) + BGP4_INCREMENT_BY_ONE;
           }
            break;
        case BGP4_PEER_EXT_PASSIVE_SET_OBJECT:
       
 	   if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
               (BGP4_PEER_SHADOW_PASSIVE (pPeer) != BGP4_FALSE))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_PASSIVE (pPeer);
           }
           else
           {
            *pi4RetVal = BGP4_PEER_CONN_PASSIVE (pPeer);
           }
            break;
        case BGP4_PEER_EXT_DEFAULT_ROUTE_OBJECT:
          if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
               (BGP4_PEER_SHADOW_DFT_ROUTE_ORIG_STATUS (pPeer) != BGP4_DEF_ROUTE_ORIG_DISABLE))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_DFT_ROUTE_ORIG_STATUS (pPeer);
           }
           else
           {
            *pi4RetVal = BGP4_PEER_DEF_ROUTE_ORIG_STATUS (pPeer);
           }
            break;
        case BGP4_PEER_EXT_ACTIVATE_MP_CAP_OBJECT:
        case BGP4_PEER_EXT_DEACTIVATE_MP_CAP_OBJECT:
            *pi4RetVal = 0;
            break;
#ifdef L3VPN
        case BGP4_PEER_EXT_VPN_CE_RT_ADVT_OBJECT:
            *pi4RetVal = BGP4_VPN4_PEER_CERT_STATUS (pPeer);
            break;
#endif
        case BGP4_PEER_EXT_OVERRIDE_CAPABILITY_OBJECT:
           if (
#ifdef L2RED_WANTED
           (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) && 
#endif
               (BGP4_PEER_SHADOW_OVERRIDE_CAP (pPeer) != BGP4_DISABLE_OVERRIDE_CAPABILITY))
           {
               *pi4RetVal = (INT4) BGP4_PEER_SHADOW_OVERRIDE_CAP (pPeer);
           }
           else
           {
            *pi4RetVal = BGP4_PEER_OVERRIDE_CAPABILITY (pPeer);
           }
            break;
        default:
            return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

INT4
Bgp4SetFsBgp4PeerExtTableIntegerObject (UINT4 u4Context,
                                        tAddrPrefix PeerAddress,
                                        INT4 i4ObjectType, INT4 i4SetVal)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tBgp4QMsg          *pQMsg = NULL;
    INT4                i4RetVal = 0;
    UINT1               u1IsDeactive = BGP4_FALSE;
    UINT1               u1EBGPMultiHop = 0;

    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);

    switch (i4ObjectType)
    {
        case BGP4_PEER_EXT_CONF_OBJECT:
            switch (i4SetVal)
            {
                case BGP4_PEER_CREATE:
                    if (pPeer != NULL)
                    {
                        /* Peer already present. If peer  delete_pending
                         * flag is set, then create operation fails */
                        if ((BGP4_GET_PEER_PEND_FLAG (pPeer) &
                             BGP4_PEER_DELETE_PENDING) ==
                            BGP4_PEER_DELETE_PENDING)
                        {
                            return BGP4_FAILURE;
                        }
                    }
                    else
                    {
                        /* Create the entry for the peer */
                        Bgp4PeerCreatePeerEntry (u4Context, PeerAddress);
                    }
                    break;
                case BGP4_PEER_DELETE:
                    if (pPeer == NULL)
                    {
                        return BGP4_FAILURE;
                    }

                    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
                    if (pQMsg == NULL)
                    {
                        return BGP4_FAILURE;
                    }

                    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
                    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_SNMP_PEER_DELETE_EVENT;
                    BGP4_INPUTQ_CXT (pQMsg) = u4Context;
                    Bgp4CopyAddrPrefixStruct (&
                                              (BGP4_INPUTQ_PEER_ADDR_INFO
                                               (pQMsg)),
                                              BGP4_PEER_REMOTE_ADDR_INFO
                                              (pPeer));
                    /* Send the peer entry to the bgp-task */
                    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
                    {
                        return BGP4_FAILURE;
                    }
                    break;
                default:
                    return BGP4_FAILURE;
            }
            break;
        case BGP4_PEER_EXT_REMOTE_AS_OBJECT:
            if (pPeer == NULL)
            {
                return BGP4_FAILURE;
            }

            if (BGP4_PEER_ASNO (pPeer) == (UINT4) i4SetVal)
            {
                return BGP4_SUCCESS;
            }
            if (BGP4_PEER_ADMIN_STATUS (pPeer) != BGP4_PEER_STOP)
            {
                return BGP4_FAILURE;
            }
            BGP4_PEER_ASNO (pPeer) = (UINT4) i4SetVal;
            BGP4_PEER_SHADOW_REMOTE_AS_NO(pPeer) = BGP4_PEER_ASNO(pPeer);

            /* check if Confed is configured  and mark, if peer 
             * is confed peer */
            if (BGP4_CONFED_ID (u4Context) != BGP4_INV_AS)
            {
                UINT4               u4Indx;

                for (u4Indx = 0; u4Indx < BGP4_CONFED_MAX_MEMBER_AS; u4Indx++)
                {
                    if (BGP4_PEER_ASNO (pPeer) ==
                        BGP4_CONFED_PEER (u4Context, u4Indx))
                    {
                        BGP4_CONFED_PEER_STATUS (pPeer) = BGP4_TRUE;
                        break;
                    }
                }
            }
	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                  (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u4RemoteASNo !=  BGP4_INV_AS) && 
                  (pPeer->pPeerGrp->u4RemoteASNo == BGP4_PEER_SHADOW_REMOTE_AS_NO(pPeer)))
               {
                  BGP4_PEER_SHADOW_REMOTE_AS_NO(pPeer) = BGP4_INV_AS;
               }
            }
            break;
        case BGP4_PEER_EXT_MULTIHOP_OBJECT:
            if (pPeer == NULL)
            {
                return BGP4_FAILURE;
            }

            u1EBGPMultiHop = BGP4_PEER_SHADOW_EBGP_MULTIHOP (pPeer);

            if (
#ifdef L2RED_WANTED
             (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                  (pPeer->pPeerGrp != NULL))
             {
                if((pPeer->pPeerGrp->u1EBGPMultiHop != BGP4_EBGP_MULTI_HOP_DISABLE) &&
                   (pPeer->pPeerGrp->u1EBGPMultiHop == BGP4_PEER_SHADOW_EBGP_MULTIHOP(pPeer)))
                {
                   BGP4_PEER_SHADOW_EBGP_MULTIHOP(pPeer) = BGP4_EBGP_MULTI_HOP_DISABLE;
                }
             }

            if (u1EBGPMultiHop == (UINT1) i4SetVal)
            {
                return BGP4_SUCCESS;
            }

            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                return BGP4_FAILURE;
            }

            BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
            BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_SNMP_EBGP_MULTIHOP_EVENT;
            BGP4_INPUTQ_CXT (pQMsg) = u4Context;
            Bgp4CopyAddrPrefixStruct (&(BGP4_INPUTQ_PEER_ADDR_INFO (pQMsg)),
                                      BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
            BGP4_PEER_EBGP_MULTIHOP (pPeer) = (UINT1) i4SetVal;
            BGP4_PEER_SHADOW_EBGP_MULTIHOP(pPeer) = BGP4_PEER_EBGP_MULTIHOP(pPeer);
            /* Send the peer entry to the bgp-task */
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                BGP4_PEER_EBGP_MULTIHOP (pPeer) = u1EBGPMultiHop;
                BGP4_PEER_SHADOW_EBGP_MULTIHOP(pPeer) = BGP4_PEER_EBGP_MULTIHOP(pPeer);
                return BGP4_FAILURE;
            }

            break;
        case BGP4_PEER_EXT_NEXTHOP_SELF_OBJECT:
            if (pPeer == NULL)
            {
                return BGP4_FAILURE;
            }
            BGP4_PEER_SELF_NEXTHOP (pPeer) = (UINT1) i4SetVal;
            BGP4_PEER_SHADOW_SELF_NEXTHOP(pPeer) = BGP4_PEER_SELF_NEXTHOP(pPeer);

            if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                  (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u1SelfNextHop != BGP4_DISABLE_NEXTHOP_SELF) && 
                  (pPeer->pPeerGrp->u1SelfNextHop == BGP4_PEER_SHADOW_SELF_NEXTHOP(pPeer)))
               {
                  BGP4_PEER_SHADOW_SELF_NEXTHOP(pPeer) = BGP4_DISABLE_NEXTHOP_SELF;
               }
            }
            break;
        case BGP4_PEER_EXT_HOPLIMIT_OBJECT:
            if (pPeer == NULL)
            {
                return BGP4_FAILURE;
            }
            BGP4_PEER_HOPLIMIT (pPeer) = (UINT1) i4SetVal;
            BGP4_PEER_SHADOW_HOP_LIMIT(pPeer) = BGP4_PEER_HOPLIMIT(pPeer);

	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                  (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u1HopLimit != BGP4_DEFAULT_HOPLIMIT) && 
                  (pPeer->pPeerGrp->u1HopLimit == BGP4_PEER_SHADOW_HOP_LIMIT(pPeer)))
               {
                  BGP4_PEER_SHADOW_HOP_LIMIT(pPeer) = BGP4_DEFAULT_HOPLIMIT;
               }
            }
            break;
        case BGP4_PEER_EXT_TCP_SENDBUF_OBJECT:
            if (pPeer == NULL)
            {
                return BGP4_FAILURE;
            }
            BGP4_PEER_SENDBUF (pPeer) = i4SetVal;
            BGP4_PEER_SHADOW_SEND_BUF(pPeer) = BGP4_PEER_SENDBUF(pPeer);

            if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                 (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u4SendBuf != BGP4_DEFAULT_SENDBUF) && 
                  (pPeer->pPeerGrp->u4SendBuf == BGP4_PEER_SHADOW_SEND_BUF(pPeer)))
               {
                  BGP4_PEER_SHADOW_SEND_BUF(pPeer) = BGP4_DEFAULT_SENDBUF;
               }
            }
            break;
        case BGP4_PEER_EXT_TCP_RECVBUF_OBJECT:
            if (pPeer == NULL)
            {
                return BGP4_FAILURE;
            }
            BGP4_PEER_RECVBUF (pPeer) = (UINT4) i4SetVal;
            BGP4_PEER_SHADOW_RECV_BUF(pPeer) = BGP4_PEER_RECVBUF(pPeer);

	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                  (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u4RecvBuf != BGP4_DEFAULT_RECVBUF) && 
                  (pPeer->pPeerGrp->u4RecvBuf == BGP4_PEER_SHADOW_RECV_BUF(pPeer)))
               {
                  BGP4_PEER_SHADOW_RECV_BUF(pPeer) = BGP4_DEFAULT_RECVBUF;
               }
            }
            break;
        case BGP4_PEER_EXT_RFL_CLIENT_OBJECT:
            if (pPeer == NULL)
            {
                return BGP4_FAILURE;
            }
            if (BGP4_GET_PEER_TYPE (u4Context, pPeer) == BGP4_EXTERNAL_PEER)
            {
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)), BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - External Peer. Reflector Client not enabled.\n");
                return BGP4_FAILURE;
            }

            /* Check if the reflector client event is pending or not. If
             * pending, then already reflector client event is in progress.
             * If flag set and the new status is NON_CLIENT & current
             * status is CLIENT or if new status is CLIENT & current status
             * is NON_CLIENT, no need to process. Else if flag is set and
             * current status & new status are same, then just reset the
             * flag, since no need to change the status after peer down
             * is complete.
             */
            if ((BGP4_GET_PEER_PEND_FLAG (pPeer) &
                 BGP4_PEER_RFL_CLIENT_PEND) == BGP4_PEER_RFL_CLIENT_PEND)
            {
                if (((BGP4_PEER_RFL_CLIENT (pPeer) == NON_CLIENT) &&
                     (i4SetVal == CLIENT)) ||
                    ((BGP4_PEER_RFL_CLIENT (pPeer) == CLIENT) &&
                     (i4SetVal == NON_CLIENT)))
                {
                    return BGP4_SUCCESS;
                }
                else if (((BGP4_PEER_RFL_CLIENT (pPeer) == CLIENT) &&
                          (i4SetVal == CLIENT)) ||
                         ((BGP4_PEER_RFL_CLIENT (pPeer) == NON_CLIENT) &&
                          (i4SetVal == NON_CLIENT)))
                {
                    BGP4_RESET_PEER_PEND_FLAG (pPeer,
                                               BGP4_PEER_RFL_CLIENT_PEND);
                    return BGP4_SUCCESS;
                }
            }

            /* No peer client pend flag set. If there is a change in 
             * client status then post a message to BGP task. */
            if (BGP4_PEER_RFL_CLIENT (pPeer) == i4SetVal)
            {
                return BGP4_SUCCESS;
            }

            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tUnable to allocate BGP Q Msg.\n");
                return BGP4_FAILURE;
            }

            BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
            /* Post message to BGP task to indicate this event. */
            BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_SNMP_RFL_PEER_TYPE_EVENT;
            BGP4_INPUTQ_CXT (pQMsg) = u4Context;
            Bgp4CopyAddrPrefixStruct (&(BGP4_INPUTQ_PEER_ADDR_INFO (pQMsg)),
                                      BGP4_PEER_REMOTE_ADDR_INFO (pPeer));

            /* Send the peer entry to the bgp-task */
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                return BGP4_FAILURE;
            }
             BGP4_PEER_SHADOW_RFL_CLIENT(pPeer) = (UINT1) i4SetVal;

	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                 (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u1RflClient != NON_CLIENT) && 
                  (pPeer->pPeerGrp->u1RflClient == BGP4_PEER_SHADOW_RFL_CLIENT(pPeer)))
               {
                  BGP4_PEER_SHADOW_RFL_CLIENT(pPeer) = NON_CLIENT;
               }
            }
            break;

        case BGP4_PEER_EXT_COMM_SEND_STATUS_OBJECT:
            if (pPeer == NULL)
            {
                return BGP4_FAILURE;
            }
            BGP4_PEER_COMM_SEND_STATUS (pPeer) =
                (UINT1) (i4SetVal - BGP4_DECREMENT_BY_ONE);
            BGP4_PEER_SHADOW_COMM_SEND_STATUS(pPeer) = BGP4_PEER_COMM_SEND_STATUS(pPeer);

	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                  (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u1CommSendStatus != BGP4_TRUE) && 
                  (pPeer->pPeerGrp->u1CommSendStatus == BGP4_PEER_SHADOW_COMM_SEND_STATUS(pPeer)))
               {
                  BGP4_PEER_SHADOW_COMM_SEND_STATUS(pPeer) = BGP4_TRUE;
               }
            }
            break;
        case BGP4_PEER_EXT_ECOMM_SEND_STATUS_OBJECT:
            if (pPeer == NULL)
            {
                return BGP4_FAILURE;
            }
            BGP4_PEER_ECOMM_SEND_STATUS (pPeer) =
                (UINT1) (i4SetVal - BGP4_DECREMENT_BY_ONE);
            BGP4_PEER_SHADOW_ECOMM_SEND_STATUS(pPeer) = BGP4_PEER_ECOMM_SEND_STATUS(pPeer);

	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS)  &&
#endif
                  (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u1ECommSendStatus != BGP4_TRUE) && 
                  (pPeer->pPeerGrp->u1ECommSendStatus == BGP4_PEER_SHADOW_ECOMM_SEND_STATUS(pPeer)))
               {
                  BGP4_PEER_SHADOW_ECOMM_SEND_STATUS(pPeer) = BGP4_TRUE;
               }
            }
            break;
        case BGP4_PEER_EXT_PASSIVE_SET_OBJECT:
            if (pPeer == NULL)
            {
                return BGP4_FAILURE;
            }
            BGP4_PEER_CONN_PASSIVE (pPeer) = (UINT1) i4SetVal;
            BGP4_PEER_SHADOW_PASSIVE(pPeer) = BGP4_PEER_CONN_PASSIVE(pPeer);
            if (
#ifdef L2RED_WANTED 
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                  (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u1Passive != BGP4_FALSE) && 
                  (pPeer->pPeerGrp->u1Passive == BGP4_PEER_SHADOW_PASSIVE(pPeer)))
               {
                  BGP4_PEER_SHADOW_PASSIVE(pPeer) = BGP4_FALSE;
               }
            }
            break;

        case BGP4_PEER_EXT_DEFAULT_ROUTE_OBJECT:
            if (pPeer == NULL)
            {
                return BGP4_FAILURE;
            }
            if ((BGP4_PEER_ADMIN_STATUS (pPeer) == BGP4_PEER_STOP) ||
                (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_DOWN))
            {
                BGP4_PEER_DEF_ROUTE_ORIG_STATUS (pPeer) = (UINT1) i4SetVal;
                BGP4_PEER_SHADOW_DFT_ROUTE_ORIG_STATUS(pPeer) = BGP4_PEER_DEF_ROUTE_ORIG_STATUS(pPeer);
                return BGP4_SUCCESS;
            }

            if (BGP4_PEER_STATE (pPeer) != BGP4_ESTABLISHED_STATE)
            {
                /* Peer is in non-Established State. */
                BGP4_PEER_DEF_ROUTE_ORIG_STATUS (pPeer) = (UINT1) i4SetVal;
                BGP4_PEER_SHADOW_DFT_ROUTE_ORIG_STATUS(pPeer) = BGP4_PEER_DEF_ROUTE_ORIG_STATUS(pPeer);
                return BGP4_SUCCESS;
            }

            if ((BGP4_GET_PEER_CURRENT_STATE (pPeer) ==
                 BGP4_PEER_INIT_INPROGRESS) &&
                (i4SetVal == BGP4_DEF_ROUTE_ORIG_ENABLE))
            {
                /* Peer is going through init and we are trying to enable the
                 * default information origination command. In this case, just
                 * set the peer origination status as enable, the default route
                 * will be send after the normal peer init is complete. */
                BGP4_PEER_DEF_ROUTE_ORIG_STATUS (pPeer) = (UINT1) i4SetVal;
                BGP4_PEER_SHADOW_DFT_ROUTE_ORIG_STATUS(pPeer) = BGP4_PEER_DEF_ROUTE_ORIG_STATUS(pPeer);
                return BGP4_SUCCESS;
            }

            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                          BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                          "\tMemory Allocation for posting BGP Peer Default "
                          "Route Origination to BGP Queue FAILED!!!\n");
                return BGP4_FAILURE;
            }

            /* Post message to BGP task to indicate this event. */
            BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_PEER_DEF_ROUTE_ORIG_CHG_EVENT;
            BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
            BGP4_INPUTQ_CXT (pQMsg) = u4Context;
            Bgp4CopyAddrPrefixStruct
                ((&(BGP4_INPUTQ_DEF_RT_PEER_ADDR_INFO (pQMsg))),
                 BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
            BGP4_INPUTQ_DEF_RT_STATUS (pQMsg) = (UINT1) i4SetVal;

            /* Send the peer entry to the bgp-task */
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                return BGP4_FAILURE;
            }
            BGP4_PEER_SHADOW_DFT_ROUTE_ORIG_STATUS(pPeer) = (UINT1) i4SetVal;

	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                  (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u1DftRouteOrigStatus != BGP4_DEF_ROUTE_ORIG_DISABLE) && 
                  (pPeer->pPeerGrp->u1DftRouteOrigStatus == BGP4_PEER_SHADOW_DFT_ROUTE_ORIG_STATUS(pPeer)))
               {
                  BGP4_PEER_SHADOW_DFT_ROUTE_ORIG_STATUS(pPeer) = BGP4_DEF_ROUTE_ORIG_DISABLE;
               }
            }
            break;
        case BGP4_PEER_EXT_ACTIVATE_MP_CAP_OBJECT:
            if (pPeer == NULL)
            {
                return BGP4_FAILURE;
            }
            switch (i4SetVal)
            {
                case CAP_NEG_IPV4_UNI_MASK:
                    if (BGP4_IPV4_AFI_FLAG (u4Context) == BGP4_FALSE)
                    {
                        /* IPv4 Unicast not Supported */
                        return BGP4_FAILURE;
                    }

                    if (((BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                          CAP_NEG_IPV4_UNI_MASK) == CAP_NEG_IPV4_UNI_MASK) &&
                        ((BGP4_PEER_NEG_CAP_MASK (pPeer) &
                          CAP_NEG_ROUTE_REFRESH_MASK) ==
                         CAP_NEG_ROUTE_REFRESH_MASK))
                    {
                        /* Already the capabilities are negotiated. */
                        return BGP4_SUCCESS;
                    }

                    /* Enable IPv4 Unicast Capability */
                    i4RetVal =
                        Bgp4SnmphEnableMPCapability (u4Context,
                                                     BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeer),
                                                     BGP4_INET_AFI_IPV4,
                                                     BGP4_INET_SAFI_UNICAST);
                    if (i4RetVal == SNMP_FAILURE)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                  BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                                  BGP4_MOD_NAME,
                                  "\tUnable to activate ipv4-unicast "
                                  "capability for peer\n");
                        return (BGP4_FAILURE);
                    }
#ifdef EVPN_WANTED
                    pPeer->b1IsMPIpv4Enabled = BGP4_TRUE;
#endif
                    /* Enable Route Refresh Capability */
                    Bgp4SnmphRtRefCapabilityEnable (u4Context,
                                                    BGP4_PEER_REMOTE_ADDR_INFO
                                                    (pPeer));
                    /* Enable GR capability */
                    Bgp4SnmphGrCapabilityEnable (u4Context,
                                                 BGP4_PEER_REMOTE_ADDR_INFO(pPeer));
                    break;
#ifdef BGP4_IPV6_WANTED
                case CAP_NEG_IPV6_UNI_MASK:
                    if (BGP4_IPV6_AFI_FLAG (u4Context) == BGP4_FALSE)
                    {
                        /* IPv6 Unicast not Supported */
                        return BGP4_FAILURE;
                    }

                    if (((BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                          CAP_NEG_IPV6_UNI_MASK) == CAP_NEG_IPV6_UNI_MASK) &&
                        ((BGP4_PEER_NEG_CAP_MASK (pPeer) &
                          CAP_NEG_ROUTE_REFRESH_MASK) ==
                         CAP_NEG_ROUTE_REFRESH_MASK))
                    {
                        /* Already the capabilities are negotiated. */
                        return BGP4_SUCCESS;
                    }

                    /* Enable IPv6 Unicast Capability */
                    i4RetVal =
                        Bgp4SnmphEnableMPCapability (u4Context,
                                                     BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeer),
                                                     BGP4_INET_AFI_IPV6,
                                                     BGP4_INET_SAFI_UNICAST);
                    if (i4RetVal == SNMP_FAILURE)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                  BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                                  BGP4_MOD_NAME,
                                  "\tUnable to activate ipv6-unicast "
                                  "capability for peer\n");
                        return (BGP4_FAILURE);
                    }

                    /* Enable Route Refresh Capability */
                    Bgp4SnmphRtRefCapabilityEnable (u4Context,
                                                    BGP4_PEER_REMOTE_ADDR_INFO
                                                    (pPeer));
                    /* Enable GR capability */
                    Bgp4SnmphGrCapabilityEnable (u4Context,
                                                 BGP4_PEER_REMOTE_ADDR_INFO(pPeer));

                    break;
#endif
#ifdef L3VPN
                case CAP_NEG_VPN4_UNI_MASK:
                    if (BGP4_VPNV4_AFI_FLAG == BGP4_FALSE)
                    {
                        /* IPv4 Unicast not Supported */
                        return BGP4_FAILURE;
                    }

                    if (((BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                          CAP_NEG_VPN4_UNI_MASK) == CAP_NEG_VPN4_UNI_MASK) &&
                        ((BGP4_PEER_NEG_CAP_MASK (pPeer) &
                          CAP_NEG_ROUTE_REFRESH_MASK) ==
                         CAP_NEG_ROUTE_REFRESH_MASK))
                    {
                        /* Already the capabilities are negotiated. */
                        return BGP4_SUCCESS;
                    }

                    /* Enable Vpnv4 Unicast Capability */
                    i4RetVal =
                        Bgp4SnmphEnableMPCapability (u4Context,
                                                     BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeer),
                                                     BGP4_INET_AFI_IPV4,
                                                     BGP4_INET_SAFI_VPNV4_UNICAST);
                    if (i4RetVal == SNMP_FAILURE)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                  BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                                  BGP4_MOD_NAME,
                                  "\tUnable to activate vpnv4-unicast "
                                  "capability for peer\n");
                        return (BGP4_FAILURE);
                    }

                    /* Enable Route Refresh Capability */
                    Bgp4SnmphRtRefCapabilityEnable (u4Context,
                                                    BGP4_PEER_REMOTE_ADDR_INFO
                                                    (pPeer));
                    break;
#endif
#ifdef VPLSADS_WANTED
                case CAP_NEG_L2VPN_VPLS_MASK:
                    if (BGP4_L2VPN_AFI_FLAG (u4Context) == BGP4_FALSE)
                    {
                        /* L2VPN_VPLS not Supported */
                        return BGP4_FAILURE;
                    }

                    if (((BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                          CAP_NEG_L2VPN_VPLS_MASK) == CAP_NEG_L2VPN_VPLS_MASK)
                        &&
                        ((BGP4_PEER_NEG_CAP_MASK (pPeer) &
                          CAP_NEG_ROUTE_REFRESH_MASK) ==
                         CAP_NEG_ROUTE_REFRESH_MASK))
                    {
                        /* Already the capabilities are negotiated. */
                        return BGP4_SUCCESS;
                    }

                    /* Enable IPv4 Unicast Capability */
                    i4RetVal =
                        Bgp4SnmphEnableMPCapability (u4Context,
                                                     BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeer),
                                                     BGP4_INET_AFI_L2VPN,
                                                     BGP4_INET_SAFI_VPLS);
                    if (i4RetVal == SNMP_FAILURE)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                  BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                                  BGP4_MOD_NAME,
                                  "\tUnable to activate l2vpn-vpls "
                                  "capability for peer\n");
                        return (BGP4_FAILURE);
                    }

                    /* Enable Route Refresh Capability */
                    Bgp4SnmphRtRefCapabilityEnable (u4Context,
                                                    BGP4_PEER_REMOTE_ADDR_INFO
                                                    (pPeer));

                    break;
#endif /*VPLSADS_WANTED */
#ifdef EVPN_WANTED
                case CAP_NEG_L2VPN_EVPN_MASK:
                    if (BGP4_EVPN_AFI_FLAG == BGP4_FALSE)
                    {
                        /* EVPN not Supported */
                        return BGP4_FAILURE;
                    }

                    if (((BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                          CAP_NEG_L2VPN_EVPN_MASK) == CAP_NEG_L2VPN_EVPN_MASK)
                        &&
                        ((BGP4_PEER_NEG_CAP_MASK (pPeer) &
                          CAP_NEG_ROUTE_REFRESH_MASK) ==
                         CAP_NEG_ROUTE_REFRESH_MASK))
                    {
                        /* Already the capabilities are negotiated. */
                        return BGP4_SUCCESS;
                    }
                    /* Enable L2VPN EVPN MP Capability */
                    i4RetVal =
                        Bgp4SnmphEnableMPCapability (u4Context,
                                                     BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeer),
                                                     BGP4_INET_AFI_L2VPN,
                                                     BGP4_INET_SAFI_EVPN);
                    if (i4RetVal == SNMP_FAILURE)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                  BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                                  BGP4_MOD_NAME,
                                  "\tUnable to activate evpn "
                                  "capability for peer\n");
                        return (BGP4_FAILURE);
                    }

                    /* Enable Route Refresh Capability */
                    Bgp4SnmphRtRefCapabilityEnable (u4Context,
                                                    BGP4_PEER_REMOTE_ADDR_INFO
                                                    (pPeer));

                    break;
#endif
        default:
            return BGP4_FAILURE;
            }
            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                          BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                          "\tMemory Allocation for posting BGP Peer MP "
                          "CAP Activation to BGP Queue FAILED!!!\n");
                return BGP4_FAILURE;
            }

            /* Post message to BGP task to indicate this event. */
            if ((BGP4_PEER_ADMIN_STATUS (pPeer) != BGP4_PEER_STOP) &&
                (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_UP))
            {
                BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_CLI_CLEAR_IP_BGP_EVENT;
                BGP4_INPUTQ_CXT (pQMsg) = u4Context;
                Bgp4CopyAddrPrefixStruct
                    ((&(BGP4_INPUTQ_PEER_ADDR_INFO (pQMsg))),
                     BGP4_PEER_REMOTE_ADDR_INFO (pPeer));

                /* Send the peer entry to the bgp-task */
                if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
                {
                    return BGP4_FAILURE;
                }
            }
            else
            {
                BGP4_QMSG_FREE (pQMsg);
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                          BGP4_MOD_NAME, "\tBGP Admin status is stopped\n");
            }
            break;

        case BGP4_PEER_EXT_DEACTIVATE_MP_CAP_OBJECT:
            if (pPeer == NULL)
            {
                return BGP4_FAILURE;
            }
            switch (i4SetVal)
            {
                case CAP_NEG_IPV4_UNI_MASK:
                    if (BGP4_IPV4_AFI_FLAG (u4Context) == BGP4_FALSE)
                    {
                        /* IPv4 Unicast not Supported */
                        return BGP4_FAILURE;
                    }

                    if ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                         CAP_NEG_IPV4_UNI_MASK) == CAP_NEG_IPV4_UNI_MASK)
                    {
                        /* Already the capabilities are negotiated. */
                        u1IsDeactive = BGP4_TRUE;
                    }

                    /* Disable IPV4 MP Capability */
                    i4RetVal =
                        Bgp4SnmphDisableMPCapability (u4Context,
                                                      BGP4_PEER_REMOTE_ADDR_INFO
                                                      (pPeer),
                                                      BGP4_INET_AFI_IPV4,
                                                      BGP4_INET_SAFI_UNICAST);
                    if (i4RetVal == SNMP_FAILURE)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                  BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                                  BGP4_MOD_NAME,
                                  "\tUnable to de-activate ipv4-unicast "
                                  "capability for peer\n");
                        return (BGP4_FAILURE);
                    }
#ifdef BGP4_IPV6_WANTED
                    if (Bgp4SnmphGetMPCapSupportStatus
                        (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                         BGP4_INET_AFI_IPV6,
                         BGP4_INET_SAFI_UNICAST) == BGP4_FALSE)
                    {
                        /* Disable Route Refresh Capability */
                        Bgp4SnmphRtRefCapabilityDisable
                            (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
                    }
#else
                    /* Disable Route Refresh Capability */
                    Bgp4SnmphRtRefCapabilityDisable (u4Context,
                                                     BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeer));
#endif
                    /* Disable GR capability */
                    Bgp4SnmphGrCapabilityDisable (BGP4_PEER_REMOTE_ADDR_INFO (pPeer));


                    break;
#ifdef BGP4_IPV6_WANTED
                case CAP_NEG_IPV6_UNI_MASK:
                    if (BGP4_IPV6_AFI_FLAG (u4Context) == BGP4_FALSE)
                    {
                        /* IPv6 Unicast not Supported */
                        return BGP4_FAILURE;
                    }

                    if ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                         CAP_NEG_IPV6_UNI_MASK) == CAP_NEG_IPV6_UNI_MASK)
                    {
                        /* Already the capabilities are negotiated. */
                        u1IsDeactive = BGP4_TRUE;
                    }

                    /* Disable IPV6 MP Capability */
                    i4RetVal =
                        Bgp4SnmphDisableMPCapability (u4Context,
                                                      BGP4_PEER_REMOTE_ADDR_INFO
                                                      (pPeer),
                                                      BGP4_INET_AFI_IPV6,
                                                      BGP4_INET_SAFI_UNICAST);
                    if (i4RetVal == SNMP_FAILURE)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                  BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                                  BGP4_MOD_NAME,
                                  "\tUnable to de-activate ipv6-unicast "
                                  "capability for peer\n");
                        return (BGP4_FAILURE);
                    }
                    if (Bgp4SnmphGetMPCapSupportStatus
                        (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                         BGP4_INET_AFI_IPV4,
                         BGP4_INET_SAFI_UNICAST) == BGP4_FALSE)
                    {
                        Bgp4SnmphRtRefCapabilityDisable
                            (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
                    }

                    /* Disable GR capability */
                    Bgp4SnmphGrCapabilityDisable (BGP4_PEER_REMOTE_ADDR_INFO (pPeer));

                    break;
#endif
#ifdef L3VPN
                case CAP_NEG_VPN4_UNI_MASK:
                    if (BGP4_VPNV4_AFI_FLAG == BGP4_FALSE)
                    {
                        /* IPv4 Unicast not Supported */
                        return BGP4_FAILURE;
                    }

                    if ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                         CAP_NEG_VPN4_UNI_MASK) == CAP_NEG_VPN4_UNI_MASK)
                    {
                        /* Already the capabilities are negotiated. */
                        u1IsDeactive = BGP4_TRUE;
                    }

                    /* Disable VPNv4 MP Capability */
                    i4RetVal =
                        Bgp4SnmphDisableMPCapability (u4Context,
                                                      BGP4_PEER_REMOTE_ADDR_INFO
                                                      (pPeer),
                                                      BGP4_INET_AFI_IPV4,
                                                      BGP4_INET_SAFI_VPNV4_UNICAST);
                    if (i4RetVal == SNMP_FAILURE)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                  BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                                  BGP4_MOD_NAME,
                                  "\tUnable to de-activate vpnv4-unicast "
                                  "capability for peer\n");
                        return (BGP4_FAILURE);
                    }
                    if (Bgp4SnmphGetMPCapSupportStatus
                        (u4Context,
                         BGP4_PEER_REMOTE_ADDR_INFO (pPeer), BGP4_INET_AFI_IPV4,
                         BGP4_INET_SAFI_VPNV4_UNICAST) == BGP4_FALSE)
                    {
                        Bgp4SnmphRtRefCapabilityDisable
                            (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
                    }
                    break;
#endif
#ifdef EVPN_WANTED
                case CAP_NEG_L2VPN_EVPN_MASK:
                    if (BGP4_EVPN_AFI_FLAG == BGP4_FALSE)
                    {
                        /* IPv4 Unicast not Supported */
                        return BGP4_FAILURE;
                    }

                    if ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                         CAP_NEG_L2VPN_EVPN_MASK) == CAP_NEG_L2VPN_EVPN_MASK)
                    {
                        /* Already the capabilities are negotiated. */
                        u1IsDeactive = BGP4_TRUE;
                    }

                    /* Disable L2VPN EVPN MP Capability */
                    i4RetVal =
                        Bgp4SnmphDisableMPCapability (u4Context,
                                                      BGP4_PEER_REMOTE_ADDR_INFO
                                                      (pPeer),
                                                      BGP4_INET_AFI_L2VPN,
                                                      BGP4_INET_SAFI_EVPN);
                    if (i4RetVal == SNMP_FAILURE)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                  BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                                  BGP4_MOD_NAME,
                                  "\tUnable to de-activate Evpn-unicast "
                                  "capability for peer\n");
                        return (BGP4_FAILURE);
                    }
                    if (Bgp4SnmphGetMPCapSupportStatus
                        (u4Context,
                         BGP4_PEER_REMOTE_ADDR_INFO (pPeer), BGP4_INET_AFI_L2VPN,
                         BGP4_INET_SAFI_EVPN) == BGP4_FALSE)
                    {
                        Bgp4SnmphRtRefCapabilityDisable
                            (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
                    }
                    break;
#endif
#ifdef VPLSADS_WANTED
                case CAP_NEG_L2VPN_VPLS_MASK:
                    if (BGP4_L2VPN_AFI_FLAG (u4Context) == BGP4_FALSE)
                    {
                        /* L2VPN VPLS not Supported */
                        return BGP4_FAILURE;
                    }

                    if ((BGP4_PEER_NEG_ASAFI_MASK (pPeer) &
                         CAP_NEG_L2VPN_VPLS_MASK) == CAP_NEG_L2VPN_VPLS_MASK)
                    {
                        /* Already the capabilities are negotiated. */
                        u1IsDeactive = BGP4_TRUE;
                    }

                    /* Disable L2VPN-VPLS MP Capability */
                    i4RetVal =
                        Bgp4SnmphDisableMPCapability (u4Context,
                                                      BGP4_PEER_REMOTE_ADDR_INFO
                                                      (pPeer),
                                                      BGP4_INET_AFI_L2VPN,
                                                      BGP4_INET_SAFI_VPLS);
                    if (i4RetVal == SNMP_FAILURE)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                                  BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                                  BGP4_MOD_NAME,
                                  "\tUnable to de-activate l2vpn-vpls "
                                  "capability for peer\n");
                        return (BGP4_FAILURE);
                    }

                    if ((Bgp4SnmphGetMPCapSupportStatus
                         (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                          BGP4_INET_AFI_IPV4,
                          BGP4_INET_SAFI_UNICAST) == BGP4_FALSE)
#ifdef BGP4_IPV6_WANTED
                        && (Bgp4SnmphGetMPCapSupportStatus
                            (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                             BGP4_INET_AFI_IPV6,
                             BGP4_INET_SAFI_UNICAST) == BGP4_FALSE)
#endif
                        )
                    {
                        Bgp4SnmphRtRefCapabilityDisable
                            (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
                    }

                    break;
#endif
        default:
            return BGP4_FAILURE;
            }
            if (u1IsDeactive == BGP4_TRUE)
            {
                pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
                if (pQMsg == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC | BGP4_OS_RESOURCE_TRC,
                              BGP4_MOD_NAME,
                              "\tMemory Allocation for posting BGP Peer MP "
                              "CAP DeActivation to BGP Queue FAILED!!!\n");
                    return BGP4_FAILURE;
                }

                /* Post message to BGP task to indicate this event. */
                BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_CLI_CLEAR_IP_BGP_EVENT;
                BGP4_INPUTQ_CXT (pQMsg) = u4Context;
                Bgp4CopyAddrPrefixStruct
                    ((&(BGP4_INPUTQ_PEER_ADDR_INFO (pQMsg))),
                     BGP4_PEER_REMOTE_ADDR_INFO (pPeer));

                /* Send the peer entry to the bgp-task */
                if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
                {
                    return BGP4_FAILURE;
                }
            }
            break;
#ifdef L3VPN
        case BGP4_PEER_EXT_VPN_CE_RT_ADVT_OBJECT:
            if (pPeer == NULL)
            {
                return BGP4_FAILURE;
            }
            BGP4_VPN4_PEER_CERT_STATUS (pPeer) = (UINT1) i4SetVal;
            break;
#endif
        case BGP4_PEER_EXT_OVERRIDE_CAPABILITY_OBJECT:
            if (pPeer == NULL)
            {
                return BGP4_FAILURE;
            }
            BGP4_PEER_OVERRIDE_CAPABILITY (pPeer) = (UINT1) i4SetVal;
            BGP4_PEER_SHADOW_OVERRIDE_CAP(pPeer) = BGP4_PEER_OVERRIDE_CAPABILITY(pPeer);

	    if (
#ifdef L2RED_WANTED
            (BGP4_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_IN_PROGRESS) &&
#endif
                  (pPeer->pPeerGrp != NULL))
            {
               if((pPeer->pPeerGrp->u1OverrideCap != BGP4_DISABLE_OVERRIDE_CAPABILITY) && 
                  (pPeer->pPeerGrp->u1OverrideCap == BGP4_PEER_SHADOW_OVERRIDE_CAP(pPeer)))
               {
                  BGP4_PEER_SHADOW_OVERRIDE_CAP(pPeer) = BGP4_DISABLE_OVERRIDE_CAPABILITY;
               }
            }
            break;
        default:
            return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

INT4
Bgp4TestFsBgp4PeerExtTableIntegerObject (UINT4 u4Context,
                                         tAddrPrefix PeerAddress,
                                         INT4 i4ObjectType, INT4 i4TestVal,
                                         UINT4 *pu4ErrorCode)
{
    tBgp4PeerEntry     *pPeer = NULL;
    UINT4               u4ContextId = u4Context;
    UINT1               u1ValidStatus;
#ifdef BGP4_IPV6_WANTED
    UINT4		u4Port =0;
#endif

    if (BGP4_LOCAL_AS_NO (u4Context) == BGP4_INV_AS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_AS_NOT_CONFIG_ERR);
        return BGP4_FAILURE;
    }

    /* Validate the Peer Remote Address */
    u1ValidStatus = Bgp4IsValidAddress (PeerAddress, BGP4_FALSE);
    if (u1ValidStatus != BGP4_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
        return BGP4_FAILURE;
    }

    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress))
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4BgpPeerRemoteAddr;

            PTR_FETCH_4 (u4BgpPeerRemoteAddr,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddress));
            if (u4BgpPeerRemoteAddr == BGP4_LOCAL_BGP_ID (u4Context))
            {
                CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return BGP4_FAILURE;
            }
            /* Reject Peer Remote Address, if same as local address */
            if (NetIpv4IfIsOurAddressInCxt (u4ContextId, 
                        u4BgpPeerRemoteAddr) == IP_SUCCESS)
                {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
                    return (BGP4_FAILURE);
                }
            }
            break;
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
        {
            /* Reject Peer Remote Address, if same as local address */
            if (NetIpv6IsOurAddressInCxt (u4Context,  
                        (tIp6Addr *) BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddress),
                        &u4Port) == NETIPV6_SUCCESS)
            {
                CLI_SET_ERR (CLI_BGP4_INVALID_PEER_ADDRESS_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (BGP4_FAILURE);
            }
        }
            break;
#endif
        default:
            return (BGP4_FAILURE);
    }

    switch (i4ObjectType)
    {
        case BGP4_PEER_EXT_CONF_OBJECT:
            pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
            if ((i4TestVal == BGP4_PEER_CREATE) && (pPeer != NULL)) 
            {
                /* Peer already present. If peer  delete_pending
                 * flag is set, then create operation fails */
                if ((BGP4_GET_PEER_PEND_FLAG (pPeer) &
                     BGP4_PEER_DELETE_PENDING) == BGP4_PEER_DELETE_PENDING)
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    CLI_SET_ERR (CLI_BGP4_PEER_DELETE_PENDING_ERR);
                    return BGP4_FAILURE;
                }
            }
            if ((i4TestVal == BGP4_PEER_CREATE) ||
                (i4TestVal == BGP4_PEER_DELETE))
            {
                return BGP4_SUCCESS;
            }
            break;
        case BGP4_PEER_EXT_REMOTE_AS_OBJECT:
            if (((UINT4) i4TestVal != BGP4_INV_AS))
            {
                return BGP4_SUCCESS;
            }
            break;
        case BGP4_PEER_EXT_MULTIHOP_OBJECT:
            if ((i4TestVal == BGP4_EBGP_MULTI_HOP_DISABLE) ||
                (i4TestVal == BGP4_EBGP_MULTI_HOP_ENABLE))
            {
                return BGP4_SUCCESS;
            }
            break;
        case BGP4_PEER_EXT_NEXTHOP_SELF_OBJECT:
            if ((i4TestVal == BGP4_ENABLE_NEXTHOP_SELF) ||
                (i4TestVal == BGP4_DISABLE_NEXTHOP_SELF))
            {
                return BGP4_SUCCESS;
            }
            break;
        case BGP4_PEER_EXT_HOPLIMIT_OBJECT:
            if ((i4TestVal >= BGP4_MIN_HOPLIMIT) &&
                (i4TestVal <= BGP4_MAX_HOPLIMIT))
            {
                return BGP4_SUCCESS;
            }
            break;
        case BGP4_PEER_EXT_TCP_SENDBUF_OBJECT:
            if ((i4TestVal >= BGP4_MIN_SENDBUFSIZE) &&
                (i4TestVal <= BGP4_MAX_SENDBUFSIZE))
            {
                return BGP4_SUCCESS;
            }
            break;
        case BGP4_PEER_EXT_TCP_RECVBUF_OBJECT:
            if ((i4TestVal >= BGP4_MIN_RECVBUFSIZE) &&
                (i4TestVal <= BGP4_MAX_RECVBUFSIZE))
            {
                return BGP4_SUCCESS;
            }
            break;
        case BGP4_PEER_EXT_RFL_CLIENT_OBJECT:
            pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
            if (pPeer == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_BGP4_PEER_NOT_ENABLED);
                return BGP4_FAILURE;
            }
            if ((i4TestVal == NON_CLIENT) || (i4TestVal == CLIENT))
            {
                if (BGP4_GET_PEER_TYPE (u4Context, pPeer) == BGP4_EXTERNAL_PEER)
                {
                    BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeer)),
                              BGP4_TRC_FLAG,
                              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                              BGP4_MOD_NAME,
                              "\tERROR - External Peer. Reflector Client not enabled.\n");
                    CLI_SET_ERR (CLI_BGP4_INVALID_RFL_PEER_ENTRY);
                    return BGP4_FAILURE;
                }
                return BGP4_SUCCESS;
            }
            break;
        case BGP4_PEER_EXT_COMM_SEND_STATUS_OBJECT:
            if ((i4TestVal == COMM_PEER_SEND + BGP4_INCREMENT_BY_ONE) ||
                (i4TestVal == COMM_PEER_DO_NOT_SEND + BGP4_INCREMENT_BY_ONE))
            {
                return BGP4_SUCCESS;
            }
            break;
        case BGP4_PEER_EXT_ECOMM_SEND_STATUS_OBJECT:
            if ((i4TestVal == EXT_COMM_PEER_SEND + BGP4_INCREMENT_BY_ONE) ||
                (i4TestVal == EXT_COMM_PEER_DO_NOT_SEND +
                 BGP4_INCREMENT_BY_ONE))
            {
                return BGP4_SUCCESS;
            }
            break;
        case BGP4_PEER_EXT_PASSIVE_SET_OBJECT:
            if ((i4TestVal == BGP4_TRUE) || (i4TestVal == BGP4_FALSE))
            {
                return BGP4_SUCCESS;
            }
            break;

        case BGP4_PEER_EXT_DEFAULT_ROUTE_OBJECT:
            if ((i4TestVal == BGP4_DEF_ROUTE_ORIG_ENABLE) ||
                (i4TestVal == BGP4_DEF_ROUTE_ORIG_DISABLE))
            {
                return BGP4_SUCCESS;
            }
            break;
        case BGP4_PEER_EXT_ACTIVATE_MP_CAP_OBJECT:
        case BGP4_PEER_EXT_DEACTIVATE_MP_CAP_OBJECT:
#ifdef L3VPN
            pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
            if (pPeer == NULL)
            {
                CLI_SET_ERR (CLI_BGP4_PEER_NOT_ENABLED);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return BGP4_FAILURE;
            }
            /* For CE peer, in this release, only <ipv4, unicast> 
             * config is allowed
             */
            if ((BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_CE_PEER) &&
                (i4TestVal != CAP_NEG_IPV4_UNI_MASK))
            {
                return BGP4_FAILURE;
            }
#endif

#ifdef BGP4_IPV6_WANTED
#ifdef L3VPN
#ifdef VPLSADS_WANTED
            if ((i4TestVal == CAP_NEG_IPV4_UNI_MASK) ||
                (i4TestVal == CAP_NEG_IPV6_UNI_MASK) ||
                (i4TestVal == CAP_NEG_VPN4_UNI_MASK) ||
                (i4TestVal == CAP_NEG_L2VPN_VPLS_MASK))
#else
            if ((i4TestVal == CAP_NEG_IPV4_UNI_MASK) ||
                (i4TestVal == CAP_NEG_IPV6_UNI_MASK) ||
                (i4TestVal == CAP_NEG_VPN4_UNI_MASK))
#endif
#else /*Else of L3VPN */
#ifdef VPLSADS_WANTED
            if ((i4TestVal == CAP_NEG_IPV4_UNI_MASK) ||
                (i4TestVal == CAP_NEG_IPV6_UNI_MASK) ||
                (i4TestVal == CAP_NEG_L2VPN_VPLS_MASK))
#else
            if ((i4TestVal == CAP_NEG_IPV4_UNI_MASK) ||
                (i4TestVal == CAP_NEG_IPV6_UNI_MASK))
#endif
#endif /*L3VPN */
#else /*Else of BGP4_IPV6_WANTED */
#ifdef L3VPN
#ifdef VPLSADS_WANTED
            if ((i4TestVal == CAP_NEG_IPV4_UNI_MASK) ||
                (i4TestVal == CAP_NEG_VPN4_UNI_MASK) ||
                (i4TestVal == CAP_NEG_L2VPN_VPLS_MASK))
#else
            if ((i4TestVal == CAP_NEG_IPV4_UNI_MASK) ||
                (i4TestVal == CAP_NEG_VPN4_UNI_MASK))
#endif
#else /*Else of L3VPN */
#ifdef VPLSADS_WANTED
            if ((i4TestVal == CAP_NEG_IPV4_UNI_MASK) ||
                (i4TestVal == CAP_NEG_L2VPN_VPLS_MASK))
#else
            if (i4TestVal == CAP_NEG_IPV4_UNI_MASK)
#endif
#endif /*L3VPN */
#endif /*BGP4_IPV6_WANTED */
            {
                return BGP4_SUCCESS;
            }
#ifdef EVPN_WANTED
            if (i4TestVal == CAP_NEG_L2VPN_EVPN_MASK)
            {
                return BGP4_SUCCESS;
            }
#endif
            break;
#ifdef L3VPN
        case BGP4_PEER_EXT_VPN_CE_RT_ADVT_OBJECT:
            pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
            if (pPeer == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_BGP4_PEER_NOT_ENABLED);
                return BGP4_FAILURE;
            }
            if ((BGP4_VPN4_PEER_ROLE (pPeer) == BGP4_VPN4_CE_PEER) &&
                ((i4TestVal == BGP4_VPN4_CE_RT_SEND) ||
                 (i4TestVal == BGP4_VPN4_CE_RT_DONOTSEND)))
            {
                return BGP4_SUCCESS;
            }
            break;
#endif
        case BGP4_PEER_EXT_OVERRIDE_CAPABILITY_OBJECT:
            if ((i4TestVal == BGP4_ENABLE_OVERRIDE_CAPABILITY) ||
                (i4TestVal == BGP4_DISABLE_OVERRIDE_CAPABILITY))
            {
                return BGP4_SUCCESS;
            }
            break;

        default:
            return BGP4_FAILURE;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return BGP4_FAILURE;
}

INT4
Bgp4Ipv4GetFirstFsbgp4ImportTableIndex (tNetAddress * pRoutePrefix,
                                        INT4 *pi4Protocol,
                                        tAddrPrefix * pNexthop,
                                        INT4 *pi4IfIndex, INT4 *pi4Metric)
{
    VOID               *pRibNode = NULL;
    tRouteProfile      *pFndRtProfile = NULL;
    tBgp4Info          *pBgp4Info = NULL;
    UINT4               u4Context = 0;

    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return BGP4_FAILURE;
    }

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    Bgp4GetRibNode (u4Context, CAP_MP_IPV4_UNICAST, &pRibNode);
    if (pRibNode == NULL)
    {
        bgp4RibUnlock ();
        return BGP4_FAILURE;
    }
    pFndRtProfile = Bgp4RibhGetFirstImportAsafiRtEntry (CAP_MP_IPV4_UNICAST,
                                                        &pRibNode);
    if (pFndRtProfile == NULL)
    {
        bgp4RibUnlock ();
        return BGP4_FAILURE;
    }
    else
    {
        Bgp4CopyNetAddressStruct (pRoutePrefix,
                                  BGP4_RT_NET_ADDRESS_INFO (pFndRtProfile));
        *pi4Protocol = BGP4_RT_PROTOCOL (pFndRtProfile);
        pBgp4Info = BGP4_RT_BGP_INFO (pFndRtProfile);
        Bgp4CopyAddrPrefixStruct (pNexthop, BGP4_INFO_NEXTHOP_INFO (pBgp4Info));
        *pi4IfIndex = (INT4)BGP4_RT_GET_RT_IF_INDEX (pFndRtProfile) +
            BGP4_INCREMENT_BY_ONE;
        *pi4Metric = (INT4) BGP4_INFO_RCVD_MED (pBgp4Info);
    }
    bgp4RibUnlock ();
    return BGP4_SUCCESS;
}

#ifdef L3VPN
INT4
Bgp4Vpnv4GetFirstFsbgp4ImportTableIndex (tNetAddress * pRoutePrefix,
                                         INT4 *pi4Protocol,
                                         tAddrPrefix * pNexthop,
                                         INT4 *pi4IfIndex, INT4 *pi4Metric,
                                         tSNMP_OCTET_STRING_TYPE * pVrfName)
{
    tRouteProfile      *pFndRtProfile = NULL;
    tBgp4Info          *pBgp4Info = NULL;
    UINT4               u4VrfId;

    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return BGP4_FAILURE;
    }
    Bgp4RibhGetFirstVrfImportRtEntry (&pFndRtProfile, &u4VrfId);
    if (pFndRtProfile == NULL)
    {
        bgp4RibUnlock ();
        return BGP4_FAILURE;
    }
    else
    {
        Bgp4CopyNetAddressStruct (pRoutePrefix,
                                  BGP4_RT_NET_ADDRESS_INFO (pFndRtProfile));
        *pi4Protocol = BGP4_RT_PROTOCOL (pFndRtProfile);
        pBgp4Info = BGP4_RT_BGP_INFO (pFndRtProfile);
        Bgp4CopyAddrPrefixStruct (pNexthop, BGP4_INFO_NEXTHOP_INFO (pBgp4Info));
        *pi4IfIndex = (INT4) BGP4_RT_GET_RT_IF_INDEX (pFndRtProfile) +
            BGP4_INCREMENT_BY_ONE;
        *pi4Metric = (INT4) BGP4_INFO_RCVD_MED (pBgp4Info);
        VcmGetAliasName (u4VrfId, pVrfName->pu1_OctetList);
        pVrfName->i4_Length = (INT4) STRLEN (pVrfName->pu1_OctetList);
    }
    bgp4RibUnlock ();
    return BGP4_SUCCESS;
}

INT4
Bgp4RibhGetFirstVrfImportRtEntry (tRouteProfile ** pFndRtProfile,
                                  UINT4 *pu4VrfId)
{
    VOID               *pRibNode = NULL;
    tRouteProfile      *pRtProfile = NULL;
    UINT4               u4Context;
    INT4                i4RetVal;

    i4RetVal = BgpGetFirstActiveContextID (&u4Context);
    while (i4RetVal == SNMP_SUCCESS)
    {
        pRibNode = BGP4_VPN4_RIB_TREE (u4Context);
        pRtProfile =
            Bgp4RibhGetFirstImportAsafiRtEntry (CAP_MP_VPN4_UNICAST, &pRibNode);
        if (pRtProfile != NULL)
        {
            *pFndRtProfile = pRtProfile;
            *pu4VrfId = u4Context;
            return BGP4_SUCCESS;
        }
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
    }
    *pFndRtProfile = NULL;
    *pu4VrfId = 0;
    return BGP4_FAILURE;
}
#endif

INT4
Bgp4ValidateFsbgp4ImportTableIndex (tNetAddress RoutePrefix,
                                    INT4 i4Protocol, tAddrPrefix Nexthop,
                                    INT4 i4IfIndex, INT4 i4Metric,
                                    tSNMP_OCTET_STRING_TYPE * pVrfName)
{
#ifdef L3VPN
    UINT4               u4VcId;
#endif
    UINT1               u1ValidStatus = BGP4_FALSE;

#ifndef L3VPN
    UNUSED_PARAM (pVrfName);
#endif
    /* Validate SAFI */
    u1ValidStatus =
        Bgp4MpeValidateSubAddressType (BGP4_SAFI_IN_NET_ADDRESS_INFO
                                       (RoutePrefix));
    if (u1ValidStatus == BGP4_FALSE)
    {
        return BGP4_FAILURE;
    }
    if ((Bgp4IsValidAddress
         (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (RoutePrefix),
          BGP4_TRUE)) != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }

    u1ValidStatus = Bgp4IsValidPrefixLength (RoutePrefix);
    if (u1ValidStatus == BGP4_FALSE)
    {
        return BGP4_FAILURE;
    }

    if ((i4Protocol != BGP4_RIP_ID) && (i4Protocol != BGP4_STATIC_ID) &&
        (i4Protocol != BGP4_LOCAL_ID) && (i4Protocol != BGP4_OSPF_ID))
    {
        return BGP4_FAILURE;
    }

    if ((Bgp4IsValidAddress (Nexthop, BGP4_TRUE)) != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }

    if ((i4IfIndex < BGP4_MIN_IFINDX) || (i4Metric < BGP4_MIN_METRIC))
    {
        return BGP4_FAILURE;
    }
#ifdef L3VPN
    if (VcmIsVrfExist (pVrfName->pu1_OctetList, &u4VcId) != VCM_TRUE)
    {
        return BGP4_FAILURE;
    }
#endif

    return BGP4_SUCCESS;
}

INT4
Bgp4GetFirstIndexFsbgp4ImportTable (tNetAddress * pRoutePrefix,
                                    INT4 *pi4Protocol, tAddrPrefix * pNexthop,
                                    INT4 *pi4IfIndex, INT4 *pi4Metric,
                                    tSNMP_OCTET_STRING_TYPE * pVrfName)
{
    INT4                i4RetVal;

    pVrfName->i4_Length = 0;
    /* First get the IPV4 import routes, if any */
    i4RetVal =
        Bgp4Ipv4GetFirstFsbgp4ImportTableIndex (pRoutePrefix, pi4Protocol,
                                                pNexthop, pi4IfIndex,
                                                pi4Metric);
    if (i4RetVal == BGP4_FAILURE)
    {
#ifdef BGP4_IPV6_WANTED
        /* If there are no IPV4 imported routes then try to
         * get the IPV6 imported routes
         */
        i4RetVal =
            Bgp4Ipv6GetFirstFsbgp4ImportTableIndex (pRoutePrefix, pi4Protocol,
                                                    pNexthop, pi4IfIndex,
                                                    pi4Metric);
#endif
    }
#ifdef L3VPN
    if (i4RetVal == BGP4_FAILURE)
    {
        /* First get the IPV4 import routes, if any */
        i4RetVal =
            Bgp4Vpnv4GetFirstFsbgp4ImportTableIndex (pRoutePrefix, pi4Protocol,
                                                     pNexthop, pi4IfIndex,
                                                     pi4Metric, pVrfName);
    }

#endif
    return i4RetVal;
}

INT4
Bgp4GetNextIndexFsbgp4ImportTable (tNetAddress RoutePrefix,
                                   INT4 i4Protocol, tAddrPrefix Nexthop,
                                   INT4 i4IfIndex, INT4 i4Metric,
                                   tSNMP_OCTET_STRING_TYPE * pVrfName,
                                   tNetAddress * pNextRoutePrefix,
                                   INT4 *pi4NextProtocol,
                                   tAddrPrefix * pNextNexthop,
                                   INT4 *pi4NextIfIndex, INT4 *pi4NextMetric,
                                   tSNMP_OCTET_STRING_TYPE * pNextVrfName)
{
    VOID               *pRibNode = NULL;
    tRouteProfile      *pFndRtProfile = NULL;
    tRouteProfile       RtProfile;
#ifdef L3VPN
    UINT1               u1VrfImport = BGP4_FALSE;
    UINT4               u4NextVrfId;
#endif
    UINT4               u4AsafiMask;

    UNUSED_PARAM (pVrfName);
    UNUSED_PARAM (pNextVrfName);
    UNUSED_PARAM (i4Metric);
    UNUSED_PARAM (Nexthop);

    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return BGP4_FAILURE;
    }

    MEMSET (&RtProfile, 0, sizeof (tRouteProfile));
    Bgp4CopyNetAddressStruct (&(RtProfile.NetAddress), RoutePrefix);
    RtProfile.u1Protocol = (UINT1) i4Protocol;
    RtProfile.u4RtIfIndx = (UINT4) i4IfIndex;
    RtProfile.pBgpCxtNode = Bgp4GetContextEntry (gpBgpCurrCxtNode->u4ContextId);
#ifdef L3VPN
    BGP4_RT_AFI_INFO ((&RtProfile)) = BGP4_INET_AFI_IPV4;
    BGP4_RT_SAFI_INFO ((&RtProfile)) = BGP4_INET_SAFI_VPNV4_UNICAST;
    Bgp4RibhGetNextVrfImportRtEntry (&(RtProfile),
                                     gpBgpCurrCxtNode->u4ContextId,
                                     &pFndRtProfile, &u4NextVrfId);
    if (pFndRtProfile != NULL)
    {
        u1VrfImport = BGP4_TRUE;
    }
    if (pFndRtProfile == NULL)
    {
        Bgp4RibhGetFirstRouteFromNextAddrFamily (gpBgpCurrCxtNode->u4ContextId,
                                                 CAP_MP_VPN4_UNICAST,
                                                 &pFndRtProfile);
    }
#endif

#ifdef L3VPN
    if (u1VrfImport != BGP4_TRUE)
    {
#endif
        pFndRtProfile = Bgp4RibhGetNextImportRtEntry (&(RtProfile), &pRibNode);
#ifdef L3VPN
        if (pFndRtProfile == NULL)
        {
            Bgp4RibhGetFirstVrfImportRtEntry (&pFndRtProfile, &u4NextVrfId);
        }
    }
#endif
    if (pFndRtProfile == NULL)
    {
        bgp4RibUnlock ();
        return BGP4_FAILURE;
    }
    else
    {
        Bgp4CopyNetAddressStruct (pNextRoutePrefix,
                                  BGP4_RT_NET_ADDRESS_INFO (pFndRtProfile));
        *pi4NextProtocol = BGP4_RT_PROTOCOL (pFndRtProfile);
        *pi4NextIfIndex = (INT4)BGP4_RT_GET_RT_IF_INDEX (pFndRtProfile) +
            BGP4_INCREMENT_BY_ONE;
        Bgp4CopyAddrPrefixStruct (pNextNexthop,
                                  BGP4_INFO_NEXTHOP_INFO (BGP4_RT_BGP_INFO
                                                          (pFndRtProfile)));
        *pi4NextMetric =
            (INT4) BGP4_INFO_RCVD_MED (BGP4_RT_BGP_INFO (pFndRtProfile));
        BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pFndRtProfile),
                               BGP4_RT_SAFI_INFO (pFndRtProfile), u4AsafiMask);
#ifdef L3VPN
        if (u4AsafiMask == CAP_MP_VPN4_UNICAST)
        {
            VcmGetAliasName (u4NextVrfId, pNextVrfName->pu1_OctetList);
            pNextVrfName->i4_Length =
                (INT4) STRLEN (pNextVrfName->pu1_OctetList);
        }
#endif
    }
    bgp4RibUnlock ();
    return BGP4_SUCCESS;
}

INT4
Bgp4GetFsbgp4ImportTableRouteAction (UINT4 u4VrfId, tNetAddress RoutePrefix,
                                     INT4 i4Protocol, tAddrPrefix Nexthop,
                                     INT4 i4IfIndex, INT4 i4Metric,
                                     tSNMP_OCTET_STRING_TYPE * pVrfName,
                                     INT4 *pi4RetVal)
{
    VOID               *pRibNode = NULL;
    tRouteProfile      *pRtProfileList = NULL;
    tRouteProfile       RtProfile;
    INT4                i4RetVal = BGP4_FAILURE;

    UNUSED_PARAM (pVrfName);
    UNUSED_PARAM (i4Metric);

    Bgp4GetPrefixForPrefixLen (&RoutePrefix, RoutePrefix.u2PrefixLen);
    Bgp4CopyNetAddressStruct (&(RtProfile.NetAddress), RoutePrefix);
    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return BGP4_FAILURE;
    }
#ifdef L3VPN
    BGP4_RT_AFI_INFO ((&RtProfile)) = BGP4_INET_AFI_IPV4;
    BGP4_RT_SAFI_INFO ((&RtProfile)) = BGP4_INET_SAFI_VPNV4_UNICAST;
#endif
    i4RetVal = Bgp4RibhLookupRtEntry (&RtProfile, u4VrfId, BGP4_TREE_FIND_EXACT,
                                      &pRtProfileList, &pRibNode);
    if (i4RetVal == BGP4_FAILURE)
    {
        /* No matching route. */
        bgp4RibUnlock ();
        return BGP4_FAILURE;
    }

    while (pRtProfileList != NULL)
    {
        if (((PrefixMatch
              (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (RoutePrefix),
               BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
               (BGP4_RT_NET_ADDRESS_INFO (pRtProfileList)))) == BGP4_TRUE)
            && (i4Protocol == pRtProfileList->u1Protocol)
            &&
            ((PrefixMatch
              (Nexthop,
               BGP4_INFO_NEXTHOP_INFO (BGP4_RT_BGP_INFO (pRtProfileList)))) ==
             BGP4_TRUE)
            && ((i4IfIndex - BGP4_DECREMENT_BY_ONE) ==
                (INT4) pRtProfileList->u4RtIfIndx))
        {
            *pi4RetVal = BGP4_IMPORT_RT_ADD;
            bgp4RibUnlock ();
            return BGP4_SUCCESS;
        }
        pRtProfileList = BGP4_RT_NEXT (pRtProfileList);
    }

    *pi4RetVal = BGP4_IMPORT_RT_DELETE;
    bgp4RibUnlock ();
    return BGP4_FAILURE;
}

INT4
Bgp4SetFsbgp4ImportTableRouteAction (tNetAddress RoutePrefix,
                                     INT4 i4Protocol, tAddrPrefix Nexthop,
                                     INT4 i4IfIndex, INT4 i4Metric,
                                     tSNMP_OCTET_STRING_TYPE * pVrfName,
                                     INT4 i4SetVal)
{
    UINT4               u4VrfId = gpBgpCurrCxtNode->u4ContextId;

    Bgp4GetPrefixForPrefixLen (&RoutePrefix, RoutePrefix.u2PrefixLen);
    UNUSED_PARAM (pVrfName);

    if (Bgp4RouteLeak (u4VrfId, RoutePrefix, i4Protocol, Nexthop,
                       i4SetVal, BGP4_INVALID_ROUTE_TAG,
                       i4Metric, (i4IfIndex - (INT4) BGP4_DECREMENT_BY_ONE), 0, 0, NULL, BGP4_FALSE)
        != BGP4_SUCCESS)
    {
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

INT4
Bgp4TestFsbgp4ImportTableRouteAction (tNetAddress RoutePrefix,
                                      INT4 i4Protocol, tAddrPrefix Nexthop,
                                      INT4 i4IfIndex, INT4 i4Metric,
                                      tSNMP_OCTET_STRING_TYPE * pVrfName,
                                      INT4 i4TestVal, UINT4 *pu4ErrorCode)
{
#ifdef L3VPN
    tBgpSystemSize      BgpSystemSize;
    UINT4               u4VrfId = gpBgpCurrCxtNode->u4ContextId;
#endif

    UNUSED_PARAM (pVrfName);
    UNUSED_PARAM (RoutePrefix);
    UNUSED_PARAM (i4Protocol);
    UNUSED_PARAM (Nexthop);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4Metric);

    if ((i4TestVal != BGP4_IMPORT_RT_ADD) &&
        (i4TestVal != BGP4_IMPORT_RT_DELETE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return BGP4_FAILURE;
    }
#ifdef L3VPN
    GetBgpSizingParams (&BgpSystemSize);
    /* When any of the following conditions occur, then return failure
     * 1. There is no corresponding VRF
     * 2. When the VRF is not operationally UP
     * 3. VRF has already installed maximum no.of routes 
     */
    if ((BGP4_VPN4_VRF_SPEC_CURRENT_STATE (u4VrfId) != BGP4_L3VPN_VRF_UP)
        || ((i4TestVal == BGP4_IMPORT_RT_ADD) &&
            (BGP4_RIB_ROUTES_COUNT (u4VrfId) >=
             BgpSystemSize.u4BgpMaxNumOfRoutes)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return BGP4_FAILURE;
    }
    else if ((i4TestVal == BGP4_IMPORT_RT_ADD) &&
             (BGP4_RIB_ROUTE_CNT (BGP4_DFLT_VRFID) >=
              BgpSystemSize.u4BgpMaxNumOfRoutes))
    {
        /* RIB route count is exhausted, reached maximum no.of routes */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return BGP4_FAILURE;
    }
    switch (i4Protocol)
    {
        case BGP4_STATIC_ID:
            /* Check for the redistribution policy */
            if (!(BGP4_RRD_PROTO_MASK (u4VrfId) & BGP4_IMPORT_STATIC))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return BGP4_FAILURE;
            }
            break;
        default:
            /* no other protocol than static is allowed to import via
             * this function
             */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return BGP4_FAILURE;
    }
#endif
    return BGP4_SUCCESS;
}

INT4
Bgp4ValidateFsbgp4FsmTransHistTableIndex (UINT4 u4Context,
                                          tAddrPrefix PeerAddress)
{
    tBgp4PeerEntry     *pPeerentry = NULL;
    UINT1               u1ValidStatus;

    u1ValidStatus = Bgp4IsValidAddress (PeerAddress, BGP4_FALSE);
    if (u1ValidStatus != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeerentry, tBgp4PeerEntry *)
    {
        if ((PrefixMatch (PeerAddress, BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)))
            == BGP4_TRUE)
        {
            return BGP4_SUCCESS;
        }
    }
    return BGP4_FAILURE;
}

INT4
Bgp4GetFirstIndexFsbgp4FsmTransHistTable (UINT4 u4Context,
                                          tAddrPrefix * pPeerAddress)
{
    tBgp4PeerEntry     *pPeerentry = NULL;

    pPeerentry =
        (tBgp4PeerEntry *) TMO_SLL_First (BGP4_PEERENTRY_HEAD (u4Context));
    if (pPeerentry == NULL)
    {
        return BGP4_FAILURE;
    }
    else
    {
        Bgp4CopyAddrPrefixStruct (pPeerAddress,
                                  BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry));
        return BGP4_SUCCESS;
    }
}

INT4
Bgp4GetNextIndexFsbgp4FsmTransHistTable (UINT4 u4Context,
                                         tAddrPrefix PeerAddress,
                                         tAddrPrefix * pNextPeerAddress)
{
    INT4                i4RetVal;

    i4RetVal =
        Bgp4GetNextIndexBgpPeerTable (u4Context, PeerAddress, pNextPeerAddress);
    return i4RetVal;
}

INT4
Bgp4ValidateFsbgp4RflPeerReflectTableIndex (UINT4 u4Context,
                                            tAddrPrefix PeerAddress)
{
    UINT2               u2Port = 0;

    if ((Bgp4IsValidAddress (PeerAddress, BGP4_FALSE)) != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }

    switch (BGP4_AFI_IN_ADDR_PREFIX_INFO (PeerAddress))
    {
        case BGP4_INET_AFI_IPV4:
        {
            UINT4               u4BgpPeerRemoteAddr;

            PTR_FETCH_4 (u4BgpPeerRemoteAddr,
                         BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddress));
            if (u4BgpPeerRemoteAddr == BGP4_LOCAL_BGP_ID (u4Context))
            {
                return BGP4_FAILURE;
            }
            /* Reject Peer Remote Address, if same as local address */
            if (IpIsLocalAddrInCxt (u4Context,
                                    u4BgpPeerRemoteAddr, &u2Port) == IP_SUCCESS)
            {
                if (BgpGetIpIfAddr (u2Port) == u4BgpPeerRemoteAddr)
                {
                    return (BGP4_FAILURE);
                }
            }
        }
            break;
#ifdef BGP4_IPV6_WANTED
        case BGP4_INET_AFI_IPV6:
        {
            /* Reject Peer Remote Address, if same as local address */
            if (Ip6IsLocalAddr
                (u4Context, BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (PeerAddress),
                 &u2Port) == IP_SUCCESS)
            {
                return (BGP4_FAILURE);
            }
        }
            break;
#endif
        default:
            return (BGP4_FAILURE);
    }
    return BGP4_SUCCESS;
}

INT4
Bgp4GetFirstIndexFsbgp4RflRouteReflectTable (UINT4 u4Context,
                                             tNetAddress * pRoutePrefix,
                                             tAddrPrefix * pPeerAddress)
{
    INT4                i4RetVal;

    i4RetVal =
        Bgp4GetFirstPathAttrTableIndex (u4Context, pRoutePrefix, pPeerAddress);
    return i4RetVal;

}

INT4
Bgp4GetNextIndexFsbgp4RflRouteReflectTable (UINT4 u4Context,
                                            tNetAddress RoutePrefix,
                                            tAddrPrefix PeerAddress,
                                            tNetAddress * pRoutePrefix,
                                            tAddrPrefix * pPeerAddress)
{
    INT4                i4RetVal;

    i4RetVal =
        Bgp4GetNextPathAttrTableIndex (u4Context, RoutePrefix, PeerAddress,
                                       pRoutePrefix, pPeerAddress);
    return i4RetVal;

}

INT4
Bgp4ValidateFsbgp4RfdDampHistTableIndex (tNetAddress RoutePrefix,
                                         tAddrPrefix PeerAddress)
{
    UINT1               u1ValidStatus;

    u1ValidStatus =
        Bgp4MpeValidateSubAddressType (BGP4_SAFI_IN_NET_ADDRESS_INFO
                                       (RoutePrefix));
    if (u1ValidStatus != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }

    u1ValidStatus = Bgp4IsValidPrefixLength (RoutePrefix);
    if (u1ValidStatus == BGP4_FALSE)
    {
        return BGP4_FAILURE;
    }

    u1ValidStatus = Bgp4IsValidRouteAddress (RoutePrefix);
    if (u1ValidStatus != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }

    u1ValidStatus = Bgp4IsValidAddress (PeerAddress, BGP4_FALSE);
    if (u1ValidStatus != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }

    return BGP4_SUCCESS;
}

INT4
Bgp4GetFirstIndexFsbgp4RfdDampHistTable (UINT4 u4Context,
                                         tNetAddress * pRoutePrefix,
                                         tAddrPrefix * pPeerAddress)
{
#ifdef RFD_WANTED
    return (Bgp4GetFirstIndexFsbgp4RfdRtsReuseListTable
            (u4Context, pRoutePrefix, pPeerAddress));
#else
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (pRoutePrefix);
    UNUSED_PARAM (pPeerAddress);
    return BGP4_FAILURE;
#endif
}

INT4
Bgp4GetNextIndexFsbgp4RfdDampHistTable (UINT4 u4Context,
                                        tNetAddress RoutePrefix,
                                        tAddrPrefix PeerAddress,
                                        tNetAddress * pNextRoutePrefix,
                                        tAddrPrefix * pNextPeerAddress)
{
#ifdef RFD_WANTED
    return (Bgp4GetNextIndexFsbgp4RfdRtsReuseListTable
            (u4Context, RoutePrefix, PeerAddress, pNextRoutePrefix,
             pNextPeerAddress));
#else
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (RoutePrefix);
    UNUSED_PARAM (PeerAddress);
    UNUSED_PARAM (pNextRoutePrefix);
    UNUSED_PARAM (pNextPeerAddress);
    return BGP4_FAILURE;
#endif
}

INT4
Bgp4GetFsbgp4RfdDampHistTableIntegerObject (UINT4 u4Context,
                                            tNetAddress RoutePrefix,
                                            tAddrPrefix PeerAddress,
                                            INT4 i4ObjectType, INT4 *pi4RetVal)
{
#ifdef RFD_WANTED
    tRtDampHist        *pRtDampHist = NULL;
    tRouteProfile      *pRtProfile = NULL;
    VOID               *pRibNode = NULL;
    RFDFLOAT            FomTempVal;

    UINT4               u4CurrTime = 0;
    UINT2               u2ReuseInd = 0;
    UINT2               u2TimeDiff = 0;

    pRtProfile = Bgp4GetRoute (u4Context, RoutePrefix, PeerAddress, pRibNode);
    if ((pRtProfile == NULL) || (BGP4_RT_DAMP_HIST (pRtProfile) == NULL))
    {
        return BGP4_FAILURE;
    }
    pRtDampHist = BGP4_RT_DAMP_HIST (pRtProfile);

    switch (i4ObjectType)
    {
        case BGP4_RFD_RT_FOM_OBJECT:
        RFD_GET_SYS_TIME (&u4CurrTime);
        u2TimeDiff =
            RFD_GET_TIME_DIFF_SEC (u4CurrTime,pRtDampHist->u4RtLastTime);
        FomTempVal = ((RFDFLOAT)u2TimeDiff)/
                     ((RFDFLOAT) RFD_DECAY_HALF_LIFE_TIME
                      (BGP4_RT_CXT_ID(pRtDampHist->pRtProfile)));
        FomTempVal = (RFDFLOAT)RFD_POW (0.5, FomTempVal);



        *pi4RetVal=((pRtDampHist->RtFom) * FomTempVal);

            break;

        case BGP4_RFD_RT_LASTUPDT_TIME_OBJECT:
            *pi4RetVal = (INT4) pRtDampHist->u4RtLastTime;
            break;

        case BGP4_RFD_RT_STATE_OBJECT:
            if ((pRtDampHist->u1RtStatus == RFD_SUPPRESSED_STATE) ||
                (pRtDampHist->u1RtStatus == RFD_UNSUPPRESSED_STATE))
            {
                *pi4RetVal = pRtDampHist->u1RtStatus;
            }
            else
            {
                *pi4RetVal = RFD_NONE_VALUE;
            }
            break;

        case BGP4_RFD_RT_STATUS_OBJECT:
            if ((pRtDampHist->u1RtFlag == RFD_FEASIBLE_ROUTE) ||
                (pRtDampHist->u1RtFlag == RFD_UNFEASIBLE_ROUTE))
            {
                *pi4RetVal = pRtDampHist->u1RtFlag;
            }
            else
            {
                *pi4RetVal = RFD_NONE_VALUE;
            }
            break;

        case BGP4_RFD_RT_FLAP_COUNT_OBJECT:
            *pi4RetVal =(INT4) pRtDampHist->u4RtFlapCount;
            break;

        case BGP4_RFD_RT_FLAP_TIME_OBJECT:
            RFD_GET_SYS_TIME (&u4CurrTime);
            u4CurrTime = (u4CurrTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
            *pi4RetVal =(INT4)(u4CurrTime - pRtDampHist->u4RtFlapTime);
            break;

        case BGP4_RFD_RT_REUSE_TIME_OBJECT:
            if (pRtDampHist->u1RtStatus == RFD_SUPPRESSED_STATE)
            {
                if (RFD_REUSE_LIST_OFFSET (u4Context) >
                    pRtDampHist->u2ReuseIndex)
                {
                    u2ReuseInd = (RFD_NO_OF_REUSE_LISTS -
                                  (pRtDampHist->u2ReuseIndex -
                                   RFD_REUSE_LIST_OFFSET (u4Context)));
                    *pi4RetVal =
                        (u2ReuseInd * RFD_REUSE_TIMER_GRANULARITY (u4Context));
                }
                else
                {
                    u2ReuseInd =
                        (pRtDampHist->u2ReuseIndex -
                         RFD_REUSE_LIST_OFFSET (u4Context));
                    *pi4RetVal =
                        (u2ReuseInd * RFD_REUSE_TIMER_GRANULARITY (u4Context));
                }
            }
            else
            {
                *pi4RetVal = 0;
            }
            break;
        default:
            return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
#else
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (RoutePrefix);
    UNUSED_PARAM (PeerAddress);
    UNUSED_PARAM (i4ObjectType);
    UNUSED_PARAM (pi4RetVal);
    return BGP4_FAILURE;
#endif
}

INT4
Bgp4ValidateFsbgp4PeerDampHistTableIndex (tAddrPrefix PeerAddress)
{
    UINT1               u1ValidStatus;

    u1ValidStatus = Bgp4IsValidAddress (PeerAddress, BGP4_FALSE);
    if (u1ValidStatus != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

INT4
Bgp4GetFirstIndexFsbgp4PeerDampHistTable (UINT4 u4Context,
                                          tAddrPrefix * pPeerAddress)
{
#ifdef RFD_WANTED
    tPeerDampHist      *pPeerDampHist = NULL;
    UINT4               u4HashIndex;

    if (PEERS_DAMP_HIST_LIST (u4Context) == NULL)
    {
        return BGP4_FAILURE;
    }
    TMO_HASH_Scan_Table (PEERS_DAMP_HIST_LIST (u4Context), u4HashIndex)
    {
        pPeerDampHist =
            (tPeerDampHist
             *) (TMO_HASH_Get_First_Bucket_Node (PEERS_DAMP_HIST_LIST
                                                 (u4Context), u4HashIndex));
        if (pPeerDampHist != NULL)
        {
            Bgp4CopyAddrPrefixStruct (pPeerAddress,
                                      RFD_PEER_INFO_IN_PEER_DAMPHIST_STRUCT
                                      (pPeerDampHist));
            return BGP4_SUCCESS;
        }
    }
#else
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (pPeerAddress);
#endif
    return BGP4_FAILURE;
}

INT4
Bgp4GetNextIndexFsbgp4PeerDampHistTable (UINT4 u4Context,
                                         tAddrPrefix PeerAddress,
                                         tAddrPrefix * pNextPeerAddress)
{
#ifdef RFD_WANTED
    tPeerDampHist      *pPeerDampHist = NULL;
    UINT4               u4HashIndex;
    UINT4               u4HashBkt;
    UINT1               u1MatchFnd = RFD_FALSE;

    if (PEERS_DAMP_HIST_LIST (u4Context) == NULL)
    {
        return BGP4_FAILURE;
    }
    Bgp4GetPrefixHashIndex (PeerAddress, &u4HashIndex);
    for (u4HashBkt = u4HashIndex; u4HashBkt < BGP4_MAX_PREFIX_HASH_TBL_SIZE;
         u4HashBkt++)
    {
        TMO_HASH_Scan_Bucket (PEERS_DAMP_HIST_LIST (u4Context), u4HashBkt,
                              pPeerDampHist, tPeerDampHist *)
        {
            if (u1MatchFnd == RFD_TRUE)
            {
                Bgp4CopyAddrPrefixStruct (pNextPeerAddress,
                                          RFD_PEER_INFO_IN_PEER_DAMPHIST_STRUCT
                                          (pPeerDampHist));
                return BGP4_SUCCESS;
            }
            if ((PrefixMatch
                 (RFD_PEER_INFO_IN_PEER_DAMPHIST_STRUCT (pPeerDampHist),
                  PeerAddress)) == BGP4_TRUE)
            {
                u1MatchFnd = RFD_TRUE;
            }
        }
    }
#else
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (PeerAddress);
    UNUSED_PARAM (pNextPeerAddress);
#endif
    return BGP4_FAILURE;
}

INT4
Bgp4GetFsbgp4PeerDampHistTableIntegerObject (UINT4 u4Context,
                                             tAddrPrefix PeerAddress,
                                             INT4 i4ObjectType, INT4 *pi4RetVal)
{
#ifdef RFD_WANTED
    tBgp4PeerEntry     *pPeerInfo = NULL;
    tPeerDampHist      *pPeerDampHist = NULL;
    static tBgp4PeerEntry PeerEntry;
    INT4                i4RetVal;
    UINT1               u1PeerStatus;

    Bgp4CopyAddrPrefixStruct (&(PeerEntry.peerConfig.RemoteAddrInfo),
                              PeerAddress);
    (&PeerEntry)->pBgpCxtNode = Bgp4GetContextEntry (u4Context);

    i4RetVal = RfdFetchPeerDampHist (&PeerEntry, &pPeerDampHist);
    if ((i4RetVal != RFD_SUCCESS) || (pPeerDampHist == NULL))
    {
        return BGP4_FAILURE;
    }
    switch (i4ObjectType)
    {
        case BGP4_RFD_PEER_FOM_OBJECT:
            *pi4RetVal = (INT4) BGP4_CEIL (pPeerDampHist->PeerFom);
            break;
        case BGP4_RFD_PEER_LASTUPDT_TIME_OBJECT:
            *pi4RetVal = (INT4)pPeerDampHist->u4PeerLastTime;
            break;
        case BGP4_RFD_PEER_STATE_OBJECT:
            if ((pPeerDampHist->u1PeerStatus == RFD_SUPPRESSED_STATE) ||
                (pPeerDampHist->u1PeerStatus == RFD_UNSUPPRESSED_STATE))
            {
                *pi4RetVal = pPeerDampHist->u1PeerStatus;
            }
            else
            {
                *pi4RetVal = RFD_NONE_VALUE;
            }
            break;
        case BGP4_RFD_PEER_STATUS_OBJECT:
            pPeerInfo = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
            if (pPeerInfo == NULL)
            {
                return BGP4_FAILURE;
            }
            u1PeerStatus = RFD_PEER_STATE (u4Context, pPeerInfo);
            if (u1PeerStatus == RFD_PEER_UP)
            {
                *pi4RetVal = RFD_MIB_PEER_UP;
            }
            else if (u1PeerStatus < RFD_PEER_UP)
            {
                *pi4RetVal = RFD_MIB_PEER_DOWN;
            }
            else
            {
                *pi4RetVal = RFD_NONE_VALUE;
            }
            break;
        default:
            return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
#else
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (PeerAddress);
    UNUSED_PARAM (i4ObjectType);
    UNUSED_PARAM (pi4RetVal);
    return BGP4_FAILURE;
#endif
}

INT4
Bgp4ValidateFsbgp4RfdRtsReuseListTableIndex (tNetAddress RoutePrefix,
                                             tAddrPrefix PeerAddress)
{
#ifdef RFD_WANTED
    UINT1               u1ValidStatus;

    u1ValidStatus =
        Bgp4MpeValidateSubAddressType (BGP4_SAFI_IN_NET_ADDRESS_INFO
                                       (RoutePrefix));
    if (u1ValidStatus != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }

    u1ValidStatus = Bgp4IsValidPrefixLength (RoutePrefix);
    if (u1ValidStatus == BGP4_FALSE)
    {
        return BGP4_FAILURE;
    }

    u1ValidStatus = Bgp4IsValidRouteAddress (RoutePrefix);
    if (u1ValidStatus != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }

    u1ValidStatus = Bgp4IsValidAddress (PeerAddress, BGP4_FALSE);
    if (u1ValidStatus != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }

    return BGP4_SUCCESS;
#else
    UNUSED_PARAM (RoutePrefix);
    UNUSED_PARAM (PeerAddress);
    return BGP4_FAILURE;
#endif
}

INT4
Bgp4GetFirstIndexFsbgp4RfdRtsReuseListTable (UINT4 u4Context,
                                             tNetAddress * pRoutePrefix,
                                             tAddrPrefix * pPeerAddress)
{
#ifdef RFD_WANTED
    tRtDampHist        *pRtDampHist = NULL;
    UINT2               u2Index = 0;

    while (u2Index < RFD_NO_OF_REUSE_LISTS)
    {
        if (RFD_RTS_REUSE_LIST_FIRST (u4Context, u2Index) != NULL)
        {
            pRtDampHist = RFD_RTS_REUSE_LIST_FIRST (u4Context, u2Index);
            break;
        }
        u2Index++;
    }

    if (pRtDampHist == NULL)
    {
        /* No Reuse entry is not present. */
        return BGP4_FAILURE;
    }

    Bgp4SnmphCopyNetAddr (pRtDampHist->pRtProfile,
                          BGP4_RT_PEER_ENTRY (pRtDampHist->pRtProfile),
                          pRoutePrefix);
    Bgp4CopyAddrPrefixStruct (pPeerAddress,
                              BGP4_PEER_REMOTE_ADDR_INFO (BGP4_RT_PEER_ENTRY
                                                          (pRtDampHist->
                                                           pRtProfile)));
    return BGP4_SUCCESS;
#else
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (pRoutePrefix);
    UNUSED_PARAM (pPeerAddress);
    return BGP4_FAILURE;
#endif
}

INT4
Bgp4GetNextIndexFsbgp4RfdRtsReuseListTable (UINT4 u4Context,
                                            tNetAddress RoutePrefix,
                                            tAddrPrefix PeerAddress,
                                            tNetAddress * pNextRoutePrefix,
                                            tAddrPrefix * pNextPeerAddress)
{
#ifdef RFD_WANTED
    tRouteProfile      *pRtProfile;
    tRtDampHist        *pRtDampHist = NULL;
    VOID               *pRibNode = NULL;
    UINT2               u2Index = 0;

    pRtProfile = Bgp4GetRoute (u4Context, RoutePrefix, PeerAddress, pRibNode);
    if ((pRtProfile == NULL) || (BGP4_RT_DAMP_HIST (pRtProfile) == NULL))
    {
        /* No Matching Damp History is available. */
        return BGP4_FAILURE;
    }
    pRtDampHist = BGP4_RT_DAMP_HIST (pRtProfile);

    /* Check if Next Entry is present in the same Reuse-List or not.
     * If present, then return it else try and get the first available
     * entry in the subsequent Reuse-List. */
    if (pRtDampHist->pReuseNext != NULL)
    {
        /* Next Reuse Entry is present in the same List. */
        pRtDampHist = pRtDampHist->pReuseNext;
    }
    else
    {
        /* Try and get the first entry from the next list. */
        u2Index = (UINT2) (pRtDampHist->u2ReuseIndex + 1);
        pRtDampHist = NULL;
        while (u2Index < RFD_NO_OF_REUSE_LISTS)
        {
            if (RFD_RTS_REUSE_LIST_FIRST (u4Context, u2Index) != NULL)
            {
                pRtDampHist = RFD_RTS_REUSE_LIST_FIRST (u4Context, u2Index);
                break;
            }
            u2Index++;
        }
    }

    if (pRtDampHist == NULL)
    {
        /* Next entry is not present. */
        return BGP4_FAILURE;
    }
    Bgp4SnmphCopyNetAddr (pRtDampHist->pRtProfile,
                          BGP4_RT_PEER_ENTRY (pRtDampHist->pRtProfile),
                          pNextRoutePrefix);
    Bgp4CopyAddrPrefixStruct (pNextPeerAddress,
                              BGP4_PEER_REMOTE_ADDR_INFO (BGP4_RT_PEER_ENTRY
                                                          (pRtDampHist->
                                                           pRtProfile)));
    return BGP4_SUCCESS;
#else
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (RoutePrefix);
    UNUSED_PARAM (PeerAddress);
    UNUSED_PARAM (pNextRoutePrefix);
    UNUSED_PARAM (pNextPeerAddress);
    return BGP4_FAILURE;
#endif
}

INT4
Bgp4GetFsbgp4RfdRtsReuseListTableIntegerObject (UINT4 u4Context,
                                                tNetAddress RoutePrefix,
                                                tAddrPrefix PeerAddress,
                                                INT4 i4ObjectType,
                                                INT4 *pi4RetVal)
{
#ifdef RFD_WANTED
    tRouteProfile      *pRtProfile = NULL;
    tRtDampHist        *pRtDampHist = NULL;
    VOID               *pRibNode = NULL;

    pRtProfile = Bgp4GetRoute (u4Context, RoutePrefix, PeerAddress, pRibNode);
    if ((pRtProfile == NULL) || (BGP4_RT_DAMP_HIST (pRtProfile) == NULL))
    {
        return BGP4_FAILURE;
    }
    pRtDampHist = BGP4_RT_DAMP_HIST (pRtProfile);

    switch (i4ObjectType)
    {
        case BGP4_RFD_REUSE_RT_FOM_OBJECT:
            *pi4RetVal = (INT4) BGP4_CEIL (pRtDampHist->RtFom);
            break;
        case BGP4_RFD_REUSE_RT_LASTUPDT_TIME_OBJECT:
            *pi4RetVal = (INT4)pRtDampHist->u4RtLastTime;
            break;
        case BGP4_RFD_REUSE_RT_STATE_OBJECT:
            if ((pRtDampHist->u1RtStatus == RFD_SUPPRESSED_STATE) ||
                (pRtDampHist->u1RtStatus == RFD_UNSUPPRESSED_STATE))
            {
                *pi4RetVal = pRtDampHist->u1RtStatus;
            }
            else
            {
                *pi4RetVal = RFD_NONE_VALUE;
            }
            break;
        case BGP4_RFD_REUSE_RT_STATUS_OBJECT:
            if ((pRtDampHist->u1RtFlag == RFD_FEASIBLE_ROUTE) ||
                (pRtDampHist->u1RtFlag == RFD_UNFEASIBLE_ROUTE))
            {
                *pi4RetVal = pRtDampHist->u1RtFlag;
            }
            else
            {
                *pi4RetVal = RFD_NONE_VALUE;
            }
            break;
        default:
            return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
#else
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (RoutePrefix);
    UNUSED_PARAM (PeerAddress);
    UNUSED_PARAM (i4ObjectType);
    UNUSED_PARAM (pi4RetVal);
    return BGP4_FAILURE;
#endif
}

INT4
Bgp4ValidateFsbgp4RfdPeerReuseListTableIndex (tAddrPrefix PeerAddress)
{
#ifdef RFD_WANTED
    UINT1               u1ValidStatus;

    u1ValidStatus = Bgp4IsValidAddress (PeerAddress, BGP4_FALSE);
    if (u1ValidStatus != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
#else
    UNUSED_PARAM (PeerAddress);
    return BGP4_FAILURE;
#endif
}

INT4
Bgp4GetFirstIndexFsbgp4RfdPeerReuseListTable (UINT4 u4Context,
                                              tAddrPrefix * pPeerAddress)
{
#ifdef RFD_WANTED
    tReusePeer         *pReusePeer = NULL;

    RFD_GLOBAL_PARAMS (u4Context).pLastReuseList = NULL;

    pReusePeer =
        (tReusePeer *) (TMO_SLL_First (RFD_PEERS_REUSE_LIST (u4Context)));
    if (pReusePeer == NULL)
    {
        return BGP4_FAILURE;
    }

    Bgp4CopyAddrPrefixStruct (pPeerAddress,
                              BGP4_PEER_REMOTE_ADDR_INFO
                              (pReusePeer->pPeerInfo));
    RFD_GLOBAL_PARAMS (u4Context).pLastReuseList = pReusePeer;
    return BGP4_SUCCESS;
#else
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (pPeerAddress);
    return BGP4_FAILURE;
#endif
}

INT4
Bgp4GetNextIndexFsbgp4RfdPeerReuseListTable (UINT4 u4Context,
                                             tAddrPrefix PeerAddress,
                                             tAddrPrefix * pNextPeerAddress)
{
#ifdef RFD_WANTED
    tReusePeer         *pReusePeer = NULL;
    UINT1               u1MatchFnd = RFD_FALSE;

    if (RFD_GLOBAL_PARAMS (u4Context).pLastReuseList != NULL &&
        (PrefixMatch
         (BGP4_PEER_REMOTE_ADDR_INFO
          (RFD_GLOBAL_PARAMS (u4Context).pLastReuseList->pPeerInfo),
          PeerAddress)) == BGP4_TRUE)
    {
        RFD_GLOBAL_PARAMS (u4Context).pLastReuseList =
            (tReusePeer *) TMO_SLL_Next (RFD_PEERS_REUSE_LIST (u4Context),
                                         &RFD_GLOBAL_PARAMS
                                         (u4Context).
                                         pLastReuseList->NextReusePeer);

        if (RFD_GLOBAL_PARAMS (u4Context).pLastReuseList == NULL)
        {
            return BGP4_FAILURE;
        }
        Bgp4CopyAddrPrefixStruct (pNextPeerAddress,
                                  BGP4_PEER_REMOTE_ADDR_INFO (RFD_GLOBAL_PARAMS
                                                              (u4Context).
                                                              pLastReuseList->
                                                              pPeerInfo));
        return BGP4_SUCCESS;

    }
    TMO_SLL_Scan (RFD_PEERS_REUSE_LIST (u4Context), pReusePeer, tReusePeer *)
    {
        if (u1MatchFnd == RFD_TRUE)
        {
            Bgp4CopyAddrPrefixStruct (pNextPeerAddress,
                                      BGP4_PEER_REMOTE_ADDR_INFO
                                      (pReusePeer->pPeerInfo));
            RFD_GLOBAL_PARAMS (u4Context).pLastReuseList = pReusePeer;
            return BGP4_SUCCESS;
        }
        if ((PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pReusePeer->pPeerInfo),
                          PeerAddress)) == BGP4_TRUE)
        {
            u1MatchFnd = RFD_TRUE;
        }
    }
    RFD_GLOBAL_PARAMS (u4Context).pLastReuseList = NULL;
#else
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (PeerAddress);
    UNUSED_PARAM (pNextPeerAddress);
#endif
    return BGP4_FAILURE;
}

INT4
Bgp4GetFsbgp4RfdPeerReuseListTableIntegerObject (UINT4 u4Context,
                                                 tAddrPrefix PeerAddress,
                                                 INT4 i4ObjectType,
                                                 INT4 *pi4RetVal)
{
#ifdef RFD_WANTED
    tReusePeer         *pReusePeer = NULL;
    tPeerDampHist      *pPeerDampHist = NULL;
    UINT1               u1MatchFnd = RFD_FALSE;
    INT4                i4RetSts;

    if (RFD_GLOBAL_PARAMS (u4Context).pLastReuseList != NULL
        &&
        (PrefixMatch
         (BGP4_PEER_REMOTE_ADDR_INFO
          (RFD_GLOBAL_PARAMS (u4Context).pLastReuseList->pPeerInfo),
          PeerAddress)) == BGP4_TRUE)
    {
        pReusePeer = RFD_GLOBAL_PARAMS (u4Context).pLastReuseList;
        u1MatchFnd = RFD_TRUE;
    }
    else
    {
        TMO_SLL_Scan (RFD_PEERS_REUSE_LIST (u4Context), pReusePeer,
                      tReusePeer *)
        {
            if ((PrefixMatch
                 (BGP4_PEER_REMOTE_ADDR_INFO (pReusePeer->pPeerInfo),
                  PeerAddress)) == BGP4_TRUE)
            {
                u1MatchFnd = RFD_TRUE;
                break;
            }
        }
    }
    if (u1MatchFnd != RFD_TRUE)
    {
        return BGP4_FAILURE;
    }
    if (i4ObjectType != BGP4_RFD_REUSE_PEER_STATUS_OBJECT)
    {
        i4RetSts = RfdFetchPeerDampHist (pReusePeer->pPeerInfo, &pPeerDampHist);
        if ((i4RetSts != RFD_SUCCESS) || (pPeerDampHist == NULL))
        {
            return BGP4_FAILURE;
        }
    }

    switch (i4ObjectType)
    {
        case BGP4_RFD_REUSE_PEER_FOM_OBJECT:
            *pi4RetVal = (INT4) BGP4_CEIL (pPeerDampHist->PeerFom);
            break;
        case BGP4_RFD_REUSE_PEER_LASTUPDT_TIME_OBJECT:
            *pi4RetVal = (INT4)pPeerDampHist->u4PeerLastTime;
            break;
        case BGP4_RFD_REUSE_PEER_STATE_OBJECT:
            if ((pPeerDampHist->u1PeerStatus == RFD_SUPPRESSED_STATE) ||
                (pPeerDampHist->u1PeerStatus == RFD_UNSUPPRESSED_STATE))
            {
                *pi4RetVal = pPeerDampHist->u1PeerStatus;
            }
            else
            {
                *pi4RetVal = RFD_NONE_VALUE;
            }
            break;
        case BGP4_RFD_REUSE_PEER_STATUS_OBJECT:
            if (u1MatchFnd == RFD_TRUE)
            {
                switch (RFD_PEER_STATE (u4Context, pReusePeer->pPeerInfo))
                {
                    case RFD_PEER_UP:
                        *pi4RetVal = RFD_MIB_PEER_UP;
                        break;
                    case RFD_PEER_DOWN:
                        *pi4RetVal = RFD_MIB_PEER_DOWN;
                        break;
                    default:
                        *pi4RetVal = RFD_NONE_VALUE;
                        break;
                }
            }
            break;
        default:
            return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
#else
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (PeerAddress);
    UNUSED_PARAM (i4ObjectType);
    UNUSED_PARAM (pi4RetVal);
    return BGP4_FAILURE;
#endif
}

INT4
Bgp4ValidateCommRouteAddCommTableIndex (tNetAddress RoutePrefix,
                                        UINT4 u4CommVal)
{
    INT4                i4RetStatus;
    UINT1               u1ValidStatus;

    /* Validate the prefix length  */
    u1ValidStatus = Bgp4IsValidPrefixLength (RoutePrefix);
    if ((u1ValidStatus == BGP4_FALSE) ||
        (BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RoutePrefix) == 0))
    {
        return BGP4_FAILURE;
    }

    /* Validate the prefix */
    u1ValidStatus =
        Bgp4IsValidAddress (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                            (RoutePrefix), BGP4_TRUE);
    if (u1ValidStatus != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }

    /* Validate the community value */
    i4RetStatus = CommValidateCommunity (u4CommVal);
    if (i4RetStatus == COMM_FAILURE)
    {
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

INT4
Bgp4GetFirstIndexCommRouteAddCommTable (UINT4 u4Context,
                                        tNetAddress * pRoutePrefix,
                                        UINT4 *pu4CommVal)
{
    tRouteConfComm     *pRouteConfComm = NULL;
    tRouteConfComm      RouteConfComm;
    tCommProfile       *pCommProfile = NULL;

    MEMSET (&RouteConfComm, 0, sizeof (tRouteConfComm));

    RouteConfComm.u4Context = u4Context;
    pRouteConfComm = (tRouteConfComm *)
        BGP4_RB_TREE_GET_NEXT (ROUTES_COMM_ADD_TBL,
                               (tBgp4RBElem *) (&RouteConfComm), NULL);
    if ((pRouteConfComm != NULL) && (pRouteConfComm->u4Context == u4Context))
    {
        pCommProfile =
            (tCommProfile *) TMO_SLL_First (&(pRouteConfComm->TSConfComms));
        if (pCommProfile != NULL)
        {
            Bgp4CopyNetAddressStruct (pRoutePrefix,
                                      COMM_NET_ADDRESS_IN_ROUTE_CONF_STRUCT
                                      (pRouteConfComm));
            *pu4CommVal = pCommProfile->u4Comm;
            return BGP4_SUCCESS;
        }
    }
    return BGP4_FAILURE;
}

INT4
Bgp4GetNextIndexCommRouteAddCommTable (UINT4 u4Context, tNetAddress RoutePrefix,
                                       UINT4 u4CommVal,
                                       tNetAddress * pNextRoutePrefix,
                                       UINT4 *pu4NextCommVal)
{
    tRouteConfComm     *pRouteConfComm = NULL;
    tCommProfile       *pCommProfile = NULL;
    tRouteConfComm      RouteConfComm;
    UINT1               u1NodeDel = BGP4_FALSE;
    UINT1               u1MatchFnd = COMM_FALSE;

    MEMSET (&RouteConfComm, 0, sizeof (tRouteConfComm));
    RouteConfComm.u4Context = u4Context;
    Bgp4CopyNetAddressStruct (&(RouteConfComm.RtNetAddress), RoutePrefix);
    pRouteConfComm = BGP4_RB_TREE_GET (ROUTES_COMM_ADD_TBL,
                                       (tBgp4RBElem *) (&RouteConfComm));
    if (pRouteConfComm == NULL)
    {
        u1NodeDel = BGP4_TRUE;
    }
    if (u1NodeDel != BGP4_TRUE)
    {
        TMO_SLL_Scan (&(pRouteConfComm->TSConfComms), pCommProfile,
                      tCommProfile *)
        {
            if (u1MatchFnd == COMM_TRUE)
            {
                Bgp4CopyNetAddressStruct (pNextRoutePrefix,
                                          COMM_NET_ADDRESS_IN_ROUTE_CONF_STRUCT
                                          (pRouteConfComm));
                *pu4NextCommVal = pCommProfile->u4Comm;
                return BGP4_SUCCESS;
            }

            if (((PrefixMatch
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                   (COMM_NET_ADDRESS_IN_ROUTE_CONF_STRUCT (pRouteConfComm)),
                   BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (RoutePrefix))) ==
                 BGP4_TRUE)
                && (COMM_RT_PREFIXLEN_IN_ROUTE_CONF_STRUCT (pRouteConfComm) ==
                    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RoutePrefix))
                && (pCommProfile->u4Comm == u4CommVal))
            {
                u1MatchFnd = COMM_TRUE;
            }
        }
    }
    pRouteConfComm =
        BGP4_RB_TREE_GET_NEXT (ROUTES_COMM_ADD_TBL,
                               (tBgp4RBElem *) (&RouteConfComm), NULL);
    if ((pRouteConfComm != NULL) && (pRouteConfComm->u4Context == u4Context))
    {
        pCommProfile = (tCommProfile *)
            TMO_SLL_First (&(pRouteConfComm->TSConfComms));
        if (pCommProfile == NULL)
        {
            return SNMP_FAILURE;
        }
        Bgp4CopyNetAddressStruct (pNextRoutePrefix,
                                  COMM_NET_ADDRESS_IN_ROUTE_CONF_STRUCT
                                  (pRouteConfComm));
        *pu4NextCommVal = pCommProfile->u4Comm;
        return SNMP_SUCCESS;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - No more Community Value Configured.\n");
    return BGP4_FAILURE;
}

INT4
Bgp4ValidateCommRouteDelCommTableIndex (tNetAddress RoutePrefix,
                                        UINT4 u4CommVal)
{
    return (Bgp4ValidateCommRouteAddCommTableIndex (RoutePrefix, u4CommVal));
}

INT4
Bgp4GetFirstIndexCommRouteDelCommTable (UINT4 u4Context,
                                        tNetAddress * pRoutePrefix,
                                        UINT4 *pu4CommVal)
{
    tRouteConfComm     *pRouteConfComm = NULL;
    tRouteConfComm      RouteConfComm;
    tCommProfile       *pCommProfile = NULL;

    MEMSET (&RouteConfComm, 0, sizeof (tRouteConfComm));
    RouteConfComm.u4Context = u4Context;
    pRouteConfComm = (tRouteConfComm *)
        BGP4_RB_TREE_GET_NEXT (ROUTES_COMM_DELETE_TBL,
                               (tBgp4RBElem *) (&RouteConfComm), NULL);
    if ((pRouteConfComm != NULL) && (pRouteConfComm->u4Context == u4Context))
    {
        pCommProfile =
            (tCommProfile *) TMO_SLL_First (&(pRouteConfComm->TSConfComms));
        if (pCommProfile != NULL)
        {
            Bgp4CopyNetAddressStruct (pRoutePrefix,
                                      COMM_NET_ADDRESS_IN_ROUTE_CONF_STRUCT
                                      (pRouteConfComm));
            *pu4CommVal = pCommProfile->u4Comm;
            return BGP4_SUCCESS;
        }
    }
    return BGP4_FAILURE;
}

INT4
Bgp4GetNextIndexCommRouteDelCommTable (UINT4 u4Context, tNetAddress RoutePrefix,
                                       UINT4 u4CommVal,
                                       tNetAddress * pNextRoutePrefix,
                                       UINT4 *pu4NextCommVal)
{
    tRouteConfComm     *pRouteConfComm = NULL;
    tCommProfile       *pCommProfile = NULL;
    tRouteConfComm      RouteConfComm;
    UINT1               u1NodeDel = BGP4_FALSE;
    UINT1               u1MatchFnd = COMM_FALSE;

    MEMSET (&RouteConfComm, 0, sizeof (tRouteConfComm));
    RouteConfComm.u4Context = u4Context;
    Bgp4CopyNetAddressStruct (&(RouteConfComm.RtNetAddress), RoutePrefix);
    pRouteConfComm = BGP4_RB_TREE_GET (ROUTES_COMM_DELETE_TBL,
                                       (tBgp4RBElem *) (&RouteConfComm));
    if (pRouteConfComm == NULL)
    {
        u1NodeDel = BGP4_TRUE;
    }
    if (u1NodeDel != BGP4_TRUE)
    {
        TMO_SLL_Scan (&(pRouteConfComm->TSConfComms), pCommProfile,
                      tCommProfile *)
        {
            if (u1MatchFnd == COMM_TRUE)
            {
                Bgp4CopyNetAddressStruct (pNextRoutePrefix,
                                          COMM_NET_ADDRESS_IN_ROUTE_CONF_STRUCT
                                          (pRouteConfComm));
                *pu4NextCommVal = pCommProfile->u4Comm;
                return BGP4_SUCCESS;
            }

            if (((PrefixMatch
                  (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                   (COMM_NET_ADDRESS_IN_ROUTE_CONF_STRUCT (pRouteConfComm)),
                   BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (RoutePrefix))) ==
                 BGP4_TRUE)
                && (COMM_RT_PREFIXLEN_IN_ROUTE_CONF_STRUCT (pRouteConfComm) ==
                    BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RoutePrefix))
                && (pCommProfile->u4Comm == u4CommVal))
            {
                u1MatchFnd = COMM_TRUE;
            }
        }
    }
    pRouteConfComm =
        BGP4_RB_TREE_GET_NEXT (ROUTES_COMM_DELETE_TBL,
                               (tBgp4RBElem *) (&RouteConfComm), NULL);
    if ((pRouteConfComm != NULL) && (pRouteConfComm->u4Context == u4Context))
    {
        pCommProfile = (tCommProfile *)
            TMO_SLL_First (&(pRouteConfComm->TSConfComms));
        if (pCommProfile == NULL)
        {
            return SNMP_FAILURE;
        }
        Bgp4CopyNetAddressStruct (pNextRoutePrefix,
                                  COMM_NET_ADDRESS_IN_ROUTE_CONF_STRUCT
                                  (pRouteConfComm));
        *pu4NextCommVal = pCommProfile->u4Comm;
        return SNMP_SUCCESS;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tERROR - No more Community entry Configured.\n");
    return BGP4_FAILURE;
}

INT4
Bgp4ValidateCommRouteCommSetStatusTableIndex (tNetAddress RoutePrefix)
{
    UINT1               u1ValidStatus;

    /* Validate the prefix length  */
    u1ValidStatus = Bgp4IsValidPrefixLength (RoutePrefix);
    if ((u1ValidStatus == BGP4_FALSE) ||
        (BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO (RoutePrefix) == 0))
    {
        return BGP4_FAILURE;
    }

    /* Validate the prefix */
    u1ValidStatus =
        Bgp4IsValidAddress (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                            (RoutePrefix), BGP4_TRUE);
    if (u1ValidStatus != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

INT4
Bgp4GetFirstIndexCommRouteCommSetStatusTable (UINT4 u4Context,
                                              tNetAddress * pRoutePrefix)
{
    tRouteCommSetStatus *pRouteCommSetStatus = NULL;
    tRouteCommSetStatus RouteCommSetStatus;

    MEMSET (&RouteCommSetStatus, 0, sizeof (tRouteCommSetStatus));
    RouteCommSetStatus.u4Context = u4Context;
    pRouteCommSetStatus = (tRouteCommSetStatus *)
        BGP4_RB_TREE_GET_NEXT (ROUTES_COMM_SET_STATUS_TBL,
                               (tBgp4RBElem *) (&RouteCommSetStatus), NULL);
    if ((pRouteCommSetStatus != NULL)
        && (pRouteCommSetStatus->u4Context == u4Context))
    {
        Bgp4CopyNetAddressStruct (pRoutePrefix,
                                  COMM_NET_ADDRESS_IN_ROUTE_SET_STATUS_STRUCT
                                  (pRouteCommSetStatus));
        return BGP4_SUCCESS;
    }
    return BGP4_FAILURE;
}

INT4
Bgp4GetNextIndexCommRouteCommSetStatusTable (UINT4 u4Context,
                                             tNetAddress RoutePrefix,
                                             tNetAddress * pNextRoutePrefix)
{
    tRouteCommSetStatus *pRouteCommSetStatus = NULL;
    tRouteCommSetStatus RouteCommSetStatus;

    MEMSET (&RouteCommSetStatus, 0, sizeof (tRouteCommSetStatus));
    RouteCommSetStatus.u4Context = u4Context;
    Bgp4CopyNetAddressStruct (&(RouteCommSetStatus.RtNetAddress), RoutePrefix);
    pRouteCommSetStatus =
        BGP4_RB_TREE_GET_NEXT (ROUTES_COMM_SET_STATUS_TBL,
                               (tBgp4RBElem *) (&RouteCommSetStatus), NULL);
    if ((pRouteCommSetStatus != NULL)
        && (pRouteCommSetStatus->u4Context == u4Context))
    {
        Bgp4CopyNetAddressStruct (pNextRoutePrefix,
                                  COMM_NET_ADDRESS_IN_ROUTE_SET_STATUS_STRUCT
                                  (pRouteCommSetStatus));
        return BGP4_SUCCESS;
    }
    return BGP4_FAILURE;
}

INT4
CommGetInputFilterEntry (UINT4 u4Context, UINT4 u4Fsbgp4InFilterCommVal,
                         tCommFilterInfo ** ppCommFilterInfo)
{
    tCommFilterInfo    *pTmpCommFilterInfo = NULL;
    tCommFilterInfo     CommFilterInfo;

    /* The key is the number of ones in peer address */
    CommFilterInfo.u4Context = u4Context;
    CommFilterInfo.u4Comm = u4Fsbgp4InFilterCommVal;
    pTmpCommFilterInfo = BGP4_RB_TREE_GET (COMM_INPUT_FILTER_TBL,
                                           (tBgp4RBElem *) (&CommFilterInfo));
    if (pTmpCommFilterInfo != NULL)
    {
        *ppCommFilterInfo = pTmpCommFilterInfo;
        return COMM_SUCCESS;
    }
    /* Router doesnot have  Filtering policy configured for this community */

    *ppCommFilterInfo = NULL;
    return COMM_FAILURE;
}

INT4
CommCreateInputFilterEntry (UINT4 u4Context, UINT4 u4Fsbgp4InFilterCommVal,
                            tCommFilterInfo ** ppCommFilterInfo)
{
    UINT4               u4Ret;

    COMM_INPUT_FILTER_ENTRY_CREATE (*ppCommFilterInfo);
    if (*ppCommFilterInfo == NULL)
    {
        return COMM_FAILURE;
    }
    TMO_SLL_Init_Node (&((*ppCommFilterInfo)->CommInfoNext));
    (*ppCommFilterInfo)->u4Comm = u4Fsbgp4InFilterCommVal;
    (*ppCommFilterInfo)->u4Context = u4Context;
    u4Ret = BGP4_RB_TREE_ADD (COMM_INPUT_FILTER_TBL,
                              (tBgp4RBElem *) (*ppCommFilterInfo));
    if (u4Ret == RB_FAILURE)
    {
        return COMM_FAILURE;
    }
    return COMM_SUCCESS;
}

INT4
CommGetOutputFilterEntry (UINT4 u4Context, UINT4 u4Fsbgp4OutFilterCommVal,
                          tCommFilterInfo ** ppCommFilterInfo)
{
    tCommFilterInfo    *pTmpCommFilterInfo = NULL;
    tCommFilterInfo     CommFilterInfo;

    /* The key is the number of ones in peer address */
    CommFilterInfo.u4Comm = u4Fsbgp4OutFilterCommVal;
    CommFilterInfo.u4Context = u4Context;
    pTmpCommFilterInfo = BGP4_RB_TREE_GET (COMM_OUTPUT_FILTER_TBL,
                                           (tBgp4RBElem *) (&CommFilterInfo));
    if (pTmpCommFilterInfo != NULL)
    {
        *ppCommFilterInfo = pTmpCommFilterInfo;
        return COMM_SUCCESS;
    }
    /* Router doesnot have  Filtering policy */
    /* configured for this community         */

    *ppCommFilterInfo = NULL;
    return COMM_FAILURE;
}

INT4
CommCreateOutputFilterEntry (UINT4 u4Context, UINT4 u4Fsbgp4OutFilterCommVal,
                             tCommFilterInfo ** ppCommFilterInfo)
{
    UINT4               u4Ret;

    COMM_OUTPUT_FILTER_ENTRY_CREATE (*ppCommFilterInfo);
    if (*ppCommFilterInfo == NULL)
    {
        return COMM_FAILURE;
    }
    TMO_SLL_Init_Node (&((*ppCommFilterInfo)->CommInfoNext));
    (*ppCommFilterInfo)->u4Comm = u4Fsbgp4OutFilterCommVal;
    (*ppCommFilterInfo)->u4Context = u4Context;
    u4Ret = BGP4_RB_TREE_ADD (COMM_OUTPUT_FILTER_TBL,
                              (tBgp4RBElem *) (*ppCommFilterInfo));
    if (u4Ret == RB_FAILURE)
    {
        return COMM_FAILURE;
    }
    return COMM_SUCCESS;
}

INT4
CommCreateAddCommEntry (UINT4 u4Context, tNetAddress RcvdPrefix,
                        UINT4 u4Fsbgp4AddCommVal, tCommProfile ** ppCommProfile)
{
    tRouteConfComm     *pRouteConfComm = NULL;
    tCommProfile       *pCommProfile = NULL;
    tRouteConfComm      RouteConfComm;
    UINT4               u4Ret;
    UINT1               u1MatchFnd = COMM_FALSE;

    MEMSET (&RouteConfComm, 0, sizeof (tRouteConfComm));
    RouteConfComm.u4Context = u4Context;
    Bgp4CopyNetAddressStruct (&(RouteConfComm.RtNetAddress), RcvdPrefix);
    pRouteConfComm = BGP4_RB_TREE_GET (ROUTES_COMM_ADD_TBL,
                                       (tBgp4RBElem *) (&RouteConfComm));
    if (pRouteConfComm != NULL)
    {
        u1MatchFnd = COMM_TRUE;
    }

    if (u1MatchFnd == COMM_FALSE)
    {
        COMM_ROUTE_CONF_COMM_NODE_CREATE (pRouteConfComm);
        if (pRouteConfComm == NULL)
        {
            return COMM_FAILURE;
        }
        Bgp4CopyNetAddressStruct (&
                                  (COMM_NET_ADDRESS_IN_ROUTE_CONF_STRUCT
                                   (pRouteConfComm)), RcvdPrefix);
        pRouteConfComm->u4Context = u4Context;
        TMO_SLL_Init (&pRouteConfComm->TSConfComms);
        u4Ret = BGP4_RB_TREE_ADD (ROUTES_COMM_ADD_TBL,
                                  (tBgp4RBElem *) pRouteConfComm);
        if (u4Ret == RB_FAILURE)
        {
            return COMM_FAILURE;
        }
    }

    /* Allocate tCommProfile and link it to */
    /* TSConfComms of tRouteConfComm Node   */

    COMM_COMM_PROFILE_NODE_CREATE (pCommProfile);

    if (pCommProfile == NULL)
    {
        return COMM_FAILURE;
    }

    CommCopyCommunityToProfile (pCommProfile, u4Fsbgp4AddCommVal);
    TMO_SLL_Add (&pRouteConfComm->TSConfComms, &pCommProfile->CommProfileNext);
    *ppCommProfile = pCommProfile;
    return COMM_SUCCESS;
}

INT4
CommDeleteAddCommEntry (UINT4 u4Context, tNetAddress RcvdPrefix,
                        UINT4 u4Fsbgp4AddCommVal, tCommProfile ** ppCommProfile)
{
    tRouteConfComm     *pRouteConfComm = NULL;
    tRouteConfComm     *pTmpRouteConfComm = NULL;
    tCommProfile       *pCommProfile = NULL;
    tCommProfile       *pTmpCommProfile = NULL;
    tRouteConfComm      RouteConfComm;
    UINT4               u4Ret;
    UINT1               u1MatchFnd = COMM_FALSE;

    UNUSED_PARAM (ppCommProfile);
    MEMSET (&RouteConfComm, 0, sizeof (tRouteConfComm));
    RouteConfComm.u4Context = u4Context;
    Bgp4CopyNetAddressStruct (&(RouteConfComm.RtNetAddress), RcvdPrefix);
    pRouteConfComm = BGP4_RB_TREE_GET (ROUTES_COMM_ADD_TBL,
                                       (tBgp4RBElem *) (&RouteConfComm));
    if (pRouteConfComm != NULL)
    {
        BGP_SLL_DYN_Scan (&(pRouteConfComm->TSConfComms),
                          pCommProfile, pTmpCommProfile, tCommProfile *)
        {
            u1MatchFnd = (UINT1) CommCmpCommunity (u4Fsbgp4AddCommVal,
                                                   pCommProfile);
            if (u1MatchFnd == COMM_TRUE)
            {
                pTmpRouteConfComm = pRouteConfComm;
                TMO_SLL_Delete (&(pRouteConfComm->TSConfComms),
                                &pCommProfile->CommProfileNext);
                COMM_COMM_PROFILE_NODE_FREE (pCommProfile);
                break;
            }
        }
    }
    else
    {
        return COMM_FAILURE;
    }

    /* Free tRouteConfComm if TSConfComms of tRouteConfComm does not */
    /* contain any CommProfile Node   */
    if (u1MatchFnd == COMM_TRUE)
    {
        if (TMO_SLL_Count (&(pTmpRouteConfComm->TSConfComms)) == 0)
        {
            u4Ret = BGP4_RB_TREE_REMOVE (ROUTES_COMM_ADD_TBL,
                                         (tBgp4RBElem *) pTmpRouteConfComm);
            if (u4Ret == RB_FAILURE)
            {
                return COMM_FAILURE;
            }
            COMM_ROUTE_CONF_COMM_NODE_FREE (pTmpRouteConfComm);
        }
    }

    return COMM_SUCCESS;
}

INT4
CommGetAddCommEntry (UINT4 u4Context, tNetAddress RcvdPrefix,
                     UINT4 u4Fsbgp4AddCommVal, tCommProfile ** ppCommProfile)
{
    tRouteConfComm     *pRouteConfComm = NULL;
    tCommProfile       *pCommProfile = NULL;
    tRouteConfComm      RouteConfComm;
    UINT1               u1MatchFnd = COMM_FALSE;

    MEMSET (&RouteConfComm, 0, sizeof (tRouteConfComm));
    RouteConfComm.u4Context = u4Context;
    Bgp4CopyNetAddressStruct (&(RouteConfComm.RtNetAddress), RcvdPrefix);
    pRouteConfComm = BGP4_RB_TREE_GET (ROUTES_COMM_ADD_TBL,
                                       (tBgp4RBElem *) (&RouteConfComm));
    if (pRouteConfComm != NULL)
    {
        TMO_SLL_Scan (&(pRouteConfComm->TSConfComms),
                      pCommProfile, tCommProfile *)
        {
            u1MatchFnd = (UINT1) CommCmpCommunity (u4Fsbgp4AddCommVal,
                                                   pCommProfile);
            if (u1MatchFnd == COMM_TRUE)
            {
                *ppCommProfile = pCommProfile;
                return COMM_SUCCESS;
            }
        }
    }
    return COMM_FAILURE;
}

INT4
CommCreateDeleteCommEntry (UINT4 u4Context, tNetAddress RcvdPrefix,
                           UINT4 u4Fsbgp4DeleteCommVal,
                           tCommProfile ** ppCommProfile)
{
    tRouteConfComm     *pRouteConfComm = NULL;
    tCommProfile       *pCommProfile = NULL;
    tRouteConfComm      RouteConfComm;
    UINT4               u4Ret;
    UINT1               u1MatchFnd = COMM_FALSE;

    MEMSET (&RouteConfComm, 0, sizeof (tRouteConfComm));
    RouteConfComm.u4Context = u4Context;
    Bgp4CopyNetAddressStruct (&(RouteConfComm.RtNetAddress), RcvdPrefix);
    pRouteConfComm = BGP4_RB_TREE_GET (ROUTES_COMM_DELETE_TBL,
                                       (tBgp4RBElem *) (&RouteConfComm));
    if (pRouteConfComm != NULL)
    {
        u1MatchFnd = COMM_TRUE;
    }

    if (u1MatchFnd == COMM_FALSE)
    {
        COMM_ROUTE_CONF_COMM_NODE_CREATE (pRouteConfComm);
        if (pRouteConfComm == NULL)
        {
            return COMM_FAILURE;
        }
        Bgp4CopyNetAddressStruct (&
                                  (COMM_NET_ADDRESS_IN_ROUTE_CONF_STRUCT
                                   (pRouteConfComm)), RcvdPrefix);
        TMO_SLL_Init (&pRouteConfComm->TSConfComms);
        u4Ret = BGP4_RB_TREE_ADD (ROUTES_COMM_DELETE_TBL,
                                  (tBgp4RBElem *) pRouteConfComm);
        if (u4Ret == RB_FAILURE)
        {
            return COMM_FAILURE;
        }
    }

    /* Allocate tCommProfile and link it to */
    /* TSConfComms of tRouteConfComm Node   */

    COMM_COMM_PROFILE_NODE_CREATE (pCommProfile);
    if (pCommProfile == NULL)
    {
        return COMM_FAILURE;
    }

    CommCopyCommunityToProfile (pCommProfile, u4Fsbgp4DeleteCommVal);

    TMO_SLL_Add (&pRouteConfComm->TSConfComms, &pCommProfile->CommProfileNext);
    *ppCommProfile = pCommProfile;
    return COMM_SUCCESS;
}

INT4
CommDeleteDeleteCommEntry (UINT4 u4Context, tNetAddress RcvdPrefix,
                           UINT4 u4Fsbgp4DeleteCommVal,
                           tCommProfile ** ppCommProfile)
{
    tRouteConfComm     *pRouteConfComm = NULL;
    tRouteConfComm     *pTmpRouteConfComm = NULL;
    tCommProfile       *pCommProfile = NULL;
    tCommProfile       *pTmpCommProfile = NULL;
    tRouteConfComm      RouteConfComm;
    UINT4               u4Ret;
    UINT1               u1MatchFnd = COMM_FALSE;

    UNUSED_PARAM (ppCommProfile);
    MEMSET (&RouteConfComm, 0, sizeof (tRouteConfComm));
    RouteConfComm.u4Context = u4Context;
    Bgp4CopyNetAddressStruct (&(RouteConfComm.RtNetAddress), RcvdPrefix);
    pRouteConfComm = BGP4_RB_TREE_GET (ROUTES_COMM_DELETE_TBL,
                                       (tBgp4RBElem *) (&RouteConfComm));
    if (pRouteConfComm != NULL)
    {
        BGP_SLL_DYN_Scan (&(pRouteConfComm->TSConfComms),
                          pCommProfile, pTmpCommProfile, tCommProfile *)
        {
            u1MatchFnd = (UINT1) CommCmpCommunity (u4Fsbgp4DeleteCommVal,
                                                   pCommProfile);
            if (u1MatchFnd == COMM_TRUE)
            {
                pTmpRouteConfComm = pRouteConfComm;
                TMO_SLL_Delete (&(pRouteConfComm->TSConfComms),
                                &pCommProfile->CommProfileNext);
                COMM_COMM_PROFILE_NODE_FREE (pCommProfile);
                break;
            }
        }

    }
    else
    {
        return COMM_FAILURE;
    }

    /* Free tRouteConfComm if TSConfComms of tRouteConfComm does not */
    /* contain any CommProfile Node   */
    if (u1MatchFnd == COMM_TRUE)
    {
        if (TMO_SLL_Count (&(pTmpRouteConfComm->TSConfComms)) == 0)
        {
            u4Ret = BGP4_RB_TREE_REMOVE (ROUTES_COMM_DELETE_TBL,
                                         (tBgp4RBElem *) pTmpRouteConfComm);
            if (u4Ret == RB_FAILURE)
            {
                return COMM_FAILURE;
            }
            COMM_ROUTE_CONF_COMM_NODE_FREE (pTmpRouteConfComm);
        }
    }
    return COMM_SUCCESS;
}

INT4
CommGetDeleteCommEntry (UINT4 u4Context, tNetAddress RcvdPrefix,
                        UINT4 u4Fsbgp4DeleteCommVal,
                        tCommProfile ** ppCommProfile)
{
    tRouteConfComm     *pRouteConfComm = NULL;
    tCommProfile       *pCommProfile = NULL;
    tRouteConfComm      RouteConfComm;
    UINT1               u1MatchFnd = COMM_FALSE;

    MEMSET (&RouteConfComm, 0, sizeof (tRouteConfComm));
    RouteConfComm.u4Context = u4Context;
    Bgp4CopyNetAddressStruct (&(RouteConfComm.RtNetAddress), RcvdPrefix);
    pRouteConfComm = BGP4_RB_TREE_GET (ROUTES_COMM_DELETE_TBL,
                                       (tBgp4RBElem *) (&RouteConfComm));
    if (pRouteConfComm != NULL)
    {
        TMO_SLL_Scan (&(pRouteConfComm->TSConfComms),
                      pCommProfile, tCommProfile *)
        {
            u1MatchFnd = (UINT1) CommCmpCommunity (u4Fsbgp4DeleteCommVal,
                                                   pCommProfile);
            if (u1MatchFnd == COMM_TRUE)
            {
                *ppCommProfile = pCommProfile;
                return COMM_SUCCESS;
            }
        }
    }
    return COMM_FAILURE;
}

INT4
CommGetRouteCommSetStatusEntry (UINT4 u4Context, tNetAddress RcvdPrefix,
                                tRouteCommSetStatus ** ppRouteCommSetStatus)
{
    tRouteCommSetStatus *pTmpRouteCommSetStatus = NULL;
    tRouteCommSetStatus RouteCommSetStatus;

    MEMSET (&RouteCommSetStatus, 0, sizeof (tRouteCommSetStatus));
    RouteCommSetStatus.u4Context = u4Context;
    Bgp4CopyNetAddressStruct (&(RouteCommSetStatus.RtNetAddress), RcvdPrefix);
    pTmpRouteCommSetStatus = BGP4_RB_TREE_GET (ROUTES_COMM_SET_STATUS_TBL,
                                               (tBgp4RBElem
                                                *) (&RouteCommSetStatus));
    if (pTmpRouteCommSetStatus != NULL)
    {
        *ppRouteCommSetStatus = pTmpRouteCommSetStatus;
        return COMM_SUCCESS;
    }
    /* No set status configured for this route */
    *ppRouteCommSetStatus = NULL;
    return COMM_FAILURE;
}

INT4
CommCreateRouteCommSetStatusEntry (UINT4 u4Context, tNetAddress RcvdPrefix,
                                   tRouteCommSetStatus ** ppRouteCommSetStatus)
{
    INT4                i4GetCommEntrySts;
    UINT4               u4Ret;

    i4GetCommEntrySts =
        CommGetRouteCommSetStatusEntry (u4Context, RcvdPrefix,
                                        ppRouteCommSetStatus);
    if (i4GetCommEntrySts == COMM_SUCCESS)
    {
        /* Entry already present */
        return COMM_FAILURE;
    }

    COMM_ROUTES_COMM_SET_STATUS_ENTRY_CREATE (*ppRouteCommSetStatus);
    if (*ppRouteCommSetStatus == NULL)
    {
        return COMM_FAILURE;
    }

    Bgp4CopyNetAddressStruct (&
                              (COMM_NET_ADDRESS_IN_ROUTE_SET_STATUS_STRUCT
                               ((*ppRouteCommSetStatus))), RcvdPrefix);
    (*ppRouteCommSetStatus)->u4Context = u4Context;
    u4Ret =
        BGP4_RB_TREE_ADD (ROUTES_COMM_SET_STATUS_TBL,
                          (tBgp4RBElem *) (*ppRouteCommSetStatus));
    if (u4Ret == RB_FAILURE)
    {
        return COMM_FAILURE;
    }
    return COMM_SUCCESS;
}

INT4
ExtCommValidateExtCommunityType (UINT2 u2Fsbgp4ExtCommInFilterCommVal)
{
    UINT1               u1HighOrderByte;
    UINT1               u1LowOrderByte;
    UINT1               u1ExtCommValidStatus = EXT_COMM_FALSE;

    u1HighOrderByte = (UINT1) (u2Fsbgp4ExtCommInFilterCommVal >>
                               BGP4_ONE_BYTE_BITS);
    u1LowOrderByte = (UINT1) (u2Fsbgp4ExtCommInFilterCommVal &
                              EXT_COMM_LOW_ORDER_OCTET_MASK);

    if ((u1HighOrderByte >
         EXT_COMM_MIN_VENDOR_DEFINED_HIGH_ORDER_REG_TYPE) &&
        (u1HighOrderByte <= EXT_COMM_MAX_VENDOR_DEFINED_HIGH_ORDER_REG_TYPE))
    {
        /* Type values 0x80-0xbf for regular types         */
        /* (0x8000-0xbfff when expressed as extended types) */
        /* are vendor- specific types                      */
        /* This usugae to be discussed and defined -GANI   */
        u1ExtCommValidStatus = EXT_COMM_TRUE;
    }

#ifdef VPLSADS_WANTED
    if ((u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_80) &&
        (u1LowOrderByte == EXT_COMM_LOW_ORDER_BYTE_TYPE_0A))
    {
        /* this is for Layer 2 PE capability ,send as ext.commnity
           higher order is 0x80 and low order is 0x0a */

        return EXT_COMM_SUCCESS;

    }
#endif

    if ((u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_40) ||
        (u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_00))
    {
        /* This is Two-Octet AS specific extended community.
         *  The low-order byte in this case is 'SubType' as defined
         *  by the draft, so there is no validation required for
         *  low-order byte
         */
        u1ExtCommValidStatus = EXT_COMM_TRUE;
    }
    if ((u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_41) ||
        (u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_01))
    {
        /* This is IPv4 address specific extended community.
         *  The low-order byte in this case is 'SubType' as defined
         *  by the draft, so there is no validation required for
         *  low-order byte
         */
        u1ExtCommValidStatus = EXT_COMM_TRUE;
    }
    if ((u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_42) ||
        (u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_02))
    {
        /* This is Four-octet AS specific extended community.
         *  The low-order byte in this case is 'SubType' as defined
         *  by the draft, so there is no validation required for
         *  low-order byte
         */
        u1ExtCommValidStatus = EXT_COMM_TRUE;
    }
    if ((u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_43) ||
        (u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_03))
    {
        /* This is Opaque extended community.
         *  The low-order byte in this case is 'SubType' as defined
         *  by the draft, so there is no validation required for
         *  low-order byte
         */
        u1ExtCommValidStatus = EXT_COMM_TRUE;
    }

    /* Check for the Route-Target extended Community */
    if (u1LowOrderByte == EXT_COMM_LOW_ORDER_BYTE_TYPE_02)
    {
        if (u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_00)
        {
            /* When the value of the Type field is 0x00, the value
             * of the Local Administrator sub-field in the value field
             * MUST be unique within the AS carried in the Global
             * Administrator sub-field.
             */
            /* Validation should be done, when BGP-MPLS VPN solution
             * is developed
             */
            return EXT_COMM_SUCCESS;
        }
        else if (u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_01)
        {
            /* Need to validate the value field. */
            return EXT_COMM_SUCCESS;
        }
        else if (u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_02)
        {
            /* Need to validate the value field. */
            return EXT_COMM_SUCCESS;
        }
        else if (u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_40)
        {
            /* Need to validate the value field. */
            return EXT_COMM_SUCCESS;
        }
        else if (u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_42)
        {
            /* Need to validate the value field. */
            return EXT_COMM_SUCCESS;
        }
        else
        {
            /* Invalid High order bit for Route-Target extended Communtity. */
            return EXT_COMM_FAILURE;
        }
    }

    /* Check for the Route Origin extended Community */
    if (u1LowOrderByte == EXT_COMM_LOW_ORDER_BYTE_TYPE_03)
    {
        if (u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_00)
        {
            /* When the value of the Type field is 0x00, the value
             * of the Local Administrator sub-field in the value field
             * MUST be unique within the AS carried in the Global
             * Administrator sub-field.
             */
            /* Validation should be done, when BGP-MPLS VPN solution
             * is developed
             */
            return EXT_COMM_SUCCESS;
        }
        else if (u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_01)
        {
            /* Need to validate value field. */
            return EXT_COMM_SUCCESS;
        }
        else if (u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_02)
        {
            /* Need to validate value field. */
            return EXT_COMM_SUCCESS;
        }
        else if (u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_40)
        {
            /* Need to validate the value field. */
            return EXT_COMM_SUCCESS;
        }
        else if (u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_42)
        {
            /* Need to validate the value field. */
            return EXT_COMM_SUCCESS;
        }
        else
        {
            /* Invalid High order bit for Route-Origin extended Communtity. */
            return EXT_COMM_FAILURE;
        }
    }

    /* Check for Link Bandwidth Community */
    if (u1LowOrderByte == EXT_COMM_LOW_ORDER_BYTE_TYPE_04)
    {
        if (u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_40)
        {
            /* Need to validate value field. */
            return EXT_COMM_SUCCESS;
        }
        else
        {
            /* Invalid High order bit for LINK_BANDWIDTH extended Communtity. */
            return EXT_COMM_FAILURE;
        }
    }
#ifdef EVPN_WANTED
    /* Check for EVPN MAC Mobility Ext Community */
    if ( (u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_06) &&
         (u1LowOrderByte == EXT_COMM_LOW_ORDER_BYTE_TYPE_00))
     {
             /* Need to validate value field. */
             return EXT_COMM_SUCCESS;
     }
#endif

    if (u1ExtCommValidStatus == EXT_COMM_FALSE)
    {
        return EXT_COMM_FAILURE;
    }
    else
    {
        return EXT_COMM_SUCCESS;
    }
}

/*****************************************************************************/
/* Function Name : ExtCommValidateExtCommunityVal                            */
/* Description   : This command validates the Global Administrator & Local   */
/*                 Administrator field of the Extended Community Attribute.  */
/*                 As of now, the validation is done for                     */
/*                         1. 4-octet Specific Extended Communities          */
/*                         2. 2-octed Specific Extended Communities          */
/* Input(s)      : Extended Community Value to be validated                  */
/* Output(s)     : None.                                                     */
/* Return(s)     : EXT_COMM_SUCCESS - If the value is valid                  */
/*               : EXT_COMM_FAILURE - If the value is invalid                */
/*****************************************************************************/
INT4
ExtCommValidateExtCommunityVal (tSNMP_OCTET_STRING_TYPE * pFsbgp4AddExtCommVal)
{
    UINT1               u1HighOrderByte, u1LowOrderByte;
    UINT2               u2Fsbgp4ExtCommInFilterCommVal = 0;
    UINT2               u2TwoOctetAsn;
    UINT4               u4FourOctetAsn;
    UINT4               u4Context = 0;

    u4Context = gpBgpCurrCxtNode->u4ContextId;

    PTR_FETCH2 ((u2Fsbgp4ExtCommInFilterCommVal),
                (pFsbgp4AddExtCommVal->pu1_OctetList));

    u1HighOrderByte = (UINT1) (u2Fsbgp4ExtCommInFilterCommVal >>
                               BGP4_ONE_BYTE_BITS);
    u1LowOrderByte = (UINT1) (u2Fsbgp4ExtCommInFilterCommVal &
                              EXT_COMM_LOW_ORDER_OCTET_MASK);

/*  0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   | Type          |   Sub-Type    |    Global Administrator       :
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   : Global Administrator (cont.)  |    Local Administrator        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/

    /* Validation done for 4-octet AS specific ECOMM and 2-octet AS Specific
       ECOMM */
    if (((u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_42) ||
         (u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_02)) &&
        ((u1LowOrderByte == EXT_COMM_LOW_ORDER_BYTE_TYPE_02) ||
         (u1LowOrderByte == EXT_COMM_LOW_ORDER_BYTE_TYPE_03)))
    {
        /* Type         = 0x02 or 0x42
           Sub-type     = 0x02 or 0x03
           Global Admin = 4 byte Local AS No. */
        PTR_FETCH4 (u4FourOctetAsn,
                    pFsbgp4AddExtCommVal->pu1_OctetList +
                    PATH_ATTRIBUTE_TYPE_SIZE);

        if ((BGP4_FOUR_BYTE_ASN_SUPPORT (u4Context) ==
             BGP4_ENABLE_4BYTE_ASN_SUPPORT) &&
            (u4FourOctetAsn == BGP4_LOCAL_AS_NO (u4Context)))
        {
            return EXT_COMM_SUCCESS;
        }
    }

    if (((u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_40) ||
         (u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_00)) &&
        ((u1LowOrderByte == EXT_COMM_LOW_ORDER_BYTE_TYPE_02) ||
         (u1LowOrderByte == EXT_COMM_LOW_ORDER_BYTE_TYPE_03)))
    {
        /* Type         = 0x00 or 0x40
           Sub-type     = 0x02 or 0x03
           Global Admin = 2 byte Local AS No. */
        PTR_FETCH2 (u2TwoOctetAsn,
                    pFsbgp4AddExtCommVal->pu1_OctetList +
                    PATH_ATTRIBUTE_TYPE_SIZE);

        if ((BGP4_FOUR_BYTE_ASN_SUPPORT (u4Context) ==
             BGP4_DISABLE_4BYTE_ASN_SUPPORT) &&
            (u2TwoOctetAsn == BGP4_LOCAL_AS_NO (u4Context)))
        {
            return EXT_COMM_SUCCESS;
        }
    }

    return EXT_COMM_FAILURE;
}

/****************************************************************************
 Function    :  CapsDeletePeerCaps 
 Description :  This function deletes all entries in PeerCapsHashTable and 
                releases the memory allocated for them.
 Input       :  None
 Output      :  None 
 Returns     :  CAPS_SUCCESS or CAPS_FAILURE
****************************************************************************/

INT4
CapsDeletePeerCaps (UINT4 u4Context)
{
    tBgp4PeerEntry     *pPeer = NULL;
    INT4                i4Status = CAPS_FAILURE;

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
    {
        i4Status = CapsDeletePeerCapsInfo (pPeer);
        if (i4Status == CAPS_FAILURE)
        {
            return CAPS_FAILURE;
        }
    }
    return CAPS_SUCCESS;
}

/****************************************************************************
 Function    :  CapsDeleteSpeakerCaps 
 Description :  This function deletes all entries in SpkrCapsHashTable and 
                releases the memory allocated for them.
 Input       :  None
 Output      :  None 
 Returns     :  CAPS_SUCCESS or CAPS_FAILURE
****************************************************************************/

INT4
CapsDeleteSpeakerCaps (UINT4 u4Context)
{
    tBgp4PeerEntry     *pPeer = NULL;
    INT4                i4RetStatus = CAPS_FAILURE;

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
    {
        i4RetStatus = CapsDeleteSpkrCapsInfo (pPeer);
        if (i4RetStatus == CAPS_FAILURE)
        {
            return CAPS_FAILURE;
        }
    }
    return CAPS_SUCCESS;
}

/****************************************************************************
 Function    :  GetFindSpkrSupCap 
 Description :  This function gives the supported capability corresponding to 
                the inputs.
 Input       : pPeerEntry 
                u1CapCode
                u1CapInstance
                u1CapSts
 Output      :  ppSpkrSupCap 
 Returns     :  CAPS_SUCCESS or CAPS_FAILURE
****************************************************************************/
INT4
GetFindSpkrSupCap (tBgp4PeerEntry * pPeerEntry, UINT1 u1CapCode,
                   UINT1 u1CapLength, tSNMP_OCTET_STRING_TYPE * pCapValue,
                   UINT1 u1CapSupRcvd, tSupCapsInfo ** ppSpkrSupCap)
{
    tSupCapsInfo       *pTmpSupCap = NULL;
    tTMO_SLL           *pCapsList = NULL;
    UINT1               tmpFourByteAsnCapValue[CAP_4_BYTE_ASN_LENGTH];

    if (u1CapSupRcvd == CAPS_SUP)
    {
        pCapsList = BGP4_PEER_SUP_CAPS_LIST (pPeerEntry);
    }
    else
    {
        pCapsList = BGP4_PEER_RCVD_CAPS_LIST (pPeerEntry);
    }
    TMO_SLL_Scan (pCapsList, pTmpSupCap, tSupCapsInfo *)
    {
        if (pTmpSupCap->SupCapability.u1CapCode != u1CapCode)
        {
            continue;
        }
        if (pTmpSupCap->SupCapability.u1CapLength != u1CapLength)
        {
            continue;
        }
        if ((u1CapLength > 0) && (u1CapLength < MAX_CAP_LENGTH))
        {
            if ((u1CapSupRcvd == CAPS_RCVD) &&
                (u1CapCode == CAP_CODE_4_OCTET_ASNO))
            {
                PTR_ASSIGN4 (tmpFourByteAsnCapValue,
                             BGP4_PEER_ASNO (pPeerEntry));
                if (MEMCMP (pTmpSupCap->SupCapability.au1CapValue,
                            tmpFourByteAsnCapValue,
                            MEM_MAX_BYTES (sizeof (tmpFourByteAsnCapValue),
                                           u1CapLength)) != 0)
                {
                    continue;
                }
            }
            /* For ORF capability, only Afi, Safi and ORF type needs to be match */
            else if (u1CapCode == CAP_CODE_ORF)
            {
                if (MEMCMP (pTmpSupCap->SupCapability.au1CapValue,
                            pCapValue->pu1_OctetList,
                            BGP_CAP_ORF_MODE_OFFSET) != 0)
                {
                    continue;
                }
            }

            else
            {

                if (MEMCMP (pTmpSupCap->SupCapability.au1CapValue,
                            pCapValue->pu1_OctetList,
                            MEM_MAX_BYTES (sizeof (pCapValue->pu1_OctetList),
                                           u1CapLength)) != 0)
                {
                    continue;
                }
            }
        }
        *ppSpkrSupCap = pTmpSupCap;
        return CAPS_SUCCESS;
    }
    *ppSpkrSupCap = NULL;
    return CAPS_FAILURE;
}

/****************************************************************************
 Function    :  CapsSpkrSupCapCreate 
 Description :  This function creates supported capability corresponding to 
                the inputs.
 Input       :  pPeerEntry
                u1CapCode
 Output      :  ppSpkrSupCap 
 Returns     :  CAPS_SUCCESS or CAPS_FAILURE
****************************************************************************/
INT4
CapsSpkrSupCapCreate (tBgp4PeerEntry * pPeerEntry, UINT1 u1CapCode,
                      UINT1 u1CapLength, tSNMP_OCTET_STRING_TYPE * pCapValue,
                      tSupCapsInfo ** ppSpkrSupCap)
{
    tSupCapsInfo       *pPrevSpkrSupCap = NULL;
    tSupCapsInfo       *pSpkrSupCap = NULL;
    UINT1               u1ToInsert = OSIX_FALSE;

    GetFindSpkrSupCap (pPeerEntry, u1CapCode, u1CapLength,
                       pCapValue, CAPS_SUP, &pSpkrSupCap);
    if (pSpkrSupCap != NULL)
    {
        if (BGP4_GET_NODE_STATUS == RM_STANDBY)
        {
            *ppSpkrSupCap = pSpkrSupCap;
            return CAPS_SUCCESS;
        }
        else
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "ERROR - CapsSpkrSupCapCreate Capability already present\n");
            return CAPS_FAILURE;
        }
    }

    SPKR_SUP_CAPS_ENTRY_CREATE (*ppSpkrSupCap);
    if (*ppSpkrSupCap == NULL)
    {
        return CAPS_FAILURE;
    }
    TMO_SLL_Init_Node (&((*ppSpkrSupCap)->NextCapability));
    (*ppSpkrSupCap)->SupCapability.u1CapCode = u1CapCode;
    (*ppSpkrSupCap)->SupCapability.u1CapLength = u1CapLength;
    BGP4_PEER_SUP_CAPS_ANCD_STATUS ((*ppSpkrSupCap)) = CAPS_FALSE;
    if ((u1CapLength > 0) && (u1CapLength < MAX_CAP_LENGTH))
    {
        MEMCPY (((*ppSpkrSupCap)->SupCapability.au1CapValue),
                pCapValue->pu1_OctetList, MEM_MAX_BYTES (u1CapLength,
                                                         pCapValue->i4_Length));
    }

    TMO_SLL_Scan (BGP4_PEER_SUP_CAPS_LIST (pPeerEntry), pSpkrSupCap,
                  tSupCapsInfo *)
    {
        if (pSpkrSupCap->SupCapability.u1CapCode > u1CapCode)
        {
            u1ToInsert = OSIX_TRUE;
            break;
        }
        else if (pSpkrSupCap->SupCapability.u1CapCode == u1CapCode)
        {
            if (pSpkrSupCap->SupCapability.u1CapLength > u1CapLength)
            {
                u1ToInsert = OSIX_TRUE;
                break;
            }
            else if (pSpkrSupCap->SupCapability.u1CapLength == u1CapLength)
            {
                if ((pSpkrSupCap->SupCapability.u1CapLength > 0) &&
                    (pSpkrSupCap->SupCapability.u1CapLength <= MAX_CAP_LENGTH))
                {
                    if (MEMCMP (pSpkrSupCap->SupCapability.au1CapValue,
                                pCapValue->pu1_OctetList,
                                MEM_MAX_BYTES (u1CapLength,
                                               pCapValue->i4_Length)) > 0)
                    {
                        u1ToInsert = OSIX_TRUE;
                        break;
                    }
                }
            }
        }
        pPrevSpkrSupCap = pSpkrSupCap;
    }
    if (u1ToInsert == OSIX_TRUE)
    {
        TMO_SLL_Insert (BGP4_PEER_SUP_CAPS_LIST (pPeerEntry),
                        &pPrevSpkrSupCap->NextCapability,
                        &((*ppSpkrSupCap)->NextCapability));
    }
    else
    {
        TMO_SLL_Add (BGP4_PEER_SUP_CAPS_LIST (pPeerEntry),
                     &((*ppSpkrSupCap)->NextCapability));
    }
    return CAPS_SUCCESS;
}

/****************************************************************************
 Function    :  Bgp4GetPeerGroupEntry
 Description :  This function obtains the peer group entry for the given peer 
                group name.
 Input       :  Peer group name.
 Output      :  None
 Returns     :  Pointer to peer group entry 
****************************************************************************/
tBgpPeerGroupEntry *
Bgp4GetPeerGroupEntry (UINT4 u4Context, UINT1 *pu1PeerGroup)
{
    tBgpPeerGroupEntry *pPeerGroup = NULL;
    INT4                i4PeerGrpNameLen = STRLEN (pu1PeerGroup);

    TMO_SLL_Scan (BGP4_PEERGROUPENTRY_HEAD (u4Context), pPeerGroup,
                  tBgpPeerGroupEntry *)
    {
        if ((INT4) STRLEN (pPeerGroup->au1PeerGroupName) == i4PeerGrpNameLen)
        {
            if (MEMCMP
                (pPeerGroup->au1PeerGroupName, pu1PeerGroup,
                 i4PeerGrpNameLen) == 0)
            {
                break;
            }
        }
    }
    return pPeerGroup;
}

/****************************************************************************
 Function    :  Bgp4CreatePeerGroup  
 Description :  This function adds the peer group to the global peer group list 
 Input       :  Peer group name.
 Output      :  None
 Returns     :  Pointer to peer group entry 
****************************************************************************/
tBgpPeerGroupEntry *
Bgp4CreatePeerGroup (UINT4 u4Context, tSNMP_OCTET_STRING_TYPE * pPeerGroupName)
{
    tBgpPeerGroupEntry *pPeerGroup = NULL;
    tBgpPeerGroupEntry *pTmpPeerGrp = NULL;
    tBgpPeerGroupEntry *pPrevPeerGrp = NULL;
    UINT1              *pu1Block = NULL;

    if (MemAllocateMemBlock (gBgpNode.Bgp4PeerGroupPoolId, &pu1Block)
        == MEM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "Unable to allocate memory for the peer group\n");
        return (NULL);
    }
    pPeerGroup = (tBgpPeerGroupEntry *) (VOID *) pu1Block;

    MEMSET (pPeerGroup, 0, sizeof (tBgpPeerGroupEntry));
    MEMCPY (pPeerGroup->au1PeerGroupName, pPeerGroupName->pu1_OctetList,
            pPeerGroupName->i4_Length);

    /* Initialize the peer group entry */
    TMO_SLL_Init_Node (&(pPeerGroup->NextGroup));
    TMO_SLL_Init (&(pPeerGroup->PeerList));
    pPeerGroup->u4SendBuf = BGP4_DEFAULT_SENDBUF;
    pPeerGroup->u4RecvBuf = BGP4_DEFAULT_RECVBUF;
    pPeerGroup->u4ConnectRetryInterval = BGP4_DEF_CONNRETRYINTERVAL;
    pPeerGroup->u1ConnectCount = 0;
    pPeerGroup->u4HoldInterval = BGP4_DEF_HOLDINTERVAL;
    pPeerGroup->u4DelayOpenTimeInterval = BGP4_DEF_DELAYOPENINTERVAL;
    pPeerGroup->u4IdleHoldTimeInterval = BGP4_DEF_IDLEHOLDINTERVAL;
    pPeerGroup->u4KeepAliveInterval = BGP4_DEF_KEEPALIVEINTERVAL;
    pPeerGroup->u4MinASOrigInterval = BGP4_DEF_MINASORIGINTERVAL;
    pPeerGroup->u4MinRouteAdvInterval = BGP4_DEF_EBGP_MINROUTEADVINTERVAL;
    pPeerGroup->u1Passive = BGP4_PASSIVE_DISABLE;
    pPeerGroup->u1ConnectRetryCount = BGP4_DEF_CONNECT_RETRY_COUNT;
    pPeerGroup->u1HopLimit = BGP4_DEFAULT_HOPLIMIT;
    pPeerGroup->u4PeerPrefixUpperLimit = BGP4_DEF_MAX_PEER_PREFIX_LIMIT;
    pPeerGroup->u1AutomaticStart = BGP4_PEER_AUTOMATICSTART_DISABLE;
    pPeerGroup->u1AutomaticStop = BGP4_PEER_AUTOMATICSTOP_DISABLE;
    pPeerGroup->u1DelayOpenStatus = BGP4_PEER_DELAY_OPEN_DISABLE;
    pPeerGroup->u1DampPeerOscillationStatus =
        BGP4_DAMP_PEER_OSCILLATIONS_DISABLE;
    pPeerGroup->u1EBGPMultiHop = BGP4_EBGP_MULTI_HOP_DISABLE;
    pPeerGroup->u1SelfNextHop = BGP4_DISABLE_NEXTHOP_SELF;
    pPeerGroup->u1RflClient = NON_CLIENT;
    pPeerGroup->u1CommSendStatus = BGP4_TRUE;
    pPeerGroup->u1ECommSendStatus = BGP4_TRUE;
    pPeerGroup->u1DftRouteOrigStatus = BGP4_DEF_ROUTE_ORIG_DISABLE;
    pPeerGroup->u1PeerGrpStatus = ACTIVE;
    pPeerGroup->u1BfdStatus = BGP4_BFD_DISABLE;
    pPeerGroup->u1OverrideCap = BGP4_DISABLE_OVERRIDE_CAPABILITY;
    pPeerGroup->u4LocalASNo = BGP4_LOCAL_AS_NO (u4Context);
    pPeerGroup->u1PeerGrpType = BGP4_EXT_OR_INT_PEER;
    pPeerGroup->u1LocalASConfig = BGP4_FALSE;
#ifdef ROUTEMAP_WANTED
    MEMSET (&(pPeerGroup->RMapIn), 0, sizeof (tFilteringRMap));
    MEMSET (&(pPeerGroup->RMapOut), 0, sizeof (tFilteringRMap));
    MEMSET (&(pPeerGroup->IpPrefixFilterIn), 0, sizeof (tFilteringRMap));
    MEMSET (&(pPeerGroup->IpPrefixFilterOut), 0, sizeof (tFilteringRMap));
#endif

    /* Add the peer group to the global list */
    TMO_SLL_Scan (BGP4_PEERGROUPENTRY_HEAD (u4Context), pTmpPeerGrp,
                  tBgpPeerGroupEntry *)
    {
        if ((INT4) STRLEN (pTmpPeerGrp->au1PeerGroupName) >
            pPeerGroupName->i4_Length)
        {
            break;
        }
        else if ((INT4) STRLEN (pTmpPeerGrp->au1PeerGroupName) <
                 pPeerGroupName->i4_Length)
        {
            pPrevPeerGrp = pTmpPeerGrp;
            continue;
        }
        if (MEMCMP (pTmpPeerGrp->au1PeerGroupName,
                    pPeerGroup->au1PeerGroupName,
                    pPeerGroupName->i4_Length) > 0)
        {
            break;
        }
        pPrevPeerGrp = pTmpPeerGrp;
    }
    if (pPrevPeerGrp == NULL)
    {
        TMO_SLL_Insert (BGP4_PEERGROUPENTRY_HEAD (u4Context), NULL,
                        &(pPeerGroup->NextGroup));
    }
    else
    {
        TMO_SLL_Insert (BGP4_PEERGROUPENTRY_HEAD (u4Context),
                        &(pPrevPeerGrp->NextGroup), &(pPeerGroup->NextGroup));
    }

    return pPeerGroup;
}

/****************************************************************************
 Function    :  Bgp4CreatePeerGroup  
 Description :  This function adds the peer group to the global peer group list 
 Input       :  Peer group name.
 Output      :  None
 Returns     :  Pointer to peer group entry 
****************************************************************************/
INT4
Bgp4DeletePeerGroup (UINT4 u4Context, tBgpPeerGroupEntry * pPeerGroup)
{
    tBgp4QMsg          *pQMsg = NULL;

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        return BGP4_FAILURE;
    }

    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
    BGP4_INPUTQ_CXT (pQMsg) = u4Context;
    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_SNMP_PEER_GROUP_DELETE_EVENT;
    MEMSET (BGP4_INPUTQ_PEER_GROUP_NAME (pQMsg),0,BGP_MAX_PEER_GROUP_NAME);
    MEMCPY (BGP4_INPUTQ_PEER_GROUP_NAME (pQMsg), pPeerGroup->au1PeerGroupName,
            STRLEN (pPeerGroup->au1PeerGroupName));
    /* Send the peer entry to the bgp-task */
    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/****************************************************************************
 Function    :  Bgp4RestorePeerPropertyFromShadowPeer 
 Description :  This function restores peer attributes from shadow peer
		configuration to running configuration.
 Input       :  pPeerEntry
 Output      :  None
 Returns     :  VOID
****************************************************************************/
VOID
Bgp4RestorePeerPropertyFromShadowPeer (tBgp4PeerEntry * pPeerEntry)
{
    UINT4       u4Context = 0;
    INT4        i4TableObject = 0;

	BGP4_PEER_SENDBUF(pPeerEntry) = BGP4_PEER_SHADOW_SEND_BUF(pPeerEntry);
	BGP4_PEER_RECVBUF(pPeerEntry) = BGP4_PEER_SHADOW_RECV_BUF(pPeerEntry);
	BGP4_PEER_CONN_RETRY_TIME(pPeerEntry) = BGP4_PEER_SHADOW_CONNECT_RETRY_INTERVAL(pPeerEntry);
	BGP4_PEER_HOLD_TIME(pPeerEntry) = BGP4_PEER_SHADOW_HOLD_INTERVAL(pPeerEntry);
	BGP4_PEER_DELAY_OPEN_TIME(pPeerEntry) = BGP4_PEER_SHADOW_DELAY_OPEN_TIME_INTERVAL(pPeerEntry);
	BGP4_PEER_KEEP_ALIVE_TIME(pPeerEntry) = BGP4_PEER_SHADOW_KEEP_ALIVE_INTERVAL(pPeerEntry);
	BGP4_PEER_MIN_AS_ORIG_TIME(pPeerEntry) = BGP4_PEER_SHADOW_MIN_AS_ORIG_INTERVAL(pPeerEntry);
	BGP4_PEER_MIN_ADV_TIME(pPeerEntry) = BGP4_PEER_SHADOW_MIN_ROUTE_ADV_INTERVAL(pPeerEntry);
	BGP4_PEER_PREFIX_UPPER_LIMIT(pPeerEntry) = BGP4_PEER_SHADOW_PEER_PREFIX_UPPER_LIMIT(pPeerEntry);
	BGP4_PEER_IDLE_HOLD_TIME(pPeerEntry) = BGP4_PEER_SHADOW_IDLE_HOLDTIME_INTERVAL(pPeerEntry);
	BGP4_PEER_ASNO(pPeerEntry) = BGP4_PEER_SHADOW_REMOTE_AS_NO(pPeerEntry);
	BGP4_PEER_LOCAL_AS(pPeerEntry) = BGP4_PEER_SHADOW_LOCAL_AS_NO(pPeerEntry);
	BGP4_PEER_CONN_PASSIVE(pPeerEntry) = BGP4_PEER_SHADOW_PASSIVE(pPeerEntry);
	BGP4_PEER_TCP_CONFIGURED_CONNECT_COUNT(pPeerEntry) = BGP4_PEER_SHADOW_CONNECT_RETRY_COUNT(pPeerEntry);
	BGP4_PEER_HOPLIMIT(pPeerEntry) = BGP4_PEER_SHADOW_HOP_LIMIT(pPeerEntry);
	BGP4_PEER_ALLOW_AUTOMATIC_START(pPeerEntry) = BGP4_PEER_SHADOW_AUTO_START(pPeerEntry);
	BGP4_PEER_ALLOW_AUTOMATIC_STOP(pPeerEntry) = BGP4_PEER_SHADOW_AUTO_STOP(pPeerEntry);
	BGP4_PEER_DELAY_OPEN(pPeerEntry) = BGP4_PEER_SHADOW_DELAY_OPEN_STATUS(pPeerEntry);
	BGP4_DAMP_PEER_OSCILLATIONS(pPeerEntry) = BGP4_PEER_SHADOW_DAMP_PEER_OSC_STATUS(pPeerEntry);
	BGP4_PEER_EBGP_MULTIHOP(pPeerEntry) = BGP4_PEER_SHADOW_EBGP_MULTIHOP(pPeerEntry);
	BGP4_PEER_SELF_NEXTHOP(pPeerEntry) = BGP4_PEER_SHADOW_SELF_NEXTHOP(pPeerEntry);
	BGP4_PEER_RFL_CLIENT(pPeerEntry) = BGP4_PEER_SHADOW_RFL_CLIENT(pPeerEntry);
	BGP4_PEER_COMM_SEND_STATUS(pPeerEntry) = BGP4_PEER_SHADOW_COMM_SEND_STATUS(pPeerEntry);
	BGP4_PEER_ECOMM_SEND_STATUS(pPeerEntry) = BGP4_PEER_SHADOW_ECOMM_SEND_STATUS(pPeerEntry);
	BGP4_PEER_DEF_ROUTE_ORIG_STATUS(pPeerEntry) = BGP4_PEER_SHADOW_DFT_ROUTE_ORIG_STATUS(pPeerEntry);
	BGP4_PEER_LOCAL_AS_CFG(pPeerEntry) = BGP4_PEER_SHADOW_LOCAL_AS_CONFIG(pPeerEntry);
	BGP4_PEER_OVERRIDE_CAPABILITY(pPeerEntry) = BGP4_PEER_SHADOW_OVERRIDE_CAP(pPeerEntry);
	BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT(pPeerEntry) = BGP4_PEER_SHADOW_CONNECT_COUNT(pPeerEntry);
    pPeerEntry->u1BfdStatus = BGP4_PEER_SHADOW_BFD_STATUS(pPeerEntry);
    if (pPeerEntry->u1BfdStatus == BGP4_BFD_DISABLE)
    {
        Bgp4DeRegisterWithBfd (BGP4_PEER_CXT_ID (pPeerEntry),pPeerEntry, TRUE);
    }
#ifdef ROUTEMAP_WANTED
	MEMCPY(&(pPeerEntry->FilterInRMap),&(pPeerEntry->shadowPeerConfig.FilterInRMap),
		sizeof (tFilteringRMap));
	MEMCPY(&(pPeerEntry->FilterOutRMap),&(pPeerEntry->shadowPeerConfig.FilterOutRMap),
		sizeof (tFilteringRMap));
	MEMCPY(&(pPeerEntry->IpPrefixFilterIn),&(pPeerEntry->shadowPeerConfig.IpPrefixFilterIn),
		sizeof (tFilteringRMap));
	MEMCPY(&(pPeerEntry->IpPrefixFilterOut),&(pPeerEntry->shadowPeerConfig.IpPrefixFilterOut),
		sizeof (tFilteringRMap));
#endif

   u4Context = BGP4_PEER_CXT_ID (pPeerEntry);
   i4TableObject = Bgp4SnmphGetRtRefCapSupportStatus (u4Context, BGP4_PEER_REMOTE_ADDR_INFO
           (pPeerEntry));

   /* Restore capability to the peer configured setting */
   if ((i4TableObject == BGP4_TRUE) &&
           ((BGP4_PEER_SHADOW_MP_CAP_MASK (pPeerEntry) & CAP_NEG_ROUTE_REFRESH_MASK)
            != CAP_NEG_ROUTE_REFRESH_MASK))
   {
       /* Disable Route Refresh Capability */
       Bgp4SnmphRtRefCapabilityDisable (u4Context,
               BGP4_PEER_REMOTE_ADDR_INFO
               (pPeerEntry));
   }

   if (BGP4_PEER_SHADOW_ORF_MODE (pPeerEntry) == ZERO)
   {
       /* Disable ORF if enabled through Peer Group */
       Bgp4SnmphOrfCapabilityDisable
           (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry),
            BGP4_PEER_REMOTE_ADDR_TYPE (pPeerEntry),
            BGP4_INET_SAFI_UNICAST, BGP_ORF_TYPE_ADDRESS_PREFIX, BGP4_CLI_ORF_MODE_SEND);
       Bgp4SnmphOrfCapabilityDisable
           (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry),
            BGP4_PEER_REMOTE_ADDR_TYPE (pPeerEntry),
            BGP4_INET_SAFI_UNICAST, BGP_ORF_TYPE_ADDRESS_PREFIX, BGP4_CLI_ORF_MODE_RECEIVE);
   }
   else
   {
       /* Enable ORF based on shadow Peer config */
       Bgp4SnmphOrfCapabilityEnable
           (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry),
            BGP4_PEER_REMOTE_ADDR_TYPE (pPeerEntry),
            BGP4_INET_SAFI_UNICAST, BGP_ORF_TYPE_ADDRESS_PREFIX, BGP4_PEER_SHADOW_ORF_MODE (pPeerEntry));
   }

   /* If 4-byte ASN Support status is enabled,
    * then add 4-byte ASN capability to
    * BGP4_PEER_SUP_CAPS_LIST */
   if (BGP4_FOUR_BYTE_ASN_SUPPORT (u4Context) ==
           BGP4_ENABLE_4BYTE_ASN_SUPPORT)
   {
       Bgp4Snmph4ByteAsnCapabilityEnable (u4Context,
               BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));
   }

	return;
}

/****************************************************************************
 Function    :  Bgp4InitShadowPeerProperty
 Description :  This function intializes peer attributes in shadow peer structure
                based on running peer attributes.
 Input       :  pPeerEntry
 Output      :  None
 Returns     :  VOID
****************************************************************************/
VOID
Bgp4InitShadowPeerProperty (tBgp4PeerEntry * pPeerEntry)
{
	BGP4_PEER_SHADOW_SEND_BUF(pPeerEntry) = BGP4_PEER_SENDBUF(pPeerEntry);
	BGP4_PEER_SHADOW_RECV_BUF(pPeerEntry) = BGP4_PEER_RECVBUF(pPeerEntry);
	BGP4_PEER_SHADOW_CONNECT_RETRY_INTERVAL(pPeerEntry) = BGP4_PEER_CONN_RETRY_TIME(pPeerEntry);
	BGP4_PEER_SHADOW_HOLD_INTERVAL(pPeerEntry) = BGP4_PEER_HOLD_TIME(pPeerEntry);
	BGP4_PEER_SHADOW_DELAY_OPEN_TIME_INTERVAL(pPeerEntry) = BGP4_PEER_DELAY_OPEN_TIME(pPeerEntry);
	BGP4_PEER_SHADOW_KEEP_ALIVE_INTERVAL(pPeerEntry) = BGP4_PEER_KEEP_ALIVE_TIME(pPeerEntry);
	BGP4_PEER_SHADOW_MIN_AS_ORIG_INTERVAL(pPeerEntry) = BGP4_PEER_MIN_AS_ORIG_TIME(pPeerEntry);
	BGP4_PEER_SHADOW_MIN_ROUTE_ADV_INTERVAL(pPeerEntry) = BGP4_PEER_MIN_ADV_TIME(pPeerEntry);
	BGP4_PEER_SHADOW_PEER_PREFIX_UPPER_LIMIT(pPeerEntry) = BGP4_PEER_PREFIX_UPPER_LIMIT(pPeerEntry);
	BGP4_PEER_SHADOW_IDLE_HOLDTIME_INTERVAL(pPeerEntry) = BGP4_PEER_IDLE_HOLD_TIME(pPeerEntry);
	BGP4_PEER_SHADOW_REMOTE_AS_NO(pPeerEntry) = BGP4_PEER_ASNO(pPeerEntry);
	BGP4_PEER_SHADOW_LOCAL_AS_NO(pPeerEntry) = BGP4_PEER_LOCAL_AS(pPeerEntry);
	BGP4_PEER_SHADOW_PASSIVE(pPeerEntry) = BGP4_PEER_CONN_PASSIVE(pPeerEntry);
	BGP4_PEER_SHADOW_CONNECT_RETRY_COUNT(pPeerEntry) = BGP4_PEER_TCP_CONFIGURED_CONNECT_COUNT(pPeerEntry);
	BGP4_PEER_SHADOW_HOP_LIMIT(pPeerEntry) = BGP4_PEER_HOPLIMIT(pPeerEntry);
	BGP4_PEER_SHADOW_AUTO_START(pPeerEntry) = BGP4_PEER_ALLOW_AUTOMATIC_START(pPeerEntry);
	BGP4_PEER_SHADOW_AUTO_STOP(pPeerEntry) = BGP4_PEER_ALLOW_AUTOMATIC_STOP(pPeerEntry);
	BGP4_PEER_SHADOW_DELAY_OPEN_STATUS(pPeerEntry) = BGP4_PEER_DELAY_OPEN(pPeerEntry);
	BGP4_PEER_SHADOW_DAMP_PEER_OSC_STATUS(pPeerEntry) = BGP4_DAMP_PEER_OSCILLATIONS(pPeerEntry);
	BGP4_PEER_SHADOW_EBGP_MULTIHOP(pPeerEntry) = BGP4_PEER_EBGP_MULTIHOP(pPeerEntry);
	BGP4_PEER_SHADOW_SELF_NEXTHOP(pPeerEntry) = BGP4_PEER_SELF_NEXTHOP(pPeerEntry);
	BGP4_PEER_SHADOW_RFL_CLIENT(pPeerEntry) = BGP4_PEER_RFL_CLIENT(pPeerEntry);
	BGP4_PEER_SHADOW_COMM_SEND_STATUS(pPeerEntry) = BGP4_PEER_COMM_SEND_STATUS(pPeerEntry);
	BGP4_PEER_SHADOW_ECOMM_SEND_STATUS(pPeerEntry) = BGP4_PEER_ECOMM_SEND_STATUS(pPeerEntry);
	BGP4_PEER_SHADOW_DFT_ROUTE_ORIG_STATUS(pPeerEntry) = BGP4_PEER_DEF_ROUTE_ORIG_STATUS(pPeerEntry);
	BGP4_PEER_SHADOW_LOCAL_AS_CONFIG(pPeerEntry) = BGP4_PEER_LOCAL_AS_CFG(pPeerEntry);
	BGP4_PEER_SHADOW_OVERRIDE_CAP(pPeerEntry) = BGP4_PEER_OVERRIDE_CAPABILITY(pPeerEntry);
	BGP4_PEER_SHADOW_CONNECT_COUNT(pPeerEntry) = BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT(pPeerEntry);
	BGP4_PEER_SHADOW_MP_CAP_MASK(pPeerEntry) = ZERO;
    BGP4_PEER_SHADOW_ORF_MODE(pPeerEntry) = ZERO;
    BGP4_PEER_SHADOW_BFD_STATUS(pPeerEntry) = BGP4_BFD_DISABLE;
#ifdef ROUTEMAP_WANTED
	MEMCPY (&(pPeerEntry->shadowPeerConfig.FilterInRMap), &(pPeerEntry->FilterInRMap), sizeof (tFilteringRMap));
	MEMCPY (&(pPeerEntry->shadowPeerConfig.FilterOutRMap), &(pPeerEntry->FilterOutRMap), sizeof (tFilteringRMap));
	MEMCPY (&(pPeerEntry->shadowPeerConfig.IpPrefixFilterIn), &(pPeerEntry->IpPrefixFilterIn), sizeof (tFilteringRMap));
	MEMCPY (&(pPeerEntry->shadowPeerConfig.IpPrefixFilterOut), &(pPeerEntry->IpPrefixFilterOut), sizeof (tFilteringRMap));
#endif
	return;
}

/****************************************************************************
 Function    :  Bgp4CopyPeerGroupPropertyToPeer
 Description :  This function copies the mentioned peer group property to the
                given peer 
 Input       :  Peer group entry
                Peer entry
                Object to be copied.
 Output      :  None
 Returns     :  BGP4_SUCCESS/BGP4_FAILURE   
****************************************************************************/
INT4
Bgp4CopyPeerGroupPropertyToPeer (tBgpPeerGroupEntry * pPeerGroup,
                                 tBgp4PeerEntry * pPeerEntry, INT4 i4ObjectType)
{
    tBgp4QMsg          *pQMsg = NULL;
    INT4                i4RetVal = 0;
    UINT1               u1IsDeactive = BGP4_FALSE;
    UINT4               u4Context = 0;
    UINT1               u1SupportStatus;
    tSupCapsInfo       *pSpkrSupCap = NULL;
    u4Context = BGP4_PEER_CXT_ID (pPeerEntry);
    switch (i4ObjectType)
    {
        case BGP4_ALL_PEER_GRP_PROP_CONF:
            if (BGP4_PEER_ASNO (pPeerEntry) == 0)
            {
                BGP4_PEER_ASNO (pPeerEntry) = pPeerGroup->u4RemoteASNo;
                if (BGP4_PEER_ASNO (pPeerEntry) == BGP4_LOCAL_AS_NO (u4Context))
                {
                     BGP4_PEER_SHADOW_MIN_ROUTE_ADV_INTERVAL (pPeerEntry) = BGP4_DEF_IBGP_MINROUTEADVINTERVAL;
                }

                /* check if Confed is configured  and mark, if peer 
                 * is confed peer */
                if (BGP4_CONFED_ID (u4Context) != BGP4_INV_AS)
                {
                    UINT4               u4Indx;

                    for (u4Indx = 0; u4Indx < BGP4_CONFED_MAX_MEMBER_AS;
                         u4Indx++)
                    {
                        if (BGP4_PEER_ASNO (pPeerEntry) ==
                            BGP4_CONFED_PEER (u4Context, u4Indx))
                        {
                            BGP4_CONFED_PEER_STATUS (pPeerEntry) = BGP4_TRUE;
                            break;
                        }
                    }
                }
            }
            if (BGP4_PEER_SHADOW_BFD_STATUS (pPeerEntry)== BGP4_BFD_DISABLE)
            {
                 pPeerEntry->u1BfdStatus = pPeerGroup->u1BfdStatus;
            }
            if (BGP4_PEER_SHADOW_LOCAL_AS_NO (pPeerEntry) == BGP4_INV_AS) 
            {
                BGP4_PEER_LOCAL_AS(pPeerEntry) = pPeerGroup->u4LocalASNo;
            }
            if (BGP4_PEER_SHADOW_LOCAL_AS_CONFIG (pPeerEntry) == BGP4_FALSE)
            {
                BGP4_PEER_LOCAL_AS_CFG (pPeerEntry) = pPeerGroup->u1LocalASConfig;
            }
            if (BGP4_PEER_SHADOW_EBGP_MULTIHOP (pPeerEntry) == BGP4_EBGP_MULTI_HOP_DISABLE)
            {
            BGP4_PEER_EBGP_MULTIHOP (pPeerEntry) = pPeerGroup->u1EBGPMultiHop;
            }
            if (BGP4_PEER_SHADOW_SELF_NEXTHOP (pPeerEntry) == BGP4_DISABLE_NEXTHOP_SELF)
            {
            BGP4_PEER_SELF_NEXTHOP (pPeerEntry) = pPeerGroup->u1SelfNextHop;
            }
            if (BGP4_PEER_SHADOW_OVERRIDE_CAP (pPeerEntry) == BGP4_DISABLE_OVERRIDE_CAPABILITY)
            {
            BGP4_PEER_OVERRIDE_CAPABILITY (pPeerEntry) =
                pPeerGroup->u1OverrideCap;
            }
            if (BGP4_PEER_SHADOW_HOP_LIMIT (pPeerEntry) == BGP4_DEFAULT_HOPLIMIT)
            {
            BGP4_PEER_HOPLIMIT (pPeerEntry) = pPeerGroup->u1HopLimit;
            }
            if (BGP4_PEER_SHADOW_SEND_BUF (pPeerEntry) == BGP4_DEFAULT_SENDBUF)
            {
            BGP4_PEER_SENDBUF (pPeerEntry) = pPeerGroup->u4SendBuf;
            }
            if (BGP4_PEER_SHADOW_RECV_BUF (pPeerEntry) == BGP4_DEFAULT_RECVBUF)
            {
            BGP4_PEER_RECVBUF (pPeerEntry) = pPeerGroup->u4RecvBuf;
            }
            if (BGP4_PEER_SHADOW_COMM_SEND_STATUS (pPeerEntry) == BGP4_TRUE)
            {
                BGP4_PEER_COMM_SEND_STATUS (pPeerEntry) = pPeerGroup->u1CommSendStatus;
            }
            if (BGP4_PEER_SHADOW_ECOMM_SEND_STATUS (pPeerEntry) == BGP4_TRUE)
            {
                BGP4_PEER_ECOMM_SEND_STATUS (pPeerEntry) = pPeerGroup->u1ECommSendStatus;
            }
            if (BGP4_PEER_SHADOW_PASSIVE (pPeerEntry) == BGP4_FALSE)
            {
            BGP4_PEER_CONN_PASSIVE (pPeerEntry) = pPeerGroup->u1Passive;
            }
            if (BGP4_PEER_SHADOW_DFT_ROUTE_ORIG_STATUS (pPeerEntry) == BGP4_DEF_ROUTE_ORIG_DISABLE)
            {
            BGP4_PEER_DEF_ROUTE_ORIG_STATUS (pPeerEntry)
                = pPeerGroup->u1DftRouteOrigStatus;
            }
            if (BGP4_PEER_SHADOW_RFL_CLIENT (pPeerEntry) == NON_CLIENT)
            {
            BGP4_PEER_RFL_CLIENT (pPeerEntry) = pPeerGroup->u1RflClient;
            }
            if (BGP4_PEER_SHADOW_HOLD_INTERVAL (pPeerEntry) == BGP4_DEF_HOLDINTERVAL)
            {
            BGP4_PEER_HOLD_TIME (pPeerEntry) = pPeerGroup->u4HoldInterval;
            }
            if (BGP4_PEER_SHADOW_KEEP_ALIVE_INTERVAL (pPeerEntry) == BGP4_DEF_KEEPALIVEINTERVAL)
            {
            BGP4_PEER_KEEP_ALIVE_TIME (pPeerEntry) =
                pPeerGroup->u4KeepAliveInterval;
            }
            if (BGP4_PEER_SHADOW_CONNECT_RETRY_INTERVAL (pPeerEntry) == BGP4_DEF_CONNRETRYINTERVAL)
            {
            BGP4_PEER_CONN_RETRY_TIME (pPeerEntry) =
                pPeerGroup->u4ConnectRetryInterval;
            }
            if (BGP4_PEER_SHADOW_CONNECT_RETRY_COUNT (pPeerEntry) == 0)
            {
            BGP4_PEER_TCP_CONFIGURED_CONNECT_COUNT(pPeerEntry) = 
                pPeerGroup->u1ConnectCount;
            }
            if (BGP4_PEER_SHADOW_MIN_AS_ORIG_INTERVAL (pPeerEntry) == BGP4_DEF_MINASORIGINTERVAL)
            {
            BGP4_PEER_MIN_AS_ORIG_TIME (pPeerEntry) =
                pPeerGroup->u4MinASOrigInterval;
            }
            if ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerEntry), pPeerEntry) ==
                 BGP4_EXTERNAL_PEER))
            {
	    	if (BGP4_PEER_SHADOW_MIN_ROUTE_ADV_INTERVAL (pPeerEntry) == BGP4_DEF_EBGP_MINROUTEADVINTERVAL)
            	{
            BGP4_PEER_MIN_ADV_TIME (pPeerEntry) =
                pPeerGroup->u4MinRouteAdvInterval;
            	}
            }
            else
            {
                if (BGP4_PEER_SHADOW_MIN_ROUTE_ADV_INTERVAL (pPeerEntry) == BGP4_DEF_IBGP_MINROUTEADVINTERVAL)
                {
            BGP4_PEER_MIN_ADV_TIME (pPeerEntry) =
                pPeerGroup->u4MinRouteAdvInterval;
                }
            }
            if (BGP4_PEER_SHADOW_AUTO_START (pPeerEntry) == BGP4_PEER_AUTOMATICSTART_DISABLE)
            {
            BGP4_PEER_ALLOW_AUTOMATIC_START (pPeerEntry) =
                pPeerGroup->u1AutomaticStart;
            }
            if (BGP4_PEER_SHADOW_AUTO_STOP (pPeerEntry) == BGP4_PEER_AUTOMATICSTOP_DISABLE)
            {
            BGP4_PEER_ALLOW_AUTOMATIC_STOP (pPeerEntry) =
                pPeerGroup->u1AutomaticStop;
            }
            if (BGP4_PEER_SHADOW_IDLE_HOLDTIME_INTERVAL (pPeerEntry) == BGP4_DEF_IDLEHOLDINTERVAL)
            {
            BGP4_PEER_IDLE_HOLD_TIME (pPeerEntry) =
                pPeerGroup->u4IdleHoldTimeInterval;
            }
            if (BGP4_PEER_SHADOW_DAMP_PEER_OSC_STATUS (pPeerEntry) == BGP4_DAMP_PEER_OSCILLATIONS_DISABLE)
            {
            BGP4_DAMP_PEER_OSCILLATIONS (pPeerEntry) =
                pPeerGroup->u1DampPeerOscillationStatus;
            }
            if (BGP4_PEER_SHADOW_DELAY_OPEN_STATUS (pPeerEntry) == BGP4_PEER_DELAY_OPEN_DISABLE)
            {
            BGP4_PEER_DELAY_OPEN (pPeerEntry) = pPeerGroup->u1DelayOpenStatus;
            }
            if (BGP4_PEER_SHADOW_DELAY_OPEN_TIME_INTERVAL (pPeerEntry) == BGP4_DEF_DELAYOPENINTERVAL)
            {
            BGP4_PEER_DELAY_OPEN_TIME (pPeerEntry) =
                pPeerGroup->u4DelayOpenTimeInterval;
            }
            if (BGP4_PEER_SHADOW_PEER_PREFIX_UPPER_LIMIT (pPeerEntry) == BGP4_DEF_MAX_PEER_PREFIX_LIMIT)
            {
            BGP4_PEER_PREFIX_UPPER_LIMIT (pPeerEntry) =
                pPeerGroup->u4PeerPrefixUpperLimit;
            }
            if (BGP4_PEER_SHADOW_CONNECT_COUNT (pPeerEntry) == BGP4_DEF_CONNECT_RETRY_COUNT)
            {
            BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT (pPeerEntry) =
                pPeerGroup->u1ConnectRetryCount;
            }
#ifdef ROUTEMAP_WANTED
            if (STRLEN(pPeerEntry->shadowPeerConfig.FilterInRMap.au1DistInOutFilterRMapName)==0 &&
                pPeerEntry->shadowPeerConfig.FilterInRMap.u1Status != FILTERNIG_STAT_ENABLE)
            {
            MEMCPY (&pPeerEntry->FilterInRMap, &pPeerGroup->RMapIn,
                    sizeof (tFilteringRMap));
	    }
            if (STRLEN(pPeerEntry->shadowPeerConfig.FilterOutRMap.au1DistInOutFilterRMapName)==0 &&
                pPeerEntry->shadowPeerConfig.FilterOutRMap.u1Status != FILTERNIG_STAT_ENABLE)
            {
            MEMCPY (&pPeerEntry->FilterOutRMap, &pPeerGroup->RMapOut,
                    sizeof (tFilteringRMap));
	    }
            if (STRLEN(pPeerEntry->shadowPeerConfig.IpPrefixFilterIn.au1DistInOutFilterRMapName)==0 &&
                pPeerEntry->IpPrefixFilterIn.u1Status != FILTERNIG_STAT_ENABLE)
            {
            MEMCPY (&pPeerEntry->IpPrefixFilterIn,
                    &pPeerGroup->IpPrefixFilterIn, sizeof (tFilteringRMap));
            }
            if (STRLEN(pPeerEntry->shadowPeerConfig.IpPrefixFilterOut.au1DistInOutFilterRMapName)==0 &&
                pPeerEntry->IpPrefixFilterOut.u1Status != FILTERNIG_STAT_ENABLE)
            {
            MEMCPY (&pPeerEntry->IpPrefixFilterOut,
                    &pPeerGroup->IpPrefixFilterOut, sizeof (tFilteringRMap));
            }
#endif
            if ((pPeerGroup->u1MPCapabilty & CAP_NEG_ROUTE_REFRESH_MASK) ==
                    CAP_NEG_ROUTE_REFRESH_MASK)
            {
                if ((BGP4_PEER_NEG_CAP_MASK (pPeerEntry) &
                            CAP_NEG_ROUTE_REFRESH_MASK) != CAP_NEG_ROUTE_REFRESH_MASK)
                {
                    /* Enable Route Refresh Capability */
                    Bgp4SnmphRtRefCapabilityEnable (u4Context,
                            BGP4_PEER_REMOTE_ADDR_INFO
                            (pPeerEntry));
                }
            }

            if (pPeerGroup->u1OrfCapMode != 0)
            {
                Bgp4SnmphOrfCapabilityEnable (u4Context,
                                              BGP4_PEER_REMOTE_ADDR_INFO
                                              (pPeerEntry),
                                              BGP4_PEER_REMOTE_ADDR_TYPE
                                              (pPeerEntry),
                                              BGP4_INET_SAFI_UNICAST,
                                              pPeerGroup->u1OrfType,
                                              pPeerGroup->u1OrfCapMode);

            }

            /* If 4-byte ASN Support status is enabled,
             * then add 4-byte ASN capability to
             * BGP4_PEER_SUP_CAPS_LIST */
            if (BGP4_FOUR_BYTE_ASN_SUPPORT (u4Context) ==
                BGP4_ENABLE_4BYTE_ASN_SUPPORT)
            {
                Bgp4Snmph4ByteAsnCapabilityEnable (u4Context,
                                   BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));
            }

            if (pPeerGroup->u1PeerGrpStatus == NOT_IN_SERVICE)
            {
                /* When peer group is not active. do not start the peer */
                break;
            }
            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                return BGP4_FAILURE;
            }
            BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_SNMP_START_EVENT;
            BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
            BGP4_INPUTQ_CXT (pQMsg) = u4Context;
            Bgp4CopyAddrPrefixStruct (&(BGP4_INPUTQ_PEER_ADDR_INFO (pQMsg)),
                                      BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));
            /* Send the peer entry to the bgp-task */
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                return BGP4_FAILURE;
            }

            break;

        case BGP4_PEER_EXT_REMOTE_AS_OBJECT:
            if (BGP4_PEER_SHADOW_REMOTE_AS_NO (pPeerEntry) != 0)
            {
                /* Remote AS is already configured for the peer
                 * Dont disturb it */
                return BGP4_SUCCESS;
            }
            BGP4_PEER_ASNO (pPeerEntry) = pPeerGroup->u4RemoteASNo;

            /* check if Confed is configured  and mark, if peer 
             * is confed peer */
            if (BGP4_CONFED_ID (u4Context) != BGP4_INV_AS)
            {
                UINT4               u4Indx;

                for (u4Indx = 0; u4Indx < BGP4_CONFED_MAX_MEMBER_AS; u4Indx++)
                {
                    if (BGP4_PEER_ASNO (pPeerEntry) ==
                        BGP4_CONFED_PEER (u4Context, u4Indx))
                    {
                        BGP4_CONFED_PEER_STATUS (pPeerEntry) = BGP4_TRUE;
                        break;
                    }
                }
            }
            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                return BGP4_FAILURE;
            }
            if (BGP4_PEER_ADMIN_STATUS (pPeerEntry) == BGP4_PEER_STOP)
            {
            BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_SNMP_START_EVENT;
            }
            else
            {
                BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_CLI_CLEAR_IP_BGP_EVENT;
            }
            BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
            BGP4_INPUTQ_CXT (pQMsg) = u4Context;
            Bgp4CopyAddrPrefixStruct (&(BGP4_INPUTQ_PEER_ADDR_INFO (pQMsg)),
                                      BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));
            /* Send the peer entry to the bgp-task */
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                return BGP4_FAILURE;
            }

            break;

        case BGP4_PEER_EXT_LOCAL_AS_OBJECT:
            if (BGP4_PEER_LOCAL_AS (pPeerEntry) != 0) 
            {
                if (BGP4_PEER_LOCAL_AS (pPeerEntry) != pPeerGroup->u4ASNoTmp )
                {
                    /* Local AS is already configured for the peer
                     * Dont disturb it */
                    return BGP4_SUCCESS;
                }
            }
            if (BGP4_PEER_LOCAL_AS (pPeerEntry) == pPeerGroup->u4LocalASNo)
            {
                /* Local AS is already configured for the peer
                 * Dont disturb it */
                return BGP4_SUCCESS;
            }

                pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
                if (pQMsg == NULL)
                {
                    return BGP4_FAILURE;
                }
            BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_LOCAL_AS_CFG_EVENT;


            if (pPeerGroup->u4LocalASNo != 0)                                                                                                           
            {
                pQMsg->QMsg.Bgp4PeerMsg.u4LocalAs = pPeerGroup->u4LocalASNo;
                BGP4_PEER_LOCAL_AS_CFG (pPeerEntry) = pPeerGroup->u1LocalASConfig;
            }
            BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
            BGP4_INPUTQ_CXT (pQMsg) = u4Context;
            Bgp4CopyAddrPrefixStruct (&(BGP4_INPUTQ_PEER_ADDR_INFO (pQMsg)),
                    BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));
            /* Send the peer entry to the bgp-task */
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                return BGP4_FAILURE;
            }
            u1SupportStatus =
                Bgp4SnmphGet4ByteAsnCapSupportStatus (BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));
            if (u1SupportStatus == BGP4_TRUE)
            {
                TMO_SLL_Scan (BGP4_PEER_SUP_CAPS_LIST (pPeerEntry), pSpkrSupCap,
                        tSupCapsInfo *)
                {
                    /* Search only 4-byte ASN CAPABILITY Support */
                    if (pSpkrSupCap->SupCapability.u1CapCode ==
                            CAP_CODE_4_OCTET_ASNO)
                    {
                        if (pPeerGroup->u4LocalASNo == 0)
                        {
                            PTR_ASSIGN4 (pSpkrSupCap->SupCapability.au1CapValue,
                                    BGP4_LOCAL_AS_NO (u4Context));
                        }
                        else                                                                                                                                                         {
                            PTR_ASSIGN4 (pSpkrSupCap->SupCapability.au1CapValue,
                                    pPeerGroup->u4LocalASNo);
                        }
                    }
                }
            }                       
            break;
        case BGP4_PEER_EXT_MULTIHOP_OBJECT:

            if ((BGP4_PEER_SHADOW_EBGP_MULTIHOP (pPeerEntry) == BGP4_EBGP_MULTI_HOP_DISABLE)
		&& (BGP4_PEER_EBGP_MULTIHOP (pPeerEntry) != pPeerGroup->u1EBGPMultiHop))
            {
                BGP4_PEER_EBGP_MULTIHOP (pPeerEntry) =
                    pPeerGroup->u1EBGPMultiHop;
                pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
                if (pQMsg == NULL)
                {
                    return BGP4_FAILURE;
                }
                BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
                BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_SNMP_EBGP_MULTIHOP_EVENT;
                BGP4_INPUTQ_CXT (pQMsg) = u4Context;
                Bgp4CopyAddrPrefixStruct (&(BGP4_INPUTQ_PEER_ADDR_INFO (pQMsg)),
                                          BGP4_PEER_REMOTE_ADDR_INFO
                                          (pPeerEntry));
                /* Send the peer entry to the bgp-task */
                if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
                {
                    return BGP4_FAILURE;
                }
            }
            break;
        case BGP4_PEER_EXT_NEXTHOP_SELF_OBJECT:
            if (BGP4_PEER_SHADOW_SELF_NEXTHOP (pPeerEntry) == BGP4_DISABLE_NEXTHOP_SELF)
            {
            BGP4_PEER_SELF_NEXTHOP (pPeerEntry) = pPeerGroup->u1SelfNextHop;
                /*Post a clear ip bgp soft out event to resend update message for already sent routes */
                pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
                if (pQMsg == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                            BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                            "\tMemory Allocation for posting BGP Outbound "
                            "Refresh Msg to BGP Queue FAILED!!!\n");
                    return BGP4_FAILURE;
                }
                BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_SNMP_OUTBOUND_SOFTCONFIG_EVENT;
                BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
                Bgp4CopyAddrPrefixStruct (&(BGP4_INPUTQ_RTREF_PEER_ADDR_INFO (pQMsg)),
                        BGP4_PEER_REMOTE_ADDR_INFO(pPeerEntry));
                BGP4_INPUTQ_CXT (pQMsg) = u4Context;
                if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
                {
                    return BGP4_FAILURE;
                }
            }
            break;
        case BGP4_PEER_EXT_HOPLIMIT_OBJECT:
            if (BGP4_PEER_SHADOW_HOP_LIMIT (pPeerEntry) == BGP4_DEFAULT_HOPLIMIT)
            {
            BGP4_PEER_HOPLIMIT (pPeerEntry) = pPeerGroup->u1HopLimit;
            }
            break;
        case BGP4_PEER_EXT_TCP_SENDBUF_OBJECT:
            if (BGP4_PEER_SHADOW_SEND_BUF (pPeerEntry) == BGP4_DEFAULT_SENDBUF)
            {
            BGP4_PEER_SENDBUF (pPeerEntry) = pPeerGroup->u4SendBuf;
            }
            break;
        case BGP4_PEER_EXT_TCP_RECVBUF_OBJECT:
            if (BGP4_PEER_SHADOW_RECV_BUF (pPeerEntry) == BGP4_DEFAULT_RECVBUF)
            {
            BGP4_PEER_RECVBUF (pPeerEntry) = pPeerGroup->u4RecvBuf;
            }
            break;
        case BGP4_PEER_EXT_COMM_SEND_STATUS_OBJECT:
            if (BGP4_PEER_SHADOW_COMM_SEND_STATUS (pPeerEntry) == BGP4_TRUE)
            {
                BGP4_PEER_COMM_SEND_STATUS (pPeerEntry) = pPeerGroup->u1CommSendStatus;
            }
            break;
        case BGP4_PEER_EXT_ECOMM_SEND_STATUS_OBJECT:
            if (BGP4_PEER_SHADOW_ECOMM_SEND_STATUS (pPeerEntry) == BGP4_TRUE)
            { 
                BGP4_PEER_ECOMM_SEND_STATUS (pPeerEntry) = pPeerGroup->u1ECommSendStatus;
            }
            break;
        case BGP4_PEER_EXT_PASSIVE_SET_OBJECT:
            if (BGP4_PEER_SHADOW_PASSIVE (pPeerEntry) == BGP4_FALSE)
            {
            BGP4_PEER_CONN_PASSIVE (pPeerEntry) = pPeerGroup->u1Passive;
            }
            break;
        case BGP4_PEER_EXT_OVERRIDE_CAPABILITY_OBJECT:
            if (BGP4_PEER_SHADOW_OVERRIDE_CAP (pPeerEntry) == BGP4_DISABLE_OVERRIDE_CAPABILITY)
            {
            BGP4_PEER_OVERRIDE_CAPABILITY (pPeerEntry) =
                pPeerGroup->u1OverrideCap;
            }
            break;
        case BGP4_PEER_EXT_DEFAULT_ROUTE_OBJECT:
            if (BGP4_PEER_SHADOW_DFT_ROUTE_ORIG_STATUS (pPeerEntry) == BGP4_DEF_ROUTE_ORIG_DISABLE)
            {
            if ((BGP4_PEER_ADMIN_STATUS (pPeerEntry) == BGP4_PEER_STOP) ||
                (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_DOWN))
            {
                BGP4_PEER_DEF_ROUTE_ORIG_STATUS (pPeerEntry)
                    = pPeerGroup->u1DftRouteOrigStatus;
                return BGP4_SUCCESS;
            }

            if (BGP4_PEER_STATE (pPeerEntry) != BGP4_ESTABLISHED_STATE)
            {
                /* Peer is in non-Established State. */
                BGP4_PEER_DEF_ROUTE_ORIG_STATUS (pPeerEntry)
                    = pPeerGroup->u1DftRouteOrigStatus;
                return BGP4_SUCCESS;
            }

            if ((BGP4_GET_PEER_CURRENT_STATE (pPeerEntry) ==
                 BGP4_PEER_INIT_INPROGRESS) &&
                (pPeerGroup->u1DftRouteOrigStatus ==
                 BGP4_DEF_ROUTE_ORIG_ENABLE))
            {
                /* Peer is going through init and we are trying to enable the
                 * default information origination command. In this case, just
                 * set the peer origination status as enable, the default route
                 * will be send after the normal peer init is complete. */
                BGP4_PEER_DEF_ROUTE_ORIG_STATUS (pPeerEntry)
                    = pPeerGroup->u1DftRouteOrigStatus;
                return BGP4_SUCCESS;
            }

            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                          BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                          "\tMemory Allocation for posting BGP Peer Default "
                          "Route Origination to BGP Queue FAILED!!!\n");
                return BGP4_FAILURE;
            }

            /* Post message to BGP task to indicate this event. */
            BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_PEER_DEF_ROUTE_ORIG_CHG_EVENT;
            BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
            BGP4_INPUTQ_CXT (pQMsg) = u4Context;
            Bgp4CopyAddrPrefixStruct
                ((&(BGP4_INPUTQ_DEF_RT_PEER_ADDR_INFO (pQMsg))),
                 BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));
            BGP4_INPUTQ_DEF_RT_STATUS (pQMsg) =
                pPeerGroup->u1DftRouteOrigStatus;

            /* Send the peer entry to the bgp-task */
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                return BGP4_FAILURE;
            }
            }
            break;
        case BGP4_PEER_EXT_ACTIVATE_MP_CAP_OBJECT:

            switch (pPeerGroup->u1MPCapabilty)
            {
                case CAP_NEG_IPV4_UNI_MASK:
                    if (BGP4_IPV4_AFI_FLAG (u4Context) == BGP4_FALSE)
                    {
                        /* IPv4 Unicast not Supported */
                        return BGP4_FAILURE;
                    }
                    if (((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) &
                          CAP_NEG_IPV4_UNI_MASK) == CAP_NEG_IPV4_UNI_MASK) &&
                        ((BGP4_PEER_NEG_CAP_MASK (pPeerEntry) &
                          CAP_NEG_ROUTE_REFRESH_MASK) ==
                         CAP_NEG_ROUTE_REFRESH_MASK))
                    {
                        /* Already the capabilities are negotiated. */
                        return BGP4_SUCCESS;
                    }

                    /* Enable IPv4 Unicast Capability */
                    i4RetVal =
                        Bgp4SnmphEnableMPCapability (u4Context,
                                                     BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerEntry),
                                                     BGP4_INET_AFI_IPV4,
                                                     BGP4_INET_SAFI_UNICAST);
                    if (i4RetVal == SNMP_FAILURE)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                                  BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                                  BGP4_MOD_NAME,
                                  "\tUnable to activate ipv4-unicast "
                                  "capability for peer\n");
                        return (BGP4_FAILURE);
                    }
                    /* Enable Route Refresh Capability */
                    Bgp4SnmphRtRefCapabilityEnable (u4Context,
                                                    BGP4_PEER_REMOTE_ADDR_INFO
                                                    (pPeerEntry));
                    break;
#ifdef BGP4_IPV6_WANTED
                case CAP_NEG_IPV6_UNI_MASK:
                    if (BGP4_IPV6_AFI_FLAG (u4Context) == BGP4_FALSE)
                    {
                        /* IPv6 Unicast not Supported */
                        return BGP4_FAILURE;
                    }

                    if (((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) &
                          CAP_NEG_IPV6_UNI_MASK) == CAP_NEG_IPV6_UNI_MASK) &&
                        ((BGP4_PEER_NEG_CAP_MASK (pPeerEntry) &
                          CAP_NEG_ROUTE_REFRESH_MASK) ==
                         CAP_NEG_ROUTE_REFRESH_MASK))
                    {
                        /* Already the capabilities are negotiated. */
                        return BGP4_SUCCESS;
                    }

                    /* Enable IPv6 Unicast Capability */
                    i4RetVal =
                        Bgp4SnmphEnableMPCapability (u4Context,
                                                     BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerEntry),
                                                     BGP4_INET_AFI_IPV6,
                                                     BGP4_INET_SAFI_UNICAST);
                    if (i4RetVal == SNMP_FAILURE)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                                  BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                                  BGP4_MOD_NAME,
                                  "\tUnable to activate ipv6-unicast "
                                  "capability for peer\n");
                        return (BGP4_FAILURE);
                    }
                    /* Enable Route Refresh Capability */
                    Bgp4SnmphRtRefCapabilityEnable (u4Context,
                                                    BGP4_PEER_REMOTE_ADDR_INFO
                                                    (pPeerEntry));
                    break;
#endif
#ifdef VPLSADS_WANTED
                case CAP_NEG_L2VPN_VPLS_MASK:
                    if (BGP4_L2VPN_AFI_FLAG (u4Context) == BGP4_FALSE)
                    {
                        /* L2VPN VPLS not Supported */
                        return BGP4_FAILURE;
                    }

                    if (((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) &
                          CAP_NEG_L2VPN_VPLS_MASK) == CAP_NEG_L2VPN_VPLS_MASK)
                        &&
                        ((BGP4_PEER_NEG_CAP_MASK (pPeerEntry) &
                          CAP_NEG_ROUTE_REFRESH_MASK) ==
                         CAP_NEG_ROUTE_REFRESH_MASK))
                    {
                        /* Already the capabilities are negotiated. */
                        return BGP4_SUCCESS;
                    }

                    /* Enable L2VPN VPLS Capability */
                    i4RetVal =
                        Bgp4SnmphEnableMPCapability (u4Context,
                                                     BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerEntry),
                                                     BGP4_INET_AFI_L2VPN,
                                                     BGP4_INET_SAFI_VPLS);
                    if (i4RetVal == SNMP_FAILURE)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                                  BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                                  BGP4_MOD_NAME,
                                  "\tUnable to activate l2vpn-vpls "
                                  "capability for peer\n");
                        return (BGP4_FAILURE);
                    }
                    /* Enable Route Refresh Capability */
                    Bgp4SnmphRtRefCapabilityEnable (u4Context,
                                                    BGP4_PEER_REMOTE_ADDR_INFO
                                                    (pPeerEntry));
                    break;
#endif
                default:
                    return (BGP4_FAILURE);
            }
            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                          BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                          "\tMemory Allocation for posting BGP Peer MP "
                          "CAP Activation to BGP Queue FAILED!!!\n");
                return BGP4_FAILURE;
            }

            /* Post message to BGP task to indicate this event. */
            if ((BGP4_PEER_ADMIN_STATUS (pPeerEntry) != BGP4_PEER_STOP) &&
                (BGP4_LOCAL_ADMIN_STATUS (u4Context) == BGP4_ADMIN_UP))
            {
                BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_CLI_CLEAR_IP_BGP_EVENT;
                BGP4_INPUTQ_CXT (pQMsg) = u4Context;
                Bgp4CopyAddrPrefixStruct
                    ((&(BGP4_INPUTQ_PEER_ADDR_INFO (pQMsg))),
                     BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));

                /* Send the peer entry to the bgp-task */
                if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
                {
                    return BGP4_FAILURE;
                }
            }
            else
            {
                BGP4_QMSG_FREE (pQMsg);
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                          BGP4_MOD_NAME, "\tBGP Admin status is stopped\n");
            }
            break;

        case BGP4_PEER_EXT_DEACTIVATE_MP_CAP_OBJECT:
            switch (pPeerGroup->u1MPCapabilty)
            {
                case CAP_NEG_IPV4_UNI_MASK:
                    if (BGP4_IPV4_AFI_FLAG (u4Context) == BGP4_FALSE)
                    {
                        /* IPv4 Unicast not Supported */
                        return BGP4_FAILURE;
                    }

                    if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) &
                         CAP_NEG_IPV4_UNI_MASK) == CAP_NEG_IPV4_UNI_MASK)

                    {
                        /* Already the capabilities are negotiated. */
                        u1IsDeactive = BGP4_TRUE;
                    }

                    /* Disable IPV4 MP Capability */
                    i4RetVal =
                        Bgp4SnmphDisableMPCapability (u4Context,
                                                      BGP4_PEER_REMOTE_ADDR_INFO
                                                      (pPeerEntry),
                                                      BGP4_INET_AFI_IPV4,
                                                      BGP4_INET_SAFI_UNICAST);
                    if (i4RetVal == SNMP_FAILURE)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                                  BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                                  BGP4_MOD_NAME,
                                  "\tUnable to de-activate ipv4-unicast "
                                  "capability for peer\n");
                        return (BGP4_FAILURE);
                    }
#ifdef BGP4_IPV6_WANTED
                    if (Bgp4SnmphGetMPCapSupportStatus
                        (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry),
                         BGP4_INET_AFI_IPV6,
                         BGP4_INET_SAFI_UNICAST) == BGP4_FALSE)
                    {
                        /* Disable Route Refresh Capability */
                        Bgp4SnmphRtRefCapabilityDisable
                            (u4Context,
                             BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));
                    }
#else
                    /* Disable Route Refresh Capability */
                    Bgp4SnmphRtRefCapabilityDisable (u4Context,
                                                     BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerEntry));
#endif
                    break;
#ifdef BGP4_IPV6_WANTED
                case CAP_NEG_IPV6_UNI_MASK:
                    if (BGP4_IPV6_AFI_FLAG (u4Context) == BGP4_FALSE)
                    {
                        /* IPv6 Unicast not Supported */
                        return BGP4_FAILURE;
                    }

                    if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) &
                         CAP_NEG_IPV6_UNI_MASK) == CAP_NEG_IPV6_UNI_MASK)
                    {
                        /* Already the capabilities are negotiated. */
                        u1IsDeactive = BGP4_TRUE;
                    }

                    /* Disable IPV6 MP Capability */
                    i4RetVal =
                        Bgp4SnmphDisableMPCapability (u4Context,
                                                      BGP4_PEER_REMOTE_ADDR_INFO
                                                      (pPeerEntry),
                                                      BGP4_INET_AFI_IPV6,
                                                      BGP4_INET_SAFI_UNICAST);
                    if (i4RetVal == SNMP_FAILURE)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                                  BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                                  BGP4_MOD_NAME,
                                  "\tUnable to de-activate ipv6-unicast "
                                  "capability for peer\n");
                        return (BGP4_FAILURE);
                    }

                    if (Bgp4SnmphGetMPCapSupportStatus
                        (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry),
                         BGP4_INET_AFI_IPV4,
                         BGP4_INET_SAFI_UNICAST) == BGP4_FALSE)
                    {
                        Bgp4SnmphRtRefCapabilityDisable
                            (u4Context,
                             BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));
                    }
                    break;
#endif
#ifdef VPLSADS_WANTED
                case CAP_NEG_L2VPN_VPLS_MASK:
                    if (BGP4_L2VPN_AFI_FLAG (u4Context) == BGP4_FALSE)
                    {
                        /* IPv6 Unicast not Supported */
                        return BGP4_FAILURE;
                    }

                    if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerEntry) &
                         CAP_NEG_L2VPN_VPLS_MASK) == CAP_NEG_L2VPN_VPLS_MASK)
                    {
                        /* Already the capabilities are negotiated. */
                        u1IsDeactive = BGP4_TRUE;
                    }

                    /* Disable L2VPN VPLS Capability */
                    i4RetVal =
                        Bgp4SnmphDisableMPCapability (u4Context,
                                                      BGP4_PEER_REMOTE_ADDR_INFO
                                                      (pPeerEntry),
                                                      BGP4_INET_AFI_L2VPN,
                                                      BGP4_INET_SAFI_VPLS);
                    if (i4RetVal == SNMP_FAILURE)
                    {
                        BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                                  BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                                  BGP4_MOD_NAME,
                                  "\tUnable to de-activate l2vpn-vpls "
                                  "capability for peer\n");
                        return (BGP4_FAILURE);
                    }

                    if ((Bgp4SnmphGetMPCapSupportStatus
                         (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry),
                          BGP4_INET_AFI_IPV4,
                          BGP4_INET_SAFI_UNICAST) == BGP4_FALSE)
#ifdef BGP4_IPV6_WANTED
                        && (Bgp4SnmphGetMPCapSupportStatus
                            (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry),
                             BGP4_INET_AFI_IPV6,
                             BGP4_INET_SAFI_UNICAST) == BGP4_FALSE)
#endif
                        )
                    {
                        Bgp4SnmphRtRefCapabilityDisable
                            (u4Context,
                             BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));
                    }
                    break;
#endif
                default:
                    return (BGP4_FAILURE);
            }
            if (u1IsDeactive == BGP4_TRUE)
            {
                pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
                if (pQMsg == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_MGMT_TRC | BGP4_OS_RESOURCE_TRC,
                              BGP4_MOD_NAME,
                              "\tMemory Allocation for posting BGP Peer MP "
                              "CAP Activation to BGP Queue FAILED!!!\n");
                    return BGP4_FAILURE;
                }

                /* Post message to BGP task to indicate this event. */
                BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_CLI_CLEAR_IP_BGP_EVENT;
                BGP4_INPUTQ_CXT (pQMsg) = u4Context;
                Bgp4CopyAddrPrefixStruct
                    ((&(BGP4_INPUTQ_PEER_ADDR_INFO (pQMsg))),
                     BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));

                /* Send the peer entry to the bgp-task */
                if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
                {
                    return BGP4_FAILURE;
                }
            }
            break;

        case BGP4_PEER_EXT_ACTIVATE_ORF_CAP_OBJECT:

            /* Enable ORF capability */
            i4RetVal =
                Bgp4SnmphOrfCapabilityEnable (u4Context,
                                              BGP4_PEER_REMOTE_ADDR_INFO
                                              (pPeerEntry),
                                              BGP4_PEER_REMOTE_ADDR_TYPE
                                              (pPeerEntry),
                                              BGP4_INET_SAFI_UNICAST,
                                              pPeerGroup->u1OrfType,
                                              pPeerGroup->u1OrfCapMode);
            if (i4RetVal == SNMP_FAILURE)
            {
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                          BGP4_MOD_NAME,
                          "\tUnable to activate ipv4-unicast "
                          "capability for peer\n");
                return (BGP4_FAILURE);
            }

            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC | BGP4_OS_RESOURCE_TRC,
                          BGP4_MOD_NAME,
                          "\tMemory Allocation for posting BGP Peer MP "
                          "CAP Activation to BGP Queue FAILED!!!\n");
                return BGP4_FAILURE;
            }

            /* Post message to BGP task to indicate this event. */
            BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_CLI_CLEAR_IP_BGP_EVENT;
            BGP4_INPUTQ_CXT (pQMsg) = u4Context;
            Bgp4CopyAddrPrefixStruct
                ((&(BGP4_INPUTQ_PEER_ADDR_INFO (pQMsg))),
                 BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));

            /* Send the peer entry to the bgp-task */
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                return BGP4_FAILURE;
            }
            break;

        case BGP4_PEER_EXT_DEACTIVATE_ORF_CAP_OBJECT:

            /* Enable ORF capability */
            i4RetVal =
                Bgp4SnmphOrfCapabilityDisable (u4Context,
                                               BGP4_PEER_REMOTE_ADDR_INFO
                                               (pPeerEntry),
                                               BGP4_PEER_REMOTE_ADDR_TYPE
                                               (pPeerEntry),
                                               BGP4_INET_SAFI_UNICAST,
                                               pPeerGroup->u1OrfType,
                                               BGP4_CLI_ORF_MODE_BOTH);
            if (i4RetVal == SNMP_FAILURE)
            {
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                          BGP4_MOD_NAME,
                          "\tUnable to activate ipv4-unicast "
                          "capability for peer\n");
                return (BGP4_FAILURE);
            }

            pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
            if (pQMsg == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_MGMT_TRC | BGP4_OS_RESOURCE_TRC,
                          BGP4_MOD_NAME,
                          "\tMemory Allocation for posting BGP Peer MP "
                          "CAP Activation to BGP Queue FAILED!!!\n");
                return BGP4_FAILURE;
            }

            /* Post message to BGP task to indicate this event. */
            BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_CLI_CLEAR_IP_BGP_EVENT;
            BGP4_INPUTQ_CXT (pQMsg) = u4Context;
            Bgp4CopyAddrPrefixStruct
                ((&(BGP4_INPUTQ_PEER_ADDR_INFO (pQMsg))),
                 BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));

            /* Send the peer entry to the bgp-task */
            if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
            {
                return BGP4_FAILURE;
            }
            break;

        case BGP4_PEER_EXT_RFL_CLIENT_OBJECT:

            /* Check if the reflector client event is pending or not. If
             * pending, then already reflector client event is in progress.
             * If flag set and the new status is NON_CLIENT & current
             * status is CLIENT or if new status is CLIENT & current status
             * is NON_CLIENT, no need to process. Else if flag is set and
             * current status & new status are same, then just reset the
             * flag, since no need to change the status after peer down
             * is complete.
             */
            if (BGP4_PEER_SHADOW_RFL_CLIENT (pPeerEntry) == NON_CLIENT)
            {
            if ((BGP4_GET_PEER_PEND_FLAG (pPeerEntry) &
                 BGP4_PEER_RFL_CLIENT_PEND) == BGP4_PEER_RFL_CLIENT_PEND)
            {
                if (((BGP4_PEER_RFL_CLIENT (pPeerEntry) == NON_CLIENT) &&
                     (pPeerGroup->u1RflClient == CLIENT)) ||
                    ((BGP4_PEER_RFL_CLIENT (pPeerEntry) == CLIENT) &&
                     (pPeerGroup->u1RflClient == NON_CLIENT)))
                {
                    return BGP4_SUCCESS;
                }
                else if (((BGP4_PEER_RFL_CLIENT (pPeerEntry) == CLIENT) &&
                          (pPeerGroup->u1RflClient == CLIENT)) ||
                         ((BGP4_PEER_RFL_CLIENT (pPeerEntry) == NON_CLIENT) &&
                          (pPeerGroup->u1RflClient == NON_CLIENT)))
                {
                    BGP4_RESET_PEER_PEND_FLAG (pPeerEntry,
                                               BGP4_PEER_RFL_CLIENT_PEND);
                    return BGP4_SUCCESS;
                }
            }

            /* No peer client pend flag set. If there is a change in 
             * client status then post a message to BGP task. */
            if (BGP4_PEER_RFL_CLIENT (pPeerEntry) != pPeerGroup->u1RflClient)
            {
                pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
                if (pQMsg == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                              BGP4_OS_RESOURCE_TRC | BGP4_MGMT_TRC,
                              BGP4_MOD_NAME,
                              "\tUnable to allocate BGP Q Msg.\n");
                    return BGP4_FAILURE;
                }

                BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
                /* Post message to BGP task to indicate this event. */
                BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_SNMP_RFL_PEER_TYPE_EVENT;
                BGP4_INPUTQ_CXT (pQMsg) = u4Context;
                Bgp4CopyAddrPrefixStruct (&(BGP4_INPUTQ_PEER_ADDR_INFO (pQMsg)),
                                          BGP4_PEER_REMOTE_ADDR_INFO
                                          (pPeerEntry));

                /* Send the peer entry to the bgp-task */
                if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
                {
                    return BGP4_FAILURE;
                }
            }
            }
            break;
        case BGP4_PEER_HOLD_TIME_CONF_OBJECT:
            if (BGP4_PEER_SHADOW_HOLD_INTERVAL (pPeerEntry) == BGP4_DEF_HOLDINTERVAL)
            {
            BGP4_PEER_HOLD_TIME (pPeerEntry) = pPeerGroup->u4HoldInterval;
            }
            break;
        case BGP4_PEER_KEEPALIVE_CONF_OBJECT:
            if (BGP4_PEER_SHADOW_KEEP_ALIVE_INTERVAL (pPeerEntry) == BGP4_DEF_KEEPALIVEINTERVAL)
            {
            BGP4_PEER_KEEP_ALIVE_TIME (pPeerEntry) =
                pPeerGroup->u4KeepAliveInterval;
            }
            break;
        case BGP4_PEER_CONNECT_RETRY_TIME_OBJECT:
            if (BGP4_PEER_SHADOW_CONNECT_RETRY_INTERVAL (pPeerEntry) == BGP4_DEF_CONNRETRYINTERVAL)
            {
            BGP4_PEER_CONN_RETRY_TIME (pPeerEntry) =
                pPeerGroup->u4ConnectRetryInterval;
            }
            break;
        case BGP4_PEER_MINAS_ORIG_OBJECT:
            if (BGP4_PEER_SHADOW_MIN_AS_ORIG_INTERVAL (pPeerEntry) == BGP4_DEF_MINASORIGINTERVAL)
            {
            BGP4_PEER_MIN_AS_ORIG_TIME (pPeerEntry) =
                pPeerGroup->u4MinASOrigInterval;
            }
            break;
        case BGP4_PEER_MINROUTE_ADVT_OBJECT:
            if ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerEntry), pPeerEntry) ==
                 BGP4_EXTERNAL_PEER))
            {
            	if (BGP4_PEER_SHADOW_MIN_ROUTE_ADV_INTERVAL (pPeerEntry) == BGP4_DEF_EBGP_MINROUTEADVINTERVAL)
            	{
            BGP4_PEER_MIN_ADV_TIME (pPeerEntry) =
                pPeerGroup->u4MinRouteAdvInterval;
            	}
            }
            else
            {
                if (BGP4_PEER_SHADOW_MIN_ROUTE_ADV_INTERVAL (pPeerEntry) == BGP4_DEF_IBGP_MINROUTEADVINTERVAL)
                {
            BGP4_PEER_MIN_ADV_TIME (pPeerEntry) =
                pPeerGroup->u4MinRouteAdvInterval;
                }
            }
            break;
        case BGP4_PEER_ALLOW_AUTOMATIC_START_OBJECT:
            if (BGP4_PEER_SHADOW_AUTO_START (pPeerEntry) == BGP4_PEER_AUTOMATICSTART_DISABLE)
            {
            BGP4_PEER_ALLOW_AUTOMATIC_START (pPeerEntry) =
                pPeerGroup->u1AutomaticStart;
            }
            break;
        case BGP4_PEER_ALLOW_AUTOMATIC_STOP_OBJECT:
            if (BGP4_PEER_SHADOW_AUTO_STOP (pPeerEntry) == BGP4_PEER_AUTOMATICSTOP_DISABLE)
            {
            BGP4_PEER_ALLOW_AUTOMATIC_STOP (pPeerEntry) =
                pPeerGroup->u1AutomaticStop;
            }
            break;
        case BGP4_PEER_IDLE_HOLD_TIME_CONF_OBJECT:
            if (BGP4_PEER_SHADOW_IDLE_HOLDTIME_INTERVAL (pPeerEntry) == BGP4_DEF_IDLEHOLDINTERVAL)
            {
            BGP4_PEER_IDLE_HOLD_TIME (pPeerEntry) =
                pPeerGroup->u4IdleHoldTimeInterval;
            }
            break;
        case BGP4_DAMP_PEER_OSCILLATIONS_OBJECT:
            if (BGP4_PEER_SHADOW_DAMP_PEER_OSC_STATUS (pPeerEntry) == BGP4_DAMP_PEER_OSCILLATIONS_DISABLE)
            {
            BGP4_DAMP_PEER_OSCILLATIONS (pPeerEntry) =
                pPeerGroup->u1DampPeerOscillationStatus;
            }
            break;
        case BGP4_PEER_DELAY_OPEN_OBJECT:
            if (BGP4_PEER_SHADOW_DELAY_OPEN_STATUS (pPeerEntry) == BGP4_PEER_DELAY_OPEN_DISABLE)
            {
            BGP4_PEER_DELAY_OPEN (pPeerEntry) = pPeerGroup->u1DelayOpenStatus;
            }
            break;
        case BGP4_PEER_DELAY_OPEN_TIME_CONF_OBJECT:
            if (BGP4_PEER_SHADOW_DELAY_OPEN_TIME_INTERVAL (pPeerEntry) == BGP4_DEF_DELAYOPENINTERVAL)
            {
            BGP4_PEER_DELAY_OPEN_TIME (pPeerEntry) =
                pPeerGroup->u4DelayOpenTimeInterval;
            }
            break;
        case BGP4_PEER_PREFIX_UPPER_LIMIT_OBJECT:
            if (BGP4_PEER_SHADOW_PEER_PREFIX_UPPER_LIMIT (pPeerEntry) == BGP4_DEF_MAX_PEER_PREFIX_LIMIT)
            {
            BGP4_PEER_PREFIX_UPPER_LIMIT (pPeerEntry) =
                pPeerGroup->u4PeerPrefixUpperLimit;
            }
            break;
        case BGP4_PEER_TCP_CONNECT_RETRY_COUNT_OBJECT:
            if (BGP4_PEER_SHADOW_CONNECT_COUNT (pPeerEntry) == BGP4_DEF_CONNECT_RETRY_COUNT)
            {
            BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT (pPeerEntry) =
                pPeerGroup->u1ConnectRetryCount;
            }
            break;
        case BGP4_PEER_ROUTE_MAP_IN_CONF_OBJECT:
#ifdef ROUTEMAP_WANTED
            if (STRLEN(pPeerEntry->shadowPeerConfig.FilterInRMap.au1DistInOutFilterRMapName)==0 &&
                pPeerEntry->shadowPeerConfig.FilterInRMap.u1Status != FILTERNIG_STAT_ENABLE)
            {
            MEMCPY (&pPeerEntry->FilterInRMap, &pPeerGroup->RMapIn,
                    sizeof (tFilteringRMap));
            }
#endif
            break;
        case BGP4_PEER_ROUTE_MAP_OUT_CONF_OBJECT:
#ifdef ROUTEMAP_WANTED
            if (STRLEN(pPeerEntry->shadowPeerConfig.FilterOutRMap.au1DistInOutFilterRMapName)==0 &&
                pPeerEntry->shadowPeerConfig.FilterOutRMap.u1Status != FILTERNIG_STAT_ENABLE)
            {
            MEMCPY (&pPeerEntry->FilterOutRMap, &pPeerGroup->RMapOut,
                    sizeof (tFilteringRMap));
            }
#endif
            break;
        case BGP4_PEER_IP_PREFIX_LIST_IN_CONF_OBJECT:
#ifdef ROUTEMAP_WANTED
            if (STRLEN(pPeerEntry->shadowPeerConfig.IpPrefixFilterIn.au1DistInOutFilterRMapName)==0 &&
                pPeerEntry->shadowPeerConfig.IpPrefixFilterIn.u1Status != FILTERNIG_STAT_ENABLE)
            {
            MEMCPY (&pPeerEntry->IpPrefixFilterIn,
                    &pPeerGroup->IpPrefixFilterIn, sizeof (tFilteringRMap));
            }
#endif
            break;
        case BGP4_PEER_IP_PREFIX_LIST_OUT_CONF_OBJECT:
#ifdef ROUTEMAP_WANTED
            if (STRLEN(pPeerEntry->shadowPeerConfig.IpPrefixFilterOut.au1DistInOutFilterRMapName)==0 &&
                pPeerEntry->shadowPeerConfig.IpPrefixFilterOut.u1Status != FILTERNIG_STAT_ENABLE)
            {
            MEMCPY (&pPeerEntry->IpPrefixFilterOut,
                    &pPeerGroup->IpPrefixFilterOut, sizeof (tFilteringRMap));
            }
#endif
            break;
        case BGP4_PEER_BFD_STATUS_CONF_OBJECT:
#ifdef BFD_WANTED
            if (BGP4_PEER_SHADOW_BFD_STATUS (pPeerEntry)== BGP4_BFD_DISABLE)
            {
                pPeerEntry->u1BfdStatus = pPeerGroup->u1BfdStatus;
                pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
                if (pQMsg == NULL)
                {
                    return SNMP_FAILURE;
                }

                BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
                BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_PEER_GROUP_SET_BFD_STATUS;

                MEMCPY (BGP4_INPUTQ_PEER_GROUP_NAME (pQMsg), pPeerGroup->au1PeerGroupName,
                        STRLEN (pPeerGroup->au1PeerGroupName));
                BGP4_INPUTQ_CXT (pQMsg) = u4Context;

                if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
#endif

        default:
            return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/****************************************************************************
 Function    :  Bgp4CopyPeerGroupProperties    
 Description :  This function copies the mentioned peer group property to the
                all the peers attached to the peer group
 Input       :  Peer group entry
                Object to be copied.
 Output      :  None
 Returns     :  BGP4_SUCCESS/BGP4_FAILURE   
****************************************************************************/
INT4
Bgp4CopyPeerGroupProperties (tBgpPeerGroupEntry * pPeerGroup, INT4 i4ObjectType)
{
    tPeerNode          *pPeerNode = NULL;
    tBgp4PeerEntry     *pPeerEntry = NULL;

    TMO_SLL_Scan (&(pPeerGroup->PeerList), pPeerNode, tPeerNode *)
    {
        pPeerEntry = pPeerNode->pPeer;
        if (Bgp4CopyPeerGroupPropertyToPeer (pPeerGroup, pPeerEntry,
                                             i4ObjectType) == BGP4_FAILURE)
        {
            BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG,
                           BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                           "Unable to configure the property to the peer %s\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerEntry))));
            return BGP4_FAILURE;
        }
    }
    return BGP4_SUCCESS;
}

/****************************************************************************
 Function    :  Bgp4ValidatePeerGroupName      
 Description :  This function validates the given peer group name.
 Input       :  Peer group Name
 Output      :  None
 Returns     :  BGP4_SUCCESS/BGP4_FAILURE   
****************************************************************************/
INT4
Bgp4ValidatePeerGroupName (UINT1 *pu1PeerGroupName)
{
    INT4                i4Length;

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pu1PeerGroupName, STRLEN (pu1PeerGroupName))
        == OSIX_FAILURE)
    {
        return BGP4_FAILURE;
    }
#endif
    /* Check to validate any special char like * or | symbol are used */
    if ((STRSTR(pu1PeerGroupName, "*")) || (STRSTR (pu1PeerGroupName, "|")))
    {
        return BGP4_FAILURE;
    }
    i4Length = CliDotStrLength (pu1PeerGroupName);
    if (i4Length == 4)
    {
        return BGP4_FAILURE;
    }
#ifdef BGP4_IPV6_WANTED
    else if (str_to_ip6addr (pu1PeerGroupName) != NULL)
    {
        return BGP4_FAILURE;
    }
#endif
    return BGP4_SUCCESS;
}

/****************************************************************************
 Function    :  Bgp4ActivateMpCapability      
 Description :  This function scans the peers attached to the given peer 
                 group and activates the given capability in it.
 Input       :  u4Context - Context Identifier
                pu1PeerGroupName - Peer group Name
                u4Capability - Capability to be enabled
                u1OrfType - ORF type (used only if Capability is ORF)
                u1OrfMode - ORF mode )send/receive/both)
                              (used only if Capability is ORF)
 Output      :  None
 Returns     :  BGP4_SUCCESS/BGP4_FAILURE   
****************************************************************************/
INT4
Bgp4ActivateMpCapability (UINT4 u4Context, UINT1 *pu1PeerGroupName,
                          UINT4 u4Capability, UINT1 u1OrfType, UINT1 u1OrfMode)
{
    tBgpPeerGroupEntry *pPeerGroup = NULL;
    tPeerNode          *pPeerNode = NULL;
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    UINT4               u4ErrorCode = 0;

    pPeerGroup = Bgp4GetPeerGroupEntry (u4Context, pu1PeerGroupName);
    if (pPeerGroup == NULL)
    {
        return BGP4_FAILURE;
    }

    if (u4Capability == CAP_CODE_ORF)
    {
        PeerAddr.i4_Length = STRLEN (pu1PeerGroupName);
        PeerAddr.pu1_OctetList = pu1PeerGroupName;

        if (nmhTestv2FsMIBgp4PeerGroupOrfType (&u4ErrorCode, 
                    (INT4) u4Context, &PeerAddr, (UINT4) u1OrfType) == SNMP_FAILURE)
        {
            return BGP4_FAILURE;
        }
        if (nmhSetFsMIBgp4PeerGroupOrfType ((INT4) u4Context, &PeerAddr, 
			(UINT4) u1OrfType) == SNMP_FAILURE)
        {
            return BGP4_FAILURE;
        }

	if (nmhTestv2FsMIBgp4PeerGroupOrfCapMode (&u4ErrorCode,(INT4) u4Context,
			&PeerAddr, (INT4)u1OrfMode) == SNMP_FAILURE)
        {
            return BGP4_FAILURE;
        }
	if (nmhSetFsMIBgp4PeerGroupOrfCapMode ((INT4) u4Context, &PeerAddr,
				(INT4) u1OrfMode) == SNMP_FAILURE)
        {
            return BGP4_FAILURE;
        }
    }
    else if (u4Capability == CAP_NEG_ROUTE_REFRESH_MASK)
    {
	PeerAddr.i4_Length = STRLEN (pu1PeerGroupName);
	PeerAddr.pu1_OctetList = pu1PeerGroupName;
        if (SNMP_FAILURE == nmhTestv2FsMIBgp4PeerGroupActivateMPCapability 
            (&u4ErrorCode, (INT4) u4Context, &PeerAddr, (INT4)u4Capability))
        {
            return BGP4_FAILURE;
        }
        if (SNMP_FAILURE == nmhSetFsMIBgp4PeerGroupActivateMPCapability ((INT4) u4Context,
							 &PeerAddr,
                                                       (INT4)u4Capability))
        {
            return BGP4_FAILURE;
        }
    }

    TMO_SLL_Scan (&(pPeerGroup->PeerList), pPeerNode, tPeerNode *)
    {
        pPeerEntry = pPeerNode->pPeer;
        switch (u4Capability)
        {
            case CAP_MP_IPV4_UNICAST:
                if (BGP4_IPV4_AFI_FLAG (u4Context) == BGP4_TRUE)
                {
                    Bgp4SnmphEnableMPCapability
                        (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry),
                         BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST);
                }
                else
                {
                    return BGP4_FAILURE;
                }
                break;
#ifdef BGP4_IPV6_WANTED
            case CAP_MP_IPV6_UNICAST:
                if (BGP4_IPV6_AFI_FLAG (u4Context) == BGP4_TRUE)
                {
                    Bgp4SnmphEnableMPCapability
                        (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry),
                         BGP4_INET_AFI_IPV6, BGP4_INET_SAFI_UNICAST);
                }
                else
                {
                    return BGP4_FAILURE;
                }
#endif
            case CAP_NEG_ROUTE_REFRESH_MASK:
                Bgp4SnmphRtRefCapabilityEnable
                    (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));
                break;

            case CAP_CODE_ORF:
                Bgp4SnmphOrfCapabilityEnable
                    (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry),
                     BGP4_PEER_REMOTE_ADDR_TYPE (pPeerEntry),
                     BGP4_INET_SAFI_UNICAST, u1OrfType, u1OrfMode);
                break;

#ifdef VPLSADS_WANTED
            case CAP_MP_L2VPN_VPLS:
                if (BGP4_L2VPN_AFI_FLAG (u4Context) == BGP4_TRUE)
                {
                    Bgp4SnmphEnableMPCapability
                        (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry),
                         BGP4_INET_AFI_L2VPN, BGP4_INET_SAFI_VPLS);
                }
                else
                {
                    return BGP4_FAILURE;
                }
                break;
#endif
            default:
                break;
        }
    }
    return BGP4_SUCCESS;
}

/****************************************************************************
 Function    :  Bgp4DeActivateMpCapability      
 Description :  This function scans the peers attached to the given peer 
                 group and de-activates the given MP capability in it.
 Input       :  Peer group Name
                Capability to be disabled
 Output      :  None
 Returns     :  BGP4_SUCCESS/BGP4_FAILURE   
****************************************************************************/
INT4
Bgp4DeActivateMpCapability (UINT4 u4Context, UINT1 *pu1PeerGroupName,
                            UINT4 u4Capability, UINT1 u1OrfType,
                            UINT1 u1OrfMode)
{
    tBgpPeerGroupEntry *pPeerGroup = NULL;
    tPeerNode          *pPeerNode = NULL;
    tBgp4PeerEntry     *pPeerEntry = NULL;
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    UINT4               u4ErrorCode = 0;

    pPeerGroup = Bgp4GetPeerGroupEntry (u4Context, pu1PeerGroupName);
    if (pPeerGroup == NULL)
    {
        return BGP4_FAILURE;
    }

    if (u4Capability == CAP_CODE_ORF)
    {
        pPeerGroup->u1OrfCapMode &= (UINT1) ~u1OrfMode;
    }
    else if (u4Capability == CAP_NEG_ROUTE_REFRESH_MASK)
    {
        PeerAddr.i4_Length = STRLEN (pu1PeerGroupName);
        PeerAddr.pu1_OctetList = pu1PeerGroupName;
        if (SNMP_FAILURE ==  nmhTestv2FsBgp4PeerGroupDeactivateMPCapability
            (&u4ErrorCode, &PeerAddr, (INT4)u4Capability))
        {
            return BGP4_FAILURE;
        }
        if (SNMP_FAILURE == nmhSetFsBgp4PeerGroupDeactivateMPCapability (&PeerAddr,
                                                         (INT4)u4Capability))
        {
            return BGP4_FAILURE;
        }
    }
    TMO_SLL_Scan (&(pPeerGroup->PeerList), pPeerNode, tPeerNode *)
    {
        pPeerEntry = pPeerNode->pPeer;
        switch (u4Capability)
        {
            case CAP_MP_IPV4_UNICAST:
                Bgp4SnmphDisableMPCapability (u4Context,
                                              BGP4_PEER_REMOTE_ADDR_INFO
                                              (pPeerEntry), BGP4_INET_AFI_IPV4,
                                              BGP4_INET_SAFI_UNICAST);
                break;
#ifdef BGP4_IPV6_WANTED
            case CAP_MP_IPV6_UNICAST:
                Bgp4SnmphDisableMPCapability (u4Context,
                                              BGP4_PEER_REMOTE_ADDR_INFO
                                              (pPeerEntry), BGP4_INET_AFI_IPV6,
                                              BGP4_INET_SAFI_UNICAST);
                break;
#endif
            case CAP_NEG_ROUTE_REFRESH_MASK:
		if (((BGP4_PEER_SHADOW_MP_CAP_MASK (pPeerEntry)) & CAP_NEG_ROUTE_REFRESH_MASK)
			!= CAP_NEG_ROUTE_REFRESH_MASK)
		{
                Bgp4SnmphRtRefCapabilityDisable
                    (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry));
		}
                break;

            case CAP_CODE_ORF:
                if (((BGP4_PEER_SHADOW_ORF_MODE (pPeerEntry)) & u1OrfMode)
                        != u1OrfMode)
                {
                Bgp4SnmphOrfCapabilityDisable
                    (u4Context, BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry),
                     BGP4_PEER_REMOTE_ADDR_TYPE (pPeerEntry),
                     BGP4_INET_SAFI_UNICAST, u1OrfType, u1OrfMode);
                }
                break;
#ifdef VPLSADS_WANTED
            case CAP_MP_L2VPN_VPLS:
                Bgp4SnmphDisableMPCapability (u4Context,
                                              BGP4_PEER_REMOTE_ADDR_INFO
                                              (pPeerEntry), BGP4_INET_AFI_L2VPN,
                                              BGP4_INET_SAFI_VPLS);
                break;
#endif
            default:
                break;
        }
    }
    return BGP4_SUCCESS;
}

/****************************************************************************
 Function    :  Bgp4EnableRRDProtoMask          
 Description :  This function enables the BGP RRD proto mask with the given
                 value and inform RTM the same. If RRD is not enabled, then 
                 this function also enables RRD.
 Input       :  u4Context - context Id
                i4RRDProtoMask - ProtoMask
 Output      :  None
 Returns     :  BGP4_SUCCESS/BGP4_FAILURE   
****************************************************************************/
INT4
Bgp4EnableRRDProtoMask (UINT4 u4Context, UINT4 i4RRDProtoMask)
{
    INT1                i1RetVal = SNMP_FAILURE;

    BGP4_RRD_PROTO_MASK (u4Context) |= i4RRDProtoMask;

    /* check if the BGP RRD Admin status is Enable */
    if (BGP4_RRD_ADMIN_STATE (u4Context) == BGP4_RRD_ENABLE)
    {
#ifdef RRD_WANTED
        /* If Redistribution is not enabled then enable the 
         * redistribution by default */
        i1RetVal = RtmApiConfigRRDAdminStatus (u4Context);
        if (i1RetVal != SNMP_SUCCESS)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\t Unable to enable Redistribution\n");
            BGP4_RTM_REG_FLAG_STATUS (u4Context) = BGP4_FALSE;
            return SNMP_FAILURE;
        }
#endif
    }
    else
    {
        return SNMP_SUCCESS;
    }

#ifndef ROUTEMAP_WANTED
    BGP4_RRD_RMAP_NAME (u4Context)[0] = 0;
#endif

#ifdef RRD_WANTED
    i1RetVal =
        SendingMessageToRRDQueue (u4Context, i4RRDProtoMask,
                                  RTM_REDISTRIBUTE_ENABLE_MESSAGE,
                                  BGP4_RRD_RMAP_NAME (u4Context),
                                  BGP4_RRD_MATCH_MASK (u4Context));
#ifdef BGP4_IPV6_WANTED
    i1RetVal =
        SendingMessageToRRDQueue6 (u4Context, i4RRDProtoMask,
                                   RTM6_REDISTRIBUTE_ENABLE_MESSAGE,
                                   BGP4_RRD_RMAP_NAME (u4Context),
                                   BGP4_RRD_MATCH_MASK (u4Context));
#endif /* BGP4_IPV6_WANTED */

    return (i1RetVal);
#else
    return SNMP_SUCCESS;
#endif /* RRD_WANTED */
}

/****************************************************************************
 Function    :  Bgp4CheckMKTNeighborAssocation
 Description :  This function checks if a given MKT is associated with a BGP
         PEER in the given context. Incase a MKT is associated with any
        neighbor SNMP_SUCCESS is returned. If MKT is not associated with 
        any neighbor then SNMP_FAILURE is returned.
 Input       :  u4Context - context Id
                u1MktId- MKT ID
 Output      :  None
 Returns     :  BGP4_SUCCESS/BGP4_FAILURE   
****************************************************************************/
INT4
Bgp4CheckMKTNeighborAssociation (UINT4 u4Context, UINT1 u1MktId)
{

    tAddrPrefix         PeerAddress;
    tAddrPrefix         NextPeerAddress;
    tBgp4PeerEntry      *pPeer = NULL;
    INT4                i4Sts = BGP4_FAILURE;
    INT4                i4KeyId = 0;

    i4Sts = Bgp4GetFirstIndexBgpPeerTable (u4Context, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - No entry exists in Peer Table.\n");
        return BGP4_FAILURE;
    }

    i4Sts = Bgp4GetFirstTCPAOKeyId (u4Context, PeerAddress, &i4KeyId);

     pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);

    if (pPeer == NULL)
    {
        return BGP4_FAILURE;
    }
    if ( (i4Sts == BGP4_SUCCESS) && ( BGP4_PEER_STATE (pPeer) == BGP4_ESTABLISHED_STATE) )
    {
        if (u1MktId == (UINT1) i4KeyId)
        {
            return BGP4_SUCCESS;
        }
    }

    /*Here the logic is to scan till you find an peer entry with AO configured. */
        /*This means the peer does not have any AO key configured */
        while ((i4Sts =
                Bgp4GetNextIndexBgpPeerTable (u4Context, PeerAddress,
                                              &NextPeerAddress)) ==
               BGP4_SUCCESS)
        {
            PeerAddress = NextPeerAddress;
            i4Sts = Bgp4GetFirstTCPAOKeyId (u4Context, PeerAddress, &i4KeyId);
           pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
           if ( ( pPeer != NULL ) && ( BGP4_PEER_STATE (pPeer) == BGP4_ESTABLISHED_STATE ) )
           {
            if (i4Sts == BGP4_SUCCESS)
            {
                if (u1MktId == (UINT1) i4KeyId)
                {
                    return BGP4_SUCCESS;
                }
            }
        }
    }

    return BGP4_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssHealthChkClearCtrBgp                                    */
/*                                                                           */
/* Description    This function clears the BGP counters.                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
BgpHealthChkClearCtr ()
{

    UINT4               u4MaxCxtValue = 0;
    UINT4               u4TmpContext = 0;
    INT4                i4Fsbgp4mpebgpPeerRemoteAddrType = 0,
        i4Fsbgp4mpebgpPrevPeerRemoteAddrType = 0;
    INT4                i4FsMIBgp4mpeRtRefreshStatisticsPeerType = 0,
        i4FsMIBgp4mpePrevRtRefreshStatisticsPeerType = 0;
    INT4                i4Afi = 0, i4PrevAfi = 0;
    INT4                i4Safi = 0, i4PrevSafi = 0;
    UINT1               au1lenght[BGP4_PEER_OCTET_LIST_LEN];
    UINT1               aulprevlength[BGP4_PEER_OCTET_LIST_LEN];
    UINT1               aulRtprevlength[BGP4_PEER_OCTET_LIST_LEN];
    UINT1               aulRtlength[BGP4_PEER_OCTET_LIST_LEN];

    tSNMP_OCTET_STRING_TYPE MpebgpPeerRemoteAddr;
    tSNMP_OCTET_STRING_TYPE MpebgpPrevPeerRemoteAddr;
    tSNMP_OCTET_STRING_TYPE FsMIBgp4mpeRtRefreshStatisticsPeerAddr;
    tSNMP_OCTET_STRING_TYPE FsMIBgp4mpePrevRtRefreshStatisticsPeerAddr;
    MpebgpPeerRemoteAddr.pu1_OctetList = au1lenght;
    MpebgpPrevPeerRemoteAddr.pu1_OctetList = aulprevlength;
    FsMIBgp4mpeRtRefreshStatisticsPeerAddr.pu1_OctetList = aulRtlength;
    FsMIBgp4mpePrevRtRefreshStatisticsPeerAddr.pu1_OctetList = aulRtprevlength;

    BgpLock ();
    u4MaxCxtValue =
        FsBGPSizingParams[MAX_BGP_CXT_NODES_SIZING_ID].u4PreAllocatedUnits;

    if (gBgpCxtNode == NULL)
    {
        BgpUnLock ();
        return;
    }

    for (u4TmpContext = 0; u4TmpContext < u4MaxCxtValue; u4TmpContext++)
    {
        if ((NULL != gBgpCxtNode[u4TmpContext])
            && (BGP4_STATUS (u4TmpContext) == BGP4_TRUE))
        {

            if (BgpSetContext ((INT4) u4TmpContext) == SNMP_FAILURE)
            {
                BgpUnLock ();
                return;
            }
            /*Peer Table */
            if ((nmhGetFirstIndexFsbgp4MpeBgpPeerTable
                 (&i4Fsbgp4mpebgpPeerRemoteAddrType,
                  &MpebgpPeerRemoteAddr)) != SNMP_FAILURE)
            {

                do
                {
                    BgpClearPeerCounter (i4Fsbgp4mpebgpPeerRemoteAddrType,
                                         &MpebgpPeerRemoteAddr);

                    i4Fsbgp4mpebgpPrevPeerRemoteAddrType =
                        i4Fsbgp4mpebgpPeerRemoteAddrType;
                    MpebgpPrevPeerRemoteAddr.i4_Length =
                        MpebgpPeerRemoteAddr.i4_Length;
                    MEMCPY (MpebgpPrevPeerRemoteAddr.pu1_OctetList,
                            MpebgpPeerRemoteAddr.pu1_OctetList,
                            MpebgpPrevPeerRemoteAddr.i4_Length);
                }
                while (nmhGetNextIndexFsbgp4MpeBgpPeerTable
                       (i4Fsbgp4mpebgpPrevPeerRemoteAddrType,
                        &i4Fsbgp4mpebgpPeerRemoteAddrType,
                        &MpebgpPrevPeerRemoteAddr,
                        &MpebgpPeerRemoteAddr) != BGP4_CLI_FAILURE);

            }
            /*Route Refresh Table */

            if ((nmhGetFirstIndexFsbgp4MpeRtRefreshStatisticsTable
                 (&i4FsMIBgp4mpeRtRefreshStatisticsPeerType,
                  &FsMIBgp4mpeRtRefreshStatisticsPeerAddr,
                  &i4Afi, &i4Safi)) != BGP4_CLI_FAILURE)
            {
                do
                {
                    BgpClearRouterRefreshCounter
                        (i4FsMIBgp4mpeRtRefreshStatisticsPeerType,
                         &FsMIBgp4mpeRtRefreshStatisticsPeerAddr, i4Afi,
                         i4Safi);

                    i4FsMIBgp4mpePrevRtRefreshStatisticsPeerType =
                        i4FsMIBgp4mpeRtRefreshStatisticsPeerType;
                    FsMIBgp4mpePrevRtRefreshStatisticsPeerAddr.i4_Length =
                        FsMIBgp4mpeRtRefreshStatisticsPeerAddr.i4_Length;
                    MEMCPY
                        (FsMIBgp4mpePrevRtRefreshStatisticsPeerAddr.
                         pu1_OctetList,
                         FsMIBgp4mpeRtRefreshStatisticsPeerAddr.pu1_OctetList,
                         FsMIBgp4mpeRtRefreshStatisticsPeerAddr.i4_Length);
                    i4PrevAfi = i4Afi;
                    i4PrevSafi = i4Safi;
                }
                while (nmhGetNextIndexFsbgp4MpeRtRefreshStatisticsTable
                       (i4FsMIBgp4mpePrevRtRefreshStatisticsPeerType,
                        &i4FsMIBgp4mpeRtRefreshStatisticsPeerType,
                        &FsMIBgp4mpePrevRtRefreshStatisticsPeerAddr,
                        &FsMIBgp4mpeRtRefreshStatisticsPeerAddr,
                        i4PrevAfi, &i4Afi,
                        i4PrevSafi, &i4Safi) != BGP4_CLI_FAILURE);

            }

            /*Prefix Counter Table */
            if (nmhGetFirstIndexFsbgp4MpePrefixCountersTable
                (&i4Fsbgp4mpebgpPeerRemoteAddrType, &MpebgpPeerRemoteAddr,
                 &i4Afi, &i4Safi) != BGP4_CLI_FAILURE)
            {
                do
                {
                    BgpClearPrefixCounter (i4Fsbgp4mpebgpPeerRemoteAddrType,
                                           &MpebgpPeerRemoteAddr, i4Afi,
                                           i4Safi);
                    i4Fsbgp4mpebgpPrevPeerRemoteAddrType =
                        i4Fsbgp4mpebgpPeerRemoteAddrType;
                    MpebgpPrevPeerRemoteAddr.i4_Length =
                        MpebgpPeerRemoteAddr.i4_Length;
                    MEMCPY (MpebgpPrevPeerRemoteAddr.pu1_OctetList,
                            MpebgpPeerRemoteAddr.pu1_OctetList,
                            MpebgpPrevPeerRemoteAddr.i4_Length);
                    i4PrevAfi = i4Afi;
                    i4PrevSafi = i4Safi;

                }
                while (nmhGetNextIndexFsbgp4MpePrefixCountersTable
                       (i4Fsbgp4mpebgpPrevPeerRemoteAddrType,
                        &i4Fsbgp4mpebgpPeerRemoteAddrType,
                        &MpebgpPrevPeerRemoteAddr, &MpebgpPeerRemoteAddr,
                        i4PrevAfi, &i4Afi, i4PrevSafi,
                        &i4Safi) != BGP4_CLI_FAILURE);

            }

        }
    }
    BgpUnLock ();
    return;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : BgpClearPeerCounter                                        */
/*                                                                           */
/* Description    This function clears the Peer counter table.               */
/*                                                                           */
/* INPUT        :i4Fsbgp4mpebgpPeerRemoteAddrType                            */
/*               pMpebgpPeerRemoteAddr                                       */
/*               i4Afi,i4Safi                                                */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
BgpClearPeerCounter (INT4 i4Fsbgp4mpebgpPeerRemoteAddrType,
                     tSNMP_OCTET_STRING_TYPE * pMpebgpPeerRemoteAddr)
{

    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddress;
    UINT4               u4Context = 0;
    INT4                i4Sts = 0;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    i4Sts =
        Bgp4LowGetIpAddress (i4Fsbgp4mpebgpPeerRemoteAddrType,
                             pMpebgpPeerRemoteAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME, "\tERROR - Unable to get the Peer Address.\n");
        return;
    }

    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeer == NULL)
    {
        return;
    }
    BGP4_PEER_IN_UPDATES (pPeer) = 0;
    BGP4_PEER_OUT_UPDATES (pPeer) = 0;
    BGP4_PEER_IN_MSGS (pPeer) = 0;
    BGP4_PEER_OUT_MSGS (pPeer) = 0;
    BGP4_PEER_LAST_ERROR (pPeer) = 0x00;
    BGP4_PEER_FSM_TRANS (pPeer) = 0;
    BGP4_PEER_ESTAB_TIME (pPeer) = 0;
    return;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : BgpClearRouterRefreshCounter                               */
/*                                                                           */
/* Description    This function clears the RouterRefresh table counters.     */
/*                                                                           */
/* INPUT        :i4FsMIBgp4mpeRtRefreshStatisticsPeerType                    */
/*               pFsMIBgp4mpeRtRefreshStatisticsPeerAddr                     */
/*               i4Afi,i4Safi                                                */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
BgpClearRouterRefreshCounter (INT4 i4FsMIBgp4mpeRtRefreshStatisticsPeerType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsMIBgp4mpeRtRefreshStatisticsPeerAddr,
                              INT4 i4Afi, INT4 i4Safi)
{

    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddress;
    UINT4               u4Context = 0;
    UINT4               u4Index = 0;
    INT4                i4Sts = 0;
    INT4                i4Status = 0;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    i4Sts =
        Bgp4LowGetIpAddress (i4FsMIBgp4mpeRtRefreshStatisticsPeerType,
                             pFsMIBgp4mpeRtRefreshStatisticsPeerAddr,
                             &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME, "\tERROR - Unable to get the Peer Address.\n");
        return;
    }

    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeer == NULL)
    {
        return;
    }
    i4Status = Bgp4GetAfiSafiIndex ((UINT2) i4Afi, (UINT1) i4Safi, &u4Index);
    if (i4Status == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME, "\tERROR - Unable to get AFI/SAFI Index.\n");
        return;
    }
    if ((pPeer->apAsafiInstance[u4Index] == NULL) ||
        (BGP4_PEER_RTREF_DATA_PTR (pPeer, u4Index) == NULL))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME,
                  "\tERROR - Route Refresh not enabled for Peer.\n");
        return;
    }
    BGP4_RTREF_MSG_SENT_CTR (pPeer, u4Index) = 0;
    BGP4_RTREF_MSG_TXERR_CTR (pPeer, u4Index) = 0;
    BGP4_RTREF_MSG_RCVD_CTR (pPeer, u4Index) = 0;
    BGP4_RTREF_MSG_RCVD_INVALID_CTR (pPeer, u4Index) = 0;
    return;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : BgpClearPrefixCounter                                      */
/*                                                                           */
/* Description    This function clears the Prefix counter table.             */
/*                                                                           */
/* INPUT        :i4Fsbgp4mpebgpPeerRemoteAddrType                            */
/*               pMpebgpPeerRemoteAddr                                        */
/*               i4Afi,i4Safi                                                */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
BgpClearPrefixCounter (INT4 i4Fsbgp4mpebgpPeerRemoteAddrType,
                       tSNMP_OCTET_STRING_TYPE * pMpebgpPeerRemoteAddr,
                       INT4 i4Afi, INT4 i4Safi)
{

    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddress;
    UINT4               u4Context = 0;
    INT4                i4Sts = 0;
    UINT4               u4AsafiMask = 0;

    u4Context = gpBgpCurrCxtNode->u4ContextId;
    i4Sts =
        Bgp4LowGetIpAddress (i4Fsbgp4mpebgpPeerRemoteAddrType,
                             pMpebgpPeerRemoteAddr, &PeerAddress);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                  BGP4_MOD_NAME, "\tERROR - Unable to get the Peer Address.\n");
        return;
    }

    pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
    if (pPeer == NULL)
    {
        return;
    }
    BGP4_GET_AFISAFI_MASK ((UINT2) i4Afi, (UINT1) i4Safi, u4AsafiMask);
    if (u4AsafiMask == CAP_MP_IPV4_UNICAST)
    {

        BGP4_PEER_IPV4_PREFIX_RCVD_CNT (pPeer) = 0;
        BGP4_PEER_IPV4_PREFIX_SENT_CNT (pPeer) = 0;
        BGP4_PEER_IPV4_WITHDRAWS_RCVD_CNT (pPeer) = 0;
        BGP4_PEER_IPV4_WITHDRAWS_SENT_CNT (pPeer) = 0;
        BGP4_PEER_IPV4_OUT_PREFIXES_CNT (pPeer) = 0;
    }
#ifdef BGP4_IPV6_WANTED
    if (u4AsafiMask == CAP_MP_IPV6_UNICAST)
    {
        BGP4_PEER_IPV6_PREFIX_RCVD_CNT (pPeer) = 0;
        BGP4_PEER_IPV6_PREFIX_SENT_CNT (pPeer) = 0;
        BGP4_PEER_IPV6_WITHDRAWS_RCVD_CNT (pPeer) = 0;
        BGP4_PEER_IPV6_WITHDRAWS_SENT_CNT (pPeer) = 0;
        BGP4_PEER_IPV6_OUT_PREFIXES_CNT (pPeer) = 0;
    }

#endif

    return;

}

/*PopulateBgpCounters is test code.This function is invoked from ISS.
 * It populates the BGP counters/statistics for verifying the BGP clear counters*/

/*****************************************************************************/
/*                                                                           */
/* Function     : PopulateBgpCounters                                        */
/*                                                                           */
/* Description    This function populates the bgp counter table.             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
PopulateBgpCounters (VOID)
{
    tAddrPrefix         peer;
    tBgp4PeerEntry     *peerEntry = NULL;
    UINT4               u4Value = 1;
    UINT4               u4Index = 0;
    UINT4               u4AsafiMask = 0;
    INT4                i4Status = 0;

    MEMSET (&peer, 0 , sizeof (tAddrPrefix));
    peer.u2Afi = 1;
    peer.u2AddressLen = 4;

    peer.au1Address[0] = 8;
    peer.au1Address[1] = 0;
    peer.au1Address[2] = 0;
    peer.au1Address[3] = 12;
    gpBgpCurrCxtNode = gBgpCxtNode[0];

    BgpLock ();
    peerEntry = Bgp4SnmphAddPeerEntry (0, peer);
    if (peerEntry == NULL)
    {
        BgpUnLock ();
        return;
    }

    if (BgpSetContext (0) == SNMP_FAILURE)
    {
        BgpUnLock ();
        return;
    }
    BGP4_STATUS (0) = BGP4_TRUE;
    BGP4_PEER_IN_UPDATES (peerEntry) = u4Value;
    BGP4_PEER_IN_UPDATES (peerEntry) = u4Value;
    BGP4_PEER_OUT_UPDATES (peerEntry) = u4Value;
    BGP4_PEER_IN_MSGS (peerEntry) = u4Value;
    BGP4_PEER_OUT_MSGS (peerEntry) = u4Value;
    BGP4_PEER_LAST_ERROR (peerEntry) = 0x1;
    BGP4_PEER_LAST_ERROR_SUB_CODE (peerEntry) = 0x5;
    BGP4_PEER_FSM_TRANS (peerEntry) = u4Value;
    BGP4_PEER_ESTAB_TIME (peerEntry) = u4Value;
    i4Status = Bgp4GetAfiSafiIndex ((UINT2) peer.u2Afi, 1, &u4Index);
    if (i4Status == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Unable to get AFI/SAFI Index.\n");
        BgpUnLock ();
        return;
    }

    BGP4_RTREF_MSG_SENT_CTR (peerEntry, u4Index) = u4Value;
    BGP4_RTREF_MSG_TXERR_CTR (peerEntry, u4Index) = u4Value;
    BGP4_RTREF_MSG_RCVD_CTR (peerEntry, u4Index) = u4Value;
    BGP4_RTREF_MSG_RCVD_INVALID_CTR (peerEntry, u4Index) = u4Value;

    BGP4_GET_AFISAFI_MASK ((UINT2) peer.u2Afi, 1, u4AsafiMask);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
            BGP4_PEER_IPV4_PREFIX_RCVD_CNT (peerEntry) = u4Value;
            BGP4_PEER_IPV4_PREFIX_SENT_CNT (peerEntry) = u4Value;
            BGP4_PEER_IPV4_WITHDRAWS_RCVD_CNT (peerEntry) = u4Value;
            BGP4_PEER_IPV4_WITHDRAWS_SENT_CNT (peerEntry) = u4Value;
            BGP4_PEER_IPV4_OUT_PREFIXES_CNT (peerEntry) = u4Value;
            break;
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
            BGP4_PEER_IPV6_PREFIX_RCVD_CNT (peerEntry) = u4Value;
            BGP4_PEER_IPV6_PREFIX_SENT_CNT (peerEntry) = u4Value;
            BGP4_PEER_IPV6_WITHDRAWS_RCVD_CNT (peerEntry) = u4Value;
            BGP4_PEER_IPV6_WITHDRAWS_SENT_CNT (peerEntry) = u4Value;
            BGP4_PEER_IPV6_OUT_PREFIXES_CNT (peerEntry) = u4Value;

            break;
#endif
        default:
            BgpUnLock ();
            return;
    }

    BgpUnLock ();
    return;
}
/******************************************************************************/
/* Function    :  Bgp4GetFirstIndexFsBgp4RRDNetworkTable                      */ 
/* Description :  This function gets the first index of the Network Table     */
/* Input       :  u4Context - context Id                                      */
/*                tAddrPrefix * pNetworkAddr                                  */
/* Output      :  None                                                        */
/* Returns     :  BGP4_SUCCESS/BGP4_FAILURE                                   */
/******************************************************************************/

tNetworkAddr *
Bgp4GetFirstIndexFsBgp4RRDNetworkTable (UINT4 u4Context)
{
    tNetworkAddr    *pNetworkRt = NULL;

    pNetworkRt = (tNetworkAddr *) 
        RBTreeGetFirst (BGP4_NETWORK_ROUTE_DATABASE(u4Context));
    if (pNetworkRt == NULL)
    {
        return NULL;
    }
    return pNetworkRt;
}
/******************************************************************************/
/* Function    :  Bgp4GetNextIndexFsBgp4RRDNetworkTable                       */
/* Description :  This function gets the next index of the Network Table      */
/* Input       :  u4Context - context Id                                      */
/*                tAddrPrefix * pNetworkAddr                                  */
/* Output      :  None                                                        */
/* Returns     :  BGP4_SUCCESS/BGP4_FAILURE                                   */
/******************************************************************************/

tNetworkAddr *Bgp4GetNextIndexFsBgp4RRDNetworkTable (UINT4 u4Context,
                                   tNetworkAddr  *pNetworkAddr) 
{
    tNetworkAddr    *pNextNetworkRt = NULL;

    pNextNetworkRt = (tNetworkAddr *) 
                     RBTreeGetNext (BGP4_NETWORK_ROUTE_DATABASE(u4Context), 
                     (tRBElem *) pNetworkAddr, Bgp4RRDNetworkTableCmp);
    if (pNextNetworkRt == NULL)
    {
        return NULL;
    }
    return pNextNetworkRt;
}

/******************************************************************************/
/* Function    :  Bgp4GetFsBgp4RRDNetworkTableIntegerObject                   */
/* Description :  This function gets the objects of network table             */
/* Input       :  u4Context - context Id                                      */
/*                tAddrPrefix * pNetworkAddr                                  */
/* Output      :  None                                                        */
/* Returns     :  BGP4_SUCCESS/BGP4_FAILURE                                   */
/******************************************************************************/
INT4 
Bgp4GetFsBgp4RRDNetworkTableIntegerObject (UINT4 u4Context,tNetworkAddr *pNetworkAddr,
                                                   INT4 i4ObjectType, INT4 *pi4RetVal)
{
    tNetworkAddr    *pNetworkRt = NULL;



    pNetworkRt = (tNetworkAddr *)
        RBTreeGet (BGP4_NETWORK_ROUTE_DATABASE(u4Context),
                (tRBElem *) pNetworkAddr) ;

    if ( pNetworkRt == NULL)
    {
        return BGP4_FAILURE;
    }
    switch (i4ObjectType)
    {
        case BGP4_NETWORK_ROUTE_ADDR_TYPE:
            *pi4RetVal = (INT4) pNetworkRt->NetworkAddr.u2Afi;
            break;
        case BGP4_NETWORK_ROUTE_ADDR_LEN:
            *pi4RetVal = (INT4) pNetworkRt->u2PrefixLen;
            break;
        case BGP4_NETWORK_ROUTE_ROWSTATUS:
            *pi4RetVal = (INT4) pNetworkRt->u4RowStatus;
            break;
        default:
            return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function    :  Bgp4SetFsBgp4RRDNetworkTableIntegerObject                   */
/* Description :  This function sets the objects of network table             */
/* Input       :  u4Context - context Id                                      */
/*                tAddrPrefix * pNetworkAddr                                  */
/* Output      :  None                                                        */
/* Returns     :  BGP4_SUCCESS/BGP4_FAILURE                                   */
/******************************************************************************/
INT4
Bgp4SetFsBgp4RRDNetworkTableIntegerObject (UINT4 u4Context,tNetworkAddr *pNetworkAddr,
        INT4 i4ObjectType, INT4 i4SetVal)
{
    tNetworkAddr    *pNetworkRt = NULL;

    pNetworkRt = (tNetworkAddr *)
        RBTreeGet (BGP4_NETWORK_ROUTE_DATABASE(u4Context),
                (tRBElem *) pNetworkAddr) ;

    if ( pNetworkRt == NULL)
    {
        return BGP4_FAILURE;
    }
    switch (i4ObjectType)
    {
        case BGP4_NETWORK_ROUTE_ADDR_TYPE:
            pNetworkRt->NetworkAddr.u2Afi = (UINT2) i4SetVal;
            break;
        case BGP4_NETWORK_ROUTE_ADDR_LEN:
            pNetworkRt->u2PrefixLen = (UINT2) i4SetVal;
            break;
        case BGP4_NETWORK_ROUTE_ROWSTATUS:
            pNetworkRt->u4RowStatus = (UINT4) i4SetVal;
            break;
        default:
            return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}
/******************************************************************************/
/* Function    :  Bgp4TestFsBgp4RRDNetworkTableIntegerObject                  */
/* Description :  This function tests values of the  objects of network table */
/* Input       :  u4Context - context Id                                      */
/*                tAddrPrefix * pNetworkAddr                                  */
/* Output      :  None                                                        */
/* Returns     :  BGP4_SUCCESS/BGP4_FAILURE                                   */
/******************************************************************************/

INT4 
Bgp4TestFsBgp4RRDNetworkTableIntegerObject (UINT4 u4Context,tNetworkAddr *pNetworkAddr,
                                             INT4 i4ObjectType, INT4 i4TestVal)   
{
    UINT4 u4NetAddress = 0;
    UINT4 u4Mask = 0;
    UINT1               u1ValidStatus;
#ifdef BGP4_IPV6_WANTED
    tIp6Addr            IPAddr;
    tIp6Addr            IPAddr1;
#endif
     
    if (pNetworkAddr == NULL)
    {
        return BGP4_FAILURE;
    }
    switch (i4ObjectType)
    {
        case BGP4_NETWORK_ROUTE_ADDR_TYPE:          
            /* Validate the prefix */
            if(pNetworkAddr->NetworkAddr.u2Afi == BGP4_INET_AFI_IPV4 ||
                     pNetworkAddr->NetworkAddr.u2Afi == BGP4_INET_AFI_IPV6)
     
            {               
                u1ValidStatus =
                    Bgp4IsValidAddress (pNetworkAddr->NetworkAddr, BGP4_TRUE);
                if (u1ValidStatus != BGP4_TRUE)
    {
        return BGP4_FAILURE;
    }
            }
            else
            {
                CLI_SET_ERR (CLI_BGP4_INVALID_NETWORK_MASK);
                return BGP4_FAILURE;
            } 
            break;
        case BGP4_NETWORK_ROUTE_ADDR_LEN:

            if(pNetworkAddr->NetworkAddr.u2Afi == BGP4_INET_AFI_IPV4)
            {                     
                PTR_FETCH4 (u4NetAddress,pNetworkAddr->NetworkAddr.au1Address);
                u4Mask = Bgp4GetSubnetmask (i4TestVal);
                /* Given Ip address should be Network Address*/
                if((u4NetAddress & (~u4Mask)))
                {                            
                    CLI_SET_ERR (CLI_BGP4_INVALID_NETWORK_MASK);
                    return BGP4_FAILURE;
                }
            }
#ifdef BGP4_IPV6_WANTED
            else if(pNetworkAddr->NetworkAddr.u2Afi == BGP4_INET_AFI_IPV6)
            {
                MEMCPY (IPAddr1.u1_addr,pNetworkAddr->NetworkAddr.au1Address,
                        BGP4_IPV6_PREFIX_LEN);
                Ip6CopyAddrBits (&IPAddr, &IPAddr1,i4TestVal);
                if(Ip6AddrMatch(&IPAddr,&IPAddr1,IP6_ADDR_SIZE_IN_BITS)
                                                        == FALSE)
    {
                    CLI_SET_ERR (CLI_BGP4_INVALID_NETWORK_MASK);
        return BGP4_FAILURE;
    }
            }      
#endif     
            break;
    }

    UNUSED_PARAM (u4Context);
    return BGP4_SUCCESS;

}

/******************************************************************************/
/* Function    :  Bgp4NetworkRouteCreateEntry                                 */
/* Description :  This function creates the network table entry               */
/* Input       :  u4Context - context Id                                      */
/*                tNetworkAddr * pNetworkAddr                                 */
/* Output      :  tNetworkAddr *pNetworkRoute                                 */
/* Returns     :  BGP4_SUCCESS/BGP4_FAILURE                                   */
/******************************************************************************/
INT4 
Bgp4NetworkRouteCreateEntry (UINT4 u4Context,tNetworkAddr *pNetworkAddr,
        tNetworkAddr *pNetworkRoute)
{
    tNetworkAddr    *pNetworkRt = NULL;

    pNetworkRt = (tNetworkAddr *)
        RBTreeGet (BGP4_NETWORK_ROUTE_DATABASE(u4Context),
                (tRBElem *) pNetworkAddr) ;

    if (pNetworkRt != NULL && 
        pNetworkRt->NetworkAddr.u2Afi == pNetworkAddr->NetworkAddr.u2Afi)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                "\tEntry Found.\n");
        return BGP4_FAILURE;
    }
    pNetworkRoute = Bgp4AllocateNetworkRtEntry(pNetworkRoute);
    if(pNetworkRoute == NULL)
    {
        return BGP4_FAILURE;
    }
    Bgp4CopyAddrPrefixStruct (&pNetworkRoute->NetworkAddr,pNetworkAddr->NetworkAddr);
    if (RBTreeAdd (BGP4_NETWORK_ROUTE_DATABASE(u4Context),
                (tRBElem *) pNetworkRoute) != RB_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                BGP4_MOD_NAME, "\tRBTree Add for Network route entry FAILED!!!\n");
        Bgp4ReleaseNetworkRtEntry (pNetworkRoute);
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS; 
}

/******************************************************************************/
/* Function    :  Bgp4NetworkRouteGetEntry                                    */
/* Description :  This function gets the entry from network route list        */
/* Input       :  u4Context - context Id                                      */
/*                tNetworkAddr *pNetworkAddr                                  */
/* Output      :  tNetworkAddr *pNetworkRoute                                 */
/* Returns     :  BGP4_SUCCESS/BGP4_FAILURE                                   */
/******************************************************************************/
tNetworkAddr * 
Bgp4NetworkRouteGetEntry (UINT4 u4Context, tNetworkAddr *pNetworkAddr,
                               tNetworkAddr *pNetworkRoute)
{
    
    pNetworkRoute = (tNetworkAddr *)
        RBTreeGet (BGP4_NETWORK_ROUTE_DATABASE(u4Context),
                (tRBElem *) pNetworkAddr) ;

    if (pNetworkRoute == NULL)
    {
        return NULL;
    }
   
    return pNetworkRoute;

}

/******************************************************************************/
/* Function    :  Bgp4NetworkRouteDeleteEntry                                 */
/* Description :  This function deletes the network entry from list           */
/* Input       :  u4Context - context Id                                      */
/*                tNetworkAddr *pNetworkAddr                                  */
/* Output      :  None                                                        */
/* Returns     :  BGP4_SUCCESS/BGP4_FAILURE                                   */
/******************************************************************************/
INT4 
Bgp4NetworkRouteDeleteEntry (UINT4 u4Context,tNetworkAddr *pNetworkRoute)
{
    tNetworkAddr    *pNetworkRt = NULL;

    pNetworkRt = (tNetworkAddr *)
        RBTreeGet (BGP4_NETWORK_ROUTE_DATABASE(u4Context),
                (tRBElem *) pNetworkRoute) ;

    if (pNetworkRt == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                "\tERROR - Entry Not Found.\n");
        return BGP4_FAILURE;
    }
    if (pNetworkRt->u2PrefixLen == pNetworkRoute->u2PrefixLen)
    {
        if (RBTreeRemove (BGP4_NETWORK_ROUTE_DATABASE(u4Context),
                    (tRBElem *) pNetworkRt) != RB_SUCCESS)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                    BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                    "\tERROR - RB Deletion Failure!!!!.\n");
            return BGP4_FAILURE;
        }
        Bgp4ReleaseNetworkRtEntry (pNetworkRt);
        return BGP4_SUCCESS;
    }
    return BGP4_FAILURE;
}   

/****************************************************************************
 Function    :  Bgp4DisableRRDProtoMask
 Description :  This function Disables the BGP RRD proto mask with the given
                 value and inform RTM the same.

 Input       :  u4Context - context Id
                i4RRDProtoMask - ProtoMask
 Output      :  None
 Returns     :  BGP4_SUCCESS/BGP4_FAILURE
****************************************************************************/
INT4
Bgp4DisableRRDProtoMask (UINT4 u4Context, UINT4 u4RRDProtoMask)
{
       tBgp4QMsg          *pQMsg = NULL;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
           BGP4_TRC (NULL, BGP4_TRC_FLAG,
                           BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                           "\tERROR - BGP Task not initialized.\n");
           return SNMP_FAILURE;
    }

    /* check if the BGP RRD Admin status is Enable */
    if (BGP4_RRD_ADMIN_STATE (u4Context) != BGP4_RRD_ENABLE)
    {
        /* Redistribution is not enabled. So just update the
         * redistribution protocol mask with the given value
         * and return */
        BGP4_RRD_PROTO_MASK (u4Context) &=
            (~u4RRDProtoMask);
    }
    else
    {
#ifdef RRD_WANTED
        BGP4_RRD_PROTO_MASK (u4Context) =
                    (BGP4_RRD_PROTO_MASK (u4Context) & (~u4RRDProtoMask));
        if (SendingMessageToRRDQueue
             (u4Context, u4RRDProtoMask,
             (UINT1) RTM_REDISTRIBUTE_DISABLE_MESSAGE,
              BGP4_RRD_RMAP_NAME (u4Context),
             (UINT2) BGP4_RRD_MATCH_DISABLE_MASK_VALUE (u4Context)) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
#ifdef BGP4_IPV6_WANTED
        if (SendingMessageToRRDQueue6
           (u4Context, u4RRDProtoMask,
           (UINT1) RTM6_REDISTRIBUTE_DISABLE_MESSAGE,
            BGP4_RRD_RMAP_NAME (u4Context),
            (UINT2) BGP4_RRD_MATCH_DISABLE_MASK_VALUE (u4Context)) ==
            SNMP_FAILURE)
        {
            return (SNMP_FAILURE);
        }
#endif
#endif
        pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
        if (pQMsg == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                      BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for posting BGP Peer Status Msg "
                      "to BGP Queue FAILED!!!\n");
            return SNMP_FAILURE;
        }

        BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_IGP_RRD_DISABLE_EVENT;
        BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
        BGP4_INPUTQ_DATA (pQMsg) = u4RRDProtoMask;
        BGP4_INPUTQ_CXT (pQMsg) = u4Context;
        if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
          return SNMP_SUCCESS;
}
#endif /* BGPSNIF4_C */
