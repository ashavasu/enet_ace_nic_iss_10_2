/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgp4red.c,v 1.17 2017/09/15 06:19:52 siva Exp $
 *
 * Description: This file contains BGP Redundancy related
 *              routines
 *
 *******************************************************************/
#ifndef BGP4RED_C
#define BGP4RED_C

#include "bgp4com.h"
INT1                gi1SyncStatus = 0;
extern INT1         gi1RFDSyncStatus;

#ifdef L2RED_WANTED
/************************************************************************/
/* Function Name      : Bgp4RedInitGlobalInfo                           */
/* Description        : This function will be invoked by the BGP4       */
/*                      to register with RM and initialize RM queue     */
/* Input(s)           : None                                            */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                     */
/************************************************************************/
PUBLIC INT4
Bgp4RedInitGlobalInfo (VOID)
{
    tRmRegParams        RmRegParams;
    tDbDescrParams      DbDescrParams;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_INIT_SHUT_TRC, BGP4_MOD_NAME,
              "Entered RedInitGlobalInfo\n");
    if (OsixQueCrt ((UINT1 *) BGP4_RM_PKT_QUE, OSIX_MAX_Q_MSG_LEN,
                    FsBGPSizingParams[MAX_BGP_RM_MSGS_SIZING_ID].
                    u4PreAllocatedUnits, &BGP4_RM_Q_ID) != OSIX_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "INIT: Queue Creation Failed\r\n");
        return BGP4_FAILURE;
    }

    MEMSET (&RmRegParams, 0, sizeof (tRmRegParams));

    RmRegParams.u4EntId = RM_BGP_APP_ID;
    RmRegParams.pFnRcvPkt = Bgp4RedRmCallBack;

    /* Register with RM */
    if (RmRegisterProtocols (&(RmRegParams)) == RM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tRegistration with RM failed\n");
        return BGP4_FAILURE;
    }
    MEMSET (&DbDescrParams, 0, sizeof (tDbDescrParams));

    DbDescrParams.u4ModuleId = RM_BGP_APP_ID;
    DbDescrParams.pDbDataDescList = NULL;
    DbUtilTblInit (&gBgpNode.DynInfoList, &DbDescrParams);
    BGP4_PREV_RED_NODE_STATUS = RM_INIT;
    BGP4_GET_NODE_STATUS = RM_INIT;
    BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_NOT_STARTED;
    BGP4_RED_INIT_BULK_STATUS = BGP4_FALSE;

    return BGP4_SUCCESS;
}

/************************************************************************/
/* Function Name      : Bgp4RedDeInitGlobalInfo                         */
/* Description        : This function will be invoked by the BGP4       */
/*                      to de-register with RM and delete RM queue      */
/* Input(s)           : None                                            */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                     */
/************************************************************************/
PUBLIC INT4
Bgp4RedDeInitGlobalInfo (VOID)
{
    if (RmDeRegisterProtocols (RM_BGP_APP_ID) == RM_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tDe-Registration with RM failed\n");
        return BGP4_FAILURE;
    }
    if (BGP4_RM_Q_ID != NULL)
    {
        Bgp4RedHandleRmEvents (BGP4_TRUE);
        OsixQueDel (BGP4_RM_Q_ID);
    }
    return BGP4_SUCCESS;
}

/************************************************************************/
/* Function Name      : Bgp4RedRmCallBack                                */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to BGP        */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/* Output(s)          : None                                            */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/
PUBLIC INT4
Bgp4RedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tBgpRmMsg          *pMsg = NULL;

    /* Callback function for RM events. The event and the message is sent as 
     * parameters to this function */

    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != RM_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tInvalid event received in RM \r\n");
        return OSIX_FAILURE;
    }

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Queue message associated with the event is not present */
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tQueue Message associated with the event "
                  "is not sent by RM \r\n");
        return OSIX_FAILURE;
    }

    pMsg = (tBgpRmMsg *) MemAllocMemBlk (BGP4_RED_MSG_POOL_ID);
    while (pMsg == NULL)
    {
        RmApiSetNoLock ();
        BgpLock ();
        Bgp4RedHandleRmEvents (BGP4_FALSE);
        BgpUnLock ();
        RmApiSetLock ();
        pMsg = (tBgpRmMsg *) MemAllocMemBlk (BGP4_RED_MSG_POOL_ID);
        /* this while loop will get executed only once as invocation of 
         * Bgp4RedHandleRmEvents will free a RM mem pool node. This verification
         * is done is while to avoid further null verification of pMsg */
    }

    MEMSET (pMsg, 0, sizeof (tBgpRmMsg));

    pMsg->pBuf = pData;
    pMsg->u1Event = u1Event;
    pMsg->u4DataLen = (UINT4) u2DataLen;

    /* Send the message associated with the event to BGP module */
    /* QueSend verification will never fail here because 
     * RM queue depth and BGP4_RED_MSG_POOL size is the same. When 
     * BGP4_RED_MSG_POOL memory block is available, queue will be
     * available for posting message. Failure verification is added
     * for obtaining any fatal corruption errors */
    if (OsixQueSend (BGP4_RM_Q_ID,
                     (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "Bgp4RedRmCallBack: Q send failure\r \n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, BGP4_SYSLOG_ID,
                      "Bgp4RedRmCallBack: BGP_RM_Q send failed\r\n"));
        MemReleaseMemBlock (BGP4_RED_MSG_POOL_ID, (UINT1 *) pMsg);
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            Bgp4RedRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return OSIX_FAILURE;
    }
    /* Post a event to BGP to process RM events */
    OsixEvtSend (BGP4_TASK_ID, BGP4_RM_EVENT);

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Bgp4RedHandleRmEvents                           */
/* Description        : This function is invoked by the BGP module to   */
/*                      process all the events and messages posted by   */
/*                      the RM module                                   */
/* Input(s)           : None                                            */
/* Output(s)          : None                                            */
/* Returns            : None                                            */
/************************************************************************/
PUBLIC VOID
Bgp4RedHandleRmEvents (UINT1 u1Flag)
{
    tBgpRmMsg          *pMsg = NULL;
    tRmNodeInfo        *pData = NULL;
    tRmProtoAck         ProtoAck;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4SeqNum = 0;

    MEMSET (&ProtoAck, 0, sizeof (tRmProtoAck));
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    while (OsixQueRecv (BGP4_RM_Q_ID,
                        (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        switch (pMsg->u1Event)
        {
            case GO_ACTIVE:

                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tReceived GO_ACTIVE event\r\n");
                Bgp4RedHandleGoActive ();
                break;
            case GO_STANDBY:
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tReceived GO_STANDBY event\r\n");
                Bgp4RedHandleGoStandby ();
                break;
            case RM_STANDBY_UP:
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tReceived RM_STANDBY_UP" " event \r\n");
                /* Standby up event, number of standby node is updated. 
                 * BulkReqRcvd flag is checked, if it is true, then Bulk update
                 * message is sent to the standby node and the flag is reset */

                pData = (tRmNodeInfo *) pMsg->pBuf;
                BGP4_RED_STANDBY_NODES = pData->u1NumStandby;
                Bgp4RedRmReleaseMemoryForMsg ((UINT1 *) pData);
                if (BGP4_RED_BULK_REQ_RCVD == OSIX_TRUE)
                {
                    BGP4_RED_BULK_REQ_RCVD = OSIX_FALSE;
                    BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_NOT_STARTED;
                    Bgp4RedSendBulkUpdMsg ();
                }
                break;
            case RM_STANDBY_DOWN:
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tReceived RM_STANDBY_DOWN " "event \r\n");
                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, BGP4_SYSLOG_ID,
                              "Bgp4RedHandleRmEvents: Received RM_STANDBY_DOWN "
                              "event"));
                /* Standby down event, number of standby nodes is updated */
                pData = (tRmNodeInfo *) pMsg->pBuf;
                BGP4_RED_STANDBY_NODES = pData->u1NumStandby;
                Bgp4RedRmReleaseMemoryForMsg ((UINT1 *) pData);
                break;
            case RM_MESSAGE:
                /* Read the sequence number from the RM Header */
                RM_PKT_GET_SEQNUM (pMsg->pBuf, &u4SeqNum);
                /* Remove the RM Header */
                RM_STRIP_OFF_RM_HDR (pMsg->pBuf, pMsg->u4DataLen);
                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, BGP4_SYSLOG_ID,
                              "Bgp4RedHandleRmEvents:Received RM_MESSAGE event"));
                ProtoAck.u4AppId = RM_BGP_APP_ID;
                ProtoAck.u4SeqNumber = u4SeqNum;

                if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
                {
                    /* Process the message at active */
                    Bgp4RedProcessPeerMsgAtActive (pMsg->pBuf);
                }
                else if (BGP4_GET_NODE_STATUS == RM_STANDBY)
                {
                    /* Process the message at standby */
                    Bgp4RedProcessPeerMsgAtStandby (pMsg->pBuf);
                }
                else
                {
                    /* Message is received at the idle node so ignore */
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                              BGP4_MGMT_TRC, BGP4_MOD_NAME,
                              "\tSync-up message received"
                              " at Idle Node!!!!\r\n");
                    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, BGP4_SYSLOG_ID,
                                  "Bgp4RedHandleRmEvents: Sync-up message "
                                  "received at Idle Node"));
                }
                RM_FREE (pMsg->pBuf);
                RmApiSendProtoAckToRM (&ProtoAck);
                break;
            case RM_CONFIG_RESTORE_COMPLETE:
                /* If the node status is init, and the rm state is standby,
                 * then the set the bgp node status to standby and inform RM */
                if (BGP4_GET_NODE_STATUS == RM_INIT)
                {
                    if (RmGetNodeState () == RM_STANDBY)
                    {
                        BGP4_PREV_RED_NODE_STATUS = BGP4_GET_NODE_STATUS;
                        BGP4_GET_NODE_STATUS = RM_STANDBY;
                        BGP4_RED_STANDBY_NODES = 0;
                        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                        RmApiHandleProtocolEvent (&ProtoEvt);
                    }
                }
                break;
            case RM_INITIATE_BULK_UPDATES:
                /* L2 Initiate bulk update is sent by RM to the standby node 
                 * to send a bulk update request to the active node */
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tReceived " "RM_INITIATE_BULK_UPDATES event.\r\n");
                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, BGP4_SYSLOG_ID,
                              "Bgp4RedHandleRmEvents: Received "
                              "RM_INITIATE_BULK_UPDATES"));
                BGP4_RED_INIT_BULK_STATUS = BGP4_TRUE;
                Bgp4RedVerifyAndSendBulkRequest ();
                break;
            default:
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                          BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tInvalid RM event received\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, BGP4_SYSLOG_ID,
                              "Bgp4RedHandleRmEvents: Invalid RM event received"));
                break;

        }
        MemReleaseMemBlock (BGP4_RED_MSG_MEMPOOL_ID, (UINT1 *) pMsg);
        if (u1Flag == BGP4_FALSE)
        {
            /* The function is invoked to process a single message.
             * break the loop here */
            break;
        }
    }
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, BGP4_SYSLOG_ID,
                  "Exiting Bgp4RedHandleRmEvents"));
    return;
}

/************************************************************************
 * Function Name      : Bgp4RedHandleGoActive                          
 * Description        : This function is invoked by the BGP upon
 *                      receiving the GO_ACTIVE indication from RM      
 *                      module. And this function responds to RM with an
 *                      acknowledgement.                                
 * Input(s)           : None                                            
 * Output(s)          : None                                            
 * Returns            : None                                            
 ************************************************************************/
PUBLIC VOID
Bgp4RedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, BGP4_SYSLOG_ID,
                  "Entering Bgp4RedHandleGoActive"));
    ProtoEvt.u4AppId = RM_BGP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Bulk update status is set to not started. */
    BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_NOT_STARTED;

    if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
    {
        /* Go active received by the active node, so ignore */
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tGO_ACTIVE event reached "
                  "when node is already active \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, BGP4_SYSLOG_ID,
                      "Bgp4RedHandleGoActive: GO_ACTIVE event reached when "
                      "node is already active"));
        return;
    }
    else if (BGP4_GET_NODE_STATUS == RM_INIT)
    {
        /* Go active received by idle node, so state changed to active */
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tIdle to Active transition...\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, BGP4_SYSLOG_ID,
                      "Bgp4RedHandleGoActive: Idle to Active transition"));

        BGP4_PREV_RED_NODE_STATUS = BGP4_GET_NODE_STATUS;
        BGP4_GET_NODE_STATUS = RM_ACTIVE;
        BGP4_RED_GET_NUM_STANDBY_NODES_UP ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    else if (BGP4_GET_NODE_STATUS == RM_STANDBY)
    {
        /* Go active received by standby node, 
         * Do hardware audit, and start the timers for all the arp entries 
         * and change the state to active */
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tStandby to Active transition...\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, BGP4_SYSLOG_ID,
                      "Bgp4RedHandleGoActive: Standby to Active transition"));
        Bgp4RedHandleStandbyToActive ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }
    if (BGP4_RED_BULK_REQ_RCVD == OSIX_TRUE)
    {
        /* If bulk update req received flag is true, send the bulk updates */
        BGP4_RED_BULK_REQ_RCVD = OSIX_FALSE;
        BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_NOT_STARTED;
        Bgp4RedSendBulkUpdMsg ();
    }
    RmApiHandleProtocolEvent (&ProtoEvt);
    return;
}

/************************************************************************/
/* Function Name      : Bgp4RedHandleStandbyToActive                     */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/* Input(s)           : None                                            */
/* Output(s)          : None                                            */
/* Returns            : None                                            */
/************************************************************************/
PUBLIC VOID
Bgp4RedHandleStandbyToActive (VOID)
{
    UINT4               u4CxtId = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT1                i1RetVal = BGP4_FAILURE;
    UINT1               u1FwdPath;

    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, BGP4_SYSLOG_ID,
                  "Entering Bgp4RedHandleStandbyToActive"));
    BGP4_PREV_RED_NODE_STATUS = BGP4_GET_NODE_STATUS;
    BGP4_GET_NODE_STATUS = RM_ACTIVE;
    BGP4_RED_GET_NUM_STANDBY_NODES_UP ();
    BGP4_GLB_RESTART_MODE = BGP4_RESTARTING_MODE;

    i4RetStatus = BgpGetFirstActiveContextID (&u4CxtId);
    while (i4RetStatus == SNMP_SUCCESS)
    {
        BGP4_SELECTION_DEFERAL_FLAG (u4CxtId) = BGP4_TRUE;
        BGP4_ROUTE_SELECTION_FLAG (u4CxtId) = BGP4_FALSE;
        BGP4_RESTART_MODE (u4CxtId) = BGP4_RESTARTING_MODE;
        BGP4_RESTART_REASON (u4CxtId) = BGP4_GR_REASON_UPGRADE;

        /* Notify RTM about BGP restart */
        BGP4_TRC_ARG1 (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC | BGP4_MGMT_TRC,
                       BGP4_MOD_NAME,
                       "\tNotify to RTM with  Stale route interval.\r\n");
        /* Mark all routes as stale in RIB and in RTM */
        Bgp4GRMarkAllRoutesAsStale (u4CxtId);
        Bgp4GRNotifyRestartRTM (u4CxtId, BGP_ID,
                                gBgpCxtNode[u4CxtId]->u4StalePathInterval);
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
        /* Mark VPLS SPEC entry Stale */
        Bgp4VplsMarkVplsEntryAsStale (u4CxtId);
        /* Mark EOVPLS flag False */
        BGP4_EOVPLS_FLAG (u4CxtId) = BGP4_FALSE;
        /* Notify to MPLS-L2VPN and mark autodiscovered PW as stale */
        Bgp4VplsGRInProgress (u4CxtId);
#endif
#endif

        /* Query RTM to get the address families preserved  across restart */
        i1RetVal =
            (INT1) Bgp4GRQueryRTMForwarding (u4CxtId, BGP4_INET_AFI_IPV4,
                                             &u1FwdPath);
        if (i1RetVal == BGP4_SUCCESS)
        {
            BGP4_GR_AFI_SUPPORT (u4CxtId) |= BGP4_INET_AFI_IPV4;
        }
#ifdef BGP4_IPV6_WANTED
        i1RetVal =
            (INT1) Bgp4GRQueryRTMForwarding (u4CxtId, BGP4_INET_AFI_IPV6,
                                             &u1FwdPath);
        if (i1RetVal == BGP4_SUCCESS)
        {
            BGP4_GR_AFI_SUPPORT (u4CxtId) |= BGP4_INET_AFI_IPV6;
        }
#endif /* BGP4_IPV6_WANTED */
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
        BGP4_GR_AFI_SUPPORT (u4CxtId) |= BGP4_INET_AFI_L2VPN;
#endif
#endif
        Bgp4PeerSetAllActivePeerAdminStatus (u4CxtId, BGP4_ADMIN_UP);
        if (BGP4_LOCAL_BGP_ID_CONFIG_TYPE (u4CxtId) ==
            BGP4_LOCAL_BGP_ID_DYNAMIC)
        {
            Bgp4GetDefaultBgpIdentifier (u4CxtId,
                                         &(BGP4_LOCAL_BGP_ID (u4CxtId)));
        }
        i4RetStatus = BgpGetNextActiveContextID (u4CxtId, &u4CxtId);
    }
    /* Start the restart timer with restart timer interval */
    gBgpNode.pBgpRestartTmr->u4Data = BGP4_RESTART_TIMER;
    BGP4_TIMER_START (BGP4_TIMER_LISTID, gBgpNode.pBgpRestartTmr,
                      BGP4_RESTART_TIME_INTERVAL (BGP4_DFLT_VRFID));

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC, BGP4_MOD_NAME,
              "\tNode Status Standby to " "Active\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, BGP4_SYSLOG_ID,
                  "Bgp4RedHandleStandbyToActive: Node Status Standby to Active"));
    return;
}

/*****************************************************************************/
/*    Function Name       : Bgp4RedSyncDynInfo                               */
/*    Description         : This function will initiate sync the BGP4        */
/*                          dynamic information present in the DB list       */
/*                          to RM using the DB utilities                     */
/*    Input(s)            : None                                             */
/*    Output(s)           : None                                             */
/*    Returns             : None.                                            */
/*****************************************************************************/
VOID
Bgp4RedSyncDynInfo (VOID)
{
    if ((BGP4_IS_STANDBY_UP () == OSIX_FALSE) ||
        (BGP4_GET_NODE_STATUS != RM_ACTIVE))
    {
        return;
    }

    DbUtilSyncModuleBuffers (&(gBgpNode.DynInfoList), BGP4_RED_MSG_MEMPOOL_ID);
    return;
}

/*****************************************************************************/
/*    Function Name       : Bgp4RedSyncBgpIdBulkMsg                          */
/*    Description         : This function will update the BGP identifier     */
/*                           and send information in the bulk message        */
/*    Input(s)            : tBgpRmMsg , pu4Offset                            */
/*    Output(s)           : None                                             */
/*    Returns             : BGP4_SUCCESS                                     */
/*****************************************************************************/

INT4
Bgp4RedSyncBgpIdBulkMsg (tBgpRmMsg * pMsg, UINT4 *pu4Offset)
{

    UINT4               u4Context = 0;
    UINT4               u4OffSet = *pu4Offset;
    UINT4               u4LenOffSet = 0;
    INT4                i4RetVal = BGP4_FAILURE;

    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedSyncBgpIdBulkMsg\r\n");
    i4RetVal = BgpGetFirstActiveContextID (&u4Context);
    while (i4RetVal == SNMP_SUCCESS)
    {
        BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, u4Context);
        BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                            BGP4_LOCAL_BGP_ID (u4Context));
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
    }
    u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
    BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
    DbUtilAddTblNode (&(gBgpNode.DynInfoList), &pMsg->DbNode);
    pMsg->u4DataLen = u4OffSet;
    Bgp4RedSyncDynInfo ();
    KW_FALSEPOSITIVE_FIX1 (pMsg);
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedSyncBgpIdBulkMsg\r\n");
    return BGP4_SUCCESS;

}

/*****************************************************************************/
/*    Function Name       : Bgp4RedSyncBgpAdminStatusBulkMsg /swa/           */
/*    Description         : This function will update the BGP admin status   */
/*                           and send information in the bulk message        */
/*    Input(s)            : tBgpRmMsg , pu4Offset                            */
/*    Output(s)           : None                                             */
/*    Returns             : BGP4_SUCCESS                                     */
/*****************************************************************************/

INT4
Bgp4RedSyncBgpAdminStatusBulkMsg (tBgpRmMsg * pMsg, UINT4 *pu4Offset,
                                  INT1 i1SyncStatus)
{
    UINT4               u4Context = 0;
    UINT4               u4OffSet = *pu4Offset;
    UINT4               u4LenOffSet = 0;
    INT4                i4RetVal = BGP4_FAILURE;

    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedSyncBgpAdminStatusBulkMsg\r\n");
    BGP4_RM_PUT_1_BYTE (pMsg->pBuf, &u4OffSet, i1SyncStatus);
    i4RetVal = BgpGetFirstActiveContextID (&u4Context);
    while (i4RetVal == SNMP_SUCCESS)
    {
        BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, u4Context);
        BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                            BGP4_ADMIN_STATUS (u4Context));
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
    }
    u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
    BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
    DbUtilAddTblNode (&(gBgpNode.DynInfoList), &pMsg->DbNode);
    pMsg->u4DataLen = u4OffSet;
    Bgp4RedSyncDynInfo ();
    KW_FALSEPOSITIVE_FIX1 (pMsg);
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedSyncBgpAdminStatusBulkMsg\r\n");
    return BGP4_SUCCESS;

}

/*****************************************************************************/
/*    Function Name       : Bgp4RedSyncRestartReasonBulkMsg                  */
/*    Description         : This function will update the BGP GR Restart     */
/*                          Reason using the bulk message                    */
/*    Input(s)            : tBgpRmMsg , pu4Offset                            */
/*    Output(s)           : None                                             */
/*    Returns             : BGP4_SUCCESS                                     */
/*****************************************************************************/

INT4
Bgp4RedSyncRestartReasonBulkMsg (tBgpRmMsg * pMsg, UINT4 *pu4Offset)
{

    UINT4               u4OffSet = *pu4Offset;
    UINT4               u4LenOffSet = 0;

    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedSyncRestartReasonBulkMsg\r\n");

    BGP4_RM_PUT_1_BYTE (pMsg->pBuf, &u4OffSet, BGP4_GLB_RESTART_REASON);

    u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
    BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
    DbUtilAddTblNode (&(gBgpNode.DynInfoList), &pMsg->DbNode);
    pMsg->u4DataLen = u4OffSet;
    Bgp4RedSyncDynInfo ();
    KW_FALSEPOSITIVE_FIX1 (pMsg);
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedSyncRestartReasonBulkMsg\r\n");
    return BGP4_SUCCESS;

}

/*****************************************************************************/
/*    Function Name       : Bgp4RedSyncRRDMetricMaskBulkMsg                  */
/*    Description         : This function will update the BGP RRD Metric     */
/*                          Mask using the bulk message                      */
/*    Input(s)            : tBgpRmMsg , pu4Offset                            */
/*    Output(s)           : None                                             */
/*    Returns             : BGP4_SUCCESS                                     */
/*****************************************************************************/

INT4
Bgp4RedSyncRRDMetricMaskBulkMsg (tBgpRmMsg * pMsg, UINT4 *pu4Offset,
                                 UINT4 u4CxtId)
{

    UINT4               u4OffSet = *pu4Offset;
    UINT4               u4LenOffSet = 0;
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedSyncRRDMetricMaskBulkMsg\r\n");
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, u4CxtId);
    BGP4_RM_PUT_1_BYTE (pMsg->pBuf, &u4OffSet, BGP4_RRD_METRIC_MASK (u4CxtId));

    u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
    BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
    DbUtilAddTblNode (&(gBgpNode.DynInfoList), &pMsg->DbNode);
    pMsg->u4DataLen = u4OffSet;
    Bgp4RedSyncDynInfo ();
    KW_FALSEPOSITIVE_FIX1 (pMsg);
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedSyncRRDMetricMaskBulkMsg\r\n");
    return BGP4_SUCCESS;

}

/*****************************************************************************/
/*    Function Name       : Bgp4RedSyncTableVersionBulkMsg                   */
/*    Description         : This function will update the BGP Table Version  */
/*                          using the bulk message                           */
/*    Input(s)            : tBgpRmMsg , pu4Offset                            */
/*    Output(s)           : None                                             */
/*    Returns             : BGP4_SUCCESS                                     */
/*****************************************************************************/

INT4
Bgp4RedSyncTableVersionBulkMsg (tBgpRmMsg * pMsg, UINT4 *pu4Offset)
{

    UINT4               u4Context = 0;
    UINT4               u4OffSet = *pu4Offset;
    UINT4               u4LenOffSet = 0;
    INT4                i4RetVal = BGP4_FAILURE;

    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedSyncTableVersionBulkMsg\r\n");
    i4RetVal = BgpGetFirstActiveContextID (&u4Context);
    while (i4RetVal == SNMP_SUCCESS)
    {
        BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, u4Context);
        BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                            BGP4_TABLE_VERSION (u4Context));
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
    }
    u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
    BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
    DbUtilAddTblNode (&(gBgpNode.DynInfoList), &pMsg->DbNode);
    pMsg->u4DataLen = u4OffSet;
    Bgp4RedSyncDynInfo ();
    KW_FALSEPOSITIVE_FIX1 (pMsg);
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedSyncTableVersionBulkMsg\r\n");
    return BGP4_SUCCESS;

}

/*****************************************************************************/
/*    Function Name       : Bgp4RedSyncPeerDampBulkMsg                       */
/*    Description         : This function will scan through all the BGP      */
/*                          peers and send the Dampened information in the   */
/*                          bulk message                                     */
/*    Input(s)            : None                                             */
/*    Output(s)           : None                                             */
/*    Returns             : None.                                            */
/*****************************************************************************/
INT4
Bgp4RedSyncPeerDampBulkMsg (VOID)
{
    tPeerDampHist      *pPeerHist = NULL;
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4Context = 0;
    UINT4               u4OffSet = 0;
    UINT4               u4LenOffSet = 0;
    UINT4               u4HashIndex = 0;
    INT4                i4RetVal = BGP4_FAILURE;

    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedSyncPeerDampBulkMsg\r\n");

    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_BULK_UPD_PEER_MESSAGE, &u4OffSet)
        == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }

    /* Save the offset for writing the length of the bulk update message
     * and increment the offset to start writing the peer damp status */
    u4LenOffSet = u4OffSet;
    u4OffSet += 2;

    i4RetVal = BgpGetFirstActiveContextID (&u4Context);
    while (i4RetVal == SNMP_SUCCESS)
    {
        if (PEERS_DAMP_HIST_LIST (u4Context) == NULL)
        {
            i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
            continue;
        }
        TMO_HASH_Scan_Table (PEERS_DAMP_HIST_LIST (u4Context), u4HashIndex)
        {
            TMO_HASH_Scan_Bucket (PEERS_DAMP_HIST_LIST (u4Context), u4HashIndex,
                                  pPeerHist, tPeerDampHist *)
            {
                if (u4OffSet >= BGP4_RED_PEER_MSG_MAX_LEN)
                {
                    /* If the u2OffSet is not sufficient to write the next peer
                     * damp information. write the length of the message and
                     * allocate new msg and rm buffer for writing remaining 
                     * peer information */
                    u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
                    BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
                    DbUtilAddTblNode (&(gBgpNode.DynInfoList), &pMsg->DbNode);
                    pMsg->u4DataLen = u4OffSet;
                    KW_FALSEPOSITIVE_FIX (pMsg);
                    pMsg = NULL;
                    u4OffSet = 0;
                    if (Bgp4RedGetBgpRmMsg
                        (&pMsg, BGP4_RED_BULK_UPD_PEER_MESSAGE,
                         &u4OffSet) == BGP4_FAILURE)
                    {
                        return BGP4_FAILURE;
                    }
                    u4OffSet += 2;
                }
                BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, u4Context);
                Bgp4RedFillBufAddr (RFD_PEER_INFO_IN_PEER_DAMPHIST_STRUCT
                                    (pPeerHist), pMsg->pBuf, &u4OffSet);
                BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, pPeerHist->PeerFom);
            }
        }
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
    }
    u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
    BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
    DbUtilAddTblNode (&(gBgpNode.DynInfoList), &pMsg->DbNode);
    pMsg->u4DataLen = u4OffSet;
    Bgp4RedSyncDynInfo ();
    KW_FALSEPOSITIVE_FIX1 (pMsg);
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedSyncPeerDampBulkMsg\r\n");
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : Bgp4RedGetBgpRmMsg                               */
/*    Description         : This function will allocates a Bgp RM message    */
/*                          writes the message type and add the node to the  */
/*                          Bgp data descriptor table                        */
/*    Input(s)            : u1Message - Bgp RM message type                  */
/*    Output(s)           : pu4OffSet - Offset of the written message        */
/*    Returns             : BGP4_SUCCESS/BGP4_FAILURE                        */
/*****************************************************************************/
INT4
Bgp4RedGetBgpRmMsg (tBgpRmMsg ** pMsg, UINT1 u1Message, UINT4 *pu4OffSet)
{
    tRmProtoEvt         ProtoEvt;
    tBgpRmMsg          *pBgpMsg = NULL;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    if ((BGP4_IS_STANDBY_UP () == OSIX_FALSE) ||
        (BGP4_GET_NODE_STATUS != RM_ACTIVE))
    {
        return BGP4_FAILURE;
    }

    if ((pBgpMsg =
         (tBgpRmMsg *) MemAllocMemBlk (BGP4_RED_MSG_MEMPOOL_ID)) == NULL)
    {

        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tUnable to allocate node for BgpRmMsg\n");
        gu4BgpDebugCnt[MAX_BGP_RM_MSGS_SIZING_ID]++;
        return BGP4_FAILURE;
    }
    MEMSET (pBgpMsg, 0, sizeof (tBgpRmMsg));

    pBgpMsg->pBuf = RM_ALLOC_TX_BUF (BGP4_RED_MSG_MAX_LEN);
    if (pBgpMsg->pBuf == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tRM Memory allocation failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, BGP4_SYSLOG_ID,
                      "Bgp4RedGetBgpRmMsg: RM Memory allocation failed"));
        MemReleaseMemBlock (BGP4_RED_MSG_MEMPOOL_ID, (UINT1 *) pBgpMsg);
        ProtoEvt.u4AppId = RM_BGP_APP_ID;
        ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return BGP4_FAILURE;
    }
    BGP4_RM_PUT_1_BYTE (pBgpMsg->pBuf, pu4OffSet, u1Message);
    DbUtilNodeInit (&(pBgpMsg->DbNode), 0);

    *pMsg = pBgpMsg;
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : Bgp4RedProcessPeerDampMsg                        */
/*    Description         : This function will process the peer damp message */
/*                          received from Active instance and update the     */
/*                          peer damp information for the peer               */
/*    Input(s)            : pMsg - Peer Damp Update message                  */
/*                          pu4OffSet - Offset of the update message         */
/*    Output(s)           : None                                             */
/*    Returns             : None.                                            */
/*****************************************************************************/
PUBLIC VOID
Bgp4RedProcessPeerDampMsg (tRmMsg * pMsg, UINT4 *pu4OffSet, UINT2 u2LenOffSet)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddress;
    tPeerDampHist      *pPeerDampHist = NULL;
    UINT4               u4Context = 0;
    UINT4               u4Fom = 0;
    INT4                i4RetVal = BGP4_FAILURE;
    UINT1               u1IsNewDampHist = BGP4_FALSE;

    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedProcessPeerDampMsg\r\n");

    while (*pu4OffSet < (UINT4) u2LenOffSet)
    {
        BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4Context);
        MEMSET (&PeerAddress, 0, sizeof (tAddrPrefix));
        Bgp4RedReadAddrPrefix (pMsg, pu4OffSet, &PeerAddress);
        BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4Fom);
        pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddress);
        if (pPeer == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC, BGP4_MOD_NAME,
                      "\tPeer entry is not present\n");
            continue;
        }
        i4RetVal = RfdPDHFetchCreatePeerDampHist (pPeer, &pPeerDampHist,
                                                  &u1IsNewDampHist);
        if ((i4RetVal == RFD_FAILURE) || (pPeerDampHist == NULL))
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC,
                      BGP4_MOD_NAME, "\t"
                      "Creating/fetching damp history failed\n");
            continue;
        }
        Bgp4RedUpdPeerFOM (u4Context, pPeerDampHist, u4Fom);
        i4RetVal = RfdInsertPeerReuseList (pPeer);
        if (i4RetVal == RFD_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_DAMP_TRC,
                      BGP4_MOD_NAME, "\t"
                      "Inserting in peer reuse list failed\n");
            continue;
        }
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedProcessPeerDampMsg\r\n");
    return;
}

/*****************************************************************************/
/*    Function Name       : Bgp4RedProcessBgpPeerInfoSyncMsg                */
/*    Description         : This function will process the Bgp neighbor info    */
/*                          message received from Active instance            */
/*                          and update the information                       */
/*    Input(s)            : pMsg                                             */
/*                          pu4OffSet                                        */
/*    Output(s)           : None                                             */
/*    Returns             : None.                                            */
/*****************************************************************************/
PUBLIC VOID
Bgp4RedProcessBgpPeerInfoSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                  UINT2 u2LenOffSet)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddr;
    tAddrPrefix         LocalAddr;
    UINT4               u4Context = 0;
    UINT4               u4RemRouterId = 0;
    UINT1               u1NegVersion = 0;
    UINT1               u1AdminStatus = 0;

    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedProcessBgpPeerInfoSyncMsg\r\n");

    while (*pu4OffSet < (UINT4) u2LenOffSet)
    {
        BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4Context);
#ifndef VRF_WANTED
        if (u4Context != BGP_DEF_CONTEXTID)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                      "Bgp4RedProcessBgpPeerInfoSyncMsg: Invalid context\r\n");
            return;
        }
#endif
        TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
        {
            if (pPeer != NULL)
            {
                Bgp4RedReadAddrPrefix (pMsg, pu4OffSet, &PeerAddr);
                Bgp4RedReadAddrPrefix (pMsg, pu4OffSet, &LocalAddr);
                BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4RemRouterId);
                BGP4_RM_GET_1_BYTE (pMsg, pu4OffSet, u1NegVersion);
                BGP4_RM_GET_1_BYTE (pMsg, pu4OffSet, u1AdminStatus);
                Bgp4CopyAddrPrefixStruct (&BGP4_PEER_LOCAL_ADDR_INFO (pPeer),
                                          LocalAddr);
                BGP4_PEER_BGP_ID (pPeer) = u4RemRouterId;
                BGP4_PEER_NEG_VER (pPeer) = u1NegVersion;
                BGP4_PEER_ADMIN_STATUS (pPeer) = u1AdminStatus;
            }
        }

    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedProcessBgpPeerInfoSyncMsg\r\n");
    return;

}

/*****************************************************************************/
/*    Function Name       : Bgp4RedProcessBgpPeerInfoDynamicSyncMsg                */
/*    Description         : This function will process the Bgp neighbor info    */
/*                          message received from Active instance            */
/*                          and update the information                       */
/*    Input(s)            : pMsg                                             */
/*                          pu4OffSet                                        */
/*    Output(s)           : None                                             */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Bgp4RedProcessBgpPeerInfoDynamicSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                         UINT2 u2LenOffSet)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddr;
    tAddrPrefix         LocalAddr;
    UINT4               u4Context = 0;
    UINT4               u4RemRouterId = 0;
    UINT1               u1NegVersion = 0;

    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedProcessBgpPeerInfoDynamicSyncMsg\r\n");

    if (*pu4OffSet < (UINT4) u2LenOffSet)
    {
        BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4Context);
#ifndef VRF_WANTED
        if (u4Context != BGP_DEF_CONTEXTID)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "Bgp4RedProcessBgpPeerInfoDynamicSyncMsg : invalid context ID.\r\n");
            return;
        }
#endif

        Bgp4RedReadAddrPrefix (pMsg, pu4OffSet, &PeerAddr);
        pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddr);
        if (pPeer == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                      BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                      "\tERROR - No matching Peer exist.\n");
            return;
        }

        Bgp4RedReadAddrPrefix (pMsg, pu4OffSet, &LocalAddr);
        BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4RemRouterId);
        BGP4_RM_GET_1_BYTE (pMsg, pu4OffSet, u1NegVersion);
        Bgp4CopyAddrPrefixStruct (&BGP4_PEER_LOCAL_ADDR_INFO (pPeer),
                                  LocalAddr);
        BGP4_PEER_BGP_ID (pPeer) = u4RemRouterId;
        BGP4_PEER_NEG_VER (pPeer) = u1NegVersion;

    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedProcessBgpPeerInfoDynamicSyncMsg\r\n");
    return;

}

/*****************************************************************************/
/*    Function Name       : Bgp4RedProcessBgpIDSyncMsg   /swa/               */
/*    Description         : This function will process the Bgp Identifier    */
/*                          message received from Active instance            */
/*                          and update the information                       */
/*    Input(s)            : pMsg                                             */
/*                          pu4OffSet                                        */
/*    Output(s)           : None                                             */
/*    Returns             : None.                                            */
/*****************************************************************************/
PUBLIC VOID
Bgp4RedProcessBgpIDSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet, UINT2 u2LenOffSet)
{
    UINT4               u4Context = 0;
    UINT4               u4BgpId = 0;

    UNUSED_PARAM (u2LenOffSet);
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedProcessBgpIDSyncMsg\r\n");

    while (*pu4OffSet < (UINT4) u2LenOffSet)
    {
        BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4Context);
        BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4BgpId);
        BGP4_LOCAL_BGP_ID (u4Context) = u4BgpId;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedProcessBgpIDSyncMsg\r\n");
    return;

}

/*****************************************************************************/
/*    Function Name       : Bgp4RedProcessBgpAdminStatusSyncMsg              */
/*    Description         : This function will process the Bgp Identifier    */
/*                          message received from Active instance            */
/*                          and update the information                       */
/*                 This Function is used to  sync both RFD STATUS   */
/*                          and BGP Admin Status                  */
/*    Input(s)            : pMsg                                             */
/*                          pu4OffSet                                        */
/*    Output(s)           : None                                             */
/*    Returns             : None.                                            */
/*****************************************************************************/
PUBLIC VOID
Bgp4RedProcessBgpAdminStatusSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                     UINT2 u2LenOffSet)
{
    UINT4               u4Context = 0;
    UINT4               u4BgpId = 0;

    UNUSED_PARAM (u2LenOffSet);
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedProcessBgpAdminStatusSyncMsg\r\n");

    BGP4_RM_GET_1_BYTE (pMsg, pu4OffSet, gi1RFDSyncStatus);

    while (*pu4OffSet < (UINT4) u2LenOffSet)
    {
        BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4Context);
        BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4BgpId);
        BGP4_ADMIN_STATUS (u4Context) = u4BgpId;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedProcessBgpAdminStatusSyncMsg\r\n");
    return;

}

/*****************************************************************************/
/*    Function Name       : Bgp4RedProcessMpathOperCntSyncMsg                */
/*    Description         : This function will process the Operational Mpath */
/*                          Count message received from Active instance      */
/*                          and update the information                       */
/*    Input(s)            : pMsg                                             */
/*                          pu4OffSet                                        */
/*    Output(s)           : None                                             */
/*    Returns             : None.                                            */
/*****************************************************************************/
PUBLIC VOID
Bgp4RedProcessMpathOperCntSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                   UINT2 u2LenOffSet)
{
    UINT4               u4Context = 0;
    UINT4               u4OperIbgpCnt = 0;
    UINT4               u4OperEbgpCnt = 0;
    UINT4               u4OperEIbgpCnt = 0;

    UNUSED_PARAM (u2LenOffSet);
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedProcessMpathOperCntSyncMsg\r\n");

    while (*pu4OffSet < (UINT4) u2LenOffSet)
    {
        BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4Context);
        BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4OperIbgpCnt);
        BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4OperEbgpCnt);
        BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4OperEIbgpCnt);
        BGP4_MPATH_OPER_EBGP_COUNT (u4Context) = u4OperEbgpCnt;
        BGP4_MPATH_OPER_IBGP_COUNT (u4Context) = u4OperIbgpCnt;
        BGP4_MPATH_OPER_EIBGP_COUNT (u4Context) = u4OperEIbgpCnt;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedProcessMpathOperCntSyncMsg\r\n");
    return;

}

/*****************************************************************************/
/*    Function Name       : Bgp4RedProcessRestartReasonSyncMsg               */
/*    Description         : This function will process the Bgp GR Restart    */
/*                          Reason  received from Active instance            */
/*                          and update the information                       */
/*    Input(s)            : pMsg                                             */
/*                          pu4OffSet                                        */
/*    Output(s)           : None                                             */
/*    Returns             : None.                                            */
/*****************************************************************************/
PUBLIC VOID
Bgp4RedProcessRestartReasonSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                    UINT2 u2LenOffSet)
{
    UINT1               u1RestartReason = 0;

    UNUSED_PARAM (u2LenOffSet);
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedProcessRestartReasonSyncMsg\r\n");
    BGP4_RM_GET_1_BYTE (pMsg, pu4OffSet, u1RestartReason);
    BGP4_GLB_RESTART_REASON = u1RestartReason;
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedProcessRestartReasonSyncMsg\r\n");
    return;

}

/*****************************************************************************/
/*    Function Name       : Bgp4RedProcessRRDMetricMaskSyncMsg               */
/*    Description         : This function will process the Bgp               */
/*                BGP4_RRD_METRIC_MASK                             */
/*    Input(s)            : pMsg                                             */
/*                          pu4OffSet                                        */
/*    Output(s)           : None                                             */
/*    Returns             : None.                                            */
/*****************************************************************************/
PUBLIC VOID
Bgp4RedProcessRRDMetricMaskSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                    UINT2 u2LenOffSet)
{
    UINT4               u4Context = 0;

    UNUSED_PARAM (u2LenOffSet);
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedProcessRRDMetricMaskSyncMsg\r\n");
    BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4Context);
    BGP4_RM_GET_1_BYTE (pMsg, pu4OffSet, BGP4_RRD_METRIC_MASK (u4Context));
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedProcessRRDMetricMaskSyncMsg\r\n");
    return;

}

/*****************************************************************************/
/*    Function Name       : Bgp4RedProcessTableVersionSyncMsg                */
/*    Description         : This function will process the Bgp Table         */
/*                          Version received from Active instance            */
/*                          and update the information                       */
/*    Input(s)            : pMsg                                             */
/*                          pu4OffSet                                        */
/*    Output(s)           : None                                             */
/*    Returns             : None.                                            */
/*****************************************************************************/
PUBLIC VOID
Bgp4RedProcessTableVersionSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                   UINT2 u2LenOffSet)
{
    UINT4               u4Context = 0;
    UINT4               u4TableVersion = 0;

    UNUSED_PARAM (u2LenOffSet);
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedProcessTableVersionSyncMsg\r\n");

    while (*pu4OffSet < (UINT4) u2LenOffSet)
    {
        BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4Context);
        BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4TableVersion);
        BGP4_TABLE_VERSION (u4Context) = u4TableVersion;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedProcessTableVersionSyncMsg\r\n");
    return;

}

/*****************************************************************************/
/*    Function Name       : Bgp4RedProcessBgpPeerMPCapV4SyncMsg                */
/*    Description         : This function will process the Bgp Peer MP Cap   */
/*                          Sync message received from Active instance       */
/*                          and update the information                       */
/*    Input(s)            : pMsg                                             */
/*                          pu4OffSet                                        */
/*    Output(s)           : None                                             */
/*    Returns             : None.                                            */
/*****************************************************************************/
PUBLIC VOID
Bgp4RedProcessBgpPeerMPCapV4SyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                     UINT2 u2LenOffSet)
{
    UINT4               u4Context = 0;
    UINT4               u4PeerCount = 0;
    tAddrPrefix         RemoteAddrInfo;
    tBgp4PeerEntry     *pPeer = NULL;
    tBgp4PeerEntry     *pDupPeer = NULL;
    tSupCapsInfo       *pCapsInfo = NULL;
    tSupCapsInfo       *pTmpCapsInfo = NULL;
    UINT1               u1RemMPCapFlag = BGP4_FALSE;
    tSNMP_OCTET_STRING_TYPE CapValue;
    UINT1               u1ResField = 0;
    UINT1               au1MpcapOctetList[BGP4_MPE_MP_CAP_LEN];

    MEMSET (&RemoteAddrInfo, 0, sizeof (tAddrPrefix));

    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedProcessBgpPeerMPCapV4SyncMsg\r\n");
    while (*pu4OffSet < (UINT4) u2LenOffSet)
    {
        BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4Context);
        BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4PeerCount);
        while (u4PeerCount > 0)
        {
            Bgp4RedReadAddrPrefix (pMsg, pu4OffSet, &RemoteAddrInfo);
            pPeer = Bgp4SnmphGetPeerEntry (u4Context, RemoteAddrInfo);
            if (pPeer == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Peer exist.\n");
                return;
            }
            SPKR_SUP_CAPS_ENTRY_CREATE (pCapsInfo);
            if (pCapsInfo == NULL)
            {
                return;
            }
            MEMSET (au1MpcapOctetList, 0, BGP4_MPE_MP_CAP_LEN);
            CapValue.pu1_OctetList = au1MpcapOctetList;
            PTR_ASSIGN2 (CapValue.pu1_OctetList, BGP4_INET_AFI_IPV4);
            *((CapValue.pu1_OctetList) + BGP4_MPE_ADDRESS_FAMILY_LEN)
                = u1ResField;
            *((CapValue.pu1_OctetList) + BGP4_MPE_ADDRESS_FAMILY_LEN
              + BGP4_MPE_MP_CAP_RESERVED_FIELD_LEN) = BGP4_INET_SAFI_UNICAST;

            CapValue.i4_Length = BGP4_MPE_MP_CAP_LEN;

            GetFindSpkrSupCap (pPeer,
                               CAP_CODE_MP_EXTN,
                               CAP_MP_CAP_LENGTH,
                               &CapValue, CAPS_SUP, &pTmpCapsInfo);

            BGP4_RM_GET_1_BYTE (pMsg, pu4OffSet, u1RemMPCapFlag);
            if (u1RemMPCapFlag == BGP4_TRUE)
            {
                BGP4_RM_GET_N_BYTE (pMsg, pu4OffSet, pCapsInfo,
                                    sizeof (tSupCapsInfo));
                if (pTmpCapsInfo == NULL)
                {
                    if ((CapsSpkrSupCapCreate (pPeer,
                                               CAP_CODE_MP_EXTN,
                                               CAP_MP_CAP_LENGTH,
                                               &CapValue,
                                               &pTmpCapsInfo)) == CAPS_FAILURE)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                                  BGP4_MOD_NAME,
                                  "\tERROR - No matching Peer exist.\n");

                        return;
                    }
                    pTmpCapsInfo->u1RowStatus = pCapsInfo->u1RowStatus;
                    pTmpCapsInfo->u1CapsFlag = pCapsInfo->u1CapsFlag;
                    pTmpCapsInfo->u1ConfiguredFlag =
                        pCapsInfo->u1ConfiguredFlag;
                }
                else
                {
                    pTmpCapsInfo->SupCapability.u1CapCode =
                        pCapsInfo->SupCapability.u1CapCode;
                    pTmpCapsInfo->SupCapability.u1CapLength =
                        pCapsInfo->SupCapability.u1CapLength;
                    if ((pCapsInfo->SupCapability.u1CapLength > 0) &&
                        (pCapsInfo->SupCapability.u1CapLength <=
                         MAX_CAP_LENGTH))
                    {
                        MEMCPY (pTmpCapsInfo->SupCapability.au1CapValue,
                                pCapsInfo->SupCapability.au1CapValue,
                                pCapsInfo->SupCapability.u1CapLength);
                    }
                    pTmpCapsInfo->u1RowStatus = pCapsInfo->u1RowStatus;
                    pTmpCapsInfo->u1CapsFlag = pCapsInfo->u1CapsFlag;
                    pTmpCapsInfo->u1ConfiguredFlag =
                        pCapsInfo->u1ConfiguredFlag;
                }
            }
            if (u1RemMPCapFlag == BGP4_FALSE && pTmpCapsInfo != NULL)
            {
                TMO_SLL_Delete (BGP4_PEER_SUP_CAPS_LIST (pPeer),
                                &pTmpCapsInfo->NextCapability);
                if (SPKR_SUP_CAPS_ENTRY_FREE (pTmpCapsInfo) != CAPS_MEM_SUCCESS)
                {
                    return;
                }
                pTmpCapsInfo = NULL;
                pDupPeer = Bgp4SnmphGetDuplicatePeerEntry (pPeer);
                if (pDupPeer != NULL)
                {
                    GetFindSpkrSupCap (pDupPeer,
                                       CAP_CODE_MP_EXTN,
                                       CAP_MP_CAP_LENGTH,
                                       &CapValue, CAPS_SUP, &pTmpCapsInfo);
                    if (pTmpCapsInfo == NULL)
                    {
                        return;
                    }
                    TMO_SLL_Delete (BGP4_PEER_SUP_CAPS_LIST (pDupPeer),
                                    &pTmpCapsInfo->NextCapability);
                    if (SPKR_SUP_CAPS_ENTRY_FREE (pTmpCapsInfo) !=
                        CAPS_MEM_SUCCESS)
                    {
                        return;
                    }
                }
            }

            if (SPKR_SUP_CAPS_ENTRY_FREE (pCapsInfo) != CAPS_MEM_SUCCESS)
            {
                return;
            }

            MEMSET (&RemoteAddrInfo, 0, sizeof (tAddrPrefix));
            u4PeerCount--;
        }
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedProcessBgpPeerMPCapV4SyncMsg\r\n");
    return;
}

/*****************************************************************************/
/*    Function Name       : Bgp4RedProcessBgpPeerMPCapV6SyncMsg                */
/*    Description         : This function will process the Bgp Peer MP Cap   */
/*                          Sync message received from Active instance       */
/*                          and update the information                       */
/*    Input(s)            : pMsg                                             */
/*                          pu4OffSet                                        */
/*    Output(s)           : None                                             */
/*    Returns             : None.                                            */
/*****************************************************************************/
PUBLIC VOID
Bgp4RedProcessBgpPeerMPCapV6SyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                     UINT2 u2LenOffSet)
{
    UINT4               u4Context = 0;
    UINT4               u4PeerCount = 0;
    tAddrPrefix         RemoteAddrInfo;
    tBgp4PeerEntry     *pPeer = NULL;
    tBgp4PeerEntry     *pDupPeer = NULL;
    tSupCapsInfo       *pCapsInfo = NULL;
    tSupCapsInfo       *pTmpCapsInfo = NULL;
    UINT1               u1RemMPCapFlag = BGP4_FALSE;
    tSNMP_OCTET_STRING_TYPE CapValue;
    UINT1               u1ResField = 0;
    UINT1               au1MpcapOctetList[BGP4_MPE_MP_CAP_LEN];

    MEMSET (&RemoteAddrInfo, 0, sizeof (tAddrPrefix));

    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedProcessBgpPeerMPCapV6SyncMsg\r\n");
    while (*pu4OffSet < (UINT4) u2LenOffSet)
    {
        BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4Context);
        BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4PeerCount);
        while (u4PeerCount > 0)
        {
            Bgp4RedReadAddrPrefix (pMsg, pu4OffSet, &RemoteAddrInfo);
            pPeer = Bgp4SnmphGetPeerEntry (u4Context, RemoteAddrInfo);
            if (pPeer == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                          "\tERROR - No matching Peer exist.\n");
                return;
            }
            SPKR_SUP_CAPS_ENTRY_CREATE (pCapsInfo);
            if (pCapsInfo == NULL)
            {
                return;
            }
            MEMSET (au1MpcapOctetList, 0, BGP4_MPE_MP_CAP_LEN);
            CapValue.pu1_OctetList = au1MpcapOctetList;
            PTR_ASSIGN2 (CapValue.pu1_OctetList, BGP4_INET_AFI_IPV6);
            *((CapValue.pu1_OctetList) + BGP4_MPE_ADDRESS_FAMILY_LEN)
                = u1ResField;
            *((CapValue.pu1_OctetList) + BGP4_MPE_ADDRESS_FAMILY_LEN
              + BGP4_MPE_MP_CAP_RESERVED_FIELD_LEN) = BGP4_INET_SAFI_UNICAST;

            CapValue.i4_Length = BGP4_MPE_MP_CAP_LEN;

            GetFindSpkrSupCap (pPeer,
                               CAP_CODE_MP_EXTN,
                               CAP_MP_CAP_LENGTH,
                               &CapValue, CAPS_SUP, &pTmpCapsInfo);

            BGP4_RM_GET_1_BYTE (pMsg, pu4OffSet, u1RemMPCapFlag);
            if (u1RemMPCapFlag == BGP4_TRUE)
            {
                BGP4_RM_GET_N_BYTE (pMsg, pu4OffSet, pCapsInfo,
                                    sizeof (tSupCapsInfo));
                if (pTmpCapsInfo == NULL)
                {
                    if ((CapsSpkrSupCapCreate (pPeer,
                                               CAP_CODE_MP_EXTN,
                                               CAP_MP_CAP_LENGTH,
                                               &CapValue,
                                               &pTmpCapsInfo)) == CAPS_FAILURE)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC,
                                  BGP4_MOD_NAME,
                                  "\tERROR - No matching Peer exist.\n");

                        return;
                    }
                    pTmpCapsInfo->u1RowStatus = pCapsInfo->u1RowStatus;
                    pTmpCapsInfo->u1CapsFlag = pCapsInfo->u1CapsFlag;
                    pTmpCapsInfo->u1ConfiguredFlag =
                        pCapsInfo->u1ConfiguredFlag;
                }
                else
                {
                    pTmpCapsInfo->SupCapability.u1CapCode =
                        pCapsInfo->SupCapability.u1CapCode;
                    pTmpCapsInfo->SupCapability.u1CapLength =
                        pCapsInfo->SupCapability.u1CapLength;
                    if ((pCapsInfo->SupCapability.u1CapLength > 0) &&
                        (pCapsInfo->SupCapability.u1CapLength <=
                         MAX_CAP_LENGTH))
                    {
                        MEMCPY (pTmpCapsInfo->SupCapability.au1CapValue,
                                pCapsInfo->SupCapability.au1CapValue,
                                pCapsInfo->SupCapability.u1CapLength);
                    }
                    pTmpCapsInfo->u1RowStatus = pCapsInfo->u1RowStatus;
                    pTmpCapsInfo->u1CapsFlag = pCapsInfo->u1CapsFlag;
                    pTmpCapsInfo->u1ConfiguredFlag =
                        pCapsInfo->u1ConfiguredFlag;
                }
            }
            if (u1RemMPCapFlag == BGP4_FALSE && pTmpCapsInfo != NULL)
            {
                TMO_SLL_Delete (BGP4_PEER_SUP_CAPS_LIST (pPeer),
                                &pTmpCapsInfo->NextCapability);
                if (SPKR_SUP_CAPS_ENTRY_FREE (pTmpCapsInfo) != CAPS_MEM_SUCCESS)
                {
                    return;
                }
                pTmpCapsInfo = NULL;
                pDupPeer = Bgp4SnmphGetDuplicatePeerEntry (pPeer);
                if (pDupPeer != NULL)
                {
                    GetFindSpkrSupCap (pDupPeer,
                                       CAP_CODE_MP_EXTN,
                                       CAP_MP_CAP_LENGTH,
                                       &CapValue, CAPS_SUP, &pTmpCapsInfo);
                    if (pTmpCapsInfo == NULL)
                    {
                        return;
                    }
                    TMO_SLL_Delete (BGP4_PEER_SUP_CAPS_LIST (pDupPeer),
                                    &pTmpCapsInfo->NextCapability);
                    if (SPKR_SUP_CAPS_ENTRY_FREE (pTmpCapsInfo) !=
                        CAPS_MEM_SUCCESS)
                    {
                        return;
                    }
                }
            }

            if (SPKR_SUP_CAPS_ENTRY_FREE (pCapsInfo) != CAPS_MEM_SUCCESS)
            {
                return;
            }

            MEMSET (&RemoteAddrInfo, 0, sizeof (tAddrPrefix));
            u4PeerCount--;
        }
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedProcessBgpPeerMPCapV6SyncMsg\r\n");
    return;
}

/*****************************************************************************/
/*    Function Name       : Bgp4RedUpdPeerFOM                                */
/*    Description         : This function updates the given FOM to the given */
/*                          peer. and send the Dampened information in the   */
/*                          bulk message                                     */
/*    Input(s)            : u4Context - Context in which peer is present     */
/*                          pPeerDampHist - Peer damp Entry                  */
/*                          u4FOM - The fom value to be updated to peer      */
/*    Output(s)           : None                                             */
/*    Returns             : None.                                            */
/*****************************************************************************/
VOID
Bgp4RedUpdPeerFOM (UINT4 u4Context, tPeerDampHist * pPeerDampHist, UINT4 u4Fom)
{
    UINT4               u4CurrentTime;
    UINT2               u2TimeDiff;
    UINT2               u2DecayIndx;

    RFD_GET_SYS_TIME (&u4CurrentTime);
    u2TimeDiff =
        (UINT2) (RFD_GET_TIME_DIFF_SEC
                 (u4CurrentTime, pPeerDampHist->u4PeerLastTime));
    u2DecayIndx =
        (UINT2) (u2TimeDiff / RFD_DECAY_TIMER_GRANULARITY (u4Context));

    if (u2DecayIndx > RFD_DECAY_ARRAY_SIZE (u4Context))
    {
        pPeerDampHist->PeerFom = RFD_DEF_FOM;
        pPeerDampHist->u4PeerLastTime = u4CurrentTime;
    }
    else
    {
        pPeerDampHist->PeerFom = u4Fom;
        pPeerDampHist->u4PeerLastTime = u4CurrentTime;
    }
    RfdPDHPeerAdjustCeiling (u4Context, pPeerDampHist);
    if (pPeerDampHist->PeerFom > RFD_CUTOFF_THRESHOLD (u4Context))
    {
        pPeerDampHist->u1PeerStatus = RFD_SUPPRESSED_STATE;
    }
    return;
}

/*****************************************************************************/
/*    Function Name       : Bgp4RedSyncPeerDampMsg                           */
/*    Description         : This function will send the peer damp info       */
/*                          in a dynamic bulk update message                 */
/*    Input(s)            : u4Context - Context in which peer is present     */
/*                          pPeerDampHist - Peer damp Entry                  */
/*    Output(s)           : None                                             */
/*    Returns             : BGP4_SUCCESS/BGP4_FAILURE                        */
/*****************************************************************************/
PUBLIC INT4
Bgp4RedSyncPeerDampMsg (UINT4 u4Context, tPeerDampHist * pPeerHist)
{
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT4               u4LenOffSet = 0;

    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedSyncPeerDampMsg\r\n");
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_BULK_UPD_PEER_MESSAGE, &u4OffSet)
        == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }

    /* Save the offset for writing the length of the bulk update message
     * and increment the offset to start writing the peer damp status */
    u4LenOffSet = u4OffSet;
    u4OffSet += 2;

    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, u4Context);
    Bgp4RedFillBufAddr (RFD_PEER_INFO_IN_PEER_DAMPHIST_STRUCT
                        (pPeerHist), pMsg->pBuf, &u4OffSet);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, pPeerHist->PeerFom);
    BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
    DbUtilAddTblNode (&(gBgpNode.DynInfoList), &pMsg->DbNode);
    pMsg->u4DataLen = u4OffSet;
    Bgp4RedSyncDynInfo ();
    KW_FALSEPOSITIVE_FIX1 (pMsg);
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedSyncPeerDampMsg\r\n");
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : Bgp4RedFillBufRouteInfo                          */
/*    Description         : This function fills the given route information  */
/*                          in the given buffer                              */
/*    Input(s)            : pRtProfile - Route information                   */
/*                          pBuf - RM buffer to which info is to be written  */
/*                          pu4OffSet - offset of the buffer                 */
/*    Output(s)           : pu4OffSet - updated offset value                 */
/*    Returns             : BGP4_SUCCESS/BGP4_FAILURE                        */
/*****************************************************************************/
INT4
Bgp4RedFillBufRouteInfo (tRouteProfile * pRtProfile, tRmMsg * pBuf,
                         UINT4 *pu4OffSet)
{
    tAddrPrefix         InvAddr;
    UINT4               u4Flag;
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
    UINT4               u4AsafiMask;
#endif
#endif
#ifdef L3VPN
    tRtInstallVrfInfo  *pRtVrfInfo = NULL;
    UINT4               u4VrfId = 0;
#endif
    /*
     *    Bgp Route Information
     *    <-4 bytes->|<- 19 bytes->|<- 19 bytes->|<-2 bytes->|<-2 bytes->|
     *    ----------------------------------------------------------------
     *    | Context  | Peer Address| Net Address |Prefix Len | Rt. SAFI  |
     *    |---------------------------------------------------------------
     *        <-19 bytes->|<-4 bytes->|<-4 bytes->|<-4 bytes->|<-4 bytes ->|
     *        --------------------------------------------------------------
     *        | Next hop  | Flags     |Ext Flags  |  MED      | Local Pref |
     *        --------------------------------------------------------------
     *        <-4 bytes->|<-4 bytes->|<-1 byte->|<-4 bytes ->|<-1 byte ->|
     *        -----------------------------------------------------------
     *        | If Index | NH Metric | Protocol |Label       |Label count|
     *        -----------------------------------------------------------
     *        <-8 bytes ->|<------4Bytes---->|<-----20 bytes------>|
     *        ------------------------------------------------------
     *        |    RD     |    VplsIndex     |       VplsNLRI      |
     *        ------------------------------------------------------
     */
    /* Context */
    BGP4_RM_PUT_4_BYTE (pBuf, pu4OffSet, BGP4_RT_CXT_ID (pRtProfile));
    /* Peer Address - If peer is null Fill 0 as the peer address */
    if (BGP4_RT_PEER_ENTRY (pRtProfile) == NULL)
    {
        MEMSET (&InvAddr, 0, sizeof (tAddrPrefix));
        Bgp4RedFillBufAddr (InvAddr, pBuf, pu4OffSet);
    }
    else
    {
        Bgp4RedFillBufAddr (BGP4_PEER_REMOTE_ADDR_INFO
                            (BGP4_RT_PEER_ENTRY (pRtProfile)), pBuf, pu4OffSet);
    }
    /* Net Addr */
    Bgp4RedFillBufAddr (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                        (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
                        pBuf, pu4OffSet);
    BGP4_RM_PUT_2_BYTE (pBuf, pu4OffSet, BGP4_RT_PREFIXLEN (pRtProfile));
    BGP4_RM_PUT_2_BYTE (pBuf, pu4OffSet, BGP4_RT_SAFI_INFO (pRtProfile));
    /* Imm nexthop */
    Bgp4RedFillBufAddr (BGP4_RT_IMMEDIATE_NEXTHOP_INFO (pRtProfile),
                        pBuf, pu4OffSet);
    /* route properties */
    u4Flag = (BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_SYNC_FLAG);
    BGP4_RM_PUT_4_BYTE (pBuf, pu4OffSet, u4Flag);
    BGP4_RM_PUT_4_BYTE (pBuf, pu4OffSet, BGP4_RT_GET_EXT_FLAGS (pRtProfile));
    BGP4_RM_PUT_4_BYTE (pBuf, pu4OffSet, BGP4_RT_MED (pRtProfile));
    BGP4_RM_PUT_4_BYTE (pBuf, pu4OffSet, BGP4_RT_LOCAL_PREF (pRtProfile));
    BGP4_RM_PUT_4_BYTE (pBuf, pu4OffSet, BGP4_RT_GET_RT_IF_INDEX (pRtProfile));
    BGP4_RM_PUT_4_BYTE (pBuf, pu4OffSet, BGP4_RT_NH_METRIC (pRtProfile));
    BGP4_RM_PUT_1_BYTE (pBuf, pu4OffSet, BGP4_RT_PROTOCOL (pRtProfile));
    BGP4_RM_PUT_4_BYTE (pBuf, pu4OffSet, BGP4_RT_BGP_MED_CHANGE (pRtProfile));
    BGP4_RM_PUT_1_BYTE (pBuf, pu4OffSet, BGP4_RT_GET_ADV_FLAG (pRtProfile));
#ifdef L3VPN
    BGP4_RM_PUT_4_BYTE (pBuf, pu4OffSet, BGP4_RT_LABEL_INFO (pRtProfile));
    BGP4_RM_PUT_1_BYTE (pBuf, pu4OffSet, BGP4_RT_LABEL_CNT (pRtProfile));
    BGP4_RM_PUT_N_BYTE (pBuf, pu4OffSet, BGP4_RT_ROUTE_DISTING (pRtProfile),
                        BGP4_VPN4_ROUTE_DISTING_SIZE);
    TMO_SLL_Scan (BGP4_VPN4_RT_INSTALL_VRF_LIST (pRtProfile), pRtVrfInfo,
                  tRtInstallVrfInfo *)
    {
        u4VrfId = BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrfInfo);
        break;
    }
    BGP4_RM_PUT_4_BYTE (pBuf, pu4OffSet, u4VrfId);
#endif

#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
    if (CAP_MP_L2VPN_VPLS == u4AsafiMask)
    {
        if (pRtProfile->pVplsSpecInfo != NULL)
        {
#ifdef MPLS_WANTED
            BGP4_RM_PUT_4_BYTE (pBuf, pu4OffSet,
                                BGP4_VPLS_SPEC_VPLS_INDX (pRtProfile->
                                                          pVplsSpecInfo));
#endif
            BGP4_RM_PUT_N_BYTE (pBuf, pu4OffSet, &(pRtProfile->VplsNlriInfo),
                                sizeof (tVplsNlriInfo));
        }
    }
#endif
#endif
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : Bgp4RedFillBufAddr                               */
/*    Description         : This function writes the given peer address to   */
/*                          the given buffer in the mentioned offset.        */
/*    Input(s)            : Address - Peer address inforamtion               */
/*                          pBuf - RM buffer to which info is to be written  */
/*                          pu4OffSet - offset of the buffer                 */
/*    Output(s)           : pu4OffSet - New updated offset.                  */
/*    Returns             : None.                                            */
/*****************************************************************************/
VOID
Bgp4RedFillBufAddr (tAddrPrefix Address, tRmMsg * pBuf, UINT4 *pu4OffSet)
{
    /*
     *    Bgp Address Prefix
     *    <-1 byte ->|<-2 bytes->|<- 16 bytes ->|
     *    ---------------------------------------
     *    | Addr Afi | Addr Len  | Address      |
     *    |--------------------------------------
     */
    BGP4_RM_PUT_2_BYTE (pBuf, pu4OffSet,
                        BGP4_AFI_IN_ADDR_PREFIX_INFO (Address));
    BGP4_RM_PUT_2_BYTE (pBuf, pu4OffSet,
                        BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO (Address));
    BGP4_RM_PUT_N_BYTE (pBuf, pu4OffSet,
                        BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Address),
                        BGP4_MAX_INET_ADDRESS_LEN);
    return;
}

/*****************************************************************************/
/*    Function Name       : Bgp4RedReadAddrPrefix                            */
/*    Description         : This function reads the address prefix info from */
/*                          the given buffer in the mentioned offset.        */
/*    Input(s)            : pBuf - RM buffer                                 */
/*                          pu4OffSet - offset of the buffer                 */
/*                          PeerAddr - Peer address inforamtion              */
/*    Output(s)           : pu4OffSet - New updated offset.                  */
/*    Returns             : None.                                            */
/*****************************************************************************/
VOID
Bgp4RedReadAddrPrefix (tRmMsg * pBuf, UINT4 *pu4OffSet, tAddrPrefix * pAddr)
{
    tAddrPrefix         Address;
    MEMSET (&Address, 0, sizeof (tAddrPrefix));
    /*
     *    Bgp Address Prefix
     *    <-1 byte ->|<-2 bytes->|<- 16 bytes ->|
     *    ---------------------------------------
     *    | Addr Afi | Addr Len  | Address      |
     *    |--------------------------------------
     */
    BGP4_RM_GET_2_BYTE (pBuf, pu4OffSet,
                        BGP4_AFI_IN_ADDR_PREFIX_INFO (Address));
    BGP4_RM_GET_2_BYTE (pBuf, pu4OffSet,
                        BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO (Address));
    BGP4_RM_GET_N_BYTE (pBuf, pu4OffSet,
                        BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (Address),
                        BGP4_MAX_INET_ADDRESS_LEN);
    MEMCPY (pAddr, &Address, sizeof (tAddrPrefix));
    return;
}

/*****************************************************************************/
/*    Function Name       : Bgp4RedReadRouteInfo                             */
/*    Description         : This function reads the route information from   */
/*                          the given buffer                                 */
/*    Input(s)            : pBuf - RM buffer                                 */
/*                          pu4OffSet - offset of the buffer                 */
/*    Output(s)           : pu4OffSet - updated offset value                 */
/*                          pRtProfile - Route information                   */
/*    Returns             : Pointer to tRouteProfile                         */
/*****************************************************************************/
tRouteProfile      *
Bgp4RedReadRouteInfo (tRmMsg * pBuf, UINT4 *pu4OffSet)
{
    tRouteProfile      *pRtProfile = NULL;
    tBgpCxtNode        *pBgpCxtNode = NULL;
    tBgp4PeerEntry     *pPeer = NULL;
    tAddrPrefix         PeerAddr;
    tAddrPrefix         InvAddr;
    UINT4               u4Context = 0;
    UINT4               u4Metric = 0;
#ifdef L3VPN
    UINT4               u4VrfId = 0;
    INT4                i4RetVal = 0;
#endif

#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
    UINT4               u4VplsIndex = 0;
    UINT4               u4AsafiMask;
#endif
#endif
    UINT1               u1AdvFlag = 0;
    if (*pu4OffSet >= BGP4_RED_ROUTE_MSG_MAX_LEN)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC | BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME,
                  "\tRead offset exceeded the max route msg len\n");
        return NULL;
    }
    pRtProfile = Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
    if (pRtProfile == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "Allocate Route Profile for syncing route information"
                  " FAILS!!!\n");
        gu4BgpDebugCnt[MAX_BGP_ROUTES_SIZING_ID]++;
        return NULL;
    }
    MEMSET (&PeerAddr, 0, sizeof (tAddrPrefix));
    MEMSET (&InvAddr, 0, sizeof (tAddrPrefix));
    /*
     *    Bgp Route Information
     *    <-4 bytes->|<- 19 bytes->|<- 19 bytes->|<-2 bytes->|<-2 bytes->|
     *    ----------------------------------------------------------------
     *    | Context  | Peer Address| Net Address |Prefix Len | Rt. SAFI  |
     *    |---------------------------------------------------------------
     *        <-19 bytes->|<-4 bytes->|<-4 bytes->|<-4 bytes->|<-4 bytes ->|
     *        --------------------------------------------------------------
     *        | Next hop  | Flags     |Ext Flags  |  MED      | Local Pref |
     *        --------------------------------------------------------------
     *        <-4 bytes->|<-4 bytes->|<-1 byte->|
     *        -----------------------------------
     *        | If Index | NH Metric | Protocol |
     *        -----------------------------------
     */
    /* Context */
    BGP4_RM_GET_4_BYTE (pBuf, pu4OffSet, u4Context);
    /* Peer Address */
    Bgp4RedReadAddrPrefix (pBuf, pu4OffSet, &PeerAddr);
    /* Net Addr */
    Bgp4RedReadAddrPrefix (pBuf, pu4OffSet,
                           &(BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                             (BGP4_RT_NET_ADDRESS_INFO (pRtProfile))));
    BGP4_RM_GET_2_BYTE (pBuf, pu4OffSet, BGP4_RT_PREFIXLEN (pRtProfile));
    BGP4_RM_GET_2_BYTE (pBuf, pu4OffSet, BGP4_RT_SAFI_INFO (pRtProfile));

    if (NULL != Bgp4GetContextEntry (u4Context))
    {
        pBgpCxtNode = Bgp4GetContextEntry (u4Context);
    }
    else
    {
        BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG,
                       BGP4_ALL_FAILURE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                       "Unable to obtain the context entry for "
                       "context %d for syncing route %s\n", u4Context,
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        Bgp4DshReleaseRtInfo (pRtProfile);
        return NULL;
    }
    if (MEMCMP (&PeerAddr, &InvAddr, sizeof (tAddrPrefix)) == 0)
    {
        /* The route is a non-bgp route. Assign the back pointer
         * to the context structure */
        pRtProfile->pBgpCxtNode = pBgpCxtNode;
    }
    else
    {
        /* Route is bgp route. Identify the peer entry and assign
         * the back pointer of the route to it */
        pPeer = Bgp4SnmphGetPeerEntry (u4Context, PeerAddr);
        if (pPeer == NULL)
        {
            BGP4_TRC_ARG3 (NULL, BGP4_TRC_FLAG,
                           BGP4_ALL_FAILURE_TRC | BGP4_EVENTS_TRC,
                           BGP4_MOD_NAME,
                           "Unable to obtain the peer entry %s"
                           " in context %d for syncing route %s\n",
                           Bgp4PrintIpAddr (PeerAddr.au1Address,
                                            PeerAddr.u2Afi), u4Context,
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            Bgp4DshReleaseRtInfo (pRtProfile);
            return NULL;
        }
        BGP4_RT_PEER_ENTRY (pRtProfile) = pPeer;
    }

    BGP4_RT_REF_COUNT (pRtProfile)++;
    /* Imm nexthop */
    Bgp4RedReadAddrPrefix (pBuf, pu4OffSet,
                           &(BGP4_RT_IMMEDIATE_NEXTHOP_INFO (pRtProfile)));
    /* route properties */
    BGP4_RM_GET_4_BYTE (pBuf, pu4OffSet, BGP4_RT_GET_FLAGS (pRtProfile));
    BGP4_RM_GET_4_BYTE (pBuf, pu4OffSet, BGP4_RT_GET_EXT_FLAGS (pRtProfile));
    BGP4_RM_GET_4_BYTE (pBuf, pu4OffSet, BGP4_RT_MED (pRtProfile));
    BGP4_RM_GET_4_BYTE (pBuf, pu4OffSet, BGP4_RT_LOCAL_PREF (pRtProfile));
    BGP4_RM_GET_4_BYTE (pBuf, pu4OffSet, BGP4_RT_GET_RT_IF_INDEX (pRtProfile));
    BGP4_RM_GET_4_BYTE (pBuf, pu4OffSet, u4Metric);
    BGP4_RT_NH_METRIC (pRtProfile) = (INT4) u4Metric;
    BGP4_RM_GET_1_BYTE (pBuf, pu4OffSet, BGP4_RT_PROTOCOL (pRtProfile));
    BGP4_RM_GET_4_BYTE (pBuf, pu4OffSet, BGP4_RT_BGP_MED_CHANGE (pRtProfile));
    BGP4_RM_GET_1_BYTE (pBuf, pu4OffSet, u1AdvFlag);
    BGP4_RT_SET_ADV_FLAG (pRtProfile, u1AdvFlag);
#ifdef L3VPN
    BGP4_RM_GET_4_BYTE (pBuf, pu4OffSet, BGP4_RT_LABEL_INFO (pRtProfile));
    BGP4_RM_GET_1_BYTE (pBuf, pu4OffSet, BGP4_RT_LABEL_CNT (pRtProfile));
    BGP4_RM_GET_N_BYTE (pBuf, pu4OffSet, BGP4_RT_ROUTE_DISTING (pRtProfile),
                        BGP4_VPN4_ROUTE_DISTING_SIZE);
    BGP4_RM_GET_4_BYTE (pBuf, pu4OffSet, u4VrfId);
    if (BGP4_RT_LABEL_CNT (pRtProfile) != 0)
    {
        i4RetVal = Bgp4Vpn4ConvertRtToVpn4Route (pRtProfile, u4VrfId);
    }
    UNUSED_PARAM (i4RetVal);
#endif
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pRtProfile),
                           BGP4_RT_SAFI_INFO (pRtProfile), u4AsafiMask);
    if (CAP_MP_L2VPN_VPLS == u4AsafiMask)
    {

#ifdef MPLS_WANTED
        BGP4_RM_GET_4_BYTE (pBuf, pu4OffSet, u4VplsIndex);
        Bgp4LinkVplsSpecToRouteInStandby (pRtProfile, u4VplsIndex, u4Context);
        if (pRtProfile->pVplsSpecInfo == NULL)
        {
            /* VPLS specfic info is not present for this vpls index */
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                           BGP4_MOD_NAME, "VplsSpec in RouteProfile  "
                           "NULL\n");
            Bgp4DshReleaseRtInfo (pRtProfile);
            return NULL;
        }
#endif
        BGP4_RM_GET_N_BYTE (pBuf, pu4OffSet, &(pRtProfile->VplsNlriInfo),
                            sizeof (tVplsNlriInfo));
    }
#endif
#endif
    return pRtProfile;
}

/*****************************************************************************/
/*    Function Name       : Bgp4RedReadBgp4Info                              */
/*    Description         : This function reads the route bgp information    */
/*                          the given buffer                                 */
/*    Input(s)            : pBuf - RM buffer                                 */
/*                          pu4OffSet - offset of the buffer                 */
/*                          pRtProfile - Update route info with bgp info     */
/*    Output(s)           : pu4OffSet - updated offset value                 */
/*    Returns             : BGP4_SUCCESS/BGP4_FAILURE                        */
/*****************************************************************************/
tBgp4Info          *
Bgp4RedReadBgp4Info (tRmMsg * pBuf, UINT4 *pu4Offset)
{
    tBgp4Info          *pBgp4Info = NULL;
    tAsPath            *pAsPath = NULL;
    tCommunity         *pComm = NULL;
    tExtCommunity      *pEcomm;
    tClusterList       *pClusList = NULL;
    UINT1              *pu1ClusterList = NULL;
    UINT1              *pu1CommStr = NULL;
    UINT1              *pu1EcommStr = NULL;
    UINT1              *pu1UnknownAttr = NULL;
    UINT2               u2AsCount = 0;
    UINT2               u2Counter = 0;

    if (*pu4Offset >= BGP4_RED_ROUTE_MSG_MAX_LEN)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC | BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME,
                  "\tBuffer not sufficient for route information\n");
        return NULL;
    }

    pBgp4Info = Bgp4MemAllocateBgp4info (sizeof (tBgp4Info));
    if (pBgp4Info == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tAllocate Bgp4Info for syncing route information"
                  " FAILS!!!\n");
        gu4BgpDebugCnt[MAX_BGP_ROUTE_INFO_ENTRIES_SIZING_ID]++;
        return NULL;
    }
    /*
     *    Bgp Route Bgp4 Information
     *    <-4 bytes->|<- N bytes->|<- 7 bytes->|<-N bytes->|<-12 bytes->|
     *    ----------------------------------------------------------------
     *    | Attr Flag|   AS Path  | Aggregator |Community  | Ext Comm  |
     *    |---------------------------------------------------------------
     *      <-N bytes->|<-19 bytes->|<-1 bytes->|<-19 bytes->|<-N bytes ->|
     *      --------------------------------------------------------------
     *      | Cluster  | Next hop   | LL flag   |  LL Addr   | Unkn Attr  |
     *      --------------------------------------------------------------
     *      <-4 bytes->|<-4 bytes->|<-4 bytes->|<-1 byte->|<-2 bytes->|<-1byte->
     *      --------------------------------------------------------------------
     *      | Org Id   | Rcv MED   | Rcv LP    | Origin   |  Weight   | SAFI   |
     *      --------------------------------------------------------------------
     */

    BGP4_RM_GET_4_BYTE (pBuf, pu4Offset, BGP4_INFO_ATTR_FLAG (pBgp4Info));

    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_PATH_MASK)
        == BGP4_ATTR_PATH_MASK)
    {
        /*        <-2 bytes ->|<-  1 byte  ->|<- 1 byte  ->|<-N bytes->|...
         *        ---------------------------------------------------------
         *        |AS Path Cnt| AS Path type | AS Path Len |  AS Path  |...
         *        ---------------------------------------------------------
         */
        BGP4_RM_GET_2_BYTE (pBuf, pu4Offset, u2AsCount);
        for (u2Counter = 0; u2Counter < u2AsCount; u2Counter++)
        {
            pAsPath = Bgp4MemGetASNode (sizeof (tAsPath));
            if (pAsPath == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                          "\tMemory Allocation for AsPath while syncing route "
                          "information FAILED\n");
                Bgp4DshReleaseBgpInfo (pBgp4Info);
                gu4BgpDebugCnt[MAX_BGP_AS_PATHS_SIZING_ID]++;
                return NULL;
            }
            TMO_SLL_Add (BGP4_INFO_ASPATH (pBgp4Info), &pAsPath->sllNode);

            BGP4_RM_GET_1_BYTE (pBuf, pu4Offset, BGP4_ASPATH_TYPE (pAsPath));
            BGP4_RM_GET_1_BYTE (pBuf, pu4Offset, BGP4_ASPATH_LEN (pAsPath));
            BGP4_RM_GET_N_BYTE (pBuf, pu4Offset, BGP4_ASPATH_NOS (pAsPath),
                                (UINT4) (BGP4_AS4_LENGTH *
                                         BGP4_ASPATH_LEN (pAsPath)));
        }
    }

    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_AGGREGATOR_MASK)
        == BGP4_ATTR_AGGREGATOR_MASK)
    {
        /*        <-4 bytes->|<-2 bytes->|<-1 byte ->|
         *        ------------------------------------
         *        | Aggr IP  | Aggr ASN  | Aggr Flag |
         *        ------------------------------------
         */
        AGGR_NODE_CREATE (BGP4_INFO_AGGREGATOR (pBgp4Info));
        if (BGP4_INFO_AGGREGATOR (pBgp4Info) == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for Aggregator Attribute while syncing "
                      "route information FAILED\n");
            Bgp4DshReleaseBgpInfo (pBgp4Info);
            gu4BgpDebugCnt[MAX_BGP_AGGR_NODE_SIZING_ID]++;
            return NULL;
        }
        BGP4_RM_GET_4_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_AGGREGATOR_NODE (pBgp4Info));
        BGP4_RM_GET_4_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_AGGREGATOR_AS (pBgp4Info));
        BGP4_RM_GET_1_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_AGGREGATOR_FLAG (pBgp4Info));
    }

    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_COMM_MASK)
        == BGP4_ATTR_COMM_MASK)
    {
        /*        <-2 bytes ->|<- N bytes ->|<- 1 byte->|
         *        ---------------------------------------
         *        | Comm Cnt  | Comm Value  | Comm Flag |
         *        ---------------------------------------
         */
        COMMUNITY_NODE_CREATE (pComm);
        if (pComm == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for Community Attribute while syncing "
                      "route information FAILED\n");
            Bgp4DshReleaseBgpInfo (pBgp4Info);
            gu4BgpDebugCnt[MAX_BGP_COMMUNITY_NODE_SIZING_ID]++;
            return NULL;
        }
        ATTRIBUTE_NODE_CREATE (pu1CommStr);
        if (pu1CommStr == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for Community Attribute string while syncing "
                      "route information FAILED\n");
            COMMUNITY_NODE_FREE (pComm);
            Bgp4DshReleaseBgpInfo (pBgp4Info);
            gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
            return NULL;
        }

        BGP4_INFO_COMM_ATTR (pBgp4Info) = pComm;
        BGP4_INFO_COMM_ATTR_VAL (pBgp4Info) = pu1CommStr;
        BGP4_RM_GET_2_BYTE (pBuf, pu4Offset, BGP4_INFO_COMM_COUNT (pBgp4Info));
        BGP4_RM_GET_N_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_COMM_ATTR_VAL (pBgp4Info),
                            (UINT4) ((BGP4_INFO_COMM_COUNT (pBgp4Info)) *
                                     COMM_VALUE_LEN));
        BGP4_RM_GET_1_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_COMM_ATTR_FLAG (pBgp4Info));
    }

    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_ECOMM_MASK)
        == BGP4_ATTR_ECOMM_MASK)
    {
        /*        <-2 bytes ->|<- 1 byte ->|<-   1 byte   ->|<-4 bytes->|<-2 bytes->|<-2 bytes->
         *        -----------------------------------------------------------------------------
         *        | ECom Cnt  | ECom flag  | Ecomm cost flg | Ecom cost | Ecom POI  | ECom comm|
         *        -----------------------------------------------------------------------------
         */
        EXT_COMMUNITY_NODE_CREATE (pEcomm);
        if (pEcomm == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for Ext Community Attribute while syncing "
                      "route information FAILED\n");
            Bgp4DshReleaseBgpInfo (pBgp4Info);
            gu4BgpDebugCnt[MAX_BGP_EXT_COMM_NODE_SIZING_ID]++;
            return NULL;
        }
        BGP4_INFO_ECOMM_ATTR (pBgp4Info) = pEcomm;
        BGP4_RM_GET_2_BYTE (pBuf, pu4Offset, BGP4_INFO_ECOMM_COUNT (pBgp4Info));
        BGP4_RM_GET_1_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_ECOMM_ATTR_FLAG (pBgp4Info));
        BGP4_RM_GET_1_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_ECOMM_ATTR_COST_FLAG (pBgp4Info));
        if (BGP4_INFO_ECOMM_ATTR_COST_FLAG (pBgp4Info) == BGP4_TRUE)
        {
            BGP4_RM_GET_4_BYTE (pBuf, pu4Offset,
                                BGP4_INFO_ECOMM_ATTR_COST_VAL (pBgp4Info));
            BGP4_RM_GET_2_BYTE (pBuf, pu4Offset,
                                BGP4_INFO_ECOMM_ATTR_COST_POI (pBgp4Info));
            BGP4_RM_GET_2_BYTE (pBuf, pu4Offset,
                                BGP4_INFO_ECOMM_ATTR_COST_COMM (pBgp4Info));
        }
        else
        {
            ATTRIBUTE_NODE_CREATE (pu1EcommStr);
            if (pu1EcommStr == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                          "\tMemory Allocation for Ext Community Attribute while syncing "
                          "route information FAILED\n");
                Bgp4DshReleaseBgpInfo (pBgp4Info);
                gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
                return NULL;
            }
            BGP4_INFO_ECOMM_ATTR_VAL (pBgp4Info) = pu1EcommStr;
            BGP4_RM_GET_N_BYTE (pBuf, pu4Offset,
                                BGP4_INFO_ECOMM_ATTR_VAL (pBgp4Info),
                                (UINT4) ((BGP4_INFO_ECOMM_COUNT (pBgp4Info)) *
                                         EXT_COMM_VALUE_LEN));
        }
    }

    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_CLUS_LIST_MASK)
        == BGP4_ATTR_CLUS_LIST_MASK)
    {
        /*        <-2 bytes ->|<- N bytes ->|<- 1 byte->|
         *        ---------------------------------------
         *        | Clus Cnt  | Clus Value  | Clus Flag |
         *        ---------------------------------------
         */
        CLUSTER_LIST_CREATE (pClusList);
        if (pClusList == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for Cluster list Attribute while syncing "
                      "route information FAILED\n");
            Bgp4DshReleaseBgpInfo (pBgp4Info);
            gu4BgpDebugCnt[MAX_BGP_CLUSTER_LIST_SIZING_ID]++;
            return NULL;
        }
        ATTRIBUTE_NODE_CREATE (pu1ClusterList);
        if (pu1ClusterList == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for Cluster list Attribute while syncing "
                      "route information FAILED\n");
            CLUSTER_LIST_FREE (pClusList);
            Bgp4DshReleaseBgpInfo (pBgp4Info);
            gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
            return NULL;
        }
        BGP4_INFO_CLUS_LIST_ATTR (pBgp4Info) = pClusList;
        BGP4_INFO_CLUS_LIST_ATTR_VAL (pBgp4Info) = pu1ClusterList;
        BGP4_RM_GET_2_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_CLUS_LIST_COUNT (pBgp4Info));
        BGP4_RM_GET_N_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_CLUS_LIST_ATTR_VAL (pBgp4Info),
                            (UINT4) ((BGP4_INFO_CLUS_LIST_COUNT (pBgp4Info)) *
                                     CLUSTER_ID_LENGTH));
        BGP4_RM_GET_1_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_CLUS_LIST_ATTR_FLAG (pBgp4Info));
    }
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) &
         BGP4_ATTR_NEXT_HOP_MASK) == BGP4_ATTR_NEXT_HOP_MASK)
    {
        Bgp4RedReadAddrPrefix (pBuf, pu4Offset,
                               &(BGP4_INFO_NEXTHOP_INFO (pBgp4Info)));
    }
#ifdef BGP4_IPV6_WANTED
    BGP4_RM_GET_1_BYTE (pBuf, pu4Offset,
                        BGP4_INFO_LINK_LOCAL_PRESENT (pBgp4Info));
    if (BGP4_INFO_LINK_LOCAL_PRESENT (pBgp4Info) == BGP4_TRUE)
    {
        Bgp4RedReadAddrPrefix (pBuf, pu4Offset,
                               &(BGP4_INFO_LINK_LOCAL_ADDR_INFO (pBgp4Info)));
    }
#endif
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_UNKNOWN_MASK)
        == BGP4_ATTR_UNKNOWN_MASK)
    {
        /*        <-4 bytes ->|<- N bytes ->
         *        ---------------------------
         *        | Attr Len  | Attribute   |
         *        ---------------------------
         */
        ATTRIBUTE_NODE_CREATE (pu1UnknownAttr);
        if (pu1UnknownAttr == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                      BGP4_OS_RESOURCE_TRC | BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tMemory Allocation for Unknown Attribute while syncing "
                      "information FAILED\n");
            Bgp4DshReleaseBgpInfo (pBgp4Info);
            gu4BgpDebugCnt[MAX_BGP_ATTRIBUTE_SIZE_SIZING_ID]++;
            return NULL;
        }
        BGP4_INFO_UNKNOWN_ATTR (pBgp4Info) = pu1UnknownAttr;
        BGP4_RM_GET_4_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_UNKNOWN_ATTR_LEN (pBgp4Info));
        BGP4_RM_GET_N_BYTE (pBuf, pu4Offset, BGP4_INFO_UNKNOWN_ATTR (pBgp4Info),
                            BGP4_INFO_UNKNOWN_ATTR_LEN (pBgp4Info));
    }
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_ORIG_ID_MASK)
        == BGP4_ATTR_ORIG_ID_MASK)
    {
        BGP4_RM_GET_4_BYTE (pBuf, pu4Offset, BGP4_INFO_ORIG_ID (pBgp4Info));
    }
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_MED_MASK)
        == BGP4_ATTR_MED_MASK)
    {
        BGP4_RM_GET_4_BYTE (pBuf, pu4Offset, BGP4_INFO_RCVD_MED (pBgp4Info));
    }
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_LOCAL_PREF_MASK)
        == BGP4_ATTR_LOCAL_PREF_MASK)
    {
        BGP4_RM_GET_4_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_RCVD_LOCAL_PREF (pBgp4Info));
    }
    if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_ORIGIN_MASK)
        == BGP4_ATTR_ORIGIN_MASK)
    {
        BGP4_RM_GET_1_BYTE (pBuf, pu4Offset, BGP4_INFO_ORIGIN (pBgp4Info));
    }

    BGP4_RM_GET_2_BYTE (pBuf, pu4Offset, BGP4_INFO_WEIGHT (pBgp4Info));
    BGP4_RM_GET_1_BYTE (pBuf, pu4Offset, BGP4_MPE_BGP_SAFI_INFO (pBgp4Info));
    BGP4_INFO_REF_COUNT (pBgp4Info)++;
    return pBgp4Info;
}

/*****************************************************************************/
/*    Function Name       : Bgp4RmFillBufBgpInfo                             */
/*    Description         : This function writes the route bgp information   */
/*                          in the given buffer                              */
/*    Input(s)            : pBuf - RM buffer                                 */
/*                          pu4OffSet - offset of the buffer                 */
/*                          pRtProfile - Update route info with bgp info     */
/*    Output(s)           : pu4OffSet - updated offset value                 */
/*    Returns             : BGP4_SUCCESS/BGP4_FAILURE                        */
/*****************************************************************************/
INT4
Bgp4RmFillBufBgpInfo (tBgp4Info * pRtInfo, tRmMsg * pBuf, UINT4 *pu4Offset)
{
    tAsPath            *pASSeg;

    if (*pu4Offset >= BGP4_RED_RT_BGPINFO_MSG_MAX_LEN)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC | BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME,
                  "\tBuffer not sufficient for route information\n");
        return BGP4_FAILURE;
    }

    BGP4_RM_PUT_4_BYTE (pBuf, pu4Offset, BGP4_INFO_ATTR_FLAG (pRtInfo));

    if ((BGP4_INFO_ATTR_FLAG (pRtInfo) & BGP4_ATTR_PATH_MASK)
        == BGP4_ATTR_PATH_MASK)
    {
        BGP4_RM_PUT_2_BYTE (pBuf, pu4Offset,
                            TMO_SLL_Count (BGP4_INFO_ASPATH (pRtInfo)));
        TMO_SLL_Scan (BGP4_INFO_ASPATH (pRtInfo), pASSeg, tAsPath *)
        {
            BGP4_RM_PUT_1_BYTE (pBuf, pu4Offset, BGP4_ASPATH_TYPE (pASSeg));
            BGP4_RM_PUT_1_BYTE (pBuf, pu4Offset, BGP4_ASPATH_LEN (pASSeg));
            BGP4_RM_PUT_N_BYTE (pBuf, pu4Offset, BGP4_ASPATH_NOS (pASSeg),
                                (UINT4) (BGP4_AS4_LENGTH *
                                         BGP4_ASPATH_LEN (pASSeg)));
        }
    }

    if ((BGP4_INFO_ATTR_FLAG (pRtInfo) & BGP4_ATTR_AGGREGATOR_MASK)
        == BGP4_ATTR_AGGREGATOR_MASK)
    {
        BGP4_RM_PUT_4_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_AGGREGATOR_NODE (pRtInfo));
        BGP4_RM_PUT_4_BYTE (pBuf, pu4Offset, BGP4_INFO_AGGREGATOR_AS (pRtInfo));
        BGP4_RM_PUT_1_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_AGGREGATOR_FLAG (pRtInfo));
    }

    if ((BGP4_INFO_ATTR_FLAG (pRtInfo) & BGP4_ATTR_COMM_MASK)
        == BGP4_ATTR_COMM_MASK)
    {
        BGP4_RM_PUT_2_BYTE (pBuf, pu4Offset, BGP4_INFO_COMM_COUNT (pRtInfo));
        BGP4_RM_PUT_N_BYTE (pBuf, pu4Offset, BGP4_INFO_COMM_ATTR_VAL (pRtInfo),
                            (UINT4) ((BGP4_INFO_COMM_COUNT (pRtInfo)) *
                                     COMM_VALUE_LEN));
        BGP4_RM_PUT_1_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_COMM_ATTR_FLAG (pRtInfo));
    }

    if ((BGP4_INFO_ATTR_FLAG (pRtInfo) & BGP4_ATTR_ECOMM_MASK)
        == BGP4_ATTR_ECOMM_MASK)
    {
        BGP4_RM_PUT_2_BYTE (pBuf, pu4Offset, BGP4_INFO_ECOMM_COUNT (pRtInfo));
        BGP4_RM_PUT_1_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_ECOMM_ATTR_FLAG (pRtInfo));
        BGP4_RM_PUT_1_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_ECOMM_ATTR_COST_FLAG (pRtInfo));
        if (BGP4_INFO_ECOMM_ATTR_COST_FLAG (pRtInfo) == BGP4_TRUE)
        {
            BGP4_RM_PUT_4_BYTE (pBuf, pu4Offset,
                                BGP4_INFO_ECOMM_ATTR_COST_VAL (pRtInfo));
            BGP4_RM_PUT_2_BYTE (pBuf, pu4Offset,
                                BGP4_INFO_ECOMM_ATTR_COST_POI (pRtInfo));
            BGP4_RM_PUT_2_BYTE (pBuf, pu4Offset,
                                BGP4_INFO_ECOMM_ATTR_COST_COMM (pRtInfo));
        }
        else
        {
            BGP4_RM_PUT_N_BYTE (pBuf, pu4Offset,
                                BGP4_INFO_ECOMM_ATTR_VAL (pRtInfo),
                                (UINT4) ((BGP4_INFO_ECOMM_COUNT (pRtInfo)) *
                                         EXT_COMM_VALUE_LEN));
        }
    }

    if ((BGP4_INFO_ATTR_FLAG (pRtInfo) & BGP4_ATTR_CLUS_LIST_MASK)
        == BGP4_ATTR_CLUS_LIST_MASK)
    {
        BGP4_RM_PUT_2_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_CLUS_LIST_COUNT (pRtInfo));
        BGP4_RM_PUT_N_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_CLUS_LIST_ATTR_VAL (pRtInfo),
                            (UINT4) ((BGP4_INFO_CLUS_LIST_COUNT (pRtInfo)) *
                                     CLUSTER_ID_LENGTH));
        BGP4_RM_PUT_1_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_CLUS_LIST_ATTR_FLAG (pRtInfo));
    }
    if ((BGP4_INFO_ATTR_FLAG (pRtInfo) &
         BGP4_ATTR_NEXT_HOP_MASK) == BGP4_ATTR_NEXT_HOP_MASK)
    {
        Bgp4RedFillBufAddr (BGP4_INFO_NEXTHOP_INFO (pRtInfo), pBuf, pu4Offset);
    }
#ifdef BGP4_IPV6_WANTED
    BGP4_RM_PUT_1_BYTE (pBuf, pu4Offset,
                        BGP4_INFO_LINK_LOCAL_PRESENT (pRtInfo));
    if (BGP4_INFO_LINK_LOCAL_PRESENT (pRtInfo) == BGP4_TRUE)
    {
        Bgp4RedFillBufAddr (BGP4_INFO_LINK_LOCAL_ADDR_INFO (pRtInfo),
                            pBuf, pu4Offset);
    }
#endif
    if ((BGP4_INFO_ATTR_FLAG (pRtInfo) & BGP4_ATTR_UNKNOWN_MASK)
        == BGP4_ATTR_UNKNOWN_MASK)
    {
        BGP4_RM_PUT_4_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_UNKNOWN_ATTR_LEN (pRtInfo));
        BGP4_RM_PUT_N_BYTE (pBuf, pu4Offset, BGP4_INFO_UNKNOWN_ATTR (pRtInfo),
                            BGP4_INFO_UNKNOWN_ATTR_LEN (pRtInfo));
    }
    if ((BGP4_INFO_ATTR_FLAG (pRtInfo) & BGP4_ATTR_ORIG_ID_MASK)
        == BGP4_ATTR_ORIG_ID_MASK)
    {
        BGP4_RM_PUT_4_BYTE (pBuf, pu4Offset, BGP4_INFO_ORIG_ID (pRtInfo));
    }
    if ((BGP4_INFO_ATTR_FLAG (pRtInfo) & BGP4_ATTR_MED_MASK)
        == BGP4_ATTR_MED_MASK)
    {
        BGP4_RM_PUT_4_BYTE (pBuf, pu4Offset, BGP4_INFO_RCVD_MED (pRtInfo));
    }
    if ((BGP4_INFO_ATTR_FLAG (pRtInfo) & BGP4_ATTR_LOCAL_PREF_MASK)
        == BGP4_ATTR_LOCAL_PREF_MASK)
    {
        BGP4_RM_PUT_4_BYTE (pBuf, pu4Offset,
                            BGP4_INFO_RCVD_LOCAL_PREF (pRtInfo));
    }
    if ((BGP4_INFO_ATTR_FLAG (pRtInfo) & BGP4_ATTR_ORIGIN_MASK)
        == BGP4_ATTR_ORIGIN_MASK)
    {
        BGP4_RM_PUT_1_BYTE (pBuf, pu4Offset, BGP4_INFO_ORIGIN (pRtInfo));
    }

    BGP4_RM_PUT_2_BYTE (pBuf, pu4Offset, BGP4_INFO_WEIGHT (pRtInfo));
    BGP4_RM_PUT_1_BYTE (pBuf, pu4Offset, BGP4_MPE_BGP_SAFI_INFO (pRtInfo));
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : Bgp4RedProcessRouteSyncMsg                       */
/*    Description         : This function will process the dynamic update    */
/*                          route message and sync the route information to  */
/*                          Standby RIB                                      */
/*    Input(s)            : pMsg - Route bulk update message                 */
/*                          pu4OffSet - Offset of the update message         */
/*    Output(s)           : None                                             */
/*    Returns             : None.                                            */
/*****************************************************************************/
PUBLIC INT4
Bgp4RedProcessRouteSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                            UINT1 u1Message, UINT2 u2LenOffSet)
{
    tRouteProfile      *pRtProfile = NULL;
    tBgp4Info          *pBgp4Info = NULL;
    tRouteProfile      *pFndRtProfileList = NULL;
    tRouteProfile      *pPeerRoute = NULL;
    tBgp4PeerEntry     *pPeer = NULL;
    VOID               *pRibNode = NULL;
    INT4                i4RetVal = BGP4_FAILURE;
    INT4                i4RtFound = BGP4_FAILURE;

    if (*pu4OffSet < u2LenOffSet)
    {
        i4RetVal = BGP4_SUCCESS;
        pRtProfile = Bgp4RedReadRouteInfo (pMsg, pu4OffSet);
        if (pRtProfile == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_EVENTS_TRC | BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                      "\tUnable to read the route information\n");
            return BGP4_FAILURE;
        }
        if (u1Message == BGP4_RED_UPD_ROUTE_MESSAGE)
        {
            pBgp4Info = Bgp4RedReadBgp4Info (pMsg, pu4OffSet);
            if (pBgp4Info == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                          "\tUnable to read the route path attributes\n");
                Bgp4DshReleaseRtInfo (pRtProfile);
                return BGP4_FAILURE;
            }
            BGP4_LINK_INFO_TO_PROFILE (pBgp4Info, pRtProfile);
        }
        else if (u1Message == BGP4_RED_UPD_ROUTE_FLAG_MSG)
        {
            /* The route flag information has modified. Find the route in the 
             * RIB and update the values. If the route is not present in the RIB
             * return failure */
            i4RtFound =
                Bgp4RibhLookupRtEntry (pRtProfile, BGP4_RT_CXT_ID (pRtProfile),
                                       BGP4_TREE_FIND_EXACT, &pFndRtProfileList,
                                       &pRibNode);
            if (i4RtFound == BGP4_SUCCESS)
            {
                pPeerRoute = Bgp4DshGetPeerRtFromProfileList
                    (pFndRtProfileList, BGP4_RT_PEER_ENTRY (pRtProfile));
                if (pPeerRoute != NULL)
                {
                    /* The previous added entry is obtained. 
                     * Link the route to the old bgp info and add it to RIB */
                    BGP4_LINK_INFO_TO_PROFILE (BGP4_RT_BGP_INFO (pPeerRoute),
                                               pRtProfile);
                    /* getting Route Updation Msg to Update FIB,so no need to perform RFD */
                    BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_NO_RFD);
                }
                else
                {
                    i4RtFound = BGP4_FAILURE;
                }
            }
            if (i4RtFound == BGP4_FAILURE)
            {
                BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                               "Previous RIB entry "
                               "of the route %s mask %s is not found\n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               Bgp4PrintIpMask ((UINT1)
                                                BGP4_RT_PREFIXLEN (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)));
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, BGP4_SYSLOG_ID,
                              "Bgp4RedProcessRouteSyncMsg: Previous RIB entry "
                              "of the route %s mask %s is not found\n",
                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                               BGP4_RT_AFI_INFO (pRtProfile)),
                              Bgp4PrintIpMask ((UINT1)
                                               BGP4_RT_PREFIXLEN (pRtProfile),
                                               BGP4_RT_AFI_INFO (pRtProfile))));
                Bgp4DshReleaseRtInfo (pRtProfile);
                return BGP4_FAILURE;
            }
        }

        if (bgp4RibLock () != OSIX_SUCCESS)
        {
            Bgp4DshReleaseRtInfo (pRtProfile);
            if (u1Message == BGP4_RED_UPD_ROUTE_MESSAGE)
            {
                Bgp4DshReleaseBgpInfo (pBgp4Info);
            }
            return BGP4_FAILURE;
        }
        /* if route has an associated peer entry, then it is a BGP route
         * else it is can be a igp route */
        if (BGP4_RT_PEER_ENTRY (pRtProfile) != NULL)
        {
            pPeer = BGP4_RT_PEER_ENTRY (pRtProfile);
            if ((((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_WITHDRAWN) ==
                  BGP4_RT_WITHDRAWN) || ((BGP4_RT_GET_EXT_FLAGS (pRtProfile)
                                          & BGP4_ROUTE_WITHDRAWN) ==
                                         BGP4_ROUTE_WITHDRAWN))
                &&
                ((BGP4_RT_GET_EXT_FLAGS (pRtProfile) &
                  BGP4_ROUTE_BEST_ROUTE_UPDATE) ==
                 BGP4_ROUTE_BEST_ROUTE_UPDATE))

            {
                BGP4_RT_RESET_EXT_FLAG (pRtProfile,
                                        BGP4_ROUTE_BEST_ROUTE_UPDATE);
            }
            else if (((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_WITHDRAWN) ==
                      BGP4_RT_WITHDRAWN) || ((BGP4_RT_GET_EXT_FLAGS (pRtProfile)
                                              & BGP4_ROUTE_WITHDRAWN) ==
                                             BGP4_ROUTE_WITHDRAWN))
            {
                if (BGP4_GET_PEER_CURRENT_STATE (pPeer) !=
                    BGP4_PEER_DEINIT_INPROGRESS)
                {
                    i4RetVal =
                        Bgp4RibhProcessWithdRoute (pPeer, pRtProfile, NULL);
                }

            }
            else
            {
                i4RetVal = Bgp4RibhProcessFeasibleRoute (pPeer, pRtProfile);
            }
        }
        else
        {

            if (((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_WITHDRAWN) ==
                 BGP4_RT_WITHDRAWN) ||
                ((BGP4_RT_GET_EXT_FLAGS (pRtProfile) & BGP4_ROUTE_WITHDRAWN) ==
                 BGP4_ROUTE_WITHDRAWN))

            {
                /*
                   if GLOBAL_ADMIN_DOWN is in progress,then from the workhorse thread
                   Bgp4RibhFreeTree will be called,and all the RIB entries will be 
                   cleared.No need to do explicitly here.
                   And also if we come here after the completion of the ADMIN_DOWN cycle
                   in dispatcher now global state defaults to READY but the RIB_COUNT
                   will be zero in this case.
                 */
                if (BGP4_GLOBAL_STATE (BGP4_RT_CXT_ID (pRtProfile))
                    != BGP4_GLOBAL_ADMIN_DOWN_INPROGRESS &&
                    BGP4_RIB_ROUTE_CNT (BGP4_RT_CXT_ID (pRtProfile)) != 0)
                {
                    i4RetVal = Bgp4IgphProcessWithdrawnImportRoute (pRtProfile);
                }
            }
            else
            {
                i4RetVal = Bgp4IgphProcessFeasibleImportRoute (pRtProfile);
            }
        }
        bgp4RibUnlock ();
        if (i4RetVal == BGP4_FAILURE)
        {
            BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                           "\tFAILED to add the "
                           "route %s mask %s \n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)),
                           Bgp4PrintIpMask ((UINT1)
                                            BGP4_RT_PREFIXLEN (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, BGP4_SYSLOG_ID,
                          "Bgp4RedProcessRouteSyncMsg: FAILED to add the "
                          "route %s mask %s \n",
                          Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                           BGP4_RT_AFI_INFO (pRtProfile)),
                          Bgp4PrintIpMask ((UINT1)
                                           BGP4_RT_PREFIXLEN (pRtProfile),
                                           BGP4_RT_AFI_INFO (pRtProfile))));

        }
        /* If the route is added to the RIB, then the pRtProfile reference count
         * alone will be decremented here. Else, the pRtProfile will be released
         * here */
        if (u1Message == BGP4_RED_UPD_ROUTE_MESSAGE)
        {
            Bgp4DshReleaseBgpInfo (pBgp4Info);
        }
        Bgp4DshReleaseRtInfo (pRtProfile);
    }
    return i4RetVal;
}

/*****************************************************************************/
/*    Function Name       : Bgp4RedProcessBulkRouteSyncMsg                   */
/*    Description         : This function will process the bulk update route */
/*                          message and sync the route information to        */
/*                          Standby RIB                                      */
/*    Input(s)            : pMsg - Route bulk update message                 */
/*                          pu4OffSet - Offset of the update message         */
/*    Output(s)           : None                                             */
/*    Returns             : None.                                            */
/*****************************************************************************/
PUBLIC INT4
Bgp4RedProcessBulkRouteSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                UINT2 u2LenOffSet)
{
    tRouteProfile      *pRtProfile = NULL;
    tBgp4Info          *pBgp4Info = NULL;
    UINT4               u4RouteCnt = 0;
    UINT4               u4Counter = 0;
    INT4                i4RetVal = BGP4_FAILURE;

    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedProcessBulkRouteSyncMsg \r\n");
    while (*pu4OffSet < u2LenOffSet)
    {
        pBgp4Info = Bgp4RedReadBgp4Info (pMsg, pu4OffSet);
        if (pBgp4Info == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_EVENTS_TRC | BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                      "\tUnable to read the route path attributes\n");
            return BGP4_FAILURE;
        }
        KW_FALSEPOSITIVE_FIX (pBgp4Info);
        BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4RouteCnt);
        for (u4Counter = 0; u4Counter < u4RouteCnt; u4Counter++)
        {
            if (*pu4OffSet > u2LenOffSet)
            {
                break;
            }
            pRtProfile = Bgp4RedReadRouteInfo (pMsg, pu4OffSet);
            if (pRtProfile == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                          BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                          "\tUnable to read the route information\n");
                Bgp4DshReleaseBgpInfo (pBgp4Info);
                return BGP4_FAILURE;
            }

            BGP4_LINK_INFO_TO_PROFILE (pBgp4Info, pRtProfile);
            /* if route has an associated peer entry, then it is a BGP route
             * else it is can be a igp route */
            i4RetVal = BGP4_FAILURE;
            if (bgp4RibLock () != OSIX_SUCCESS)
            {
                Bgp4DshReleaseRtInfo (pRtProfile);
                Bgp4DshReleaseBgpInfo (pBgp4Info);
                return BGP4_FAILURE;
            }

            if (BGP4_RT_PEER_ENTRY (pRtProfile) != NULL)
            {
                i4RetVal = Bgp4RibhProcessFeasibleRoute
                    (BGP4_RT_PEER_ENTRY (pRtProfile), pRtProfile);
            }
            else
            {
                /* For imported routes, nexthop information will not be present.
                 * initialize the next hop based on the AFI of the route
                 * as by default it will be initialized for v4 next hop address */
                if ((BGP4_INFO_ATTR_FLAG (pBgp4Info) & BGP4_ATTR_NEXT_HOP_MASK)
                    != BGP4_ATTR_NEXT_HOP_MASK)
                {
                    Bgp4InitAddrPrefixStruct (&
                                              (BGP4_INFO_NEXTHOP_INFO
                                               (pBgp4Info)),
                                              BGP4_RT_AFI_INFO (pRtProfile));
                }
                i4RetVal = Bgp4IgphProcessFeasibleImportRoute (pRtProfile);
            }
            bgp4RibUnlock ();

            if (i4RetVal == BGP4_FAILURE)
            {
                BGP4_TRC_ARG2 (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                               "\tFAILED to add the "
                               "route %s mask %s \n",
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)),
                               Bgp4PrintIpMask ((UINT1)
                                                BGP4_RT_PREFIXLEN (pRtProfile),
                                                BGP4_RT_AFI_INFO (pRtProfile)));
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, BGP4_SYSLOG_ID,
                              "Bgp4RedProcessBulkRouteSyncMsg: FAILED to add the "
                              "route %s mask %s \n",
                              Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                               BGP4_RT_AFI_INFO (pRtProfile)),
                              Bgp4PrintIpMask ((UINT1)
                                               BGP4_RT_PREFIXLEN (pRtProfile),
                                               BGP4_RT_AFI_INFO (pRtProfile))));
            }
            /* If the route is added to the RIB, then the pRtProfile reference count
             * alone will be decremented here. Else, the pRtProfile will be released
             * here */
            Bgp4DshReleaseRtInfo (pRtProfile);
        }
        Bgp4DshReleaseBgpInfo (pBgp4Info);
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedProcessBulkRouteSyncMsg \r\n");
    return BGP4_SUCCESS;
}

/************************************************************************/
/* Function Name      : Bgp4RedSendBulkRequestOrTailMsg                 */
/* Description        : This function sends the bulk request message or */
/*                      bulk tail message                               */
/* Input(s)           : u1Message - BGP4_RED_BULK_REQ_MESSAGE /         */
/*                                   BGP4_RED_BULK_TAIL_MESSAGE         */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS/BGP4_FAILURE                       */
/************************************************************************/
PUBLIC INT4
Bgp4RedSendBulkRequestOrTailMsg (UINT1 u1Message)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entergin Bgp4RedSendBulkRequestOrTailMsg\r\n");
    ProtoEvt.u4AppId = RM_BGP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    ProtoEvt.u4Error = RM_MEMALLOC_FAIL;

    pMsg = RM_ALLOC_TX_BUF (BGP4_RED_MIN_MSG_SIZE);
    if (pMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC | BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME, "\tRM Memory allocation failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, BGP4_SYSLOG_ID,
                      "Bgp4RedSendBulkRequestOrTailMsg: RM Memory allocation failed\r\n"));
        RmApiHandleProtocolEvent (&ProtoEvt);
        return BGP4_FAILURE;
    }
    BGP4_RM_PUT_1_BYTE (pMsg, &u4OffSet, u1Message);
    BGP4_RM_PUT_2_BYTE (pMsg, &u4OffSet, BGP4_RED_MIN_MSG_SIZE);

    if (RmEnqMsgToRmFromAppl (pMsg, (UINT2) u4OffSet,
                              RM_BGP_APP_ID, RM_BGP_APP_ID) == RM_FAILURE)
    {
        RM_FREE (pMsg);
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC | BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME, "\tEnqueue msg to RM FAILED\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, BGP4_SYSLOG_ID,
                      "Bgp4RedSendBulkRequestOrTailMsg: Enqueue msg to RM FAILED\n"));
        return BGP4_FAILURE;
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedSendBulkRequestOrTailMsg\r\n");
    return BGP4_SUCCESS;
}

/************************************************************************/
/* Function Name      : Bgp4RedSendBulkUpdMsg                           */
/* Description        : This function sends the dynamic bulk update of  */
/*                       the routes available in the RIB                */
/* Input(s)           : None                                            */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS/BGP4_FAILURE/BGP4_RELINQUISH       */
/************************************************************************/
PUBLIC INT4
Bgp4RedSendBulkUpdMsg (VOID)
{
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    INT4                i4RetVal = 0;
    INT4                i4GlobalAdmin = 0;
    UINT4               u4Context = 0;
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, BGP4_SYSLOG_ID,
                  "Entering Bgp4RedSendBulkUpdMsg"));
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedSendBulkUpdMsg\r\n");
    if (BGP4_RED_BULK_UPD_STATUS == BGP4_HA_UPD_NOT_STARTED)
    {
        if (BgpGetFirstActiveContextID (&BGP4_RED_SYNC_CXT_ID) == SNMP_FAILURE)
        {
            /* BGP is not enabled in any context.
             * No need to send bulk update message */
            BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_COMPLETED;
            Bgp4RedSendBulkRequestOrTailMsg (BGP4_RED_BULK_TAIL_MESSAGE);
            RmSetBulkUpdatesStatus (RM_BGP_APP_ID);
            return BGP4_SUCCESS;
        }
        Bgp4GetAfiSafiIndex (BGP4_INET_AFI_IPV4, BGP4_INET_SAFI_UNICAST,
                             &(BGP4_RED_SYNC_AFSAFI_INDEX));
        BGP4_RED_SYNC_HASH_KEY = 0;
        BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_IN_PROGRESS;

        if (Bgp4RedSyncPeerDampBulkMsg () == BGP4_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                      BGP4_MOD_NAME,
                      "\tSending Peer damp bulk update " "message failed\n");
            return BGP4_FAILURE;
        }
    }

#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
    if (Bgp4RedGetBgpRmMsg
        (&pMsg, BGP4_RED_BULK_UPD_VPLS_INST_MESSAGE, &u4OffSet) == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "Bgp4RedSendBulkUpdMsg: BgpRmMsg allocation failed\n");
        BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_IN_PROGRESS;
        return BGP4_RELINQUISH;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    i4RetVal = Bgp4RedSyncVplsInfoBulkMsg (pMsg, &u4OffSet);
    if (i4RetVal == BGP4_FAILURE)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, BGP4_SYSLOG_ID,
                      "Bgp4RedSyncVplsInfoBulkMsg return failure."));
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                  "Bgp4RedSendBulkUpdMsg: Bgp4RedSyncVplsInfoBulkMsg returned failure\n");
        BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_ABORTED;
        return BGP4_FAILURE;
    }
#endif
#endif
    u4OffSet = 0;
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_BULK_UPD_BGP_ID, &u4OffSet)
        == BGP4_FAILURE)
    {
        BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_IN_PROGRESS;
        return BGP4_RELINQUISH;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;
    if (Bgp4RedSyncBgpIdBulkMsg (pMsg, &u4OffSet) == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                  BGP4_MOD_NAME,
                  "\tBGP ID Sync bulk update " "message failed\n");
        return BGP4_FAILURE;
    }
    u4OffSet = 0;
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_BULK_UPD_RESTART_REASON, &u4OffSet)
        == BGP4_FAILURE)
    {
        BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_IN_PROGRESS;
        return BGP4_RELINQUISH;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;
    if (Bgp4RedSyncRestartReasonBulkMsg (pMsg, &u4OffSet) == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                  BGP4_MOD_NAME,
                  "\tBGP GR Restart Reason Sync bulk"
                  "update message failed\n");
        return BGP4_FAILURE;
    }

    u4OffSet = 0;
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_BULK_UPD_TABLE_VERSION, &u4OffSet)
        == BGP4_FAILURE)
    {
        BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_IN_PROGRESS;
        return BGP4_RELINQUISH;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;
    if (Bgp4RedSyncTableVersionBulkMsg (pMsg, &u4OffSet) == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                  BGP4_MOD_NAME,
                  "\tBGP ID Sync bulk update " "message failed\n");
        return BGP4_FAILURE;
    }

    u4OffSet = 0;

    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_BULK_UPD_BGP_PEER_INFO, &u4OffSet)
        == BGP4_FAILURE)
    {
        BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_IN_PROGRESS;
        return BGP4_RELINQUISH;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;
    if (Bgp4RedSyncBgpPeerInfoBulkMsg (pMsg, &u4OffSet) == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                  BGP4_MOD_NAME,
                  "\tLocal Address Sync bulk update " "message failed\n");
        return BGP4_FAILURE;
    }

    u4OffSet = 0;
    if (Bgp4RedGetBgpRmMsg
        (&pMsg, BGP4_RED_BULK_UPD_PEER_MPCAP_V4_INFO,
         &u4OffSet) == BGP4_FAILURE)
    {
        BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_IN_PROGRESS;
        return BGP4_RELINQUISH;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;
    if (Bgp4RedSyncBgpPeerMPCapV4BulkMsg (pMsg, &u4OffSet) == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                  BGP4_MOD_NAME,
                  "\tBGP Peer MP Cap Sync bulk update " "message failed\n");
        return BGP4_FAILURE;
    }
    u4OffSet = 0;
    if (Bgp4RedGetBgpRmMsg
        (&pMsg, BGP4_RED_BULK_UPD_PEER_MPCAP_V6_INFO,
         &u4OffSet) == BGP4_FAILURE)
    {
        BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_IN_PROGRESS;
        return BGP4_RELINQUISH;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;
    if (Bgp4RedSyncBgpPeerMPCapV6BulkMsg (pMsg, &u4OffSet) == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                  BGP4_MOD_NAME,
                  "\tBGP Peer MP Cap Sync bulk update " "message failed\n");
        return BGP4_FAILURE;
    }

    i4RetVal = BgpGetFirstActiveContextID (&u4Context);
    i4GlobalAdmin = BGP4_LOCAL_ADMIN_STATUS (u4Context);
    u4OffSet = 0;
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_BULK_BGP_ADMIN_STATUS, &u4OffSet)
        == BGP4_FAILURE)
    {
        BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_IN_PROGRESS;
        return BGP4_RELINQUISH;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;
    if (Bgp4RedSyncBgpAdminStatusBulkMsg (pMsg, &u4OffSet, gi1RFDSyncStatus) ==
        BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                  BGP4_MOD_NAME,
                  "\tBGP Clobal admin status" "update message failed\n");
        return BGP4_FAILURE;
    }

    while (i4RetVal == SNMP_SUCCESS && i4GlobalAdmin == BGP4_ADMIN_UP)
    {
        u4OffSet = 0;
        if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_BULK_BGP_RRD_MASK, &u4OffSet)
            == BGP4_FAILURE)
        {
            BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_IN_PROGRESS;
            return BGP4_RELINQUISH;
        }
        /* increment the offset to write the length of the update mesasge */
        u4OffSet += 2;
        if (Bgp4RedSyncRRDMetricMaskBulkMsg (pMsg, &u4OffSet, u4Context) ==
            BGP4_FAILURE)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                      BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC,
                      BGP4_MOD_NAME,
                      "\tBGP GR Restart Reason Sync bulk"
                      "update message failed\n");
            return BGP4_FAILURE;
        }
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
        i4GlobalAdmin = BGP4_LOCAL_ADMIN_STATUS (u4Context);
    }

    u4OffSet = 0;
    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_BULK_UPD_ROUTE_MESSAGE, &u4OffSet)
        == BGP4_FAILURE)
    {
        BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_IN_PROGRESS;
        return BGP4_RELINQUISH;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;

    i4RetVal = Bgp4RmFillBufferFromPADB (pMsg, &u4OffSet);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, BGP4_SYSLOG_ID,
                  "Exiting Bgp4RedSendBulkUpdMsg"));
    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "\tSending Bulk update message is successful\r\n");
    if (i4RetVal == BGP4_SUCCESS)
    {
        Bgp4RedSendBulkRequestOrTailMsg (BGP4_RED_BULK_TAIL_MESSAGE);
        BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_COMPLETED;
        RmSetBulkUpdatesStatus (RM_BGP_APP_ID);
        return BGP4_SUCCESS;
    }
    else if (i4RetVal == BGP4_RELINQUISH)
    {
        BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_IN_PROGRESS;
        return BGP4_RELINQUISH;
    }
    else
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, BGP4_SYSLOG_ID,
                      "Bgp4RmFillBufferFromPADB return failure."));
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                  BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                  "\tFilling buffer from PA DB returned failure\n");
        BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_ABORTED;
        return BGP4_FAILURE;
    }
}

/************************************************************************/
/* Function Name      : Bgp4RmFillBufferFromPADB                        */
/* Description        : This function fills the dynamic bulk update msg */
/*                       with the routes available in the PADB table    */
/*                       The bgpinfo of the routes is written first,    */
/*                       followed by the route information. When the    */
/*                       number of hash indices processed exceeds       */
/*                       BGP4_RM_PROCESS_ROUTES_THRESHOLD, the function */
/*                       is relinquished. The PADB table access is      */
/*                       started from the global last synced context,   */
/*                       AFSAFI index, hash index.                      */
/* Input(s)           : pBuf - Update messgage buffer                   */
/*                      pu4Offset - Offset of message                   */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS/BGP4_FAILURE/BGP4_RELINQUISH       */
/************************************************************************/
INT4
Bgp4RmFillBufferFromPADB (tBgpRmMsg * pMsg, UINT4 *pu4Offset)
{
    tRouteProfile      *pRtProfile = NULL;
    tBgp4RcvdPathAttr  *pBgp4RcvdPA = NULL;
    UINT4               u4HashKey = BGP4_RED_SYNC_HASH_KEY;
    UINT4               u4AFIndex = BGP4_RED_SYNC_AFSAFI_INDEX;
    UINT4               u4CxtId = BGP4_RED_SYNC_CXT_ID;
    UINT4               u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
    UINT4               u4RtCnt = 0;
    UINT4               u4RtOffset = *pu4Offset;
    INT4                i4RetVal = BGP4_FAILURE;
    UINT1               u1HashCount = 0;
    UINT1               u1RtFlag = BGP4_FALSE;

    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RmFillBufferFromPADB\r\n");

    do
    {
        if (BGP4_LOCAL_ADMIN_STATUS (u4CxtId) == BGP4_ADMIN_DOWN)
        {
            if (BgpGetNextActiveContextID (u4CxtId, &u4CxtId) == SNMP_FAILURE)
            {
                u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
                BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4RtOffset, u4RtCnt);
                BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, *pu4Offset);
                pMsg->u4DataLen = *pu4Offset;
                Bgp4RedAddTblNode (pMsg);
                Bgp4RedSyncDynInfo ();
                return BGP4_SUCCESS;
            }
            continue;
        }
#if ((defined VPLSADS_WANTED) && (defined VPLS_GR_WANTED))
        for (; u4AFIndex <= (BGP4_MPE_ADDR_FAMILY_MAX_INDEX); u4AFIndex++)
#else
        for (; u4AFIndex <= (BGP4_MPE_ADDR_FAMILY_MAX_INDEX
                             - BGP4_DECREMENT_BY_ONE); u4AFIndex++)
#endif
        {
#ifdef L3VPN
            if (u4AFIndex == BGP4_IPV4_LBLD_INDEX)
            {
                continue;
            }
#endif
            BGP4_HASH_Scan_Table (BGP4_RCVD_PA_DB (u4CxtId, u4AFIndex),
                                  u4HashKey, u4HashKey)
            {
                if (u1HashCount > BGP4_RM_PROCESS_ROUTES_THRESHOLD)
                {
                    u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
                    BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, *pu4Offset);
                    pMsg->u4DataLen = *pu4Offset;
                    Bgp4RedAddTblNode (pMsg);
                    Bgp4RedSyncDynInfo ();
                    BGP4_RED_SYNC_CXT_ID = u4CxtId;
                    BGP4_RED_SYNC_HASH_KEY = u4HashKey;
                    BGP4_RED_SYNC_AFSAFI_INDEX = u4AFIndex;
                    return BGP4_RELINQUISH;
                }
                TMO_HASH_Scan_Bucket (BGP4_RCVD_PA_DB (u4CxtId, u4AFIndex),
                                      u4HashKey, pBgp4RcvdPA,
                                      tBgp4RcvdPathAttr *)
                {
                    if (BGP4_PA_BGP4INFO (pBgp4RcvdPA) == NULL)
                    {
                        continue;
                    }
                    u1RtFlag = BGP4_TRUE;
                    pRtProfile = BGP4_PA_IDENT_BEST_ROUTE_FIRST (pBgp4RcvdPA);
                    while ((pRtProfile != NULL)
                           && (pRtProfile->pMultiPathRtNext != NULL))
                    {
                        Bgp4RedFillBufMultiPathListRoutes
                            (pRtProfile,
                             BGP4_PA_IDENT_BEST_ROUTE_COUNT (pBgp4RcvdPA),
                             &pMsg, pu4Offset, &u4RtCnt, &u4RtOffset);

                        pRtProfile = BGP4_RT_IDENT_ATTR_LINK_NEXT (pRtProfile);
                    }

                    i4RetVal =
                        Bgp4RmFillBufBgpInfo (BGP4_PA_BGP4INFO (pBgp4RcvdPA),
                                              pMsg->pBuf, pu4Offset);
                    if (i4RetVal == BGP4_FAILURE)
                    {
                        u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
                        BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet,
                                            *pu4Offset);
                        pMsg->u4DataLen = *pu4Offset;
                        Bgp4RedAddTblNode (pMsg);
                        Bgp4RedSyncDynInfo ();
                        pMsg = NULL;
                        *pu4Offset = 0;
                        i4RetVal =
                            Bgp4RedGetBgpRmMsg (&pMsg,
                                                BGP4_RED_BULK_UPD_ROUTE_MESSAGE,
                                                pu4Offset);
                        if (i4RetVal == BGP4_FAILURE)
                        {
                            /* Memory is not availble for forming bulk update message
                             * relinquish the process so that the in the next iteration
                             * the some message would have been sent to RM and memory would
                             * be available to send the bulk update */
                            BGP4_RED_SYNC_CXT_ID = u4CxtId;
                            BGP4_RED_SYNC_HASH_KEY = u4HashKey;
                            BGP4_RED_SYNC_AFSAFI_INDEX = u4AFIndex;
                            return BGP4_RELINQUISH;
                        }
                        /* increment the offset to write the length of the update mesasge */
                        *pu4Offset += 2;
                        Bgp4RmFillBufBgpInfo (BGP4_PA_BGP4INFO (pBgp4RcvdPA),
                                              pMsg->pBuf, pu4Offset);
                    }
                    u4RtOffset = *pu4Offset;
                    *pu4Offset += sizeof (UINT4);
                    u4RtCnt = 0;

                    Bgp4RedFillBufListRoutes
                        (BGP4_PA_IDENT_BEST_ROUTE_FIRST (pBgp4RcvdPA),
                         BGP4_PA_IDENT_BEST_ROUTE_COUNT (pBgp4RcvdPA),
                         &pMsg, pu4Offset, &u4RtCnt, &u4RtOffset);
                    Bgp4RedFillBufListRoutes
                        (BGP4_PA_IDENT_NON_BEST_ROUTE_FIRST (pBgp4RcvdPA),
                         BGP4_PA_IDENT_NON_BEST_ROUTE_COUNT (pBgp4RcvdPA),
                         &pMsg, pu4Offset, &u4RtCnt, &u4RtOffset);
                    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4RtOffset, u4RtCnt);
                }
                if (u1RtFlag == BGP4_TRUE)
                {
                    u1HashCount++;
                    u1RtFlag = BGP4_FALSE;
                }
            }
            u4HashKey = 0;
        }
        u4AFIndex = BGP4_IPV4_UNI_INDEX;
    }
    while (BgpGetNextActiveContextID (u4CxtId, &u4CxtId) == SNMP_SUCCESS);

    u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
    BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, *pu4Offset);
    pMsg->u4DataLen = *pu4Offset;
    Bgp4RedAddTblNode (pMsg);
    Bgp4RedSyncDynInfo ();
    BGP4_RED_SYNC_CXT_ID = 0;
    BGP4_RED_SYNC_HASH_KEY = 0;
    BGP4_RED_SYNC_AFSAFI_INDEX = 0;

    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RmFillBufferFromPADB\r\n");

    return BGP4_SUCCESS;
}

/************************************************************************/
/* Function Name      : Bgp4RedFillBufListRoutes                        */
/* Description        : This function fills the update msg with the     */
/*                       routes present in the given best/non best route*/
/*                       list                                           */
/* Input(s)           : pRtProfile - Route list                         */
/*                      u4ListRtCount - Route count in list             */
/*                      pMsg - Bgp RM message                           */
/*                      pu4OffSet - Offset of route information written */
/*                                   in pMsg                            */
/* Output(s)          : pu4RtCnt - Number of routes written to the pMsg */
/*                      pu4RtOffset - Offset position of filling route  */
/*                                     count in pMsg                    */
/* Returns            : None                                            */
/************************************************************************/
VOID
Bgp4RedFillBufListRoutes (tRouteProfile * pRtProfile, UINT4 u4ListRtCount,
                          tBgpRmMsg ** pMessage, UINT4 *pu4OffSet,
                          UINT4 *pu4RtCnt, UINT4 *pu4RtOffset)
{
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
    UINT4               u4Counter = 0;
    UINT4               u4RtCnt = *pu4RtCnt;

    pMsg = *pMessage;
    while ((pRtProfile != NULL) && (u4Counter < u4ListRtCount))
    {
        if (*pu4OffSet >= BGP4_RED_ROUTE_MSG_MAX_LEN)
        {
            u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
            BGP4_RM_PUT_4_BYTE (pMsg->pBuf, pu4RtOffset, u4RtCnt);
            BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, *pu4OffSet);
            pMsg->u4DataLen = *pu4OffSet;
            Bgp4RedAddTblNode (pMsg);
            *pu4OffSet = 0;
            pMsg = NULL;
            if (Bgp4RedGetBgpRmMsg
                (&pMsg, BGP4_RED_BULK_UPD_ROUTE_MESSAGE,
                 pu4OffSet) == BGP4_FAILURE)
            {
                /* Bgp4RedGetBgpRmMsg as no more BgpRmMsg block are available. Sync
                 * the Bgp dynamic information to free the blocks */
                Bgp4RedSyncDynInfo ();
                if (Bgp4RedGetBgpRmMsg
                    (&pMsg, BGP4_RED_BULK_UPD_ROUTE_MESSAGE,
                     pu4OffSet) == BGP4_FAILURE)
                {
                }
            }
            KW_FALSEPOSITIVE_FIX1 (pMsg);
            /* increment the offset to write the length of the update mesasge */
            *pu4OffSet += 2;
            Bgp4RmFillBufBgpInfo (BGP4_RT_BGP_INFO (pRtProfile), pMsg->pBuf,
                                  pu4OffSet);
            *pu4RtOffset = *pu4OffSet;
            *pu4OffSet += sizeof (UINT4);
            u4RtCnt = 0;
        }
        Bgp4RedFillBufRouteInfo (pRtProfile, pMsg->pBuf, pu4OffSet);
        u4RtCnt++;

        pRtProfile = BGP4_RT_IDENT_ATTR_LINK_NEXT (pRtProfile);
        u4Counter++;
    }
    *pu4RtCnt = u4RtCnt;
    *pMessage = pMsg;
    return;
}

/************************************************************************/
/* Function Name      : Bgp4RedFillBufMultiPathListRoutes               */
/* Description        : This function fills the update msg with the     */
/*                      multipath routes present in the given best route*/
/*                       list                                           */
/* Input(s)           : pRtProfile - Route list                         */
/*                      u4ListRtCount - Route count in list             */
/*                      pMsg - Bgp RM message                           */
/*                      pu4OffSet - Offset of route information written */
/*                                   in pMsg                            */
/* Output(s)          : pu4RtCnt - Number of routes written to the pMsg */
/*                      pu4RtOffset - Offset position of filling route  */
/*                                     count in pMsg                    */
/* Returns            : None                                            */
/************************************************************************/
VOID
Bgp4RedFillBufMultiPathListRoutes (tRouteProfile * pRtProfile,
                                   UINT4 u4ListRtCount, tBgpRmMsg ** pMessage,
                                   UINT4 *pu4OffSet, UINT4 *pu4RtCnt,
                                   UINT4 *pu4RtOffset)
{
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
    UINT4               u4Counter = 0;
    UINT4               u4RtCnt = 0;
    INT4                i4RetVal = BGP4_FAILURE;

    pMsg = *pMessage;
    while ((pRtProfile != NULL) && (u4Counter < u4ListRtCount))
    {
        i4RetVal = Bgp4RmFillBufBgpInfo (BGP4_RT_BGP_INFO (pRtProfile),
                                         pMsg->pBuf, pu4OffSet);
        u4RtCnt = 0;
        if ((i4RetVal == BGP4_FAILURE) ||
            (*pu4OffSet >= BGP4_RED_ROUTE_MSG_MAX_LEN))
        {
            u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
            BGP4_RM_PUT_4_BYTE (pMsg->pBuf, pu4RtOffset, u4RtCnt);
            BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, *pu4OffSet);
            pMsg->u4DataLen = *pu4OffSet;
            Bgp4RedAddTblNode (pMsg);
            *pu4OffSet = 0;
            pMsg = NULL;
            if (Bgp4RedGetBgpRmMsg
                (&pMsg, BGP4_RED_BULK_UPD_ROUTE_MESSAGE,
                 pu4OffSet) == BGP4_FAILURE)
            {
                /* Bgp4RedGetBgpRmMsg as no more BgpRmMsg block are available. Sync
                 * the Bgp dynamic information to free the blocks */
                Bgp4RedSyncDynInfo ();
                if (Bgp4RedGetBgpRmMsg
                    (&pMsg, BGP4_RED_BULK_UPD_ROUTE_MESSAGE,
                     pu4OffSet) == BGP4_FAILURE)
                {
                }
            }
            KW_FALSEPOSITIVE_FIX1 (pMsg);
            /* increment the offset to write the length of the update mesasge */
            *pu4OffSet += 2;
            Bgp4RmFillBufBgpInfo (BGP4_RT_BGP_INFO (pRtProfile),
                                  pMsg->pBuf, pu4OffSet);
        }
        *pu4RtOffset = *pu4OffSet;
        *pu4OffSet += sizeof (UINT4);
        Bgp4RedFillBufRouteInfo (pRtProfile, pMsg->pBuf, pu4OffSet);
        u4RtCnt++;
        BGP4_RM_PUT_4_BYTE (pMsg->pBuf, pu4RtOffset, u4RtCnt);

        pRtProfile = pRtProfile->pMultiPathRtNext;
        u4Counter++;
    }
    *pu4RtCnt = u4RtCnt;
    *pMessage = pMsg;
    return;
}

/************************************************************************/
/* Function Name      : Bgp4RedRmReleaseMemoryForMsg                     */
/* Description        : This function is invoked by the BGP module to   */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/* Input(s)           : pu1Block -- Memory Block                        */
/* Output(s)          : None                                            */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/
INT4
Bgp4RedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, BGP4_SYSLOG_ID,
                  "Entering Bgp4RedRmReleaseMemoryForMsg"));
    if (RmReleaseMemoryForMsg (pu1Block) == OSIX_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tFailure in releasing allocated RM" " memory\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, BGP4_SYSLOG_ID,
                      "Bgp4RedRmReleaseMemoryForMsg:Failure in releasing "
                      "allocated memory"));
        return OSIX_FAILURE;
    }
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, BGP4_SYSLOG_ID,
                  "Exiting Bgp4RedRmReleaseMemoryForMsg"));
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Bgp4RedSyncRouteInfo                            */
/* Description        : This function is invoked by the BGP module to   */
/*                      send dynamic updates for route addition/deletion*/
/*                      to the RIB                                      */
/* Input(s)           : pRtProfile - Route information                  */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                     */
/************************************************************************/
PUBLIC INT4
Bgp4RedSyncRouteInfo (tRouteProfile * pRtProfile)
{
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT4               u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
    UINT4               u4AsafiIndex = 0;
    UINT4               u4HashKey = 0;

    if (BGP4_RED_BULK_UPD_STATUS == BGP4_HA_UPD_IN_PROGRESS)
    {
        Bgp4GetAfiSafiIndex (BGP4_RT_AFI_INFO (pRtProfile),
                             BGP4_RT_SAFI_INFO (pRtProfile), &u4AsafiIndex);
        Bgp4PAFormHashKey (BGP4_RT_BGP_INFO (pRtProfile), &u4HashKey,
                           BGP4_MAX_RCVD_PA_HASH_INDEX);

        if ((BGP4_RT_CXT_ID (pRtProfile) > BGP4_RED_SYNC_CXT_ID)
            || (u4AsafiIndex > BGP4_RED_SYNC_AFSAFI_INDEX)
            || (u4HashKey > BGP4_RED_SYNC_HASH_KEY))
        {
            /* Bulk update is still in progress. This route's hashkey is not
             * yet processed. Hence, it will be sent along with the bulk update
             * process */
            return BGP4_SUCCESS;
        }
    }

    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_ROUTE_MESSAGE, &u4OffSet)
        == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;
    Bgp4RedFillBufRouteInfo (pRtProfile, pMsg->pBuf, &u4OffSet);
    Bgp4RmFillBufBgpInfo (BGP4_RT_BGP_INFO (pRtProfile), pMsg->pBuf, &u4OffSet);
    BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
    pMsg->u4DataLen = u4OffSet;
    Bgp4RedAddTblNode (pMsg);
    Bgp4RedSyncDynInfo ();
    return BGP4_SUCCESS;
}

/************************************************************************/
/* Function Name      : Bgp4RedSyncBgpPeerInfo                          */
/* Description        : This function is invoked by the BGP module to   */
/*                      send dynamic update for peer local address          */
/* Input(s)           : u4Context , bgppeerentry                             */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Bgp4RedSyncBgpPeerInfo (UINT4 u4Context, tBgp4PeerEntry * pPeer)
{
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    INT4                u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;

#ifndef VRF_WANTED
    if (u4Context != BGP_DEF_CONTEXTID)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tInvalid context ID\n");
        return BGP4_FAILURE;
    }
#endif

    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_BGP_PEER_INFO, &u4OffSet)
        == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }

    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, u4Context);
    Bgp4RedFillBufAddr (BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                        pMsg->pBuf, &u4OffSet);
    Bgp4RedFillBufAddr (BGP4_PEER_LOCAL_ADDR_INFO (pPeer),
                        pMsg->pBuf, &u4OffSet);
    /*remote router id */
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, BGP4_PEER_BGP_ID (pPeer));
    /* Bgp Negotiated Version */
    BGP4_RM_PUT_1_BYTE (pMsg->pBuf, &u4OffSet, BGP4_PEER_NEG_VER (pPeer));
    BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
    pMsg->u4DataLen = u4OffSet;
    Bgp4RedAddTblNode (pMsg);
    Bgp4RedSyncDynInfo ();
    return BGP4_SUCCESS;
}

/************************************************************************/
/* Function Name      : Bgp4RedSyncBgpId                                */
/* Description        : This function is invoked by the BGP module to   */
/*                      send dynamic updates of bgp Identifier          */
/* Input(s)           : u4Context , u4BgpId                             */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Bgp4RedSyncBgpId (UINT4 u4Context, UINT4 u4BgpId)
{
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    INT4                u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;

    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_BGP_ID, &u4OffSet)
        == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }

    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, u4Context);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, u4BgpId);
    BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
    pMsg->u4DataLen = u4OffSet;
    Bgp4RedAddTblNode (pMsg);
    Bgp4RedSyncDynInfo ();
    return BGP4_SUCCESS;

}

/************************************************************************/
/* Function Name      : Bgp4RedSyncRestartReason                        */
/* Description        : This function is invoked by the BGP module to   */
/*                      send dynamic updates of bgp GR Restart Reason   */
/* Input(s)           : u1RestartReason                                 */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Bgp4RedSyncRestartReason (UINT1 u1RestartReason)
{
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    INT4                u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;

    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_RESTART_REASON, &u4OffSet)
        == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }

    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;
    BGP4_RM_PUT_1_BYTE (pMsg->pBuf, &u4OffSet, u1RestartReason);
    BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
    pMsg->u4DataLen = u4OffSet;
    Bgp4RedAddTblNode (pMsg);
    Bgp4RedSyncDynInfo ();
    return BGP4_SUCCESS;

}

/************************************************************************/
/* Function Name      : Bgp4RedSyncTableVersion                         */
/* Description        : This function is invoked by the BGP module to   */
/*                      send dynamic updates of bgp Table Version       */
/* Input(s)           : u4Context , u4TableVersion                      */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Bgp4RedSyncTableVersion (UINT4 u4Context, UINT4 u4TableVersion)
{
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    INT4                u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;

    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_TABLE_VERSION, &u4OffSet)
        == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }

    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, u4Context);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, u4TableVersion);
    BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
    pMsg->u4DataLen = u4OffSet;
    Bgp4RedAddTblNode (pMsg);
    Bgp4RedSyncDynInfo ();
    return BGP4_SUCCESS;

}

/************************************************************************/
/* Function Name      : Bgp4RedSyncRouteUpdToFIB                        */
/* Description        : This function is invoked by the BGP module to   */
/*                      send dynamic updates for routes added is        */
/*                      selected as best route and added to IP FIB table*/
/* Input(s)           : pRtProfile - Route information                  */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                     */
/************************************************************************/
PUBLIC INT4
Bgp4RedSyncRouteUpdToFIB (tRouteProfile * pRtProfile)
{
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT4               u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;

    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_ROUTE_FLAG_MSG, &u4OffSet)
        == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;
    /* Only route profile flag would have been updated. Send that alone */
    Bgp4RedFillBufRouteInfo (pRtProfile, pMsg->pBuf, &u4OffSet);
    BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
    DbUtilAddTblNode (&(gBgpNode.DynInfoList), &pMsg->DbNode);
    pMsg->u4DataLen = u4OffSet;
    Bgp4RedSyncDynInfo ();
    KW_FALSEPOSITIVE_FIX1 (pMsg);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : Bgp4RedSyncBgpPeerInfoBulkMsg                          */
/*    Description         : This function will update the BGP identifier     */
/*                           and send information in the bulk message        */
/*    Input(s)            : tBgpRmMsg , pu4Offset                            */
/*    Output(s)           : None                                             */
/*    Returns             : BGP4_SUCCESS                                     */
/*****************************************************************************/

INT4
Bgp4RedSyncBgpPeerInfoBulkMsg (tBgpRmMsg * pMsg, UINT4 *pu4Offset)
{
    tBgp4PeerEntry     *pPeer = NULL;
    UINT4               u4Context = 0;
    UINT4               u4OffSet = *pu4Offset;
    UINT4               u4LenOffSet = 0;
    INT4                i4RetVal = BGP4_FAILURE;

    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedSyncBgpPeerInfoBulkMsg\r\n");
    i4RetVal = BgpGetFirstActiveContextID (&u4Context);
    while (i4RetVal == SNMP_SUCCESS)
    {
        BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, u4Context);
#ifndef VRF_WANTED
        if (u4Context != BGP_DEF_CONTEXTID)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                      BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                      "\tInvalid context\r\n");
            return BGP4_FAILURE;
        }
#endif

        TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
        {
            if ((pPeer != NULL)
                && (BGP4_GET_PEER_CURRENT_STATE (pPeer) !=
                    BGP4_PEER_DEINIT_INPROGRESS))
            {
                Bgp4RedFillBufAddr (BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                                    pMsg->pBuf, &u4OffSet);
                Bgp4RedFillBufAddr (BGP4_PEER_LOCAL_ADDR_INFO (pPeer),
                                    pMsg->pBuf, &u4OffSet);
                BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet,
                                    BGP4_PEER_BGP_ID (pPeer));
                BGP4_RM_PUT_1_BYTE (pMsg->pBuf, &u4OffSet,
                                    BGP4_PEER_NEG_VER (pPeer));
                BGP4_RM_PUT_1_BYTE (pMsg->pBuf, &u4OffSet,
                                    BGP4_PEER_ADMIN_STATUS (pPeer));

            }
        }
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
    }
    u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
    BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
    DbUtilAddTblNode (&(gBgpNode.DynInfoList), &pMsg->DbNode);
    pMsg->u4DataLen = u4OffSet;
    Bgp4RedSyncDynInfo ();
    KW_FALSEPOSITIVE_FIX1 (pMsg);
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Exiting Bgp4RedSyncBgpPeerInfoBulkMsg\r\n");
    return BGP4_SUCCESS;

}

/*************************************************************************/
/* Function Name      : Bgp4RedHandleGoStandby                           */
/* Description        : This function is invoked by the BGP upon         */
/*                      receiving the GO_STANDBY indication from RM      */
/*                      module. And this function responds to RM with an */
/*                      acknowledgement.                                 */
/* Input(s)           : None                                             */
/* Output(s)          : None                                             */
/* Returns            : None                                             */
/*************************************************************************/
PUBLIC VOID
Bgp4RedHandleGoStandby (VOID)
{
    tRmProtoEvt         ProtoEvt;
    INT4                i4GlobalAdmin = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4Context = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC, BGP4_MOD_NAME,
              "\tProcessing GO_STANDBY event\n");

    /* Bulk update status is set to not started. */
    BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_NOT_STARTED;

    if (BGP4_GET_NODE_STATUS == RM_STANDBY)
    {
        /* Go standby received in the standby node, so ignore */
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tGO_STANDBY event reached in " "Standby node\n");
        return;
    }
    if (BGP4_GET_NODE_STATUS == RM_INIT)
    {
        /* GO_STANDBY event is not processed here. It is done when
         * CONFIG_RESTORE_COMPLETE event is received. Since static bulk
         * update will be completed only by then, the acknowledgement can be
         * sent during CONFIG_RESTORE_COMPLETE handling, which will trigger RM
         * to send dynamic bulk update event to modules.
         */
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tGO_STANDBY event reached in "
                  "Idle node. Idle to Standby state transition will occur "
                  "after receiving CONFIG_RESTORE_COMPLETE event\n");
        return;
    }
    if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
    {
        /* Go_Standby is received in an Active node, disable the module and 
         * re-enable it again */
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                  "\tActive to Standby transition\n");
        BGP4_PREV_RED_NODE_STATUS = BGP4_GET_NODE_STATUS;
        BGP4_GET_NODE_STATUS = RM_STANDBY;
        i4RetVal = BgpGetFirstActiveContextID (&u4Context);
        i4GlobalAdmin = BGP4_LOCAL_ADMIN_STATUS (u4Context);
        while (i4RetVal == SNMP_SUCCESS && i4GlobalAdmin == BGP4_ADMIN_UP)
        {
            /* This can happen in consecutive redundancy swithover,if we 
               Gr restart is still in progress,throw an error to the user */
            if (Bgp4GRCheckRestartMode (u4Context) == BGP4_RESTARTING_MODE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                          "\tActive switchover triggered while BGP Graceful restart is "
                          "still in progress.Please execute \"clear ip bgp *\" once Active "
                          "card comes up to avoid HA sync issues.\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, BGP4_SYSLOG_ID,
                              "Active switchover triggered while BGP Graceful restart is"
                              " still in progress.Please execute \"clear ip bgp *\" once Active"
                              " card  comes up to avoid HA sync issues."));
            }
            gi1SyncStatus = SYNC_IN_PROGRESS;
            gi1RFDSyncStatus = SYNC_IN_PROGRESS;
            Bgp4GlobalAdminStatusHandler (u4Context, BGP4_ADMIN_DOWN);
            gi1SyncStatus = 0;
            gi1RFDSyncStatus = 0;

            if (Bgp4GlobalAdminStatusHandler (u4Context, BGP4_ADMIN_UP) ==
                BGP4_FAILURE)
            {
                return;
            }
            i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
            i4GlobalAdmin = BGP4_LOCAL_ADMIN_STATUS (u4Context);
        }
        BGP4_RED_STANDBY_NODES = 0;
        ProtoEvt.u4AppId = RM_BGP_APP_ID;
        ProtoEvt.u4Error = RM_NONE;
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/*************************************************************************/
/* Function Name      : Bgp4RedProcessPeerMsgAtActive                    */
/* Description        : This function is invoked to process the RM msgs  */
/*                      (Bulk request msg) at the active node.           */
/* Input(s)           : None                                             */
/* Output(s)          : None                                             */
/* Returns            : None                                             */
/*************************************************************************/
PUBLIC VOID
Bgp4RedProcessPeerMsgAtActive (tRmMsg * pMsg)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;
    UINT1               u1MsgType = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_BGP_APP_ID;

    BGP4_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);
    BGP4_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

    if (u2Length != BGP4_RED_BULK_REQ_MSG_SIZE)
    {
        /* The only RM message processed in Active in BULK REQUEST Message
         * If length does not match, sent failure event to RM */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    if (u1MsgType == BGP4_RED_BULK_REQ_MESSAGE)
    {
        BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_NOT_STARTED;
        if (BGP4_IS_STANDBY_UP () == OSIX_FALSE)
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event. */
            BGP4_RED_BULK_REQ_RCVD = OSIX_TRUE;

            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC, BGP4_MOD_NAME,
                      "\tBulk request message "
                      "received before RM_STANDBY_UP. Return without sending updates\n");
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, BGP4_SYSLOG_ID,
                          "Bgp4RedProcessPeerMsgAtActive: Bulk request message "
                          "received before RM_STANDBY_UP. Return without sending updates\n"));
            return;
        }
        Bgp4RedSendBulkUpdMsg ();
    }
    return;
}

/*************************************************************************/
/* Function Name      : Bgp4RedProcessPeerMsgAtStandby                   */
/* Description        : This function is invoked to process the RM msgs  */
/*                      (Bulk update, update, tail messages) in the      */
/*                      Standby node                                     */
/* Input(s)           : None                                             */
/* Output(s)          : None                                             */
/* Returns            : None                                             */
/*************************************************************************/
PUBLIC VOID
Bgp4RedProcessPeerMsgAtStandby (tRmMsg * pMsg)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;
    UINT1               u1MsgType = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_BGP_APP_ID;

    BGP4_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);
    BGP4_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

    if (u2Length < BGP4_RED_MIN_MSG_SIZE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC | BGP4_ALL_FAILURE_TRC,
                  BGP4_MOD_NAME,
                  "\tRM message length less " "that minimum message length\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, BGP4_SYSLOG_ID,
                      "Bgp4RedProcessPeerMsgAtStandby: RM message length less "
                      "that minimum message length\n"));
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    switch (u1MsgType)
    {
        case BGP4_RED_BULK_TAIL_MESSAGE:
            ProtoEvt.u4Error = RM_NONE;
            ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
            RmApiHandleProtocolEvent (&ProtoEvt);
            break;
        case BGP4_RED_BULK_UPD_PEER_MESSAGE:
            Bgp4RedProcessPeerDampMsg (pMsg, &u4OffSet, u2Length);
            break;
        case BGP4_RED_BULK_UPD_ROUTE_MESSAGE:
            Bgp4RedProcessBulkRouteSyncMsg (pMsg, &u4OffSet, u2Length);
            break;
        case BGP4_RED_UPD_ROUTE_MESSAGE:
            Bgp4RedProcessRouteSyncMsg (pMsg, &u4OffSet, u1MsgType, u2Length);
            break;
        case BGP4_RED_UPD_ROUTE_FLAG_MSG:
            Bgp4RedProcessRouteSyncMsg (pMsg, &u4OffSet, u1MsgType, u2Length);
            break;
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
        case BGP4_RED_UPD_VPLSINSTANCE_MESSAGE:
            Bgp4RedProcessVplsInfoSyncMsg (pMsg, &u4OffSet, u2Length);
            break;
        case BGP4_RED_BULK_UPD_VPLS_INST_MESSAGE:
            Bgp4RedProcessVplsInfoBulkMsg (pMsg, &u4OffSet, u2Length);
            break;
#endif
#endif
        case BGP4_RED_BULK_UPD_BGP_ID:
        case BGP4_RED_UPD_BGP_ID:
            Bgp4RedProcessBgpIDSyncMsg (pMsg, &u4OffSet, u2Length);
            break;
        case BGP4_RED_BULK_UPD_RESTART_REASON:
        case BGP4_RED_UPD_RESTART_REASON:
            Bgp4RedProcessRestartReasonSyncMsg (pMsg, &u4OffSet, u2Length);
            break;
        case BGP4_RED_BULK_UPD_TABLE_VERSION:
        case BGP4_RED_UPD_TABLE_VERSION:
            Bgp4RedProcessTableVersionSyncMsg (pMsg, &u4OffSet, u2Length);
            break;
        case BGP4_RED_BULK_UPD_BGP_PEER_INFO:
            Bgp4RedProcessBgpPeerInfoSyncMsg (pMsg, &u4OffSet, u2Length);
            break;
        case BGP4_RED_UPD_BGP_PEER_INFO:
            Bgp4RedProcessBgpPeerInfoDynamicSyncMsg (pMsg, &u4OffSet, u2Length);
            break;
        case BGP4_RED_UPD_MPATH_OPER_CNT:
            Bgp4RedProcessMpathOperCntSyncMsg (pMsg, &u4OffSet, u2Length);
            break;
        case BGP4_RED_BULK_UPD_PEER_MPCAP_V4_INFO:
            Bgp4RedProcessBgpPeerMPCapV4SyncMsg (pMsg, &u4OffSet, u2Length);
            break;
        case BGP4_RED_BULK_UPD_PEER_MPCAP_V6_INFO:
            Bgp4RedProcessBgpPeerMPCapV6SyncMsg (pMsg, &u4OffSet, u2Length);
            break;
        case BGP4_RED_BULK_BGP_ADMIN_STATUS:
            Bgp4RedProcessBgpAdminStatusSyncMsg (pMsg, &u4OffSet, u2Length);
            break;
        case BGP4_RED_BULK_BGP_RRD_MASK:
            Bgp4RedProcessRRDMetricMaskSyncMsg (pMsg, &u4OffSet, u2Length);
            break;
        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG,
                      BGP4_EVENTS_TRC | BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                      "\tInvalid RM message received\n");
            return;
    }
    return;
}

/*****************************************************************************/
/*    Function Name       : Bgp4RedAddTblNode                                */
/*    Description         : This function will check the SyncUpdate flag to  */
/*                           determine whether the given msg can be added to */
/*                           DbList or not. When the SyncUpdate is zero the  */
/*                           the route is added to DbList to sync to standby.*/
/*                           Else the message is skipped till SyncUpdate is  */
/*                           decremented to zero.                            */
/*    Input(s)            : pMsg - Bgp RM message                            */
/*    Output(s)           : None                                             */
/*****************************************************************************/
VOID
Bgp4RedAddTblNode (tBgpRmMsg * pMsg)
{
    if (BGP4_RED_SYNC_UPDATES != BGP4_RED_CONT_SYNC_UPDATES)
    {
        RM_FREE (pMsg->pBuf);
        MemReleaseMemBlock (BGP4_RED_MSG_MEMPOOL_ID, (UINT1 *) pMsg);
        BGP4_RED_SYNC_UPDATES--;
    }
    else
    {
        DbUtilAddTblNode (&(gBgpNode.DynInfoList), &pMsg->DbNode);
    }
}

/*****************************************************************************/
/*    Function Name       : Bgp4RedVerifyAndSendBulkRequest                  */
/*    Description         : This function will check if BGP in all the VRs   */
/*                           are admin up and then sends the Bulk Request to */
/*                           the Active node.                                */
/*    Input(s)            : None                                             */
/*    Output(s)           : None                                             */
/*****************************************************************************/
VOID
Bgp4RedVerifyAndSendBulkRequest ()
{
    if (BGP4_RED_INIT_BULK_STATUS == BGP4_TRUE)
    {
        if (Bgp4VerifyGlobalState () == BGP4_TRUE)
        {
            Bgp4RedSendBulkRequestOrTailMsg (BGP4_RED_BULK_REQ_MESSAGE);
            BGP4_RED_INIT_BULK_STATUS = BGP4_FALSE;
        }
    }
    return;
}

/************************************************************************/
/* Function Name      : Bgp4RedSyncMpathOperCnt                         */
/* Description        : This function is invoked by the BGP module to   */
/*                      send dynamic updates of Mpath Oper count        */
/* Input(s)           : u4Context , u4BgpId                             */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Bgp4RedSyncMpathOperCnt (UINT4 u4Context, UINT4 u4OperIbgpCnt,
                         UINT4 u4OperEbgpCnt, UINT4 U4OperEIbgpCnt)
{
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    INT4                u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;

    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_MPATH_OPER_CNT, &u4OffSet)
        == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }

    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, u4Context);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, u4OperIbgpCnt);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, u4OperEbgpCnt);
    BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, U4OperEIbgpCnt);
    BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
    pMsg->u4DataLen = u4OffSet;
    Bgp4RedAddTblNode (pMsg);
    Bgp4RedSyncDynInfo ();
    return BGP4_SUCCESS;
}

 /*****************************************************************************/
/*    Function Name       : Bgp4RedSyncBgpPeerMPCapV4BulkMsg                   */
/*    Description         : This function will update the BGP identifier     */
/*                           and send information in the bulk message        */
/*    Input(s)            : tBgpRmMsg , pu4Offset                            */
/*    Output(s)           : None                                             */
/*    Returns             : BGP4_SUCCESS                                     */
/*****************************************************************************/
INT4
Bgp4RedSyncBgpPeerMPCapV4BulkMsg (tBgpRmMsg * pMsg, UINT4 *pu4Offset)
{
    tBgp4PeerEntry     *pPeer = NULL;
    UINT4               u4Context = 0;
    UINT4               u4PeerCount = 0;
    UINT4               u4OffSet = *pu4Offset;
    UINT4               u4LenOffSet = 0;
    INT4                i4RetVal = BGP4_FAILURE;
    tSupCapsInfo       *pCapsInfo = NULL;
    UINT1               u1MPCapFlag = BGP4_FALSE;
    tSNMP_OCTET_STRING_TYPE CapValue;
    UINT1               u1ResField = 0;
    UINT1               au1MpcapOctetList[BGP4_MPE_MP_CAP_LEN];

    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedSyncBgpPeerMPCapV4BulkMsg\r\n");
    i4RetVal = BgpGetFirstActiveContextID (&u4Context);
    while (i4RetVal == SNMP_SUCCESS)
    {
        BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, u4Context);
#ifndef VRF_WANTED
        if (u4Context != BGP_DEF_CONTEXTID)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                      BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                      "\tInvalid context\r\n");
            return BGP4_FAILURE;
        }
#endif
        u4PeerCount = TMO_SLL_Count (BGP4_PEERENTRY_HEAD (u4Context));
        BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, u4PeerCount);
        TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
        {
            if (pPeer != NULL)
            {
                Bgp4RedFillBufAddr (BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                                    pMsg->pBuf, &u4OffSet);

                MEMSET (au1MpcapOctetList, 0, BGP4_MPE_MP_CAP_LEN);
                CapValue.pu1_OctetList = au1MpcapOctetList;
                PTR_ASSIGN2 (CapValue.pu1_OctetList, BGP4_INET_AFI_IPV4);
                *((CapValue.pu1_OctetList) + BGP4_MPE_ADDRESS_FAMILY_LEN)
                    = u1ResField;
                *((CapValue.pu1_OctetList) + BGP4_MPE_ADDRESS_FAMILY_LEN
                  + BGP4_MPE_MP_CAP_RESERVED_FIELD_LEN) =
BGP4_INET_SAFI_UNICAST;

                CapValue.i4_Length = BGP4_MPE_MP_CAP_LEN;

                GetFindSpkrSupCap (pPeer,
                                   CAP_CODE_MP_EXTN,
                                   CAP_MP_CAP_LENGTH,
                                   &CapValue, CAPS_SUP, &pCapsInfo);

                if (pCapsInfo == NULL)
                {
                    u1MPCapFlag = BGP4_FALSE;
                    BGP4_RM_PUT_1_BYTE (pMsg->pBuf, &u4OffSet, u1MPCapFlag);
                }
                else
                {
                    u1MPCapFlag = BGP4_TRUE;
                    BGP4_RM_PUT_1_BYTE (pMsg->pBuf, &u4OffSet, u1MPCapFlag);
                    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet,
                                        pCapsInfo, sizeof (tSupCapsInfo));
                }
            }
        }
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
    }
    u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
    BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
    DbUtilAddTblNode (&(gBgpNode.DynInfoList), &pMsg->DbNode);
    pMsg->u4DataLen = u4OffSet;
    Bgp4RedSyncDynInfo ();
    return BGP4_SUCCESS;
}

 /*****************************************************************************/
/*    Function Name       : Bgp4RedSyncBgpPeerMPCapV6BulkMsg                   */
/*    Description         : This function will update the BGP identifier     */
/*                           and send information in the bulk message        */
/*    Input(s)            : tBgpRmMsg , pu4Offset                            */
/*    Output(s)           : None                                             */
/*    Returns             : BGP4_SUCCESS                                     */
/*****************************************************************************/
INT4
Bgp4RedSyncBgpPeerMPCapV6BulkMsg (tBgpRmMsg * pMsg, UINT4 *pu4Offset)
{
    tBgp4PeerEntry     *pPeer = NULL;
    UINT4               u4Context = 0;
    UINT4               u4PeerCount = 0;
    UINT4               u4OffSet = *pu4Offset;
    UINT4               u4LenOffSet = 0;
    INT4                i4RetVal = BGP4_FAILURE;
    tSupCapsInfo       *pCapsInfo = NULL;
    UINT1               u1MPCapFlag = BGP4_FALSE;
    tSNMP_OCTET_STRING_TYPE CapValue;
    UINT1               u1ResField = 0;
    UINT1               au1MpcapOctetList[BGP4_MPE_MP_CAP_LEN];

    BGP4_TRC (NULL, BGP4_TRC_FLAG,
              BGP4_MGMT_TRC, BGP4_MOD_NAME,
              "Entering Bgp4RedSyncBgpPeerMPCapV6BulkMsg\r\n");
    i4RetVal = BgpGetFirstActiveContextID (&u4Context);
    while (i4RetVal == SNMP_SUCCESS)
    {
        BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, u4Context);
#ifndef VRF_WANTED
        if (u4Context != BGP_DEF_CONTEXTID)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_EVENTS_TRC |
                      BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                      "\tInvalid context\r\n");
            return BGP4_FAILURE;
        }
#endif
        u4PeerCount = TMO_SLL_Count (BGP4_PEERENTRY_HEAD (u4Context));
        BGP4_RM_PUT_4_BYTE (pMsg->pBuf, &u4OffSet, u4PeerCount);
        TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (u4Context), pPeer, tBgp4PeerEntry *)
        {
            if (pPeer != NULL)
            {
                Bgp4RedFillBufAddr (BGP4_PEER_REMOTE_ADDR_INFO (pPeer),
                                    pMsg->pBuf, &u4OffSet);

                MEMSET (au1MpcapOctetList, 0, BGP4_MPE_MP_CAP_LEN);
                CapValue.pu1_OctetList = au1MpcapOctetList;
                PTR_ASSIGN2 (CapValue.pu1_OctetList, BGP4_INET_AFI_IPV6);
                *((CapValue.pu1_OctetList) + BGP4_MPE_ADDRESS_FAMILY_LEN)
                    = u1ResField;
                *((CapValue.pu1_OctetList) + BGP4_MPE_ADDRESS_FAMILY_LEN
                  + BGP4_MPE_MP_CAP_RESERVED_FIELD_LEN) =
BGP4_INET_SAFI_UNICAST;

                CapValue.i4_Length = BGP4_MPE_MP_CAP_LEN;

                GetFindSpkrSupCap (pPeer,
                                   CAP_CODE_MP_EXTN,
                                   CAP_MP_CAP_LENGTH,
                                   &CapValue, CAPS_SUP, &pCapsInfo);

                if (pCapsInfo == NULL)
                {
                    u1MPCapFlag = BGP4_FALSE;
                    BGP4_RM_PUT_1_BYTE (pMsg->pBuf, &u4OffSet, u1MPCapFlag);
                }
                else
                {
                    u1MPCapFlag = BGP4_TRUE;
                    BGP4_RM_PUT_1_BYTE (pMsg->pBuf, &u4OffSet, u1MPCapFlag);
                    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, &u4OffSet,
                                        pCapsInfo, sizeof (tSupCapsInfo));
                }
            }
        }
        i4RetVal = BgpGetNextActiveContextID (u4Context, &u4Context);
    }
    u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
    BGP4_RM_PUT_2_BYTE (pMsg->pBuf, &u4LenOffSet, u4OffSet);
    DbUtilAddTblNode (&(gBgpNode.DynInfoList), &pMsg->DbNode);
    pMsg->u4DataLen = u4OffSet;
    Bgp4RedSyncDynInfo ();
    return BGP4_SUCCESS;
}

#else /* L2RED_WANTED */
/************************************************************************/
/* Function Name      : Bgp4RedInitGlobalInfo                           */
/* Description        : This function will be invoked by the BGP4       */
/*                      to register with RM and initialize RM queue     */
/* Input(s)           : None                                            */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                     */
/************************************************************************/
PUBLIC INT4
Bgp4RedInitGlobalInfo (VOID)
{
    BGP4_GET_NODE_STATUS = RM_ACTIVE;
    BGP4_PREV_RED_NODE_STATUS = RM_INIT;
    BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_NOT_STARTED;
    BGP4_RED_INIT_BULK_STATUS = BGP4_FALSE;
    return BGP4_SUCCESS;
}

/************************************************************************/
/* Function Name      : Bgp4RedDeInitGlobalInfo                         */
/* Description        : This function will be invoked by the BGP4       */
/*                      to de-register with RM and delete RM queue      */
/* Input(s)           : None                                            */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                     */
/************************************************************************/
PUBLIC INT4
Bgp4RedDeInitGlobalInfo (VOID)
{
    return BGP4_SUCCESS;
}

/************************************************************************/
/* Function Name      : Bgp4RedHandleRmEvents                           */
/* Description        : This function is invoked by the BGP module to   */
/*                      process all the events and messages posted by   */
/*                      the RM module                                   */
/* Input(s)           : None                                            */
/* Output(s)          : None                                            */
/* Returns            : None                                            */
/************************************************************************/
PUBLIC VOID
Bgp4RedHandleRmEvents (UINT1 u1Flag)
{
    UNUSED_PARAM (u1Flag);
    return;
}

/*****************************************************************************/
/*    Function Name       : Bgp4RedSyncPeerDampMsg                           */
/*    Description         : This function will send the peer damp info       */
/*                          in a dynamic bulk update message                 */
/*    Input(s)            : u4Context - Context in which peer is present     */
/*                          pPeerDampHist - Peer damp Entry                  */
/*    Output(s)           : None                                             */
/*    Returns             : BGP4_SUCCESS/BGP4_FAILURE                        */
/*****************************************************************************/
PUBLIC INT4
Bgp4RedSyncPeerDampMsg (UINT4 u4Context, tPeerDampHist * pPeerHist)
{
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (pPeerHist);

    return BGP4_SUCCESS;
}

/************************************************************************/
/* Function Name      : Bgp4RedSendBulkRouteUpdMsg                      */
/* Description        : This function sends the dynamic bulk update of  */
/*                       the routes available in the RIB                */
/* Input(s)           : None                                            */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS/BGP4_FAILURE/BGP4_RELINQUISH       */
/************************************************************************/
PUBLIC INT4
Bgp4RedSendBulkUpdMsg (VOID)
{
    return BGP4_SUCCESS;
}

/************************************************************************/
/* Function Name      : Bgp4RedSyncRouteInfo                            */
/* Description        : This function is invoked by the BGP module to   */
/*                      send dynamic updates for route addition/deletion*/
/*                      to the RIB                                      */
/* Input(s)           : pRtProfile - Route information                  */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                     */
/************************************************************************/
PUBLIC INT4
Bgp4RedSyncRouteInfo (tRouteProfile * pRtProfile)
{
    UNUSED_PARAM (pRtProfile);
    return BGP4_SUCCESS;
}

/************************************************************************/
/* Function Name      : Bgp4RedSyncBgpPeerInfo                                */
/* Description        : This function is invoked by the BGP module to   */
/*                      send dynamic updates of bgp peer local address          */
/* Input(s)           : u4Context , Bgp4PeerEntry                             */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Bgp4RedSyncBgpPeerInfo (UINT4 u4Context, tBgp4PeerEntry * pPeer)
{
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (pPeer);
    return BGP4_SUCCESS;
}

/************************************************************************/
/* Function Name      : Bgp4RedSyncBgpId                                */
/* Description        : This function is invoked by the BGP module to   */
/*                      send dynamic updates of bgp Identifier          */
/* Input(s)           : u4Context , u4BgpId                             */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                     */
/************************************************************************/
PUBLIC INT4
Bgp4RedSyncBgpId (UINT4 u4Context, UINT4 u4BgpId)
{
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (u4BgpId);
    return BGP4_SUCCESS;
}

/************************************************************************/
/* Function Name      : Bgp4RedSyncRestartReason                        */
/* Description        : This function is invoked by the BGP module to   */
/*                      send dynamic updates of BGP GR Restart Reason   */
/* Input(s)           : u1RestartReason                                 */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                     */
/************************************************************************/
PUBLIC INT4
Bgp4RedSyncRestartReason (UINT1 u1RestartReason)
{
    UNUSED_PARAM (u1RestartReason);
    return BGP4_SUCCESS;
}

/************************************************************************/
/* Function Name      : Bgp4RedSyncTableVersion                         */
/* Description        : This function is invoked by the BGP module to   */
/*                      send dynamic updates of bgp Table Version       */
/* Input(s)           : u4Context , u4TableVersion                      */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                     */
/************************************************************************/
PUBLIC INT4
Bgp4RedSyncTableVersion (UINT4 u4Context, UINT4 u4TableVersion)
{
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (u4TableVersion);
    return BGP4_SUCCESS;
}

/************************************************************************/
/* Function Name      : Bgp4RedSyncRouteUpdToFIB                        */
/* Description        : This function is invoked by the BGP module to   */
/*                      send dynamic updates for routes added is        */
/*                      selected as best route and added to IP FIB table*/
/* Input(s)           : pRtProfile - Route information                  */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                     */
/************************************************************************/
PUBLIC INT4
Bgp4RedSyncRouteUpdToFIB (tRouteProfile * pRtProfile)
{
    UNUSED_PARAM (pRtProfile);
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : Bgp4RedVerifyAndSendBulkRequest                  */
/*    Description         : This function will check if BGP in all the VRs   */
/*                           are admin up and then sends the Bulk Request to */
/*                           the Active node.                                */
/*    Input(s)            : None                                             */
/*    Output(s)           : None                                             */
/*****************************************************************************/
VOID
Bgp4RedVerifyAndSendBulkRequest ()
{
    return;
}

INT4
Bgp4RedSyncRRDMetricMaskBulkMsg (tBgpRmMsg * pMsg, UINT4 *pu4Offset,
                                 UINT4 u4CxtId)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4Offset);
    UNUSED_PARAM (u4CxtId);
    return BGP4_SUCCESS;
}

PUBLIC INT4
Bgp4RedSyncMpathOperCnt (UINT4 u4Context, UINT4 u4OperIbgpCnt,
                         UINT4 u4OperEbgpCnt, UINT4 U4OperEIbgpCnt)
{
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (u4OperIbgpCnt);
    UNUSED_PARAM (u4OperEbgpCnt);
    UNUSED_PARAM (U4OperEIbgpCnt);
    return BGP4_SUCCESS;
}

INT4
Bgp4RedGetBgpRmMsg (tBgpRmMsg ** pMsg, UINT1 u1Message, UINT4 *pu4OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u1Message);
    UNUSED_PARAM (pu4OffSet);
    return BGP4_SUCCESS;
}

INT4
Bgp4RedSyncBgpAdminStatusBulkMsg (tBgpRmMsg * pMsg, UINT4 *pu4Offset,
                                  INT1 i1SyncStatus)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4Offset);
    UNUSED_PARAM (i1SyncStatus);
    return BGP4_SUCCESS;
}
#endif /* L2RED_WANTED */
#endif
