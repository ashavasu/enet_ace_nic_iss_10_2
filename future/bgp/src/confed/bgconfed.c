/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgconfed.c,v 1.11 2016/12/27 12:35:57 siva Exp $
 *
 * Description: This file contains functions related to AS
 *              Confederation feature.
 *
 *******************************************************************/
#ifndef _BGCONFED_C
#define _BGCONFED_C

#include "bgp4com.h"

/******************************************************************************/
/* Function Name   : Bgp4MhGetMyASField                                       */
/* Description     : Determines whether the peer is Confed-peer (the peer     */
/*                   belong to neighbouring AS, which is part of the same     */
/*                   confederation) or internal peer or external peer.        */
/*                   Depending on the type of peer, it returns data to be     */
/*                   filled in "my AS" field.                                 */
/*                   If Confederation exists and peer belongs to neighboring  */
/*                   AS outside the Confederation, fill Confed-Id in "my AS"  */
/*                   Field.                                                   */
/*                   Else Fill Member-AS number in the "my AS" Field.         */
/* Input(s)        : pPeerInfo - pointer to the peer entry to whom the open   */
/*                               message has to be sent.                      */
/* Output(s)       : None.                                                    */
/* Global Variables Referred : BGP4_CONFED_ID and BGP4_LOCAL_AS_NO            */
/* Global variables Modified : None                                           */
/* Exceptions or Operating System Error Handling : None                       */
/* Use of Recursion: None.                                                    */
/* Returns         : ASNo - AS number to be filled in "my AS" field of the    */
/*                          open message.                                     */
/******************************************************************************/
UINT2
Bgp4MhGetMyASField (const tBgp4PeerEntry * pPeerInfo)
{
    UINT4               u4MyAS;
    /* 
     * if Confed exist and Peer belongs to neighboring AS outside the Confed, 
     * Fill CONFED_ID in ASField 
     */
    if ((BGP4_CONFED_ID (BGP4_PEER_CXT_ID (pPeerInfo)) != BGP4_INV_AS) &&
        (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerInfo), pPeerInfo) ==
         BGP4_EXTERNAL_PEER)
        && (BGP4_CONFED_PEER_STATUS (pPeerInfo) == BGP4_FALSE))
    {
        u4MyAS = BGP4_CONFED_ID (BGP4_PEER_CXT_ID (pPeerInfo));
    }
    else
    {
        u4MyAS = BGP4_PEER_LOCAL_AS (pPeerInfo);
    }

    if (u4MyAS > BGP4_MAX_TWO_BYTE_AS)
    {
        u4MyAS = BGP4_AS_TRANS;
    }
    return ((UINT2) u4MyAS);

}

/******************************************************************************/
/* Function Name   : Bgp4MhCheckASPathRoutingLoop                             */
/* Description     : This function reports the presence of any routing loop.  */
/*                   It reports routing loop:                                 */
/*                   If Confed exists and any of the AS_SET/ AS_SEQUENCE      */
/*                   segments has own Confed-Id.                              */
/*                   If no Confed exists and any of the AS_SET/ AS_SEQUENCE   */
/*                   segments has own AS number.                              */
/*                   If any of the AS_CONFED type of segments has own         */
/*                   Member-AS number.                                        */
/* Input(s)        : PtsNewRoutes - List of feasible route profiles containing*/
/*                                  the AS path information.                  */
/* Output(s)       : None.                                                    */
/* Global Variables Referred :BGP4_CONFED_ID(BGP4_DFLT_VRFID) and 
 *                              BGP4_LOCAL_AS_NO(BGP4_DFLT_VRFID)             */
/* Global variables Modified :None                                            */
/* Exceptions or Operating System Error Handling : None                       */
/* Use of Recursion: None.                                                    */
/* REturns         : RtLoopCheck - Routing loop status. Returns BGP4_FAILURE, */
/*                          if AS path routing loop found, else BGP4_SUCCESS. */
/******************************************************************************/
INT4
Bgp4MhCheckASPathRoutingLoop (const tTMO_SLL * PtsNewroutes)
{
    tLinkNode          *pLknode = NULL;
    tAsPath            *pASSeg = NULL;
    tTMO_SLL           *pASList = NULL;
    UINT4               u4Context = 0;
    UINT4               u4PeerLocalAs = 0;
    UINT1               u1LocalAs = 0;

    pLknode = (tLinkNode *) TMO_SLL_First (PtsNewroutes);
    if (pLknode == NULL)
    {
        return (BGP4_SUCCESS);
    }
    pASList = &(((tBgp4Info *) (pLknode->pRouteProfile->pRtInfo))->TSASPath);
    u4Context = BGP4_RT_CXT_ID (pLknode->pRouteProfile);
    u4PeerLocalAs = BGP4_PEER_LOCAL_AS (pLknode->pRouteProfile->pPEPeer);

    if (BGP4_PEER_LOCAL_AS_CFG(pLknode->pRouteProfile->pPEPeer) == BGP4_TRUE)
    {
        u1LocalAs = BGP4_TRUE;
    }

    TMO_SLL_Scan (pASList, pASSeg, tAsPath *)
    {
        UINT4              *pASNo = NULL;
        UINT4               u4ASNo;
        UINT4               u4Indx;

        pASNo = (UINT4 *) (VOID *) pASSeg->au1ASSegs;

        switch (BGP4_ASPATH_TYPE (pASSeg))
        {
            case BGP4_ATTR_PATH_SET:
                /*fall through */

            case BGP4_ATTR_PATH_SEQUENCE:
                if (BGP4_CONFED_ID (u4Context) == BGP4_INV_AS)
                {
                    for (u4Indx = 0; u4Indx < BGP4_ASPATH_LEN (pASSeg);
                         u4Indx++)
                    {
                        PTR_FETCH4 (u4ASNo, (pASNo + u4Indx));
                        if ((u4Indx == 0) &&(u1LocalAs ==BGP4_TRUE))
                        {
                            continue;
                        }
                        if (u4ASNo == u4PeerLocalAs)
                        {
                            return (BGP4_FAILURE);
                        }
                    }
                }
                else
                {
                    for (u4Indx = 0; u4Indx < BGP4_ASPATH_LEN (pASSeg);
                         u4Indx++)
                    {
                        PTR_FETCH4 (u4ASNo, (pASNo + u4Indx));
                        if (u4ASNo == BGP4_CONFED_ID (u4Context))
                        {
                            return (BGP4_FAILURE);
                        }
                    }
                }
                break;

            case BGP4_ATTR_PATH_CONFED_SET:
                /*fall through */

            case BGP4_ATTR_PATH_CONFED_SEQUENCE:
                for (u4Indx = 0; u4Indx < BGP4_ASPATH_LEN (pASSeg); u4Indx++)
                {
                    PTR_FETCH4 (u4ASNo, (pASNo + u4Indx));
                    if ((u4Indx == 0) &&(u1LocalAs ==BGP4_TRUE))
                    {
                        continue;
                    }
                    if (u4ASNo == u4PeerLocalAs)
                    {
                        return (BGP4_FAILURE);
                    }
                }
                break;

            default:
                break;
        }
    }                            /*end for */
    return (BGP4_SUCCESS);
}

/******************************************************************************/
/* Function Name   : Bgp4AttrAspathLength                                     */
/* Description     : This function calculates the length of AS path attribute,*/
/*                   which will be used in decision process.                  */
/*                   Length of an AS_SET segment is considered 1,  no matter  */
/*                   how many ASs are in the segment.                         */
/*                   Length of an AS_SEQUENCE segment is same as number of    */
/*                   ASs present in the segment                               */
/*                   Length of any AS_CONFED type segment is considered 0,    */
/*                   no matter how many ASs are in the segment                */
/* Input(s)        : pASPathInfo - List of AS path segments.                  */
/* Output(s)       : None.                                                    */
/* Global Variables Referred :None                                            */
/* Global variables Modified :None                                            */
/* Exceptions or Operating System Error Handling : None                       */
/* Use of Recursion: None.                                                    */
/* Returns         :  ASPathLen - Length of AS path attribute.                */
/******************************************************************************/
UINT2
Bgp4AttrAspathLength (const tTMO_SLL * pTsAspath)
{
    tAsPath            *pAspath = NULL;
    UINT2               u2AspathLen = 0;

    TMO_SLL_Scan (pTsAspath, pAspath, tAsPath *)
    {
        if (BGP4_ASPATH_TYPE (pAspath) == BGP4_ATTR_PATH_SEQUENCE)
        {
            u2AspathLen += BGP4_ASPATH_LEN (pAspath);
        }
        else if (BGP4_ASPATH_TYPE (pAspath) == BGP4_ATTR_PATH_SET)
        {
            u2AspathLen++;
        }
    }
    return u2AspathLen;
}

/*****************************************************************************/
/* Function Name   : Bgp4DechCompareMED                                      */
/* Description     : This function finds out whether MED values should be    */
/*                   compared in resolving ties among candidate route or not.*/
/*                   If "Always compare MED" is enabled, compare MED.        */
/*                   If "Best Path MED Confed" is enabled, and candidate     */
/*                   routes have only AS_CONFED type of segments in AS path  */
/*                   attribute, then also MED is compared.                   */
/*                   If first entries in sanitized AS path of the candidate  */
/*                   routes are same, then also MED is compared.             */
/* Input(s)        : pPresentRtASPathInfo - List of AS path segments of the  */
/*                                      route profile already present in RIB */
/*                   pNewRtASPathInfo - List of AS path segments of the new  */
/*                                     feasible route received in the update.*/
/* Output(s)       : None.                                                   */
/* Global Variables Referred :None                                           */
/* Global variables Modified :None                                           */
/* Exceptions or Operating System Error Handling : None                      */
/* Use of Recursion: None.                                                   */
/* Returns         :  CompareMED - Hold TRUE, if MED is found to be compared */
/*                                 in decision process, else FALSE.          */
/*****************************************************************************/
UINT1
Bgp4DechCompareMED (UINT4 u4CxtId, const tTMO_SLL * pPresentRtASPath,
                    const tTMO_SLL * pNewRtASPath)
{
    tAsPath            *pAspath = NULL;
    tAsPath            *pAspathPresent = NULL;
    UINT4               u4PreRtAS = 0;    /* First AS entry in presentRtASPath */
    UINT4               u4NewRtAS = 0;    /* First AS entry in newRtASPath */

    pAspathPresent = (tAsPath *) TMO_SLL_First (pPresentRtASPath);
    pAspath = (tAsPath *) TMO_SLL_First (pNewRtASPath);

    if ((pAspathPresent == NULL) && (pAspath == NULL))
    {
        /* Incase of only iBGP peers exists in route contention,
	 * MED comparison should still happen as metric comparison */
        return BGP4_TRUE;
    }

    if (pAspathPresent == NULL)
    {
        return BGP4_FALSE;
    }

    if (pAspath == NULL)
    {
        return BGP4_FALSE;
    }

    if (BGP4_ALWAYS_COMPARE_MED (u4CxtId) == BGP4_MED_COMPARE_ENABLE)
    {
        return BGP4_TRUE;
    }

    /* 
     * If MED needs to be compared only when receivied from the peers 
     * of same AS, then the following check needs to be included here.
     */
    else
    {
        TMO_SLL_Scan (pPresentRtASPath, pAspath, tAsPath *)
        {
            if (BGP4_ASPATH_TYPE (pAspath) == BGP4_ATTR_PATH_SEQUENCE)
            {
                PTR_FETCH4 (u4PreRtAS, BGP4_ASPATH_NOS (pAspath));
                if ((BGP4_FOUR_BYTE_ASN_SUPPORT (u4CxtId) ==
                     BGP4_DISABLE_4BYTE_ASN_SUPPORT) &&
                    (u4PreRtAS > BGP4_MAX_TWO_BYTE_AS))
                {
                    u4PreRtAS = BGP4_AS_TRANS;
                }

                break;
            }
            else if (BGP4_ASPATH_TYPE (pAspath) == BGP4_ATTR_PATH_SET)
            {
                return BGP4_FALSE;
            }
            /* 
             * else: since path info is non-empty, its AS_confed segment,
             * and PreRtAS = 0, So if for loop overs and 
             * PreRtAS ==0 means there are only AS_CONFED
             * segments in the as path
             */
        }
        TMO_SLL_Scan (pNewRtASPath, pAspath, tAsPath *)
        {
            if (BGP4_ASPATH_TYPE (pAspath) == BGP4_ATTR_PATH_SEQUENCE)
            {
                PTR_FETCH4 (u4NewRtAS, BGP4_ASPATH_NOS (pAspath));
                if ((BGP4_FOUR_BYTE_ASN_SUPPORT (u4CxtId) ==
                     BGP4_DISABLE_4BYTE_ASN_SUPPORT) &&
                    (u4NewRtAS > BGP4_MAX_TWO_BYTE_AS))
                {
                    u4NewRtAS = BGP4_AS_TRANS;
                }

                break;
            }
            else if (BGP4_ASPATH_TYPE (pAspath) == BGP4_ATTR_PATH_SET)
            {
                return BGP4_FALSE;
            }
            /* 
             * else: since path info is non-empty, its AS_confed segment, 
             * and NewRtAS = 0, So if for loop overs and 
             * NewRtAS = 0 means there are only AS_Confed
             * segments in the as path
             */
        }
        if ((u4NewRtAS == 0) && (u4PreRtAS == 0))
        {
            if (BGP4_CONFED_BESTPATH_COMPARE_MED (u4CxtId) == BGP4_TRUE)
            {
                return BGP4_TRUE;
            }
        }
        else if ((u4PreRtAS == u4NewRtAS))
        {
            return BGP4_TRUE;
        }
    }
    return BGP4_FALSE;
}

/******************************************************************************/
/* Function Name   : Bgp4ConfedPreserveLP                                     */
/* Description     : This function is responsible for preserving LP attribute */
/*                   in the route propagation to confederation peers.         */
/* Input(s)        : pAdvRtProfile - Route profile to be advertised in this   */
/*                                   update                                   */
/* Output(s)       : pAdvtBgpInfo  - Updated BGP4-INFO to be advertised.      */
/* Global Variables Referred : None                                           */
/* Global variables Modified : None                                           */
/* Exceptions or Operating System Error Handling : None                       */
/* Use of Recursion: None.                                                    */
/* Returns         : BGP4_SUCCESS/BGP4_FAILURE                                */
/******************************************************************************/
INT4
Bgp4ConfedPreserveLP (const tRouteProfile * pRtProfile,
                      tBgp4Info * pAdvtBgpInfo)
{
    BGP4_INFO_RCVD_LOCAL_PREF (pAdvtBgpInfo) =
        BGP4_INFO_RCVD_LOCAL_PREF (BGP4_RT_BGP_INFO (pRtProfile));
    BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_LOCAL_PREF_MASK;
    return BGP4_SUCCESS;
}

/******************************************************************************/
/* Function Name   : Bgp4ConfedPreserveMED                                    */
/* Description     : This function is responsible for preserving MED attribute*/
/*                   in the route propagation to confederation peers.         */
/* Input(s)        : pAdvRtProfile - Route profile which is to be advertised  */
/*                                  in the update message.                    */
/* Output(s)       : pAdvtBgpInfo - Updated BGP4-INFO that is to be           */
/*                                  advertised.                               */
/* Global Variables Referred : None                                           */
/* Global variables Modified : None                                           */
/* Exceptions or Operating System Error Handling : None                       */
/* Use of Recursion: None.                                                    */
/* Returns         : BGP4_SUCCESS/BGP4_FAILURE                                */
/******************************************************************************/
INT4
Bgp4ConfedPreserveMED (const tRouteProfile * pRtProfile,
                       tBgp4Info * pAdvtBgpInfo)
{
    BGP4_INFO_RCVD_MED (pAdvtBgpInfo) =
        BGP4_INFO_RCVD_MED (BGP4_RT_BGP_INFO (pRtProfile));

    BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_MED_MASK;
    return BGP4_SUCCESS;
}

#endif /* _BGCONFED_C */
