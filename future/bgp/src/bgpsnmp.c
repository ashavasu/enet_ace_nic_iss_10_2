/*****************************************************************************/
/* $Id: bgpsnmp.c,v 1.9 2015/02/24 11:57:42 siva Exp $                       */
/* Copyright (C) 2009 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : bgpsnmp.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        :                                                */
/*    MODULE NAME           : BGP                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 22 July 2009                                   */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           :  This file contains routines for gr feature    */
/*                             to generate traps during state change         */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    22 July 2009           Initial Creation                        */
/*---------------------------------------------------------------------------*/

#ifndef BGPSNMP_C
#define BGPSNMP_C

#include "fssnmp.h"
#include "bgp4com.h"
#include "snmputil.h"
#include "stdbgp4.h"
#undef _SNMP_MIB_H
#include "fsbgp.h"
#undef _SNMP_MIB_H
#include "fsmpbgp4.h"

PRIVATE tSNMP_OID_TYPE *MakeObjIdFromDotNew (INT1 *textStr, INT1 flag);

PRIVATE INT4        ParseSubIdNew (UINT1 **pptempPtr);
PRIVATE UINT4       ga_BGP4_SNMP2_TRAP_OID[] =
    { ONE, THREE, SIX, ONE, SIX, THREE, ONE, ONE, FOUR, ONE, ZERO };
PRIVATE UINT4       ga_SNMP2_TRAP_OID[] =
    { ONE, THREE, SIX, ONE, SIX, THREE, ONE, ONE, FOUR, ONE, ZERO };

/*****************************************************************************/
/*  Function Name   :   Bgp4SnmpSendStatusChgTrap                            */
/*  Description     :   This function will do the following                  */
/*                      generate trap when the speaker enters or exits GR    */
/*  Input(s)        :   u4Trap - The trap id                                 */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
PUBLIC VOID
Bgp4SnmpSendStatusChgTrap (UINT4 u4CxtId, UINT4 u4Trap)
{
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1               au1Buf[BGP4_MAX_BUFFER_LENGTH];
    tSNMP_COUNTER64_TYPE u8CounterVal = { ZERO, ZERO };

    MEMSET (au1Buf, ZERO, BGP4_MAX_BUFFER_LENGTH);

    pEnterpriseOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pEnterpriseOid == NULL)
    {
        return;
    }

    MEMCPY (pEnterpriseOid->pu4_OidList, ga_BGP4_SNMP2_TRAP_OID,
            sizeof (ga_BGP4_SNMP2_TRAP_OID));
    pEnterpriseOid->u4_Length = pEnterpriseOid->u4_Length - TWO;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = ZERO;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u4Trap;
    pSnmpTrapOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    MEMCPY (pSnmpTrapOid->pu4_OidList, ga_SNMP2_TRAP_OID,
            SNMP_V2_TRAP_OID_LEN * sizeof (UINT4));
    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       ZERO, ZERO, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    pStartVb = pVbList;

    /* fill the Router information */
    SPRINTF ((CHR1 *) au1Buf, "fsMIBgp4ContextId");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_MI)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER32,
                                                 gBgpCxtNode[u4CxtId]->
                                                 u4ContextId, ZERO, NULL, NULL,
                                                 u8CounterVal);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    SPRINTF ((CHR1 *) au1Buf, "fsbgp4Identifier");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_FUT)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER32,
                                                 gBgpCxtNode[u4CxtId]->u4BGP4Id,
                                                 ZERO, NULL, NULL,
                                                 u8CounterVal);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    /* fill the Router information */
    SPRINTF ((CHR1 *) au1Buf, "fsbgp4GRRestartTimeInterval");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_FUT)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER32,
                                                 0,
                                                 gBgpCxtNode[u4CxtId]->
                                                 u4RestartInterval, NULL, NULL,
                                                 u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    SPRINTF ((CHR1 *) au1Buf, "fsbgp4GRMode");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_FUT)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER32,
                                                 gBgpCxtNode[u4CxtId]->
                                                 u1RestartStatus, ZERO, NULL,
                                                 NULL, u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    SPRINTF ((CHR1 *) au1Buf, "fsbgp4RestartExitReason");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_FUT)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER32,
                                                 ZERO,
                                                 gBgpCxtNode[u4CxtId]->
                                                 u1RestartExitReason, NULL,
                                                 NULL, u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
    return;
}

/*****************************************************************************/
/*  Function Name   :   Bgp4SnmpSendPeerStatusChgTrap                        */
/*  Description     :   This function will do the following                  */
/*                      generate trap when the peers restart mode changes    */
/*  Input(s)        :   pPeerentry                                           */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/

PUBLIC VOID
Bgp4SnmpSendPeerStatusChgTrap (tBgp4PeerEntry * pPeerentry, UINT4 u4Trap)
{
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1               au1Buf[BGP4_MAX_BUFFER_LENGTH];
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;

    MEMSET (au1Buf, 0, BGP4_MAX_BUFFER_LENGTH);
    pEnterpriseOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pEnterpriseOid == NULL)
    {
        return;
    }

    MEMCPY (pEnterpriseOid->pu4_OidList, ga_BGP4_SNMP2_TRAP_OID,
            sizeof (ga_BGP4_SNMP2_TRAP_OID));
    pEnterpriseOid->u4_Length = pEnterpriseOid->u4_Length - TWO;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = ZERO;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u4Trap;

    /* Allocating for SNMP Trap Oid */
    pSnmpTrapOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    MEMCPY (pSnmpTrapOid->pu4_OidList, ga_SNMP2_TRAP_OID,
            SNMP_V2_TRAP_OID_LEN * sizeof (UINT4));
    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       ZERO, ZERO, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    pStartVb = pVbList;

    /* fill the Router information */
    SPRINTF ((CHR1 *) au1Buf, "fsMIBgp4ContextId");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_MI)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
        (pOid, SNMP_DATA_TYPE_INTEGER32,
         gBgpCxtNode[BGP4_PEER_CXT_ID (pPeerentry)]->u4ContextId,
         ZERO, NULL, NULL, u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    SPRINTF ((CHR1 *) au1Buf, "fsbgp4PeerExtPeerRemoteAddr");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_FUT)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    if ((pOstring =
         (SNMP_AGT_FormOctetString (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                    BGP4_MAX_INET_ADDRESS_LEN))) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_OCTET_PRIM,
                                                 ZERO, ZERO,
                                                 pOstring, NULL, u8CounterVal);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        free_octetstring (pOstring);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    /* fill the restart information */
    SPRINTF ((CHR1 *) au1Buf, "fsbgp4GRRestartTimeInterval");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_FUT)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER32,
                                                 0, BGP4_PEER_RESTART_INTERVAL
                                                 (pPeerentry), NULL, NULL,
                                                 u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        free_octetstring (pOstring);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    SPRINTF ((CHR1 *) au1Buf, "fsbgp4RestartStatus");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_FUT)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        free_octetstring (pOstring);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER32, ZERO,
                                                 ((pPeerentry->peerStatus).
                                                  u1RestartMode), NULL, NULL,
                                                 u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        free_octetstring (pOstring);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    SPRINTF ((CHR1 *) au1Buf, "fsbgp4RestartExitReason");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_FUT)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        free_octetstring (pOstring);
        SNMP_FreeOid (pOid);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER32,
                                                 ZERO,
                                                 BGP4_GR_EXIT_NONE, NULL, NULL,
                                                 u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        free_octetstring (pOstring);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
    return;
}

/*****************************************************************************/
/*  Function Name   :   Bgp4SnmpSendRTMRouteAdditionFailureTrap              */
/*  Description     :   This function will do the following                  */
/*                      generate trap when the route addition to RTM fails   */
/*  Input(s)        :   pPeerentry                                           */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/

PUBLIC VOID
Bgp4SnmpSendRTMRouteAdditionFailureTrap (tRouteProfile * pRouteProfile,
                                         UINT4 u4Trap)
{
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1               au1Buf[BGP4_MAX_BUFFER_LENGTH];
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;

    MEMSET (au1Buf, 0, BGP4_MAX_BUFFER_LENGTH);
    pEnterpriseOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pEnterpriseOid == NULL)
    {
        return;
    }

    MEMCPY (pEnterpriseOid->pu4_OidList, ga_BGP4_SNMP2_TRAP_OID,
            sizeof (ga_BGP4_SNMP2_TRAP_OID));
    pEnterpriseOid->u4_Length = pEnterpriseOid->u4_Length - TWO;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = ZERO;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u4Trap;

    /* Allocating for SNMP Trap Oid */
    pSnmpTrapOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    MEMCPY (pSnmpTrapOid->pu4_OidList, ga_SNMP2_TRAP_OID,
            SNMP_V2_TRAP_OID_LEN * sizeof (UINT4));
    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       ZERO, ZERO, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    pStartVb = pVbList;
    /* fill the Router information */
    SPRINTF ((CHR1 *) au1Buf, "fsMIBgp4TrapContextId");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_MI)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
        (pOid, SNMP_DATA_TYPE_INTEGER32,
         pRouteProfile->pPEPeer->pBgpCxtNode->u4ContextId, ZERO, NULL, NULL,
         u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    SPRINTF ((CHR1 *) au1Buf, "fsMIBgp4Identifier");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_MI)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

/*To check.. Need to pass string*/
/*    if ((pOstring =
                 SNMP_AGT_FormOctetString (pRouteProfile->pBgpCxtNode->u4BGP4Id,
                                           sizeof(UINT4))) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }
*/
/*    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_OCTET_PRIM,
                                                 0L, 0, pOstring, NULL,
                                                 u8CounterVal);
 */

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER32,
                                                 0,
                                                 (INT4) pRouteProfile->pPEPeer->
                                                 pBgpCxtNode->u4BGP4Id, NULL,
                                                 NULL, u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;
    SPRINTF ((CHR1 *) au1Buf, "fsMIBgp4TrapRouteAddressType");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_MI)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER32,
                                                 0,
                                                 BGP4_RT_AFI_INFO
                                                 (pRouteProfile), NULL, NULL,
                                                 u8CounterVal);

    pVbList = pVbList->pNextVarBind;
    SPRINTF ((CHR1 *) au1Buf, "fsMIBgp4TrapRoutePrefix");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_MI)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    if ((pOstring =
         SNMP_AGT_FormOctetString (pRouteProfile->NetAddress.NetAddr.au1Address,
                                   pRouteProfile->NetAddress.NetAddr.
                                   u2AddressLen)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_OCTET_PRIM,
                                                 0L, 0, pOstring, NULL,
                                                 u8CounterVal);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }
    pVbList = pVbList->pNextVarBind;
    SPRINTF ((CHR1 *) au1Buf, "fsMIBgp4TrapPeerAddrType");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_MI)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    /*Need to check if peer type is required */
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER32,
                                                 0,
                                                 BGP4_RT_AFI_INFO
                                                 (pRouteProfile), NULL, NULL,
                                                 u8CounterVal);

    pVbList = pVbList->pNextVarBind;
    SPRINTF ((CHR1 *) au1Buf, "fsMIBgp4TrapPeerAddr");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_MI)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    if ((pOstring =
         SNMP_AGT_FormOctetString ((BGP4_PEER_REMOTE_ADDR_INFO
                                    (BGP4_RT_PEER_ENTRY (pRouteProfile))).
                                   au1Address,
                                   (BGP4_PEER_REMOTE_ADDR_INFO
                                    (BGP4_RT_PEER_ENTRY (pRouteProfile))).
                                   u2AddressLen)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_OCTET_PRIM,
                                                 0L, 0, pOstring, NULL,
                                                 u8CounterVal);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;
    SPRINTF ((CHR1 *) au1Buf, "fsMIBgp4mpebgp4PathAttrNextHop");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_MI)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    if ((pOstring =
         SNMP_AGT_FormOctetString (BGP4_INFO_NEXTHOP_INFO
                                   (BGP4_RT_BGP_INFO (pRouteProfile)).
                                   au1Address,
                                   BGP4_INFO_NEXTHOP_INFO (BGP4_RT_BGP_INFO
                                                           (pRouteProfile)).
                                   u2AddressLen)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_OCTET_PRIM,
                                                 0L, 0, pOstring, NULL,
                                                 u8CounterVal);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }
    pVbList = pVbList->pNextVarBind;

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
    return;

}

/******************************************************************************
* Function : MakeObjIdFromDotNew                                         *
* Input    : textStr
* Output   : NONE
* Returns  : poidPtr or NULL
*******************************************************************************/

PRIVATE tSNMP_OID_TYPE *
MakeObjIdFromDotNew (INT1 *textStr, INT1 flag)
{
    tSNMP_OID_TYPE     *poidPtr = NULL;
    INT1               *ptempPtr = NULL, *pdotPtr = NULL;
    INT2                i2Temp = 0;
    UINT2               u2dotCount = 0;
    UINT1              *pu1TmpPtr = NULL;
    UINT4               u4Len = 0;
    INT1                ai1tempBuffer[BGP4_MAX_BUFFER_LENGTH +
                                      BGP4_INCREMENT_BY_ONE];
    INT2                i2OidTableSize;
    struct MIB_OID     *mib_oid_table = NULL;

    if (flag == BGP4_MI)
    {
        mib_oid_table = fsmpbgp_orig_mib_oid_table;
        i2OidTableSize = (INT2) sizeof (fsmpbgp_orig_mib_oid_table);
    }
    else if (flag == BGP4_STD)
    {
        mib_oid_table = orig_mib_oid_table;
        i2OidTableSize = (INT2) sizeof (orig_mib_oid_table);
    }
    else
    {
        mib_oid_table = ga_orig_mib_oid_table;
        i2OidTableSize = (INT2) sizeof (ga_orig_mib_oid_table);
    }

    MEMSET (ai1tempBuffer, 0, BGP4_MAX_BUFFER_LENGTH + BGP4_INCREMENT_BY_ONE);
    /* see if there is an alpha descriptor at begining */
    if (0 != ISALPHA (*textStr))
    {
        pdotPtr = (INT1 *) STRCHR ((INT1 *) textStr, '.');

        /* if no dot, point to end of string */
        if (pdotPtr == NULL)
        {
            pdotPtr = textStr + STRLEN ((INT1 *) textStr);
        }
        ptempPtr = textStr;

        for (i2Temp = 0; ((ptempPtr < pdotPtr) &&
                          (i2Temp < BGP4_MAX_BUFFER_LENGTH)); i2Temp++)
        {
            ai1tempBuffer[i2Temp] = *ptempPtr++;
        }
        ai1tempBuffer[i2Temp] = '\0';

        for (i2Temp = 0;
             ((i2Temp < (i2OidTableSize / (INT2) sizeof (struct MIB_OID))) &&
              (mib_oid_table[i2Temp].pName) != NULL); i2Temp++)
        {
            if ((STRCMP (mib_oid_table[i2Temp].pName,
                         (INT1 *) ai1tempBuffer) == 0)
                && (STRLEN ((INT1 *) ai1tempBuffer) ==
                    STRLEN (mib_oid_table[i2Temp].pName)))
            {
                u4Len = ((STRLEN (mib_oid_table[i2Temp].pNumber) <
                          sizeof (ai1tempBuffer)) ?
                         STRLEN (mib_oid_table[i2Temp].pNumber) :
                         sizeof (ai1tempBuffer) - 1);
                STRNCPY ((INT1 *) ai1tempBuffer,
                         mib_oid_table[i2Temp].pNumber, u4Len);
                ai1tempBuffer[u4Len] = '\0';
                break;
            }
        }

        if ((i2Temp < (i2OidTableSize / (INT2) sizeof (struct MIB_OID))) &&
            (mib_oid_table[i2Temp].pName == NULL))
        {
            return (NULL);
        }

        /* now concatenate the non-alpha part to the begining */
        if (STRLEN (ai1tempBuffer) + STRLEN (pdotPtr) < sizeof (ai1tempBuffer))
        {
            STRCAT ((INT1 *) ai1tempBuffer, (INT1 *) pdotPtr);
        }
    }
    else
    {                            /* is not alpha, so just copy into ai1tempBuffer */
        STRCPY ((INT1 *) ai1tempBuffer, (INT1 *) textStr);
    }

    /* Now we've got something 
     * with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2dotCount = 0;
    for (i2Temp = 0; ((i2Temp < BGP4_MAX_BUFFER_LENGTH) &&
                      (ai1tempBuffer[i2Temp] != '\0')); i2Temp++)
    {
        if (ai1tempBuffer[i2Temp] == '.')
        {
            u2dotCount++;
        }
    }
    if ((poidPtr = alloc_oid ((INT4)
                              (u2dotCount + BGP4_INCREMENT_BY_ONE))) == NULL)
    {
        return (NULL);
    }

    /* now we convert number.number.... strings */
    pu1TmpPtr = (UINT1 *) ai1tempBuffer;
    for (i2Temp = 0; i2Temp < u2dotCount + BGP4_INCREMENT_BY_ONE; i2Temp++)
    {
        if ((poidPtr->pu4_OidList[i2Temp] =
             ((UINT4) (ParseSubIdNew (&pu1TmpPtr)))) ==
            (UINT4) -BGP4_DECREMENT_BY_ONE)
        {
            free_oid (poidPtr);
            return (NULL);
        }
        if (*pu1TmpPtr == '.')
        {
            pu1TmpPtr++;        /* to skip over dot */
        }
        else if (*pu1TmpPtr != '\0')
        {
            free_oid (poidPtr);
            return (NULL);
        }
    }                            /* end of for loop */

    return (poidPtr);
}

/******************************************************************************
* Function : ParseSubIdNew
* Input    : ptempPtr
* Output   : None
* Returns  : i4value of ptempPtr or -1
*******************************************************************************/

PRIVATE INT4
ParseSubIdNew (UINT1 **pptempPtr)
{
    INT4                i4value = 0;
    UINT1              *ptmp = NULL;

    for (ptmp = *pptempPtr; (((*ptmp >= '0') && (*ptmp <= '9')) ||
                             ((*ptmp >= 'a') && (*ptmp <= 'f')) ||
                             ((*ptmp >= 'A') && (*ptmp <= 'F'))); ptmp++)
    {
        i4value = (i4value * TEN) + (*ptmp & BGP4_MAX_SUBID);
    }

    if (*pptempPtr == ptmp)
    {
        i4value = -BGP4_DECREMENT_BY_ONE;
    }
    *pptempPtr = ptmp;
    return (i4value);
}

/*****************************************************************************/
/*  Function Name   :   Bgp4SnmpSendEstablishedStatusChgTrap                 */
/*  Description     :   This function will do the following                  */
/*                      generate trap when the speaker enters into the       */
/*                      Established state.                                   */
/*  Input(s)        :   pPeerentry - Peer Entry                              */
/*                      u4Trap - The trap id                                 */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
PUBLIC VOID
Bgp4SnmpSendEstablishedStatusChgTrap (tBgp4PeerEntry * pPeerentry, UINT4 u4Trap)
{
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1               au1Buf[BGP4_MAX_BUFFER_LENGTH];
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;

    MEMSET (au1Buf, 0, BGP4_MAX_BUFFER_LENGTH);
    pEnterpriseOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pEnterpriseOid == NULL)
    {
        return;
    }

    MEMCPY (pEnterpriseOid->pu4_OidList, ga_BGP4_SNMP2_TRAP_OID,
            sizeof (ga_BGP4_SNMP2_TRAP_OID));
    pEnterpriseOid->u4_Length = pEnterpriseOid->u4_Length - TWO;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = ZERO;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u4Trap;

    /* Allocating for SNMP Trap Oid */
    pSnmpTrapOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    MEMCPY (pSnmpTrapOid->pu4_OidList, ga_SNMP2_TRAP_OID,
            SNMP_V2_TRAP_OID_LEN * sizeof (UINT4));
    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       ZERO, ZERO, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    pStartVb = pVbList;

    /* fill the Router context information */
    SPRINTF ((CHR1 *) au1Buf, "fsMIBgp4ContextId");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_MI)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
        (pOid, SNMP_DATA_TYPE_INTEGER32,
         gBgpCxtNode[BGP4_PEER_CXT_ID (pPeerentry)]->u4ContextId,
         ZERO, NULL, NULL, u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    SPRINTF ((CHR1 *) au1Buf, "bgpPeerRemoteAddr");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_STD)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    if ((pOstring =
         (SNMP_AGT_FormOctetString (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                    BGP4_MAX_INET_ADDRESS_LEN))) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_OCTET_PRIM,
                                                 ZERO, ZERO,
                                                 pOstring, NULL, u8CounterVal);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        free_octetstring (pOstring);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    /* fill the Router information */
    SPRINTF ((CHR1 *) au1Buf, "bgpPeerRemoteAddr");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_STD)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    if ((pOstring =
         (SNMP_AGT_FormOctetString (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                    BGP4_MAX_INET_ADDRESS_LEN))) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_OCTET_PRIM,
                                                 ZERO, ZERO,
                                                 pOstring, NULL, u8CounterVal);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        free_octetstring (pOstring);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    /* fill the restart information */
    SPRINTF ((CHR1 *) au1Buf, "bgpPeerLastError");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_STD)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER32,
                                                 0, (UINT4) BGP4_PEER_LAST_ERROR
                                                 (pPeerentry), NULL, NULL,
                                                 u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        free_octetstring (pOstring);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    SPRINTF ((CHR1 *) au1Buf, "bgpPeerState");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_STD)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        free_octetstring (pOstring);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER32, ZERO,
                                                 (UINT4) BGP4_PEER_STATE
                                                 (pPeerentry), NULL, NULL,
                                                 u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        free_octetstring (pOstring);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
    return;
}

/*****************************************************************************/
/*  Function Name   :   Bgp4SnmpSendBackwardTransitionTrap                   */
/*  Description     :   This function will do the following                  */
/*                      generate trap when the speaker moves to lower states */
/*                      from its current state                               */
/*  Input(s)        :   pPeerentry - Peer Entry                              */
/*                      u4Trap - The trap id                                 */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
PUBLIC VOID
Bgp4SnmpSendBackwardTransitionTrap (tBgp4PeerEntry * pPeerentry, UINT4 u4Trap)
{
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1               au1Buf[BGP4_MAX_BUFFER_LENGTH];
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;

    MEMSET (au1Buf, 0, BGP4_MAX_BUFFER_LENGTH);
    pEnterpriseOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pEnterpriseOid == NULL)
    {
        return;
    }

    MEMCPY (pEnterpriseOid->pu4_OidList, ga_BGP4_SNMP2_TRAP_OID,
            sizeof (ga_BGP4_SNMP2_TRAP_OID));
    pEnterpriseOid->u4_Length = pEnterpriseOid->u4_Length - TWO;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = ZERO;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u4Trap;

    /* Allocating for SNMP Trap Oid */
    pSnmpTrapOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    MEMCPY (pSnmpTrapOid->pu4_OidList, ga_SNMP2_TRAP_OID,
            SNMP_V2_TRAP_OID_LEN * sizeof (UINT4));
    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       ZERO, ZERO, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    pStartVb = pVbList;

    /* fill the Router information */
    SPRINTF ((CHR1 *) au1Buf, "fsMIBgp4ContextId");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_MI)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
        (pOid, SNMP_DATA_TYPE_INTEGER32,
         gBgpCxtNode[BGP4_PEER_CXT_ID (pPeerentry)]->u4ContextId,
         ZERO, NULL, NULL, u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    SPRINTF ((CHR1 *) au1Buf, "bgpPeerRemoteAddr");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_STD)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    if ((pOstring =
         (SNMP_AGT_FormOctetString (BGP4_PEER_REMOTE_ADDR (pPeerentry),
                                    BGP4_MAX_INET_ADDRESS_LEN))) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_OCTET_PRIM,
                                                 ZERO, ZERO,
                                                 pOstring, NULL, u8CounterVal);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        free_octetstring (pOstring);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    /* fill the restart information */
    SPRINTF ((CHR1 *) au1Buf, "bgpPeerLastError");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_STD)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER32,
                                                 0, (UINT4) BGP4_PEER_LAST_ERROR
                                                 (pPeerentry), NULL, NULL,
                                                 u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        free_octetstring (pOstring);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    SPRINTF ((CHR1 *) au1Buf, "bgpPeerState");
    if ((pOid = MakeObjIdFromDotNew ((INT1 *) au1Buf, BGP4_STD)) == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        free_octetstring (pOstring);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER32, ZERO,
                                                 (UINT4) BGP4_PEER_STATE
                                                 (pPeerentry), NULL, NULL,
                                                 u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        SNMP_FreeOid (pOid);
        free_octetstring (pOstring);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
    return;
}
#endif /* _BGP4SNMP_C */
