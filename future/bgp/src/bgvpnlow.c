/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgvpnlow.c,v 1.9 2016/10/15 09:18:02 siva Exp $
 *
 *******************************************************************/
# include  "bgp4com.h"
# include  "include.h"
# include  "fsbgpcon.h"
# include  "fsbgpogi.h"
# include  "midconst.h"

/* LOW LEVEL Routines for Table : Fsbgp4MplsVpnVrfRouteTargetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4MplsVpnVrfRouteTargetTable
 Input       :  The Indices
                Fsbgp4MplsVpnVrfName
                Fsbgp4MplsVpnVrfRouteTargetType
                Fsbgp4MplsVpnVrfRouteTarget
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsbgp4MplsVpnVrfRouteTargetTable
    (tSNMP_OCTET_STRING_TYPE * pFsbgp4MplsVpnVrfName,
     INT4 i4Fsbgp4MplsVpnVrfRouteTargetType,
     tSNMP_OCTET_STRING_TYPE * pFsbgp4MplsVpnVrfRouteTarget)
{
#ifdef L3VPN
    UINT1               au1RTExtCom[EXT_COMM_VALUE_LEN];
    UINT4               u4VrfId;
    INT4                i4RetSts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    if (VcmIsVrfExist (pFsbgp4MplsVpnVrfName->pu1_OctetList, &u4VrfId) !=
        VCM_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (!((i4Fsbgp4MplsVpnVrfRouteTargetType == BGP4_IMPORT_ROUTE_TARGET_TYPE)
          || (i4Fsbgp4MplsVpnVrfRouteTargetType ==
              BGP4_EXPORT_ROUTE_TARGET_TYPE)))
    {
        return SNMP_FAILURE;
    }
    i4RetSts =
        Bgp4Vpnv4ParseExtCommunity (pFsbgp4MplsVpnVrfRouteTarget->pu1_OctetList,
                                    pFsbgp4MplsVpnVrfRouteTarget->i4_Length,
                                    au1RTExtCom, (UINT1) BGP4_VPN4_RT_EXT_COM);
    if (i4RetSts != BGP4_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pFsbgp4MplsVpnVrfName);
    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRouteTargetType);
    UNUSED_PARAM (pFsbgp4MplsVpnVrfRouteTarget);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4MplsVpnVrfRouteTargetTable
 Input       :  The Indices
                Fsbgp4MplsVpnVrfName
                Fsbgp4MplsVpnVrfRouteTargetType
                Fsbgp4MplsVpnVrfRouteTarget
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4MplsVpnVrfRouteTargetTable (tSNMP_OCTET_STRING_TYPE *
                                                  pFsbgp4MplsVpnVrfName,
                                                  INT4
                                                  *pi4Fsbgp4MplsVpnVrfRouteTargetType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsbgp4MplsVpnVrfRouteTarget)
{
#ifdef L3VPN
    tTMO_SLL           *pRTList = NULL;
    tExtCommProfile    *pRTComm = NULL;
    UINT4               u4VrfId;
    INT4                i4RetVal = SNMP_FAILURE;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4RetVal = BgpGetFirstActiveContextID ((UINT4 *) &u4VrfId);
    while (i4RetVal == SNMP_SUCCESS)
    {
        if (TMO_SLL_Count (BGP4_VPN4_VRF_SPEC_IMPORT_TARGETS (u4VrfId)) > 0)
        {
            *pi4Fsbgp4MplsVpnVrfRouteTargetType = BGP4_IMPORT_ROUTE_TARGET_TYPE;
            pRTList = BGP4_VPN4_VRF_SPEC_IMPORT_TARGETS (u4VrfId);
        }
        else if (TMO_SLL_Count
                 (BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS (u4VrfId)) > 0)
        {
            *pi4Fsbgp4MplsVpnVrfRouteTargetType = BGP4_EXPORT_ROUTE_TARGET_TYPE;
            pRTList = BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS (u4VrfId);
        }
        else
        {
            i4RetVal = BgpGetNextActiveContextID (u4VrfId, &u4VrfId);
            continue;
        }
        VcmGetAliasName (u4VrfId, pFsbgp4MplsVpnVrfName->pu1_OctetList);
        pFsbgp4MplsVpnVrfName->i4_Length = (INT4)
            (STRLEN (pFsbgp4MplsVpnVrfName->pu1_OctetList));
        pRTComm = (tExtCommProfile *) TMO_SLL_First (pRTList);
        Bgp4Vpnv4CopyExtCom (pFsbgp4MplsVpnVrfRouteTarget->pu1_OctetList,
                             &(pFsbgp4MplsVpnVrfRouteTarget->i4_Length),
                             pRTComm->au1ExtComm, (UINT1) BGP4_VPN4_RT_EXT_COM);
        return SNMP_SUCCESS;

    }
    return SNMP_FAILURE;
#else
    UNUSED_PARAM (pFsbgp4MplsVpnVrfName);
    UNUSED_PARAM (pi4Fsbgp4MplsVpnVrfRouteTargetType);
    UNUSED_PARAM (pFsbgp4MplsVpnVrfRouteTarget);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4MplsVpnVrfRouteTargetTable
 Input       :  The Indices
                Fsbgp4MplsVpnVrfName
                nextFsbgp4MplsVpnVrfName
                Fsbgp4MplsVpnVrfRouteTargetType
                nextFsbgp4MplsVpnVrfRouteTargetType
                Fsbgp4MplsVpnVrfRouteTarget
                nextFsbgp4MplsVpnVrfRouteTarget
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4MplsVpnVrfRouteTargetTable (tSNMP_OCTET_STRING_TYPE *
                                                 pFsbgp4MplsVpnVrfName,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pNextFsbgp4MplsVpnVrfName,
                                                 INT4
                                                 i4Fsbgp4MplsVpnVrfRouteTargetType,
                                                 INT4
                                                 *pi4NextFsbgp4MplsVpnVrfRouteTargetType,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pFsbgp4MplsVpnVrfRouteTarget,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pNextFsbgp4MplsVpnVrfRouteTarget)
{
#ifdef L3VPN_TEST_WANTED
    tTMO_SLL           *pRTCommList = NULL;
    tExtCommProfile    *pRTComm = NULL;
    tBgp4VrfName        VrfName;
    UINT1               au1ExtCom[EXT_COMM_VALUE_LEN];
    UINT4               u4VrfId;
    INT4                i4RetSts;
    UINT1               u1MatchFound = BGP4_FALSE;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4RetSts =
        Bgp4Vpnv4ParseExtCommunity (pFsbgp4MplsVpnVrfRouteTarget->pu1_OctetList,
                                    pFsbgp4MplsVpnVrfRouteTarget->i4_Length,
                                    au1ExtCom, (UINT1) BGP4_VPN4_RT_EXT_COM);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    VrfName.i4Length = pFsbgp4MplsVpnVrfName->i4_Length;
    MEMCPY (VrfName.au1VrfName, pFsbgp4MplsVpnVrfName->pu1_OctetList,
            VrfName.i4Length);
    if (VcmIsVrfExist (VrfName.au1VrfName, &u4VrfId) == VCM_FALSE)
    {
        /* If the given Vrf name is invalid, return the first RT entry */
        return nmhGetFirstIndexFsbgp4MplsVpnVrfRouteTargetTable
            (pNextFsbgp4MplsVpnVrfName, pi4NextFsbgp4MplsVpnVrfRouteTargetType,
             pNextFsbgp4MplsVpnVrfRouteTarget);
    }
    i4RetSts = SNMP_SUCCESS;
    while (i4RetSts == SNMP_SUCCESS)
    {
        if (u1MatchFound == BGP4_TRUE)
        {
            if (TMO_SLL_Count (BGP4_VPN4_VRF_SPEC_IMPORT_TARGETS (u4VrfId)) > 0)
            {
                *pi4NextFsbgp4MplsVpnVrfRouteTargetType =
                    BGP4_IMPORT_ROUTE_TARGET_TYPE;
                pRTCommList = BGP4_VPN4_VRF_SPEC_IMPORT_TARGETS (u4VrfId);
            }
            else if (TMO_SLL_Count
                     (BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS (u4VrfId)) > 0)
            {
                *pi4NextFsbgp4MplsVpnVrfRouteTargetType =
                    BGP4_EXPORT_ROUTE_TARGET_TYPE;
                pRTCommList = BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS (u4VrfId);
            }
            else
            {
                i4RetSts = BgpGetNextActiveContextID (u4VrfId, &u4VrfId);
                continue;
            }
            VcmGetAliasName (u4VrfId, pNextFsbgp4MplsVpnVrfName->pu1_OctetList);
            pNextFsbgp4MplsVpnVrfName->i4_Length = (INT4)
                (STRLEN (pNextFsbgp4MplsVpnVrfName->pu1_OctetList));
            pRTComm = (tExtCommProfile *) TMO_SLL_First (pRTCommList);
            Bgp4Vpnv4CopyExtCom (pNextFsbgp4MplsVpnVrfRouteTarget->
                                 pu1_OctetList,
                                 &(pNextFsbgp4MplsVpnVrfRouteTarget->
                                   i4_Length), pRTComm->au1ExtComm,
                                 (UINT1) BGP4_VPN4_RT_EXT_COM);
            return SNMP_SUCCESS;
        }
        else
        {
            u1MatchFound = BGP4_TRUE;
            /* Matches with this vrf-name */
            pRTCommList = (i4Fsbgp4MplsVpnVrfRouteTargetType ==
                           BGP4_IMPORT_ROUTE_TARGET_TYPE) ?
                BGP4_VPN4_VRF_SPEC_IMPORT_TARGETS (u4VrfId) :
                BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS (u4VrfId);
            pRTComm =
                Bgp4Vpnv4GetNextRouteTargetFromVrf (u4VrfId, au1ExtCom,
                                                    (UINT1)
                                                    i4Fsbgp4MplsVpnVrfRouteTargetType);
            if (pRTComm == NULL)
            {
                if (i4Fsbgp4MplsVpnVrfRouteTargetType ==
                    BGP4_IMPORT_ROUTE_TARGET_TYPE)
                {
                    /* Move to export route targets */
                    pRTCommList = BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS (u4VrfId);
                    if (TMO_SLL_Count (pRTCommList) > 0)
                    {
                        pRTComm = (tExtCommProfile *)
                            TMO_SLL_First (pRTCommList);
                        *pi4NextFsbgp4MplsVpnVrfRouteTargetType =
                            BGP4_EXPORT_ROUTE_TARGET_TYPE;
                    }
                    else
                    {
                        /* move to next VR */
                        i4RetSts =
                            BgpGetNextActiveContextID (u4VrfId, &u4VrfId);
                        continue;
                    }
                }
                else
                {
                    /* export target are over, move to next VR */
                    i4RetSts = BgpGetNextActiveContextID (u4VrfId, &u4VrfId);
                    continue;
                }
            }
            MEMCPY (pNextFsbgp4MplsVpnVrfName->pu1_OctetList,
                    pFsbgp4MplsVpnVrfName->pu1_OctetList,
                    pFsbgp4MplsVpnVrfName->i4_Length);
            pNextFsbgp4MplsVpnVrfName->i4_Length =
                pFsbgp4MplsVpnVrfName->i4_Length;

            Bgp4Vpnv4CopyExtCom (pNextFsbgp4MplsVpnVrfRouteTarget->
                                 pu1_OctetList,
                                 &(pNextFsbgp4MplsVpnVrfRouteTarget->
                                   i4_Length), pRTComm->au1ExtComm,
                                 (UINT1) BGP4_VPN4_RT_EXT_COM);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
#else

    UNUSED_PARAM (pFsbgp4MplsVpnVrfName);
    UNUSED_PARAM (pNextFsbgp4MplsVpnVrfName);
    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRouteTargetType);
    UNUSED_PARAM (pi4NextFsbgp4MplsVpnVrfRouteTargetType);
    UNUSED_PARAM (pFsbgp4MplsVpnVrfRouteTarget);
    UNUSED_PARAM (pNextFsbgp4MplsVpnVrfRouteTarget);
    return SNMP_FAILURE;
#endif
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4MplsVpnVrfRouteTargetRowStatus
 Input       :  The Indices
                Fsbgp4MplsVpnVrfName
                Fsbgp4MplsVpnVrfRouteTargetType
                Fsbgp4MplsVpnVrfRouteTarget

                The Object 
                retValFsbgp4MplsVpnVrfRouteTargetRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4MplsVpnVrfRouteTargetRowStatus (tSNMP_OCTET_STRING_TYPE *
                                            pFsbgp4MplsVpnVrfName,
                                            INT4
                                            i4Fsbgp4MplsVpnVrfRouteTargetType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsbgp4MplsVpnVrfRouteTarget,
                                            INT4
                                            *pi4RetValFsbgp4MplsVpnVrfRouteTargetRowStatus)
{
#ifdef L3VPN
    tExtCommProfile    *pRTComm = NULL;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT4               u4VrfId;
    INT4                i4RetSts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    if (i4Fsbgp4MplsVpnVrfRouteTargetType == BGP4_BOTH_ROUTE_TARGET_TYPE)
    {
        return SNMP_FAILURE;
    }

    if (VcmIsVrfExist (pFsbgp4MplsVpnVrfName->pu1_OctetList, &u4VrfId) !=
        VCM_TRUE)
    {
        return SNMP_FAILURE;
    }
    i4RetSts =
        Bgp4Vpnv4ParseExtCommunity (pFsbgp4MplsVpnVrfRouteTarget->pu1_OctetList,
                                    pFsbgp4MplsVpnVrfRouteTarget->i4_Length,
                                    au1ExtComm, (UINT1) BGP4_VPN4_RT_EXT_COM);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pRTComm = Bgp4Vpnv4GetRouteTargetFromVrf (u4VrfId, au1ExtComm,
                                              (UINT1)
                                              i4Fsbgp4MplsVpnVrfRouteTargetType);
    if (pRTComm == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4MplsVpnVrfRouteTargetRowStatus =
        BGP4_VPN4_EXT_COMM_ROW_STATUS (pRTComm);
    return SNMP_SUCCESS;
#else

    UNUSED_PARAM (pFsbgp4MplsVpnVrfName);
    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRouteTargetType);
    UNUSED_PARAM (pFsbgp4MplsVpnVrfRouteTarget);
    UNUSED_PARAM (pi4RetValFsbgp4MplsVpnVrfRouteTargetRowStatus);
    return SNMP_FAILURE;
#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4MplsVpnVrfRouteTargetRowStatus
 Input       :  The Indices
                Fsbgp4MplsVpnVrfName
                Fsbgp4MplsVpnVrfRouteTargetType
                Fsbgp4MplsVpnVrfRouteTarget

                The Object 
                setValFsbgp4MplsVpnVrfRouteTargetRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4MplsVpnVrfRouteTargetRowStatus (tSNMP_OCTET_STRING_TYPE *
                                            pFsbgp4MplsVpnVrfName,
                                            INT4
                                            i4Fsbgp4MplsVpnVrfRouteTargetType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsbgp4MplsVpnVrfRouteTarget,
                                            INT4
                                            i4SetValFsbgp4MplsVpnVrfRouteTargetRowStatus)
{
#ifdef L3VPN_TEST_WANTED
    tExtCommProfile    *pRTComm = NULL;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT4               u4VrfId;
    INT4                i4RetSts;
    INT4                i4SecondRTType;
    UINT1               u1NoRTs = 1;
    UINT1               u1Index;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    if (VcmIsVrfExist (pFsbgp4MplsVpnVrfName->pu1_OctetList, &u4VrfId) !=
        VCM_TRUE)
    {
        return SNMP_FAILURE;
    }
    i4RetSts =
        Bgp4Vpnv4ParseExtCommunity (pFsbgp4MplsVpnVrfRouteTarget->pu1_OctetList,
                                    pFsbgp4MplsVpnVrfRouteTarget->i4_Length,
                                    au1ExtComm, (UINT1) BGP4_VPN4_RT_EXT_COM);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (i4Fsbgp4MplsVpnVrfRouteTargetType == BGP4_BOTH_ROUTE_TARGET_TYPE)
    {
        u1NoRTs = 2;
        i4Fsbgp4MplsVpnVrfRouteTargetType = BGP4_IMPORT_ROUTE_TARGET_TYPE;
        i4SecondRTType = BGP4_EXPORT_ROUTE_TARGET_TYPE;
    }
    else
    {
        i4SecondRTType = 0;
    }

    for (u1Index = 0; u1Index < u1NoRTs; u1Index++)
    {
        switch (i4SetValFsbgp4MplsVpnVrfRouteTargetRowStatus)
        {
            case CREATE_AND_GO:
                pRTComm = Bgp4Vpnv4GetRouteTargetFromVrf (u4VrfId, au1ExtComm,
                                                          (UINT1)
                                                          i4Fsbgp4MplsVpnVrfRouteTargetType);
                if (pRTComm != NULL)
                {
                    return SNMP_FAILURE;
                }
                pRTComm = Bgp4Vpnv4RouteTargetChgHandler (u4VrfId,
                                                          i4Fsbgp4MplsVpnVrfRouteTargetType,
                                                          au1ExtComm,
                                                          BGP4_ROUTE_TARGET_ADD);
                if (pRTComm == NULL)
                {
                    return SNMP_FAILURE;
                }
                BGP4_VPN4_EXT_COMM_ROW_STATUS (pRTComm) = ACTIVE;
                break;

            case CREATE_AND_WAIT:
                pRTComm = Bgp4Vpnv4GetRouteTargetFromVrf (u4VrfId, au1ExtComm,
                                                          (UINT1)
                                                          i4Fsbgp4MplsVpnVrfRouteTargetType);
                if (pRTComm != NULL)
                {
                    return SNMP_FAILURE;
                }
                pRTComm = Bgp4Vpnv4RouteTargetChgHandler (u4VrfId,
                                                          i4Fsbgp4MplsVpnVrfRouteTargetType,
                                                          au1ExtComm,
                                                          BGP4_ROUTE_TARGET_ADD);
                if (pRTComm == NULL)
                {
                    return SNMP_FAILURE;
                }
                BGP4_VPN4_EXT_COMM_ROW_STATUS (pRTComm) = NOT_READY;
                break;

            case ACTIVE:
                pRTComm = Bgp4Vpnv4GetRouteTargetFromVrf (u4VrfId, au1ExtComm,
                                                          (UINT1)
                                                          i4Fsbgp4MplsVpnVrfRouteTargetType);
                if (pRTComm == NULL)
                {
                    return SNMP_FAILURE;
                }

                BGP4_VPN4_EXT_COMM_ROW_STATUS (pRTComm) = ACTIVE;
                /* The route target is already informed to VPN module during
                 * either createAndWait/createAndGo. hence just update the
                 * status as active. We are not updating the status to VPN
                 */
                break;
            case NOT_IN_SERVICE:
                pRTComm = Bgp4Vpnv4GetRouteTargetFromVrf (u4VrfId, au1ExtComm,
                                                          (UINT1)
                                                          i4Fsbgp4MplsVpnVrfRouteTargetType);
                if (pRTComm == NULL)
                {
                    return SNMP_FAILURE;
                }

                BGP4_VPN4_EXT_COMM_ROW_STATUS (pRTComm) = NOT_IN_SERVICE;
                break;

            case DESTROY:
                pRTComm = Bgp4Vpnv4GetRouteTargetFromVrf (u4VrfId, au1ExtComm,
                                                          (UINT1)
                                                          i4Fsbgp4MplsVpnVrfRouteTargetType);
                if (pRTComm != NULL)
                {
                    pRTComm = Bgp4Vpnv4RouteTargetChgHandler (u4VrfId,
                                                              i4Fsbgp4MplsVpnVrfRouteTargetType,
                                                              au1ExtComm,
                                                              BGP4_ROUTE_TARGET_DEL);
                }
                break;
        }
        i4Fsbgp4MplsVpnVrfRouteTargetType = i4SecondRTType;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pFsbgp4MplsVpnVrfName);
    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRouteTargetType);
    UNUSED_PARAM (pFsbgp4MplsVpnVrfRouteTarget);
    UNUSED_PARAM (i4SetValFsbgp4MplsVpnVrfRouteTargetRowStatus);
    return SNMP_FAILURE;
#endif
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4MplsVpnVrfRouteTargetRowStatus
 Input       :  The Indices
                Fsbgp4MplsVpnVrfName
                Fsbgp4MplsVpnVrfRouteTargetType
                Fsbgp4MplsVpnVrfRouteTarget

                The Object 
                testValFsbgp4MplsVpnVrfRouteTargetRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4MplsVpnVrfRouteTargetRowStatus (UINT4 *pu4ErrorCode,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsbgp4MplsVpnVrfName,
                                               INT4
                                               i4Fsbgp4MplsVpnVrfRouteTargetType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsbgp4MplsVpnVrfRouteTarget,
                                               INT4
                                               i4TestValFsbgp4MplsVpnVrfRouteTargetRowStatus)
{
#ifdef L3VPN
    tExtCommProfile    *pRTComm = NULL;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT4               u4VrfId;
    INT4                i4RetSts;
    INT4                i4SecondRTType;
    UINT1               u1NoRTs;
    UINT1               u1Index;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    if (VcmIsVrfExist (pFsbgp4MplsVpnVrfName->pu1_OctetList, &u4VrfId) !=
        VCM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    i4RetSts =
        Bgp4Vpnv4ParseExtCommunity (pFsbgp4MplsVpnVrfRouteTarget->pu1_OctetList,
                                    pFsbgp4MplsVpnVrfRouteTarget->i4_Length,
                                    au1ExtComm, (UINT1) BGP4_VPN4_RT_EXT_COM);
    if (i4RetSts == BGP4_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (!((i4Fsbgp4MplsVpnVrfRouteTargetType == BGP4_IMPORT_ROUTE_TARGET_TYPE)
          || (i4Fsbgp4MplsVpnVrfRouteTargetType ==
              BGP4_EXPORT_ROUTE_TARGET_TYPE)
          || (i4Fsbgp4MplsVpnVrfRouteTargetType ==
              BGP4_BOTH_ROUTE_TARGET_TYPE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    u1NoRTs = 1;
    if (i4Fsbgp4MplsVpnVrfRouteTargetType == BGP4_BOTH_ROUTE_TARGET_TYPE)
    {
        u1NoRTs = 2;
        i4Fsbgp4MplsVpnVrfRouteTargetType = BGP4_IMPORT_ROUTE_TARGET_TYPE;
        i4SecondRTType = BGP4_EXPORT_ROUTE_TARGET_TYPE;
    }
    else
    {
        i4SecondRTType = 0;
    }

    for (u1Index = 0; u1Index < u1NoRTs; u1Index++)
    {
        pRTComm = Bgp4Vpnv4GetRouteTargetFromVrf (u4VrfId, au1ExtComm,
                                                  (UINT1)
                                                  i4Fsbgp4MplsVpnVrfRouteTargetType);
        switch (i4TestValFsbgp4MplsVpnVrfRouteTargetRowStatus)
        {
            case CREATE_AND_GO:
            case CREATE_AND_WAIT:
                if (pRTComm != NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                break;

            case ACTIVE:
            case NOT_IN_SERVICE:
            case DESTROY:
                break;
            default:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
        }
        i4Fsbgp4MplsVpnVrfRouteTargetType = i4SecondRTType;
    }
    return SNMP_SUCCESS;
#else

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsbgp4MplsVpnVrfName);
    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRouteTargetType);
    UNUSED_PARAM (pFsbgp4MplsVpnVrfRouteTarget);
    UNUSED_PARAM (i4TestValFsbgp4MplsVpnVrfRouteTargetRowStatus);
    return SNMP_FAILURE;
#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4MplsVpnVrfRouteTargetTable
 Input       :  The Indices
                Fsbgp4MplsVpnVrfName
                Fsbgp4MplsVpnVrfRouteTargetType
                Fsbgp4MplsVpnVrfRouteTarget
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4MplsVpnVrfRouteTargetTable (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4MplsVpnVrfRedistributeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4MplsVpnVrfRedistributeTable
 Input       :  The Indices
                Fsbgp4MplsVpnVrfRedisAfi
                Fsbgp4MplsVpnVrfRedisSafi
                Fsbgp4MplsVpnVrfName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4MplsVpnVrfRedistributeTable (INT4
                                                           i4Fsbgp4MplsVpnVrfRedisAfi,
                                                           INT4
                                                           i4Fsbgp4MplsVpnVrfRedisSafi,
                                                           tSNMP_OCTET_STRING_TYPE
                                                           *
                                                           pFsbgp4MplsVpnVrfName)
{
#ifdef L3VPN_TEST_WANTED
    tVrfSpecInfo       *pVrfInfo = NULL;
    tBgp4VrfName        VrfName;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    if ((i4Fsbgp4MplsVpnVrfRedisAfi != BGP4_INET_AFI_IPV4) ||
        (i4Fsbgp4MplsVpnVrfRedisSafi != BGP4_INET_SAFI_UNICAST))
    {
        return SNMP_FAILURE;
    }

    VrfName.i4Length = pFsbgp4MplsVpnVrfName->i4_Length;
    MEMCPY (VrfName.au1VrfName, pFsbgp4MplsVpnVrfName->pu1_OctetList,
            VrfName.i4Length);
    Bgp4Vpn4GetVrfInfoFromName (&VrfName, &pVrfInfo);
    if (pVrfInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else

    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRedisAfi);
    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRedisSafi);
    UNUSED_PARAM (pFsbgp4MplsVpnVrfName);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4MplsVpnVrfRedistributeTable
 Input       :  The Indices
                Fsbgp4MplsVpnVrfRedisAfi
                Fsbgp4MplsVpnVrfRedisSafi
                Fsbgp4MplsVpnVrfName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4MplsVpnVrfRedistributeTable (INT4
                                                   *pi4Fsbgp4MplsVpnVrfRedisAfi,
                                                   INT4
                                                   *pi4Fsbgp4MplsVpnVrfRedisSafi,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pFsbgp4MplsVpnVrfName)
{
#ifdef L3VPN_TEST_WANTED
    tVrfSpecInfo       *pVrfInfo = NULL;
    UINT4               u4HashKey;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    TMO_HASH_Scan_Table (BGP4_VPN4_VRF_GLOBAL_INFO, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (BGP4_VPN4_VRF_GLOBAL_INFO, u4HashKey, pVrfInfo,
                              tVrfSpecInfo *)
        {
            *pi4Fsbgp4MplsVpnVrfRedisAfi = BGP4_INET_AFI_IPV4;
            *pi4Fsbgp4MplsVpnVrfRedisSafi = BGP4_INET_SAFI_UNICAST;
            pFsbgp4MplsVpnVrfName->i4_Length =
                BGP4_VPN4_VRF_SPEC_NAME_SIZE (pVrfInfo);
            MEMCPY (pFsbgp4MplsVpnVrfName->pu1_OctetList,
                    BGP4_VPN4_VRF_SPEC_VRFNAME_VALUE (pVrfInfo),
                    BGP4_VPN4_VRF_SPEC_NAME_SIZE (pVrfInfo));
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
#else

    UNUSED_PARAM (pi4Fsbgp4MplsVpnVrfRedisAfi);
    UNUSED_PARAM (pi4Fsbgp4MplsVpnVrfRedisSafi);
    UNUSED_PARAM (pFsbgp4MplsVpnVrfName);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4MplsVpnVrfRedistributeTable
 Input       :  The Indices
                Fsbgp4MplsVpnVrfRedisAfi
                nextFsbgp4MplsVpnVrfRedisAfi
                Fsbgp4MplsVpnVrfRedisSafi
                nextFsbgp4MplsVpnVrfRedisSafi
                Fsbgp4MplsVpnVrfName
                nextFsbgp4MplsVpnVrfName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4MplsVpnVrfRedistributeTable (INT4
                                                  i4Fsbgp4MplsVpnVrfRedisAfi,
                                                  INT4
                                                  *pi4NextFsbgp4MplsVpnVrfRedisAfi,
                                                  INT4
                                                  i4Fsbgp4MplsVpnVrfRedisSafi,
                                                  INT4
                                                  *pi4NextFsbgp4MplsVpnVrfRedisSafi,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsbgp4MplsVpnVrfName,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pNextFsbgp4MplsVpnVrfName)
{
#ifdef L3VPN_TEST_WANTED
    tVrfSpecInfo       *pVrfInfo = NULL;
    tBgp4VrfName        VrfName;
    UINT4               u4HashKey;
    UINT1               u1MatchFound = BGP4_FALSE;

    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRedisAfi);
    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRedisSafi);
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }
    VrfName.i4Length = pFsbgp4MplsVpnVrfName->i4_Length;
    MEMCPY (VrfName.au1VrfName, pFsbgp4MplsVpnVrfName->pu1_OctetList,
            VrfName.i4Length);
    TMO_HASH_Scan_Table (BGP4_VPN4_VRF_GLOBAL_INFO, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (BGP4_VPN4_VRF_GLOBAL_INFO, u4HashKey, pVrfInfo,
                              tVrfSpecInfo *)
        {
            if (u1MatchFound == BGP4_TRUE)
            {
                *pi4NextFsbgp4MplsVpnVrfRedisAfi = BGP4_INET_AFI_IPV4;
                *pi4NextFsbgp4MplsVpnVrfRedisSafi = BGP4_INET_SAFI_UNICAST;
                MEMCPY (pNextFsbgp4MplsVpnVrfName->pu1_OctetList,
                        BGP4_VPN4_VRF_SPEC_VRFNAME_VALUE (pVrfInfo),
                        BGP4_VPN4_VRF_SPEC_NAME_SIZE (pVrfInfo));
                pNextFsbgp4MplsVpnVrfName->i4_Length =
                    BGP4_VPN4_VRF_SPEC_NAME_SIZE (pVrfInfo);
                return SNMP_SUCCESS;
            }
            else
            {
                if (BGP4_VPN4_VRF_NAME_CMP ((&VrfName),
                                            (&
                                             (BGP4_VPN4_VRF_SPEC_VRFNAME
                                              (pVrfInfo)))) == BGP4_TRUE)
                {
                    u1MatchFound = BGP4_TRUE;
                }
            }
        }
    }
    return SNMP_FAILURE;
#else

    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRedisAfi);
    UNUSED_PARAM (pi4NextFsbgp4MplsVpnVrfRedisAfi);
    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRedisSafi);
    UNUSED_PARAM (pi4NextFsbgp4MplsVpnVrfRedisSafi);
    UNUSED_PARAM (pFsbgp4MplsVpnVrfName);
    UNUSED_PARAM (pNextFsbgp4MplsVpnVrfName);
    return SNMP_FAILURE;
#endif
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4MplsVpnVrfRedisProtoMask
 Input       :  The Indices
                Fsbgp4MplsVpnVrfRedisAfi
                Fsbgp4MplsVpnVrfRedisSafi
                Fsbgp4MplsVpnVrfName

                The Object 
                retValFsbgp4MplsVpnVrfRedisProtoMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4MplsVpnVrfRedisProtoMask (INT4 i4Fsbgp4MplsVpnVrfRedisAfi,
                                      INT4 i4Fsbgp4MplsVpnVrfRedisSafi,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsbgp4MplsVpnVrfName,
                                      INT4
                                      *pi4RetValFsbgp4MplsVpnVrfRedisProtoMask)
{
#ifdef L3VPN_TEST_WANTED
    tVrfSpecInfo       *pVrfInfo = NULL;
    tBgp4VrfName        VrfName;

    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRedisAfi);
    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRedisSafi);

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    VrfName.i4Length = pFsbgp4MplsVpnVrfName->i4_Length;
    MEMCPY (VrfName.au1VrfName, pFsbgp4MplsVpnVrfName->pu1_OctetList,
            VrfName.i4Length);
    Bgp4Vpn4GetVrfInfoFromName (&VrfName, &pVrfInfo);
    if (pVrfInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsbgp4MplsVpnVrfRedisProtoMask =
        BGP4_VPN4_VRF_SPEC_REDIS_MASK (pVrfInfo);
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRedisAfi);
    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRedisSafi);
    UNUSED_PARAM (pFsbgp4MplsVpnVrfName);
    UNUSED_PARAM (pi4RetValFsbgp4MplsVpnVrfRedisProtoMask);
    return SNMP_FAILURE;
#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsbgp4MplsVpnVrfRedisProtoMask
 Input       :  The Indices
                Fsbgp4MplsVpnVrfRedisAfi
                Fsbgp4MplsVpnVrfRedisSafi
                Fsbgp4MplsVpnVrfName

                The Object 
                setValFsbgp4MplsVpnVrfRedisProtoMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsbgp4MplsVpnVrfRedisProtoMask (INT4 i4Fsbgp4MplsVpnVrfRedisAfi,
                                      INT4 i4Fsbgp4MplsVpnVrfRedisSafi,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsbgp4MplsVpnVrfName,
                                      INT4
                                      i4SetValFsbgp4MplsVpnVrfRedisProtoMask)
{
#ifdef L3VPN_TEST_WANTED
    tBgp4QMsg          *pQMsg = NULL;
    tVrfSpecInfo       *pVrfInfo = NULL;
    tBgp4VrfName        VrfName;

    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRedisAfi);
    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRedisSafi);
    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    VrfName.i4Length = pFsbgp4MplsVpnVrfName->i4_Length;
    MEMCPY (VrfName.au1VrfName, pFsbgp4MplsVpnVrfName->pu1_OctetList,
            VrfName.i4Length);
    Bgp4Vpn4GetVrfInfoFromName (&VrfName, &pVrfInfo);
    if (pVrfInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (BGP4_VPN4_VRF_SPEC_REDIS_MASK (pVrfInfo) ==
        (UINT4) i4SetValFsbgp4MplsVpnVrfRedisProtoMask)
    {
        return SNMP_SUCCESS;
    }
    if (BGP4_VPN4_VRF_SPEC_REDIS_MASK (pVrfInfo) == 0)
    {
        BGP4_VPN4_VRF_SPEC_REDIS_MASK (pVrfInfo) =
            (UINT4) i4SetValFsbgp4MplsVpnVrfRedisProtoMask;
        return SNMP_SUCCESS;
    }
    BGP4_VPN4_VRF_SPEC_REDIS_MASK (pVrfInfo) =
        (UINT4) i4SetValFsbgp4MplsVpnVrfRedisProtoMask;

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting BGP Peer Status Msg "
                  "to BGP Queue FAILED!!!\n");
        return SNMP_FAILURE;
    }

    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_IGP_RRD_DISABLE_EVENT;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_WAIT_FOR_RESP;
    BGP4_INPUTQ_VRF_NAME_LEN (pQMsg) = pFsbgp4MplsVpnVrfName->i4_Length;
    MEMCPY (BGP4_INPUTQ_VRF_NAME_VAL (pQMsg),
            pFsbgp4MplsVpnVrfName->pu1_OctetList,
            pFsbgp4MplsVpnVrfName->i4_Length);

    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRedisAfi);
    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRedisSafi);
    UNUSED_PARAM (pFsbgp4MplsVpnVrfName);
    UNUSED_PARAM (i4SetValFsbgp4MplsVpnVrfRedisProtoMask);
    return SNMP_FAILURE;
#endif
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsbgp4MplsVpnVrfRedisProtoMask
 Input       :  The Indices
                Fsbgp4MplsVpnVrfRedisAfi
                Fsbgp4MplsVpnVrfRedisSafi
                Fsbgp4MplsVpnVrfName

                The Object 
                testValFsbgp4MplsVpnVrfRedisProtoMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsbgp4MplsVpnVrfRedisProtoMask (UINT4 *pu4ErrorCode,
                                         INT4 i4Fsbgp4MplsVpnVrfRedisAfi,
                                         INT4 i4Fsbgp4MplsVpnVrfRedisSafi,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsbgp4MplsVpnVrfName,
                                         INT4
                                         i4TestValFsbgp4MplsVpnVrfRedisProtoMask)
{
#ifdef L3VPN_TEST_WANTED
    tVrfSpecInfo       *pVrfInfo = NULL;
    tBgp4VrfName        VrfName;
    INT4                i4Range = BGP4_ALL_PROTO_MASK;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    VrfName.i4Length = pFsbgp4MplsVpnVrfName->i4_Length;
    MEMCPY (VrfName.au1VrfName, pFsbgp4MplsVpnVrfName->pu1_OctetList,
            VrfName.i4Length);
    Bgp4Vpn4GetVrfInfoFromName (&VrfName, &pVrfInfo);
    if (pVrfInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4Fsbgp4MplsVpnVrfRedisAfi != BGP4_INET_AFI_IPV4) ||
        (i4Fsbgp4MplsVpnVrfRedisSafi != BGP4_INET_SAFI_UNICAST))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Since we do not have the mib object for disabling, we should accept
     * the value of zero  for disabling the redistribution completely
     */
    if (i4TestValFsbgp4MplsVpnVrfRedisProtoMask & (~i4Range))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - Invalid Redistribution Protocol Mask.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRedisAfi);
    UNUSED_PARAM (i4Fsbgp4MplsVpnVrfRedisSafi);
    UNUSED_PARAM (pFsbgp4MplsVpnVrfName);
    UNUSED_PARAM (i4TestValFsbgp4MplsVpnVrfRedisProtoMask);
    return SNMP_FAILURE;
#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsbgp4MplsVpnVrfRedistributeTable
 Input       :  The Indices
                Fsbgp4MplsVpnVrfRedisAfi
                Fsbgp4MplsVpnVrfRedisSafi
                Fsbgp4MplsVpnVrfName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsbgp4MplsVpnVrfRedistributeTable (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsbgp4MplsVpnRRRouteTargetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsbgp4MplsVpnRRRouteTargetTable
 Input       :  The Indices
                Fsbgp4MplsVpnRRRouteTarget
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsbgp4MplsVpnRRRouteTargetTable (tSNMP_OCTET_STRING_TYPE
                                                         *
                                                         pFsbgp4MplsVpnRRRouteTarget)
{
#ifdef L3VPN
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    INT4                i4RetSts;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4RetSts =
        Bgp4Vpnv4ParseExtCommunity (pFsbgp4MplsVpnRRRouteTarget->pu1_OctetList,
                                    pFsbgp4MplsVpnRRRouteTarget->i4_Length,
                                    au1ExtComm, (UINT1) BGP4_VPN4_RT_EXT_COM);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else

    UNUSED_PARAM (pFsbgp4MplsVpnRRRouteTarget);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsbgp4MplsVpnRRRouteTargetTable
 Input       :  The Indices
                Fsbgp4MplsVpnRRRouteTarget
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsbgp4MplsVpnRRRouteTargetTable (tSNMP_OCTET_STRING_TYPE *
                                                 pFsbgp4MplsVpnRRRouteTarget)
{
#ifdef L3VPN
    tBgp4RrImportTargetInfo *pRrImportTarget = NULL;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    if (TMO_SLL_Count (BGP4_VPN4_RR_TARGETS_SET) == 0)
    {
        return SNMP_FAILURE;
    }
    pRrImportTarget = (tBgp4RrImportTargetInfo *)
        TMO_SLL_First (BGP4_VPN4_RR_TARGETS_SET);
    Bgp4Vpnv4CopyExtCom (pFsbgp4MplsVpnRRRouteTarget->pu1_OctetList,
                         &(pFsbgp4MplsVpnRRRouteTarget->i4_Length),
                         BGP4_VPN4_RR_IMPORT_TARGET (pRrImportTarget),
                         (UINT1) BGP4_VPN4_RT_EXT_COM);

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pFsbgp4MplsVpnRRRouteTarget);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsbgp4MplsVpnRRRouteTargetTable
 Input       :  The Indices
                Fsbgp4MplsVpnRRRouteTarget
                nextFsbgp4MplsVpnRRRouteTarget
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsbgp4MplsVpnRRRouteTargetTable (tSNMP_OCTET_STRING_TYPE *
                                                pFsbgp4MplsVpnRRRouteTarget,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pNextFsbgp4MplsVpnRRRouteTarget)
{
#ifdef L3VPN
    tBgp4RrImportTargetInfo *pRrImportTarget = NULL;
    tBgp4RrImportTargetInfo *pRrImportTargetNext = NULL;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    INT4                i4RetSts = BGP4_FAILURE;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4RetSts =
        Bgp4Vpnv4ParseExtCommunity (pFsbgp4MplsVpnRRRouteTarget->pu1_OctetList,
                                    pFsbgp4MplsVpnRRRouteTarget->i4_Length,
                                    au1ExtComm, (UINT1) BGP4_VPN4_RT_EXT_COM);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pRrImportTarget = Bgp4Vpnv4IsRTMatchInRRImportRTList (au1ExtComm);
    if (pRrImportTarget != NULL)
    {
        return SNMP_FAILURE;
    }

    pRrImportTargetNext = (tBgp4RrImportTargetInfo *)
        TMO_SLL_Next (BGP4_VPN4_RR_TARGETS_SET,
                      (tTMO_SLL_NODE *) pRrImportTarget);
    if (pRrImportTargetNext != NULL)
    {
        Bgp4Vpnv4CopyExtCom (pNextFsbgp4MplsVpnRRRouteTarget->pu1_OctetList,
                             &(pNextFsbgp4MplsVpnRRRouteTarget->i4_Length),
                             BGP4_VPN4_RR_IMPORT_TARGET (pRrImportTargetNext),
                             (UINT1) BGP4_VPN4_RT_EXT_COM);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
#else
    UNUSED_PARAM (pFsbgp4MplsVpnRRRouteTarget);
    UNUSED_PARAM (pNextFsbgp4MplsVpnRRRouteTarget);
    return SNMP_FAILURE;
#endif
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsbgp4MplsVpnRRRouteTargetRtCnt
 Input       :  The Indices
                Fsbgp4MplsVpnRRRouteTarget

                The Object 
                retValFsbgp4MplsVpnRRRouteTargetRtCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4MplsVpnRRRouteTargetRtCnt (tSNMP_OCTET_STRING_TYPE *
                                       pFsbgp4MplsVpnRRRouteTarget,
                                       INT4
                                       *pi4RetValFsbgp4MplsVpnRRRouteTargetRtCnt)
{
#ifdef L3VPN
    tBgp4RrImportTargetInfo *pRrImportTarget = NULL;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    INT4                i4RetSts = BGP4_FAILURE;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4RetSts =
        Bgp4Vpnv4ParseExtCommunity (pFsbgp4MplsVpnRRRouteTarget->pu1_OctetList,
                                    pFsbgp4MplsVpnRRRouteTarget->i4_Length,
                                    au1ExtComm, (UINT1) BGP4_VPN4_RT_EXT_COM);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pRrImportTarget = Bgp4Vpnv4IsRTMatchInRRImportRTList (au1ExtComm);
    if (pRrImportTarget == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsbgp4MplsVpnRRRouteTargetRtCnt = (INT4)
        (BGP4_VPN4_RR_IMPORT_TARGET_RTCNT (pRrImportTarget));
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pFsbgp4MplsVpnRRRouteTarget);
    UNUSED_PARAM (pi4RetValFsbgp4MplsVpnRRRouteTargetRtCnt);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsbgp4MplsVpnRRRouteTargetTimeStamp
 Input       :  The Indices
                Fsbgp4MplsVpnRRRouteTarget

                The Object 
                retValFsbgp4MplsVpnRRRouteTargetTimeStamp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsbgp4MplsVpnRRRouteTargetTimeStamp (tSNMP_OCTET_STRING_TYPE *
                                           pFsbgp4MplsVpnRRRouteTarget,
                                           INT4
                                           *pi4RetValFsbgp4MplsVpnRRRouteTargetTimeStamp)
{
#ifdef L3VPN
    tBgp4RrImportTargetInfo *pRrImportTarget = NULL;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    INT4                i4Delay = 0;
    INT4                i4RetSts = BGP4_FAILURE;

    if (BGP4_TASK_INIT_STATUS != BGP4_TRUE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC, BGP4_MOD_NAME,
                  "\tERROR - BGP Task not initialized.\n");
        return SNMP_FAILURE;
    }

    i4RetSts =
        Bgp4Vpnv4ParseExtCommunity (pFsbgp4MplsVpnRRRouteTarget->pu1_OctetList,
                                    pFsbgp4MplsVpnRRRouteTarget->i4_Length,
                                    au1ExtComm, (UINT1) BGP4_VPN4_RT_EXT_COM);
    if (i4RetSts == BGP4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pRrImportTarget = Bgp4Vpnv4IsRTMatchInRRImportRTList (au1ExtComm);
    if (pRrImportTarget == NULL)
    {
        return SNMP_FAILURE;
    }

    i4Delay = (INT4)
        (BGP4_VPN4_RR_TARGETS_DEL_INTERVAL -
         ((UINT4) Bgp4ElapTime () -
          BGP4_VPN4_RR_IMPORT_TARGET_TIMESTAMP (pRrImportTarget)));
    *pi4RetValFsbgp4MplsVpnRRRouteTargetTimeStamp =
        (i4Delay >= 0) ? i4Delay : 0;
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pFsbgp4MplsVpnRRRouteTarget);
    UNUSED_PARAM (pi4RetValFsbgp4MplsVpnRRRouteTargetTimeStamp);
    return SNMP_FAILURE;
#endif
}

#ifdef L3VPN
/********************************************************************/
/* Function Name   : Bgp4Vpnv4SendRtRefToPEPeers                    */
/* Description     : Posts event to BGP task to send route-refresh  */
/*                   message to PE peers                            */
/* Input(s)        : None.                                          */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4Vpnv4SendRtRefToPEPeers ()
{
    tBgp4QMsg          *pQMsg = NULL;
    tAddrPrefix         PeerAddress;

    /* This function MUST be called from LOW level routines only as
     * this posts event to the BGP task.
     */
    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting BGP Route Refresh "
                  "Msg to BGP Queue FAILED!!!\n");
        return BGP4_FAILURE;
    }
    Bgp4InitAddrPrefixStruct (&(PeerAddress), BGP4_INET_AFI_IPV4);
    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_SNMP_INBOUND_SOFTCONFIG_RTREF_EVENT;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
    BGP4_INPUTQ_CXT (pQMsg) = BGP4_DFLT_VRFID;
    Bgp4CopyAddrPrefixStruct (&(BGP4_INPUTQ_RTREF_PEER_ADDR_INFO (pQMsg)),
                              PeerAddress);
    BGP4_INPUTQ_RTREF_ASAFI_MASK (pQMsg) = CAP_MP_VPN4_UNICAST;
    BGP4_INPUTQ_RTREF_OPER_TYPE (pQMsg) = BGP4_RR_ADVT_PE_PEERS;
    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

INT4
Bgp4Vpnv4PostExpTgtChgEvent (UINT4 u4VrfId)
{
    tBgp4QMsg          *pQMsg = NULL;

    /* This function MUST be called from LOW level routines only as
     * this posts event to the BGP task.
     */
    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for posting BGP Route Refresh "
                  "Msg to BGP Queue FAILED!!!\n");
        return BGP4_FAILURE;
    }

    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_EXPORT_TARGET_CHG_EVENT;
    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
    BGP4_INPUTQ_VRF_ID (pQMsg) = u4VrfId;
    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        return BGP4_FAILURE;
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4ParseExtCommunity                     */
/* Description     : Parses the route-target value according to the */
/*                   type of the value                              */
/* Input(s)        : pVpn4ExtCom - route target value.              */
/*                   pu1ExtCommValue -  parsed value                */
/*                   u1ExtComType - type of the value               */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4Vpnv4ParseExtCommunity (UINT1 *pVpn4ExtCom,
                            INT4 i4EcomLen,
                            UINT1 *pu1ExtCommValue, UINT1 u1ExtComType)
{
    INT4                i4Len = i4EcomLen;
    UINT1              *pu1Temp = NULL;
    UINT1               u1HighOrderType = BGP4_VPN4_AS_EXT_COM_TYPE;
    UINT4               u4Address;
    UINT4               au4ComVal[EXT_COMM_VALUE_LEN];
    UINT1               au1TempCom[80];
    UINT1               u1Temp = '\0';
    UINT1               u1Index;

    pu1Temp = MemAllocMemBlk (gBgpNode.Bgp4BufNodeMsgPoolId);;
    if (pu1Temp == NULL)
    {
        return BGP4_FAILURE;
    }
    MEMSET (pu1Temp, 0, i4Len);
    if (MEMCMP (pVpn4ExtCom, pu1Temp, i4Len) == 0)
    {
        if (u1ExtComType == BGP4_VPN4_ROUTE_DISTINGUISHER)
        {
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1Temp);
            return BGP4_FAILURE;
        }
        MEMSET (pu1ExtCommValue, 0, EXT_COMM_VALUE_LEN);
        MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1Temp);
        return BGP4_SUCCESS;
    }
    MEMCPY (pu1Temp, pVpn4ExtCom, i4Len);
    *(pu1Temp + i4Len) = 0;
    i4Len = sscanf ((const char *) pu1Temp, "%d:%2s",
                    (int *) &au4ComVal[0], (char *) au1TempCom);
    if (i4Len == 2)
    {
        for (u1Index = 0; u1Index < STRLEN (au1TempCom); u1Index++)
        {
            if (!(ISDIGIT (au1TempCom[u1Index])))
            {
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1Temp);
                return BGP4_FAILURE;
            }
        }
        au4ComVal[1] = BGP_DECSTRTOUL (au1TempCom);
        if ((au4ComVal[0] > BGP_MAX_AS_VALUE) ||
            (au4ComVal[1] >= BGP4_VPN4_MAX_4BYTE_VAL))
        {
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1Temp);
            return BGP4_FAILURE;
        }
        PTR_ASSIGN2 ((pu1ExtCommValue + 2), au4ComVal[0]);
        PTR_ASSIGN4 ((pu1ExtCommValue + 4), au4ComVal[1]);
    }
    else
    {
        u1HighOrderType = BGP4_VPN4_IPADDR_EXT_COM_TYPE;
        i4Len = sscanf ((const char *) pu1Temp, "%d.%d.%d.%d:%d%2s",
                        (int *) (&au4ComVal[0]), (int *) (&au4ComVal[1]),
                        (int *) (&au4ComVal[2]), (int *) (&au4ComVal[3]),
                        (int *) (&au4ComVal[4]), &u1Temp);
        if (i4Len == 5)
        {
            for (u1Index = 0; u1Index < BGP4_IPV4_PREFIX_LEN; u1Index++)
            {
                if (au4ComVal[u1Index] > BGP4_ONE_BYTE_MAX_VAL)
                {
                    MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1Temp);
                    return BGP4_FAILURE;
                }
                *(pu1ExtCommValue + 2 + u1Index) = (UINT1) au4ComVal[u1Index];
            }
            if (au4ComVal[4] > BGP4_TWO_BYTE_MAX_VAL)
            {
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1Temp);
                return BGP4_FAILURE;
            }
            PTR_ASSIGN2 ((pu1ExtCommValue + 6), (UINT2) au4ComVal[4]);
            PTR_FETCH4 (u4Address, (pu1ExtCommValue + 2));
            if ((BGP4_IS_VALID_ADDRESS (u4Address) == 0) ||
                (BGP4_IS_BROADCAST_ADDRESS (u4Address) == 1))
            {
                MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1Temp);
                return BGP4_FAILURE;
            }
        }
        else
        {
            MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1Temp);
            return BGP4_FAILURE;
        }
    }
    if (u1ExtComType == BGP4_VPN4_ROUTE_DISTINGUISHER)
    {
        *pu1ExtCommValue = 0;
        *(pu1ExtCommValue + 1) = u1HighOrderType;
    }
    else
    {
        *pu1ExtCommValue = u1HighOrderType;
        if (u1ExtComType == BGP4_VPN4_RT_EXT_COM)
        {
            *(pu1ExtCommValue + 1) = BGP4_VPN4_RT_EXT_COM_LOW_ORDER_TYPE;
        }
        else if (u1ExtComType == BGP4_VPN4_SOO_EXT_COM)
        {
            *(pu1ExtCommValue + 1) = BGP4_VPN4_SOO_EXT_COM_LOW_ORDER_TYPE;
        }
    }

    MemReleaseMemBlock (gBgpNode.Bgp4BufNodeMsgPoolId, pu1Temp);
    return BGP4_SUCCESS;
}

INT4
Bgp4Vpnv4CopyExtCom (UINT1 *pu1DstExtCom, INT4 *pi4Length,
                     UINT1 *pu1ExtComm, UINT1 u1VpnExtComType)
{
    UINT2               u2HighOrderType;
    UINT1               u1Index;
    UINT1               u1DotIndex = 0;
    UINT2               u2TwoByte;
    UINT4               u4FourByte;
    INT4                i4Len = 0;
    INT4                i4NotationType = 0;

    if (pu1ExtComm == NULL)
    {
        return BGP4_FAILURE;
    }
    PTR_FETCH2 (u2HighOrderType, pu1ExtComm);
    if (u1VpnExtComType != BGP4_VPN4_ROUTE_DISTINGUISHER)
    {
        u2HighOrderType =
            (UINT2) (((u2HighOrderType & 0xff00) >> BGP4_ONE_BYTE_BITS));
    }
    if (u2HighOrderType == (UINT2) BGP4_VPN4_AS_EXT_COM_TYPE)
    {
        PTR_FETCH2 (u2TwoByte, (pu1ExtComm + PATH_ATTRIBUTE_TYPE_SIZE));
        SPRINTF ((CHR1 *) pu1DstExtCom, "%d", u2TwoByte);
        i4Len = (INT4) (STRLEN (pu1DstExtCom));

        *((pu1DstExtCom) + i4Len) = ':';
        PTR_FETCH4 (u4FourByte, (pu1ExtComm + PATH_ATTRIBUTE_TYPE_SIZE +
                                 BGP4_VPN4_EXT_COM_2BYTE_AS_TYPE_LEN));
        SPRINTF ((CHR1 *) (pu1DstExtCom + i4Len + 1), "%d", u4FourByte);

        *pi4Length = (INT4) (STRLEN (pu1DstExtCom));

        return BGP4_SUCCESS;

    }
    else if (u2HighOrderType == BGP4_VPN4_IPADDR_EXT_COM_TYPE)
    {
        for (u1Index = 0; u1Index < BGP4_VPN4_EXT_COM_IPADDR_LEN; u1Index++)
        {

            SPRINTF ((CHR1 *) (pu1DstExtCom + i4Len + u1DotIndex),
                     "%d", *(pu1ExtComm + u1Index + PATH_ATTRIBUTE_TYPE_SIZE));
            i4Len = (INT4) (STRLEN (pu1DstExtCom));

            if (u1Index < (BGP4_VPN4_EXT_COM_IPADDR_LEN - 1))
            {
                *(pu1DstExtCom + i4Len) = '.';
                u1DotIndex = 1;
            }
        }

        *((pu1DstExtCom) + i4Len) = ':';

        PTR_FETCH2 (u2TwoByte,
                    (pu1ExtComm + u1Index + PATH_ATTRIBUTE_TYPE_SIZE));
        SPRINTF ((CHR1 *) (pu1DstExtCom + i4Len + 1), "%d", u2TwoByte);

        *pi4Length = (INT4) (STRLEN (pu1DstExtCom));
        return BGP4_SUCCESS;

    }
    else if (u2HighOrderType == BGP4_VPN4_4BYTE_AS_EXT_COM_TYPE)
    {
        PTR_FETCH4 (u4FourByte, (pu1ExtComm + PATH_ATTRIBUTE_TYPE_SIZE));
        nmhGetFsbgp4FourByteASNotationType (&i4NotationType);

        BGP4_ASN_CONVERT_TO_STRING (i4NotationType,
                                    u4FourByte, (CHR1 *) pu1DstExtCom);

        i4Len = (INT4) (STRLEN (pu1DstExtCom));

        *((pu1DstExtCom) + i4Len) = ':';
        PTR_FETCH2 (u2TwoByte, (pu1ExtComm + PATH_ATTRIBUTE_TYPE_SIZE +
                                BGP4_VPN4_EXT_COM_4BYTE_LOCAL_ADMIN_LEN));
        SPRINTF ((CHR1 *) (pu1DstExtCom + i4Len + 1), "%d", u2TwoByte);

        *pi4Length = (INT4) (STRLEN (pu1DstExtCom));

        return BGP4_SUCCESS;
    }

    return BGP4_FAILURE;
}

/********************************************************************/
/* Function Name   : Bgp4Vpnv4GetNextRouteTargetFromVrf             */
/* Description     : Gets next route target pointer from a give vrf */
/* Input(s)        : pVrfInfo - vrf pointer                         */
/*                   pu1ExtComm - route target value                */
/*                   u1RTType - route target type (import/export)   */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : Next Route Target pointer            */
/********************************************************************/
tExtCommProfile    *
Bgp4Vpnv4GetNextRouteTargetFromVrf (UINT4 u4VrfId, UINT1 *pu1ExtComm,
                                    UINT1 u1RTType)
{
    tTMO_SLL           *pRTComList = NULL;
    tExtCommProfile    *pExtRTCom = NULL;
    UINT1               u1MatchFound = BGP4_FALSE;

    pRTComList = (u1RTType == BGP4_IMPORT_ROUTE_TARGET_TYPE) ?
        BGP4_VPN4_VRF_SPEC_IMPORT_TARGETS (u4VrfId) :
        BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS (u4VrfId);
    TMO_SLL_Scan (pRTComList, pExtRTCom, tExtCommProfile *)
    {
        if (u1MatchFound == BGP4_TRUE)
        {
            return (pExtRTCom);
        }
        if ((pExtRTCom->u1RowStatus == ACTIVE) &&
            ((MEMCMP (pExtRTCom->au1ExtComm,
                      pu1ExtComm, EXT_COMM_VALUE_LEN)) > 0))
        {
            u1MatchFound = BGP4_TRUE;
        }
    }
    return (NULL);
}

INT4
Bgp4Vpnv4GetFirstPathAttrTableIndex (tNetAddress * pRoutePrefix,
                                     tAddrPrefix * pPeerAddress)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tBgp4Info          *pRtInfo = NULL;

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (BGP4_DFLT_VRFID), pPeer,
                  tBgp4PeerEntry *)
    {
        if (bgp4RibLock () != OSIX_SUCCESS)
        {
            return BGP4_FAILURE;
        }

        /* Search for the IPV4 routes in RIB */
        pRtProfile = Bgp4RibhGetFirstAsafiPeerRtEntry (pPeer,
                                                       CAP_MP_VPN4_UNICAST);
        if (pRtProfile != NULL)
        {
            pRtInfo = BGP4_RT_BGP_INFO (pRtProfile);
            if (pRtInfo != NULL)
            {
                Bgp4CopyVpn4NetAddressFromRt (pRoutePrefix, pRtProfile);
                Bgp4CopyAddrPrefixStruct (pPeerAddress,
                                          BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
                bgp4RibUnlock ();
                return BGP4_SUCCESS;
            }
        }
        bgp4RibUnlock ();
    }
    return BGP4_FAILURE;
}

INT4
Bgp4Vpnv4GetFirstPeerPathAttrTableIndex (tNetAddress * pRoutePrefix,
                                         tAddrPrefix * pPeerAddress,
                                         tBgp4PeerEntry * pPeer)
{
    tRouteProfile      *pRtProfile = NULL;
    tBgp4Info          *pRtInfo = NULL;

    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return BGP4_FAILURE;
    }

    /* Search for the IPV4 routes in RIB */
    pRtProfile = Bgp4RibhGetFirstAsafiPeerRtEntry (pPeer, CAP_MP_VPN4_UNICAST);
    if (pRtProfile != NULL)
    {
        pRtInfo = BGP4_RT_BGP_INFO (pRtProfile);
        if (pRtInfo != NULL)
        {
            Bgp4CopyVpn4NetAddressFromRt (pRoutePrefix, pRtProfile);
            Bgp4CopyAddrPrefixStruct (pPeerAddress,
                                      BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
            bgp4RibUnlock ();
            return BGP4_SUCCESS;
        }
    }
    bgp4RibUnlock ();
    return BGP4_FAILURE;
}

INT4
Bgp4Vpnv4GetNextPathAttrTableIndex (tNetAddress RoutePrefix,
                                    tAddrPrefix PeerAddress,
                                    tNetAddress * pNextRoutePrefix,
                                    tAddrPrefix * pNextPeerAddress)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tBgp4PeerEntry     *pTmpPeer = NULL;
    tRouteProfile      *pFndRtProfile = NULL;
    tRouteProfile       RtProfile;
    tBgp4Info           RtInfo;
    INT4                i4RetVal;
    UINT1               u1PeerFound = BGP4_FALSE;

    Bgp4InitBgp4info (&RtInfo);

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (BGP4_DFLT_VRFID), pPeer,
                  tBgp4PeerEntry *)
    {
        if ((PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pPeer), PeerAddress))
            == BGP4_TRUE)
        {
            u1PeerFound = BGP4_TRUE;
            break;
        }
    }
    if (u1PeerFound == BGP4_FALSE)
    {
        return BGP4_FAILURE;
    }

    Bgp4CopyVpn4NetAddressToRt (&RtProfile, RoutePrefix);
    BGP4_RT_PEER_ENTRY ((&RtProfile)) = pPeer;
    BGP4_RT_BGP4_INF ((&RtProfile)) = &RtInfo;

    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return BGP4_FAILURE;
    }
    pFndRtProfile = Bgp4RibhGetNextPeerRtEntry (&RtProfile, pPeer);
    bgp4RibUnlock ();
    if (pFndRtProfile != NULL)
    {
        Bgp4CopyVpn4NetAddressFromRt (pNextRoutePrefix, pFndRtProfile);
        Bgp4CopyAddrPrefixStruct (pNextPeerAddress,
                                  BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
        return BGP4_SUCCESS;
    }
    else
    {
        pTmpPeer =
            (tBgp4PeerEntry *)
            TMO_SLL_Next (BGP4_PEERENTRY_HEAD (BGP4_DFLT_VRFID),
                          (tTMO_SLL_NODE *) pPeer);
        while (pTmpPeer)
        {
            pPeer = pTmpPeer;
            /* First get the IPV4 route */
            i4RetVal =
                Bgp4Vpnv4GetFirstPeerPathAttrTableIndex (pNextRoutePrefix,
                                                         pNextPeerAddress,
                                                         pPeer);
            if (i4RetVal == BGP4_SUCCESS)
            {
                return BGP4_SUCCESS;
            }
            pTmpPeer =
                (tBgp4PeerEntry *)
                TMO_SLL_Next (BGP4_PEERENTRY_HEAD (BGP4_DFLT_VRFID),
                              (tTMO_SLL_NODE *) pPeer);

        }
    }

    return BGP4_FAILURE;
}

INT4
Bgp4Vpn4GetNextIpv4PathAttrTableIndex (tNetAddress RoutePrefix,
                                       tAddrPrefix PeerAddress,
                                       tNetAddress * pNextRoutePrefix,
                                       tAddrPrefix * pNextPeerAddress)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tBgp4PeerEntry     *pTmpPeer = NULL;
    tBgp4Info           RtInfo;
    tRouteProfile      *pFndRtProfile = NULL;
    tRouteProfile       RtProfile;
    UINT1               u1PeerFound = BGP4_FALSE;
    UINT1               u1MatchFound = BGP4_FALSE;

    Bgp4InitBgp4info (&RtInfo);

    TMO_SLL_Scan (BGP4_PEERENTRY_HEAD (BGP4_DFLT_VRFID), pPeer,
                  tBgp4PeerEntry *)
    {
        if ((PrefixMatch (BGP4_PEER_REMOTE_ADDR_INFO (pPeer), PeerAddress))
            == BGP4_TRUE)
        {
            u1PeerFound = BGP4_TRUE;
            break;
        }
    }
    if (u1PeerFound == BGP4_FALSE)
    {
        return BGP4_FAILURE;
    }

    Bgp4CopyVpn4NetAddressToRt (&RtProfile, RoutePrefix);
    BGP4_RT_PEER_ENTRY ((&RtProfile)) = pPeer;
    BGP4_RT_BGP4_INF ((&RtProfile)) = &RtInfo;

    if (bgp4RibLock () != OSIX_SUCCESS)
    {
        return BGP4_FAILURE;
    }
    pFndRtProfile = Bgp4RibhGetNextIpv4PeerRtEntry (&RtProfile, pPeer);
    bgp4RibUnlock ();
    if (pFndRtProfile == NULL)
    {
        /* Get the next peer from peer list */
        pTmpPeer =
            (tBgp4PeerEntry *)
            TMO_SLL_Next (BGP4_PEERENTRY_HEAD (BGP4_DFLT_VRFID),
                          (tTMO_SLL_NODE *) pPeer);
        u1MatchFound = (UINT1)
            (Bgp4GetNextVpn4RouteProfile (pNextRoutePrefix, pNextPeerAddress,
                                          pTmpPeer));
        if (u1MatchFound == BGP4_TRUE)
        {
            return BGP4_SUCCESS;
        }
        else
        {
            return BGP4_FAILURE;
        }
    }
    else
    {
        Bgp4CopyVpn4NetAddressFromRt (pNextRoutePrefix, pFndRtProfile);
        Bgp4CopyAddrPrefixStruct (pNextPeerAddress,
                                  BGP4_PEER_REMOTE_ADDR_INFO (pPeer));
        return BGP4_SUCCESS;
    }
}

INT1
Bgp4GetNextVpn4RouteProfile (tNetAddress * pNextRoutePrefix,
                             tAddrPrefix * pNextPeerAddress,
                             tBgp4PeerEntry * pPeer)
{
    tBgp4PeerEntry     *pNextPeer = NULL;
    INT1                i1RetVal;
    INT4                i4RetVal;

    if (pPeer == NULL)
    {
        return BGP4_FALSE;
    }
    /* First get the IPV4 route */
    i4RetVal = Bgp4Vpnv4GetFirstPeerPathAttrTableIndex (pNextRoutePrefix,
                                                        pNextPeerAddress,
                                                        pPeer);
    if (i4RetVal == BGP4_SUCCESS)
    {
        return BGP4_TRUE;
    }

    /* Get the next peer from peer list */
    pNextPeer =
        (tBgp4PeerEntry *) TMO_SLL_Next (BGP4_PEERENTRY_HEAD (BGP4_DFLT_VRFID),
                                         (tTMO_SLL_NODE *) pPeer);
    i1RetVal =
        Bgp4GetNextVpn4RouteProfile (pNextRoutePrefix, pNextPeerAddress,
                                     pNextPeer);
    return (i1RetVal);
}

#endif
