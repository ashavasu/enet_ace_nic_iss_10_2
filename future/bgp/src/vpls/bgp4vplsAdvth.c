/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved             *
 *                                                                  *
 * $Id: bgp4vplsAdvth.c,v 1.5 2014/11/20 12:12:13 siva Exp $         *
 *                                                                  *
 * Description: These routines are part of the VPLS Handler which   *
 *              handles VPLS route advertised by L2VPN or received  *
 *              by other BGP peer.                                  *
 ********************************************************************/

#ifndef _BGP4VPLSADVTH_C
#define _BGP4VPLSADVTH_C

#include "bgp4com.h"
#include "bgp4vpls.h"

#ifdef MPLS_WANTED
/*****************************************************************************/
/* FUNCTION NAME : Bgp4VplsMPAdvtHandler                                     */
/* DESCRIPTION   : This function handles the advertisement of VPLS routes    */
/*                 from L2VPN and interact with RIB handler module for       */
/*                 creation and sending of UPDATE message to VPLS BGP peers. */
/* INPUTS        : pPeerRouteAdvtInfo: Pointer to the peer information that  */
/*                 contains the advt.info, extended community info (rouet    */
/*                 target), layer2 extended community info (control flag and */
/*                 MTU)                                                      */
/*                 u1DestType: Unicast, Broadcast                            */
/* OUTPUTS       : None                                                      */
/* Return        : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/

INT4
Bgp4VplsMPAdvtHandler (tVplsAdvtInfo * pPeerRouteAdvtInfo, UINT4 u4VplsIndex,
                       UINT4 u4Context)
{
    tBgp4PeerEntry     *pPeer = NULL;
    tBgp4Info          *pBGP4Info = NULL;
    INT4                i4RetValue = BGP4_FAILURE;
    tRouteProfile      *pRtprofile = NULL;
    tPeerRouteAdvtInfo *pAdvtInfo = NULL;
    tVplsNlriInfo       VplsNlri;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    pAdvtInfo = &(pPeerRouteAdvtInfo->PeerRouteAdvtInfo);

    /*create BGP path attribute Info */
    pBGP4Info = Bgp4MemAllocateBgp4info (sizeof (tBgp4Info));
    if (pBGP4Info == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_VPLS_CRI_TRC, BGP4_MOD_NAME,
                  "\t%s:%d Memory Allocation for Route's BGP-Info FAILED!!!\n",
                  __func__, __LINE__);
        return (BGP4_RSRC_ALLOC_FAIL);
    }
    BGP4_INFO_REF_COUNT (pBGP4Info)++;

    if (BGP4_FAILURE ==
        Bgp4VplsGetPathAttrinfo (pPeerRouteAdvtInfo, pBGP4Info, u4Context))
    {
        Bgp4DshReleaseBgpInfo (pBGP4Info);
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                  BGP4_MOD_NAME, "\t!!! Path Attribute Info not found!!!\n");
        return BGP4_FAILURE;
    }

    VplsNlri.u2Length = BGP4_VPLS_NLRI_PREFIX_LEN;
    MEMCPY (VplsNlri.au1RouteDistinguisher,
            pAdvtInfo->au1RouteDistinguisher, MAX_LEN_RD_VALUE);
    VplsNlri.u2VeId = pAdvtInfo->u2VeId;
    VplsNlri.u2VeBaseOffset = pAdvtInfo->u2VeBaseOffset;
    VplsNlri.u2VeBaseSize = pAdvtInfo->u2VeBaseSize;
    VplsNlri.u4labelBase = pAdvtInfo->u4labelBase;

    /* create route profile Information here */
    pRtprofile = Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
    if (pRtprofile == NULL)
    {
        Bgp4DshReleaseBgpInfo (pBGP4Info);
        /*return (BGP4_RSRC_ALLOC_FAIL); */
        return BGP4_FAILURE;
    }

    MEMSET (pRtprofile, 0, sizeof (tRouteProfile));

    i4RetValue =
        Bgp4VplsConstructRouteProfile (u4Context, pRtprofile, pPeer, &VplsNlri,
                                       u4VplsIndex);
    if (i4RetValue == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_VPLS_CRI_TRC, BGP4_MOD_NAME,
                  "\t!!!!!BGP Context node not created !!!!\n");
        Bgp4DshReleaseBgpInfo (pBGP4Info);
        Bgp4MemReleaseRouteProfile (pRtprofile);
        return BGP4_FAILURE;
    }
    BGP4_LINK_INFO_TO_PROFILE (pBGP4Info, pRtprofile);

    /* Needs to add the route to REDISTRIBUTION update list. */
    BGP4_RT_SET_FLAG (pRtprofile, BGP4_RT_IN_REDIST_LIST);

    if (BGP4_FAILURE ==
        Bgp4DshAddRouteToList (BGP4_REDIST_LIST (u4Context), pRtprofile, 0))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_VPLS_CRI_TRC, BGP4_MOD_NAME,
                  "\t!!!!!BGP Route not added to REDIST List !!!!\n");
        Bgp4DshReleaseBgpInfo (pBGP4Info);
        Bgp4MemReleaseRouteProfile (pRtprofile);
        return BGP4_FAILURE;
    }

    KW_FALSEPOSITIVE_FIX1 (pRtprofile);
    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME : Bgp4VplsConstructRouteProfile                             */
/* DESCRIPTION   : This function fills the route profile for the VPLS route  */
/*                 profile                                                   */
/* INPUTS        : pBGP4Info: Pointer to the route profile structue          */
/* OUTPUTS       : None                                                      */
/* Return        : BGP4_TRUE/BGP4_FALSE                                      */
/*****************************************************************************/
INT4
Bgp4VplsConstructRouteProfile (UINT4 u4Context,
                               tRouteProfile * pRtprofile,
                               tBgp4PeerEntry * pPeer,
                               tVplsNlriInfo * VplsNlri, UINT4 u4VplsIndex)
{
    tBgpCxtNode        *pBgpCxtNode = NULL;
    UINT1              *pNlri = NULL;
    UINT1               au1LabelBase[BGP4_VPLS_NLRI_LB_LEN + 1];

    UNUSED_PARAM (pPeer);

    pBgpCxtNode = Bgp4GetContextEntry (u4Context);

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    if (NULL == pBgpCxtNode)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                  BGP4_MOD_NAME, "\t%s:%d !!!Context entry not found !!!\n",
                  __func__, __LINE__);
        return BGP4_FAILURE;
    }

    pRtprofile->pBgpCxtNode = pBgpCxtNode;

    BGP4_RT_PROTOCOL (pRtprofile) = BGP4_LOCAL_ID;
    BGP4_RT_PREFIXLEN (pRtprofile) = BGP4_VPLS_NLRI_PREFIX_LEN;

    BGP4_RT_AFI_INFO (pRtprofile) = BGP4_INET_AFI_L2VPN;

    BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO
        (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
         (BGP4_RT_NET_ADDRESS_INFO (pRtprofile))) = BGP4_VPLS_NLRI_PREFIX_LEN;

    BGP4_RT_SAFI_INFO (pRtprofile) = BGP4_INET_SAFI_VPLS;
    pNlri = BGP4_RT_IP_PREFIX (pRtprofile);

    MEMCPY (pNlri, (UINT1 *) (VplsNlri->au1RouteDistinguisher),
            MAX_LEN_RD_VALUE);

    pNlri += MAX_LEN_RD_VALUE;

    PTR_ASSIGN2 (pNlri, VplsNlri->u2VeId);
    pNlri += BGP4_VPLS_NLRI_VEID_LEN;
    PTR_ASSIGN2 (pNlri, VplsNlri->u2VeBaseOffset);
    pNlri += BGP4_VPLS_NLRI_VBO_LEN;
    PTR_ASSIGN2 (pNlri, VplsNlri->u2VeBaseSize);
    pNlri += BGP4_VPLS_NLRI_VBS_LEN;

    /*MEMCPY(pNlri , &(VplsNlri->u4labelBase), BGP4_VPLS_NLRI_LB_LEN); */

    VplsNlri->u4labelBase = OSIX_NTOHL (VplsNlri->u4labelBase);
    MEMCPY (au1LabelBase, &(VplsNlri->u4labelBase), sizeof (UINT4));

    MEMCPY (pNlri, &(au1LabelBase[1]), BGP4_VPLS_NLRI_LB_LEN);

    BGP4_VPLS_RT_LEN (pRtprofile) = VplsNlri->u2Length;
    MEMCPY (BGP4_VPLS_RT_ROUTE_DISTING (pRtprofile),
            VplsNlri->au1RouteDistinguisher, MAX_LEN_RD_VALUE);
    BGP4_VPLS_RT_VE_ID (pRtprofile) = VplsNlri->u2VeId;
    BGP4_VPLS_RT_VBS (pRtprofile) = VplsNlri->u2VeBaseSize;
    BGP4_VPLS_RT_VBO (pRtprofile) = VplsNlri->u2VeBaseOffset;
    BGP4_VPLS_RT_LB (pRtprofile) = VplsNlri->u4labelBase;

    if (BGP4_FAILURE ==
        Bgp4VplsGetVplsInfoFromIndex (u4VplsIndex,
                                      &(pRtprofile->pVplsSpecInfo)))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                  BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);
        return BGP4_FAILURE;
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return BGP4_SUCCESS;
}

#endif

/*****************************************************************************/
/* FUNCTION NAME : Bgp4VplsGetPathAttrinfo                                   */
/* DESCRIPTION   : This function fills the BGP_INFO for the VPLS route       */
/*                 profile                                                   */
/* INPUTS        : pBGP4Info: Pointer to the bgpInfo structue                */
/* OUTPUTS       : None                                                      */
/* Return        : BGP4_TRUE/BGP4_FALSE                                      */
/*****************************************************************************/
INT4
Bgp4VplsGetPathAttrinfo (tVplsAdvtInfo * pPeerRouteAdvtInfo,
                         tBgp4Info * pBGP4Info, UINT4 u4Context)
{

    UINT1              *pu1Str = NULL;
    tExtCommunity      *pExtComm = NULL;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    /* ORIGIN Path Attribute : for IBGP VPLS  IGP->0, however not learned 
       via any IGP */
    BGP4_INFO_ORIGIN (pBGP4Info) = BGP4_ATTR_ORIGIN_IGP;
    BGP4_INFO_ATTR_FLAG (pBGP4Info) |= BGP4_ATTR_ORIGIN_MASK;

    /* AS-PATH Path Attribute : Speaker is Originating this UPDATE message to internal
     * it should include empty AS-PATH attribute, set the flag here */

    BGP4_INFO_ATTR_FLAG (pBGP4Info) |= BGP4_ATTR_PATH_MASK;

    /* Local Pref Attribute */
    BGP4_INFO_ATTR_FLAG (pBGP4Info) |= BGP4_ATTR_LOCAL_PREF_MASK;
    BGP4_INFO_RCVD_LOCAL_PREF (pBGP4Info) = BGP4_DEFAULT_LOCAL_PREF (u4Context);

    /* Fill Extended Community */
    EXT_COMMUNITY_NODE_CREATE (pExtComm);
    if (pExtComm == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_VPLS_CRI_TRC, BGP4_MOD_NAME,
                  "\tMemory Allocation for Extended Community Attribute "
                  "FAILED!!!\n");
        return (EXT_COMM_FAILURE);
    }

    pExtComm->pu1EcommVal = NULL;
    pExtComm->u2EcommCnt = 0;
    pExtComm->u1Flag = 0;
    BGP4_INFO_ECOMM_ATTR (pBGP4Info) = pExtComm;

    ATTRIBUTE_NODE_CREATE (pu1Str);
    if (pu1Str == NULL)
    {
        EXT_COMMUNITY_NODE_FREE (pExtComm);
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_VPLS_CRI_TRC, BGP4_MOD_NAME,
                  "\t!!!!Memory Allocation for Extended Community Attribute FAILED!!!!\n");
        /*return (BGP4_RSRC_ALLOC_FAIL); */
        return BGP4_FAILURE;
    }

    BGP4_INFO_ECOMM_COUNT (pBGP4Info) = L2VPN_EXT_COMM_COUNT /*2 */ ;
    BGP4_INFO_ECOMM_ATTR_COST_FLAG (pBGP4Info) = BGP4_FALSE;

    MEMCPY (pu1Str, pPeerRouteAdvtInfo->ExtendedCommInfo.au1RouteTarget,
            EXT_COMM_VALUE_LEN);
    MEMCPY (pu1Str + EXT_COMM_VALUE_LEN,
            &(pPeerRouteAdvtInfo->ExtCommLayer2Info), EXT_COMM_VALUE_LEN);

    BGP4_INFO_ECOMM_ATTR_VAL (pBGP4Info) = pu1Str;
    BGP4_INFO_ATTR_FLAG (pBGP4Info) |= (BGP4_ATTR_ECOMM_MASK);
    BGP4_INFO_ECOMM_ATTR_COST_FLAG (pBGP4Info) = BGP4_FALSE;

    pBGP4Info->u1BgpInfoSafi = BGP4_INET_SAFI_VPLS;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return BGP4_SUCCESS;
}

#ifdef MPLS_WANTED
/*****************************************************************************/
/* FUNCTION NAME : Bgp4VplsCreateAdvtMsgforL2VPN                             */
/* DESCRIPTION   : This function create L2VPN_BGP_ADVERTISEMENT and send it  */
/*                 to L2VPN                                                  */
/* INPUTS        : pRtProfile Route Profile                                  */
/* OUTPUTS       : None                                                      */
/* Return        : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4VplsCreateAdvtMsgforL2VPN (tRouteProfile * pRtProfile)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    UINT4               u4RetVal;
    tL2VpnBgpEvtInfo    L2vpnEvntInfo;
    tExtCommLayer2Info  ExtCommInfo;
    tVplsSpecInfo      *pVplsSpecInfo = NULL;
    UINT1               au1RouteTarget[MAX_LEN_RT_VALUE];
    BOOL1               bControlWordFlag = ECOM_L2VPN_CW_ENABLED;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    MEMSET (&L2vpnEvntInfo, 0, sizeof (L2vpnEvntInfo));
    MEMSET (&ExtCommInfo, 0, sizeof (ExtCommInfo));

    /* In case of Advt message, the information send to L2VPN
     * is MessageType (Advt Message), VE-ID,, RD, LB, VBO, VBS
     * RT, MTU, Control word and Peer Address*/

    pVplsSpecInfo = (BGP4_VPLS_RT_VPLS_SPEC_INFO (pRtProfile));
    if (pVplsSpecInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                  BGP4_MOD_NAME,
                  "\t%s:%d !!![ BGP-L2VPN Advertise] - VPLS information is NULL in the VPLS route !!!\n",
                  __func__, __LINE__);
        return BGP4_FAILURE;
    }
    L2vpnEvntInfo.u4VplsIndex = pVplsSpecInfo->u4VplsIndex;

    STRNCPY (L2vpnEvntInfo.au1VplsName, pVplsSpecInfo->au1VplsName,
             STRNLEN (pVplsSpecInfo->au1VplsName,
                      BGP4_VPLS_MAX_VPLS_NAME_SIZE));

    /* copying VE-ID, RD, LB, VBO, VBS from NLRI */
    L2vpnEvntInfo.u2VeId = BGP4_VPLS_RT_VE_ID (pRtProfile);
    MEMCPY (L2vpnEvntInfo.au1RouteDistinguisher,
            BGP4_VPLS_RT_ROUTE_DISTING (pRtProfile), MAX_LEN_RD_VALUE);
    L2vpnEvntInfo.u4LabelBase = BGP4_VPLS_RT_LB (pRtProfile);
    L2vpnEvntInfo.u2VeBaseOffset = BGP4_VPLS_RT_VBO (pRtProfile);
    L2vpnEvntInfo.u2VeBaseSize = BGP4_VPLS_RT_VBS (pRtProfile);

    /*Message Type */
    L2vpnEvntInfo.u4MsgType = L2VPN_BGP_ADVERTISE_MSG;

    /*Peer Address */
    if (BGP4_PEER_REMOTE_ADDR_TYPE(BGP4_RT_PEER_ENTRY(pRtProfile)) == BGP4_INET_AFI_IPV4)
    {
        L2vpnEvntInfo.u4IpAddr.u4AddrType = BGP4_INET_AFI_IPV4;
        MEMCPY (&(L2vpnEvntInfo.u4IpAddr.uIpAddr.Ip4Addr),
                BGP4_INFO_NEXTHOP (BGP4_RT_BGP_INFO (pRtProfile)),
                BGP4_IPV4_PREFIX_LEN);
    }
#ifdef BGP4_IPV6_WANTED
    else
    {
        L2vpnEvntInfo.u4IpAddr.u4AddrType = BGP4_INET_AFI_IPV6;
        MEMCPY (&(L2vpnEvntInfo.u4IpAddr.uIpAddr.Ip6Addr.u1_addr),
                BGP4_INFO_NEXTHOP (BGP4_RT_BGP_INFO (pRtProfile)),
                BGP4_IPV6_PREFIX_LEN); 
    }
#endif

    /*RT, MTU, Control word */
    i4RetVal = Bgp4VplsGetRTfromRouteProfile (pRtProfile, au1RouteTarget);
    if (i4RetVal == BGP4_SUCCESS)
    {
        MEMCPY ((L2vpnEvntInfo.au1RouteTarget), au1RouteTarget,
                MAX_LEN_RT_VALUE);
    }

    i4RetVal = Bgp4VplsGetL2ExtCommInfo (pRtProfile, &ExtCommInfo);
    if (i4RetVal == BGP4_SUCCESS)
    {
        if ((ExtCommInfo.bControlWordFlag & ECOM_CONTROL_WORD_ENABLE)
            == ECOM_CONTROL_WORD_ENABLE)
        {
            bControlWordFlag = ECOM_L2VPN_CW_ENABLED;
        }
        else
        {
            bControlWordFlag = ECON_L2VPN_CW_DISABLED;
        }
        L2vpnEvntInfo.bControlWordFlag = bControlWordFlag;
        L2vpnEvntInfo.u2Mtu = (UINT4) ExtCommInfo.u2Mtu;
    }

    /*This API is used by BGP to send messages to L2VPN queue. */
    u4RetVal = L2VpnBgpEventHandler (&L2vpnEvntInfo);
    if (u4RetVal == L2VPN_FAILURE)
    {
        i4RetVal = BGP4_FAILURE;
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                  BGP4_MOD_NAME,
                  "\t%s:%d !!! Sending message to L2VPN interface failed !!!\n",
                  __func__, __LINE__);
    }
    else
    {
        i4RetVal = BGP4_SUCCESS;
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return i4RetVal;
}

/*****************************************************************************/
/* FUNCTION NAME : Bgp4VplsGetRTfromRouteProfile                             */
/* DESCRIPTION   : This function gets the Route Target Extended community    */
/*                 info from route profile                                   */
/* INPUTS        : pRouteProfile route profile                               */
/*                 au1RouteTarget RouteTarget Extended community Info        */
/* OUTPUTS       : None                                                      */
/* Return        : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4VplsGetRTfromRouteProfile (tRouteProfile * pRouteProfile,
                               UINT1 *au1RouteTarget)
{
    tBgp4Info          *pRouteInfo = NULL;
    UINT2               u2ExtCommType = 0;
    UINT4               u4Index;
    UINT1               u1HighOrderByte;
    UINT1               u1LowOrderByte;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    pRouteInfo = BGP4_RT_BGP_INFO (pRouteProfile);

    /* Check for the presence of ext-comm  path attribute */
    if (BGP4_INFO_ECOMM_ATTR (pRouteInfo) == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                  BGP4_MOD_NAME,
                  "\t%s:%d !!! Ext-Community path attribute NOT present !!!\n",
                  __func__, __LINE__);

        /* Ext-Community path attribute NOT present */
        return BGP4_FAILURE;
    }

    /* Process all the Ext-communities */
    for (u4Index = 0; u4Index < BGP4_INFO_ECOMM_COUNT (pRouteInfo); u4Index++)
    {
        MEMCPY ((UINT1 *) au1RouteTarget,
                (BGP4_INFO_ECOMM_ATTR_VAL (pRouteInfo) +
                 (u4Index * EXT_COMM_VALUE_LEN)), EXT_COMM_VALUE_LEN);
        PTR_FETCH2 (u2ExtCommType, au1RouteTarget);

        u1HighOrderByte = (UINT1) (u2ExtCommType >> BGP4_ONE_BYTE_BITS);
        u1LowOrderByte =
            (UINT1) (u2ExtCommType & EXT_COMM_LOW_ORDER_OCTET_MASK);

        if (u1LowOrderByte == EXT_COMM_LOW_ORDER_BYTE_TYPE_02)
        {

            if ((u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_00) ||
                (u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_01) ||
                (u1HighOrderByte == EXT_COMM_HIGH_ORDER_BYTE_TYPE_02))
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                          BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);
                return BGP4_SUCCESS;
            }

        }
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return BGP4_FAILURE;
}

/*****************************************************************************/
/* FUNCTION NAME : Bgp4VplsGetL2ExtCommInfo                                  */
/* DESCRIPTION   : This function gets the layer2 Extended community info from*/
/*                 route profile                                             */
/* INPUTS        : pRouteProfile route profile                               */
/*                 pExtCommInfo Layer2 Extended community Info               */
/* OUTPUTS       : None                                                      */
/* Return        : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4VplsGetL2ExtCommInfo (tRouteProfile * pRouteProfile,
                          tExtCommLayer2Info * pExtCommInfo)
{
    tBgp4Info          *pRouteInfo = NULL;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT2               u2ExtCommType = 0;
    UINT1               u1EncapsType = 0;
    UINT4               u4Index;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    pRouteInfo = BGP4_RT_BGP_INFO (pRouteProfile);

    /* Check for the presence of ext-comm  path attribute */
    if (BGP4_INFO_ECOMM_ATTR (pRouteInfo) == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                  BGP4_MOD_NAME,
                  "\t%s:%d !!! Ext-Community path attribute NOT present !!!\n",
                  __func__, __LINE__);

        /* Ext-Community path attribute NOT present */
        return BGP4_FAILURE;
    }

    /* Process all the Ext-communities */
    for (u4Index = 0; u4Index < BGP4_INFO_ECOMM_COUNT (pRouteInfo); u4Index++)
    {
        MEMCPY ((UINT1 *) au1ExtComm,
                (BGP4_INFO_ECOMM_ATTR_VAL (pRouteInfo) +
                 (u4Index * EXT_COMM_VALUE_LEN)), EXT_COMM_VALUE_LEN);
        PTR_FETCH_2 (u2ExtCommType, au1ExtComm);
        u2ExtCommType = OSIX_NTOHS (u2ExtCommType);
        u1EncapsType = (UINT1) *(au1ExtComm + BGP4_VPLS_L2ECOM_TYPE_SIZE);

        if ((u2ExtCommType == ECOMM_VALUE_LAYER2_INFO) &&
            (u1EncapsType == ECOMM_LAYER2_INFO_ENCAPS_TYPE))
        {
            pExtCommInfo->extCommType = u2ExtCommType;
            pExtCommInfo->encapsType = u1EncapsType;
            pExtCommInfo->bControlWordFlag =
                (BOOL1) * (au1ExtComm + BGP4_VPLS_L2ECOM_TYPE_SIZE +
                           BGP4_VPLS_L2ECOM_ENCAP_TPYE_SIZE);
            pExtCommInfo->u2Mtu =
                OSIX_NTOHS ((UINT2)
                            *((UINT2 *) (VOID *) (au1ExtComm +
                                                  BGP4_VPLS_L2ECOM_TYPE_SIZE +
                                                  BGP4_VPLS_L2ECOM_ENCAP_TPYE_SIZE
                                                  +
                                                  BGP4_VPLS_L2ECOM_CTRL_FLAG_SIZE)));
            pExtCommInfo->u2Reserved =
                (UINT2)
                *((UINT2 *) (VOID *) (au1ExtComm + BGP4_VPLS_L2ECOM_TYPE_SIZE +
                                      BGP4_VPLS_L2ECOM_ENCAP_TPYE_SIZE +
                                      BGP4_VPLS_L2ECOM_CTRL_FLAG_SIZE +
                                      BGP4_VPLS_L2ECOM_MTU_SIZE));

            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                      BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

            return BGP4_SUCCESS;
        }
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return BGP4_FAILURE;
}

/********************************************************************/
/* Function Name   : Bgp4AdminStatusEventToL2Vpn                    */
/* Description     : This function sent the BGP Admin Up/Down event */
/*                   to L2VPN Module                                */
/* Input(s)        : u4AdminStatus - Admin Status                   */
/* Output(s)       : None.                                          */
/* Use of Recursion: None.                                          */
/* Returns         : BGP4_SUCCESS or BGP4_FAILURE                   */
/********************************************************************/
INT4
Bgp4AdminStatusEventToL2Vpn (UINT4 u4AdminStatus)
{
    tL2VpnBgpEvtInfo    L2vpnEvntInfo;
    INT4                i4RetVal = BGP4_SUCCESS;
    UINT4               u4RetVal;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    /*Message Type */
    if (u4AdminStatus == BGP4_ADMIN_UP)
    {
        L2vpnEvntInfo.u4MsgType = L2VPN_BGP_ADMIN_UP;
    }
    else
    {
        /*BGP4_ADMIN_DOWN */
        L2vpnEvntInfo.u4MsgType = L2VPN_BGP_ADMIN_DOWN;
    }

    /*This API is used by BGP to send messages to L2VPN queue. */
    u4RetVal = L2VpnBgpEventHandler (&L2vpnEvntInfo);
    if (u4RetVal == L2VPN_FAILURE)
    {
        i4RetVal = BGP4_FAILURE;
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                  BGP4_MOD_NAME,
                  "\t%s:%d !!! Sending message to L2VPN interface failed !!!\n",
                  __func__, __LINE__);
    }
    else
    {
        i4RetVal = BGP4_SUCCESS;
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return i4RetVal;
}
#endif /*MPLS_WANTED */

#endif /* _BGP4VPLSADVTH_C */
