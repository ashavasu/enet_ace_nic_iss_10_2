/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved             *
 *                                                                  *
 * $Id: bgp4vplsGr.c,v 1.1 2014/10/14 12:14:23 siva Exp $           *
 *                                                                  *
 * Description: These routines are part of the VPLS Handler which   *
 *              interfaces with L2VPN and different modules of BGP4 *
 ********************************************************************/
#ifndef _BGP4VPLSGR_C
#define _BGP4VPLSGR_C

#include "bgp4com.h"

#ifdef VPLS_GR_WANTED
#ifdef MPLS_WANTED

/*****************************************************************************/
/*  Function Name   :  Bgp4VplsGRInProgress                                  */
/*  Description     :  This function is used to notify MPLS-L2VPN that BGP   */
/*                     module comes up after GR. This function is also called*/
/*                     whenever switchover happens                           */
/*  Input(s)        :  u4CtxId - Context ID                                  */
/*  Output(s)       :  None                                                  */
/*  Returns         :  BGP4_SUCCESS                                          */
/*****************************************************************************/
INT4
Bgp4VplsGRInProgress (UINT4 u4CtxId)
{

    INT4                i4RetVal;
    UINT4               u4RetVal;
    tL2VpnBgpEvtInfo    L2vpnEvntInfo;

    MEMSET (&L2vpnEvntInfo, 0, sizeof (L2vpnEvntInfo));

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    /*Message Type */
    L2vpnEvntInfo.u4MsgType = L2VPN_BGP_GR_IN_PROGRESS;
    /*Stale timer*/
    L2vpnEvntInfo.u4TimerInterval = BGP4_STALE_PATH_INTERVAL(u4CtxId);
    if (BGP4_RESTART_EXIT_REASON (u4CtxId) == BGP4_GR_EXIT_FAILURE)
     {
        /*late restart after graceful shutdown , instant cleanup */
        L2vpnEvntInfo.u4TimerInterval = 0;
     }

    /*This API is used by BGP to send messages to L2VPN queue. */
    u4RetVal = L2VpnBgpEventHandler (&L2vpnEvntInfo);

    if (u4RetVal == L2VPN_FAILURE)
    {
        i4RetVal = BGP4_FAILURE;
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                  BGP4_MOD_NAME,
                  "\t%s:%d !!! Sending GR_IN_PROGRESS message to L2VPN interface failed !!!\n",
                  __func__, __LINE__);
    }
    else
    {
        i4RetVal = BGP4_SUCCESS;
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return i4RetVal;
}

/*****************************************************************************/
/*  Function Name   : Bgp4VplsGREORFromL2VPN                                 */
/*  Description     : This function sets the global EOVPLS flag after getting*/
/*                    EOVPLS from MPLS-L2VPN                                 */
/*  Input(s)        : u4CtxId - Context ID                                   */
/*  Output(s)       : None                                                   */
/*  Returns         : BGP4_SUCCESS                                           */
/*****************************************************************************/
INT4
Bgp4VplsGREORFromL2VPN (UINT4 u4CtxId)
{
    BGP4_EOVPLS_FLAG(u4CtxId) = BGP4_TRUE;
    return BGP4_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : Bgp4VplsNotifyGRToL2VPN                                */
/*  Description     : This function is used to notify MPLS-L2VPN when BGP    */
/*                    module restarts.                                       */
/*  Input(s)        : u4CtxId - Context ID                                   */
/*  Output(s)       : None                                                   */
/*  Returns         : BGP4_SUCCESS                                           */
/*****************************************************************************/
INT4
Bgp4VplsNotifyGRToL2VPN (UINT4 u4CtxId)
{

    INT4                i4RetVal;
    UINT4               u4RetVal;
    tL2VpnBgpEvtInfo    L2vpnEvntInfo;

    UNUSED_PARAM (u4CtxId);
    MEMSET (&L2vpnEvntInfo, 0, sizeof (L2vpnEvntInfo));
    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    /*Message Type */
    L2vpnEvntInfo.u4MsgType = L2VPN_BGP_GR_START;

    /*This API is used by BGP to send messages to L2VPN queue. */
    u4RetVal = L2VpnBgpEventHandler (&L2vpnEvntInfo);

    if (u4RetVal == L2VPN_FAILURE)
    {
        i4RetVal = BGP4_FAILURE;
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                  BGP4_MOD_NAME,
                  "\t%s:%d !!! Sending GR_START message to L2VPN interface failed !!!\n",
                  __func__, __LINE__);
    }
    else
    {
        i4RetVal = BGP4_SUCCESS;
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return i4RetVal;
}


/*****************************************************************************/
/*  Function Name   : Bgp4VplsNotifyRestartToL2VPN                           */
/*  Description     : This function is used to notify MPLS-L2VPN about BGP   */ 
/*                    module restart if GR Capability is not enabled.        */
/*  Input(s)        : u4CtxId - Context ID                                   */
/*  Output(s)       : None                                                   */
/*  Returns         : BGP4_SUCCESS                                           */
/*****************************************************************************/
INT4
Bgp4VplsNotifyRestartToL2VPN (UINT4 u4CtxId)
{

    INT4                i4RetVal;
    UINT4               u4RetVal;
    tL2VpnBgpEvtInfo    L2vpnEvntInfo;

    UNUSED_PARAM (u4CtxId);
    MEMSET (&L2vpnEvntInfo, 0, sizeof (L2vpnEvntInfo));
    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    /*Message Type */
    L2vpnEvntInfo.u4MsgType = L2VPN_BGP_MODULE_DOWN;

    /*This API is used by BGP to send messages to L2VPN queue. */
    u4RetVal = L2VpnBgpEventHandler (&L2vpnEvntInfo);

    if (u4RetVal == L2VPN_FAILURE)
    {
        i4RetVal = BGP4_FAILURE;
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                  BGP4_MOD_NAME,
                  "\t%s:%d !!! Sending BGP_MODULE_DOWN message to L2VPN interface failed !!!\n",
                  __func__, __LINE__);
    }
    else
    {
        i4RetVal = BGP4_SUCCESS;
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return i4RetVal;
}

#endif /* MPLS_WANTED*/
#endif /* VPLS_GR_WANTED*/
#endif /* _BGP4VPLSGR_C */
