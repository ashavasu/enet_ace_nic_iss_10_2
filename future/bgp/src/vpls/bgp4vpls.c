/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved             *
 *                                                                  *
 * $Id: bgp4vpls.c,v 1.8 2017/02/09 14:07:41 siva Exp $         *
 *                                                                  *
 * Description: These routines are part of the VPLS Handler which   *
 *              interfaces with L2VPN and different modules of BGP4 *
 ********************************************************************/
#ifndef _BGP4VPLS_C
#define _BGP4VPLS_C

#include "bgp4com.h"
#include "bgp4vpls.h"

/********************************************************************/
/* Function Name   : Bgp4VplsInit                                   */
/* Description     : Initializes the memory pools used in           */
/*                   VPLS and other global variables                */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Global Variables Referred : BGP4_VPLS_GLOBAL_PARAMS              */
/* Global variables Modified : BGP4_VPLS_GLOBAL_PARAMS              */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   :  BGP4_SUCCESS or BGP4_FAILURE        */
/********************************************************************/
INT4
Bgp4VplsInit (VOID)
{

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

#ifdef MPLS_WANTED
    BGP4_VPLS_SPEC_MEMPOOL_ID = BGPMemPoolIds[MAX_BGP_VPLS_INSTANCE_SIZING_ID];
    BGP4_VPLS_GLOBAL_INFO =
        TMO_HASH_Create_Table (BGP4_MAX_VPLS_HASH_TBL_SIZE, NULL, FALSE);
    if (BGP4_VPLS_GLOBAL_INFO == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                  BGP4_VPLS_CRI_TRC | BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tBgp4VplsInit () : Creation of Global VPLS Hash Tbl "
                  "FAILED !!!\n");
        return BGP4_FAILURE;
    }
    BGP4_VPLS_SPEC_CNT = 0;
#endif
    BGP4_VPLS_RR_TARGETS_SET_MEMPOOL_ID =
        BGPMemPoolIds[MAX_BGP_VPLS_RR_IMPORT_TARGETS_SIZING_ID];
    /* Initialize the global variables here */
    TMO_SLL_Init (BGP4_VPLS_RR_TARGETS_SET);
    BGP4_VPLS_RR_TARGETS_INTERVAL = 0;
    BGP4_VPLS_RR_TARGETS_SET_CNT = 0;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4VplsShutdown                               */
/* Description     : De-allocates memory for the pools used in      */
/*                   Vpls route processing and Initializes          */
/*                   global variables                               */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Global Variables Referred : BGP4_VPLS_GLOBAL_PARAMS              */
/* Global variables Modified : BGP4_VPLS_GLOBAL_PARAMS              */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   :  BGP4_SUCCESS or BGP4_FAILURE        */
/********************************************************************/
INT4
Bgp4VplsShutdown (VOID)
{
#ifdef MPLS_WANTED
    tVplsSpecInfo      *pVplsSpecInfo = NULL;
    tVplsSpecInfo      *pTmpVplsSpecInfo = NULL;
    UINT4               u4HashKey;
#endif

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

#ifdef MPLS_WANTED
    if (BGP4_VPLS_GLOBAL_INFO != NULL)
    {
        TMO_HASH_Scan_Table (BGP4_VPLS_GLOBAL_INFO, u4HashKey)
        {
            BGP4_HASH_DYN_Scan_Bucket (BGP4_VPLS_GLOBAL_INFO,
                                       u4HashKey, pVplsSpecInfo,
                                       pTmpVplsSpecInfo, tVplsSpecInfo *)
            {
                TMO_HASH_Delete_Node (BGP4_VPLS_GLOBAL_INFO,
                                      (tTMO_HASH_NODE *) pVplsSpecInfo,
                                      u4HashKey);
                Bgp4MemReleaseVplsSpecEntry (pVplsSpecInfo);
            }
        }
        TMO_HASH_Delete_Table (BGP4_VPLS_GLOBAL_INFO, NULL);
        BGP4_VPLS_GLOBAL_INFO = NULL;
    }

    BGP4_VPLS_SPEC_MEMPOOL_ID = 0;
    BGP4_VPLS_SPEC_CNT = 0;
#endif

    BGP4_VPLS_RR_TARGETS_SET_MEMPOOL_ID = 0;

    /* Initialize the global variables */
    BGP4_VPLS_RR_TARGETS_INTERVAL = 0;
    BGP4_VPLS_RR_TARGETS_SET_CNT = 0;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4VplsIsRTMatchInRRImportRTList              */
/* Description     : Finds whether a particular route target is     */
/*                   present in RR import targets set. If present,  */
/*                   returns success else failure                   */
/* Input(s)        : pu1RTCommVal - import route target             */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
tBgp4VplsRrImportTargetInfo *
Bgp4VplsIsRTMatchInRRImportRTList (UINT1 *pu1RTCommVal)
{
    tBgp4VplsRrImportTargetInfo *pBgp4RrImportTargetInfo = NULL;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    TMO_SLL_Scan (BGP4_VPLS_RR_TARGETS_SET,
                  pBgp4RrImportTargetInfo, tBgp4VplsRrImportTargetInfo *)
    {
        if ((MEMCMP
             ((UINT1 *) BGP4_VPLS_RR_IMPORT_TARGET (pBgp4RrImportTargetInfo),
              pu1RTCommVal, EXT_COMM_VALUE_LEN)) == 0)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                      BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);
            return (pBgp4RrImportTargetInfo);
        }
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);
    return (NULL);
}

#ifdef MPLS_WANTED
/****************************************************************************/
/* Function Name : Bgp4VplsReleaseRouteTargetsFromVpls                      */
/* Description   : Releases the given list of route targets.                */
/* Input(s)      : List of route targets (pTsList)                          */
/*                 type of the route targets                                */
/* Output(s)     : None.                                                    */
/* Return(s)     : Number of Nodes removed.                                 */
/****************************************************************************/
UINT4
Bgp4VplsReleaseRouteTargetsFromVpls (tTMO_SLL * pTsList, UINT1 u1RTType)
{
    tExtCommProfile    *pRouteTarget = NULL;
    tBgp4VplsRrImportTargetInfo *pRrImportTarget = NULL;
    UINT4               u4TargetCnt = 0;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    if (TMO_SLL_Count (pTsList) == 0)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                  BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);
        return 0;
    }

    for (;;)
    {
        pRouteTarget = (tExtCommProfile *) TMO_SLL_First (pTsList);
        if (pRouteTarget == NULL)
        {
            break;
        }
        if (u1RTType == BGP4_IMPORT_ROUTE_TARGET_TYPE)
        {
            /* delete this route target from the RR import route target set if
             * it is present
             */
            pRrImportTarget =
                Bgp4VplsIsRTMatchInRRImportRTList (pRouteTarget->au1ExtComm);
            if (pRrImportTarget != NULL)
            {
                TMO_SLL_Delete (BGP4_VPLS_RR_TARGETS_SET,
                                (tTMO_SLL_NODE *) pRrImportTarget);
                Bgp4MemReleaseRrImportTargetVpls (pRrImportTarget);
            }
        }
        TMO_SLL_Delete (pTsList, (tTMO_SLL_NODE *) pRouteTarget);
        EXT_COMM_EXT_COMM_PROFILE_NODE_FREE (pRouteTarget);
        u4TargetCnt++;
    }
    TMO_SLL_Init (pTsList);

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return (u4TargetCnt);
}

/********************************************************************/
/* Function Name   : Bgp4VplsGetVplsHashKey                         */
/* Description     : Gets the HashKey for a given VPLS index        */
/* Input(s)        : u4VplsIndex - VPLS index                       */
/* Output(s)       : pu1HashKey - pointer to the hashkey value      */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4VplsGetVplsHashKey (UINT4 u4VplsIndex, UINT1 *pu1HashKey)
{
    UINT1               u1Index;
    UINT1               u1BitCnt = 0;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    for (u1Index = 0; u1Index < BGP4_FOUR_BYTE_BITS; u1Index++)
    {
        if (u4VplsIndex & BGP4_MSB)
        {
            u1BitCnt++;
        }
        u4VplsIndex = u4VplsIndex << 1;
    }
    *pu1HashKey = (UINT1) (u1BitCnt % BGP4_MAX_VPLS_HASH_TBL_SIZE);

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4VplsGetVplsInfoFromIndex                   */
/* Description     : Given a VPLS index, this gets the VPLS spec    */
/*                   info                                           */
/* Input(s)        : u4VplsIndex - VPLS Index                       */
/* Output(s)       : ppVplsSpecInfo - address of the VPLS spec      */
/*                   pointer                                        */
/* Global Variables Referred : BGP4_VPLS_GLOBAL_INFO                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4VplsGetVplsInfoFromIndex (UINT4 u4VplsIndex, tVplsSpecInfo ** ppVplsInfo)
{
    tVplsSpecInfo      *pTmpVplsInfo = NULL;
    UINT4               u4HashKey;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    /* Scan the global VPLS Hash table to get the information */
    TMO_HASH_Scan_Table (BGP4_VPLS_GLOBAL_INFO, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (BGP4_VPLS_GLOBAL_INFO, u4HashKey,
                              pTmpVplsInfo, tVplsSpecInfo *)
        {
            if (u4VplsIndex == BGP4_VPLS_SPEC_VPLS_INDX (pTmpVplsInfo))
            {
                *ppVplsInfo = pTmpVplsInfo;
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                          BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);
                return BGP4_SUCCESS;
            }
        }
    }
    *ppVplsInfo = NULL;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
              BGP4_MOD_NAME, "\t%s:%d !!!!!Getting VPLS info NULL !!!!!\n",
              __func__, __LINE__);

    return BGP4_FAILURE;
}

/********************************************************************/
/* Function Name   : Bgp4VplsUpdate                                 */
/* Description     : Updates VPLS information                       */
/* Input(s)        : pL2vpnMsg: QMsg coming form L2VPN              */
/*                   pVplsInfo - VPLS pointer                       */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4VplsUpdate (tBgp4L2VpnEvtInfo * pL2vpnMsg, tVplsSpecInfo * pVplsInfo)
{

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    MEMCPY (BGP4_VPLS_SPEC_ROUTE_DISTING (pVplsInfo),
            pL2vpnMsg->au1RouteDistinguisher, BGP4_VPLS_ROUTE_DISTING_SIZE);
    STRNCPY ((char *) BGP4_VPLS_SPEC_VPLSNAME_VALUE (pVplsInfo),
             (char *) (pL2vpnMsg->au1VplsName), BGP4_VPLS_MAX_VPLS_NAME_SIZE);

    BGP4_VPLS_SPEC_CURRENT_STATE (pVplsInfo) = BGP4_VPLS_CREATE;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4VplsCreate                                 */
/* Description     : Creates a new VPLS                             */
/* Input(s)        : u4VplsIndex - Vpls identifier.                 */
/* Output(s)       : ppVplsInfo - address of VPLS pointer           */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : None.                                */
/********************************************************************/
INT4
Bgp4VplsCreate (tBgp4L2VpnEvtInfo * pL2vpnMsg, tVplsSpecInfo ** ppVplsInfo,
                UINT4 u4Context)
{
    tVplsSpecInfo      *pVplsSpecInfo = NULL;
    UINT1               u1HashKey = 0;
    tTMO_SLL_NODE       *pNextVplsNode = NULL ;
    UNUSED_PARAM (u4Context);

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    *ppVplsInfo = NULL;
    pVplsSpecInfo = Bgp4MemAllocateVplsSpecEntry (sizeof (tVplsSpecInfo));
    if (pVplsSpecInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                  BGP4_MOD_NAME, "\t%s:%d !!!Memory Allocation Fail!!!\n",
                  __func__, __LINE__);
        return BGP4_FAILURE;
    }
   
    MEMSET(pVplsSpecInfo,0, sizeof(tVplsSpecInfo));
    pNextVplsNode = &(pVplsSpecInfo->NextVplsNode) ; 
    TMO_HASH_Init_Node(pNextVplsNode); 
    TMO_SLL_Init(BGP4_VPLS_SPEC_IMPORT_TARGETS(pVplsSpecInfo));
    TMO_SLL_Init(BGP4_VPLS_SPEC_EXPORT_TARGETS(pVplsSpecInfo));

    BGP4_VPLS_SPEC_VPLS_INDX (pVplsSpecInfo) = pL2vpnMsg->u4VplsIndex;
    /* Add this VPLS information into global VPLS hash table */
    Bgp4VplsGetVplsHashKey (pL2vpnMsg->u4VplsIndex, &u1HashKey);
    TMO_HASH_Add_Node (BGP4_VPLS_GLOBAL_INFO,
                       (tTMO_HASH_NODE *) pVplsSpecInfo,
                       (UINT4) u1HashKey, NULL);

    *ppVplsInfo = pVplsSpecInfo;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4VplsDelete                                 */
/* Description     : Deletes an existing VPLS                       */
/* Input(s)        : pVplsSpecInfo - Pointer to VPLS specific info  */
/* Output(s)       : BGP4_SUCCESS/ BGP4_FAILURE.                    */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : None.                                */
/********************************************************************/
INT4
Bgp4VplsDelete (tVplsSpecInfo * pVplsSpecInfo, UINT4 u4Context)
{
    UINT1               u1HashKey;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    if (BGP4_VPLS_SPEC_CURRENT_STATE (pVplsSpecInfo) == BGP4_VPLS_UP)
    {
        /* If VPLS is UP mark it down first */
        Bgp4VplsDownHdlr (pVplsSpecInfo, u4Context);
    }

    Bgp4VplsGetVplsHashKey (BGP4_VPLS_SPEC_VPLS_INDX (pVplsSpecInfo),
                            &u1HashKey);
    /* delete the VPLS information from global VPLS hash table */
    if ((TMO_SLL_Count (BGP4_VPLS_SPEC_IMPORT_TARGETS (pVplsSpecInfo)) == 0) &&
        (TMO_SLL_Count (BGP4_VPLS_SPEC_EXPORT_TARGETS (pVplsSpecInfo)) == 0))
    {
        TMO_HASH_Delete_Node (BGP4_VPLS_GLOBAL_INFO,
                              (tTMO_HASH_NODE *) pVplsSpecInfo,
                              (UINT4) u1HashKey);
        Bgp4MemReleaseVplsSpecEntry (pVplsSpecInfo);

    }
    else
    {
        pVplsSpecInfo->u4VplsState = BGP4_VPLS_RT_ONLY;
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4VplsUpHdlr                                 */
/* Description     : Makes VPLS up                                  */
/* Input(s)        : pVplsInfo - VPLS pointer                       */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4VplsUpHdlr (tVplsSpecInfo * pVplsInfo, UINT4 u4Context)
{

    UNUSED_PARAM (u4Context);

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    if ((BGP4_VPLS_SPEC_CURRENT_STATE (pVplsInfo) == BGP4_VPLS_CREATE)
        || (BGP4_VPLS_SPEC_CURRENT_STATE (pVplsInfo) == BGP4_VPLS_DOWN))
    {

        /* CREATE-> UP : New RT is added so route refresh should be sent */
        /* DOWN-> UP: All routes are deleted in DOWN so when VPLS is marked UP, 
           the routes are required to be received afresh.
           In case VPLS is down we should not apply specific Route targets during filter also.
           In case of L3VPN, RT modification can be done without impact to VRF. */

        if (TMO_SLL_Count (BGP4_VPLS_SPEC_IMPORT_TARGETS (pVplsInfo)) > 0)
        {
            /* Route-refresh is sent to all peers for AFI-SAFI - L2VPN VPLS */
            tAddrPrefix         RemAddr;
            Bgp4InitAddrPrefixStruct (&(RemAddr), BGP4_INET_AFI_IPV4);
            Bgp4RtRefRequestHandler (BGP4_DFLT_VRFID, RemAddr,
                                     BGP4_INET_AFI_L2VPN, BGP4_INET_SAFI_VPLS,
                                     BGP4_RR_ADVT_VPLS_PE_PEERS);
#ifdef BGP4_IPV6_WANTED 
            MEMSET(&RemAddr, 0, sizeof(tAddrPrefix));
            Bgp4InitAddrPrefixStruct (&(RemAddr), BGP4_INET_AFI_IPV6);
            Bgp4RtRefRequestHandler (BGP4_DFLT_VRFID, RemAddr,
                                     BGP4_INET_AFI_L2VPN, BGP4_INET_SAFI_VPLS,
                                     BGP4_RR_ADVT_VPLS_PE_PEERS);
#endif
        }

        BGP4_VPLS_SPEC_CURRENT_STATE (pVplsInfo) = BGP4_VPLS_UP;
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4VplsDownHdlr                               */
/* Description     : Makes VPLS down                                */
/* Input(s)        : pVplsInfo - Vpls pointer                       */
/*                   u1VplsState - the state of Vpls                */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4VplsDownHdlr (tVplsSpecInfo * pVplsInfo, UINT4 u4Context)
{

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    if ((BGP4_VPLS_SPEC_CURRENT_STATE (pVplsInfo) == BGP4_VPLS_CREATE)
        || (BGP4_VPLS_SPEC_CURRENT_STATE (pVplsInfo) == BGP4_VPLS_UP))
    {

        /*   trigger withdraw from BGP and delete the VPLS 
           specific routes from LOCAL RIB */
        Bgp4ProcessVplsDown (pVplsInfo, u4Context);
        BGP4_VPLS_SPEC_CURRENT_STATE (pVplsInfo) = BGP4_VPLS_DOWN;
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4VplsGetRouteTarget                         */
/* Description     : Gets route target pointer                      */
/* Input(s)        : pu1ExtComm - route target value                */
/*                   u1RTType - route target type (import/export)   */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : Route Target pointer                 */
/********************************************************************/
tExtCommProfile    *
Bgp4VplsGetRouteTarget (tVplsSpecInfo * pVplsInfo, UINT1 *pu1ExtComm,
                        UINT1 u1RTType)
{
    tTMO_SLL           *pRTComList = NULL;
    tExtCommProfile    *pExtRTCom = NULL;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    pRTComList = ((u1RTType == BGP4_IMPORT_ROUTE_TARGET_TYPE) ?
                  BGP4_VPLS_SPEC_IMPORT_TARGETS (pVplsInfo) :
                  BGP4_VPLS_SPEC_EXPORT_TARGETS (pVplsInfo));

    TMO_SLL_Scan (pRTComList, pExtRTCom, tExtCommProfile *)
    {
        if ((MEMCMP (pExtRTCom->au1ExtComm,
                     pu1ExtComm, EXT_COMM_VALUE_LEN)) == 0)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                      BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);
            return (pExtRTCom);
        }
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return (NULL);
}

/********************************************************************/
/* Function Name   : Bgp4VplsRouteTargetChgHandler                  */
/* Description     : If the import route target is added/deleted    */
/*                   route-refresh message will be sent to PE peers */
/*                   else if the export route target is             */
/*                   added/deleted then this policy is applied on   */
/*                   the routes and the best routes will be         */
/*                   advertised to PEs                              */
/* Input(s)        : pVplsInfo - pointer to VPLS info               */
/*                   i4RTType - route target type                   */
/*                   pu1ExtComm - route target value                */
/*                   u1OperType - route target (add/ delete)        */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : Route Target pointer                 */
/********************************************************************/

tExtCommProfile    *
Bgp4VplsRouteTargetChgHandler (tVplsSpecInfo * pVplsInfo, INT4 i4RTType,
                               UINT1 *pu1ExtComm, UINT1 u1OperType)
{
    tTMO_SLL           *pRTComList = NULL;
    tExtCommProfile    *pExtRTCom = NULL;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    pRTComList = ((i4RTType == BGP4_IMPORT_ROUTE_TARGET_TYPE) ?
                  BGP4_VPLS_SPEC_IMPORT_TARGETS (pVplsInfo) :
                  BGP4_VPLS_SPEC_EXPORT_TARGETS (pVplsInfo));

    if (u1OperType == BGP4_ROUTE_TARGET_ADD)
    {
        EXT_COMM_PROFILE_CREATE (pExtRTCom);
        if (pExtRTCom == NULL)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                      BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);
            return (NULL);
        }

        MEMSET (pExtRTCom, 0, sizeof (tExtCommProfile));

        TMO_SLL_Init_Node (&(pExtRTCom->ExtCommProfileNext));
        MEMCPY (pExtRTCom->au1ExtComm, pu1ExtComm, EXT_COMM_VALUE_LEN);
        TMO_SLL_Add (pRTComList, (tTMO_SLL_NODE *) pExtRTCom);
    }

    if (i4RTType == BGP4_IMPORT_ROUTE_TARGET_TYPE)
    {
        /* Operation is for import route targets.
         * If any import route target is added or deleted, we should send
         * route refresh message to PE peers.
         */
        if (u1OperType == BGP4_ROUTE_TARGET_DEL)
        {
            pExtRTCom =
                Bgp4VplsGetRouteTarget (pVplsInfo, pu1ExtComm,
                                        (UINT1) i4RTType);
            if (pExtRTCom != NULL)
            {
                TMO_SLL_Delete (BGP4_VPLS_SPEC_IMPORT_TARGETS (pVplsInfo),
                                (tTMO_SLL_NODE *) pExtRTCom);
                EXT_COMM_PROFILE_FREE (pExtRTCom);
                pExtRTCom = NULL;
            }
        }
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                  BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

        return (pExtRTCom);
    }
    else
    {
        /* Operation is for export route targets. Apply this new policy
         * to the routes present for the VPLS and advertise to PE peers.
         */
        if (u1OperType == BGP4_ROUTE_TARGET_DEL)
        {
            pExtRTCom =
                Bgp4VplsGetRouteTarget (pVplsInfo, pu1ExtComm,
                                        (UINT1) i4RTType);
            if (pExtRTCom != NULL)
            {
                TMO_SLL_Delete (BGP4_VPLS_SPEC_EXPORT_TARGETS (pVplsInfo),
                                (tTMO_SLL_NODE *) pExtRTCom);
                EXT_COMM_PROFILE_FREE (pExtRTCom);
                pExtRTCom = NULL;
            }
        }

        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                  BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

        return (pExtRTCom);
    }
}

/*****************************************************************************/
/* FUNCTION NAME : Bgp4VplsHandler                                           */
/* DESCRIPTION   : This function interacts with L2VPN module and process the */
/*                 message received from L2VPN. This function also interact  */
/*                 with connection handler module and receives session       */
/*                 up/down information form it.                              */
/* INPUTS        : pL2vpnMsg: This field can be a QMsg message if the message*/
/*                 is received from L2VPN,or pointer to the peer information */
/*                 if the session up/down information is coming from         */
/*                 connection handler(depends on u1MsgType)                  */
/*                 u1MsgType: Type of Message                                */
/* OUTPUTS       : None                                                      */
/* Return        : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/

INT4
Bgp4VplsHandler (UINT1 *pL2vpnMsg, UINT1 u1MsgType)
{

    INT4                i4RetVal = BGP4_FAILURE;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    /*Message coming from L2VPN, TypeCast message with tBgp4QMsg */
    i4RetVal =
        Bgp4VplsUpdateHandler (&
                               (((tBgp4QMsg *) (VOID *) pL2vpnMsg)->QMsg.
                                Bgp4L2vpnMsg), u1MsgType, BGP4_DFLT_VRFID);

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return i4RetVal;
}

/********************************************************************/
/* Function Name   : Bgp4VplsRouteTargetDel                         */
/* Description     : Deletes the Route Target                       */
/* Input(s)        : pL2vpnMsg - L2VPN Queue message                */
/*                   u1RtType - RT type- import/ export             */
/*                   u1RtOpr - RT operation                         */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4VplsRouteTargetDel (tBgp4L2VpnEvtInfo * pL2vpnMsg, UINT1 u1RtType,
                        UINT1 u1RtOpr, UINT4 u4Context)
{
    tExtCommProfile    *pRTComm = NULL;
    tVplsSpecInfo      *pVplsInfo = NULL;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
            BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    if (BGP4_FAILURE ==
            Bgp4VplsGetVplsInfoFromIndex (pL2vpnMsg->u4VplsIndex, &pVplsInfo))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                BGP4_MOD_NAME, "\t%s:%d VPLS not created..\n", __func__,
                __LINE__);
    }

    if (pVplsInfo != NULL)
    {
        pRTComm = Bgp4VplsGetRouteTarget (pVplsInfo, pL2vpnMsg->au1RouteTarget,
                                          (UINT1) u1RtType);
        if (pRTComm != NULL)
        {
            if (pVplsInfo->u4VplsState == BGP4_VPLS_UP)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                          BGP4_MOD_NAME,
                          "\t%s:%d !!! RT run-time change is not supported !!!\n",
                          __func__, __LINE__);

                /* RT run-time change is not supported. VPLS should be marked down first */
                return BGP4_FAILURE;
            }
            pRTComm = Bgp4VplsRouteTargetChgHandler (pVplsInfo, u1RtType,
                                                     pL2vpnMsg->au1RouteTarget,
                                                     u1RtOpr);
            if (pRTComm == NULL)
            {
                /* RT is deleted successfully */
                if ((pVplsInfo->u4VplsState == BGP4_VPLS_RT_ONLY) &&
                    (TMO_SLL_Count (BGP4_VPLS_SPEC_IMPORT_TARGETS (pVplsInfo))
                     == 0)
                    &&
                    (TMO_SLL_Count (BGP4_VPLS_SPEC_EXPORT_TARGETS (pVplsInfo))
                     == 0))
                {
                    /*VPLS was created due to RT addition. Last RT was deleted. So delete the VPLS */
                    Bgp4VplsDelete (pVplsInfo, u4Context);
                }

                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                          BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

                return BGP4_SUCCESS;
            }
        }

    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return BGP4_FAILURE;
}

/********************************************************************/
/* Function Name   : Bgp4VplsRouteTargetAdd                         */
/* Description     : Adds the Route-Target to VPLS spec structure   */
/* Input(s)        : pL2vpnMsg - L2VPN Queue message                */
/*                   u1RtType - RT type- import/ export             */
/*                   u1RtOpr - RT operation                         */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4VplsRouteTargetAdd (tBgp4L2VpnEvtInfo * pL2vpnMsg, UINT1 u1RtType,
                        UINT1 u1RtOpr, UINT4 u4Context)
{
    tExtCommProfile    *pRTComm = NULL;
    tVplsSpecInfo      *pVplsInfo = NULL;
    UINT1               u1VplsCreated = BGP4_FALSE;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
            BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    if (BGP4_FAILURE == 
            Bgp4VplsGetVplsInfoFromIndex (pL2vpnMsg->u4VplsIndex, &pVplsInfo))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                BGP4_MOD_NAME, "\t%s:%d VPLS not created..\n", __func__,
                __LINE__);
    }

    /* Create VPLS if not created already */
    if (pVplsInfo == NULL)
    {
        if (BGP4_SUCCESS == Bgp4VplsCreate (pL2vpnMsg, &pVplsInfo, u4Context))
        {
            u1VplsCreated = BGP4_TRUE;
            BGP4_VPLS_SPEC_CURRENT_STATE (pVplsInfo) = BGP4_VPLS_RT_ONLY;
        }
    }
    /* Add Route-Target */
    if (pVplsInfo != NULL)
    {
        pRTComm = Bgp4VplsGetRouteTarget (pVplsInfo, pL2vpnMsg->au1RouteTarget,
                                          (UINT1) u1RtType);
        if (pRTComm != NULL)
        {
#ifdef VPLS_GR_WANTED
            /* In case of Switchover, Audit RT Entry and Mark as unstale*/
            if ((BGP4_PREV_RED_NODE_STATUS == RM_STANDBY) && (BGP4_GET_NODE_STATUS == RM_ACTIVE)
                    && (Bgp4GRCheckRestartMode(u4Context) == BGP4_RESTARTING_MODE))
            {
                pRTComm->u1Flag = BGP4_VPLS_RT_UNSTALE;
                return BGP4_SUCCESS;
            }
            else
            {
#endif
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                        BGP4_MOD_NAME, "\t%s:%d !!1Already Exist!!!\n", __func__,
                        __LINE__);
                return BGP4_FAILURE;
#ifdef VPLS_GR_WANTED
            }
#endif
        }

        if (pVplsInfo->u4VplsState == BGP4_VPLS_UP)
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                      BGP4_MOD_NAME,
                      "\t%s:%d !!!RT run-time change is not supported!!!\n",
                      __func__, __LINE__);
            /* RT run-time change is not supported. VPLS should be marked down first */
            return BGP4_FAILURE;
        }
        pRTComm = Bgp4VplsRouteTargetChgHandler (pVplsInfo, u1RtType,
                                                 pL2vpnMsg->au1RouteTarget,
                                                 u1RtOpr);
        if (pRTComm == NULL)
        {
            if (BGP4_TRUE == u1VplsCreated)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                          BGP4_MOD_NAME,
                          "\t%s:%d !!!RT could not be added!!!\n", __func__,
                          __LINE__);
                /*RT could not be added. VPLS was created due to RT addition so delete it */
                Bgp4VplsDelete (pVplsInfo, u4Context);
            }
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                      BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);
            return BGP4_FAILURE;
        }
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                  BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);
        return BGP4_SUCCESS;
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return BGP4_FAILURE;
}

/*****************************************************************************/
/* FUNCTION NAME : Bgp4VplsUpdateHandler                                     */
/* DESCRIPTION   : This function process the queue message coming from L2VPN */
/* INPUTS        : pL2vpnMsg: QMsg coming form L2VPN                         */
/*                 u1MsgType: Type of Message                                */
/* OUTPUTS       : None                                                      */
/* Return        : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4VplsUpdateHandler (tBgp4L2VpnEvtInfo * pL2vpnMsg, UINT1 u1MsgType,
                       UINT4 u4Context)
{
    tVplsAdvtInfo       PeerRouteVplsInfo;
    UINT1               u1RtType;
    UINT1               u1RtOpr;
    INT4                i4Ret = BGP4_SUCCESS;
    tPeerRouteAdvtInfo  WithdrawnRouteInfo;
    tVplsSpecInfo      *pVplsInfo = NULL;
    BOOL1               bControlWordFlag = ECOM_L2VPN_CW_ENABLED;
#ifdef VPLS_GR_WANTED
    tVplsSyncEventInfo  VplsSyncInfo;

    MEMSET (&VplsSyncInfo, 0, sizeof(tVplsSyncEventInfo));
#endif 

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    if (BGP4_FAILURE ==
        Bgp4VplsGetVplsInfoFromIndex (pL2vpnMsg->u4VplsIndex, &pVplsInfo))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                  BGP4_MOD_NAME, "\t%s:%d VPLS not created..\n", __func__,
                  __LINE__);
    }

    switch (u1MsgType)
    {
        case BGP_L2VPN_ADVERTISE_MSG:

            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                      BGP4_MOD_NAME,
                      "\t%s:%d In BGP_L2VPN_ADVERTISE_MSG processing..\n",
                      __func__, __LINE__);

            /* Route Advertisement information */
            PeerRouteVplsInfo.PeerRouteAdvtInfo.u2VeId = pL2vpnMsg->u2VeId;
            PeerRouteVplsInfo.PeerRouteAdvtInfo.u4labelBase =
                pL2vpnMsg->u4LabelBase;
            PeerRouteVplsInfo.PeerRouteAdvtInfo.u2VeBaseOffset =
                pL2vpnMsg->u2VeBaseOffset;
            PeerRouteVplsInfo.PeerRouteAdvtInfo.u2VeBaseSize =
                pL2vpnMsg->u2VeBaseSize;
            MEMCPY (PeerRouteVplsInfo.PeerRouteAdvtInfo.au1RouteDistinguisher,
                    pL2vpnMsg->au1RouteDistinguisher, MAX_LEN_RD_VALUE);

            /* export route target value information */
            MEMCPY (PeerRouteVplsInfo.ExtendedCommInfo.au1RouteTarget,
                    pL2vpnMsg->au1RouteTarget, MAX_LEN_RT_VALUE);

            /* Layer 2 PE Capability as ExtComm Info */
            /* PTR_ASSIGN2(&PeerRouteVplsInfo.ExtCommLayer2Info.extCommType ,ECOMM_VALUE_LAYER2_INFO) ; *//*0x800a */
            PeerRouteVplsInfo.ExtCommLayer2Info.extCommType = OSIX_HTONS (ECOMM_VALUE_LAYER2_INFO);    /*0x800a */
            PeerRouteVplsInfo.ExtCommLayer2Info.encapsType = ECOMM_LAYER2_INFO_ENCAPS_TYPE;    /* 19 */

            if (pL2vpnMsg->bControlWordFlag == ECOM_L2VPN_CW_ENABLED)
            {
                bControlWordFlag = (BOOL1) (bControlWordFlag << 1);
            }
            else
            {
                bControlWordFlag = 0;
            }
            bControlWordFlag |= ECOMM_LAYER2_SEQUENCE_DELIVERY;
            PeerRouteVplsInfo.ExtCommLayer2Info.bControlWordFlag = bControlWordFlag;

            PeerRouteVplsInfo.ExtCommLayer2Info.u2Mtu =
                OSIX_HTONS (pL2vpnMsg->u2Mtu);
            PeerRouteVplsInfo.ExtCommLayer2Info.u2Reserved =
                BGP4_RESERVE_FIELD_VALUE /*0x0000 */ ;

            i4Ret = Bgp4VplsMPAdvtHandler (&PeerRouteVplsInfo,
                                       pL2vpnMsg->u4VplsIndex, u4Context);
            if ((i4Ret == BGP4_RSRC_ALLOC_FAIL) || (i4Ret == BGP4_FAILURE))
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                          BGP4_MOD_NAME, "\t%s:%d !!!Failed  !!!\n", __func__,
                          __LINE__);
                i4Ret = BGP4_FAILURE;
                break;
            }
            break;

        case BGP_L2VPN_WITHDRAWN_MSG:

            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                      BGP4_MOD_NAME,
                      "\t%s:%d In BGP_L2VPN_WITHDRAWN_MSG processing..\n",
                      __func__, __LINE__);

            /* Route Advertisement information */
            WithdrawnRouteInfo.u2VeId = pL2vpnMsg->u2VeId;
            WithdrawnRouteInfo.u4labelBase = pL2vpnMsg->u4LabelBase;
            WithdrawnRouteInfo.u2VeBaseOffset = pL2vpnMsg->u2VeBaseOffset;
            WithdrawnRouteInfo.u2VeBaseSize = pL2vpnMsg->u2VeBaseSize;
            MEMCPY (WithdrawnRouteInfo.au1RouteDistinguisher,
                    pL2vpnMsg->au1RouteDistinguisher, MAX_LEN_RD_VALUE);

            i4Ret = Bgp4VplsMPWithdrawnHandler (&WithdrawnRouteInfo,
                                                pL2vpnMsg->u4VplsIndex,
                                                u4Context);
            break;

        case BGP_L2VPN_VPLS_CREATE:

            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                      BGP4_MOD_NAME,
                      "\t%s:%d In BGP_L2VPN_VPLS_CREATE processing..\n",
                      __func__, __LINE__);

            if (pVplsInfo == NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                          BGP4_MOD_NAME, "\t%s:%d VPLS Info Null\n", __func__,
                          __LINE__);

                i4Ret = Bgp4VplsCreate (pL2vpnMsg, &pVplsInfo, u4Context);
                if (i4Ret == BGP4_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                              BGP4_MOD_NAME, "\t%s:%d !!!Failed!!!\n", __func__,
                              __LINE__);
                    break;
                }

                i4Ret = Bgp4VplsUpdate (pL2vpnMsg, pVplsInfo);
            }
            else if (pVplsInfo->u4VplsState == BGP4_VPLS_RT_ONLY)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                          BGP4_MOD_NAME,
                          "\t%s:%d VPLS State BGP4_VPLS_RT_ONLY \n", __func__,
                          __LINE__);

                i4Ret = Bgp4VplsUpdate (pL2vpnMsg, pVplsInfo);
            }

#if ((defined VPLS_GR_WANTED) && (defined L2RED_WANTED))
            /* Dynamic Sync*/
            if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
            {
                /* In case of Switchover, Audit VPLS Spec Entry and Mark as unstale*/
                if ((BGP4_PREV_RED_NODE_STATUS == RM_STANDBY) && 
                        (Bgp4GRCheckRestartMode(u4Context) == BGP4_RESTARTING_MODE))
                {
                    BGP4_VPLS_SPEC_FLAG (pVplsInfo) = BGP4_VPLS_SPEC_UNSTALE;
                }

                VplsSyncInfo.u4VplsMsgType = u1MsgType;
                Bgp4RedSyncVplsInfo(pVplsInfo, &VplsSyncInfo);
            }
#endif
            break;

        case BGP_L2VPN_VPLS_UP:

            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                      BGP4_MOD_NAME,
                      "\t%s:%d In BGP_L2VPN_VPLS_UP processing..\n", __func__,
                      __LINE__);

            if (pVplsInfo != NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                          BGP4_MOD_NAME, "\t%s:%d in BGP_L2VPN_VPLS_UP\n",
                          __func__, __LINE__);

                /* VPLS RD and VPLS name might have changed when VPLS was down.
                 * So by default update here */
                MEMCPY (BGP4_VPLS_SPEC_ROUTE_DISTING (pVplsInfo),
                        pL2vpnMsg->au1RouteDistinguisher,
                        BGP4_VPLS_ROUTE_DISTING_SIZE);
                STRNCPY ((char *) BGP4_VPLS_SPEC_VPLSNAME_VALUE (pVplsInfo),
                         (char *) (pL2vpnMsg->au1VplsName),
                         BGP4_VPLS_MAX_VPLS_NAME_SIZE);

                i4Ret = Bgp4VplsUpHdlr (pVplsInfo, u4Context);
            }

#if ((defined VPLS_GR_WANTED) && (defined L2RED_WANTED))
            /* Dynamic Sync*/
            if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
            {
                VplsSyncInfo.u4VplsMsgType = u1MsgType;
                Bgp4RedSyncVplsInfo(pVplsInfo, &VplsSyncInfo);
            }
#endif
            break;

        case BGP_L2VPN_VPLS_DOWN:

            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                      BGP4_MOD_NAME,
                      "\t%s:%d In BGP_L2VPN_VPLS_DOWN processing..\n", __func__,
                      __LINE__);

            if (pVplsInfo != NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                          BGP4_MOD_NAME, "\t%s:%d in BGP_L2VPN_VPLS_DOWN\n",
                          __func__, __LINE__);

                i4Ret = Bgp4VplsDownHdlr (pVplsInfo, u4Context);
            }

#if ((defined VPLS_GR_WANTED) && (defined L2RED_WANTED))
            /* Dynamic Sync*/
            if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
            {
                VplsSyncInfo.u4VplsMsgType = u1MsgType;
                Bgp4RedSyncVplsInfo(pVplsInfo, &VplsSyncInfo);
            }
#endif
            break;

        case BGP_L2VPN_VPLS_DELETE:

            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                      BGP4_MOD_NAME,
                      "\t%s:%d In BGP_L2VPN_VPLS_DELETE processing..\n",
                      __func__, __LINE__);

            if (pVplsInfo != NULL)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                          BGP4_MOD_NAME, "\t%s:%d in BGP_L2VPN_VPLS_DELETE\n",
                          __func__, __LINE__);
                i4Ret = Bgp4VplsDelete (pVplsInfo, u4Context);
            }
#if ((defined VPLS_GR_WANTED) && (defined L2RED_WANTED))
            /* Dynamic Sync*/
            if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
            {
                VplsSyncInfo.u4VplsMsgType = u1MsgType;
                Bgp4RedSyncVplsInfo(pVplsInfo, &VplsSyncInfo);
            }
#endif
            break;

        case BGP_L2VPN_IMPORT_RT_ADD:

            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                    BGP4_MOD_NAME,
                    "\t%s:%d In BGP_L2VPN_IMPORT_RT_ADD processing..\n",
                    __func__, __LINE__);

            u1RtType = BGP4_IMPORT_ROUTE_TARGET_TYPE;
            u1RtOpr = BGP4_ROUTE_TARGET_ADD;
            i4Ret = Bgp4VplsRouteTargetAdd (pL2vpnMsg, u1RtType, u1RtOpr,
                    u4Context);
            if (i4Ret == BGP4_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                        BGP4_MOD_NAME, "\t%s:%d !!!Failed!!!\n", __func__,
                        __LINE__);
                break;
            }

#if ((defined VPLS_GR_WANTED) && (defined L2RED_WANTED))
            /* Dynamic Sync*/
            if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
            {
                if (pVplsInfo == NULL)
                {
                    if (Bgp4VplsGetVplsInfoFromIndex (pL2vpnMsg->u4VplsIndex, &pVplsInfo) 
                            == BGP4_FAILURE)
                    {
                        i4Ret = BGP4_FAILURE;
                        break;
                    }
                }
                VplsSyncInfo.u4VplsMsgType = u1MsgType;
                MEMCPY (&VplsSyncInfo.ExtendedCommInfo.au1RouteTarget,
                        pL2vpnMsg->au1RouteTarget,EXT_COMM_VALUE_LEN);
                Bgp4RedSyncVplsInfo(pVplsInfo, &VplsSyncInfo);
            }
#endif
            break;

        case BGP_L2VPN_IMPORT_RT_DELETE:

            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                    BGP4_MOD_NAME,
                    "\t%s:%d In BGP_L2VPN_IMPORT_RT_DELETE processing..\n",
                    __func__, __LINE__);

            u1RtType = BGP4_IMPORT_ROUTE_TARGET_TYPE;
            u1RtOpr = BGP4_ROUTE_TARGET_DEL;
            i4Ret = Bgp4VplsRouteTargetDel (pL2vpnMsg, u1RtType, u1RtOpr,
                        u4Context);
            if (i4Ret == BGP4_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                        BGP4_MOD_NAME, "\t%s:%d !!!Failed!!!\n", __func__,
                        __LINE__);
                break;
            }
#if ((defined VPLS_GR_WANTED) && (defined L2RED_WANTED))
            /* Dynamic Sync*/
            if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
            {
                if (pVplsInfo == NULL)
                {
                    if (Bgp4VplsGetVplsInfoFromIndex (pL2vpnMsg->u4VplsIndex, &pVplsInfo)
                            == BGP4_FAILURE)
                    {
                        i4Ret = BGP4_FAILURE;
                        break;
                    }
                }
                VplsSyncInfo.u4VplsMsgType = u1MsgType;
                MEMCPY (&VplsSyncInfo.ExtendedCommInfo.au1RouteTarget,
                        pL2vpnMsg->au1RouteTarget,EXT_COMM_VALUE_LEN);
                Bgp4RedSyncVplsInfo(pVplsInfo, &VplsSyncInfo);
            }
#endif
            break;

        case BGP_L2VPN_EXPORT_RT_ADD:

            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                    BGP4_MOD_NAME,
                    "\t%s:%d In BGP_L2VPN_EXPORT_RT_ADD processing..\n",
                    __func__, __LINE__);

            u1RtType = BGP4_EXPORT_ROUTE_TARGET_TYPE;
            u1RtOpr = BGP4_ROUTE_TARGET_ADD;
            i4Ret = Bgp4VplsRouteTargetAdd (pL2vpnMsg, u1RtType, u1RtOpr,
                        u4Context);
            if (i4Ret == BGP4_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                        BGP4_MOD_NAME, "\t%s:%d !!!Failed!!!\n", __func__,
                        __LINE__);
                break;
            }
#if ((defined VPLS_GR_WANTED) && (defined L2RED_WANTED))
            /* Dynamic Sync*/
            if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
            {
                if (pVplsInfo == NULL)
                {
                    if (Bgp4VplsGetVplsInfoFromIndex (pL2vpnMsg->u4VplsIndex, &pVplsInfo)
                            == BGP4_FAILURE)
                    {
                        i4Ret = BGP4_FAILURE;
                        break;
                    }
                }
                VplsSyncInfo.u4VplsMsgType = u1MsgType;
                MEMCPY (&VplsSyncInfo.ExtendedCommInfo.au1RouteTarget,
                        pL2vpnMsg->au1RouteTarget,EXT_COMM_VALUE_LEN);
                Bgp4RedSyncVplsInfo(pVplsInfo, &VplsSyncInfo);
            }
#endif
            break;

        case BGP_L2VPN_EXPORT_RT_DELETE:
        
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                      BGP4_MOD_NAME,
                      "\t%s:%d In BGP_L2VPN_EXPORT_RT_DELETE processing..\n",
                      __func__, __LINE__);

            u1RtType = BGP4_EXPORT_ROUTE_TARGET_TYPE;
            u1RtOpr = BGP4_ROUTE_TARGET_DEL;
            i4Ret = Bgp4VplsRouteTargetDel (pL2vpnMsg, u1RtType, u1RtOpr,
                                        u4Context);
            if (i4Ret == BGP4_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                        BGP4_MOD_NAME, "\t%s:%d !!!Failed!!!\n", __func__,
                        __LINE__);
                break;
            }
#if ((defined VPLS_GR_WANTED) && (defined L2RED_WANTED))
            /* Dynamic Sync*/
            if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
            {
                if (pVplsInfo == NULL)
                {
                    if (Bgp4VplsGetVplsInfoFromIndex (pL2vpnMsg->u4VplsIndex, &pVplsInfo)
                             == BGP4_FAILURE)
                    {
                        i4Ret = BGP4_FAILURE;
                        break;
                    }
                }
                VplsSyncInfo.u4VplsMsgType = u1MsgType;
                MEMCPY (&VplsSyncInfo.ExtendedCommInfo.au1RouteTarget,
                        pL2vpnMsg->au1RouteTarget,EXT_COMM_VALUE_LEN);
                Bgp4RedSyncVplsInfo(pVplsInfo, &VplsSyncInfo);
            }
#endif
            break;

        case BGP_L2VPN_BOTH_RT_ADD:
        
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                      BGP4_MOD_NAME,
                      "\t%s:%d In BGP_L2VPN_BOTH_RT_ADD processing..\n",
                      __func__, __LINE__);

            u1RtOpr = BGP4_ROUTE_TARGET_ADD;
            u1RtType = BGP4_IMPORT_ROUTE_TARGET_TYPE;
            i4Ret = Bgp4VplsRouteTargetAdd (pL2vpnMsg, u1RtType, u1RtOpr,
                                        u4Context);
            if (i4Ret == BGP4_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                          BGP4_MOD_NAME, "\t%s:%d !!!Failed!!!\n", __func__,
                          __LINE__);
                break;
            }

            u1RtType = BGP4_EXPORT_ROUTE_TARGET_TYPE;
            i4Ret = Bgp4VplsRouteTargetAdd (pL2vpnMsg, u1RtType, u1RtOpr,
                                        u4Context);

            if (i4Ret == BGP4_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                          BGP4_MOD_NAME, "\t%s:%d !!!Failed!!!\n", __func__,
                          __LINE__);
                break;
            }
#if ((defined VPLS_GR_WANTED) && (defined L2RED_WANTED))
            /* Dynamic Sync*/
            if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
            {
                if (pVplsInfo == NULL)
                {
                    if (Bgp4VplsGetVplsInfoFromIndex (pL2vpnMsg->u4VplsIndex, &pVplsInfo)
                             == BGP4_FAILURE)
                    {
                        i4Ret = BGP4_FAILURE;
                        break;
                    }
                }
                VplsSyncInfo.u4VplsMsgType = u1MsgType;
                MEMCPY (&VplsSyncInfo.ExtendedCommInfo.au1RouteTarget,
                        pL2vpnMsg->au1RouteTarget,EXT_COMM_VALUE_LEN);
                Bgp4RedSyncVplsInfo(pVplsInfo, &VplsSyncInfo);
            }
#endif
            break;

        case BGP_L2VPN_BOTH_RT_DELETE:
        
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                      BGP4_MOD_NAME,
                      "\t%s:%d In BGP_L2VPN_BOTH_RT_DELETE processing..\n",
                      __func__, __LINE__);

            u1RtOpr = BGP4_ROUTE_TARGET_DEL;
            u1RtType = BGP4_IMPORT_ROUTE_TARGET_TYPE;

            i4Ret = Bgp4VplsRouteTargetDel (pL2vpnMsg, u1RtType, u1RtOpr,
                                        u4Context);
            if (i4Ret == BGP4_FAILURE)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                          BGP4_MOD_NAME, "\t%s:%d !!!Failed!!!\n", __func__,
                          __LINE__);
                break;
            }

            u1RtType = BGP4_EXPORT_ROUTE_TARGET_TYPE;
            i4Ret = Bgp4VplsRouteTargetDel (pL2vpnMsg, u1RtType, u1RtOpr,
                                        u4Context);

            if (BGP4_FAILURE == i4Ret)
            {
                BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                          BGP4_MOD_NAME, "\t%s:%d !!!Failed!!!\n", __func__,
                          __LINE__);
                break;
            }
#if ((defined VPLS_GR_WANTED) && (defined L2RED_WANTED))
            /* Dynamic Sync*/
            if (BGP4_GET_NODE_STATUS == RM_ACTIVE)
            {
                if (pVplsInfo == NULL)
                {
                    if (Bgp4VplsGetVplsInfoFromIndex (pL2vpnMsg->u4VplsIndex, &pVplsInfo)
                            == BGP4_FAILURE)
                    {
                        i4Ret = BGP4_FAILURE;
                        break;
                    }
                }
                VplsSyncInfo.u4VplsMsgType = u1MsgType;
                MEMCPY (&VplsSyncInfo.ExtendedCommInfo.au1RouteTarget,
                        pL2vpnMsg->au1RouteTarget,EXT_COMM_VALUE_LEN);
                Bgp4RedSyncVplsInfo(pVplsInfo, &VplsSyncInfo);
            }
#endif
            break;

        case BGP_L2VPN_DOWN:
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                      BGP4_MOD_NAME,
                      "\t%s:%d in BGP_L2VPN_DOWN processing.. \n", __func__,
                      __LINE__);
            i4Ret = BGP4_SUCCESS;
            /*Do Nothing */
            break;

#ifdef VPLS_GR_WANTED
        case BGP_L2VPN_EOVPLS:
            /*
             * After getting L2VPN_BGP_GR_IN_PROGRESS message, L2VPN will share the VPLS 
             * information with existing messages and when done, will send BGP_L2VPN_EOVPLS 
             * to BGP. Once this message is received at BGP, BGP will accept OPEN from 
             * helper nodes and will establish the session.
             */
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                      BGP4_MOD_NAME,
                      "\t%s:%d In BGP_L2VPN_EOVPLS  processing..\n",
                      __func__, __LINE__);

            if ((BGP4_PREV_RED_NODE_STATUS == RM_STANDBY) && (BGP4_GET_NODE_STATUS == RM_ACTIVE))
            {
                /* Audit VPLS spec info */
#ifdef L2RED_WANTED
                Bgp4VplsAuditVplsSpecEntry (u4Context);
                Bgp4VplsDeleteStaleVplsLocalRoutes (u4Context);
#endif
            }
            i4Ret = Bgp4VplsGREORFromL2VPN (u4Context);
            break;
#endif

        default:
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                      BGP4_MOD_NAME, "\t%s:%d !!!!Invalid Message Type!!!! \n",
                      __func__, __LINE__);
            return BGP4_FAILURE;
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return i4Ret;
}

/********************************************************************/
/* Function Name   : Bgp4VplsFindMatchingImportRT                   */
/* Description     : This function finds the RT for VPLS address    */
/*                   family in the configued import RT list for     */
/*                   VPLS and adds the VPLS info to the route       */
/*                   profile if the import route target matches.    */
/*                   If the import route target does not match then */
/*                   the route is filtered                          */
/* Input(s)        : pu1RTCommVal - import route target value       */
/*                   pRtProfile  - route information                */
/* Output(s)       : VPLS spec info.                                */
/* Global Variables Referred : BGP4_VPLS_GLOBAL_INFO.               */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
tVplsSpecInfo      *
Bgp4VplsFindMatchingImportRT (UINT1 *pu1RTCommVal, tRouteProfile * pRtProfile)
{
    tVplsSpecInfo      *pVplsInfo = NULL;
    tExtCommProfile    *pExtCommProfile = NULL;
    UINT4               u4HashKey;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    /* If the received import route-target matches with any configured
     * import route targets of the VPLS then link that particular
     * VPLS information to the route profile.
     */
    TMO_HASH_Scan_Table (BGP4_VPLS_GLOBAL_INFO, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (BGP4_VPLS_GLOBAL_INFO, u4HashKey,
                              pVplsInfo, tVplsSpecInfo *)
        {

            if (pVplsInfo->u4VplsState == BGP4_VPLS_UP)
            {
                TMO_SLL_Scan (BGP4_VPLS_SPEC_IMPORT_TARGETS (pVplsInfo),
                              pExtCommProfile, tExtCommProfile *)
                {
                    if (MEMCMP ((UINT1 *) pExtCommProfile->au1ExtComm,
                                pu1RTCommVal, EXT_COMM_VALUE_LEN) == 0)
                    {
                        /* received import route-target matches with the
                         * configured import route-target of this VPLS.
                         * Route - Target is unique across VPLS. 
                         * So not required to search further
                         */
                        BGP4_VPLS_RT_VPLS_SPEC_INFO (pRtProfile) = pVplsInfo;

                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                                  BGP4_MOD_NAME, "\t%s:%d\n", __func__,
                                  __LINE__);

                        return pVplsInfo;
                    }
                }
            }
        }
    }
    BGP4_VPLS_RT_VPLS_SPEC_INFO (pRtProfile) = pVplsInfo;
    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return NULL;
}

/********************************************************************/
/* Function Name   : Bgp4VplsIsRouteCanBeProcessed                  */
/* Description     : This function checks whether a route can be    */
/*                   processed based on the VPLS the route belongs  */
/*                   to.                                            */
/* Input(s)        : pRtProfile - route profile pointer             */
/* Output(s)       : None.                                          */
/* Use of Recursion: None.                                          */
/* Returns         : BGP4_SUCCESS or BGP4_FAILURE                   */
/********************************************************************/
INT4
Bgp4VplsIsRouteCanBeProcessed (tRouteProfile * pRtProfile)
{
    tVplsSpecInfo      *pVplsSpecInfo = NULL;
    pVplsSpecInfo = BGP4_VPLS_RT_VPLS_SPEC_INFO (pRtProfile);

    if (pVplsSpecInfo == NULL)
    {
        if ((BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) > 0) ||
            (BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_FILTERED_INPUT))
        {
            /* the speaker is a route-reflector and some clients are
             ** configred, then also return success
             **/
            return (BGP4_SUCCESS);
        }
        return (BGP4_FAILURE);
    }

    if (BGP4_VPLS_UP != BGP4_VPLS_RT_VPLS_SPEC_STATE (pVplsSpecInfo))
    {
        return BGP4_FAILURE;
    }

    return BGP4_SUCCESS;
}
#endif /*MPLS_WANTED */

/*****************************************************************************/
/* Function Name : Bgp4IsSameVplsRoute                                       */
/* Description   : This routine checks for the identity of the given two     */
/*                 BGP4 route profiles.                                      */
/* Input(s)      : BGP informations                                          */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRUE if the informations are identical,                   */
/*                 FALSE otherwise.                                          */
/*****************************************************************************/
BOOL1
Bgp4IsSameVplsRoute (tBgp4PeerEntry * pPeer,
                     tRouteProfile * pRtProfile, tRouteProfile * pPeerRoute)
{
    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    if (BGP4_VPLS_RT_VBS (pRtProfile) != BGP4_VPLS_RT_VBS (pPeerRoute))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                  BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);
        return FALSE;
    }
    if (BGP4_VPLS_RT_LB (pRtProfile) != BGP4_VPLS_RT_LB (pPeerRoute))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                  BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);
        return FALSE;
    }
    /* Received new route is identical to the existing
     * route. RFD if enabled also stores the Old route
     * information. So no need for any action.
     */
    if ((Bgp4GRCheckRestartMode (BGP4_PEER_CXT_ID (pPeer))
         == BGP4_RECEIVING_MODE)
        && (Bgp4GRIsPeerGRCapable (pPeer) == BGP4_SUCCESS))
    {
        /* The peer is restarting. Hence revert the stale bit set for
         * the peer routes if it is already preserved */
        if ((BGP4_RT_GET_FLAGS (pPeerRoute) & BGP4_RT_STALE) == BGP4_RT_STALE)
        {
            BGP4_RT_RESET_FLAG (pPeerRoute, BGP4_RT_STALE);
        }
    }
    else if (BGP4_GET_NODE_STATUS == RM_STANDBY)
    {
        /* Route information already synced. Ext. flags
         * modification can cause route to be synced again.
         * Hence, update extflag information alone */
        BGP4_RT_GET_EXT_FLAGS (pPeerRoute) = BGP4_RT_GET_EXT_FLAGS (pRtProfile);
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return TRUE;
}

#ifdef MPLS_WANTED
/*******************************************************************************/
/* Function Name : Bgp4ASNRegister                                             */
/* Description   : This API is exposed to L2VPN to register call back functuion*/
/* Input(s)      : pBgp4RegisterASNInfo contains function pointer              */
/* Output(s)     : None.                                                       */
/* Return(s)     : VOID                                                        */
/*******************************************************************************/
VOID
Bgp4ASNRegister (tBgp4RegisterASNInfo * pBgp4RegisterASNInfo)
{
    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    BGP4_VPLS_REGISTER_ASN_INFO (BGP4_VPLS_GLOBAL_PARAMS).pBgp4NotifyASN =
        pBgp4RegisterASNInfo->pBgp4NotifyASN;

    if ((BGP4_FOUR_BYTE_ASN_SUPPORT (BGP4_DFLT_VRFID) ==
         BGP4_ENABLE_4BYTE_ASN_SUPPORT) &&
        ((gBgpCxtNode[BGP4_DFLT_VRFID]->u4BGP4LocalASno) <= BGP4_MAX_AS))
    {
        Bgp4IndicateASNUpdate (BGP4_ENABLE_4BYTE_ASN_SUPPORT,
                               (gBgpCxtNode[BGP4_DFLT_VRFID]->u4BGP4LocalASno));
    }
    else if ((BGP4_FOUR_BYTE_ASN_SUPPORT (BGP4_DFLT_VRFID) ==
              BGP4_DISABLE_4BYTE_ASN_SUPPORT) &&
             ((gBgpCxtNode[BGP4_DFLT_VRFID]->u4BGP4LocalASno) <=
              BGP4_MAX_TWO_BYTE_AS))
    {
        Bgp4IndicateASNUpdate (BGP4_DISABLE_4BYTE_ASN_SUPPORT,
                               (gBgpCxtNode[BGP4_DFLT_VRFID]->u4BGP4LocalASno));
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);
}

/*****************************************************************************/
/* Function Name : Bgp4IndicateASNUpdate                                     */
/* Description   : Pointer to Call back function which is used to notify ASN */
/*                 update to L2VPN                                           */
/* Input(s)      : ASN Type and ASN Value                                    */
/* Output(s)     : None.                                                     */
/* Return(s)     : VOID                                                      */
/*****************************************************************************/
VOID
Bgp4IndicateASNUpdate (UINT1 u1ASNType, UINT4 u4ASNValue)
{
    (*(BGP4_VPLS_REGISTER_ASN_INFO (BGP4_VPLS_GLOBAL_PARAMS).pBgp4NotifyASN))
        (u1ASNType, u4ASNValue);
}

/********************************************************************/
/* Function Name   : Bgp4ProcessVplsDown                            */
/* Description     : Process vpls for down. It removes all the routes*/
/*                   from the vpls Local Rib that belong to a vpls  */
/* Input(s)        : pVplsInfo - Vpls info                          */
/* Output(s)       : None.                                          */
/* Global Variables Referred : None.                                */
/* Global variables Modified : None.                                */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_FALSE, if the list becomes empty*/
/*                             BGP4_TRUE, some Vpls are pending     */
/********************************************************************/
INT4
Bgp4ProcessVplsDown (tVplsSpecInfo * pVplsInfo, UINT4 u4Context)
{
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pNextRt = NULL;
    tRouteProfile      *pNextRoute = NULL;
    VOID               *pRibNode = NULL;
    UINT4               u4RtCnt = 0;
    INT4                i4Sts = BGP4_TRUE;
    INT4                i4Ret;
    INT4                i4DelRtRet;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    pRibNode = BGP4_VPLS_RIB_TREE (u4Context);

    /* Get the first route */
    i4Sts = Bgp4RibhGetFirstEntry (CAP_MP_L2VPN_VPLS, &pRtProfile,
                                   &pRibNode, TRUE);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                  BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);
        return (BGP4_FALSE);
    }

    for (;;)
    {
        i4Ret = Bgp4RibhGetNextEntry (pRtProfile, u4Context,
                                      &pNextRoute, &pRibNode, TRUE);
        pNextRt = pRtProfile;
        while (pNextRt != NULL)
        {
            pRtProfile = pNextRt;
            i4DelRtRet = Bgp4VplsIsRouteCanBeProcessed (pRtProfile);
            if (i4DelRtRet == BGP4_SUCCESS)
            {
                if (BGP4_RT_PROTOCOL (pRtProfile) != BGP_ID)
                {
                    /*Locally generated route ,already withdrawn and deleted 
                       as part of WithDrawn before VPLS down/delete */
                    BGP4_RT_GET_NEXT (pRtProfile, u4Context, pNextRt);
                    continue;
                }
                else
                {
                    if (BGP4_VPLS_RT_VPLS_SPEC_INDEX (pVplsInfo) ==
                        BGP4_VPLS_RT_VPLS_SPEC_INDEX
                        (BGP4_VPLS_RT_VPLS_SPEC_INFO (pRtProfile)))
                    {
                        /*delete from RIB */
                        BGP4_RT_GET_NEXT (pRtProfile, u4Context, pNextRt);
                        i4DelRtRet = Bgp4RibhDelRtEntry (pRtProfile,
                                                         pRibNode, u4Context,
                                                         BGP4_TRUE);
			if (i4DelRtRet == BGP4_FAILURE)
			{
			    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
				    BGP4_MOD_NAME,
				    "\t <L2VPN , VPLS> Bgp4RibhDelRtEntry Failed\n");
			}
                        u4RtCnt++;
                    }
                    else
                    {
                        BGP4_RT_GET_NEXT (pRtProfile, u4Context, pNextRt);

                    }
                }
            }
            else
            {
                BGP4_RT_GET_NEXT (pRtProfile, u4Context, pNextRt);
            }
        }
        if (i4Ret == BGP4_FAILURE)
        {
            if (u4RtCnt == 0)
            {
                i4Sts = BGP4_FALSE;
                break;
            }
            else
            {
                i4Sts = BGP4_TRUE;
                break;
            }
        }
        else
        {
            if (pNextRoute == NULL)
            {
                i4Sts = BGP4_TRUE;
                break;
            }
        }
        pRtProfile = pNextRoute;
        pNextRoute = NULL;

    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return i4Sts;
}
#endif /*MPLS_WANTED */

/****************************************************************************/
/* Function Name : Bgp4VplsFillVplsExtComm                                  */
/* Description   : This function fills the extended community values from   */
/*                 configured export route targets.                         */
/* Inputs        : Pointer to the route to be removed (pRtProfile)          */
/*               : Pointer to the peer entry (pPeerEntry)                   */
/*               : Pointer to the advertisement BGP info (pAdvtBgpInfo)     */
/* Output(s)       : None.                                                  */
/* Use of Recursion          : None.                                        */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE                    */
/****************************************************************************/
INT4
Bgp4VplsFillVplsExtComm (tRouteProfile * pRouteProfile,
                         tBgp4PeerEntry * pPeerEntry, tBgp4Info * pAdvtBgpInfo)
{
    tBgp4Info          *pRcvdBgp4Info = NULL;
    UNUSED_PARAM (pPeerEntry);
    pRcvdBgp4Info = BGP4_RT_BGP_INFO (pRouteProfile);

    if ((BGP4_RT_PROTOCOL (pRouteProfile) != BGP_ID) ||
        ((BGP4_RT_PROTOCOL (pRouteProfile) == BGP_ID) &&
         (BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) > 0)))
    {
        /* locally originated route */
        ATTRIBUTE_NODE_CREATE (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo));
        BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo) =
            BGP4_INFO_ECOMM_COUNT (pRcvdBgp4Info);

        BGP4_INFO_ECOMM_ATTR_COST_FLAG (pAdvtBgpInfo)
            = BGP4_INFO_ECOMM_ATTR_COST_FLAG (pRcvdBgp4Info);

        MEMCPY (BGP4_INFO_ECOMM_ATTR_VAL (pAdvtBgpInfo),
                BGP4_INFO_ECOMM_ATTR_VAL (pRcvdBgp4Info),
                EXT_COMM_VALUE_LEN * BGP4_INFO_ECOMM_COUNT (pAdvtBgpInfo));

        return BGP4_SUCCESS;
    }
    else
    {
        return BGP4_FAILURE;

    }
}

/********************************************************************/
/* Function Name   : Bgp4VplsApplyImporRTFilter                     */
/* Description     : This module applies the import Route Target    */
/*                   filter on the Route Targets received           */
/*                  with the Vpls routes from the PE peers.         */
/* Input(s)        : pInputRtsList - Pointer to the list of Vpls    */
/*                   routes reeived from the PE peer.               */
/*                   pPeerInfo- pointer to the peer information     */
/*                       from which the Vpls route  is received     */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4VplsApplyImportRTFilter (tTMO_SLL * pInputRtsList,
                             tBgp4PeerEntry * pPeerInfo)
{
    tLinkNode          *pLinkNode = NULL;
    tRouteProfile      *pVplsRoute = NULL;
    tRouteProfile      *pRtProfile = NULL;
    tBgp4Info          *pBgp4Info = NULL;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT4               u4AsafiMask;
    UINT4               u4Index;
    UINT2               u2EcommType;
#ifdef MPLS_WANTED
    tVplsSpecInfo      *pVplsSpecInfo = NULL;
#endif
    tBgp4VplsRrImportTargetInfo *pVplsRrImportTarget = NULL;
    UINT1               u1RTCommInRRList = 0;
    UINT1               u1RrRTCommAdded = BGP4_FALSE;
    INT4                i4RetStatus;

    UNUSED_PARAM (pPeerInfo);

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    /*pVplsRoute = ((tLinkNode *) TMO_SLL_First (pInputRtsList))->pRouteProfile; */

    if (TMO_SLL_Count (pInputRtsList) != 0)
    {
        pVplsRoute =
            ((tLinkNode *) TMO_SLL_First (pInputRtsList))->pRouteProfile;
    }
    else
    {
        return BGP4_FAILURE;
    }

    BGP4_GET_AFISAFI_MASK (BGP4_RT_AFI_INFO (pVplsRoute),
                           BGP4_RT_SAFI_INFO (pVplsRoute), u4AsafiMask);
    if (u4AsafiMask != CAP_MP_L2VPN_VPLS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                  BGP4_MOD_NAME, "\tCapability is not negotiated  %s:%d\n",
                  __func__, __LINE__);
        /* The routes list is not L2VPN - VPLS family. return success */
        return BGP4_SUCCESS;
    }
    pBgp4Info = BGP4_RT_BGP_INFO (pVplsRoute);

    /* Check whether extended community is received or not along
     * with the path attributes
     */
    if (BGP4_INFO_ECOMM_ATTR (pBgp4Info) == NULL)
    {
        /* No route-target community is received along with Vpls routes.
         * hence filter those routes
         */
        TMO_SLL_Scan (pInputRtsList, pLinkNode, tLinkNode *)
        {
            pVplsRoute = pLinkNode->pRouteProfile;
            BGP4_RT_SET_FLAG (pVplsRoute, BGP4_RT_FILTERED_INPUT);
        }
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                  BGP4_MOD_NAME,
                  "\t<L2VPN,VPLS> No route-target, filtered route %s:%d\n",
                  __func__, __LINE__);
        return BGP4_FAILURE;
    }

    for (u4Index = 0; u4Index < BGP4_INFO_ECOMM_COUNT (pBgp4Info); u4Index++)
    {
        MEMCPY ((UINT1 *) au1ExtComm,
                (UINT1 *) (BGP4_INFO_ECOMM_ATTR_VAL (pBgp4Info) +
                           +(u4Index * EXT_COMM_VALUE_LEN)),
                EXT_COMM_VALUE_LEN);
        PTR_FETCH2 (u2EcommType, au1ExtComm);

        if (BGP4_IS_RT_EXT_COMMUNITY (u2EcommType) == BGP4_TRUE)
        {
#ifdef MPLS_WANTED
            pVplsSpecInfo = Bgp4VplsFindMatchingImportRT (au1ExtComm,
                                                          pVplsRoute);
            if ((pVplsSpecInfo == NULL)
                && (BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) == 0))
            {
                /* there is no matching VPLS found */
                continue;
            }
#endif
            if (BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) > 0)
            {
                pVplsRrImportTarget =
                    Bgp4VplsIsRTMatchInRRImportRTList (au1ExtComm);
                if (pVplsRrImportTarget != NULL)
                {
                    /* If the speaker is route-reflector and we received
                     * a matching route taret community from the nonclient 
                     * peer, then apply the filter
                     */
                    if (BGP4_PEER_RFL_CLIENT (pPeerInfo) == NON_CLIENT)
                    {
                        u1RTCommInRRList++;
                    }
                }
                else
                {
                    if (BGP4_PEER_RFL_CLIENT (pPeerInfo) == CLIENT)
                    {
                        /* If the speaker is route-reflector and we 
                         * received a new route target community from 
                         * the client peer, then add into the Import RT set
                         */
                        pVplsRrImportTarget =
                            Bgp4VplsAddNewRTInRRImportList (au1ExtComm);
                        if (pVplsRrImportTarget == NULL)
                        {
                            return BGP4_FAILURE;
                        }
                        u1RrRTCommAdded = BGP4_TRUE;
                        /* Increment the route-count */
                        BGP4_VPLS_RR_IMPORT_TARGET_RTCNT (pVplsRrImportTarget)
                            += TMO_SLL_Count (pInputRtsList);
                        BGP4_VPLS_RR_IMPORT_TARGET_TIMESTAMP
                            (pVplsRrImportTarget) = 0;
                    }
                }
            }
        }
    }

    if (u1RrRTCommAdded == BGP4_TRUE)
    {
        /* The speaker is route-reflector and the route is received from a
         * client peer. If a new route target is added into the set,
         * then send Route-refresh message to the peers */
        i4RetStatus = Bgp4VplsRrJoin ();
        if (i4RetStatus == BGP4_FAILURE)
        {
            return BGP4_FAILURE;
        }
    }

    /* If route profile doesn't belong to any import route targets of the
     * speaker, then if the speaker is not a route-reflector, filter all
     * the routes, these belong to some other VPLS which the speaker is not
     * intended to receive. If the speaker is a route-reflector and the
     * peer is non-client, then apply the filtering of route targets
     * against the stored route-target set received from the client peers
     */
    TMO_SLL_Scan (pInputRtsList, pLinkNode, tLinkNode *)
    {
        pRtProfile = pLinkNode->pRouteProfile;
        if (
#ifdef MPLS_WANTED
               ((BGP4_VPLS_RT_VPLS_SPEC_INFO (pVplsRoute) == NULL) &&
                (BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) == 0)) ||
#endif
               ((BGP4_RR_CLIENT_CNT (BGP4_DFLT_VRFID) > 0) &&
                (BGP4_PEER_RFL_CLIENT (pPeerInfo) == NON_CLIENT) &&
                (u1RTCommInRRList == 0)))
        {

            /*  VPLS update message shall not contain any other family 
             * (IPv4/ IPv6) routes.Hence the input route list shall contain 
             * all VPLS routes. If one is filtered out. Mark all as filtered out 
             */

            BGP4_RT_SET_FLAG (pRtProfile, BGP4_RT_FILTERED_INPUT);
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                      BGP4_MOD_NAME, "\t<L2VPN,VPLS> Filtered Route%s:%d\n",
                      __func__, __LINE__);
        }

#ifdef MPLS_WANTED
        else
        {
            if ((pRtProfile == pVplsRoute) ||
                ((BGP4_VPLS_RT_VPLS_SPEC_INFO (pVplsRoute) == NULL)))
            {
                continue;
            }
            /* If not filtered out, update VPLS info in the Route */
            BGP4_VPLS_RT_VPLS_SPEC_INFO (pRtProfile) = pVplsSpecInfo;
        }
#endif
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME,
              "\t <L2VPN , VPLS> Applied input RT Filter policy %s:%d\n",
              __func__, __LINE__);

    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4VplsAddNewRTInRRImportList                 */
/* Description     : Adds a new route target into RR targets set    */
/* Input(s)        : pu1RTExtComm - route target value              */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
tBgp4VplsRrImportTargetInfo *
Bgp4VplsAddNewRTInRRImportList (UINT1 *pu1RTExtComm)
{
    tBgp4VplsRrImportTargetInfo *pVplsRrImportTarget = NULL;

    pVplsRrImportTarget =
        Bgp4MemAllocateRrImportTargetVpls (sizeof
                                           (tBgp4VplsRrImportTargetInfo));
    if (pVplsRrImportTarget == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                  BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                  "\tVPLS: Creating RR Import Target Node failed\n");
        return (NULL);
    }

    MEMCPY (BGP4_VPLS_RR_IMPORT_TARGET (pVplsRrImportTarget),
            pu1RTExtComm, EXT_COMM_VALUE_LEN);
    TMO_SLL_Add (BGP4_VPLS_RR_TARGETS_SET,
                 (tTMO_SLL_NODE *) pVplsRrImportTarget);
    return (pVplsRrImportTarget);
}

/********************************************************************/
/* Function Name   : Bgp4VplsRrJoin                                 */
/* Description     : Sends route-refresh message to all the         */
/*                   non-clients if its negotiated, otherwise, the  */
/*                   session will be reset by send CEASE message    */
/* Input(s)        : None.                                          */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4VplsRrJoin (VOID)
{
    tAddrPrefix         PeerAddress;
    /* New import route target is added. Send Route-Refresh message
     ** to the Nonclients */
    Bgp4InitAddrPrefixStruct (&(PeerAddress), BGP4_INET_AFI_IPV4);
    Bgp4RtRefRequestHandler (BGP4_DFLT_VRFID, PeerAddress, BGP4_INET_AFI_L2VPN,
                             BGP4_INET_SAFI_VPLS, BGP4_RR_ADVT_NON_CLIENTS);
#ifdef BGP4_IPV6_WANTED
    MEMSET(&PeerAddress, 0, sizeof(tAddrPrefix));
    Bgp4InitAddrPrefixStruct (&(PeerAddress), BGP4_INET_AFI_IPV6);
    Bgp4RtRefRequestHandler (BGP4_DFLT_VRFID, PeerAddress, BGP4_INET_AFI_L2VPN,
                             BGP4_INET_SAFI_VPLS, BGP4_RR_ADVT_NON_CLIENTS);
#endif
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4VplsRrPrune                               */
/* Description     : This module decrements the route count for the */
/*                   route target to be deleted. If the count       */
/*                   becomes zero, then this will inform BGP to wait*/
/*                   for few hours before deleting this route target*/
/*                   from the set. It marks time stamp when there   */
/*                   are no routes correspond to this route target. */
/*                   This function will be called if the route      */
/*                   received from client is deleted                */
/* Input(s)        : pVplsRoute - Pointer to route profile          */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS                         */
/********************************************************************/
INT4
Bgp4VplsRrPrune (tRouteProfile * pVplsRoute)
{
    tBgp4Info          *pBgp4Info = NULL;
    tBgp4VplsRrImportTargetInfo *pVplsRrImportTarget = NULL;
    UINT1               au1ExtComm[EXT_COMM_VALUE_LEN];
    UINT4               u4Index;
    UINT2               u2EcommType;

    pBgp4Info = BGP4_RT_BGP_INFO (pVplsRoute);
    /* Check whether extended community is received or not along
     * * with the path attributes
     * */
    if (BGP4_INFO_ECOMM_ATTR (pBgp4Info) == NULL)
    {
        /* No ecomm received, hence return success */
        return BGP4_SUCCESS;
    }

    if ((BGP4_RT_PROTOCOL (pVplsRoute) != BGP_ID) ||
        (BGP4_PEER_RFL_CLIENT (BGP4_RT_PEER_ENTRY (pVplsRoute)) != CLIENT))
    {
        /* route is not received from client peer */
        return BGP4_SUCCESS;
    }

    for (u4Index = 0; u4Index < BGP4_INFO_ECOMM_COUNT (pBgp4Info); u4Index++)
    {
        MEMCPY ((UINT1 *) au1ExtComm,
                (UINT1 *) (BGP4_INFO_ECOMM_ATTR_VAL (pBgp4Info) +
                           +(u4Index * EXT_COMM_VALUE_LEN)),
                EXT_COMM_VALUE_LEN);
        PTR_FETCH2 (u2EcommType, au1ExtComm);
        if (BGP4_IS_RT_EXT_COMMUNITY (u2EcommType) == BGP4_TRUE)
        {
            pVplsRrImportTarget =
                Bgp4VplsIsRTMatchInRRImportRTList (au1ExtComm);
            if (pVplsRrImportTarget != NULL)
            {
                /* Decrement the route-count */
                BGP4_VPLS_RR_IMPORT_TARGET_RTCNT (pVplsRrImportTarget)--;
                if (BGP4_VPLS_RR_IMPORT_TARGET_RTCNT (pVplsRrImportTarget) == 0)
                {
                    BGP4_VPLS_RR_IMPORT_TARGET_TIMESTAMP (pVplsRrImportTarget) =
                        Bgp4ElapTime ();
                    /* Inform BGP to start timer to take care of deletion
                     ** of route target from the set. If the timer is already
                     ** running then no need to do anything.
                     **/
                    if (BGP4_VPLS_RR_TARGETS_INTERVAL == 0)
                    {
                        BGP4_VPLS_RR_TARGETS_INTERVAL++;
                    }
                }
            }
        }
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4VplsRrDeleteTargetFromSet                 */
/* Description     : This function actually deletes the Route Target*/
/*                   from the Set. This will be called when         */
/*                   BGP4_VPN4_RR_TARGETS_INTERVAL time equals to   */
/*                   the delay (few hours) it is supposed to be     */
/*                   allowed before deleting the route target from  */
/*                   the set.                                       */
/* Input(s)        : None.                                          */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS                         */
/********************************************************************/
INT4
Bgp4VplsRrDeleteTargetFromSet (VOID)
{
    tBgp4VplsRrImportTargetInfo *pVplsRrImportTarget = NULL;
    tBgp4VplsRrImportTargetInfo *pVplsTmpRrImportTarget = NULL;
    UINT4               u4Delay;
    UINT4               u4NextTimeStamp = 0;

    BGP_SLL_DYN_Scan (BGP4_VPLS_RR_TARGETS_SET, pVplsRrImportTarget,
                      pVplsTmpRrImportTarget, tBgp4VplsRrImportTargetInfo *)
    {
        if (BGP4_VPLS_RR_IMPORT_TARGET_RTCNT (pVplsRrImportTarget) == 0)
        {
            u4Delay = ((UINT4) Bgp4ElapTime () -
                       BGP4_VPLS_RR_IMPORT_TARGET_TIMESTAMP
                       (pVplsRrImportTarget));
            if (u4Delay >= BGP4_VPLS_RR_TARGETS_DEL_INTERVAL)
            {
                /* Route target delay exceeds the time interval it should
                 ** be delayed. hence remove this target from the set.
                 **/
                TMO_SLL_Delete (BGP4_VPLS_RR_TARGETS_SET,
                                (tTMO_SLL_NODE *) pVplsRrImportTarget);
                Bgp4MemReleaseRrImportTargetVpls (pVplsRrImportTarget);
            }
            else if (u4Delay > u4NextTimeStamp)
            {
                u4NextTimeStamp = u4Delay;
            }
        }
    }

    BGP4_VPLS_RR_TARGETS_INTERVAL = u4NextTimeStamp;
    return BGP4_SUCCESS;
}
#ifdef BGP_TEST_WANTED
#ifdef MPLS_WANTED
/********************************************************************/
/* Function Name   : Bgp4VplsShowVplsInstance                       */
/* Description     : This function is used to show all the vpls inst*/
/*                   created over a node                            */
/* Input(s)        : u4Context.                                     */
/* Returns         : BGP4_SUCCESS                                   */
/********************************************************************/
INT4
Bgp4VplsShowVplsInstance (tCliHandle CliHandle, UINT4 u4Context)
{
    tVplsSpecInfo      *pVplsInfo = NULL;
    tExtCommProfile    *pExtCommProfile = NULL;
    UINT4               u4HashKey;

    UNUSED_PARAM (u4Context);
    if (BGP4_VPLS_GLOBAL_INFO != NULL)
    {
        CliPrintf (CliHandle,
                " VPLSIndex\t Name\t State\t StaleFlag\t RD\n");
        CliPrintf (CliHandle,
                " ---------\t ----\t -----\t ---------\t --\n");

        TMO_HASH_Scan_Table (BGP4_VPLS_GLOBAL_INFO, u4HashKey)
        {
            TMO_HASH_Scan_Bucket (BGP4_VPLS_GLOBAL_INFO, u4HashKey,
                    pVplsInfo, tVplsSpecInfo *)
            {
                CliPrintf (CliHandle, "     %d    \t",BGP4_VPLS_SPEC_VPLS_INDX (pVplsInfo));
                CliPrintf (CliHandle, " %s\t",BGP4_VPLS_SPEC_VPLSNAME_VALUE (pVplsInfo));
                CliPrintf (CliHandle, " %d\t",BGP4_VPLS_RT_VPLS_SPEC_STATE (pVplsInfo));
                if (BGP4_VPLS_SPEC_FLAG(pVplsInfo))
                {
                    CliPrintf (CliHandle, " Stale\t");
                }
                else
                {
                    CliPrintf (CliHandle, " Unstale\t");
                }
                CliPrintf (CliHandle, " HashKey%d\t",u4HashKey);
                CliPrintf (CliHandle, " %s\t",BGP4_VPLS_SPEC_ROUTE_DISTING (pVplsInfo));
                CliPrintf (CliHandle, " \n");
                if ((TMO_SLL_Count (BGP4_VPLS_SPEC_EXPORT_TARGETS (pVplsInfo)) != 0))
                {
                    TMO_SLL_Scan (BGP4_VPLS_SPEC_EXPORT_TARGETS (pVplsInfo),
                            pExtCommProfile, tExtCommProfile *)
                    {                
                        CliPrintf (CliHandle, " RT\t Export\t");
                        CliPrintf (CliHandle, "%s\n",(UINT1 *) pExtCommProfile->au1ExtComm);
                    }
                }
                if ((TMO_SLL_Count (BGP4_VPLS_SPEC_IMPORT_TARGETS (pVplsInfo)) != 0))
                {
                    TMO_SLL_Scan (BGP4_VPLS_SPEC_IMPORT_TARGETS (pVplsInfo),
                            pExtCommProfile, tExtCommProfile *)
                    {
                        CliPrintf (CliHandle, " RT\t Import\t");
                        CliPrintf (CliHandle, "%s\n",(UINT1 *) pExtCommProfile->au1ExtComm);
                    }
                }
            }
        }
    }
    else 
    {
        CliPrintf (CliHandle, "\n%% No VPLS Instance\n");
    }
    return BGP4_SUCCESS;
}
#endif
#endif
#endif /* _BGP4VPLS_C */
