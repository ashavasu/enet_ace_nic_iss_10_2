/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved             *
 *                                                                  *
 * $Id: bgp4l2vpn.c,v 1.1 2014/03/14 12:50:49 siva Exp $        *
 *                                                                  *
 * Description: These routines are part of the VPLS Handler which   *
 *              interfaces with L2VPN                               *
 ********************************************************************/
#ifndef _BGP4L2VPN_C
#define _BGP4L2VPN_C

#include "bgp4com.h"
#include "bgp4l2vpn.h"

#ifdef MPLS_WANTED

/*****************************************************************************/
/* FUNCTION NAME : Bgp4L2vpnEventHandler                                      */
/* DESCRIPTION   : This API is used by L2VPN to send messages to BGP queue.  */
/* INPUTS        : tBgp4L2VpnEvtInfo: structure with NLRI information from    */
/*                 L2VPN or just route-target in case BGP_L2VPN_RT_ADD/      */
/*                 BGP_L2VPN_RT_DELETE                                       */
/* OUTPUTS       : None                                                      */
/* Return        : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/

INT4
Bgp4L2vpnEventHandler (tBgp4L2VpnEvtInfo *pEvtInfo)
{
    tBgp4QMsg          *pQMsg = NULL;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n",__func__, __LINE__);

    pQMsg = Bgp4MemAllocateQMsg (sizeof (tBgp4QMsg));
    if (pQMsg == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC | BGP4_VPLS_CRI_TRC,
                BGP4_MOD_NAME,
                "\ttBgpL2vpnEventHandler() : Unable to"
                "trigger the L2VPN message to VPLS handler\n");
        return BGP4_FAILURE;
    }

    BGP4_INPUTQ_MSGTYPE (pQMsg) = BGP4_L2VPN_MESSAGE;
    MEMCPY (&(pQMsg->QMsg.Bgp4L2vpnMsg), pEvtInfo, sizeof (tBgp4L2VpnEvtInfo));

    BGP4_INPUTQ_FLAG (pQMsg) = BGP4_DONT_WAIT_FOR_RESP;
    BGP4_INPUTQ_CXT (pQMsg) = BGP4_DFLT_VRFID;
    if (Bgp4Enqueue (pQMsg) != BGP4_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                BGP4_ALL_FAILURE_TRC | BGP4_MGMT_TRC | BGP4_VPLS_CRI_TRC,
                BGP4_MOD_NAME,
                "\tBgpL2vpnEventHandler() : Unable to"
                "trigger the L2VPN message to VPLS handler\n");
        return BGP4_FAILURE;
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n",__func__, __LINE__);

    return BGP4_SUCCESS;
}
#endif  /*MPLS_WANTED */

#endif /* _BGP4L2VPN_C */
