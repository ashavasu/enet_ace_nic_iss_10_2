/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved             *
 *                                                                  *
 * $Id: bgp4vplsWithdh.c,v 1.3 2014/10/14 12:13:16 siva Exp $         *
 *                                                                  *
 * Description: These routines are part of the VPLS Handler which   *
 *              interfaces with L2VPN and different modules of BGP4 *
 ********************************************************************/
#ifndef _BGP4VPLSWITHDH_C
#define _BGP4VPLSWITHDH_C

#include "bgp4com.h"

#ifdef MPLS_WANTED
/*****************************************************************************/
/* FUNCTION NAME : Bgp4VplsMPWithdrawnHandler                                */
/* DESCRIPTION   : This function handles the withdrawn message of VPLS routes*/
/*                 from L2VPN and interact with RIB handler module for       */
/*                 creation and sending of UPDATE message to VPLS BGP peers. */
/* INPUTS        : pPeerRouteWithdInfo: Pointer to the peer information that */
/*                 contains the withdrawn route information,                 */
/*                 u1DestType: Unicast, Broadcast                            */
/* OUTPUTS       : None                                                      */
/* Return        : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/

INT4
Bgp4VplsMPWithdrawnHandler (tPeerRouteAdvtInfo * pPeerRouteWithdInfo,
                            UINT4 u4VplsIndex, UINT4 u4Context)
{

    INT4                i4RetValue = BGP4_FAILURE;
    tBgp4PeerEntry     *pPeer = NULL;
    tVplsNlriInfo       VplsNlri;
    tRouteProfile      *pRtprofile = NULL;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    VplsNlri.u2Length = BGP4_VPLS_NLRI_PREFIX_LEN;
    MEMCPY (VplsNlri.au1RouteDistinguisher,
            pPeerRouteWithdInfo->au1RouteDistinguisher, MAX_LEN_RD_VALUE);
    VplsNlri.u2VeId = pPeerRouteWithdInfo->u2VeId;
    VplsNlri.u2VeBaseOffset = pPeerRouteWithdInfo->u2VeBaseOffset;
    VplsNlri.u2VeBaseSize = pPeerRouteWithdInfo->u2VeBaseSize;
    VplsNlri.u4labelBase = pPeerRouteWithdInfo->u4labelBase;

    /* create route profile Information here */
    pRtprofile = Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
    if (pRtprofile == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                  BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

        /* return (BGP4_RSRC_ALLOC_FAIL); */
        return BGP4_FAILURE;
    }

    MEMSET (pRtprofile, 0, sizeof (tRouteProfile));

    i4RetValue =
        Bgp4VplsConstructRouteProfile (u4Context, pRtprofile, pPeer, &VplsNlri,
                                       u4VplsIndex);
    if (i4RetValue == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_VPLS_FUN_EXIT, BGP4_MOD_NAME,
                  "\tBGP Context node not created \n");
        Bgp4MemReleaseRouteProfile (pRtprofile);
        return BGP4_FAILURE;
    }

    BGP4_RT_SET_FLAG (pRtprofile, BGP4_RT_WITHDRAWN);

    /* Needs to add the route to REDISTRIBUTION update list. */
    BGP4_RT_SET_FLAG (pRtprofile, BGP4_RT_IN_REDIST_LIST);
    if (BGP4_FAILURE ==
        Bgp4DshAddRouteToList (BGP4_REDIST_LIST (u4Context), pRtprofile, 0))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_VPLS_FUN_EXIT, BGP4_MOD_NAME,
                  "\tBGP Route not added in REDIST List \n");
        Bgp4MemReleaseRouteProfile (pRtprofile);
        return BGP4_FAILURE;
    }

    KW_FALSEPOSITIVE_FIX1 (pRtprofile);
    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return BGP4_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME : Bgp4VplsCreateWithdMsgforL2VPN                            */
/* DESCRIPTION   : This function create L2VPN_BGP_WITHDRAWN msg and send it  */
/*                 to L2VPN                                                  */
/* INPUTS        : pRouteProfile route profile                               */
/* OUTPUTS       : None                                                      */
/* Return        : BGP4_SUCCESS/BGP4_FAILURE                                 */
/*****************************************************************************/
INT4
Bgp4VplsCreateWithdMsgforL2VPN (tRouteProfile * pRtProfile)
{
    INT4                i4RetVal;
    UINT4               u4RetVal;
    tL2VpnBgpEvtInfo    L2vpnEvntInfo;
    tVplsSpecInfo      *pVplsSpecInfo = NULL;
    UINT1               au1RouteTarget[MAX_LEN_RT_VALUE];

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    /* In case of Withdrawn message, the information send to L2VPN
     * is MessageType (Withdrawn Message), VE-ID,, RD, LB, VBO, VBS
     * and Peer Address*/

    /* copying VE-ID, RD, LB, VBO, VBS from NLRI */
    L2vpnEvntInfo.u2VeId = BGP4_VPLS_RT_VE_ID (pRtProfile);
    MEMCPY (L2vpnEvntInfo.au1RouteDistinguisher,
            BGP4_VPLS_RT_ROUTE_DISTING (pRtProfile), MAX_LEN_RD_VALUE);
    L2vpnEvntInfo.u4LabelBase = BGP4_VPLS_RT_LB (pRtProfile);
    L2vpnEvntInfo.u2VeBaseOffset = BGP4_VPLS_RT_VBO (pRtProfile);
    L2vpnEvntInfo.u2VeBaseSize = BGP4_VPLS_RT_VBS (pRtProfile);

    /*Message Type */
    L2vpnEvntInfo.u4MsgType = L2VPN_BGP_WITHDRAW_MSG;

    /*Peer Address */
    if (BGP4_PEER_REMOTE_ADDR_TYPE(BGP4_RT_PEER_ENTRY(pRtProfile)) == BGP4_INET_AFI_IPV4)
    {
        L2vpnEvntInfo.u4IpAddr.u4AddrType = BGP4_INET_AFI_IPV4;
        MEMCPY (&(L2vpnEvntInfo.u4IpAddr.uIpAddr.Ip4Addr),
                BGP4_PEER_REMOTE_ADDR(BGP4_RT_PEER_ENTRY(pRtProfile)),
                BGP4_IPV4_PREFIX_LEN);
    }
#ifdef BGP4_IPV6_WANTED
    else
    {
        L2vpnEvntInfo.u4IpAddr.u4AddrType = BGP4_INET_AFI_IPV6;
        MEMCPY (&(L2vpnEvntInfo.u4IpAddr.uIpAddr.Ip6Addr.u1_addr),
                BGP4_PEER_REMOTE_ADDR(BGP4_RT_PEER_ENTRY(pRtProfile)),
                BGP4_IPV6_PREFIX_LEN);
    }
#endif

    /*RT, MTU, Control word */
    i4RetVal = Bgp4VplsGetRTfromRouteProfile (pRtProfile, au1RouteTarget);
    if (i4RetVal == BGP4_SUCCESS)
    {
        MEMCPY ((L2vpnEvntInfo.au1RouteTarget), au1RouteTarget,
                MAX_LEN_RT_VALUE);
    }

    /* Find the VPLS to which the route belongs based on the RT value */
    pVplsSpecInfo = Bgp4VplsFindMatchingImportRT (au1RouteTarget, pRtProfile);
    if (pVplsSpecInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                  BGP4_MOD_NAME,
                  "\t%s:%d !!![ BGP-L2VPN Withdraw] - VPLS information is NULL in the VPLS route !!!\n",
                  __func__, __LINE__);
        return BGP4_FAILURE;
    }

    L2vpnEvntInfo.u4VplsIndex = pVplsSpecInfo->u4VplsIndex;

    STRNCPY (L2vpnEvntInfo.au1VplsName, pVplsSpecInfo->au1VplsName,
             STRNLEN (pVplsSpecInfo->au1VplsName,
                      BGP4_VPLS_MAX_VPLS_NAME_SIZE));

    /*This API is used by BGP to send messages to L2VPN queue. */
    u4RetVal = L2VpnBgpEventHandler (&L2vpnEvntInfo);

    if (u4RetVal == L2VPN_FAILURE)
    {
        i4RetVal = BGP4_FAILURE;
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                  BGP4_MOD_NAME,
                  "\t%s:%d !!! Sending message to L2VPN interface failed !!!\n",
                  __func__, __LINE__);
    }
    else
    {
        i4RetVal = BGP4_SUCCESS;
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
              BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    return i4RetVal;
}
#endif /*MPLS_WANTED */

#endif /* _BGP4VPLSWITHDH_C */
