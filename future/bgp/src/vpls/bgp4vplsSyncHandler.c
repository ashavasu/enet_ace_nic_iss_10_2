/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved             *
 *                                                                  *
 * $Id: bgp4vplsSyncHandler.c,v 1.2 2014/11/20 12:12:13 siva Exp $  *
 *                                                                  *
 * Description: These routines are part of the VPLS Handler which   *
 *              interfaces with L2VPN and different modules of BGP4 *
 ********************************************************************/
#ifndef _BGP4VPLSSYNCHANDLER_C
#define _BGP4VPLSSYNCHANDLER_C

#include "bgp4com.h"
#ifdef L2RED_WANTED
#ifdef VPLS_GR_WANTED
#ifdef MPLS_WANTED
/*****************************************************************************/
/* FUNCTION NAME : Bgp4LinkVplsSpecToRouteInStandby                          */
/* DESCRIPTION   : This function Links route profiles for VPLS routes with   */
/*                 Vpls specfic instances.                                   */
/* INPUTS        : pRtProfile: Pointer to the Route Profile                  */
/*                 u4VplsIndex: Vpls Index                                   */
/*                 u4Context: context id                                     */
/* OUTPUTS       : None                                                      */
/* Return        : NONE                                                      */
/*****************************************************************************/
VOID 
Bgp4LinkVplsSpecToRouteInStandby (tRouteProfile *pRtProfile,
                                       UINT4 u4VplsIndex,
                                       UINT4  u4Context)
{
    tVplsSpecInfo      *pVplsInfo = NULL;

    UNUSED_PARAM (u4Context);

    if (BGP4_FAILURE ==
            Bgp4VplsGetVplsInfoFromIndex (u4VplsIndex, &pVplsInfo))
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                BGP4_MOD_NAME,
                "\t%s:%d !!! Vpls instance is not synced for VplsIndex in StandBy !!!\n",
                __func__, __LINE__);
        return;
    }
    pRtProfile->pVplsSpecInfo = pVplsInfo ;
}

/************************************************************************/
/* Function Name      : Bgp4RedSyncVplsRouteInfo                        */
/* Description        : This function is invoked by the BGP module to   */
/*                      send dynamic updates for vpls  creation/updates */
/* Input(s)           : tVplsSpecInfo - VPLS Info                       */
/*                      tVplsSyncEventInfo- For RT value and Msg Type   */
/* Output(s)          : None                                            */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                     */
/************************************************************************/
INT4 
Bgp4RedSyncVplsInfo (tVplsSpecInfo * pVplsInfo,tVplsSyncEventInfo * pVplsSyncInfo)
{
    tBgpRmMsg          *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT4               u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;

    if (Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_UPD_VPLSINSTANCE_MESSAGE, &u4OffSet)
        == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "Bgp4RedSyncVplsInfo: BgpRmMsg allocation failed\n");
        return BGP4_FAILURE;
    }
    /* increment the offset to write the length of the update mesasge */
    u4OffSet += 2;
    Bgp4RedFillBufVplsSpecInfo (pVplsInfo, pMsg->pBuf, &u4OffSet, pVplsSyncInfo);

    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, u4LenOffSet, u4OffSet);

    pMsg->u4DataLen = u4OffSet;
    Bgp4RedAddTblNode (pMsg);
    Bgp4RedSyncDynInfo ();
    return BGP4_SUCCESS;
}

/*************************************************************************/
/* Function Name      : Bgp4RedFillBufVplsSpecInfo                       */
/* Description        : This function will process the dynamic update    */
/*                      VplsInfo  message and sync the Vpls info to Vpls */
/*                      global info in standby node.                     */
/* Input(s)           : tVplsSpecInfo - VPLS Info                        */
/*                      tRmMsg- RTM Buff Message                         */
/*                      pu4OffSet- Message Offset                        */
/*                      tVplsSyncEventInfo- For RT value and Msg Type    */            
/* Output(s)          : None                                             */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                      */
/*************************************************************************/
INT4 
Bgp4RedFillBufVplsSpecInfo (tVplsSpecInfo * pVplsInfo, 
                            tRmMsg * pBuf,  UINT4 *pu4OffSet,
                            tVplsSyncEventInfo * pVplsSyncInfo)
{

    if (pVplsInfo == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC, BGP4_MOD_NAME,
                "\t%s:%d !!! VPLS info NULL !!!\n", __func__, __LINE__);
        return BGP4_SUCCESS;
    }
    switch (pVplsSyncInfo->u4VplsMsgType)
    {
        case BGP_L2VPN_VPLS_CREATE:

            /*
             *     <-4 bytes->  |<-4 bytes->|<-32 bytes->|<-8 bytes->|
             *    ----------------------------------------------------
             *    | MessageType | VplsIndex | Vpls Name  |     RD    |
             *    ----------------------------------------------------
             */
        case BGP_L2VPN_VPLS_UP:
            /* VPLS RD and VPLS name might have changed when VPLS was down
             * So VPLS Name and RD should be updated*/

            BGP4_RM_PUT_4_BYTE (pBuf, pu4OffSet, pVplsSyncInfo->u4VplsMsgType);
            BGP4_RM_PUT_4_BYTE (pBuf, pu4OffSet, BGP4_VPLS_SPEC_VPLS_INDX (pVplsInfo));
            BGP4_RM_PUT_N_BYTE (pBuf, pu4OffSet, BGP4_VPLS_SPEC_VPLSNAME_VALUE (pVplsInfo), 
                    BGP4_VPLS_MAX_VPLS_NAME_SIZE);
            BGP4_RM_PUT_N_BYTE (pBuf, pu4OffSet, BGP4_VPLS_SPEC_ROUTE_DISTING (pVplsInfo), 
                    BGP4_VPLS_ROUTE_DISTING_SIZE);

            break;
        case BGP_L2VPN_VPLS_DOWN:
        case BGP_L2VPN_VPLS_DELETE:
            /*
             *     <-4 bytes->  |<-4 bytes->|
             *    ---------------------------
             *    | MessageType | VplsIndex |
             *    ---------------------------
             */
            BGP4_RM_PUT_4_BYTE (pBuf, pu4OffSet, pVplsSyncInfo->u4VplsMsgType);
            BGP4_RM_PUT_4_BYTE (pBuf, pu4OffSet, BGP4_VPLS_SPEC_VPLS_INDX (pVplsInfo));
            break;
        case BGP_L2VPN_IMPORT_RT_ADD:
        case BGP_L2VPN_IMPORT_RT_DELETE:
        case BGP_L2VPN_EXPORT_RT_ADD:
        case BGP_L2VPN_EXPORT_RT_DELETE:
        case BGP_L2VPN_BOTH_RT_ADD:
        case BGP_L2VPN_BOTH_RT_DELETE:
            /*
             *     <-4 bytes->  |<-4 bytes->|<-8 bytes->|
             *    ---------------------------------------
             *    | MessageType | VplsIndex |   RT      |
             *    ---------------------------------------
             */
            BGP4_RM_PUT_4_BYTE (pBuf, pu4OffSet, pVplsSyncInfo->u4VplsMsgType);
            BGP4_RM_PUT_4_BYTE (pBuf, pu4OffSet, BGP4_VPLS_SPEC_VPLS_INDX (pVplsInfo));
            BGP4_RM_PUT_N_BYTE (pBuf, pu4OffSet, pVplsSyncInfo->ExtendedCommInfo.au1RouteTarget, 
                    EXT_COMM_VALUE_LEN);

            break;
        default:
            return BGP4_SUCCESS;
    }

    return BGP4_SUCCESS;
}

/*************************************************************************/
/* Function Name      : Bgp4RedProcessVplsInfoSyncMsg                    */
/* Description        : This function will process the dynamic update    */
/*                      VplsInfo  message and sync the Vpls info to Vpls */
/*                      global info in standby node.                     */
/* Input(s)           : tRmMsg- RTM Buff Message                         */
/*                      pu4OffSet- Message Offset                        */
/*                      u2LenOffSet- Offset Length                       */
/* Output(s)          : None                                             */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                      */
/*************************************************************************/
INT4 
Bgp4RedProcessVplsInfoSyncMsg (tRmMsg * pBuf, UINT4 *pu4OffSet, UINT2 u2LenOffSet)
{
    tVplsSpecInfo      *pVplsSpecInfo = NULL;
    UINT4               u4VplsMsgType = 0;
    UINT1               au1RouteTarget[MAX_LEN_RT_VALUE];
    UINT4               u4VplsIndex = 0;
    UINT4               u4Context = BGP4_DFLT_VRFID;
    tBgp4L2VpnEvtInfo   Bgp4L2VpnEvtInfo;
    UINT1               u1HashKey;
    UINT1               u1RtType,u1RtOpr;
    INT4                i4Ret = BGP4_SUCCESS;

    if (*pu4OffSet >= BGP4_RED_ROUTE_MSG_MAX_LEN)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC, BGP4_MOD_NAME,
                "Bgp4RedReadRouteInfo: Read offset exceeded the max route msg len\n");
        return BGP4_FAILURE;
    }

    if (*pu4OffSet < u2LenOffSet)
    {
        /*These two fields are common in all buff message*/
        /*VPLS Interface message Type*/
        BGP4_RM_GET_4_BYTE (pBuf, pu4OffSet, u4VplsMsgType);
        /*VPLS Index*/
        BGP4_RM_GET_4_BYTE (pBuf, pu4OffSet, u4VplsIndex);

        if (BGP4_FAILURE ==
                Bgp4VplsGetVplsInfoFromIndex (u4VplsIndex, &pVplsSpecInfo))
        {
            BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_INFO_TRC,
                    BGP4_MOD_NAME, "\t%s:%d VPLS not created..\n", __func__, __LINE__);
        }

        switch (u4VplsMsgType)
        {
            case BGP_L2VPN_VPLS_CREATE:

                /*
                 *     <-4 bytes->  |<-4 bytes->|<-32 bytes->|<-8 bytes->|
                 *    ----------------------------------------------------
                 *    | MessageType | VplsIndex | Vpls Name  |     RD    |
                 *    ----------------------------------------------------
                 */

                if (pVplsSpecInfo == NULL)
                {
                    pVplsSpecInfo = Bgp4MemAllocateVplsSpecEntry (sizeof (tVplsSpecInfo));
                    if (pVplsSpecInfo == NULL)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                                BGP4_MOD_NAME, "\t%s:%d !!!Memory Allocation Fail!!!\n",
                                __func__, __LINE__);
                        return BGP4_FAILURE;
                    }

                    BGP4_VPLS_SPEC_VPLS_INDX (pVplsSpecInfo) = u4VplsIndex;
                    /* Add this VPLS information into global VPLS hash table */
                    Bgp4VplsGetVplsHashKey (BGP4_VPLS_SPEC_VPLS_INDX (pVplsSpecInfo), &u1HashKey);
                    TMO_HASH_Add_Node (BGP4_VPLS_GLOBAL_INFO,
                            (tTMO_HASH_NODE *) pVplsSpecInfo,
                            (UINT4) u1HashKey, NULL);
                    BGP4_RM_GET_N_BYTE (pBuf, pu4OffSet, BGP4_VPLS_SPEC_VPLSNAME_VALUE (pVplsSpecInfo),
                            BGP4_VPLS_MAX_VPLS_NAME_SIZE);
                    BGP4_RM_GET_N_BYTE (pBuf, pu4OffSet, BGP4_VPLS_SPEC_ROUTE_DISTING (pVplsSpecInfo),
                            BGP4_VPLS_ROUTE_DISTING_SIZE);
                    BGP4_VPLS_SPEC_CURRENT_STATE (pVplsSpecInfo) = BGP4_VPLS_CREATE;
                }
                else if (pVplsSpecInfo->u4VplsState == BGP4_VPLS_RT_ONLY)
                {
                    BGP4_RM_GET_N_BYTE (pBuf, pu4OffSet, BGP4_VPLS_SPEC_VPLSNAME_VALUE (pVplsSpecInfo),
                            BGP4_VPLS_MAX_VPLS_NAME_SIZE);
                    BGP4_RM_GET_N_BYTE (pBuf, pu4OffSet, BGP4_VPLS_SPEC_ROUTE_DISTING (pVplsSpecInfo),
                            BGP4_VPLS_ROUTE_DISTING_SIZE);
                    BGP4_VPLS_SPEC_CURRENT_STATE (pVplsSpecInfo) = BGP4_VPLS_CREATE;
                }
                break;

            case BGP_L2VPN_VPLS_UP:
                if (pVplsSpecInfo != NULL)
                {
                    BGP4_RM_GET_N_BYTE (pBuf, pu4OffSet, BGP4_VPLS_SPEC_VPLSNAME_VALUE (pVplsSpecInfo),
                            BGP4_VPLS_MAX_VPLS_NAME_SIZE);
                    BGP4_RM_GET_N_BYTE (pBuf, pu4OffSet, BGP4_VPLS_SPEC_ROUTE_DISTING (pVplsSpecInfo),
                            BGP4_VPLS_ROUTE_DISTING_SIZE);
                    BGP4_VPLS_SPEC_CURRENT_STATE (pVplsSpecInfo) = BGP4_VPLS_UP;
                }
                break;

            case BGP_L2VPN_VPLS_DOWN:
                /*
                 *     <-4 bytes->  |<-4 bytes->|
                 *    ---------------------------
                 *    | MessageType | VplsIndex |
                 *    ---------------------------
                 */
                if (pVplsSpecInfo != NULL)
                {
                    Bgp4ProcessVplsDown (pVplsSpecInfo, u4Context);
                    BGP4_VPLS_SPEC_CURRENT_STATE (pVplsSpecInfo) = BGP4_VPLS_DOWN;
                }
                break;

            case BGP_L2VPN_VPLS_DELETE:
                if (pVplsSpecInfo != NULL)
                {
                    Bgp4VplsDelete (pVplsSpecInfo, u4Context);
                }
                break;

            case BGP_L2VPN_IMPORT_RT_ADD:
                /*
                 *     <-4 bytes->  |<-4 bytes->|<-8 bytes->|
                 *    ---------------------------------------
                 *    | MessageType | VplsIndex |   RT      |
                 *    ---------------------------------------
                 */

                if (pVplsSpecInfo == NULL)
                {
                    pVplsSpecInfo = Bgp4MemAllocateVplsSpecEntry (sizeof (tVplsSpecInfo));
                    if (pVplsSpecInfo == NULL)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                                BGP4_MOD_NAME, "\t%s:%d !!!Memory Allocation Fail!!!\n",
                                __func__, __LINE__);
                        return BGP4_FAILURE;
                    }
                    BGP4_VPLS_SPEC_VPLS_INDX (pVplsSpecInfo) = u4VplsIndex;
                    /* Add this VPLS information into global VPLS hash table */
                    Bgp4VplsGetVplsHashKey (BGP4_VPLS_SPEC_VPLS_INDX (pVplsSpecInfo), &u1HashKey);
                    TMO_HASH_Add_Node (BGP4_VPLS_GLOBAL_INFO,
                            (tTMO_HASH_NODE *) pVplsSpecInfo,
                            (UINT4) u1HashKey, NULL);
                    BGP4_VPLS_SPEC_CURRENT_STATE (pVplsSpecInfo) = BGP4_VPLS_RT_ONLY;
                }
                else
                {

                    MEMCPY ((Bgp4L2VpnEvtInfo.au1VplsName),
                            BGP4_VPLS_SPEC_VPLSNAME_VALUE (pVplsSpecInfo),
                            BGP4_VPLS_MAX_VPLS_NAME_SIZE);
                }
                BGP4_RM_GET_N_BYTE (pBuf, pu4OffSet, au1RouteTarget,
                        EXT_COMM_VALUE_LEN);
                Bgp4L2VpnEvtInfo.u4VplsIndex = u4VplsIndex;
                MEMCPY ((Bgp4L2VpnEvtInfo.au1RouteTarget),au1RouteTarget, MAX_LEN_RT_VALUE);

                u1RtType = BGP4_IMPORT_ROUTE_TARGET_TYPE;
                u1RtOpr = BGP4_ROUTE_TARGET_ADD;
                i4Ret =
                    Bgp4VplsRouteTargetAdd (&Bgp4L2VpnEvtInfo, u1RtType, u1RtOpr,
                            u4Context);
                break;

            case BGP_L2VPN_IMPORT_RT_DELETE:
                if (pVplsSpecInfo != NULL)
                {
                    BGP4_RM_GET_N_BYTE (pBuf, pu4OffSet, au1RouteTarget,
                            EXT_COMM_VALUE_LEN);
                    Bgp4L2VpnEvtInfo.u4VplsIndex = u4VplsIndex;
                    STRNCPY ((char *) (Bgp4L2VpnEvtInfo.au1VplsName),
                            (char *) BGP4_VPLS_SPEC_VPLSNAME_VALUE (pVplsSpecInfo),
                            BGP4_VPLS_MAX_VPLS_NAME_SIZE);
                    MEMCPY ((Bgp4L2VpnEvtInfo.au1RouteTarget),au1RouteTarget, MAX_LEN_RT_VALUE);

                    u1RtType = BGP4_IMPORT_ROUTE_TARGET_TYPE;
                    u1RtOpr = BGP4_ROUTE_TARGET_DEL;
                    i4Ret =
                        Bgp4VplsRouteTargetDel (&Bgp4L2VpnEvtInfo, u1RtType, u1RtOpr,
                                u4Context);
                }
                break;

            case BGP_L2VPN_EXPORT_RT_ADD:
                if (pVplsSpecInfo == NULL)
                {
                    pVplsSpecInfo = Bgp4MemAllocateVplsSpecEntry (sizeof (tVplsSpecInfo));
                    if (pVplsSpecInfo == NULL)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                                BGP4_MOD_NAME, "\t%s:%d !!!Memory Allocation Fail!!!\n",
                                __func__, __LINE__);
                        return BGP4_FAILURE;
                    }
                    BGP4_VPLS_SPEC_VPLS_INDX (pVplsSpecInfo) = u4VplsIndex;
                    /* Add this VPLS information into global VPLS hash table */
                    Bgp4VplsGetVplsHashKey (BGP4_VPLS_SPEC_VPLS_INDX (pVplsSpecInfo), &u1HashKey);
                    TMO_HASH_Add_Node (BGP4_VPLS_GLOBAL_INFO,
                            (tTMO_HASH_NODE *) pVplsSpecInfo,
                            (UINT4) u1HashKey, NULL);
                    BGP4_VPLS_SPEC_CURRENT_STATE (pVplsSpecInfo) = BGP4_VPLS_RT_ONLY;
                }
                else
                {

                    MEMCPY ((Bgp4L2VpnEvtInfo.au1VplsName),
                            BGP4_VPLS_SPEC_VPLSNAME_VALUE (pVplsSpecInfo),
                            BGP4_VPLS_MAX_VPLS_NAME_SIZE);
                }
                BGP4_RM_GET_N_BYTE (pBuf, pu4OffSet, au1RouteTarget,
                        EXT_COMM_VALUE_LEN);
                Bgp4L2VpnEvtInfo.u4VplsIndex = u4VplsIndex;
                MEMCPY ((Bgp4L2VpnEvtInfo.au1RouteTarget),au1RouteTarget, MAX_LEN_RT_VALUE);

                u1RtType = BGP4_EXPORT_ROUTE_TARGET_TYPE;
                u1RtOpr = BGP4_ROUTE_TARGET_ADD;
                i4Ret =
                    Bgp4VplsRouteTargetAdd (&Bgp4L2VpnEvtInfo, u1RtType, u1RtOpr,
                            u4Context);
                break;

            case BGP_L2VPN_EXPORT_RT_DELETE:
                 if (pVplsSpecInfo != NULL)
                 {
                     BGP4_RM_GET_N_BYTE (pBuf, pu4OffSet, au1RouteTarget,
                             EXT_COMM_VALUE_LEN);
                     Bgp4L2VpnEvtInfo.u4VplsIndex = u4VplsIndex;
                     STRNCPY ((char *) (Bgp4L2VpnEvtInfo.au1VplsName),
                             (char *) BGP4_VPLS_SPEC_VPLSNAME_VALUE (pVplsSpecInfo),
                             BGP4_VPLS_MAX_VPLS_NAME_SIZE);
                     MEMCPY ((Bgp4L2VpnEvtInfo.au1RouteTarget),au1RouteTarget, MAX_LEN_RT_VALUE);

                     u1RtType = BGP4_EXPORT_ROUTE_TARGET_TYPE;
                     u1RtOpr = BGP4_ROUTE_TARGET_DEL;
                     i4Ret =
                         Bgp4VplsRouteTargetDel (&Bgp4L2VpnEvtInfo, u1RtType, u1RtOpr,
                                 u4Context);
                 }
                break;

            case BGP_L2VPN_BOTH_RT_ADD:
                if (pVplsSpecInfo == NULL)
                {
                    pVplsSpecInfo = Bgp4MemAllocateVplsSpecEntry (sizeof (tVplsSpecInfo));
                    if (pVplsSpecInfo == NULL)
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                                BGP4_MOD_NAME, "\t%s:%d !!!Memory Allocation Fail!!!\n",
                                __func__, __LINE__);
                        return BGP4_FAILURE;
                    }
                    BGP4_VPLS_SPEC_VPLS_INDX (pVplsSpecInfo) = u4VplsIndex;
                    /* Add this VPLS information into global VPLS hash table */
                    Bgp4VplsGetVplsHashKey (BGP4_VPLS_SPEC_VPLS_INDX (pVplsSpecInfo), &u1HashKey);
                    TMO_HASH_Add_Node (BGP4_VPLS_GLOBAL_INFO,
                            (tTMO_HASH_NODE *) pVplsSpecInfo,
                            (UINT4) u1HashKey, NULL);
                    BGP4_VPLS_SPEC_CURRENT_STATE (pVplsSpecInfo) = BGP4_VPLS_RT_ONLY;
                }
                else
                {

                    MEMCPY ((Bgp4L2VpnEvtInfo.au1VplsName),
                            BGP4_VPLS_SPEC_VPLSNAME_VALUE (pVplsSpecInfo),
                            BGP4_VPLS_MAX_VPLS_NAME_SIZE);
                }

                BGP4_RM_GET_N_BYTE (pBuf, pu4OffSet, au1RouteTarget,
                        EXT_COMM_VALUE_LEN);
                Bgp4L2VpnEvtInfo.u4VplsIndex = u4VplsIndex;
                MEMCPY ((Bgp4L2VpnEvtInfo.au1RouteTarget),au1RouteTarget, MAX_LEN_RT_VALUE);

                u1RtType = BGP4_IMPORT_ROUTE_TARGET_TYPE;
                u1RtOpr = BGP4_ROUTE_TARGET_ADD;
                i4Ret =
                    Bgp4VplsRouteTargetAdd (&Bgp4L2VpnEvtInfo, u1RtType, u1RtOpr,
                            u4Context);
                u1RtType = BGP4_EXPORT_ROUTE_TARGET_TYPE;
                i4Ret =
                    Bgp4VplsRouteTargetAdd (&Bgp4L2VpnEvtInfo, u1RtType, u1RtOpr,
                            u4Context);
                break;

            case BGP_L2VPN_BOTH_RT_DELETE:
                BGP4_RM_GET_N_BYTE (pBuf, pu4OffSet, au1RouteTarget,
                        EXT_COMM_VALUE_LEN);
                Bgp4L2VpnEvtInfo.u4VplsIndex = u4VplsIndex;
                STRNCPY ((char *) (Bgp4L2VpnEvtInfo.au1VplsName),
                        (char *) BGP4_VPLS_SPEC_VPLSNAME_VALUE (pVplsSpecInfo),
                        BGP4_VPLS_MAX_VPLS_NAME_SIZE);
                MEMCPY ((Bgp4L2VpnEvtInfo.au1RouteTarget),au1RouteTarget, MAX_LEN_RT_VALUE);

                u1RtType = BGP4_IMPORT_ROUTE_TARGET_TYPE;
                u1RtOpr = BGP4_ROUTE_TARGET_DEL;
                i4Ret =
                    Bgp4VplsRouteTargetDel (&Bgp4L2VpnEvtInfo, u1RtType, u1RtOpr,
                            u4Context);
                u1RtType = BGP4_EXPORT_ROUTE_TARGET_TYPE;
                i4Ret =
                    Bgp4VplsRouteTargetDel (&Bgp4L2VpnEvtInfo, u1RtType, u1RtOpr,
                            u4Context);
                break;

            default:
                return BGP4_FAILURE;
        }
    }
    return i4Ret;
}

/*************************************************************************/
/* Function Name      : Bgp4RedSyncVplsInfoBulkMsg                       */
/* Description        : This function will scan through all the Vpls     */
/*                      instances present in VPLS global info  and       */
/*                      send the BULK information to standby in BULK     */
/*                      UPDATE messageThis function will process         */
/*                      the dynamic update                               */
/* Input(s)           : tBgpRmMsg- RTM Buff Message                         */
/*                      pu4OffSet- Message Offset                        */
/* Output(s)          : None                                             */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                      */
/*************************************************************************/
INT4 
Bgp4RedSyncVplsInfoBulkMsg (tBgpRmMsg * pMsg, UINT4 *pu4Offset)
{
    tVplsSpecInfo      *pVplsInfo = NULL;
    tExtCommProfile    *pExtCommProfile = NULL;
    UINT4               u4HashKey;
    UINT4               u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
    UINT4               u4VplsInstCountOffSet = BGP4_RED_UPD_LEN_OFFSET + 2;
    UINT4               u4VplsInstCount = 0;
    UINT4               u4VplsInsLen = 0;
    INT4                i4RetVal ;
    /*
     *    VPLS Specfic Information
     *    <-2 bytes->          |<-4 bytes->|<-4 bytes-> |<-4 bytes->|
     *     ---------------------------------------------------------
     *    | VPLS Instance count|   Index   | VPLS State |  u4Flag   |
     *     ---------------------------------------------------------
     *    <-32 bytes->|<-8 bytes->|  <-2 bytes->    |<-8 bytes->|<-8 bytes->|<-N bytes ->|
     *     ------------------------------------------------------------------------------
     *    | VPLS Name |    RD     | Import RT count | Import RT1| Import RT2| ---------  |
     *     ------------------------------------------------------------------------------
     *    <-2 bytes->      |<-8 bytes-> |<-8 bytes-> |<-N byte->|    <-2 bytes->   |
     *     ------------------------------------------------------------------------
     *    | Export RT Count| Export RT1 | Export RT2 | ------   |  Next VPLS Index |
     *     ------------------------------------------------------------------------
     */
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
            BGP4_MGMT_TRC, BGP4_MOD_NAME,
            "Entering Bgp4RedSyncVplsInfoBulkMsg\r\n");

    if (0 == BGP4_VPLS_SPEC_CNT)
    {
        return BGP4_SUCCESS;
    }

    /* Offset for VPLS count*/
    *pu4Offset += 2;
    
    TMO_HASH_Scan_Table (BGP4_VPLS_GLOBAL_INFO, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (BGP4_VPLS_GLOBAL_INFO, u4HashKey,
                pVplsInfo, tVplsSpecInfo *)
        {
            u4VplsInsLen = Bgp4VplsGetVplsIntsSize (pVplsInfo);

            if ((BGP4_RED_MSG_MAX_LEN - *pu4Offset) <= u4VplsInsLen)
            {
                /* Fill VPLS Inst count*/
                RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, u4VplsInstCountOffSet,
                        u4VplsInstCount);
                /* Fill Length Offset*/
                u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
                RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, u4LenOffSet, *pu4Offset);
                pMsg->u4DataLen = *pu4Offset;

                Bgp4RedAddTblNode (pMsg);
                Bgp4RedSyncDynInfo ();
                pMsg = NULL;
                *pu4Offset = 0;
                u4VplsInstCount = 0;

                i4RetVal =
                    Bgp4RedGetBgpRmMsg (&pMsg, BGP4_RED_BULK_UPD_VPLS_INST_MESSAGE,
                            pu4Offset);
                if (i4RetVal == BGP4_FAILURE)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG,
                            BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                            "Bgp4RedSendBulkUpdMsg: BgpRmMsg allocation failed\n");
                    BGP4_RED_BULK_UPD_STATUS = BGP4_HA_UPD_IN_PROGRESS;
                    return BGP4_FAILURE;
                }

                /* Length Offset*/
                *pu4Offset += 2;
                /* VPLS Count Offset*/
                *pu4Offset += 2;
            }

            u4VplsInstCount += 1;

            BGP4_RM_PUT_4_BYTE (pMsg->pBuf, pu4Offset, BGP4_VPLS_SPEC_VPLS_INDX (pVplsInfo));
            BGP4_RM_PUT_4_BYTE (pMsg->pBuf, pu4Offset, BGP4_VPLS_RT_VPLS_SPEC_STATE (pVplsInfo));
            BGP4_RM_PUT_4_BYTE (pMsg->pBuf, pu4Offset, BGP4_VPLS_SPEC_FLAG(pVplsInfo));
            BGP4_RM_PUT_N_BYTE (pMsg->pBuf, pu4Offset, BGP4_VPLS_SPEC_VPLSNAME_VALUE (pVplsInfo),
                    BGP4_VPLS_MAX_VPLS_NAME_SIZE);
            BGP4_RM_PUT_N_BYTE (pMsg->pBuf, pu4Offset, BGP4_VPLS_SPEC_ROUTE_DISTING (pVplsInfo),
                    BGP4_VPLS_ROUTE_DISTING_SIZE);

            if ((TMO_SLL_Count (BGP4_VPLS_SPEC_IMPORT_TARGETS (pVplsInfo)) != 0))
            {
                BGP4_RM_PUT_2_BYTE (pMsg->pBuf, pu4Offset, 
                        TMO_SLL_Count (BGP4_VPLS_SPEC_IMPORT_TARGETS (pVplsInfo)));

                TMO_SLL_Scan (BGP4_VPLS_SPEC_IMPORT_TARGETS (pVplsInfo),
                        pExtCommProfile, tExtCommProfile *)
                {
                    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, pu4Offset,
                            (UINT1 *) pExtCommProfile->au1ExtComm,EXT_COMM_VALUE_LEN);    
                }
            }

            if ((TMO_SLL_Count (BGP4_VPLS_SPEC_EXPORT_TARGETS (pVplsInfo)) != 0))
            {
                BGP4_RM_PUT_2_BYTE (pMsg->pBuf, pu4Offset,
                        TMO_SLL_Count (BGP4_VPLS_SPEC_EXPORT_TARGETS (pVplsInfo)));
                TMO_SLL_Scan (BGP4_VPLS_SPEC_EXPORT_TARGETS (pVplsInfo),
                        pExtCommProfile, tExtCommProfile *)
                {
                    BGP4_RM_PUT_N_BYTE (pMsg->pBuf, pu4Offset,
                            (UINT1 *) pExtCommProfile->au1ExtComm,EXT_COMM_VALUE_LEN);
                }
            }
        }
    }

    /*Fill VPLS Inst count*/
    
    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, u4VplsInstCountOffSet, u4VplsInstCount);

    u4LenOffSet = BGP4_RED_UPD_LEN_OFFSET;
    RM_DATA_ASSIGN_2_BYTE (pMsg->pBuf, u4LenOffSet, *pu4Offset);
    pMsg->u4DataLen = *pu4Offset;
    Bgp4RedAddTblNode (pMsg);
    Bgp4RedSyncDynInfo ();
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
            BGP4_MGMT_TRC, BGP4_MOD_NAME,
            "Exiting Bgp4RedSyncVplsInfoBulkMsg\r\n");

    return BGP4_SUCCESS;
}

/*************************************************************************/
/* Function Name      : Bgp4VplsGetVplsIntsSize                          */
/* Description        : This function will get the length of VPLS        */
/*                      instance                                         */
/* Input(s)           : pVplsInfo - VPLS Specific information            */
/* Output(s)          : None                                             */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                      */
/*************************************************************************/
UINT4
Bgp4VplsGetVplsIntsSize (tVplsSpecInfo * pVplsInfo)
{
    /* VPLS Instance Size*/

    return (UINT4) (sizeof (UINT4) /*VPLS Index*/
            + sizeof (UINT4) /*VPLS State*/
            + sizeof (UINT4) /*u4Flags*/
            + sizeof (UINT4) * 8 /*VPLS Name 32 Bytes*/
            + sizeof (UINT4) * 2 /*RD Len 8 Bytes*/
            + sizeof (UINT4) /*Size of all RT count*/
            + (TMO_SLL_Count (BGP4_VPLS_SPEC_IMPORT_TARGETS (pVplsInfo))) * 8 /*Import RT Len*/
            + (TMO_SLL_Count (BGP4_VPLS_SPEC_EXPORT_TARGETS (pVplsInfo))) * 8 /*Export RT Len*/
            );
}
/*************************************************************************/
/* Function Name      : Bgp4RedProcessVplsInfoBulkMsg                    */
/* Description        : This function will process the Vpls information  */
/*                      present in BULK UPDATE message received from     */
/*                      active node to standby node and update VPLS      */
/*                      global info.                                     */
/* Input(s)           : tRmMsg- RTM Buff Message                         */
/*                      pu4OffSet- Message Offset                        */
/*                      u2LenOffSet- Offset Length                       */
/* Output(s)          : None                                             */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                      */
/*************************************************************************/
INT4 
Bgp4RedProcessVplsInfoBulkMsg (tRmMsg * pMsg, UINT4 *pu4OffSet, UINT2 u2LenOffSet)
{

    UINT2               u2VplsInstanceCount = 0;
    tVplsSpecInfo       *pVplsInfo = NULL;
    UINT4               u4VplsIndex;
    UINT2               u2ImportRTCount = 0;
    UINT2               u2ExportRTCount = 0;
    UINT1               au1RouteTarget[MAX_LEN_RT_VALUE];
    int                 i,j;
    tExtCommProfile    *pRTComm = NULL;
    UINT1               u1HashKey = 0;
    
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
            BGP4_MGMT_TRC, BGP4_MOD_NAME,
            "Entering Bgp4RedProcessVplsInfoBulkMsg\r\n");

    /*
     *    VPLS Specfic Information
     *    <-2 bytes->          |  <-4 bytes-> |<-4 bytes-> |<-4 bytes-> |
     *     --------------------------------------------------------------
     *    | VPLS Instance count|      Index   | VPLS State |  u4Flag    |
     *     --------------------------------------------------------------
     *    <-32 bytes->|<-8 bytes->|  <-2 bytes->    |<-8 bytes->|<-8 bytes->|<-N bytes ->|
     *     ------------------------------------------------------------------------------
     *    | VPLS Name |    RD     | Import RT count | Import RT1| Import RT2| ---------  |
     *     ------------------------------------------------------------------------------
     *    <-2 bytes->      |<-8 bytes-> |<-8 bytes-> |<-N byte->|    <-2 bytes->   |
     *     ------------------------------------------------------------------------
     *    | Export RT Count| Export RT1 | Export RT2 | ------   |  Next VPLS Index |
     *     ------------------------------------------------------------------------
     */

    while (*pu4OffSet < u2LenOffSet)
    {
        BGP4_RM_GET_2_BYTE (pMsg, pu4OffSet, u2VplsInstanceCount);
        for (i=0; i<u2VplsInstanceCount; i++)
        {
            BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, u4VplsIndex);
            if (BGP4_FAILURE ==
                    Bgp4VplsGetVplsInfoFromIndex (u4VplsIndex, &pVplsInfo))
            {
                pVplsInfo = Bgp4MemAllocateVplsSpecEntry (sizeof (tVplsSpecInfo));
                if (pVplsInfo == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                            BGP4_MOD_NAME, "\t%s:%d !!!Memory Allocation Fail!!!\n",
                            __func__, __LINE__);
                    return BGP4_FAILURE;
                }
                BGP4_VPLS_SPEC_VPLS_INDX (pVplsInfo) = u4VplsIndex;
                Bgp4VplsGetVplsHashKey (u4VplsIndex, &u1HashKey);
                TMO_HASH_Add_Node (BGP4_VPLS_GLOBAL_INFO,
                        (tTMO_HASH_NODE *) pVplsInfo,
                        (UINT4) u1HashKey, NULL);
            }
            BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, BGP4_VPLS_SPEC_CURRENT_STATE(pVplsInfo));
            BGP4_RM_GET_4_BYTE (pMsg, pu4OffSet, BGP4_VPLS_SPEC_FLAG(pVplsInfo));
            BGP4_RM_GET_N_BYTE (pMsg, pu4OffSet, BGP4_VPLS_SPEC_VPLSNAME_VALUE (pVplsInfo),
                    BGP4_VPLS_MAX_VPLS_NAME_SIZE);
            BGP4_RM_GET_N_BYTE (pMsg, pu4OffSet,BGP4_VPLS_SPEC_ROUTE_DISTING (pVplsInfo),
                    BGP4_VPLS_ROUTE_DISTING_SIZE);
            /*Import RT Add*/
            BGP4_RM_GET_2_BYTE (pMsg, pu4OffSet, u2ImportRTCount);
            for (j=0 ; j<u2ImportRTCount; j++)
            {
                BGP4_RM_GET_N_BYTE (pMsg, pu4OffSet,au1RouteTarget, MAX_LEN_RT_VALUE);
                pRTComm = Bgp4VplsRouteTargetChgHandler (pVplsInfo, 
                        BGP4_IMPORT_ROUTE_TARGET_TYPE, au1RouteTarget, BGP4_ROUTE_TARGET_ADD);
                if (pRTComm == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC, BGP4_MOD_NAME,
                            "\t%s:%d !!!RT could not be added!!!\n", __func__, __LINE__);
                }
            }

            /*Export RT Add*/
            BGP4_RM_GET_2_BYTE (pMsg, pu4OffSet, u2ExportRTCount);
            for (j=0 ; j<u2ImportRTCount; j++)
            {
                BGP4_RM_GET_N_BYTE (pMsg, pu4OffSet,au1RouteTarget, MAX_LEN_RT_VALUE);
                pRTComm = Bgp4VplsRouteTargetChgHandler (pVplsInfo, 
                        BGP4_EXPORT_ROUTE_TARGET_TYPE, au1RouteTarget, BGP4_ROUTE_TARGET_ADD);
                if (pRTComm == NULL)
                {
                    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC, BGP4_MOD_NAME,
                            "\t%s:%d !!!RT could not be added!!!\n", __func__, __LINE__);
                }
            }
        }
    }
    BGP4_TRC (NULL, BGP4_TRC_FLAG,
            BGP4_MGMT_TRC, BGP4_MOD_NAME,
            "Exiting Bgp4RedProcessVplsInfoBulkMsg\r\n");

    return BGP4_SUCCESS;
}
/*************************************************************************/
/* Function Name      : Bgp4VplsMarkVplsEntryAsStale                     */
/* Description        : This function will mark VPLS Spec Entry as stale */
/*                      when BGP node goes from Stand-by to Active HA    */
/* Input(s)           : u4CtxId - Context ID                             */
/* Output(s)          : None                                             */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                      */
/*************************************************************************/
INT4
Bgp4VplsMarkVplsEntryAsStale (UINT4 u4CtxId)
{

    tVplsSpecInfo      *pTmpVplsInfo = NULL;
    UINT4               u4HashKey;
    tExtCommProfile    *pExtCommProfile = NULL;
    UNUSED_PARAM (u4CtxId);

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
            BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);
    /* Scan the global VPLS Hash table to get the information */
    TMO_HASH_Scan_Table (BGP4_VPLS_GLOBAL_INFO, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (BGP4_VPLS_GLOBAL_INFO, u4HashKey,
                pTmpVplsInfo, tVplsSpecInfo *)
        {
            /*Mark VPLS Spec entry stale*/
            BGP4_VPLS_SPEC_FLAG (pTmpVplsInfo) = BGP4_VPLS_SPEC_STALE;

            /*Mark import or export RT as stale*/
            if ((TMO_SLL_Count (BGP4_VPLS_SPEC_IMPORT_TARGETS (pTmpVplsInfo)) != 0))
            {
                TMO_SLL_Scan (BGP4_VPLS_SPEC_IMPORT_TARGETS (pTmpVplsInfo),
                        pExtCommProfile, tExtCommProfile *)
                {
                    pExtCommProfile->u1Flag = BGP4_VPLS_RT_STALE;
                }
            }

            if ((TMO_SLL_Count (BGP4_VPLS_SPEC_EXPORT_TARGETS (pTmpVplsInfo)) != 0))
            {
                TMO_SLL_Scan (BGP4_VPLS_SPEC_EXPORT_TARGETS (pTmpVplsInfo),
                        pExtCommProfile, tExtCommProfile *)
                {
                    pExtCommProfile->u1Flag = BGP4_VPLS_RT_STALE;
                }
            }
        }
    }

    return BGP4_SUCCESS;
}

/*************************************************************************/
/* Function Name      : Bgp4VplsAuditVplsSpecEntry                       */
/* Description        : This function will mark VPLS Spec Entry as stale */
/*                      when BGP node goes from Stand-by to Active HA    */
/* Input(s)           : u4CtxId - Context ID                             */
/* Output(s)          : None                                             */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                      */
/*************************************************************************/
INT4
Bgp4VplsAuditVplsSpecEntry (UINT4 u4CtxId)
{
    tVplsSpecInfo      *pTmpVplsInfo = NULL;
    tVplsSpecInfo      *pNextTmpVplsInfo = NULL;
    UINT4               u4HashKey;
    tExtCommProfile    *pExtCommProfile = NULL;
    tExtCommProfile    *pTmpExtCommProfile = NULL;
    
    UNUSED_PARAM (u4CtxId);

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
            BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);
    /* Scan the global VPLS Hash table to get the information */
    if (BGP4_VPLS_GLOBAL_INFO != NULL)
    {
        TMO_HASH_Scan_Table (BGP4_VPLS_GLOBAL_INFO, u4HashKey)
        {
            TMO_HASH_DYN_Scan_Bucket (BGP4_VPLS_GLOBAL_INFO, u4HashKey,
                    pTmpVplsInfo,pNextTmpVplsInfo, tVplsSpecInfo *)
            {
                /*Delete Stale VPLS Spec Info*/
                if (BGP4_VPLS_SPEC_FLAG (pTmpVplsInfo) == BGP4_VPLS_SPEC_STALE)
                {
                    Bgp4VplsDeleteVplsStaleEntry (pTmpVplsInfo, u4CtxId); 
                }
                else
                {
                    /*Delete Stale Import and Export RT*/
                    if ((TMO_SLL_Count (BGP4_VPLS_SPEC_IMPORT_TARGETS (pTmpVplsInfo)) != 0))
                    {
                        BGP_SLL_DYN_Scan (BGP4_VPLS_SPEC_IMPORT_TARGETS (pTmpVplsInfo),
                                pExtCommProfile,pTmpExtCommProfile, tExtCommProfile *)
                        {
                            if (pExtCommProfile->u1Flag == BGP4_VPLS_RT_STALE)
                            {
                                Bgp4VplsDeleteRT (pTmpVplsInfo, (tTMO_SLL_NODE *) pExtCommProfile
                                                                ,BGP4_IMPORT_ROUTE_TARGET_TYPE);
                            }
                        }
                    }

                    if ((TMO_SLL_Count (BGP4_VPLS_SPEC_EXPORT_TARGETS (pTmpVplsInfo)) != 0))
                    {
                        BGP_SLL_DYN_Scan (BGP4_VPLS_SPEC_EXPORT_TARGETS (pTmpVplsInfo),
                                pExtCommProfile, pTmpExtCommProfile,tExtCommProfile *)
                        {
                            if (pExtCommProfile->u1Flag == BGP4_VPLS_RT_STALE)
                            {
                                Bgp4VplsDeleteRT (pTmpVplsInfo, (tTMO_SLL_NODE *) pExtCommProfile
                                                                ,BGP4_EXPORT_ROUTE_TARGET_TYPE);
                            }
                        }
                    }
                }
            }
        }
    }
    return BGP4_SUCCESS;
}
/*************************************************************************/
/* Function Name      : Bgp4VplsDeleteVplsStaleEntry                     */
/* Description        : This function will delete the Stale VPLS Spec    */
/*                      entry from from Link List                        */
/* Input(s)           : u4CtxId - Context ID                             */
/*                      tVplsSpecInfo: VPLS Spec Info                    */
/* Output(s)          : None                                             */
/* Returns            : BGP4_SUCCESS                                     */
/*************************************************************************/
INT4
Bgp4VplsDeleteVplsStaleEntry (tVplsSpecInfo * pVplsSpecInfo, UINT4 u4Context)
{

    tExtCommProfile    *pExtCommProfile = NULL;
    tExtCommProfile    *pTmpExtCommProfile = NULL;
    UINT1               u1HashKey = 0;
    
    UNUSED_PARAM (u4Context);
    /* Delete all RT related to VPLS Instance*/
    
    if ((TMO_SLL_Count (BGP4_VPLS_SPEC_IMPORT_TARGETS (pVplsSpecInfo)) != 0))
    {
        BGP_SLL_DYN_Scan(BGP4_VPLS_SPEC_IMPORT_TARGETS (pVplsSpecInfo),
                pExtCommProfile,pTmpExtCommProfile, tExtCommProfile *)
        {
            Bgp4VplsDeleteRT (pVplsSpecInfo, (tTMO_SLL_NODE *) pExtCommProfile, 
                    BGP4_IMPORT_ROUTE_TARGET_TYPE);
        }
    }

    if ((TMO_SLL_Count (BGP4_VPLS_SPEC_EXPORT_TARGETS (pVplsSpecInfo)) != 0))
    {
        BGP_SLL_DYN_Scan(BGP4_VPLS_SPEC_EXPORT_TARGETS (pVplsSpecInfo),
                pExtCommProfile,pTmpExtCommProfile, tExtCommProfile *)
        {
            Bgp4VplsDeleteRT (pVplsSpecInfo, (tTMO_SLL_NODE *) pExtCommProfile,
                    BGP4_EXPORT_ROUTE_TARGET_TYPE);
        }
    }

    /* delete the VPLS information from global VPLS hash table */
    Bgp4VplsGetVplsHashKey (BGP4_VPLS_SPEC_VPLS_INDX (pVplsSpecInfo),
            &u1HashKey);

    TMO_HASH_Delete_Node (BGP4_VPLS_GLOBAL_INFO,
            (tTMO_HASH_NODE *) pVplsSpecInfo,
            (UINT4) u1HashKey);
    Bgp4MemReleaseVplsSpecEntry (pVplsSpecInfo);

    return BGP4_SUCCESS;
}
/*************************************************************************/
/* Function Name      : Bgp4VplsDeleteRT                                 */
/* Description        : This function delete the RT entry from VPLS spec */
/*                      info                                             */
/* Input(s)           : tVplsSpecInfo: VPLS SpecInfo                     */
/*                      tTMO_SLL_NODE: Link List node of RT in VPLS Spec */
/* Output(s)          : None                                             */
/* Returns            : BGP4_SUCCESS                                     */
/*************************************************************************/
INT4
Bgp4VplsDeleteRT (tVplsSpecInfo * pVplsSpecInfo,tTMO_SLL_NODE * pExtCommProfile, UINT1 u1RtType)
{
    if (u1RtType == BGP4_IMPORT_ROUTE_TARGET_TYPE)
    {
        TMO_SLL_Delete (BGP4_VPLS_SPEC_IMPORT_TARGETS (pVplsSpecInfo),
                (tTMO_SLL_NODE *) pExtCommProfile);
    }
    if (u1RtType == BGP4_EXPORT_ROUTE_TARGET_TYPE)
    {
        TMO_SLL_Delete (BGP4_VPLS_SPEC_EXPORT_TARGETS (pVplsSpecInfo),
                (tTMO_SLL_NODE *) pExtCommProfile);
    }
    EXT_COMM_PROFILE_FREE (pExtCommProfile);
    pExtCommProfile = NULL;

    return BGP4_SUCCESS;
}

/*************************************************************************/
/* Function Name      : Bgp4VplsDeleteStaleVplsLocalRoutes               */
/* Description        : This function will Delete all the VPLS Stale     */
/*                      routes after EOVPLS comes from MPLS-L2VPN        */
/* Input(s)           : u4CtxId - Context ID                             */
/* Output(s)          : None                                             */
/* Returns            : BGP4_SUCCESS / BGP4_FAILURE                      */
/*************************************************************************/
INT4
Bgp4VplsDeleteStaleVplsLocalRoutes (UINT4 u4Context)
{
    tRouteProfile      *pRtProfile = NULL;
    tRouteProfile      *pNextRt = NULL;
    tRouteProfile      *pNextRoute = NULL;
    VOID               *pRibNode = NULL;
    UINT4               u4RtCnt = 0;
    INT4                i4Sts = BGP4_TRUE;
    INT4                i4Ret;
    INT4                i4DelRtRet;

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_ENTRY,
            BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);

    pRibNode = BGP4_VPLS_RIB_TREE (u4Context);

    /* Get the first route */
    i4Sts = Bgp4RibhGetFirstEntry (CAP_MP_L2VPN_VPLS, &pRtProfile,
            &pRibNode, TRUE);
    if (i4Sts == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
                BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);
        return (BGP4_FALSE);
    }

    for (;;)
    {
        i4Ret = Bgp4RibhGetNextEntry (pRtProfile, u4Context,
                &pNextRoute, &pRibNode, TRUE);
        pNextRt = pRtProfile;
        while (pNextRt != NULL)
        {
            pRtProfile = pNextRt;
            if (BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID)
            {
                BGP4_RT_GET_NEXT (pRtProfile, u4Context, pNextRt);
                continue;
            }
            else
            {
                if ((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_STALE)
                        == BGP4_RT_STALE)
                {
                    /*delete from RIB */
                    BGP4_RT_GET_NEXT (pRtProfile, u4Context, pNextRt);
                    i4DelRtRet = Bgp4RibhDelRtEntry (pRtProfile,
                            pRibNode, u4Context,
                            BGP4_TRUE);
                    if (i4DelRtRet != BGP4_FAILURE)
                    {
                        u4RtCnt++;
                    }
                    else
                    {
                        BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_CRI_TRC,
                                BGP4_MOD_NAME,
                                "\t%s:%d !!! Entry deletion from RIB failed !!!\n",
                                __func__, __LINE__);
                    }
                }
                else
                {
                    BGP4_RT_GET_NEXT (pRtProfile, u4Context, pNextRt);
                }
            }
        }
        if (i4Ret == BGP4_FAILURE)
        {
            if (u4RtCnt == 0)
            {
                i4Sts = BGP4_FALSE;
                break;
            }
            else
            {
                i4Sts = BGP4_TRUE;
                break;
            }
        }
        else
        {
            if (pNextRoute == NULL)
            {
                i4Sts = BGP4_TRUE;
                break;
            }
        }
        pRtProfile = pNextRoute;
        pNextRoute = NULL;
    }

    BGP4_TRC (NULL, BGP4_TRC_FLAG, BGP4_VPLS_FUN_EXIT,
            BGP4_MOD_NAME, "\t%s:%d\n", __func__, __LINE__);
    return i4Sts;
}

#endif /* MPLS_WANTED */
#endif /* VPLS_GR_WANTED*/
#endif /*L2RED_WANTED*/
#endif /* _BGP4VPLSSYNCHANDLER_C */
