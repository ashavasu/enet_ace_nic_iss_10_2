/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: bgmpinit.c,v 1.6 2013/02/07 12:18:57 siva Exp $
 * *
 * * Description: BGP MPE initialize routines
 * *********************************************************************/
#ifndef BGMPINIT_C
#define BGMPINIT_C

#include "bgp4com.h"

/********************************************************************/
/* Function Name   : Bgp4MpeInit                                    */
/* Description     : Allocates memory for the pools used in         */
/*                   Multiprotocol Extensions and Initializes       */
/*                   global variables                               */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Global Variables Referred : BGP4_MPE_GLOBAL_DATA(BGP4_DFLT_VRFID)                 */
/* Global variables Modified : BGP4_MPE_GLOBAL_DATA(BGP4_DFLT_VRFID)                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   :  BGP4_SUCCESS or BGP4_FAILURE        */
/********************************************************************/
INT4
Bgp4MpeInit (UINT4 u4CxtId)
{
    /* Initialize the global variables here */
    BGP4_MPE_ALLOC_AFISAFI_INSTANCE_CNT (u4CxtId) = 0;

    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4MpeShutdown                                */
/* Description     : De-allocates memory for the pools used in      */
/*                   Multiprotocol Extensions and Initializes       */
/*                   global variables                               */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Global Variables Referred : BGP4_MPE_GLOBAL_DATA(BGP4_DFLT_VRFID)                 */
/* Global variables Modified : BGP4_MPE_GLOBAL_DATA(BGP4_DFLT_VRFID)                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   :  BGP4_SUCCESS or BGP4_FAILURE        */
/********************************************************************/
INT4
Bgp4MpeShutdown (VOID)
{

    BGP4_MPE_RT_SNPA_POOL_ID = 0;
    BGP4_MPE_RT_AFISAFI_INSTANCE_POOL_ID = 0;
    return BGP4_SUCCESS;
}

#endif /* End of BGMPINIT_C */
