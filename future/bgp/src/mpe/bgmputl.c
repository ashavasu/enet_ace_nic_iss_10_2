/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgmputl.c,v 1.15 2017/09/15 06:19:54 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
#ifndef BGMPUTL_C
#define BGMPUTL_C

#include "bgp4com.h"
#ifdef VPLSADS_WANTED
#include "bgp4vpls.h"
#endif
/********************************************************************/
/* Function Name   : Bgp4MpeInitSnpaNode                            */
/* Description     : Initializes an SNPA node                       */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   :  BGP4_SUCCESS                        */
/********************************************************************/
INT4
Bgp4MpeInitSnpaNode (tSnpaInfoNode * pSnpaInfoNode)
{
    MEMSET (pSnpaInfoNode, 0, sizeof (tSnpaInfoNode));
    TMO_SLL_Init_Node (&(BGP4_MPE_SNPA_NEXT (pSnpaInfoNode)));
    BGP4_MPE_SNPA_NODE_LEN (pSnpaInfoNode) = 0;
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4MpeAllocateSnpaNode                        */
/* Description     : Allocates memory for SNPA node                 */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Global Variables Referred : BGP4_MPE_RT_SNPA_POOL_ID             */
/*                             BGP4_MPE_ALLOC_SNPA_NODE_CNT(BGP4_DFLT_VRFID)         */
/* Global variables Modified : BGP4_MPE_ALLOC_SNPA_NODE_CNT(BGP4_DFLT_VRFID)         */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   :  NULL or Pointer to the allocated    */
/*                              SNPA node information               */
/********************************************************************/
tSnpaInfoNode      *
Bgp4MpeAllocateSnpaNode (UINT4 u4Size)
{
    tSnpaInfoNode      *pSnpaInfoNode = NULL;
    UINT1              *pu1Block = NULL;

    UNUSED_PARAM (u4Size);
    if (MemAllocateMemBlock (BGP4_MPE_RT_SNPA_POOL_ID, &pu1Block)
        != MEM_SUCCESS)
    {
        return (NULL);
    }

    pSnpaInfoNode = (tSnpaInfoNode *) (VOID *) (pu1Block);
    if (pSnpaInfoNode == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBGP4 : (MPE) Allocate SNPA node failed\n");
        gu4BgpDebugCnt[MAX_BGP_SNPA_INFO_NODES_SIZING_ID]++;
        return (NULL);
    }

    Bgp4MpeInitSnpaNode (pSnpaInfoNode);
    BGP4_MPE_ALLOC_SNPA_NODE_CNT++;
    return (pSnpaInfoNode);
}

/********************************************************************/
/* Function Name   : Bgp4MpeReleaseSnpaNode                         */
/* Description     : Releases memory for SNPA node                  */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Global Variables Referred : BGP4_MPE_RT_SNPA_POOL_ID             */
/*                             BGP4_MPE_ALLOC_SNPA_NODE_CNT(BGP4_DFLT_VRFID)         */
/* Global variables Modified : BGP4_MPE_ALLOC_SNPA_NODE_CNT(BGP4_DFLT_VRFID)         */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_FAILURE or BGP4_SUCCESS         */
/********************************************************************/
INT4
Bgp4MpeReleaseSnpaNode (tSnpaInfoNode * pSnpaInfoNode)
{

    if (MemReleaseMemBlock (BGP4_MPE_RT_SNPA_POOL_ID, (UINT1 *) pSnpaInfoNode)
        != MEM_SUCCESS)
    {
        return BGP4_FAILURE;
    }

    (BGP4_MPE_ALLOC_SNPA_NODE_CNT)--;
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4MpeInitAfiSafiInstance                     */
/* Description     : Initializes an Asafi Instance                  */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   :  BGP4_SUCCESS/BGP4_FAILURE           */
/********************************************************************/
INT4
Bgp4MpeInitAfiSafiInstance (tBgp4PeerEntry * pPeerentry, UINT4 u4Index)
{
    switch (u4Index)
    {
        case BGP4_IPV4_UNI_INDEX:
            /* Create Hash Table for storing the routes filtered while advertising
             * to this peer. */
            BGP4_PEER_IPV4_OUTPUT_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_IPV4_OUTPUT_ROUTES (pPeerentry) == NULL)
            {
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer Output "
                          "List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the routes for advertising when the
             * min-as origination timer expires. */
            BGP4_PEER_IPV4_NEW_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_IPV4_NEW_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_OUTPUT_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer New Route "
                          "List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the routes for advertising when the
             * min-route advertisement timer expires. */
            BGP4_PEER_IPV4_NEW_LOCAL_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_IPV4_NEW_LOCAL_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_OUTPUT_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_NEW_ROUTES (pPeerentry),
                                       NULL);
                BGP4_PEER_IPV4_NEW_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer New Local Route "
                          "List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the advertised withdrawn routes */
            BGP4_PEER_IPV4_ADVT_WITH_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_IPV4_ADVT_WITH_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_OUTPUT_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_NEW_ROUTES (pPeerentry),
                                       NULL);
                BGP4_PEER_IPV4_NEW_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_NEW_LOCAL_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_NEW_LOCAL_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer Advt "
                          "Withdrawn Route List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the advertised feasible routes */
            BGP4_PEER_IPV4_ADVT_FEAS_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_IPV4_ADVT_FEAS_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_OUTPUT_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_NEW_ROUTES (pPeerentry),
                                       NULL);
                BGP4_PEER_IPV4_NEW_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_NEW_LOCAL_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_NEW_LOCAL_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_ADVT_WITH_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_ADVT_WITH_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer Advt Feasible "
                          "Route List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            TMO_SLL_Init (BGP4_PEER_IPV4_DEINIT_RT_LIST (pPeerentry));

            /* Initialize the Peer Route List for all <AFI> <SAFI> combinations. */
            BGP4_DLL_FIRST_ROUTE (BGP4_PEER_ROUTE_LIST (pPeerentry,
                                                        BGP4_IPV4_UNI_INDEX)) =
                NULL;
            BGP4_DLL_LAST_ROUTE (BGP4_PEER_ROUTE_LIST
                                 (pPeerentry, BGP4_IPV4_UNI_INDEX)) = NULL;
            BGP4_DLL_COUNT (BGP4_PEER_ROUTE_LIST
                            (pPeerentry, BGP4_IPV4_UNI_INDEX)) = 0;

            BGP4_PEER_IPV4_PREFIX_RCVD_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV4_PREFIX_SENT_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV4_WITHDRAWS_RCVD_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV4_WITHDRAWS_SENT_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV4_IN_PREFIXES_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV4_IN_PREFIXES_ACPTD_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV4_IN_PREFIXES_RJCTD_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV4_OUT_PREFIXES_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV4_INVALID_MPE_UPDATE_CNT (pPeerentry) = 0;
            break;

#ifdef L3VPN
        case BGP4_IPV4_LBLD_INDEX:
            /* Create Hash Table for storing the routes filtered while advertising
             * to this peer. */
            BGP4_PEER_IPV4_LBLD_OUTPUT_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_IPV4_LBLD_OUTPUT_ROUTES (pPeerentry) == NULL)
            {
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer Output "
                          "List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the routes for advertising when the
             * min-as origination timer expires. */
            BGP4_PEER_IPV4_LBLD_NEW_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_IPV4_LBLD_NEW_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_LBLD_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_LBLD_OUTPUT_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer New Route "
                          "List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the routes for advertising when the
             * min-route advertisement timer expires. */
            BGP4_PEER_IPV4_LBLD_NEW_LOCAL_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_IPV4_LBLD_NEW_LOCAL_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_LBLD_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_LBLD_OUTPUT_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_LBLD_NEW_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_LBLD_NEW_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer New Local Route "
                          "List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the advertised withdrawn routes */
            BGP4_PEER_IPV4_LBLD_ADVT_WITH_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_IPV4_LBLD_ADVT_WITH_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_LBLD_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_LBLD_OUTPUT_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_LBLD_NEW_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_LBLD_NEW_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_LBLD_NEW_LOCAL_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_LBLD_NEW_LOCAL_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer Advt "
                          "Withdrawn Route List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the advertised feasible routes */
            BGP4_PEER_IPV4_LBLD_ADVT_FEAS_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_IPV4_LBLD_ADVT_FEAS_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_LBLD_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_LBLD_OUTPUT_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_LBLD_NEW_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_LBLD_NEW_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_LBLD_NEW_LOCAL_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_LBLD_NEW_LOCAL_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_LBLD_ADVT_WITH_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_LBLD_ADVT_WITH_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer Advt Feasible "
                          "Route List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            TMO_SLL_Init (BGP4_PEER_IPV4_LBLD_DEINIT_RT_LIST (pPeerentry));

            /* Initialize the Peer Route List for all <AFI> <SAFI> combinations. */
            BGP4_DLL_FIRST_ROUTE (BGP4_PEER_ROUTE_LIST (pPeerentry,
                                                        BGP4_IPV4_LBLD_INDEX)) =
                NULL;
            BGP4_DLL_LAST_ROUTE (BGP4_PEER_ROUTE_LIST
                                 (pPeerentry, BGP4_IPV4_LBLD_INDEX)) = NULL;
            BGP4_DLL_COUNT (BGP4_PEER_ROUTE_LIST
                            (pPeerentry, BGP4_IPV4_LBLD_INDEX)) = 0;

            BGP4_PEER_IPV4_LBLD_PREFIX_RCVD_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV4_LBLD_PREFIX_SENT_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV4_LBLD_WITHDRAWS_RCVD_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV4_LBLD_WITHDRAWS_SENT_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV4_LBLD_IN_PREFIXES_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV4_LBLD_IN_PREFIXES_ACPTD_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV4_LBLD_IN_PREFIXES_RJCTD_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV4_LBLD_OUT_PREFIXES_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV4_LBLD_INVALID_MPE_UPDATE_CNT (pPeerentry) = 0;
            break;
        case BGP4_VPN4_UNI_INDEX:
            /* Create Hash Table for storing the routes filtered while advertising
             * to this peer. */
            BGP4_PEER_VPN4_OUTPUT_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_VPN4_OUTPUT_ROUTES (pPeerentry) == NULL)
            {
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer Output "
                          "List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the routes for advertising when the
             * min-as origination timer expires. */
            BGP4_PEER_VPN4_NEW_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_VPN4_NEW_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_VPN4_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPN4_OUTPUT_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer New Route "
                          "List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the routes for advertising when the
             * min-route advertisement timer expires. */
            BGP4_PEER_VPN4_NEW_LOCAL_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_VPN4_NEW_LOCAL_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_VPN4_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPN4_OUTPUT_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_VPN4_NEW_ROUTES (pPeerentry),
                                       NULL);
                BGP4_PEER_VPN4_NEW_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer New Local Route "
                          "List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the advertised withdrawn routes */
            BGP4_PEER_VPN4_ADVT_WITH_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_VPN4_ADVT_WITH_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_VPN4_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPN4_OUTPUT_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_VPN4_NEW_ROUTES (pPeerentry),
                                       NULL);
                BGP4_PEER_VPN4_NEW_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_VPN4_NEW_LOCAL_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPN4_NEW_LOCAL_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer Advt "
                          "Withdrawn Route List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the advertised feasible routes */
            BGP4_PEER_VPN4_ADVT_FEAS_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_VPN4_ADVT_FEAS_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_VPN4_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPN4_OUTPUT_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_VPN4_NEW_ROUTES (pPeerentry),
                                       NULL);
                BGP4_PEER_VPN4_NEW_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_VPN4_NEW_LOCAL_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPN4_NEW_LOCAL_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_VPN4_ADVT_WITH_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPN4_ADVT_WITH_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer Advt Feasible "
                          "Route List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            TMO_SLL_Init (BGP4_PEER_VPN4_DEINIT_RT_LIST (pPeerentry));

            /* Initialize the Peer Route List for all <AFI> <SAFI> combinations. */
            BGP4_DLL_FIRST_ROUTE (BGP4_PEER_ROUTE_LIST (pPeerentry,
                                                        BGP4_VPN4_UNI_INDEX)) =
                NULL;
            BGP4_DLL_LAST_ROUTE (BGP4_PEER_ROUTE_LIST
                                 (pPeerentry, BGP4_VPN4_UNI_INDEX)) = NULL;
            BGP4_DLL_COUNT (BGP4_PEER_ROUTE_LIST
                            (pPeerentry, BGP4_VPN4_UNI_INDEX)) = 0;

            BGP4_PEER_VPN4_PREFIX_RCVD_CNT (pPeerentry) = 0;
            BGP4_PEER_VPN4_PREFIX_SENT_CNT (pPeerentry) = 0;
            BGP4_PEER_VPN4_WITHDRAWS_RCVD_CNT (pPeerentry) = 0;
            BGP4_PEER_VPN4_WITHDRAWS_SENT_CNT (pPeerentry) = 0;
            BGP4_PEER_VPN4_IN_PREFIXES_CNT (pPeerentry) = 0;
            BGP4_PEER_VPN4_IN_PREFIXES_ACPTD_CNT (pPeerentry) = 0;
            BGP4_PEER_VPN4_IN_PREFIXES_RJCTD_CNT (pPeerentry) = 0;
            BGP4_PEER_VPN4_OUT_PREFIXES_CNT (pPeerentry) = 0;
            BGP4_PEER_VPN4_INVALID_MPE_UPDATE_CNT (pPeerentry) = 0;
            break;
#endif
#ifdef BGP4_IPV6_WANTED
        case BGP4_IPV6_UNI_INDEX:
            /* Create Hash Table for storing the routes filtered while advertising
             * to this peer. */
            BGP4_PEER_IPV6_OUTPUT_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_IPV6_OUTPUT_ROUTES (pPeerentry) == NULL)
            {
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer Output List "
                          "FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the routes for advertising when the
             * min-as origination timer expires. */
            BGP4_PEER_IPV6_NEW_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_IPV6_NEW_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV6_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV6_OUTPUT_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer New Route "
                          "List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the routes for advertising when the
             * min-route advertisement timer expires. */
            BGP4_PEER_IPV6_NEW_LOCAL_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_IPV6_NEW_LOCAL_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV6_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV6_OUTPUT_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_IPV6_NEW_ROUTES (pPeerentry),
                                       NULL);
                BGP4_PEER_IPV6_NEW_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer New Local "
                          "Route List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the advertised withdrawn routes */
            BGP4_PEER_IPV6_ADVT_WITH_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_IPV6_ADVT_WITH_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV6_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV6_OUTPUT_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_IPV6_NEW_ROUTES (pPeerentry),
                                       NULL);
                BGP4_PEER_IPV6_NEW_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_IPV6_NEW_LOCAL_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV6_NEW_LOCAL_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer Advt "
                          "Withdrawn Route List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the advertised feasible routes */
            BGP4_PEER_IPV6_ADVT_FEAS_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_IPV6_ADVT_FEAS_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV6_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV6_OUTPUT_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_IPV6_NEW_ROUTES (pPeerentry),
                                       NULL);
                BGP4_PEER_IPV6_NEW_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_IPV6_NEW_LOCAL_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV6_NEW_LOCAL_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_IPV6_ADVT_WITH_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV6_ADVT_WITH_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer Advt Feasible "
                          "Route List FAILED !!!\n");
                return BGP4_FAILURE;
            }
            TMO_SLL_Init (BGP4_PEER_IPV6_DEINIT_RT_LIST (pPeerentry));

            BGP4_DLL_FIRST_ROUTE (BGP4_PEER_ROUTE_LIST (pPeerentry,
                                                        BGP4_IPV6_UNI_INDEX)) =
                NULL;
            BGP4_DLL_LAST_ROUTE (BGP4_PEER_ROUTE_LIST
                                 (pPeerentry, BGP4_IPV6_UNI_INDEX)) = NULL;
            BGP4_DLL_COUNT (BGP4_PEER_ROUTE_LIST
                            (pPeerentry, BGP4_IPV6_UNI_INDEX)) = 0;

            BGP4_PEER_IPV6_PREFIX_RCVD_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV6_PREFIX_SENT_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV6_WITHDRAWS_RCVD_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV6_WITHDRAWS_SENT_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV6_IN_PREFIXES_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV6_IN_PREFIXES_ACPTD_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV6_IN_PREFIXES_RJCTD_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV6_OUT_PREFIXES_CNT (pPeerentry) = 0;
            BGP4_PEER_IPV6_INVALID_MPE_UPDATE_CNT (pPeerentry) = 0;
            break;
#endif
#ifdef VPLSADS_WANTED
        case BGP4_L2VPN_VPLS_INDEX:
            /* Create Hash Table for storing the routes filtered while advertising
             * to this peer. */
            BGP4_PEER_VPLS_OUTPUT_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_VPLS_OUTPUT_ROUTES (pPeerentry) == NULL)
            {
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer Output List "
                          "FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the routes for advertising when the
             * min-as origination timer expires. */
            BGP4_PEER_VPLS_NEW_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_VPLS_NEW_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_VPLS_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPLS_OUTPUT_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer New Route "
                          "List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the routes for advertising when the
             * min-route advertisement timer expires. */
            BGP4_PEER_VPLS_NEW_LOCAL_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_VPLS_NEW_LOCAL_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_VPLS_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPLS_OUTPUT_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_VPLS_NEW_ROUTES (pPeerentry),
                                       NULL);
                BGP4_PEER_VPLS_NEW_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer New Local "
                          "Route List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the advertised withdrawn routes */
            BGP4_PEER_VPLS_ADVT_WITH_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_VPLS_ADVT_WITH_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_VPLS_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPLS_OUTPUT_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_VPLS_NEW_ROUTES (pPeerentry),
                                       NULL);
                BGP4_PEER_VPLS_NEW_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_VPLS_NEW_LOCAL_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPLS_NEW_LOCAL_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer Advt "
                          "Withdrawn Route List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the advertised feasible routes */
            BGP4_PEER_VPLS_ADVT_FEAS_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_VPLS_ADVT_FEAS_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_VPLS_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPLS_OUTPUT_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_VPLS_NEW_ROUTES (pPeerentry),
                                       NULL);
                BGP4_PEER_VPLS_NEW_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_VPLS_NEW_LOCAL_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPLS_NEW_LOCAL_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_VPLS_ADVT_WITH_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPLS_ADVT_WITH_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer Advt Feasible "
                          "Route List FAILED !!!\n");
                return BGP4_FAILURE;
            }
            TMO_SLL_Init (BGP4_PEER_VPLS_DEINIT_RT_LIST (pPeerentry));

            BGP4_DLL_FIRST_ROUTE (BGP4_PEER_ROUTE_LIST (pPeerentry,
                                                        BGP4_L2VPN_VPLS_INDEX))
                = NULL;
            BGP4_DLL_LAST_ROUTE (BGP4_PEER_ROUTE_LIST
                                 (pPeerentry, BGP4_L2VPN_VPLS_INDEX)) = NULL;
            BGP4_DLL_COUNT (BGP4_PEER_ROUTE_LIST
                            (pPeerentry, BGP4_L2VPN_VPLS_INDEX)) = 0;

            BGP4_PEER_VPLS_PREFIX_RCVD_CNT (pPeerentry) = 0;
            BGP4_PEER_VPLS_PREFIX_SENT_CNT (pPeerentry) = 0;
            BGP4_PEER_VPLS_WITHDRAWS_RCVD_CNT (pPeerentry) = 0;
            BGP4_PEER_VPLS_WITHDRAWS_SENT_CNT (pPeerentry) = 0;
            BGP4_PEER_VPLS_IN_PREFIXES_CNT (pPeerentry) = 0;
            BGP4_PEER_VPLS_IN_PREFIXES_ACPTD_CNT (pPeerentry) = 0;
            BGP4_PEER_VPLS_IN_PREFIXES_RJCTD_CNT (pPeerentry) = 0;
            BGP4_PEER_VPLS_OUT_PREFIXES_CNT (pPeerentry) = 0;
            BGP4_PEER_VPLS_INVALID_MPE_UPDATE_CNT (pPeerentry) = 0;
            break;
#endif
#ifdef EVPN_WANTED
        case BGP4_L2VPN_EVPN_INDEX:
            /* Create Hash Table for storing the routes filtered while advertising
             * to this peer. */
            BGP4_PEER_EVPN_OUTPUT_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_EVPN_OUTPUT_ROUTES (pPeerentry) == NULL)
            {
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer Output List "
                          "FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the routes for advertising when the
             * min-as origination timer expires. */
            BGP4_PEER_EVPN_NEW_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_EVPN_NEW_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_EVPN_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_EVPN_OUTPUT_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer New Route "
                          "List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the routes for advertising when the
             * min-route advertisement timer expires. */
            BGP4_PEER_EVPN_NEW_LOCAL_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_EVPN_NEW_LOCAL_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_EVPN_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_EVPN_OUTPUT_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_EVPN_NEW_ROUTES (pPeerentry),
                                       NULL);
                BGP4_PEER_EVPN_NEW_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer New Local "
                          "Route List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the advertised withdrawn routes */
            BGP4_PEER_EVPN_ADVT_WITH_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_EVPN_ADVT_WITH_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_EVPN_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_EVPN_OUTPUT_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_EVPN_NEW_ROUTES (pPeerentry),
                                       NULL);
                BGP4_PEER_EVPN_NEW_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_EVPN_NEW_LOCAL_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_EVPN_NEW_LOCAL_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer Advt "
                          "Withdrawn Route List FAILED !!!\n");
                return BGP4_FAILURE;
            }

            /* Create Hash Table for storing the advertised feasible routes */
            BGP4_PEER_EVPN_ADVT_FEAS_ROUTES (pPeerentry) =
                TMO_HASH_Create_Table (BGP4_MAX_ROUTE_HASH_TBL_SIZE, NULL,
                                       FALSE);
            if (BGP4_PEER_EVPN_ADVT_FEAS_ROUTES (pPeerentry) == NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_EVPN_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_EVPN_OUTPUT_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_EVPN_NEW_ROUTES (pPeerentry),
                                       NULL);
                BGP4_PEER_EVPN_NEW_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_EVPN_NEW_LOCAL_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_EVPN_NEW_LOCAL_ROUTES (pPeerentry) = NULL;
                TMO_HASH_Delete_Table (BGP4_PEER_EVPN_ADVT_WITH_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_EVPN_ADVT_WITH_ROUTES (pPeerentry) = NULL;
                BGP4_TRC (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerentry)),
                          BGP4_TRC_FLAG,
                          BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC |
                          BGP4_CONTROL_PATH_TRC, BGP4_MOD_NAME,
                          "\tCREATING HASH TABLE For Peer Advt Feasible "
                          "Route List FAILED !!!\n");
                return BGP4_FAILURE;
            }
            TMO_SLL_Init (BGP4_PEER_EVPN_DEINIT_RT_LIST (pPeerentry));

            BGP4_DLL_FIRST_ROUTE (BGP4_PEER_ROUTE_LIST (pPeerentry,
                                                        BGP4_L2VPN_EVPN_INDEX))
                = NULL;
            BGP4_DLL_LAST_ROUTE (BGP4_PEER_ROUTE_LIST
                                 (pPeerentry, BGP4_L2VPN_EVPN_INDEX)) = NULL;
            BGP4_DLL_COUNT (BGP4_PEER_ROUTE_LIST
                            (pPeerentry, BGP4_L2VPN_EVPN_INDEX)) = 0;
            break;
#endif
        default:
            return BGP4_FAILURE;
    }

    if (BGP4_PEER_RTREF_DATA_PTR (pPeerentry, u4Index) != NULL)
    {
        BGP4_RTREF_MSG_SENT_CTR (pPeerentry, u4Index) = 0;
        BGP4_RTREF_MSG_RCVD_CTR (pPeerentry, u4Index) = 0;
        BGP4_RTREF_MSG_TXERR_CTR (pPeerentry, u4Index) = 0;
        BGP4_RTREF_MSG_RCVD_INVALID_CTR (pPeerentry, u4Index) = 0;
        BGP4_RTREF_MSG_REQ_PEER (pPeerentry, u4Index) = 0;
    }
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4MpeAllocateAfiSafiInstance                 */
/* Description     : Allocates memory for <AFI, SAFI> instance      */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Global Variables Referred : BGP4_MPE_RT_AFISAFI_INSTANCE_POOL_ID */
/*                             BGP4_MPE_ALLOC_AFISAFI_INSTANCE_CNT(BGP4_DFLT_VRFID)  */
/* Global variables Modified : BGP4_MPE_ALLOC_AFISAFI_INSTANCE_CNT(BGP4_DFLT_VRFID)  */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   :  NULL or Pointer to the allocated    */
/*                              <AFI, SAFI> instance information    */
/********************************************************************/
tAfiSafiSpecInfo   *
Bgp4MpeAllocateAfiSafiInstance (tBgp4PeerEntry * pPeerentry, UINT4 u4Index)
{
    tAfiSafiSpecInfo   *pAfiSafiInstance = NULL;
    UINT1              *pu1Block = NULL;
    INT4                i4RetStatus;

    i4RetStatus = MemAllocateMemBlock (BGP4_MPE_RT_AFISAFI_INSTANCE_POOL_ID,
                                       &pu1Block);
    if (i4RetStatus != MEM_SUCCESS)
    {
        return (NULL);
    }

    pAfiSafiInstance = (tAfiSafiSpecInfo *) (VOID *) (pu1Block);
    if (pAfiSafiInstance == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBGP4 : (MPE) Allocate <AFI, SAFI> instance failed\n");
        gu4BgpDebugCnt[MAX_BGP_AFI_SAFI_SPEC_INFOS_SIZING_ID]++;
        return (NULL);
    }

    /* Create Route refresh data pointer */
    i4RetStatus = MemAllocateMemBlock (BGP4_RTREF_MEM_POOL_ID, &pu1Block);
    if (i4RetStatus != MEM_SUCCESS)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBGP4 : (MPE) Allocate For Route Refresh Data failed\n");
        MemReleaseMemBlock (BGP4_MPE_RT_AFISAFI_INSTANCE_POOL_ID,
                            (UINT1 *) pAfiSafiInstance);
        gu4BgpDebugCnt[MAX_BGP_RT_REF_DATA_SIZING_ID]++;
        return (NULL);
    }

    /* Put the created instance pointer into the peer entry  and initialise
     * all the Lists and Counters in that */
    switch (u4Index)
    {
        case BGP4_IPV4_UNI_INDEX:
            BGP4_PEER_IPV4_AFISAFI_INSTANCE (pPeerentry) = pAfiSafiInstance;
            break;
#ifdef L3VPN
        case BGP4_IPV4_LBLD_INDEX:
            BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeerentry) =
                pAfiSafiInstance;
            break;
        case BGP4_VPN4_UNI_INDEX:
            BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeerentry) = pAfiSafiInstance;
            break;
#endif
#ifdef BGP4_IPV6_WANTED
        case BGP4_IPV6_UNI_INDEX:
            BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeerentry) = pAfiSafiInstance;
            break;
#endif
#ifdef VPLSADS_WANTED
        case BGP4_L2VPN_VPLS_INDEX:
            BGP4_PEER_VPLS_AFISAFI_INSTANCE (pPeerentry) = pAfiSafiInstance;
            break;
#endif
#ifdef EVPN_WANTED
        case BGP4_L2VPN_EVPN_INDEX:
            BGP4_PEER_EVPN_AFISAFI_INSTANCE (pPeerentry) = pAfiSafiInstance;
            break;
#endif
        default:
            MemReleaseMemBlock (BGP4_RTREF_MEM_POOL_ID, (UINT1 *) pu1Block);
            MemReleaseMemBlock (BGP4_MPE_RT_AFISAFI_INSTANCE_POOL_ID,
                                (UINT1 *) pAfiSafiInstance);
            return (NULL);
    }

    /* Store the allocated memory for Route Refresh Data. */
    BGP4_PEER_RTREF_DATA_PTR (pPeerentry, u4Index) =
        (tRtRefreshData *) (VOID *) (pu1Block);
    BGP4_RTREF_ALLOC_CTR (BGP4_PEER_CXT_ID (pPeerentry))++;

    if (Bgp4MpeInitAfiSafiInstance (pPeerentry, u4Index) == BGP4_FAILURE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tInitialisation for Peer Entry FAILED!!!\n");
        MemReleaseMemBlock (BGP4_MPE_RT_AFISAFI_INSTANCE_POOL_ID,
                            (UINT1 *) pAfiSafiInstance);
        MemReleaseMemBlock (BGP4_RTREF_MEM_POOL_ID, (UINT1 *) pu1Block);
        BGP4_PEER_RTREF_DATA_PTR (pPeerentry, u4Index) = NULL;
        switch (u4Index)
        {
            case BGP4_IPV4_UNI_INDEX:
                BGP4_PEER_IPV4_AFISAFI_INSTANCE (pPeerentry) = NULL;
                break;
#ifdef L3VPN
            case BGP4_IPV4_LBLD_INDEX:
                BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeerentry) = NULL;
                break;
            case BGP4_VPN4_UNI_INDEX:
                BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeerentry) = NULL;
                break;
#endif
#ifdef BGP4_IPV6_WANTED
            case BGP4_IPV6_UNI_INDEX:
                BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeerentry) = NULL;
                break;
#endif
#ifdef VPLSADS_WANTED
            case BGP4_L2VPN_VPLS_INDEX:
                BGP4_PEER_VPLS_AFISAFI_INSTANCE (pPeerentry) = NULL;
                break;
#endif
#ifdef EVPN_WANTED
            case BGP4_L2VPN_EVPN_INDEX:
                BGP4_PEER_EVPN_AFISAFI_INSTANCE (pPeerentry) = NULL;
                break;
#endif
        }
        return (NULL);
    }

    BGP4_MPE_ALLOC_AFISAFI_INSTANCE_CNT (BGP4_PEER_CXT_ID (pPeerentry))++;

    return (pAfiSafiInstance);
}

/********************************************************************/
/* Function Name   : Bgp4MpeReleaseAfiSafiInstance                  */
/* Description     : Releases memory of <AFI, SAFI> instance        */
/* Input(s)        : None                                           */
/* Output(s)       : None                                           */
/* Global Variables Referred : BGP4_MPE_RT_AFISAFI_INSTANCE_POOL_ID */
/*                             BGP4_MPE_ALLOC_AFISAFI_INSTANCE_CNT(BGP4_DFLT_VRFID)  */
/* Global variables Modified : BGP4_MPE_ALLOC_AFISAFI_INSTANCE_CNT(BGP4_DFLT_VRFID)  */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_FAILURE or BGP4_SUCCESS         */
/********************************************************************/
INT4
Bgp4MpeReleaseAfiSafiInstance (tBgp4PeerEntry * pPeerentry, UINT4 u4Index)
{
    tAfiSafiSpecInfo   *pAfiSafiInstance = NULL;
    UINT4               u4Routes = 0;
    INT4                i4Status;

    switch (u4Index)
    {
        case BGP4_IPV4_UNI_INDEX:
            pAfiSafiInstance = BGP4_PEER_IPV4_AFISAFI_INSTANCE (pPeerentry);
            break;
#ifdef L3VPN
        case BGP4_IPV4_LBLD_INDEX:
            pAfiSafiInstance =
                BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE (pPeerentry);
            break;
        case BGP4_VPN4_UNI_INDEX:
            pAfiSafiInstance = BGP4_PEER_VPN4_AFISAFI_INSTANCE (pPeerentry);
            break;
#endif
#ifdef BGP4_IPV6_WANTED
        case BGP4_IPV6_UNI_INDEX:
            pAfiSafiInstance = BGP4_PEER_IPV6_AFISAFI_INSTANCE (pPeerentry);
            break;
#endif
#ifdef VPLSADS_WANTED
        case BGP4_L2VPN_VPLS_INDEX:
            pAfiSafiInstance = BGP4_PEER_VPLS_AFISAFI_INSTANCE (pPeerentry);
            break;
#endif
#ifdef EVPN_WANTED
        case BGP4_L2VPN_EVPN_INDEX:
            pAfiSafiInstance = BGP4_PEER_EVPN_AFISAFI_INSTANCE (pPeerentry);
            break;
#endif
        default:
            return BGP4_FAILURE;
    }

    if (pAfiSafiInstance == NULL)
    {
        return BGP4_SUCCESS;
    }

    i4Status =
        Bgp4DshReleasePeerAsafiRtLists (pPeerentry, u4Index, BGP4_FALSE,
                                        &u4Routes);
    if (i4Status == BGP4_FALSE)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\t Failure in Bgp4DshReleasePeerAsafiRtLists!!!\n");
    }
    Bgp4DshReleasePeerRouteList (pPeerentry, u4Index);
    /* Free the Peer Hash Tables */
    switch (u4Index)
    {
        case BGP4_IPV4_UNI_INDEX:
            if (BGP4_PEER_IPV4_OUTPUT_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_OUTPUT_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_IPV4_NEW_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_NEW_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_NEW_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_IPV4_NEW_LOCAL_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_NEW_LOCAL_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_NEW_LOCAL_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_IPV4_ADVT_WITH_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_ADVT_WITH_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_ADVT_WITH_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_IPV4_ADVT_FEAS_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_ADVT_FEAS_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_ADVT_FEAS_ROUTES (pPeerentry) = NULL;
            }
            break;

#ifdef L3VPN
        case BGP4_IPV4_LBLD_INDEX:
            if (BGP4_PEER_IPV4_LBLD_OUTPUT_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_LBLD_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_LBLD_OUTPUT_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_IPV4_LBLD_NEW_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_LBLD_NEW_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_LBLD_NEW_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_IPV4_LBLD_NEW_LOCAL_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_LBLD_NEW_LOCAL_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_LBLD_NEW_LOCAL_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_IPV4_LBLD_ADVT_WITH_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_LBLD_ADVT_WITH_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_LBLD_ADVT_WITH_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_IPV4_LBLD_ADVT_FEAS_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV4_LBLD_ADVT_FEAS_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV4_LBLD_ADVT_FEAS_ROUTES (pPeerentry) = NULL;
            }
            break;
        case BGP4_VPN4_UNI_INDEX:
            if (BGP4_PEER_VPN4_OUTPUT_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_VPN4_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPN4_OUTPUT_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_VPN4_NEW_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_VPN4_NEW_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPN4_NEW_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_VPN4_NEW_LOCAL_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_VPN4_NEW_LOCAL_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPN4_NEW_LOCAL_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_VPN4_ADVT_WITH_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_VPN4_ADVT_WITH_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPN4_ADVT_WITH_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_VPN4_ADVT_FEAS_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_VPN4_ADVT_FEAS_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPN4_ADVT_FEAS_ROUTES (pPeerentry) = NULL;
            }
            break;
#endif
#ifdef BGP4_IPV6_WANTED
        case BGP4_IPV6_UNI_INDEX:
            if (BGP4_PEER_IPV6_OUTPUT_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV6_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV6_OUTPUT_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_IPV6_NEW_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV6_NEW_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV6_NEW_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_IPV6_NEW_LOCAL_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV6_NEW_LOCAL_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV6_NEW_LOCAL_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_IPV6_ADVT_WITH_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV6_ADVT_WITH_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV6_ADVT_WITH_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_IPV6_ADVT_FEAS_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_IPV6_ADVT_FEAS_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_IPV6_ADVT_FEAS_ROUTES (pPeerentry) = NULL;
            }
            break;
#endif
#ifdef VPLSADS_WANTED
        case BGP4_L2VPN_VPLS_INDEX:
            if (BGP4_PEER_VPLS_OUTPUT_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_VPLS_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPLS_OUTPUT_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_VPLS_NEW_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_VPLS_NEW_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPLS_NEW_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_VPLS_NEW_LOCAL_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_VPLS_NEW_LOCAL_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPLS_NEW_LOCAL_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_VPLS_ADVT_WITH_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_VPLS_ADVT_WITH_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPLS_ADVT_WITH_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_VPLS_ADVT_FEAS_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_VPLS_ADVT_FEAS_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_VPLS_ADVT_FEAS_ROUTES (pPeerentry) = NULL;
            }
            break;
#endif
#ifdef EVPN_WANTED
        case BGP4_L2VPN_EVPN_INDEX:
            if (BGP4_PEER_EVPN_OUTPUT_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_EVPN_OUTPUT_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_EVPN_OUTPUT_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_EVPN_NEW_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_EVPN_NEW_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_EVPN_NEW_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_EVPN_NEW_LOCAL_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_EVPN_NEW_LOCAL_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_EVPN_NEW_LOCAL_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_EVPN_ADVT_WITH_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_EVPN_ADVT_WITH_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_EVPN_ADVT_WITH_ROUTES (pPeerentry) = NULL;
            }
            if (BGP4_PEER_EVPN_ADVT_FEAS_ROUTES (pPeerentry) != NULL)
            {
                TMO_HASH_Delete_Table (BGP4_PEER_EVPN_ADVT_FEAS_ROUTES
                                       (pPeerentry), NULL);
                BGP4_PEER_EVPN_ADVT_FEAS_ROUTES (pPeerentry) = NULL;
            }
            break;
#endif
    }

    /* Release Route Refresh data */
    if (BGP4_PEER_RTREF_DATA_PTR (pPeerentry, u4Index) != NULL)
    {
        i4Status =
            MemReleaseMemBlock (BGP4_RTREF_MEM_POOL_ID,
                                (UINT1 *) BGP4_PEER_RTREF_DATA_PTR (pPeerentry,
                                                                    u4Index));
        if (i4Status != MEM_SUCCESS)
        {
            return BGP4_FAILURE;
        }
        BGP4_RTREF_ALLOC_CTR (BGP4_PEER_CXT_ID (pPeerentry))--;
    }

    if (MemReleaseMemBlock (BGP4_MPE_RT_AFISAFI_INSTANCE_POOL_ID,
                            (UINT1 *) pAfiSafiInstance) != MEM_SUCCESS)
    {
        return BGP4_FAILURE;
    }

    BGP4_MPE_ALLOC_AFISAFI_INSTANCE_CNT (BGP4_PEER_CXT_ID (pPeerentry))--;
    return BGP4_SUCCESS;
}

/* LOW-LEVEL added */

/*****************************************************************************/
/* Function Name : Bgp4MpeValidateAddressType                                */
/* Description   : Validates the AFI                                         */
/* Input(s)      : InetAddressType (AFI)                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE/BGP4_FALSE                                      */
/*****************************************************************************/
UINT1
Bgp4MpeValidateAddressType (UINT4 u4InetAddrType)
{
    UINT1               u1Status = BGP4_FALSE;

    if (u4InetAddrType == BGP4_INET_AFI_IPV4)
    {
        u1Status = BGP4_TRUE;
    }
#ifdef BGP4_IPV6_WANTED
    if (u4InetAddrType == BGP4_INET_AFI_IPV6)
    {
        u1Status = BGP4_TRUE;
    }
#endif

    return u1Status;
}

/*****************************************************************************/
/* Function Name : Bgp4MpeValidateSubAddressType                             */
/* Description   : Validates the SAFI                                        */
/* Input(s)      : InetSubAddressType (SAFI)                                 */
/* Output(s)     : None.                                                     */
/* Return(s)     : BGP4_TRUE/BGP4_FALSE                                      */
/*****************************************************************************/
UINT1
Bgp4MpeValidateSubAddressType (UINT4 u4InetSubAddrType)
{
    switch (u4InetSubAddrType)
    {
        case BGP4_INET_SAFI_UNICAST:
#ifdef L3VPN
        case BGP4_INET_SAFI_LABEL:
        case BGP4_INET_SAFI_VPNV4_UNICAST:
#endif
            return BGP4_TRUE;
        default:
            return BGP4_FALSE;
    }
}

/*****************************************************************************/
/* Function Name : Bgp4MpeGetRoute                                           */
/* Description   : Gets the Route profile entry from the RIB Tree or from the*/
/*                 Input list with the matching informations as given.       */
/* Input(s)      : Prefix                                                    */
/*                 PrefixType                                                */
/*                 Prefix Length (u1PrefixLen),                              */
/*                 PeerType                                                  */
/*                 IP Address of the Peer (PeerAddr)                         */
/* Output(s)     : None.                                                     */
/* Return(s)     : Route Profile if a matching entry is found,               */
/*                 NULL if not.                                              */
/*****************************************************************************/
tRouteProfile      *
Bgp4MpeGetRoute (UINT1 *pu1Prefix, UINT4 u4PrefixAsafiMask, UINT1 u1PrefixLen,
                 UINT4 u4PeerAddrType, UINT1 *pu1PeerAddr)
{
    UNUSED_PARAM (pu1Prefix);
    UNUSED_PARAM (u4PrefixAsafiMask);
    UNUSED_PARAM (u1PrefixLen);
    UNUSED_PARAM (u4PeerAddrType);
    UNUSED_PARAM (pu1PeerAddr);
    return ((tRouteProfile *) NULL);
}

/*****************************************************************************/
/* Function Name : Bgp4MpeGetRoute                                           */
/* Description   : Gets the Route profile entry from the RIB Tree or from the*/
/*                 Input list with the matching informations as given.       */
/* Input(s)      : Prefix                                                    */
/*                 PrefixType                                                */
/*                 Prefix Length (u1PrefixLen),                              */
/*                 PeerType                                                  */
/*                 IP Address of the Peer (PeerAddr)                         */
/* Output(s)     : None.                                                     */
/* Return(s)     : Route Profile if a matching entry is found,               */
/*                 NULL if not.                                              */
/*****************************************************************************/
INT4
BgpValidateIpAddr (const CHR1 * pc1Addr, tUtlInAddr * pInAddr)
{
    tAddrPrefix         IpAddr;
    if (UtlInetAton (pc1Addr, pInAddr) == 0)
    {
        return FALSE;
    }
    else
    {
        MEMCPY ((IpAddr.au1Address), pInAddr, sizeof (UINT4));
        IpAddr.u2AddressLen = sizeof (UINT4);
        IpAddr.u2Afi = 1;

        /* Avoid 0.x.y.z network */
        if (IpAddr.au1Address[0] == 0)
        {
            return FALSE;
        }
        /* Avoid class d and class e addresses */
        else if (IpAddr.au1Address[0] >= 224)
        {
            return FALSE;
        }
        /* Avoid network broadcast address */
        else if (IpAddr.au1Address[3] == 255)
        {
            return FALSE;
        }
        return TRUE;
    }
}

/*****************************************************************************/
/* Function Name : BgpValidateIpv4Addr                                       */
/* Description   : This function verifies whethter given ipv4 address is     */
/*                 valid                                                     */
/* Input(s)      : pc1Addr                                                   */
/*                 pInAddr                                                   */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRUE if address is valid else returns FALSE               */
/*****************************************************************************/
INT4
BgpValidateIpv4Addr (const CHR1 * pc1Addr, tUtlInAddr * pInAddr)
{
    tAddrPrefix         IpAddr;
    if (UtlInetAton (pc1Addr, pInAddr) == 0)
    {
        return FALSE;
    }
    else
    {
        MEMCPY ((IpAddr.au1Address), pInAddr, sizeof (UINT4));
        IpAddr.u2AddressLen = sizeof (UINT4);
        IpAddr.u2Afi = 1;

        /* Avoid class d and class e addresses */
        if (IpAddr.au1Address[0] >= 224)
        {
            return FALSE;
        }
        /* Avoid network broadcast address */
        else if (IpAddr.au1Address[3] == 255)
        {
            return FALSE;
        }
        return TRUE;
    }
}

#endif /* Endof BGMPUTL_C */
