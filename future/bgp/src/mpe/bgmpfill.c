/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: bgmpfill.c,v 1.21 2017/09/15 06:19:54 siva Exp $
 * *
 * * Description: This file contains functions related to 
 *                  Mpe Fill pathattributes.
 * *********************************************************************/
#ifndef BGMPFILL_C
#define BGMPFILL_C

#include "bgp4com.h"

/********************************************************************/
/* Function Name   : Bgp4MpeFillPathAttribute                       */
/* Description     : Fills either MP_UNREACH_NLRI path attribute    */
/*                   value or MP_REACH_NLRI path attribute value in */
/*                   the BGP4-INFO to be advertised.                */
/* Input(s)        : pMpeRtProfile - pointer to the route profile   */
/*                   pPeerInfo - pointer to the peer information    */
/*                               to which the UPDATE is to be sent  */
/* Output(s)       : pAdvtBgpInfo - Pointer to the updated BGP4-INFO*/
/*                                  to be advertised.               */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4MpeFillPathAttribute (tRouteProfile * pMpeRtProfile,
                          tBgp4Info * pAdvtBgpInfo, tBgp4PeerEntry * pPeerInfo)
{
    INT4                i4RetVal;

    BGP4_DBG2 (BGP4_DBG_ALL,
               "\tBgp4MpeFillPathAttribute() : Route to be filled is of "
               "<%d, %d> family\n", BGP4_RT_AFI_INFO (pMpeRtProfile),
               BGP4_RT_SAFI_INFO (pMpeRtProfile));
    /* 
     * If the route profile is received through Withdrawns Routes Field
     * or MP_UNREACH_NLRI path attribute, then no need to fill further
     */

    if (((BGP4_RT_GET_FLAGS (pMpeRtProfile) & BGP4_RT_WITHDRAWN) != 0) ||
        ((BGP4_RT_GET_FLAGS (pMpeRtProfile) & BGP4_MPE_RT_ADVT_MP_UNREACH) !=
         0))
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s : The received route %s is a unreachable route.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pMpeRtProfile),
                                        BGP4_RT_AFI_INFO (pMpeRtProfile)));
        return BGP4_SUCCESS;
    }

    BGP4_DBG (BGP4_DBG_ALL,
              "\tBgp4MpeFillPathAttribute() : Reachable route details "
              "follows ...\n");
    /* Fill the NEXT_HOP value */
    i4RetVal = Bgp4MpeFillNexthop (pMpeRtProfile, pAdvtBgpInfo, pPeerInfo);
    if (i4RetVal == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }

#ifndef VPLSADS_WANTED
    /* Fill the SNPA value */
    /* As per RFC 4760 there is no SNPA field */
    i4RetVal = Bgp4MpeFillSnpaInfo (pMpeRtProfile, pAdvtBgpInfo, pPeerInfo);
#endif
    return i4RetVal;
}

/********************************************************************/
/* Function Name   : Bgp4MpeFillNexthop                             */
/* Description     : Fills the NEXT_HOP information for reachable   */
/*                   destinations in MP_REACH_NLRI path attribute   */
/*                   to be advertised.                              */
/* Input(s)        : pMpeRtProfile - pointer to the route profile   */
/*                   pPeerInfo - pointer to the peer information    */
/*                               to which the UPDATE is to be sent  */
/* Output(s)       : pAdvtBgpInfo - Pointer to the updated BGP4-INFO*/
/*                                  to be advertised.               */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4MpeFillNexthop (tRouteProfile * pMpeRtProfile,
                    tBgp4Info * pAdvtBgpInfo, tBgp4PeerEntry * pPeerInfo)
{
    UINT4               u4AsafiMask = 0;
    INT4                i4RetVal = BGP4_FAILURE;

    u4AsafiMask = ((UINT4) BGP4_RT_AFI_INFO (pMpeRtProfile) <<
                   BGP4_TWO_BYTE_BITS) |
        BGP4_SAFI_IN_NET_ADDRESS_INFO (BGP4_RT_NET_ADDRESS_INFO
                                       (pMpeRtProfile));
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
        case CAP_MP_VPN4_UNICAST:
            /* Incase of VPNv4 nexthop, 
             * IPv4 nexthop will be filled in advertisement bgpinfo.
             * At the time of filling nexthop value in MP_REACH_NLRI
             * attribute, RD of zero will be prepended to this nexthop
             */
#endif
            i4RetVal = Bgp4AttrFillNexthop (pMpeRtProfile, pAdvtBgpInfo,
                                            pPeerInfo);
            break;
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
            i4RetVal = Bgp4MpeFillIpv6Nexthop (pMpeRtProfile, pAdvtBgpInfo,
                                               pPeerInfo);
            break;
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:

#ifdef BGP4_IPV6_WANTED
            if (BGP4_PEER_REMOTE_ADDR_TYPE (pPeerInfo) == BGP4_INET_AFI_IPV6)
            {
                BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_INFO_NEXTHOP_INFO
                                              (BGP4_RT_BGP_INFO
                                               (pMpeRtProfile))) =
                    BGP4_INET_AFI_IPV6;
                BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO (BGP4_INFO_NEXTHOP_INFO
                                                   (BGP4_RT_BGP_INFO
                                                    (pMpeRtProfile))) =
                    BGP4_IPV6_PREFIX_LEN;
                i4RetVal =
                    Bgp4MpeFillIpv6Nexthop (pMpeRtProfile, pAdvtBgpInfo,
                                            pPeerInfo);
            }
            else
#endif
            {
                BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_INFO_NEXTHOP_INFO
                                              (BGP4_RT_BGP_INFO
                                               (pMpeRtProfile))) =
                    BGP4_INET_AFI_IPV4;
                BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO (BGP4_INFO_NEXTHOP_INFO
                                                   (BGP4_RT_BGP_INFO
                                                    (pMpeRtProfile))) =
                    BGP4_IPV4_PREFIX_LEN;
                i4RetVal =
                    Bgp4AttrFillNexthop (pMpeRtProfile, pAdvtBgpInfo,
                                         pPeerInfo);
            }

            break;
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:

#ifdef BGP4_IPV6_WANTED
            if (BGP4_PEER_REMOTE_ADDR_TYPE (pPeerInfo) == BGP4_INET_AFI_IPV6)
            {
                BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_INFO_NEXTHOP_INFO
                                              (BGP4_RT_BGP_INFO
                                               (pMpeRtProfile))) =
                    BGP4_INET_AFI_IPV6;
                BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO (BGP4_INFO_NEXTHOP_INFO
                                                   (BGP4_RT_BGP_INFO
                                                    (pMpeRtProfile))) =
                    BGP4_IPV6_PREFIX_LEN;
                i4RetVal =
                    Bgp4MpeFillIpv6Nexthop (pMpeRtProfile, pAdvtBgpInfo,
                                            pPeerInfo);
            }
            else
#endif
            {
                BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_INFO_NEXTHOP_INFO
                                              (BGP4_RT_BGP_INFO
                                               (pMpeRtProfile))) =
                    BGP4_INET_AFI_IPV4;
                BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO (BGP4_INFO_NEXTHOP_INFO
                                                   (BGP4_RT_BGP_INFO
                                                    (pMpeRtProfile))) =
                    BGP4_IPV4_PREFIX_LEN;
                i4RetVal =
                    Bgp4AttrFillNexthop (pMpeRtProfile, pAdvtBgpInfo,
                                         pPeerInfo);
            }

            break;
#endif

        default:
            break;
    }

    return i4RetVal;
}

#ifdef BGP4_IPV6_WANTED
/********************************************************************/
/* Function Name   : Bgp4MpeFillIpv6Nexthop                         */
/* Description     : Fills the IPV6 NEXT_HOP information for        */
/*                   reachable destinations in MP_REACH_NLRI attr   */
/*                   to be advertised.                              */
/* Input(s)        : pMpeRtProfile - pointer to the route profile   */
/*                   pPeerInfo - pointer to the peer information    */
/*                               to which the UPDATE is to be sent  */
/* Output(s)       : pAdvtBgpInfo - Pointer to the Updated BGP4-INFO*/
/*                                  to be advertised.               */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS/BGP4_FAILURE            */
/********************************************************************/
INT4
Bgp4MpeFillIpv6Nexthop (tRouteProfile * pMpeRtProfile,
                        tBgp4Info * pAdvtBgpInfo, tBgp4PeerEntry * pPeerInfo)
{
    tAddrPrefix         RemAddr;
    tAddrPrefix         LocalV4CompatAddr;
    tAddrPrefix         GatewayAddress;
    tAddrPrefix         InvAddr;
    tNetAddress         LocalAddr;
    tBgp4PeerEntry     *pSrcPeer = NULL;
    tBgp4Info          *pBgp4Info = NULL;
    UINT4               u4LocalAddr;
    INT4                i4Ret;
    INT4                i4IsNetAddrPresent = BGP4_TRUE;
    INT4                i4IsLocalAddrPresent = BGP4_TRUE;
    UINT4               u4GatewayFlag = BGP4_FALSE;

    Bgp4InitAddrPrefixStruct (&InvAddr, BGP4_INET_AFI_IPV6);
    pBgp4Info = BGP4_RT_BGP_INFO (pMpeRtProfile);

    pSrcPeer = ((BGP4_RT_PROTOCOL (pMpeRtProfile) == BGP_ID) ?
                BGP4_RT_PEER_ENTRY (pMpeRtProfile) : NULL);

    /* This function is only for <IPV6> address family */
    if ((BGP4_AFI_IN_ADDR_PREFIX_INFO
         (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
          (BGP4_RT_NET_ADDRESS_INFO (pMpeRtProfile))) != BGP4_INET_AFI_IPV6)
#ifdef VPLSADS_WANTED
        && (BGP4_AFI_IN_ADDR_PREFIX_INFO
            (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
             (BGP4_RT_NET_ADDRESS_INFO (pMpeRtProfile))) != BGP4_INET_AFI_L2VPN)
#endif
        )
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s : The route %s does not support IPV6 Address family.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pMpeRtProfile),
                                        BGP4_RT_AFI_INFO (pMpeRtProfile)));
        return BGP4_SUCCESS;
    }

    Bgp4InitAddrPrefixStruct (&(BGP4_INFO_NEXTHOP_INFO (pAdvtBgpInfo)),
                              BGP4_INET_AFI_IPV6);
    Bgp4InitAddrPrefixStruct (&(BGP4_INFO_LINK_LOCAL_ADDR_INFO (pAdvtBgpInfo)),
                              BGP4_INET_AFI_IPV6);
    BGP4_INFO_LINK_LOCAL_PRESENT (pAdvtBgpInfo) = BGP4_FALSE;

    i4Ret = Bgp4GetPeerNetworkAddress (pPeerInfo, BGP4_INET_AFI_IPV6, &RemAddr);
    if (i4Ret == BGP4_FAILURE)
    {
        /* Network address (v6) is not configured for peer. */
        Bgp4CopyAddrPrefixStruct (&RemAddr,
                                  BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo));
        i4IsNetAddrPresent = BGP4_FALSE;
    }

    i4Ret = Bgp4GetLocalAddrForPeer (pPeerInfo, RemAddr, &LocalAddr, BGP4_TRUE);
    if (i4Ret == BGP4_FAILURE)
    {
        i4IsLocalAddrPresent = BGP4_FALSE;
    }

    i4Ret = Bgp4GetPeerGatewayAddress (pPeerInfo, BGP4_INET_AFI_IPV6,
                                       &GatewayAddress);
    if (i4Ret == BGP4_SUCCESS)
    {
        /* check if gateway is configured for the peer */
        u4GatewayFlag = BGP4_TRUE;
    }

    /* Protocol is Self-Aggregated route */
    if ((BGP4_RT_GET_FLAGS (pMpeRtProfile) & BGP4_RT_AGGR_ROUTE) ==
        BGP4_RT_AGGR_ROUTE)
    {
        BGP4_DBG (BGP4_DBG_CTRL_FLOW,
                  "\tBgp4AttrFillNexthop() : Self-Aggr Rt\n");
        if (i4IsLocalAddrPresent == BGP4_FALSE)
        {
            /* Unable to determine the local interface address. */
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG,
                           BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Failure in finding the corresponding local interface address "
                           "for the aggregate route %s.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pMpeRtProfile),
                                            BGP4_RT_AFI_INFO (pMpeRtProfile)));
            return BGP4_FAILURE;
        }
        if (i4IsNetAddrPresent == BGP4_FALSE)
        {

            /* Since the actual v6 address of the interface is not known but the
             *              * v4 address of the interface is known, send the v4 address as the
             *                           * next-hop in IPv6-v4 Compatible format. */
            PTR_FETCH4 (u4LocalAddr, LocalAddr.NetAddr.au1Address);
            Bgp4InitAddrPrefixStruct (&LocalV4CompatAddr, BGP4_INET_AFI_IPV6);
            PTR_ASSIGN_4 (&(LocalV4CompatAddr.au1Address[BGP4_IPV6_PREFIX_LEN -
                                                         BGP4_IPV4_PREFIX_LEN]),
                          u4LocalAddr);
            PTR_ASSIGN_2 (&
                          (LocalV4CompatAddr.
                           au1Address[BGP4_IPV6_PREFIX_LEN -
                                      BGP4_IPV4_PREFIX_LEN -
                                      BGP4_IP6_IP4_ADDR_LEN]),
                          BGP4_IP6_IP4_ADDR);
            MEMCPY (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                    BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalV4CompatAddr),
                    BGP4_IPV6_PREFIX_LEN);
        }
        else
        {
            MEMCPY (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                    BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalAddr.NetAddr),
                    BGP4_IPV6_PREFIX_LEN);
        }
        if (Bgp4IsOnSameSubnet (LocalAddr.NetAddr, pPeerInfo) == TRUE)
        {
            BGP4_IN6_IS_ADDR_LINKLOCAL (pPeerInfo, i4Ret);
            if (i4Ret == BGP4_TRUE)
            {
                /* Set the Link_Local flag and fill the peer link local address */
                MEMCPY (BGP4_INFO_LINK_LOCAL_ADDR (pAdvtBgpInfo),
                        BGP4_PEER_LINK_LOCAL_ADDR (pPeerInfo),
                        BGP4_IPV6_PREFIX_LEN);
                BGP4_INFO_LINK_LOCAL_PRESENT (pAdvtBgpInfo) = BGP4_TRUE;
            }
        }
    }
    /* Always preserve nexthop for confed peers or when reflecting
     * routes to clients/non-clients.  */
    else if ((BGP4_CONFED_PEER_STATUS (pPeerInfo) == BGP4_TRUE) ||
             ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerInfo), pPeerInfo) ==
               BGP4_INTERNAL_PEER) && (pSrcPeer != NULL)
              && (BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerInfo), pSrcPeer) ==
                  BGP4_INTERNAL_PEER)))
    {
        MEMCPY (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                BGP4_INFO_NEXTHOP (pBgp4Info), BGP4_IPV6_PREFIX_LEN);
    }
    else if (BGP4_PEER_SELF_NEXTHOP (pPeerInfo) == BGP4_ENABLE_NEXTHOP_SELF)
    {
        if (i4IsLocalAddrPresent == BGP4_FALSE)
        {
            /* Unable to determine the local interface address. */
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG,
                           BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Failure in finding the corresponding local interface address "
                           "for the route %s when nexthop self is enabled.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pMpeRtProfile),
                                            BGP4_RT_AFI_INFO (pMpeRtProfile)));
            return BGP4_FAILURE;
        }
        MEMCPY (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalAddr.NetAddr),
                BGP4_IPV6_PREFIX_LEN);

        if (Bgp4IsOnSameSubnet (LocalAddr.NetAddr, pPeerInfo) == TRUE)
        {
            /* Set the Link_Local flag and fill the peer link local address */
            MEMCPY (BGP4_INFO_LINK_LOCAL_ADDR (pAdvtBgpInfo),
                    BGP4_PEER_LINK_LOCAL_ADDR (pPeerInfo),
                    BGP4_IPV6_PREFIX_LEN);
            BGP4_INFO_LINK_LOCAL_PRESENT (pAdvtBgpInfo) = BGP4_TRUE;
        }
    }
    else
        /* if peer is external peer */
    if ((BGP4_GET_PEER_TYPE (BGP4_PEER_CXT_ID (pPeerInfo), pPeerInfo) ==
             BGP4_EXTERNAL_PEER)
            && (BGP4_CONFED_PEER_STATUS (pPeerInfo) == BGP4_FALSE))
    {
        /* if gateway is configured and is in the same subnet,advertise it */
        if ((u4GatewayFlag == BGP4_TRUE) &&
            (!((BGP4_PEER_EBGP_MULTIHOP (pPeerInfo) ==
                BGP4_EBGP_MULTI_HOP_DISABLE) &&
               (Bgp4IsOnSameSubnet (GatewayAddress, pPeerInfo) == FALSE))))
        {
            MEMCPY (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                    BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (GatewayAddress),
                    BGP4_IPV6_PREFIX_LEN);
        }
        else /* if peer is multi-hop enabled */
        if (BGP4_PEER_EBGP_MULTIHOP (pPeerInfo) == BGP4_EBGP_MULTI_HOP_ENABLE)
        {
            if (i4IsLocalAddrPresent == BGP4_FALSE)
            {
                /* Unable to determine the local interface address. */
                BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                               BGP4_TRC_FLAG,
                               BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : Failure in finding the corresponding local interface address "
                               "for the route %s when EBGP multihop is enabled.\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerInfo))),
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                (pMpeRtProfile),
                                                BGP4_RT_AFI_INFO
                                                (pMpeRtProfile)));
                return BGP4_FAILURE;
            }
            MEMCPY (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                    BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalAddr.NetAddr),
                    BGP4_IPV6_PREFIX_LEN);
        }
        else if (i4IsNetAddrPresent == BGP4_FALSE)
        {
            if (i4IsLocalAddrPresent == BGP4_FALSE)
            {
                /* Unable to determine the local interface address. */
                BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                               BGP4_TRC_FLAG,
                               BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : Failure in finding the corresponding local interface address "
                               "for the route %s.\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerInfo))),
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                (pMpeRtProfile),
                                                BGP4_RT_AFI_INFO
                                                (pMpeRtProfile)));
                return BGP4_FAILURE;
            }
            /* Since the actual v6 address of the interface is not known but the
             * v4 address of the interface is known, send the v4 address as the
             * next-hop in IPv6-v4 Compatible format. */
            PTR_FETCH4 (u4LocalAddr, LocalAddr.NetAddr.au1Address);
            Bgp4InitAddrPrefixStruct (&LocalV4CompatAddr, BGP4_INET_AFI_IPV6);
            PTR_ASSIGN_4 (&(LocalV4CompatAddr.au1Address[BGP4_IPV6_PREFIX_LEN -
                                                         BGP4_IPV4_PREFIX_LEN]),
                          u4LocalAddr);
            PTR_ASSIGN_2 (&(LocalV4CompatAddr.au1Address[BGP4_IPV6_PREFIX_LEN -
                                                         BGP4_IPV4_PREFIX_LEN -
                                                         BGP4_IP6_IP4_ADDR_LEN]),
                          BGP4_IP6_IP4_ADDR);

            MEMCPY (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                    BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalV4CompatAddr),
                    BGP4_IPV6_PREFIX_LEN);
            BGP4_INFO_LINK_LOCAL_PRESENT (pAdvtBgpInfo) = BGP4_FALSE;
        }
        else /* if received route is in the same subnet, advertise it */
        if ((Bgp4IsOnSameSubnet (LocalAddr.NetAddr, pPeerInfo) == TRUE) &&
            ((PrefixMatch
              (BGP4_INFO_NEXTHOP_INFO (pBgp4Info),
               BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo))) == BGP4_TRUE))
        {
            MEMCPY (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                    BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalAddr.
                                                           NetAddr),
                    BGP4_IPV6_PREFIX_LEN);
            MEMCPY (BGP4_INFO_LINK_LOCAL_ADDR (pAdvtBgpInfo),
                    BGP4_PEER_LINK_LOCAL_ADDR (pPeerInfo),
                    BGP4_IPV6_PREFIX_LEN);
            BGP4_INFO_LINK_LOCAL_PRESENT (pAdvtBgpInfo) = BGP4_TRUE;

        }
        if (i4IsLocalAddrPresent == BGP4_FALSE)
        {
            /* Unable to determine the local interface address. */
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG,
                           BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s : Failure in finding the corresponding local interface address "
                           "for the route %s.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pMpeRtProfile),
                                            BGP4_RT_AFI_INFO (pMpeRtProfile)));
            return BGP4_FAILURE;
        }

        if (i4IsNetAddrPresent != BGP4_FALSE)
        {
            MEMCPY (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                    BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalAddr.NetAddr),
                    BGP4_IPV6_PREFIX_LEN);
        }

        if ((Bgp4IsOnSameSubnet (LocalAddr.NetAddr, pPeerInfo) == TRUE)
            && (i4IsNetAddrPresent != BGP4_FALSE))
        {
            MEMCPY (BGP4_INFO_LINK_LOCAL_ADDR (pAdvtBgpInfo),
                    BGP4_PEER_LINK_LOCAL_ADDR (pPeerInfo),
                    BGP4_IPV6_PREFIX_LEN);
            BGP4_INFO_LINK_LOCAL_PRESENT (pAdvtBgpInfo) = BGP4_TRUE;
        }
    }
    else                        /* if peer is internal */
    {
        if (u4GatewayFlag == BGP4_TRUE)    /* advertise configured gateway */
        {
            MEMCPY (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                    BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (GatewayAddress),
                    BGP4_IPV6_PREFIX_LEN);
        }
        else if (i4IsNetAddrPresent == BGP4_FALSE)
        {
            if (i4IsLocalAddrPresent == BGP4_FALSE)
            {
                /* Unable to determine the local interface address. */
                BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                               BGP4_TRC_FLAG,
                               BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s : Failure in finding the corresponding local interface address "
                               "for the route %s.\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerInfo))),
                               Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                (pMpeRtProfile),
                                                BGP4_RT_AFI_INFO
                                                (pMpeRtProfile)));
                return BGP4_FAILURE;
            }
            /* Since the actual v6 address of the interface is not known but the
             * v4 address of the interface is known, send the v4 address as the
             * next-hop in IPv6-v4 Compatible format. */

            if (PrefixMatch (BGP4_INFO_NEXTHOP_INFO (pBgp4Info),
                             InvAddr) == BGP4_TRUE)
            {
                PTR_FETCH4 (u4LocalAddr, LocalAddr.NetAddr.au1Address);
                Bgp4InitAddrPrefixStruct (&LocalV4CompatAddr,
                                          BGP4_INET_AFI_IPV6);
                PTR_ASSIGN_4 (&
                              (LocalV4CompatAddr.
                               au1Address[BGP4_IPV6_PREFIX_LEN -
                                          BGP4_IPV4_PREFIX_LEN]), u4LocalAddr);
                PTR_ASSIGN_2 (&
                              (LocalV4CompatAddr.
                               au1Address[BGP4_IPV6_PREFIX_LEN -
                                          BGP4_IPV4_PREFIX_LEN -
                                          BGP4_IP6_IP4_ADDR_LEN]),
                              BGP4_IP6_IP4_ADDR);

                MEMCPY (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                        BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                        (LocalV4CompatAddr), BGP4_IPV6_PREFIX_LEN);
            }
            else
            {
                MEMCPY (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                        BGP4_INFO_NEXTHOP (pBgp4Info), BGP4_IPV6_PREFIX_LEN);
                if (PrefixMatch (BGP4_INFO_NEXTHOP_INFO (pBgp4Info),
                                 BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)) ==
                    BGP4_TRUE)
                {
                    MEMCPY (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                            BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalAddr.
                                                                   NetAddr),
                            BGP4_IPV6_PREFIX_LEN);
                    if (Bgp4IsOnSameSubnet (LocalAddr.NetAddr, pPeerInfo) ==
                        BGP4_TRUE)
                    {
                        MEMCPY (BGP4_INFO_LINK_LOCAL_ADDR (pAdvtBgpInfo),
                                BGP4_PEER_LINK_LOCAL_ADDR (pPeerInfo),
                                BGP4_IPV6_PREFIX_LEN);
                        BGP4_INFO_LINK_LOCAL_PRESENT (pAdvtBgpInfo) = BGP4_TRUE;
                    }

                }
            }

        }
        else
        {
            /* if directly connected route OR route with nexthop as 0 */
            BGP4_IN6_IS_ADDR_LINKLOCAL (BGP4_INFO_NEXTHOP (pBgp4Info), i4Ret);
            if ((PrefixMatch (BGP4_INFO_NEXTHOP_INFO (pBgp4Info),
                              InvAddr) == BGP4_TRUE) || (i4Ret == BGP4_TRUE))
            {
                if (i4IsLocalAddrPresent == BGP4_FALSE)
                {
                    /* Unable to determine the local interface address. */
                    BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                                   BGP4_TRC_FLAG,
                                   BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : Failure in finding the corresponding local interface address "
                                   "for the route %s.\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerInfo),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerInfo))),
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pMpeRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pMpeRtProfile)));
                    return BGP4_FAILURE;
                }
                MEMCPY (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                        BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalAddr.
                                                               NetAddr),
                        BGP4_IPV6_PREFIX_LEN);

                if (Bgp4IsOnSameSubnet (LocalAddr.NetAddr, pPeerInfo) == TRUE)
                {
                    MEMCPY (BGP4_INFO_LINK_LOCAL_ADDR (pAdvtBgpInfo),
                            BGP4_PEER_LINK_LOCAL_ADDR (pPeerInfo),
                            BGP4_IPV6_PREFIX_LEN);
                    BGP4_INFO_LINK_LOCAL_PRESENT (pAdvtBgpInfo) = BGP4_TRUE;
                }
            }
            else
            {
                MEMCPY (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                        BGP4_INFO_NEXTHOP (pBgp4Info), BGP4_IPV6_PREFIX_LEN);
                if (i4IsLocalAddrPresent == BGP4_FALSE)
                {
                    /* Unable to determine the local interface address. */
                    BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                                   BGP4_TRC_FLAG,
                                   BGP4_ALL_FAILURE_TRC | BGP4_UPD_MSG_TRC,
                                   BGP4_MOD_NAME,
                                   "\tPEER %s : Failure in finding the corresponding local interface address "
                                   "for the route %s.\n",
                                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                    (pPeerInfo),
                                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                                     (pPeerInfo))),
                                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX
                                                    (pMpeRtProfile),
                                                    BGP4_RT_AFI_INFO
                                                    (pMpeRtProfile)));
                    return BGP4_FAILURE;
                }
                if (PrefixMatch (BGP4_INFO_NEXTHOP_INFO (pBgp4Info),
                                 BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)) ==
                    BGP4_TRUE)
                {
                    MEMCPY (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                            BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (LocalAddr.
                                                                   NetAddr),
                            BGP4_IPV6_PREFIX_LEN);

                    if ((Bgp4IsOnSameSubnet (LocalAddr.NetAddr, pPeerInfo) ==
                         TRUE)
                        &&
                        ((PrefixMatch
                          (BGP4_INFO_NEXTHOP_INFO (pBgp4Info),
                           BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo))) ==
                         BGP4_TRUE))

                    {
                        MEMCPY (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                                BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                                (LocalAddr.NetAddr), BGP4_IPV6_PREFIX_LEN);

                        MEMCPY (BGP4_INFO_LINK_LOCAL_ADDR (pAdvtBgpInfo),
                                BGP4_PEER_LINK_LOCAL_ADDR (pPeerInfo),
                                BGP4_IPV6_PREFIX_LEN);
                        BGP4_INFO_LINK_LOCAL_PRESENT (pAdvtBgpInfo) = BGP4_TRUE;
                    }

                }

            }
        }
    }

    BGP4_INFO_ATTR_FLAG (pAdvtBgpInfo) |= BGP4_ATTR_NEXT_HOP_MASK;
    BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                   BGP4_TRC_FLAG, BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                   "\tPEER %s : The Nexthop %s attribute is successfully filled.\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo))),
                   Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP (pAdvtBgpInfo),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_INFO_NEXTHOP_INFO (pAdvtBgpInfo))));
    return BGP4_SUCCESS;
}
#endif

/********************************************************************/
/* Function Name   : Bgp4MpeFillSnpaInfo                            */
/* Description     : Fills the SNPA information for                 */
/*                   reachable destinations in MP_REACH_NLRI attr.  */
/* Input(s)        : pMpeRtProfile - pointer to the route profile   */
/*                   pPeerInfo - pointer to the peer information    */
/*                               to which the UPDATE is to be sent  */
/* Output(s)       : pAdvtBgpInfo - Pointer to the Updated BGP4-INFO*/
/*                                  to be advertised.               */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4MpeFillSnpaInfo (tRouteProfile * pMpeRtProfile,
                     tBgp4Info * pAdvtBgpInfo, tBgp4PeerEntry * pPeerInfo)
{

    UNUSED_PARAM (pMpeRtProfile);
    UNUSED_PARAM (pPeerInfo);
    UNUSED_PARAM (pAdvtBgpInfo);
    /* Fill the total number of SNPAs */
    BGP4_DBG (BGP4_DBG_ALL, "\t\tSNPA is filled with length [0]\n");
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4MpeFillNlriPrefix                          */
/* Description     : Fills the NLRI tuple in either MP_REACH_NLRI   */
/*                   or MP_UNREACH_NLRI path attribute              */
/* Input(s)        : pUpdBuffToSend - pointer to the UPDATE message */
/*                               in which the NLRI tuple value      */
/*                               is to be filled.                   */
/*                   pMpeRtProfile - pointer to the route profile   */
/*                   pPeerInfo - pointer to the peer information    */
/*                               to which the UPDATE is to be sent  */
/* Output(s)       : pUpdBuffToSend - pointer to the UPDATE message */
/*                               in which the NLRI tuple value      */
/*                               is filled.                         */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
UINT2
Bgp4MpeFillNlriPrefix (UINT1 *pUpdBuffToSend, tRouteProfile * pMpeRtProfile)
{
    UINT4               u4AsafiMask = 0;
    UINT2               u2Bytes = 0;

    u4AsafiMask = ((UINT4) BGP4_RT_AFI_INFO (pMpeRtProfile) <<
                   BGP4_TWO_BYTE_BITS) |
        BGP4_SAFI_IN_NET_ADDRESS_INFO (BGP4_RT_NET_ADDRESS_INFO
                                       (pMpeRtProfile));
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
            u2Bytes += Bgp4MpeFillIpv4Prefix (pUpdBuffToSend, pMpeRtProfile);
            BGP4_DBG2 (BGP4_DBG_ALL,
                       "\t\t<ipv4, unicast> NLRI - [%s/%d]\n",
                       Bgp4PrintIpAddr (pUpdBuffToSend +
                                        BGP4_MPE_IPV4_PREFIX_LEN,
                                        BGP4_INET_AFI_IPV4), (*pUpdBuffToSend));
            break;
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
            u2Bytes += Bgp4MpeFillIpv6Prefix (pUpdBuffToSend, pMpeRtProfile);
            BGP4_DBG2 (BGP4_DBG_ALL,
                       "\t\t<ipv6, unicast> NLRI - [%s/%d]\n",
                       Bgp4PrintIpAddr (pUpdBuffToSend +
                                        BGP4_MPE_IPV6_PREFIX_LEN,
                                        BGP4_INET_AFI_IPV6), (*pUpdBuffToSend));
            break;
#endif
        default:
            break;
    }

    return u2Bytes;
}

/********************************************************************/
/* Function Name   : Bgp4MpeFillIpv4Prefix                          */
/* Description     : Fills the IPv4 NLRI tuple in either            */
/*                   MP_REACH_NLRI or MP_UNREACH_NLRI path attr.    */
/* Input(s)        : pUpdBuffToSend - pointer to the UPDATE message */
/*                               in which the IPv4 NLRI tuple value */
/*                               is to be filled.                   */
/*                   pMpeRtProfile - pointer to the route profile   */
/*                   pPeerInfo - pointer to the peer information    */
/*                               to which the UPDATE is to be sent  */
/* Output(s)       : pUpdBuffToSend - pointer to the UPDATE message */
/*                               in which the IPv4 NLRI tuple value */
/*                               is filled.                         */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
UINT2
Bgp4MpeFillIpv4Prefix (UINT1 *pUpdBuffToSend, tRouteProfile * pMpeRtProfile)
{
    UINT2               u2Bytes = 0;

    u2Bytes += Bgp4AttrFillPrefix (pUpdBuffToSend, pMpeRtProfile);

    return u2Bytes;
}

#ifdef BGP4_IPV6_WANTED
/********************************************************************/
/* Function Name   : Bgp4MpeFillIpv6Prefix                          */
/* Description     : Fills the IPv6 NLRI tuple in either            */
/*                   MP_REACH_NLRI or MP_UNREACH_NLRI path attr.    */
/* Input(s)        : pUpdBuffToSend - pointer to the UPDATE message */
/*                               in which the IPv6 NLRI tuple value */
/*                               is to be filled.                   */
/*                   pMpeRtProfile - pointer to the route profile   */
/*                   pPeerInfo - pointer to the peer information    */
/*                               to which the UPDATE is to be sent  */
/* Output(s)       : pUpdBuffToSend - pointer to the UPDATE message */
/*                               in which the IPv6 NLRI tuple value */
/*                               is filled.                         */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
UINT2
Bgp4MpeFillIpv6Prefix (UINT1 *pUpdBuffToSend, tRouteProfile * pMpeRtProfile)
{
    UINT2               u2Bytes = 0;

    u2Bytes += Bgp4AttrFillPrefix (pUpdBuffToSend, pMpeRtProfile);

    return u2Bytes;
}
#endif

#ifdef L3VPN
/********************************************************************/
/* Function Name   : Bgp4ClFillIpLabelInUpdMsg                      */
/* Description     : Fills the Label associated with  the route     */
/* Input(s)        : pUpdBuffToSend - pointer to the UPDATE message */
/*                               in which Label is to be filled     */
/*                   pRoute - route information                     */
/*                   pPeer - pointer to the peer information        */
/* Output(s)       : pUpdBuffToSend - pointer to the UPDATE message */
/*                               in which the Label(s) is filled    */
/*                   pu2Len - pointer to number of bytes used for   */
/*                            filling Label(s) information          */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4ClFillIpLabelInUpdMsg (UINT1 *pUpdBuffToSend,
                           tRouteProfile * pRtProfile,
                           tBgp4AdvtPathAttr * pAdvtBgpInfo,
                           tBgp4PeerEntry * pPeerEntry, UINT2 *pu2Len)
{
    tBgp4Info          *pSndBgp4Info = NULL;
    tBgp4Info          *pRcdBgp4Info = NULL;
    UINT4               u4AsafiMask;
    UINT4               u4Label;
    UINT1               u1FillLblCnt = 0;
    Bgp4GetPeerAdvtAfiSafi (pRtProfile, pPeerEntry, &u4AsafiMask);
    if (pAdvtBgpInfo == NULL)
    {
        /* Unreachable route is being filled in MP_UNREACH_NLRI path
         * attribute. hence fill 0x800000 in the label field
         */
        u4Label = BGP4_DEF_WDR_LABEL;
        BGP4_COPY_LABEL_TOBUF (pUpdBuffToSend, u4Label);
        *pu2Len = BGP4_VPN4_LABEL_SIZE;
        switch (u4AsafiMask)
        {
            case CAP_MP_LABELLED_IPV4:
                /* Delete the ILM entry for PE peer with the aggregate 
                 * label */
                Bgp4MplsLabelUpdateInFmTable (BGP4_RT_LABEL_INFO
                                              (pRtProfile),
                                              BGP4_PEER_IF_INDEX
                                              (pPeerEntry),
                                              BGP4_RT_CXT_ID (pRtProfile),
                                              BGP4_FM_LABEL_DELETE,
                                              BGP4_LABEL_OPER_POP);
                break;
            case CAP_MP_VPN4_UNICAST:
                /* Delete the ILM entry for PE peer with the aggregate 
                 * label */

                Bgp4UpdateRouteInMplsDb (pRtProfile,
                                         BGP4_RT_CXT_ID (pRtProfile),
                                         L3VPN_BGP4_ROUTE_DEL,
                                         L3VPN_BGP4_LABEL_POP);
                break;
            default:
                return (BGP4_FAILURE);
        }
        return BGP4_SUCCESS;
    }
    *pu2Len = 0;
    /* If the nexthop is same then propagate the received label only.
     * If the nexthop is changed, then send a new label associated with
     * the filed nexthop
     */
    pSndBgp4Info = BGP4_ADVT_PA_BGP4INFO (pAdvtBgpInfo);
    pRcdBgp4Info = BGP4_RT_BGP_INFO (pRtProfile);
    if ((pSndBgp4Info == NULL) || (pRcdBgp4Info == NULL))
    {
        return BGP4_FAILURE;
    }

    if ((BGP4_RT_PROTOCOL (pRtProfile) == BGP_ID) &&
        (BGP4_RT_LABEL_CNT (pRtProfile)) &&
        (PrefixMatch (BGP4_INFO_NEXTHOP_INFO (pSndBgp4Info),
                      BGP4_INFO_NEXTHOP_INFO (pRcdBgp4Info))) == BGP4_TRUE)
    {
        /* Nexthop is unchanged */
        u4Label = (BGP4_RT_LABEL_INFO (pRtProfile) << BGP4_FOUR_BITS);
        u4Label = u4Label | BGP4_LABEL_BOTTOM_STACK_BIT;
        BGP4_COPY_LABEL_TOBUF (pUpdBuffToSend, u4Label);
        pUpdBuffToSend += BGP4_VPN4_LABEL_SIZE;
        u1FillLblCnt++;
    }
    else
    {
        /* Nexthop is changed. Assumption, peer local address */
        switch (u4AsafiMask)
        {
            case CAP_MP_LABELLED_IPV4:
                /* Update the ILM entry for PE peer with the new aggregate 
                 * label */
                Bgp4MplsLabelUpdateInFmTable (BGP4_RT_LABEL_INFO
                                              (pRtProfile),
                                              BGP4_PEER_IF_INDEX
                                              (pPeerEntry),
                                              BGP4_RT_CXT_ID (pRtProfile),
                                              BGP4_FM_LABEL_ADD,
                                              BGP4_LABEL_OPER_POP);
                break;
            case CAP_MP_VPN4_UNICAST:
                /* Update the ILM entry for PE peer with the new aggregate 
                 * label */
                Bgp4UpdateRouteInMplsDb (pRtProfile,
                                         BGP4_RT_CXT_ID (pRtProfile),
                                         L3VPN_BGP4_ROUTE_ADD,
                                         L3VPN_BGP4_LABEL_POP);
                break;
            default:
                return (BGP4_FAILURE);
        }
        u4Label = (BGP4_RT_LABEL_INFO (pRtProfile) << BGP4_FOUR_BITS);
        u4Label = u4Label | BGP4_LABEL_BOTTOM_STACK_BIT;
        BGP4_COPY_LABEL_TOBUF (pUpdBuffToSend, u4Label);
        pUpdBuffToSend += BGP4_VPN4_LABEL_SIZE;
        u1FillLblCnt++;

    }
    *pu2Len = (UINT2) (u1FillLblCnt * BGP4_VPN4_LABEL_SIZE);
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4MpeFillIpLbldPrefix                        */
/* Description     : Fills the Labelled IP prefix in                */
/*                   MP_REACH_NLRI or MP_UNREACH_NLRI path attr.    */
/* Input(s)        : pUpdBuffToSend - pointer to the UPDATE message */
/*                               in which the IPv6 NLRI tuple value */
/*                               is to be filled.                   */
/*                   pMpeRtProfile - pointer to the route profile   */
/*                   pPeerInfo - pointer to the peer information    */
/*                               to which the UPDATE is to be sent  */
/* Output(s)       : pUpdBuffToSend - pointer to the UPDATE message */
/*                               in which the IPv6 NLRI tuple value */
/*                               is filled.                         */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : No.of Bytes filled or BGP4_FAILURE   */
/********************************************************************/
INT4
Bgp4MpeFillIpLbldPrefix (UINT1 *pUpdBuffToSend,
                         tRouteProfile * pRtProfile,
                         tBgp4AdvtPathAttr * pAdvtBgpInfo,
                         tBgp4PeerEntry * pPeerEntry)
{
    UINT4               u4AsafiMask;
    INT4                i4Status;
    UINT2               u2Bytes;
    UINT2               u2BytesFilled = 0;
    UINT1               u1PrefixBytes = 0;

    /* Fill the total length later. move the update message ptr now */
    pUpdBuffToSend++;
    u2BytesFilled++;
    /* Fill the Label information */
    i4Status = Bgp4ClFillIpLabelInUpdMsg (pUpdBuffToSend, pRtProfile,
                                          pAdvtBgpInfo, pPeerEntry, &u2Bytes);
    if (i4Status == BGP4_FAILURE)
    {
        return (BGP4_FAILURE);
    }
    u2BytesFilled = (UINT2) (u2BytesFilled + u2Bytes);
    pUpdBuffToSend += u2Bytes;
    Bgp4GetPeerAdvtAfiSafi (pRtProfile, pPeerEntry, &u4AsafiMask);
    switch (u4AsafiMask)
    {
        case CAP_MP_LABELLED_IPV4:
            /* Start filling IPv4 route */
            u1PrefixBytes = (UINT1) Bgp4AttrFillVpnv4Prefix (pUpdBuffToSend,
                                                             pRtProfile);
            *(pUpdBuffToSend - u2BytesFilled) =
                (UINT1) (((u2BytesFilled - BGP4_MPE_IPV4_PREFIX_LEN) *
                          BGP4_ONE_BYTE_BITS) + BGP4_RT_PREFIXLEN (pRtProfile));
            u2BytesFilled = (UINT2) (u2BytesFilled + u1PrefixBytes);
            break;
        case CAP_MP_VPN4_UNICAST:
            /* Start filling Vpnv4 route information */
            /* Route-Disntinguisher */
            MEMCPY (pUpdBuffToSend, BGP4_RT_ROUTE_DISTING (pRtProfile),
                    BGP4_VPN4_ROUTE_DISTING_SIZE);
            pUpdBuffToSend += BGP4_VPN4_ROUTE_DISTING_SIZE;
            u2BytesFilled =
                (UINT2) (u2BytesFilled + BGP4_VPN4_ROUTE_DISTING_SIZE);

            u1PrefixBytes =
                (UINT1) Bgp4AttrFillVpnv4Prefix (pUpdBuffToSend, pRtProfile);
            /* Fill out the total length in the update message */
            *(pUpdBuffToSend - u2BytesFilled) =
                (UINT1) (((u2BytesFilled - BGP4_MPE_VPN4_PREFIX_LEN) *
                          BGP4_ONE_BYTE_BITS) + BGP4_RT_PREFIXLEN (pRtProfile));
            u2BytesFilled = (UINT2) (u2BytesFilled + u1PrefixBytes);
            break;
    }
    return u2BytesFilled;
}
#endif /* L3VPN */

#ifdef VPLSADS_WANTED
/*****************************************************************************/
/* Function Name : Bgp4AttrFillVplsPrefix                                    */
/* Description   : This routine fills the VPLS NLRI information in the buffer*/
/* Input(s)      : Route profile which is going to be advertised             */
/*                 (pRtProfile),                                             */
/*                 Buffer in which NLRI is going to be filled (pu1MsgBuf)    */
/* Output(s)     : None.                                                     */
/* Return(s)     : Number of bytes required to be filled .                   */
/*****************************************************************************/
UINT2
Bgp4AttrFillVplsPrefix (UINT1 *pu1MsgBuf,
                        tRouteProfile * pRtProfile, tBgp4PeerEntry * pPeerEntry)
{
    UINT4               u4AsafiMask;
    UINT1               u1PrefixLenBytes = 0;
    UINT2               u2BytesFilled = 0;

    Bgp4GetPeerAdvtAfiSafi (pRtProfile, pPeerEntry, &u4AsafiMask);
    switch (u4AsafiMask)
    {
        case CAP_MP_L2VPN_VPLS:
            u1PrefixLenBytes = (UINT1) BGP4_RT_PREFIXLEN (pRtProfile);
            /* *(UINT2 *)pu1MsgBuf = BGP4_RT_PREFIXLEN (pRtProfile); */
            PTR_ASSIGN2 (pu1MsgBuf, BGP4_RT_PREFIXLEN (pRtProfile));
            pu1MsgBuf++;
            pu1MsgBuf++;        /* 2 time increment advances it to 2 bytes Len field */
            u2BytesFilled += (UINT2) BGP4_VPLS_NLRI_PREFIX_FIELD_LEN;
            MEMCPY (pu1MsgBuf, BGP4_RT_IP_PREFIX (pRtProfile),
                    u1PrefixLenBytes);
            break;
        default:
            break;
    }
    pu1MsgBuf += u1PrefixLenBytes;
    u2BytesFilled += (UINT2) u1PrefixLenBytes;

    BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                   BGP4_TRC_FLAG, BGP4_VPLS_TRC, BGP4_MOD_NAME,
                   "\tPEER %s : L2VPN, VPLS Route Sending"
                   "NLRI - [%s]\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                     (pPeerEntry))),
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)));

    return u2BytesFilled;
}
#endif /* VPLSADS_WANTED */
#ifdef EVPN_WANTED
/********************************************************************/
/* Function Name   : Bgp4MpeFillEvpnPrefix                          */
/* Description     : Fills the Evpn prefix in                       */
/*                   MP_REACH_NLRI or MP_UNREACH_NLRI path attr.    */
/* Input(s)        : pUpdBuffToSend - pointer to the UPDATE message */
/*                               in which the NLRI tuple value      */
/*                               is to be filled.                   */
/*                   pMpeRtProfile - pointer to the route profile   */
/*                   pPeerInfo - pointer to the peer information    */
/*                               to which the UPDATE is to be sent  */
/* Output(s)       : pUpdBuffToSend - pointer to the UPDATE message */
/*                               in which the IPv6 NLRI tuple value */
/*                               is filled.                         */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : No.of Bytes filled or BGP4_FAILURE   */
/********************************************************************/
INT4
Bgp4MpeFillEvpnPrefix (UINT1 *pUpdBuffToSend,
                       tRouteProfile * pRtProfile, tBgp4PeerEntry * pPeerEntry)
{
    UINT4               u4AsafiMask = 0;
    UINT1               u1Byte = 1;
    UINT2               u2BytesFilled = 0;
    UINT1               u1RouteType = 0;
    UINT2               u2RouteLen = 0;
    UINT4               u4VniNumber = 0;

    Bgp4GetPeerAdvtAfiSafi (pRtProfile, pPeerEntry, &u4AsafiMask);
    switch (u4AsafiMask)
    {
        case CAP_MP_L2VPN_EVPN:
        {
            if (pRtProfile->EvpnNlriInfo.u1RouteType == EVPN_MAC_ROUTE)
            {
                u1RouteType = EVPN_MAC_ROUTE;
                /* Start filling Evpn route information */
                /* 1 Byte for route type and one byte for Length */
                MEMCPY (pUpdBuffToSend, &u1RouteType, 1);
                u2BytesFilled = (UINT2) (u2BytesFilled + u1Byte);
                pUpdBuffToSend += u1Byte;
                /* One byte for Length */
                u2BytesFilled = (UINT2) (u2BytesFilled + 1);
                pUpdBuffToSend += 1;
                /* Route-Disntinguisher */
                MEMCPY (pUpdBuffToSend, BGP4_EVPN_RD_DISTING (pRtProfile),
                        BGP4_EVPN_ROUTE_DISTING_SIZE);
                pUpdBuffToSend += BGP4_EVPN_ROUTE_DISTING_SIZE;
                u2BytesFilled =
                    (UINT2) (u2BytesFilled + BGP4_EVPN_ROUTE_DISTING_SIZE);
                /* Ethernet Segment Identifier */
                MEMCPY (pUpdBuffToSend, &BGP4_EVPN_ETHSEGID (pRtProfile),
                        BGP4_EVPN_ETH_SEG_ID_SIZE);
                pUpdBuffToSend += BGP4_EVPN_ETH_SEG_ID_SIZE;
                u2BytesFilled =
                    (UINT2) (u2BytesFilled + BGP4_EVPN_ETH_SEG_ID_SIZE);
                /* Ethernet Tag ID */
                MEMCPY (pUpdBuffToSend, &BGP4_EVPN_ETHTAG (pRtProfile),
                        BGP4_EVPN_ETH_TAG_SIZE);
                pUpdBuffToSend += BGP4_EVPN_ETH_TAG_SIZE;
                u2BytesFilled =
                    (UINT2) (u2BytesFilled + BGP4_EVPN_ETH_TAG_SIZE);
                /* MAC Address Length */
                MEMCPY (pUpdBuffToSend,
                        (UINT1 *) &BGP4_EVPN_MACADDR_LEN (pRtProfile),
                        BGP4_EVPN_ADDR_LEN_SIZE);
                pUpdBuffToSend += BGP4_EVPN_ADDR_LEN_SIZE;
                u2BytesFilled =
                    (UINT2) (u2BytesFilled + BGP4_EVPN_ADDR_LEN_SIZE);
                /* MAC Address */
                MEMCPY (pUpdBuffToSend, &BGP4_EVPN_MACADDR (pRtProfile),
                        sizeof (tMacAddr));
                pUpdBuffToSend += BGP4_EVPN_MAC_ADDR_SIZE;
                u2BytesFilled =
                    (UINT2) (u2BytesFilled + BGP4_EVPN_MAC_ADDR_SIZE);
                /* IP Address Length */
                MEMCPY ((UINT1 *) pUpdBuffToSend,
                        (UINT1 *) &BGP4_EVPN_IPADDR_LEN (pRtProfile),
                        BGP4_EVPN_ADDR_LEN_SIZE);
                pUpdBuffToSend += BGP4_EVPN_ADDR_LEN_SIZE;
                u2BytesFilled =
                    (UINT2) (u2BytesFilled + BGP4_EVPN_ADDR_LEN_SIZE);
                if (BGP4_EVPN_IPADDR_LEN (pRtProfile) ==
                    BGP4_EVPN_IP_ADDR_SIZE * BGP4_ONE_BYTE_BITS)
                {
                    MEMCPY (pUpdBuffToSend, &BGP4_EVPN_IPADDR (pRtProfile),
                            BGP4_EVPN_IP_ADDR_SIZE);
                    pUpdBuffToSend += BGP4_EVPN_IP_ADDR_SIZE;
                    u2BytesFilled =
                        (UINT2) (u2BytesFilled + BGP4_EVPN_IP_ADDR_SIZE);
                }
                else if (BGP4_EVPN_IPADDR_LEN (pRtProfile) ==
                         BGP4_EVPN_IP6_ADDR_SIZE * BGP4_ONE_BYTE_BITS)
                {
                    MEMCPY (pUpdBuffToSend, &BGP4_EVPN_IPADDR (pRtProfile),
                            BGP4_EVPN_IP6_ADDR_SIZE);
                    pUpdBuffToSend += BGP4_EVPN_IP6_ADDR_SIZE;
                    u2BytesFilled =
                        (UINT2) (u2BytesFilled + BGP4_EVPN_IP6_ADDR_SIZE);
                }
                /* VN Id */
                u4VniNumber = BGP4_EVPN_VNID (pRtProfile);
                u4VniNumber = OSIX_HTONL ((u4VniNumber << 8) & 0xffffff00);
                MEMCPY (pUpdBuffToSend, &u4VniNumber, BGP4_EVPN_VN_ID_SIZE);
                pUpdBuffToSend += BGP4_EVPN_VN_ID_SIZE;
                u2BytesFilled = (UINT2) (u2BytesFilled + BGP4_EVPN_VN_ID_SIZE);
                u2RouteLen = (UINT2) (u2BytesFilled - 2);    /*2 Bytes is constant */
                *(pUpdBuffToSend - (u2RouteLen + 1)) = (UINT1) u2RouteLen;
            }
            else if (pRtProfile->EvpnNlriInfo.u1RouteType ==
                     EVPN_ETH_SEGMENT_ROUTE)
            {
                u1RouteType = EVPN_ETH_SEGMENT_ROUTE;
                /* Start filling Evpn route information */
                /* 1 Byte for route type and one byte for Length */
                MEMCPY (pUpdBuffToSend, &u1RouteType, 1);
                u2BytesFilled = (UINT2) (u2BytesFilled + u1Byte);
                pUpdBuffToSend += u1Byte;
                /* One byte for Length */
                u2BytesFilled = (UINT2) (u2BytesFilled + 1);
                pUpdBuffToSend += 1;
                /* Route-Disntinguisher */
                MEMCPY (pUpdBuffToSend, BGP4_EVPN_RD_DISTING (pRtProfile),
                        BGP4_EVPN_ROUTE_DISTING_SIZE);
                pUpdBuffToSend += BGP4_EVPN_ROUTE_DISTING_SIZE;
                u2BytesFilled =
                    (UINT2) (u2BytesFilled + BGP4_EVPN_ROUTE_DISTING_SIZE);
                /* Ethernet Segment Identifier */
                MEMCPY (pUpdBuffToSend, &BGP4_EVPN_ETHSEGID (pRtProfile),
                        BGP4_EVPN_ETH_SEG_ID_SIZE);
                pUpdBuffToSend += BGP4_EVPN_ETH_SEG_ID_SIZE;
                u2BytesFilled =
                    (UINT2) (u2BytesFilled + BGP4_EVPN_ETH_SEG_ID_SIZE);
                /* IP Address Length */
                MEMCPY ((UINT1 *) pUpdBuffToSend,
                        (UINT1 *) &BGP4_EVPN_IPADDR_LEN (pRtProfile),
                        BGP4_EVPN_ADDR_LEN_SIZE);
                pUpdBuffToSend += BGP4_EVPN_ADDR_LEN_SIZE;
                u2BytesFilled =
                    (UINT2) (u2BytesFilled + BGP4_EVPN_ADDR_LEN_SIZE);
                if (BGP4_EVPN_IPADDR_LEN (pRtProfile) ==
                    BGP4_EVPN_IP_ADDR_SIZE * BGP4_ONE_BYTE_BITS)
                {
                    MEMCPY (pUpdBuffToSend, &BGP4_EVPN_IPADDR (pRtProfile),
                            BGP4_EVPN_IP_ADDR_SIZE);
                    pUpdBuffToSend += BGP4_EVPN_IP_ADDR_SIZE;
                    u2BytesFilled =
                        (UINT2) (u2BytesFilled + BGP4_EVPN_IP_ADDR_SIZE);
                }
                else if (BGP4_EVPN_IPADDR_LEN (pRtProfile) ==
                         BGP4_EVPN_IP6_ADDR_SIZE * BGP4_ONE_BYTE_BITS)
                {
                    MEMCPY (pUpdBuffToSend, &BGP4_EVPN_IPADDR (pRtProfile),
                            BGP4_EVPN_IP6_ADDR_SIZE);
                    pUpdBuffToSend += BGP4_EVPN_IP6_ADDR_SIZE;
                    u2BytesFilled =
                        (UINT2) (u2BytesFilled + BGP4_EVPN_IP6_ADDR_SIZE);
                }
                u2RouteLen = (UINT2) (u2BytesFilled - 2);    /*2 Bytes is constant */
                *(pUpdBuffToSend - (u2RouteLen + 1)) = (UINT1) u2RouteLen;
            }
            else if (pRtProfile->EvpnNlriInfo.u1RouteType == EVPN_AD_ROUTE)
            {
                u1RouteType = EVPN_AD_ROUTE;
                /* Start filling Evpn route information */
                /* 1 Byte for route type and one byte for Length */
                MEMCPY (pUpdBuffToSend, &u1RouteType, 1);
                u2BytesFilled = (UINT2) (u2BytesFilled + u1Byte);
                pUpdBuffToSend += u1Byte;
                /* One byte for Length */
                u2BytesFilled = (UINT2) (u2BytesFilled + 1);
                pUpdBuffToSend += 1;
                /* Route-Disntinguisher */
                MEMCPY (pUpdBuffToSend, BGP4_EVPN_RD_DISTING (pRtProfile),
                        BGP4_EVPN_ROUTE_DISTING_SIZE);
                pUpdBuffToSend += BGP4_EVPN_ROUTE_DISTING_SIZE;
                u2BytesFilled =
                    (UINT2) (u2BytesFilled + BGP4_EVPN_ROUTE_DISTING_SIZE);
                /* Ethernet Segment Identifier */
                MEMCPY (pUpdBuffToSend, &BGP4_EVPN_ETHSEGID (pRtProfile),
                        BGP4_EVPN_ETH_SEG_ID_SIZE);
                pUpdBuffToSend += BGP4_EVPN_ETH_SEG_ID_SIZE;
                u2BytesFilled =
                    (UINT2) (u2BytesFilled + BGP4_EVPN_ETH_SEG_ID_SIZE);
                /* Ethernet Tag ID */
                MEMCPY (pUpdBuffToSend, &BGP4_EVPN_ETHTAG (pRtProfile),
                        BGP4_EVPN_ETH_TAG_SIZE);
                pUpdBuffToSend += BGP4_EVPN_ETH_TAG_SIZE;
                u2BytesFilled =
                    (UINT2) (u2BytesFilled + BGP4_EVPN_ETH_TAG_SIZE);

                /* VN Id */
                u4VniNumber = BGP4_EVPN_VNID (pRtProfile);
                u4VniNumber = OSIX_HTONL ((u4VniNumber << 8) & 0xffffff00);
                MEMCPY (pUpdBuffToSend, &u4VniNumber, BGP4_EVPN_VN_ID_SIZE);
                pUpdBuffToSend += BGP4_EVPN_VN_ID_SIZE;
                u2BytesFilled = (UINT2) (u2BytesFilled + BGP4_EVPN_VN_ID_SIZE);
                u2RouteLen = (UINT2) (u2BytesFilled - 2);    /*2 Bytes is constant */
                *(pUpdBuffToSend - (u2RouteLen + 1)) = (UINT1) u2RouteLen;
            }
        }
            break;
        default:
            break;
    }
    BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerEntry)),
                   BGP4_TRC_FLAG, BGP4_EVPN_TRC, BGP4_MOD_NAME,
                   "\tPEER %s : L2VPN, EVPN Route Sending"
                   "NLRI - [%s]\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerEntry),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                     (pPeerEntry))),
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)));
    return u2BytesFilled;
}
#endif
#endif /* End of BGMPFILL_C */
