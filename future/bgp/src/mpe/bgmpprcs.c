/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgmpprcs.c,v 1.38 2017/09/15 06:19:54 siva Exp $
 *
 *******************************************************************/
#ifndef BGMPPRCS_C
#define BGMPPRCS_C

#include "bgp4com.h"

#ifdef  BGP_SCAL_TESTING
UINT4               NoOfRoutes = 0;
#endif

/********************************************************************/
/* Function Name   : Bgp4MpeProcessAttribute                        */
/* Description     : This module processes the received MPE path    */
/*                   attribute. The path attribute could be either  */
/*                   MP_REACH_NLRI or MP_UNREACH_NLRI. It does      */
/*                   validation of Next hop and other information   */
/*                   received in path attribute and extracts the    */
/*                   NLRIs information received in the path         */
/*                   attribute(s).If the <AFI, SAFI> received in the*/
/*                   path attribute is a non-negotiated <AFI, SAFI> */
/*                   then that path attribute value will be ignored.*/
/*                   On any failure, this module sends NOTIFICATION */
/*                   message with 'Update Message Error'/'Optional  */
/*                   Attribute Error'.                              */
/* Input(s)        : pPeerInfo - pointer to the peer information    */
/*                               from which the UPDATE is received  */
/*                   pu1RcvdUpdateMessage - received UPDATE message */
/* Output(s)       : pTsMpeRcvdRoutes - pointer to the linkded list */
/*                               of route profiles which contain    */
/*                               NLRIs information extracted from   */
/*                               the input UPDATE message           */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : On success returns Number of bytes   */
/*                             processed.                           */
/*                             On failure returns BGP4_FAILURE or   */
/*                                BGP4_SEMANTIC_INVALID_NEXTHOP     */
/*                             BGP4_RSRC_ALLOC_FAIL, if resource    */
/*                                allocation fails                  */
/********************************************************************/
INT4
Bgp4MpeProcessAttribute (tBgp4PeerEntry * pPeerInfo,
                         UINT1 *pu1RcvdUpdateMessage,
                         INT4 i4RemPattrLen, tTMO_SLL * pTsMpeRcvdRoutes,
                         UINT4 *pu4Flag)
{
    UINT1              *pu1RcvdBuf = NULL;
    tBgp4Info          *pBgp4Info = NULL;
    UINT4               u4PrefixesCnt = 0;
    UINT4               u4AsafiMask;
    UINT4               u4Index = 0;
    INT4                i4TotLenValidated = 0;
    INT4                i4NlriLen = 0;
    INT4                i4Status = BGP4_FAILURE;
    UINT2               u2MpeAttribLen;
    UINT2               u2RcvdAfi;
    UINT2               u2LenValidated = 0;
    UINT1               u1Flags;
    UINT1               u1MpeAttr;
    UINT1               u1RcvdSafi;
    static UINT1        u1Flag = BGP4_FALSE;
#ifdef BGP4_IPV6_WANTED
    UINT1               u1NextHopLength = 0;
#endif

    pu1RcvdBuf = pu1RcvdUpdateMessage;

    u1Flags = *(pu1RcvdBuf);
    u1MpeAttr = *(pu1RcvdBuf + BGP4_ATYPE_FLAGS_LEN);

    if ((u1Flags & BGP4_EXT_LEN_FLAG_MASK) != 0)
    {
        PTR_FETCH2 (u2MpeAttribLen, (pu1RcvdBuf + BGP4_ATYPE_FLAGS_LEN +
                                     BGP4_ATYPE_CODE_LEN));

        u2MpeAttribLen += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_EXTENDED_ATTR_LEN_SIZE;
        pu1RcvdBuf += BGP4_ATYPE_FLAGS_LEN +
            BGP4_ATYPE_CODE_LEN + BGP4_EXTENDED_ATTR_LEN_SIZE;
        i4TotLenValidated += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_EXTENDED_ATTR_LEN_SIZE;
    }
    else
    {
        u2MpeAttribLen = *(pu1RcvdBuf + BGP4_ATYPE_FLAGS_LEN +
                           BGP4_ATYPE_CODE_LEN);
        u2MpeAttribLen += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_NORMAL_ATTR_LEN_SIZE;
        pu1RcvdBuf += BGP4_ATYPE_FLAGS_LEN +
            BGP4_ATYPE_CODE_LEN + BGP4_NORMAL_ATTR_LEN_SIZE;
        i4TotLenValidated += BGP4_ATYPE_FLAGS_LEN + BGP4_ATYPE_CODE_LEN +
            BGP4_NORMAL_ATTR_LEN_SIZE;
    }

    if (u2MpeAttribLen > i4RemPattrLen)
    {
        BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Update MSG ERR - Invalid MultiprotoExt."
                       "Attribute Length - Attr Code: %d, Length: %d\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))), u1MpeAttr,
                       u2MpeAttribLen);
        Bgp4EhSendError (pPeerInfo, BGP4_UPDATE_MSG_ERR, BGP4_ATTR_LEN_ERR,
                         pu1RcvdUpdateMessage, u2MpeAttribLen);
        return (BGP4_FAILURE);
    }

    PTR_FETCH2 (u2RcvdAfi, pu1RcvdBuf);
    u1RcvdSafi = *(pu1RcvdBuf + BGP4_MPE_ADDRESS_FAMILY_LEN);

    u4AsafiMask = ((UINT4) u2RcvdAfi << BGP4_TWO_BYTE_BITS) | u1RcvdSafi;
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) &
                 CAP_NEG_IPV4_UNI_MASK) != CAP_NEG_IPV4_UNI_MASK)
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s - ipv4 unicast capability is not negotiated "
                               "with peer. IPv4 route in update message is ignored\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerInfo))));
                return u2MpeAttribLen;
            }
            u4Index = BGP4_IPV4_UNI_INDEX;

            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - ipv4 unicast route is received in update message\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            break;
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) &
                 CAP_NEG_IPV6_UNI_MASK) != CAP_NEG_IPV6_UNI_MASK)
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s - ipv6 unicast capability is not negotiated "
                               "with peer. IPv6 route in update message is ignored\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerInfo))));
                return u2MpeAttribLen;
            }
            u4Index = BGP4_IPV6_UNI_INDEX;
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - ipv6 unicast route is received in update message\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            break;
#endif
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) & CAP_NEG_LBL_IPV4_MASK)
                != CAP_NEG_LBL_IPV4_MASK)
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s - ipv4 label capability is not negotiated "
                               "with peer. IPv4 route in update message is ignored\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerInfo))));
                return u2MpeAttribLen;
            }
            u4Index = BGP4_IPV4_LBLD_INDEX;
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - ipv4 label route is received in update message\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            break;
        case CAP_MP_VPN4_UNICAST:
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) &
                 CAP_NEG_VPN4_UNI_MASK) != CAP_NEG_VPN4_UNI_MASK)
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s - vpnv4 unicast capability is not negotiated "
                               "with peer. Vpnv4 route in update message is ignored\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerInfo))));
                return u2MpeAttribLen;
            }
            u4Index = BGP4_VPN4_UNI_INDEX;
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - vpnv4 unicast route is received in update message\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            break;
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) & CAP_NEG_L2VPN_VPLS_MASK)
                != CAP_NEG_L2VPN_VPLS_MASK)
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC |
                               BGP4_VPLS_TRC, BGP4_MOD_NAME,
                               "\tPEER %s - L2VPN VPLS capability is not negotiated "
                               "with peer. VPLS route in update message is ignored\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerInfo))));
                return u2MpeAttribLen;
            }
            u4Index = BGP4_L2VPN_VPLS_INDEX;

            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC |
                           BGP4_VPLS_TRC, BGP4_MOD_NAME,
                           "\tPEER %s - L2VPN VPLS route is received in update message\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            break;
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
            if ((BGP4_PEER_NEG_ASAFI_MASK (pPeerInfo) & CAP_NEG_L2VPN_EVPN_MASK)
                != CAP_NEG_L2VPN_EVPN_MASK)
            {
                BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC |
                               BGP4_EVPN_TRC, BGP4_MOD_NAME,
                               "\tPEER %s - L2VPN EVPN capability is not negotiated "
                               "with peer. EVPN route in update message is ignored\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerInfo))));
                return u2MpeAttribLen;
            }
            u4Index = BGP4_L2VPN_EVPN_INDEX;

            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC |
                           BGP4_EVPN_TRC, BGP4_MOD_NAME,
                           "\tPEER %s - L2VPN EVPN route is received in update mmessage\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            break;

#endif
        default:
            BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - Route with unknown AFI %d SAFI %d is received"
                           "from peer. Route is ignored\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))), u2RcvdAfi,
                           u1RcvdSafi);
            return u2MpeAttribLen;
    }

    pBgp4Info = Bgp4MemAllocateBgp4info (sizeof (tBgp4Info));
    if (pBgp4Info == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tUnable to allocate memory " "for bgp4info entry.\n");
        gu4BgpDebugCnt[MAX_BGP_ROUTE_INFO_ENTRIES_SIZING_ID]++;
        return (BGP4_RSRC_ALLOC_FAIL);
    }

    pu1RcvdBuf += BGP4_MPE_ADDRESS_FAMILY_LEN + BGP4_MPE_SUB_ADDRESS_FAMILY_LEN;
    i4TotLenValidated += BGP4_MPE_ADDRESS_FAMILY_LEN +
        BGP4_MPE_SUB_ADDRESS_FAMILY_LEN;
#ifdef BGP4_IPV6_WANTED
    u1NextHopLength = *(pu1RcvdBuf);
#endif

#if (defined (VPLSADS_WANTED) || defined (EVPN_WANTED))
    if (u2RcvdAfi == BGP4_INET_AFI_L2VPN)
    {
#ifdef BGP4_IPV6_WANTED
        if ((u1NextHopLength == BGP4_IPV6_PREFIX_LEN)
            || (u1NextHopLength == BGP4_IPV6_PREFIX_LEN * 2))
        {
            Bgp4InitAddrPrefixStruct (&(BGP4_INFO_NEXTHOP_INFO (pBgp4Info)),
                                      BGP4_INET_AFI_IPV6);
            BGP4_MPE_BGP_SAFI_INFO (pBgp4Info) = BGP4_INET_SAFI_UNICAST;
        }
        else
#endif
        {
            /*Default NextHop is IPv4 */
            Bgp4InitAddrPrefixStruct (&(BGP4_INFO_NEXTHOP_INFO (pBgp4Info)),
                                      BGP4_INET_AFI_IPV4);
            BGP4_MPE_BGP_SAFI_INFO (pBgp4Info) = BGP4_INET_SAFI_UNICAST;
        }
    }
    else
#endif
    {
#ifdef BGP4_IPV6_WANTED
        if ((u1NextHopLength == BGP4_IPV6_PREFIX_LEN)
            || (u1NextHopLength == BGP4_IPV6_PREFIX_LEN * 2))
        {
            Bgp4InitAddrPrefixStruct (&(BGP4_INFO_NEXTHOP_INFO (pBgp4Info)),
                                      BGP4_INET_AFI_IPV6);
            BGP4_MPE_BGP_SAFI_INFO (pBgp4Info) = BGP4_INET_SAFI_UNICAST;
        }
        else
#endif
        {
            /*Default NextHop is IPv4 */
            Bgp4InitAddrPrefixStruct (&(BGP4_INFO_NEXTHOP_INFO (pBgp4Info)),
                                      u2RcvdAfi);
            BGP4_MPE_BGP_SAFI_INFO (pBgp4Info) = u1RcvdSafi;
        }
    }
    if (u1MpeAttr == BGP4_ATTR_MP_REACH_NLRI)
    {
        BGP4_DBG (BGP4_DBG_ALL,
                  "\tBgp4MpeProcessPathAttribute() : Reachable "
                  "route details follows ...\n");
        i4Status = Bgp4MpeProcessNexthop (pPeerInfo, pu1RcvdBuf,
                                          pBgp4Info, &u2LenValidated);
        if (i4Status == BGP4_SEMANTIC_INVALID_NEXTHOP)
        {
            Bgp4DshReleaseBgpInfo (pBgp4Info);
            return (BGP4_SEMANTIC_INVALID_NEXTHOP);
        }
        if (i4Status == BGP4_FAILURE)
        {
            BGP4_PEER_INVALID_MPE_UPDATE_CNT (pPeerInfo, u4Index)++;
            Bgp4EhSendError (pPeerInfo, BGP4_UPDATE_MSG_ERR,
                             BGP4_INVALID_NEXTHOP,
                             pu1RcvdUpdateMessage, u2MpeAttribLen);
            Bgp4DshReleaseBgpInfo (pBgp4Info);
            return BGP4_FAILURE;
        }

        pu1RcvdBuf += u2LenValidated;
        i4TotLenValidated += u2LenValidated;

#if (defined (VPLSADS_WANTED) || defined (EVPN_WANTED))
        u2LenValidated = 1;        /* Reserved - 1 octet */
#else
        /*In case of L2VPN-VPLS, no need to process SNPA */
        i4Status = Bgp4MpeProcessSnpa (pPeerInfo, pu1RcvdBuf,
                                       pBgp4Info, &u2LenValidated);
        if (i4Status == BGP4_FAILURE)
        {
            /* Currently processing of SNPA occurs only when memory allocation
             * is failed. There is no validation done for SNPA information.
             * hence do not increment the Invalid UPDATE count for SNPA
             * processing failure */

            /*BGP4_PEER_INVALID_MPE_UPDATE_CNT (pPeerInfo, u4Index)++; */

            Bgp4EhSendError (pPeerInfo, BGP4_UPDATE_MSG_ERR,
                             BGP4_OPTIONAL_ATTR_ERR,
                             pu1RcvdUpdateMessage, u2MpeAttribLen);
            Bgp4DshReleaseBgpInfo (pBgp4Info);
            return BGP4_FAILURE;
        }
#endif
        pu1RcvdBuf += u2LenValidated;
        i4TotLenValidated += u2LenValidated;
    }
    else
    {
        BGP4_DBG (BGP4_DBG_ALL,
                  "\tBgp4MpeProcessPathAttribute() : Unreachable "
                  "route details follows ...\n");
    }

    i4NlriLen = u2MpeAttribLen - i4TotLenValidated;
    if (i4NlriLen <= 0)
    {
        if ((i4NlriLen == 0) && (u1MpeAttr == BGP4_ATTR_MP_UNREACH_NLRI))
        {
            return BGP4_SUCCESS;
        }

        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Failed while validating optional attribute."
                       " The NLRI length is less than zero.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))));

        BGP4_PEER_INVALID_MPE_UPDATE_CNT (pPeerInfo, u4Index)++;
        Bgp4EhSendError (pPeerInfo, BGP4_UPDATE_MSG_ERR,
                         BGP4_OPTIONAL_ATTR_ERR,
                         pu1RcvdUpdateMessage, u2MpeAttribLen);
        Bgp4DshReleaseBgpInfo (pBgp4Info);
        return BGP4_FAILURE;
    }

    while (i4NlriLen > 0)
    {
        if ((u1Flag == BGP4_FALSE) || (u1MpeAttr == BGP4_ATTR_MP_UNREACH_NLRI))
        {
            i4Status = Bgp4MpeProcessNlri (pPeerInfo, pu1RcvdBuf,
                                           pBgp4Info, u1MpeAttr,
                                           pTsMpeRcvdRoutes, &u2LenValidated,
                                           u4AsafiMask);

            KW_FALSEPOSITIVE_FIX (pBgp4Info);
            if (i4Status == BGP4_FAILURE)
            {
                BGP4_PEER_INVALID_MPE_UPDATE_CNT (pPeerInfo, u4Index)++;
                Bgp4EhSendError (pPeerInfo, BGP4_UPDATE_MSG_ERR,
                                 BGP4_OPTIONAL_ATTR_ERR,
                                 pu1RcvdUpdateMessage, u2MpeAttribLen);
                if (u1MpeAttr == BGP4_ATTR_MP_UNREACH_NLRI)
                {
                    Bgp4DshReleaseBgpInfo (pBgp4Info);
                }
                else
                {
                    /* We should release this memory only when the processing of
                     * first NLRI itself becomes failure. Otherwise, if we release
                     * here and then caller releases the list will become DOUBLE
                     * release. 
                     */
                    if (u4PrefixesCnt == 0)
                    {
                        Bgp4DshReleaseBgpInfo (pBgp4Info);
                    }
                }
                return BGP4_FAILURE;
            }
            else if (i4Status == BGP4_RSRC_ALLOC_FAIL)
            {
                if (u1MpeAttr == BGP4_ATTR_MP_UNREACH_NLRI)
                {
                    Bgp4DshReleaseBgpInfo (pBgp4Info);
                }
                else
                {
                    /* We should release this memory only when the processing of
                     * first NLRI itself becomes failure. Otherwise, if we release
                     * here and then caller releases the list will become DOUBLE
                     * release. 
                     */
                    if (u4PrefixesCnt == 0)
                    {
                        Bgp4DshReleaseBgpInfo (pBgp4Info);
                    }
                    i4NlriLen = 0;
                    i4TotLenValidated = 0;
                    u1Flag = BGP4_TRUE;
                    break;
                }
                return BGP4_RSRC_ALLOC_FAIL;
            }
            else if (i4Status == BGP4_MAX_ROUTES_LIMIT)
            {
                i4NlriLen = 0;
                i4TotLenValidated = 0;
                u1Flag = BGP4_TRUE;
                break;
            }
            else if (i4Status == BGP4_MAX_MP_ROUTE_PREFIX_LIMIT)
            {
                BGP4_DBG (BGP4_DBG_ALL,
                          "\tBgp4MpeProcessPathAttribute() : BGP4_MAX_MP_ROUTE_PREFIX_LIMIT exceeded "
                          "route can't be stored ...\n");
            }
        }

        i4NlriLen -= u2LenValidated;
        pu1RcvdBuf += u2LenValidated;
        i4TotLenValidated += u2LenValidated;
        if (i4Status == BGP4_SUCCESS)
        {
            u4PrefixesCnt++;
        }
    }

    if (i4NlriLen != 0)
    {
        BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                       "\tPEER %s - Invalid MPE attribute is received in update\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))));
        BGP4_PEER_INVALID_MPE_UPDATE_CNT (pPeerInfo, u4Index)++;
        Bgp4EhSendError (pPeerInfo, BGP4_UPDATE_MSG_ERR,
                         BGP4_INVALID_NLRI,
                         pu1RcvdUpdateMessage, u2MpeAttribLen);
        if ((u1MpeAttr == BGP4_ATTR_MP_UNREACH_NLRI) || (u4PrefixesCnt == 0))
        {
            Bgp4DshReleaseBgpInfo (pBgp4Info);
        }
        return BGP4_FAILURE;
    }

    if (u1MpeAttr == BGP4_ATTR_MP_REACH_NLRI)
    {
        if (u4PrefixesCnt > 0)
        {
            BGP4_PEER_PREFIX_RCVD_CNT (pPeerInfo, u4Index) += u4PrefixesCnt;
            BGP4_INFO_ATTR_FLAG (pBgp4Info) |= BGP4_ATTR_MP_REACH_NLRI_MASK;
            *pu4Flag = BGP4_INFO_ATTR_FLAG (pBgp4Info);
        }
        else
        {
            /* No NLRI received in the Packet or Received NLRI are not filled.
             * In either case release the BGP4INFO */
            Bgp4DshReleaseBgpInfo (pBgp4Info);
        }
    }
    else
    {
        Bgp4DshReleaseBgpInfo (pBgp4Info);
        BGP4_PEER_WITHDRAWS_RCVD_CNT (pPeerInfo, u4Index) += u4PrefixesCnt;
        u1Flag = BGP4_FALSE;
    }
    return (i4TotLenValidated);
}

/********************************************************************/
/* Function Name   : Bgp4MpeProcessSnpa                             */
/* Description     : This module processes the received SNPA        */
/*                   information in MP_REACH_NLRI path attribute.   */
/*                   This module stores the received SNPA           */
/*                   information in path attribute information      */
/*                   structure.This module processes the received   */
/*                   MPE path                                       */
/* Input(s)        : pPeerInfo - pointer to the peer information    */
/*                               from which the UPDATE is received  */
/*                   pu1RcvdUpdateMessage - received UPDATE message */
/*                               which contains SNPA information in */
/*                               MP_REACH_NLRI path attribute       */
/*                   pRtPathAttribInfo - pointer to the path        */
/*                               attribute information structure    */
/* Output(s)       : pRtPathAttribInfo - pointer to the path        */
/*                               attribute information structure    */
/*                               in which received SNPA information */
/*                               is stored                          */
/*                   pu2LenValidated - pointer to the number of     */
/*                               bytes validated                    */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4MpeProcessSnpa (tBgp4PeerEntry * pPeerInfo,
                    UINT1 *pu1RcvdUpdateMessage,
                    tBgp4Info * pRtPathAttribInfo, UINT2 *pu2LenValidated)
{
    tSnpaInfoNode      *pSnpaInfoNode = NULL;
    UINT4               u4TotSnpa = 0;
    UINT4               u4SnpaLen = 0;
    UINT2               u2Len = 0;

    *pu2LenValidated = 0;
    UNUSED_PARAM (pPeerInfo);

    u4TotSnpa = *(pu1RcvdUpdateMessage);
    pu1RcvdUpdateMessage += BGP4_MPE_NO_OF_SNPA_LEN;
    u2Len += BGP4_MPE_NO_OF_SNPA_LEN;

    TMO_SLL_Init (BGP4_MPE_SNPA_INFO_LIST (pRtPathAttribInfo));
    while (u4TotSnpa > 0)
    {
        u4SnpaLen = *(pu1RcvdUpdateMessage);
        pu1RcvdUpdateMessage += BGP4_MPE_SINGLE_SNPA_LEN;

        pSnpaInfoNode = Bgp4MpeAllocateSnpaNode (sizeof (tSnpaInfoNode));
        if (pSnpaInfoNode == NULL)
        {
            /* No memory is left for processing SNPA information -
             * so just skip this information
             */
            pu1RcvdUpdateMessage +=
                ((u4SnpaLen + BGP4_INCREMENT_BY_ONE) / sizeof (UINT2));
            u4TotSnpa -= BGP4_MPE_SINGLE_SNPA_LEN;
            u2Len += (UINT2) (BGP4_MPE_SINGLE_SNPA_LEN +
                              ((u4SnpaLen +
                                BGP4_INCREMENT_BY_ONE) / sizeof (UINT2)));
            continue;
        }
        BGP4_MPE_SNPA_NODE_LEN (pSnpaInfoNode) = (UINT1) u4SnpaLen;
        MEMCPY (BGP4_MPE_SNPA_NODE_INFO (pSnpaInfoNode), pu1RcvdUpdateMessage,
                ((u4SnpaLen + BGP4_INCREMENT_BY_ONE) / sizeof (UINT2)));

        pu1RcvdUpdateMessage +=
            ((u4SnpaLen + BGP4_INCREMENT_BY_ONE) / sizeof (UINT2));
        u4TotSnpa -= BGP4_MPE_SINGLE_SNPA_LEN;
        u2Len += (UINT2) (BGP4_MPE_SINGLE_SNPA_LEN +
                          ((u4SnpaLen +
                            BGP4_INCREMENT_BY_ONE) / sizeof (UINT2)));

        TMO_SLL_Add (BGP4_MPE_SNPA_INFO_LIST (pRtPathAttribInfo),
                     &pSnpaInfoNode->TSNextSNPA);
    }

    *pu2LenValidated = u2Len;
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4MpeProcessNexthop                          */
/* Description     : This module processes the received Next hop    */
/*                   information in MP_REACH_NLRI path attribute.   */
/*                   It does validation of Next hop and will be     */
/*                   stored in path attribute information structure */
/* Input(s)        : pPeerInfo - pointer to the peer information    */
/*                               from which the UPDATE is received  */
/*                   pu1RcvdUpdateMessage - received UPDATE message */
/*                               which contains Nexthop information */
/*                               in MP_REACH_NLRI path attribute    */
/*                   pRtPathAttribInfo - pointer to the path        */
/*                               attribute information structure    */
/* Output(s)       : pRtPathAttribInfo - pointer to the path        */
/*                               attribute information structure    */
/*                               in which received Nexthop info     */
/*                               is stored                          */
/*                   pu2LenValidated - pointer to the number of     */
/*                               bytes validated                    */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4MpeProcessNexthop (tBgp4PeerEntry * pPeerInfo,
                       UINT1 *pu1RcvdUpdateMessage,
                       tBgp4Info * pRtPathAttribInfo, UINT2 *pu2LenValidated)
{
    UINT2               u2AddrFamily = 0;
    UINT4               u4AsafiMask = 0;
    INT4                i4NexthopStatus = BGP4_FAILURE;
    UINT2               u2LenValidated = 0;
    UINT1               u1Safi = 0;

    *pu2LenValidated = 0;

    u2AddrFamily =
        BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_INFO_NEXTHOP_INFO
                                      (pRtPathAttribInfo));

    u1Safi = BGP4_MPE_BGP_SAFI_INFO (pRtPathAttribInfo);
    u4AsafiMask = ((UINT4) u2AddrFamily << BGP4_TWO_BYTE_BITS) | u1Safi;

    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
            i4NexthopStatus = Bgp4MpeProcessIpv4Nexthop (pPeerInfo,
                                                         pu1RcvdUpdateMessage,
                                                         pRtPathAttribInfo,
                                                         &u2LenValidated);
            break;
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
            i4NexthopStatus = Bgp4MpeProcessIpv6Nexthop (pPeerInfo,
                                                         pu1RcvdUpdateMessage,
                                                         pRtPathAttribInfo,
                                                         &u2LenValidated);
            break;
#endif
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
            i4NexthopStatus = Bgp4MpeProcessIpv4Nexthop (pPeerInfo,
                                                         pu1RcvdUpdateMessage,
                                                         pRtPathAttribInfo,
                                                         &u2LenValidated);
            break;

        case CAP_MP_VPN4_UNICAST:
            i4NexthopStatus = Bgp4MpeProcessVpnv4Nexthop (pPeerInfo,
                                                          pu1RcvdUpdateMessage,
                                                          pRtPathAttribInfo,
                                                          &u2LenValidated);
            break;
#endif
        default:
            break;
    }

    if (i4NexthopStatus == BGP4_SEMANTIC_INVALID_NEXTHOP)
    {
        return (BGP4_SEMANTIC_INVALID_NEXTHOP);
    }
    if (i4NexthopStatus == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }

    BGP4_INFO_ATTR_FLAG (pRtPathAttribInfo) |= BGP4_ATTR_NEXT_HOP_MASK;
    *pu2LenValidated = u2LenValidated;
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4MpeProcessIpv4Nexthop                      */
/* Description     : This module processes the received IPv4 Nexthop*/
/*                   information in MP_REACH_NLRI path attribute.   */
/*                   It does validation of IPv4 Next hop and will be*/
/*                   stored in path attribute information structure */
/* Input(s)        : pPeerInfo - pointer to the peer information    */
/*                               from which the UPDATE is received  */
/*                   pu1RcvdUpdateMessage - received UPDATE message */
/*                               which contains Nexthop information */
/*                               in MP_REACH_NLRI path attribute    */
/*                   pRtPathAttribInfo - pointer to the path        */
/*                               attribute information structure    */
/* Output(s)       : pRtPathAttribInfo - pointer to the path        */
/*                               attribute information structure    */
/*                               in which received IPv4 Nexthop info*/
/*                               is stored                          */
/*                   pu2LenValidated - pointer to the number of     */
/*                               bytes validated                    */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4MpeProcessIpv4Nexthop (tBgp4PeerEntry * pPeerInfo,
                           UINT1 *pu1RcvdUpdateMessage,
                           tBgp4Info * pRtPathAttribInfo,
                           UINT2 *pu2LenValidated)
{
    INT4                i4Ret;
    UINT1               u1NexthopLen = 0;

    u1NexthopLen = *(pu1RcvdUpdateMessage);

    if (u1NexthopLen != BGP4_ATTR_NEXTHOP_LEN)
    {
        BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Nexthop %s is received with Wrong Length %d\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))),
                       Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP (pRtPathAttribInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_INFO_NEXTHOP_INFO
                                         (pRtPathAttribInfo))), u1NexthopLen);
        return BGP4_FAILURE;
    }

    BGP4_MPE_INFO_NEXTHOP_LEN (pRtPathAttribInfo) = u1NexthopLen;

    i4Ret = Bgp4MsghValidateIpv4Nexthop (pPeerInfo, pRtPathAttribInfo,
                                         pu1RcvdUpdateMessage +
                                         BGP4_MPE_NEXTHOP_FIELD_LEN);
    if (i4Ret == BGP4_FAILURE)
    {
        Bgp4EhSendError (pPeerInfo, BGP4_UPDATE_MSG_ERR,
                         BGP4_INVALID_NEXTHOP, pu1RcvdUpdateMessage,
                         u1NexthopLen);
        return BGP4_FAILURE;
    }
    else if (i4Ret == BGP4_SEMANTIC_INVALID_NEXTHOP)
    {
        return BGP4_SEMANTIC_INVALID_NEXTHOP;
    }

    *pu2LenValidated = (UINT2) (u1NexthopLen + BGP4_MPE_NEXTHOP_FIELD_LEN);

    BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                   BGP4_MOD_NAME,
                   "\tPEER %s - IPv4 unicast nexthop %s is processed successfully\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO
                                     (pPeerInfo))),
                   Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP (pRtPathAttribInfo),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_INFO_NEXTHOP_INFO
                                     (pRtPathAttribInfo))));
    return BGP4_SUCCESS;

}

#ifdef BGP4_IPV6_WANTED
/********************************************************************/
/* Function Name   : Bgp4MpeProcessIpv6Nexthop                      */
/* Description     : This module processes the received IPv6 Nexthop*/
/*                   information in MP_REACH_NLRI path attribute.   */
/*                   It does validation of IPv4 Next hop and will be*/
/*                   stored in path attribute information structure */
/* Input(s)        : pPeerInfo - pointer to the peer information    */
/*                               from which the UPDATE is received  */
/*                   pu1RcvdUpdateMessage - received UPDATE message */
/*                               which contains Nexthop information */
/*                               in MP_REACH_NLRI path attribute    */
/*                   pRtPathAttribInfo - pointer to the path        */
/*                               attribute information structure    */
/* Output(s)       : pRtPathAttribInfo - pointer to the path        */
/*                               attribute information structure    */
/*                               in which received IPv6 Nexthop info*/
/*                               is stored                          */
/*                   pu2LenValidated - pointer to the number of     */
/*                               bytes validated                    */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4MpeProcessIpv6Nexthop (tBgp4PeerEntry * pPeerInfo,
                           UINT1 *pu1RcvdUpdateMessage,
                           tBgp4Info * pRtPathAttribInfo,
                           UINT2 *pu2LenValidated)
{
    tAddrPrefix         InvPrefix;
    tAddrPrefix         LocalBgpIdAddrPrefix;
    tAddrPrefix         RemAddr;
    tNetAddress         LocalAddr;
    tAddrPrefix         NexthopAddr;
    INT4                i4Ret;
    INT4                i4Status = BGP4_FALSE;
    INT4                i4IsV4Compat = BGP4_FALSE;
    INT4                i4LocalAddrPresent = BGP4_TRUE;
    INT4                i4NetAddrPresent = BGP4_TRUE;
    UINT2               u2Port = 0;
    UINT1               u1NexthopLen = 0;
    UINT1               u1Status = BGP4_FALSE;
    UINT4               u4Context = 0;

    u1NexthopLen = *(pu1RcvdUpdateMessage);
    u4Context = BGP4_PEER_CXT_ID (pPeerInfo);

    /* Check if the nexthop length is 16 bytes or 32 bytes */
    if (!((u1NexthopLen == BGP4_IPV6_PREFIX_LEN) ||
          (u1NexthopLen == BGP4_IPV6_PREFIX_LEN * sizeof (UINT2))))
    {
        BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Nexthop %s is received with Wrong Length %d\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))),
                       Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP (pRtPathAttribInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_INFO_NEXTHOP_INFO
                                         (pRtPathAttribInfo))), u1NexthopLen);
        return BGP4_FAILURE;
    }

    BGP4_MPE_INFO_NEXTHOP_LEN (pRtPathAttribInfo) = BGP4_IPV6_PREFIX_LEN;
    pu1RcvdUpdateMessage = pu1RcvdUpdateMessage + BGP4_MPE_NEXTHOP_FIELD_LEN;

    MEMCPY (NexthopAddr.au1Address, pu1RcvdUpdateMessage, BGP4_IPV6_PREFIX_LEN);

    i4Ret = Bgp4GetPeerNetworkAddress (pPeerInfo, BGP4_INET_AFI_IPV6, &RemAddr);
    if (i4Ret == BGP4_FAILURE)
    {
        /* Network address (v6) is not configured for peer. */
        BGP4_IN6_IS_ADDR_V4COMPATIBLE (NexthopAddr.au1Address, i4IsV4Compat);
        NexthopAddr.u2Afi = BGP4_INET_AFI_IPV6;
        Bgp4CopyAddrPrefixStruct (&RemAddr, NexthopAddr);
        i4NetAddrPresent = BGP4_FALSE;
    }

    Bgp4InitAddrPrefixStruct (&InvPrefix, BGP4_INET_AFI_IPV6);

    /* Global address */
    /* If Global address is zero, then its a default route of that peer
     * , take the peer network address as the nexthop
     */
    if ((MEMCMP (pu1RcvdUpdateMessage,
                 BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (InvPrefix),
                 BGP4_IPV6_PREFIX_LEN)) == 0)
    {
        if (i4NetAddrPresent == BGP4_TRUE)
        {
            MEMCPY (BGP4_INFO_NEXTHOP (pRtPathAttribInfo),
                    BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (RemAddr),
                    BGP4_IPV6_PREFIX_LEN);
        }
        else
        {
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - Received Nexthop address as zero\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            return BGP4_SEMANTIC_INVALID_NEXTHOP;
        }
    }
    else
    {
        MEMCPY (BGP4_INFO_NEXTHOP (pRtPathAttribInfo),
                pu1RcvdUpdateMessage, BGP4_IPV6_PREFIX_LEN);
        BGP4_IN6_IS_ADDR_LINKLOCAL (BGP4_INFO_NEXTHOP (pRtPathAttribInfo),
                                    u1Status);
        if (((Bgp4IsValidAddress (BGP4_INFO_NEXTHOP_INFO (pRtPathAttribInfo),
                                  BGP4_FALSE)) == BGP4_FALSE) ||
            (u1Status == BGP4_TRUE))
        {
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - Nexthop %s received is invalid\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))),
                           Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP
                                            (pRtPathAttribInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_INFO_NEXTHOP_INFO
                                             (pRtPathAttribInfo))));
            BGP4_IN6_IS_ADDR_LOOPBACK (&BGP4_INFO_NEXTHOP_INFO
                                       (pRtPathAttribInfo), i4Status);
            if (i4Status == BGP4_TRUE)
            {
                return BGP4_SEMANTIC_INVALID_NEXTHOP;
            }

            return BGP4_FAILURE;
        }
    }

    if (u1NexthopLen == BGP4_IPV6_PREFIX_LEN * sizeof (UINT2))
    {
        /* Link Local address is present. Use Link local as Nexthop 
         * instead of Global address
         */
        MEMCPY (BGP4_INFO_LINK_LOCAL_ADDR (pRtPathAttribInfo),
                pu1RcvdUpdateMessage + BGP4_IPV6_PREFIX_LEN,
                BGP4_IPV6_PREFIX_LEN);
        BGP4_IN6_IS_ADDR_LINKLOCAL ((pu1RcvdUpdateMessage +
                                     BGP4_IPV6_PREFIX_LEN), u1Status);
        if (u1Status == BGP4_FALSE)
        {
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - Link-local address %s received is invalid\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))),
                           Bgp4PrintIpAddr (BGP4_INFO_LINK_LOCAL_ADDR
                                            (pRtPathAttribInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_INFO_LINK_LOCAL_ADDR_INFO
                                             (pRtPathAttribInfo))));
            return BGP4_FAILURE;
        }
        BGP4_INFO_LINK_LOCAL_PRESENT (pRtPathAttribInfo) = BGP4_TRUE;
    }
    i4Ret = Bgp4GetLocalAddrForPeer (pPeerInfo, RemAddr, &LocalAddr, BGP4_TRUE);
    if (i4Ret == BGP4_FAILURE)
    {
        i4LocalAddrPresent = BGP4_FALSE;
    }

    /** Check for Semantic correctness - Sharing the same subnet */
    if (BGP4_PEER_EBGP_MULTIHOP (pPeerInfo) == BGP4_EBGP_MULTI_HOP_DISABLE)
    {
        if ((BGP4_GET_PEER_TYPE (u4Context, pPeerInfo) == BGP4_EXTERNAL_PEER) &&
            (BGP4_CONFED_PEER_STATUS (pPeerInfo) == BGP4_FALSE))
        {
            if ((BGP4_INFO_LINK_LOCAL_PRESENT (pRtPathAttribInfo)
                 == BGP4_FALSE) &&
                ((Bgp4IsOnSameSubnet
                  (BGP4_INFO_NEXTHOP_INFO (pRtPathAttribInfo),
                   pPeerInfo) == FALSE)) && (i4LocalAddrPresent == BGP4_FALSE))
            {
                BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                               BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                               BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                               BGP4_MOD_NAME,
                               "\tPEER %s - Nexthop %s received from external peer not "
                               "sharing the same subnet\n",
                               Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR
                                                (pPeerInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_PEER_REMOTE_ADDR_INFO
                                                 (pPeerInfo))),
                               Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP
                                                (pRtPathAttribInfo),
                                                BGP4_AFI_IN_ADDR_PREFIX_INFO
                                                (BGP4_INFO_NEXTHOP_INFO
                                                 (pRtPathAttribInfo))));
                return BGP4_SEMANTIC_INVALID_NEXTHOP;
            }
        }
    }

    /* Check for semantic correctness - Same as LOCAL address - */
    if (i4IsV4Compat == BGP4_TRUE)
    {
        Bgp4InitAddrPrefixStruct (&LocalBgpIdAddrPrefix, BGP4_INET_AFI_IPV4);

        PTR_ASSIGN_4 (BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO
                      (LocalBgpIdAddrPrefix), BGP4_LOCAL_BGP_ID (u4Context));
        if (((i4LocalAddrPresent == BGP4_TRUE)
             && ((PrefixMatch (NexthopAddr, LocalAddr.NetAddr)) == BGP4_TRUE))
            || ((PrefixMatch (NexthopAddr, LocalBgpIdAddrPrefix)) == BGP4_TRUE))
        {
            BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - Nexthop %s received is same as the local interface address.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))),
                           Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP
                                            (pRtPathAttribInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_INFO_NEXTHOP_INFO
                                             (pRtPathAttribInfo))));
            return (BGP4_SEMANTIC_INVALID_NEXTHOP);
        }
    }
    else if (Ip6IsLocalAddr (u4Context, BGP4_INFO_NEXTHOP (pRtPathAttribInfo),
                             &u2Port) == IP_SUCCESS)
    {
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Nexthop %s received is same as the local interface address.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))),
                       Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP (pRtPathAttribInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_INFO_NEXTHOP_INFO
                                         (pRtPathAttribInfo))));
        return (BGP4_SEMANTIC_INVALID_NEXTHOP);
    }

    *pu2LenValidated = (UINT2) (u1NexthopLen + BGP4_MPE_NEXTHOP_FIELD_LEN);
    BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                   BGP4_MOD_NAME,
                   "\tPEER %s - IPv6 unicast Nexthop %s is proccessed successfully.\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo))),
                   Bgp4PrintIpAddr (BGP4_INFO_NEXTHOP (pRtPathAttribInfo),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_INFO_NEXTHOP_INFO
                                     (pRtPathAttribInfo))));

    if (u1NexthopLen == (sizeof (UINT2) * BGP4_IPV6_PREFIX_LEN))
    {
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s - Link-local address %s is proccessed successfully.\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))),
                       Bgp4PrintIpAddr (BGP4_INFO_LINK_LOCAL_ADDR
                                        (pRtPathAttribInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_INFO_LINK_LOCAL_ADDR_INFO
                                         (pRtPathAttribInfo))));
    }
    return BGP4_SUCCESS;
}
#endif

/********************************************************************/
/* Function Name   : Bgp4MpeProcessNlri                             */
/* Description     : This module forms the list of received NLRIs   */
/*                   present in either MP_REACH_NLRI or             */
/*                   MP_UNREACH_NLRI path attribute. Each NLRI is   */
/*                   stored in a route profile and for all reachable*/
/*                   routes, path attribute information structure is*/
/*                   linked to that route profile to know the       */
/*                   remaining path attribute information associated*/
/*                   with it.                                       */
/* Input(s)        : pPeerInfo - pointer to the peer information    */
/*                               from which the UPDATE is received  */
/*                   pu1RcvdUpdateMessage - received UPDATE message */
/*                               which contains NLRI information    */
/*                               in MPE path attribute              */
/*                   pRtPathAttribInfo - pointer to the path        */
/*                               attribute information structure    */
/*                   u1MpeAttribType - Type of MPE path attribute   */
/* Output(s)       : pTsMpeRcvdRoutes - pointer to the linked list  */
/*                               of route profiles which contain    */
/*                               NLRI information                   */
/*                   pu2NlriLenValidated - pointer to the number of */
/*                               bytes validated                    */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/*                             BGP4_RSRC_ALLOC_FAIL, if resource    */
/*                               allocation fails                   */
/*                             BGP4_MAX_ROUTES_LIMIT, if route      */
/*                               count exceed MAX Limit.            */
/********************************************************************/
INT4
Bgp4MpeProcessNlri (tBgp4PeerEntry * pPeerInfo,
                    UINT1 *pu1RcvdUpdateMessage,
                    tBgp4Info * pRtPathAttribInfo,
                    UINT1 u1MpeAttribType,
                    tTMO_SLL * pTsMpeRcvdRoutes,
                    UINT2 *pu2NlriLenValidated, UINT4 u4AfiSafiMask)
{
    tLinkNode          *pLknode = NULL;
    tRouteProfile      *pRtprofile = NULL;
    tRouteProfile      *pAltRtProfile = NULL;
    UINT4               u4AsafiMask = 0;
    UINT4               u4RtProfileCnt = 0;
    UINT4               u4MaxRoutes = 0;
    UINT4               u4Context = 0;
    INT4                i4GetRoutesStatus = BGP4_FAILURE;
    UINT2               u2Len = 0;
    INT4                i4RetVal = 0;
    static tRouteProfile TmpRtProfile;

    MEMSET (&TmpRtProfile, 0, sizeof (tRouteProfile));

#if (!defined (VPLSADS_WANTED)) || (!defined (EVPN_WANTED))
    UNUSED_PARAM (u4AfiSafiMask);
#endif

    *pu2NlriLenValidated = 0;
    pLknode = Bgp4MemAllocateLinkNode (sizeof (tLinkNode));
    if (pLknode == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4MpeProcessNlri() : Unable to allocate memory "
                  "for Linknode entry.\n");
        gu4BgpDebugCnt[MAX_BGP_LINK_NOCDES_SIZING_ID]++;
        return BGP4_RSRC_ALLOC_FAIL;
    }

    pRtprofile = Bgp4MemAllocateRouteProfile (sizeof (tRouteProfile));
    if (pRtprofile == NULL)
    {
        BGP4_TRC (NULL, BGP4_TRC_FLAG,
                  BGP4_ALL_FAILURE_TRC | BGP4_CONTROL_PATH_TRC |
                  BGP4_OS_RESOURCE_TRC, BGP4_MOD_NAME,
                  "\tBgp4MpeProcessNlri() : Unable to allocate memory "
                  "for Route profile entry.\n");
        Bgp4DshReleaseLinkNode (pLknode);
        gu4BgpDebugCnt[MAX_BGP_ROUTES_SIZING_ID]++;
        if (u1MpeAttribType == BGP4_ATTR_MP_REACH_NLRI)
        {
            return BGP4_RSRC_ALLOC_FAIL;
        }
        else
        {
            i4RetVal = BGP4_RSRC_ALLOC_FAIL;
        }
    }

    u4Context = BGP4_PEER_CXT_ID (pPeerInfo);
    if (i4RetVal != BGP4_RSRC_ALLOC_FAIL)
    {
        BGP4_RT_PROTOCOL (pRtprofile) = BGP_ID;
        BGP4_RT_PEER_ENTRY (pRtprofile) = pPeerInfo;
    }
    else if (i4RetVal == BGP4_RSRC_ALLOC_FAIL)
    {
        pRtprofile = &TmpRtProfile;
        BGP4_RT_PROTOCOL (pRtprofile) = BGP_ID;
        BGP4_RT_PEER_ENTRY (pRtprofile) = pPeerInfo;
    }

#ifdef VPLSADS_WANTED
    if (u4AfiSafiMask == CAP_MP_L2VPN_VPLS)
    {
        /*For L2VPN-VPLS */
        BGP4_RT_AFI_INFO (pRtprofile) = BGP4_INET_AFI_L2VPN;
        BGP4_RT_SAFI_INFO (pRtprofile) = BGP4_INET_SAFI_VPLS;
    }
    else
    {
#endif
        BGP4_RT_AFI_INFO (pRtprofile) =
            BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_INFO_NEXTHOP_INFO
                                          (pRtPathAttribInfo));
        BGP4_RT_SAFI_INFO (pRtprofile) =
            BGP4_MPE_BGP_SAFI_INFO (pRtPathAttribInfo);

#ifdef VPLSADS_WANTED
    }
#endif
#ifdef EVPN_WANTED
    if (u4AfiSafiMask == CAP_MP_L2VPN_EVPN)
    {
        /* For EVPN */
        BGP4_RT_AFI_INFO (pRtprofile) = BGP4_INET_AFI_L2VPN;
        BGP4_RT_SAFI_INFO (pRtprofile) = BGP4_INET_SAFI_EVPN;
    }
#endif
    u4AsafiMask = ((UINT4) BGP4_RT_AFI_INFO (pRtprofile) <<
                   BGP4_TWO_BYTE_BITS) | BGP4_RT_SAFI_INFO (pRtprofile);
    switch (u4AsafiMask)
    {
        case CAP_MP_IPV4_UNICAST:
            i4GetRoutesStatus = Bgp4MpeProcessIpv4Prefix (pPeerInfo,
                                                          pu1RcvdUpdateMessage,
                                                          pRtprofile, &u2Len);
            break;
#ifdef BGP4_IPV6_WANTED
        case CAP_MP_IPV6_UNICAST:
            i4GetRoutesStatus = Bgp4MpeProcessIpv6Prefix (pPeerInfo,
                                                          pu1RcvdUpdateMessage,
                                                          pRtprofile, &u2Len);
            break;
#endif
#ifdef L3VPN
        case CAP_MP_LABELLED_IPV4:
        case CAP_MP_VPN4_UNICAST:
            /* Labelled routes */
            i4GetRoutesStatus =
                Bgp4MpeProcessIpLbldPrefix (pu1RcvdUpdateMessage,
                                            pRtprofile, u1MpeAttribType,
                                            &u2Len);
            break;
#endif
#ifdef VPLSADS_WANTED
        case CAP_MP_L2VPN_VPLS:
            i4GetRoutesStatus =
                Bgp4MpeProcessL2vpnPrefix (pu1RcvdUpdateMessage,
                                           pRtprofile, &u2Len);
            break;
#endif
#ifdef EVPN_WANTED
        case CAP_MP_L2VPN_EVPN:
            i4GetRoutesStatus =
                Bgp4MpeProcessEvpnPrefix (pu1RcvdUpdateMessage,
                                          pRtprofile, &u2Len);
            break;
#endif
        default:
            break;
    }

    if (i4GetRoutesStatus == BGP4_FAILURE)
    {
        Bgp4DshReleaseLinkNode (pLknode);

        if (i4RetVal != BGP4_RSRC_ALLOC_FAIL)
        {
            Bgp4DshReleaseRtInfo (pRtprofile);
        }
        return BGP4_FAILURE;
    }
#ifdef L3VPN
    else if (i4GetRoutesStatus == BGP4_NOT_NEGOTIATED_FAMILY)
    {
        Bgp4DshReleaseLinkNode (pLknode);
        if (i4RetVal != BGP4_RSRC_ALLOC_FAIL)
        {
            Bgp4DshReleaseRtInfo (pRtprofile);
        }
        *pu2NlriLenValidated = u2Len;
        return BGP4_NOT_NEGOTIATED_FAMILY;
    }
#endif /* L3VPN */

    if (u1MpeAttribType == BGP4_ATTR_MP_REACH_NLRI)
    {
        u4MaxRoutes = FsBGPSizingParams[MAX_BGP_ROUTES_SIZING_ID].
            u4PreAllocatedUnits;
        u4RtProfileCnt =
            BGP4_RIB_ROUTE_CNT (BGP4_RT_CXT_ID (pRtprofile)) +
            (BGP4_NONSYNC_LIST (BGP4_RT_CXT_ID (pRtprofile)))->u4_Count +
            TMO_SLL_Count (pTsMpeRcvdRoutes);

        if (u4RtProfileCnt >= u4MaxRoutes)
        {
            tNetAddress         IpAddress;

            Bgp4CopyNetAddressStruct (&(IpAddress),
                                      BGP4_RT_NET_ADDRESS_INFO (pRtprofile));
            pAltRtProfile = Bgp4GetRoute (u4Context, IpAddress,
                                          BGP4_PEER_REMOTE_ADDR_INFO
                                          (pPeerInfo), NULL);
        }
        if ((u4RtProfileCnt >= u4MaxRoutes) && (pAltRtProfile == NULL))
        {
            BGP4_TRC_ARG4 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - No of Routes %d received exceeding maximum routes %d."
                           " Discarding the route %s received in update message\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))), u4RtProfileCnt,
                           u4MaxRoutes,
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtprofile),
                                            BGP4_RT_AFI_INFO (pRtprofile)));
            Bgp4DshReleaseLinkNode (pLknode);
            Bgp4DshReleaseRtInfo (pRtprofile);
            *pu2NlriLenValidated = u2Len;
            return BGP4_MAX_ROUTES_LIMIT;
        }

        if (BGP4_PEER_CURRENT_PREFIX_LIMIT (pPeerInfo) >=
            BGP4_PEER_PREFIX_UPPER_LIMIT (pPeerInfo))
        {
            BGP4_TRC_ARG4 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG,
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s - No of Routes %d received for the peer exceeding the "
                           "maximum prefix limit of the peer %d. Discarding the route %s received\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))),
                           BGP4_PEER_CURRENT_PREFIX_LIMIT (pPeerInfo),
                           BGP4_PEER_PREFIX_UPPER_LIMIT (pPeerInfo),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtprofile),
                                            BGP4_RT_AFI_INFO (pRtprofile)));
            Bgp4DshReleaseLinkNode (pLknode);
            Bgp4DshReleaseRtInfo (pRtprofile);
            *pu2NlriLenValidated = u2Len;
            return BGP4_MAX_MP_ROUTE_PREFIX_LIMIT;
        }

        BGP4_LINK_INFO_TO_PROFILE (pRtPathAttribInfo, pRtprofile);
        BGP4_LINK_PROFILE_TO_NODE (pRtprofile, pLknode);
        BGP4_RT_SET_FLAG (pRtprofile, BGP4_MPE_RT_ADVT_MP_REACH);
        BGP4_PEER_CURRENT_PREFIX_LIMIT (pPeerInfo) += 1;
    }
    else
    {
        if (i4RetVal != BGP4_RSRC_ALLOC_FAIL)
        {
            BGP4_LINK_PROFILE_TO_NODE (pRtprofile, pLknode);
        }
        BGP4_RT_SET_FLAG (pRtprofile, BGP4_MPE_RT_ADVT_MP_UNREACH);
        BGP4_RT_SET_FLAG (pRtprofile, BGP4_RT_WITHDRAWN);

        if (i4RetVal == BGP4_RSRC_ALLOC_FAIL)
        {
            tNetAddress         IpAddress;
            Bgp4CopyNetAddressStruct (&(IpAddress),
                                      BGP4_RT_NET_ADDRESS_INFO (pRtprofile));
            pAltRtProfile =
                Bgp4GetRoute (u4Context, IpAddress,
                              BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo), NULL);
            if (pAltRtProfile != NULL)
            {
                Bgp4RibhProcessWithdRoute (pPeerInfo, pAltRtProfile, NULL);
                BGP4_PEER_CURRENT_PREFIX_LIMIT (pPeerInfo) -= 1;
            }
        }
        else
        {
            if (BGP4_PEER_CURRENT_PREFIX_LIMIT (pPeerInfo) > 0)
            {
                BGP4_PEER_CURRENT_PREFIX_LIMIT (pPeerInfo) -= 1;
            }
        }
    }
    if (i4RetVal != BGP4_RSRC_ALLOC_FAIL)
    {
        TMO_SLL_Add (pTsMpeRcvdRoutes, &pLknode->TSNext);
    }
    *pu2NlriLenValidated = u2Len;
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4MpeProcessIpv4Prefix                       */
/* Description     : This module processes the received IPv4 NLRI   */
/*                   present in either MP_REACH_NLRI or             */
/*                   MP_UNREACH_NLRI. It does validation of received*/
/*                   IPv4 NLRI and is stored in the input route     */
/*                   profile.                                       */
/* Input(s)        : pPeerInfo - pointer to the peer information    */
/*                               from which the UPDATE is received  */
/*                   pu1RcvdUpdateMessage - received UPDATE message */
/*                               which contains NLRI information    */
/*                               in MPE path attribute              */
/*                   pRtProfile - pointer to the path route profile */
/*                               information structure              */
/* Output(s)       : pu2NlriLenValidated - pointer to the number of */
/*                                bytes validated                   */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4MpeProcessIpv4Prefix (tBgp4PeerEntry * pPeerInfo,
                          UINT1 *pu1RcvdUpdateMessage,
                          tRouteProfile * pRtProfile,
                          UINT2 *pu2NlriLenValidated)
{
    UINT4               u4Prefix = 0;
    UINT2               u2Len = 0;
    UINT2               u2BytesForNlri = 0;

    *pu2NlriLenValidated = 0;

    UNUSED_PARAM (pPeerInfo);

    BGP4_RT_PREFIXLEN (pRtProfile) = *(pu1RcvdUpdateMessage);
    if (BGP4_RT_PREFIXLEN (pRtProfile) > BGP4_MAX_PREFIXLEN)
    {
        BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s -  Prefix length is received with wrong value %d"
                       " for IPv4 prefix %s in update message\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))),
                       BGP4_RT_PREFIXLEN (pRtProfile),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_FAILURE;
    }

    pu1RcvdUpdateMessage += BGP4_MPE_IPV4_PREFIX_LEN;

    u2BytesForNlri = (UINT2) BGP4_PREFIX_BYTES (BGP4_RT_PREFIXLEN (pRtProfile));
    BGP4_GET_PREFIX (pu1RcvdUpdateMessage, &u4Prefix, u2BytesForNlri);

    /* Put the AFI */
    BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                  (BGP4_RT_NET_ADDRESS_INFO (pRtProfile))) =
        BGP4_INET_AFI_IPV4;

    /* Put the Address Length = 4 */
    BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                       (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)))
        = BGP4_IPV4_PREFIX_LEN;

    /* Put the SAFI */
    BGP4_SAFI_IN_NET_ADDRESS_INFO (BGP4_RT_NET_ADDRESS_INFO (pRtProfile))
        = BGP4_INET_SAFI_UNICAST;
    PTR_ASSIGN4 (BGP4_RT_IP_PREFIX (pRtProfile), (u4Prefix));

    if ((Bgp4IsValidAddress
         (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
          (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)), BGP4_TRUE)) != BGP4_TRUE)
    {
        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                       "\tPEER %s -  Received prefix %s in update message is invalid\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_FAILURE;
    }

    u2Len += BGP4_MPE_IPV4_PREFIX_LEN + u2BytesForNlri;

    /* This field actually will be useful when the address family is other
     * than IPV4 unicast. To give an idea how it should be updated, its
     * updated for IPV4 unicast also
     */
    *pu2NlriLenValidated = u2Len;
    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                   BGP4_MOD_NAME,
                   "\tPEER %s -  Received prefix %s of prefix length %d is "
                   "processed successfully in update message\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo))),
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   BGP4_RT_PREFIXLEN (pRtProfile));
    return BGP4_SUCCESS;
}

#ifdef VPLSADS_WANTED
/********************************************************************/
/* Function Name   : Bgp4MpeProcessL2vpnPrefix                      */
/* Description     : This module processes the received L2VPN NLRI  */
/*                   present in either MP_REACH_NLRI or             */
/*                   MP_UNREACH_NLRI. It does validation of received*/
/*                   L2VPN NLRI and is stored in the input route    */
/*                   profile.                                       */
/* Input(s)        : pPeerInfo - pointer to the peer information    */
/*                               from which the UPDATE is received  */
/*                   pu1RcvdUpdateMessage - received UPDATE message */
/*                               which contains NLRI information    */
/*                               in MPE path attribute              */
/*                   pRtProfile - pointer to the path route profile */
/*                               information structure              */
/* Output(s)       : pu2NlriLenValidated - pointer to the number of */
/*                                bytes validated                   */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4MpeProcessL2vpnPrefix (UINT1 *pu1RcvdUpdateMessage,
                           tRouteProfile * pRtProfile,
                           UINT2 *pu2NlriLenValidated)
{
    UINT2               u2Len = 0;
    /*UINT1               au1LabelBase[BGP4_VPLS_NLRI_LB_LEN + 1]={0} ; */

    *pu2NlriLenValidated = 0;

    /*((pRtProfile->NetAddress).u2PrefixLen)
       This prefix len is comming form the NLRI field. <Len (1-oct), Pre(var)>
       for L2VPN-VPLS,len is 2 oct. */

    /*BGP4_RT_PREFIXLEN (pRtProfile) = *(UINT2 *)(pu1RcvdUpdateMessage) ; */
    PTR_FETCH2 (BGP4_RT_PREFIXLEN (pRtProfile), pu1RcvdUpdateMessage);

    if (BGP4_RT_PREFIXLEN (pRtProfile) > BGP4_VPLS_NLRI_PREFIX_LEN)
    {
        return BGP4_FAILURE;
    }

    pu1RcvdUpdateMessage += BGP4_VPLS_NLRI_PREFIX_FIELD_LEN;    /*Length=2 */

    /* Put the Address Length = 17 */
    BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                       (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)))
        = BGP4_VPLS_NLRI_PREFIX_LEN;

    MEMCPY (BGP4_RT_IP_PREFIX (pRtProfile), pu1RcvdUpdateMessage,
            BGP4_VPLS_NLRI_PREFIX_LEN);

    MEMCPY (BGP4_VPLS_RT_ROUTE_DISTING (pRtProfile), pu1RcvdUpdateMessage,
            BGP4_VPLS_ROUTE_DISTING_SIZE);

    pu1RcvdUpdateMessage += BGP4_VPLS_ROUTE_DISTING_SIZE;

    /*BGP4_VPLS_RT_VE_ID(pRtProfile) = *(UINT2 *)(pu1RcvdUpdateMessage); */
    PTR_FETCH2 (BGP4_VPLS_RT_VE_ID (pRtProfile), pu1RcvdUpdateMessage);
    pu1RcvdUpdateMessage += BGP4_VPLS_NLRI_VEID_LEN;    /*Length=2 */

    /*BGP4_VPLS_RT_VBO(pRtProfile) =*(UINT2*)(pu1RcvdUpdateMessage); */
    PTR_FETCH2 (BGP4_VPLS_RT_VBO (pRtProfile), pu1RcvdUpdateMessage);
    pu1RcvdUpdateMessage += BGP4_VPLS_NLRI_VBO_LEN;    /*Length=2 */

    /*BGP4_VPLS_RT_VBS(pRtProfile) = *(UINT2 *)(pu1RcvdUpdateMessage); */
    PTR_FETCH2 (BGP4_VPLS_RT_VBS (pRtProfile), pu1RcvdUpdateMessage);
    pu1RcvdUpdateMessage += BGP4_VPLS_NLRI_VBS_LEN;    /*Length=2 */

    /* LB is 3 bytes field ,copying from index 1 */
    MEMCPY (&(((UINT1 *) (&BGP4_VPLS_RT_LB (pRtProfile)))[1]),
            pu1RcvdUpdateMessage, BGP4_VPLS_NLRI_LB_LEN);
    BGP4_VPLS_RT_LB (pRtProfile) = OSIX_HTONL (BGP4_VPLS_RT_LB (pRtProfile));

    u2Len += (UINT2) (BGP4_VPLS_NLRI_PREFIX_LEN + BGP4_VPLS_NLRI_PREFIX_FIELD_LEN);    /*19 bytes */

    /* This field actually will be useful when the address family is other
     * than IPV4 unicast. To give an idea how it should be updated, its
     * updated for IPV4 unicast also
     */
    *pu2NlriLenValidated = u2Len;
    return BGP4_SUCCESS;
}
#endif
#ifdef EVPN_WANTED
/********************************************************************/
/* Function Name   : Bgp4MpeProcessEvpnPrefix                       */
/* Description     : This module processes the received EVPN NLRI   */
/*                   present in either MP_REACH_NLRI or             */
/*                   MP_UNREACH_NLRI. It does validation of received*/
/*                   EVPN NLRI and is stored in the input route     */
/*                   profile.                                       */
/* Input(s)        : pPeerInfo - pointer to the peer information    */
/*                               from which the UPDATE is received  */
/*                   pu1RcvdUpdateMessage - received UPDATE message */
/*                               which contains NLRI information    */
/*                               in MPE path attribute              */
/*                   pRtProfile - pointer to the path route profile */
/*                               information structure              */
/* Output(s)       : pu2NlriLenValidated - pointer to the number of */
/*                                bytes validated                   */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4MpeProcessEvpnPrefix (UINT1 *pu1RcvdUpdateMessage,
                          tRouteProfile * pRtProfile,
                          UINT2 *pu2NlriLenValidated)
{

    INT4                i4Len = 0;

    *pu2NlriLenValidated = 0;

    if (pu1RcvdUpdateMessage[0] == EVPN_MAC_ROUTE)
    {

        BGP4_RT_PREFIXLEN (pRtProfile) = pu1RcvdUpdateMessage[1];

        if (!
            ((BGP4_RT_PREFIXLEN (pRtProfile) ==
              (BGP4_EVPN_MAC_NLRI_PREFIX_LEN - BGP4_TWO_BYTE))
             || (BGP4_RT_PREFIXLEN (pRtProfile) ==
                 ((BGP4_EVPN_MAC_NLRI_PREFIX_LEN - BGP4_TWO_BYTE) +
                  BGP4_EVPN_IP_ADDR_SIZE))
             || (BGP4_RT_PREFIXLEN (pRtProfile) ==
                 (BGP4_EVPN_MAC_NLRI_PREFIX_LEN - BGP4_TWO_BYTE) +
                 BGP4_EVPN_IP6_ADDR_SIZE)))

        {
            BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC | BGP4_EVPN_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s -  Prefix length is received with wrong value %d"
                           " for the EVPN route %s in update message\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))),
                           BGP4_RT_PREFIXLEN (pRtProfile),
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
            return BGP4_FAILURE;
        }
        MEMCPY (&BGP4_EVPN_RT_TYPE (pRtProfile), pu1RcvdUpdateMessage,
                BGP4_EVPN_RT_TYPE_SIZE);
        pu1RcvdUpdateMessage += 2;    /* Rt Type + Length */
        i4Len += 2;

        MEMCPY (BGP4_EVPN_RD_DISTING (pRtProfile), pu1RcvdUpdateMessage,
                BGP4_EVPN_ROUTE_DISTING_SIZE);
        pu1RcvdUpdateMessage += BGP4_EVPN_ROUTE_DISTING_SIZE;    /*Length=6 for RD */
        i4Len += BGP4_EVPN_ROUTE_DISTING_SIZE;

        MEMCPY (BGP4_EVPN_ETHSEGID (pRtProfile), pu1RcvdUpdateMessage,
                BGP4_EVPN_ETH_SEG_ID_SIZE);
        pu1RcvdUpdateMessage += BGP4_EVPN_ETH_SEG_ID_SIZE;    /*Length=10 for EthSegId */
        i4Len += BGP4_EVPN_ETH_SEG_ID_SIZE;

        MEMCPY (&BGP4_EVPN_ETHTAG (pRtProfile), pu1RcvdUpdateMessage,
                BGP4_EVPN_ETH_TAG_SIZE);
        pu1RcvdUpdateMessage += BGP4_EVPN_ETH_TAG_SIZE;    /* Length=4 for Eth Tag */
        i4Len += BGP4_EVPN_ETH_TAG_SIZE;

        MEMCPY ((UINT1 *) &BGP4_EVPN_MACADDR_LEN (pRtProfile),
                pu1RcvdUpdateMessage, BGP4_EVPN_ADDR_LEN_SIZE);
        pu1RcvdUpdateMessage += BGP4_EVPN_ADDR_LEN_SIZE;    /* Length=1 for MacAddr Len */
        i4Len += BGP4_EVPN_ADDR_LEN_SIZE;

        MEMCPY (BGP4_EVPN_MACADDR (pRtProfile), pu1RcvdUpdateMessage,
                BGP4_EVPN_MAC_ADDR_SIZE);
        pu1RcvdUpdateMessage += BGP4_EVPN_MAC_ADDR_SIZE;    /* Length=6 for Mac Addr */
        i4Len += BGP4_EVPN_MAC_ADDR_SIZE;

        MEMCPY ((UINT1 *) &BGP4_EVPN_IPADDR_LEN (pRtProfile),
                pu1RcvdUpdateMessage, BGP4_EVPN_ADDR_LEN_SIZE);
        pu1RcvdUpdateMessage += BGP4_EVPN_ADDR_LEN_SIZE;    /* Length=1 for IPAddr Len */
        i4Len += BGP4_EVPN_ADDR_LEN_SIZE;

        if (BGP4_EVPN_IPADDR_LEN (pRtProfile) ==
            BGP4_EVPN_IP_ADDR_SIZE * BGP4_ONE_BYTE_BITS)
        {
            MEMCPY (&BGP4_EVPN_IPADDR (pRtProfile), pu1RcvdUpdateMessage,
                    BGP4_EVPN_IP_ADDR_SIZE);
            pu1RcvdUpdateMessage += BGP4_EVPN_IP_ADDR_SIZE;    /* Length=4 for Ip Addr */
            i4Len += BGP4_EVPN_IP_ADDR_SIZE;
        }
        else if (BGP4_EVPN_IPADDR_LEN (pRtProfile) ==
                 BGP4_EVPN_IP6_ADDR_SIZE * BGP4_ONE_BYTE_BITS)
        {
            MEMCPY (&BGP4_EVPN_IPADDR (pRtProfile), pu1RcvdUpdateMessage,
                    BGP4_EVPN_IP6_ADDR_SIZE);
            pu1RcvdUpdateMessage += BGP4_EVPN_IP6_ADDR_SIZE;    /* Length=16 for Ip Addr */
            i4Len += BGP4_EVPN_IP6_ADDR_SIZE;
        }

        MEMCPY (&BGP4_EVPN_VNID (pRtProfile), pu1RcvdUpdateMessage,
                BGP4_EVPN_VN_ID_SIZE);
        BGP4_EVPN_VNID (pRtProfile) = OSIX_NTOHL (BGP4_EVPN_VNID (pRtProfile));

        BGP4_EVPN_VNID (pRtProfile) = BGP4_EVPN_VNID (pRtProfile) >> 8;

        pu1RcvdUpdateMessage += BGP4_EVPN_VN_ID_SIZE;    /* Length=4 for VNId */
        i4Len += BGP4_EVPN_VN_ID_SIZE;

        *pu2NlriLenValidated = (UINT2) i4Len;
        BGP4_DBG2 (BGP4_DBG_ALL,
                   "\t\tEVPN MAC/IP NLRI - [%s/%d]\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   BGP4_RT_PREFIXLEN (pRtProfile));
    }
    else if (pu1RcvdUpdateMessage[0] == EVPN_ETH_SEGMENT_ROUTE)
    {
        BGP4_RT_PREFIXLEN (pRtProfile) = pu1RcvdUpdateMessage[1];

        if (!
            ((BGP4_RT_PREFIXLEN (pRtProfile) ==
              (BGP4_EVPN_ETH_SEG_NLRI_PREFIX_LEN - BGP4_TWO_BYTE))
             || (BGP4_RT_PREFIXLEN (pRtProfile) ==
                 ((BGP4_EVPN_ETH_SEG_NLRI_PREFIX_LEN - BGP4_TWO_BYTE) +
                  BGP4_EVPN_IP_ADDR_SIZE))
             || (BGP4_RT_PREFIXLEN (pRtProfile) ==
                 (BGP4_EVPN_ETH_SEG_NLRI_PREFIX_LEN - BGP4_TWO_BYTE) +
                 BGP4_EVPN_IP6_ADDR_SIZE)))
        {
            BGP4_TRC_ARG1 (NULL,
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                           BGP4_MOD_NAME,
                           "\tBgp4MpeProcessEvpnPrefix() "
                           "Prefix length is received,with Wrong value [%d] \n",
                           BGP4_RT_PREFIXLEN (pRtProfile));
            return BGP4_FAILURE;
        }
        MEMCPY (&BGP4_EVPN_RT_TYPE (pRtProfile), pu1RcvdUpdateMessage,
                BGP4_EVPN_RT_TYPE_SIZE);
        pu1RcvdUpdateMessage += 2;    /* Rt Type + Length */
        i4Len += 2;

        MEMCPY (BGP4_EVPN_RD_DISTING (pRtProfile), pu1RcvdUpdateMessage,
                BGP4_EVPN_ROUTE_DISTING_SIZE);
        pu1RcvdUpdateMessage += BGP4_EVPN_ROUTE_DISTING_SIZE;    /*Length=6 for RD */
        i4Len += BGP4_EVPN_ROUTE_DISTING_SIZE;

        MEMCPY (BGP4_EVPN_ETHSEGID (pRtProfile), pu1RcvdUpdateMessage,
                BGP4_EVPN_ETH_SEG_ID_SIZE);
        pu1RcvdUpdateMessage += BGP4_EVPN_ETH_SEG_ID_SIZE;    /*Length=10 for EthSegId */
        i4Len += BGP4_EVPN_ETH_SEG_ID_SIZE;

        MEMCPY ((UINT1 *) &BGP4_EVPN_IPADDR_LEN (pRtProfile),
                pu1RcvdUpdateMessage, BGP4_EVPN_ADDR_LEN_SIZE);
        pu1RcvdUpdateMessage += BGP4_EVPN_ADDR_LEN_SIZE;    /* Length=1 for IPAddr Len */
        i4Len += BGP4_EVPN_ADDR_LEN_SIZE;

        if (BGP4_EVPN_IPADDR_LEN (pRtProfile) ==
            BGP4_EVPN_IP_ADDR_SIZE * BGP4_ONE_BYTE_BITS)
        {
            MEMCPY (&BGP4_EVPN_IPADDR (pRtProfile), pu1RcvdUpdateMessage,
                    BGP4_EVPN_IP_ADDR_SIZE);
            pu1RcvdUpdateMessage += BGP4_EVPN_IP_ADDR_SIZE;    /* Length=4 for Ip Addr */
            i4Len += BGP4_EVPN_IP_ADDR_SIZE;
        }
        else if (BGP4_EVPN_IPADDR_LEN (pRtProfile) ==
                 BGP4_EVPN_IP6_ADDR_SIZE * BGP4_ONE_BYTE_BITS)
        {
            MEMCPY (&BGP4_EVPN_IPADDR (pRtProfile), pu1RcvdUpdateMessage,
                    BGP4_EVPN_IP6_ADDR_SIZE);
            pu1RcvdUpdateMessage += BGP4_EVPN_IP6_ADDR_SIZE;    /* Length=16 for Ip Addr */
            i4Len += BGP4_EVPN_IP6_ADDR_SIZE;
        }

        /* VNI is filled as 0 in Route Profile */
        BGP4_EVPN_VNID (pRtProfile) = 0;
        *pu2NlriLenValidated = (UINT2) i4Len;

        BGP4_DBG2 (BGP4_DBG_ALL,
                   "\t\tEVPN Ethernet Segment Route NLRI - [%s/%d]\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   BGP4_RT_PREFIXLEN (pRtProfile));

    }
    else if (pu1RcvdUpdateMessage[0] == EVPN_AD_ROUTE)
    {
        BGP4_RT_PREFIXLEN (pRtProfile) = pu1RcvdUpdateMessage[1];

        if (BGP4_RT_PREFIXLEN (pRtProfile) >
            BGP4_EVPN_ETH_AD_ROUTE_NLRI_PREFIX_LEN)
        {
            BGP4_TRC_ARG1 (NULL,
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC,
                           BGP4_MOD_NAME,
                           "\tBgp4MpeProcessEvpnPrefix() "
                           "Prefix length is received,with Wrong value [%d] \n",
                           BGP4_RT_PREFIXLEN (pRtProfile));
            return BGP4_FAILURE;
        }
        MEMCPY (&BGP4_EVPN_RT_TYPE (pRtProfile), pu1RcvdUpdateMessage,
                BGP4_EVPN_RT_TYPE_SIZE);
        pu1RcvdUpdateMessage += 2;    /* Rt Type + Length */
        i4Len += 2;

        MEMCPY (BGP4_EVPN_RD_DISTING (pRtProfile), pu1RcvdUpdateMessage,
                BGP4_EVPN_ROUTE_DISTING_SIZE);
        pu1RcvdUpdateMessage += BGP4_EVPN_ROUTE_DISTING_SIZE;    /*Length=6 for RD */
        i4Len += BGP4_EVPN_ROUTE_DISTING_SIZE;

        MEMCPY (BGP4_EVPN_ETHSEGID (pRtProfile), pu1RcvdUpdateMessage,
                BGP4_EVPN_ETH_SEG_ID_SIZE);
        pu1RcvdUpdateMessage += BGP4_EVPN_ETH_SEG_ID_SIZE;    /*Length=10 for EthSegId */
        i4Len += BGP4_EVPN_ETH_SEG_ID_SIZE;

        MEMCPY (&BGP4_EVPN_ETHTAG (pRtProfile), pu1RcvdUpdateMessage,
                BGP4_EVPN_ETH_TAG_SIZE);
        pu1RcvdUpdateMessage += BGP4_EVPN_ETH_TAG_SIZE;    /* Length=4 for Eth Tag */
        i4Len += BGP4_EVPN_ETH_TAG_SIZE;
        MEMCPY (&BGP4_EVPN_VNID (pRtProfile), pu1RcvdUpdateMessage,
                BGP4_EVPN_VN_ID_SIZE);
        BGP4_EVPN_VNID (pRtProfile) = OSIX_NTOHL (BGP4_EVPN_VNID (pRtProfile));

        BGP4_EVPN_VNID (pRtProfile) = BGP4_EVPN_VNID (pRtProfile) >> 8;

        pu1RcvdUpdateMessage += BGP4_EVPN_VN_ID_SIZE;    /* Length=4 for VNId */
        i4Len += BGP4_EVPN_VN_ID_SIZE;

        *pu2NlriLenValidated = (UINT2) i4Len;
        BGP4_DBG2 (BGP4_DBG_ALL,
                   "\t\tEVPN  Ethernet A-D NLRI - [%s/%d]\n",
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   BGP4_RT_PREFIXLEN (pRtProfile));

    }

    return BGP4_SUCCESS;
}
#endif
#ifdef BGP4_IPV6_WANTED
/********************************************************************/
/* Function Name   : Bgp4MpeProcessIpv6Prefix                       */
/* Description     : This module processes the received IPv6 NLRI   */
/*                   present in either MP_REACH_NLRI or             */
/*                   MP_UNREACH_NLRI. It does validation of received*/
/*                   IPv4 NLRI and is stored in the input route     */
/*                   profile.                                       */
/* Input(s)        : pPeerInfo - pointer to the peer information    */
/*                               from which the UPDATE is received  */
/*                   pu1RcvdUpdateMessage - received UPDATE message */
/*                               which contains NLRI information    */
/*                               in MPE path attribute              */
/*                   pRtProfile - pointer to the path route profile */
/*                               information structure              */
/* Output(s)       : pu2NlriLenValidated - pointer to the number of */
/*                                bytes validated                   */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4MpeProcessIpv6Prefix (tBgp4PeerEntry * pPeerInfo,
                          UINT1 *pu1RcvdUpdateMessage,
                          tRouteProfile * pRtProfile,
                          UINT2 *pu2NlriLenValidated)
{
    UINT2               u2Len = 0;
    UINT2               u2BytesForNlri = 0;

    *pu2NlriLenValidated = 0;

    UNUSED_PARAM (pPeerInfo);

    BGP4_RT_PREFIXLEN (pRtProfile) = *(pu1RcvdUpdateMessage);
    /* Do not accept prefix length of zero .
     * This is not according to RFC, but accoring to CISCO router
     */
    if (BGP4_RT_PREFIXLEN (pRtProfile) > BGP4_MAX_IPV6_PREFIXLEN)
    {
        BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                       BGP4_MOD_NAME,
                       "\tPEER %s -  Prefix length is received with wrong value %d"
                       " for IPv6 prefix %s in update message\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))),
                       BGP4_RT_PREFIXLEN (pRtProfile),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));
        return BGP4_FAILURE;
    }

    pu1RcvdUpdateMessage += BGP4_MPE_IPV6_PREFIX_LEN;

    u2BytesForNlri = (UINT2) BGP4_PREFIX_BYTES (BGP4_RT_PREFIXLEN (pRtProfile));

    /* Put the AFI */
    BGP4_AFI_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                  (BGP4_RT_NET_ADDRESS_INFO (pRtProfile))) =
        BGP4_INET_AFI_IPV6;

    /* Put the Address Length = 16 */
    BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                                       (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)))
        = BGP4_IPV6_PREFIX_LEN;

    /* Put the SAFI */
    BGP4_SAFI_IN_NET_ADDRESS_INFO (BGP4_RT_NET_ADDRESS_INFO (pRtProfile))
        = BGP4_INET_SAFI_UNICAST;

    MEMCPY (BGP4_RT_IP_PREFIX (pRtProfile), pu1RcvdUpdateMessage,
            u2BytesForNlri);

    if ((Bgp4IsValidAddress
         (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
          (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)), BGP4_TRUE)) != BGP4_TRUE)
    {
        tAddrPrefix         InvPrefix;
        UINT1               u1PrefLen = 0;

        BGP4_TRC_ARG2 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                       BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                       BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC, BGP4_MOD_NAME,
                       "\tPEER %s -  Received prefix %s in update message is invalid\n",
                       Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                        BGP4_AFI_IN_ADDR_PREFIX_INFO
                                        (BGP4_PEER_REMOTE_ADDR_INFO
                                         (pPeerInfo))),
                       Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                        BGP4_RT_AFI_INFO (pRtProfile)));

        Bgp4InitAddrPrefixStruct (&InvPrefix, BGP4_INET_AFI_IPV6);
        u1PrefLen = Bgp4RoundToBytes (BGP4_RT_PREFIXLEN (pRtProfile));
        if (((u1PrefLen <= BGP4_MAX_INET_ADDRESS_LEN)
             &&
             (MEMCMP
              (pu1RcvdUpdateMessage,
               BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO (InvPrefix),
               u1PrefLen))) != 0)
        {
            /* We do not process the default routes. But CISCO and someother
             * can send default routes, if that case we should not send
             * NOTIFICATION, instead if any other invalid prefix otherthan
             * default prefix (all 0's) then only send NOTIFICATION
             */
            BGP4_TRC_ARG1 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                           BGP4_TRC_FLAG, BGP4_ALL_FAILURE_TRC |
                           BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                           BGP4_MOD_NAME,
                           "\tPEER %s -  The Received prefix in update message is default route."
                           " Default route are not processed.\n",
                           Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                            BGP4_AFI_IN_ADDR_PREFIX_INFO
                                            (BGP4_PEER_REMOTE_ADDR_INFO
                                             (pPeerInfo))));
            return BGP4_FAILURE;
        }
    }

    u2Len += BGP4_MPE_IPV6_PREFIX_LEN + u2BytesForNlri;

    /* This field actually will be useful when the address family is other
     * than IPV6 unicast. To give an idea how it should be updated, its
     * updated for IPV6 unicast also
     */
    *pu2NlriLenValidated = u2Len;
    BGP4_TRC_ARG3 (&(BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo)),
                   BGP4_TRC_FLAG, BGP4_CONTROL_PATH_TRC | BGP4_UPD_MSG_TRC,
                   BGP4_MOD_NAME,
                   "\tPEER %s -  Received prefix %s of prefix length %d is "
                   "processed successfully in update message\n",
                   Bgp4PrintIpAddr (BGP4_PEER_REMOTE_ADDR (pPeerInfo),
                                    BGP4_AFI_IN_ADDR_PREFIX_INFO
                                    (BGP4_PEER_REMOTE_ADDR_INFO (pPeerInfo))),
                   Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                    BGP4_RT_AFI_INFO (pRtProfile)),
                   BGP4_RT_PREFIXLEN (pRtProfile));
    return BGP4_SUCCESS;
}
#endif

#ifdef L3VPN
/********************************************************************/
/* Function Name   : Bgp4MpeProcessIpLbldPrefix                     */
/* Description     : process the Labelled route                     */
/* Input(s)        : pu1RcvdUpdateMessage - pointer to the UPDATE   */
/*                   message in which Label is received             */
/*                   pRtProfile - route information                 */
/* Output(s)       : pu2Len - pointer to number of bytes processed  */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4MpeProcessIpLbldPrefix (UINT1 *pu1RcvdUpdateMessage,
                            tRouteProfile * pRtProfile,
                            UINT1 u1MpeAttrType, UINT2 *pu2Len)
{
    UINT4               u4AsafiMask;
    INT4                i4Status;
    UINT2               u2Bytes = 0;
    UINT2               u2BytesPrcsd = 0;
    UINT1               u1LenInBits = 0;

    u1LenInBits = *(pu1RcvdUpdateMessage);
    u2BytesPrcsd++;
    pu1RcvdUpdateMessage++;
    i4Status = Bgp4MpeProcessIpLabelInUpdMsg (pu1RcvdUpdateMessage,
                                              u1LenInBits,
                                              pRtProfile,
                                              u1MpeAttrType, &u2Bytes);
    if (i4Status == BGP4_FAILURE)
    {
        return BGP4_FAILURE;
    }
    /*Withdrawn Nlri has to be dropped */
    if (i4Status == BGP4_NOT_NEGOTIATED_FAMILY)
    {
        *pu2Len = (UINT2) ((Bgp4RoundToBytes (u1LenInBits)) + 1);
        return BGP4_NOT_NEGOTIATED_FAMILY;
    }

    u2BytesPrcsd = (UINT2) (u2BytesPrcsd + u2Bytes);

    /* Move the update message pointer by label length */
    pu1RcvdUpdateMessage += u2Bytes;

    u4AsafiMask = ((UINT4) BGP4_RT_AFI_INFO (pRtProfile) <<
                   BGP4_TWO_BYTE_BITS) | BGP4_RT_SAFI_INFO (pRtProfile);
    switch (u4AsafiMask)
    {
        case CAP_MP_LABELLED_IPV4:
        {
            UINT4               u4Prefix;

            /* Labelled IPv4 routes */
            BGP4_RT_PREFIXLEN (pRtProfile) = (UINT2) (u1LenInBits -
                                                      (u2Bytes) *
                                                      BGP4_ONE_BYTE_BITS);
            if (BGP4_RT_PREFIXLEN (pRtProfile) > BGP4_MAX_PREFIXLEN)
            {
                BGP4_DBG1 (BGP4_DBG_ALL,
                           "\tBgp4MpeProcessIpLbldPrefix() : Prefix length is received "
                           "with Wrong value [%d]\n",
                           BGP4_RT_PREFIXLEN (pRtProfile));
                return BGP4_FAILURE;
            }
            u2Bytes = Bgp4RoundToBytes (BGP4_RT_PREFIXLEN (pRtProfile));
            BGP4_GET_PREFIX (pu1RcvdUpdateMessage, &u4Prefix, u2Bytes);
            PTR_ASSIGN4 (BGP4_RT_IP_PREFIX (pRtProfile), (u4Prefix));

            if ((Bgp4IsValidAddress
                 (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO
                  (BGP4_RT_NET_ADDRESS_INFO (pRtProfile)),
                  BGP4_TRUE)) != BGP4_TRUE)
            {
                BGP4_DBG1 (BGP4_DBG_ALL,
                           "\tBgp4MpeProcessIpLbldPrefix() : Prefix is received "
                           "with Wrong value [%s]\n",
                           Bgp4PrintIpAddr (BGP4_RT_IP_PREFIX (pRtProfile),
                                            BGP4_RT_AFI_INFO (pRtProfile)));
                return BGP4_FAILURE;
            }
            u2BytesPrcsd = (UINT2) (u2BytesPrcsd + u2Bytes);
            break;
        }
        case CAP_MP_VPN4_UNICAST:
            BGP4_RT_PREFIXLEN (pRtProfile) = (UINT2) (u1LenInBits -
                                                      ((u2Bytes +
                                                        BGP4_VPN4_ROUTE_DISTING_SIZE)
                                                       * BGP4_ONE_BYTE_BITS));
            i4Status =
                Bgp4MpeProcessVpnv4Prefix (pu1RcvdUpdateMessage, pRtProfile,
                                           &u2Bytes);
            if (i4Status == BGP4_FAILURE)
            {
                return BGP4_FAILURE;
            }
            u2BytesPrcsd = (UINT2) (u2BytesPrcsd + u2Bytes);
            break;

        default:
            return BGP4_FAILURE;
    }

    *pu2Len = u2BytesPrcsd;
    return BGP4_SUCCESS;
}

/********************************************************************/
/* Function Name   : Bgp4MpeProcessIpLabelInUpdMsg                  */
/* Description     : process the Label received with  the route     */
/* Input(s)        : pu1RcvdUpdateMessage - pointer to the UPDATE   */
/*                   message in which Label is received             */
/*                   u1LenInBits - length in bits received for      */
/*                          value that is reeived in MP_REACH_NLRI  */
/*                   pRoute - route information                     */
/* Output(s)       : pu2Len - pointer to number of bytes processed  */
/* Global Variables Referred : None                                 */
/* Global variables Modified : None                                 */
/* Exceptions or Operating System Error Handling : None             */
/* Use of Recursion          : None.                                */
/* Returns                   : BGP4_SUCCESS or BGP4_FAILURE         */
/********************************************************************/
INT4
Bgp4MpeProcessIpLabelInUpdMsg (UINT1 *pu1RcvdUpdateMessage,
                               UINT1 u1LenInBits,
                               tRouteProfile * pRtProfile,
                               UINT1 u1MpeAttrType, UINT2 *pu2Len)
{
    UINT1               u1Index = 0;
    UINT4               u4RtLabelInfo = 0;

    *pu2Len = 0;
    u1Index++;
    if ((u1Index * BGP4_VPN4_LABEL_SIZE) > u1LenInBits)
    {
        BGP4_DBG (BGP4_DBG_RX,
                  "\t<vpn4, unicast> NLRI - Wrong attribute length\n");
        return BGP4_FAILURE;
    }
    if (u1Index > BGP4_VPN4_MAX_LABELS)
    {
        BGP4_DBG (BGP4_DBG_RX,
                  "\t<vpn4, unicast> NLRI - Max Labels must be " "increased\n");
        return BGP4_FAILURE;
    }
    MEMCPY ((UINT1 *) (&(BGP4_RT_LABEL_INFO (pRtProfile))),
            (pu1RcvdUpdateMessage + ((u1Index - 1) * BGP4_VPN4_LABEL_SIZE)),
            BGP4_VPN4_LABEL_SIZE);
    /* Label is in network order, convert it into host order */
    u4RtLabelInfo = OSIX_NTOHL (BGP4_RT_LABEL_INFO (pRtProfile));

    BGP4_RT_LABEL_INFO (pRtProfile) = ((u4RtLabelInfo) >> 8);

    /* When Multiple Routes Capability (ECMP)is supported,
     * this ahs to be modified*/
    if ((u1MpeAttrType == BGP4_ATTR_MP_UNREACH_NLRI) && (u1Index == 1))
    {
        if (BGP4_RT_LABEL_INFO (pRtProfile) != BGP4_DEF_WDR_LABEL)
        {
            /*The withdrawn message should be discarded */
            *pu2Len = (UINT2) (u1Index * BGP4_VPN4_LABEL_SIZE);
            return BGP4_NOT_NEGOTIATED_FAMILY;
        }
    }
    if (BGP4_RT_LABEL_INFO (pRtProfile) & BGP4_LABEL_BOTTOM_STACK_BIT)
    {

        BGP4_RT_LABEL_INFO (pRtProfile) =
            (BGP4_RT_LABEL_INFO (pRtProfile) & (~BGP4_LABEL_BOTTOM_STACK_BIT));
        BGP4_RT_LABEL_INFO (pRtProfile) =
            (BGP4_RT_LABEL_INFO (pRtProfile) >> BGP4_FOUR_BITS);
        /* Bottom of stack reached. No more labels */
    }
    else
    {
        BGP4_RT_LABEL_INFO (pRtProfile) =
            (BGP4_RT_LABEL_INFO (pRtProfile) >> BGP4_FOUR_BITS);
    }
    if (BGP4_RT_LABEL_INFO (pRtProfile) == BGP4_DEF_WDR_LABEL)
    {
        /* Withdrawn route received with this label. */
        return BGP4_SUCCESS;
    }
    BGP4_RT_LABEL_CNT (pRtProfile) = u1Index;
    *pu2Len = (UINT2) (u1Index * BGP4_VPN4_LABEL_SIZE);
    return BGP4_SUCCESS;
}
#endif /* L3VPN */

#endif /* End of BGMPPRCS_C */
