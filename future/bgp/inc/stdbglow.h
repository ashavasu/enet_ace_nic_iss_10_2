/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdbglow.h,v 1.6 2012/12/03 10:20:49 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/
#ifndef STDBGLOW_H
#define STDBGLOW_H
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetBgpVersion ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetBgpLocalAs ARG_LIST((INT4 *));

INT1
nmhGetBgpIdentifier ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for BgpPeerTable. */
INT1
nmhValidateIndexInstanceBgpPeerTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for BgpPeerTable  */

INT1
nmhGetFirstIndexBgpPeerTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexBgpPeerTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetBgpPeerIdentifier ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBgpPeerState ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBgpPeerAdminStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBgpPeerNegotiatedVersion ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBgpPeerLocalAddr ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBgpPeerLocalPort ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBgpPeerRemotePort ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBgpPeerRemoteAs ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBgpPeerInUpdates ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBgpPeerOutUpdates ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBgpPeerInTotalMessages ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBgpPeerOutTotalMessages ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBgpPeerLastError ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetBgpPeerFsmEstablishedTransitions ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBgpPeerFsmEstablishedTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetBgpPeerConnectRetryInterval ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBgpPeerHoldTime ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBgpPeerKeepAlive ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBgpPeerHoldTimeConfigured ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBgpPeerKeepAliveConfigured ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBgpPeerMinASOriginationInterval ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBgpPeerMinRouteAdvertisementInterval ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetBgpPeerInUpdateElapsedTime ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetBgpPeerAdminStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetBgpPeerConnectRetryInterval ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetBgpPeerHoldTimeConfigured ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetBgpPeerKeepAliveConfigured ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetBgpPeerMinASOriginationInterval ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetBgpPeerMinRouteAdvertisementInterval ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2BgpPeerAdminStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2BgpPeerConnectRetryInterval ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2BgpPeerHoldTimeConfigured ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2BgpPeerKeepAliveConfigured ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2BgpPeerMinASOriginationInterval ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2BgpPeerMinRouteAdvertisementInterval ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2BgpPeerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Bgp4PathAttrTable. */
INT1
nmhValidateIndexInstanceBgp4PathAttrTable ARG_LIST((UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Bgp4PathAttrTable  */

INT1
nmhGetFirstIndexBgp4PathAttrTable ARG_LIST((UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexBgp4PathAttrTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetBgp4PathAttrOrigin ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetBgp4PathAttrASPathSegment ARG_LIST((UINT4  , INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetBgp4PathAttrNextHop ARG_LIST((UINT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetBgp4PathAttrMultiExitDisc ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetBgp4PathAttrLocalPref ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetBgp4PathAttrAtomicAggregate ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetBgp4PathAttrAggregatorAS ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetBgp4PathAttrAggregatorAddr ARG_LIST((UINT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetBgp4PathAttrCalcLocalPref ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetBgp4PathAttrBest ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetBgp4PathAttrUnknown ARG_LIST((UINT4  , INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
#endif /* STDBGLOW_H */
