/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgmpdfs.h,v 1.12 2016/12/27 12:35:52 siva Exp $
 *
 * Description:Contain the prototypes of all the functions used
 *             in the system.
 *
 *******************************************************************/

/************************************************************/
/*  FILE NAME             : bgmpdfs.h                       */
/*  PRINCIPAL AUTHOR      : FCS                             */
/*  SUBSYSTEM NAME        : Multiprotocol Extensions        */
/*  MODULE NAME           : Global variables, Constants,    */
/*                          type defintions and macros used */
/*                          in the Multiprotocol Extensions */ 
/*  LANGUAGE              : C                               */
/*  TARGET ENVIRONMENT    : ANY                             */
/*  DATE OF FIRST RELEASE :                                 */
/*  AUTHOR                :                                 */
/*  DESCRIPTION           :                                 */
/************************************************************/
/*                                                          */
/*  Change History                                          */
/*  Version               :                                 */
/*  Date(DD/MM/YYYY)      :                                 */
/*  Modified by           :                                 */
/*  Description of change :                                 */
/************************************************************/
#ifndef _BGMPDFS_H
#define _BGMPDFS_H

/********** Constants Decleration **********/
#define BGP4_MPE_ADDRESS_FAMILY_LEN               0x02
#define BGP4_MPE_SUB_ADDRESS_FAMILY_LEN           0x01
#define BGP4_MPE_NEXTHOP_FIELD_LEN                0x01
#define BGP4_MPE_NO_OF_SNPA_LEN                   0x01
#define BGP4_MPE_SINGLE_SNPA_LEN                  0x01
#define BGP4_MPE_IPV4_PREFIX_LEN                  0x01
#define BGP4_MPE_MAX_SNPA_NODES                     10 

/* LOW-LEVEL Additions */
#define BGP4_INET_MAX_AFI                       0xFFFF
#define BGP4_INET_MAX_SAFI                        0x80
#define BGP4_MPE_MP_CAP_SUPPORT_NONE                 0
#define BGP4_MPE_MP_CAP_SUPPORT_ENABLE               1
#define BGP4_MPE_MP_CAP_SUPPORT_DISABLE              2
#define BGP4_MPE_MP_CAP_RESERVED_FIELD_LEN           1
#define BGP4_MPE_MP_CAP_LEN                          4


/********** Type Definitions **********/
/* 
 * This structure is used to maintain the SNPA information
 * received in the UPDATE message
 */
typedef struct _tSnpaInfoNode {
    tTMO_SLL_NODE   TSNextSNPA; /* The next SNPA node */
    UINT1           au1SnpaInfo[16];/* SNPA information */
    UINT1           u1SnpaLength;/* Length of SNPA information */
    UINT1           au1Pad[3];   /* used for 4 byte alignment */
} tSnpaInfoNode;

/* 
 * This structure represents the global information used for 
 * Multiprotocol Extensions feature
 */
typedef struct _tBgp4MpeGlobalData {

    UINT4       u4AllocMpeAfiSafiInstanceCnt;
                /* Used to count number of memory blocks allocated from
                 * MpeRtAfiSafiInstanceMemPoolId */
} tBgp4MpeGlobalData;

/********** Macro Definitions **********/
#define BGP4_MPE_GLOBAL_DATA(u4CxtId)  (gBgpCxtNode[u4CxtId]->Bgp4MpeGlobalData)
#define BGP4_MPE_RT_SNPA_POOL_ID \
                    (gBgpNode.MpeRtSnpaNodeMemPoolId)
#define BGP4_MPE_RT_AFISAFI_INSTANCE_POOL_ID \
                    (gBgpNode.MpeRtAfiSafiInstanceMemPoolId)
#define BGP4_MPE_ALLOC_SNPA_NODE_CNT \
                    (gBgpNode.u4AllocMpeSnpaNodeCnt)
#define BGP4_MPE_ALLOC_AFISAFI_INSTANCE_CNT(u4CxtId) \
                    (BGP4_MPE_GLOBAL_DATA(u4CxtId)).u4AllocMpeAfiSafiInstanceCnt


/* Macros for counters */
#define BGP4_PEER_PREFIX_RCVD_CNT(pPeerentry, index) \
            ((pPeerentry->apAsafiInstance[index])->PrefixCounters).u4PrefixesRcvd
#define BGP4_PEER_PREFIX_SENT_CNT(pPeerentry, index) \
            ((pPeerentry->apAsafiInstance[index])->PrefixCounters).u4PrefixesSent
#define BGP4_PEER_WITHDRAWS_RCVD_CNT(pPeerentry, index) \
            ((pPeerentry->apAsafiInstance[index])->PrefixCounters).u4WithdrawsRcvd
#define BGP4_PEER_WITHDRAWS_SENT_CNT(pPeerentry, index) \
            ((pPeerentry->apAsafiInstance[index])->PrefixCounters).u4WithdrawsSent
#define BGP4_PEER_IN_PREFIXES_CNT(pPeerentry, index) \
            ((pPeerentry->apAsafiInstance[index])->PrefixCounters).u4InPrefixes
#define BGP4_PEER_IN_PREFIXES_ACPTD_CNT(pPeerentry, index) \
            ((pPeerentry->apAsafiInstance[index])->PrefixCounters).u4InPrefixesAcptd
#define BGP4_PEER_IN_PREFIXES_RJCTD_CNT(pPeerentry, index) \
            ((pPeerentry->apAsafiInstance[index])->PrefixCounters).u4InPrefixesRjctd
#define BGP4_PEER_OUT_PREFIXES_CNT(pPeerentry, index) \
            ((pPeerentry->apAsafiInstance[index])->PrefixCounters).u4OutPrefixes
#define BGP4_PEER_INVALID_MPE_UPDATE_CNT(pPeerentry, index) \
            ((pPeerentry->apAsafiInstance[index])->PrefixCounters).u4InvalidMpeAttribCntr

#define BGP4_MPE_BGP_SAFI_INFO(pBgp4Info) (pBgp4Info->u1BgpInfoSafi)
#define BGP4_MPE_INFO_NEXTHOP_LEN(pBgp4Info) \
                ((pBgp4Info->NextHopInfo).u2AddressLen)
#define BGP4_MPE_SNPA_INFO_LIST(pBgp4Info) (&(pBgp4Info->SnpaInfoList))
#define BGP4_MPE_SNPA_NODE_LEN(pSnpaInfoNode) (pSnpaInfoNode->u1SnpaLength)
#define BGP4_MPE_SNPA_NODE_INFO(pSnpaInfoNode) (pSnpaInfoNode->au1SnpaInfo)
#define BGP4_MPE_SNPA_NEXT(pSnpaInfoNode) (pSnpaInfoNode->TSNextSNPA)
#define BGP4_GET_AFISAFI_MASK(u2Afi, u2Safi, u4AfiSafiMask) (u4AfiSafiMask = (UINT4)((u2Afi << BGP4_TWO_BYTE_BITS) | u2Safi))
#define BGP4_GET_AFI_MASK(u4AfiSafiMask) ((u4AfiSafiMask & 0xffff0000) >> BGP4_TWO_BYTE_BITS)
#define BGP4_GET_SAFI_MASK(u4AfiSafiMask) ((u4AfiSafiMask & 0x0000ffff))

/********** Function Prototypes Decleration **********/
INT4    Bgp4MpeProcessAttribute (tBgp4PeerEntry *, UINT1 *, INT4, tTMO_SLL *, UINT4 *);
INT4    Bgp4MpeProcessSnpa      (tBgp4PeerEntry *, UINT1 *, tBgp4Info *,
                                 UINT2 *);
INT4    Bgp4MpeProcessNexthop   (tBgp4PeerEntry *, UINT1 *, tBgp4Info *,
                                 UINT2 *);
INT4    Bgp4MpeProcessNlri      (tBgp4PeerEntry *, UINT1 *, tBgp4Info *,
                                 UINT1, tTMO_SLL *, UINT2 *, UINT4);
INT4    Bgp4MpeProcessIpv4Prefix (tBgp4PeerEntry *, UINT1 *, tRouteProfile *,
                                  UINT2 *);
INT4    Bgp4MpeFillPathAttribute (tRouteProfile *, tBgp4Info *,
                                  tBgp4PeerEntry *);
INT4    Bgp4MpeFillSnpaInfo     (tRouteProfile *, tBgp4Info *, tBgp4PeerEntry *);
INT4    Bgp4MpeFillNexthop      (tRouteProfile *, tBgp4Info *, tBgp4PeerEntry *);
UINT2   Bgp4MpeFillNlriPrefix   (UINT1 *, tRouteProfile *);
UINT2   Bgp4MpeFillIpv4Prefix   (UINT1 *, tRouteProfile *);
INT4    Bgp4MpeProcessIpv4Nexthop (tBgp4PeerEntry *, UINT1 *, tBgp4Info *, UINT2 *);

#ifdef BGP4_IPV6_WANTED

INT4    Bgp4MpeFillIpv6Nexthop  (tRouteProfile *, tBgp4Info *, tBgp4PeerEntry *);
INT4    Bgp4MpeProcessIpv6Nexthop (tBgp4PeerEntry *, UINT1 *, tBgp4Info *, UINT2 *);
UINT2   Bgp4MpeFillIpv6Prefix   (UINT1 *, tRouteProfile *);
INT4    Bgp4MpeProcessIpv6Prefix (tBgp4PeerEntry *, UINT1 *, tRouteProfile  *, UINT2 *);
#endif

#ifdef VPLSADS_WANTED
UINT2 Bgp4AttrFillVplsPrefix(UINT1 *pu1MsgBuf, tRouteProfile * pRtProfile, tBgp4PeerEntry * pPeerEntry);

#endif
#ifdef EVPN_WANTED

INT4 Bgp4MpeFillEvpnPrefix (UINT1 *pu1MsgBuf, tRouteProfile * pRtProfile, tBgp4PeerEntry * pPeerEntry);
#endif
 
#ifdef L3VPN
INT4    Bgp4MpeFillIpLbldPrefix (UINT1 *, tRouteProfile *, tBgp4AdvtPathAttr *,
                                tBgp4PeerEntry *);
INT4    Bgp4MpeProcessIpLbldPrefix (UINT1 *, tRouteProfile *, UINT1, 
                                    UINT2 *);
INT4    Bgp4MpeProcessIpLabelInUpdMsg (UINT1  *, UINT1 ,
                                       tRouteProfile *, UINT1, UINT2  *);
#endif
INT4    Bgp4MpeInit             (UINT4);
INT4    Bgp4MpeShutdown         (VOID);
INT4    Bgp4MpeInitSnpaNode (tSnpaInfoNode *);
tSnpaInfoNode  *Bgp4MpeAllocateSnpaNode (UINT4);
INT4    Bgp4MpeReleaseSnpaNode (tSnpaInfoNode *);
UINT1  *Bgp4MpeAllocateSnpaInfo (UINT4);
INT4    Bgp4MpeReleaseSnpaInfo (UINT1 *);
INT4    Bgp4MpeInitAfiSafiInstance (tBgp4PeerEntry *, UINT4);
tAfiSafiSpecInfo   *Bgp4MpeAllocateAfiSafiInstance (tBgp4PeerEntry *, UINT4);
INT4    Bgp4MpeReleaseAfiSafiInstance (tBgp4PeerEntry *, UINT4);

/* LOW-LEVEL added */
tRouteProfile * Bgp4MpeGetRoute (UINT1 *, UINT4 , UINT1 , UINT4 , UINT1 *);
UINT1   Bgp4MpeValidateAddressType (UINT4);
UINT1   Bgp4MpeValidateSubAddressType (UINT4);

#endif /* End of BGMPDFS_H */
