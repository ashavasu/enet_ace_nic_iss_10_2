/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved    
 *
 * $Id: bgp4mac.h,v 1.70 2017/09/15 06:19:50 siva Exp $
 *
 * Description: This file contains the macro defns  
 *
 *******************************************************************/
#ifndef  BGP4MAC_H

#define  BGP4MAC_H

/* These macros needs to be replaced appropriately while porting */

/* Macros for accessing the PA Database. */
#define BGP4_RCVD_PA_DB(u4CxtId,u4Index)        (gBgpCxtNode[u4CxtId]->pBgp4RcvdPAHashTbl[u4Index])
    
#define BGP4_PA_NEXT(pBgp4RcvdPA)       (&(pBgp4RcvdPA)->NextNonIdentPA)
#define BGP4_PA_IDENT_BEST_ROUTE_LIST(pBgp4RcvdPA)\
                                        (&(pBgp4RcvdPA)->BestRouteList)
#define BGP4_PA_IDENT_BEST_ROUTE_FIRST(pBgp4RcvdPA)\
                                        (pBgp4RcvdPA->BestRouteList.pFirst)
#define BGP4_PA_IDENT_BEST_ROUTE_LAST(pBgp4RcvdPA)\
                                        (pBgp4RcvdPA->BestRouteList.pLast)
#define BGP4_PA_IDENT_BEST_ROUTE_COUNT(pBgp4RcvdPA)\
                                        (pBgp4RcvdPA->BestRouteList.u4Count)
#define BGP4_PA_IDENT_NON_BEST_ROUTE_LIST(pBgp4RcvdPA)\
                                        (&(pBgp4RcvdPA)->NonBestRouteList)
#define BGP4_PA_IDENT_NON_BEST_ROUTE_FIRST(pBgp4RcvdPA)\
                                        (pBgp4RcvdPA->NonBestRouteList.pFirst)
#define BGP4_PA_IDENT_NON_BEST_ROUTE_LAST(pBgp4RcvdPA)\
                                        (pBgp4RcvdPA->NonBestRouteList.pLast)
#define BGP4_PA_IDENT_NON_BEST_ROUTE_COUNT(pBgp4RcvdPA)\
                                        (pBgp4RcvdPA->NonBestRouteList.u4Count)
#define BGP4_PA_IDENT_ROUTE_COUNT(pBgp4RcvdPA)\
                                        (pBgp4RcvdPA->BestRouteList.u4Count + \
                                         pBgp4RcvdPA->NonBestRouteList.u4Count)
#define BGP4_PA_BGP4INFO(pBgp4RcvdPA)   (pBgp4RcvdPA->pBgp4RcvdInfo)

/* Macros for access the Advertise Hash Table structure. */
#define BGP4_ADVT_PA_DB(u4CxtId)                       (gBgpCxtNode[u4CxtId]->pBgp4AdvtHashTbl)
#define BGP4_ADVT_PA_NEXT(pBgp4AdvtPA)        (&(pBgp4AdvtPA)->NextNonIdentPA)
#define BGP4_ADVT_PA_ROUTE_LIST(pBgp4AdvtPA)\
                                        (&(pBgp4AdvtPA)->RouteList)
#define BGP4_ADVT_PA_ROUTE_FIRST(pBgp4AdvtPA)\
                                 (TMO_SLL_First (&(pBgp4AdvtPA->RouteList)))
#define BGP4_ADVT_PA_ROUTE_LAST(pBgp4AdvtPA)\
                                 (TMO_SLL_Last (&(pBgp4AdvtPA->RouteList)))
#define BGP4_ADVT_PA_ROUTE_COUNT(pBgp4AdvtPA)\
                                 (TMO_SLL_Count (&(pBgp4AdvtPA->RouteList)))
#define BGP4_ADVT_PA_BGP4INFO(pBgp4AdvtPA)    (pBgp4AdvtPA->pBgp4AdvtInfo)
#define BGP4_ADVT_PA_PEER(pBgp4AdvtPA)        (pBgp4AdvtPA->pAdvtPeer)
#define BGP4_ADVT_PA_AFI(pBgp4AdvtPA)         (pBgp4AdvtPA->u2AFI)
#define BGP4_ADVT_PA_SAFI(pBgp4AdvtPA)        (pBgp4AdvtPA->u2SAFI)
#define BGP4_ADVT_PA_SIZE(pBgp4AdvtPA)    (pBgp4AdvtPA->u2PASize)

/* if MATH_SUPPORT is OFF, then system functions are used.
 * Else functions in FSAP are used. */

#if (MATH_SUPPORT == FSAP_OFF)
#define BGP4_NAT_LOG(x)        (log ((double)x))
#define BGP4_EXP(x)            (exp ((double)x))
#define BGP4_POW(x,y)          (pow ((double)x,(double)y))
#define BGP4_CEIL(x)           (ceil ((double)x))
#else
#define BGP4_NAT_LOG(x)    (UtilLog ((double)x))
#define BGP4_EXP(x)        (UtilExp ((double)x))
#define BGP4_POW(x,y)      (UtilPow ((double)x, (double)y))
#define BGP4_CEIL(x)       (UtilCeil ((double)x))
#endif
#define BGP4_PEER_OCTET_LIST_LEN          16
#define BGP4_PEER_ASPATH_LIST_LEN         68
#define BGP4_EXT_COM_OCTET_LIST_LEN        8
#define BGP4_FIB_UPD_HOLD_TIME(u4CxtId)            (gBgpCxtNode[u4CxtId]->u1FIBUpdHoldTime)
#define BGP4_REDIST_LIST(u4CxtId)                  (&(gBgpCxtNode[u4CxtId]->BGP4RedistList))
#define BGP4_NETWORK_ROUTE_DATABASE(u4CxtId)           (gBgpCxtNode[u4CxtId]->BGP4NetworkRtList)
#define BGP4_FIB_UPD_LIST(u4CxtId)                 (&(gBgpCxtNode[u4CxtId]->BGP4FIBUpdateList))
#define BGP4_DISP_TIME(u4CxtId)                     gBgpCxtNode[u4CxtId]->au4DispatcherTime
#define BGP4_DLL_FIRST_ROUTE(pList)       (pList->pFirst)
#define BGP4_DLL_LAST_ROUTE(pList)        (pList->pLast)
#define BGP4_DLL_COUNT(pList)             (pList->u4Count)
#define BGP4_CHG_LIST_FIRST_ROUTE(u4CxtId)         (gBgpCxtNode[u4CxtId]->BGP4ChgList.pFirst)
#define BGP4_CHG_LIST_LAST_ROUTE(u4CxtId)          (gBgpCxtNode[u4CxtId]->BGP4ChgList.pLast)
#define BGP4_CHG_LIST_COUNT(u4CxtId)               (gBgpCxtNode[u4CxtId]->BGP4ChgList.u4Count)
#define BGP4_FIB_LIST_FIRST_ROUTE(u4CxtId)         (gBgpCxtNode[u4CxtId]->BGP4FIBUpdateList.pFirst)
#define BGP4_FIB_LIST_LAST_ROUTE(u4CxtId)          (gBgpCxtNode[u4CxtId]->BGP4FIBUpdateList.pLast)
#define BGP4_FIB_LIST_COUNT(u4CxtId)               (gBgpCxtNode[u4CxtId]->BGP4FIBUpdateList.u4Count)
#define BGP4_INT_PEERS_ADVT_LIST(u4CxtId)          (&(gBgpCxtNode[u4CxtId]->BGP4IntPeerAdvtList))
#define BGP4_INT_PEER_LIST_FIRST_ROUTE(u4CxtId)    (gBgpCxtNode[u4CxtId]->BGP4IntPeerAdvtList.pFirst)
#define BGP4_INT_PEER_LIST_LAST_ROUTE(u4CxtId)     (gBgpCxtNode[u4CxtId]->BGP4IntPeerAdvtList.pLast)
#define BGP4_INT_PEER_LIST_COUNT(u4CxtId)          (gBgpCxtNode[u4CxtId]->BGP4IntPeerAdvtList.u4Count)
#define BGP4_EXT_PEERS_ADVT_LIST(u4CxtId)          (&(gBgpCxtNode[u4CxtId]->BGP4ExtPeerAdvtList))
#define BGP4_EXT_PEER_LIST_FIRST_ROUTE(u4CxtId)    (gBgpCxtNode[u4CxtId]->BGP4ExtPeerAdvtList.pFirst)
#define BGP4_EXT_PEER_LIST_LAST_ROUTE(u4CxtId)     (gBgpCxtNode[u4CxtId]->BGP4ExtPeerAdvtList.pLast)
#define BGP4_EXT_PEER_LIST_COUNT(u4CxtId)          (gBgpCxtNode[u4CxtId]->BGP4ExtPeerAdvtList.u4Count)
#define BGP4_OTHER_PROTO_REGISTER_MASK(u4CxtId)    (gBgpCxtNode[u4CxtId]->u4Registered)
#define BGP4_IPV4_RIB_TREE(u4CxtId)                \
                        (gBgpCxtNode[u4CxtId]->gapBgp4Rib[BGP4_IPV4_UNI_INDEX])
#define BGP4_SYNC_INIT_ROUTE_NETADDR_INFO(u4CxtId) (gBgpCxtNode[u4CxtId]->BGP4SyncInitRouteInfo)
#define BGP4_SYNC_INIT_ROUTE_PREFIX_INFO(u4CxtId)  (gBgpCxtNode[u4CxtId]->BGP4SyncInitRouteInfo.NetAddr)
#define BGP4_SYNC_INIT_ROUTE_PREFIX_LEN(u4CxtId) \
        (gBgpCxtNode[u4CxtId]->BGP4SyncInitRouteInfo.u2PrefixLen)
#define BGP4_SYNC_INTERVAL(u4CxtId)                (gBgpCxtNode[u4CxtId]->u4BGP4SyncInterval)
#define BGP4_NH_CHG_INIT_ROUTE_NETADDR_INFO(u4CxtId) \
            (gBgpCxtNode[u4CxtId]->BGP4NHChgInitInfo.NHInitRoute)
#define BGP4_NH_CHG_INIT_ROUTE_PREFIX_INFO(u4CxtId)  \
            (gBgpCxtNode[u4CxtId]->BGP4NHChgInitInfo.NHInitRoute.NetAddr)
#define BGP4_NH_CHG_INIT_ROUTE_PREFIX_LEN(u4CxtId)   \
            (gBgpCxtNode[u4CxtId]->BGP4NHChgInitInfo.NHInitRoute.u2PrefixLen)
#define BGP4_NH_CHG_INIT_NEXTHOP_PEER_INFO(u4CxtId)       \
            (gBgpCxtNode[u4CxtId]->BGP4NHChgInitInfo.NHInitPeerAddr)
#define BGP4_NH_CHG_INIT_NEXTHOP_INFO(u4CxtId)       \
            (gBgpCxtNode[u4CxtId]->BGP4NHChgInitInfo.NHInitNextHop)
#define BGP4_NH_CHG_INIT_VRFID(u4CxtId)  (gBgpCxtNode[u4CxtId]->BGP4NHChgInitInfo.u4VrfId)
#define BGP4_NH_CHG_INTERVAL(u4CxtId)               (gBgpCxtNode[u4CxtId]->u4BGP4NHChgInterval)
#define BGP4_NH_CHG_PRCS_INTERVAL(u4CxtId)          (gBgpCxtNode[u4CxtId]->u4BGP4NHChgPrcsInterval)
#define BGP4_DEF_ROUTE_ORIG_STATUS(u4CxtId)         (gBgpCxtNode[u4CxtId]->u1BGP4DftRouteOrigStatus)
#define BGP4_DEF_ROUTE_ORIG_NEW_STATUS(u4CxtId)     (gBgpCxtNode[u4CxtId]->u1BGP4DftRouteOrigNewStatus)

#define BGP4_PEER_NEXT(pPeerentry)        (&(pPeerentry->TsnNextpeer))
#define BGP4_PEER_ASNO(pPeerentry)        (pPeerentry->peerConfig.u4ASNo)
#define BGP4_PEER_ADMIN_STATUS(pPeerentry) \
                                    (pPeerentry->peerConfig.u1AdminStatus)
#define BGP4_PEER_ALLOW_AUTOMATIC_START(pPeerentry) (pPeerentry->peerConfig.u1AutomaticStart)
#define BGP4_PEER_HOLD_ADVT_ROUTES(pPeerentry) (pPeerentry->peerConfig.u1HoldAdvtRoutes)
#define BGP4_PEER_ALLOW_AUTOMATIC_STOP(pPeerentry) (pPeerentry->peerConfig.u1AutomaticStop)
#define BGP4_PEER_IDLE_HOLD_TIME(pPeerentry) (pPeerentry->peerConfig.u4IdleHoldTimeInterval)
#define BGP4_PEER_CURRENT_IDLE_HOLD_TIME(pPeerentry) (pPeerentry->peerConfig.u4IdleHoldIntvlActual)
#define BGP4_DAMP_PEER_OSCILLATIONS(pPeerentry) (pPeerentry->peerConfig.u1DampPeerOscillationStatus)
#define BGP4_PEER_DELAY_OPEN(pPeerentry) (pPeerentry->peerConfig.u1DelayOpenStatus)
#define BGP4_PEER_DELAY_OPEN_TIME(pPeerentry) (pPeerentry->peerConfig.u4DelayOpenTimeInterval)
#define BGP4_PEER_PREFIX_UPPER_LIMIT(pPeerentry) (pPeerentry->peerConfig.u4PeerPrefixUpperLimit)
#define BGP4_PEER_CURRENT_PREFIX_LIMIT(pPeerentry) (pPeerentry->peerStatus.u4LocalPeerPrefixLimit)
#define BGP4_PEER_TCP_CONFIGURED_CONNECT_RETRY_COUNT(pPeerentry) (pPeerentry->peerConfig.u1ConnectRetryCount)
#define BGP4_PEER_TCP_CONFIGURED_CONNECT_COUNT(pPeerentry) (pPeerentry->peerConfig.u1ConnectCount)
#define BGP4_PEER_TCP_DAMP_CONNECT_RETRY_COUNT(pPeerentry) (pPeerentry->peerStatus.u1LocalConnectRetryCount)
#define BGP4_PEER_DAMPED_STATUS(pPeerentry) (pPeerentry->peerStatus.u1IsPeerDamped) 
#define BGP4_PEER_LOCAL_AS(pPeerentry) (pPeerentry->peerConfig.u4LocalASNo)
#define BGP4_PEER_LOCAL_AS_CFG(pPeerentry) (pPeerentry->peerConfig.u1VrfConfig)

#define BGP4_PEER_CONN_RETRY_TIME(pPeerentry) (pPeerentry->peerConfig.u4ConnectRetryInterval)
#define BGP4_PEER_HOLD_TIME(pPeerentry) (pPeerentry->peerConfig.u4HoldInterval)
#define BGP4_PEER_KEEP_ALIVE_TIME(pPeerentry) (pPeerentry->peerConfig.u4KeepAliveInterval)
#define BGP4_PEER_MIN_AS_ORIG_TIME(pPeerentry) (pPeerentry->peerConfig.u4MinASOrigInterval)
#define BGP4_PEER_MIN_ADV_TIME(pPeerentry) (pPeerentry->peerConfig.u4MinRouteAdvInterval)
#define BGP4_PEER_AUTH_TYPE(pPeerentry) (pPeerentry->peerConfig.u1AuthType)
#define BGP4_PEER_EBGP_MULTIHOP(pPeerentry) (pPeerentry->peerConfig.u1EBGPMultiHop)
#define BGP4_PEER_SELF_NEXTHOP(pPeerentry) (pPeerentry->peerConfig.u1SelfNextHop)
#define BGP4_PEER_RFL_CLIENT(pPeerentry) (pPeerentry->peerConfig.u1RflClient)
#define BGP4_PEER_COMM_SEND_STATUS(pPeerentry) \
                (pPeerentry->peerConfig.u1CommSendStatus)
#define BGP4_PEER_ECOMM_SEND_STATUS(pPeerentry) \
                (pPeerentry->peerConfig.u1ECommSendStatus)
#define BGP4_PEER_DEF_ROUTE_ORIG_STATUS(pPeerentry)\
                (pPeerentry->peerConfig.u1DftRouteOrigStatus)
#define BGP4_PEER_OVERRIDE_CAPABILITY(pPeerentry) \
                (pPeerentry->peerConfig.u1OverrideCap)

#ifdef L3VPN
#define BGP4_VPN4_PEER_VRFID(pPeerentry) (pPeerentry->peerStatistics.pVrfInfo->u4VrfId)
#define BGP4_VPN4_PEER_ROLE(pPeerentry) (pPeerentry->peerConfig.u1PeerRole)
#define BGP4_VPN4_PEER_CERT_STATUS(pPeerentry)  \
                        (pPeerentry->peerConfig.u1CERTAdvtStatus)
#define BGP4_VPNV4_LABEL(u4CxtId)            (gBgpCxtNode[u4CxtId]->u4Label)
#endif

#define BGP4_GET_PROCESS_VRFID(pRoute, u4VrfId)\
 if (gBgpNode.u1Bgp4TaskInit == BGP4_FALSE)\
 {\
  u4VrfId = (UINT1) BGP4_INVALID_VRF_ID;\
 }\
 else\
 {\
  u4VrfId = BGP4_RT_CXT_ID(pRoute);\
 }
#define BGP4_PEER_DIRECTLY_CONNECTED(pPeerentry)\
                ((pPeerentry)->peerStatus.u1DirectlyConnected)
#define BGP4_PEER_IS_IN_TRANX_LIST(pPeerentry)\
                ((pPeerentry)->peerStatus.u1IsPeerInTxList)
#define BGP4_GET_PEER_CURRENT_STATE(pPeerentry)\
                (pPeerentry->peerStatus.u4PeerCurrentState)
#define BGP4_SET_PEER_CURRENT_STATE(pPeerentry,u4Flag)\
    (pPeerentry->peerStatus.u4PeerCurrentState = u4Flag)
#define BGP4_RESET_PEER_CURRENT_STATE(pPeerentry)\
                (pPeerentry->peerStatus.u4PeerCurrentState = 0)
#define BGP4_GET_PEER_PEND_FLAG(pPeerentry)\
                (pPeerentry->peerStatus.u4PeerPendState)
#define BGP4_SET_PEER_PEND_FLAG(pPeerentry,u4Flag)\
                (pPeerentry->peerStatus.u4PeerPendState |= u4Flag)
#define BGP4_RESET_PEER_PEND_FLAG(pPeerentry,u4Flag)\
                (pPeerentry->peerStatus.u4PeerPendState &= (UINT4)( ~u4Flag))
#define BGP4_GLOBAL_STATE(u4CxtId) (gBgpCxtNode[u4CxtId]->u4BGP4GlobalState) 
#define BGP4_GET_GLOBAL_FLAG(u4CxtId) (gBgpCxtNode[u4CxtId]->u4BGP4GlobalFlag)
#define BGP4_SET_GLOBAL_FLAG(u4CxtId,u4Flag) ((gBgpCxtNode[u4CxtId]->u4BGP4GlobalFlag) |= u4Flag)
#define BGP4_RESET_GLOBAL_FLAG(u4CxtId,u4Flag) \
            ((gBgpCxtNode[u4CxtId]->u4BGP4GlobalFlag) &= (UINT4)(~u4Flag))

#define BGP4_PEER_STATE(pPeerentry) (pPeerentry->peerStatus.u1PeerState)
#define BGP4_PEER_PREV_STATE(pPeerentry)\
                        (pPeerentry->peerStatus.u1PeerPrevState)
#define BGP4_PEER_IN_UPDATES(pPeerentry)\
                        (pPeerentry->peerStatistics.u4InUpdates)
#define BGP4_PEER_OUT_UPDATES(pPeerentry)\
                        (pPeerentry->peerStatistics.u4OutUpdates)
#define BGP4_PEER_IN_MSGS(pPeerentry)\
                        (pPeerentry->peerStatistics.u4InMessages)
#define BGP4_PEER_OUT_MSGS(pPeerentry)\
                        (pPeerentry->peerStatistics.u4OutMessages)
#define BGP4_PEER_LAST_ERROR(pPeerentry)\
                        (pPeerentry->peerStatistics.u1LastErrorCode)
#define BGP4_PEER_LAST_ERROR_SUB_CODE(pPeerentry)\
                        (pPeerentry->peerStatistics.u1LastErrorSubCode)
#define BGP4_PEER_IF_INDEX(pPeerentry)      \
                        (pPeerentry->peerStatistics.u2IfIndex)
#define BGP4_PEER_FSM_TRANS(pPeerentry)\
                        (pPeerentry->peerStatistics.u4FSMTransitions)
#define BGP4_PEER_ESTAB_TIME(pPeerentry)\
                        (pPeerentry->peerStatistics.u4EstablishedTimer)
#define BGP4_PEER_IN_UPDATE_ELAP_TIME(pPeerentry)\
                        (pPeerentry->peerStatistics.u4InUpdateElapTime)
#define BGP4_PEER_START_TIME(pPeerentry)\
                        (pPeerentry->peerLocal.u4StartInterval)
#define BGP4_PEER_BGP_ID(pPeerentry)\
                        (pPeerentry->peerLocal.u4PeerBGPId)
#define BGP4_PEER_LOCAL_PORT(pPeerentry)\
                        (pPeerentry->peerLocal.u4LocalPortNo)
#define BGP4_PEER_REMOTE_PORT(pPeerentry)\
                        (pPeerentry->peerLocal.u4RemotePortNo)
#define BGP4_PEER_CONN_ID(pPeerentry) (pPeerentry->peerLocal.i4TCPConnId)
#define BGP4_PEER_NEG_VER(pPeerentry)\
                        ((pPeerentry->peerLocal).u1NegotiatedVersion)
#define BGP4_PEER_NEG_HOLD_INT(pPeerentry)\
                        (pPeerentry->peerLocal.u4NegHoldInterval)
#define BGP4_PEER_NEG_KEEP_ALIVE(pPeerentry)\
                        (pPeerentry->peerLocal.u4NegKeepAliveInterval)

#define BGP4_PEER_NEG_CAP_MASK(pPeerentry) (pPeerentry->u4NegCapMask)
#define BGP4_PEER_NEG_ASAFI_MASK(pPeerentry) (pPeerentry->u4NegAsafiMask)
#define BGP4_PEER_SUP_ASAFI_MASK(pPeerentry) (pPeerentry->u4SupAsafiMask)

#define BGP4_PEER_AFI_SAFI_INSTANCE(pPeerentry, u4Index) \
            (pPeerentry->apAsafiInstance[u4Index])
#define BGP4_PEER_RTREF_DATA_PTR(pPeerentry, u4Index) \
            ((pPeerentry->apAsafiInstance[u4Index])->pRtRefreshData)
#define BGP4_PEER_TCPMD5_PTR(pPeerentry) (pPeerentry->pTcpMd5AuthPasswd)
#define BGP4_PEER_TCPMD5_PASSWD(pPeerentry)\
                        (pPeerentry->pTcpMd5AuthPasswd->au1TcpMd5Passwd)
#define BGP4_PEER_TCPMD5_PASSWD_LEN(pPeerentry)\
                        (pPeerentry->pTcpMd5AuthPasswd->u1TcpMd5PasswdLength)
#define BGP4_PEER_TCPMD5_PASSWD_STATUS(pPeerentry)\
                        (pPeerentry->pTcpMd5AuthPasswd->u1PwdStatus)

#define BGP4_PEER_TCPAO_ICMP_ACCEPT_STATUS(pPeerEntry)\
   (pPeerEntry->u1TcpAoAuthIcmpAcc)

#define BGP4_PEER_TCPAO_NOMKT_PKTDISC_STATUS(pPeerEntry) \
   (pPeerEntry->u1TcpAoMktDiscard)

#define BGP4_PEER_GET_TCPAO_ASSOCIATED_MKT_ID(pPeerEntry) \
   (((pPeerEntry->pTcpAOAuthMKT == NULL) ? -1 : pPeerEntry->pTcpAOAuthMKT->u1SendKeyId))

#define BGP4_PEER_GET_TCPAO_ASSOCIATED_MKT_STATUS(pPeerEntry) \
            (((pPeerEntry->pTcpAOAuthMKT == NULL) ? 2 : 1)) /*2 Clear 1 Set*/

#define BGP4_PEER_TCPAO_MKT(pPeerEntry) (pPeerEntry->pTcpAOAuthMKT)
#define BGP4_PEER_TCPAO_ICMPACCEPT(pPeerEntry) (pPeerEntry->u1TcpAoAuthIcmpAcc) 
#define BGP4_PEER_TCPAO_NOMKTDISC(pPeerEntry)  (pPeerEntry->u1TcpAoMktDiscard)
#define BGP4_PEER_GATEWAY_ADDR_INFO(pPeerentry) (pPeerentry->peerConfig.GatewayAddrInfo)
#define BGP4_PEER_GATEWAY_ADDR(pPeerentry) \
                ((pPeerentry->peerConfig.GatewayAddrInfo).au1Address)
#define BGP4_PEER_CONN_PASSIVE(pPeerentry) (pPeerentry->peerConfig.u1Passive)
#define BGP4_PEER_HOPLIMIT(pPeerentry)  (pPeerentry->peerConfig.u1HopLimit)
#define BGP4_PEER_SENDBUF(pPeerentry)   (pPeerentry->peerConfig.u4SendBuf)
#define BGP4_PEER_RECVBUF(pPeerentry)   (pPeerentry->peerConfig.u4RecvBuf)

#define BGP4_PEER_IPV4_AFISAFI_INSTANCE(pPeerentry) \
                    (pPeerentry->apAsafiInstance[BGP4_IPV4_UNI_INDEX])
#ifdef L3VPN
/* Carrying Label Informatin - RFC 3107 */
#define BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE(pPeerentry) \
                    (pPeerentry->apAsafiInstance[BGP4_IPV4_LBLD_INDEX])
#endif

/* Graceful restart related macros */

#define BGP4_PEER_RESTART_MODE(pPeerEntry)  (pPeerEntry->peerStatus.u1RestartMode)
#define BGP4_PEER_RESTART_INTERVAL(pPeerEntry) (pPeerEntry->u4RestartInterval)
#define BGP4_PEER_RECVD_EOR_MARKER(pBgp4PeerEntry) \
    (pBgp4PeerEntry->peerStatus.u1EndOfRIBMarkerReceived)
#define BGP4_EOR_RCVD(pBgp4PeerEntry) \
    (pBgp4PeerEntry->peerStatus.u1EORRcvdForSupportedCapabilities)
#define BGP4_PEER_EOR_MARKER(pBgp4PeerEntry) \
    (pBgp4PeerEntry->peerStatus.u1EORForPeer)
#define BGP4_PEER_SENT_EOR_MARKER(pBgp4PeerEntry) \
 (pBgp4PeerEntry->peerStatus.u1EndOfRIBMarkerSent)
/* Peer related ipv4 lists macros */
#define BGP4_PEER_IPV4_OUTPUT_ROUTES(pPeerentry) \
        (BGP4_PEER_IPV4_AFISAFI_INSTANCE(pPeerentry)->pOutputList)
#define BGP4_PEER_IPV4_NEW_ROUTES(pPeerentry) \
        (BGP4_PEER_IPV4_AFISAFI_INSTANCE(pPeerentry)->pNewList)
#define BGP4_PEER_IPV4_NEW_LOCAL_ROUTES(pPeerentry) \
        (BGP4_PEER_IPV4_AFISAFI_INSTANCE(pPeerentry)->pNewLocalList)
#define BGP4_PEER_IPV4_ADVT_WITH_ROUTES(pPeerentry) \
        (BGP4_PEER_IPV4_AFISAFI_INSTANCE(pPeerentry)->pAdvtWithList)
#define BGP4_PEER_IPV4_ADVT_FEAS_ROUTES(pPeerentry) \
        (BGP4_PEER_IPV4_AFISAFI_INSTANCE(pPeerentry)->pAdvtFeasList)
#define BGP4_PEER_IPV4_DEINIT_RT_LIST(pPeerentry) \
        (&(BGP4_PEER_IPV4_AFISAFI_INSTANCE(pPeerentry)->TSDeInitRtList))
#ifdef L3VPN
/* Carrying Label Informatin - RFC 3107 */
#define BGP4_PEER_IPV4_LBLD_OUTPUT_ROUTES(pPeerentry) \
        (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE(pPeerentry)->pOutputList)
#define BGP4_PEER_IPV4_LBLD_NEW_ROUTES(pPeerentry) \
        (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE(pPeerentry)->pNewList)
#define BGP4_PEER_IPV4_LBLD_NEW_LOCAL_ROUTES(pPeerentry) \
        (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE(pPeerentry)->pNewLocalList)
#define BGP4_PEER_IPV4_LBLD_ADVT_WITH_ROUTES(pPeerentry) \
        (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE(pPeerentry)->pAdvtWithList)
#define BGP4_PEER_IPV4_LBLD_ADVT_FEAS_ROUTES(pPeerentry) \
        (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE(pPeerentry)->pAdvtFeasList)
#define BGP4_PEER_IPV4_LBLD_DEINIT_RT_LIST(pPeerentry) \
        (&(BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE(pPeerentry)->TSDeInitRtList))
#endif

/* Counters */
#define BGP4_PEER_IPV4_PREFIX_RCVD_CNT(pPeerentry) \
   (BGP4_PEER_IPV4_AFISAFI_INSTANCE(pPeerentry)->PrefixCounters).u4PrefixesRcvd
#define BGP4_PEER_IPV4_PREFIX_SENT_CNT(pPeerentry) \
   (BGP4_PEER_IPV4_AFISAFI_INSTANCE(pPeerentry)->PrefixCounters).u4PrefixesSent
#define BGP4_PEER_IPV4_WITHDRAWS_RCVD_CNT(pPeerentry) \
  (BGP4_PEER_IPV4_AFISAFI_INSTANCE(pPeerentry)->PrefixCounters).u4WithdrawsRcvd
#define BGP4_PEER_IPV4_WITHDRAWS_SENT_CNT(pPeerentry) \
  (BGP4_PEER_IPV4_AFISAFI_INSTANCE(pPeerentry)->PrefixCounters).u4WithdrawsSent


#define BGP4_PEER_IPV4_IN_PREFIXES_CNT(pPeerentry) \
  (BGP4_PEER_IPV4_AFISAFI_INSTANCE(pPeerentry)->PrefixCounters).u4InPrefixes
#define BGP4_PEER_IPV4_IN_PREFIXES_ACPTD_CNT(pPeerentry) \
(BGP4_PEER_IPV4_AFISAFI_INSTANCE(pPeerentry)->PrefixCounters).u4InPrefixesAcptd
#define BGP4_PEER_IPV4_IN_PREFIXES_RJCTD_CNT(pPeerentry) \
(BGP4_PEER_IPV4_AFISAFI_INSTANCE(pPeerentry)->PrefixCounters).u4InPrefixesRjctd
#define BGP4_PEER_IPV4_OUT_PREFIXES_CNT(pPeerentry) \
    (BGP4_PEER_IPV4_AFISAFI_INSTANCE(pPeerentry)->PrefixCounters).u4OutPrefixes
#define BGP4_PEER_IPV4_INVALID_MPE_UPDATE_CNT(pPeerentry) \
   (BGP4_PEER_IPV4_AFISAFI_INSTANCE(pPeerentry)->PrefixCounters).u4InvalidMpeAttribCntr
#ifdef L3VPN
/* Carrying Label Informatin - RFC 3107 */
#define BGP4_PEER_IPV4_LBLD_PREFIX_RCVD_CNT(pPeerentry) \
   (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE(pPeerentry)->PrefixCounters).u4PrefixesRcvd
#define BGP4_PEER_IPV4_LBLD_PREFIX_SENT_CNT(pPeerentry) \
   (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE(pPeerentry)->PrefixCounters).u4PrefixesSent
#define BGP4_PEER_IPV4_LBLD_WITHDRAWS_RCVD_CNT(pPeerentry) \
  (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE(pPeerentry)->PrefixCounters).u4WithdrawsRcvd
#define BGP4_PEER_IPV4_LBLD_WITHDRAWS_SENT_CNT(pPeerentry) \
  (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE(pPeerentry)->PrefixCounters).u4WithdrawsSent
#define BGP4_PEER_IPV4_LBLD_IN_PREFIXES_CNT(pPeerentry) \
  (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE(pPeerentry)->PrefixCounters).u4InPrefixes
#define BGP4_PEER_IPV4_LBLD_IN_PREFIXES_ACPTD_CNT(pPeerentry) \
(BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE(pPeerentry)->PrefixCounters).u4InPrefixesAcptd
#define BGP4_PEER_IPV4_LBLD_IN_PREFIXES_RJCTD_CNT(pPeerentry) \
(BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE(pPeerentry)->PrefixCounters).u4InPrefixesRjctd
#define BGP4_PEER_IPV4_LBLD_OUT_PREFIXES_CNT(pPeerentry) \
    (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE(pPeerentry)->PrefixCounters).u4OutPrefixes
#define BGP4_PEER_IPV4_LBLD_INVALID_MPE_UPDATE_CNT(pPeerentry) \
   (BGP4_PEER_IPV4_LBLD_AFISAFI_INSTANCE(pPeerentry)->PrefixCounters).u4InvalidMpeAttribCntr
#endif

#define BGP4_PEER_ROUTE_LIST(pPeerentry,u4AFIndex)\
                   (&(pPeerentry->apAsafiInstance[u4AFIndex]->TSPeerRouteList))
#define BGP4_PEER_RCVD_ROUTES(pPeerentry) (&(pPeerentry->TSRcvdRouteList))
#define BGP4_PEER_SENT_ROUTES(pPeerentry) (pPeerentry->AdvRoutes)
#define BGP4_PEER_ADVT_MSG_LIST(pPeerentry) (&(pPeerentry->TSAdvtBufList))

#define BGP4_PEER_READVT_BUFFER(pPeerentry) (pPeerentry->pu1ReadvtBuffer)
#define BGP4_PEER_READVT_BUF_SIZE(pPeerentry)\
                    (((tBufNode *)(pPeerentry->pu1ReadvtBuffer))->u4MsgSize)
#define BGP4_PEER_MSG_IN_ADVT_BUF_NODE(pNode) (pNode->pu1Msg)
#define BGP4_PEER_MSG_SIZE_ADVT_BUF_NODE(pNode) (pNode->u4MsgSize)
#define BGP4_PEER_MSG_OFFSET_ADVT_BUF_NODE(pNode) (pNode->u4MsgOffset)

#define BGP4_PEER_TMR_ENTRY (pPeerentry) (pPeerentry->peerLocal.pTmr)
#define BGP4_PEER_TMR_DATA (pPeerentry) ((pPeerentry->peerLocal.pTmr)->u4Data)
#define BGP4_PEER_ENTRY_INDEX(pPeerentry) (pPeerentry->peerStatus.u2PeerIndex)
#define BGP4_PEER_RESIDUAL_BUF(pPeerentry) (pPeerentry->ResidualBuf)
#define BGP4_PEER_RESIDUAL_BUF_DATA(pPeerentry)\
                                          (pPeerentry->ResidualBuf.pu1Msg)
#define BGP4_PEER_RESIDUAL_CUR_MSG_LEN(pPeerentry)\
                                          (pPeerentry->ResidualBuf.u4MsgSize)
#define BGP4_PEER_RESIDUAL_BUF_OFFSET(pPeerentry)\
                                          (pPeerentry->ResidualBuf.u4MsgOffset)
        
/* Added for Multiprotocol Extesions support */
#define  BGP4_PEER_REMOTE_ADDR_INFO(pPeerentry)\
                ((pPeerentry->peerConfig).RemoteAddrInfo)
#define  BGP4_PEER_REMOTE_ADDR(pPeerentry)\
                (((pPeerentry->peerConfig).RemoteAddrInfo).au1Address)
#define  BGP4_PEER_REMOTE_ADDR_TYPE(pPeerentry)\
                (((pPeerentry->peerConfig).RemoteAddrInfo).u2Afi)
#define  BGP4_PEER_LOCAL_NETADDR_INFO(pPeerentry)\
                (((pPeerentry)->peerConfig).LocalAddrInfo)
#define  BGP4_PEER_LOCAL_NETADDR_PREFIXLEN(pPeerentry)\
                (((pPeerentry)->peerConfig).LocalAddrInfo.u2PrefixLen)
#define  BGP4_PEER_LOCAL_ADDR_INFO(pPeerentry)\
                (((pPeerentry)->peerConfig).LocalAddrInfo.NetAddr)
#define  BGP4_PEER_LOCAL_ADDR(pPeerentry)\
                (((pPeerentry->peerConfig).LocalAddrInfo.NetAddr).au1Address)
#define  BGP4_PEER_LOCAL_ADDR_CONFIGURED(pPeerentry)\
                (pPeerentry->peerConfig.u1LocalAddrConfigured)
#define  BGP4_PEER_LINK_LOCAL_ADDR_INFO(pPeerInfo) \
                ((pPeerInfo->peerConfig).LinkLocalAddrInfo)
#define  BGP4_PEER_LINK_LOCAL_ADDR(pPeerInfo) \
                (((pPeerInfo->peerConfig).LinkLocalAddrInfo).au1Address)
#define  BGP4_PEER_NETWORK_ADDR_INFO(pPeerentry)\
                (((pPeerentry)->peerConfig).NetworkAddrInfo)
#define  BGP4_PEER_NETWORK_ADDR(pPeerentry)\
                (((pPeerentry->peerConfig).NetworkAddrInfo).au1Address)
#define  BGP4_PEER_LCL_NETADDR_INFO(pPeerentry)\
                (((pPeerentry)->peerConfig).LclAddrInfo)
#define  BGP4_PEER_LCL_NETADDR_PREFIXLEN(pPeerentry)\
                (((pPeerentry)->peerConfig).LclAddrInfo.u2PrefixLen)
#define  BGP4_PEER_LCLADDR_INFO(pPeerentry)\
                (((pPeerentry)->peerConfig).LclAddrInfo.NetAddr)
#define  BGP4_PEER_LCL_ADDR(pPeerentry)\
                (((pPeerentry->peerConfig).LclAddrInfo.NetAddr).au1Address)

#define  BGP4_INET_ADDRESS_IN_ADDR_PREFIX_INFO(AddrPrefix)\
                (AddrPrefix.au1Address)
#define  BGP4_AFI_IN_ADDR_PREFIX_INFO(AddrPrefix)\
                (AddrPrefix.u2Afi)
#define  BGP4_ADDR_LEN_IN_ADDR_PREFIX_INFO(AddrPrefix)\
                (AddrPrefix.u2AddressLen)
#define  BGP4_PREFIXLEN_IN_NET_ADDRESS_INFO(NetAddress)\
                (NetAddress.u2PrefixLen)
#define  BGP4_SAFI_IN_NET_ADDRESS_INFO(NetAddress)\
                (NetAddress.u2Safi)
#define  BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO(NetAddress)\
                (NetAddress.NetAddr)
#define  BGP4_INFO_NEXTHOP(pBgpInfo)\
                ((pBgpInfo->NextHopInfo).au1Address)
#define  BGP4_INFO_AFI(pBgpInfo)\
                    ((pBgpInfo->NextHopInfo).u2Afi)
#define  BGP4_INFO_ADDR_LEN(pBgpInfo)\
                    ((pBgpInfo->NextHopInfo).u2AddressLen)
#define  BGP4_INFO_NEXTHOP_INFO(pBgpInfo)\
                (pBgpInfo->NextHopInfo)
#ifdef BGP4_IPV6_WANTED 
#define  BGP4_INFO_LINK_LOCAL_ADDR_INFO(pBgpInfo)\
                (pBgpInfo->LinkLocalAddrInfo)
#define  BGP4_INFO_LINK_LOCAL_ADDR(pBgpInfo)\
                ((pBgpInfo->LinkLocalAddrInfo).au1Address)
#define  BGP4_INFO_LINK_LOCAL_PRESENT(pBgpInfo)\
                (pBgpInfo->u1LinkLocalPresent)
#endif
#define  BGP4_RT_NET_ADDRESS_INFO(pRtProfile)\
                (pRtProfile->NetAddress)
#define  BGP4_RT_AFI_INFO(pRtProfile)\
                (((pRtProfile->NetAddress).NetAddr).u2Afi)
#define  BGP4_RT_SAFI_INFO(pRtProfile) \
                ((pRtProfile->NetAddress).u2Safi)
#define  BGP4_RT_PREFIXLEN(pRtProfile)  ((pRtProfile->NetAddress).u2PrefixLen)
#define  BGP4_RT_IP_PREFIX(pRtProfile)\
                (((pRtProfile->NetAddress).NetAddr).au1Address)
#define  BGP4_RT_IP_ADDRLEN(pRtProfile)\
                (((pRtProfile->NetAddress).NetAddr).u2AddressLen)

#define BGP4_IFACE_ENTRY_VRFID(pIfInfo) (pIfInfo->u4VrfId)
#ifdef L3VPN
#define  BGP4_RT_ROUTE_DISTING(pRtProfile)  (pRtProfile->au1RouteDisting)
#define  BGP4_RT_IGP_LSP_ID(pRtProfile)   (pRtProfile->u4IgpLabelId)
#define  BGP4_RT_LABEL_CNT(pRtProfile)    (pRtProfile->u1LabelCnt)
#define  BGP4_RT_HWSTATUS(pRtProfile)     (pRtProfile->u1HwStatus)
#define  BGP4_RT_LABEL_INFO(pRtProfile) \
                        (pRtProfile->u4Label)

#define  BGP4_RT_IS_LEAK_ROUTE(pRtProfile) (pRtProfile->u1IsVpnLeakedRoute)
#define  BGP4_RT_IS_IN_PADB(pRtProfile)    (pRtProfile->u1IsAddedInPADB)
 
#define BGP4_IFACE_ENTRY_LABEL(pIfInfo) (pIfInfo->u4Label)
#define BGP4_IFACE_ENTRY_VPN_CLASS(pIfInfo)     \
                (pIfInfo->u1VpnClassification)
#define BGP4_IFACE_LABEL_PEERS(pIfInfo)   \
                (pIfInfo->u2LabelPeers)
#endif

#ifdef EVPN_WANTED
#define BGP4_EVPN_RT_TYPE(pRtProfile) (pRtProfile->EvpnNlriInfo.u1RouteType)
#define BGP4_EVPN_RD_DISTING(pRtProfile) (pRtProfile->EvpnNlriInfo.au1RouteDistinguisher)
#define BGP4_EVPN_VNID(pRtProfile) (pRtProfile->EvpnNlriInfo.u4VnId)
#define BGP4_EVPN_VRFID(pRtProfile) (pRtProfile->u4VrfId)
#define BGP4_EVPN_ETHSEGID(pRtProfile) (pRtProfile->EvpnNlriInfo.au1EthernetSegmentID)
#define BGP4_EVPN_ETHTAG(pRtProfile) (pRtProfile->EvpnNlriInfo.u4EthernetTag)
#define BGP4_EVPN_MACADDR_LEN(pRtProfile) (pRtProfile->EvpnNlriInfo.u1MacAddrLen)
#define BGP4_EVPN_MACADDR(pRtProfile) (pRtProfile->EvpnNlriInfo.MacAddress)
#define BGP4_EVPN_IPADDR_LEN(pRtProfile) (pRtProfile->EvpnNlriInfo.u1IpAddrLen)
#define BGP4_EVPN_IPADDR(pRtProfile) (pRtProfile->EvpnNlriInfo.au1IpAddr)
#define BGP4_EVPN_MAC_DUPLICATE(pRtProfile)   (pRtProfile->EvpnNlriInfo.bIsMacDuplicate)
#define BGP4_EVPN_MAC_TYPE(pRtProfile) (pRtProfile->EvpnNlriInfo.u1MacType)
#define EVPN_BGP4_STATIC_MAC  1
#endif

#ifdef VPLSADS_WANTED
#define BGP4_VPLS_RT_LEN(pRtProfile) (pRtProfile->VplsNlriInfo.u2Length)
#define BGP4_VPLS_RT_ROUTE_DISTING(pRtProfile) (pRtProfile->VplsNlriInfo.au1RouteDistinguisher)
#define BGP4_VPLS_RT_VE_ID(pRtProfile) (pRtProfile->VplsNlriInfo.u2VeId)
#define BGP4_VPLS_RT_VBS(pRtProfile) (pRtProfile->VplsNlriInfo.u2VeBaseSize)
#define BGP4_VPLS_RT_VBO(pRtProfile) (pRtProfile->VplsNlriInfo.u2VeBaseOffset)
#define BGP4_VPLS_RT_LB(pRtProfile) (pRtProfile->VplsNlriInfo.u4labelBase)
#define BGP4_VPLS_NLRI_LB_FIELD_LEN   3 
#endif




#define BGP4_IFACE_LIST_POOLID          (gBgpNode.Bgp4IfPoolId)
#define BGP4_IFACE_ENTRY_IFTYPE(pIfInfo)    (pIfInfo->u4IfType)
#define BGP4_IFACE_ENTRY_INDEX(pIfInfo) (pIfInfo->u4IfIndex)
#define BGP4_IFACE_ENTRY_ADDR(pIfInfo) (pIfInfo->u4IfAddr)
#define BGP4_IFACE_ENTRY_OPER_STATUS(pIfInfo)   (pIfInfo->u1OperStatus)
#define BGP4_INPUTQ_MSGTYPE(x)         (x->u4MsgType)
#define BGP4_INPUTQ_FLAG(x)            (x->u4Flag)
#define BGP4_INPUTQ_CXT(x)             (x->u4ContextId)
#define BGP4_INPUTQ_DATA(x)            (x->QMsg.u4Data)
#define BGP4_INPUTQ_AGGR_INDEX(x)      (x->QMsg.AggrChgMsg.u4Index)
#define BGP4_INPUTQ_AGGR_ACTION(x)     (x->QMsg.AggrChgMsg.i4Action)
#define BGP4_INPUTQ_DEF_RT_PEER_ADDR_INFO(x)(x->QMsg.DftRouteMsg.RemoteAddress)
#define BGP4_INPUTQ_DEF_RT_STATUS(x)   (x->QMsg.DftRouteMsg.u1DftRouteStatus)
#define BGP4_INPUTQ_PEER_ADDR_INFO(x)  (x->QMsg.RemoteAddress)
#define BGP4_INPUTQ_IFACE_INDEX(x)  (x->QMsg.Bgp4IfMsg.u4IfIndex)
#define BGP4_INPUTQ_IFACE_STATUS(x)  (x->QMsg.Bgp4IfMsg.u4Status)
#define BGP4_INPUTQ_TCP_MD5_PEER_ADDR_INFO(x)  (x->QMsg.Bgp4TcpMd5AuthMsg.RemoteAddress)
#define BGP4_INPUTQ_TCP_MD5_PASSWORD(x)    (x->QMsg.Bgp4TcpMd5AuthMsg.au1TcpMd5Passwd)
#define BGP4_INPUTQ_TCP_MD5_PASSWORD_LEN(x)    (x->QMsg.Bgp4TcpMd5AuthMsg.u1TcpMd5PasswdLength)
#define BGP4_INPUTQ_PEER_GROUP_NAME(x)    (x->QMsg.au1PeerGroupName)
#define BGP4_INPUTQ_ROUTE_ADDR_INFO(x)  (x->QMsg.Bgp4RtInfoMsg.DestAddr)
#define BGP4_INPUTQ_ROUTE_NEXTHOP_INFO(x)  (x->QMsg.Bgp4RtInfoMsg.NextHop)
#define BGP4_INPUTQ_ROUTE_ADDR_PREFIXLEN_INFO(x)  (x->QMsg.Bgp4RtInfoMsg.u2DestPrefixlen)
#define BGP4_INPUTQ_ROUTE_ACTION(x)  (x->QMsg.Bgp4RtInfoMsg.u4Action)
#define BGP4_INPUTQ_ROUTE_IF_INDEX(x)  (x->QMsg.Bgp4RtInfoMsg.u4RtIfIndex)
#define BGP4_INPUTQ_ROUTE_METRIC(x)  (x->QMsg.Bgp4RtInfoMsg.i4Metric)
#define BGP4_INPUTQ_ROUTE_PROTO_ID(x)  (x->QMsg.Bgp4RtInfoMsg.u2RtProtId)

#ifdef L3VPN
#define BGP4_INPUTQ_VRF_ID(x)  (x->QMsg.Bgp4Vpn4Msg.Vpn4MsgType.u4VrfId)
#define BGP4_INPUTQ_VRF_STATUS(x)  (x->QMsg.Bgp4Vpn4Msg.u4Status)
#define BGP4_INPUTQ_LSP_ID(x)  (x->QMsg.Bgp4Vpn4Msg.Vpn4MsgType.u4LspId)
#define BGP4_INPUTQ_LSP_STATUS(x)  (x->QMsg.Bgp4Vpn4Msg.u4Status)
#define BGP4_INPUTQ_ROUTE_PARAMS(x)  (x->QMsg.Bgp4Vpn4Msg.au1RouteParam)
#define BGP4_INPUTQ_PARAM_TYPE(x)  (x->QMsg.Bgp4Vpn4Msg.u1ParamType)
#define BGP4_INPUTQ_ACTION_TYPE(x)  (x->QMsg.Bgp4Vpn4Msg.u1Action)


#endif

#ifdef EVPN_WANTED
#define BGP4_VXLAN_INPUTQ_VRF_ID(x)  (x->QMsg.Bgp4VxlanMsg.u4VrfId)
#define BGP4_VXLAN_INPUTQ_VNI_ID(x)  (x->QMsg.Bgp4VxlanMsg.u4VNId)
#define BGP4_VXLAN_INPUTQ_MAC_ADDR(x)  (x->QMsg.Bgp4VxlanMsg.MacAddress)
#define BGP4_VXLAN_INPUTQ_ETH_SEG_ID(x)  (x->QMsg.Bgp4VxlanMsg.au1EthSegId)
#define BGP4_VXLAN_INPUTQ_VRF_STATUS(x)  (x->QMsg.Bgp4VxlanMsg.u4Status)
#define BGP4_VXLAN_INPUTQ_ROUTE_PARAMS(x)  (x->QMsg.Bgp4VxlanMsg.au1RouteParam)
#define BGP4_VXLAN_INPUTQ_PARAM_TYPE(x)  (x->QMsg.Bgp4VxlanMsg.u1ParamType)
#define BGP4_VXLAN_INPUTQ_ACTION_TYPE(x)  (x->QMsg.Bgp4VxlanMsg.u1Action)
#define BGP4_VXLAN_INPUTQ_IP_ADDR(x)  (x->QMsg.Bgp4VxlanMsg.au1IpAddr)
#define BGP4_VXLAN_INPUTQ_IP_ADDR_LEN(x)  (x->QMsg.Bgp4VxlanMsg.i4IpAddrLen)
#define BGP4_VXLAN_INPUTQ_SINGLE_ACTIVE_FLAG(x)  (x->QMsg.Bgp4VxlanMsg.i4SingleActiveFlag)
#define BGP4_VXLAN_INPUTQ_ENTRY_STATUS_TYPE(x) (x->QMsg.Bgp4VxlanMsg.u1EntryStatus)
#endif

#define BGP4_INPUTQ_RTREF_PEER_ADDR_INFO(x) \
                            (x->QMsg.RtRefReqMsg.PeerRemoteAddress)
#define BGP4_INPUTQ_RTREF_ASAFI_MASK(x) (x->QMsg.RtRefReqMsg.u4AsafiMask)
#define BGP4_INPUTQ_RTREF_OPER_TYPE(x)  (x->QMsg.RtRefReqMsg.u1OperType)
#define BGP4_PEERENTRY_HEAD(u4CxtId)            (&(gBgpCxtNode[u4CxtId]->BGP4PeerList))
#define BGP4_PEER_TRANS_LIST(u4CxtId)           (&(gBgpCxtNode[u4CxtId]->BGP4PeerTransList))
#define BGP4_IFACE_GLOBAL_LIST         (&(gBgpNode.BGP4IfInfoList))
#define BGP4_PEERGROUPENTRY_HEAD(u4CxtId)            (&(gBgpCxtNode[u4CxtId]->BGP4PeerGrpList))
#ifdef L3VPN
#define BGP4_LSP_CHG_LIST     (&(gBgpNode.BGP4LspChngList))
#define BGP4_LSP_CHG_INIT_ROUTE_NETADDR_INFO \
    (gBgpNode.BGP4LspChgInitInfo.NHInitRoute)
#define BGP4_LSP_CHG_INIT_ROUTE_PREFIX_INFO  \
                (gBgpNode.BGP4LspChgInitInfo.NHInitRoute.NetAddr)
#define BGP4_LSP_CHG_INIT_ROUTE_PREFIX_LEN(u4CxtId)   \
            (gBgpCxtNode[u4CxtId]->BGP4LspChgInitInfo.NHInitRoute.u2PrefixLen)
#define BGP4_LSP_CHG_INIT_VRFID(u4CxtId) \
    (gBgpNode.BGP4LspChgInitInfo.u4VrfId)
#endif
#define BGP4_NONSYNC_LIST(u4CxtId)              (&(gBgpCxtNode[u4CxtId]->BGP4NonsyncList))
#define BGP4_PEER_INIT_LIST(u4CxtId)            (&(gBgpCxtNode[u4CxtId]->BGP4PeerInit))
/*BGP Multipath */
#ifdef EVPN_WANTED
#define BGP4_EVPN_AFI_FLAG             (gBgpNode.u1EvpnAfiSupFlag)
#endif

#define BGP4_MPATH_IBGP_COUNT(u4CxtId)          (gBgpCxtNode[u4CxtId]->u4IntBgpConfigMaxPaths)
#define BGP4_MPATH_EBGP_COUNT(u4CxtId)          (gBgpCxtNode[u4CxtId]->u4ExtBgpConfigMaxPaths) 
#define BGP4_MPATH_EIBGP_COUNT(u4CxtId)         (gBgpCxtNode[u4CxtId]->u4ExtOrIntBgpConfigMaxPaths)                        

#define BGP4_MPATH_OPER_IBGP_COUNT(u4CxtId)          (gBgpCxtNode[u4CxtId]->u4IntBgpOperMaxPaths)
#define BGP4_MPATH_OPER_EBGP_COUNT(u4CxtId)          (gBgpCxtNode[u4CxtId]->u4ExtBgpOperMaxPaths) 
#define BGP4_MPATH_OPER_EIBGP_COUNT(u4CxtId)         (gBgpCxtNode[u4CxtId]->u4ExtOrIntBgpOperMaxPaths)                        
#define BGP4_MPATH_NOT_ENABLED(u4CxtId) \
    (BGP4_MPATH_OPER_EBGP_COUNT(u4CxtId) == BGP_DEFAULT_MAXPATH)\
             && (BGP4_MPATH_OPER_IBGP_COUNT (u4CxtId) == BGP_DEFAULT_MAXPATH)\
                     && (BGP4_MPATH_OPER_EIBGP_COUNT (u4CxtId) == BGP_DEFAULT_MAXPATH)
#define BGP4_PEER_INIT_NETADDR_INFO(pPeer) (pPeer->peerInitInfo.peerInitRtInfo)
#define BGP4_PEER_INIT_PREFIX_INFO(pPeer)  \
            (pPeer->peerInitInfo.peerInitRtInfo.NetAddr)
#define BGP4_PEER_INIT_PREFIXLEN(pPeer) \
            (pPeer->peerInitInfo.peerInitRtInfo.u2PrefixLen)
#define BGP4_PEER_INIT_PA_ENTRY(pPeer)    (pPeer->peerInitInfo.pBgp4InitRcvdPA)
#define BGP4_PEER_INIT_PA_HASHKEY(pPeer)  (pPeer->peerInitInfo.u4HashKey)
#define BGP4_PEER_INIT_AFI(pPeer) \
            (pPeer->peerInitInfo.u2PeerInitAfi)
#define BGP4_PEER_INIT_SAFI(pPeer) \
            (pPeer->peerInitInfo.u2PeerInitSafi)
#ifdef L3VPN
#define BGP4_PEER_INIT_PE_CONTEXT(pPeer) \
            (pPeer->peerInitInfo.u4Context)
#endif
#define BGP4_PEER_SOFTCONFIG_IN_PEND_LIST(pPeer) (&(pPeer->SoftConfData.InPendAfiSafiList))
#define BGP4_PEER_SOFTCONFIG_OUT_PEND_LIST(pPeer) (&(pPeer->SoftConfData.OutPendAfiSafiList))
#define BGP4_PEER_SOFTCONFIG_INBOUND_AFI(pPeer) (pPeer->SoftConfData.u2InboundAfi)
#define BGP4_PEER_SOFTCONFIG_INBOUND_SAFI(pPeer) (pPeer->SoftConfData.u2InboundSafi)
#define BGP4_PEER_SOFTCONFIG_OUTBOUND_AFI(pPeer) (pPeer->SoftConfData.u2OutboundAfi)
#define BGP4_PEER_SOFTCONFIG_OUTBOUND_SAFI(pPeer) (pPeer->SoftConfData.u2OutboundSafi)
#define BGP4_PEER_SOFTCONFIG_INITIAL_AFISAFI(pPeer) \
                (pPeer->SoftConfData.u4InitialAsafiMask)
#define BGP4_SOFTCFG_OUT_REQ_PEER(pPeer, u4Index) \
            ((pPeer->apAsafiInstance[u4Index])->u4SoftCfgOutReq)
#define BGP4_PEER_DEINIT_LIST(u4CxtId)            (&(gBgpCxtNode[u4CxtId]->BGP4PeerDeInit))
#define BGP4_SUP_PEER_REUSE_LIST(u4CxtId)         (&(gBgpCxtNode[u4CxtId]->BGP4SupPeerReuseList))
#define BGP4_SOFTCFG_OUTBOUND_LIST(u4CxtId)         (&(gBgpCxtNode[u4CxtId]->BGP4SoftCfgOutList))
#define BGP4_LOCAL_ADMIN_STATUS(u4CxtId)        (gBgpCxtNode[u4CxtId]->u1BGP4Adminstatus)
#define BGP4_CLI_ADMIN_STATUS(u4CxtId)          (gBgpCxtNode[u4CxtId]->u4CliAdminStatus)
#define BGP4_ADMIN_STATUS(u4CxtId)              (gBgpCxtNode[u4CxtId]->u4AdminStatus)
#define BGP4_LOCAL_AS_NO(u4CxtId)               (gBgpCxtNode[u4CxtId]->u4BGP4LocalASno)
#define BGP4_ADMIN_STATUS_FLAG(u4CxtId)         (gBgpCxtNode[u4CxtId]->u1AdminStatusFlag)
#define BGP4_IS_AS_RESET(u4CxtId)               (gBgpCxtNode[u4CxtId]->u1IsAsReset)
#define BGP4_LOCAL_AS_CFG(u4CxtId)              (gBgpCxtNode[u4CxtId]->u1VrfConfig)
#define BGP4_TRAP_STATUS(u4CxtId)               (gBgpCxtNode[u4CxtId]->u1IsTrapEnabled)
#define BGP4_IBGP_REDISTRIBUTE_STATUS(u4CxtId)  (gBgpCxtNode[u4CxtId]->u1IBGPRedistributeStatus)
#define BGP4_LOCAL_BGP_ID(u4CxtId)              (gBgpCxtNode[u4CxtId]->u4BGP4Id)
#define BGP4_LOCAL_BGP_ID_CONFIG_TYPE(u4CxtId)  (gBgpCxtNode[u4CxtId]->u1BGP4IdConfigType)
#define BGP4_LOCAL_SYNC_STATUS(u4CxtId)         (gBgpCxtNode[u4CxtId]->u1BGP4Syncstatus)
#define BGP4_LOCAL_LISTEN_PORT(u4CxtId)         (gBgpCxtNode[u4CxtId]->u2BGP4Listenport)
#define BGP4_LOCAL_V4_LISTEN_CONN(u4CxtId)      (gBgpCxtNode[u4CxtId]->i4BGP4Listenconn)
#ifdef BGP_TCP6_WANTED
#define BGP4_LOCAL_V6_LISTEN_CONN(u4CxtId)      (gBgpCxtNode[u4CxtId]->i4BGP4v6Listenconn)
#endif
#define BGP4_DEFAULT_LOCAL_PREF(u4CxtId)        (gBgpCxtNode[u4CxtId]->u4BGP4DefaultLP)
#define BGP4_DEFAULT_IGP_METRIC(u4CxtId)        (gBgpCxtNode[u4CxtId]->u4BGP4DefaultIgpMetric)
#define BGP4_DEFAULT_AFI_SAFI_FLAG(u4CxtId)     (gBgpCxtNode[u4CxtId]->u1DefaultAfiSafi)
#define BGP4_IPV4_AFI_FLAG(u4CxtId)             (gBgpCxtNode[u4CxtId]->u1Ipv4AfiSupFlag)
#define BGP4_IPV6_AFI_FLAG(u4CxtId)             (gBgpCxtNode[u4CxtId]->u1Ipv6AfiSupFlag)
#ifdef VPLSADS_WANTED
#define BGP4_L2VPN_AFI_FLAG(u4CxtId)             (gBgpCxtNode[u4CxtId]->u1L2vpnAfiSupFlag)
#endif

#define BGP4_NON_BGP_RT_EXPORT_POLICY(u4CxtId)  (gBgpCxtNode[u4CxtId]->u4BGP4NonBgpRtExport)
#define BGP4_NON_BGP_INIT_PREFIX(u4CxtId)       \
                  (gBgpCxtNode[u4CxtId]->BGP4NonBgpInitPrefix.NonBgpInfo.NetAddr.au1Address)
#define BGP4_NON_BGP_INIT_VRFID \
                  (gBgpNode.BGP4NonBgpInitPrefix.NonBgpInfo.u4VrfId)
#define BGP4_NON_BGP_INIT_PREFIX_INFO(u4CxtId)  \
                (gBgpCxtNode[u4CxtId]->BGP4NonBgpInitPrefix.NonBgpInfo.NetAddr)
#define BGP4_NON_BGP_INIT_ROUTE_INFO(u4CxtId)  (gBgpCxtNode[u4CxtId]->BGP4NonBgpInitPrefix.NonBgpInfo)
#define BGP4_NON_BGP_INIT_PREFIXLEN(u4CxtId)    (gBgpCxtNode[u4CxtId]->BGP4NonBgpInitPrefix.NonBgpInfo.u2PrefixLen)
#define BGP4_ALWAYS_COMPARE_MED(u4CxtId)        (gBgpCxtNode[u4CxtId]->u1BGP4MedCompare)
#define BGP4_RIB_TREE(u4CxtId)                  (&(gBgpCxtNode[u4CxtId]->Bgp4RibHead))
#define BGP4_OVERLAP_ROUTE_POLICY(u4CxtId)      (gBgpCxtNode[u4CxtId]->u4InstallOverlapRt)
#define BGP4_OVERLAP_ROUTE_LIST(u4CxtId)        (&(gBgpCxtNode[u4CxtId]->BGP4OverlapRouteList))
#define BGP4_RTM_REG_ID(u4CxtId)                (gBgpCxtNode[u4CxtId]->u1BGP4RTMRegistrationId)
#define BGP4_RTM_REG_FLAG_STATUS(u4CxtId)       (gBgpCxtNode[u4CxtId]->u1RrdRegStatus)
#define BGP4_STATIC_IMPORT_LIST(u4CxtId)        (&(gBgpCxtNode[u4CxtId]->StaticImportList))
#define BGP4_STATIC_IMPORT_LIST_FIRST(u4CxtId)  (gBgpCxtNode[u4CxtId]->StaticImportList.pFirst)
#define BGP4_STATIC_IMPORT_LIST_LAST(u4CxtId)   (gBgpCxtNode[u4CxtId]->StaticImportList.pLast)
#define BGP4_STATIC_IMPORT_LIST_COUNT(u4CxtId)  (gBgpCxtNode[u4CxtId]->StaticImportList.u4Count)
#define BGP4_RIP_IMPORT_LIST(u4CxtId)           (&(gBgpCxtNode[u4CxtId]->RipImportList))
#define BGP4_RIP_IMPORT_LIST_FIRST(u4CxtId)     (gBgpCxtNode[u4CxtId]->RipImportList.pFirst)
#define BGP4_RIP_IMPORT_LIST_LAST(u4CxtId)      (gBgpCxtNode[u4CxtId]->RipImportList.pLast)
#define BGP4_RIP_IMPORT_LIST_COUNT(u4CxtId)     (gBgpCxtNode[u4CxtId]->RipImportList.u4Count)
#define BGP4_OSPF_IMPORT_LIST(u4CxtId)          (&(gBgpCxtNode[u4CxtId]->OspfImportList))
#define BGP4_OSPF_IMPORT_LIST_FIRST(u4CxtId)    (gBgpCxtNode[u4CxtId]->OspfImportList.pFirst)
#define BGP4_OSPF_IMPORT_LIST_LAST(u4CxtId)     (gBgpCxtNode[u4CxtId]->OspfImportList.pLast)
#define BGP4_OSPF_IMPORT_LIST_COUNT(u4CxtId)    (gBgpCxtNode[u4CxtId]->OspfImportList.u4Count)
#define BGP4_DIRECT_IMPORT_LIST(u4CxtId)        (&(gBgpCxtNode[u4CxtId]->DirectImportList))
#define BGP4_DIRECT_IMPORT_LIST_FIRST(u4CxtId)  (gBgpCxtNode[u4CxtId]->DirectImportList.pFirst)
#define BGP4_DIRECT_IMPORT_LIST_LAST(u4CxtId)   (gBgpCxtNode[u4CxtId]->DirectImportList.pLast)
#define BGP4_DIRECT_IMPORT_LIST_COUNT(u4CxtId)  (gBgpCxtNode[u4CxtId]->DirectImportList.u4Count)
#define BGP4_ISISL1_IMPORT_LIST(u4CxtId)        (&(gBgpCxtNode[u4CxtId]->IsisL1ImportList))
#define BGP4_ISISL1_IMPORT_LIST_FIRST(u4CxtId)  (gBgpCxtNode[u4CxtId]->IsisL1ImportList.pFirst)
#define BGP4_ISISL1_IMPORT_LIST_LAST(u4CxtId)   (gBgpCxtNode[u4CxtId]->IsisL1ImportList.pLast)
#define BGP4_ISISL1_IMPORT_LIST_COUNT(u4CxtId)  (gBgpCxtNode[u4CxtId]->IsisL1ImportList.u4Count)
#define BGP4_ISISL2_IMPORT_LIST(u4CxtId)        (&(gBgpCxtNode[u4CxtId]->IsisL2ImportList))
#define BGP4_ISISL2_IMPORT_LIST_FIRST(u4CxtId)  (gBgpCxtNode[u4CxtId]->IsisL2ImportList.pFirst)
#define BGP4_ISISL2_IMPORT_LIST_LAST(u4CxtId)   (gBgpCxtNode[u4CxtId]->IsisL2ImportList.pLast)
#define BGP4_ISISL2_IMPORT_LIST_COUNT(u4CxtId)  (gBgpCxtNode[u4CxtId]->IsisL2ImportList.u4Count)
#define BGP4_PEER_DATA                 (gBgpNode.BgpPeerData)
#define BGP4_PEER_DATA_PEER_PTR(u4CxtId)        (gBgpCxtNode[u4CxtId]->BgpPeerData.paPeerPtr)
#define BGP4_PEER_DATA_MAX_PEER(u4CxtId)        (gBgpCxtNode[u4CxtId]->BgpPeerData.u4MaxPeer)
#define BGP4_PEER_DATA_MAX_FD(u4CxtId)          (gBgpCxtNode[u4CxtId]->BgpPeerData.i4MaxFd)
#define BGP4_READ_SOCK_FD_SET(u4CxtId)          (&(gBgpCxtNode[u4CxtId]->BgpReadSockFdSet))
#define BGP4_READ_SOCK_FD_BITS(u4CxtId)         (&(gBgpCxtNode[u4CxtId]->BgpReadSockFdBits))
#define BGP4_WRITE_SOCK_FD_SET(u4CxtId)         (&(gBgpCxtNode[u4CxtId]->BgpWriteSockFdSet))
#define BGP4_WRITE_SOCK_FD_BITS(u4CxtId)        (&(gBgpCxtNode[u4CxtId]->BgpWriteSockFdBits))
#define BGP4_TABLE_VERSION(u4CxtId)             (gBgpCxtNode[u4CxtId]->u4BGP4TableVersion)
#define BGP4_RIB_ROUTES_COUNT(u4CxtId)          (gBgpCxtNode[u4CxtId]->u4RIBRoutesCount)
#define BGP4_MSG_CNT                   (gBgpNode.u4BgpmsgCnt)
#define BGP4_RTPROFILE_CNT             (gBgpNode.u4RtprofileCnt)
#define BGP4_LINKNODE_CNT              (gBgpNode.u4LinknodeCnt) 
#define BGP4_ADVT_RT_LINKNODE_CNT      (gBgpNode.u4PeerRtLinknodeCnt)
#define BGP4_INFO_CNT                  (gBgpNode.u4BgpinfoCnt)
#define BGP4_ASNODE_CNT                (gBgpNode.u4AsnodeCnt)
#define BGP4_INPUTQ_MSG_CNT            (gBgpNode.u4InputQMsgCnt)
#define BGP4_RIB_ROUTE_CNT(u4CxtId)             (gBgpCxtNode[u4CxtId]->u4RouteProfileCnt)
#define BGP4_PEER_CNT(u4CxtId)                  (gBgpCxtNode[u4CxtId]->u4PeerCount)
#define BGP4_RCVD_PA_CNT               (gBgpNode.u4RcvdPACnt)
#define BGP4_RR_CLIENT_CNT(u4CxtId)             (gBgpCxtNode[u4CxtId]->u4RrClientCnt)
#ifdef L3VPN
#define BGP4_LBL_GROUP_ID              gBgpNode.u2LabelGroupId
#endif
#define BGP4_ORF_ENTRY_TABLE(u4CxtId)           (gBgpCxtNode[u4CxtId]->OrfTable)

#define BGP4_IFACE_CNT                 (gBgpNode.u4IfInfoCnt)
#define BGP4_RFD_FLAG                  (gBgpNode.u4RfdFlag)
#define BGP4_TIMER_LISTID              (gBgpNode.Bgp4TmrListId)
#define BGP4_1S_TIMER_LISTID           (gBgpNode.Bgp41SecTmrListId)
#define BGP4_TASK_INIT_STATUS          (gBgpNode.u1Bgp4TaskInit) 
#define BGP4_MAX_PEERS_ALLOCATED       (gBgpNode.u4BGP4MaxPeersAllocated)
#define BGP4_MAX_ROUTES_ALLOCATED      (gBgpNode.u4BGP4MaxRoutesAllocated)
#define BGP4_TASK_ID                   (gBgpNode.Bgp4TaskId)
#define BGP4_Q_ID                      (gBgpNode.Bgp4QId)
#define BGP4_RM_Q_ID                      (gBgpNode.Bgp4RMQId)
#define BGP4_RES_Q_ID                  (gBgpNode.Bgp4SnmpQId)
#define BGP4_RIB_SEM_ID                (gBgpNode.Bgp4RibSemId)
#define BGP4_SYSLOG_ID                (gBgpNode.u4SysLogId)
#define BGP4_VPNV4_AFI_FLAG           (gBgpNode.u1Vpnv4AfiSupFlag)
#define BGP4_ROUTE_LEAK_FLAG           (gBgpNode.u1RouteLeakFlag)

/* Added for 4 byte ASN support in BGP */
#define BGP4_GLB_FOUR_BYTE_ASN_SUPPORT     (gBgpNode.u1FourByteASNSupportStatus)
#define BGP4_GLB_FOUR_BYTE_ASN_TYPE        (gBgpNode.u1FourByteASNNotationType)

#define BGP4_MAX_AS_LEN                 10
#define BGP4_MAX_AS_LEN_WITH_DOT        11
#define BGP4_MAX_AS_DIGITS_LEN           5
#define BGP4_MAX_AS_DIGITS_LEN_IN_STR    6

#define BGP4_FOUR_BYTE_ASN_FOUND(i4FirstIndex, i4LastIndex, i4Index)\
{\
    if (i4FirstIndex == -1)\
    {\
        i4FirstIndex = i4Index; \
        i4LastIndex = i4Index; \
    }\
    else\
    {\
        i4LastIndex = i4Index;\
    }\
}

#define BGP4_FOUR_BYTE_ASN_COUNT(u1Type, i2AsSetCount, i2AsSeqCount, i2FourByteAsnCount)\
{\
    if (u1Type == BGP4_ATTR_PATH_SET)\
    {\
        i2FourByteAsnCount = i2AsSetCount;\
    }\
    else if (u1Type == BGP4_ATTR_PATH_SEQUENCE)\
    {\
        i2FourByteAsnCount = i2AsSeqCount;\
    }\
}

#define BGP4_FOUR_BYTE_ASN_SET_SEQ_COUNT(u1Type, i2FourByteAsnCount, i2AsSetCount, i2AsSeqCount)\
{\
    if (u1Type == BGP4_ATTR_PATH_SET)\
    {\
        i2AsSetCount = i2FourByteAsnCount;\
    }\
    else if (u1Type == BGP4_ATTR_PATH_SEQUENCE)\
    {\
        i2AsSeqCount = i2FourByteAsnCount;\
    }\
}

#define BGP4_ASN_CONVERT_TO_STRING(i4NotationType, u4LocalAs, ac1Asn)\
{\
    if (i4NotationType == BGP4_ENABLE_4BYTE_ASN_ASDOT_NOTATION)\
    {\
       SPRINTF (ac1Asn, "%u.%u", u4LocalAs/(BGP4_MAX_TWO_BYTE_AS +1), u4LocalAs%(BGP4_MAX_TWO_BYTE_AS +1));\
    }\
    else\
    {\
       SPRINTF (ac1Asn, "%u", u4LocalAs);\
    }\
}

/* Added for Graceful restart support in BGP-MI for nmhGet functions alone */
#define BGP4_GLB_RESTART_SUPPORT          (gBgpNode.u1RestartSupport)
#define BGP4_GLB_RESTART_EXIT_REASON      (gBgpNode.u1RestartExitReason)
#define BGP4_GLB_RESTART_STATUS        (gBgpNode.u1RestartStatus)
#define BGP4_GLB_RESTART_REASON        (gBgpNode.u1RestartReason)
#define BGP4_GLB_RESTART_MODE        (gBgpNode.u1RestartMode)
#define BGP4_GLB_ROUTE_SELECTION_FLAG        (gBgpNode.u1RouteSelectionFlag)
#define BGP4_GLB_SEL_DEF_TIMER_INTERVAL      (gBgpNode.u4SelectionDeferralInterval)
#define BGP4_GLB_STALE_PATH_INTERVAL         (gBgpNode.u4StalePathInterval)
#define BGP4_GLB_RESTART_TIME_INTERVAL       (gBgpNode.u4RestartInterval)
#define BGP4_GLB_GR_ADMIN_STATUS             (gBgpNode.u1GrAdminStatus)
#define BGP4_GLB_GR_AFI_SUPPORT              (gBgpNode.u4GrAfiPreserved)
#define BGP4_GLB_SELECTION_DEFERAL_INTERVAL  (gBgpNode.u4SelectionDeferralInterval)
#define BGP4_GLB_LOCAL_AS_NO                 (gBgpNode.u4BGP4LocalASno)
#ifdef EVPN_WANTED
#define BGP4_GLB_MAC_DUP_TIMER_INTERVAL      (gBgpNode.u4MacMobDuplicationInterval)
#define BGP4_GLB_MAX_MAC_MOVES               (gBgpNode.u4MaxMacMoves)
#endif

/* Added for Graceful restart support in BGP */
#define BGP4_RESTART_SUPPORT(u4CxtId)          (gBgpCxtNode[u4CxtId]->u1RestartSupport)
#define BGP4_RESTART_EXIT_REASON(u4CxtId)      (gBgpCxtNode[u4CxtId]->u1RestartExitReason)
#define BGP4_RESTART_STATUS(u4CxtId)        (gBgpCxtNode[u4CxtId]->u1RestartStatus)
#define BGP4_RESTART_REASON(u4CxtId)        (gBgpCxtNode[u4CxtId]->u1RestartReason)
#define BGP4_RESTART_MODE(u4CxtId)        (gBgpCxtNode[u4CxtId]->u1RestartMode)
#define BGP4_ROUTE_SELECTION_FLAG(u4CxtId)        (gBgpCxtNode[u4CxtId]->u1RouteSelectionFlag)
#define BGP4_SEL_DEF_TIMER_INTERVAL(u4CxtId)      (gBgpCxtNode[u4CxtId]->u4SelectionDeferralInterval)
#define BGP4_STALE_PATH_INTERVAL(u4CxtId)         (gBgpCxtNode[u4CxtId]->u4StalePathInterval)
#define BGP4_RESTART_TIME_INTERVAL(u4CxtId)       (gBgpCxtNode[u4CxtId]->u4RestartInterval)
#define BGP4_GR_ADMIN_STATUS(u4CxtId)             (gBgpCxtNode[u4CxtId]->u1GrAdminStatus)
#define BGP4_GR_AFI_SUPPORT(u4CxtId)              (gBgpCxtNode[u4CxtId]->u4GrAfiPreserved)
#define BGP4_SELECTION_DEFERAL_INTERVAL(u4CxtId)  (gBgpCxtNode[u4CxtId]->u4SelectionDeferralInterval)
#define BGP4_SELECTION_DEFERAL_FLAG(u4CxtId) (gBgpCxtNode[u4CxtId]->u1SelectionDeferralFlag)

#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
#define BGP4_EOVPLS_FLAG(u4CxtId)                 (gBgpCxtNode[u4CxtId]->u1EOVPLSFlag)
#endif
#endif

/* Added for 4 byte ASN support in BGP */
#define BGP4_FOUR_BYTE_ASN_SUPPORT(u4CxtId)     (gBgpCxtNode[u4CxtId]->u1FourByteASNSupportStatus)
#define BGP4_FOUR_BYTE_ASN_TYPE(u4CxtId)        (gBgpCxtNode[u4CxtId]->u1FourByteASNNotationType)

#define  BGP4_GR_ADVT                         1
#define  BGP4_GR_RCVD                         2
#define  BGP4_GR_ADVT_RCVD                    3
#define  BGP4_GR_NONE                         4


#define BGP4_CHANGE_STATE(pPeerentry, u1_State)\
   BGP4_PEER_PREV_STATE(pPeerentry) = BGP4_PEER_STATE(pPeerentry); \
 BGP4_PEER_STATE(pPeerentry) = u1_State

#define  BGP4_PREFIX_BYTES(x)  (((x)+7) >> 3)
#define  BGP4_GET_PREFIX(pBuf, p_Prefix, i4Bytes) do { INT2 i2_Index; for (i2_Index=0; i2_Index < i4Bytes; i2_Index++) { *(((UINT1 *)(p_Prefix))+(i2_Index)) = *pBuf; pBuf++; } for (;i2_Index < 4;i2_Index++) { *(((UINT1 *)(p_Prefix))+(i2_Index)) = 0; } } while (0)

#define  BGP4_LINK_INFO_TO_PROFILE(pi, pf)   pf->pRtInfo = pi; pi->u4Ref++;
#define  BGP4_LINK_PROFILE_TO_NODE(pf, nd)   nd->pRouteProfile = pf; pf->u1Ref++;
#define BGP4_GET_PEER_TYPE(u4CxtId,pPeerentry) ((((pPeerentry->peerConfig).u4ASNo) == ((pPeerentry->peerConfig).u4LocalASNo)) ?  BGP4_INTERNAL_PEER : BGP4_EXTERNAL_PEER)
#define  BGP4_IS_EXTD_ATTRIBUTE_LEN(u1AttribFlag) (u1AttribFlag & 0x10)
#define  BGP4_GET_ATTRIBUTE_LEN(u1AttribFlag)  (BGP4_IS_EXTD_ATTRIBUTE_LEN(u1AttribFlag)  ? 2:1)

#define  BGP4_INFO_REF_COUNT(pBgpInfo)          (pBgpInfo->u4Ref)
#define  BGP4_INFO_ATTR_FLAG(pBgpInfo)          (pBgpInfo->u4AttrFlag)
#define  BGP4_INFO_ORIGIN(pBgpInfo)             (pBgpInfo->u1Origin)
#define  BGP4_INFO_IS_RMAP_SET(pBgpInfo)        (pBgpInfo->u1IsRmapSet)
#define  BGP4_INFO_ASPATH(pBgpInfo)             (&(pBgpInfo->TSASPath))
#define  BGP4_INFO_RCVD_MED(pBgpInfo)         (pBgpInfo->u4RecdMED)
#define  BGP4_INFO_SEND_MED(pBgpInfo)         (pBgpInfo->u4SendMED)
#define  BGP4_INFO_RCVD_LOCAL_PREF(pBgpInfo)  (pBgpInfo->u4RecdLocalPref)
#define  BGP4_INFO_AGGREGATOR(pBgpInfo)       (pBgpInfo->pAggr)
#define  BGP4_INFO_AGGREGATOR_AS(pBgpInfo)    (pBgpInfo->pAggr->u4ASNo)
#define  BGP4_INFO_AGGREGATOR_NODE(pBgpInfo)  (pBgpInfo->pAggr->u4IPAddr)
#define  BGP4_INFO_AGGREGATOR_FLAG(pBgpInfo)  (pBgpInfo->pAggr->u1Flag)
#define  BGP4_INFO_UNKNOWN_ATTR(pBgpInfo)     (pBgpInfo->pu1UnknownAttr)
#define  BGP4_INFO_UNKNOWN_ATTR_LEN(pBgpInfo) (pBgpInfo->u4UnknownAttrLen)
#define  BGP4_UNKNOWN_INFO_COUNT(pUnknownInfo) \
                (((tUnknownInfo *)pUnknownInfo)->u2UnknownInfoCount)
/* BGP4-INFO Recv Path Attribute DB. */
#define BGP4_INFO_RCVD_PA_ENTRY(pBgpInfo)    (pBgpInfo->pRcvdPA)
/* BGP4-INFO Community Attributes. */
#define  BGP4_INFO_COMM_ATTR(pBgpInfo)       (pBgpInfo->pCommunity)
#define  BGP4_INFO_COMM_COUNT(pBgpInfo)      (pBgpInfo->pCommunity->u2CommCnt)
#define  BGP4_INFO_COMM_ATTR_VAL(pBgpInfo)   (pBgpInfo->pCommunity->pu1CommVal)
#define  BGP4_INFO_COMM_ATTR_FLAG(pBgpInfo)  (pBgpInfo->pCommunity->u1Flag)
    
/* BGP4-INFO Extended-Community Attributes. */
#define  BGP4_INFO_ECOMM_ATTR(pBgpInfo)       (pBgpInfo->pExtCommunity)
#define  BGP4_INFO_ECOMM_COUNT(pBgpInfo) \
                              (pBgpInfo->pExtCommunity->u2EcommCnt)
#define  BGP4_INFO_ECOMM_ATTR_VAL(pBgpInfo)  \
                              (pBgpInfo->pExtCommunity->pu1EcommVal)
#define  BGP4_INFO_ECOMM_ATTR_COST_POI(pBgpInfo) \
                        (pBgpInfo->pExtCommunity->ExtCommCost.u2PointofIsertion)
#define  BGP4_INFO_ECOMM_ATTR_COST_COMM(pBgpInfo) \
                        (pBgpInfo->pExtCommunity->ExtCommCost.u2CostCommunity)
#define  BGP4_INFO_ECOMM_ATTR_COST_VAL(pBgpInfo) \
                        (pBgpInfo->pExtCommunity->ExtCommCost.u4CostValue)
#define  BGP4_INFO_ECOMM_ATTR_FLAG(pBgpInfo) \
                              (pBgpInfo->pExtCommunity->u1Flag)
#define  BGP4_INFO_ECOMM_ATTR_COST_FLAG(pBgpInfo) \
                              (pBgpInfo->pExtCommunity->u1CostFlag)
#define  BGP4_INFO_ECOMM_ATTR_COST_COMM_FLAG(pRoute)  \
                              (pRoute->pRtInfo->pExtCommunity->u1CostFlag)
#define  BGP4_INFO_ECOMM_ATTR_COST_COMM_VAL(pRoute)   \
                        (pRoute->pRtInfo->pExtCommunity->ExtCommCost.u4CostValue)
#define  BGP4_INFO_ECOMM_ATTR_COST_COMM_ID(pRoute)    \
                    (pRoute->pRtInfo->pExtCommunity->ExtCommCost.u2CostCommunity)

/* BGP4-INFO Originator Identifier. */
#define  BGP4_INFO_ORIG_ID(pBgpInfo)    (pBgpInfo->u4OrigId)

/* BGP4-INFO Cluster Identifier list. */
#define  BGP4_INFO_CLUS_LIST_ATTR(pBgpInfo)       (pBgpInfo->pClusterList)
#define  BGP4_INFO_CLUS_LIST_COUNT(pBgpInfo) \
                              (pBgpInfo->pClusterList->u2ClusCnt)
#define  BGP4_INFO_CLUS_LIST_ATTR_VAL(pBgpInfo)  \
                              (pBgpInfo->pClusterList->pu1ClusId)
#define  BGP4_INFO_CLUS_LIST_ATTR_FLAG(pBgpInfo) \
                              (pBgpInfo->pClusterList->u1Flag)
/* BGP4-INFO Weight */
#define  BGP4_INFO_WEIGHT(pBgpInfo)       (pBgpInfo->u2Weight)   
#define  BGP4_WEIGHT_DEFAULT_VALUE         32768
 
#define  BGP4_RT_NEXT(pRtProfile)       (pRtProfile->p_RPNext)
#ifdef L3VPN
#define  BGP4_RT_SET_NEXT(pRt1, pRt2, u4VrfId) \
         {\
             if (u4VrfId == BGP4_DFLT_VRFID)\
             {\
                 BGP4_RT_NEXT(pRt1) = pRt2;\
             }\
             else if(BGP4_RT_SAFI_INFO (pRt1) != BGP4_INET_SAFI_VPNV4_UNICAST)\
             {\
                 BGP4_RT_NEXT(pRt1) = pRt2;\
             }\
             else\
             {\
                 BGP4_VPN4_RT_INSTALL_VRFINFO_SET_RTNEXT (pRt1, pRt2, u4VrfId);\
             }\
         }
#define  BGP4_RT_GET_NEXT(pRt1, u4VrfId, pRtNext) \
         {\
             if (u4VrfId == BGP4_DFLT_VRFID)\
             {\
                 pRtNext = BGP4_RT_NEXT(pRt1);\
             }\
             else if (BGP4_RT_SAFI_INFO (pRt1) != BGP4_INET_SAFI_VPNV4_UNICAST) \
             {\
                 pRtNext = BGP4_RT_NEXT(pRt1);\
             }\
             else\
             {\
                 BGP4_VPN4_RT_INSTALL_VRFINFO_GET_RTNEXT(pRt1, u4VrfId, pRtNext);\
             }\
         }
#else
#define  BGP4_RT_SET_NEXT(pRt1, pRt2, u4VrfId) BGP4_RT_NEXT(pRt1) = pRt2
#define  BGP4_RT_GET_NEXT(pRt1, u4VrfId, pRtNext) pRtNext = BGP4_RT_NEXT(pRt1)
#endif
#define  BGP4_RT_REF_COUNT(pRtProfile)  (pRtProfile->u1Ref)

#define  BGP4_RT_PROTOCOL(pRtProfile)   (pRtProfile->u1Protocol)
#define  BGP4_RT_METRIC_TYPE(pRtProfile)   (pRtProfile->u1MetricType)
#define  BGP4_RT_PEER_ENTRY(pRtProfile) (pRtProfile->pPEPeer)
#define  BGP4_RT_BGP_INFO(pRtProfile)   (pRtProfile->pRtInfo)
#define  BGP4_RT_BGP_MED_CHANGE(pRtProfile)   (pRtProfile->u4MedChange)
#define  BGP4_RT_BGP_STATIC_MED_CHANGE(pRtProfile)   (pRtProfile->u4StaticMedChange)
#ifdef RFD_WANTED
#define  BGP4_RT_DAMP_HIST(pRtProfile)   (pRtProfile->pRtDampHist)
#endif
#define  BGP4_RT_NH_METRIC(pRtProfile)  (pRtProfile->i4NextHopMetric)
#define  BGP4_RT_IMMEDIATE_NEXTHOP(pRtProfile)\
                    ((pRtProfile->ImmediateNextHopInfo).au1Address)
#define  BGP4_RT_IMMEDIATE_NEXTHOP_INFO(pRtProfile)\
                    (pRtProfile->ImmediateNextHopInfo)
#define  BGP4_RT_IMMEDIATE_NEXTHOP_AFI_INFO(pRtProfile)\
                    ((pRtProfile->ImmediateNextHopInfo).u2Afi)

#define  BGP4_RT_MED(pRtProfile)              (pRtProfile->u4MED)
#define  BGP4_RT_NEW_MED(pRtProfile)              (pRtProfile->u4NewMED)
#define  BGP4_RT_LOCAL_PREF(pRtProfile)       (pRtProfile->u4LocalPref)

#define  BGP4_TREENODE_COUNT(pNode)    (pNode->u4Count)
#define  BGP4_LEAF_COUNT(pLeaf)        (pLeaf->u4Count)

/* For use of the above macro as a legal lvalue */
#define  BGP4_RT_BGP4_INF(pRtProfile)          ((pRtProfile)->pRtInfo)
#define  BGP4_RT_GET_FLAGS(pRtProfile)         (pRtProfile->u4Flags)
#define  BGP4_RT_GET_EXT_FLAGS(pRtProfile)         (pRtProfile->u4ExtFlags)
#define  BGP4_RT_SET_FLAG(pRtProfile,u4_flag)  (pRtProfile->u4Flags |= (u4_flag))
#define  BGP4_RT_SET_EXT_FLAG(pRtProfile,u4_flag)  (pRtProfile->u4ExtFlags |= (u4_flag))
#define  BGP4_RT_GET_RT_IF_INDEX(pRtProfile)   (pRtProfile->u4RtIfIndx)
#define  BGP4_RT_SET_RT_IF_INDEX(pRtProfile,u4Index)\
                                           (pRtProfile->u4RtIfIndx = u4Index)
#define  BGP4_RT_RESET_FLAG(pRtProfile, u4_flag)\
                                           (pRtProfile->u4Flags &= (UINT4)~(u4_flag))
#define  BGP4_RT_RESET_EXT_FLAG(pRtProfile, u4_flag)\
                                           (pRtProfile->u4ExtFlags &= (UINT4)~(u4_flag))


#define BGP4_RT_GET_ADV_FLAG(pRtProfile) (pRtProfile->u1AdvFlag)
#define BGP4_RT_SET_ADV_FLAG(pRtProfile,u1_AdvFlag)\
                                          (pRtProfile->u1AdvFlag |= u1_AdvFlag)
#define BGP4_RT_RESET_ADV_FLAG(pRtProfile,u1_AdvFlag)\
                                          (pRtProfile->u1AdvFlag &= (UINT1)~(u1_AdvFlag))

#define BGP4_RT_UPDATE_SENT_TIME(pAdvRtLinkNode) (pAdvRtLinkNode->u4UpdSentTime)
#define BGP4_RT_CHG_LIST_LINK_PREV(pRtProfile)   (pRtProfile->pChgListPrev)
#define BGP4_RT_CHG_LIST_LINK_NEXT(pRtProfile)   (pRtProfile->pChgListNext)
#define BGP4_RT_NEXTHOP_LINK_PREV(pRtProfile)    (pRtProfile->pNextHopPrev)
#define BGP4_RT_NEXTHOP_LINK_NEXT(pRtProfile)    (pRtProfile->pNextHopNext)
#define BGP4_RT_IDENT_ATTR_LINK_PREV(pRtProfile) (pRtProfile->pIdentAttrPrev)
#define BGP4_RT_IDENT_ATTR_LINK_NEXT(pRtProfile) (pRtProfile->pIdentAttrNext)
#define BGP4_RT_PROTO_LINK_PREV(pRtProfile)      (pRtProfile->pProtoRtPrev)
#define BGP4_RT_PROTO_LINK_NEXT(pRtProfile)      (pRtProfile->pProtoRtNext)


#ifdef L3VPN
#define BGP4_RT_VPN4_LINK_PREV(pRtProfile)       (pRtProfile->pVpnv4RtPrev)
#define BGP4_RT_VPN4_LINK_NEXT(pRtProfile)       (pRtProfile->pVpnv4RtNext)
#endif
#define BGP4_RT_OUT_BGP4_INFO(pRtProfile)      (pRtProfile->pOutRtInfo)

#define BGP4_GET_NH_HASH_KEY(u4VrfId, Nexthop, u4HashKey) \
        {\
            Bgp4GetPrefixHashIndex (Nexthop, &u4HashKey);\
            u4HashKey = (u4HashKey + u4VrfId) % BGP4_MAX_PREFIX_HASH_TBL_SIZE;\
        }

/* Macros for Aggregate Table entries */
#define  BGP4_AGGRENTRY                    (gBgpNode.aBGP4Aggrtbl)
#define  BGP4_AGGRENTRY_ADMIN(index)  \
        (gBgpNode.aBGP4Aggrtbl[index].u1AdminStatus)
#define  BGP4_AGGRENTRY_IPADDR(index)      \
                (((gBgpNode.aBGP4Aggrtbl[index].AggrInetAddress).NetAddr).au1Address)
#define  BGP4_AGGRENTRY_IPADDR_FAMILY(index)      \
                (((gBgpNode.aBGP4Aggrtbl[index].AggrInetAddress).NetAddr).u2Afi)
#define  BGP4_AGGRENTRY_IPSUBADDR_FAMILY(index)      \
                ((gBgpNode.aBGP4Aggrtbl[index].AggrInetAddress).u2Safi)
#define  BGP4_AGGRENTRY_IPADDR_LEN(index)      \
        (((gBgpNode.aBGP4Aggrtbl[index].AggrInetAddress).NetAddr).u2AddressLen)
#define  BGP4_AGGRENTRY_PREFIXLEN(index)   \
           ((gBgpNode.aBGP4Aggrtbl[index].AggrInetAddress).u2PrefixLen)
#define  BGP4_AGGRENTRY_NET_ADDR_INFO(index) \
          (gBgpNode.aBGP4Aggrtbl[index].AggrInetAddress)
#define  BGP4_AGGRENTRY_ADV_TYPE(index) \
        (gBgpNode.aBGP4Aggrtbl[index].u1AdvType)
#define  BGP4_AGGRENTRY_AS_SET(index) \
        (gBgpNode.aBGP4Aggrtbl[index].u1AsSet)
#define BGP4_AGGRENTRY_AS_ATOMIC_AGGR(index) \
        (gBgpNode.aBGP4Aggrtbl[index].u1AsAtomicAggr)
#define  BGP4_AGGRENTRY_AGGR_LIST(index)\
        (&(gBgpNode.aBGP4Aggrtbl[index].TSRouteProfile))
#define  BGP4_AGGRENTRY_AGGR_ROUTE(index)\
           (gBgpNode.aBGP4Aggrtbl[index].pAggregatedRoute)
#define  BGP4_AGGRENTRY_AGGR_STATUS(index) \
            (gBgpNode.aBGP4Aggrtbl[index].u1AggrStatus)
#define  BGP4_AGGRENTRY_ADV_RMAP_NAME(index) \
    (gBgpNode.aBGP4Aggrtbl[index].au1AdvMapName)
#define  BGP4_AGGRENTRY_SUPP_RMAP_NAME(index) \
    (gBgpNode.aBGP4Aggrtbl[index].au1SuppressMapName)
#define  BGP4_AGGRENTRY_ATTR_RMAP_NAME(index) \
    (gBgpNode.aBGP4Aggrtbl[index].au1AttMapName)
/* length */
#define  BGP4_AGGRENTRY_ADV_RMAP_LEN(index) \
    (gBgpNode.aBGP4Aggrtbl[index].u1AdvMapLen)
#define  BGP4_AGGRENTRY_SUPP_RMAP_LEN(index) \
    (gBgpNode.aBGP4Aggrtbl[index].u1SuppressMapLen)
#define  BGP4_AGGRENTRY_ATTR_RMAP_LEN(index) \
    (gBgpNode.aBGP4Aggrtbl[index].u1AttMapLen)
    /**/
#define  BGP4_AGGR_GET_STATUS(pAggrTblEntry) pAggrTblEntry->u1AggrStatus
#define  BGP4_AGGR_SET_STATUS(pAggrTblEntry, u1_flag)\
                        (pAggrTblEntry->u1AggrStatus |= u1_flag)
#define  BGP4_AGGR_RESET_STATUS(pAggrTblEntry, u1_flag)\
                        (pAggrTblEntry->u1AggrStatus &= ((UINT1)(~u1_flag)))
#define  BGP4_AGGRENTRY_INIT_PREFIX_INFO(index) \
            (gBgpNode.aBGP4Aggrtbl[index].AggrInitAddr.NetAddr)
#define  BGP4_AGGRENTRY_INIT_PREFIX(index) \
            (gBgpNode.aBGP4Aggrtbl[index].AggrInitAddr.NetAddr.au1Address)
#define  BGP4_AGGRENTRY_INIT_PREFIX_LEN(index) \
            (gBgpNode.aBGP4Aggrtbl[index].AggrInitAddr.u2PrefixLen)
#define  BGP4_AGGRENTRY_VRFNAME_SIZE(index) \
            ((gBgpNode.aBGP4Aggrtbl[index].VrfName).i4Length)
#define  BGP4_AGGRENTRY_VRFNAME_VALUE(index) \
            ((gBgpNode.aBGP4Aggrtbl[index].VrfName).au1VrfName)
#define  BGP4_AGGRENTRY_VRFID(index) \
            (gBgpNode.aBGP4Aggrtbl[index].u4VrfId)


/* Macros for MED Table entries */
#define  BGP4_MED_ENTRY                    (gBgpNode.aBGP4MEDtbl)
#define  BGP4_MED_ENTRY_ADMIN(index)  \
                             (gBgpNode.aBGP4MEDtbl[index].u1AdminStatus)
#define  BGP4_MED_ENTRY_VRFNAME_SIZE(index)  \
                ((gBgpNode.aBGP4MEDtbl[index].VrfName).i4Length)
#define  BGP4_MED_ENTRY_VRFNAME_VALUE(index)  \
                ((gBgpNode.aBGP4MEDtbl[index].VrfName).au1VrfName)
#define  BGP4_MED_ENTRY_ASNO(index) (gBgpNode.aBGP4MEDtbl[index].u4ASNo)
#define  BGP4_MED_ENTRY_PREFIX(index)      \
                (((gBgpNode.aBGP4MEDtbl[index].InetAddress).NetAddr).au1Address)
#define  BGP4_MED_ENTRY_IPADDR_FAMILY(index)      \
                (((gBgpNode.aBGP4MEDtbl[index].InetAddress).NetAddr).u2Afi)
#define  BGP4_MED_ENTRY_IPSUBADDR_FAMILY(index)      \
                ((gBgpNode.aBGP4MEDtbl[index].InetAddress).u2Safi)
#define  BGP4_MED_ENTRY_IPADDR_LEN(index)      \
             (((gBgpNode.aBGP4MEDtbl[index].InetAddress).NetAddr).u2AddressLen)
#define  BGP4_MED_ENTRY_PREFIXLEN(index)   \
                ((gBgpNode.aBGP4MEDtbl[index].InetAddress).u2PrefixLen)
#define  BGP4_MED_ENTRY_INTER_ASNO(index) \
            (gBgpNode.aBGP4MEDtbl[index].au4InterAS)
#define  BGP4_MED_ENTRY_DIRECTION(index)   (gBgpNode.aBGP4MEDtbl[index].u1Dir)
#define  BGP4_MED_ENTRY_MED(index)         (gBgpNode.aBGP4MEDtbl[index].u4MEDLP)
#define  BGP4_MED_ENTRY_APPL(index)  \
                        (gBgpNode.aBGP4MEDtbl[index].u1Applicability)
#define  BGP4_MED_ENTRY_VRFID(index) \
            (gBgpNode.aBGP4MEDtbl[index].u4VrfId)
#define  BGP4_MED_ENTRY_SET_ROW_ACTIVE(index) (gBgpNode.aBGP4MEDtbl[index].au1RowActive)
/* Macros for LP Table entries. */
#define  BGP4_LP_ENTRY                    (gBgpNode.aBGP4LPtbl)
#define  BGP4_LP_ENTRY_ADMIN(index)  (gBgpNode.aBGP4LPtbl[index].u1AdminStatus)
#define  BGP4_LP_ENTRY_VRFNAME_SIZE(index)  \
                ((gBgpNode.aBGP4LPtbl[index].VrfName).i4Length)
#define  BGP4_LP_ENTRY_VRFNAME_VALUE(index)  \
                ((gBgpNode.aBGP4LPtbl[index].VrfName).au1VrfName)
#define  BGP4_LP_ENTRY_ASNO(index)   (gBgpNode.aBGP4LPtbl[index].u4ASNo)
#define  BGP4_LP_ENTRY_PREFIX(index)      \
                (((gBgpNode.aBGP4LPtbl[index].InetAddress).NetAddr).au1Address)
#define  BGP4_LP_ENTRY_IPADDR_FAMILY(index)      \
                (((gBgpNode.aBGP4LPtbl[index].InetAddress).NetAddr).u2Afi)
#define  BGP4_LP_ENTRY_IPSUBADDR_FAMILY(index)      \
                ((gBgpNode.aBGP4LPtbl[index].InetAddress).u2Safi)
#define  BGP4_LP_ENTRY_IPADDR_LEN(index)      \
                (((gBgpNode.aBGP4LPtbl[index].InetAddress).NetAddr).u2AddressLen)
#define  BGP4_LP_ENTRY_PREFIXLEN(index)   \
                ((gBgpNode.aBGP4LPtbl[index].InetAddress).u2PrefixLen)
#define  BGP4_LP_ENTRY_INTER_ASNO(index) (gBgpNode.aBGP4LPtbl[index].au4InterAS)
#define  BGP4_LP_ENTRY_DIRECTION(index)  (gBgpNode.aBGP4LPtbl[index].u1Dir)
#define  BGP4_LP_ENTRY_LOCAL_PREF(index) (gBgpNode.aBGP4LPtbl[index].u4MEDLP)
#define  BGP4_LP_ENTRY_APPL(index)      \
                        (gBgpNode.aBGP4LPtbl[index].u1Applicability)
#define  BGP4_LP_ENTRY_VRFID(index) \
            (gBgpNode.aBGP4LPtbl[index].u4VrfId)
#define  BGP4_LP_ENTRY_SET_ROW_ACTIVE(index) (gBgpNode.aBGP4LPtbl[index].au1RowActive)
/* Macros for Filter Table entries. */
#define  BGP4_FILTER_ENTRY                    (gBgpNode.aBGP4UpdFiltertbl)
#define  BGP4_FILTER_ENTRY_ADMIN(index)   \
                         (gBgpNode.aBGP4UpdFiltertbl[index].u1AdminStatus)
#define  BGP4_FILTER_ENTRY_VRFNAME_VALUE(index)   \
                        (gBgpNode.aBGP4UpdFiltertbl[index].VrfName.au1VrfName)
#define  BGP4_FILTER_ENTRY_VRFNAME_SIZE(index)   \
                        (gBgpNode.aBGP4UpdFiltertbl[index].VrfName.i4Length)
#define  BGP4_FILTER_ENTRY_ASNO(index)    \
                         (gBgpNode.aBGP4UpdFiltertbl[index].u4ASNo)
#define  BGP4_FILTER_ENTRY_PREFIX(index)      \
     (((gBgpNode.aBGP4UpdFiltertbl[index].UpdtInetAddress).NetAddr).au1Address)
#define  BGP4_FILTER_ENTRY_IPADDR_FAMILY(index)      \
         (((gBgpNode.aBGP4UpdFiltertbl[index].UpdtInetAddress).NetAddr).u2Afi)
#define  BGP4_FILTER_ENTRY_IPSUBADDR_FAMILY(index)      \
            ((gBgpNode.aBGP4UpdFiltertbl[index].UpdtInetAddress).u2Safi)
#define  BGP4_FILTER_ENTRY_IPADDR_LEN(index)      \
   (((gBgpNode.aBGP4UpdFiltertbl[index].UpdtInetAddress).NetAddr).u2AddressLen)
#define  BGP4_FILTER_ENTRY_PREFIXLEN(index)   \
            ((gBgpNode.aBGP4UpdFiltertbl[index].UpdtInetAddress).u2PrefixLen)
#define  BGP4_FILTER_ENTRY_SUBADDR_FAMILY(index)   \
            ((gBgpNode.aBGP4UpdFiltertbl[index].UpdtInetAddress).u2Safi)
#define  BGP4_FILTER_ENTRY_INTER_ASNO(index)  \
                         (gBgpNode.aBGP4UpdFiltertbl[index].au4InterAS)
#define  BGP4_FILTER_ENTRY_DIRECTION(index)  \
                         (gBgpNode.aBGP4UpdFiltertbl[index].u1Dir)
#define  BGP4_FILTER_ENTRY_ACTION(index)    \
                         (gBgpNode.aBGP4UpdFiltertbl[index].u1Action)
#define  BGP4_FILTER_ENTRY_VRFID(index) \
            (gBgpNode.aBGP4UpdFiltertbl[index].u4VrfId)

#define  BGP4_ERR_TCP_RECV_MSG_ERROR(u4CxtId)    \
                         (gBgpCxtNode[u4CxtId]->Bgp4ErrorRecord.u4TcpRecvMsgErr)

#define  BGP4_FILTER_ENTRY_SET_ROW_ACTIVE(index) (gBgpNode.aBGP4UpdFiltertbl[index].au1RowActive)

#define  BGP4_ASPATH_TYPE(pASPath)           (pASPath->u1Type)
#define  BGP4_ASPATH_LEN(pASPath)            (pASPath->u1Length)
#define  BGP4_ASPATH_NOS(pASPath)            (pASPath->au1ASSegs)
#define  BGP4_REUSEADDR_OPT_VAL               1
#define  BGP4_LISTEN_PEER_SIZE                5

#define  BGP_CLASS_A_DEF_MASK  (0xff000000)
#define  BGP_CLASS_B_DEF_MASK  (0xffff0000)
#define  BGP_CLASS_C_DEF_MASK  (0xffffff00)
#define  BGP_DEF_NET_MASK      (0xffffffff)
#define  BGP_HOST_ADDR_CHECK   (0x00ffffff)
#define  BGP4_CLASS_C_START_ADD (191)
#define  QUERY_FOR_NEXTHOP     (0x01)
#define  QUERY_FOR_DESTINATION (0x02)
#define  QUERY_FOR_LOCAL_ROUTE (0x03)
#define  BGP_EXT_COMM_COST_TYPE_FIELD       17153 /* type feild identifier for Extended
                                                   cost community.*/
#define  BGP4_ECOMM_COST_DEFAULT  2147483647 /*Defaulf Extened community cost value
                                              0x7FFFFFFF */

#define  BGP_EXT_COMM_IGP_POI  129 /*Extended Comm Cost Point of Insertion value for 
                                   IGP cost. currently the POI is supported only for IGP 
                                   cost in best route selection process.*/

#define SNMP_V2_TRAP_OID_LEN            11

#define BGP4_OSPF_METRIC           0x1
#define BGP4_RIP_METRIC            0x2
#define BGP4_DIRECT_METRIC         0x4
#define BGP4_STATIC_METRIC         0x8
#define BGP4_ISIS_METRIC           0x10
#define BGP4_ALL_METRIC            0x20
#define BGP4_MAX_METRIC_TYPE       0x10

/* BIT_MASK to store wether redistribute metric is configured */
#define BGP4_OSPF_METRIC_MASK           0x01
#define BGP4_RIP_METRIC_MASK            0x02
#define BGP4_DIRECT_METRIC_MASK         0x04
#define BGP4_STATIC_METRIC_MASK         0x08
#define BGP4_ISIS_METRIC_MASK           0x10

/* TCP-MD5 related macros */
#define BGP4_TCPMD5_MEM_POOL_ID   (gBgpNode.Md5PoolId)
#define BGP4_TCPMD5_ALLOC_CTR(u4CxtId)     (gBgpCxtNode[u4CxtId]->Bgp4TcpMd5GlobalData.u4Md5AllocCtr)

/* TCP-AO related macros */
#define BGP4_TCPAO_MEM_POOL_ID   (gBgpNode.TcpAoPoolId)
#define BGP4_TCPAO_ALLOC_CTR(u4CxtId)     (gBgpCxtNode[u4CxtId]->Bgp4TcpAoGlobalData.u4TcpAoAllocCtr)
#define BGP4_TCPAO_MKT_LIST(u4CxtId)   (gBgpCxtNode[u4CxtId]->BGP4MktList)
/* Next Hop Metrics related macros */
#define BGP4_NEXTHOP_METRIC_TBL_POOLID\
                                (gBgpNode.Bgp4NextHopMetricPoolId)
#define BGP4_NEXTHOP_METRIC_TBL(u4CxtId)\
                                (gBgpCxtNode[u4CxtId]->pBgp4NextHopMetricTbl) 
#define BGP4_NH_METRIC_NEXTHOP_INFO(pIgpMetricEntry)\
                                    pIgpMetricEntry->LinkNexthop
#define BGP4_NH_METRIC_ROUTES(pIgpMetricEntry)\
                                    (&(pIgpMetricEntry->NextHopIdentRoute))
#define BGP4_NH_METRIC_METRIC(pIgpMetricEntry)\
                                    pIgpMetricEntry->i4IgpMetric
#define BGP4_NH_METRIC_IMME_NEXTHOP_INFO(pIgpMetricEntry)\
                                    pIgpMetricEntry->ImmediateNextHop
#define BGP4_NH_METRIC_IMME_NEXTHOP(pIgpMetricEntry)\
                                    pIgpMetricEntry->ImmediateNextHop.au1Address
#define BGP4_NH_METRIC_VRFID(pIgpMetricEntry) \
                                    pIgpMetricEntry->u4VrfId
#ifdef L3VPN
#define BGP4_NH_METRIC_LSPID(pIgpMetricEntry) \
                                    pIgpMetricEntry->u4IgpLspId
#define BGP4_NH_METRIC_LSP_STATUS(pIgpMetricEntry) \
                                    pIgpMetricEntry->u1LspStatus
#define BGP4_NH_METRIC_LSP_CHECK(pIgpMetricEntry) \
                                    pIgpMetricEntry->u1LspCheck
#endif
#define BGP4_NH_METRIC_IFINDEX(pIgpMetricEntry)\
                                    pIgpMetricEntry->u4IfIndex
#define BGP4_NH_METRIC_RT_CNT(pIgpMetricEntry)\
                                    pIgpMetricEntry->NextHopIdentRoute.u4Count
#define BGP4_NH_METRIC_ACTION(pIgpMetricEntry)\
                                    pIgpMetricEntry->u1Action
#define BGP4_NH_METRIC_RESOLVE_STATUS(pIgpMetricEntry)\
                                    pIgpMetricEntry->u1IsResolved
#ifdef L3VPN
#define BGP4_IFACE_VRFID(pIfinfo) (pIfinfo->u4VrfId)
#endif
#define BGP4_IFACE_INDEX(pIfinfo) (pIfinfo->u4IfIndex)
#define BGP4_IFACE_ADDR(pIfinfo) (pIfinfo->u4IfAddr)
#define BGP4_IFACE_STAUS(pIfinfo) (pIfinfo->u1OperStatus)

#if L3VPN
#define BGP4_GET_NH_RESOLVE_STATUS(u1RtResolve, u1LspStatus)\
                (((u1RtResolve == BGP4_TRUE) && \
                  (u1LspStatus == BGP4_LSP_UP)) ? BGP4_TRUE:BGP4_FALSE)
#endif
/* Macro to Scan the Hash table dynamically, so that deletion of
 * a entry can be done inside the SCAN loop. */    
#define BGP4_HASH_DYN_Scan_Bucket(pHashTab, u1HashKey, pNode, \
                pTempNode, TypeCast)\
                BGP_SLL_DYN_Scan (&((pHashTab)->HashList[u1HashKey]), \
                                 pNode, pTempNode, TypeCast)

/* Macro to Scan the Hash table starting from the given hash index.
 * Ensure, that while using this routine the u4StartIndex is less
 * than the Max Hash index size */                
#define BGP4_HASH_Scan_Table(pHashTab,u4HashIndex,u4StartIndex) \
        for((u4HashIndex) = u4StartIndex; ((UINT4)u4HashIndex) < (pHashTab)->u4_HashSize; u4HashIndex++)

/*
 * Macro to Scan the list dynamically, so that deletion of the SLL node
 * can be done inside the SCAN loop.
 */
#define BGP_SLL_DYN_Scan(pList, pNode, pTempNode, TypeCast) \
        for(((pNode) = (TypeCast)(VOID *)(TMO_SLL_First ((pList)))),   \
            ((pTempNode) = ((pNode == NULL) ? NULL : \
                           ((TypeCast)(VOID *)    \
                            (TMO_SLL_Next((pList),  \
                                          ((tTMO_SLL_NODE *)(VOID *)(pNode))))))); \
            (pNode) != NULL;  \
            (pNode) = (pTempNode),  \
            ((pTempNode) = ((pNode == NULL) ? NULL :  \
                           ((TypeCast) (VOID *)  \
                            (TMO_SLL_Next((pList),  \
                                          ((tTMO_SLL_NODE *)(VOID *)(pNode))))))))


#define BGP4_IS_MCAST_LOOP_ADDR(u4Addr) \
        ((((u4Addr & 0xff000000) >= 0xE0000000) || \
          ((u4Addr & 0xff000000) == 0x7f000000)) ? 0 : 1)

#define BGP4_IS_VALID_ADDRESS(u4Addr) \
        ((((u4Addr & 0xff000000) == 0x00000000) || \
          ((u4Addr & 0xff000000) >= 0xE0000000) || \
          ((u4Addr & 0xff000000) == 0x7f000000)) ? 0 : 1)

#define BGP4_IS_BROADCAST_ADDRESS(u4Addr) \
            ((((u4Addr & 0x000000ff) == 0x000000ff) || \
            ((u4Addr & 0x0000ffff) == 0x0000ffff) || \
            ((u4Addr & 0x00ffffff) == 0x00ffffff) || \
            ((u4Addr & 0xffffffff) == 0xffffffff)) ? 1 : 0)

#define BGP4_IS_ALL_BROADCAST_ADDRESS(u4Addr) \
            ((((u4Addr & 0xffffffff) == 0xffffffff)) ? 1 : 0)

#define BGP4_GET_MATCHVALUE(u4Addr, u4Bit) \
              (u4Addr & (0xffffffff << (32 - u4Bit)))

#define BGP4_QMSG_FREE(x)\
        BGP4_INPUTQ_MSG_CNT--; \
        MemReleaseMemBlock(gBgpNode.Bgp4InputQPoolId,(UINT1 *)x); 

#define BGP4_PEER_FSM_HIST_HEAD(pPeerentry) (pPeerentry->peerStatus.i2PeerFsmTransitionHistHead)
#define BGP4_PEER_FSM_HIST_TAIL(pPeerentry) (pPeerentry->peerStatus.i2PeerFsmTransitionHistTail)
#define BGP4_PEER_FSM_TRANSITION_HIST(pPeerentry,index) (pPeerentry->peerStatus.ai1PeerFsmTransitionHist[index])

#define BGP4_ASSERT(x)         assert ((x))
#define BGP4_ADJUST_START_TIME_THRESHOLD(pPeerentry) \
     if (BGP4_PEER_START_TIME(pPeerentry) > BGP4_START_TIME_THRESHOLD) \
       {\
        BGP4_PEER_START_TIME(pPeerentry) = BGP4_START_TIME_THRESHOLD;\
       }

#define BGP4_IS_ROUTE_VALID(pRtProfile)\
            (((BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_FILTERED_INPUT) || \
             (BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_OVERLAP) || \
             (BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_NEXT_HOP_UNKNOWN) || \
             (BGP4_RT_GET_FLAGS (pRtProfile) & BGP4_RT_DAMPED)) ? \
             BGP4_FALSE : BGP4_TRUE)

 /* RBTree macro definitions */
#define BGP4_RB_TREE_CREATE     RBTreeCreate
#define BGP4_RB_TREE_DELETE     RBTreeDelete
#define BGP4_RB_TREE_ADD        RBTreeAdd
#define BGP4_RB_TREE_REMOVE     RBTreeRemove
#define BGP4_RB_TREE_GET        RBTreeGet
#define BGP4_RB_TREE_GET_FIRST  RBTreeGetFirst
#define BGP4_RB_TREE_GET_NEXT   RBTreeGetNext
#define BGP4_RB_TREE_GET_COUNT  RBTreeCount

/* Macros for TCP MD5 support in BGP */
#define BGP4_NO_SESSION             (1)
#define BGP4_MD5_AUTHENTICATED_SESSION (2)
#define BGP4_AUTHENTICATED_SESSION  BGP4_MD5_AUTHENTICATED_SESSION
#define BGP4_UNAUTHENTICATED_SESSION  (3)
#define BGP4_TCPAO_AUTHENTICATED_SESSION (4)

/* Macros for TCP AO support in BGP */
#define BGP4_HMAC_SHA_1  (1)
#define BGP4_AES128  (2)
#define BGP4_TCPAO_MIN_KEY_ID (0)
#define BGP4_TCPAO_MAX_KEY_ID   (255)
/* Macros for BGP context */
#define  BGP4_RT_CXT_ENTRY(pRtProfile) (pRtProfile->pPEPeer->pBgpCxtNode)
#define  BGP4_RT_CXT_ID(pRtProfile) \
      ((pRtProfile->pPEPeer != NULL) ?(pRtProfile->pPEPeer->pBgpCxtNode->u4ContextId): (pRtProfile->pBgpCxtNode->u4ContextId)) 

#define  BGP4_PEER_CXT_ENTRY(pPeer)    (pPeer->pBgpCxtNode)
#define  BGP4_PEER_CXT_ID(pPeer)       (pPeer->pBgpCxtNode->u4ContextId)
#define  BGP4_STATUS(u4Context)        (gBgpCxtNode[u4Context]->u1BGPStatus)

#define BGP4_INVALID_CONTEXT_ID       (SYS_DEF_MAX_NUM_CONTEXTS + 1)

#define BGP4_LABEL_ALLOC_POLICY(u4CxtId)  (gBgpCxtNode[u4CxtId]->u1LabelAllocPolicy)
#define BGP4_L3VPN_ROUTE_COUNT(u4CxtId)   (gBgpCxtNode[u4CxtId]->u4L3VPNRouteCount)      
#define BGP4_NOTIFY_ASN                   (gBgpNode.Bgp4L3VpnNotifyASN)
#define BGP4_IS_RT_EXT_COMMUNITY(u2EcommType) \
        (((u2EcommType == 0x0002) ||\
          (u2EcommType == 0x0102) ||\
          (u2EcommType == 0x0602) ||\
          (u2EcommType == 0x0601) ||\
          (u2EcommType == 0x0202)) ? BGP4_TRUE : BGP4_FALSE)

/* Shadow Peer Configuration Macros */
#define BGP4_PEER_SHADOW_SEND_BUF(pPeerentry)  (pPeerentry->shadowPeerConfig.u4SendBuf)
#define BGP4_PEER_SHADOW_RECV_BUF(pPeerentry)  (pPeerentry->shadowPeerConfig.u4RecvBuf)
#define BGP4_PEER_SHADOW_CONNECT_RETRY_INTERVAL(pPeerentry)  (pPeerentry->shadowPeerConfig.u4ConnectRetryInterval)
#define BGP4_PEER_SHADOW_HOLD_INTERVAL(pPeerentry)  (pPeerentry->shadowPeerConfig.u4HoldInterval)
#define BGP4_PEER_SHADOW_DELAY_OPEN_TIME_INTERVAL(pPeerentry)  (pPeerentry->shadowPeerConfig.u4DelayOpenTimeInterval)
#define BGP4_PEER_SHADOW_KEEP_ALIVE_INTERVAL(pPeerentry)  (pPeerentry->shadowPeerConfig.u4KeepAliveInterval)
#define BGP4_PEER_SHADOW_MIN_AS_ORIG_INTERVAL(pPeerentry)  (pPeerentry->shadowPeerConfig.u4MinASOrigInterval)
#define BGP4_PEER_SHADOW_MIN_ROUTE_ADV_INTERVAL(pPeerentry)  (pPeerentry->shadowPeerConfig.u4MinRouteAdvInterval)
#define BGP4_PEER_SHADOW_PEER_PREFIX_UPPER_LIMIT(pPeerentry)  (pPeerentry->shadowPeerConfig.u4PeerPrefixUpperLimit)
#define BGP4_PEER_SHADOW_IDLE_HOLDTIME_INTERVAL(pPeerentry)  (pPeerentry->shadowPeerConfig.u4IdleHoldTimeInterval)
#define BGP4_PEER_SHADOW_REMOTE_AS_NO(pPeerentry)  (pPeerentry->shadowPeerConfig.u4RemoteASNo)
#define BGP4_PEER_SHADOW_LOCAL_AS_NO(pPeerentry)  (pPeerentry->shadowPeerConfig.u4LocalASNo)
#define BGP4_PEER_SHADOW_PASSIVE(pPeerentry)  (pPeerentry->shadowPeerConfig.u1Passive)
#define BGP4_PEER_SHADOW_CONNECT_RETRY_COUNT(pPeerentry)  (pPeerentry->shadowPeerConfig.u1ConnectRetryCount)
#define BGP4_PEER_SHADOW_HOP_LIMIT(pPeerentry)  (pPeerentry->shadowPeerConfig.u1HopLimit)
#define BGP4_PEER_SHADOW_AUTO_START(pPeerentry)  (pPeerentry->shadowPeerConfig.u1AutomaticStart)
#define BGP4_PEER_SHADOW_AUTO_STOP(pPeerentry)  (pPeerentry->shadowPeerConfig.u1AutomaticStop)
#define BGP4_PEER_SHADOW_DELAY_OPEN_STATUS(pPeerentry)  (pPeerentry->shadowPeerConfig.u1DelayOpenStatus)
#define BGP4_PEER_SHADOW_DAMP_PEER_OSC_STATUS(pPeerentry)  (pPeerentry->shadowPeerConfig.u1DampPeerOscillationStatus)
#define BGP4_PEER_SHADOW_EBGP_MULTIHOP(pPeerentry)  (pPeerentry->shadowPeerConfig.u1EBGPMultiHop)
#define BGP4_PEER_SHADOW_SELF_NEXTHOP(pPeerentry)  (pPeerentry->shadowPeerConfig.u1SelfNextHop)
#define BGP4_PEER_SHADOW_RFL_CLIENT(pPeerentry)  (pPeerentry->shadowPeerConfig.u1RflClient)
#define BGP4_PEER_SHADOW_COMM_SEND_STATUS(pPeerentry)  (pPeerentry->shadowPeerConfig.u1CommSendStatus)
#define BGP4_PEER_SHADOW_ECOMM_SEND_STATUS(pPeerentry)  (pPeerentry->shadowPeerConfig.u1ECommSendStatus)
#define BGP4_PEER_SHADOW_DFT_ROUTE_ORIG_STATUS(pPeerentry)  (pPeerentry->shadowPeerConfig.u1DftRouteOrigStatus)
#define BGP4_PEER_SHADOW_LOCAL_AS_CONFIG(pPeerentry)  (pPeerentry->shadowPeerConfig.u1LocalASConfig)
#define BGP4_PEER_SHADOW_OVERRIDE_CAP(pPeerentry)  (pPeerentry->shadowPeerConfig.u1OverrideCap)
#define BGP4_PEER_SHADOW_CONNECT_COUNT(pPeerentry)  (pPeerentry->shadowPeerConfig.u1ConnectCount)
#define BGP4_PEER_SHADOW_MP_CAP_MASK(pPeerentry)  (pPeerentry->shadowPeerConfig.u1MPCapabilty)
#define BGP4_PEER_SHADOW_ORF_MODE(pPeerentry)  (pPeerentry->shadowPeerConfig.u1OrfMode)
#define BGP4_PEER_SHADOW_BFD_STATUS(pPeerentry) (pPeerentry->shadowPeerConfig.u1BfdStatus)

#endif /* BGP4MAC_H */
