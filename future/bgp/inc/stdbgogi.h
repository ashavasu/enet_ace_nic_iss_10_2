
# ifndef stdbgOGP_H
# define stdbgOGP_H

 /* The Definitions of the OGP Index Constants.  */

# define SNMP_OGP_INDEX_BGP                                          (0)
# define SNMP_OGP_INDEX_BGPPEERTABLE                                 (1)
# define SNMP_OGP_INDEX_BGP4PATHATTRTABLE                            (2)

#endif /*  stdbgp4OGP_H  */
