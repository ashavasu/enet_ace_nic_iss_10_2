/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgevpndfs.h,v 1.5 2015/10/05 14:04:16 siva Exp $
 *
 * Description: This file contains the macro defns
 *
 *******************************************************************/

#ifndef _BGEVPNDFS_H
#define _BGEVPNDFS_H

#define EVPN_BGP4_RD                     1
#define EVPN_BGP4_IMPORT_RT              2
#define EVPN_BGP4_EXPORT_RT              3
#define EVPN_BGP4_BOTH_RT                4

#define EVPN_BGP4_RD_ADD                 1
#define EVPN_BGP4_RT_ADD                 3
#define EVPN_BGP4_RT_DEL                 4

#define BGP4_EVPN_RT_TYPE_SIZE              1
#define BGP4_EVPN_ROUTE_DISTING_SIZE        8
#define BGP4_EVPN_ETH_SEG_ID_SIZE          10
#define BGP4_EVPN_ETH_TAG_SIZE              4
#define BGP4_EVPN_ADDR_LEN_SIZE             1
#define BGP4_EVPN_IP_ADDR_SIZE              4
#define BGP4_EVPN_IP6_ADDR_SIZE             16
#define BGP4_EVPN_MAC_ADDR_SIZE             6
#define BGP4_EVPN_VN_ID_SIZE                3

#define BGP4_EVPN_NORMAL_PEER               0
#define BGP4_EVPN_CE_PEER                   1
#define BGP4_EVPN_PE_PEER                   2

#define EVPN_AD_ROUTE              1
#define EVPN_MAC_ROUTE             2
#define EVPN_ETH_SEGMENT_ROUTE     4

#define EVPN_BGP_ADVERTISE_MSG     1
#define EVPN_BGP_WITHDRAW_MSG      2

#define EVPN_BGP4_ROUTE_PARAMS_REQ       1
#define EVPN_BGP4_VRF_ADMIN_STATUS_REQ   2
#define EVPN_BGP4_NOTIFY_MAC_UPDT        3
#define EVPN_BGP4_GET_MAC_NOTIF          4
#define EVPN_BGP4_NOTIFY_ESI             5
#define EVPN_BGP4_NOTIFY_ETH_AD          6

#define EVPN_PARAM_ESI_ADD        1

#define MAC_IMPORT_ROUTE_LEN             8

#define BGP4_EVPN_RR_TARGETS_DEL_INTERVAL 7200 /* 2 Hours */

#define BGP4_UPDATE_MAC 5
#define BGP4_REMOVE_MAC 6

#define BGP_MAX_ETH_RT_VALUE             0xFFFFFFFF
/*EVPN Unique Index */
#ifdef BGP4_IPV6_WANTED
       #ifdef L3VPN
           #ifdef VPLSADS_WANTED
                #define BGP4_L2VPN_EVPN_INDEX 5
           #else
                #define BGP4_L2VPN_EVPN_INDEX 4
           #endif
       #else
           #ifdef VPLSADS_WANTED
                #define BGP4_L2VPN_EVPN_INDEX 3
           #else
                #define BGP4_L2VPN_EVPN_INDEX 2
           #endif
       #endif
#else
      #ifdef L3VPN
          #ifdef VPLSADS_WANTED
                #define BGP4_L2VPN_EVPN_INDEX 4
          #else
                #define BGP4_L2VPN_EVPN_INDEX 3
          #endif
      #else
          #ifdef VPLSADS_WANTED
                #define BGP4_L2VPN_EVPN_INDEX 2
          #else
                #define BGP4_L2VPN_EVPN_INDEX 1
          #endif
      #endif
#endif

                                      /* RtType + Len + RD + ESI + EthTag + MacAddLen + MacAddr + IPAddrLen + VNId */
#define BGP4_EVPN_MAC_NLRI_PREFIX_LEN       1   +  1  + 8  + 10  +   4    +     1     +    6    +     1     +  3

                                          /* RtType + Len + RD + ESI + IPAddrLen */
#define BGP4_EVPN_ETH_SEG_NLRI_PREFIX_LEN      1    +  1  + 8  +  10 +    1

                                               /* RtType + Len + RD + ESI + EthTag + VnId */
#define BGP4_EVPN_ETH_AD_ROUTE_NLRI_PREFIX_LEN      1    +  1  + 8  +  10 +   4    +  3

#define MAX_LEN_RD_VALUE    8
#define MAX_LEN_VNI_VALUE   4
#define MAX_LEN_MAC_ADDR    6
#define BGP4_EVPN_MAX_KEY_SIZE   ( MAX_LEN_RD_VALUE + MAX_LEN_VNI_VALUE + MAX_LEN_MAC_ADDR + MAX_LEN_ETH_SEG_ID)

#define MAX_LEN_ETH_SEG_ID  10
#define MAC_IMPORT_ROUTE_LEN 8
#define BGP4_EVPN_ESI_RT_LEN 8
#define BGP4_REMOVE_ESID    2
#define AD_WITHDRAWN        2

#define ETH_SEG_RT_TYPE     6
#define ETH_SEG_RT_SUBTYPE  2
#define ETH_AD_RT_TYPE      6
#define ETH_AD_RT_SUB_TYPE  1

#define EVPN_UP                     1
#define EVPN_DOWN                   2
#define EVPN_CREATE                 3
#define EVPN_DELETE                 4
#define BGP4_EVPN_VRF_CREATE               EVPN_CREATE
#define BGP4_EVPN_VRF_DELETE               EVPN_DELETE
#define BGP4_EVPN_VRF_UP                   EVPN_UP
#define BGP4_EVPN_VRF_DOWN                 EVPN_DOWN
#define EVPN_MAC_STR_LEN     24

#define BGP4_EVPN_NLRI_PREFIX_LEN 17
#define BGP4_EVPN_RIB_TREE(u4CxtId)   \
    (gBgpCxtNode[u4CxtId]->gapBgp4Rib[BGP4_L2VPN_EVPN_INDEX])

#define BGP4_EVPN_VRF_SPEC_ROUTE_DISTING(u4Context)  \
                    (gBgpCxtNode[u4Context].EvpnRoutes->au1RouteDistinguisher)
#define BGP4_EVPN_VRF_SPEC_VNID(u4Context)  \
                    d(gBgpCxtNode[u4Context].EvpnRoutes->u2VNId)
#define BGP4_EVPN_VRF_SPEC_IMPORT_TARGETS(u4Context) \
                    (&(gBgpCxtNode[u4Context].EvpnRoutes->ImportTargets))
#define BGP4_EVPN_VRF_SPEC_EXPORT_TARGETS(u4Context) \
                    (&(gBgpCxtNode[u4Context].EvpnRoutes->ExportTargets))
#define BGP4_EVPN_EXT_COMM_ROW_STATUS(pRTECom) \
                     (pRTECom->u1RowStatus)
#define BGP4_EVPN_VRF_SPEC_CURRENT_STATE(u4Context)  \
                     (gBgpCxtNode[u4Context]->u4EvpnState)
#define BGP4_EVPN_VRF_SPEC_GET_FLAG(u4Context)  \
                     (gBgpCxtNode[u4Context]->i4EvpnFlags)
#define BGP4_EVPN_VRF_SPEC_SET_FLAG(u4Context,i4_flag)  \
                (gBgpCxtNode[u4Context]->i4EvpnFlags |= (i4_flag))
#define BGP4_EVPN_VRF_SPEC_RESET_FLAG(u4Context,i4_flag) \
                (gBgpCxtNode[u4Context]->i4EvpnFlags &= ~(i4_flag))

#define BGP4_PEER_EVPN_AFISAFI_INSTANCE(pPeerentry) \
                    ((pPeerentry)->apAsafiInstance[BGP4_L2VPN_EVPN_INDEX])

#define BGP4_PEER_EVPN_OUTPUT_ROUTES(pPeerentry) \
        (BGP4_PEER_EVPN_AFISAFI_INSTANCE(pPeerentry)->pOutputList)
#define BGP4_PEER_EVPN_NEW_ROUTES(pPeerentry)\
        (BGP4_PEER_EVPN_AFISAFI_INSTANCE(pPeerentry)->pNewList)
#define BGP4_PEER_EVPN_NEW_LOCAL_ROUTES(pPeerentry)\
        (BGP4_PEER_EVPN_AFISAFI_INSTANCE(pPeerentry)->pNewLocalList)
#define BGP4_PEER_EVPN_ADVT_WITH_ROUTES(pPeerentry)\
        (BGP4_PEER_EVPN_AFISAFI_INSTANCE(pPeerentry)->pAdvtWithList)
#define BGP4_PEER_EVPN_ADVT_FEAS_ROUTES(pPeerentry)\
        (BGP4_PEER_EVPN_AFISAFI_INSTANCE(pPeerentry)->pAdvtFeasList)
#define BGP4_PEER_EVPN_DEINIT_RT_LIST(pPeerentry)\
        (&(BGP4_PEER_EVPN_AFISAFI_INSTANCE(pPeerentry)->TSDeInitRtList))

#define BGP4_PEER_L2VPN_VPLS_AFISAFI_INSTANCE(pPeerentry) \
                    (pPeerentry->apAsafiInstance[BGP4_L2VPN_VPLS_INDEX])


#define BGP4_EVPN_RT_ROUTE_DISTING(pRtProfile) (pRtProfile->EvpnNlriInfo.au1RouteDistinguisher)
#define BGP4_EVPN_RT_VN_ID(pRtProfile) (pRtProfile->EvpnNlriInfo.u4VnId)
#define BGP4_EVPN_RT_MAC_ADDR(pRtProfile) (pRtProfile->EvpnNlriInfo.MacAddress)
#define BGP4_EVPN_RT_ETH_SEG_ID(pRtProfile) (pRtProfile->EvpnNlriInfo.au1EthernetSegmentID)

/* Used by capabilities module for Evpn processing */
#define  CAP_MP_L2VPN_EVPN                  0x00190046
#define  CAP_NEG_L2VPN_EVPN_MASK              0x00000020

/* Specifies that export targets are removed and routes to be removed */
#define BGP4_EVPN_VRF_NO_EXP_TARGETS             0x00000400
/* Specifies the export target policy change is reissued */
#define BGP4_EVPN_VRF_EXP_TGT_CHG_REISSUE        0x00000010

/* Data Structure definitions */
/*Structure to store information to a specific VRF*/

typedef struct _tEvpnVrfNode {
    tRBNodeEmbd     RbNode;  /* RbNode for the entry */
    UINT4           u4VnId;
    UINT1           au1RouteDistinguisher[MAX_LEN_RD_VALUE];
    UINT1           au1EthSegRT[BGP4_EVPN_ESI_RT_LEN]; /* Ext Comm for Eth Segment route */
    UINT1           au1EthAdRT[BGP4_EVPN_ESI_RT_LEN]; /* Ext Comm for Eth AD route */
    UINT1           au1IpAddr[BGP4_EVPN_IP6_ADDR_SIZE];
    tTMO_SLL        ImportTargets; /* List of import route targets
                                    * associated with this EVPN */
    tTMO_SLL        ExportTargets; /* List of export route targets 
                                    * associated with this EVPN */
    UINT1           au1EthSegId[MAX_LEN_ETH_SEG_ID];
    UINT1           u1IpAddrLen;   /* Ip Address Length */
    UINT1           u1EvpnStatus;
} tEvpnVrfNode;

typedef struct _tBgpMacDup {
    tRBNodeEmbd     RbNode;  /* RbNode for the entry */
    tBgp4AppTimer   tMacMobDupTmr;
    UINT4           u4SeqNolastExp;
    UINT4           u4VrfId;
    UINT4           u4VnId;
    tMacAddr        MacAddress;
    UINT1           u1pad[2];
} tBgpMacDup;

/*Strucutre containing all the Evpn global parameters*/
typedef struct _tBgp4EvpnGlobalData {
    tMemPoolId          VrfChgNodePoolId; /* For VRF configuration and
                                                * statistics */
    tMemPoolId          RrImportTargetsSetMemPoolId; /* For RR Targets SET */
    tTMO_SLL            RrImportTargetsSet; /* RR's Import Targets SET */
    tTMO_HASH_TABLE    *pVrfGlobalInfo; /* Hash table that contains VRFs
                                         * information */
    /* Linear list holding vrfs for which export targets are changed */
    tTMO_SLL            BGP4VrfChngList;
    tBgp4RegisterASNInfo   Bgp4EvpnRegisterASNInfo;
    UINT4               u4RrImportTargetsSetCnt; /* Counter to keep track of
                                                  * RR's targets Set */
    UINT4               u4RrRTDelInterval;
} tBgp4EvpnGlobalData;

typedef struct _tEvpnNlriInfo{
    UINT1         au1RouteDistinguisher[MAX_LEN_RD_VALUE];
    UINT1         au1EthernetSegmentID[MAX_LEN_ETH_SEG_ID];
    UINT1         u1RouteType;
    UINT1         u1MacAddrLen;
    UINT4         u4EthernetTag;
    UINT4         u4VnId;
    UINT1         au1IpAddr[BGP4_EVPN_IP6_ADDR_SIZE];
    tMacAddr      MacAddress;
    UINT1         u1IpAddrLen;
    BOOL1         bIsMacDuplicate;
    UINT1         u1MacType;
    UINT1         u1Pad[3];
}tEvpnNlriInfo;

/* tBgp4RrImportTargetInfo is used to store the route-targets information
 * that have been received by the speaker from the client peers.
 */
typedef struct _tBgp4EvpnRrImportTargetInfo {
    tTMO_SLL_NODE   NextTarget; /* pointer to the next import route target */
    UINT1           au1ImportTarget[MAC_IMPORT_ROUTE_LEN]; /* route-target */
    UINT4           u4TimeStamp; /* Time at which the route count associated
                                  * with this target has become zero and will
                                  * be incremented till the predefined delay
                                  * before this target is removed from the set
                                  */
    UINT4           u4RouteCount; /* Route count associated with this import
                                   * route target
                                   */
} tBgp4EvpnRrImportTargetInfo;

INT4    EvpnRouteTblCmpFunc (tBgp4RBElem *, tBgp4RBElem *);
INT4    BgpMacDupTimerTblCmpFunc (tBgp4RBElem *, tBgp4RBElem *);
#define BGP4_EVPN_GLOBAL_PARAMS     (gBgpNode.Bgp4EvpnGlobalData)
#define BGP4_EVPN_PEER_ROLE(pPeerentry) (pPeerentry->peerConfig.u1PeerRole)
#define BGP4_EVPN_VRF_NODE_CHG_MEMPOOL_ID   \
                    ((BGP4_EVPN_GLOBAL_PARAMS).VrfChgNodePoolId)

#define BGP4_EVPN_VRF_STS_CHNG_LIST \
                    (&((BGP4_EVPN_GLOBAL_PARAMS).BGP4VrfChngList))
#define BGP4_EVPN_RT_VRF_SET_FLAG(pRtVrf,u4_flag)  \
                    (pRtVrf->u4Flags |= (u4_flag))

#define BGP4_EVPN_RR_IMPORT_TARGET_RTCNT(pBgp4EvpnRrImportTargetInfo) \
             (pBgp4EvpnRrImportTargetInfo->u4RouteCount)
#define BGP4_EVPN_RR_IMPORT_TARGET_TIMESTAMP(pBgp4EvpnRrImportTargetInfo) \
             (pBgp4EvpnRrImportTargetInfo->u4TimeStamp)
#define BGP4_EVPN_RR_IMPORT_TARGET(pBgp4EvpnRrImportTargetInfo) \
                (pBgp4EvpnRrImportTargetInfo->au1ImportTarget)
#define BGP4_EVPN_RR_TARGETS_SET    \
                    (&((BGP4_EVPN_GLOBAL_PARAMS).RrImportTargetsSet))
#define BGP4_EVPN_RR_TARGETS_INTERVAL   \
                    ((BGP4_EVPN_GLOBAL_PARAMS).u4RrRTDelInterval)
#define BGP4_EVPN_RR_TARGETS_SET_MEMPOOL_ID ((BGP4_EVPN_GLOBAL_PARAMS).RrImportTargetsSetMemPoolId)
#define BGP4_EVPN_RR_TARGETS_SET_CNT    \
                    ((BGP4_EVPN_GLOBAL_PARAMS).u4RrImportTargetsSetCnt)
#define BGP4_EVPN_REGISTER_ASN_INFO(x)  x.Bgp4EvpnRegisterASNInfo


#define BGP4_IS_MAC_MOB_EXT_COMMUNITY(u2EcommType) \
        ((u2EcommType == 0x0600) ? BGP4_TRUE : BGP4_FALSE)

#endif /* BGEVPNDFS_H */
