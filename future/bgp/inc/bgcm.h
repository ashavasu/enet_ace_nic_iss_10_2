/********************************************************************
 *   Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *  
 *   $Id: bgcm.h,v 1.14 2016/12/27 12:35:52 siva Exp $
 *  
 *   Description: BGP common definitions 
 *  
 ********************************************************************/

#ifndef _BGCM_H
#define _BGCM_H

/* Constant used in the Community Attribute module */

#define       COMM_TYPE_CODE                                 BGP4_ATTR_COMM
#define       PATH_ATTRIBUTE_TYPE_SIZE                       0x2
#define       COMM_SUCCESS                                   BGP4_SUCCESS
#define       COMM_FAILURE                                   BGP4_FAILURE
#define       COMM_TRUE                                      BGP4_TRUE
#define       COMM_FALSE                                     BGP4_FALSE
#define       COMM_PEER_SEND                                 COMM_TRUE
#define       COMM_PEER_DO_NOT_SEND                          COMM_FALSE
#define       COMM_ACCEPT                                    0x1
#define       COMM_DENY                                      0x2
#define       COMM_IGNORE                                    0x4
#define       COMM_ADVERTISE                                 0x1
#define       COMM_FILTER                                    0x2
#define       ACCEPT_INCOMING_COMM                           COMM_ACCEPT
#define       DENY_INCOMING_COMM                             COMM_DENY
#define       IGNORE_INCOMING_COMM                           COMM_IGNORE
#define       ADVERTISE_OUTGOING_COMM                        COMM_ADVERTISE
#define       FILTER_OUTGOING_COMM                           COMM_FILTER
#define       IGNORE_OUTGOING_COMM                           COMM_IGNORE
#define       COMM_PATH_EXISTS                               COMM_TRUE
#define       COMM_PATH_DOES_NOT_EXIST                       COMM_FALSE
#define       COMM_PATH_LEN_VALID                            COMM_TRUE
#define       COMM_PATH_LEN_INVALID                          COMM_FALSE
#define       INTERNET                                       0
#define       NO_EXPORT                                      0xFFFFFF01
#define       NO_ADVERTISE                                   0xFFFFFF02
#define       NO_EXPORT_SUBCONFED                            0xFFFFFF03
#define       COMM_INTERNET                                  0
#define       AGGR_AND_ATOMIC_AGGR_UNSET                     COMM_TRUE
#define       AGGR_AND_ATOMIC_AGGR_SET                       COMM_FALSE
#define       ROUTE_HAS_CONF_COMM                            COMM_TRUE
#define       ROUTE_HAS_NO_CONF_COMM                         COMM_FALSE
#define       SINGLE_FEASIBLE_NLRI_LEN                       5
#define       SINGLE_FEASIBLE_NLRI_LEN                       5
#define       COMM_VALUE_LEN                                 4
#define       MIN_LEN_OF_SINGLE_COMM_PATH                    7

#define       MIN_CONFIG_COMM_VAL                            0x00010000
#define       MAX_CONFIG_COMM_VAL                            0xfffeffff

#define       COMM_INVALID_POLICY                            0xFF
#define       COMM_PARTIAL_BIT_SET                           1
#define       COMM_PARTIAL_BIT_UNSET                         0
#define       COMM_INVALID                                   COMM_INVALID_POLICY

#define       MAX_INPUT_COMM_FILTER_TBL_ENTRIES      50000
#define       MAX_OUTPUT_COMM_FILTER_TBL_ENTRIES     50000
#define       COMM_SINGLE_COMMUNITY                  1
#define       COMM_AGGR_RT_MAX_COMM                  100
#define       MAX_BGP_ATTR_LEN                       1000
#define       MAX_BGP_CLI_MSG_BUF_LEN                    660
#define       MIN_BGP_COMMUNITY_VALUE                65535


/* ********** All type definitions of Community Attribute Module *********** */ 

/* Definition of Community filter information node data structure */
typedef struct t_CommFilterInfo {
tTMO_SLL_NODE  CommInfoNext;  /* Next community information node            */
UINT4          u4Comm;        /* Holds the Community value                  */
UINT4          u4Context;     /* Context of the Community                   */
UINT1          u1RowStatus;   /* Row Status of this entry                   */
INT1           i1FilterPolicy;/* Holds the filtering policy for outgoing OR */
                              /* incoming direction. For incoming direction */
                              /* value is COMM_DENY or COMM_ACCEPT.         */
                              /* For outgoing direction value is            */
                              /* COMM_ADVERTISE or COMM_FILTER.             */
INT1           ai1Padding[2]; /* for word alignment of the data structure   */
} tCommFilterInfo;

/* Definition of Community node data structure */
typedef struct t_CommProfile {
tTMO_SLL_NODE CommProfileNext; /* Next community profile node               */
UINT4         u4Comm;          /* Community the route is associated with    */
UINT1         u1RowStatus;      /* Row Status of this entry               */  
INT1          ai1Padding[3];    /* For word alignment of data structure   */
} tCommProfile;

/* Definition of Route's List of Configured Communities data structure */
typedef struct t_RouteConfComm {
tTMO_SLL_NODE  RouteConfCommNext;  /* Next route community configuration    */
                                   /* node                                  */
tTMO_SLL       TSConfComms;        /* List that holds the communities       */
                                   /* configured for the route. It is an    */
                                   /* SLL of type tCommProfile              */ 
tNetAddress    RtNetAddress;       /* Netaddress                            */
UINT4          u4Context;          /* Context of the configured community   */
} tRouteConfComm;

/* Definition of Route's List of associated Communities data structure */
typedef struct t_RouteCommPath {
UINT4   au4Comm[BGP4_DEFAULT_MAX_NUM_COMM_PER_ROUTE]; 
                                 /* Array that holds communities of route */
UINT2   u2CommCount;             /* Count of communities for the route    */
UINT2   u2CommAdded;             /* Count of communities added for route  */
UINT2   u2CommDeleted;           /* Count of communities deleted from route*/
INT1    i1CommPartial;           /* This indicates whether community path */
                                 /* information is complete or incomplete */
INT1    i1ForWordAlign;          /* For word alignment of data structure  */
} tRouteCommPath;

/* Definition of Route's Community Set Status data structure */
typedef struct  t_RouteCommSetStatus {
tTMO_SLL_NODE  RouteConfCommNext;  /* Next route's community set config     */
                                   /* node                                  */
tNetAddress    RtNetAddress;       /* Netaddress                            */
UINT4          u4Context;          /* Context of the set community          */
INT1           i1CommSetStatus;    /* It indicates whether the existing     */
                                   /* communities are to be considered or   */ 
                                   /* not. It takes the value               */
                                   /* set/setnone/modify. set is to set only*/  
                                   /* the additive communities. setnone is  */
                                   /* for sending route without communities.*/
                                   /* Modify is to add to additive comms.   */
                                   /* & delete conf comms.in rcvd comm info */
UINT1         u1RowStatus;         /* Row Status of this entry              */  
INT1          ai1Padding[2];       /* For word alignment of data structure  */
} tRouteCommSetStatus ;

typedef struct t_BgpCliMemblkBuf{
UINT4 au1CliMsgBuf[MAX_BGP_CLI_MSG_BUF_LEN]; 
} tBgpCliMemBlkBuf;

/* Definition of BGP community params data structure */
typedef struct t_Bgp4CommParams{
tMemPoolId        CommInputFilterPoolId;  /* Memory Pool of tCommFilterInfo  */
                                          /* memory units                    */
tMemPoolId        CommOutputFilterPoolId; /* Memory Pool of tCommFilterInfo  */
                                          /* memory units                    */
tMemPoolId        RouteConfCommPoolId;    /* Memory Pool of tRouteConfComm   */
                                          /* memory units                    */
tMemPoolId        CommProfilePoolId;      /* Memory Pool of tCommProfile     */
                                          /* memory units                    */
tMemPoolId        RouteCommSetStatusPoolId;/*Memory Pool of                  */
                                          /* tRouteCommSetStatus.            */
                                          /* memory units                    */
tMemPoolId        BgpCliMemblkBufPoolId;  /* Memory Pool of tBgpCliMemBlkBuf */
                                          /* memoy unit                      */
tMemPoolId        AggrNodePoolId;        /*Memory Pool of                  */
                                         /*tAggregator memory unit          */ 
tMemPoolId        ClusterListPoolId;     /* Memory Pool of tClusterList      */
                                         /*Memory Units                     */
tMemPoolId        CommunityNodePoolId;   /* Memory Pool of tCommunity       */
                                         /* Memory Units                    */
tMemPoolId        ExtCommunityNodePoolId;/*Memory Pool of tExtCommunity      */ 
                                          /* memory units                   */
tMemPoolId        AfiSafiNodePoolId;     /*Memory Pool of tAfiSafiNode      */
                                         /* Memory units                    */
tMemPoolId        AttrNodePoolId;        /*Memmory pool Attribute node      */
                                         /*Memory units                     */
tMemPoolId        BgpPeerNodePoolId;      /*Memory Pool of tPeerNode */
                                          /* memoy unit              */
tBgp4RBTree       pRoutesCommAddTbl;     /* Pointer to the Hash table that  */
                                         /* holds the routes with configured*/
                                         /* communities to be added         */
                                         /* It holds list of tRouteConfComm */
tBgp4RBTree       pRoutesCommDeleteTbl;  /* Pointer to the Hash table that  */
                                         /* holds the routes with configured*/
                                         /* communities to be deleted       */
                                         /* It holds list of tRouteConfComm */
tBgp4RBTree       pCommInputFilterTbl;    /* Pointer to the Hash table that  */
                                          /* holds input filter policy of    */
                                          /* communities.  It is list of     */
                                          /* tCommFilterInfo.                */ 
tBgp4RBTree       pCommOutputFilterTbl;   /* Pointer to the Hash table that  */
                                          /* holds output filter policy of   */
                                          /* communities.  It is list of     */
                                          /* tCommFilterInfo.                */
tBgp4RBTree       pRouteCommSetStatusTbl; /* Pointer to the Hash table that  */
                                          /* holds set status of             */
                                          /* communities.  It is list of     */
                                          /* tRouteCommSetStatus.            */
} tBgp4CommParams;

/* ************** Macros ***************************************/ 

#define       COMM_MAX_ROUTES              Bgp4GetMaxRoutes ()
#define       COMM_MAX_PEERS               Bgp4GetMaxPeers ()
#define       COMM_GLOBAL_PARAMS           (gBgpNode.Bgp4CommParams)
#define       ROUTES_COMM_ADD_TBL  ((COMM_GLOBAL_PARAMS).pRoutesCommAddTbl)
#define       ROUTES_COMM_DELETE_TBL      \
                                ((COMM_GLOBAL_PARAMS).pRoutesCommDeleteTbl)
#define       COMM_INPUT_FILTER_TBL      \
                                 ((COMM_GLOBAL_PARAMS).pCommInputFilterTbl)
#define       COMM_OUTPUT_FILTER_TBL   \
                                ((COMM_GLOBAL_PARAMS).pCommOutputFilterTbl)
#define       ROUTES_COMM_SET_STATUS_TBL \
                     ((COMM_GLOBAL_PARAMS).pRouteCommSetStatusTbl)
#define       INPUT_COMM_FILTER_ENTRY_POOL_ID \
                               ((COMM_GLOBAL_PARAMS).CommInputFilterPoolId)
#define       OUTPUT_COMM_FILTER_ENTRY_POOL_ID  \
                              ((COMM_GLOBAL_PARAMS).CommOutputFilterPoolId)
#define       ROUTE_CONF_COMM_POOL_ID     \
                                 ((COMM_GLOBAL_PARAMS).RouteConfCommPoolId)
#define       COMM_PROFILE_POOL_ID      \
                                  ((COMM_GLOBAL_PARAMS).CommProfilePoolId)
#define       ROUTES_COMM_SET_STATUS_POOL_ID\
                                ((COMM_GLOBAL_PARAMS).RouteCommSetStatusPoolId)
#define       BGP_CLI_MEM_BLK_BUF_POOL_ID\
                                ((COMM_GLOBAL_PARAMS).BgpCliMemblkBufPoolId)
#define       BGP_AGGR_NODE_POOL_ID\
                               ((COMM_GLOBAL_PARAMS).AggrNodePoolId)
#define       BGP_CLUSTER_LIST_POOL_ID \
                               ((COMM_GLOBAL_PARAMS).ClusterListPoolId)
#define       BGP_COMMUNITY_NODE_POOL_ID \
                               ((COMM_GLOBAL_PARAMS).CommunityNodePoolId)
#define       BGP_EXT_COMMUNITY_NODE_POOL_ID \
                               ((COMM_GLOBAL_PARAMS).ExtCommunityNodePoolId)
#define       BGP_AFI_SAFI_NODE_POOL_ID \
                               ((COMM_GLOBAL_PARAMS).AfiSafiNodePoolId)
#define       BGP_ATTRIBUTE_LEN_POOL_ID \
                               ((COMM_GLOBAL_PARAMS).AttrNodePoolId)
#define       BGP_PEER_NODE_POOL_ID\
                                 ((COMM_GLOBAL_PARAMS).BgpPeerNodePoolId)
#define       COMM_NET_ADDRESS_IN_ROUTE_CONF_STRUCT(pRouteConfComm)\
                    (pRouteConfComm->RtNetAddress)
#define       COMM_NET_ADDRESS_IN_ROUTE_SET_STATUS_STRUCT(pRouteSetStatus)\
                    (pRouteSetStatus->RtNetAddress)
#define       COMM_PEER_INFO_IN_PEER_SET_STATUS_STRUCT(pPeerSetStatus)\
                    (pPeerSetStatus->PeerAddress)
#define       COMM_RT_IP_ADDRESS_IN_ROUTE_CONF_STRUCT(pRouteConfComm)\
                    (((pRouteConfComm->RtNetAddress).NetAddr).au1Address)
#define       COMM_RT_PREFIXLEN_IN_ROUTE_CONF_STRUCT(pRouteConfComm)\
                    ((pRouteConfComm->RtNetAddress).u2PrefixLen)
#define       COMM_RT_IP_ADDRESS_IN_ROUTE_SET_STATUS_STRUCT(pRouteSetStatus)\
                    (((pRouteSetStatus->RtNetAddress).NetAddr).au1Address)
#define       COMM_RT_PREFIXLEN_IN_ROUTE_SET_STATUS_STRUCT(pRouteSetStatus)\
                    ((pRouteSetStatus->RtNetAddress).u2PrefixLen)
#define       COMM_LEN_ERROR_STATS(u4CxtId) (gBgpCxtNode[u4CxtId]->u4CommLenError)
#define       COMM_BGP4_GET_ATTRIBUTE_CODE(u2AttribType) \
                                           (u2AttribType & 0x00ff)
#define       BGP4_IS_COMM_PATH_LEN_VALID(u2CommPathLen)  \
             ((u2CommPathLen % 4) ? COMM_PATH_LEN_INVALID : COMM_PATH_LEN_VALID)
#define       IS_ROUTE_AGGREATE_AND_ATOMIC_AGGR_UNSET(pRouteInfo) \
              (((pRouteInfo->u2AttrFlag & \
                  (BGP4_ATTR_AGGREGATOR_MASK | BGP4_ATTR_ATOMIC_AGGR_MASK))  \
                        == BGP4_ATTR_AGGREGATOR_MASK ) \
                    ? COMM_TRUE : COMM_FALSE)
#define       COMM_GET_HASH_INDEX CommGetHashIndex

#define       COMM_INPUT_FILTER_ENTRY_CREATE(pCommFilterInfo) \
 pCommFilterInfo = (tCommFilterInfo *) MemAllocMemBlk ( \
  INPUT_COMM_FILTER_ENTRY_POOL_ID);

#define       COMM_INPUT_FILTER_ENTRY_FREE(pCommFilterInfo) MemReleaseMemBlock (INPUT_COMM_FILTER_ENTRY_POOL_ID, (UINT1 *) pCommFilterInfo)

#define       COMM_OUTPUT_FILTER_ENTRY_CREATE(pCommFilterInfo) \
 pCommFilterInfo = (tCommFilterInfo *) MemAllocMemBlk (\
 OUTPUT_COMM_FILTER_ENTRY_POOL_ID);

#define       COMM_OUTPUT_FILTER_ENTRY_FREE(pCommFilterInfo) MemReleaseMemBlock (OUTPUT_COMM_FILTER_ENTRY_POOL_ID, (UINT1 *) pCommFilterInfo)

#define       COMM_ROUTES_ADD_TBL_ENTRY_CREATE(pRouteConfComm) \
 pRouteConfComm = (tRouteConfComm *) MemAllocMemBlk (\
 ROUTE_CONF_COMM_POOL_ID);

#define       COMM_ROUTES_ADD_TBL_ENTRY_FREE(pRouteConfComm) MemReleaseMemBlock (ROUTE_CONF_COMM_POOL_ID, (UINT1 *) pRouteConfComm)

#define       COMM_ROUTES_DELETE_TBL_ENTRY_CREATE(pRouteConfComm) \
 pRouteConfComm = (tRouteConfComm*)MemAllocMemBlk (ROUTE_CONF_COMM_POOL_ID);

#define       COMM_ROUTES_DELETE_TBL_ENTRY_FREE(pRouteConfComm) MemReleaseMemBlock (ROUTE_CONF_COMM_POOL_ID, (UINT1 *) pRouteConfComm)

#define       COMM_PROFILE_CREATE(pCommProfile) \
  pCommProfile = (tCommProfile *) MemAllocMemBlk (COMM_PROFILE_POOL_ID);

#define       COMM_PROFILE_FREE(pCommProfile) MemReleaseMemBlock (COMM_PROFILE_POOL_ID , (UINT1 *) pCommProfile)

#define       AGGR_NODE_CREATE(pAggrNode) \
                     pAggrNode = (tAggregator *) MemAllocMemBlk (BGP_AGGR_NODE_POOL_ID);

#define       AGGR_NODE_FREE(pAggrNode) MemReleaseMemBlock (BGP_AGGR_NODE_POOL_ID, (UINT1 *) pAggrNode)

#define       CLUSTER_LIST_CREATE(pClusterList) \
                     pClusterList = (tClusterList *) (VOID *) MemAllocMemBlk (BGP_CLUSTER_LIST_POOL_ID);

#define       CLUSTER_LIST_FREE(pClusterList) MemReleaseMemBlock (BGP_CLUSTER_LIST_POOL_ID, (UINT1 *)pClusterList)

#define       COMMUNITY_NODE_CREATE(pCommNode) \
                     pCommNode = (tCommunity *) (VOID *) MemAllocMemBlk (BGP_COMMUNITY_NODE_POOL_ID);

#define       COMMUNITY_NODE_FREE(pCommNode) MemReleaseMemBlock (BGP_COMMUNITY_NODE_POOL_ID, (UINT1 *)pCommNode); \
              pCommNode = NULL;

#define       EXT_COMMUNITY_NODE_CREATE(pExtCommNode)\
                     pExtCommNode = (tExtCommunity *) MemAllocMemBlk (BGP_EXT_COMMUNITY_NODE_POOL_ID);

#define       EXT_COMMUNITY_NODE_FREE(pExtCommNode) MemReleaseMemBlock (BGP_EXT_COMMUNITY_NODE_POOL_ID, (UINT1 *)pExtCommNode)

#define       AFI_SAFI_NODE_CREATE(pAfiSafiNode) \
                     pAfiSafiNode = (tAfiSafiNode *) MemAllocMemBlk (BGP_AFI_SAFI_NODE_POOL_ID);

#define       AFI_SAFI_NODE_FREE(pAfiSafiNode) MemReleaseMemBlock (BGP_AFI_SAFI_NODE_POOL_ID, (UINT1 *)pAfiSafiNode)

#define       ATTRIBUTE_NODE_CREATE(pAttrLen) \
                     pAttrLen = (UINT1 *) MemAllocMemBlk (BGP_ATTRIBUTE_LEN_POOL_ID);

#define      ATTRIBUTE_NODE_FREE(pAttrLen) \
    if(pAttrLen != NULL) \
{ \
    MemReleaseMemBlock (BGP_ATTRIBUTE_LEN_POOL_ID, (UINT1 *)pAttrLen); \
    pAttrLen = NULL; \
} 
#define       COMM_ROUTES_COMM_SET_STATUS_ENTRY_CREATE(pRouteCommSetSts) \
 pRouteCommSetSts = (tRouteCommSetStatus *)MemAllocMemBlk (ROUTES_COMM_SET_STATUS_POOL_ID)

#define       COMM_ROUTES_COMM_SET_STATUS_ENTRY_FREE(pRouteCommSetSts) MemReleaseMemBlock ( ROUTES_COMM_SET_STATUS_POOL_ID, (UINT1 *) pRouteCommSetSts)

#define  BGP_CLI_MEM_BLK_ALLOC(pBgpCliAlloc,type) \
          {\
          pBgpCliAlloc = (type *) (VOID *) MemAllocMemBlk (BGP_CLI_MEM_BLK_BUF_POOL_ID); \
          if (pBgpCliAlloc != NULL)\
            MEMSET(pBgpCliAlloc,'\0',MAX_BGP_CLI_MSG_BUF_LEN); \
          }

#define  BGP_CLI_MEM_BLK_FREE(pBgpcliFree) \
             MemReleaseMemBlock (BGP_CLI_MEM_BLK_BUF_POOL_ID, (UINT1 *)pBgpcliFree)

#define       COMM_ROUTE_CONF_COMM_NODE_CREATE(pRouteConfComm)    \
 pRouteConfComm = (tRouteConfComm *) MemAllocMemBlk ( ROUTE_CONF_COMM_POOL_ID);

#define       COMM_ROUTE_CONF_COMM_NODE_FREE(pRouteConfComm)    MemReleaseMemBlock ( ROUTE_CONF_COMM_POOL_ID, (UINT1 *)  pRouteConfComm)


#define       COMM_COMM_PROFILE_NODE_CREATE     COMM_PROFILE_CREATE

#define       COMM_COMM_PROFILE_NODE_FREE       COMM_PROFILE_FREE

#define COMM_HASH_DYN_Scan_Bucket(pHashTab, u1HashKey, pNode, pTempNode, TypeCast)  BGP_SLL_DYN_Scan (&((pHashTab)->HashList[u1HashKey]), pNode, pTempNode, TypeCast)

#define       COMM_SLL_DYN_Scan     BGP_SLL_DYN_Scan
#define       COMM_ROUTES_LIST_FREE(pRouteConfComm)    \
              COMM_SLL_DYN_Scan(&(pRouteConfComm->TSConfComms),\
                          pCommProf, pTmpCommProf, tCommProfile *)     \
             {  \
                 COMM_PROFILE_FREE(pCommProf);          \
             }  \
             TMO_SLL_Init(&(pRouteConfComm->TSConfComms));

#define       COMM_ROUTES_ADD_LIST_FREE(pRouteConfComm) \
              COMM_ROUTES_LIST_FREE(pRouteConfComm)

#define       COMM_ROUTES_DELETE_LIST_FREE(pRouteConfComm) \
              COMM_ROUTES_LIST_FREE(pRouteConfComm)

#define  BGP_PEER_NODE_CREATE(pPeerNode)   \
              pPeerNode = (tPeerNode *) MemAllocMemBlk (BGP_PEER_NODE_POOL_ID);

#define  BGP_PEER_NODE_FREE(pPeerNode)  \
            MemReleaseMemBlock (BGP_PEER_NODE_POOL_ID, (UINT1 *)  pPeerNode)


#define       COMM_SNMP_SET_INPUT_FILTER_POLICY(pCommFilterInfo, i4SetInVal) \
                    pCommFilterInfo->i1FilterPolicy = (INT1) (i4SetInVal -1 )

#define       COMM_SNMP_GET_INPUT_FILTER_POLICY(i4GetInVal, pCommFilterInfo) \
                    i4GetInVal = ( pCommFilterInfo->i1FilterPolicy +1 )

#define       COMM_SNMP_SET_OUTPUT_FILTER_POLICY(pCommFilterInfo, i4SetOutVal) \
                    pCommFilterInfo->i1FilterPolicy = (INT1) (i4SetOutVal -1 )

#define       COMM_SNMP_GET_OUTPUT_FILTER_POLICY(i4GetOutVal, pCommFilterInfo) \
                    i4GetOutVal = ( pCommFilterInfo->i1FilterPolicy +1 )

      
/* Function Prototypes */

INT4 CommInit (INT4 i4MaxRoutes);
INT4 CommShut (VOID);
INT4 CommApplyRoutesInboundFilterPolicy (tRouteProfile *pFeasibleRoute);
INT4 CommFormListForInst (tTMO_SLL *pFeasibleRoutesList,
                          tBgp4PeerEntry * pPeerInfo);
INT4 CommApplyOutboundFilterPolicy (tRouteProfile    *pRtProfile,
                                    tBgp4PeerEntry   *pPeerInfo);
INT1 CommGetInputFilterStatus (tRouteProfile *pFeasibleRoute,
                                UINT2         u2CommPathStartOffSet);
INT1 CommGetOutputFilterStatus (tRouteProfile *pBestRoute,
                                UINT2         u2CommPathStartOffSet);
INT4 GetAdvtStatus (tRouteProfile *pRoute ,
                    tBgp4PeerEntry *pPeerInfo,
                    INT1 *pi1AdvtStatus);
INT4 CommGetUpdCommAttrib (tRouteProfile *pSelRouteProfile,
                           tBgp4Info *pAdvtBgpInfo, tBgp4Info *);
INT4 CommFormRouteCommPath (tRouteProfile *pSelRouteProfile,
                            tBgp4Info *pAdvtBgpInfo, tBgp4Info *);
INT1 IsCommWellKnown (const tRouteProfile *pRoute,
                      UINT2 u2CommPathStartOffSet,
                      UINT4 *pu4WellKnownComm);
INT1 ApplyOutgoingCommFilter (const tRouteProfile *pBestRoute,
                              UINT2         u2CommPathStartOffSet);
INT1 ApplyIncomingCommFilter (const tRouteProfile *pFeasibleRoute,
                              UINT2         u2CommPathStartOffSet);

INT4 CommUpdateAggrComm (tRouteProfile *pAggrRtProfile,
                         tRouteProfile *pNewRtProfile);
INT4 CommUpdateRouteCommAdd (tRouteProfile *pSelRouteProfile,
                             tBgp4Info *pAdvtBgpInfo);
INT1 CommIsRouteCommDelete (tRouteProfile *pSelRouteProfile,UINT4 u4CommVal);
INT1 CommGetCommSetStatus (tRouteProfile *pSelRouteProfile);
INT1 NoExportHandler  (const tBgp4PeerEntry *pPeerInfo);
INT1 NoExportSubConfHandler (const tBgp4PeerEntry *pPeerInfo);
UINT2 FillCalcCommInUpdMesg (UINT1 *pu1UpdateMesg,
                             tBgp4Info *pAdvtBgpInfo);

VOID CommGetHashIndex (UINT4 u4HashKey1, UINT1 u1HashKey2, UINT4 *pu4HashIndex);

INT4 Bgp4GetMaxPeers(VOID);
INT4 CommRtTblCmpFunc (tBgp4RBElem *, tBgp4RBElem *);
INT4 CommRtStatTblCmpFunc (tBgp4RBElem *, tBgp4RBElem *);
INT4 CommFiltTblCmpFunc (tBgp4RBElem *, tBgp4RBElem *);

#ifdef ROUTEMAP_WANTED
INT1 Bgp4UpdateRouteInfo PROTO ((tRtMapInfo *, tRouteProfile*, UINT4));
INT1 Bgp4ApplyInOutFilter PROTO (( tFilteringRMap*, tRouteProfile*, UINT4 ));
INT1 Bgp4ApplyPrefixFilter PROTO((tFilteringRMap *, tRouteProfile *));
#endif /* ROUTEMAP_WANTED */

#endif /* End of _BGCM_H */
