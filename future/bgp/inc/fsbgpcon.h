/* $Id: fsbgpcon.h,v 1.6 2013/06/23 13:14:04 siva Exp $ */
# ifndef fsbgpOCON_H
# define fsbgpOCON_H
/*
 *  The Constant Declarations for
 *  fsbgp4Scalars
 */

# define FSBGP4GLOBALADMINSTATUS                           (1)
# define FSBGP4LOCALAS                                     (2)
# define FSBGP4IDENTIFIER                                  (3)
# define FSBGP4SYNCHRONIZATION                             (4)
# define FSBGP4DEFAULTLOCALPREF                            (5)
# define FSBGP4ADVTNONBGPRT                                (6)
# define FSBGP4TRACEENABLE                                 (7)
# define FSBGP4DEBUGENABLE                                 (8)
# define FSBGP4OVERLAPPINGROUTE                            (9)
# define FSBGP4MAXPEERENTRY                                (10)
# define FSBGP4MAXNOOFROUTES                               (11)
# define FSBGP4ALWAYSCOMPAREMED                            (12)
# define FSBGP4DEFAULTORIGINATE                            (13)
# define FSBGP4DEFAULTIPV4UNICAST                          (14)
# define FSBGP4FOURBYTEASNSUPPORTSTATUS                    (15)
# define FSBGP4FOURBYTEASNNOTATIONTYPE                     (16)

/*
 *  The Constant Declarations for
 *  fsbgp4MpeBgpPeerTable
 */

# define FSBGP4MPEBGPPEERREMOTEADDRTYPE                    (1)
# define FSBGP4MPEBGPPEERIDENTIFIER                        (2)
# define FSBGP4MPEBGPPEERSTATE                             (3)
# define FSBGP4MPEBGPPEERADMINSTATUS                       (4)
# define FSBGP4MPEBGPPEERNEGOTIATEDVERSION                 (5)
# define FSBGP4MPEBGPPEERLOCALADDR                         (6)
# define FSBGP4MPEBGPPEERLOCALPORT                         (7)
# define FSBGP4MPEBGPPEERREMOTEADDR                        (8)
# define FSBGP4MPEBGPPEERREMOTEPORT                        (9)
# define FSBGP4MPEBGPPEERREMOTEAS                          (10)
# define FSBGP4MPEBGPPEERINUPDATES                         (11)
# define FSBGP4MPEBGPPEEROUTUPDATES                        (12)
# define FSBGP4MPEBGPPEERINTOTALMESSAGES                   (13)
# define FSBGP4MPEBGPPEEROUTTOTALMESSAGES                  (14)
# define FSBGP4MPEBGPPEERLASTERROR                         (15)
# define FSBGP4MPEBGPPEERFSMESTABLISHEDTRANSITIONS         (16)
# define FSBGP4MPEBGPPEERFSMESTABLISHEDTIME                (17)
# define FSBGP4MPEBGPPEERCONNECTRETRYINTERVAL              (18)
# define FSBGP4MPEBGPPEERHOLDTIME                          (19)
# define FSBGP4MPEBGPPEERKEEPALIVE                         (20)
# define FSBGP4MPEBGPPEERHOLDTIMECONFIGURED                (21)
# define FSBGP4MPEBGPPEERKEEPALIVECONFIGURED               (22)
# define FSBGP4MPEBGPPEERMINASORIGINATIONINTERVAL          (23)
# define FSBGP4MPEBGPPEERMINROUTEADVERTISEMENTINTERVAL     (24)
# define FSBGP4MPEBGPPEERINUPDATEELAPSEDTIME               (25)

/*
 *  The Constant Declarations for
 *  fsbgp4MpeBgp4PathAttrTable
 */

# define FSBGP4MPEBGP4PATHATTRROUTEAFI                     (1)
# define FSBGP4MPEBGP4PATHATTRROUTESAFI                    (2)
# define FSBGP4MPEBGP4PATHATTRPEERTYPE                     (3)
# define FSBGP4MPEBGP4PATHATTRPEER                         (4)
# define FSBGP4MPEBGP4PATHATTRIPADDRPREFIXLEN              (5)
# define FSBGP4MPEBGP4PATHATTRIPADDRPREFIX                 (6)
# define FSBGP4MPEBGP4PATHATTRORIGIN                       (7)
# define FSBGP4MPEBGP4PATHATTRASPATHSEGMENT                (8)
# define FSBGP4MPEBGP4PATHATTRNEXTHOP                      (9)
# define FSBGP4MPEBGP4PATHATTRMULTIEXITDISC                (10)
# define FSBGP4MPEBGP4PATHATTRLOCALPREF                    (11)
# define FSBGP4MPEBGP4PATHATTRATOMICAGGREGATE              (12)
# define FSBGP4MPEBGP4PATHATTRAGGREGATORAS                 (13)
# define FSBGP4MPEBGP4PATHATTRAGGREGATORADDR               (14)
# define FSBGP4MPEBGP4PATHATTRCALCLOCALPREF                (15)
# define FSBGP4MPEBGP4PATHATTRBEST                         (16)
# define FSBGP4MPEBGP4PATHATTRCOMMUNITY                    (17)
# define FSBGP4MPEBGP4PATHATTRORIGINATORID                 (18)
# define FSBGP4MPEBGP4PATHATTRCLUSTERLIST                  (19)
# define FSBGP4MPEBGP4PATHATTREXTCOMMUNITY                 (20)
# define FSBGP4MPEBGP4PATHATTRUNKNOWN                      (21)
# define FSBGP4MPEBGP4PATHATTRLABEL                        (22)
# define FSBGP4MPEBGP4PATHATTRAS4PATHSEGMENT               (23)
# define FSBGP4MPEBGP4PATHATTRAGGREGATORAS4                (24)

/*
 *  The Constant Declarations for
 *  fsbgp4PeerExtTable
 */

# define FSBGP4PEEREXTPEERTYPE                             (1)
# define FSBGP4PEEREXTPEERREMOTEADDR                       (2)
# define FSBGP4PEEREXTCONFIGUREPEER                        (3)
# define FSBGP4PEEREXTPEERREMOTEAS                         (4)
# define FSBGP4PEEREXTEBGPMULTIHOP                         (5)
# define FSBGP4PEEREXTEBGPHOPLIMIT                         (6)
# define FSBGP4PEEREXTTCPSENDBUFSIZE                       (7)
# define FSBGP4PEEREXTTCPRCVBUFSIZE                        (8)
# define FSBGP4PEEREXTNEXTHOPSELF                          (9)
# define FSBGP4PEEREXTLCLADDRESS                           (10)
# define FSBGP4PEEREXTNETWORKADDRESS                       (11)
# define FSBGP4PEEREXTGATEWAY                              (12)
# define FSBGP4PEEREXTRFLCLIENT                            (13)
# define FSBGP4PEEREXTCOMMSENDSTATUS                       (14)
# define FSBGP4PEEREXTECOMMSENDSTATUS                      (15)
# define FSBGP4PEEREXTPASSIVE                              (16)
# define FSBGP4PEEREXTDEFAULTORIGINATE                     (17)
# define FSBGP4PEEREXTACTIVATEMPCAPABILITY                 (18)
# define FSBGP4PEEREXTDEACTIVATEMPCAPABILITY               (19)
# define FSBGP4PEEREXTMPLSVPNVRFASSOCIATED                 (20)
# define FSBGP4PEEREXTMPLSVPNCEROUTETARGETADVT             (21)
# define FSBGP4PEEREXTMPLSVPNCESITEOFORIGIN                (22)

/*
 *  The Constant Declarations for
 *  fsbgp4MEDTable
 */

# define FSBGP4MEDINDEX                                    (1)
# define FSBGP4MEDADMINSTATUS                              (2)
# define FSBGP4MEDREMOTEAS                                 (3)
# define FSBGP4MEDIPADDRAFI                                (4)
# define FSBGP4MEDIPADDRSAFI                               (5)
# define FSBGP4MEDIPADDRPREFIX                             (6)
# define FSBGP4MEDIPADDRPREFIXLEN                          (7)
# define FSBGP4MEDINTERMEDIATEAS                           (8)
# define FSBGP4MEDDIRECTION                                (9)
# define FSBGP4MEDVALUE                                    (10)
# define FSBGP4MEDPREFERENCE                               (11)
# define FSBGP4MEDVRFNAME                                  (12)

/*
 *  The Constant Declarations for
 *  fsbgp4LocalPrefTable
 */

# define FSBGP4LOCALPREFINDEX                              (1)
# define FSBGP4LOCALPREFADMINSTATUS                        (2)
# define FSBGP4LOCALPREFREMOTEAS                           (3)
# define FSBGP4LOCALPREFIPADDRAFI                          (4)
# define FSBGP4LOCALPREFIPADDRSAFI                         (5)
# define FSBGP4LOCALPREFIPADDRPREFIX                       (6)
# define FSBGP4LOCALPREFIPADDRPREFIXLEN                    (7)
# define FSBGP4LOCALPREFINTERMEDIATEAS                     (8)
# define FSBGP4LOCALPREFDIRECTION                          (9)
# define FSBGP4LOCALPREFVALUE                              (10)
# define FSBGP4LOCALPREFPREFERENCE                         (11)
# define FSBGP4LOCALPREFVRFNAME                            (12)

/*
 *  The Constant Declarations for
 *  fsbgp4UpdateFilterTable
 */

# define FSBGP4UPDATEFILTERINDEX                           (1)
# define FSBGP4UPDATEFILTERADMINSTATUS                     (2)
# define FSBGP4UPDATEFILTERREMOTEAS                        (3)
# define FSBGP4UPDATEFILTERIPADDRAFI                       (4)
# define FSBGP4UPDATEFILTERIPADDRSAFI                      (5)
# define FSBGP4UPDATEFILTERIPADDRPREFIX                    (6)
# define FSBGP4UPDATEFILTERIPADDRPREFIXLEN                 (7)
# define FSBGP4UPDATEFILTERINTERMEDIATEAS                  (8)
# define FSBGP4UPDATEFILTERDIRECTION                       (9)
# define FSBGP4UPDATEFILTERACTION                          (10)
# define FSBGP4UPDATEFILTERVRFNAME                         (11)

/*
 *  The Constant Declarations for
 *  fsbgp4AggregateTable
 */

# define FSBGP4AGGREGATEINDEX                              (1)
# define FSBGP4AGGREGATEADMINSTATUS                        (2)
# define FSBGP4AGGREGATEIPADDRAFI                          (3)
# define FSBGP4AGGREGATEIPADDRSAFI                         (4)
# define FSBGP4AGGREGATEIPADDRPREFIX                       (5)
# define FSBGP4AGGREGATEIPADDRPREFIXLEN                    (6)
# define FSBGP4AGGREGATEADVERTISE                          (7)

/*
 *  The Constant Declarations for
 *  fsbgp4RRDGroup
 */

# define FSBGP4RRDADMINSTATUS                              (1)
# define FSBGP4RRDPROTOMASKFORENABLE                       (2)
# define FSBGP4RRDSRCPROTOMASKFORDISABLE                   (3)
# define FSBGP4RRDDEFAULTMETRIC                            (4)

/*
 *  The Constant Declarations for
 *  fsbgp4ImportRouteTable
 */

# define FSBGP4IMPORTROUTEPREFIXAFI                        (1)
# define FSBGP4IMPORTROUTEPREFIXSAFI                       (2)
# define FSBGP4IMPORTROUTEPREFIX                           (3)
# define FSBGP4IMPORTROUTEPREFIXLEN                        (4)
# define FSBGP4IMPORTROUTEPROTOCOL                         (5)
# define FSBGP4IMPORTROUTENEXTHOP                          (6)
# define FSBGP4IMPORTROUTEIFINDEX                          (7)
# define FSBGP4IMPORTROUTEMETRIC                           (8)
# define FSBGP4IMPORTROUTEACTION                           (9)

/*
 *  The Constant Declarations for
 *  fsbgp4FsmTransitionHistTable
 */

# define FSBGP4PEERTYPE                                    (1)
# define FSBGP4PEER                                        (2)
# define FSBGP4FSMTRANSITIONHIST                           (3)

/*
 *  The Constant Declarations for
 *  fsbgp4RflScalars
 */

# define FSBGP4RFLBGPCLUSTERID                             (1)
# define FSBGP4RFLRFLSUPPORT                               (2)

/*
 *  The Constant Declarations for
 *  fsbgp4RfdScalars
 */

# define FSBGP4RFDCUTOFF                                   (1)
# define FSBGP4RFDREUSE                                    (2)
# define FSBGP4RFDCEILING                                  (3)
# define FSBGP4RFDMAXHOLDDOWNTIME                          (4)
# define FSBGP4RFDDECAYHALFLIFETIME                        (5)
# define FSBGP4RFDDECAYTIMERGRANULARITY                    (6)
# define FSBGP4RFDREUSETIMERGRANULARITY                    (7)
# define FSBGP4RFDREUSEINDXARRAYSIZE                       (8)

/*
 *  The Constant Declarations for
 *  fsbgp4RfdRtDampHistTable
 */

# define FSBGP4PATHATTRADDRPREFIXAFI                       (1)
# define FSBGP4PATHATTRADDRPREFIXSAFI                      (2)
# define FSBGP4PATHATTRADDRPREFIX                          (3)
# define FSBGP4PATHATTRADDRPREFIXLEN                       (4)
# define FSBGP4PATHATTRPEERTYPE                            (5)
# define FSBGP4PATHATTRPEER                                (6)
# define FSBGP4RFDRTFOM                                    (7)
# define FSBGP4RFDRTLASTUPDTTIME                           (8)
# define FSBGP4RFDRTSTATE                                  (9)
# define FSBGP4RFDRTSTATUS                                 (10)

/*
 *  The Constant Declarations for
 *  fsbgp4RfdPeerDampHistTable
 */

# define FSBGP4PEERREMOTEIPADDRTYPE                        (1)
# define FSBGP4PEERREMOTEIPADDR                            (2)
# define FSBGP4RFDPEERFOM                                  (3)
# define FSBGP4RFDPEERLASTUPDTTIME                         (4)
# define FSBGP4RFDPEERSTATE                                (5)
# define FSBGP4RFDPEERSTATUS                               (6)

/*
 *  The Constant Declarations for
 *  fsbgp4RfdRtsReuseListTable
 */

# define FSBGP4RTAFI                                       (1)
# define FSBGP4RTSAFI                                      (2)
# define FSBGP4RTIPPREFIX                                  (3)
# define FSBGP4RTIPPREFIXLEN                               (4)
# define FSBGP4RFDRTSREUSEPEERTYPE                         (5)
# define FSBGP4PEERREMADDRESS                              (6)
# define FSBGP4RFDRTREUSELISTRTFOM                         (7)
# define FSBGP4RFDRTREUSELISTRTLASTUPDTTIME                (8)
# define FSBGP4RFDRTREUSELISTRTSTATE                       (9)
# define FSBGP4RFDRTREUSELISTRTSTATUS                      (10)

/*
 *  The Constant Declarations for
 *  fsbgp4RfdPeerReuseListTable
 */

# define FSBGP4RFDPEERREMIPADDRTYPE                        (1)
# define FSBGP4RFDPEERREMIPADDR                            (2)
# define FSBGP4RFDPEERREUSELISTPEERFOM                     (3)
# define FSBGP4RFDPEERREUSELISTLASTUPDTTIME                (4)
# define FSBGP4RFDPEERREUSELISTPEERSTATE                   (5)
# define FSBGP4RFDPEERREUSELISTPEERSTATUS                  (6)

/*
 *  The Constant Declarations for
 *  fsbgp4CommScalars
 */

# define FSBGP4COMMMAXINFTBLENTRIES                        (1)
# define FSBGP4COMMMAXOUTFTBLENTRIES                       (2)

/*
 *  The Constant Declarations for
 *  fsbgp4CommRouteAddCommTable
 */

# define FSBGP4ADDCOMMRTAFI                                (1)
# define FSBGP4ADDCOMMRTSAFI                               (2)
# define FSBGP4ADDCOMMIPNETWORK                            (3)
# define FSBGP4ADDCOMMIPPREFIXLEN                          (4)
# define FSBGP4ADDCOMMVAL                                  (5)
# define FSBGP4ADDCOMMROWSTATUS                            (6)

/*
 *  The Constant Declarations for
 *  fsbgp4CommRouteDeleteCommTable
 */

# define FSBGP4DELETECOMMRTAFI                             (1)
# define FSBGP4DELETECOMMRTSAFI                            (2)
# define FSBGP4DELETECOMMIPNETWORK                         (3)
# define FSBGP4DELETECOMMIPPREFIXLEN                       (4)
# define FSBGP4DELETECOMMVAL                               (5)
# define FSBGP4DELETECOMMROWSTATUS                         (6)

/*
 *  The Constant Declarations for
 *  fsbgp4CommRouteCommSetStatusTable
 */

# define FSBGP4COMMSETSTATUSAFI                            (1)
# define FSBGP4COMMSETSTATUSSAFI                           (2)
# define FSBGP4COMMSETSTATUSIPNETWORK                      (3)
# define FSBGP4COMMSETSTATUSIPPREFIXLEN                    (4)
# define FSBGP4COMMSETSTATUS                               (5)
# define FSBGP4COMMSETSTATUSROWSTATUS                      (6)

/*
 *  The Constant Declarations for
 *  fsbgp4CommInFilterTable
 */

# define FSBGP4INFILTERCOMMVAL                             (1)
# define FSBGP4COMMINCOMINGFILTERSTATUS                    (2)
# define FSBGP4INFILTERROWSTATUS                           (3)

/*
 *  The Constant Declarations for
 *  fsbgp4CommOutFilterTable
 */

# define FSBGP4OUTFILTERCOMMVAL                            (1)
# define FSBGP4COMMOUTGOINGFILTERSTATUS                    (2)
# define FSBGP4OUTFILTERROWSTATUS                          (3)

/*
 *  The Constant Declarations for
 *  fsbgp4ExtCommScalars
 */

# define FSBGP4EXTCOMMMAXINFTBLENTRIES                     (1)
# define FSBGP4EXTCOMMMAXOUTFTBLENTRIES                    (2)

/*
 *  The Constant Declarations for
 *  fsbgp4ExtCommRouteAddExtCommTable
 */

# define FSBGP4ADDEXTCOMMRTAFI                             (1)
# define FSBGP4ADDEXTCOMMRTSAFI                            (2)
# define FSBGP4ADDEXTCOMMIPNETWORK                         (3)
# define FSBGP4ADDEXTCOMMIPPREFIXLEN                       (4)
# define FSBGP4ADDEXTCOMMVAL                               (5)
# define FSBGP4ADDEXTCOMMROWSTATUS                         (6)

/*
 *  The Constant Declarations for
 *  fsbgp4ExtCommRouteDeleteExtCommTable
 */

# define FSBGP4DELETEEXTCOMMRTAFI                          (1)
# define FSBGP4DELETEEXTCOMMRTSAFI                         (2)
# define FSBGP4DELETEEXTCOMMIPNETWORK                      (3)
# define FSBGP4DELETEEXTCOMMIPPREFIXLEN                    (4)
# define FSBGP4DELETEEXTCOMMVAL                            (5)
# define FSBGP4DELETEEXTCOMMROWSTATUS                      (6)

/*
 *  The Constant Declarations for
 *  fsbgp4ExtCommRouteExtCommSetStatusTable
 */

# define FSBGP4EXTCOMMSETSTATUSRTAFI                       (1)
# define FSBGP4EXTCOMMSETSTATUSRTSAFI                      (2)
# define FSBGP4EXTCOMMSETSTATUSIPNETWORK                   (3)
# define FSBGP4EXTCOMMSETSTATUSIPPREFIXLEN                 (4)
# define FSBGP4EXTCOMMSETSTATUS                            (5)
# define FSBGP4EXTCOMMSETSTATUSROWSTATUS                   (6)

/*
 *  The Constant Declarations for
 *  fsbgp4ExtCommInFilterTable
 */

# define FSBGP4EXTCOMMINFILTERCOMMVAL                      (1)
# define FSBGP4EXTCOMMINCOMINGFILTERSTATUS                 (2)
# define FSBGP4EXTCOMMINFILTERROWSTATUS                    (3)

/*
 *  The Constant Declarations for
 *  fsbgp4ExtCommOutFilterTable
 */

# define FSBGP4EXTCOMMOUTFILTERCOMMVAL                     (1)
# define FSBGP4EXTCOMMOUTGOINGFILTERSTATUS                 (2)
# define FSBGP4EXTCOMMOUTFILTERROWSTATUS                   (3)

/*
 *  The Constant Declarations for
 *  fsbgp4PeerLinkBwTable
 */

# define FSBGP4PEERLINKTYPE                                (1)
# define FSBGP4PEERLINKREMADDR                             (2)
# define FSBGP4LINKBANDWIDTH                               (3)
# define FSBGP4PEERLINKBWROWSTATUS                         (4)

/*
 *  The Constant Declarations for
 *  fsbgpCapScalars
 */

# define FSBGP4CAPABILITYSUPPORTAVAILABLE                  (1)
# define FSBGP4MAXCAPSPERPEER                              (2)
# define FSBGP4MAXINSTANCESPERCAP                          (3)
# define FSBGP4MAXCAPDATASIZE                              (4)

/*
 *  The Constant Declarations for
 *  fsbgp4CapSupportedCapsTable
 */

# define FSBGP4CAPPEERTYPE                                 (1)
# define FSBGP4CAPPEERREMOTEIPADDR                         (2)
# define FSBGP4SUPPORTEDCAPABILITYCODE                     (3)
# define FSBGP4SUPPORTEDCAPABILITYLENGTH                   (4)
# define FSBGP4SUPPORTEDCAPABILITYVALUE                    (5)
# define FSBGP4CAPSUPPORTEDCAPSROWSTATUS                   (6)
# define FSBGP4CAPANNOUNCEDSTATUS                          (7)
# define FSBGP4CAPRECEIVEDSTATUS                           (8)
# define FSBGP4CAPNEGOTIATEDSTATUS                         (9)
# define FSBGP4CAPCONFIGUREDSTATUS                         (10)

/*
 *  The Constant Declarations for
 *  fsbgp4StrictCapabilityMatchTable
 */

# define FSBGP4STRICTCAPPEERTYPE                           (1)
# define FSBGP4PEERREMIPADDR                               (2)
# define FSBGP4STRICTCAPABILITYMATCH                       (3)

/*
 *  The Constant Declarations for
 *  fsbgpAscScalars
 */

# define FSBGPASCCONFEDID                                  (1)
# define FSBGPASCCONFEDBESTPATHCOMPAREMED                  (2)

/*
 *  The Constant Declarations for
 *  fsbgpAscConfedPeerTable
 */

# define FSBGPASCCONFEDPEERASNO                            (1)
# define FSBGPASCCONFEDPEERSTATUS                          (2)

/*
 *  The Constant Declarations for
 *  fsbgp4RtRefreshInboundTable
 */

# define FSBGP4RTREFRESHINBOUNDPEERTYPE                    (1)
# define FSBGP4RTREFRESHINBOUNDPEERADDR                    (2)
# define FSBGP4RTREFRESHINBOUNDAFI                         (3)
# define FSBGP4RTREFRESHINBOUNDSAFI                        (4)
# define FSBGP4RTREFRESHINBOUNDREQUEST                     (5)

/*
 *  The Constant Declarations for
 *  fsbgp4RtRefreshStatisticsTable
 */

# define FSBGP4RTREFRESHSTATISTICSPEERTYPE                 (1)
# define FSBGP4RTREFRESHSTATISTICSPEERADDR                 (2)
# define FSBGP4RTREFRESHSTATISTICSAFI                      (3)
# define FSBGP4RTREFRESHSTATISTICSSAFI                     (4)
# define FSBGP4RTREFRESHSTATISTICSRTREFMSGSENTCNTR         (5)
# define FSBGP4RTREFRESHSTATISTICSRTREFMSGTXERRCNTR        (6)
# define FSBGP4RTREFRESHSTATISTICSRTREFMSGRCVDCNTR         (7)
# define FSBGP4RTREFRESHSTATISTICSRTREFMSGINVALIDCNTR      (8)

/*
 *  The Constant Declarations for
 *  fsbgp4TCPMD5AuthTable
 */

# define FSBGP4TCPMD5AUTHPEERTYPE                          (1)
# define FSBGP4TCPMD5AUTHPEERADDR                          (2)
# define FSBGP4TCPMD5AUTHPASSWORD                          (3)
# define FSBGP4TCPMD5AUTHPWDSET                            (4)

/*
 *  The Constant Declarations for
 *  fsbgp4SoftReconfigOutboundTable
 */

# define FSBGP4SOFTRECONFIGOUTBOUNDPEERTYPE                (1)
# define FSBGP4SOFTRECONFIGOUTBOUNDPEERADDR                (2)
# define FSBGP4SOFTRECONFIGOUTBOUNDAFI                     (3)
# define FSBGP4SOFTRECONFIGOUTBOUNDSAFI                    (4)
# define FSBGP4SOFTRECONFIGOUTBOUNDREQUEST                 (5)

/*
 *  The Constant Declarations for
 *  fsbgp4MpePrefixCountersTable
 */

# define FSBGP4MPEPEERREMOTEADDRTYPE                       (1)
# define FSBGP4MPEPEERREMOTEADDR                           (2)
# define FSBGP4MPEPREFIXCOUNTERSAFI                        (3)
# define FSBGP4MPEPREFIXCOUNTERSSAFI                       (4)
# define FSBGP4MPEPREFIXCOUNTERSPREFIXESRECEIVED           (5)
# define FSBGP4MPEPREFIXCOUNTERSPREFIXESSENT               (6)
# define FSBGP4MPEPREFIXCOUNTERSWITHDRAWSRECEIVED          (7)
# define FSBGP4MPEPREFIXCOUNTERSWITHDRAWSSENT              (8)
# define FSBGP4MPEPREFIXCOUNTERSINPREFIXES                 (9)
# define FSBGP4MPEPREFIXCOUNTERSINPREFIXESACCEPTED         (10)
# define FSBGP4MPEPREFIXCOUNTERSINPREFIXESREJECTED         (11)
# define FSBGP4MPEPREFIXCOUNTERSOUTPREFIXES                (12)

/*
 *  The Constant Declarations for
 *  fsbgp4MplsVpnVrfRouteTargetTable
 */

# define FSBGP4MPLSVPNVRFNAME                              (1)
# define FSBGP4MPLSVPNVRFROUTETARGETTYPE                   (2)
# define FSBGP4MPLSVPNVRFROUTETARGET                       (3)
# define FSBGP4MPLSVPNVRFROUTETARGETROWSTATUS              (4)

/*
 *  The Constant Declarations for
 *  fsbgp4MplsVpnVrfRedistributeTable
 */

# define FSBGP4MPLSVPNVRFREDISAFI                          (1)
# define FSBGP4MPLSVPNVRFREDISSAFI                         (2)
# define FSBGP4MPLSVPNVRFREDISPROTOMASK                    (3)

#endif /*  fsbgp4OCON_H  */
