/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgpgr.h,v 1.12 2016/12/27 12:35:53 siva Exp $
 *
 * Description:Contain the prototypes of all the functions used
 *             in the system.
 *
 *******************************************************************/


/*****************************************************************************/
/* Copyright (C) 2009 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : bgpgr.h                                        */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        :                                                */
/*    MODULE NAME           : BGP                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 22 July 2009                                   */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           :  This file contains macros and declarations of */
/*                             functions  related to the BGP features        */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    22 July 2009           Initial Creation                        */
/*---------------------------------------------------------------------------*/
#ifndef  _BGPGR_H
#define  _BGPGR_H

/* Macros related to file handling */
#define BGP4_GR_CONF                      (const CHR1 *)"bgpgr.txt"
/* Maximum length allowed in file
 * operations */
#define MAX_COLUMN_LENGTH                  80       
/* Maximum buffer length */
#define BGP4_MAX_BUFFER_LENGTH             256
/* Maximum temp value used in bgpsnmp.c file */
#define BGP4_MAX_SUBID                     0xf
/* Number of seconds in a year */
#define BGP4_MIB_OID_TABLE_LEN             672
#define BGP4_SEC_IN_YEAR                   86400    
/* Number of seconds in an hour */
#define BGP4_SEC_IN_HOUR                   3600     
/* Number of seconds in a minute */
#define BGP4_SEC_IN_MIN                    60       
#define SYNC_IN_PROGRESS 1
/* Restarting Mode macros */
/* This MACRO is used to define 
 * the speaker in restarting mode */
#define BGP4_RESTARTING_MODE             1        
/* This MACRO is used to define 
 * the speaker in receiving mode */
#define BGP4_RECEIVING_MODE                2        
/* This MACRO is used to define 
 * the speaker is neither in receiving
 * or restarting  mode */
#define BGP4_RESTART_MODE_NONE             3        


/* Macros for Traps generated during restart */
/* This MACRO is used to identify the 
 * restart status change TRAP of the 
 * BGP speaker */
#define BGP4_RST_STATUS_CHANGE_TRAP        1        
/* This MACRO is used to identify the 
 * peer's restart status change TRAP of                                            * the BGP speaker */
#define BGP4_PEER_RST_STATUS_CHANGE_TRAP   2        

#define BGP4_RTM_ROUTE_FAILURE_TRAP        3        

/* Timer related macros for Graceful restart support */

/* Minimal restart interval allowed */ 
#define BGP4_MIN_RESTART_TIME              1        
/* Default restart interval value */ 
#define BGP4_DEF_RESTART_TIME              90       
/* Maximal restart interval value */
#define BGP4_MAX_RESTART_TIME              4096     
/* Minimal value of the selection
* deferral timer value */
#define BGP4_MIN_SEL_DEF_TIME              60       
/* Default value of the selection 
* deferral timer value */
#define BGP4_DEF_SEL_DEF_TIME              60      
/* Maximal value of the selection 
* Deferral timer */
#define BGP4_MAX_SEL_DEF_TIME              1800     
/* Minimal value of the stale timer */
#define BGP4_MIN_STALE_TIME                90       
/* Maximal value of the stale timer */
#define BGP4_DEF_STALE_TIME                150      
/* Default value of the stale timer */
#define BGP4_MAX_STALE_TIME                3600     


/* Graceful restart admin status */
/* GR Enabled status */
#define     BGP4_ENABLE_GR                 1        
/* GR disabled status */
#define     BGP4_DISABLE_GR                2        

/*GR enabled context is exist */
#define     BGP4_GR_CXT_EXISTS             1

/* Graceful restart exit status */
/* GR exit status is none */
#define     BGP4_GR_EXIT_NONE              1        
/* GR exit on timedout */
#define     BGP4_GR_EXIT_INPROGRESS        2        
/* Successful GR Exit */
#define     BGP4_GR_EXIT_SUCCESS    3        
/* Failure in GR Exit */
#define     BGP4_GR_EXIT_FAILURE    4        

/* Restart support */

/* GR is not supported */
#define  BGP4_GR_SUPPORT_NONE           1        
/* speaker  supports only planned 
 * Restart */
#define  BGP4_GR_SUPPORT_PLANNED        2        
/* Speaker supports both planned and
 * unplanned restart */
#define  BGP4_GR_SUPPORT_BOTH           3        

/* Restart status */

/* Speaker has not restarted */
#define  BGP4_GR_STATUS_NONE            1        
/* Speaker is now undergoing a planned
 * restart */
#define     BGP4_GR_STATUS_PLANNED         2        
/* Speaker is now undergoing an 
 * unplanned restart */
#define  BGP4_GR_STATUS_UNPLANNED       3        

/* Restart reason */
#define  BGP4_GR_REASON_UNKNOWN         0        
/* Restart reason is unknown */
#define  BGP4_GR_REASON_RESTART         1        
/* Restart is due to software restart*/
#define     BGP4_GR_REASON_UPGRADE         2        

/* Bit Masks used for Graceful restart */
/* Bit mask for fetching the restart 
 * bit and forwarding bit */
#define BGP4_GR_BIT_MASK                   0x80     
/* Bit mask to seggregate the 
 * restart time and restart bit 
 * from the Graceful restart
 * message */
#define BGP4_RST_BIT_MASK                  0x0FFF   

/*
    RFC 4724, The Capability value for capability code 64 is:
    Restart Flag(4 Bits)  
    Restart Time(12 Bits) 
    AFI IPV4 (2 Byte)     
    SAFI UNI (1 Byte)     
    Preserve Forwarding State (1 Byte) (value is 0x80 for yes)
    AFI IPV6 (2 Byte)
    SAFI UNI (1 Byte)
    Preserve Forwarding State (1 Byte) (value is 0x80 for yes)
    AFI L2VPN (2 Byte)
    SAFI UNI (1 Byte)
    Preserve Forwarding State (1 Byte) (value is 0x80 for yes)
*/
/* Used to index IPv4 in GR cap 'preserve forwarding state index'*/
#define BGP4_GR_AFI_IPV4             5        
/* Used to index IPv6 in GR cap 'preserve forwarding state index'*/
#define BGP4_GR_AFI_IPV6             9        
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
/* Used to index L2VPN in GR cap 'preserve forwarding state index'*/
#define BGP4_GR_AFI_L2VPN            13
#endif
#endif
/* Invalid status of GR */
#define BGP4_INVALID_STATUS                0       

#define BGP4_TCPAO_ICMP_ACCEPT 1
#define BGP4_TCPAO_ICMP_IGNORE  2
#define BGP4_TCPAO_NOMKT_DISCARD_TRUE  1
#define BGP4_TCPAO_NOMKT_DISCARD_FALSE  2


/* Prototypes of Graceful restart module */
VOID  Bgp4GRRestartProcess (UINT4);
VOID  Bgp4GRNotifyRestartRTM(UINT4, UINT4, UINT4);
VOID  Bgp4GRKillProcess (VOID);
INT4  Bgp4GRCheckEORMarkerRecvd (UINT4);
INT4  Bgp4GRCheckEORMarkerRecvdAtEstb (UINT4);
INT4  Bgp4GRCheckEORMarkerSent (UINT4);
INT4  Bgp4GRProcessSelDefTimerExpiry(VOID);
INT4  Bgp4GRProcessNetworkMsrTempTimerExpiry (VOID);
INT4  Bgp4GRProcessPeerRstTimerExpiry (tBgp4PeerEntry *);
INT4  Bgp4GRProcessRestartTimerExpiry (VOID);
INT4  Bgp4GRProcessStaleTimerExpiry (tBgp4PeerEntry *);
VOID  Bgp4GRInit (UINT4);
VOID  Bgp4GRFillRestartCap(UINT4, UINT1 *, tBgp4PeerEntry *);
VOID  Bgp4GRProcessRestartCap (tBgp4PeerEntry *);
VOID  Bgp4GRProcessIdleState(tBgp4PeerEntry *);
VOID  Bgp4GRProcessEndOfRIBRcvd(tBgp4PeerEntry *);
UINT1 Bgp4GRCheckRouteSelection(UINT4);
UINT1 Bgp4GRCheckRestartMode(UINT4);
VOID  Bgp4GRDeleteAllStaleRoutes (tBgp4PeerEntry *);
VOID  Bgp4GRDeleteAfiSafiStaleRoutes(tBgp4PeerEntry *, UINT4 );
INT4  Bgp4GRPutPeerInDeInitList (tBgp4PeerEntry  *);
VOID  Bgp4GRMarkPeerRoutesAsStale(tBgp4PeerEntry *);
INT4  Bgp4GRIsPeerGRCapable (tBgp4PeerEntry *);
INT1  Bgp4GRCheckGRCapability(UINT4);
VOID  Bgp4GRExitRestart(UINT4);
VOID  Bgp4GRMarkEndOfRIBMarker(tBgp4PeerEntry *);
VOID  Bgp4GRSendEndOfRIBMarker(tBgp4PeerEntry *);
INT4 Bgp4EORForNegotiatedCapabilities(tBgp4PeerEntry * pPeerInfo);
INT4 Bgp4SendEORForNLRI(tBgp4PeerEntry * pPeerentry);
INT4 Bgp4SendEORForMPNLRI(tBgp4PeerEntry * pPeerentry,UINT4 u4afi,UINT4 u4safi);
INT1  Bgp4GRReStoreRestartConfig (UINT4);
UINT4 Bgp4GRGetSecondsSinceBase (tUtlTm);
INT1  Bgp4GRQueryRTMIgpConvergence (UINT4, UINT2);
UINT4 Bgp4GRQueryRTMForwarding(UINT4, UINT2, UINT1*);
VOID  Bgp4GRStoreRestartConfig (VOID);
#ifdef BGP4_IPV6_WANTED
VOID  Bgp4GRProcessRtm6RouteCleanUp (tRtm6RegnId *,
                                    INT1 );
#endif 
VOID  Bgp4GRProcessRtmRouteCleanUp (tRtmRegnId *,
                                    INT1 );

#ifdef BGP_DEBUG
VOID  Bgp4GRUnPlannedShutdownProcess (VOID);
#endif /* BGP_DEBUG */
VOID  Bgp4GlbGRInit (VOID);
VOID  Bgp4GRMarkAllRoutesAsStale (UINT4 u4Context);
#ifdef VPLSADS_WANTED
#ifdef VPLS_GR_WANTED
UINT1 Bgp4GRSendOpenToRFLPeer(tBgp4PeerEntry *pPeerentry);
#endif
#endif

#endif /* BGP4GR_H*/
