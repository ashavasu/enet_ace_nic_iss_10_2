/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgrfd.h,v 1.17 2016/12/27 12:35:53 siva Exp $
 *
 * Description:This file contains the RFD related Macros
 *
 *******************************************************************/
/**********************  Constants   ***************************************/
#define RFD_DECAY_ARRAY_MEM_ALLOC_FAIL        -1
#define RFD_RINDEX_ARRAY_MEM_ALLOC_FAIL       -2
#define RFD_PEER_DAMP_HIST_DELETE_FAIL        -3
#define RFD_PEER_RLIST_ENTRY_NOT_FOUND        -4
#define RFD_RT_RLIST_INSERT_FAIL              -5
#define RFD_RT_DAMP_HIST_DELETE_FAIL          -6
#define RFD_RT_RLIST_ENTRY_NOT_FOUND          -7
#define RFD_RT_DAMP_HIST_MEM_ALLOC_FAIL       -8
#define RFD_RT_NO_ALTERNATE_FOUND             -9
#define RFD_PEER_RLIST_INSERT_FAIL            -10
#define RFD_PEER_DAMP_HIST_MEM_ALLOC_FAIL     -11
#define RFD_SUPPRESSED_STATE                   2
#define RFD_UNSUPPRESSED_STATE                 3
#define RFD_USABLE                             1
#define RFD_SUPPRESS                           2
#define RFD_REUSE                              3
#define RFD_FEASIBLE_ROUTE                     2
#define RFD_UNFEASIBLE_ROUTE                   3
#define RFD_NONE_VALUE                         1
#define RFD_ALT_RT_EXIST                       1
#define RFD_ALT_RT_NOT_EXIST                   2
#define RFD_DAMP_HIST_PRESENT                  1
#define RFD_DAMP_HIST_NOT_PRESENT              2
#define RFD_MIB_PEER_UP                        2
#define RFD_MIB_PEER_DOWN                      3
#define RFD_RT_MATCH                           1
#define RFD_RT_NO_MATCH                        0
#define RFD_NO_OF_REUSE_LISTS                  256
#define RFD_DEF_FOM                            1000
#define RFD_INVALID_FOM                        0xFFFF 
#define RFD_INVALID_INDX                       0xFFFF 
#define RFD_DEF_CUTOFF_THRESHOLD               2000
#define RFD_DEF_REUSE_THRESHOLD                750
#define RFD_DEF_MAX_HOLD_DOWN_TIME             3600
#define RFD_DEF_DECAY_HALF_LIFE_TIME           900
#define RFD_DEF_DECAY_TIMER_GRANULARITY        1
#define RFD_DEF_REUSE_TIMER_GRANULARITY        15
#define RFD_DEF_REUSE_INDEX_ARRAY_SIZE         1024
#define RFD_MIN_CUTOFF_THRESHOLD               2000
#define RFD_MAX_CUTOFF_THRESHOLD               3999
#define RFD_MIN_REUSE_THRESHOLD                100
#define RFD_MAX_REUSE_THRESHOLD                1999
#define RFD_MIN_MAXHOLDDOWNTIME                1800
#define RFD_MAX_MAXHOLDDOWNTIME                10800 
#define RFD_MIN_DECAY_HALF_LIFE_TIME           600
#define RFD_MAX_DECAY_HALF_LIFE_TIME           2700
#define RFD_MIN_DECAY_TIMER_GRANULARITY        1
#define RFD_MAX_DECAY_TIMER_GRANULARITY        10800
#define RFD_MIN_REUSE_TIMER_GRANULARITY        15
#define RFD_MAX_REUSE_TIMER_GRANULARITY        10800
#define RFD_MIN_REUSE_INDEX_ARRAY_SIZE         256
#define RFD_MAX_REUSE_INDEX_ARRAY_SIZE         65535
#define RFD_MAX_SYS_TIME_TICKS                 0xFFFFFFFF
#define RFD_MAX_RTS_INSTANCES                  10
#define RFD_SINGLE_INSTANCE                     1
#define RFD_SUCCESS          BGP4_SUCCESS
#define RFD_FAILURE          BGP4_FAILURE
#define RFD_TRUE             BGP4_TRUE
#define RFD_FALSE            BGP4_FALSE
#define RFD_MEM_FAILURE      MEM_FAILURE
#define RFD_ENABLE_SYNC      BGP4_ENABLE_SYNC
#define RFD_PEER_UP          BGP4_ESTABLISHED_STATE
#define RFD_PEER_DOWN        BGP4_IDLE_STATE
#define RFD_SYSCONFIG_NAME   "BGP4RFD"
#define RFD_SYSCONFIG_CUTOFF "BGP4_RFD_CUTOFF_THRESHOLD"
#define RFD_SYSCONFIG_REUSE  "BGP4_RFD_REUSE_THRESHOLD"
#define RFD_SYSCONFIG_MAXHOLDDOWNTIME "BGP4_RFD_MAX_HOLD_DOWN_TIME"
#define RFD_SYSCONFIG_DECAYHALFLIFETIME "BGP4_RFD_DECAY_HALF_LIFE_TIME"
#define RFD_SYSCONFIG_DECAYTMRGRANULARITY "BGP4_RFD_DECAY_TIMER_GRANULARITY"
#define RFD_SYSCONFIG_REUSETMRGRANULARITY "BGP4_RFD_REUSE_TIMER_GRANULARITY"
#define RFD_SYSCONFIG_REUSEINDEXARRAYSIZE "BGP4_RFD_REUSE_INDEX_ARRAY_SIZE"

#define RFD_MAX_ROUTE_STATES 4
#define RFD_MAX_ROUTE_STATE_STRING 3

#define RFD_STRING_LEN 85
#define RFD_FILE_DESCR_LEN 80

typedef double  tRfdFloat;
#define RFDFLOAT tRfdFloat

typedef struct t_RtDampHist 
{
     tRouteProfile    *pRtProfile;     /* This is a pointer to route profile
                                        * exists in RIB or routes reuse list.
                                        */
     struct t_RtDampHist  *pReusePrev; /* This is the pointer to the
                                        * previous Damp entry present in the
                                        * Route REUSE List
                                        */
     struct t_RtDampHist  *pReuseNext; /* This is the pointer to the
                                        * next Damp entry present in the
                                        * Route REUSE List
                                        */
     tBgp4AppTimer       RtReuseTmr;   /*Pointer to store the reuse timer*/
     RFDFLOAT          RtFom;          /* This is a figure-of-merit value of
                                        * route, which represents the
                                        * instability of a route.
                                        */
     UINT4             u4RtLastTime;   /* This is time stamp taken when 
                                        * figure-of-merit value updated.
                                        */
     UINT4             u4RtFlapCount;  /* This value to store the number of 
                                        * times the route being flapped
                                        */
     UINT4             u4RtFlapTime;   /* This is time stamp taken from the first time 
                                        * route is being flapped.
                                        */
     UINT4             u4RtReuseTime;  /* This is time after which a route can be reused
                                        * again from unsuppressed state.
                                        */
     UINT2             u2ReuseIndex;   /* Reuse List Index in which this
                                        * entry is present.
                                        */
     UINT1             u1RtStatus;     /* This flag gives the status whether
                                        * the route is suppressed
                                        * (RFD_SUPPRESSED_STATE) or not
                                        * (RFD_UNSUPPRESSED_STATE)
                                        */
     UINT1             u1RtFlag;       /* This flag gives the status whether
                                        * the route is feasible
                                        * (RFD_FEASIBLE_ROUTE) or unfeasible
                                        * (RFD_UNFEASIBLE_ROUTE)
                                        */
} tRtDampHist;


typedef struct t_PeerDampHist 
{
     tTMO_SLL_NODE    NextPeerDampHist; /* Damping history of next peer. */
     RFDFLOAT         PeerFom;          /* This is figure-of-merit value of
                                         * peer that represents the instability
                                         * of peer.
                                         */
     tAddrPrefix      PeerRemoteAddr;   /* The address of the remote end of 
                                         * peering session
                                         */
     UINT4            u4PeerLastTime;   /* This is time stamp taken when peer
                                         * figure-of-merit updated.
                                         */
     UINT1            u1PeerStatus;     /* This flag represents whether the peer
                                         * is suppressed (RFD_SUPPRESSED_STATE)
                                         * or not (RFD_UNSUPPRESSED_STATE).
                                         */
     UINT1            au1Padding [3];   /* Added for 4 bytes alignment. */
} tPeerDampHist;

typedef struct  t_ReusePeer
{
     tTMO_SLL_NODE    NextReusePeer;/*The next Peer in the Peers reuse list*/
     tBgp4PeerEntry * pPeerInfo;    /*Pointer to the peer information that 
                                      is suppressed.*/
     tAddrPrefix      PeerRemoteAddr;/*The address of the remote end of 
                                       peering session*/
} tReusePeer;

typedef struct t_ConfigurableData 
{
     RFDFLOAT  Cutoff;            /*This is cut off threshold above which 
                                      the route advertisement will be 
                                      suppressed.*/
     RFDFLOAT  Reuse;            /*This is reuse threshold below which the 
                                     suppressed route will be used again.*/
     UINT2     u2MaxHoldDownTime;  /*The maximum time a route can be 
                                    suppressed.*/
     UINT2     u2DecayHalfLifeTime; /*The time required to reduce the 
                                      figure-of-merit by half.*/
     UINT2     u2DecayTimerGranularity;/*Time granularity in seconds used to 
                                         perform all decay calculations.*/
     UINT2     u2ReuseTimerGranularity;/*The time interval between evaluations 
                                         of the reuse lists.*/
     UINT2     u2ReuseIndexArraySize;  /*This is size of reuse index array.*/
     UINT1     u1RfdAdminStatus;
     UINT1     u1Padding;         /*Added for 4 bytes alignment.*/
}   tConfigurableData;

typedef struct t_PrecomputedData 
{
     RFDFLOAT     Ceiling;          /*Highest value that figure-of-merit can 
                                      attain.*/
     RFDFLOAT     ScaleFactor;      /*ScaleFactor is used for reuse index 
                                     array calculation.*/
     RFDFLOAT    * pDecayArray;     /*This is a pointer to Decay array that 
                                       holds decay values that can be used 
                                       when updating figure-of-merit. These 
                                       values are pre-computed.*/
     UINT2      * pReuseIndexArray;  /*This is a pointer to Reuse index array 
                                       that holds indexes into reuse list 
                                       heads.*/
                                           
}  tPrecomputedData;


typedef struct t_RfdErrInfo 
{
     UINT4      u4DecayArrayMemAllocFail;  /*Counter to log the error when 
                                             memory allocation for decay 
                                             array fails.*/
                                             
     UINT4      u4RIndexArrayMemAllocFail; /*Counter to log the error when 
                                             memory allocation for reuse 
                                             index array fails.*/
                                             
     UINT4      u4PeerDampHistDeleteFail; /*Counter to log the error when a 
                                            deletion of peers damping history 
                                            fails.*/
                                            
     UINT4      u4PeerRListEntryNotFound; /*Counter to log the error when 
                                            peer entry is not found in peers 
                                            reuse list.*/
                                            
     UINT4      u4RtRListInsertFail;     /*Counter to log the error when 
                                           insertion into routes reuse list 
                                           fails.*/
                                           
     UINT4      u4RtDampHistDeleteFail; /*Counter to log the error when 
                                          deletion of routes damping 
                                          strucutre fails.*/
                                          
     UINT4      u4RtRListEntryNotFound; /* Counter to log the error when 
                                           route entry is not found in 
                                           routes reuse list.*/
                                           
     UINT4      u4RtDampHistMemAllocFail;/* Counter to log the error when 
                                            memory allocation for damping 
                                            history fails.*/
                                            
     UINT4      u4RtNoAlternateFound;   /*Counter to log the error when no 
                                          alternate route exists.*/
                                          
     UINT4      u4PeerRListInsertFail;  /*Counter to log the error when peer 
                                          information insertion into peers 
                                          reuse list fails.*/
     UINT4      u4PeerDampHistMemAllocFail; /* Counter to log the error when 
                                            peer damping history memory 
                                            allocation fails.*/
}  tRfdErrInfo;
 
typedef struct t_Bgp4RfdDLL
{
    tRtDampHist         *pFirst;
    tRtDampHist         *pLast;
    UINT4                u4Count;
} tRfd_DLL;

typedef struct t_Bgp4RfdParams 
{
     UINT4                u4Context;        /* The VR instance for this Rfd parameters */
     tConfigurableData    ConfigurableData; /* This contains configured data */
     tPrecomputedData     PrecomputedData;  /* This contains precomputed data*/
     tTMO_SLL             PeersReuseList;   /* List of suppressed peers, which
                                             * will be evaluated during peers
                                             * reuse list processing. Each
                                             * node is of type tBgp4PeerEntry
                                             */
     tReusePeer           *pLastReuseList;  /* Last node accessed by GET_NEXT
                                               BGP ReuseListTable */

     tRfd_DLL             aRtsReuseList [RFD_NO_OF_REUSE_LISTS];
                                            /*This is an array of lists that
                                             * holds suppressed routes. The
                                             * routes in this list array are 
                                             * processing when Reuse Timer 
                                             * expires.
                                             */
     tRfdErrInfo          RfdErrStats;       /*Holds the error information of 
                                               RFD.*/
     tTMO_HASH_TABLE   *  pPeersDampHistList;/*This is a pointer to hash 
                                               table that contains peers 
                                               damping history.*/
                                               
     tBgp4AppTimer       *pRfdReuseTmr;   /*Pointer to store the reuse timer*/
     UINT4                u4FeasRtProfileCnt; /*Count that gives the no of 
                                                feasible routes that are not 
                                                present in RIB , but present 
                                                in RFD data storage*/
     UINT2                u2ReuseOffset;   /*On Reuse Timer expiry, the reuse 
                                             lists corresponding to
                                             u2ReuseOffset is evaluated. 
                                             Also when the route is 
                                             suppressed, it is inserted 
                                             into the reuse list corresponding 
                                             to u2ReuseOffset.*/
                                             
     UINT1                au1Padding [2];/* Added for 4 bytes alignment.*/
}   tBgp4RfdParams;


/*********************  Macros *********************************************/
#define RFD_MAX_PEERS        Bgp4GetMaxPeers ()
#define RFD_MAX_ROUTES        Bgp4GetMaxRoutes ()
#define RFD_GLOBAL_PARAMS(u4CxtId)      (gBgpCxtNode[u4CxtId]->Bgp4RfdParams)
#define RFD_DECAY_ARRAY(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).PrecomputedData.pDecayArray)
#define RFD_REUSE_INDEX_ARRAY(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).PrecomputedData.pReuseIndexArray)
#define RT_DAMP_HIST_MEMPOOLID        (gBgpNode.RtDampHistMemPoolId)
#define PEER_DAMP_HIST_MEMPOOLID        (gBgpNode.PeerDampHistMemPoolId)
#define REUSE_PEER_MEMPOOLID        (gBgpNode.ReusePeerMemPoolId)
#define PEERS_DAMP_HIST_LIST(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).pPeersDampHistList)
#define RFD_PEERS_REUSE_LIST(u4CxtId)        (&((RFD_GLOBAL_PARAMS(u4CxtId)).PeersReuseList))
#define RFD_RTS_REUSE_LIST(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).aRtsReuseList)
#define RFD_RTS_REUSE_LIST_FIRST(u4CxtId,u4Index) \
                    ((RFD_GLOBAL_PARAMS(u4CxtId)).aRtsReuseList[u4Index].pFirst)
#define RFD_RTS_REUSE_LIST_LAST(u4CxtId,u4Index)  \
                    ((RFD_GLOBAL_PARAMS(u4CxtId)).aRtsReuseList[u4Index].pLast)
#define RFD_RTS_REUSE_LIST_COUNT(u4CxtId,u4Index) \
                    ((RFD_GLOBAL_PARAMS(u4CxtId)).aRtsReuseList[u4Index].u4Count)
#define RFD_PEER_STATE(u4CxtId,pPeerInfo)        BGP4_PEER_STATE(pPeerInfo)
#define DECAY_ARRAY_MEM_ALLOC_FAIL_ERR(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).RfdErrStats.u4DecayArrayMemAllocFail)
#define RINDEX_ARRAY_MEM_ALLOC_FAIL_ERR(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).RfdErrStats.u4RIndexArrayMemAllocFail)
#define PEER_DAMP_HIST_DELETE_FAIL_ERR(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).RfdErrStats.u4PeerDampHistDeleteFail)
#define PEER_RLIST_ENTRY_NOT_FOUND_ERR(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).RfdErrStats.u4PeerRListEntryNotFound)
#define RT_RLIST_INSERT_FAIL_ERR(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).RfdErrStats.u4RtRListInsertFail)
#define RT_DAMP_HIST_DELETE_FAIL_ERR(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).RfdErrStats.u4RtDampHistDeleteFail)
#define RT_RLIST_ENTRY_NOT_FOUND_ERR(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).RfdErrStats.u4RtRListEntryNotFound)
#define RT_DAMP_HIST_MEM_ALLOC_FAIL_ERR(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).RfdErrStats.u4RtDampHistMemAllocFail)
#define RT_NO_ALTERNATE_FOUND_ERR(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).RfdErrStats.u4RtNoAlternateFound)
#define PEER_RLIST_INSERT_FAIL_ERR(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).RfdErrStats.u4PeerRListInsertFail)
#define PEER_DAMP_HIST_MEM_ALLOC_FAIL_ERR(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).RfdErrStats.u4PeerDampHistMemAllocFail)
#define RFD_MAX_HOLD_DOWN_TIME(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).ConfigurableData.u2MaxHoldDownTime)
#define RFD_REUSE_THRESHOLD(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).ConfigurableData.Reuse)
#define RFD_CUTOFF_THRESHOLD(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).ConfigurableData.Cutoff)
#define RFD_DECAY_HALF_LIFE_TIME(u4CxtId)   ((RFD_GLOBAL_PARAMS(u4CxtId)).ConfigurableData.u2DecayHalfLifeTime)
#define RFD_DECAY_TIMER_GRANULARITY(u4CxtId)  ((RFD_GLOBAL_PARAMS(u4CxtId)).ConfigurableData.u2DecayTimerGranularity)
#define RFD_REUSE_TIMER_GRANULARITY(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).ConfigurableData.u2ReuseTimerGranularity)
#define RFD_REUSE_INDEX_ARRAY_SIZE(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).ConfigurableData.u2ReuseIndexArraySize)
#define RFD_REUSE_LIST_OFFSET(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).u2ReuseOffset)
#define RFD_DECAY_ARRAY_SIZE(u4CxtId)        ((RFD_MAX_HOLD_DOWN_TIME(u4CxtId))/ (RFD_DECAY_TIMER_GRANULARITY(u4CxtId)))
#define RFD_CEILING_VALUE(u4CxtId)        ((RFD_GLOBAL_PARAMS(u4CxtId)).PrecomputedData.Ceiling)
#define RFD_SCALE_FACTOR(u4CxtId)        (RFD_GLOBAL_PARAMS(u4CxtId)).PrecomputedData.ScaleFactor
#define RFD_REUSE_TIMER_ENTRY(u4CxtId)  ((RFD_GLOBAL_PARAMS(u4CxtId)).pRfdReuseTmr)
#define RFD_NAT_LOG                 BGP4_NAT_LOG
#define RFD_EXP                     BGP4_EXP 
#define RFD_POW                     BGP4_POW
#define RFD_CEIL                    BGP4_CEIL
#define RFD_MIN(x,y)                (x < y) ? x:y
#define REUSE_PEER_ENTRY_CREATE() \
 MemAllocMemBlk (REUSE_PEER_MEMPOOLID)
#define PEER_DAMP_HIST_ENTRY_CREATE(pPeerDampHist) \
 pPeerDampHist = (tPeerDampHist *) MemAllocMemBlk (PEER_DAMP_HIST_MEMPOOLID);
#define REUSE_PEER_ENTRY_FREE(pReusePeer) MemReleaseMemBlock (REUSE_PEER_MEMPOOLID, (UINT1 *)pReusePeer)
#define PEER_DAMP_HIST_ENTRY_FREE(pPeerDampHist) MemReleaseMemBlock (PEER_DAMP_HIST_MEMPOOLID, (UINT1 *)pPeerDampHist)
#define RFD_PEER_INFO_IN_PEER_DAMPHIST_STRUCT(pPeerDampHist)\
            (pPeerDampHist->PeerRemoteAddr)
#define RFD_PEER_INFO_IN_REUSE_PEER_STRUCT(pReusePeer)\
            (pReusePeer->PeerRemoteAddr)
#define RFD_PEER_INFO_IN_SUP_PEER_RT_STRUCT(pSupPeerRt)\
            (pSupPeerRt->PeerRemoteAddr)
#define RFD_HASH_DYN_Scan_Bucket(pHashTab, u1HashIndx, pNode, pTempNode, TypeCast)  BGP_SLL_DYN_Scan (&((pHashTab)->HashList[u1HashIndx]), pNode, pTempNode, TypeCast)
#define RFD_IS_RTS_MATCH   Bgp4IsRtsIdentical  

#define RFD_IS_SAME_DEST(pRtInfo, pRtProfile) \
(((PrefixMatch (BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (\
                BGP4_RT_NET_ADDRESS_INFO (pRtInfo)),\
                BGP4_ADDR_PREFIX_INFO_IN_NET_ADDRESS_INFO (\
                BGP4_RT_NET_ADDRESS_INFO (pRtProfile))))\
   == BGP4_TRUE) && \
  (BGP4_RT_PREFIXLEN (pRtInfo) == BGP4_RT_PREFIXLEN (pRtProfile)))? 1:0

#define RFD_IS_SAME_PEER_DEST(IpPrefix1, PrefixLen1, PeerRemAddr1,\
                              IpPrefix2, PrefixLen2, PeerRemAddr2)\
           (((\
            ((PrefixMatch (IpPrefix1, IpPrefix2)) == BGP4_TRUE) && \
            (PrefixLen1 == PrefixLen2) &&\
            ((PrefixMatch (PeerRemAddr1, PeerRemAddr2)) == BGP4_TRUE))\
               == BGP4_TRUE) ? 1:0 )

#define RFD_GET_SYS_TIME(pu4CurrentTime )  \
  OsixGetSysTime ((tOsixSysTime *) (pu4CurrentTime))
#define RFD_GET_TIME_DIFF_SEC(u4CurrentTime, u4LastTime) \
   (u4CurrentTime < u4LastTime) ? \
   ((UINT2) (((RFD_MAX_SYS_TIME_TICKS - u4LastTime)+u4CurrentTime)/SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) : \
   ((UINT2) ((u4CurrentTime - u4LastTime)/SYS_NUM_OF_TIME_UNITS_IN_A_SEC))
#define BGP4_RFD_FEAS_RTPROFILE_CNT(u4CxtId) ((RFD_GLOBAL_PARAMS(u4CxtId)).u4FeasRtProfileCnt)
#define RFD_ADMIN_STATUS(u4CxtId)             ((RFD_GLOBAL_PARAMS(u4CxtId)).ConfigurableData.u1RfdAdminStatus)

/********************** Proto Types ***************************************/

INT4   RfdMemInit (UINT4);
VOID   RfdInitialise (UINT4);
INT4   RfdFaultReportReceive (UINT4 u4Context, INT1 i1FaultReport);
VOID   RfdCalcCeiling (UINT4);
VOID   RfdBuildDecayArray (UINT4);
VOID   RfdBuildRIndxArray (UINT4);
INT4   RfdShutDown (UINT4);
VOID   RfdReadSystemConfigInfo (UINT4);
        
VOID   Bgp4InitRfdDampHist (tRtDampHist *pRtDampHist);
INT4   RfdDeletePeersDampHist (UINT4);
INT4   RfdDeleteRouteDampHist (VOID);
INT4   RfdDeletePeersReuseList (VOID);
VOID   RfdGetPeerHashIndex (UINT4 u4HashKey, UINT1 * pu1HashIndex);
INT4   RfdPeersHandler ( tBgp4PeerEntry *  pPeerInfo);
INT4   RfdPeerUpHandler (tBgp4PeerEntry *pPeerInfo);
INT4   RfdRemovePeerFromPeerReuseList ( tBgp4PeerEntry * pPeerInfo,
                                        tReusePeer ** ppReusePeer);
INT4   RfdInsertPeerReuseList (tBgp4PeerEntry * pPeerInfo);
INT4   RfdPUHDampHistHandler (tBgp4PeerEntry * pPeerInfo,
                                UINT1 * pu1PeerStatus,
                                UINT1 * pu1IsFreshPeer,
                                UINT1 * pu1IsDampHistFreed);
INT4   RfdPUHCalcPeerFOM (UINT4 u4ContextId, tPeerDampHist * pPeerDampHist);
INT4   RfdPUHProcessPeerUp (UINT4 u4Context, tPeerDampHist * pPeerDampHist,
                            UINT1 * pu1PeerStatus,
                            UINT1 * pu1IsDampHistFreed);
INT4   RfdFetchPeerDampHist (tBgp4PeerEntry * pPeerInfo,
                             tPeerDampHist ** ppPeerDampHist);
INT4   RfdPeerDownHandler (tBgp4PeerEntry * pPeerInfo);
INT4   RfdPDHFetchCreatePeerDampHist (tBgp4PeerEntry * pPeerInfo,
                            tPeerDampHist ** ppPeerDampHist,
                            UINT1 * pu1IsNewPeerDampHist);
INT4   RfdPDHCalcPeerFOM (UINT4 u4Context, tPeerDampHist * pPeerDampHist);
INT4   RfdPDHPeerAdjustCeiling (UINT4 u4Context, tPeerDampHist *pPeerDampHist);
INT4   RfdGetPeerState (tBgp4PeerEntry *pPeerInfo, UINT1 *pu1PeerStatus);
INT4   RfdReuseListsHandler (UINT4);
INT4   RfdPeersReuseListHandler (UINT4);
INT4   RfdPRLFetchPeerReuseList ( UINT4 u4ContextId, tReusePeer * pReusePeer,
                                  tReusePeer ** ppNextReusePeer);
INT4   RfdPRLDampHistHandler (tBgp4PeerEntry *pPeerInfo,
                              tPeerDampHist **ppPeerDampHist,
                              UINT1 *pu1PeerState);
INT4   RfdPRLCalcPeerFOM (UINT4, tPeerDampHist * pPeerDampHist);
INT4   RfdPRLReuseCheck (UINT4, tPeerDampHist *pPeerDampHist,
                         UINT1 *pu1PeerState);
INT4   RfdReusePeerRtsHandler (tBgp4PeerEntry * pPeerInfo,
                               tPeerDampHist * pPeerDampHist);
INT4   RfdSupPeerReusePendHandler (tBgp4PeerEntry  *pPeerEntry);
INT4   RfdSupPeerReuseHandler (tBgp4PeerEntry  *pPeerEntry);
INT4   RfdSupPeerReuseListHandler (UINT4);
INT4   RfdRoutesReuseListHandler (UINT4);
INT4   RfdRRLFetchRtReuseList (UINT4 u4Context, tRtDampHist *pRtDampHist,
                               tRtDampHist **ppNextRtDampHist);
INT4   RfdRRLDampHistHandler (tRtDampHist * pRtDampHist,
                              UINT1 * pu1RtState);
INT4   RfdRRLCalcRtFOM (tRtDampHist * pRtDampHist);
INT4   RfdRRLCheckReuse (tRtDampHist * pRtDampHist,
                         UINT1 * pu1RtState);
INT4   RfdInsertRtInRtsReuseList (tRtDampHist *pRtDampHist);
INT4   RfdRemoveRtFromRtsReuseList (tRouteProfile * , tRtDampHist *);
INT4   RfdRoutesClassifier (tBgp4PeerEntry * pPeerInfo,
                            tRouteProfile * pRtInfo,
                            tRouteProfile * pOldRoute, UINT4 u4IsFeasRoute,
                            VOID *pRibNode);
INT4   RfdReadvertisedRoutesHandler (tRouteProfile *pRtInfo,
                                     tBgp4PeerEntry * pPeerInfo,
                                     tRtDampHist * pDampHist);
INT4   RfdReplacementRoutesHandler (tRouteProfile *pRtInfo,
                                    tRtDampHist * pDampHist);
INT4   RfdRARDampHistHandler (tRouteProfile *, tRtDampHist *, UINT1 *,
                              UINT1 *);
INT4   RfdCalcRtNoPenaltyFOM (tRtDampHist *pRtDampHist);
INT4   RfdRARProcessRouteAdvt (tRtDampHist *pRtDampHist,
                               UINT1 *pu1RtState,
                               UINT1 *pu1IsDampHistFreed);
INT4   RfdWithdrawnRoutesHandler (tRouteProfile * pRtInfo,
                                  VOID *pRouteRibNode);
INT4   RfdWRHDampHistHandler (tRouteProfile * pRtInfo,
                              UINT1 u1IsWithdRoute,
                              tRtDampHist ** ppRtDampHist,
                              UINT1 * pu1IsNewDampHist);
INT4   RfdWRHUpdateCreateRtDampHist (tRouteProfile * pRtInfo,
                                     UINT1 u1IsWithdRoute,
                                     tRtDampHist ** ppRtDampHist, 
                                     UINT1 * pu1IsNewDampHist);
INT4   RfdWRHCalcRtFOM (tRtDampHist *pRtDampHist, UINT1 u1IsWithdRoute);
INT4   RfdWRHRtAdjustCeiling (tRtDampHist * pRtDampHist);
INT4   RfdClearPeerHistory (tBgp4PeerEntry * pPeerInfo);
VOID   RfdDisplayRtsDampHist (VOID);
UINT1  Bgp4GetSyncStatus (VOID);

tRtDampHist        *Bgp4MemAllocateRfdDampHist (UINT4);
INT4                Bgp4MemReleaseRfdDampHist (tRtDampHist *);
INT4   RfdClearRouteDampHist (UINT4);
INT4  RfdUtlClearDampHist (tRouteProfile  *pRtProfile);
INT4 RfdClearPeersReuseList (UINT4);
INT4   RfdEnable (UINT4);
INT4   RfdDisable (UINT4);
INT4 RfdInit (UINT4);
