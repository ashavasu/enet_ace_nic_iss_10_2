
/*  Prototype for Get Test & Set for fsbgp4Scalars.  */
tSNMP_VAR_BIND*
fsbgp4ScalarsGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4ScalarsSet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4ScalarsTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4MpeBgpPeerTable.  */
tSNMP_VAR_BIND*
fsbgp4MpeBgpPeerEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4MpeBgpPeerEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4MpeBgpPeerEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4MpeBgp4PathAttrTable.  */
tSNMP_VAR_BIND*
fsbgp4MpeBgp4PathAttrEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));


/*  Prototype for Get Test & Set for fsbgp4PeerExtTable.  */
tSNMP_VAR_BIND*
fsbgp4PeerExtEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4PeerExtEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4PeerExtEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4MEDTable.  */
tSNMP_VAR_BIND*
fsbgp4MEDEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4MEDEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4MEDEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4LocalPrefTable.  */
tSNMP_VAR_BIND*
fsbgp4LocalPrefEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4LocalPrefEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4LocalPrefEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4UpdateFilterTable.  */
tSNMP_VAR_BIND*
fsbgp4UpdateFilterEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4UpdateFilterEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4UpdateFilterEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4AggregateTable.  */
tSNMP_VAR_BIND*
fsbgp4AggregateEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4AggregateEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4AggregateEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4RRDGroup.  */
tSNMP_VAR_BIND*
fsbgp4RRDGroupGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4RRDGroupSet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4RRDGroupTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4ImportRouteTable.  */
tSNMP_VAR_BIND*
fsbgp4ImportRouteEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4ImportRouteEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4ImportRouteEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4FsmTransitionHistTable.  */
tSNMP_VAR_BIND*
fsbgp4FsmTransitionHistEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));


/*  Prototype for Get Test & Set for fsbgp4RflScalars.  */
tSNMP_VAR_BIND*
fsbgp4RflScalarsGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4RflScalarsSet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4RflScalarsTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4RfdScalars.  */
tSNMP_VAR_BIND*
fsbgp4RfdScalarsGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4RfdScalarsSet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4RfdScalarsTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4RfdRtDampHistTable.  */
tSNMP_VAR_BIND*
fsbgp4RfdRtDampHistEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));


/*  Prototype for Get Test & Set for fsbgp4RfdPeerDampHistTable.  */
tSNMP_VAR_BIND*
fsbgp4RfdPeerDampHistEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));


/*  Prototype for Get Test & Set for fsbgp4RfdRtsReuseListTable.  */
tSNMP_VAR_BIND*
fsbgp4RfdRtsReuseListEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));


/*  Prototype for Get Test & Set for fsbgp4RfdPeerReuseListTable.  */
tSNMP_VAR_BIND*
fsbgp4RfdPeerReuseListEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));


/*  Prototype for Get Test & Set for fsbgp4CommScalars.  */
tSNMP_VAR_BIND*
fsbgp4CommScalarsGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4CommScalarsSet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4CommScalarsTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4CommRouteAddCommTable.  */
tSNMP_VAR_BIND*
fsbgp4CommRouteAddCommEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4CommRouteAddCommEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4CommRouteAddCommEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4CommRouteDeleteCommTable.  */
tSNMP_VAR_BIND*
fsbgp4CommRouteDeleteCommEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4CommRouteDeleteCommEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4CommRouteDeleteCommEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4CommRouteCommSetStatusTable.  */
tSNMP_VAR_BIND*
fsbgp4CommRouteCommSetStatusEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4CommRouteCommSetStatusEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4CommRouteCommSetStatusEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4CommInFilterTable.  */
tSNMP_VAR_BIND*
fsbgp4CommInFilterEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4CommInFilterEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4CommInFilterEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4CommOutFilterTable.  */
tSNMP_VAR_BIND*
fsbgp4CommOutFilterEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4CommOutFilterEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4CommOutFilterEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4ExtCommScalars.  */
tSNMP_VAR_BIND*
fsbgp4ExtCommScalarsGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4ExtCommScalarsSet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4ExtCommScalarsTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4ExtCommRouteAddExtCommTable.  */
tSNMP_VAR_BIND*
fsbgp4ExtCommRouteAddExtCommEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4ExtCommRouteAddExtCommEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4ExtCommRouteAddExtCommEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4ExtCommRouteDeleteExtCommTable.  */
tSNMP_VAR_BIND*
fsbgp4ExtCommRouteDeleteExtCommEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4ExtCommRouteDeleteExtCommEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4ExtCommRouteDeleteExtCommEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4ExtCommRouteExtCommSetStatusTable.  */
tSNMP_VAR_BIND*
fsbgp4ExtCommRouteExtCommSetStatusEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4ExtCommRouteExtCommSetStatusEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4ExtCommRouteExtCommSetStatusEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4ExtCommInFilterTable.  */
tSNMP_VAR_BIND*
fsbgp4ExtCommInFilterEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4ExtCommInFilterEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4ExtCommInFilterEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4ExtCommOutFilterTable.  */
tSNMP_VAR_BIND*
fsbgp4ExtCommOutFilterEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4ExtCommOutFilterEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4ExtCommOutFilterEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4PeerLinkBwTable.  */
tSNMP_VAR_BIND*
fsbgp4PeerLinkBwEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4PeerLinkBwEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4PeerLinkBwEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgpCapScalars.  */
tSNMP_VAR_BIND*
fsbgpCapScalarsGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgpCapScalarsSet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgpCapScalarsTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4CapSupportedCapsTable.  */
tSNMP_VAR_BIND*
fsbgp4CapSupportedCapsEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4CapSupportedCapsEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4CapSupportedCapsEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4StrictCapabilityMatchTable.  */
tSNMP_VAR_BIND*
fsbgp4StrictCapabilityMatchEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4StrictCapabilityMatchEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4StrictCapabilityMatchEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgpAscScalars.  */
tSNMP_VAR_BIND*
fsbgpAscScalarsGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgpAscScalarsSet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgpAscScalarsTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgpAscConfedPeerTable.  */
tSNMP_VAR_BIND*
fsbgpAscConfedPeerEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgpAscConfedPeerEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgpAscConfedPeerEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4RtRefreshInboundTable.  */
tSNMP_VAR_BIND*
fsbgp4RtRefreshInboundEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4RtRefreshInboundEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4RtRefreshInboundEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4RtRefreshStatisticsTable.  */
tSNMP_VAR_BIND*
fsbgp4RtRefreshStatisticsEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));


/*  Prototype for Get Test & Set for fsbgp4TCPMD5AuthTable.  */
tSNMP_VAR_BIND*
fsbgp4TCPMD5AuthEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4TCPMD5AuthEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4TCPMD5AuthEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4SoftReconfigOutboundTable.  */
tSNMP_VAR_BIND*
fsbgp4SoftReconfigOutboundEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4SoftReconfigOutboundEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4SoftReconfigOutboundEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4MpePrefixCountersTable.  */
tSNMP_VAR_BIND*
fsbgp4MpePrefixCountersEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));


/*  Prototype for Get Test & Set for fsbgp4MplsVpnVrfRouteTargetTable.  */
tSNMP_VAR_BIND*
fsbgp4MplsVpnVrfRouteTargetEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4MplsVpnVrfRouteTargetEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4MplsVpnVrfRouteTargetEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsbgp4MplsVpnVrfRedistributeTable.  */
tSNMP_VAR_BIND*
fsbgp4MplsVpnVrfRedistributeEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsbgp4MplsVpnVrfRedistributeEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsbgp4MplsVpnVrfRedistributeEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));

