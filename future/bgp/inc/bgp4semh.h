/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved    
 *
 * $Id: bgp4semh.h,v 1.14 2012/07/09 09:52:41 siva Exp $
 *
 * Description:This file contains the definitions that are related  
 *             to the BGP4 SEM.    
 *
 *******************************************************************/

#ifndef  BGP4SEM_H

#define  BGP4SEM_H                              
#define  BGP4_NO_EVT                     0x0    
#define  BGP4_START_EVT                  0x1    
#define  BGP4_STOP_EVT                   0x2    
#define  BGP4_TCP_CONN_OPENED_EVT        0x4    
#define  BGP4_TCP_CONN_CLOSED_EVT        0x8    
#define  BGP4_TCP_CONN_OPEN_FAIL_EVT     0x10   
#define  BGP4_TCP_FATAL_ERROR_EVT        0x20   
#define  BGP4_CONNECTRETRY_TMR_EXP_EVT   0x40   
#define  BGP4_HOLD_TMR_EXP_EVT           0x80   
#define  BGP4_KEEPALIVE_TMR_EXP_EVT      0x100  
#define  BGP4_OPEN_MSG_RECD_EVT          0x200  
#define  BGP4_KEEPALIVE_MSG_RECD_EVT     0x400  
#define  BGP4_UPDATE_MSG_RECD_EVT        0x800  
#define  BGP4_NOTIFICATION_MSG_RECD_EVT  0x1000 
#define  BGP4_ERROR_EVT                  0x2000 
#define  BGP4_ROUTE_REFRESH_MSG_RECD_EVT 0x4000
#define  BGP4_DELAY_OPEN_TMR_EXP_EVT     0x4001
#define  BGP4_AUTOMATIC_STOP_EVT         0x4002

/* STATES */
#define  BGP4_INVALID_STATE     -1
#define  BGP4_NO_STATE           0 
#define  BGP4_IDLE_STATE         1 
#define  BGP4_CONNECT_STATE      2 
#define  BGP4_ACTIVE_STATE       3 
#define  BGP4_OPENSENT_STATE     4 
#define  BGP4_OPENCONFIRM_STATE  5 
#define  BGP4_ESTABLISHED_STATE  6 
#define  BGP4_MAX_STATES         6 
#define  BGP4_FSM_HIST_MAX_SIZE 10
#define  BGP4_FSM_MAX_STRING_SIZE 11
#define  BGP4_AU_FSM_STRING  330



#endif  /* BGP4SEM_H */
