/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpbglw.h,v 1.16 2016/12/27 12:35:53 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4GlobalTraceDebug ARG_LIST((INT4 *));

INT1
nmhGetFsMIBgp4LocalAs ARG_LIST((UINT4 *));

INT1
nmhGetFsMIBgp4MaxPeerEntry ARG_LIST((INT4 *));

INT1
nmhGetFsMIBgp4MaxNoofRoutes ARG_LIST((INT4 *));

INT1
nmhGetFsMIBgp4GRAdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFsMIBgp4GRRestartTimeInterval ARG_LIST((INT4 *));

INT1
nmhGetFsMIBgp4RestartExitReason ARG_LIST((INT4 *));

INT1
nmhGetFsMIBgp4GRSelectionDeferralTimeInterval ARG_LIST((INT4 *));

INT1
nmhGetFsMIBgp4GRStaleTimeInterval ARG_LIST((INT4 *));

INT1
nmhGetFsMIBgp4GRMode ARG_LIST((INT4 *));

INT1
nmhGetFsMIBgp4RestartSupport ARG_LIST((INT4 *));

INT1
nmhGetFsMIBgp4RestartStatus ARG_LIST((INT4 *));

INT1
nmhGetFsMIBgp4ForwardingPreservation ARG_LIST((INT4 *));

INT1
nmhGetFsMIBgp4mpePeerHoldAdvtRoutes ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4GlobalTraceDebug ARG_LIST((INT4 ));

INT1
nmhSetFsMIBgp4LocalAs ARG_LIST((UINT4 ));

INT1
nmhSetFsMIBgp4MaxPeerEntry ARG_LIST((INT4 ));

INT1
nmhSetFsMIBgp4MaxNoofRoutes ARG_LIST((INT4 ));

INT1
nmhSetFsMIBgp4GRAdminStatus ARG_LIST((INT4 ));

INT1
nmhSetFsMIBgp4GRRestartTimeInterval ARG_LIST((INT4 ));

INT1
nmhSetFsMIBgp4GRSelectionDeferralTimeInterval ARG_LIST((INT4 ));

INT1
nmhSetFsMIBgp4GRStaleTimeInterval ARG_LIST((INT4 ));

INT1
nmhSetFsMIBgp4RestartSupport ARG_LIST((INT4 ));

INT1
nmhSetFsMIBgp4mpePeerHoldAdvtRoutes ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4GlobalTraceDebug ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIBgp4LocalAs ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMIBgp4MaxPeerEntry ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIBgp4MaxNoofRoutes ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIBgp4GRAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIBgp4GRRestartTimeInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIBgp4GRSelectionDeferralTimeInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIBgp4GRStaleTimeInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIBgp4RestartSupport ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpePeerHoldAdvtRoutes ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4GlobalTraceDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIBgp4LocalAs ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIBgp4MaxPeerEntry ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIBgp4MaxNoofRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIBgp4GRAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIBgp4GRRestartTimeInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIBgp4GRSelectionDeferralTimeInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIBgp4GRStaleTimeInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIBgp4RestartSupport ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgpContextTable. */
INT1
nmhValidateIndexInstanceFsMIBgpContextTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgpContextTable  */

INT1
nmhGetFirstIndexFsMIBgpContextTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgpContextTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4GlobalAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4Identifier ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4Synchronization ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4DefaultLocalPref ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4AdvtNonBgpRt ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4TraceEnable ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4DebugEnable ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4OverlappingRoute ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4AlwaysCompareMED ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4DefaultOriginate ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4DefaultIpv4UniCast ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4IsTrapEnabled ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4NextHopProcessingInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4IBGPRedistributionStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4RRDAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4RRDProtoMaskForEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4RRDSrcProtoMaskForDisable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4RRDDefaultMetric ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4RRDRouteMapName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4RRDMatchTypeEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4RRDMatchTypeDisable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4AscConfedId ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4AscConfedBestPathCompareMED ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4RflbgpClusterId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4RflRflSupport ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4RfdCutOff ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4RfdReuse ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4RfdCeiling ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4RfdMaxHoldDownTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4RfdDecayHalfLifeTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4RfdDecayTimerGranularity ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4RfdReuseTimerGranularity ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4RfdReuseIndxArraySize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4RfdAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4CommMaxInFTblEntries ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4CommMaxOutFTblEntries ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4ExtCommMaxInFTblEntries ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4ExtCommMaxOutFTblEntries ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4CapabilitySupportAvailable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4MaxCapsPerPeer ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4MaxInstancesPerCap ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4MaxCapDataSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4PreferenceValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4IBGPMaxPaths ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4EBGPMaxPaths ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4EIBGPMaxPaths ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4OperIBGPMaxPaths ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4OperEBGPMaxPaths ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4OperEIBGPMaxPaths ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4ContextStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4RIBRoutes ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4Ipv4AddrFamily ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4Ipv6AddrFamily ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4VPNV4AddrFamily ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4L2vpnAddrFamily ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4EvpnAddrFamily ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4FourByteASNSupportStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4FourByteASNotationType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4LocalAsNo ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4VpnLabelAllocPolicy ARG_LIST((INT4 ,INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsMIBgp4DebugType ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4DebugTypeIPAddr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4GlobalAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4Identifier ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIBgp4Synchronization ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4DefaultLocalPref ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIBgp4AdvtNonBgpRt ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4TraceEnable ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIBgp4DebugEnable ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIBgp4OverlappingRoute ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4AlwaysCompareMED ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4DefaultOriginate ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4DefaultIpv4UniCast ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4IsTrapEnabled ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4NextHopProcessingInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4IBGPRedistributionStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4RRDAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4RRDProtoMaskForEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4RRDSrcProtoMaskForDisable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4RRDDefaultMetric ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIBgp4RRDRouteMapName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4RRDMatchTypeEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4RRDMatchTypeDisable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4AscConfedId ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIBgp4AscConfedBestPathCompareMED ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4RflbgpClusterId ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4RflRflSupport ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4RfdCutOff ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4RfdReuse ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4RfdMaxHoldDownTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4RfdDecayHalfLifeTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4RfdDecayTimerGranularity ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4RfdReuseTimerGranularity ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4RfdReuseIndxArraySize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4RfdAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4CommMaxInFTblEntries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4CommMaxOutFTblEntries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4ExtCommMaxInFTblEntries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4ExtCommMaxOutFTblEntries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4CapabilitySupportAvailable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4MaxCapsPerPeer ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4MaxInstancesPerCap ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4MaxCapDataSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4PreferenceValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4IBGPMaxPaths ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4EBGPMaxPaths ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4EIBGPMaxPaths ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4ContextStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4FourByteASNSupportStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4FourByteASNotationType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4LocalAsNo ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIBgp4Ipv4AddrFamily ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4Ipv6AddrFamily ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4VpnLabelAllocPolicy ARG_LIST((INT4 ,INT4 ));

INT1
nmhSetFsMIBgp4VPNV4AddrFamily ARG_LIST((INT4  ,INT4 ));
INT1
nmhSetFsMIBgp4L2vpnAddrFamily ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4EvpnAddrFamily ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4DebugType ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIBgp4DebugTypeIPAddr ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4GlobalAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4Identifier ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIBgp4Synchronization ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4DefaultLocalPref ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIBgp4AdvtNonBgpRt ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4TraceEnable ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIBgp4DebugEnable ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIBgp4OverlappingRoute ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4AlwaysCompareMED ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4DefaultOriginate ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4DefaultIpv4UniCast ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4IsTrapEnabled ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4NextHopProcessingInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4IBGPRedistributionStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4RRDAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4RRDProtoMaskForEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4RRDSrcProtoMaskForDisable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4RRDDefaultMetric ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIBgp4RRDRouteMapName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4RRDMatchTypeEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4RRDMatchTypeDisable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4AscConfedId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIBgp4AscConfedBestPathCompareMED ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4RflbgpClusterId ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4RflRflSupport ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4RfdCutOff ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4RfdReuse ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4RfdMaxHoldDownTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4RfdDecayHalfLifeTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4RfdDecayTimerGranularity ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4RfdReuseTimerGranularity ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4RfdReuseIndxArraySize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4RfdAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4CommMaxInFTblEntries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4CommMaxOutFTblEntries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4ExtCommMaxInFTblEntries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4ExtCommMaxOutFTblEntries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4CapabilitySupportAvailable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4MaxCapsPerPeer ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4MaxInstancesPerCap ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4MaxCapDataSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4PreferenceValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4IBGPMaxPaths ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4EBGPMaxPaths ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4EIBGPMaxPaths ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4ContextStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4FourByteASNSupportStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4FourByteASNotationType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4LocalAsNo  ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIBgp4Ipv4AddrFamily ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4Ipv6AddrFamily ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4VpnLabelAllocPolicy ARG_LIST((UINT4 * ,INT4 ,INT4 ));

INT1
nmhTestv2FsMIBgp4VPNV4AddrFamily ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
INT1
nmhTestv2FsMIBgp4L2vpnAddrFamily ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4EvpnAddrFamily ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4DebugType ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIBgp4DebugTypeIPAddr ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgpContextTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4RRDMetricTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4RRDMetricTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4RRDMetricTable  */

INT1
nmhGetFirstIndexFsMIBgp4RRDMetricTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4RRDMetricTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4RRDMetricValue ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4RRDMetricValue ARG_LIST((INT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4RRDMetricValue ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4RRDMetricTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4CommInFilterTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4CommInFilterTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4CommInFilterTable  */

INT1
nmhGetFirstIndexFsMIBgp4CommInFilterTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4CommInFilterTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4CommIncomingFilterStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIBgp4InFilterRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4CommIncomingFilterStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIBgp4InFilterRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4CommIncomingFilterStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4InFilterRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4CommInFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4CommOutFilterTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4CommOutFilterTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4CommOutFilterTable  */

INT1
nmhGetFirstIndexFsMIBgp4CommOutFilterTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4CommOutFilterTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4CommOutgoingFilterStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIBgp4OutFilterRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4CommOutgoingFilterStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIBgp4OutFilterRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4CommOutgoingFilterStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4OutFilterRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4CommOutFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4ExtCommInFilterTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4ExtCommInFilterTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4ExtCommInFilterTable  */

INT1
nmhGetFirstIndexFsMIBgp4ExtCommInFilterTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4ExtCommInFilterTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4ExtCommIncomingFilterStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4ExtCommInFilterRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4ExtCommIncomingFilterStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4ExtCommInFilterRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4ExtCommIncomingFilterStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4ExtCommInFilterRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4ExtCommInFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4ExtCommOutFilterTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4ExtCommOutFilterTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4ExtCommOutFilterTable  */

INT1
nmhGetFirstIndexFsMIBgp4ExtCommOutFilterTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4ExtCommOutFilterTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4ExtCommOutgoingFilterStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4ExtCommOutFilterRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4ExtCommOutgoingFilterStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4ExtCommOutFilterRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4ExtCommOutgoingFilterStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4ExtCommOutFilterRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4ExtCommOutFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4TCPMD5AuthTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4TCPMD5AuthTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4TCPMD5AuthTable  */

INT1
nmhGetFirstIndexFsMIBgp4TCPMD5AuthTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4TCPMD5AuthTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4TCPMD5AuthPassword ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4TCPMD5AuthPwdSet ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4TCPMD5AuthPassword ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4TCPMD5AuthPwdSet ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4TCPMD5AuthPassword ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4TCPMD5AuthPwdSet ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4TCPMD5AuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgpAscConfedPeerTable. */
INT1
nmhValidateIndexInstanceFsMIBgpAscConfedPeerTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgpAscConfedPeerTable  */

INT1
nmhGetFirstIndexFsMIBgpAscConfedPeerTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgpAscConfedPeerTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgpAscConfedPeerStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgpAscConfedPeerStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgpAscConfedPeerStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgpAscConfedPeerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4MpeBgpPeerTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeBgpPeerTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeBgpPeerTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeBgpPeerTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeBgpPeerTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpebgpPeerIdentifier ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpebgpPeerLocalAs ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerAdminStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerNegotiatedVersion ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerLocalAddr ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpebgpPeerLocalPort ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerRemotePort ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerRemoteAs ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerInUpdates ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerOutUpdates ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerInTotalMessages ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerOutTotalMessages ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerLastError ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpebgpPeerFsmEstablishedTransitions ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerFsmEstablishedTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerConnectRetryInterval ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerHoldTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerKeepAlive ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerHoldTimeConfigured ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerKeepAliveConfigured ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerMinASOriginationInterval ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerMinRouteAdvertisementInterval ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerInUpdateElapsedTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerEndOfRIBMarkerSentStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerEndOfRIBMarkerReceivedStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerRestartMode ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpePeerRestartTimeInterval ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpePeerAllowAutomaticStart ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpePeerAllowAutomaticStop ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerIdleHoldTimeConfigured ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeDampPeerOscillations ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpePeerDelayOpen ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgpPeerDelayOpenTimeConfigured ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpePeerPrefixUpperLimit ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpePeerTcpConnectRetryCnt ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpePeerTcpCurrentConnectRetryCnt ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeIsPeerDamped ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpePeerSessionAuthStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsMIBgp4mpePeerTCPAOKeyIdInUse ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
INT1
nmhGetFsMIBgp4mpePeerTCPAOAuthNoMKTDiscard ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
INT1
nmhGetFsMIBgp4mpePeerTCPAOAuthICMPAccept ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpePeerIpPrefixNameIn ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpePeerIpPrefixNameOut ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpePeerBfdStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */
INT1
nmhSetFsMIBgp4mpebgpPeerLocalAs ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsMIBgp4mpebgpPeerAdminStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpebgpPeerConnectRetryInterval ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpebgpPeerHoldTimeConfigured ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpebgpPeerKeepAliveConfigured ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpebgpPeerMinASOriginationInterval ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpebgpPeerMinRouteAdvertisementInterval ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpePeerAllowAutomaticStart ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpePeerAllowAutomaticStop ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpebgpPeerIdleHoldTimeConfigured ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpeDampPeerOscillations ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpePeerDelayOpen ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpebgpPeerDelayOpenTimeConfigured ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpePeerPrefixUpperLimit ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpePeerTcpConnectRetryCnt ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
INT1
nmhSetFsMIBgp4mpePeerTCPAOAuthNoMKTDiscard ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
INT1
nmhSetFsMIBgp4mpePeerTCPAOAuthICMPAccept ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
INT1
nmhSetFsMIBgp4mpePeerIpPrefixNameIn ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4mpePeerIpPrefixNameOut ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4mpePeerBfdStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4mpebgpPeerLocalAs ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsMIBgp4mpebgpPeerAdminStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpebgpPeerConnectRetryInterval ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpebgpPeerHoldTimeConfigured ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpebgpPeerKeepAliveConfigured ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpebgpPeerMinASOriginationInterval ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpebgpPeerMinRouteAdvertisementInterval ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpePeerAllowAutomaticStart ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpePeerAllowAutomaticStop ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpebgpPeerIdleHoldTimeConfigured ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeDampPeerOscillations ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpePeerDelayOpen ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpebgpPeerDelayOpenTimeConfigured ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpePeerPrefixUpperLimit ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpePeerTcpConnectRetryCnt ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
INT1
nmhTestv2FsMIBgp4mpePeerTCPAOAuthNoMKTDiscard ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
INT1
nmhTestv2FsMIBgp4mpePeerTCPAOAuthICMPAccept ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
INT1
nmhTestv2FsMIBgp4mpePeerIpPrefixNameIn ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4mpePeerIpPrefixNameOut ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4mpePeerBfdStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4MpeBgpPeerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4MpeBgp4PathAttrTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeBgp4PathAttrTable ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeBgp4PathAttrTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeBgp4PathAttrTable ARG_LIST((INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeBgp4PathAttrTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpebgp4PathAttrOrigin ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgp4PathAttrASPathSegment ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpebgp4PathAttrNextHop ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpebgp4PathAttrMultiExitDisc ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgp4PathAttrLocalPref ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgp4PathAttrAtomicAggregate ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgp4PathAttrAggregatorAS ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIBgp4mpebgp4PathAttrAggregatorAddr ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIBgp4mpebgp4PathAttrCalcLocalPref ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgp4PathAttrBest ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpebgp4PathAttrCommunity ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpebgp4PathAttrOriginatorId ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpebgp4PathAttrClusterList ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpebgp4PathAttrExtCommunity ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpebgp4PathAttrUnknown ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpebgp4PathAttrLabel ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpebgp4PathAttrAS4PathSegment ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpebgp4PathAttrAggregatorAS4 ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto Validate Index Instance for FsMIBgp4MpePeerExtTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpePeerExtTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpePeerExtTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpePeerExtTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpePeerExtTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpePeerExtConfigurePeer ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpePeerExtPeerRemoteAs ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIBgp4mpePeerExtEBGPMultiHop ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpePeerExtEBGPHopLimit ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpePeerExtNextHopSelf ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpePeerExtRflClient ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpePeerExtTcpSendBufSize ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpePeerExtTcpRcvBufSize ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpePeerExtLclAddress ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpePeerExtNetworkAddress ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpePeerExtGateway ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpePeerExtCommSendStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpePeerExtECommSendStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpePeerExtPassive ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpePeerExtDefaultOriginate ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpePeerExtOverrideCapability ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4mpePeerExtConfigurePeer ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpePeerExtPeerRemoteAs ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsMIBgp4mpePeerExtEBGPMultiHop ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpePeerExtEBGPHopLimit ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpePeerExtNextHopSelf ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpePeerExtRflClient ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpePeerExtTcpSendBufSize ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpePeerExtTcpRcvBufSize ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpePeerExtLclAddress ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4mpePeerExtNetworkAddress ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4mpePeerExtGateway ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4mpePeerExtCommSendStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpePeerExtECommSendStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpePeerExtPassive ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpePeerExtDefaultOriginate ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4mpePeerExtOverrideCapability ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4mpePeerExtConfigurePeer ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpePeerExtPeerRemoteAs ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsMIBgp4mpePeerExtEBGPMultiHop ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpePeerExtEBGPHopLimit ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpePeerExtNextHopSelf ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpePeerExtRflClient ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpePeerExtTcpSendBufSize ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpePeerExtTcpRcvBufSize ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpePeerExtLclAddress ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4mpePeerExtNetworkAddress ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4mpePeerExtGateway ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4mpePeerExtCommSendStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpePeerExtECommSendStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpePeerExtPassive ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpePeerExtDefaultOriginate ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpePeerExtOverrideCapability ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4MpePeerExtTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4MpeMEDTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeMEDTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeMEDTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeMEDTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeMEDTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeMEDAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeMEDRemoteAS ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4mpeMEDIPAddrAfi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeMEDIPAddrSafi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeMEDIPAddrPrefix ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpeMEDIPAddrPrefixLen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeMEDIntermediateAS ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpeMEDDirection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeMEDValue ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4mpeMEDPreference ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeMEDVrfName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4mpeMEDAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeMEDRemoteAS ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIBgp4mpeMEDIPAddrAfi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeMEDIPAddrSafi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeMEDIPAddrPrefix ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4mpeMEDIPAddrPrefixLen ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeMEDIntermediateAS ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4mpeMEDDirection ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeMEDValue ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIBgp4mpeMEDPreference ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeMEDVrfName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4mpeMEDAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeMEDRemoteAS ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIBgp4mpeMEDIPAddrAfi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeMEDIPAddrSafi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeMEDIPAddrPrefix ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4mpeMEDIPAddrPrefixLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeMEDIntermediateAS ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4mpeMEDDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeMEDValue ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIBgp4mpeMEDPreference ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeMEDVrfName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4MpeMEDTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4MpeLocalPrefTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeLocalPrefTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeLocalPrefTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeLocalPrefTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeLocalPrefTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeLocalPrefAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeLocalPrefRemoteAS ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4mpeLocalPrefIPAddrAfi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeLocalPrefIPAddrSafi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeLocalPrefIPAddrPrefix ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpeLocalPrefIPAddrPrefixLen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeLocalPrefIntermediateAS ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpeLocalPrefDirection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeLocalPrefValue ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4mpeLocalPrefPreference ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeLocalPrefVrfName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4mpeLocalPrefAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeLocalPrefRemoteAS ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIBgp4mpeLocalPrefIPAddrAfi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeLocalPrefIPAddrSafi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeLocalPrefIPAddrPrefix ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4mpeLocalPrefIPAddrPrefixLen ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeLocalPrefIntermediateAS ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4mpeLocalPrefDirection ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeLocalPrefValue ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIBgp4mpeLocalPrefPreference ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeLocalPrefVrfName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4mpeLocalPrefAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeLocalPrefRemoteAS ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIBgp4mpeLocalPrefIPAddrAfi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeLocalPrefIPAddrSafi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeLocalPrefIPAddrPrefix ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4mpeLocalPrefIPAddrPrefixLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeLocalPrefIntermediateAS ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4mpeLocalPrefDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeLocalPrefValue ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIBgp4mpeLocalPrefPreference ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeLocalPrefVrfName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4MpeLocalPrefTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4MpeUpdateFilterTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeUpdateFilterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeUpdateFilterTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeUpdateFilterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeUpdateFilterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeUpdateFilterAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeUpdateFilterRemoteAS ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4mpeUpdateFilterIPAddrAfi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeUpdateFilterIPAddrSafi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeUpdateFilterIPAddrPrefix ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpeUpdateFilterIPAddrPrefixLen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeUpdateFilterIntermediateAS ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpeUpdateFilterDirection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeUpdateFilterAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeUpdateFilterVrfName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4mpeUpdateFilterAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeUpdateFilterRemoteAS ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIBgp4mpeUpdateFilterIPAddrAfi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeUpdateFilterIPAddrSafi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeUpdateFilterIPAddrPrefix ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4mpeUpdateFilterIPAddrPrefixLen ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeUpdateFilterIntermediateAS ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4mpeUpdateFilterDirection ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeUpdateFilterAction ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeUpdateFilterVrfName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4mpeUpdateFilterAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeUpdateFilterRemoteAS ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIBgp4mpeUpdateFilterIPAddrAfi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeUpdateFilterIPAddrSafi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeUpdateFilterIPAddrPrefix ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4mpeUpdateFilterIPAddrPrefixLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeUpdateFilterIntermediateAS ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4mpeUpdateFilterDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeUpdateFilterAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeUpdateFilterVrfName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4MpeUpdateFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4MpeAggregateTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeAggregateTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeAggregateTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeAggregateTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeAggregateTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeAggregateAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeAggregateIPAddrAfi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeAggregateIPAddrSafi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeAggregateIPAddrPrefix ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpeAggregateIPAddrPrefixLen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeAggregateAdvertise ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeAggregateVrfName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpeAggregateAsSet ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeAggregateAdvertiseRouteMapName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpeAggregateSuppressRouteMapName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4mpeAggregateAttributeRouteMapName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4mpeAggregateAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeAggregateIPAddrAfi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeAggregateIPAddrSafi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeAggregateIPAddrPrefix ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4mpeAggregateIPAddrPrefixLen ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeAggregateAdvertise ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeAggregateVrfName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4mpeAggregateAsSet ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeAggregateAdvertiseRouteMapName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4mpeAggregateSuppressRouteMapName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4mpeAggregateAttributeRouteMapName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4mpeAggregateAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeAggregateIPAddrAfi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeAggregateIPAddrSafi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeAggregateIPAddrPrefix ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4mpeAggregateIPAddrPrefixLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeAggregateAdvertise ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeAggregateVrfName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4mpeAggregateAsSet ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeAggregateAdvertiseRouteMapName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4mpeAggregateSuppressRouteMapName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4mpeAggregateAttributeRouteMapName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4MpeAggregateTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4MpeImportRouteTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeImportRouteTable ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeImportRouteTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeImportRouteTable ARG_LIST((INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeImportRouteTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeImportRouteAction ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4mpeImportRouteAction ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4mpeImportRouteAction ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4MpeImportRouteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4MpeFsmTransitionHistTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeFsmTransitionHistTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeFsmTransitionHistTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeFsmTransitionHistTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeFsmTransitionHistTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeFsmTransitionHist ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsMIBgp4MpeRfdRtDampHistTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeRfdRtDampHistTable ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeRfdRtDampHistTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeRfdRtDampHistTable ARG_LIST((INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeRfdRtDampHistTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeRfdRtFom ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeRfdRtLastUpdtTime ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeRfdRtState ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeRfdRtStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeRfdRtFlapCount ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeRfdRtFlapTime ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeRfdRtReuseTime ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for FsMIBgp4MpeRfdPeerDampHistTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeRfdPeerDampHistTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeRfdPeerDampHistTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeRfdPeerDampHistTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeRfdPeerDampHistTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeRfdPeerFom ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeRfdPeerLastUpdtTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeRfdPeerState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeRfdPeerStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for FsMIBgp4MpeRfdRtsReuseListTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeRfdRtsReuseListTable ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeRfdRtsReuseListTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeRfdRtsReuseListTable ARG_LIST((INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeRfdRtsReuseListTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeRfdRtReuseListRtFom ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeRfdRtReuseListRtLastUpdtTime ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeRfdRtReuseListRtState ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeRfdRtReuseListRtStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for FsMIBgp4MpeRfdPeerReuseListTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeRfdPeerReuseListTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeRfdPeerReuseListTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeRfdPeerReuseListTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeRfdPeerReuseListTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeRfdPeerReuseListPeerFom ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeRfdPeerReuseListLastUpdtTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeRfdPeerReuseListPeerState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeRfdPeerReuseListPeerStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for FsMIBgp4MpeCommRouteAddCommTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeCommRouteAddCommTable ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeCommRouteAddCommTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeCommRouteAddCommTable ARG_LIST((INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeCommRouteAddCommTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeAddCommRowStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4mpeAddCommRowStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4mpeAddCommRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4MpeCommRouteAddCommTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4MpeCommRouteDeleteCommTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeCommRouteDeleteCommTable ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeCommRouteDeleteCommTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeCommRouteDeleteCommTable ARG_LIST((INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeCommRouteDeleteCommTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeDeleteCommRowStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4mpeDeleteCommRowStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4mpeDeleteCommRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4MpeCommRouteDeleteCommTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4MpeCommRouteCommSetStatusTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeCommRouteCommSetStatusTable ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeCommRouteCommSetStatusTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeCommRouteCommSetStatusTable ARG_LIST((INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeCommRouteCommSetStatusTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeCommSetStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeCommSetStatusRowStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4mpeCommSetStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeCommSetStatusRowStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4mpeCommSetStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeCommSetStatusRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4MpeCommRouteCommSetStatusTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4MpeExtCommRouteAddExtCommTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeExtCommRouteAddExtCommTable ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeExtCommRouteAddExtCommTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeExtCommRouteAddExtCommTable ARG_LIST((INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeExtCommRouteAddExtCommTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeAddExtCommRowStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4mpeAddExtCommRowStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4mpeAddExtCommRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4MpeExtCommRouteAddExtCommTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4MpeExtCommRouteDeleteExtCommTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeExtCommRouteDeleteExtCommTable ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeExtCommRouteDeleteExtCommTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeExtCommRouteDeleteExtCommTable ARG_LIST((INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeExtCommRouteDeleteExtCommTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeDeleteExtCommRowStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4mpeDeleteExtCommRowStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4mpeDeleteExtCommRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4MpeExtCommRouteDeleteExtCommTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4MpeExtCommRouteExtCommSetStatusTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeExtCommRouteExtCommSetStatusTable ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeExtCommRouteExtCommSetStatusTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeExtCommRouteExtCommSetStatusTable ARG_LIST((INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeExtCommRouteExtCommSetStatusTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeExtCommSetStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeExtCommSetStatusRowStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4mpeExtCommSetStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeExtCommSetStatusRowStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4mpeExtCommSetStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeExtCommSetStatusRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4MpeExtCommRouteExtCommSetStatusTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4MpePeerLinkBwTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpePeerLinkBwTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpePeerLinkBwTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpePeerLinkBwTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpePeerLinkBwTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeLinkBandWidth ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIBgp4mpePeerLinkBwRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4mpeLinkBandWidth ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsMIBgp4mpePeerLinkBwRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4mpeLinkBandWidth ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsMIBgp4mpePeerLinkBwRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4MpePeerLinkBwTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4MpeCapSupportedCapsTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeCapSupportedCapsTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeCapSupportedCapsTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeCapSupportedCapsTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeCapSupportedCapsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeCapSupportedCapsRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeCapAnnouncedStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeCapReceivedStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeCapNegotiatedStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4mpeCapConfiguredStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4mpeCapSupportedCapsRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4mpeCapSupportedCapsRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4MpeCapSupportedCapsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4MpeRtRefreshInboundTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeRtRefreshInboundTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeRtRefreshInboundTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeRtRefreshInboundTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeRtRefreshInboundTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeRtRefreshInboundRequest ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4mpeRtRefreshInboundPrefixFilter ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4mpeRtRefreshInboundRequest ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4mpeRtRefreshInboundPrefixFilter ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4mpeRtRefreshInboundRequest ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4mpeRtRefreshInboundPrefixFilter ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4MpeRtRefreshInboundTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4MpeRtRefreshStatisticsTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeRtRefreshStatisticsTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeRtRefreshStatisticsTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeRtRefreshStatisticsTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeRtRefreshStatisticsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeRtRefreshStatisticsRtRefMsgSentCntr ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4mpeRtRefreshStatisticsRtRefMsgTxErrCntr ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4mpeRtRefreshStatisticsRtRefMsgRcvdCntr ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4mpeRtRefreshStatisticsRtRefMsgInvalidCntr ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIBgp4MpeSoftReconfigOutboundTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpeSoftReconfigOutboundTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpeSoftReconfigOutboundTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpeSoftReconfigOutboundTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpeSoftReconfigOutboundTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4mpeSoftReconfigOutboundRequest ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4mpeSoftReconfigOutboundRequest ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4mpeSoftReconfigOutboundRequest ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4MpeSoftReconfigOutboundTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4MpePrefixCountersTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4MpePrefixCountersTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4MpePrefixCountersTable  */

INT1
nmhGetFirstIndexFsMIBgp4MpePrefixCountersTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4MpePrefixCountersTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4MpePrefixCountersPrefixesReceived ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4MpePrefixCountersPrefixesSent ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4MpePrefixCountersWithdrawsReceived ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4MpePrefixCountersWithdrawsSent ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4MpePrefixCountersInPrefixes ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4MpePrefixCountersInPrefixesAccepted ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4MpePrefixCountersInPrefixesRejected ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIBgp4MpePrefixCountersOutPrefixes ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIBgp4DistInOutRouteMapTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4DistInOutRouteMapTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4DistInOutRouteMapTable  */

INT1
nmhGetFirstIndexFsMIBgp4DistInOutRouteMapTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4DistInOutRouteMapTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4DistInOutRouteMapValue ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4DistInOutRouteMapRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4DistInOutRouteMapValue ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4DistInOutRouteMapRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4DistInOutRouteMapValue ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4DistInOutRouteMapRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4DistInOutRouteMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4NeighborRouteMapTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4NeighborRouteMapTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4NeighborRouteMapTable  */

INT1
nmhGetFirstIndexFsMIBgp4NeighborRouteMapTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4NeighborRouteMapTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4NeighborRouteMapName ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4NeighborRouteMapRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4NeighborRouteMapName ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4NeighborRouteMapRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4NeighborRouteMapName ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4NeighborRouteMapRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4NeighborRouteMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4PeerGroupTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4PeerGroupTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4PeerGroupTable  */

INT1
nmhGetFirstIndexFsMIBgp4PeerGroupTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4PeerGroupTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4PeerGroupAddrType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupRemoteAs ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIBgp4PeerGroupHoldTimeConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupKeepAliveConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupConnectRetryInterval ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupMinASOriginInterval ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupMinRouteAdvInterval ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupAllowAutomaticStart ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupAllowAutomaticStop ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupIdleHoldTimeConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupDampPeerOscillations ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupDelayOpen ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupDelayOpenTimeConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupPrefixUpperLimit ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupTcpConnectRetryCnt ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupEBGPMultiHop ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupEBGPHopLimit ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupNextHopSelf ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupRflClient ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupTcpSendBufSize ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupTcpRcvBufSize ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupCommSendStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupECommSendStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupPassive ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupDefaultOriginate ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupActivateMPCapability ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupDeactivateMPCapability ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupRouteMapNameIn ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4PeerGroupRouteMapNameOut ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4PeerGroupStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupIpPrefixNameIn ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4PeerGroupIpPrefixNameOut ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4PeerGroupOrfType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIBgp4PeerGroupOrfCapMode ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupOrfRequest ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupBfdStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupOverrideCapability ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4PeerGroupLocalAs ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4PeerGroupRemoteAs ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsMIBgp4PeerGroupHoldTimeConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupKeepAliveConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupConnectRetryInterval ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupMinASOriginInterval ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupMinRouteAdvInterval ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupAllowAutomaticStart ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupAllowAutomaticStop ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupIdleHoldTimeConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupDampPeerOscillations ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupDelayOpen ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupDelayOpenTimeConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupPrefixUpperLimit ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupTcpConnectRetryCnt ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupEBGPMultiHop ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupEBGPHopLimit ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupNextHopSelf ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupRflClient ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupTcpSendBufSize ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupTcpRcvBufSize ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupCommSendStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupECommSendStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupPassive ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupDefaultOriginate ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupActivateMPCapability ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupDeactivateMPCapability ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupRouteMapNameIn ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4PeerGroupRouteMapNameOut ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4PeerGroupStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupIpPrefixNameIn ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4PeerGroupIpPrefixNameOut ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4PeerGroupOrfType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsMIBgp4PeerGroupOrfCapMode ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupOrfRequest ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupBfdStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupOverrideCapability ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4PeerGroupLocalAs ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4PeerGroupRemoteAs ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupHoldTimeConfigured ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupKeepAliveConfigured ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupConnectRetryInterval ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupMinASOriginInterval ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupMinRouteAdvInterval ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupAllowAutomaticStart ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupAllowAutomaticStop ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupIdleHoldTimeConfigured ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupDampPeerOscillations ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupDelayOpen ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupDelayOpenTimeConfigured ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupPrefixUpperLimit ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupTcpConnectRetryCnt ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupEBGPMultiHop ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupEBGPHopLimit ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupNextHopSelf ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupRflClient ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupTcpSendBufSize ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupTcpRcvBufSize ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupCommSendStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupECommSendStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupPassive ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupDefaultOriginate ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupActivateMPCapability ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupDeactivateMPCapability ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupRouteMapNameIn ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4PeerGroupRouteMapNameOut ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4PeerGroupStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupIpPrefixNameIn ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4PeerGroupIpPrefixNameOut ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4PeerGroupOrfType ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupOrfCapMode ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupOrfRequest ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupBfdStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupOverrideCapability ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4PeerGroupLocalAs ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4PeerGroupTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4PeerGroupListTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4PeerGroupListTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4PeerGroupListTable  */

INT1
nmhGetFirstIndexFsMIBgp4PeerGroupListTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4PeerGroupListTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4PeerAddStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4PeerAddStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4PeerAddStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4PeerGroupListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4RestartReason ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4RestartReason ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4RestartReason ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4RestartReason ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4TCPMKTAuthTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4TCPMKTAuthTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4TCPMKTAuthTable  */

INT1
nmhGetFirstIndexFsMIBgp4TCPMKTAuthTable ARG_LIST((INT4 * , INT4 *));

/* Proto Validate Index Instance for FsMIBgp4ORFListTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4ORFListTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4ORFListTable  */

INT1
nmhGetFirstIndexFsMIBgp4ORFListTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , UINT4 * , UINT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , UINT4 * , UINT4 * , INT4 *));
/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4TCPMKTAuthTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4TCPMKTAuthRecvKeyId ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4TCPMKTAuthMasterKey ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4TCPMKTAuthAlgo ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4TCPMKTAuthTcpOptExc ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4TCPMKTAuthRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4TCPMKTAuthRecvKeyId ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4TCPMKTAuthMasterKey ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4TCPMKTAuthAlgo ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4TCPMKTAuthTcpOptExc ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4TCPMKTAuthRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4TCPMKTAuthRecvKeyId ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4TCPMKTAuthMasterKey ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4TCPMKTAuthAlgo ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4TCPMKTAuthTcpOptExc ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4TCPMKTAuthRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4TCPMKTAuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIBgp4TCPAOAuthPeerTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4TCPAOAuthPeerTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4TCPAOAuthPeerTable  */

INT1
nmhGetFirstIndexFsMIBgp4TCPAOAuthPeerTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4TCPAOAuthPeerTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4TCPAOAuthKeyStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsMIBgp4TCPAOAuthKeyStartAccept ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4TCPAOAuthKeyStartGenerate ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4TCPAOAuthKeyStopGenerate ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIBgp4TCPAOAuthKeyStopAccept ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4TCPAOAuthKeyStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsMIBgp4TCPAOAuthKeyStartAccept ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4TCPAOAuthKeyStartGenerate ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4TCPAOAuthKeyStopGenerate ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIBgp4TCPAOAuthKeyStopAccept ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4TCPAOAuthKeyStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsMIBgp4TCPAOAuthKeyStartAccept ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4TCPAOAuthKeyStartGenerate ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4TCPAOAuthKeyStopGenerate ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIBgp4TCPAOAuthKeyStopAccept ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4TCPAOAuthPeerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhGetNextIndexFsMIBgp4ORFListTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));
/* Proto Validate Index Instance for FsMIBgp4RRDNetworkTable. */
INT1
nmhValidateIndexInstanceFsMIBgp4RRDNetworkTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIBgp4RRDNetworkTable  */

INT1
nmhGetFirstIndexFsMIBgp4RRDNetworkTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIBgp4RRDNetworkTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIBgp4RRDNetworkAddrType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4RRDNetworkPrefixLen ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIBgp4RRDNetworkRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIBgp4RRDNetworkAddrType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4RRDNetworkPrefixLen ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIBgp4RRDNetworkRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIBgp4RRDNetworkAddrType ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4RRDNetworkPrefixLen ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIBgp4RRDNetworkRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4RRDNetworkTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */
INT1
nmhGetFsMIBgp4MacMobDuplicationTimeInterval ARG_LIST((INT4 *));

INT1
nmhGetFsMIBgp4MaxMacMoves ARG_LIST((INT4 *));
                                                        
/* Low Level SET Routine for All Objects.  */                                                                             
INT1
nmhSetFsMIBgp4MacMobDuplicationTimeInterval ARG_LIST((INT4 ));

INT1
nmhSetFsMIBgp4MaxMacMoves ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */                                                                                                                          
INT1
nmhTestv2FsMIBgp4MacMobDuplicationTimeInterval ARG_LIST((UINT4 *  ,INT4 ));
INT1
nmhTestv2FsMIBgp4MaxMacMoves ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIBgp4MacMobDuplicationTimeInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIBgp4MaxMacMoves ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
