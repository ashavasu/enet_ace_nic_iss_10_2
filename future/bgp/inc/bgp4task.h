/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved    
 *
 * $Id: bgp4task.h,v 1.30 2018/01/03 11:31:18 siva Exp $
 *
 * Description:Contain the definitions of the various task queues             
 *             used in the system.     
 *
 *******************************************************************/
#ifndef BGP4TASK_H
#define  BGP4TASK_H

/* These events will be reported to the BGP task */
#define  BGP4_TMR_EVENT                         (0x00000001)
#define  BGP4_INPUT_Q_EVENT                     (0x00000002)
#define  BGP4_1S_TMR_EVENT                      (0x00000004)
#define  BGP4_GR_TMR_EVENT                      (0x00000008)
#define  BGP4_RM_EVENT                          (0x00000010)

/* These events are embedded in the BGP4_INPUT_Q_MSG */
#define  BGP4_SNMP_GLOBAL_ADMIN_UP_EVENT           (1)
#define  BGP4_SNMP_GLOBAL_ADMIN_DOWN_EVENT         (2)
#define  BGP4_SNMP_PEER_CREATE_EVENT               (3)
#define  BGP4_SNMP_PEER_DELETE_EVENT               (4)
#define  BGP4_SNMP_START_EVENT                     (5)
#define  BGP4_SNMP_STOP_EVENT                      (6)
#define  BGP4_IGP_RRD_DISABLE_EVENT                (7)
#define  BGP4_RRD_DISABLE_EVENT                    (8)
#define  BGP4_RTM_REG_EVENT                        (9)
#define  BGP4_RTM_RECV_INFO_EVENT                  (10)
#define  BGP4_SNMP_LOCAL_BGP_ID_CHANGE_EVENT       (11)
#define  BGP4_CLI_NO_ROUTER_BGP_EVENT              (12)
#define  BGP4_SNMP_NON_BGP_RT_ADVT_EVENT           (13)
#define  BGP4_SNMP_EBGP_MULTIHOP_EVENT             (14)
#define  BGP4_SNMP_RFL_CLUSTER_ID_EVENT            (15)
#define  BGP4_SNMP_RFL_PEER_TYPE_EVENT             (16)
#define  BGP4_IGP_METRIC_SET_EVENT                 (17)
#define  BGP4_SNMP_INBOUND_SOFTCONFIG_RTREF_EVENT  (18)
#define  BGP4_SNMP_OUTBOUND_SOFTCONFIG_EVENT       (19)
#define  BGP4_CLI_CLEAR_IP_BGP_EVENT               (20)
#define  BGP4_SNMP_PEER_SRCADDR_EVENT              (21)
#define  BGP4_SNMP_RFL_CLIENT_SUPP_EVENT           (22)
#define  BGP4_AGGR_POLICY_CHG_EVENT                (23)
#define  BGP4_AGGR_ADMIN_CHG_EVENT                 (24)
#define  BGP4_PEER_DEF_ROUTE_ORIG_CHG_EVENT        (25)
#define  BGP4_EXPORT_TARGET_CHG_EVENT              (26)
#define  BGP4_IFACE_CHG_EVENT                      (27)
#ifdef L3VPN
#define  BGP4_LSP_STATE_CHG_EVENT                  (28)
#endif




#define  BGP4_RTM6_REG_EVENT                        (29)

#ifdef ROUTEMAP_WANTED
#define  BGP4_ROUTEMAP_STATUS_CHG_EVENT             (30)
#endif /* ROUTEMAP_WANTED */
#define  BGP4_TCP_MD5_PASSWORD_CONFIG_EVENT         (31)
#define  BGP4_TCP_MD5_PASSWORD_REMOVE_EVENT         (32)
#define  BGP4_SNMP_PEER_GROUP_DELETE_EVENT          (33)
#define  BGP4_SNMP_PEER_GROUP_ENABLE_EVENT          (34)
#define  BGP4_SNMP_PEER_GROUP_DISABLE_EVENT         (35)
#define  BGP4_PEER_GROUP_CLEAR_EVENT                (36)
#define  BGP4_PEER_GROUP_SOFT_CLEAR_IN_EVENT        (37)
#define  BGP4_PEER_GROUP_SOFT_CLEAR_OUT_EVENT       (38)
#define  BGP4_PEER_GROUP_SOFT_CLEAR_BOTH_EVENT      (40)
#ifdef RFD_WANTED
#define  BGP4_RFD_ADMIN_CHG_EVENT                   (39)
#endif
#define BGP4_VCM_EVENT                              (41)
#define BGP4_LOCAL_AS_CFG_EVENT                     (42)
#define BGP4_TCPAO_ICMPACCEPT_CFG_EVENT             (43)
#define BGP4_TCPAO_NOMKT_PKTDISC_CFG_EVENT          (44)
#define BGP4_TCPAO_MKT_ASSOCIATE_CFG_EVENT          (45)
#define BGP4_TCPAO_MKT_NO_ASSOCIATE_CFG_EVENT       (46)
#define BGP4_SNMP_INBOUND_ORF_EVENT                 (47)
#define BGP4_PEER_GROUP_ORF_EVENT                   (48)
#define BGP4_PEER_SET_BFD_STATUS                    (49)
#define BGP4_PEER_GROUP_SET_BFD_STATUS              (50)
#define BGP4_PEER_HANDLE_SESSION_DOWN_NOTIFICATION  (51)
#ifdef L3VPN
#define  BGP4_VRF_ADMIN_STATE_CHG_EVENT             (52)
#define  BGP4_ROUTE_PARAMS_CHG_EVENT                (53)
#endif
/* L2VPN-VPLS Message from L2VPN */
#ifdef VPLSADS_WANTED
#define BGP4_L2VPN_MESSAGE                          (54)
#endif
#ifdef L3VPN
#define   BGP4_VPNV4_ROUTE_TO_PEER_EVENT             (55)
#define   BGP4_RSVPTE_LSP_STATE_CHG_EVENT           (56)
#endif
#ifdef EVPN_WANTED
#define   BGP4_EVPN_VRF_ADMIN_STATE_CHG_EVENT     (57)
#define   BGP4_EVPN_ROUTE_PARAMS_CHG_EVENT          (58)
#define   BGP4_EVPN_NOTIFY_MAC                      (59)
#define   BGP4_EVPN_ROUTE_TO_PEER_EVENT             (60)
#define   BGP4_EVPN_GET_MAC_EVENT                   (61)
#define   BGP4_EVPN_NOTIFY_ESI                      (62)
#define   BGP4_EVPN_NOTIFY_ETH_AD                   (63)

#endif
#define  BGP4_RTM_ROUTE_UPDATE_EVENT                     (64)
#define  BGP4_RTM_ROUTE_CHANGE_NOTIFY_EVENT              (65)
#define  BGP4_RTM6_ROUTE_UPDATE_EVENT    (66)
#define  BGP4_RTM6_ROUTE_CHANGE_NOTIFY_EVENT              (67)
#define   BGP4_ROUTE_CHG_EVENT                   (68)
#endif /* BGP4TASK_H */
