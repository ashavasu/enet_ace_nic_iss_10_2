/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdbgpwr.h,v 1.4 2011/03/18 12:05:13 siva Exp $
 *
 * Description:
 ********************************************************************/

#ifndef _STDBGPWR_H
#define _STDBGPWR_H

VOID RegisterSTDBGP(VOID);

VOID UnRegisterSTDBGP(VOID);
INT4 BgpVersionGet(tSnmpIndex *, tRetVal *);
INT4 BgpLocalAsGet(tSnmpIndex *, tRetVal *);
INT4 BgpIdentifierGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexBgpPeerTable(tSnmpIndex *, tSnmpIndex *);
INT4 BgpPeerIdentifierGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerStateGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerNegotiatedVersionGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerLocalAddrGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerLocalPortGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerRemoteAddrGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerRemotePortGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerRemoteAsGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerInUpdatesGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerOutUpdatesGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerInTotalMessagesGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerOutTotalMessagesGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerLastErrorGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerFsmEstablishedTransitionsGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerFsmEstablishedTimeGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerConnectRetryIntervalGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerHoldTimeGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerKeepAliveGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerHoldTimeConfiguredGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerKeepAliveConfiguredGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerMinASOriginationIntervalGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerMinRouteAdvertisementIntervalGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerInUpdateElapsedTimeGet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerConnectRetryIntervalSet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerHoldTimeConfiguredSet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerKeepAliveConfiguredSet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerMinASOriginationIntervalSet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerMinRouteAdvertisementIntervalSet(tSnmpIndex *, tRetVal *);
INT4 BgpPeerAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 BgpPeerConnectRetryIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 BgpPeerHoldTimeConfiguredTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 BgpPeerKeepAliveConfiguredTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 BgpPeerMinASOriginationIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 BgpPeerMinRouteAdvertisementIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 BgpPeerTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexBgp4PathAttrTable(tSnmpIndex *, tSnmpIndex *);
INT4 Bgp4PathAttrPeerGet(tSnmpIndex *, tRetVal *);
INT4 Bgp4PathAttrIpAddrPrefixLenGet(tSnmpIndex *, tRetVal *);
INT4 Bgp4PathAttrIpAddrPrefixGet(tSnmpIndex *, tRetVal *);
INT4 Bgp4PathAttrOriginGet(tSnmpIndex *, tRetVal *);
INT4 Bgp4PathAttrASPathSegmentGet(tSnmpIndex *, tRetVal *);
INT4 Bgp4PathAttrNextHopGet(tSnmpIndex *, tRetVal *);
INT4 Bgp4PathAttrMultiExitDiscGet(tSnmpIndex *, tRetVal *);
INT4 Bgp4PathAttrLocalPrefGet(tSnmpIndex *, tRetVal *);
INT4 Bgp4PathAttrAtomicAggregateGet(tSnmpIndex *, tRetVal *);
INT4 Bgp4PathAttrAggregatorASGet(tSnmpIndex *, tRetVal *);
INT4 Bgp4PathAttrAggregatorAddrGet(tSnmpIndex *, tRetVal *);
INT4 Bgp4PathAttrCalcLocalPrefGet(tSnmpIndex *, tRetVal *);
INT4 Bgp4PathAttrBestGet(tSnmpIndex *, tRetVal *);
INT4 Bgp4PathAttrUnknownGet(tSnmpIndex *, tRetVal *);
#endif /* _STDBGPWR_H */
