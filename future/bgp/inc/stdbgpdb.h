/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdbgpdb.h,v 1.5 2011/03/18 12:05:13 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDBGPDB_H
#define _STDBGPDB_H

UINT1 BgpPeerTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Bgp4PathAttrTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};

UINT4 stdbgp [] ={1,3,6,1,2,1,15};
tSNMP_OID_TYPE stdbgpOID = {7, stdbgp};


UINT4 BgpVersion [ ] ={1,3,6,1,2,1,15,1};
UINT4 BgpLocalAs [ ] ={1,3,6,1,2,1,15,2};
UINT4 BgpIdentifier [ ] ={1,3,6,1,2,1,15,4};
UINT4 BgpPeerIdentifier [ ] ={1,3,6,1,2,1,15,3,1,1};
UINT4 BgpPeerState [ ] ={1,3,6,1,2,1,15,3,1,2};
UINT4 BgpPeerAdminStatus [ ] ={1,3,6,1,2,1,15,3,1,3};
UINT4 BgpPeerNegotiatedVersion [ ] ={1,3,6,1,2,1,15,3,1,4};
UINT4 BgpPeerLocalAddr [ ] ={1,3,6,1,2,1,15,3,1,5};
UINT4 BgpPeerLocalPort [ ] ={1,3,6,1,2,1,15,3,1,6};
UINT4 BgpPeerRemoteAddr [ ] ={1,3,6,1,2,1,15,3,1,7};
UINT4 BgpPeerRemotePort [ ] ={1,3,6,1,2,1,15,3,1,8};
UINT4 BgpPeerRemoteAs [ ] ={1,3,6,1,2,1,15,3,1,9};
UINT4 BgpPeerInUpdates [ ] ={1,3,6,1,2,1,15,3,1,10};
UINT4 BgpPeerOutUpdates [ ] ={1,3,6,1,2,1,15,3,1,11};
UINT4 BgpPeerInTotalMessages [ ] ={1,3,6,1,2,1,15,3,1,12};
UINT4 BgpPeerOutTotalMessages [ ] ={1,3,6,1,2,1,15,3,1,13};
UINT4 BgpPeerLastError [ ] ={1,3,6,1,2,1,15,3,1,14};
UINT4 BgpPeerFsmEstablishedTransitions [ ] ={1,3,6,1,2,1,15,3,1,15};
UINT4 BgpPeerFsmEstablishedTime [ ] ={1,3,6,1,2,1,15,3,1,16};
UINT4 BgpPeerConnectRetryInterval [ ] ={1,3,6,1,2,1,15,3,1,17};
UINT4 BgpPeerHoldTime [ ] ={1,3,6,1,2,1,15,3,1,18};
UINT4 BgpPeerKeepAlive [ ] ={1,3,6,1,2,1,15,3,1,19};
UINT4 BgpPeerHoldTimeConfigured [ ] ={1,3,6,1,2,1,15,3,1,20};
UINT4 BgpPeerKeepAliveConfigured [ ] ={1,3,6,1,2,1,15,3,1,21};
UINT4 BgpPeerMinASOriginationInterval [ ] ={1,3,6,1,2,1,15,3,1,22};
UINT4 BgpPeerMinRouteAdvertisementInterval [ ] ={1,3,6,1,2,1,15,3,1,23};
UINT4 BgpPeerInUpdateElapsedTime [ ] ={1,3,6,1,2,1,15,3,1,24};
UINT4 Bgp4PathAttrPeer [ ] ={1,3,6,1,2,1,15,6,1,1};
UINT4 Bgp4PathAttrIpAddrPrefixLen [ ] ={1,3,6,1,2,1,15,6,1,2};
UINT4 Bgp4PathAttrIpAddrPrefix [ ] ={1,3,6,1,2,1,15,6,1,3};
UINT4 Bgp4PathAttrOrigin [ ] ={1,3,6,1,2,1,15,6,1,4};
UINT4 Bgp4PathAttrASPathSegment [ ] ={1,3,6,1,2,1,15,6,1,5};
UINT4 Bgp4PathAttrNextHop [ ] ={1,3,6,1,2,1,15,6,1,6};
UINT4 Bgp4PathAttrMultiExitDisc [ ] ={1,3,6,1,2,1,15,6,1,7};
UINT4 Bgp4PathAttrLocalPref [ ] ={1,3,6,1,2,1,15,6,1,8};
UINT4 Bgp4PathAttrAtomicAggregate [ ] ={1,3,6,1,2,1,15,6,1,9};
UINT4 Bgp4PathAttrAggregatorAS [ ] ={1,3,6,1,2,1,15,6,1,10};
UINT4 Bgp4PathAttrAggregatorAddr [ ] ={1,3,6,1,2,1,15,6,1,11};
UINT4 Bgp4PathAttrCalcLocalPref [ ] ={1,3,6,1,2,1,15,6,1,12};
UINT4 Bgp4PathAttrBest [ ] ={1,3,6,1,2,1,15,6,1,13};
UINT4 Bgp4PathAttrUnknown [ ] ={1,3,6,1,2,1,15,6,1,14};


tMbDbEntry stdbgpMibEntry[]= {

{{8,BgpVersion}, NULL, BgpVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,BgpLocalAs}, NULL, BgpLocalAsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,BgpPeerIdentifier}, GetNextIndexBgpPeerTable, BgpPeerIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerState}, GetNextIndexBgpPeerTable, BgpPeerStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerAdminStatus}, GetNextIndexBgpPeerTable, BgpPeerAdminStatusGet, BgpPeerAdminStatusSet, BgpPeerAdminStatusTest, BgpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerNegotiatedVersion}, GetNextIndexBgpPeerTable, BgpPeerNegotiatedVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerLocalAddr}, GetNextIndexBgpPeerTable, BgpPeerLocalAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerLocalPort}, GetNextIndexBgpPeerTable, BgpPeerLocalPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerRemoteAddr}, GetNextIndexBgpPeerTable, BgpPeerRemoteAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerRemotePort}, GetNextIndexBgpPeerTable, BgpPeerRemotePortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerRemoteAs}, GetNextIndexBgpPeerTable, BgpPeerRemoteAsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerInUpdates}, GetNextIndexBgpPeerTable, BgpPeerInUpdatesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerOutUpdates}, GetNextIndexBgpPeerTable, BgpPeerOutUpdatesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerInTotalMessages}, GetNextIndexBgpPeerTable, BgpPeerInTotalMessagesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerOutTotalMessages}, GetNextIndexBgpPeerTable, BgpPeerOutTotalMessagesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerLastError}, GetNextIndexBgpPeerTable, BgpPeerLastErrorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerFsmEstablishedTransitions}, GetNextIndexBgpPeerTable, BgpPeerFsmEstablishedTransitionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerFsmEstablishedTime}, GetNextIndexBgpPeerTable, BgpPeerFsmEstablishedTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerConnectRetryInterval}, GetNextIndexBgpPeerTable, BgpPeerConnectRetryIntervalGet, BgpPeerConnectRetryIntervalSet, BgpPeerConnectRetryIntervalTest, BgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerHoldTime}, GetNextIndexBgpPeerTable, BgpPeerHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerKeepAlive}, GetNextIndexBgpPeerTable, BgpPeerKeepAliveGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerHoldTimeConfigured}, GetNextIndexBgpPeerTable, BgpPeerHoldTimeConfiguredGet, BgpPeerHoldTimeConfiguredSet, BgpPeerHoldTimeConfiguredTest, BgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerKeepAliveConfigured}, GetNextIndexBgpPeerTable, BgpPeerKeepAliveConfiguredGet, BgpPeerKeepAliveConfiguredSet, BgpPeerKeepAliveConfiguredTest, BgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerMinASOriginationInterval}, GetNextIndexBgpPeerTable, BgpPeerMinASOriginationIntervalGet, BgpPeerMinASOriginationIntervalSet, BgpPeerMinASOriginationIntervalTest, BgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerMinRouteAdvertisementInterval}, GetNextIndexBgpPeerTable, BgpPeerMinRouteAdvertisementIntervalGet, BgpPeerMinRouteAdvertisementIntervalSet, BgpPeerMinRouteAdvertisementIntervalTest, BgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{10,BgpPeerInUpdateElapsedTime}, GetNextIndexBgpPeerTable, BgpPeerInUpdateElapsedTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, BgpPeerTableINDEX, 1, 0, 0, NULL},

{{8,BgpIdentifier}, NULL, BgpIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Bgp4PathAttrPeer}, GetNextIndexBgp4PathAttrTable, Bgp4PathAttrPeerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, Bgp4PathAttrTableINDEX, 3, 0, 0, NULL},

{{10,Bgp4PathAttrIpAddrPrefixLen}, GetNextIndexBgp4PathAttrTable, Bgp4PathAttrIpAddrPrefixLenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Bgp4PathAttrTableINDEX, 3, 0, 0, NULL},

{{10,Bgp4PathAttrIpAddrPrefix}, GetNextIndexBgp4PathAttrTable, Bgp4PathAttrIpAddrPrefixGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, Bgp4PathAttrTableINDEX, 3, 0, 0, NULL},

{{10,Bgp4PathAttrOrigin}, GetNextIndexBgp4PathAttrTable, Bgp4PathAttrOriginGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Bgp4PathAttrTableINDEX, 3, 0, 0, NULL},

{{10,Bgp4PathAttrASPathSegment}, GetNextIndexBgp4PathAttrTable, Bgp4PathAttrASPathSegmentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Bgp4PathAttrTableINDEX, 3, 0, 0, NULL},

{{10,Bgp4PathAttrNextHop}, GetNextIndexBgp4PathAttrTable, Bgp4PathAttrNextHopGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, Bgp4PathAttrTableINDEX, 3, 0, 0, NULL},

{{10,Bgp4PathAttrMultiExitDisc}, GetNextIndexBgp4PathAttrTable, Bgp4PathAttrMultiExitDiscGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Bgp4PathAttrTableINDEX, 3, 0, 0, NULL},

{{10,Bgp4PathAttrLocalPref}, GetNextIndexBgp4PathAttrTable, Bgp4PathAttrLocalPrefGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Bgp4PathAttrTableINDEX, 3, 0, 0, NULL},

{{10,Bgp4PathAttrAtomicAggregate}, GetNextIndexBgp4PathAttrTable, Bgp4PathAttrAtomicAggregateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Bgp4PathAttrTableINDEX, 3, 0, 0, NULL},

{{10,Bgp4PathAttrAggregatorAS}, GetNextIndexBgp4PathAttrTable, Bgp4PathAttrAggregatorASGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Bgp4PathAttrTableINDEX, 3, 0, 0, NULL},

{{10,Bgp4PathAttrAggregatorAddr}, GetNextIndexBgp4PathAttrTable, Bgp4PathAttrAggregatorAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, Bgp4PathAttrTableINDEX, 3, 0, 0, NULL},

{{10,Bgp4PathAttrCalcLocalPref}, GetNextIndexBgp4PathAttrTable, Bgp4PathAttrCalcLocalPrefGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Bgp4PathAttrTableINDEX, 3, 0, 0, NULL},

{{10,Bgp4PathAttrBest}, GetNextIndexBgp4PathAttrTable, Bgp4PathAttrBestGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Bgp4PathAttrTableINDEX, 3, 0, 0, NULL},

{{10,Bgp4PathAttrUnknown}, GetNextIndexBgp4PathAttrTable, Bgp4PathAttrUnknownGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Bgp4PathAttrTableINDEX, 3, 0, 0, NULL},
};
tMibData stdbgpEntry = { 41, stdbgpMibEntry };
#endif /* _STDBGPDB_H */

