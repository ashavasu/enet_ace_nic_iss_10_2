
# ifndef stdbgOCON_H
# define stdbgOCON_H
/*
 *  The Constant Declarations for
 *  bgp
 */

# define BGPVERSION                                        (1)
# define BGPLOCALAS                                        (2)
# define BGPIDENTIFIER                                     (4)

/*
 *  The Constant Declarations for
 *  bgpPeerTable
 */

# define BGPPEERIDENTIFIER                                 (1)
# define BGPPEERSTATE                                      (2)
# define BGPPEERADMINSTATUS                                (3)
# define BGPPEERNEGOTIATEDVERSION                          (4)
# define BGPPEERLOCALADDR                                  (5)
# define BGPPEERLOCALPORT                                  (6)
# define BGPPEERREMOTEADDR                                 (7)
# define BGPPEERREMOTEPORT                                 (8)
# define BGPPEERREMOTEAS                                   (9)
# define BGPPEERINUPDATES                                  (10)
# define BGPPEEROUTUPDATES                                 (11)
# define BGPPEERINTOTALMESSAGES                            (12)
# define BGPPEEROUTTOTALMESSAGES                           (13)
# define BGPPEERLASTERROR                                  (14)
# define BGPPEERFSMESTABLISHEDTRANSITIONS                  (15)
# define BGPPEERFSMESTABLISHEDTIME                         (16)
# define BGPPEERCONNECTRETRYINTERVAL                       (17)
# define BGPPEERHOLDTIME                                   (18)
# define BGPPEERKEEPALIVE                                  (19)
# define BGPPEERHOLDTIMECONFIGURED                         (20)
# define BGPPEERKEEPALIVECONFIGURED                        (21)
# define BGPPEERMINASORIGINATIONINTERVAL                   (22)
# define BGPPEERMINROUTEADVERTISEMENTINTERVAL              (23)
# define BGPPEERINUPDATEELAPSEDTIME                        (24)

/*
 *  The Constant Declarations for
 *  bgp4PathAttrTable
 */

# define BGP4PATHATTRPEER                                  (1)
# define BGP4PATHATTRIPADDRPREFIXLEN                       (2)
# define BGP4PATHATTRIPADDRPREFIX                          (3)
# define BGP4PATHATTRORIGIN                                (4)
# define BGP4PATHATTRASPATHSEGMENT                         (5)
# define BGP4PATHATTRNEXTHOP                               (6)
# define BGP4PATHATTRMULTIEXITDISC                         (7)
# define BGP4PATHATTRLOCALPREF                             (8)
# define BGP4PATHATTRATOMICAGGREGATE                       (9)
# define BGP4PATHATTRAGGREGATORAS                          (10)
# define BGP4PATHATTRAGGREGATORADDR                        (11)
# define BGP4PATHATTRCALCLOCALPREF                         (12)
# define BGP4PATHATTRBEST                                  (13)
# define BGP4PATHATTRUNKNOWN                               (14)

#endif /*  stdbgp4OCON_H  */
