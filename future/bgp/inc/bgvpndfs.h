/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgvpndfs.h,v 1.8 2018/01/03 11:31:18 siva Exp $
 *
 * Description: This file contains the macro defns
 *
 *******************************************************************/

#ifndef _BGVPNDFS_H
#define _BGVPNDFS_H

#define  BGP4_VPN4_PREFIX_LEN                   12 
#define  BGP4_MAX_VPN4_PREFIXLEN                32
#define  BGP4_VPN_RT_DISTING_SIZE               8 
#define  BGP4_VPN_PREFLENINBITS_FIELD_LEN       1
#define  BGP4_VPN4_EXT_COM_VALUE_SIZE           6
#define  BGP4_VPN4_SOO_EXT_COM_VALUE_SIZE       6
#define  BGP4_VPN4_RR_IMPORT_TARGETS_SET_SIZE   100
#define  L3VPN_INVALID_TNLID          (0x0)
#define  BGP4_INVALID_IGP_LABEL                 L3VPN_INVALID_TNLID

/* other than bgp success/failure */
#define  BGP4_VRF_MATCH_NOT_FOUND               2

/* portable definitions */
#define  BGP4_MAX_VRF_HASH_TBL_SIZE         32

#define BGP4_VPN4_EXT_COM_LOW_ORDER_OCTET_VALUE 0x02
/* Used by capabilities module for vpnv4 processing */
#define  CAP_MP_VPN4_UNICAST                0x00010080
#define  CAP_NEG_VPN4_UNI_MASK              0x00000008

/* Used by MPE module */
#define BGP4_MPE_VPN4_PREFIX_LEN            0x01 

#ifndef BGP4_IPV6_WANTED
/* If <ipv6, unicast> is not there, then <ipv4, unicast> and
 * <ipv4, label> is supported.
 */
#define BGP4_VPN4_UNI_INDEX                 2
#else
#define BGP4_VPN4_UNI_INDEX                 3
#endif

#define BGP4_MAX_VPN4_INET_ADDRESS_LEN      12  /* only for VPN4 */

#define BGP4_VPN4_NORMAL_PEER               0
#define BGP4_VPN4_CE_PEER                   1
#define BGP4_VPN4_PE_PEER                   2
#define BGP4_VPN4_CE_RT_SEND                1
#define BGP4_VPN4_CE_RT_DONOTSEND           2
#define BGP4_VPN4_RR_TARGETS_DEL_INTERVAL   7200 /* 2 hours */
#define BGP4_NO_EXPORT_TARGETS              1

#define BGP4_LABEL_BOTTOM_STACK_BIT         0x000001

/* TODO: These values must be defined to MPLS defined values. */
#define BGP4_MPLS_LSP_SET                   1
#define BGP4_MPLS_LSP_UNSET                 0

/* LSP states */
#define TE_OPER_UP                   1
#define TE_OPER_DOWN                 2
#define TE_DESTROY                   6

#define BGP4_LSP_UP                         TE_OPER_UP
#define BGP4_LSP_DOWN                       TE_OPER_DOWN
#define BGP4_LSP_DESTROY                    TE_DESTROY
#define BGP4_INVALID_LABEL                  0xFFFFFFFF


/* Label addition of deletion */
#define MPLS_MLIB_ILM_CREATE                0x0104
#define MPLS_MLIB_ILM_DELETE                0x0204
#define BGP4_FM_LABEL_ADD                   MPLS_MLIB_ILM_CREATE
#define BGP4_FM_LABEL_DELETE                MPLS_MLIB_ILM_DELETE

/* Label operation used in BGP */
#define MPLS_OPR_POP           2 
#define BGP4_LABEL_OPER_POP           MPLS_OPR_POP

#define BGP4_VPN4_RT_EXT_COM                    1 
#define BGP4_VPN4_SOO_EXT_COM                   2 
#define BGP4_VPN4_ROUTE_DISTINGUISHER           3 
#define BGP4_VPN4_AS_EXT_COM_TYPE               0x00
#define BGP4_VPN4_IPADDR_EXT_COM_TYPE           0x01
#define BGP4_VPN4_4BYTE_AS_EXT_COM_TYPE         0x02
#define BGP4_VPN4_RT_EXT_COM_LOW_ORDER_TYPE     0x02
#define BGP4_VPN4_SOO_EXT_COM_LOW_ORDER_TYPE    0x03
#define BGP4_VPN4_EXT_COM_2BYTE_AS_TYPE_LEN     2
#define BGP4_VPN4_EXT_COM_LOCAL_ADMIN_LEN       4
#define BGP4_VPN4_EXT_COM_4BYTE_LOCAL_ADMIN_LEN 4
#define BGP4_VPN4_EXT_COM_2BYTE_LOCAL_ADMIN_LEN 2
#define BGP4_VPN4_EXT_COM_IPADDR_LEN            4
#define BGP4_VPN4_MAX_4BYTE_VAL                 0xffffffff

/* VRF operational status. It tells the current state of a VRF */
#define L3VPN_UP                     1 
#define L3VPN_DOWN                   2 
#define L3VPN_CREATE                 3 
#define L3VPN_DELETE                 4 
#define BGP4_L3VPN_VRF_CREATE               L3VPN_CREATE
#define BGP4_L3VPN_VRF_DELETE               L3VPN_DELETE
#define BGP4_L3VPN_VRF_UP                   L3VPN_UP
#define BGP4_L3VPN_VRF_DOWN                 L3VPN_DOWN

#ifndef MPLS_L3VPN_WANTED
#define L3VPN_BGP4_RD                     1
#define L3VPN_BGP4_IMPORT_RT              2
#define L3VPN_BGP4_EXPORT_RT              3
#define L3VPN_BGP4_BOTH_RT                4

#define L3VPN_BGP4_ROUTE_PARAM_ADD        1
#define L3VPN_BGP4_ROUTE_PARAM_DEL        2

#define L3VPN_BGP4_ROUTE_ADD        1
#define L3VPN_BGP4_ROUTE_DEL        2

#define L3VPN_BGP4_LSP_STATUS_UP          1
#define L3VPN_BGP4_LSP_STATUS_DOWN        2
/*For RSVP-TE*/
#define L3VPN_BGP4_RSVPTE_LSP_STATUS_UP   1
#define L3VPN_BGP4_RSVPTE_LSP_STATUS_DOWN 2


#define L3VPN_BGP4_VRF_UP                 1
#define L3VPN_BGP4_VRF_DOWN               2
#define L3VPN_BGP4_VRF_CREATED        3
#define L3VPN_BGP4_VRF_DELETED        4

#define L3VPN_BGP4_LABEL_PUSH             1
#define L3VPN_BGP4_LABEL_POP              2

#define L3VPN_BGP4_ROUTE_ADD              1
#define L3VPN_BGP4_ROUTE_DEL              2

#define L3VPN_BGP4_ROUTE_PARAMS_REQ       1
#define L3VPN_BGP4_LSP_STATUS_REQ         2
#define L3VPN_BGP4_VRF_ADMIN_STATUS_REQ   4

#define L3VPN_BGP4_POLICY_PER_VRF         1
#define L3VPN_BGP4_POLICY_PER_ROUTE       2
#endif

/* VRF specific related flags to identify what are the operations are
 * being or to be done for a VRF
 */
/* Specifies the initialization for VRF is going on */
#define BGP4_VRF_INIT_INPROGRESS            0x00000001
/* Specifies the de-initialization for VRF is going on */
#define BGP4_VRF_DOWN_INPROGRESS            0x00000002
/* Specifies the export target policy change softout is going on */
#define BGP4_VRF_EXP_TGT_CHG_INPROGRESS     0x00000004
/* Specifies the export target policy change is reissued */
#define BGP4_VRF_EXP_TGT_CHG_REISSUE        0x00000008
/* Specifies that the creation is pending for this VRF. */
#define BGP4_VRF_CREATE_PENDING             0x00000010
/* Specifies that the initialization is pending for this VRF */
#define BGP4_VRF_INIT_PENDING               0x00000020
/* Specifies that export targets are removed and routes to be removed */
#define BGP4_VRF_NO_EXP_TARGETS             0x00000040

/* Route flags correspond to vrfs */
/*Flags indicates whehter the route is best in particular vrf or not*/
#define  BGP4_RT_BGP_VRF_BEST               0x00000001
/*Flags indicates that route is withdrawn route in particular vrf*/
#define  BGP4_RT_VRF_WITHDRAWN              0x00000002
/*Flags indicates that the route is replacement route in vrf*/
#define  BGP4_RT_VRF_REPLACEMENT            0x00000004
/*Flag indicates that the route hsould be added newly to ip vrf*/
#define  BGP4_RT_VRF_NEWUPDATE_TOIP         0x00000008
/*Flag indicates that the route is added in ip table*/
#define  BGP4_RT_IP_VRF_BEST                0x00000010
/*Flag indicates that route should be advertised as withdrawn*/
#define  BGP4_RT_VRF_ADVT_WITHDRAWN         0x00000020
/*Flag indicates that the route should not be advertised to external peer*/
#define  BGP4_RT_VRF_ADVT_NOTTO_EXTERNAL    0x00000040
/*Flag indicates that the route is present in vrf*/
#define  BGP4_RT_PRESENT_IN_VRF             0x00000080
/*Flag indicates that the route is being processed in vrf*/
#define BGP4_RT_PROCESS_VRF_RIB             0x00000100

/* Data Structure definitions */
/*Strucutre used to handle the Vrf change notification given by IP*/
typedef struct _tVrfNode {
    tTMO_SLL_NODE   NextVrfId; /* pointer to the next node */
    tBgp4VrfName    VrfName;
    UINT4           u4VrfId; /* VRF id */
    UINT1           u1VrfChngSts;
    UINT1           u1Oper; /* tells VRF or Exp-Tgt process */
    UINT1           au1Pad[2];
} tVrfNode;

/*Structure used to handle Lsp change notification given by MPLS*/
typedef struct _tLspNode {
    tTMO_SLL_NODE   NextLspId; /* pointer to the next node */
    UINT4           u4LspId; /* Lsp Identifier of Nexthop */
    UINT1           u1ChngSts;
    UINT1           au1Pad[3];
} tLspNode;

/* tRtInstallVrfInfo is used to identify information about a route in which
 * VRF(s) it has been installed. This will be maintained as list of infos
 * in a route profile structure
 */
typedef struct _tRtInstallVrfInfo {
    tTMO_SLL_NODE   TsnNextVrfInfo; /* pointer to the next VRF info */
    struct t_RouteProfile *p_RPNext; /*
                                      * Used for Linking the Profiles Together
                                      */
    UINT4           u4VrfId; /* Unique VRF identifier */
    UINT4           u4Flags; /* Flags associated with the route */
} tRtInstallVrfInfo;

/* tBgp4RrImportTargetInfo is used to store the route-targets information
 * that have been received by the speaker from the client peers.
 */
typedef struct _tBgp4RrImportTargetInfo {
    tTMO_SLL_NODE   NextTarget; /* pointer to the next import route target */
    UINT1           au1ImportTarget[8]; /* route-target */
    UINT4           u4TimeStamp; /* Time at which the route count associated
                                  * with this target has become zero and will
                                  * be incremented till the predefined delay
                                  * before this target is removed from the set
                                  */
    UINT4           u4RouteCount; /* Route count associated with this import
                                   * route target
                                   */
} tBgp4RrImportTargetInfo;

/*Strucutre containing all the Vpnv4 global parameters*/
typedef struct _tBgp4Vpn4GlobalData {
    tMemPoolId          VrfChgNodePoolId; /* For VRF configuration and
                                                * statistics */
    tMemPoolId          RtInstallVrfMemPoolId; /* For VRF(s) info in Route */
    tMemPoolId          RrImportTargetsSetMemPoolId; /* For RR Targets SET */
    tMemPoolId          LspChgNodePoolId;
    tTMO_SLL            RrImportTargetsSet; /* RR's Import Targets SET */
    tBGP4_DLL           Vpnv4FeasWdrawList; /*List to store the Best Vpnv4 
                                              Routes*/ 
    tTMO_HASH_TABLE    *pVrfGlobalInfo; /* Hash table that contains VRFs
                                         * information */
    /* Linear list holding vrfs for which export targets are changed */
    tTMO_SLL            BGP4VrfChngList;
    UINT4               u4RrRTDelInterval;  /* Interval at which route target
                                             * must be deleted from RR import
                                             * targets set */
    UINT4               u4VrfSpecInstanceCnt; /* Counter to keep track of 
                                               * VRF specific instances */
    UINT4               u4RtInstallVrfInstanceCnt; /* Counter to keep track of
                                                    * route install VRFs */
    UINT4               u4RrImportTargetsSetCnt; /* Counter to keep track of
                                                  * RR's targets Set */
} tBgp4Vpn4GlobalData;

/* Macro definitions */
#define BGP4_VPN4_GLOBAL_PARAMS     (gBgpNode.Bgp4Vpn4GlobalData)
#define BGP4_VPN4_VRF_NODE_CHG_MEMPOOL_ID   \
                    ((BGP4_VPN4_GLOBAL_PARAMS).VrfChgNodePoolId)
#define BGP4_VPN4_IFACE_SPEC_MEMPOOL_ID \
                    ((BGP4_VPN4_GLOBAL_PARAMS).IfInfoMemPoolId)
#define BGP4_VPN4_RT_INSTALL_VRF_MEMPOOL_ID \
                    ((BGP4_VPN4_GLOBAL_PARAMS).RtInstallVrfMemPoolId)
#define BGP4_VPN4_RR_TARGETS_SET_MEMPOOL_ID \
                    ((BGP4_VPN4_GLOBAL_PARAMS).RrImportTargetsSetMemPoolId)
#define BGP4_VPN4_LSP_NODE_CHG_MEMPOOL_ID \
                    ((BGP4_VPN4_GLOBAL_PARAMS).LspChgNodePoolId)
#define BGP4_VPN4_FEAS_WDRAW_LIST    \
                    (&((BGP4_VPN4_GLOBAL_PARAMS).Vpnv4FeasWdrawList))
#define BGP4_VPN4_RR_TARGETS_SET    \
                    (&((BGP4_VPN4_GLOBAL_PARAMS).RrImportTargetsSet))
#define BGP4_VRF_STS_CHNG_LIST \
                    (&((BGP4_VPN4_GLOBAL_PARAMS).BGP4VrfChngList))
#define BGP4_VPN4_VRF_GLOBAL_INFO   \
                    ((BGP4_VPN4_GLOBAL_PARAMS).pVrfGlobalInfo)
#define BGP4_VPN4_RR_TARGETS_INTERVAL   \
                    ((BGP4_VPN4_GLOBAL_PARAMS).u4RrRTDelInterval)
#define BGP4_VPN4_VRF_SPEC_CNT          \
                    ((BGP4_VPN4_GLOBAL_PARAMS).u4VrfSpecInstanceCnt)
#define BGP4_VPN4_RT_INSTALL_VRF_CNT    \
                    ((BGP4_VPN4_GLOBAL_PARAMS).u4RtInstallVrfInstanceCnt)
#define BGP4_VPN4_RR_TARGETS_SET_CNT    \
                    ((BGP4_VPN4_GLOBAL_PARAMS).u4RrImportTargetsSetCnt)
#define BGP4_VPN4_FEAS_WDRAW_LIST_FIRST_ROUTE \
                    ((BGP4_VPN4_GLOBAL_PARAMS).Vpnv4FeasWdrawList.pFirst)
#define BGP4_VPN4_FEAS_WDRAW_LIST_LAST_ROUTE \
                    ((BGP4_VPN4_GLOBAL_PARAMS).Vpnv4FeasWdrawList.pLast)
#define BGP4_VPN4_FEAS_WDRAW_LIST_COUNT \
                    ((BGP4_VPN4_GLOBAL_PARAMS).Vpnv4FeasWdrawList.u4Count)
#define BGP4_VPN4_CLI_VRF_MODE_VRF_NAME\
                    ((BGP4_VPN4_GLOBAL_PARAMS).CliVrfName)
#define BGP4_VPN4_CLI_VRF_MODE_VRF_NAME_VALUE\
                    ((BGP4_VPN4_GLOBAL_PARAMS).CliVrfName.au1VrfName)
#define BGP4_VPN4_CLI_VRF_MODE_VRF_NAME_LEN\
                    ((BGP4_VPN4_GLOBAL_PARAMS).CliVrfName.i4Length)

#define BGP4_VPN4_VRF_SPEC_IMPORT_TARGETS(u4Context) \
                    (&(gBgpCxtNode[u4Context]->ImportTargets))
#define BGP4_VPN4_VRF_SPEC_EXPORT_TARGETS(u4Context) \
                    (&(gBgpCxtNode[u4Context]->ExportTargets))
#define BGP4_VPN4_VRF_SPEC_VRFNAME(pVrfSpec)        \
                    (pVrfSpec->VrfName)
#define BGP4_VPN4_VRF_SPEC_VRFNAME_VALUE(pVrfSpec)        \
                    (pVrfSpec->VrfName.au1VrfName)
#define BGP4_VPN4_VRF_SPEC_NAME_SIZE(pVrfSpec)        \
                    (pVrfSpec->VrfName.i4Length)
#define BGP4_VPN4_VRF_SPEC_ROUTE_DISTING(u4Context)  \
                    (gBgpCxtNode[u4Context]->au1VrfRD)
#define BGP4_VPN4_VRF_SPEC_VRFID(pVrfSpec)  (pVrfSpec->u4VrfId)
#define BGP4_VPN4_VRF_SPEC_INIT_NET_ADDR(u4Context) (gBgpCxtNode[u4Context]->VrfInitAddress)
#define BGP4_VPN4_VRF_SPEC_INIT_PREFIX(u4Context) \
        (gBgpCxtNode[u4Context]->VrfInitAddress.NetAddr.au1Address)
#define BGP4_VPN4_VRF_SPEC_CURRENT_STATE(u4Context)   (gBgpCxtNode[u4Context]->u4VpnState)
#define BGP4_VPN4_VRF_SPEC_GET_FLAG(u4Context)   (gBgpCxtNode[u4Context]->u4Flags)
#define BGP4_VPN4_VRF_SPEC_SET_FLAG(u4Context,u4_flag)  \
                (gBgpCxtNode[u4Context]->u4Flags |= (u4_flag))
#define BGP4_VPN4_VRF_SPEC_RESET_FLAG(u4Context,u4_flag) \
                (gBgpCxtNode[u4Context]->u4Flags &= ~(u4_flag))
#define BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID(pRtVrf)  (pRtVrf->u4VrfId)
#define BGP4_VPN4_RT_INSTALL_VRFINFO_FLAGS(pRtVrf)  (pRtVrf->u4Flags)
#define BGP4_VPN4_RT_INSTALL_VRFINFO_RTNEXT(pRtVrf) (pRtVrf->p_RPNext)
#define BGP4_VPN4_RT_INSTALL_VRFINFO_SET_RTNEXT(pRt1, pRt2, u4VrfId) \
        {\
            tRtInstallVrfInfo   *pRtVrf = NULL;\
                TMO_SLL_Scan ((BGP4_VPN4_RT_INSTALL_VRF_LIST(pRt1)), pRtVrf,\
                        tRtInstallVrfInfo *)\
                {\
                    if (BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrf)\
                            == u4VrfId)\
                    {\
                        /* installed vrf info is found */ \
                        BGP4_VPN4_RT_INSTALL_VRFINFO_RTNEXT(pRtVrf) = pRt2;\
                        break;\
                    }\
                }\
                if (pRtVrf == NULL)  { BGP4_RT_NEXT(pRt1) = pRt2 ;} \
        }
#define BGP4_VPN4_RT_INSTALL_VRFINFO_GET_RTNEXT(pRt1, u4VrfId, pRtNext) \
        {\
            tRtInstallVrfInfo   *pRtVrf = NULL;\
                TMO_SLL_Scan ((BGP4_VPN4_RT_INSTALL_VRF_LIST(pRt1)), pRtVrf,\
                        tRtInstallVrfInfo *)\
                {\
                    if (BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrf)\
                            == u4VrfId)\
                    {\
                        /* installed vrf info is found */ \
                        pRtNext = BGP4_VPN4_RT_INSTALL_VRFINFO_RTNEXT(pRtVrf);\
                        break;\
                    }\
                }\
                if (pRtVrf == NULL)  { pRtNext = BGP4_RT_NEXT(pRt1); }\
        }
#define BGP4_VPN4_GET_PROCESS_VRFID(pRt, u4VrfId)    \
        {\
            tRtInstallVrfInfo   *pRtVrf = NULL;\
                    TMO_SLL_Scan ((BGP4_VPN4_RT_INSTALL_VRF_LIST(pRt)), pRtVrf,\
                            tRtInstallVrfInfo *)\
                    {\
                        if (BGP4_VPN4_RT_VRF_GET_FLAG(pRtVrf) & \
                                BGP4_RT_PROCESS_VRF_RIB)\
                        {\
                            /* installed vrf info is found */ \
                                u4VrfId = BGP4_VPN4_RT_INSTALL_VRFINFO_VRFID (pRtVrf);\
                                if (gBgpCxtNode[u4VrfId] == NULL)\
                                {\
                                    continue;\
                                }\
                                break;\
                        }\
                        }\
                    if (pRtVrf == NULL)  { u4VrfId = BGP4_RT_CXT_ID (pRt); }\
        }
#define BGP4_VPN4_RT_VRF_GET_FLAG(pRtVrf)   (pRtVrf->u4Flags)
#define BGP4_VPN4_RT_VRF_SET_FLAG(pRtVrf,u4_flag)  \
                (pRtVrf->u4Flags |= (u4_flag))
#define BGP4_VPN4_RT_VRF_RESET_FLAG(pRtVrf,u4_flag) \
                (pRtVrf->u4Flags &= ~(u4_flag))

#define BGP4_VPN4_RR_IMPORT_TARGET_NEXT(pRrImportTargetInfo) \
                (pRrImportTargetInfo->NextTarget)
#define BGP4_VPN4_RR_IMPORT_TARGET(pRrImportTargetInfo) \
                (pRrImportTargetInfo->au1ImportTarget)
#define BGP4_VPN4_RR_IMPORT_TARGET_TIMESTAMP(pRrImportTargetInfo)    \
                (pRrImportTargetInfo->u4TimeStamp)
#define BGP4_VPN4_RR_IMPORT_TARGET_RTCNT(pRrImportTargetInfo)   \
                (pRrImportTargetInfo->u4RouteCount)
#define BGP4_VPN4_VRF_NAME_CMP(pVrfName1, pVrfName2)  \
            (((pVrfName1->i4Length == pVrfName2->i4Length) && \
             (MEMCMP (pVrfName1->au1VrfName, pVrfName2->au1VrfName,\
                pVrfName1->i4Length) == 0)) ? BGP4_TRUE : BGP4_FALSE)
#define BGP4_VPN4_IS_VALID_RD(pu1RcvdRD, u1Sts)     \
        {\
            UINT2       u2Type;\
            PTR_FETCH2 (u2Type, pu1RcvdRD);\
            u1Sts = ((u2Type == BGP4_VPN4_AS_EXT_COM_TYPE) ||\
                     (u2Type == BGP4_VPN4_IPADDR_EXT_COM_TYPE) ||\
                     (u2Type == BGP4_VPN4_4BYTE_AS_EXT_COM_TYPE))\
                    ? BGP4_TRUE : BGP4_FALSE;\
            if ((u1Sts == BGP4_TRUE) && \
                (u2Type == BGP4_VPN4_IPADDR_EXT_COM_TYPE))\
            {\
                UINT4   u4IpAddr;\
                PTR_FETCH4 (u4IpAddr, (pu1RcvdRD+PATH_ATTRIBUTE_TYPE_SIZE));\
                u1Sts = ((BGP4_IS_VALID_ADDRESS (u4IpAddr) == 0) ||\
                         (BGP4_IS_BROADCAST_ADDRESS (u4IpAddr) == 1))\
                        ? BGP4_FALSE : BGP4_TRUE;\
            }\
        }
#define BGP4_IS_SOO_EXT_COMMUNITY(u2EcommType)\
                  (((u2EcommType == 0x0003) ||\
                    (u2EcommType == 0x0103) ||\
                    (u2EcommType == 0x0203)) ? BGP4_TRUE : BGP4_FALSE)

#define BGP4_IPV4_LBLD_RIB_TREE                \
                        (gBgpNode.gapBgp4Rib[BGP4_IPV4_LBLD_INDEX])
#define BGP4_VPN4_RIB_TREE(u4CxtId)   \
    (gBgpCxtNode[u4CxtId]->gapBgp4Rib[BGP4_VPN4_UNI_INDEX])

#define BGP4_VPN4_CE_PEER_SOO_EXT_COM(pPeerentry) \
                   (pPeerentry->peerConfig.pSooExtCom)
#define BGP4_VPN4_CE_PEER_SOO_EXT_COM_VALUE(pPeerentry) \
                   (pPeerentry->peerConfig.pSooExtCom->au1ExtComm)
#define BGP4_VPN4_CE_PEER_SOO_EXT_COM_STATUS(pPeerentry) \
                   (pPeerentry->peerConfig.pSooExtCom->u1RowStatus)
#define BGP4_PEER_VPN4_AFISAFI_INSTANCE(pPeerentry) \
                    (pPeerentry->apAsafiInstance[BGP4_VPN4_UNI_INDEX])
/* Peer related vpn4 lists macros */
#define BGP4_PEER_VPN4_OUTPUT_ROUTES(pPeerentry) \
        (BGP4_PEER_VPN4_AFISAFI_INSTANCE(pPeerentry)->pOutputList)
#define BGP4_PEER_VPN4_NEW_ROUTES(pPeerentry) \
        (BGP4_PEER_VPN4_AFISAFI_INSTANCE(pPeerentry)->pNewList)
#define BGP4_PEER_VPN4_NEW_LOCAL_ROUTES(pPeerentry) \
        (BGP4_PEER_VPN4_AFISAFI_INSTANCE(pPeerentry)->pNewLocalList)
#define BGP4_PEER_VPN4_ADVT_WITH_ROUTES(pPeerentry) \
        (BGP4_PEER_VPN4_AFISAFI_INSTANCE(pPeerentry)->pAdvtWithList)
#define BGP4_PEER_VPN4_ADVT_FEAS_ROUTES(pPeerentry) \
        (BGP4_PEER_VPN4_AFISAFI_INSTANCE(pPeerentry)->pAdvtFeasList)
#define BGP4_PEER_VPN4_DEINIT_RT_LIST(pPeerentry) \
        (&(BGP4_PEER_VPN4_AFISAFI_INSTANCE(pPeerentry)->TSDeInitRtList))

/* Macros for counters */
#define BGP4_PEER_VPN4_PREFIX_RCVD_CNT(pPeerentry) \
            ((pPeerentry->apAsafiInstance[BGP4_VPN4_UNI_INDEX])->PrefixCounters).u4PrefixesRcvd
#define BGP4_PEER_VPN4_PREFIX_SENT_CNT(pPeerentry) \
            ((pPeerentry->apAsafiInstance[BGP4_VPN4_UNI_INDEX])->PrefixCounters).u4PrefixesSent
#define BGP4_PEER_VPN4_WITHDRAWS_RCVD_CNT(pPeerentry) \
            ((pPeerentry->apAsafiInstance[BGP4_VPN4_UNI_INDEX])->PrefixCounters).u4WithdrawsRcvd
#define BGP4_PEER_VPN4_WITHDRAWS_SENT_CNT(pPeerentry) \
            ((pPeerentry->apAsafiInstance[BGP4_VPN4_UNI_INDEX])->PrefixCounters).u4WithdrawsSent
#define BGP4_PEER_VPN4_IN_PREFIXES_CNT(pPeerentry) \
            ((pPeerentry->apAsafiInstance[BGP4_VPN4_UNI_INDEX])->PrefixCounters).u4InPrefixes
#define BGP4_PEER_VPN4_IN_PREFIXES_ACPTD_CNT(pPeerentry) \
            ((pPeerentry->apAsafiInstance[BGP4_VPN4_UNI_INDEX])->PrefixCounters).u4InPrefixesAcptd
#define BGP4_PEER_VPN4_IN_PREFIXES_RJCTD_CNT(pPeerentry) \
            ((pPeerentry->apAsafiInstance[BGP4_VPN4_UNI_INDEX])->PrefixCounters).u4InPrefixesRjctd
#define BGP4_PEER_VPN4_OUT_PREFIXES_CNT(pPeerentry) \
            ((pPeerentry->apAsafiInstance[BGP4_VPN4_UNI_INDEX])->PrefixCounters).u4OutPrefixes
#define BGP4_PEER_VPN4_INVALID_MPE_UPDATE_CNT(pPeerentry) \
            ((pPeerentry->apAsafiInstance[BGP4_VPN4_UNI_INDEX])->PrefixCounters).u4InvalidMpeAttribCntr
#define BGP4_VPN4_RCVD_RT_ECOMM_FLAG(pRouteProfile)\
                       (pRouteProfile->pRtInfo->pExtCommunity->u1RTEcommFlag)
#define BGP4_VPN4_RT_INSTALL_VRF_LIST(pRouteProfile)\
                       (&(pRouteProfile->InstalledVrfList))
#define BGP4_VPN4_EXT_COMM_VALUE(pRTECom) \
                     (&(pRTECom->au1ExtComm[PATH_ATTRIBUTE_TYPE_SIZE]))
#define BGP4_VPN4_EXT_COM_TYPE_HIGH_OCTET(pRTEcom) \
                       (pRTEcom->au1ExtComm)
#define BGP4_VPN4_EXT_COM_TYPE_LOW_OCTET(pRTEcom) \
                       (&(pRTEcom->au1ExtComm[1]))

#define BGP4_VPN4_EXT_COMM_ROW_STATUS(pRTECom) \
                     (pRTECom->u1RowStatus)
#define BGP4_VPN4_IS_VALID_RT_TYPE(u2RTType) \
          (((((u2RTType & 0xff00) == 0x01) ||\
            ((u2RTType & 0xff00) == 0x03)) &&\
            ((u2RTType & 0xff)== 0x02))? BGP4_TRUE:BGP4_FALSE)
#define BGP4_COPY_LABEL_TOBUF(pu1Buf, u4Label)\
        ((*pu1Buf = (UINT1)((u4Label & 0x00ff0000) >> 16)),\
        (*(pu1Buf+1) = (UINT1)((u4Label & 0x0000ff00) >> 8)),\
        (*(pu1Buf+2) = (UINT1)(u4Label & 0x000000ff)))

#ifdef L3VPN_TEST_WANTED
INT4    Bgp4Vpnv4UpdRTToVRF (UINT4, UINT1        *,
                             UINT1,
                             UINT1);
INT4    Bgp4Vpnv4DelRouteFromIPVRF (UINT4        ,
                                    tRouteProfile  *);
INT4    Bgp4Vpnv4ModifyRouteInIPVRF (UINT4,
                                     tRouteProfile  *);
INT4    Bgp4Vpnv4AddRouteToIPVRF (UINT4,
                                  tRouteProfile  *);
#endif
#endif /* BGVPNDFS_H */
