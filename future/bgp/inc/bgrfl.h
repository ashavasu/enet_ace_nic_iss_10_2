/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved    
 *
 * $Id: bgrfl.h,v 1.10 2013/02/07 12:12:46 siva Exp $
 *
 * Description:This file includes declarations related with
 *             Route Reflector feature.
 *
 *******************************************************************/
#ifndef _BGRFL_H
#define _BGRFL_H

/* ********** All type definitions of Route Reflcetor module *********** */ 

/* Constant used in the RR module */

#define RFL_SUCCESS             BGP4_SUCCESS
#define RFL_FAILURE             BGP4_FAILURE
#define RFL_TRUE                BGP4_TRUE
#define RFL_FALSE               BGP4_FALSE
#define RFL_ENABLED             1
#define RFL_DISABLED            2
#define RFL_MEM_FAILURE         MEM_FAILURE

/* Constants for the cluster and originator path attributes */

#define NO_CLUSTER_ID           0
#define NO_ORIG_ID              0
#define ORIGID_TYPE_CODE        BGP4_ATTR_ORIG_ID
#define CLUSTER_LIST_TYPE_CODE  BGP4_ATTR_CLUS_LIST
#define ORIGINATOR_ID_LENGTH    4
#define ORIGID_PATTR_LENGTH     7
#define CLUSTER_ID_LENGTH       4
#define ORIGID_PATTR_FLAG       0x80
#define CLRLIST_PATTR_FLAG      0x80
#define INVALID_ORIG_ID         0xFFFFFFFF
#define MAX_CLUSTERS            63
#define CLIENT_SUPPORT          2
#define NO_CLIENT_SUPPORT       3
#define NON_CLIENT              1
#define CLIENT                  2
#define MESHED_CLIENT           3

#define RFL_ATTR_FIELD_SIZE     1

#define RFL_BYTES_FOR_PREFIX_LEN            1

#define RFL_NONE                        1
#define RFL_PEER_TYPE                   1
#define RFL_PEER_REFLECT_ROWSTATUS      2
#define RFL_ROWSTATUS_ACTIVE            BGP4_ROWSTATUS_ACTIVE
#define RFL_ROWSTATUS_NOT_IN_SERVICE    BGP4_ROWSTATUS_NOT_IN_SERVICE
#define RFL_ROWSTATUS_NOT_READY         BGP4_ROWSTATUS_NOT_READY
#define RFL_ROWSTATUS_CREATE_AND_GO     BGP4_ROWSTATUS_CREATE_AND_GO
#define RFL_ROWSTATUS_CREATE_AND_WAIT   BGP4_ROWSTATUS_CREATE_AND_WAIT
#define RFL_ROWSTATUS_DESTROY           BGP4_ROWSTATUS_DESTROY
#define DPND_OBJ_UNSET                  BGP4_DPND_OBJ_UNSET
#define DPND_OBJ_SET                    BGP4_DPND_OBJ_SET

/* Unknown Path Attribute - Query type */
#define BGP4_UNKNOWN_ATTR_QUERY         0x01
#define BGP4_UNKNOWN_ATTR_FORM          0x02

#define RFL_ALLOCATE_BUF                Bgp4MemAllocateMsg
#define RFL_FREE_BUF                    Bgp4MemReleaseMsg

#define RFL_HASH_DYN_Scan_Bucket BGP4_HASH_DYN_Scan_Bucket

#define RFL_SLL_DYN_Scan        BGP_SLL_DYN_Scan

/* Definition for Route Reflector Error Information */

typedef struct t_Bgp4RrErrorInfo {
    UINT4           u4OriginatorIdError; /*Counter to log the error 
                                          * for originator Id 
                                         */
    UINT4           u4ClusterListError; /* Counter to log the error 
                                         * for cluster List error 
                                         */
} tBgp4RrErrorInfo;


/* Definition for Route Reflector Information */
typedef struct t_Bgp4RrParams {
    tBgp4RrErrorInfo RrErrorStats;     /* Holds the error statistics */
    UINT4            u4RflClusterId;   /* Cluster Identifier 
                                        * of Route Reflector */
    UINT1            u1RflSupport; /* Route Reflector mode of operation. 
                                    * It can take the value "Client support"
                                    * or "no-client support" */ 
    UINT1           au1Padding [3]; /* Added for 4 bytes alignment. */ 
}tBgp4RrParams;


/* ************** Macros definitions ***************************************/ 
#define  RFL_GLOBAL_PARAMS(u4CxtId)  (gBgpCxtNode[u4CxtId]->Bgp4RrParams)
#define  RFL_MAX_PEERS   Bgp4GetMaxPeers()
#define  RFL_CLUSTER_ID(u4CxtId)   (RFL_GLOBAL_PARAMS(u4CxtId).u4RflClusterId)
#define  RFL_CLIENT_SUPPORT(u4CxtId)   (RFL_GLOBAL_PARAMS(u4CxtId).u1RflSupport)
#define  RFL_CLUSTER_LIST_ERROR(u4CxtId)  (RFL_GLOBAL_PARAMS(u4CxtId).RrErrorStats.u4ClusterListError)
#define  RFL_ORIGINATOR_ID_ERROR(u4CxtId) (RFL_GLOBAL_PARAMS(u4CxtId).RrErrorStats.u4OriginatorIdError)
#define  RFL_MIN_EXTEN_LENGTH    255
#define  RFL_GET_ATTR_SIZE(u2AttrbSize)  \
            ((u2AttrbSize > RFL_MIN_EXTEN_LENGTH) ? 2: 1)
#define  RFL_SET_EXTEN_FLAGS(u2NoOfClusters) \
  (((u2NoOfClusters* CLUSTER_ID_LENGTH) > RFL_MIN_EXTEN_LENGTH) ? 0x10 : 0x00)
#define  RFL_LINK_NEXTHOP_INFO_IN_IGP_METRIC_STRUCT(pIgpMetricEntry)\
                (pIgpMetricEntry->LinkNexthop)
#define RFL_ROUTE_COMM_PATH(RouteCommPath) ((tRouteCommPath *)RouteCommPath)
#define RFL_ROUTE_COMM_COUNT(RouteCommPath) (((tRouteCommPath *)RouteCommPath)->u2CommCount)
#define RFL_ROUTE_COMM_VALUE(RouteCommPath,u2Index) (((tRouteCommPath *)RouteCommPath)->au4Comm[u2Index])
#define RFL_ROUTE_COMM_PARTIAL(RouteCommPath) (((tRouteCommPath *)RouteCommPath)->i1CommPartial)
#define RFL_ROUTE_EXT_COMM_PATH(RouteExtCommPath) ((tRouteExtCommPath *)RouteExtCommPath)
#define RFL_ROUTE_EXT_COMM_VALUE(RouteExtCommPath,u2Index) \
            (((tRouteExtCommPath *)RouteExtCommPath)->au1ExtComm[u2Index])
#define RFL_ROUTE_EXT_COMM_COUNT(RouteExtCommPath) (((tRouteExtCommPath *)RouteExtCommPath)->u2ExtCommCount)
#define RFL_ROUTE_EXT_COMM_PARTIAL(RouteExtCommPath) (((tRouteExtCommPath *)RouteExtCommPath)->i1ExtCommPartial)
#define RFL_BGP_INFO_CMP(pPrevBgpInfo,pBgpInfo ) (Bgp4AttrIsBgpinfosIdentical(pPrevBgpInfo, pBgpInfo))

/* Prototypes  for RR module - to be moved to BGP4 prototype file */
INT4    RflCheckForAdvertisement (
                tBgp4PeerEntry  *pPeerEntry, 
                tRouteProfile   *pRouteProfileToBeAdvt);

INT4    RflShutDown (UINT4);
VOID    RflInitHandler (UINT4);
INT4    RflOrigIdValidation (tRouteProfile *pRouteForValidation); 
INT4    RflClusterListValidation (UINT4 u4CxtId, UINT1 *pu1ClusterInfo, UINT2 u2ClusIdCnt);
INT4    RflGetOriginatorId(const tRouteProfile *pRouteForValidation,
                           UINT4 *pu4RcvdOrigId);  
INT4    RflGetClusterList (const tRouteProfile * pRouteForValidation,
                           UINT1 **pu1ClusterInfo, UINT2 *pu2ClusIdCnt);
VOID    Bgp4GetPeerListHead(tTMO_SLL **ppListofPeerEntries);

INT4    RflFillRflPathAttrInfo(
                tRouteProfile * pFeasibleRouteToBeFilled,
                tBgp4Info * pAdvtBgpInfo);

INT4    RflFillOrigPathAttrInfo(
                tRouteProfile * pFeasibleRouteToBeFilled,
                tBgp4Info * pAdvtBgpInfo);

INT4    RflFillClusterPathAttrInfo(
                tRouteProfile * pFeasibleRouteToBeFilled,
                tBgp4Info * pAdvtBgpInfo);

/* Prototypes related to low level routines */
VOID Bgp4GetBgpId (UINT4 u4CxtId, UINT4 *u4RtrOrigId);

INT1 RflTestv2Fsbgp4RflbgpClusterId (UINT4 *pu4ErrorCode,
                                     UINT4 u4bgpClusterId);
INT1 RflSetFsbgp4RflbgpClusterId (UINT4  u4SetValFsbgp4RflbgpClusterId);

#endif /* End of _BGRFL_H*/


