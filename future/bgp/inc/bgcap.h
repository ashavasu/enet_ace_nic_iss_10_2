
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved    
 *
 * $Id: bgcap.h,v 1.20 2016/12/27 12:35:52 siva Exp $
 *
 *******************************************************************/
#ifndef _BGP4CAPS_H
#define _BGP4CAPS_H

/************************** CONSTANTS ****************************************/
#define  CAPS_SUCCESS                        BGP4_SUCCESS
#define  CAPS_FAILURE                        BGP4_FAILURE
#define  CAPS_TRUE                           BGP4_TRUE
#define  CAPS_FALSE                          BGP4_FALSE
#define  CAPS_MEM_SUCCESS                    MEM_SUCCESS
#define  CAPS_MEM_FAILURE                    MEM_FAILURE
#define  CAPS_MEM_EQUAL                      0
#define  CAPS_ENABLED                        1
#define  CAPS_DISABLED                       2
#define  UNSUPPORTED_CAPABILITY              7
#define  UNSUPPORTED_OPTIONAL_PARAMETER      4
#define  CAPABILITIES_OPTIONAL_PARAMETER     2
#define  OPTPARAM_LENGTH_OFFSET              9
#define  OPTIONAL_PARAMETER_OFFSET           10
#define  CAP_CODE_SIZE                       1
#define  CAP_LENGTH_SIZE                     1
#define  CAP_CODE_LENGTH_SIZE                2
#define  OPTPARAM_TYPE_LENGTH_SIZE           2
#define  OPTPARAM_TYPE_SIZE                  1
#define  OPTPARAM_LENGTH_SIZE                1 
#define  OPTPARAM_TOTAL_LENGTH_SIZE          1
#define  MIN_CAP_OPT_LEN                     2
#define  MAX_CAP_OPTPARAM_LEN                255
#define  CAP_MP_CAP_LENGTH                    4
#define  MIN_CAP_GR_LENGTH                    6

#ifdef BGP4_IPV6_WANTED
    #ifdef VPLSADS_WANTED
       #ifdef VPLS_GR_WANTED
           #define  CAP_GR_CAP_LENGTH        14
       #else
           #define  CAP_GR_CAP_LENGTH        10
       #endif
    #else
        #define  CAP_GR_CAP_LENGTH            10
    #endif
#elif VPLSADS_WANTED
   #define  CAP_GR_CAP_LENGTH                10
#else
    #define  CAP_GR_CAP_LENGTH                6 
#endif

#define  CAP_4_BYTE_ASN_LENGTH                4
#define  CAP_CODE_MP_EXTN                     1
#define  CAP_CODE_ROUTE_REFRESH               2
#define  CAP_CODE_MULTIPLE_ROUTES             4
#define  CAP_CODE_GRACEFUL_RESTART            64
#define  CAP_CODE_4_OCTET_ASNO                65
#define  CAP_CODE_DYNAMIC_CAP                 66
#define  CAP_CODE_VENDOR_SPECIFIC_MIN         128
#define  CAP_CODE_VENDOR_SPECIFIC_MAX         255
#define  CAPS_HASH_TABLE_SIZE                 32
#define  CAP_CODE_MP_EXTN_AFI_IPV4                     1
#define  CAP_CODE_MP_EXTN_AFI_IPV6                     2
#define  CAP_CODE_MP_EXTN_AFI_L2VPN                    25
#define  CAP_CODE_MP_EXTN_SAFI_UNICAST                 1
#define  CAP_CODE_MP_EXTN_SAFI_VPNV4_UNICAST           128
#define  CAP_CODE_MP_EXTN_SAFI_LABEL                   4
#define  CAP_CODE_MP_EXTN_SAFI_VPLS                    65
#define  CAP_CODE_MP_EXTN_SAFI_EVPN                    70


#ifdef VPLSADS_WANTED
   #ifdef VPLS_GR_WANTED
       /*changes as per l2vpn GR cap earlier it was 12*/ 
       #define  MAX_CAP_LENGTH               14 
   #else
       #define  MAX_CAP_LENGTH               12
   #endif
#else 
   #define  MAX_CAP_LENGTH                   12
#endif 

#define  CAPS_SUP                              1
#define  CAPS_RCVD                             2
#define  CAP_ENTRY_DEF_MASK                   0x01
#define  CAP_ENTRY_LENGTH_MASK                0x02
#define  CAP_ENTRY_DATA_MASK                  0x04
#define  CAP_ENTRY_ZERO_LENGTH_MASK          0x08
#define  CAP_VALID_ENTRY_LEN_DATA_MASK        0x07 
#define  CAP_VALID_ENTRY_LEN_MASK             0x03 
#define  CAP_MAX_CAPS_PER_PEER                255
#define  CAP_MAX_INSTANCES_PER_CAP            255
#define  CAP_MIN_CAP_MAX_DATA_SIZE            4 
#define  CAP_MAX_CAP_MAX_DATA_SIZE            252 

#define  CAP_NEG_MP_EXTN_MASK                0x00000001
#define  CAP_NEG_ROUTE_REFRESH_MASK          0x00000002
#define  CAP_NEG_4_OCTET_ASNO_MASK           0x00000041
#ifdef L3VPN
#define  CAP_MP_LABELLED_IPV4                0x00010004
#define  CAP_MP_LABELLED_IPV6                0x00020004
#define  CAP_NEG_LBL_IPV4_MASK               0x00000004
#endif
#define  CAP_MP_IPV4_UNICAST                 0x00010001
#define  CAP_NEG_IPV4_UNI_MASK               0x00000001

#define  CAP_NEG_ORF_IPV4_UNICAST_RCV        0x00000004
#define  CAP_NEG_ORF_IPV4_UNICAST_SEND       0x00000008
#define  CAP_NEG_ORF_IPV6_UNICAST_RCV        0x00000010
#define  CAP_NEG_ORF_IPV6_UNICAST_SEND       0x00000020

#define  CAP_SINGLE_CAP_INSTANCE             1
#define  CAP_SUPPORTED_CAP_LEN               4
#define  CAP_MAX_MANDATORY                   1
#define  CAP_MAX_OPT_CAP_AFI_VALUE               3
#define  CAP_MAX_OPT_CAP_SAFI_VALUE              3

#ifdef VPLSADS_WANTED
#define  CAP_MP_L2VPN_VPLS                   0x00190041 
#define  CAP_NEG_L2VPN_VPLS_MASK             0x00000010 
#endif

#define BGP4_CAP_BITMASK_IPV4               4
#define BGP4_CAP_BITMASK_IPV6               8
#define BGP4_CAP_BITMASK_L3VPN              16
#define BGP4_CAP_BITMASK_VPLS               32

/*Flag used to find Capabilities Optional 
parameter has to be filled in the OPEN message or not. This flag 
is set as CAPS_TRUE when the router receives Unsupported 
Optional Paramter NOTIFICATION message with Error Data 
Capabilities Optional paramter from the peer*/

#define BGP4_CAPS_UNSUP_OPT_REPEERING      0x00000001

typedef struct t_capability
{
        UINT1   u1CapCode;      /* Capability Code */
        UINT1   u1CapLength;    /* Capability Length */
        UINT1   au1Padding[2];  /* Added for 4 bytes Alignment */
#ifndef VPLS_GR_WANTED
        UINT1   au1CapValue[12]; /* Capability Value */
#endif
#ifdef VPLS_GR_WANTED
        UINT1   au1CapValue[16]; /* Capability Value */
#endif
}tCapability;
   /* Note: au1CapValue is an array whose size is variable which depends 
      on configuration. For this reason au1Padding is not placed as the 
      last parameter of this  structure. The array size of au1CapValue 
      should be multiples of 4 bytes*/ 


typedef struct t_supCapsInfo
{
        tTMO_SLL_NODE   NextCapability;   /* Next Capability that is received 
                                           from peer */
        UINT1           u1RowStatus; /*Valid for only supported capabilities*/
        UINT1           u1CapsFlag;/*flag to know if the router has 
                                             announced the capability for 
                                             supported caps and negotiated or 
                                             not for received caps*/
        UINT1           u1ConfiguredFlag; /*flag to know if the capability has
                                            been configured specifically or 
                                            enabled by default*/
        UINT1           au1Padding[1];  /* Added for 4 bytes Alignment */        
        tCapability     SupCapability; 
                                     /*Capability that is received from peer or
                                      * supported for peer*/
}tSupCapsInfo;
 /* Note: The size of au1CapValue in RcvdCapability is variable which depends 
    on the configuration. So RcvdCapability should be always at the end of 
    the structure tPeerCapsRcvd */

typedef struct t_OrfCapValue
{
        UINT2   u2Afi;
        UINT1   u1Res;
        UINT1   u1Safi;
        UINT1   u1OrfCount;
        UINT1   u1OrfType;
        UINT1   u1OrfMode;
        UINT1   au1Pad[1];
}tOrfCapsValue;

typedef struct t_capsErrInfo
{
        UINT4        u4CapsValidationErr; /*Counter to log the error when the 
                                            validation of received Capabilities
                                            fails*/
        UINT4        u4CapsPrcsErr;       /*Counter to log the error when the 
                                            Capabilities processing fails */
        UINT4        u4NotfMsgSendErr;    /*Counter to log the error when there                                             is failure in sending NOTIFICATION 
                                            message with Error Sub-Code Unsuppor                                            ted Capability, to the peer*/ 
}tCapsErrInfo;

typedef struct t_bgp4CapsParams
{
       tCapsErrInfo      CapsErrStats;     /*Holds the error information of 
                                              Capabilities Advertisement*/ 
       UINT1             u1CapsAdvtSup;  /* Flag to know whether the 
                                            Capabilities Advertisement support 
                                            is enabled or not*/ 
       UINT1             au1Padding[3];    /* Added for 4 bytes Alignment */
}tBgp4CapsParams;
/*Note: CapsSizingParams should be placed in tBgp4SystemSize */

/**************************** MACROS ***************************************/

#define CAPS_MAX_PEERS       Bgp4GetMaxPeers()
#define CAPS_GLOBAL_PARAMS(u4CxtId)   gBgpCxtNode[u4CxtId]->Bgp4CapsParams
#define PEER_CAPS_MEMPOOLID    (gBgpNode.PeerCapsPoolId)
#define CAPS_VALIDATION_ERR(u4CxtId)        ((CAPS_GLOBAL_PARAMS(u4CxtId)).CapsErrStats.u4CapsValidationErr)
#define CAPS_PROCESS_ERR(u4CxtId)          ((CAPS_GLOBAL_PARAMS(u4CxtId)).CapsErrStats.u4CapsPrcsErr)
#define CAPS_NOTFMSG_SEND_ERR(u4CxtId)    ((CAPS_GLOBAL_PARAMS(u4CxtId)).CapsErrStats.u4NotfMsgSendErr)
#define CAPS_ADVT_SUP(u4CxtId)            ((CAPS_GLOBAL_PARAMS(u4CxtId)).u1CapsAdvtSup)
#define PEER_CAPS_RCVD_ENTRY_CREATE(pPeerCapsRcvd) \
 pPeerCapsRcvd = (tSupCapsInfo *) MemAllocMemBlk (PEER_CAPS_MEMPOOLID);
#define SPKR_SUP_CAPS_ENTRY_CREATE(pSpkrSupCaps)\
  pSpkrSupCaps = (tSupCapsInfo *)MemAllocMemBlk (PEER_CAPS_MEMPOOLID);
#define PEER_CAPS_RCVD_ENTRY_FREE(pPeerCapsRcvd) MemReleaseMemBlock (PEER_CAPS_MEMPOOLID, (UINT1 *)pPeerCapsRcvd)
#define SPKR_SUP_CAPS_ENTRY_FREE(pSpkrSupCaps) MemReleaseMemBlock (PEER_CAPS_MEMPOOLID, (UINT1 *)pSpkrSupCaps)

#define BGP4_PEER_SUP_CAPS_LIST(pPeer) \
                    (&(pPeer->PeerCapsInfo.SpkrSupCapsLst))
#define BGP4_PEER_RCVD_CAPS_LIST(pPeer) \
                    (&(pPeer->PeerCapsInfo.PeerCapsRcvdLst))
#define BGP4_GET_CAPS_PEER_FLAGS(pPeer) \
                    (pPeer->PeerCapsInfo.u4PeerCapsFlags)
#define BGP4_SET_CAPS_PEER_FLAGS(pPeer, u4Flag) \
        (pPeer->PeerCapsInfo.u4PeerCapsFlags |= u4Flag)
#define BGP4_RESET_CAPS_PEER_FLAGS(pPeer, u4Flag) \
        (pPeer->PeerCapsInfo.u4PeerCapsFlags &= ~(u4Flag))
#define BGP4_PEER_SUP_CAPS_ANCD_STATUS(pPeerSupCap) \
                pPeerSupCap->u1CapsFlag
#define BGP4_PEER_SUP_CAPS_CONFIGURED_STATUS(pPeerSupCap)\
                pPeerSupCap->u1ConfiguredFlag
#define BGP4_PEER_RCVD_CAPS_NEGOTIATED_STATUS(pPeerCapsRcvd) \
                pPeerCapsRcvd->u1CapsFlag

#define IS_VALID_CAPABILITY_CODE(u1CapCode)         \
      ((u1CapCode <= CAP_CODE_VENDOR_SPECIFIC_MAX) ? 1:0)
     
#define IS_VENDOR_SPECIFIC_CAPABILITY(u1CapCode)         \
      (((u1CapCode >= CAP_CODE_VENDOR_SPECIFIC_MIN) && \
      (u1CapCode <= CAP_CODE_VENDOR_SPECIFIC_MAX)) ? 1:0)

     
#define CAPS_RELEASE_SPKR_SUP_LIST(pSpkrSupLst, pSpkrSupCaps, pTmpSpkrSupCaps,typecast)\
     CAPS_SLL_DYN_Scan (pSpkrSupLst, pSpkrSupCaps, pTmpSpkrSupCaps, typecast) \
    { \
       SPKR_SUP_CAPS_ENTRY_FREE(pSpkrSupCaps); \
    }
#define CAPS_FIND_PEER_CAP_INSTANCE_NO(pCapsList, pPeerCapsRcvd, u1NumInstance, pTmpPeerCapsRcvd, typecast)      \
 u1NumInstance = 0; \
TMO_SLL_Scan (pCapsList, pTmpPeerCapsRcvd, typecast) \
{   \
   if (pTmpPeerCapsRcvd == pPeerCapsRcvd) \
   {\
      break;\
   }\
   if (pTmpPeerCapsRcvd->RcvdCapability.u1CapCode == \
        pPeerCapsRcvd->RcvdCapability.u1CapCode)\
   { \
      u1NumInstance++; \
   }\
}
 #define CAPS_FIND_SPKR_CAP_INSTANCE_NO(pCapsList, pSpkrSupCap, u1NumInstance, pTmpSpkrSupCap, typecast)      \
 u1NumInstance = 0; \
TMO_SLL_Scan (pCapsList, pTmpSpkrSupCap, typecast) \
{   \
   if (pTmpSpkrSupCap == pSpkrSupCap) \
   {\
      break;\
   }\
   if (pTmpSpkrSupCap->SpkrCapability.u1CapCode == \
        pSpkrSupCap->SpkrCapability.u1CapCode)\
   { \
      u1NumInstance++; \
   }\
}

#define MP_CAPS_VAL_ASSIGN(MpCapabilityValue, u2Afi, u1Safi)\
{\
PTR_ASSIGN2 (MpCapabilityValue.pu1_OctetList, (u2Afi));\
*((MpCapabilityValue.pu1_OctetList) + BGP4_MPE_ADDRESS_FAMILY_LEN) = 0;\
*((MpCapabilityValue.pu1_OctetList) + BGP4_MPE_ADDRESS_FAMILY_LEN + BGP4_MPE_MP_CAP_RESERVED_FIELD_LEN) = u1Safi;\
MpCapabilityValue.i4_Length = BGP4_MPE_MP_CAP_LEN;\
}

/********************  Proto types ****************************************/


INT4    CapsInitHandler (UINT4) ;
INT4    CapsFormHandler (UINT1 * pOpnMsgToFillCaps, 
                                tBgp4PeerEntry *pPeerEntry) ;
INT4    CapsUpdtCapsInOpnMsg (UINT4, UINT1 * pOpnMsgToFillCaps,
                              tTMO_SLL * pSpkrSupCapsLst, tBgp4PeerEntry * pPeerEntry) ; 
INT4    Bgp4CapsProcessNotification(UINT1 * pNotfMsgRcvd,
                    tBgp4PeerEntry * pPeerEntry);
INT4    CapsDeletePeerCapsInfo (tBgp4PeerEntry *pPeerEntry);
INT4    CapsDeleteSpkrCapsInfo ( tBgp4PeerEntry *pPeerEntry);
INT4    CapsRcvdHandler (UINT1 * pOpnMsgRcvd,     
                            tBgp4PeerEntry * pPeerEntry, UINT2 *pu2RcvdCapLen);
INT4 Bgp4DuplicatePeerCaps (tTMO_SLL *, tTMO_SLL *);
INT4  CapsDeletePeerCaps(UINT4);
INT4  CapsDeleteSpeakerCaps(UINT4);
INT4  CapsSpkrSupCapCreate (tBgp4PeerEntry *pPeerEntry, UINT1 u1CapCode,
                     UINT1 u1CapLength,tSNMP_OCTET_STRING_TYPE *pCapValue,
                     tSupCapsInfo **ppSpkrSupCap );
INT4  GetFindSpkrSupCap (tBgp4PeerEntry *pPeerEntry, UINT1 u1CapCode,
        UINT1 u1CapLength, tSNMP_OCTET_STRING_TYPE *pCapValue,
        UINT1 u1CapSupAncd, tSupCapsInfo **ppSpkrSupCap);

INT4 CapsShutDown (UINT4);
INT4 CapsClearSpkrAncdSts (tBgp4PeerEntry * pPeerEntry);
INT4 Bgp4ProcessCapNegotiation (tBgp4PeerEntry *, UINT1 , UINT4, UINT1);
INT4 Bgp4ProcessMpCapNegotiation (tBgp4PeerEntry *, UINT4 );
INT4 IsCapMandatory (UINT1 u1CapCode,UINT1 *pu1MandCapsFound);        
/****************************************************************************/
#endif /* _BGP4CAPS_H */

