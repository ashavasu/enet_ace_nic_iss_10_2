/*$ Id: bgp4trie.h,v 1.2 2011/12/30 11:52:21 siva Exp $ */
#ifndef BGP4_TRIE_H
#define  BGP4_TRIE_H

 typedef struct _TrieOutParams
{
    tRouteProfile *pRtProfile;
    UINT4          u4Data;
}tBgp4TrieOutParams;

#define BGP4_INPUT_PARAMS_KEY_SIZE 32   
/* Definition indicating TRIE Application Data */
#define     BGP4_ROUTE_TRIE                 1 /* AppSpec Information stored
                                               * in TRIE is tRouteProfile.
                                               */
/* Used by RIB module */
#define BGP4_IPV4UNICAST_RIB                1
#define BGP4_IPV6UNICAST_RIB                2
#ifdef L3VPN
#define BGP4_IPV4LBLD_UNICAST_RIB           3
#define BGP4_VPN4UNICAST_RIB                4
#endif
#ifdef VPLSADS_WANTED
#define BGP4_L2VPNVPLS_RIB                  5 
#endif
#ifdef EVPN_WANTED
#define BGP4_EVPN_RIB                       6
#endif
/*This has to be modified if new RIB is added*/
#define BGP4_MAX_DEFAILT_RIB                4

/* Trie Related function Declarations. */
INT4                Bgp4TrieInit (UINT4);
INT4                Bgp4TrieCreate (UINT4, UINT4, UINT4, VOID **);
INT4                Bgp4TrieDelete (UINT4, UINT4, UINT4, VOID **);
INT4                Bgp4TrieAddEntry (tRouteProfile *, UINT4, UINT1 *);
INT4                Bgp4TrieDeleteEntry (tRouteProfile *, VOID *, UINT4, UINT1 *);
INT4                Bgp4TrieLookupExactEntry (tRouteProfile *, UINT4,
                                              tRouteProfile **, VOID **);
INT4                Bgp4TrieLookupOverlapEntry (tRouteProfile *, UINT4,
                                                tRouteProfile **, VOID **);
INT4                Bgp4TrieGetNextEntry (tRouteProfile *, UINT4,
                                          tRouteProfile **,
                                          VOID **, INT4);
INT4                Bgp4TrieGetFirstEntry (UINT4 , tRouteProfile **, VOID **,
                                           INT4);
INT4                Bgp4TrieGetPrevEntry (tRouteProfile *, tRouteProfile **,
                                          VOID **);
INT4                Bgp4TrieTraverseAndGetNextEntry (tRouteProfile *, VOID *,
                                                     tRouteProfile **);
INT4                Bgp4TrieTraverseAndGetPrevEntry (tRouteProfile *, VOID *, 
                                                     tRouteProfile **);
/* TRIE CallBack Function Declaration */
VOID *              Bgp4TrieCbDeleteTrieInit (tInputParams *,
                                              VOID (AppSpec)(VOID *), VOID *);
VOID                Bgp4TrieCbDeleteTrieDeInit (VOID *);
INT4                Bgp4TrieDeleteAppData (VOID *, VOID **, tKey);
VOID                Bgp4TrieAppSpecDelete (VOID *);
INT4                Bgp4TrieCbAddEntry (tInputParams *, VOID *, VOID **,
                                        VOID *);
INT4                Bgp4TrieCbDeleteEntry (tInputParams *, VOID **, VOID *,
                                           VOID*, tKey);
INT4                Bgp4TrieCbLookupExactEntry (tInputParams *,
                                                tBgp4TrieOutParams *, VOID *);
INT4                Bgp4TrieCbLookupOverlapEntry (tInputParams *,
                                                  tBgp4TrieOutParams*,
                                                  VOID *, UINT2 , tKey);
INT4                Bgp4TrieCbTraverseAndGetNextEntry (tInputParams *,
                                                       tBgp4TrieOutParams *,
                                                       VOID *, tKey);
VOID                Bgp4GetRibNode (UINT4, UINT4, VOID **);
#endif /* BGP4_TRIE_H */
