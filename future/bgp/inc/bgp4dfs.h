/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved    
 *
 * $Id: bgp4dfs.h,v 1.52 2017/09/15 06:19:50 siva Exp $
 *
 * Description:This file contains all the definitions that had been           
 *             made in the system.   
 *
 *******************************************************************/
#ifndef BGP4DFS_H
#define BGP4DFS_H

#ifdef BGP4INIT_C
#define EXTERN 
#else
#define EXTERN extern
#endif

#define  BGP4_QUE                                   "BGPQ"
#define  BGP4_Q_NAME_SIZE                           5

/* This macro is to Identify the Standard BGP4 MIB table */
#define BGP4_STD  1
#define BGP4_FUT  2
#define BGP4_MI   3

/* BGP Traps */
#define BGP4_PEER_ESTABLISHED_STATE_CHANGE_TRAP  1
#define BGP4_PEER_BACKWARD_TRANSITION_TRAP       2

/* Definition to control the BGP processing. */
#define  BGP4_INIT_COMPLETE                         (2)
#define  BGP4_DEINIT_COMPLETE                       (3)
#define  BGP4_SOFTRECONFIG_COMPLETE                 (4)
#define  BGP4_SUP_PEER_REUSE_COMPLETE               (5)

/* Bgp events to be processed. */
#define  BGP4_RIBIN_PRCS_EVENT                      (1)
#define  BGP4_RIBIN_VPN4_ROUTE_PRCS_EVENT           (2)
#define  BGP4_VRF_CHANGE_EVENT                      (3)
#define  BGP4_FIB_UPD_EVENT                         (4)
#define  BGP4_EXT_PEER_ADVT_EVENT                   (5)
#define  BGP4_INT_PEER_ADVT_EVENT                   (6)
#define  BGP4_RM_BULK_PRCS_EVENT                    (7)
#define  BGP4_PEER_INIT_EVENT                       (8)
#define  BGP4_PEER_DEINIT_EVENT                     (9)
#define  BGP4_EXPTGT_EVENT                          (10)
#define  BGP4_LSP_CHNG_EVENT                        (11) 
#define  BGP4_PEER_SOFT_IN_EVENT                    (12)
#define  BGP4_PEER_SOFT_OUT_EVENT                   (13)
#define  BGP4_PEER_REUSE_EVENT                      (14)

#define  BGP4_SEMANTIC_INVALID_NEXTHOP              (-2)
#define  BGP4_INVALID_UNKNOWN_ATTR                  (-3)
#define  BGP4_RECEIVED_CONFED_SEGMENT_FROM_EXT_PEER (-4)
#define  BGP4_INVALID_ORIG_ID                       (-5)
#define  BGP4_INVALID_CLUS_LIST                     (-6)
#define  BGP4_RSRC_ALLOC_FAIL                       (-7)
#define  BGP4_PEER_NETWORK_ADDR_NOT_CONF            (-8)
#define  BGP4_PEER_LCL_ADDR_NOT_AVAILABLE           (-9)
#define  BGP4_MAX_ROUTES_LIMIT                      (-10)
#define  BGP4_NOT_NEGOTIATED_FAMILY                 (-11)
#define  BGP4_MAX_MP_ROUTE_PREFIX_LIMIT             (-12)

#define  PROTO_INVALID           0x00
#define  BGP4_VERSION_4          0x04
#define  BGP_ZERO                                   0
/* Invalid */

#define BGP4_RB_EQUAL   0
#define BGP4_RB_LESSER -1
#define BGP4_RB_GREATER 1
#define  BGP4_INV_AS             0x0000
#define  BGP4_AS_TRANS           23456
#define  BGP4_INV_IPADDRESS      0x00000000
#define  BGP4_INV_MEDLP          0x00
#define  BGP4_INV_PORT           0x00
#define  BGP4_INV_CONN_ID        -1
#define  BGP4_INV_VER            0x00
#define  BGP4_INVALID_ROUTE      0xffffffff
#define  BGP4_INVALID_MASK       0x00000000
#define  BGP4_INVALID_IFINDX     0xffffffff
#define  BGP4_INV_INDEX          -1
#define  BGP4_INVALID_NH_METRIC  -1
#define  BGP4_INVALID_ROUTE_TAG  0
#define  BGP4_INVALID_PEER       0xffffffff
#define  BGP4_INVALID_METRIC     (-1)

#define  BGP4_INVALID                 0xff
#define  BGP4_INVALID_HIST            -1 
#define  BGP4_DOWN                    0
#define  BGP4_PEER_DOWN               0
#define  BGP4_PEER_UP                 1
#define  BGP4_ADMIN_DOWN              2
#define  BGP4_ADMIN_INVALID           3
#define  BGP4_ADMIN_UP                1
#define  BGP4_RRD_ADMIN_UP            1

#define  BGP4_STOP                           0x01
#define  BGP4_START                          0x02
#define  BGP4_DISABLE_REDISTRIBUTION         0x2
#define  BGP4_ENABLE_REDISTRIBUTION          0x1
#define  BGP4_PEER_AUTOMATICSTART_ENABLE     0x1
#define  BGP4_PEER_AUTOMATICSTART_DISABLE    0x2
#define  BGP4_PEER_AUTOMATICSTOP_ENABLE      0x1
#define  BGP4_PEER_AUTOMATICSTOP_DISABLE     0x2
#define  BGP4_DAMP_PEER_OSCILLATIONS_ENABLE  0x1
#define  BGP4_DAMP_PEER_OSCILLATIONS_DISABLE 0x2 
#define  BGP4_PEER_DELAY_OPEN_ENABLE         0x1
#define  BGP4_PEER_DELAY_OPEN_DISABLE        0x2
#define  BGP4_TRAP_ENABLE                    0x1
#define  BGP4_TRAP_DISABLE                   0x2
#define  BGP4_PASSIVE_ENABLE                 0x1
#define  BGP4_PASSIVE_DISABLE                0x2
#define  BGP4_IBGP_REDISTRIBUTE_ENABLE       0x1
#define  BGP4_IBGP_REDISTRIBUTE_DISABLE      0x2
#define  BGP4_HOLD_ROUTES_ENABLE      0x1
#define  BGP4_HOLD_ROUTES_DISABLE      0x2

/* Max. allowed values */
#define  BGP4_MAX_AS          0xFFFFFFFF
#define  BGP4_MAX_TWO_BYTE_AS 0xFFFF
#define  BGP4_MAX_PREFIX     0xFFFFFFFF
#define  BGP4_MAX_PREFIXLEN  32
#define  BGP4_MIN_PREFIXLEN  0x00
#define  BGP4_MAX_INT_AS               0x0A
#define  BGP4_MAX_ORIG_STR_LEN         50
#define  BGP4_MAX_UNKNOWN_LEN          255
#define  BGP4_MAX_PEERPREFIX_LIMIT     2147483647 
#define  BGP4_MAX_CONNECTRETRY_COUNT   50

#define  BGP4_TCPAO_PEER_MKTDISCARD_TRUE   2
#define  BGP4_TCPAO_PEER_MKTDISCARD_FALSE 1
#define  BGP4_TCPAO_PEER_ICMPACCEPT_TRUE   1
#define  BGP4_TCPAO_PEER_ICMPACCEPT_FALSE 2
/* Max. Output Buffer Size */
#define  BGP4_MAX_OUTBUF_LEN  BGP4_MAX_MSG_LEN    /* 4096 */
#define  BGP4_MSG_POOL_NUM    300

/* Defaults */
#define  BGP4_DEF_LSNPORT                179
#define  BGP4_DEF_STARTINTERVAL          1      /* Secs */
#define  BGP4_DEF_CONNRETRYINTERVAL      30     /* Secs */
#define  BGP4_DEF_HOLDINTERVAL           90    /* Secs */
#define  BGP4_INITIAL_HOLD_TIME          240    /* 4 min */
#define  BGP4_DEF_KEEPALIVEINTERVAL      (BGP4_DEF_HOLDINTERVAL/3) /* Secs */
#define  BGP4_DEF_MINASORIGINTERVAL      15     /* 15 Secs */
/* RFC 4271 Updates */
#define  BGP4_DEF_IBGP_MINROUTEADVINTERVAL    5     /* 5 Secs */
#define  BGP4_DEF_EBGP_MINROUTEADVINTERVAL    30     /* 30 Secs */
#define  BGP4_DEF_LP                     100
#define  BGP4_DEF_IGP_METRIC_VAL         0
#define  BGP4_ADVT_LIST_CLEAN_GRANULARITY 10
#define  BGP4_DEF_IDLEHOLDINTERVAL       60
#define  BGP4_DEF_DELAYOPENINTERVAL      0
#define  BGP4_DEF_MAX_PEER_PREFIX_LIMIT  5000 /* MAX_BGP_ROUTES */
#define  BGP4_DEF_CONNECT_RETRY_COUNT    5 
#ifdef EVPN_WANTED
#define  BGP4_DEF_MAC_MOB_DUP_INTERVAL   180
#define  BGP4_MAX_MAC_MOB_DUP_INTERVAL   36000
#define  BGP4_MIN_MAC_MOB_DUP_INTERVAL   2
#define  BGP4_DEF_MAX_MAC_MOVES            5
#define  BGP4_MAX_MAC_MOVES               1000
#define  BGP4_MIN_MAC_MOVES               1
#endif

/* Minimum Value */
#define  BGP4_MIN_HOLD_INTERVAL       3        
#define  BGP4_MAX_HOLD_INTERVAL       0xffff
#define  BGP4_MIN_KEEP_INTERVAL       1         
#define  BGP4_MAX_KEEP_INTERVAL       (0xffff/3)
#define  BGP4_MIN_ASORIG_INTERVAL     1         
#define  BGP4_MAX_ASORIG_INTERVAL     0xffff
#define  BGP4_MIN_RA_INTERVAL         1         
#define  BGP4_MAX_RA_INTERVAL         0xffff
#define  BGP4_MIN_CONNRETRY_INTERVAL  1
#define  BGP4_MAX_CONNRETRY_INTERVAL  0xffff
#define  BGP4_MIN_IDLE_HOLD_INTERVAL  1
#define  BGP4_MAX_IDLE_HOLD_INTERVAL  0xffff
#define  BGP4_MIN_DELAY_OPEN_INTERVAL 1
#define  BGP4_MAX_DELAY_OPEN_INTERVAL 0xffff

#define  BGP4_MAX_STATE_LEN                0x0f
#define  BGP4_PEER_FSM_TRANS_HIST_MAX_INDX 0x0a
#define  BGP4_GROUP                        0x01
#define  BGP4_PREPEND                      0x02
#define  BGP4_TIMESTAMP                    0x03

/* These five constants are used in confed */
#define  BGP4_MAX_BYTE_VALUE            0xFF

#define  BGP4_NORMAL_ATTR_LEN_SIZE      0x01
#define  BGP4_EXTENDED_ATTR_LEN_SIZE    0x02
#define  BGP4_MIN_AS_IN_SEG             0x01

/* Initialisation Values */
#define  BGP4_INIT_COUNT  0x00
#define  BGP4_INIT_TIME   0x00

/* Authentication Types */
#define  BGP4_NO_AUTH  0x00

/*  NEXTHOP Self Status */
#define  BGP4_DISABLE_NEXTHOP_SELF  0x01
#define  BGP4_ENABLE_NEXTHOP_SELF   0x02

/*  Override Capability */
#define  BGP4_ENABLE_OVERRIDE_CAPABILITY   0x01
#define  BGP4_DISABLE_OVERRIDE_CAPABILITY  0x02
/* Synchronisation */
#define  BGP4_ENABLE_SYNC   0x01
#define  BGP4_DISABLE_SYNC  0x02

/* 4-Byte ASN support */
#define  BGP4_ENABLE_4BYTE_ASN_SUPPORT         0x01
#define  BGP4_DISABLE_4BYTE_ASN_SUPPORT        0x02
#define  BGP4_4BYTE_ASN_SPEAKER_ONLY              1    /* 4-byte ASN Capability is supported by the SPEAKER only. 
                                                        Not by the PEER */
#define  BGP4_4BYTE_ASN_SPEAKER_AND_PEER          2    /* 4-byte ASN Capability is supported by both SPEAKER and PEER */
#define  BGP4_4BYTE_ASN_NO_SPEAKER                3    /* 4-byte ASN Capability not supported by SPEAKER */
#define BGP4_DISABLE_4BYTE_ASN_ASDOT_NOTATION  0x01
#define BGP4_ENABLE_4BYTE_ASN_ASDOT_NOTATION   0x02

#define  BGP4_NH_CHG_PRCS_MAX_INTERVAL          120
/* Applicability */
#define  BGP4_NORMAL      0x01
#define  BGP4_OVERRIDING  0x02

/* Aggregation Types */
/*Aggregation Actions*/
#define  BGP4_SUMMARY  0x01
#define  BGP4_ALL      0x02
#define  BGP4_RESTRICT 0x03

/* Error Codes  */
#define  BGP4_NO_ERROR  0x00

/* BGP Message Types */
#define  BGP4_OPEN_MSG          0x01
#define  BGP4_UPDATE_MSG        0x02
#define  BGP4_NOTIFICATION_MSG  0x03
#define  BGP4_KEEPALIVE_MSG     0x04
#define  BGP4_ROUTE_REFRESH_MSG 0x05

/* BGP Message lengths */
#define  BGP4_MARKER_LEN          16
#define  BGP4_MSG_COMMON_HDR_LEN  19
#define  BGP4_MIN_MSG_LEN         BGP4_MSG_COMMON_HDR_LEN
#define  BGP4_MAX_MSG_LEN         4096

#define  BGP4_KEEPALIVE_MSG_LEN   19
#define  BGP4_OPEN_MSG_MIN_LEN    29
#define  BGP4_UPDATE_MSG_MIN_LEN  23
#define  BGP4_NOTIFY_MSG_MIN_LEN  21
#define  BGP4_EOR_MARKER_MAX_LEN  29

/* TCP Primitives */
#define  BGP4_TCP_OPEN         0x01
#define  BGP4_TCP_OPENED       0x02
#define  BGP4_TCP_OPEN_FAILED  0x03
#define  BGP4_TCP_CLOSE        0x04
#define  BGP4_TCP_CLOSED       0x05
#define  BGP4_TCP_SEND         0x06
#define  BGP4_TCP_ERROR        0x07

/* BGP4 SNMP Primitives */
#define  BGP4_SNMP_START  0x10
#define  BGP4_SNMP_STOP   0x11

/* BGP4 Timers */
#define  BGP4_START_TIMER         1 
#define  BGP4_CONNECTRETRY_TIMER  2 
#define  BGP4_HOLD_TIMER          3 
#define  BGP4_KEEPALIVE_TIMER     4 
#define  BGP4_MINASORIG_TIMER     5 
#define  BGP4_MINROUTEADV_TIMER   6 
#define  BGP4_ONESEC_TIMER        7
#define  BGP4_RFD_REUSE_TIMER     8 
#define  BGP4_SELECTION_DEFERRAL_TIMER    9
#define  BGP4_STALE_TIMER                 10
#define  BGP4_RESTART_TIMER               12
#define  BGP4_PEER_RESTART_TIMER          13
#define  BGP4_IDLE_HOLD_TIMER             14
#define  BGP4_DELAY_OPEN_TIMER            15
#ifdef EVPN_WANTED
#define  BGP4_MAC_MOB_DUP_TIMER           16
#endif
#define  BGP4_NETWORK_MSR_TEMP_TIMER      17
#define  BGP4_ROUTE_REUSE_TIMER           18
/* BGP4 Notification Codes */

#define  BGP4_MSG_HDR_ERR           0x01
#define  BGP4_OPEN_MSG_ERR          0x02
#define  BGP4_UPDATE_MSG_ERR        0x03
#define  BGP4_HOLD_TMR_EXPIRED_ERR  0x04
#define  BGP4_FSM_ERR               0x05
#define  BGP4_CEASE                 0x06
#define  BGP4_NOTIFY_MSG_ERR        0x07

/* Message Header Notification Subcodes */
#define  BGP4_CONN_NOT_SYNC  0x01
#define  BGP4_BAD_MSG_LEN    0x02
#define  BGP4_BAD_MSG_TYPE   0x03

/* OPEN message Notification Subcodes */
#define  BGP4_UNSUPPORTED_VER_NO     0x01
#define  BGP4_AS_UNACCEPTABLE        0x02
#define  BGP4_BGPID_INCORRECT        0x03
#define  BGP4_OPT_PARM_UNRECOGNIZED  0x04
/* 4271 Updates 
 #define  BGP4_AUTH_PROC_FAILED       0x05 ----- Deprecated */
#define  BGP4_HOLD_TMR_UNACCEPTABLE  0x06

/* UPDATE message Notification Subcode */
#define  BGP4_MALFORMED_ATTR_LIST          0x01
#define  BGP4_UNRECOGNISED_WELLKNOWN_ATTR  0x02
#define  BGP4_MISSING_WELLKNOWN_ATTR       0x03
#define  BGP4_ATTR_FLAG_ERR                0x04
#define  BGP4_ATTR_LEN_ERR                 0x05
#define  BGP4_INVALID_ORIGIN               0x06
/* 4271 Updates 
#define  BGP4_AS_ROUTING_LOOP              0x07 -------- Deprecated */
#define  BGP4_INVALID_NEXTHOP              0x08
#define  BGP4_OPTIONAL_ATTR_ERR            0x09
#define  BGP4_INVALID_NLRI                 0x0A
#define  BGP4_MALFORMED_AS_PATH            0x0B

/* Attribute Type Fields */
#define  BGP4_ATYPE_FLAGS_LEN              0x01
#define  BGP4_ATYPE_CODE_LEN               0x01

/*  Attribute Codes. */
#define  BGP4_ATTR_ORIGIN                  0x01
#define  BGP4_ATTR_PATH                    0x02
#define  BGP4_ATTR_NEXT_HOP                0x03
#define  BGP4_ATTR_MED                     0x04
#define  BGP4_ATTR_LOCAL_PREF              0x05
#define  BGP4_ATTR_ATOMIC_AGGR             0x06
#define  BGP4_ATTR_AGGREGATOR              0x07
#define  BGP4_ATTR_COMM                    0x08
#define  BGP4_ATTR_ORIG_ID                 0x09
#define  BGP4_ATTR_CLUS_LIST               0x0A
#define  BGP4_ATTR_MP_REACH_NLRI           0x0E
#define  BGP4_ATTR_MP_UNREACH_NLRI         0x0F
#define  BGP4_ATTR_ECOMM                   0x10
#define  BGP4_ATTR_PATH_FOUR               0x11        /* For AS4_PATH - New Attribute added for 4 byte ASN 
                                                          Support - RFC 6793 */
#define  BGP4_ATTR_AGGREGATOR_FOUR         0x12        /* For AS4_AGGREGATOR - New Attribute added for 4 byte ASN
                                                          Support - RFC 6793 */

/*  Attribute Codes Mask. */
#define  BGP4_ATTR_SEND_MED_MASK         0x00000001
#define  BGP4_ATTR_ORIGIN_MASK           0x00000002
#define  BGP4_ATTR_PATH_MASK             0x00000004
#define  BGP4_ATTR_NEXT_HOP_MASK         0x00000008
#define  BGP4_ATTR_MED_MASK              0x00000010
#define  BGP4_ATTR_LOCAL_PREF_MASK       0x00000020
#define  BGP4_ATTR_ATOMIC_AGGR_MASK      0x00000040
#define  BGP4_ATTR_AGGREGATOR_MASK       0x00000080
#define  BGP4_ATTR_COMM_MASK             0x00000100
#define  BGP4_ATTR_ORIG_ID_MASK          0x00000200
#define  BGP4_ATTR_CLUS_LIST_MASK        0x00000400
#define  BGP4_ATTR_ECOMM_MASK            0x00000800
#define  BGP4_ATTR_UNKNOWN_MASK          0x00001000
#define  BGP4_ATTR_MP_REACH_NLRI_MASK    0x00002000
#define  BGP4_ATTR_MP_UNREACH_NLRI_MASK  0x00004000
#define BGP4_ATTR_PATH_FOUR_MASK         0x00008000
#define BGP4_ATTR_AGGREGATOR_FOUR_MASK   0x00010000

/* Attribute Length */
#define  BGP4_ATTR_ORIGIN_LEN           0x01
#define  BGP4_ATTR_NEXTHOP_LEN          0x04
#define  BGP4_ATTR_MED_LEN              0x04
#define  BGP4_ATTR_LOCAL_PREF_LEN       0x04
#define  BGP4_ATTR_ATOMIC_AGGR_LEN      0x00
#define  BGP4_ATTR_AGGREGATOR_LEN       0x06
#define  BGP4_ATTR_AGGREGATOR_FOUR_LEN  0x08
#define  BGP4_ATTR_AGGR_ASLEN           0x02
#define  BGP4_ATTR_AGGR_AS4LEN          0x04
#define  BGP4_ATTR_AGGR_BGPID_LEN       0x04

/* Attribute Length field size */
#define  BGP4_AL_ORIGIN_SIZE        0x01
#define  BGP4_AL_NEXTHOP_SIZE       0x01
#define  BGP4_AL_MED_SIZE           0x01
#define  BGP4_AL_LP_SIZE            0x01
#define  BGP4_AL_ATOMIC_AGGR_SIZE   0x01
#define  BGP4_AL_AGGREGATOR_SIZE    0x01
#define  BGP4_AL_UNKNOWN_SIZE       0x01

/*  Attribute Flags  */
#define  BGP4_OPTIONAL_FLAG_MASK    0x80
#define  BGP4_TRANSITIVE_FLAG_MASK  0x40
#define  BGP4_PARTIAL_FLAG_MASK     0x20
#define  BGP4_EXT_LEN_FLAG_MASK     0x10

/*  Origin Type values  */
#define  BGP4_ATTR_ORIGIN_IGP         0x00
#define  BGP4_ATTR_ORIGIN_EGP         0x01
#define  BGP4_ATTR_ORIGIN_INCOMPLETE  0x02

/*  AS Path Segment Type values  */
#define  BGP4_ATTR_PATH_SET       0x01
#define  BGP4_ATTR_PATH_SEQUENCE  0x02
#define  BGP4_ATTR_PATH_TYPE_LEN  0x01
#define  BGP4_ATTR_PATH_LEN_LEN   0x01

/*  BGPCLI AS_SET Values */
#define AS_SET_ENABLE  1
#define AS_SET_DISABLE 2

/*  AS Path Segment Type values for confederation  */
#define BGP4_ATTR_PATH_CONFED_SEQUENCE 0x03
#define BGP4_ATTR_PATH_CONFED_SET      0x04

/* NLRI */
#define  BGP4_MAX_PREFIX_BYTE_LEN     0x4

/* Advertise both Feasible routes and Withdrawn routes or 
   Advertise only Withdrawn routes only.
*/
#define  BGP4_ADVT_WITHDRAWN_ONLY  0x01
#define  BGP4_ADVT_BOTH            0x02

/* Multi Hop EBGP Status */
#define BGP4_EBGP_MULTI_HOP_ENABLE  0x01
#define BGP4_EBGP_MULTI_HOP_DISABLE 0x02

/*Default multipath*/
#define BGP_DEFAULT_MAXPATH   1
/* Min - Max value for Interface Identifier */
#define BGP4_MIN_SRCIF_IFID  0x00
#define BGP4_MAX_SRCIF_IFID  0xFFFF

/* MED Send Status */
#define BGP4_MED_RECV_SEND   0x01
#define BGP4_MED_RECV_BLOCK  0x02

/* BGP4_RIBH_END_CONN_MAC */
#define BGP4_DEL_NODE_NONE          0x00
#define BGP4_DEL_NODE_LEFT_LEFT     0x01
#define BGP4_DEL_NODE_LEFT_RIGHT    0x02
#define BGP4_DEL_NODE_RIGHT_LEFT    0x03
#define BGP4_DEL_NODE_RIGHT_RIGHT   0x04

#ifdef LNXIP4_WANTED
#define BGP4_DEFAULT_HOPLIMIT       2
#else
#define BGP4_DEFAULT_HOPLIMIT       1 
#endif
#define BGP4_DEFAULT_SENDBUF        (64*1024)
#define BGP4_DEFAULT_RECVBUF        (64*1024)

/* used in config file for range checks  */
#define BGP4_MIN_HOPLIMIT           1
#define BGP4_MAX_HOPLIMIT           255
#define BGP4_MIN_SENDBUFSIZE        (1024*4)
#define BGP4_MAX_SENDBUFSIZE        (1024*64)
#define BGP4_MIN_RECVBUFSIZE        (1024*4)
#define BGP4_MAX_RECVBUFSIZE        (1024*64)
#define BGP4_PASSIVE_SET            (-2)


#define HOPLIMIT                    1
#define SEND_BUF                    2
#define RECV_BUF                    3
#define REUSE_ADDR                  4
#define REUSE_PORT                  5 
#define BGP4_BIND_FAILURE           (-2)
#define HOPLIMITFORIBGP             255

/* Defined for Multiprotocol Extensions support */
#define  BGP4_INET_SAFI_VPNV4_UNICAST           128
#define BGP4_IPV4_UNI_INDEX             0
#ifdef L3VPN
#define BGP4_IPV4_LBLD_INDEX            1
#endif
#define BGP4_BUFFER_FULL                (-2)

#define BGP4_MAX_INET_ADDR_STRING       4
#define BGP4_INET_SAFI_UNICAST          1

#ifdef VPLSADS_WANTED
#define BGP4_INET_SAFI_VPLS             65 
#endif

#ifdef EVPN_WANTED
#define BGP4_INET_SAFI_EVPN             70
#define BGP4_IPV4_PREFIX_LEN            4
#endif

#ifdef L3VPN
/* Carrying Label Information - RFC 3107 */
#define BGP4_INET_SAFI_LABEL            4
#endif
#define BGP4_IPV4_PREFIX_LEN            4
#define BGP4_IPV6_PREFIX_LEN            16
#define BGP4_NULL_STRING                "\0"

#define BGP4_NOPRINT_NEW_LINE           1
#define BGP4_PRINT_NEW_LINE             2

#define BGP4_REMOVE_NONBGP_RT           1
#define BGP4_REMOVE_AGGR_RT             2

/* Status whether just send the UDPATE or after sending
 * a new UPDATE buffer should be reconstructed
 */
#define BGP4_SEND_UPD               1
#define BGP4_SEND_CONSTRUCT_UPD     2

/* These values must be in sync with those defined by IP */

/* defined in ip/ipif/inc/ipifutls.h */
#define BGP4_IFACE_DELETE            0x00000020
/* defined in ip/ipif/inc/ipifdfs.h */
#define BGP4_IPIF_OPER_ENABLE          1
#define BGP4_IPIF_OPER_DISABLE         2
#define BGP4_IFACE_UP              CFA_IF_UP
#define BGP4_IFACE_DOWN            CFA_IF_DOWN

#ifdef L3VPN
#define L3VPN_DEF_MAX_LABELS                5
/* Defined for VPNv4 and Carrying Label features support */
#define BGP4_VPN4_LABEL_SIZE                3
#define BGP4_VPN4_MAX_LABELS                L3VPN_DEF_MAX_LABELS
#define BGP4_DEF_WDR_LABEL                  0x00800000
#define BGP4_VPN4_ROUTE_DISTING_SIZE        8
#endif

/* DEBUG  Flags  */
#define  BGP4_DEBUG_ERROR    0x00000001

#define  BGP4_DEBUG_RIBH     0x00000002
#define  BGP4_DEBUG_AH       0x00000004
#define  BGP4_DEBUG_DSH      0x00000008
#define  BGP4_DEBUG_DECH     0x00000010
#define  BGP4_DEBUG_ATTRH    0x00000020
#define  BGP4_DEBUG_AGGRH    0x00000040
#define  BGP4_DEBUG_FILTERH  0x00000080

#define  BGP4_DEBUG_TCPH     0x00000100
#define  BGP4_DEBUG_IH       0x00000200
#define  BGP4_DEBUG_SEMH     0x00000400
#define  BGP4_DEBUG_MSGH     0x00000800
#define  BGP4_DEBUG_MEMH     0x00001000
#define  BGP4_DEBUG_SNMPH    0x00002000
#define  BGP4_DEBUG_INITH    0x00004000
#define  BGP4_DEBUG_EH       0x00008000
#define  BGP4_DEBUG_TMRH     0x00010000

#define  BGP4_DEBUG_IGPH     0x00020000

#define  BGP4_PEER_STOP           1
#define  BGP4_PEER_START          2
#define  BGP4_PEER_AUTO_START     3 

#define  BGP4_PEER_CREATE         1
#define  BGP4_PEER_DELETE         2

#define  BGP4_Q_DEPTH             1050
#define  BGP4_Q_STRING_SIZE       5
#define  BGP4_Q_MODE              OSIX_LOCAL
#define  BGP4_TASK_MAIN_FUNC      Bgp4TaskMain
#define  BGP4_TASK_DEF_DEPTH      (1024 * 20)
#define  BGP4_TASK_NAME_SIZE      5
#define  SNMP_AGENT_TSK_INPUT_Q0  "AtQ0"
#define  BGP4_PRINT_BUF_SIZE      100
#define  MASK_SHIFT_LEN     8
#define  DEFAULT_MASK_LEN   1
#define  BGP4_MAX_IP_STR_LEN 50

#define  BGP4_ONE_BYTE          1
#define  BGP4_TWO_BYTE          2
#define  BGP4_THREE_BYTE        3
#define  BGP4_FOUR_BYTES        4 
#define  BGP4_FOUR_BITS         4 
#define  BGP4_ONE_BYTE_BITS     8
#define  BGP4_TWO_BYTE_BITS     16
#define  BGP4_THREE_BYTE_BITS   24
#define  BGP4_FOUR_BYTE_BITS    32
#define  BGP4_SINGLE_BIT        1
#define  BGP4_ONE_BYTE_MAX_VAL  0xFF
#define  BGP4_TWO_BYTE_MAX_VAL  0xFFFF
#define  BGP4_INCREMENT_BY_ONE  1
#define  BGP4_DECREMENT_BY_ONE  1
#define  BGP4_ONE_SECOND        1
#define  BGP4_MAX_SUPPORTED_PROTOCOLS 4 /* DIRECT, STATIC, RIP, OSPF */
#define  BGP4_GET_DECIMAL_DIGIT 10
#define  BGP4_MAX_ENTRY_PER_PEER 2

#define  BGP4_AS_PATH_PRINT      10 
#define  BGP4_RIB_CLI_PRINT      20

#define BGP4_INACTIVE            0
#define BGP4_ACTIVE              1
#define BGP4_DORMANT             2

#define BGP4_TRUE                1
#define BGP4_FALSE               2
#define BGP4_RELINQUISH          3
#define BGP4_ENTRYCREATE_FAILURE 4
#define BGP4_HISTORY             5
#define BGP4_DEFERAL_TMR_SET    3

#define BGP4_SRC_LCL_AS 1
#define BGP4_PEER_LCL_AS 1

#define BGP4_CLEAR_PEER_FAILURE             1
#define BGP4_CLEAR_FAILURE             2
#define BGP4_CLEAR_RIB_LOCK_FAILURE             3
#define BGP4_CLEAR_RIB_FAILURE             4
#define BGP4_CLEAR_RFD_DIS_FAILURE             5
#define BGP4_CLEAR_SET_FAILURE             6
#define BGP4_CLEAR_ADMIN_FAIL 7


#define BGP4_TCPH_OPEN_FAILURE      (-2)
#define BGP4_TCPH_CONN_IN_PROGRESS    2

/* Definitions used by low-level routines */
#define BGP_MAX_ERR_ARRAY_SIZE                  8
#define BGP_ERROR_CODE_LENGTH                   sizeof(UINT1)
#define BGP_ERROR_SUBCODE_LENGTH                sizeof(UINT1)
#define BGP_MAX_AS_VALUE                        0xFFFFFFFF
#define BGP4_MAX_TEMP_ASARRAY_SIZE              120
#define BGP4_AS_LENGTH                          2   /* For 2 byte ASN */
#define BGP4_AS4_LENGTH                         4   /* For 4 byte ASN */
#define BGP4_LESS_SPECIFIC_ROUTE_NOT_SELECTED   1 
#define BGP4_LESS_SPECIFIC_ROUTE_SELECTED       2 
#define BGP4_BEST_ROUTE                         2
#define BGP4_NOT_BEST_ROUTE                     1
#define BGP4_STALE_ROUTE                        3
#define BGP4_MAX_LOCAL_PREF                     0x7FFFFFFF
#define BGP4_MIN_LOCAL_PREF                     0
#define BGP4_MIN_MED                            1
#define BGP4_MAX_MED                            0x7FFFFFFF
#define BGP4_MIN_IGP_METRIC                     0
#define BGP4_MAX_IGP_METRIC                     0x7FFFFFFF
#define BGP4_MED_COMPARE_ENABLE                 1
#define BGP4_MED_COMPARE_DISABLE                2
#define BGP4_MIN_IFINDX                         0x01
#define BGP4_MIN_METRIC                         0x0
#define BGP4_MAX_METRIC                         0xFFFFFFFF
#define BGP4_MIN_AS                             0x01
#define BGP4_MIN_TRACE                          0x0
#define BGP4_MAX_TRACE                          0x7FFFFFFF
#define BGP4_MIN_DEBUG                          0x0
#define BGP4_MAX_DEBUG                          0xFFFFFFFF
#define BGP4_TASK_NAME_SIZE                     5
#define BGP4_QUEUE_NAME_SIZE                    5
#define BGP4_MIN_NODE_CNT                       1
#define BGP4_MIN_AS_CNT                         1
#define BGP4_PEER_START_EXP_INTERVAL            2
#define BGP4_MARKER_BYTE_VAL                    0xff
#define BGP4_NEXTHOP_ROUTER_MASK                0xffffffff
#define BGP4_ALL_APPIDS                         (-1) 
#define BGP4_QUERY_APPID                        (-2) 
#define BGP4_HOST_MASK                          0xffffffff
#define BGP4_MSB                                0x80000000
#define BGP4_ONE_BYTE_MSB                       0x80
#define BGP4_MIN_ROUTE_IN_LEAF                  1

/* Definition for ROW STATUS value */
#define BGP4_ROWSTATUS_ACTIVE                   1
#define BGP4_ROWSTATUS_NOT_IN_SERVICE           2
#define BGP4_ROWSTATUS_NOT_READY                3
#define BGP4_ROWSTATUS_CREATE_AND_GO            4
#define BGP4_ROWSTATUS_CREATE_AND_WAIT          5
#define BGP4_ROWSTATUS_DESTROY                  6
#define BGP4_DPND_OBJ_UNSET                     0
#define BGP4_DPND_OBJ_SET                       1

/* constants used in bgp4eh.c */
#define    PRINT_BUF_SIZE 200
#define    ARG0      0
#define    ARG1      1 
#define    ARG2      2 
#define    ARG3      3 
#define    ARG4      4 
#define    ARG5      5 
#define    ARG6      6 
#define    ARG7      7 
#define    ARG8      8 

#define BGP4_DUMP_NOTIFY_MSG_SIZE    10
#define BGP4_DUMP_BUF_LEN            12
#define BGP4_WITHDRAWN_LEN_FIELD_SIZE 2 
#define BGP4_ATTR_LEN_FIELD_SIZE      2 

#define BGP4_MAX_CHILD_NODES          2
#define BGP4_LEFT                     0
#define BGP4_RIGHT                    1

#define BGP4_SELECT_ADD               (1)
#define BGP4_SELECT_DELETE            (2)

#define BGP4_LOCAL_BGP_ID_STATIC      (1)
#define BGP4_LOCAL_BGP_ID_DYNAMIC     (2)

#define BGP4_COMM_ADD_TBL             (1)
#define BGP4_COMM_DEL_TBL             (2)

#define BGP4_COMM_IN_FLT_TBL          (1)
#define BGP4_COMM_OUT_FLT_TBL         (2)

#define BGP4_ECOMM_ADD_TBL            (1)
#define BGP4_ECOMM_DEL_TBL            (2)

#define BGP4_ECOMM_IN_FLT_TBL         (1)
#define BGP4_ECOMM_OUT_FLT_TBL        (2)

#define BGP4_ECOMM_COMM_BOTH          (2)

#define BGP4_START_TIME_THRESHOLD     (120)
#define BGP4_START_TIME_EXPONENTIAL   (2)

#define BGP4_TCPMD5_PWD_SET           (1)
#define BGP4_TCPMD5_PWD_RESET         (2)
#define BGP4_TCPMD5_PWD_SIZE          (80)

/*definition for Next hop metrics. */
#define BGP4_IGP_METRIC_HASH_TABLE_SIZE      (32)
#define BGP4_GET_HASH_INDEX_BIT_MASK         (0x80000000)

#define BGP4_IGP_METRIC                      (1)
#define BGP4_IGP_METRIC_ROWSTATUS            (2)

#define BGP4_NEXTHOP_METRIC_STABLE           (1)
#define BGP4_NEXTHOP_METRIC_CHANGED          (2)

#define BGP4_DEFAULT_NEXTHOP_METRIC          (0)
#define BGP4_MAX_NEXTHOP_METRIC              (0x7FFFFFFF)

/* Flags indicating messages sent/received */
#define BGP4_SEND                            (1)
#define BGP4_RECV                            (2)

/* Added for LOW-LEVEL routines */
/* Definitions used for Peer Table */
#define BGP4_PEER_STATE_OBJECT                   (1)
#define BGP4_PEER_ADMIN_STATUS_OBJECT            (2)
#define BGP4_PEER_NEG_VERSION_OBJECT             (3)
#define BGP4_PEER_LOCAL_PORT_OBJECT              (4)
#define BGP4_PEER_REMOTE_PORT_OBJECT             (5)
#define BGP4_PEER_REMOTE_AS_OBJECT               (6)
#define BGP4_PEER_CONNECT_RETRY_TIME_OBJECT      (7)
#define BGP4_PEER_HOLD_TIME_OBJECT               (8)
#define BGP4_PEER_KEEPALIVE_OBJECT               (9)
#define BGP4_PEER_HOLD_TIME_CONF_OBJECT          (10)
#define BGP4_PEER_KEEPALIVE_CONF_OBJECT          (11)
#define BGP4_PEER_MINAS_ORIG_OBJECT              (12)
#define BGP4_PEER_MINROUTE_ADVT_OBJECT           (13)
#define BGP4_PEER_INUPDATES_OBJECT               (14)
#define BGP4_PEER_OUTUPDATES_OBJECT              (15)
#define BGP4_PEER_IN_TOTAL_MSG_OBJECT            (16)
#define BGP4_PEER_OUT_TOTAL_MSG_OBJECT           (17)
#define BGP4_PEER_FSM_TRANS_OBJECT               (18)
#define BGP4_PEER_IN_UPDT_ELAPSE_OBJECT          (19)
#define BGP4_PEER_ESTAB_TIME_OBJECT              (20)
#define BGP4_PEER_EOR_RECVD_OBJECT               (21)
#define BGP4_PEER_EOR_SENT_OBJECT                (22)
#define BGP4_PEER_RESTART_MODE_OBJECT            (23)
#define BGP4_PEER_RST_TIME_OBJECT                (24)
#define BGP4_PEER_ALLOW_AUTOMATIC_START_OBJECT   (25)
#define BGP4_PEER_ALLOW_AUTOMATIC_STOP_OBJECT    (26)
#define BGP4_PEER_IDLE_HOLD_TIME_CONF_OBJECT     (27)
#define BGP4_DAMP_PEER_OSCILLATIONS_OBJECT       (28)
#define BGP4_PEER_DELAY_OPEN_OBJECT              (29)
#define BGP4_PEER_DELAY_OPEN_TIME_CONF_OBJECT    (30)
#define BGP4_PEER_PREFIX_UPPER_LIMIT_OBJECT      (31)
#define BGP4_PEER_TCP_CONNECT_RETRY_COUNT_OBJECT (32)
#define BGP4_PEER_TCP_CURRENT_CONNECT_RETRY_COUNT_OBJECT (33)
#define BGP4_PEER_DAMPED_STATUS_OBJECT           (34)
#define BGP4_PEER_LOCAL_AS_OBJECT                (35) 
#define BGP4_PEER_TCPAO_ICMP_ACCEPT_OBJECT  (36)
#define BGP4_PEER_TCPAO_PKT_DISCARD_OBJECT  (37)
#define BGP4_PEER_TCPAO_MKT_ASSOCIATE_OBJECT  (38)
#define BGP4_PEER_TCPAO_MKT_NO_ASSOCIATE_OBJECT  (39)
#define BGP4_PEER_TCPAO_PEERKEYID_GET            (40)
#define BGP4_PEER_HOLD_ADVT_ROUTES_OBJECT   (41)

/* Definitions used for Path attribute Table */
#define BGP4_PATHATTR_TBL_AGGRAS_OBJECT      (21)
#define BGP4_PATHATTR_TBL_CALC_LP_OBJECT     (22)
#define BGP4_PATHATTR_BEST_OBJECT            (23)

/* Defintions used for Peer Extensions Table */
#define BGP4_PEER_EXT_CONF_OBJECT              (59)
#define BGP4_PEER_EXT_REMOTE_AS_OBJECT         (60)
#define BGP4_PEER_EXT_MULTIHOP_OBJECT          (61)
#define BGP4_PEER_EXT_NEXTHOP_SELF_OBJECT      (62)
#define BGP4_PEER_EXT_HOPLIMIT_OBJECT          (63)
#define BGP4_PEER_EXT_TCP_SENDBUF_OBJECT       (64)
#define BGP4_PEER_EXT_TCP_RECVBUF_OBJECT       (65)
#define BGP4_PEER_EXT_RFL_CLIENT_OBJECT        (66)
#define BGP4_PEER_EXT_COMM_SEND_STATUS_OBJECT  (67)
#define BGP4_PEER_EXT_ECOMM_SEND_STATUS_OBJECT (68)
#define BGP4_PEER_EXT_PASSIVE_SET_OBJECT       (69)
#define BGP4_PEER_EXT_OVERRIDE_CAPABILITY_OBJECT (77)
/*RFD definitions*/
#define BGP4_RFD_RT_FLAP_COUNT_OBJECT          (70)
#define BGP4_RFD_RT_FLAP_TIME_OBJECT           (71)
#define BGP4_RFD_RT_REUSE_TIME_OBJECT          (72)
#define BGP4_PEER_EXT_DEFAULT_ROUTE_OBJECT     (35)
#define BGP4_PEER_EXT_ACTIVATE_MP_CAP_OBJECT   (36)
#define BGP4_PEER_EXT_DEACTIVATE_MP_CAP_OBJECT (37)
#ifdef L3VPN
#define BGP4_PEER_EXT_VPN_CE_RT_ADVT_OBJECT    (38)
#endif
/* Definitions used for Import Route Table */
#define BGP4_IMPORT_RT_ACTION_OBJECT           (39)

/* Definitions used for Rfd Routes Damp History Table */
#define BGP4_RFD_RT_FOM_OBJECT                 (40)
#define BGP4_RFD_RT_LASTUPDT_TIME_OBJECT       (41)
#define BGP4_RFD_RT_STATE_OBJECT               (42)
#define BGP4_RFD_RT_STATUS_OBJECT              (43)
/* Definitions used for Rfd Peer Damp History Tab4e */
#define BGP4_RFD_PEER_FOM_OBJECT               (44)
#define BGP4_RFD_PEER_LASTUPDT_TIME_OBJECT     (45)
#define BGP4_RFD_PEER_STATE_OBJECT             (46)
#define BGP4_RFD_PEER_STATUS_OBJECT            (47)
/* Definitions used for Rfd Routes Damp History Table */
#define BGP4_RFD_REUSE_RT_FOM_OBJECT           (48)
#define BGP4_RFD_REUSE_RT_LASTUPDT_TIME_OBJECT (49)
#define BGP4_RFD_REUSE_RT_STATE_OBJECT         (50)
#define BGP4_RFD_REUSE_RT_STATUS_OBJECT        (51)
/* Definitions used for Rfd Routes Damp History Table */
#define BGP4_RFD_REUSE_PEER_FOM_OBJECT         (52)
#define BGP4_RFD_REUSE_PEER_LASTUPDT_TIME_OBJECT  (53)
#define BGP4_RFD_REUSE_PEER_STATE_OBJECT       (54)
#define BGP4_RFD_REUSE_PEER_STATUS_OBJECT      (55)

#define BGP4_PEER_ROUTE_MAP_IN_CONF_OBJECT     (56)
#define BGP4_PEER_ROUTE_MAP_OUT_CONF_OBJECT    (57)
#define BGP4_ALL_PEER_GRP_PROP_CONF            (58)
#define BGP4_PEER_IP_PREFIX_LIST_IN_CONF_OBJECT (73)
#define BGP4_PEER_IP_PREFIX_LIST_OUT_CONF_OBJECT (74)
#define BGP4_PEER_EXT_ACTIVATE_ORF_CAP_OBJECT   (75)
#define BGP4_PEER_EXT_DEACTIVATE_ORF_CAP_OBJECT (76)
#define BGP4_PEER_EXT_LOCAL_AS_OBJECT           (78)
#define BGP4_PEER_EXT_LOCAL_AS_CONF_OBJECT      (79)
#define BGP4_NETWORK_ROUTE_ADDR_TYPE (81)
#define BGP4_NETWORK_ROUTE_ADDR_LEN  (82)
#define BGP4_NETWORK_ROUTE_ROWSTATUS (83)
#define BGP4_PEER_BFD_STATUS_CONF_OBJECT (84)

#define BGP4_FIB_UPD_LIST_INDEX                 (1)
#define BGP4_INT_PEER_LIST_INDEX                (2)
#define BGP4_EXT_PEER_LIST_INDEX                (3)
#define BGP4_NEXTHOP_LIST_INDEX                 (4)
#define BGP4_PROTOCOL_LIST_INDEX                (5)
#ifdef L3VPN
#define BGP4_VPN4_FEAS_WDRAW_LIST_INDEX         (6)
#endif

/* Index for Throttling Time storage. */
#define BGP4_PREV_ENTRY_TIME_INDEX              (0)
#define BGP4_PREV_EXIT_TIME_INDEX               (1)
#define BGP4_CUR_ENTRY_TIME_INDEX               (2)
#define BGP4_CUR_EXIT_TIME_INDEX                (3)

#define BGP4_DEF_ROUTE_ORIG_ENABLE              (1)
#define BGP4_DEF_ROUTE_ORIG_DISABLE             (2)

#define BGP4_WAIT_FOR_RESP                       (1)
#define BGP4_DONT_WAIT_FOR_RESP                  (2)

#define  BGP4_DFLT_VRFID                         0

#ifdef L3VPN
#define  L3VPN_DEF_VRF_ID    0
#else
#define  BGP4_INVALID_VRF_ID                     -1
#endif

#define  BGP4_IPV4_FAMLY_MODE      "af4"
#define  BGP4_IPV6_FAMLY_MODE      "af6"
#define  BGP4_IPV4_VPNV4_FAMLY_MODE  "afvpnv4"
#define  BGP4_IPV4_VRF_FAMLY_MODE    "bgpaf-"
#define  BGP4_ROOT_MODE    "bgp"
#define  BGP4_IPV4_VRF_FAMLY_MODE_LEN  6
#ifdef VPLSADS_WANTED
#define BGP4_L2VPN_VPLS_FAMLY_MODE    "afl2vpn"
#endif
#ifdef EVPN_WANTED
#define BGP4_EVPN_FAMLY_MODE    "afevpn"
#endif
/* ORF types */
#define BGP_ORF_TYPE_ADDRESS_PREFIX      64

#define CAP_CODE_ORF                    3
#define BGP_ORF_CAPS_LEN                 7
#define BGP_CAP_ORF_MODE_OFFSET          6

/* Macro to inform ORF capability for the given AFI,SAFI, 
 * ORF Type exists but with different ORF mode*/ 
#define BGP_ORF_MODE_DIFFER              3

/* These macros used where assignment of hard-coded values is disallowed
 * and any other name hinders readability */
#define ONE      1
#define TWO      2
#define THREE    3 
#define FOUR     4 
#define FIVE     5
#define SIX      6 
#define SEVEN    7 
#define EIGHT    8 
#define NINE     9 
#define TEN      10 
#define ELEVEN   11 
#define TWELVE   12
#define THIRTEEN 13
#define FOURTEEN 14
#define FIFTEEN  15

#define BGP_MAX_PEER_GROUP_NAME   20

#define  BGP4_PERVRF_LABEL_ALLOC_POLICY          0x01
#define  BGP4_PERROUTE_LABEL_ALLOC_POLICY        0x02

#define MAX_CODE_NAME 30
#define BGP4_SUP_CAP_CODE_NAME 0x01
#define BGP4_PATH_ATTR_CODE_NAME 0x02
#define BGP4_OVERLAP_POLICY_CODE_NAME 0x03
#define BGP4_ORIGIN_CODE_NAME 0x04
#define BGP4_RETURN_STATUS 0x05

#endif /* BGP4DFS_H */
