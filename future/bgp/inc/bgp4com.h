/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved    
 *
 * $Id: bgp4com.h,v 1.28 2015/07/06 02:27:49 siva Exp $
 *
 * Description:This file includes all the BGP based header files.          
 *
 *******************************************************************/
#ifndef BGP4COM_H
#define BGP4COM_H

#include "lr.h"
#include "ip.h"
#include "iss.h"
#include "cli.h"
#include "csr.h"
#include "vcm.h"
#include "ospf.h"
#ifdef ISIS_WANTED
#include "isis.h"
#endif
#include "dbutil.h"
#include "fssyslog.h"
#include "bgp.h"
#include "snmctdfs.h"
#include "rmap.h"
#ifdef MPLS_WANTED
#include "mpls.h"
#endif
#include <math.h>

#ifndef LINUX_SLI_WANTED
/* For using Future stack IP/SLI/TCP */

#include "bgfsipf4.h"
#ifdef BGP4_IPV6_WANTED
#include "bgfsipf6.h"
#endif
#include "bgp4port.h"
#include "bgpfstcp.h"
/* For using Vxworks stack IP/SLI/TCP 
 * #include "bgvxport.h"
 * #include "bgvxipf4.h"
 * #ifdef BGP4_IPV6_WANTED
 * #include "bgvxipf6.h"
 * #endif
 * #include "bgp4port.h"
 * #include "bgpvxtcp.h"
 */

/* For using AMTEC stack IP/SLI/TCP 
 * #include "bgamport.h"
 */

#else

/* For using Linux stack IP/SLI/TCP  */
#include "bglnipf4.h"
#ifdef BGP4_IPV6_WANTED
#include "bglnipf6.h"
#endif
#include "bgp4port.h"
#include "bgplntcp.h"

#endif
 


#include "bgp4mac.h"
#include "bgp4task.h"
#include "bgp4semh.h"
#include "bgrfl.h"
#include "bgrfd.h"
#include "bgcm.h"
#include "bgecm.h"
#include "bgcap.h"
#include "bgconfed.h"
#ifdef L3VPN
#include "bgvpndfs.h"
#endif

#ifdef EVPN_WANTED
#include "bgevpndfs.h"
#endif

#ifdef VPLSADS_WANTED
#include "bgp4vpls.h"
#endif

#include "bgrtref.h"
#include "bgp4rts.h"
#include "bgmpdfs.h"
#include "bgpgr.h"

#include "trie.h"
#include "triedll.h"
#include "trielib.h"
#include "bgp4trie.h"
#include "redblack.h"

#ifdef BGP4_IPV6_WANTED
#include "bgip6dfs.h"
#include "ip6util.h"
#endif

#include "snmcdefn.h"
#include "bgp4var.h"
#include "bgp4dbug.h"
#include "cfa.h"

#ifndef LINUX_SLI_WANTED
#ifdef L3VPN_PORT
#include "lblmgrex.h"
#include "mplsdefs.h"
#include "mpls.h"
#endif
#endif

#include <assert.h>
#include "bgpsz.h"
#include "snmputil.h"
#include "bgp4cli.h"
#include "stdbglow.h"
#include "fsbgplow.h"
#include "fsmpbglw.h"
#include "fsmpbgwr.h"
#include "bgp4red.h"

#ifdef ISS_WANTED
#include "msr.h"
#endif

#ifdef EVPN_WANTED
#include "fsvxlan.h"
#endif

#endif /* BGP4COM_H */
