/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved    
 *
 * $Id: bgp4rib.h,v 1.41 2016/12/27 12:35:53 siva Exp $
 *
 * Description:Contains the data structures and definitions used                 *             by the RIB Handler  and the Advertisement Handler.  
 *
 *******************************************************************/
#ifndef BGP4RIB_H

#define  BGP4RIB_H
typedef struct t_Aggregator
{
    UINT4               u4IPAddr;
    UINT4               u4ASNo;
    UINT1               u1Flag;
    UINT1               u1Pad[3];
}
tAggregator;

typedef struct t_Community
{
    UINT1              *pu1CommVal;         /* Pointer to the list of 
                                             * Community Attributes. */
    UINT2               u2CommCnt;          /* Total number of Community
                                             * attribute present. Each
                                             * attribute is 4 byte long. */
    UINT1               u1Flag;             /* Comm Attribute Flag. */
    UINT1               u1AddFlag;           
}tCommunity;

typedef struct t_ExtCommunityCost  /*Extended community Cost Attribute structure*/
{
    UINT4 u4CostValue;   /*Cost value used for Best route selection process */
    UINT2 u2PointofIsertion;  /* Specifies the point of insertion for usage of
                                 extended cost community Attribute. Currently 
                                 the POI is supported only for IGP cost in best 
                                 route selection process.*/
    UINT2 u2CostCommunity; /* Community Id used for Best route selection process,
                              when the u4CostValue  is same for the routes,
                              CommId is used in the selection process.  
                              Route with lower CommId is preferred. */
}tExtCommCost;

typedef struct t_ExtCommunity
{
    UINT1           *pu1EcommVal;        /* Pointer to the list of 
                                              * Ext-Community Attributes. */
    tExtCommCost    ExtCommCost;         /* Instance to the Cost Community
                                              * Attribute */
    UINT2               u2EcommCnt;         /* Total number of Ext-Community
                                             * attribute present. Each
                                             * attribute is 8 byte long. */
#ifdef L3VPN
    UINT1               u1RTEcommFlag;      /* Flag indicates whether
                                             * Route-Target community is 
                                             * present or not? */
    UINT1               au1Pad[3];
#endif
    UINT1               u1Flag;             /* Ext-Comm Attribute Flag. */
    UINT1               u1CostFlag;         /* Cost Comm Flag */
}tExtCommunity;

typedef struct t_ClusterList
{
    UINT1              *pu1ClusId;          /* Pointer to the list of 
                                             * Cluster-id Attributes. */
    UINT2               u2ClusCnt;          /* Total number of Cluster-id
                                             * present. Each Cluster-id
                                             * attribute is 4 byte long. */
    UINT1               u1Flag;             /* Cluster-id Attribute Flag. */
    UINT1               u1Pad;              /* For 4-byte alignment */
}tClusterList;

typedef struct t_UnknownInfo
{
    #define MAX_UNKNOWN   1000 /* Maximum array size of the unknown
                                * attribute. */
    UINT2       u2UnknownInfoCount; /* Count of unknown information */
    UINT1       au1UnknownInfo[MAX_UNKNOWN]; /* Array of the unknown
                                              * information used for 
                                              * local processing 
                                              */
    UINT1       au1Padding[2]; /* for 4 byte allignment */
}tUnknownInfo;

typedef struct t_Bgp4Info
{
    tTMO_SLL            TSASPath;
    tAggregator        *pAggr;
    tTMO_SLL            SnpaInfoList;                                         
    struct t_Bgp4RcvdPathAttr   *pRcvdPA;  /* Pointer to the Path Attribute
                                            * data base to which this info is
                                            * linked. */
    tCommunity         *pCommunity;     /* Points to the Community Attribute */
    tExtCommunity      *pExtCommunity;  /* Points to the Extended-Community
                                         * Attribute */
    tClusterList       *pClusterList;   /* Points to the Cluster-Id list
                                         * Attribute */
    UINT1              *pu1UnknownAttr;
    tAddrPrefix         NextHopInfo;
#ifdef BGP4_IPV6_WANTED
    tAddrPrefix         LinkLocalAddrInfo;
#endif
    UINT4               u4UnknownAttrLen;
    UINT4               u4OrigId;      /* Originator Identifier */
    UINT4               u4RecdMED;
    UINT4               u4RecdLocalPref;
    UINT4               u4Ref;
    UINT4               u4SendMED;    /* MED value to be advertised to
                                         peers */
    UINT4               u4AttrFlag;   /*
                                       * This flag indicates the presence of a 
                                       * particular attribute.
                                       * BIT   Attribute.
                                       *  0     Origin
                                       *  1     AS_PATH
                                       *  2     NEXT_HOP
                                       *  3     MED
                                       *  4     LOCAL_PREF
                                       *  5     ATOMIC_AGGREGATE
                                       *  6     AGGREGATOR.
                                       *  7     UNKNOWN
                                       */
    UINT2               u2Weight;      /* Prefer the path with the highest WEIGHT.
                                        * It is local to the router on which it is configured */
    UINT1               u1BgpInfoSafi;/* Represents the subsequent address
                                       * family identifier.
                                       * Default = 0x01 (Unicast) */
    UINT1               u1Origin;
    UINT1               u1IsRmapSet;
#ifdef BGP4_IPV6_WANTED 
    UINT1               u1LinkLocalPresent;
    UINT1               au1Pad[2];
#else
    UINT1               au1Pad[3];
#endif
}
tBgp4Info;

typedef struct t_RouteProfile
{
    struct t_RouteProfile *p_RPNext; /*
                                      * Used for Linking the Profiles Together
                                      */
    struct t_RouteProfile *pChgListPrev;   /* Pointer to the Route location in
                                            * the change route list
                                            * (FIB/INT-Peer/Ext-Peer List).
                                            */
    struct t_RouteProfile *pChgListNext;   /* Pointer to the Route location in
                                            * the change route list
                                            * (FIB/INT-Peer/Ext-Peer List).
                                            */
    struct t_RouteProfile *pNextHopPrev;   /* Pointer to the Route location in
                                            * the list of Routes with identical
                                            * next hop.
                                            */
    struct t_RouteProfile *pNextHopNext;   /* Pointer to the Route location in
                                            * the list of Routes with identical
                                            * next hop.
                                            */
    struct t_RouteProfile *pIdentAttrPrev; /* Pointer to the Route location in
                                            * the list of Routes with identical
                                            * incoming Attributes.
                                            */
    struct t_RouteProfile *pIdentAttrNext; /* Pointer to the Route location in
                                            * the list of Routes with identical
                                            * incoming Attribute.
                                            */
    struct t_RouteProfile *pProtoRtPrev;   /* Pointer to the Route location in
                                            * the list of Routes from the same
                                            * protocol. In case of route
                                            * learnt from BGP Peer, then this
                                            * list consists of routes from the
                                            * same peer.
                                            */
    struct t_RouteProfile *pProtoRtNext;   /* Pointer to the Route location in
                                            * the list of Routes from the same
                                            * protocol. In case of route
                                            * learnt from BGP Peer, then this
                                            * list consists of routes from the
                                            * same peer.
                                            */
   struct t_RouteProfile *pMultiPathRtNext; /* Pointer to Multipath route list 
                                             */
#ifdef L3VPN
    struct t_RouteProfile *pVpnv4RtPrev;   /* Pointer to the Route location in
                                            * the list of Vpnv4 routes for 
                                            * processing.
                                            */
    struct t_RouteProfile *pVpnv4RtNext;   /* Pointer to the Route location in
                                            * the list of Vpnv4 routes for 
                                            * processing.
                                            */
#endif
    tBgp4PeerEntry      *pPEPeer;          /* Pointer to the Peer Entry from
                                            * which the route was learned.
                                            */
    struct t_BgpCxtNode  *pBgpCxtNode ;     /* Pointer to Context node */
    tBgp4Info           *pRtInfo;    /*
                                      * Pointer to the other information 
                                      * corresponding to the route. The 
                                      * content of this depends on the
                                      * input attribute received along with
                                      * the route.
                                      */
    tBgp4Info          *pOutRtInfo;  /* Pointer to the Out Info which is the 
                                      * result of neighbor routemap */
#ifdef L3VPN
    tTMO_SLL           InstalledVrfList; /* List of VRFs in which this
                                             * route is installed */
#endif
#ifdef RFD_WANTED
    struct t_RtDampHist *pRtDampHist;/* Pointer to the Damping history for
                                      * this route. */
#endif
    tNetAddress         NetAddress;  /* The net address for which the profile
                                      * is applicable. Contains <AFI, SAFI>,
                                      * IP prefix and prefix length
                                      */
    tAddrPrefix         ImmediateNextHopInfo;
#ifdef L3VPN
    UINT4               u4Label;
                                /* Padding must be done properly, since a label
                                 * is of 3bytes only */
    UINT4               u4IgpLabelId;

    UINT4                u4ExtListCxtId;
                                    /*Lsp Id of Igp Label*/
    UINT1               au1RouteDisting[BGP4_VPN4_ROUTE_DISTING_SIZE];
                                     /* Represents the route-distinguisher
                                      * associated with this route
                                      */
#endif
#ifdef EVPN_WANTED
    UINT4               u4VrfId; /* Unique VRF ID */
    tEvpnNlriInfo       EvpnNlriInfo;
#endif

    UINT4               u4Flags;     /* 
                                      * This flag is used to store some 
                                      * characteristic
                                      * Present in INPUT_RIB
                                      * Present in LOCAL_RIB
                                      * FILTERED route.
                                      * UNKNOWN_NEXTHOP route.
                                      */
    UINT4               u4ExtFlags;  /* Extended flag for Mulitpath */

    UINT4               u4MED;       /* Calculated MED value associated with
                                      * the route. */
    UINT4               u4NewMED;     /* New MED value associated with
                                      * the route. */
    UINT4               u4MedChange ; /* Flag to indicate med
                                      * change occured for connected
                                      * routes */
    UINT4               u4StaticMedChange ; /* Flag to indicate med
                                      * change occured for static
                                      * routes */

    UINT4               u4LocalPref; /* Calculated Local Preference assocaiated
                                      * with the route.
                                      */
    UINT4               u4RtIfIndx;  /* The interface-index associated with the 
                                      * next-hop for the route
                                      */
    INT4                i4NextHopMetric; /* The metric associated with the 
                                          * immediate next-hop entry.
                                          */
#ifdef VPLSADS_WANTED
#ifdef MPLS_WANTED
   tVplsSpecInfo         *pVplsSpecInfo;
#endif
   tVplsNlriInfo         VplsNlriInfo; 
#endif 

    UINT1               u1Ref;       /* 
                                      * Count indicating how many persons are 
                                      * refering to this profile
                                      */
    UINT1               u1Protocol;  /* 
                                      * The Protocol through which the route 
                                      * has been learned.
                                      */
    UINT1               au1Pad2[2];
#ifdef L3VPN
    UINT1               u1LabelCnt;
                                 /* Represents how many labels are present */
    UINT1               u1HwStatus;/*Represents route programmed in hardware*/
    UINT1               u1IsAddedInPADB;
    UINT1               u1IsVpnLeakedRoute;
#endif
    UINT1               u1MetricType; /* Represents type of ospf route, i.e 
                                         Internal/External/NSSA External Route */
   UINT1                u1AdvFlag; /* to represent if route is via netwrok command or
                                              redistributed */
   UINT1               au1Pad1[2];
}
tRouteProfile;

typedef struct t_AdvRoute
{
     tRBNodeEmbd         RBNode;
     tRouteProfile       *pRouteProfile;
}
tAdvRoute;


typedef struct t_LinkNode
{
    tTMO_SLL_NODE       TSNext;
    tRouteProfile      *pRouteProfile;
    tBgp4Info          *pRtInfo;
}
tLinkNode;

typedef struct t_ExtAdvRtLinkNode
{
    tTMO_SLL_NODE       TSNext;
    tRouteProfile      *pRouteProfile;
    tBgp4Info          *pRtInfo;
    UINT4               u4UpdSentTime; /* Time at which route was last advt. */
}
tAdvRtLinkNode;

/* Structure for Storing the Identical Path Attributes. */
typedef struct t_Bgp4RcvdPathAttr
{
    tTMO_HASH_NODE           NextNonIdentPA;    /* The next non-identical
                                                 * PA stored in this
                                                 * bucket. */
    tBGP4_DLL                BestRouteList;     /* DLL of Best routes with
                                                 * this path attribute. */
    tBGP4_DLL                NonBestRouteList;  /* DLL of Non-Best routes
                                                 * with this path attribute. */
    tBgp4Info               *pBgp4RcvdInfo;     /* The received BGP Info
                                                 * associated with this
                                                 * PA entry. */
}
tBgp4RcvdPathAttr;

/* Structure for Storing the routes based on identical outbound info for
 * advertising to the peer. */
typedef struct t_Bgp4AdvtPathAttr
{
    tTMO_HASH_NODE           NextNonIdentPA;    /* The next non-identical
                                                 * PA stored in this
                                                 * bucket. */
    tTMO_SLL                 RouteList;         /* List of routes with
                                                 * this path attribute.
                                                 */
    tBgp4Info               *pBgp4AdvtInfo;     /* The outbound BGP Info
                                                 * associated with this
                                                 * advertise entry. 
                                                 */
    tBgp4PeerEntry          *pAdvtPeer;         /* Peer to which these
                                                 * routes are to be 
                                                 * advertised. */
    UINT2                    u2AFI;             /* Address family to
                                                 * which the route in
                                                 * this entry belong.
                                                 */
    UINT2                    u2SAFI;            /* Sub-Address family to
                                                 * which the route in
                                                 * this entry belong.
                                                 */
    UINT2                    u2PASize;          /* The actual size this
                                                 * PA required to fill the
                                                 * Updated message. */
    UINT2                    u2Pad;             /* For 4-byte alignment. */
}
tBgp4AdvtPathAttr;

typedef struct t_IgpMetricNode {
    tTMO_SLL_NODE   NextIgpMetricEntry;   /* The next Igp Metric entry */
    tBGP4_DLL       NextHopIdentRoute;    /* List holding all the routes
                                           * that are having this next hop
                                           */
    tAddrPrefix     LinkNexthop;        /* Link Nexthop address */
    tAddrPrefix     ImmediateNextHop;   /* Immediate Next hop address for
                                           * the Next hop.
                                           */
    UINT4           u4VrfId;            /* Specifies in which IP VRF, the
                                         * lookup should be done.
                                         */
#ifdef L3VPN
    UINT4           u4IgpLspId;          /*Lsp Identifier of Igp label*/
#endif
    UINT4           u4IfIndex;            /* Interface index to reach the 
                                           * immediate next hop.
                                           */
    INT4            i4IgpMetric;          /* Represents the value assigned to
                                           * u4LinkNextHopAddress
                                           */
    UINT1           u1Action;             /* This variable controls the action
                                           * to be performed.
                                           */
    UINT1           u1IsResolved;         /* This flag indicates the current
                                           * status for this next hop.
                                           * BGP4_TRUE if resolvable, else
                                           * BGP4_FALSE.
                                           */
#ifdef L3VPN
    UINT1           u1LspStatus;          /*Igp Label status*/
    UINT1           u1LspCheck;
#else
    UINT1           au1Pad[2];            /* For 4-byte alignment */
#endif
} tBgp4IgpMetricEntry;

/* tBgp4IfInfo is used to store the information about an interface. */
typedef struct _tBgp4IfInfo {
    tTMO_SLL_NODE   NextIfNode; /* pointer to the next Interface node */
    UINT4           u4IfIndex; /* Interface index */
    UINT4           u4IfAddr; 
    UINT4           u4VrfId; /* VRF identifier associated with this interface.
                              * If no VRF is associated, then default VRF
                              * identifier will be present
                              */
    UINT4           u4IfType; /* Type of the interface */
#ifdef L3VPN
    UINT4           u4Label; /* Label associated with
                              * VRF interface */
    UINT2           u2LabelPeers; /*No Of Label Peers using (PE Peers and 
                                    Carrying Label Peers )*/
#endif
    UINT1           u1OperStatus; /* Operational status of the interface */
#ifdef L3VPN
    UINT1           u1VpnClassification; /* Denotes whether link participates
                                          * in carrier's carrier or enterprise
                                          * or interprovider scenario.
                                          */
#else
    UINT1           au1Pad[3]; /* For four byte alignment */
#endif
} tBgp4IfInfo;


/* NOTE:: UpdateFilterEntry should be a subset of the
 * the MEDLPEntry
 */

typedef struct t_MEDLPEntry
{
    tBgp4VrfName        VrfName;
    UINT4               u4VrfId;
    tNetAddress         InetAddress;
    UINT4               au4InterAS[BGP4_MAX_INT_AS + 1];
    UINT4               u4ASNo;
    UINT1               u1AdminStatus;
    UINT1               u1Dir;
    UINT1               u1Applicability;
    UINT1               au1RowActive;
    UINT4               u4MEDLP;  
}
tBgp4MedlpEntry;

typedef struct t_UpdateFilterEntry
{
    tBgp4VrfName        VrfName;
    UINT4               u4VrfId;
    tNetAddress         UpdtInetAddress;
    UINT4               au4InterAS[BGP4_MAX_INT_AS + 1];
    UINT4               u4ASNo;
    UINT1               u1AdminStatus;
    UINT1               u1Dir;
    UINT1               u1Action;
    UINT1               au1RowActive;
}
tBgp4UpdatefilterEntry;

typedef struct t_AggrEntry
{
    tTMO_SLL            TSRouteProfile;
    tRouteProfile      *pAggregatedRoute;
    tNetAddress         AggrInetAddress;
    tNetAddress         AggrInitAddr;     /* Variable holding the prefix
                                           * info used while creating an
                                           * aggregate entry. */
    tBgp4VrfName        VrfName;
    UINT4               u4VrfId;
    UINT1               au1SuppressMapName[RMAP_MAX_NAME_LEN + 4]; /*Holds the
                                                                   suppress route
                                                                   map name*/
    UINT1               au1AdvMapName[RMAP_MAX_NAME_LEN + 4]; /* Holds the
                                                                 Advertise route
                                                                 map name*/
    UINT1               au1AttMapName[RMAP_MAX_NAME_LEN + 4]; /* Hold the
                                                                 Attribute Route
                                                                 map name*/

    UINT1               u1AdminStatus;
    UINT1               u1AdvType;
    UINT1               u1AggrStatus;   /* Flag indicating the operation to
                                         * be performed on this aggregate
                                         * table entry such as reaggregation
                                         * or deletion. */
    UINT1               u1AsSet;        /* Variable indicating whether to 
                                         * enable/disable the AS-SET option
                                         * while aggregation of routes */
    UINT1               u1AsAtomicAggr; /* Variable indicating whether to set
                                         * the ATOMIC-AGGREGATE in the 
                                         * update message */
    UINT1               u1SuppressMapLen;
    UINT1               u1AdvMapLen;
    UINT1               u1AttMapLen;

}
tBgp4AggrEntry;

 typedef struct t_Bgp4AggrDetails {
    tRouteProfile      *pAggregatedRoute;
    tBgp4Info           *pBgpInfo;
 }tBgp4AggrDetails;



/* Bit settings in the u4Flags of tRouteProfile used to identify the
     type of the route */

#define  BGP4_RT_DONT_ADVT         0x00000001
#define  BGP4_RT_AGGREGATED        0x00000002
#define  BGP4_RT_AGGR_ROUTE        0x00000004
#define  BGP4_RT_NEXT_HOP_UNKNOWN  0x00000008
/* If a new route rxed from a peer is selected as the best route, and previously
some other route was selected as the best one, then this route needs to be 
advertised as withdrawn (WIHTDRAWN) to the sender of the currently selected 
best route. For other peers it will be advertised as replacement route.*/
#define  BGP4_RT_ADVT_WITHDRAWN       0x00000010
#define  BGP4_RT_ADVT_NOTTO_INTERNAL  0x00000020
#define  BGP4_RT_ADVT_NOTTO_EXTERNAL  0x00000040
/* This flag is used to indicate to the Advt. Handler that the route to 
   be advertised is a replacement route, by the RIB Handler.*/
#define  BGP4_RT_REPLACEMENT          0x00000080
/* To indicate that the route is unfeasible route */
#define  BGP4_RT_WITHDRAWN            0x00000100

/* To indicate that the route should not be processed by the 
 * RFD module. */
#define  BGP4_RT_NO_RFD                0x00000200

#define  BGP4_RT_NEWUPDATE_TOIP        0x00000400

#define  BGP4_RT_AGGR_RESTRICTED       0x00000800

/* Flag used to determine if route is the best route */
#define  BGP4_RT_BEST                  0x00001000

/* Flag used to indicate, that the route is not added to the RIB Tree because of
 * the configured Policy - INSTALL Less Specific route or More Specific route */
#define  BGP4_RT_OVERLAP               0x00002000

/* Flag indicates, that these routes need only to be advertised and not
 * added to the RIB */
#define  BGP4_RT_ONLY_ADVT             0x00004000

/* Flag, indicates that the route has been filtered because of input filter
 * policy and placed in the input list of the peer from which the route is
 * received. */
#define  BGP4_RT_FILTERED_INPUT        0x00008000

/* Flag, indicates that the route has been filtered because of output filter
 * policy */
#define  BGP4_RT_FILTERED_OUTPUT       0x00010000

/* Flag indicates that the route has been suppressed or not by RFD */
#define  BGP4_RT_DAMPED                0x00020000

/* Flag indicates that the route has been withdrawn but is present in
 * RIB as RFD need to maintain the HISTORY about the route */
#define  BGP4_RT_HISTORY               0x00040000

/* Flag indicates that the route is present in the FIB Update List. */
#define  BGP4_RT_IN_FIB_UPD_LIST       0x00080000

/* Flag indicates that the route is present in the Internal Peers Advt List */
#define  BGP4_RT_IN_INT_PEER_ADVT_LIST 0x00100000

/* Flag indicates that the route is present in the External Peers Advt List */
#define  BGP4_RT_IN_EXT_PEER_ADVT_LIST 0x00200000

/* Flag indicates that the route is present in the Peers DeInit List */
#define  BGP4_RT_IN_PEER_DEINIT_LIST   0x00400000

/* Flag indicates that the route are to be advertised as withdrawn routes
 * to the peers because of the aggregation policy. */
#define  BGP4_RT_AGGR_WITHDRAWN        0x00800000

/* Flag indicates that the route is to be processed because of the change
 * in the next hop reachability/metric/immediate next hop changes. */
#define  BGP4_RT_NH_METRIC_CHANGE      0x01000000

/* Flag indicates that the route is present in the Redistribution list */
#define  BGP4_RT_IN_REDIST_LIST        0x02000000

/*Flag indicating this route in rib selected as Multipath route */
#define BGP4_RT_MULTIPATH               0x00000001

/*Flag indicating this route is present in RTM */
#define BGP4_RT_IN_RTM              0x00000002
#define  BGP4_RT_AS_PATHSET_TAG     0x00000008
/*Flag indicating this route is IGP route for Sync up -BGP External flag */
#define  BGP4_ROUTE_WITHDRAWN       0x00000004
/*Flag indicating this route is update route and not delete route for Sync up -BGP */
#define  BGP4_ROUTE_BEST_ROUTE_UPDATE       0x00000008

/* Re-use timer is not run in the standby node. So damp flag is not 
 * synced to standby. When the re-use timer is synced, damped flag 
 * need to be synced */
#define BGP4_RT_SYNC_FLAG          (0x000000ff | BGP4_RT_WITHDRAWN |\
                                    BGP4_RT_BEST | BGP4_RT_OVERLAP |\
                                    BGP4_RT_FILTERED_INPUT | BGP4_RT_FILTERED_OUTPUT |\
                                    BGP4_RT_HISTORY)

/* Flags which indicate whether route has been received through
 * MP_REACH_NLRI or MP_UNREACH_NLRI path attribute */
#define  BGP4_MPE_RT_ADVT_MP_REACH     0x04000000
#define  BGP4_MPE_RT_ADVT_MP_UNREACH   0x08000000
/* This macro is used to  determine  stale routes.*/ 
#define BGP4_RT_STALE                           0x10000000 

/* This macro is used to determine the stale routes to be deleted
 * for a peer for a particular address family */
#define BGP4_RT_DEL_STALE                       0x20000000

#ifdef L3VPN
/* Flag indicates that the route is present in the Vpnv4 FeasWithdrawn List */
/*Route Present in Vpnv4 global list*/
#define  BGP4_RT_IN_VPNV4_FEAS_WDRAW_LIST       0x40000000
/*Route received as ipv4 and converted to Vpnv4*/
#define  BGP4_RT_CONVERTED_TO_VPNV4             0x80000000
/*The flags indicates for which vrf the route is processed !!!!! */
#define  BGP4_RT_PROCESS_RIB                    0x00000000
#endif

#define BGP4_RT_INVALID_ROUTE_FLAG  \
            (BGP4_RT_NEXT_HOP_UNKNOWN | BGP4_RT_DAMPED | \
             BGP4_RT_OVERLAP | BGP4_RT_FILTERED_INPUT)

/* Flags used by the IGP route Importation Module  */
#define  BGP4_IMPORT_RT_ADD      0x00000001
#define  BGP4_IMPORT_RT_DELETE   0x00000002

#define BGP4_NETWORK_ROUTE_ADD    0x00000001
#define BGP4_NETWORK_ROUTE_DELETE 0x00000002
/* Flags used in tree search */
/* Find resulting in Exact match  */
#define  BGP4_TREE_FIND_EXACT     0x0001
/* Find resulting in a match, if for a given route, an overlapping
 * route is found in the RIB
 */  
#define  BGP4_TREE_FIND           0x0002 

#define BGP4_NETWORK_ADV_ROUTE  0x01
#define BGP4_REDIST_ADV_ROUTE  0x02

#endif /* BGP4RIB_H */
