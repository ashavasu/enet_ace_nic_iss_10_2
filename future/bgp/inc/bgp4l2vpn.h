/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved             *
 *                                                                  *
 * $Id: bgp4l2vpn.h,v 1.4 2015/08/12 10:16:03 siva Exp $        *
 *                                                                  *
 * Description:Contains the data structures and definitions         *
 *             used to interface BGP4 with L2VPN                    *
 *                                                                  *
 ********************************************************************/
#ifndef BGP4L2VPN_H
#define BGP4L2VPN_H


/* Definitions */
#define VPLS_ADVT_UNICAST           0x1
#define MAX_LEN_ADDR_VALUE          4
#define ECOMM_VALUE_LAYER2_INFO     0x800A
#define ECOMM_LAYER2_INFO_ENCAPS_TYPE 19
#define ECOMM_LAYER2_SEQUENCE_DELIVERY 1
#define ECOM_CONTROL_WORD_ENABLE     0x2   /* 0000 0010*/
#define ECOM_L2VPN_CW_ENABLED              1
#define ECON_L2VPN_CW_DISABLED             2

#define BGP4_RESERVE_FIELD_VALUE    0x0000
#define L2VPN_EXT_COMM_COUNT        2 
#define BGP4_VPLS_NLRI_PREFIX_LEN   17
#define BGP4_VPLS_NLRI_PREFIX_FIELD_LEN 2
#define BGP4_VPLS_NLRI_VEID_LEN     2
#define BGP4_VPLS_NLRI_VBO_LEN      2
#define BGP4_VPLS_NLRI_VBS_LEN      2
#define BGP4_VPLS_NLRI_LB_LEN       3

#define BGP4_VPLS_NLRI_LEN          (BGP4_VPLS_NLRI_PREFIX_LEN + BGP4_VPLS_NLRI_PREFIX_FIELD_LEN)

#define BGP4_VPLS_MAX_KEY_SIZE (MAX_LEN_RD_VALUE + BGP4_VPLS_NLRI_VEID_LEN + BGP4_VPLS_NLRI_VBO_LEN)

#define BGP4_VPLS_FEASIBLE_ROUTE_FLAG   0x01  
#define BGP4_VPLS_WITHDRAW_ROUTE_FLAG   0x00

#define BGP4_VPLS_UNICAST_ADVT          0x01
#define BGP4_VPLS_BROADCAST_ADVT        0x00

#define BGP4_VPLS_L2ECOM_TYPE_SIZE          2
#define BGP4_VPLS_L2ECOM_ENCAP_TPYE_SIZE    1
#define BGP4_VPLS_L2ECOM_CTRL_FLAG_SIZE     1
#define BGP4_VPLS_L2ECOM_MTU_SIZE           2


#define BGP4_DFLT_VPLS_INDX             0


#define  BGP4_VPLS_RT_VPLS_SPEC_INFO(pRtProfile)  (pRtProfile->pVplsSpecInfo)
#define  BGP4_VPLS_RT_VPLS_SPEC_STATE(pVplsSpecInfo)  (pVplsSpecInfo->u4VplsState)

#define  BGP4_VPLS_RT_VPLS_SPEC_INDEX(pVplsSpecInfo)  (pVplsSpecInfo->u4VplsIndex)
  
#ifndef BGP4_IPV6_WANTED  /*ipv6 not wanted */

#ifdef L3VPN   /*  <ipv4, unicast> <ipv4, label>  <vpn4,uni> <l2vpn,vpls> */ 
#define BGP4_L2VPN_VPLS_INDEX              3
#else         /* ipv4, unicast> <l2vpn,vpls> */
#define  BGP4_L2VPN_VPLS_INDEX             1
#endif

#else   /*IPv6 WANTED */
#ifdef L3VPN   /*  <ipv4, unicast> <ipv4, label>  <ipv6,uni> <vpn4,uni> <l2vpn,vpls> */
#define BGP4_L2VPN_VPLS_INDEX              4
#else   /*  <ipv4, unicast> <ipv6,uni> <l2vpn,vpls>*/
#define BGP4_L2VPN_VPLS_INDEX              2
#endif
#endif  /*BGP4_IPV6_WANTED*/


#define BGP4_VPLS_RIB_TREE(u4CxtId)                \
                        (gBgpCxtNode[u4CxtId]->gapBgp4Rib[BGP4_L2VPN_VPLS_INDEX])

#define BGP4_PEER_VPLS_AFISAFI_INSTANCE(pPeerentry) \
                    ((pPeerentry)->apAsafiInstance[BGP4_L2VPN_VPLS_INDEX])



#define BGP4_PEER_VPLS_OUTPUT_ROUTES(pPeerentry) \
        (BGP4_PEER_VPLS_AFISAFI_INSTANCE(pPeerentry)->pOutputList)
#define BGP4_PEER_VPLS_NEW_ROUTES(pPeerentry)\
        (BGP4_PEER_VPLS_AFISAFI_INSTANCE(pPeerentry)->pNewList)
#define BGP4_PEER_VPLS_NEW_LOCAL_ROUTES(pPeerentry)\
        (BGP4_PEER_VPLS_AFISAFI_INSTANCE(pPeerentry)->pNewLocalList)
#define BGP4_PEER_VPLS_ADVT_WITH_ROUTES(pPeerentry)\
        (BGP4_PEER_VPLS_AFISAFI_INSTANCE(pPeerentry)->pAdvtWithList)
#define BGP4_PEER_VPLS_ADVT_FEAS_ROUTES(pPeerentry)\
        (BGP4_PEER_VPLS_AFISAFI_INSTANCE(pPeerentry)->pAdvtFeasList)
#define BGP4_PEER_VPLS_DEINIT_RT_LIST(pPeerentry)\
        (&(BGP4_PEER_VPLS_AFISAFI_INSTANCE(pPeerentry)->TSDeInitRtList))

#define BGP4_PEER_L2VPN_VPLS_AFISAFI_INSTANCE(pPeerentry) \
                    (pPeerentry->apAsafiInstance[BGP4_L2VPN_VPLS_INDEX])


/*Counters*/

#define BGP4_PEER_VPLS_PREFIX_RCVD_CNT(pPeerentry) \
        ((pPeerentry->apAsafiInstance[BGP4_L2VPN_VPLS_INDEX])->PrefixCounters).u4PrefixesRcvd
#define BGP4_PEER_VPLS_PREFIX_SENT_CNT(pPeerentry) \
        ((pPeerentry->apAsafiInstance[BGP4_L2VPN_VPLS_INDEX])->PrefixCounters).u4PrefixesSent
#define BGP4_PEER_VPLS_WITHDRAWS_RCVD_CNT(pPeerentry) \
        ((pPeerentry->apAsafiInstance[BGP4_L2VPN_VPLS_INDEX])->PrefixCounters).u4WithdrawsRcvd
#define BGP4_PEER_VPLS_WITHDRAWS_SENT_CNT(pPeerentry) \
        ((pPeerentry->apAsafiInstance[BGP4_L2VPN_VPLS_INDEX])->PrefixCounters).u4WithdrawsSent
#define BGP4_PEER_VPLS_IN_PREFIXES_CNT(pPeerentry) \
        ((pPeerentry->apAsafiInstance[BGP4_L2VPN_VPLS_INDEX])->PrefixCounters).u4InPrefixes
#define BGP4_PEER_VPLS_IN_PREFIXES_ACPTD_CNT(pPeerentry) \
        ((pPeerentry->apAsafiInstance[BGP4_L2VPN_VPLS_INDEX])->PrefixCounters).u4InPrefixesAcptd
#define BGP4_PEER_VPLS_IN_PREFIXES_RJCTD_CNT(pPeerentry) \
        ((pPeerentry->apAsafiInstance[BGP4_L2VPN_VPLS_INDEX])->PrefixCounters).u4InPrefixesRjctd
#define BGP4_PEER_VPLS_OUT_PREFIXES_CNT(pPeerentry) \
        ((pPeerentry->apAsafiInstance[BGP4_L2VPN_VPLS_INDEX])->PrefixCounters).u4OutPrefixes
#define BGP4_PEER_VPLS_INVALID_MPE_UPDATE_CNT(pPeerentry) \
        ((pPeerentry->apAsafiInstance[BGP4_L2VPN_VPLS_INDEX])->PrefixCounters).u4InvalidMpeAttribCntr

#define BGP4_VPLS_ROUTE_DISTING_SIZE         8
#define BGP4_VPLS_MAX_VPLS_NAME_SIZE         32
#define MAX_BGP_VPLS_INSTANCE                100
#define MAX_BGP_VPLS_RR_IMPORT_TARGETS_SET   100
#define BGP4_MAX_VPLS_HASH_TBL_SIZE          32
#define BGP4_VPLS_RR_TARGETS_DEL_INTERVAL    7200 /* 2 hours */

#define BGP4_BOTH_ROUTE_TARGET_TYPE         3
#define BGP4_ROUTE_TARGET_ADD               1
#define BGP4_ROUTE_TARGET_DEL               2

/* VPLS operational status. It tells the current state of a VPLS */
#define BGP4_VPLS_UP                     1
#define BGP4_VPLS_DOWN                   2
#define BGP4_VPLS_CREATE                 3
#define BGP4_VPLS_RT_ONLY                4

/* Macro definitions */
#define BGP4_VPLS_GLOBAL_PARAMS (gBgpNode.Bgp4VplsGlobalData)
#define BGP4_VPLS_SPEC_MEMPOOL_ID ((BGP4_VPLS_GLOBAL_PARAMS).VplsSpecInstancePoolId)
#define BGP4_VPLS_RR_TARGETS_SET_MEMPOOL_ID ((BGP4_VPLS_GLOBAL_PARAMS).RrImportTargetsSetMemPoolId)
#define BGP4_VPLS_RR_TARGETS_SET (&((BGP4_VPLS_GLOBAL_PARAMS).RrImportTargetsSet))
#define BGP4_VPLS_GLOBAL_INFO ((BGP4_VPLS_GLOBAL_PARAMS).pVplsGlobalInfo)
#define BGP4_VPLS_RR_TARGETS_INTERVAL ((BGP4_VPLS_GLOBAL_PARAMS).u4RrRTDelInterval)
#define BGP4_VPLS_SPEC_CNT ((BGP4_VPLS_GLOBAL_PARAMS).u4VplsSpecInstanceCnt)
#define BGP4_VPLS_RR_TARGETS_SET_CNT ((BGP4_VPLS_GLOBAL_PARAMS).u4RrImportTargetsSetCnt)

#define BGP4_VPLS_SPEC_IMPORT_TARGETS(pVplsSpec) (&(pVplsSpec->ImportTargets))
#define BGP4_VPLS_SPEC_EXPORT_TARGETS(pVplsSpec) (&(pVplsSpec->ExportTargets))
#define BGP4_VPLS_SPEC_VPLSNAME_VALUE(pVplsSpec) (pVplsSpec->au1VplsName)
#define BGP4_VPLS_SPEC_ROUTE_DISTING(pVplsSpec)  (pVplsSpec->au1VplsRD)
#define BGP4_VPLS_SPEC_VPLS_INDX(pVplsSpec)      (pVplsSpec->u4VplsIndex)
#define BGP4_VPLS_SPEC_CURRENT_STATE(pVplsSpec)  (pVplsSpec->u4VplsState)
#define BGP4_VPLS_SPEC_FLAG(pVplsSpec)           (pVplsSpec->u4Flags)
#define BGP4_VPLS_RR_IMPORT_TARGET_NEXT(pRrImportTargetInfo) \
                (pRrImportTargetInfo->NextTarget)
#define BGP4_VPLS_RR_IMPORT_TARGET(pRrImportTargetInfo) \
                (pRrImportTargetInfo->au1ImportTarget)
#define BGP4_VPLS_RR_IMPORT_TARGET_TIMESTAMP(pRrImportTargetInfo)    \
                (pRrImportTargetInfo->u4TimeStamp)
#define BGP4_VPLS_RR_IMPORT_TARGET_RTCNT(pRrImportTargetInfo)   \
                (pRrImportTargetInfo->u4RouteCount)

#define BGP4_VPLS_REGISTER_ASN_INFO(x)  x.Bgp4RegisterASNInfo

#ifdef VPLS_GR_WANTED
/*VPLS SPEC entry Stale/Unstale*/
#define BGP4_VPLS_SPEC_STALE    0x00000001
#define BGP4_VPLS_SPEC_UNSTALE  0x00000010

/*VPLS RT Stale/Unstale*/
#define BGP4_VPLS_RT_STALE    0x01
#define BGP4_VPLS_RT_UNSTALE  0x02
#endif

/* tBgp4VplsRrImportTargetInfo is used to store the route-targets information
 * that have been received by the speaker from the client peers.
 */
typedef struct _tBgp4VplsRrImportTargetInfo {
    tTMO_SLL_NODE NextTarget; /* pointer to the next import route target */
    UINT1 au1ImportTarget[8]; /* route-target */
    UINT4 u4TimeStamp; /* Time at which the route count associated
                                  * with this target has become zero and will
                                  * be incremented till the predefined delay
                                  * before this target is removed from the set
                                  */
    UINT4 u4RouteCount; /* Route count associated with this import
                                   * route target
                                   */
} tBgp4VplsRrImportTargetInfo;

/* Route Advertisement information*/
typedef struct t_peerRouteAdvtInfo
{
    UINT1               au1RouteDistinguisher[MAX_LEN_RD_VALUE];
    UINT4               u4labelBase;
    UINT2               u2VeId;
    UINT2               u2VeBaseOffset;
    UINT2               u2VeBaseSize;
    UINT2               u2Pad;
}tPeerRouteAdvtInfo;

/* Extended community information*/
typedef struct t_extendedCommInfo
{
    UINT1               au1RouteTarget[MAX_LEN_RT_VALUE];
}tExtendedCommInfo;

/* Layer2 Extended community information*/
typedef struct t_extCommLayer2Info
{
    UINT2               extCommType;
    UINT1               encapsType ;
    BOOL1               bControlWordFlag;
    UINT2               u2Mtu;
    UINT2               u2Reserved ;
}tExtCommLayer2Info;

typedef struct t_vplsAdvtInfo
{
    tPeerRouteAdvtInfo      PeerRouteAdvtInfo;
    tExtendedCommInfo       ExtendedCommInfo;
    tExtCommLayer2Info      ExtCommLayer2Info;
}tVplsAdvtInfo;

typedef struct t_VplsNlriInfo{
    UINT4     u4labelBase;
    UINT2     u2Length ;
    UINT2     u2VeId;
    UINT2     u2VeBaseOffset;
    UINT2     u2VeBaseSize;
    UINT1     au1RouteDistinguisher[MAX_LEN_RD_VALUE];
}tVplsNlriInfo;

typedef struct t_VplsRouteInformation{
    tBgp4PeerEntry *    pVplsPeer;    /*Used to store destination Peer info for VPLS route*/
    UINT4               u4Flags;
    UINT1               u1DestType;
    UINT1               au1Pad[3];
}tVplsRouteInformation;


/* Peer information */
typedef struct t_vplsPeerAddress
{
   UINT1                u1Addr[BGP4_MAX_INET_ADDRESS_LEN];    /*if IPv4  4 bytes holds addr */

}tVplsPeerAddress;

#ifdef MPLS_WANTED

/* tVplsSpecInfo is used to specifiy the parameters associated with a Vpls.
 *  * It contains all the VPLS config information received from MPLS
 *   */
typedef struct _tVplsSpecInfo {
    tTMO_SLL_NODE          NextVplsNode; /* pointer to the next Vpls node */
    tTMO_SLL               ImportTargets; /* List of import route targets
                                           * associated with this Vpls
                                           *                                            */
    tTMO_SLL               ExportTargets; /* List of export route targets
                                    * associated with this Vpls
                                    *                                     */
    UINT1                  au1VplsRD[BGP4_VPLS_ROUTE_DISTING_SIZE]; /* RD */
    UINT4                  u4VplsIndex; /* Vpls index */
    UINT1                  au1VplsName[BGP4_VPLS_MAX_VPLS_NAME_SIZE]; /* Vpls Name */
    UINT4                  u4VplsState; /* Vpls state; up/down/create */
    UINT4                  u4Flags; /* Vpls specific flags stale or unstale*/
} tVplsSpecInfo;

#endif

#ifdef VPLS_GR_WANTED
typedef struct t_VplsSyncEventInfo{
    tExtendedCommInfo   ExtendedCommInfo; 
    UINT4               u4VplsMsgType;/*Interface Message coming from L2VPN*/
}tVplsSyncEventInfo;
#endif

/*Strucutre containing all the Vpls global parameters*/
typedef struct _tBgp4VplsGlobalData {
#ifdef MPLS_WANTED
    tMemPoolId          VplsSpecInstancePoolId; /* For VPLS configuration and
                                                * statistics */
    tTMO_HASH_TABLE    *pVplsGlobalInfo; /* Hash table that contains VPLSs
                                         * information */
    UINT4                   u4VplsSpecInstanceCnt; /* Counter to keep track of
                                               * Vpls specific instances */
    tBgp4RegisterASNInfo   Bgp4RegisterASNInfo;
#endif
    tMemPoolId          RrImportTargetsSetMemPoolId; /* For RR Targets SET */
    tTMO_SLL            RrImportTargetsSet; /* RR's Import Targets SET */

    UINT4               u4RrRTDelInterval;  /* Interval at which route target
                                             * must be deleted from RR import
                                             *                                              * targets set */
    UINT4               u4RrImportTargetsSetCnt; /* Counter to keep track of
                                                  * RR's targets Set */

} tBgp4VplsGlobalData;

#endif /* BGP4L2VPN_H */

