/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved    
 *
 * $Id: bgrtref.h,v 1.9 2015/07/20 07:20:51 siva Exp $
 *
 *******************************************************************/
#ifndef _BGRTREF_H
#define _BGRTREF_H

/* Constant definitions*/

#define  BGP4_RTREF_MSG_SIZE          23
#define  BGP4_RTREF_MSG_MIN_LEN       23
#define  BGP4_RTREF_MSG                5
#define  BGP4_RTREF_LEN_FIELD          2
#define  BGP4_MSG_TYPE_LEN             1
#define  BGP4_RTREF_AFI_IPV4          (0x0001)
#define  BGP4_RTREF_SAFI_UNICAST      (0x01)
#define  BGP4_RTREF_IPV4              (1)
#define  BGP4_RTREF_AFI_LEN            2
#define  BGP4_RTREF_RES_LEN            1
#define  BGP4_RTREF_SAFI_LEN           1
#define  BGP4_RTREF_REQ_SET            1
#define  BGP4_RTREF_REQ_RESET          0

#define  BGP4_ORF_REQ_SET              1
#define  BGP4_ORF_REQ_RESET            0

#define  BGP4_ORF_WHEN_TO_REF_LEN      1
#define  BGP4_ORF_TYPE_LEN             1
#define  BGP4_ORF_LEN_FIELD            2
#define  BGP4_ORF_ACTION_MATCH_LEN     1
#define  BGP4_ORF_SEQUENCE_LEN         4
#define  BGP4_ORF_MIN_PREFIX_LEN       1
#define  BGP4_ORF_MAX_PREFIX_LEN       1
#define  BGP4_ORF_PREFIX_LEN           1

#define  BGP4_ORF_ADD                  0
#define  BGP4_ORF_REMOVE               1
#define  BGP4_ORF_REMOVE_ALL           2

#define  BGP4_ORF_ACTION_MASK          0xC0
#define  BGP4_ORF_MATCH_MASK           0x20

#define  BGP4_RR_ADVT_DEFAULT          0
#ifdef L3VPN
#define  BGP4_RR_ADVT_NON_CLIENTS      1
#define  BGP4_RR_ADVT_PE_PEERS         2
#ifdef VPLSADS_WANTED
#define  BGP4_RR_ADVT_VPLS_PE_PEERS         3
#ifdef EVPN_WANTED
#define  BGP4_RR_ADVT_EVPN_PEERS       4
#endif
#else
#ifdef EVPN_WANTED
#define  BGP4_RR_ADVT_EVPN_PEERS       3
#endif
#endif
#else
#ifdef VPLSADS_WANTED
#define  BGP4_RR_ADVT_NON_CLIENTS      1
#define  BGP4_RR_ADVT_VPLS_PE_PEERS         2
#ifdef EVPN_WANTED
#define  BGP4_RR_ADVT_EVPN_PEERS       3
#endif
#else
#ifdef EVPN_WANTED
#define  BGP4_RR_ADVT_NON_CLIENTS      1
#define  BGP4_RR_ADVT_EVPN_PEERS       2
#endif
#endif
#endif
#define BGP4_ORF_IMMEDIATE             1
#define BGP4_ORF_DEFER                 2

/* MACROS */
/* Macros for counters */
#define BGP4_RTREF_MSG_SENT_CTR(pPeer,u1Index) \
            ((pPeer->apAsafiInstance[u1Index])->pRtRefreshData->u4RtRefMsgSentCntr)

#define BGP4_RTREF_MSG_RCVD_CTR(pPeer,u1Index) \
                ((pPeer->apAsafiInstance[u1Index])->pRtRefreshData->u4RtRefMsgRcvdCntr)

#define BGP4_RTREF_MSG_TXERR_CTR(pPeer,u1Index) \
                ((pPeer->apAsafiInstance[u1Index])->pRtRefreshData->u4RtRefMsgTxErrorCntr)

#define BGP4_RTREF_MSG_RCVD_INVALID_CTR(pPeer,u1Index) \
                ((pPeer->apAsafiInstance[u1Index])->pRtRefreshData->u4RtRefMsgRcvdInvalidCntr)

#define BGP4_RTREF_MSG_REQ_PEER(pPeer,u1Index) \
                ((pPeer->apAsafiInstance[u1Index])->pRtRefreshData->u1RtRefReqForPeer)

#define BGP4_ORF_MSG_REQ_PEER(pPeer,u1Index) \
                ((pPeer->apAsafiInstance[u1Index])->pRtRefreshData->u1OrfReqForPeer)

#define BGP4_RTREF_AFISAFI(pAfiSafi) (pAfiSafi->u4AfiSafi)

/*Globals */
#define BGP4_RTREF_MEM_POOL_ID   (gBgpNode.RtRefPoolId)
#define BGP4_ORF_MEM_POOL_ID     (gBgpNode.OrfMemPoolId)
#define BGP4_ADV_ROUTES_MEM_POOL_ID     (gBgpNode.AdvRouteMemPoolId)
#define BGP4_RTREF_ALLOC_CTR(u4CxtId)     (gBgpCxtNode[u4CxtId]->Bgp4RtRefGlobalData.u4RtRefAllocCtr) 
#define BGP4_RTREF_PEER_LIST(u4CxtId)   (&(gBgpCxtNode[u4CxtId]->Bgp4RtRefGlobalData.TsRtRefPeerList))
#ifdef EVPN_WANTED
#define BGP4_EVPN_VRF_VNI_MEM_POOL_ID (gBgpNode.EvpnRouteMemPoolId)
#define BGP4_MAC_DUP_TIMER_MEM_POOL_ID (gBgpNode.BgpMacDupTimerMemPoolId)
#endif

typedef struct t_RtRefGlobalData {
   tTMO_SLL        TsRtRefPeerList;
   UINT4           u4RtRefAllocCtr;
} tRtRefGlobalData;

#endif /* _BGRTREF_H */
