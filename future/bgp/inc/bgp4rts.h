/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved    
 *
 * $Id: bgp4rts.h,v 1.68 2018/01/03 11:31:18 siva Exp $
 *
 * Description:Contain the prototypes of all the functions used   
 *             in the system.     
 *
 *******************************************************************/
#ifndef BGP4RTS_H

#define BGP4RTS_H

/* BGP4INIT_C */
INT4                Bgp4Init (VOID);
INT4                Bgp4InitGlobals (UINT4 u4CxtId,UINT1 u1Flag);
INT4                Bgp4InitContextGlobals(UINT4);
INT4                Bgp4InitPeerEntry (tBgp4PeerEntry *);
VOID                Bgp4InitMedlpEntry (tBgp4MedlpEntry *);
VOID                Bgp4InitFilterEntry (tBgp4UpdatefilterEntry *);
VOID                Bgp4InitAggrEntry (tBgp4AggrEntry *);
VOID                Bgp4InitLp (UINT4);
VOID                Bgp4InitMed (UINT4);
VOID                Bgp4InitFilter (UINT4);
VOID                Bgp4InitAggr (UINT4);
VOID                Bgp4InitLinkNode(tLinkNode *);
VOID                Bgp4InitASNode(tAsPath *);
INT4                Bgp4ClearAndReInit (UINT4);
VOID                Bgp4InitRtProfile (tRouteProfile * pRt);
VOID                Bgp4InitBgp4info (tBgp4Info * pInfo);
VOID                Bgp4InitBgp4PAInfo (tBgp4RcvdPathAttr *);
VOID                Bgp4InitBgp4AdvtPA (tBgp4AdvtPathAttr *);
VOID                Bgp4InitIgpMetricEntry (tBgp4IgpMetricEntry *);
VOID                Bgp4InitNetworkRtEntry (tNetworkAddr *pNetworkRt);
INT4                Bgp4InitAddrPrefixStruct (tAddrPrefix *, UINT2 );
INT4                Bgp4InitNetAddressStruct (tNetAddress *, UINT2 , UINT1);

/* BGP4PRCS_C */
INT4                Bgp4Dispatcher (VOID);
INT4                Bgp4ProcessInputRIB (UINT4);
INT4                Bgp4ProcessFIBUpdates (UINT4);
INT4                Bgp4ProcessRedisUpdates (UINT4);
INT4                Bgp4ProcessExternalPeerAdvt (UINT4);
INT4                Bgp4ProcessInternalPeerAdvt (UINT4);
INT4                Bgp4ProcessPeerInit(UINT4);
INT4                Bgp4ProcessPeerDeInit(UINT4);
INT4                Bgp4AddFeasRouteToFIBUpdList (tRouteProfile *,
                                                  tRouteProfile *);
INT4                Bgp4AddWithdRouteToFIBUpdList (tRouteProfile *);
INT4                Bgp4AddFeasRouteToIntPeerAdvtList (tRouteProfile *,
                                                       tRouteProfile *);
INT4                Bgp4AddWithdRouteToIntPeerAdvtList (tRouteProfile *);
INT4                Bgp4AddFeasRouteToExtPeerAdvtList (tRouteProfile *,
                                                       tRouteProfile *);
INT4                Bgp4AddWithdRouteToExtPeerAdvtList (tRouteProfile *);
INT4                Bgp4ProcessPendJobs(UINT4);
INT4                Bgp4CheckNextHopResolution (tBgp4IgpMetricEntry *);
INT4                Bgp4UpdateNHChgInitRoute (tRouteProfile * , UINT4, 
                                              VOID *, INT4 );
INT4                Bgp4ProcessNextHopResolution (UINT4);
INT4                Bgp4ProcessDflRouteOrigPolicyChange (UINT4, UINT1, UINT1);
INT4                BGPUpdateDistanceChangeForExistRoutes (UINT4, UINT4);
INT4                Bgp4DispatcherInCxt (UINT4);

INT4                Bgp4RegisterWithVcm(VOID);
VOID                BgpNotifyContextChange(UINT4, UINT4, UINT1);
VOID                Bgp4DeleteContext(UINT4 );
INT4                Bgp4CreateContext(UINT4);
/* BGP4TCPH_C */
INT4                Bgp4TcphReceiveData (tBgp4PeerEntry *);
INT4                Bgp4TcphCommand (tBgp4PeerEntry *, UINT1, UINT1 *,
                                       UINT4);
INT4                Bgp4TcphSendData (tBgp4PeerEntry *, UINT1 *, UINT4);
INT4                Bgp4TcphCheckConnIsInProgress (INT4, tBgp4PeerEntry *);
INT4                Bgp4TcphMaxSockId (tBgp4PeerEntry *, UINT1);
INT4                Bgp4TcphWaitOnSelect(UINT4, UINT2);

/* BGP4IH_C */
INT4                Bgp4IhAuthMsg (UINT1 *);
INT4                Bgp4IhScanForMarker (UINT1 *, UINT4, UINT4 *);
INT4                Bgp4IhHandleTimer (UINT4, FS_ULONG);
INT4                Bgp4RtReuseTimerExpiry(UINT4, FS_ULONG);
INT4                Bgp4IhHandlePacket (tBgp4PeerEntry *, UINT1 *, UINT4);
INT4                Bgp4IphHandleControl (tBgp4PeerEntry *, UINT1);
INT4                Bgp4IhProcessBgpCompletePacket (tBgp4PeerEntry *, INT4);
VOID                Bgp4MessageTypeToString (INT4);
/* BGP4TMRH_C */
INT4                Bgp4TmrhStartTimer (UINT4, VOID *, UINT4);
INT4                Bgp4TmrhStopTimer (UINT4, VOID *);
INT4                Bgp4TmrhProcessTimeout (VOID);
UINT4               Bgp4DiffTime (UINT4, UINT4);
INT4                Bgp4TmrhStopAllTimers (tBgp4PeerEntry *);
INT4                Bgp4OneSecTimerExpiry(tTmrAppTimer *);
UINT4                Bgp4ElapTime (VOID);

/* BGP4EH_C */
INT4                Bgp4EhSendError (tBgp4PeerEntry *, UINT1, UINT1,
                                        UINT1 *, UINT4);

/* BGP4MH_C */
INT4        Bgp4MhGetWithdrawnRoutes (tBgp4PeerEntry *, UINT2,
                                      UINT1 *, tTMO_SLL *);
INT4        Bgp4MsghProcessOrigin (tBgp4PeerEntry *, tBgp4Info *,
                                   UINT1 *, INT4);
INT4        Bgp4MsghProcessPath (tBgp4PeerEntry *, tBgp4Info *,
                                 UINT1 *, INT4);
INT4        Bgp4MsghProcessPathFour (tBgp4PeerEntry *, tBgp4Info *,
                                     UINT1 *, INT4);
INT4        Bgp4MsghProcessNexthop (tBgp4PeerEntry *, tBgp4Info *,
                                    UINT1 *, INT4);
INT4        Bgp4MsghValidateIpv4Nexthop (tBgp4PeerEntry * ,
                                         tBgp4Info * , UINT1 *);
INT4        Bgp4MsghProcessMed (tBgp4PeerEntry *, tBgp4Info *, UINT1 *, INT4);
INT4        Bgp4MsghProcessLocalpref (tBgp4PeerEntry *,
                                      tBgp4Info *, UINT1 *, INT4);
INT4        Bgp4MsghProcessAtomicAggr (tBgp4PeerEntry *,
                                       tBgp4Info *, UINT1 *, INT4);
INT4        Bgp4MsghProcessAggregator (tBgp4PeerEntry *,
                                       tBgp4Info *, UINT1 *, INT4);
INT4        Bgp4MsghProcessAggregatorFour (tBgp4PeerEntry *,
                                       tBgp4Info *, UINT1 *, INT4);
INT4        Bgp4MsghProcessUnknown (tBgp4PeerEntry *, tBgp4Info *,
                                    UINT1 *, INT4);
INT4        Bgp4MsghCheckFlag (UINT1, UINT1, UINT1 *);

UINT2       Bgp4MhGetMyASField (const tBgp4PeerEntry *pPeerInfo);

INT4        Bgp4MhCheckASPathRoutingLoop (const tTMO_SLL * PtsNewroutes);

INT4        Bgp4MhProcessOpen (tBgp4PeerEntry *, UINT1 *, UINT4);
INT4        Bgp4MhProcessNotification (tBgp4PeerEntry *, UINT1 *,
                                       UINT4);

INT4        Bgp4MhProcessEndOfRIBMarker (tBgp4PeerEntry * pPeerentry,
                               UINT1 *pu1Buf,UINT4 u4Msglen);

INT4        Bgp4MhProcessUpdate (tBgp4PeerEntry *, UINT1 *, UINT4,
                                 tTMO_SLL *, tTMO_SLL *);
INT4        Bgp4MsghProcessCommunity (tBgp4PeerEntry *, tBgp4Info *,
                                      UINT1 *, INT4);
INT4        Bgp4MsghProcessExtComm (tBgp4PeerEntry *, tBgp4Info *,
                                    UINT1 *, INT4);
INT4        Bgp4MsghProcessOrigId (tBgp4PeerEntry *, tBgp4Info *,
                                   UINT1 *, INT4);
INT4        Bgp4MsghProcessClusterList (tBgp4PeerEntry *, tBgp4Info *,
                                        UINT1 *, INT4);
INT4        Bgp4MhGetNewRoutes (tBgp4PeerEntry *, INT4, INT4,
                                UINT1 *, tTMO_SLL *, tTMO_SLL *);
UINT1       *Bgp4MhFormOpen (tBgp4PeerEntry *, UINT4 *);
UINT1       *Bgp4MhFormKeepalive (tBgp4PeerEntry *, UINT4 *);
UINT1       *Bgp4MhFormNotification (tBgp4PeerEntry *, UINT1, UINT1,
                                               UINT1 *, UINT4 *);
INT4        Bgp4MhDisplayRouteInfo (tBgp4PeerEntry *, tTMO_SLL *, tTMO_SLL *);

/* BGP4SEMH_C */
INT4        Bgp4SEMHProcessEvent (tBgp4PeerEntry *, UINT2, UINT1 *, UINT4);
INT4        Bgp4SemhIdleState (tBgp4PeerEntry *, UINT2);
/* RFC 4271 Updates */
INT4 Bgp4ConfigPeerPrefixLimit(tCliHandle, INT4, tAddrPrefix *, UINT1 *);
INT4 Bgp4ConfigConnectRetryCount(tCliHandle, INT4, tAddrPrefix *, UINT1 *);
INT4 Bgp4ConfigAllowAutoStop(tCliHandle, UINT1, tAddrPrefix *, UINT1 *);
INT4 Bgp4ConfigAllowAutoStart(tCliHandle CliHandle, UINT1 u1Status,
                              tAddrPrefix * pIpAddress, UINT1 *pu1PeerGroupName);
INT4 Bgp4ConfigDampPeerOscillations(tCliHandle CliHandle, UINT1 u1Status,
                                    tAddrPrefix * pIpAddress, UINT1 *pu1PeerGroupName);
INT4 Bgp4ConfigDelayOpen(tCliHandle CliHandle, UINT1 u1Status,
                         tAddrPrefix * pIpAddress, UINT1 *pu1PeerGroupName);
INT4 Bgp4ConfigTcpPassiveConnection (tCliHandle CliHandle, UINT1 u1Status,
                         tAddrPrefix * pIpAddress, UINT1 *pu1PeerGroupName);
INT4
Bgp4ConfigTrapNotification(tCliHandle, UINT4);
INT4 Bgp4ConfigIBGPRedistributionStatus(tCliHandle, UINT4);
INT4        Bgp4SemhConnectState (tBgp4PeerEntry *, UINT2, UINT1 *, UINT4);
INT4        Bgp4SemhActiveState (tBgp4PeerEntry *, UINT2, UINT1 *, UINT4);
INT4        Bgp4SemhOpensentState (tBgp4PeerEntry *, UINT2, UINT1 *, UINT4);
INT4        Bgp4SemhOpenconfirmState (tBgp4PeerEntry *, UINT2, UINT1 *, UINT4);
INT4        Bgp4SemhEstablishedState (tBgp4PeerEntry *, UINT2, UINT1 *, UINT4);
INT4        Bgp4SemhProcessInvalidEvt (tBgp4PeerEntry *);
VOID        Bgp4SemhDampCheck (tBgp4PeerEntry *);
VOID        Bgp4SemhSendOpenMsg(tBgp4PeerEntry * pPeerentry);
VOID        Bgp4SemhCheckOpenMsg(tBgp4PeerEntry * pPeerentry,
                                 UINT1 *pu1Buf, UINT4 u4Buflen);
tBgp4PeerEntry *Bgp4SemhResolveCollision (tBgp4PeerEntry *);
INT4        Bgp4SemhCloseParallelConn (tBgp4PeerEntry *);
INT4        Bgp4AddTransitionToFsmHist (tBgp4PeerEntry * , INT1); 
INT4        Bgp4AddAdvtRouteToList (tBgp4PeerEntry *, tRouteProfile *);
INT4        Bgp4DelAdvtRoute (tBgp4PeerEntry *, tRouteProfile *);


/* BGRBINIT_C */
INT4        Bgp4RibhFreeRibtree (UINT4);
INT4        Bgp4RibhClearRIB (UINT4);

/* BGP4RIBH_C */
INT4                Bgp4RibhStartConnection (tBgp4PeerEntry *);
INT4                Bgp4RibhEndConnection (tBgp4PeerEntry *);
VOID                Bgp4RibhInitPeerDeInitInfo (tBgp4PeerEntry *);
#ifdef L3VPN
INT4                Bgp4RibhAddFeasRtToVpnv4List (tRouteProfile *) ;
INT4                Bgp4RibhAddReplaceRtToVpnv4List (tRouteProfile *,
                                              tRouteProfile *);
INT4                Bgp4RibhAddWithdRtToVpnv4List (tRouteProfile *);
INT4                Bgp4Vpnv4UpdtRouteInIPVRF (tRouteProfile  *);
#endif
#ifdef VPLSADS_WANTED
INT4 Bgp4MpeProcessL2vpnPrefix ( UINT1 *pu1RcvdUpdateMessage, 
                                 tRouteProfile * pRtProfile, 
                                 UINT2 *pu2NlriLenValidated);
INT4 Bgp4VplsGetNlriInfoFromNetAddr (UINT1 *, tVplsNlriInfo *);
#endif
INT4                Bgp4RibhAddFeasibleRouteToUpdLists (tRouteProfile *);
INT4                Bgp4RibhAddReplaceRouteToUpdLists (tRouteProfile *,
                                                       tRouteProfile *);
INT4                Bgp4RibhAddWithdRouteToUpdLists (tRouteProfile *);
INT4                Bgp4IgphAddFeasibleRouteToVrfUpdLists (tRouteProfile *,
                                    tRouteProfile * );
INT4                Bgp4IgphAddWithdRouteToVrfUpdLists (tRouteProfile *);
INT4                Bgp4RibhAddFeasibleRouteToVrfUpdLists (tRouteProfile *);
INT4                Bgp4RibhAddReplaceRouteToVrfUpdLists (tRouteProfile *,
                                            tRouteProfile *);
INT4                Bgp4RibhAddWithdRouteToVrfUpdLists (tRouteProfile *);
INT4                Bgp4RibhProcessWithdRoute (tBgp4PeerEntry *,
                                               tRouteProfile *,
                                               VOID *);
INT4                Bgp4RibhProcessFeasibleRoute (tBgp4PeerEntry *,
                                                  tRouteProfile *);
UINT4               Bgp4RibhIsRouteIdentical (tRouteProfile *pRtProfile1, 
                    tRouteProfile *pRtProfile2);

INT4                Bgp4RibhProcessOverlapFeasRoutes (tRouteProfile * ,
                                                      tRouteProfile ** ,
                                                      tTMO_SLL * );

/* RIB INTERFACE FUNCTIONS - INDEPENDENT OF UNDERLYING IMPLEMENTATION OF RIB */
INT4                Bgp4RibhAddRtEntry (tRouteProfile *, UINT4);
INT4                Bgp4RibhDelRtEntry (tRouteProfile *, VOID *, UINT4, UINT1);
INT4                Bgp4RibhLookupRtEntry (tRouteProfile *, UINT4, UINT4,
                                           tRouteProfile **, VOID **);
INT4                Bgp4RibhGetFirstEntry (UINT4, tRouteProfile **, VOID **, 
                                           INT4);
INT4                Bgp4RibhGetNextEntry (tRouteProfile *, UINT4,
                                          tRouteProfile **,
                                          VOID **, INT4);
INT4                Bgp4RibhGetPrevEntry (tRouteProfile *, tRouteProfile **,
                                          VOID **);
INT4                Bgp4RibhTraverseAndGetNextEntry (tRouteProfile  *, VOID *, 
                                                     tRouteProfile **);
INT4                Bgp4RibhTraverseAndGetPrevEntry (tRouteProfile  *, VOID *,
                                                     tRouteProfile **);
tRouteProfile      *Bgp4RibhGetFirstPeerRtEntry (tBgp4PeerEntry * pPeer);
tRouteProfile      *Bgp4RibhGetFirstAsafiPeerRtEntry (tBgp4PeerEntry * ,UINT4);
tRouteProfile      *Bgp4RibhGetNextIpv4PeerRtEntry (tRouteProfile  *,
                                                    tBgp4PeerEntry *);
INT4                Bgp4RibhGetFirstRouteFromNextAddrFamily (UINT4, UINT4 ,
                                                    tRouteProfile **);
tRouteProfile      *Bgp4RibhGetNextPeerRtEntry (tRouteProfile  *,
      tBgp4PeerEntry *);


#ifdef L3VPN
INT4    Bgp4RibhGetFirstVrfImportRtEntry (tRouteProfile **, UINT4 *);
INT4    Bgp4RibhGetNextVrfImportRtEntry (tRouteProfile  *, UINT4,
                                         tRouteProfile **, UINT4 *);
INT4    Bgp4Vpnv4GetFirstFsbgp4ImportTableIndex (tNetAddress *,
                                                 INT4 *,
                                                 tAddrPrefix *,
                                                 INT4 *, INT4 *,
                                                 tSNMP_OCTET_STRING_TYPE *);
#endif
#ifdef BGP4_IPV6_WANTED
tRouteProfile      *Bgp4RibhGetNextIpv6PeerRtEntry (tRouteProfile  *,
                                                    tBgp4PeerEntry *);
#endif
tRouteProfile      *Bgp4RibhGetFirstImportRtEntry (UINT4, VOID **);
tRouteProfile      *Bgp4RibhGetFirstImportAsafiRtEntry (UINT4, VOID **);
tRouteProfile      *Bgp4RibhGetNextImportRtEntry (tRouteProfile  *, VOID **);

/* BGP4DECH_C */
INT4                Bgp4DechAddRouteSorted (tRouteProfile **, tRouteProfile * );
UINT1               Bgp4DechCompareMED (UINT4 u4CxtId, const tTMO_SLL * pPresentRtASPath, 
                                        const tTMO_SLL * pNewRtASPath);
INT4
Bgp4DechReRunDecProcess (tRouteProfile **, UINT4);

/* BGP4AH_C */
BOOL1               Bgp4AhIsRouteCanbeAdvt (tBgp4PeerEntry *,
                                            tRouteProfile *, UINT1);
INT4                Bgp4AhCanRouteBeAdvtWithdrawn (tRouteProfile * ,
                                                   tRouteProfile * );
INT4                Bgp4AhProcessMinASTimerExpiryEvent (tBgp4PeerEntry *);
INT4                Bgp4AhProcessTimer (UINT4, FS_ULONG);
INT4                Bgp4AhUpdateFeasibleRouteList (tBgp4PeerEntry *,
                                                   tRouteProfile *,
                                                   tTMO_SLL *, tTMO_SLL *);
INT4                Bgp4AhUpdateWithdrawnRouteList (tBgp4PeerEntry *,
                                                    tRouteProfile *,
                                                    tTMO_SLL *);
INT4                Bgp4AhAddFeasibleRouteToPeerlist (tBgp4PeerEntry *,
                                                      tRouteProfile *,
                                                      tTMO_SLL *, tTMO_SLL *);
INT4                Bgp4AhIsTimeToSend (tBgp4PeerEntry *, tRouteProfile *,
                                        UINT4);
INT4                Bgp4AhSendUpdatesToPeer ( tBgp4PeerEntry *,
                                              tTMO_SLL       *,
                                              tTMO_SLL       *);
INT4                Bgp4AhAddRouteToAdvtPAList (tBgp4AdvtPathAttr *,
                                                tRouteProfile *);
INT4                Bgp4AhSendMpeRoutes (tBgp4PeerEntry * , tTMO_SLL  *,
                                         UINT1 **, UINT2 *, UINT2 *);
UINT2              Bgp4AhAppendMpeAttrBuffToUpdMsg (UINT1 *, UINT1 *, UINT2,
                                                     UINT1, UINT2  , UINT1);
INT4                Bgp4AhSendConstructUpdMsg (tBgp4PeerEntry *, UINT1 **,
                                               UINT2 *, UINT2 *, UINT2 *,
                                               UINT1);
INT4                Bgp4AhProcessAdvtList(VOID);
INT4                Bgp4AhHandleNonBgpRtAdvtPolicyChange (UINT4, UINT4);
INT4                Bgp4ProcessWithdrawnRoutesList (tBgp4PeerEntry *,
                                                    tTMO_SLL *, UINT1 **,
                                                    UINT2 *, UINT2 *);
INT4                Bgp4ProcessFeasibleRouteList (tBgp4PeerEntry * ,
                                                  tBgp4AdvtPathAttr *,
                                                  UINT2, UINT2, UINT1 *,
                                                  UINT2, UINT2);
INT4                Bgp4IsCurrentDiffToPrevBgp4Attr(tRouteProfile *,
                                                    tRouteProfile * );
UINT2               Bgp4AddPathAttributeToUpdate (tBgp4Info *, 
                                                  tBgp4PeerEntry *, 
                                                  UINT1 *);
/*  BGPSYNC_C */
INT4                Bgp4SyncCheckForSync (tRouteProfile *, tBgp4PeerEntry *);
INT4                Bgp4SyncProcessSynchronisation (UINT4);
INT4                Bgp4SyncUpdateSyncInitRoute (tRouteProfile * ,VOID * ,INT4);

/*  BGP4ATTR_C  */
INT4                Bgp4PAFormHashKey (tBgp4Info *, UINT4 *, UINT4);
INT4                Bgp4CreateRcvdPADB (UINT4);
INT4                Bgp4DeleteRcvdPADB (UINT4);
VOID                Bgp4RcvdPAReleasePAInfo (UINT4, tBgp4RcvdPathAttr *, UINT2, UINT2);
VOID                Bgp4RcvdPAClearPAEntry (tTMO_HASH_NODE *);
tBgp4RcvdPathAttr  *Bgp4RcvdPADBAddEntry (UINT4, tBgp4Info *, UINT2, UINT2);
INT4                Bgp4RcvdPADBAddRoute (tRouteProfile *, UINT1);
INT4                Bgp4RcvdPADBDeleteRoute (tRouteProfile *, UINT1);
INT4                Bgp4RcvdPADBGetNextEntry (UINT4, tBgp4RcvdPathAttr *, UINT4, UINT2,
                                              UINT2, tBgp4RcvdPathAttr **,
                                              UINT4 *);
INT4                Bgp4RcvdPADBGetFirstBestRoute (UINT4, tRouteProfile **, UINT2,
                                                   UINT2);
INT4                Bgp4RcvdPADBGetNextBestRoute (tRouteProfile *,
                                                  tRouteProfile **);
INT4                Bgp4CreateAdvtPADB (UINT4);
INT4                Bgp4DeleteAdvtPADB (UINT4);
VOID                Bgp4AdvtPADBClearAdvtDB (UINT4);
VOID                Bgp4AdvtPADBClearAdvtEntry (tTMO_HASH_NODE *);
tBgp4AdvtPathAttr  *Bgp4AdvtPADBAddEntry (tBgp4PeerEntry *, tBgp4Info *, UINT2,
                                          UINT2);
UINT1              *Bgp4FormBgp4Hdr (UINT1);
UINT1               Bgp4RoundToBytes (UINT2);
UINT4               Bgp4GetBytesForPrefix (tRouteProfile *, tBgp4PeerEntry *);
#ifdef L3VPN
UINT2               Bgp4AttrFillVpnv4Prefix (UINT1 *, tRouteProfile *);
BOOL1                Bgp4IsSameBgpLabel (tRouteProfile *, tRouteProfile*);
#endif
UINT2               Bgp4AttrGetUnknown (tRouteProfile *, UINT1 *);
UINT2               Bgp4AttrAttributeSize (tBgp4PeerEntry *, tBgp4Info *,
                                           UINT2, UINT2);
UINT2               Bgp4AttrFillPrefix (UINT1 *, tRouteProfile *);
INT4                Bgp4AttrFillPathAttributes (tBgp4PeerEntry *,
                                                tRouteProfile *, tBgp4Info *, tBgp4Info *);
BOOL1               Bgp4AttrIsAspathIdentical (tBgp4Info *, tBgp4Info *);
BOOL1               Bgp4AttrIsSNPAIdentical (tBgp4Info *, tBgp4Info *);
BOOL1               Bgp4AttrIsBgpinfosIdentical (tBgp4Info *,
                                                     tBgp4Info *);
BOOL1               Bgp4AttrIsBgpinfosIdenticalImport (tBgp4Info *,
                                                     tBgp4Info *,UINT1,UINT1);


UINT2               Bgp4AttrGetfirstAsno (tBgp4Info *);
INT4                Bgp4AttrFillOrigin (tBgp4Info *, tBgp4Info *);
UINT2               Bgp4AttrAsPathSize (tBgp4Info *, INT4);
UINT2               Bgp4AttrAs4PathSize (tBgp4Info *, INT4);
UINT2               Bgp4AttrAspathLength (const tTMO_SLL *);
INT4                Bgp4AttrFillAspath (tBgp4Info *, tBgp4Info *,
                                        tBgp4PeerEntry *);
INT4                Bgp4AttrFillNexthop (tRouteProfile *, tBgp4Info *,
                                         tBgp4PeerEntry *);
INT4                Bgp4AttrFillMed (tRouteProfile *, tBgp4Info *,
                                     tBgp4PeerEntry *, tBgp4Info *, UINT4);
INT4                Bgp4AttrFillLp (tRouteProfile *, tBgp4Info *,
                                    tBgp4PeerEntry *);
INT4                Bgp4AttrFillAggr (tRouteProfile *, tBgp4Info *);
UINT2               Bgp4AttrUnknownLength (tBgp4Info *);
INT4                Bgp4AttrFillUnknown (tBgp4Info *, tBgp4Info *);
INT4                Bgp4GetUnknownBgpInfo (const tRouteProfile *, UINT1 **,
                                           UINT2 *);

/*  BGP4AGGR_C  */
INT4                Bgp4AggregationHandler (UINT4);
INT4                Bgp4AggrProcessAdminUp (UINT4);
INT4                Bgp4AggrProcessAdminDown (UINT4);
INT4                Bgp4AggrProcessWithdrawnRoute (tRouteProfile *);
INT4                Bgp4AggrProcessFeasibleRoute (tRouteProfile *,
                                                  tRouteProfile *);
UINT4               Bgp4AggrFindAggrEntry (tRouteProfile *,INT4);
INT4                Bgp4AggrReleaseAggrList (UINT4);
INT4                Bgp4AggrProcessAdminChange (UINT4, INT4);
INT4                Bgp4AggrProcessAdvertiseChange (UINT4, INT4);
INT4                Bgp4AggrProcessAggregation (UINT4);
INT4                Bgp4AggrReaggregate (tRouteProfile *, tTMO_SLL *);
INT4                Bgp4AggrAggregateBgpInfos (tRouteProfile *,
                                               tRouteProfile *, UINT1, UINT1);
INT4                Bgp4AggrAddAggrRouteToAdvtList (UINT4, UINT4);
INT4                Bgp4AggrCanAggregationBeDone (tRouteProfile *, UINT4 *,INT4);
UINT1               Bgp4AggrCanAggrRouteBeAdvt (tRouteProfile *,
                                                tRouteProfile **);
INT4                Bgp4AggrWithdrawAdvtRoute (tRouteProfile *);
INT4                Bgp4AggrAdvtSuppressedRoute (tRouteProfile *);
INT4                Bgp4AggrUpdateAspath (tBgp4Info *, tBgp4Info *, UINT4);
INT4                Bgp4AggrCopyBgp4infos (tRouteProfile *, tRouteProfile *, UINT1, UINT1);
INT4                Bgp4AggrCopyRoutes (tRouteProfile *, tRouteProfile *,UINT1, UINT1);
BOOL1               Bgp4AggrFindAsnoInarray (UINT4 *, UINT2, UINT4);
UINT4               Bgp4AggrCheckForReAggregation (tRouteProfile *,tTMO_SLL *, UINT1, UINT1);
INT4                Bgp4AggrClearAggrgateTable (UINT4);
INT4                Bgp4AggrClearAggrgateTableEntry (UINT4);
INT4                Bgp4AggrAdvtAggrRouteToPeer (tBgp4PeerEntry *);
INT4                Bgp4CheckAggrRestrict(INT4 , tRouteProfile * );
INT4                Bgp4AggrUpdateAggrInitRoute (tRouteProfile *, VOID *, INT4);

/* BGP4DSH_C */
tRouteProfile      *Bgp4DshGetRouteFromList (tTMO_SLL *, tRouteProfile *);
INT4                Bgp4DshAddRouteToPeerList (tBgp4PeerEntry * , UINT4 ,
                                               tRouteProfile * , UINT4 );
INT4                Bgp4DshAddRouteToPeerAdvtList (tTMO_SLL * ,
                                                   tRouteProfile * ,
                                                   UINT4 ,
                                                   UINT4 );
tRouteProfile      *Bgp4RibhGetRouteFromPeerList (tTMO_SLL *, tRouteProfile *);
tRouteProfile      *Bgp4RibhGetRtFromPeerAdvRtList (tTMO_SLL * ,
                                                    tRouteProfile * ,
                                                     UINT4 *);
tRouteProfile      *Bgp4DshGetRouteFromPeerList (tBgp4PeerEntry * , UINT4 ,
                                                 tRouteProfile * );
tRouteProfile      *Bgp4DshGetRouteFromPeerAdvList (tBgp4PeerEntry * , UINT4 ,
                                                    tRouteProfile * , UINT4 *);
tRouteProfile      *Bgp4DshGetPeerRouteFromList (tTMO_SLL *, tRouteProfile *,
                                                 tBgp4PeerEntry *);
INT4                Bgp4DshAddRouteToList (tTMO_SLL *, tRouteProfile *, UINT4);
tRouteProfile      *Bgp4DshRemoveNodeFromList (tTMO_SLL *, tLinkNode *);
VOID                Bgp4DshRelRtFromPeerRtAdvList (tTMO_SLL * ,
                                                   tAdvRtLinkNode * );
tRouteProfile      *Bgp4DshRemoveRouteFromList (tTMO_SLL *, tRouteProfile *);
tRouteProfile      *Bgp4RibhRemoveRouteFromPeerList (tTMO_SLL * , tRouteProfile * );
tRouteProfile      *Bgp4RibhRemoveRtFromPeerAdvtList (tTMO_SLL * ,
                                                      tRouteProfile * ,
                                                      UINT4 *);
tRouteProfile      *Bgp4DshRemoveRouteFromPeerList (tBgp4PeerEntry * , UINT4 ,
                                                    tRouteProfile * );
tRouteProfile      *Bgp4DshRemoveRtFrmPeerAdvtList (tBgp4PeerEntry * , UINT4 ,
                                                    tRouteProfile * , UINT4 *);
tRouteProfile      *Bgp4DshGetFirstRouteFromProfileList (tRouteProfile * );
tRouteProfile      *Bgp4DshGetBestRouteFromProfileList (tRouteProfile *, 
                                                        UINT4 );
tRouteProfile      *Bgp4DshGetRtForNextHopFromProfileList (tRouteProfile * ,
                                                tAddrPrefix, UINT4);
INT4                Bgp4DshAddRouteToProfileList (tRouteProfile **,
                                                  tRouteProfile  *);
INT4                Bgp4DshRemoveRouteFromProfileList (tRouteProfile * ,
                                                       tRouteProfile **);
INT4                Bgp4DshLinkRouteToList (tBGP4_DLL *, tRouteProfile *,
                                            UINT4, UINT1);
tRouteProfile      *Bgp4DshDelinkRouteFromList (tBGP4_DLL *, tRouteProfile *,
                                                UINT1);
INT4                Bgp4DshClearLinkList (UINT4, tBGP4_DLL *, UINT1);
INT4                Bgp4DshDelinkRouteFromChgList (tRouteProfile *);
INT4                Bgp4DshReleaseAsPathList(tTMO_SLL *);
VOID                Bgp4DshReleaseBgpInfo (tBgp4Info *);
VOID                Bgp4DshReleaseRtInfo (tRouteProfile *);
tLinkNode          *Bgp4DshAllocateLinkNode (VOID);
tAdvRtLinkNode     *Bgp4DshAllocateAdvtRtLinkNode(VOID);
VOID                Bgp4DshReleaseLinkNode (tLinkNode *);
VOID                Bgp4DshReleaseAdvRtLinkNode (tAdvRtLinkNode * );
UINT4               Bgp4DshReleaseList (tTMO_SLL *, UINT4);
INT4                Bgp4DshReleaseNumRtsFromList (tTMO_SLL * ,UINT4 );
INT4                Bgp4DshGetDeniedRoutesFromOverlapList (tTMO_SLL *, 
                                                           tRouteProfile *,
                                                           UINT4);
INT4                Bgp4DshGetDeniedLessSpecRoutesFromOverlapList (tTMO_SLL *, 
                                                            tRouteProfile *);
INT4                Bgp4DshGetDeniedMoreSpecRoutesFromOverlapList (tTMO_SLL *, 
                                                            tRouteProfile *);
tRouteProfile      *Bgp4DshGetLessSpecRoutesFromOverlapList (tTMO_SLL *, 
                                                            tRouteProfile *);
tRouteProfile      *Bgp4DshGetMoreSpecRoutesFromOverlapList (tTMO_SLL *, 
                                                            tRouteProfile *);
UINT4              Bgp4DshCheckForMoreSpecRoutesInOverlapList ( 
                                                            tRouteProfile *);
INT4                Bgp4DshAddRouteToOverlapList (tRouteProfile *);
INT4                Bgp4DshRemovePeerRoutesFromOverlapList (tBgp4PeerEntry *);
tRouteProfile*      Bgp4DshGetNonBgpRtFromProfileList (tRouteProfile * ,
                                        tRouteProfile *, BOOL1 *);
INT4                Bgp4RibhGetFirstOverlapMoreSpecificRoute ( tRouteProfile *,
                                                               tRouteProfile **,
                                                               VOID **);

tRouteProfile      *Bgp4DshGetPeerRtFromProfileList (tRouteProfile *, 
                                                    tBgp4PeerEntry *);
INT4                Bgp4DshRemovePeerFromList (tTMO_SLL *, tBgp4PeerEntry *);
INT4                Bgp4DshAddRouteToPeerAdvList (tBgp4PeerEntry * , UINT4 ,
                                            tRouteProfile * , UINT4 ,UINT4 );
INT4                Bgp4DshRemovePeerRoutesFromList (tTMO_SLL *,
                                                     tBgp4PeerEntry *);

/*  BGP4FILT_C  */
UINT4               Bgp4UpdateFilter (tBgp4PeerEntry *, tRouteProfile *,
                                        UINT4);
UINT4               Bgp4ConfigMED (tBgp4PeerEntry *, tRouteProfile *, UINT4);
UINT4               Bgp4ConfigLP (tBgp4PeerEntry *, tRouteProfile *, UINT4);
INT4                Bgp4FiltApplyLPMED (tBgp4PeerEntry * , tTMO_SLL *);
BOOL1               Bgp4IsThisMatching (tBgp4PeerEntry *, tRouteProfile *,
                                        UINT4, tBgp4UpdatefilterEntry);
BOOL1               Bgp4IsInterASMatch (tBgp4PeerEntry *, tRouteProfile *,
                                        UINT4, UINT4 *);
BOOL1               Bgp4IsASnoFound (tTMO_SLL *, UINT4);
INT4                Bgp4FiltApplyOutgoingFilter (tBgp4PeerEntry *,
                                                 tRouteProfile *);
INT4                Bgp4FiltApplyInboundSoftCfgSendRtRef(UINT4);
INT4                Bgp4FiltApplyOutBoundSoftCfg(UINT4);
INT4                Bgp4FiltApplyIncomingSoftReconfig(tBgp4PeerEntry * );
INT4                Bgp4FiltApplyInputFilter (tTMO_SLL *, tBgp4PeerEntry *); 
tRouteProfile      *Bgp4FiltApplyInputFilterOnRoute (tRouteProfile *,
                                                     tBgp4PeerEntry *);
INT4                Bgp4FiltOutBoundSoftConfigHandler(UINT4, tAddrPrefix ,
                                                       UINT2 , UINT1 );
INT4                Bgp4SoftOutPendHandler (tBgp4PeerEntry *);

/* BGP4IGPH_C  */
INT4                Bgp4ImportRoute (UINT4 , tNetAddress ,
                                     UINT4 , tAddrPrefix , UINT4 ,
                                     UINT4 , INT4 , UINT4 ,
                                     UINT1 , UINT1 ,
                                     UINT4 *, UINT4 );

INT4                Bgp4ImportRouteAdd (UINT4 , tNetAddress ,
                                        UINT4 , tAddrPrefix ,
                                        UINT4 , INT4 , UINT4 ,
                                        UINT1 , UINT1 ,
                                        UINT4 *,UINT1, UINT4 );

INT4                Bgp4ImportRouteDelete (UINT4, tNetAddress  , UINT4 ,
                                           tAddrPrefix , UINT4 , UINT1);
INT4                Bgp4RouteLeak (UINT4, tNetAddress , UINT4 , tAddrPrefix ,
                                   UINT4 , UINT4 , INT4 , UINT4 , UINT1, UINT1 ,
                                   UINT4 *, UINT4 );
INT4                Bgp4IgphAddFeasibleRouteToUpdLists (tRouteProfile *,
                                                        tRouteProfile *);
INT4                Bgp4IgphAddWithdRouteToUpdLists (tRouteProfile *);

#ifdef L3VPN
INT4                Bgp4IgphCheckFeasibleRouteForVrfUpdt(tRouteProfile *, 
                                tRouteProfile * , UINT4, tTMO_SLL *, 
                                tTMO_SLL *);
tRouteProfile      *Bgp4DshGetPeerRtFromVrfProfileList (tRouteProfile *,
                                                        tBgp4PeerEntry *, UINT4);
tRouteProfile      *Bgp4DshGetPeerVrfRtFromProfileList (tRouteProfile *,
                                                        tBgp4PeerEntry *,tRouteProfile *);
#endif
INT4                Bgp4IgphProcessFeasibleImportRoute (tRouteProfile *);
INT4                Bgp4IgphProcessWithdrawnImportRoute (tRouteProfile *);
INT4                Bgp4IgphUpdateRouteInFIB (tRouteProfile *);
INT4                Bgp4ChangeNotificationMessageToRTM (tRouteProfile *);
INT1                Bgp4ExtractInfoFromTag (UINT4, tBgp4Info *, UINT4, tRouteProfile *);
UINT4               Bgp4SetTaginfo (tBgp4Info *, UINT4);
INT4                Bgp4ConstructAsPath (UINT4, tBgp4Info *);
INT1                Bgp4StoreImportList (tTMO_HASH_TABLE *, tRouteProfile *);
INT4                Bgp4ProcessImportList (UINT4, UINT4);
INT4                Bgp4UpdateMetricInRoutes (UINT4,UINT4,INT4);
INT4                Bgp4CreateIgpMetricTable (UINT4);
INT4                Bgp4DeleteIgpMetricTable (UINT4);
UINT1               Bgp4IgpMetricTblGetNextEntry (tBgp4IgpMetricEntry *,
                                                  tBgp4IgpMetricEntry **);
UINT1               Bgp4IgpMetricTblGetEntry (UINT4, tAddrPrefix , 
                                              tBgp4IgpMetricEntry **);
INT4                Bgp4ProcessNextHopForResolution (tRouteProfile *,
                                                     tAddrPrefix *,
                                                     INT4 *, UINT4 *);
tBgp4IgpMetricEntry *Bgp4IgpMetricTblAddEntry (UINT4, tAddrPrefix );
INT4                 Bgp4IgpMetricTblDeleteEntry (UINT4, tAddrPrefix );
INT4                Bgp4AddRouteToIdentNextHopList (tRouteProfile *, UINT4, 
                                                    tAddrPrefix);
tRouteProfile      *Bgp4RemoveRouteFromIdentNextHopList (tRouteProfile * ,
                                                         UINT4, tAddrPrefix );
tTMO_HASH_TABLE    *Bgp4CreateHashTable (UINT4);
UINT4               Bgp4ClearHashTable (tTMO_HASH_TABLE *, UINT4, UINT4);
INT4                Bgp4HashTblAddEntry (tTMO_HASH_TABLE *, tRouteProfile *, 
                                         UINT4);
INT4                Bgp4PeerRtAdvHashTblAddEntry (tTMO_HASH_TABLE * ,
                                                  tRouteProfile * , UINT4 ,
                                                   UINT4 );
tRouteProfile      *Bgp4HashTblDeleteEntry (tTMO_HASH_TABLE *, tRouteProfile *,
                                            UINT4);
tRouteProfile      *Bgp4PeerRtAdvHashTblDeleteEntry (tTMO_HASH_TABLE * ,
                                                     tRouteProfile * ,
                                                      UINT4 *);
tRouteProfile      *Bgp4HashTblGetEntry (tTMO_HASH_TABLE *, tRouteProfile *,
                                         UINT4);
tRouteProfile      *Bgp4PeerRtAdvHashTblGetEntry (tTMO_HASH_TABLE * , tRouteProfile * ,
                                                  UINT4 *);
tRouteProfile      *Bgp4HashTblGetNextEntry (tTMO_HASH_TABLE *,tRouteProfile *,
                                             UINT1 *);
INT4                Bgp4DshConcatHashTableToSLL (tTMO_SLL *,
                                                 tTMO_HASH_TABLE *);
INT4                Bgp4DshCopyDLLToSLL (tTMO_SLL *, tBGP4_DLL *, UINT4,
                                         UINT4, UINT4, UINT4, UINT4);
INT4                Bgp4DshUpdateMetricInSLL (tTMO_SLL *, tBGP4_DLL *, UINT4,
                                              UINT4, UINT4, UINT4,UINT4, INT4);
INT4               Bgp4CheckForMatchType(UINT4, UINT1);
UINT4                Bgp4DshGetHashTableCnt (tTMO_HASH_TABLE *);

/* BGP4LOW_C */
UINT1               Bgp4EnableRRD (UINT4);
INT1               *RemoveLeadingSpaces (INT1 *);
tRouteProfile      *Bgp4GetRoute (UINT4, tNetAddress, tAddrPrefix, VOID *);
tBgp4PeerEntry     *Bgp4SnmphGetDuplicatePeerEntry (tBgp4PeerEntry *);
tBgp4PeerEntry     *Bgp4SnmphGetPeerEntry (UINT4, tAddrPrefix );
tBgp4PeerEntry     *Bgp4SnmphAddPeerEntry (UINT4, tAddrPrefix);
INT4                Bgp4SnmphDeletePeerEntry (tBgp4PeerEntry *);
tBgp4PeerEntry     *Bgp4SnmphClonePeerEntry (tBgp4PeerEntry *);
INT4                Bgp4SnmphDupPeerEntry (tBgp4PeerEntry *,
                                               tBgp4PeerEntry *);

INT4                Bgp4SetCapSupportedRowStatus (UINT4, tAddrPrefix , INT4 ,
                                    INT4 , tSNMP_OCTET_STRING_TYPE *,
                                    INT4 );
INT4                Bgp4SnmphEnableMPCapability (UINT4, tAddrPrefix,
                                                 UINT2, UINT1);
INT4                Bgp4SnmphDisableMPCapability (UINT4, tAddrPrefix,
                                                  UINT2, UINT1);
INT4                Bgp4SnmphDeletePeers(UINT2);
UINT1               Bgp4SnmphGetMPCapSupportStatus (UINT4, tAddrPrefix , UINT2 ,
                                                    UINT1);
INT4                Bgp4SnmphRtRefCapabilityEnable (UINT4, tAddrPrefix );
INT4                Bgp4SnmphOrfCapabilityEnable (UINT4, tAddrPrefix, UINT2, UINT1, UINT1, UINT1);
INT4                Bgp4SnmphOrfCapabilityDisable (UINT4, tAddrPrefix, UINT2, UINT1, UINT1, UINT1);
INT4                Bgp4SnmphGrCapabilityEnable (UINT4, tAddrPrefix);
INT4                Bgp4Snmph4ByteAsnCapabilityEnable (UINT4, tAddrPrefix);
INT4                Bgp4SnmphRtRefCapabilityDisable (UINT4, tAddrPrefix );
INT4                Bgp4SnmphGrCapabilityDisable (tAddrPrefix );
INT4                Bgp4Snmph4ByteAsnCapabilityDisable (tAddrPrefix );
UINT1               Bgp4SnmphGetRtRefCapSupportStatus (UINT4, tAddrPrefix);
UINT1               Bgp4SnmphGetOrfCapSupportStatus (UINT4, tAddrPrefix, UINT2, UINT1,  UINT1, UINT1);
UINT1               Bgp4SnmphGetGrCapSupportStatus (tAddrPrefix);
UINT1               Bgp4SnmphGet4ByteAsnCapSupportStatus (tAddrPrefix);
INT4                Bgp4SnmphDisableCapsForAllPeers(VOID);
INT1                Bgp4SnmphUpdateGrCapCode(tAddrPrefix);
INT4                Bgp4SnmphDeactivateCapabilyForPeers (UINT2, UINT2);

INT1
nmhGetRouteProfile (
                    UINT4  *,
                    INT4   *,
                    UINT4  *,
                    tBgp4PeerEntry * );




UINT1               Bgp4RRDDisableMsgToRTM (UINT4);
INT1                Bgp4SetFsbgp4MEDIntermediateAS (INT4, UINT1 *);
INT1                Bgp4Testv2Fsbgp4MEDIntermediateAS (UINT4 *, INT4, 
                                                       UINT1 *, INT4);
INT1                Bgp4SetFsbgp4LocalPrefIntermediateAS (INT4, UINT1 *);
INT1                Bgp4Testv2Fsbgp4LocalPrefIntermediateAS (UINT4 *, INT4, 
                                                             UINT1 *, INT4);
INT1                Bgp4SetFsbgp4UpdateFilterIntermediateAS (INT4, UINT1 *);
INT1                Bgp4Testv2Fsbgp4UpdateFilterIntermediateAS (UINT4 *, INT4,
                                                                UINT1 *, INT4);
INT1                nmhGetNextIndexBgpRtRefPeerTable (tAddrPrefix,
                                                      tAddrPrefix *);
INT1                nmhGetNextIndexBgpTCPMD5PeerTable (tAddrPrefix, 
                                                       tAddrPrefix *);

INT1      Bgp4CheckDuplicateIntermediateAS(UINT4 *, UINT2 );

/* BGP4UTILS_C */
UINT4               Bgp4IsRtsIdentical (tRouteProfile *, tRouteProfile *);
UINT1             * Bgp4PrintIpAddr (UINT1 *, UINT2 );
INT4                Bgp4PrintIp6Addr (UINT1 *, UINT1 *);
const INT1        * Bgp4PrintIpMask (UINT1 , UINT2 );
UINT1             * Bgp4PrintCodeName (UINT1 , UINT1 );
INT4                PrefixLessThan (tAddrPrefix , tAddrPrefix );
INT4                PrefixGreaterThan (tAddrPrefix , tAddrPrefix );
INT4                NetAddrMatch (tNetAddress , tNetAddress );
INT4                AddrMatch (tAddrPrefix , tAddrPrefix , UINT4 );
INT4                AddrMatchDefault (tAddrPrefix , tAddrPrefix , UINT4 , UINT4);
UINT1               AddrGreaterThan (tAddrPrefix , tAddrPrefix , UINT4 );
INT4                AddrCompare(tAddrPrefix , UINT4 , tAddrPrefix , UINT4 );
INT4                Bgp4GetIPV6Subnetmask (UINT2 , UINT1 *);
INT4                Bgp4PrintAddress (UINT4 , UINT4 , tAddrPrefix , UINT1 );
UINT4               Bgp4GetAfiSafiMask (UINT1 *);
INT4                Bgp4GetPeerNetworkAddress(tBgp4PeerEntry *, UINT2 ,
                                              tAddrPrefix    *);
INT4                Bgp4GetPeerGatewayAddress(tBgp4PeerEntry *, UINT2 ,
                                              tAddrPrefix    *);
BOOL1               Bgp4IsLocalroute (tRouteProfile *);
UINT4               BgpDefaultNetMask (UINT4);
INT4                Bgp4DumpBuf (UINT1 *, UINT4,UINT1);
UINT4               Bgp4GetSubnetmask (UINT1);
UINT1               Bgp4GetSubnetmasklen (UINT4);
INT4                Bgp4GetTcpAoMKTPasswd(UINT4 , INT4 , tSNMP_OCTET_STRING_TYPE * );
INT4                Bgp4ValidateAsNumber(CONST CHR1 *, UINT4 *);
INT4                Bgp4GetSpeakerPeer4ByteAsnCapability (tBgp4PeerEntry *);
INT4                Bgp4PrintIntermediateAs (CONST CHR1 * ,CHR1 *);


/* BG4PUTILS_MI */
INT4  BgpSetContext(INT4);
VOID  BgpReleaseContext (VOID);
INT4  BgpResetContext (VOID);
INT4  BgpGetFirstActiveContextID (UINT4*);
INT4  BgpGetNextActiveContextID (UINT4, UINT4*);
INT4  BgpSetContextId(INT4);
UINT4 BgpGetContextId(VOID);
UINT4 BgpGetTableVersion(UINT4);

#ifdef BGP4_IPV6_WANTED
UINT1               Bgp4GetIpv6Subnetmasklen (UINT1 *);
#endif
UINT1               Bgp4ConfIsValidAddress (tAddrPrefix);
UINT1               Bgp4IsValidAddress (tAddrPrefix, UINT1);
UINT1               Bgp4IsValidNextHopAddress (tAddrPrefix, UINT1);
UINT1               Bgp4IsValidRouteAddress (tNetAddress );
UINT1               Bgp4IsValidPrefixLength (tNetAddress);
INT4                Bgp4GetAfiSafiIndex (UINT2 , UINT2 , UINT4 *);
INT4                Bgp4GetAfiSafiMaskFromIndex (UINT4, UINT4 *);
INT4                PrefixMatch (tAddrPrefix , tAddrPrefix );
INT4                Bgp4GetPrefixHashIndex (tAddrPrefix , UINT4 *);
INT4                Bgp4GetRouteHashIndex (tAddrPrefix , UINT1 , UINT4 *);
INT4                Bgp4CopyAddrPrefixStruct (tAddrPrefix *, tAddrPrefix );
INT4                Bgp4CopyNetAddressStruct (tNetAddress *, tNetAddress );
INT4                Bgp4CopyInfoToMpeInfo (tBgp4Info * , tBgp4Info * );
INT4                Bgp4CopyPrefix (tAddrPrefix *, tAddrPrefix );
INT4                Bgp4GetAfiSafiFromIndex (UINT4 , UINT2 *, UINT2 *);
INT4                Bgp4GetAfiSafiFromMask (UINT4 , UINT2 *, UINT2 *);
UINT2               Bgp4GetMaxPrefixLength (tAddrPrefix );
#ifdef L3VPN
INT4                Bgp4CopyVpn4NetAddressFromRt(tNetAddress *,
                                                 tRouteProfile *);
INT4                Bgp4CopyVpn4NetAddressToRt(tRouteProfile *, tNetAddress );
INT4                Vpnv4AddrMatch (tAddrPrefix , tRouteProfile* , UINT4);

#endif
INT4                Bgp4SnmphActivateCapabilyForPeers (UINT2, UINT2);
INT4                Bgp4GetPrefixForPrefixLen(tNetAddress *, UINT2 );

tRouteProfile *Bgp4DuplicateRouteProfile (tRouteProfile *);
tBgp4Info     * Bgp4DuplicateBgp4Info (tBgp4Info *pBgp4Info);
tRouteProfile * Bgp4DupCompleteRtProfile (tRouteProfile *pRtProfile);
INT4 Bgp4FindDupCommunity(UINT4 *au4Community,UINT2 *u2CommunityCnt);
/* BGRTREF_C */
INT4  Bgp4RtRefMsgFormHandler (tBgp4PeerEntry *, UINT2 , UINT1, UINT1); 
UINT1 *Bgp4RtRefConstructRouteRefMsg ( UINT2 , UINT1 );
INT4  Bgp4RtRefMsgRcvdHandler ( tBgp4PeerEntry *, UINT1 *, UINT4);
INT4  Bgp4RtRefRequestHandler(UINT4, tAddrPrefix, UINT2, UINT2, UINT1);
INT4  Bgp4RtRefPendHandler (tBgp4PeerEntry *);
INT4  Bgp4IsAfiSafiReqAlreadyPending (tBgp4PeerEntry *, UINT4);
INT4  Bgp4IsAfiSafiOutboundReqPending (tBgp4PeerEntry *, UINT4);

/* BGPORF_C */
INT4 Bgp4OrfRequestHandler (UINT4 u4Context, tAddrPrefix PeerAddr,
                            UINT2 u2Afi, UINT2 u2Safi);
INT4 Bgp4OrfGetOrfCapMask (UINT4 u4AsafiMask, UINT1 u1OrfMode,
                           UINT4 *pu4OrfCapMask);
INT4 Bgp4OrfMsgRcvdHandler (tBgp4PeerEntry * pPeerInfo, UINT1 *pu1OrfMsgBuf, 
                            INT4 i4BufLen, UINT2 u2Afi, UINT1 u1Safi);
INT4 BgpOrfListTableRBCmp (tBgp4RBElem * pRBElem1, tBgp4RBElem * pRBElem2);
INT4 BgpOrfProcessRcvdOrfEntry (tBgp4OrfEntry *, UINT1);
VOID BgpOrfDelRcvdOrfEntries (tBgp4PeerEntry *pPeerEntry);
UINT4 BgpOrfFillOrfEntries (tBgp4PeerEntry *pPeerInfo, UINT1 *pu1OrfMsgBuf, UINT2 u2Afi);
INT4 BgpOrfIsAddrPrefixMatching (tNetAddress *pNetAddress, tBgp4OrfEntry *pOrfEntry);
VOID Bgp4OrfReleaseOrfEntries (UINT4 u4ContextId);

/* BGP4PEER_C */
INT4  Bgp4PeerAddMsgToTxQ (tBgp4PeerEntry *, UINT1 *, UINT4, UINT1 );
INT4  Bgp4ProcessPeerTransmissions (UINT4);
UINT4 Bgp4PeerCanInitRouteBeAdvt (tBgp4PeerEntry *, tRouteProfile *);
INT4  Bgp4PeerProcessSoftInitRoutes (tBgp4PeerEntry *, UINT4 );
INT4  Bgp4PeerProcessInitRoutes (tBgp4PeerEntry *, UINT4 );
INT4  Bgp4PeerProcessDeInitRoutes (tBgp4PeerEntry *, UINT4, UINT4);
INT4  Bgp4PeerSendRoutesToPeerTxQ (tBgp4PeerEntry *, tTMO_SLL  *, tTMO_SLL  *,
                                   UINT1 );
INT4  Bgp4PeerReTxmMsg(tBgp4PeerEntry *);
INT4  Bgp4PeerUpdPeerInitRtInfo (tRouteProfile * , VOID *, INT4 );
INT4  Bgp4PeerCreatePeerEntry (UINT4, tAddrPrefix );
INT4  Bgp4PeerEbgpMultihopHandler (tBgp4PeerEntry *);
INT4  Bgp4DeletePeer (tBgp4PeerEntry *);
INT4  Bgp4EnablePeer (tBgp4PeerEntry *);
INT4  Bgp4DisablePeer (tBgp4PeerEntry *);
INT4  Bgp4ClearPeer (tBgp4PeerEntry *);
INT4  Bgp4PeerStartPeer (UINT4, tAddrPrefix);
INT4  Bgp4PeerStopPeer (UINT4, tAddrPrefix);
INT4  Bgp4PeerSetAllActivePeerAdminStatus (UINT4, INT4);
INT4  Bgp4PeerUpdateDeInitRoutesToPeerTxQ(tBgp4PeerEntry *);
UINT1 Bgp4DeleteRouteFromPeer (tNetAddress , tAddrPrefix );
INT4  Bgp4DshReleasePeerAsafiRtLists (tBgp4PeerEntry * , UINT4 ,
                                      UINT4, UINT4 *);
INT4  Bgp4DshReleasePeerRtLists (tBgp4PeerEntry *, UINT4);
INT4  Bgp4PeerHandleClearIpBgp (UINT4, tAddrPrefix );
INT4  Bgp4PeerAdvtDftRoute (tBgp4PeerEntry * , UINT1 , UINT1 );

INT4 Bgp4ValidateBgpPeerTableIndex (tAddrPrefix );
INT4 Bgp4GetFirstIndexBgpPeerTable (UINT4, tAddrPrefix *);
INT4 Bgp4GetNextIndexBgpPeerTable (UINT4, tAddrPrefix , tAddrPrefix *);
INT4 Bgp4GetBgpPeerTableIntegerObject (UINT4, tAddrPrefix , INT4  , INT4 *);
INT4 Bgp4GetBgpPeerTableUnsignIntObject (UINT4, tAddrPrefix , INT4  , UINT4 *);
INT4 Bgp4SetBgpPeerTableIntegerObject (UINT4, tAddrPrefix , INT4 , INT4 );
INT4 Bgp4TestBgpPeerTableIntegerObject (UINT4, tAddrPrefix , INT4 ,
                                        INT4 , UINT4 *);
INT4 Bgp4ValidatePathAttrTableIndex (tNetAddress , tAddrPrefix);
INT4 Bgp4GetFirstPathAttrTableIndex (UINT4, tNetAddress *, tAddrPrefix *);
INT4 Bgp4GetNextPathAttrTableIndex (UINT4, tNetAddress , tAddrPrefix ,
                                    tNetAddress *, tAddrPrefix *);
INT4 Bgp4MpGetFirstPathAttrTableIndex (UINT4, UINT4, tNetAddress *,
                                    tAddrPrefix *);

INT4 Bgp4MpGetNextPathAttrTableIndex (UINT4, tNetAddress , tAddrPrefix ,
                                   tNetAddress *, tAddrPrefix *);
INT1 Bgp4GetNextRouteProfile (tNetAddress *, tAddrPrefix *, tBgp4PeerEntry *);
INT1 Bgp4GetNextIpv4RouteProfile (tNetAddress *, tAddrPrefix *, 
                                  tBgp4PeerEntry *);
INT4 Bgp4GetPathAttrTableIntegerObject (UINT4, tNetAddress , tAddrPrefix ,
                                        INT4 , INT4 *);
INT4 Bgp4Ipv4GetFirstIndexBgpPeerTable (UINT4, tAddrPrefix *);
INT4 Bgp4Ipv4GetFirstPeerPathAttrTableIndex (tNetAddress *, tAddrPrefix *,
                                             tBgp4PeerEntry *);
INT4 Bgp4Ipv4GetNextIpv4PathAttrTableIndex (UINT4, tNetAddress , tAddrPrefix ,
                                            tNetAddress *, tAddrPrefix *);
INT4 Bgp4ValidateFsbgp4PeerExtTable (tAddrPrefix );
INT4 Bgp4GetFirstIndexFsbgp4PeerExtTable (UINT4, tAddrPrefix *);
INT4 Bgp4GetNextIndexFsbgp4PeerExtTable (UINT4, tAddrPrefix , tAddrPrefix *);
INT4 Bgp4GetFsBgp4PeerExtTableIntegerObject (UINT4, tAddrPrefix , INT4  , INT4 *);
INT4 Bgp4SetFsBgp4PeerExtTableIntegerObject (UINT4, tAddrPrefix , INT4  , INT4 );
INT4 Bgp4TestFsBgp4PeerExtTableIntegerObject (UINT4, tAddrPrefix , INT4  ,
                                              INT4 , UINT4 *);
INT4 Bgp4Ipv4GetFirstFsbgp4ImportTableIndex (tNetAddress *, INT4 *,
                                             tAddrPrefix *, INT4 *, INT4 *);

/*TCP-AO*/
INT4 Bgp4GetFirstTCPAOKeyId(UINT4 u4Context, tAddrPrefix PeerAddress, INT4* pi4TCPAOKeyId);
INT4 Bgp4GetNextTCPAOKeyId(UINT4 u4Context, tAddrPrefix PeerAddress, INT4 i4CurrentTCPAOKeyId, INT4* pi4NextTCPAOKeyId );
#ifdef BGP4_IPV6_WANTED
INT1 Bgp4GetNextIpv6RouteProfile (tNetAddress *, tAddrPrefix *, 
                                  tBgp4PeerEntry *);
INT4 Bgp4Ipv6GetFirstIndexBgpPeerTable (UINT4, tAddrPrefix *);
INT4 Bgp4Ipv6GetNextIndexBgpPeerTable (UINT4, tAddrPrefix , tAddrPrefix *);
INT4 Bgp4MaskIpv6Address (UINT1 *, UINT1 *);
INT4 Bgp4Ipv6ConfIsValidAddr(tAddrPrefix *);
INT4 Bgp4Ipv6IsValidAddr(tAddrPrefix *, UINT1);
INT4 Bgp4Ipv6GetFirstFsbgp4ImportTableIndex (tNetAddress *, INT4 *,
                                             tAddrPrefix *, INT4 *, INT4 *);
INT4 Bgp4Ipv6GetFirstPeerPathAttrTableIndex (tNetAddress *, tAddrPrefix *,
                                             tBgp4PeerEntry *);
INT4 Bgp4Ipv6GetNextIpv6PathAttrTableIndex (UINT4, tNetAddress , tAddrPrefix ,
                                            tNetAddress *, tAddrPrefix *);

#endif
INT4 Bgp4ValidateFsbgp4ImportTableIndex (tNetAddress ,
                                         INT4 , tAddrPrefix , INT4 , INT4,
                                         tSNMP_OCTET_STRING_TYPE *);
INT4 Bgp4GetFirstIndexFsbgp4ImportTable (tNetAddress *, INT4 *, 
                                         tAddrPrefix *, INT4 *, INT4 *,
                                         tSNMP_OCTET_STRING_TYPE *);
INT4 Bgp4GetNextIndexFsbgp4ImportTable (tNetAddress , INT4 , tAddrPrefix ,
                                   INT4 , INT4 , tSNMP_OCTET_STRING_TYPE*,
                                   tNetAddress *, INT4 *, tAddrPrefix *,
                                   INT4 *, INT4 *, tSNMP_OCTET_STRING_TYPE*);
INT4 Bgp4GetFsbgp4ImportTableRouteAction (UINT4, tNetAddress , INT4 , tAddrPrefix ,
                                          INT4 , INT4  ,
                                          tSNMP_OCTET_STRING_TYPE*, INT4 *);
INT4 Bgp4SetFsbgp4ImportTableRouteAction (tNetAddress , INT4, tAddrPrefix ,
                                          INT4 , INT4 ,
                                          tSNMP_OCTET_STRING_TYPE*, INT4 );
INT4 Bgp4TestFsbgp4ImportTableRouteAction (tNetAddress , INT4 , tAddrPrefix ,
                                  INT4 , INT4 ,
                                  tSNMP_OCTET_STRING_TYPE*, INT4 , UINT4 *);
INT4 Bgp4ValidateFsbgp4FsmTransHistTableIndex (UINT4, tAddrPrefix );
INT4 Bgp4GetFirstIndexFsbgp4FsmTransHistTable (UINT4, tAddrPrefix *);
INT4 Bgp4GetNextIndexFsbgp4FsmTransHistTable (UINT4, tAddrPrefix , tAddrPrefix *);
INT4 Bgp4ValidateFsbgp4RflPeerReflectTableIndex (UINT4, tAddrPrefix );
INT4 Bgp4ValidateFsbgp4RflRouteReflectTableIndex (tNetAddress , tAddrPrefix );
INT4 Bgp4GetFirstIndexFsbgp4RflRouteReflectTable (UINT4, tNetAddress *, tAddrPrefix *);
INT4 Bgp4GetNextIndexFsbgp4RflRouteReflectTable (UINT4, tNetAddress , tAddrPrefix ,
                                                 tNetAddress *, tAddrPrefix *);
INT4 Bgp4ValidateFsbgp4RfdDampHistTableIndex (tNetAddress, tAddrPrefix);
INT4 Bgp4GetFirstIndexFsbgp4RfdDampHistTable (UINT4, tNetAddress *, tAddrPrefix *);
INT4 Bgp4GetNextIndexFsbgp4RfdDampHistTable (UINT4, tNetAddress , tAddrPrefix ,
                                              tNetAddress *, tAddrPrefix *);
INT4 Bgp4GetFsbgp4RfdDampHistTableIntegerObject (UINT4, tNetAddress , tAddrPrefix ,
                                                  INT4 , INT4 *);
INT4 Bgp4ValidateFsbgp4PeerDampHistTableIndex (tAddrPrefix );
INT4 Bgp4GetFirstIndexFsbgp4PeerDampHistTable (UINT4, tAddrPrefix *);
INT4 Bgp4GetNextIndexFsbgp4PeerDampHistTable (UINT4, tAddrPrefix , tAddrPrefix *);
INT4 Bgp4GetFsbgp4PeerDampHistTableIntegerObject (UINT4, tAddrPrefix , INT4 , INT4 *);
INT4 Bgp4ValidateFsbgp4RfdRtsReuseListTableIndex (tNetAddress , tAddrPrefix);
INT4 Bgp4GetFirstIndexFsbgp4RfdRtsReuseListTable (UINT4, tNetAddress *, tAddrPrefix *);
INT4 Bgp4GetNextIndexFsbgp4RfdRtsReuseListTable (UINT4, tNetAddress , tAddrPrefix ,
                                       tNetAddress *, tAddrPrefix *);
INT4 Bgp4GetFsbgp4RfdRtsReuseListTableIntegerObject (UINT4, tNetAddress ,
                                           tAddrPrefix, INT4 , INT4 *);
INT4 Bgp4ValidateFsbgp4RfdPeerReuseListTableIndex (tAddrPrefix );
INT4 Bgp4GetFirstIndexFsbgp4RfdPeerReuseListTable (UINT4, tAddrPrefix *);
INT4 Bgp4GetNextIndexFsbgp4RfdPeerReuseListTable (UINT4, tAddrPrefix , tAddrPrefix *);
INT4 Bgp4GetFsbgp4RfdPeerReuseListTableIntegerObject (UINT4, tAddrPrefix,
                                                      INT4, INT4 *);
INT4 Bgp4ValidateCommRouteAddCommTableIndex (tNetAddress , UINT4 );
INT4 Bgp4GetFirstIndexCommRouteAddCommTable (UINT4, tNetAddress *, UINT4 *);
INT4 Bgp4GetNextIndexCommRouteAddCommTable (UINT4, tNetAddress  , UINT4 ,
                                            tNetAddress *, UINT4 *);
INT4 Bgp4ValidateCommRouteDelCommTableIndex (tNetAddress , UINT4 );
INT4 Bgp4GetFirstIndexCommRouteDelCommTable (UINT4, tNetAddress *, UINT4 *);
INT4 Bgp4GetNextIndexCommRouteDelCommTable (UINT4, tNetAddress  , UINT4 ,
                                            tNetAddress *, UINT4 *);
INT4 Bgp4ValidateCommRouteCommSetStatusTableIndex (tNetAddress );
INT4 Bgp4GetFirstIndexCommRouteCommSetStatusTable (UINT4, tNetAddress *);
INT4 Bgp4GetNextIndexCommRouteCommSetStatusTable (UINT4, tNetAddress , tNetAddress *);

INT4 CommValidateCommunity (UINT4 );
INT4 CommCopyCommunityToProfile (tCommProfile * , UINT4 );
INT4 CommCmpCommunityToProfile (tCommProfile * , UINT4 );
INT4 CommCmpCommunity (UINT4 , tCommProfile * );
INT4 CommCreateInputFilterEntry (UINT4, UINT4 , tCommFilterInfo ** );
INT4 CommGetInputFilterEntry (UINT4, UINT4 , tCommFilterInfo ** );
INT4 CommCreateOutputFilterEntry (UINT4, UINT4 , tCommFilterInfo ** );
INT4 CommGetOutputFilterEntry (UINT4, UINT4 , tCommFilterInfo ** );
INT4 CommCreateAddCommEntry (UINT4, tNetAddress, UINT4 , tCommProfile ** );
INT4 CommDeleteAddCommEntry (UINT4, tNetAddress, UINT4 , tCommProfile ** );
INT4 CommGetAddCommEntry (UINT4, tNetAddress, UINT4 , tCommProfile ** );
INT4 CommCreateDeleteCommEntry (UINT4, tNetAddress, UINT4 , tCommProfile ** );
INT4 CommDeleteDeleteCommEntry (UINT4, tNetAddress, UINT4 , tCommProfile ** );
INT4 CommGetDeleteCommEntry (UINT4, tNetAddress, UINT4 , tCommProfile ** );
INT4 CommGetRouteCommSetStatusEntry (UINT4, tNetAddress, tRouteCommSetStatus ** );
INT4 CommCreateRouteCommSetStatusEntry (UINT4, tNetAddress, tRouteCommSetStatus ** );
INT4 Bgp4LowGetRouteAddress (INT4 , INT4 , tSNMP_OCTET_STRING_TYPE *, 
                                    tNetAddress *);
INT4 Bgp4SnmphGetNetAddress (INT4 , INT4 , tSNMP_OCTET_STRING_TYPE *, 
                        tAddrPrefix , tNetAddress *);
INT4 Bgp4LowSetRouteAddress (INT4 , INT4 , tSNMP_OCTET_STRING_TYPE *, 
                                    tNetAddress );
INT4 Bgp4LowGetIpAddress (INT4 , tSNMP_OCTET_STRING_TYPE *, 
                                 tAddrPrefix *);
INT4 Bgp4LowSetIpAddress (INT4 , tSNMP_OCTET_STRING_TYPE *, 
                                 tAddrPrefix );
INT4 Bgp4LowSetIpAddressExt (INT4 , tSNMP_OCTET_STRING_TYPE *, 
                                 tAddrPrefix );

INT4 Bgp4SnmphCopyNetAddr (tRouteProfile *, tBgp4PeerEntry *,
                        tNetAddress *);

INT4    Bgp4IfacePeerAdminChange (UINT4, UINT1);
INT4    Bgp4GetIfIndexFromIfAddr(UINT4 , UINT4 *);
INT4    Bgp4GetIfInfoFromIfIndex(UINT4 , tBgp4IfInfo **);
INT4    Bgp4GetIfInfoFromIfAddr (UINT4 , tBgp4IfInfo **); 
INT4    Bgp4SetBfdStatus (UINT4 u4ContextId, tBgp4PeerEntry *pPeerEntry);
INT4    Bgp4ProcessPeerDownNotification (tBgp4PeerEntry *pPeerEntry, BOOL1);

#ifdef L3VPN
/* Prototypes defined for Carrying Label information module
 * and BGP/MPLS VPN module
 */
INT4    Bgp4Vpnv4Init (VOID);
INT4    Bgp4Vpnv4Shutdown (VOID);
INT4    Bgp4MpeProcessVpnv4Nexthop (tBgp4PeerEntry *, UINT1 *, tBgp4Info *,
                                   UINT2 *);
INT4    Bgp4MpeProcessVpnv4Prefix ( UINT1 *, tRouteProfile  *, UINT2  *);
INT4    Bgp4Vpnv4LinkMatchingImportRTVrfs (UINT1 *, tRouteProfile *);
tBgp4RrImportTargetInfo * Bgp4Vpnv4IsRTMatchInRRImportRTList (UINT1 *);
tBgp4RrImportTargetInfo * Bgp4AddNewRTInRRImportList (UINT1 *); 
INT4    Bgp4Vpnv4ApplyCEToPERTFilter (tTMO_SLL *, tBgp4PeerEntry *,
                                       tTMO_SLL *);
INT4    Bgp4Vpnv4ApplyImportRTFilter (tTMO_SLL *, tBgp4PeerEntry  *);
INT4    Bgp4GetVrfIdFromIfIndex (UINT1 , UINT4 *);
INT4    Bgp4Vpnv4GetVrfHashKey (UINT4, UINT1 *);
INT4    Bgp4Vpnv4GetLabelInfo (tRouteProfile *, UINT1 *, UINT4 *);
INT4    Bgp4Vpnv4GetVrfFlags (tRouteProfile *, UINT4 , UINT4 *);
INT4    Bgp4Vpnv4SetVrfFlags (tRouteProfile *, UINT4 , UINT4); 
INT4    Bgp4Vpnv4ResetVrfFlags (tRouteProfile *, UINT4 , UINT4); 
INT4    Bgp4Vpnv4GetFirstInstallVrfInfo (tRouteProfile *,tRtInstallVrfInfo **);
INT4    Bgp4Vpn4ConvertRtToVpn4Route (tRouteProfile *, UINT4);
INT4    Bgp4Vpnv4IsSameVrfInfo (tRouteProfile *, tRouteProfile *);
tBgp4RrImportTargetInfo* Bgp4MemAllocateRrImportTarget (UINT4 );
tRtInstallVrfInfo* Bgp4MemAllocateVpnRtInstallVrfNode (UINT4 );
INT4    Bgp4UpdateVrfChgInitInfo (tRouteProfile * );
INT4    Bgp4Vpnv4RrVpnJoin (VOID); 
INT4    Bgp4Vpnv4RrPrune (tRouteProfile *);
INT4    Bgp4Vpnv4RrDeleteTargetFromSet (VOID);
INT4    Bgp4Vpnv4LinkVrfIdToRouteProfile (tRouteProfile *, UINT4 );
INT4    Bgp4Vpnv4GetInstallVrfInfo (tRouteProfile *, UINT4 ,
                                    tRtInstallVrfInfo **);
INT1    Bgp4Vpnv4GetRTFromVrf (UINT4, UINT1 *, UINT1 ,
                             tExtCommProfile **);
INT4    Bgp4Vpnv4DeleteRtFromVrfs (tRouteProfile *);
INT4    Bgp4Vpnv4GetBgpVrfRibNode (UINT4 , VOID **);
INT4    Bgp4Vpnv4ProcessFeasWdrawRts (VOID);
INT4    Bgp4Vpnv4ProcessWithdRoute (tRouteProfile * , tBgp4PeerEntry * );
INT4    Bgp4Vpnv4ProcessFeasibleRoute (tRouteProfile * , tBgp4PeerEntry * , UINT4 );
INT4    Bgp4Vpnv4ProcessNonBgpWithdRoute (tRouteProfile * ,UINT4 );
INT4    Bgp4Vpnv4ProcessNonBgpFeasRoute (tRouteProfile * , UINT4 );
INT4    Bgp4Vpnv4DelRouteFromVPNRIB (tRouteProfile * );
INT4    Bgp4ClFillIpLabelInUpdMsg (UINT1 *, tRouteProfile *,
                                   tBgp4AdvtPathAttr *,
                                   tBgp4PeerEntry *,UINT2 *);
INT4    Bgp4ClProcessIpLabelInUpdMsg (tBgp4PeerEntry *, UINT1 *, UINT1 ,
                                      UINT1 , tRouteProfile *, UINT1 *);
INT4    Bgp4UpdateLspStatusforNextHop (tBgp4IgpMetricEntry *);
INT4    Bgp4GetIgpLspFromMplsForNextHop (tBgp4IgpMetricEntry *,
                UINT4 *, UINT1 *);
INT4    Bgp4GetAlternateLspFromMplsForNextHop (tBgp4IgpMetricEntry *,
                UINT4 *, UINT1 *);
INT4    Bgp4LspChngHandler (UINT4, UINT1 );
INT4    Bgp4RsvpTeLspChngHandler (UINT4,UINT1);
INT4    Bgp4LspUpHandler(tBgp4IgpMetricEntry *);
INT4    Bgp4LspDownHandler(tBgp4IgpMetricEntry *);
INT4    Bgp4ProcessLspDown (tBgp4IgpMetricEntry *);
INT4    Bgp4Vpnv4IsRouteCanBeProcessed (tRouteProfile *);
INT4    Bgp4GetVpn4Subnetmask (UINT2 , UINT1 *);
INT4    Bgp4MaskVpn4Address (UINT1 *, UINT1 *);

UINT4   Bgp4Vpnv4ReleaseRouteTargetsFromVrf (tTMO_SLL * , UINT1);
INT4    Bgp4VrfChangeHandler (UINT4, UINT1 );
INT4    Bgp4Vpnv4VrfCreate (UINT4);
INT4    Bgp4Vpnv4VrfDelete (UINT4);
tBgp4IfInfo  *Bgp4IfGetEntryByVrfId (UINT4);
INT4    Bgp4Vpnv4VrfUpHdlr  (UINT4, UINT1);
INT4    Bgp4Vpnv4VrfDownHdlr  (UINT4, UINT1);
INT4    Bgp4Vpnv4VrfInit  (UINT4);
INT4    Bgp4Vpnv4VrfDeInit  (UINT4, UINT1);
INT4    Bgp4Vpnv4VrfToVrfRedistribute(tTMO_SLL *);
INT4    Bgp4Vpnv4CEPeerInit (tBgp4PeerEntry *, UINT4 );
INT1    Bgp4Vpn4CheckAdvtToCEPeer (tRouteProfile *, tBgp4PeerEntry *);
INT4    Bgp4Vpnv4SetFlagsForAllVrf (tRouteProfile *, UINT4 );
INT4    Bgp4Vpnv4ResetFlagsForAllVrf (tRouteProfile *, UINT4 );
INT4    Bgp4Vpn4VpnJoinSendRtRefresh(tBgp4PeerEntry *);
INT4    Bgp4Vpn4VrfImportRouteTargetChngHandler (UINT4, tExtCommProfile *);
INT4    Bgp4Vpnv4UpdPeerInitRtInfo (tRouteProfile * , UINT4 , INT4 );
INT4    Bgp4Vpnv4FillVpnExtComm (tRouteProfile  *, tBgp4PeerEntry *,
                                 tBgp4Info *);
INT4     Bgp4Vpnv4RouteTargetChgHandler (UINT4, INT4,
                                                  UINT1 *, UINT1);
tExtCommProfile * Bgp4Vpnv4GetRouteTargetFromVrf (UINT4, UINT1 *, UINT1);
tExtCommProfile * Bgp4Vpnv4GetNextRouteTargetFromVrf (UINT4,
                                                      UINT1 *, UINT1);
INT4    Bgp4Vpnv4SetNeighborRole(tBgp4PeerEntry *, INT4 );
INT4    Bgp4Vpnv4ExportTargetChgHandler (UINT4);
INT4    Bgp4Vpnv4ProcessVrfChngList (VOID);
INT4    Bgp4Vpn4ProcessVrfDown(UINT4);
INT4    Bgp4Vpnv4ApplyVrfExpTgtChg (UINT4);
INT4    Bgp4Vpnv4DelRtFromVrfs (tRouteProfile *);
/* BGVPN_LOW */
INT4    Bgp4Vpnv4ParseExtCommunity (UINT1 *, INT4  , UINT1 *, UINT1 );
INT4    Bgp4Vpnv4CopyExtCom (UINT1 *, INT4 *, UINT1 *, UINT1 );
INT4    Bgp4Vpnv4SendRtRefToPEPeers (VOID);
INT4    Bgp4Vpnv4PostExpTgtChgEvent (UINT4);
INT4    Bgp4Vpnv4GetFirstPathAttrTableIndex (tNetAddress *, tAddrPrefix *);
INT4    Bgp4Vpnv4GetNextPathAttrTableIndex (tNetAddress , tAddrPrefix ,
                                           tNetAddress *, tAddrPrefix *);
INT4    Bgp4Vpnv4GetFirstPeerPathAttrTableIndex (tNetAddress *,
                                             tAddrPrefix *, tBgp4PeerEntry *);
INT4    Bgp4Vpnv4GetNextIpv4PathAttrTableIndex (tNetAddress , tAddrPrefix ,
                                               tNetAddress *, tAddrPrefix *);
INT1    Bgp4GetNextVpn4RouteProfile (tNetAddress *, tAddrPrefix *,
                                     tBgp4PeerEntry *);
INT4    Bgp4IgpMetricTblGetEntryForLspId (UINT4 , tBgp4IgpMetricEntry ** );
INT4    Bgp4GetBgp4Label (UINT4 *);
INT4    Bgp4DelBgp4Label (UINT4 );
INT4    Bgp4UpdateLabelInMplsFmTable (tBgp4PeerEntry *, UINT4 , UINT1 , UINT2 );
INT4    Bgp4UpdateILMEntryInFmTable (UINT4 , UINT1 , UINT2 , UINT4);
INT4    Bgp4UpdateFMTableForLbldInterface (tBgp4PeerEntry *, UINT1 , UINT2 );
INT4    Bgp4UpdateFMTableForVpnInterface(tBgp4PeerEntry *, UINT1 , UINT4 );
INT4    Bgp4MplsLabelUpdateInFmTable (UINT4 , UINT4 , UINT4 , UINT2 , UINT1 );

VOID    Bgp4UpdateRouteInMplsDb (tRouteProfile *, UINT4, UINT1, UINT1);
INT4    Bgp4ProcessLspChngList (VOID);
INT4    Bgp4Vpn4GetNextIpv4PathAttrTableIndex (tNetAddress,
                                               tAddrPrefix,
                                               tNetAddress *,
                                               tAddrPrefix *);
INT4    Bgp4RegisterWithMpls (VOID);
INT4    Bgp4DeRegisterWithMpls (VOID);
INT4
Bgp4Vpn4AddRT (tTMO_SLL *pRTComList, UINT1 *pu1RouteParam, INT4 i4RTType);
VOID
Bgp4Vpnv4SendVpnv4RtsToPeer (UINT4 u4Context);

INT4    Bgp4Vpnv4GetLeakRoutesFromPeer (UINT4);
INT4    Bgp4Vpnv4VrftoVrfRouteLeak (tRouteProfile *);
VOID    Bgp4Vpnv4DelLeakRoutesFromPeer (UINT4);
#endif
#ifdef EVPN_WANTED
INT4    Bgp4RegisterWithVxlan(VOID);
INT4    Bgp4DeRegisterWithVxlan(VOID);
INT4    Bgp4EvpnRouteTargetChgHandler (UINT4, UINT4, INT4,
                                           UINT1, UINT1 *);
INT4    Bgp4EvpnAddRT (tTMO_SLL *pRTComList,
                          UINT1 *pu1RouteParam, INT4 i4RTType);
tExtCommProfile * Bgp4EvpnGetRouteTargetFromVrf (UINT4, UINT4, UINT1 *, UINT1);
INT4    Bgp4EvpnSendRtRefreshToPEPeers(VOID);
INT4    Bgp4EvpnVrfChangeHandler (UINT4, UINT4, UINT1 );
INT4    Bgp4EvpnNotifyMacHandler (UINT4, UINT4, tMacAddr, UINT1, UINT1 *, UINT1, UINT1);
INT4    Bgp4EvpnProcessVrfDown(UINT4);
INT4    Bgp4EvpnVrfUpHdlr  (UINT4, UINT4, UINT1);
INT4    Bgp4EvpnVrfCreate (UINT4, UINT4);
INT4    Bgp4EvpnVrfDownHdlr  (UINT4, UINT4, UINT1);
INT4    Bgp4EvpnVrfInit  (UINT4, UINT4);
INT4    Bgp4EvpnVrfDeInit  (UINT4, UINT4, UINT4);
INT4    Bgp4GetVrfVniCurrentState (UINT4, UINT4, UINT4 *);
INT4    Bgp4SetVrfVniCurrentState (UINT4, UINT4, UINT4);
VOID    Bgp4SendEvpnRtsToPeer (UINT4 u4VrfId, UINT4 u4VNId);
INT4    Bgp4IsEvpnRouteCanBeProcessed (tRouteProfile *);
BOOL1   Bgp4IsSameEvpnRoute (tRouteProfile *, tRouteProfile*);
INT4    Bgp4EvpnUpdtRouteInIPVRF (tRouteProfile *);
INT4    Bgp4EvpnGetPathAttrinfo (tBgp4Info *, UINT4);
INT4    Bgp4EvpnConstructRouteProfile (UINT4, tRouteProfile *, tEvpnNlriInfo *);
UINT4   Bgp4EvpnReleaseRouteTargetsFromVrf (tTMO_SLL * , UINT1);
INT4    Bgp4EvpnApplyImportRTFilter (tTMO_SLL *,tBgp4PeerEntry *);
tEvpnVrfNode * Bgp4EvpnLinkMatchingImportRTVrfs (UINT1 *, tRouteProfile *);
tEvpnVrfNode * Bgp4EvpnLinkMatchingEthRoute(UINT4, tRouteProfile *, UINT1 *);
INT4    Bgp4EvpnCreatMacMobCommunity (UINT1 *, tExtCommunity *, tBgp4Info *);
tBgp4EvpnRrImportTargetInfo * Bgp4EvpnIsRTMatchInRRImportRTList (UINT1 *);
tBgp4EvpnRrImportTargetInfo * Bgp4EvpnAddNewRTInRRImportList (UINT1 *);
INT4    Bgp4EvpnRrJoin(VOID);
INT4    Bgp4EvpnRrDeleteTargetFromSet(VOID);
tBgp4EvpnRrImportTargetInfo * Bgp4MemAllocateEvpnRrImportTarget(UINT4);
INT4    Bgp4MemReleaseEvpnRrImportTarget (tBgp4EvpnRrImportTargetInfo *);
INT4    Bgp4MpeProcessEvpnPrefix (UINT1 *, tRouteProfile  *, UINT2 *);
INT4    Bgp4EvpnCreateWithdMsgforVXLAN(tRouteProfile *);
INT4    Bgp4EvpnCreateAdvtMsgforVXLAN(tRouteProfile *);
INT4    Bgp4EvpnFillExtComm (tRouteProfile * pRouteProfile,tBgp4PeerEntry * pPeerEntry, tBgp4Info * pAdvtBgpInfo);
INT4    Bgp4EvpnInit (VOID);
VOID    Bgp4EvpnIndicateRtToPeers (UINT4 u4ContextId);
INT4    Bgp4ProcessMacMobDupTimerExpiry (UINT4 u4TimerId, FS_ULONG u4Tmr);
INT4    Bgp4EvpnClearMacMobDupTimers (UINT4 u4Context);
INT4    Bgp4EvpnGetAllRoutes (UINT4, UINT4);
INT4    Bgp4EvpnESINotification (UINT4 u4VrfId, UINT4 u4VnId, UINT1 *pu1EthSegId,
             UINT1 *pu1IpAddr, INT4 i4IpAddrLen, UINT1 u1Action);
INT4    Bgp4DelMacAdRtFromESI (tRouteProfile * pRouteprofile);
INT4    Bgp4EvpnEthADNotification (UINT4 u4VrfId, UINT4 u4VnId, UINT1 *pu1EthSegId,INT4 i4Flag);
INT4    Bgp4ProcessMacAddressWithdrawRoute (tRouteProfile * pRouteprofile);
VOID    Bgp4EvpnProbeandSendESUpdate(UINT4 u4VrfId, UINT4 u4VNId);
INT4    Bgp4AttrFillEvpnNextHop (tRouteProfile * pRtProfile, tBgp4Info * pAdvtBgpInfo,tBgp4PeerEntry * pPeerInfo);
INT4    Bgp4CheckandProcessMacDup (tRouteProfile * pRtProfile);
INT4    Bgp4EvpnRrPrune (tRouteProfile *);
#endif
INT4    Bgp4GetPeerAdvtAfiSafi (tRouteProfile  *, tBgp4PeerEntry *, UINT4  *);
INT4    Bgp4GetNegAfiSafi (UINT4 , UINT4 *) ;
INT4    Bgp4IfCreate(UINT4 );
INT4    Bgp4IfDelete (tBgp4IfInfo *);
INT4    Bgp4IfaceChngHandler (UINT4 , UINT4 );
INT4    Bgp4ProcessIfaceDown (tBgp4IfInfo *);
INT4    Bgp4ProcessIfaceUp (tBgp4IfInfo *);
INT4    Bgp4GetIfInfoFromIp (UINT4, tBgp4IfInfo *);
INT4    Bgp4DeleteAllInterfaceInfo (UINT4);
INT4    Bgp4GetIfAddrFromIfIndex (UINT4 , UINT4 *);
tBgp4IfInfo * Bgp4IfGetEntry (UINT4 );
INT4  Bgp4RtmMsgHandler (tBgp4QMsg *);
VOID  Bgp4RegAckMsg (tBgp4QMsg *);
#ifdef ROUTEMAP_WANTED
INT4  Bgp4SendRouteMapUpdateMsg (UINT1*, UINT4);
#endif /* ROUTEMAP_WANTED */
#ifdef RRD_WANTED
INT1  Bgp4RtmProcessRtChange (tNetIpv4RtInfo *, tRtmMsgHdr *);
INT4  Bgp4RtmProcessUpdate (tNetIpv4RtInfo *, tRtmMsgHdr *);
#endif
VOID  Bgp4RouteChngHandler (tBgp4QMsg *);
VOID BgpClearNetworkprefixDatabase (UINT4 u4Context);
INT4  Bgp4RedistLock (VOID);
INT4  Bgp4RedistUnLock (VOID);
UINT4 Bgp4IsLocalAddr (UINT4 u4Addr);
VOID RfdUpdateDefaultConfigValues (UINT4 u4CxtId);
UINT1 BgpIp4FilterRouteSource (UINT4 u4CxtId, tRouteProfile * pRtProfile);
INT4 Bgp4GetIfPort (UINT4, UINT4 *);
INT4  BgpSetRoutelistflag(tTMO_SLL * pTobeAggrList,INT4 u4Flag); 
INT4 Bgp4CheckAggrForSuppMap(tRouteProfile * pRoute, UINT1 *pu1SuppRMapName);
INT4 Bgp4CheckAggrForAdvMap(tRouteProfile * pRoute, UINT1 *pu1AdvRMapName);
INT4 Bgp4FillAggrForAttMap(tRouteProfile * pRoute, UINT1 *pu1AdvRMapName, tRouteProfile *pAggrRoute);
INT4 Bgp4GetAggregateDetails(tBgp4AggrDetails *pBgp4Aggrdetails, INT4 i4AggrIndex);
/* Prototypes for snmp routines generating trap */
VOID Bgp4SnmpSendStatusChgTrap (UINT4, UINT4 );
VOID Bgp4SnmpSendPeerStatusChgTrap (tBgp4PeerEntry * , UINT4 );
VOID Bgp4SnmpSendRTMRouteAdditionFailureTrap(tRouteProfile *,UINT4 );
VOID Bgp4SnmpSendEstablishedStatusChgTrap (tBgp4PeerEntry * , UINT4 );
VOID Bgp4SnmpSendBackwardTransitionTrap (tBgp4PeerEntry * , UINT4 );
extern VOID IssStoreGraceContent (VOID*,UINT4); 
extern VOID IssRestoreGraceContent (VOID*,UINT4); 
extern double UtilCeil        (double );
extern double UtilLog         (double );
extern double UtilPow         (double , double );
extern double UtilExp         (double );

tBgpPeerGroupEntry * Bgp4GetPeerGroupEntry (UINT4, UINT1 *pu1PeerGroup);
tBgpPeerGroupEntry * Bgp4CreatePeerGroup (UINT4, tSNMP_OCTET_STRING_TYPE *pPeerGroupName);
INT4  Bgp4DeletePeerGroup (UINT4, tBgpPeerGroupEntry *pPeerGroup);
INT4  Bgp4CopyPeerGroupPropertyToPeer (tBgpPeerGroupEntry *pPeerGroup, 
                                       tBgp4PeerEntry *pPeerEntry, INT4 i4ObjectType);
INT4  Bgp4CopyPeerGroupProperties (tBgpPeerGroupEntry *pPeerGroup, INT4 i4ObjectType);
VOID Bgp4InitShadowPeerProperty (tBgp4PeerEntry *pPeerEntry);
VOID Bgp4RestorePeerPropertyFromShadowPeer (tBgp4PeerEntry *pPeerEntry);
INT4 Bgp4GetConfiguredPeerAttribute (INT4 i4PeerRemoteAddrType,
        tSNMP_OCTET_STRING_TYPE * PeerRemoteAddr,
        INT4 * i4TableObject, INT4 i4ObjectType);
INT4  Bgp4DeActivateMpCapability (UINT4, UINT1 *pu1PeerGroupName, UINT4 u4Capability,
                                  UINT1 u1OrfType, UINT1 u1OrfMode);
INT4  Bgp4ActivateMpCapability (UINT4, UINT1 *pu1PeerGroupName, UINT4 u4Capability, 
                                UINT1 u1OrfType, UINT1 u1OrfMode);
VOID
Bgp4GlobalTrace (tAddrPrefix *pAddr, UINT4 u4DbgVar, UINT4 u4mask,
                    const char *pmodule, const char *fmt,...);
VOID Bgp4TraceLog (tAddrPrefix * pAddrPrefix, tAddrPrefix * pAddr, UINT4, UINT4, const char *pmodule, CHR1 *);
struct t_BgpCxtNode * Bgp4GetContextEntry (UINT4);
INT4  Bgp4EnableRRDProtoMask (UINT4 u4Context, UINT4 i4RRDProtoMask);
INT4  Bgp4DisableRRDProtoMask (UINT4 u4Context, UINT4 u4RRDProtoMask);
INT4  Bgp4GetPeerGroupLocalAsCfgStatus (UINT4 u4Context, tSNMP_OCTET_STRING_TYPE *pPeerGroupName, UINT1 *pu1AsCfgFlag);
INT4  Bgp4GetPeerLocalAsCfgStatus (UINT4, INT4, tSNMP_OCTET_STRING_TYPE *, UINT1 *);
INT4  Bgp4GetLocalAsCfgStatus (UINT4, UINT1 *);
INT4  Bgp4GetBgpIdConfigType (UINT4 u4Context, UINT1 *pu1Type);
INT4  Bgp4GetNeighborPasswd (UINT4 u4Context,INT4 i4Fsbgp4TCPMD5AuthPeerType,
                       tSNMP_OCTET_STRING_TYPE *pFsbgp4TCPMD5AuthPeerAddr,
                       tSNMP_OCTET_STRING_TYPE *pRetValFsbgp4TCPMD5AuthPassword);
INT4 BgpCheckBgpClusterId (UINT4 u4Context, tSNMP_OCTET_STRING_TYPE *pClusterId);
INT4  Bgp4CheckSyncedRoutes (tBgp4PeerEntry *pPeer, tTMO_SLL * pTsNewRoutes);
VOID  Bgp4Disable (VOID);
INT4  Bgp4Enable (VOID);
INT1  Bgp4VerifyGlobalState (VOID);

INT4 Bgp4CheckMKTNeighborAssociation (UINT4 u4Context, UINT1 u1MktId);

tTcpAoAuthMKT * BgpFindTCPAOMktInContext(UINT4 u4Context, INT4 i4MktIndex);
VOID Bgp4TCPAOClearMKTTable(UINT4 u4Context);
INT4 Bgp4RibhAddMultipathRouteToVrfUpdLists (tRouteProfile *pRtProfile, tTMO_SLL *pPrevMPathList);

INT4 BgpFormPrevMultipathLists (tRouteProfile * pRtProfile, tTMO_SLL *pPrevMPathList);
INT4 BgpFormMultipathLists (tRouteProfile *pRtProfile);
INT4 BgpCheckForMultipath (tRouteProfile *pRtBestRoute, tRouteProfile *pRtNewRoute);
VOID  BgpReleasePrevMpList ( tTMO_SLL *pPrevMPathList);
VOID Bgp4DshReleasePeerRouteList (tBgp4PeerEntry * pPeerEntry, UINT4 u4Index);


/*BGP Clear counters for Peer Table, Routing Refresh Table and Prefix Counter Table*/

VOID BgpClearPeerCounter(INT4 i4Fsbgp4mpebgpPeerRemoteAddrType,tSNMP_OCTET_STRING_TYPE *
                         pMpebgpPeerRemoteAddr);
VOID BgpClearRouterRefreshCounter(INT4 i4FsMIBgp4mpeRtRefreshStatisticsPeerType,
                                  tSNMP_OCTET_STRING_TYPE * pFsMIBgp4mpeRtRefreshStatisticsPeerAddr,
                                  INT4 i4Afi,INT4 i4Safi);
VOID BgpClearPrefixCounter(INT4 i4Fsbgp4mpebgpPeerRemoteAddrType,tSNMP_OCTET_STRING_TYPE *
                           pMpebgpPeerRemoteAddr,INT4 i4Afi,INT4 i4Safi);

INT4 Bgp4GetPeerLocalAddrCfgStatus (UINT4 u4Context, tAddrPrefix PeerAddress);

/*Network Route Table */
INT4 Bgp4RRDNetworkTableCmp (tBgp4RBElem * pRBElem1, tBgp4RBElem * pRBElem2);
tNetworkAddr * Bgp4GetFirstIndexFsBgp4RRDNetworkTable (UINT4 u4Context);
tNetworkAddr * Bgp4GetNextIndexFsBgp4RRDNetworkTable (UINT4 u4Context, 
                                                      tNetworkAddr *pNetworkAddr);

INT4 BgpNetworkIpRtLookup(UINT4 , tAddrPrefix *, UINT2 ,INT4 , INT1 , UINT1 ,UINT4 *);
INT4  Bgp4GetFsBgp4RRDNetworkTableIntegerObject(UINT4 ,tNetworkAddr *,INT4  , INT4 *);
INT4  Bgp4SetFsBgp4RRDNetworkTableIntegerObject(UINT4 ,tNetworkAddr *,INT4  , INT4 );
INT4  Bgp4TestFsBgp4RRDNetworkTableIntegerObject (UINT4 ,tNetworkAddr *,INT4  , INT4 );                                                 
INT4                Bgp4NetworkRouteCreateEntry(UINT4,tNetworkAddr *,tNetworkAddr *);
tNetworkAddr *      Bgp4NetworkRouteGetEntry (UINT4,tNetworkAddr *,tNetworkAddr *);
INT4                Bgp4NetworkRouteDeleteEntry (UINT4,tNetworkAddr *);
#endif /* BGP4RTS_H */
