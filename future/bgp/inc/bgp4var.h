/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved    
 *
 * $Id: bgp4var.h,v 1.50 2016/12/27 12:35:53 siva Exp $
 *
 * Description:This file contains the extern declarations for all       
 *             the data structures are present in the system.        
 *
 *******************************************************************/
#ifndef BGP4VAR_H
#define BGP4VAR_H

/******** Global Variable Declaration ******************/

typedef struct t_BgpNode {
        /* Bgp4 Task Id */
        tOsixTaskId          Bgp4TaskId; 
        /* Bgp4 Input Queue Id */
        tOsixQId             Bgp4QId;   
        /* Bgp4 RRD Queue Id */ 
        tOsixQId             Bgp4RRDQId;
        /* Bgp4 SNMP/CLI Response Queue Id */ 
        tOsixQId             Bgp4SnmpQId;
        /* Bgp4 RM Queue Id */
        tOsixQId             Bgp4RMQId;
        /* Bgp4 One Second Timer List Id */
        tTimerListId         Bgp41SecTmrListId;
        /* Bgp4 Timer List Id */
        tTimerListId         Bgp4TmrListId;
        /* Pointer to the one-sec timer used for polling */
        tTmrAppTimer        *pBgpOneSecTmr;
        /* Pointer to the selection deferral timer used for polling */
        tTmrAppTimer        *pBgpSelectionDeferTmr;

        /* Pointer to the selection deferral timer used for polling */
        tTmrAppTimer        *pBgpNetworkTempMsrTmr;

        /* Pointer to the restart timer */
        tTmrAppTimer        *pBgpRestartTmr;
        /* Structure storing community info */ 
        tBgp4CommParams      Bgp4CommParams;
        /* Structure storing extended community info */
        tBgp4ExtCommParams   Bgp4ExtCommParams;
        /* Mem pool id for Peer group entries */
        tMemPoolId           Bgp4PeerGroupPoolId;
        /* Mem pool id for Peer-entries */
        tMemPoolId           Bgp4PeerEntryPoolId;
        /* Mem Pool id for peer data entry */
        tMemPoolId           Bgp4PeerDataEntryPoolId;     
        /* Mem Pool id for tBgp4RfdDecayArray */
        tMemPoolId           Bgp4RfdDecayPoolId;     
        /* Mem Pool id for tBgp4RfdReuseIndexArray */
        tMemPoolId           Bgp4RfdReuseIndexPoolId;     
        /* Mem pool id for BufNodeMSGs */
        tMemPoolId           Bgp4BufNodeMsgPoolId;
        /* Mem pool id for BufNodes */
        tMemPoolId           Bgp4BufNodesPoolId;
        /* Mem pool id for route-profiles */
        tMemPoolId           Bgp4RtProfilePoolId;
        /* Mem pool id for Network route */
        tMemPoolId           Bgp4NetworkRtPoolId;
        /* Mem pool id for Bgp4 info structures */
        tMemPoolId           Bgp4BgpInfoPoolId;
        /* Mem pool id for AS-path infos */
        tMemPoolId           Bgp4AsPathPoolId;
        /* Mem pool id for BGP input Q messages */
        tMemPoolId           Bgp4InputQPoolId;
        /* Mem pool id for polling timers */
        tMemPoolId           Bgp4PollTmrPoolId;
        /* Mem pool id for link nodes */
        tMemPoolId           Bgp4LinkNodePoolId;
        /* Mem pool id for Advertised Route link nodes */
        tMemPoolId           Bgp4AdvRtLinkNodePoolId;
        /* Mem pool id for storing next-hops */
        tMemPoolId           Bgp4NextHopMetricPoolId;
        /* Mem pool id for Identical PA Database. */
        tMemPoolId           Bgp4PAPoolId;
        /* Mem pool id for Advertise PA Database. */
        tMemPoolId           Bgp4AdvtPAPoolId;
        /* Mem pool id for interface informatin */
        tMemPoolId           Bgp4IfPoolId;
        /* Mem pool id for Bgp4RmMsg */
        tMemPoolId           Bgp4RmMsgPoolId;
        /* MED table */ 
        tBgp4MedlpEntry      aBGP4MEDtbl[BGP4_MAX_MED_ENTRIES];
        /* LP table */
        tBgp4MedlpEntry      aBGP4LPtbl[BGP4_MAX_LP_ENTRIES];
        /* Filter table */
        tBgp4UpdatefilterEntry aBGP4UpdFiltertbl[BGP4_MAX_UPDFILTER_ENTRIES];
        /* Aggregation table */
        tBgp4AggrEntry       aBGP4Aggrtbl[BGP4_MAX_AGGR_ENTRIES];

        tMemPoolId           BgpCxtMemPoolId; /* Memory poolId for contexts*/ 
        tMemPoolId           BgpGlobalCxtMemPoolId; 
                              /* Memory poolId for stroring the **gBgpCxtNode */ 
        tMemPoolId           OrfMemPoolId; 
  tMemPoolId      AdvRouteMemPoolId;
    /*Memory poolId for storing advertised routes RBtree*/
#ifdef RFD_WANTED
        /* Mempools used for RFD related info. */
        tMemPoolId           RtDampHistMemPoolId; /*This is Memory PoolId for 
                                                 damping history of routes 
                                                 (tRtDampHist)*/
                                                 
        tMemPoolId           PeerDampHistMemPoolId;/*This is Memory PoolId for 
                                                  damping history of peers 
                                                  (tPeerDampHist)*/
                                                  
        tMemPoolId           ReusePeerMemPoolId; /*Memory PoolId for tReusePeer 
                                                structure.*/
#endif
        /* MemPools for storing capabilities related info */
        tMemPoolId        PeerCapsPoolId;  /*Memory pool Id for tSpkrSupCaps                                              structure*/
        /* MemPools for storing route-reresh related info */
        tMemPoolId      RtRefPoolId;
        /* MemPools for storing TCP-MD5 related info */
        tMemPoolId      Md5PoolId;
        /* MemPools for storing MPE related info */
        tMemPoolId  MpeRtSnpaNodeMemPoolId;
                /* Memory pool for storing SNPA information nodes */

        tMemPoolId  MpeSnpaInfoMemPoolId;
                /* Memory pool for storing SNPA information */

        tMemPoolId  MpeRtAfiSafiInstanceMemPoolId;
                /* Memory pool for creating <AFI, SAFI> specific instance
                 * in peer information structure (tAfiSafiSpecInfo) */
        /* MemPools for storing TCP-AO related info */
        tMemPoolId           TcpAoPoolId;
#ifdef L3VPN
        tBgp4Vpn4GlobalData   Bgp4Vpn4GlobalData;
#endif
#ifdef L3VPN
        tNextHopInitInfo     BGP4LspChgInitInfo; 
        /* Linear list holding the peers for which LSP status to be checked */
        tTMO_SLL             BGP4LspChngList;
        /* Linear list holding information about the interfaces */
        INT4                 (*Bgp4L3VpnNotifyASN) (UINT1 u1ASNType, UINT4 u4ASNValue);
        /* Callback pointer to provide information about defaul VR ASN */
        UINT2                u2LabelGroupId;
        UINT1                u1Vpnv4AfiSupFlag;
        UINT1                u1RouteLeakFlag;
#endif
#ifdef EVPN_WANTED
        tMemPoolId      EvpnRouteMemPoolId;
        tMemPoolId      BgpMacDupTimerMemPoolId;
        /*Memory poolId for storing Evpn routes RBtree*/
        tBgp4EvpnGlobalData   Bgp4EvpnGlobalData;
        /* Variable to indicate EVPN-BGP MAC Duplication time interval */
        UINT4                 u4MacMobDuplicationInterval;
        /* Maximum no.of moves allowed before Timer expiry
         * to identify the MAC Duplication*/
        UINT4                 u4MaxMacMoves;
        UINT1                 u1EvpnAfiSupFlag;
        UINT1                 au1EvpnRsvd[3];
#endif
#ifdef VPLSADS_WANTED
        tBgp4VplsGlobalData   Bgp4VplsGlobalData;
#endif
        tOsixSemId           Bgp4RedistListSemId;        
        tOsixSemId           Bgp4ProtoSemId;
        tOsixSemId           Bgp4RibSemId;         
        tTMO_SLL             BGP4IfInfoList; /* Interfaces information */
        /* Global variable for debug/trace */
        tBgp4Glb             Bgp4DbgTrc;

        tDbTblDescriptor     DynInfoList;
        /* The no. of mem pool blocks used-up for allocating BGP 
         * messages */
        UINT4       u4AllocMpeSnpaNodeCnt;
                /* Used to count number of memory blocks allocated from
                 * MpeRtSnpaNodeMemPoolId */
        UINT4                u4BgpmsgCnt;
        /* The no. of mem pool blocks used-up for allocating route-profiles */ 
        UINT4                u4RtprofileCnt;
        /* The no. of mem pool blocks used-up for allocating link-nodes */
        UINT4                u4LinknodeCnt;
        /* The no. of mem pool blcks used-up for alloc Peer Rt link-nodes */
        UINT4                u4PeerRtLinknodeCnt;
        /* The no. of mem pool blocks used-up for allocating bgp infos */
        UINT4                u4BgpinfoCnt;
        /* The no. of mem pool blocks used-up for allocating as-paths */
        UINT4                u4AsnodeCnt;
        /* The no. of mem pool blocks used-up for allocating input q messages */
        UINT4                u4InputQMsgCnt;
        /* The no. of mem pool blocks used-up for allocating interface infos */
        UINT4                u4IfInfoCnt;
        /* The no. of mem pool blocks used-up for allocating RCVD-PA */ 
        UINT4                u4RcvdPACnt;
        /* Flag indicating if RFD is enabled or not */
        UINT4                u4RfdFlag; 
        /* The maximum no. of peers. supported by the system */
        UINT4                u4BGP4MaxPeersAllocated;
        /* The maximum no. of routes supported by the system */
        UINT4                u4BGP4MaxRoutesAllocated;
        /* Variable to indicate the BGP speaker's restart time interval */
        UINT4                u4RestartInterval;
        /* Variable to indicate BGP selection deferral time interval */
        UINT4                u4SelectionDeferralInterval;
        /* Variable to indicate the stale path interval which is used to \
           delete stale routes from RIB and forwarding table */
        UINT4                u4StalePathInterval;
        /* Variable that supports address family supported during the \
           last restart */
        UINT4                u4GrAfiPreserved;
        /* SyslogID*/
        UINT4                 u4SysLogId;
        UINT4                u4RmSyncCxt; 
        UINT4                u4RmSyncHashKey; 
        UINT4                u4RmAfSafiIndex;    
        /* Variable storing the BGP local AS no. */
        UINT4                u4BGP4LocalASno;

        UINT4                u4BgpNetworkTempTmrRunning; 
        /*  Flag indicating if BGP Task initialization is completed */
        UINT1                u1Bgp4TaskInit;
         /* Variable to indicate kind of restart support provided by this BGP
         * speaker. It can be none, planned , unplanned or both */
        UINT1                u1RestartSupport;
        /* It is  used to indicate restarting mode of the BGP speaker 
         * when GR is enabled. It can be either restarting or receiving */
        UINT1                u1RestartMode;
        /* Variable to indicate BGP speaker's restart exit reason */
        UINT1                u1RestartExitReason;
        /* Variable to indicate restart status like whether it is planned 
         * or unplanned */
        UINT1                u1RestartStatus;
        /* Variable to indicate restart reason like unknown. software restart 
         * swReloadUpgrade, switchToRedundant */
        UINT1                u1RestartReason;
        /* Used to indicate restart support in  BGP*/
        UINT1                u1GrAdminStatus;
        /* Used to trigger route selection when the speaker restarts */
        UINT1                u1RouteSelectionFlag;
        /* No of Standby nodes up*/
        UINT1                u1StandbyNodes;
        UINT1                bBulkReqRcvd;
        UINT1                u1BulkUpdStatus;
        UINT1                u1NodeStatus;
        UINT1                u1PrevNodeStatus;
        UINT1                u1BlockSync; /* This for testing purpose, 
                                             if it is non zero the route updates will 
                                             not be synced to standby */
        /* Used to indicate 4 byte ASN support in  BGP */
        UINT1                u1FourByteASNSupportStatus;
        /* Used to indicate 4 byte ASN notation type */
        UINT1                u1FourByteASNNotationType;
        UINT1                u1InitBulkStatus;
        UINT1                au1Pad[3];
#ifdef VPLSADS_WANTED
    #ifdef VPLS_GR_WANTED
        UINT1                u1EOVPLSFlag;
        UINT1                au1Pad1[3];
    #endif
#endif
}tBgpNode;

typedef struct t_BgpCxtNode {
        /* Bgp4 Task Id */
        /* Hash Table holding the Received Path Attribute */
        tTMO_HASH_TABLE      *pBgp4RcvdPAHashTbl[BGP4_MPE_ADDR_FAMILY_MAX_INDEX];
        /* Hash Table holding the Route to be advertised base
         * on identical outbound Path Attribute */
        tTMO_HASH_TABLE      *pBgp4AdvtHashTbl;
        /* Structure storing route-reflection related info. */
        tBgp4RrParams        Bgp4RrParams;
#ifdef RFD_WANTED
        /* Structure storing RFD related info. */
        tBgp4RfdParams       Bgp4RfdParams;
#endif
        /* Structure storing capabilities related info */
        tBgp4CapsParams      Bgp4CapsParams;
        /* Structure storing Route-Redistribution related info */
        tBgp4Redistribution  Bgp4Redistribution;
        /* Structure storing peer related info */
        tBgpPeerData         BgpPeerData;
        /* Structure storing confed related info */
        tBgp4ConfedParams    Bgp4ConfedParams;
        /* Structure storing route-reresh related info */
        tRtRefGlobalData     Bgp4RtRefGlobalData;
        /* Structure storing TCP-MD5 related info */
        tTcpMd5GlobalData    Bgp4TcpMd5GlobalData;
  /*TODO To check if Bgp4TcpAoGlobalData is required or TMO_SLL_COUNT can be used*/
        tTcpAoGlobalData     Bgp4TcpAoGlobalData;
        /* Structure storing MPE related info */
        tBgp4MpeGlobalData   Bgp4MpeGlobalData;
        /* Array of root pointers for each <AFI, SAFI> RIB */
        VOID                 *gapBgp4Rib[BGP4_MPE_ADDR_FAMILY_MAX_INDEX];
        /* Error Data structure holding the Error count for various types
         * errors.
         */
        tNextHopInitInfo     BGP4NHChgInitInfo;
        tNetAddress          BGP4SyncInitRouteInfo;
        /* Double Link list holding the routes that are modified for FIB update
         * or Internal/External Peer Advertisement. */
        tBGP4_DLL            BGP4ChgList; 
        /* Double Link list holding the routes that are selected as best routes
         * in BGP and need to be updated in FIB */
        tBGP4_DLL            BGP4FIBUpdateList; 
        /* Double Link list holding the routes that need to be advertised to 
         * external peers.Both feasible and withdrawn routes are stored. */ 
        tBGP4_DLL            BGP4ExtPeerAdvtList; 
        /* Double Link list holding the routes that need to be advertised to the
         * internal peers. Both feasible and withdrawn routes are stored. */
        tBGP4_DLL            BGP4IntPeerAdvtList;
        /* Linear list holding the routes obtained via redistribution that 
         * needs to be processed. */
        tTMO_SLL             BGP4RedistList; 
        /* Linear  list to hold the routes configured via network command*/
        tBgp4RBTree          BGP4NetworkRtList;
        /* Linear list holding the peers that require outbound soft reconfig */
        tTMO_SLL             BGP4SoftCfgOutList; 
        /* Linear list holding the routes that need to be synchronized with 
         * IGP */
        tTMO_SLL             BGP4NonsyncList;
        /* Linear list holding the peer group entries */
        tTMO_SLL             BGP4PeerGrpList;
        /* Linear list holding the peer-entries for some processing */
        tTMO_SLL             BGP4PeerList;
        /* Linear list holding the peer-entries for transmission */
        tTMO_SLL             BGP4PeerTransList;
        /* Linear list holding the overlapping routes */
        tTMO_SLL             BGP4OverlapRouteList;
#ifdef RFD_WANTED
        /* Linear list holding peers that have become Reusable after being
         * damped. */
        tTMO_SLL             BGP4SupPeerReuseList;
#endif
        /* Linear list holding the peers that are going thro init process */
        tTMO_SLL             BGP4PeerInit;   
        /* Linear list holding peers that are going through deinit process */
        tTMO_SLL             BGP4PeerDeInit; 
  tTMO_SLL    BGP4MktList;

        /* DLL holding the routes redistributed from RIP */
        tBGP4_DLL             RipImportList;
        /* DLL holding the routes redistributed from OSPF*/
        tBGP4_DLL             OspfImportList;
        /* DLL holding the redistributed STATIC routes. */
        tBGP4_DLL             StaticImportList;
        /* DLL holding the redistributed DIRECT routes. */
        tBGP4_DLL             DirectImportList;
        /* DLL holding the redistributed ISISL1 routes. */
        tBGP4_DLL             IsisL1ImportList;
        /* DLL holding the redistributed ISISL2 routes. */
        tBGP4_DLL             IsisL2ImportList;
        /* This table represents IGP metric values for Link Nexthop addresses */
        tTMO_HASH_TABLE     *pBgp4NextHopMetricTbl; 
        /* This variable stores the dispatcher function entry/exit time in
         * TICS. Based on the value of the previous calls entry & exit time
         * the throttling parameter is updated. */
        tBgp4RBTree              OrfTable;
#ifdef ROUTEMAP_WANTED        
        /*for Distribute in filtering feature */
        tFilteringRMap  DistributeInFilterRMap;
        /*for Distribute out filtering feature */
        tFilteringRMap  DistributeOutFilterRMap;
        /*for Distance  filtering feature*/
        tFilteringRMap  DistanceFilterRMap;
        /* Distance value for BGP protocol routes*/
#endif /* ROUTEMAP_WANTED */
#ifdef L3VPN
        tTMO_SLL             ImportTargets;
        tTMO_SLL             ExportTargets;
        tNetAddress          VrfInitAddress; /* used in init/deinit of vrf to store the
                                              * current prefix for the next iteration */
        UINT1                au1VrfRD[BGP4_VPN_RT_DISTING_SIZE];
        UINT4                u4LSPLabel;   /* Label allocated for this VR */
        UINT4                u4VpnState; /* L3vpn state; up/down/create/delete */
        UINT4                u4Flags; /* L3vpn VRF specific flags */
#endif
#ifdef EVPN_WANTED
        tBgp4RBTree          EvpnRoutes;
        tBgp4RBTree          BgpMacDupTimer;
        INT4                 i4EvpnFlags;
#endif
        UINT4                u4ContextId;
        UINT4                au4DispatcherTime [4];
        /* Field storing the maximum interval after which synchronization of 
         * Routes with IGP is verified */
        UINT4                u4BGP4SyncInterval;
        /* Bgp Identifier */
        UINT4                u4BGP4Id;
        /* Default Local preference used for all routes */
        UINT4                u4BGP4DefaultLP;
        /* Default IGP metric used for routes */
        UINT4                u4BGP4DefaultIgpMetric;
        /* Policy associated with non-bgp routes */
        UINT4                u4BGP4NonBgpRtExport;
        /* The route information used for advertising non-bgp routes */
        tNonBgpInitInfo      BGP4NonBgpInitPrefix;
        /* Field storing the global state of BGP - READY/job-in-progress.. */
        UINT4                u4BGP4GlobalState;
        /* Global flag indicating all the pending tasks */
        UINT4                u4BGP4GlobalFlag;
        /* Policy used for installing overlapping routes */
        UINT4                u4InstallOverlapRt;
        /* Time interval after which next-hop resolution is done periodically
         * for all routes */
        UINT4                u4BGP4NHChgInterval;
        UINT4                u4BGP4NHChgPrcsInterval;
        /* The no. of feasible routes present in BGP LOCAL RIB */
        UINT4                u4RouteProfileCnt;
        /* The no. of peer entries present in BGP */ 
        UINT4                u4PeerCount;
        /* The other protocols to whom BGP is registered with */
        UINT4                u4Registered;
        /* The no. of clients configured when the speaker is RR */
        UINT4                u4RrClientCnt;
        /* BGP4 table version */
        UINT4                u4BGP4TableVersion;
        /* Variable to indicate the BGP speaker's restart time interval */
        UINT4                u4RestartInterval;
        /* Variable to indicate BGP selection deferral time interval */
        UINT4                u4SelectionDeferralInterval;
        /* Variable to indicate the stale path interval which is used to \
           delete stale routes from RIB and forwarding table */
        UINT4                u4StalePathInterval;
        /* Variable that supports address family supported during the \
           last restart */
        UINT4                u4GrAfiPreserved;

        UINT4                u4IntBgpConfigMaxPaths;      /* ibgp Mulitpath value   */
        UINT4                u4ExtBgpConfigMaxPaths;      /* ebgp multipath value   */
        UINT4                u4ExtOrIntBgpConfigMaxPaths; /* eibgp multipath value  */

        UINT4                u4IntBgpOperMaxPaths;      /* ibgp Mulitpath oper value  */
        UINT4                u4ExtBgpOperMaxPaths;      /* ebgp multipath oper value  */
        UINT4                u4ExtOrIntBgpOperMaxPaths; /* eibgp multipath oper value */
        /* Variable storing the BGP VRF local AS no. */
        UINT4                u4BGP4LocalASno;

        /* Variable storing BGP listen socket-id */
#ifdef BGP_TCP4_WANTED
        INT4                 i4BGP4Listenconn;
#endif
#ifdef BGP_TCP6_WANTED
        INT4                 i4BGP4v6Listenconn;
#endif
        INT4                 au4BGP4RRDMetricVal[BGP4_ALL_METRIC];
#ifdef L3VPN
        UINT4                u4Label;
#endif
        /* Error Data structure holding the Error count for various types
         * errors.
         */
        tBgpErrorRecord      Bgp4ErrorRecord;
#if defined (BGP_TCP4_WANTED) || defined (BGP_TCP6_WANTED)
        /* socket READ FD_SET */
        BGP_FD_SET_STRUCT    BgpReadSockFdSet; 
        /* socket READ FD_SET */
        BGP_FD_SET_STRUCT    BgpReadSockFdBits;
        /* socket write FD_SET */
        BGP_FD_SET_STRUCT    BgpWriteSockFdSet;
        /* socket write FD_SET */
        BGP_FD_SET_STRUCT    BgpWriteSockFdBits;
#endif
UINT4             u4CommLenError;         /* Holds count of update message   */
                                          /* with invalid community path     */
                                          /* attribute length                */
UINT4          u4ExtCommLenError;         /* Holds count of update message   */
                                          /* with invalid ext community path */
                                          /* attribute length                */
        UINT4                u4RIBRoutesCount;/* No of routes present in the RIB*/
        UINT4                u4L3VPNRouteCount;
        UINT4                u4CliAdminStatus; /*Variable used for do shutdown ip bgp*/
        UINT4                u4AdminStatus; /*Variable used to indicate do shutdown ip bgp*/
        tBgp4Glb             Bgp4DbgTrc;
        tAddrPrefix          Bgp4TraceAddr;
        /* Variable storing the BGP listen socket port */ 
        UINT2                u2BGP4Listenport;
        UINT1                u1VrfConfig;   /* VRF local AS configuration type
                                               true - static, false - dynamic */
        /* This variable controls the updation of FIB. Only if this variable 
        * becomes >= BGP4_FIB_UPD_MAX_HOLD_TIME, the FIB will be updated with 
        * the route present in gBGP4FIBUpdateList. */
        UINT1                u1FIBUpdHoldTime;
        /* Variable storing the BGP administrative status */
        UINT1                u1BGP4Adminstatus;
        /* Variable storing the registration id with RTM */
        UINT1                u1BGP4RTMRegistrationId;
        /* */ 
        UINT1                u1BGP4IdConfigType;
        /* Variable indicating whether MED needs to be compared for all
         * routes or only selectively */
        UINT1                u1BGP4MedCompare;
        /* Variable indicating with synchronization feature is enabled or 
         * disabled */
        UINT1                u1BGP4Syncstatus;
        UINT1                u1DefaultAfiSafi;
        UINT1                u1Ipv4AfiSupFlag;
        UINT1                u1Ipv6AfiSupFlag;
        UINT1                u1BGP4DftRouteOrigStatus;
        UINT1                u1BGP4DftRouteOrigNewStatus;
         /* Variable to indicate kind of restart support provided by this BGP
         * speaker. It can be none, planned , unplanned or both */
        UINT1                u1RestartSupport;
        /* It is  used to indicate restarting mode of the BGP speaker 
         * when GR is enabled. It can be either restarting or receiving */
        UINT1                u1RestartMode;
        /* Variable to indicate BGP speaker's restart exit reason */
        UINT1                u1RestartExitReason;
        /* Variable to indicate restart status like whether it is planned 
         * or unplanned */
        UINT1                u1RestartStatus;
        /* Variable to indicate restart reason like unknown. software restart 
         * swReloadUpgrade, switchToRedundant */
        UINT1                u1RestartReason;
        /* Used to indicate restart support in  BGP*/
        UINT1                u1GrAdminStatus;
        /* Used to trigger route selection when the speaker restarts */
        UINT1                u1RouteSelectionFlag;
        UINT1       u1SelectionDeferralFlag;
        UINT1                u1AllowPeerInit;
        UINT1                u1Distance;
        /* Flag to indicate if IBGP routes should be redistributed to IGP protocols*/
        UINT1                u1IBGPRedistributeStatus;
        /* Flag to indicate Trap notification status for BGP System */
        UINT1                u1IsTrapEnabled;
        UINT1                u1BGPStatus;   /*Bgp Status for this context */
        /* Used to indicate 4 byte ASN support in  BGP */
        UINT1                u1FourByteASNSupportStatus;
        /* Used to indicate 4 byte ASN notation type */
        UINT1                u1FourByteASNNotationType;
        /* Flag to indicate if the AS number was reset to 0 when disabling 4-byte ASN */
        UINT1                u1IsAsReset;
        UINT1                u1LabelAllocPolicy;
        UINT1                u1RrdRegStatus;
        UINT1                u1AdminStatusFlag;
/* u1BGP4RRDMetricMask Flag to hold the information on protocols configured with 
 * redistribute metric */
        UINT1                u1BGP4RRDMetricMask; 
        UINT1                au1Padding[2];
#ifdef VPLSADS_WANTED
        UINT1                u1L2vpnAfiSupFlag;
        UINT1                u1Rsvd[3];
#ifdef VPLS_GR_WANTED
        UINT1                u1EOVPLSFlag;
        UINT1                au1Pad[3];
#endif
#endif
}tBgpCxtNode;

typedef struct t_BgpGlobalCxt
{
    tBgpCxtNode *CxtPtr;
}tBgpGlobalCxt;
/* Structure storing BGP global information */
EXTERN tBgpNode gBgpNode;
EXTERN tBgpCxtNode **gBgpCxtNode;
EXTERN tBgpCxtNode *gpBgpCurrCxtNode;
EXTERN tAddrPrefix gTraceAddr;
EXTERN UINT4       gu4BgpCurrContextId; 
#endif /* BGP4VAR_H */
