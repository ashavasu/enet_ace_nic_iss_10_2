/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bgconfed.h,v 1.6 2013/06/23 13:14:04 siva Exp $
 *
 * Description:This file includes declarations related with
 *             Confederation feature.
 *
 *******************************************************************/
#ifndef _CONFED_H
#define _CONFED_H

/* ********** All type definitions of AS Confederation module *********** */ 

/* Constant used in the Confed module */
#define BGP4_CONFED_MAX_MEMBER_AS      100

/* constants used in low level routines */
#define BGP4_CONFED_COMPARE_MED_SET    1
#define BGP4_CONFED_COMPARE_MED_CLEAR  2
#define BGP4_CONFED_PEER_AS_ENABLE     1
#define BGP4_CONFED_PEER_AS_DISABLE    2

/* Definition for AS confed */
typedef struct t_ConfedParams {
    UINT4    u4ConfedId;
    UINT4    au4ConfedPeer[BGP4_CONFED_MAX_MEMBER_AS];
    UINT1    u1ConfedBestPathCompareMED;
    UINT1    au1Pad[3];
} tBgp4ConfedParams;

/* this definition is the modified PeerConfig of Bgp4tdfs.h */

/* ************** Macros definitions ***************************************/ 
#define BGP4_CONFED_ID(u4CxtId)                    (gBgpCxtNode[u4CxtId]->Bgp4ConfedParams.u4ConfedId)
#define BGP4_CONFED_BESTPATH_COMPARE_MED(u4CxtId) \
        (gBgpCxtNode[u4CxtId]->Bgp4ConfedParams.u1ConfedBestPathCompareMED)
#define BGP4_CONFED_PEER(u4CxtId,i)    (gBgpCxtNode[u4CxtId]->Bgp4ConfedParams.au4ConfedPeer[i]) 

#define BGP4_CONFED_PEER_STATUS(pPeerInfo) (pPeerInfo->peerConfig.u1ConfedPeer)

/**************** Prototypes for Confed module ****************************/
INT4  Bgp4ConfedPreserveLP (const tRouteProfile *, tBgp4Info *);
INT4  Bgp4ConfedPreserveMED (const tRouteProfile *, tBgp4Info *);

#endif /* End of _CONFED_H */
