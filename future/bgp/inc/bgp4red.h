/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bgp4red.h,v 1.7 2016/12/27 12:35:53 siva Exp $
*
*********************************************************************/

typedef enum{
    BGP4_HA_UPD_NOT_STARTED = 1,/* 1 */
    BGP4_HA_UPD_IN_PROGRESS,    /* 2 */
    BGP4_HA_UPD_COMPLETED,      /* 3 */
    BGP4_HA_UPD_ABORTED,        /* 4 */
    BGP4_HA_MAX_BLK_UPD_STATUS
} eBgp4haBulkUpdStatus;
typedef enum
{
    BGP4_RED_BULK_UPD_PEER_MESSAGE = 1, /* 1 */
    BGP4_RED_BULK_UPD_ROUTE_MESSAGE,    /* 2 */
    BGP4_RED_UPD_ROUTE_MESSAGE,         /* 3 */
    BGP4_RED_UPD_ROUTE_FLAG_MSG,        /* 4 */
    BGP4_RED_BULK_TAIL_MESSAGE,         /* 5 */
#ifndef VPLSADS_WANTED
    BGP4_RED_BULK_REQ_MESSAGE,           /* 6 */
#endif
#ifdef VPLSADS_WANTED
   #ifndef VPLS_GR_WANTED
   BGP4_RED_BULK_REQ_MESSAGE,           /* 6 */
   #else
    BGP4_RED_BULK_REQ_MESSAGE,          /* 6 */
    BGP4_RED_UPD_VPLSINSTANCE_MESSAGE,  /* 7 */
    BGP4_RED_BULK_UPD_VPLS_INST_MESSAGE, /* 8 */
   #endif
#endif
   BGP4_RED_BULK_UPD_BGP_ID,
   BGP4_RED_UPD_BGP_ID,
   BGP4_RED_BULK_UPD_RESTART_REASON,
   BGP4_RED_UPD_RESTART_REASON,
   BGP4_RED_BULK_UPD_TABLE_VERSION,
   BGP4_RED_UPD_TABLE_VERSION,
   BGP4_RED_BULK_UPD_BGP_PEER_INFO,
   BGP4_RED_UPD_BGP_PEER_INFO,
   BGP4_RED_UPD_MPATH_OPER_CNT,
   BGP4_RED_BULK_UPD_PEER_MPCAP_V4_INFO,
   BGP4_RED_BULK_UPD_PEER_MPCAP_V6_INFO,
   BGP4_RED_BULK_BGP_ADMIN_STATUS,
   BGP4_RED_BULK_BGP_RRD_MASK
} eBgpRmMsgType;

#define BGP4_RED_MSG_MEMPOOL_ID           (gBgpNode.Bgp4RmMsgPoolId)
#define BGP4_RED_PEER_MSG_MAX_LEN         (BGP4_RED_MSG_MAX_LEN - 28)
#ifdef L3VPN
#define BGP4_RED_ROUTE_MSG_MAX_LEN        (BGP4_RED_MSG_MAX_LEN - 107)
#else
#define BGP4_RED_ROUTE_MSG_MAX_LEN        (BGP4_RED_MSG_MAX_LEN - 90)
#endif
#define BGP4_RED_RT_BGPINFO_MSG_MAX_LEN   (BGP4_RED_MSG_MAX_LEN - 360)
#define BGP4_RED_UPD_LEN_OFFSET           1  /* As message type is only 1 byte */
#define BGP4_RED_BULK_REQ_MSG_SIZE        3
#define BGP4_RED_MIN_MSG_SIZE             3

#define BGP4_RED_STANDBY_NODES                (gBgpNode.u1StandbyNodes)
#define BGP4_RED_BULK_REQ_RCVD   (gBgpNode.bBulkReqRcvd)
#define BGP4_GET_NODE_STATUS       (gBgpNode.u1NodeStatus)
#define BGP4_PREV_RED_NODE_STATUS  (gBgpNode.u1PrevNodeStatus)
#define BGP4_RED_BULK_UPD_STATUS   (gBgpNode.u1BulkUpdStatus)
#define BGP4_RED_INIT_BULK_STATUS  (gBgpNode.u1InitBulkStatus)
#define BGP4_RED_MSG_MAX_LEN   1500

#define BGP4_IS_STANDBY_UP() \
          ((gBgpNode.u1StandbyNodes > 0) ? OSIX_TRUE : OSIX_FALSE)

#define BGP4_RED_SYNC_CXT_ID                (gBgpNode.u4RmSyncCxt)
#define BGP4_RED_SYNC_HASH_KEY              (gBgpNode.u4RmSyncHashKey)
#define BGP4_RED_SYNC_AFSAFI_INDEX          (gBgpNode.u4RmAfSafiIndex)
#define BGP4_RED_MSG_POOL_ID          (gBgpNode.Bgp4RmMsgPoolId)

#ifdef L2RED_WANTED
#define BGP4_RED_GET_NUM_STANDBY_NODES_UP() \
              gBgpNode.u1StandbyNodes = RmGetStandbyNodeCount ()
#define BGP4_RED_GET_STATIC_CONFIG_STATUS() RmGetStaticConfigStatus()
#define BGP4_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define BGP4_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define BGP4_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define BGP4_RM_PUT_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
    RM_COPY_TO_OFFSET (pMsg, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) += u4Size;\
}while (0)
#define BGP4_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
        RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
                *(pu4Offset) += 1;\
}while (0)

#define BGP4_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
        RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
                *(pu4Offset) += 2;\
}while (0)

#define BGP4_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
        RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
                *(pu4Offset) += 4;\
}while (0)

#define BGP4_RM_GET_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
        RM_GET_DATA_N_BYTE (pMsg, psrc, *(pu4Offset), u4Size); \
                *(pu4Offset) += u4Size;\
}while (0)
#define BGP4_RED_CONT_SYNC_UPDATES 0
#endif
#define BGP4_RED_SYNC_UPDATES                (gBgpNode.u1BlockSync)

INT4  Bgp4RedSyncPeerDampMsg (UINT4 u4Context, tPeerDampHist *pPeerHist);
INT4  Bgp4RmSyncRouteInfo  (tRouteProfile *pRoute);
INT4  Bgp4RedInitGlobalInfo (VOID);
INT4  Bgp4RedDeInitGlobalInfo (VOID);
INT4  Bgp4RedRmCallBack(UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);
VOID  Bgp4RedHandleRmEvents (UINT1 u1Flag);
VOID  Bgp4RedHandleGoActive (VOID);
VOID  Bgp4RedHandleStandbyToActive (VOID);
VOID  Bgp4RedProcessPeerDampMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                  UINT2 u2Length);
VOID  Bgp4RedProcessBgpIDSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                   UINT2 u2Length);
VOID  Bgp4RedProcessBgpAdminStatusSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                   UINT2 u2Length);

VOID  Bgp4RedProcessRestartReasonSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                          UINT2 u2Length);

VOID  Bgp4RedProcessTableVersionSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                         UINT2 u2Length);

INT4  Bgp4RedProcessRouteSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                   UINT1 u1Message, UINT2 u2Length);
VOID  Bgp4RedProcessBgpPeerInfoSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                   UINT2 u2Length);
VOID  Bgp4RedProcessBgpPeerInfoDynamicSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                   UINT2 u2Length);

INT4  Bgp4RedProcessBulkRouteSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                       UINT2 u2Length);
INT4  Bgp4RedSendBulkRequestOrTailMsg (UINT1 u1Message);
INT4  Bgp4RedSendBulkUpdMsg (VOID);
INT4  Bgp4RedSyncRouteInfo (tRouteProfile *pRtProfile);
INT4  Bgp4RedSyncBgpId(UINT4 u4Context,UINT4 u4BgpId);
INT4  Bgp4RedSyncRestartReason(UINT1 u1RestartReason);
INT4  Bgp4RedSyncTableVersion(UINT4 u4Context,UINT4 u4TableVersion);
INT4  Bgp4RedSyncRouteUpdToFIB (tRouteProfile *pRtProfile);
INT4  Bgp4RedSyncBgpPeerInfo(UINT4 u4Context,tBgp4PeerEntry *pPeer);
VOID  Bgp4RedHandleGoStandby (VOID);
VOID  Bgp4RedProcessPeerMsgAtActive (tRmMsg *pMsg);
VOID  Bgp4RedProcessPeerMsgAtStandby (tRmMsg *pMsg);
INT4  Bgp4RedGetBgpRmMsg (tBgpRmMsg **pMsg, UINT1 u1Message,
                           UINT4 *pu4OffSet);
VOID  Bgp4RedSyncDynInfo (VOID) ;
INT4  Bgp4RedSyncPeerDampBulkMsg (VOID);
INT4 Bgp4RedSyncBgpPeerInfoBulkMsg(tBgpRmMsg * pMsg, UINT4 *pu4Offset);
INT4 Bgp4RedSyncBgpIdBulkMsg (tBgpRmMsg * pMsg, UINT4 *pu4Offset);
INT4 Bgp4RedSyncBgpAdminStatusBulkMsg (tBgpRmMsg * pMsg, UINT4 *pu4Offset, INT1 i1SyncStatus);
INT4 Bgp4RedSyncRestartReasonBulkMsg (tBgpRmMsg * pMsg, UINT4 *pu4Offset);
INT4 Bgp4RedSyncTableVersionBulkMsg (tBgpRmMsg * pMsg, UINT4 *pu4Offset);
VOID Bgp4RedProcessBgpPeerMPCapV4SyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet, UINT2 u2LenOffSet);
VOID Bgp4RedProcessBgpPeerMPCapV6SyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet, UINT2 u2LenOffSet);
INT4 Bgp4RedSyncBgpPeerMPCapV4BulkMsg (tBgpRmMsg * pMsg, UINT4 *pu4Offset);
VOID  Bgp4RedUpdPeerFOM (UINT4 u4Context,
                         tPeerDampHist *pPeerDampHist, UINT4 u4Fom);
INT4  Bgp4RedFillBufRouteInfo (tRouteProfile *pRtProfile,
                               tRmMsg *pBuf , UINT4 *pu4OffSet);
VOID  Bgp4RedFillBufAddr (tAddrPrefix PeerAddr, tRmMsg *pBuf,
                          UINT4 *pu4OffSet);
VOID  Bgp4RedReadAddrPrefix (tRmMsg *pBuf , UINT4 *pu4OffSet,
                             tAddrPrefix *pAddr);
tRouteProfile * Bgp4RedReadRouteInfo (tRmMsg *pBuf , UINT4 *pu4OffSet);
tBgp4Info * Bgp4RedReadBgp4Info (tRmMsg *pBuf , UINT4 *pu4Offset);
INT4  Bgp4RmFillBufBgpInfo (tBgp4Info *pRtInfo, tRmMsg *pBuf,
                            UINT4 *pu4Offset);
INT4  Bgp4RmFillBufferFromPADB (tBgpRmMsg *pMsg, UINT4 *pu4Offset);
VOID  Bgp4RedFillBufListRoutes (tRouteProfile *pRtProfile,
                                UINT4 u4RtCount, tBgpRmMsg **pMsg,
                                UINT4 *pu4OffSet, UINT4 *, UINT4 *);
VOID  Bgp4RedFillBufMultiPathListRoutes (tRouteProfile *pRtProfile,
                                UINT4 u4RtCount, tBgpRmMsg **pMsg,
                                UINT4 *pu4OffSet, UINT4 *, UINT4 *);
INT4  Bgp4RedRmReleaseMemoryForMsg (UINT1 *pu1Block);
VOID  Bgp4RedAddTblNode (tBgpRmMsg *pMsg);
VOID Bgp4SetNodeStatus (UINT1 u1Status);
VOID  Bgp4RedVerifyAndSendBulkRequest (VOID);
INT4  Bgp4RedSyncMpathOperCnt (UINT4 u4Context,UINT4 u4OperIbgpCnt,
                               UINT4 u4OperEbgpCnt,UINT4 U4OperEIbgpCnt);
VOID Bgp4RedProcessMpathOperCntSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet, 
                                   UINT2 u2LenOffSet);
INT4 Bgp4RedSyncBgpPeerMPV4CapBulkMsg (tBgpRmMsg * pMsg, UINT4 *pu4Offset);
VOID Bgp4RedProcessBgpPeerMPV4CapSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                   UINT2 u2Length);
INT4 Bgp4RedSyncBgpPeerMPCapV6BulkMsg (tBgpRmMsg * pMsg, UINT4 *pu4Offset);
VOID Bgp4RedProcessBgpPeerMPV6CapSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                   UINT2 u2Length);
INT4
Bgp4RedSyncRRDMetricMaskBulkMsg (tBgpRmMsg * pMsg, UINT4 *pu4Offset,
            UINT4 u4CxtId);
VOID
Bgp4RedProcessRRDMetricMaskSyncMsg (tRmMsg * pMsg, UINT4 *pu4OffSet, UINT2 u2LenOffSet);


