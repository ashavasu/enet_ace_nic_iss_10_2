/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved    
 *
 * $Id: bgp4tdfs.h,v 1.40 2016/12/27 12:35:53 siva Exp $
 *
 * Description:Contain the data structure definitions related to          
 *             the BGP peers.
 *
 *******************************************************************/
#ifndef BGP4_H
#define BGP4_H

/* The flag values for peer which represents the peer state
 *  * and action to be performed by the peer. */
/* RED-BLACK TREE  */
typedef tRBTree     tBgp4RBTree;
typedef tRBNode     tBgp4RBNode;
typedef tRBElem     tBgp4RBElem;


typedef struct t_BgpAppTimer
{
    tTmrAppTimer        Tmr;
    UINT4               u4TimerId;
    UINT4               u4Flag;
}tBgp4AppTimer;

typedef struct t_ResBuf
{
    UINT4               u4WriteOffset;
    UINT4               u4MsgSize;
    UINT1               au1Data[BGP4_MAX_MSG_LEN];
}
tResBuf;

typedef struct t_BufNodeMsg
{
    UINT1               au1BufMsg[BGP4_MAX_MSG_LEN];
}tBufNodeMsg;

typedef struct t_BgpVcmParams
{
   UINT4    u4ContextId;
   UINT1    u1Event;
   UINT1    au1Padding[3];
}tBgp4VcmParams;

typedef struct t_BgpPeerLocalASMSg
{
    tAddrPrefix  PeerAddress;
    UINT4        u4LocalAs;
    UINT4  u4Data; /*Introduced as part of TCP-AO implementation. Can be used for other purposes also*/
}tBgpPeerLocalASMSg;

/* linear list for holding buffer */
typedef struct t_BufNode
{
    tTMO_SLL_NODE       TSNext;
    UINT1              *pu1Msg;
    UINT4               u4MsgSize;
    UINT4               u4MsgOffset;
}
tBufNode;

typedef struct _tPrefixCounters
{
    UINT4   u4PrefixesRcvd;
    UINT4   u4PrefixesSent;
    UINT4   u4WithdrawsRcvd;
    UINT4   u4WithdrawsSent;
    UINT4   u4InPrefixes;
    UINT4   u4InPrefixesAcptd;
    UINT4   u4InPrefixesRjctd;
    UINT4   u4OutPrefixes;
    UINT4   u4InvalidMpeAttribCntr;
} tPrefixCounters;

typedef struct t_BGP4_DLL{
    struct t_RouteProfile   *pFirst;    /* Points at the first route in
                                           the list */
    struct t_RouteProfile   *pLast;     /* Points at the last route in
                                           the list */
    UINT4                   u4Count;   /**** Number Of Routes In List ****/
}
tBGP4_DLL;

/* 
 * Route refresh statistical data. This data corresponds to a particular peer
 * and is applicable for the time the BGP session is active with the peer. 
 */

typedef struct t_RtRefreshData {
   UINT4       u4RtRefMsgSentCntr; /* Counter specifying no. of route-refresh
                                    * messages sent successfully */

   UINT4       u4RtRefMsgRcvdCntr; /* Counter specifying no. of route-refresh
                                    * messages received and processed 
                                    * successfully  */

   UINT4       u4RtRefMsgTxErrorCntr;  /* Counter specifying no. of 
                                        * route-refresh messages that could not
                                        * be sent out successfully */
                                
   UINT4       u4RtRefMsgRcvdInvalidCntr; /* Counter specifying no. of invalid
                                           * route-refresh messages received
                                           * from peer. */

   UINT1       u1RtRefReqForPeer;   /* This field is set when the route-refresh
                                     * request needs to be sent to peer. This is
                                     * reset by default or after sending the
                                     * route-refresh message to the peer */
   UINT1       u1OrfReqForPeer;     /* This field is set when the ORF message
                                     * needs to be sent to peer. This is
                                     * reset by default or after sending the
                                     * ORF message (route-refresh message with ORF entries)
                                     * to the peer */

   UINT1       au1Padding[2];       /* Added for padding */
} tRtRefreshData; 

typedef struct _tAfiSafiSpecInfo
{
    tPrefixCounters     PrefixCounters;
    tRtRefreshData     *pRtRefreshData;    /* Pointer to the route-refresh 
                                            * related data  */
    tTMO_HASH_TABLE     *pOutputList;    /* List holding the routes that
                                          * are filtered out in the
                                          * outgoing direction. */
    tTMO_HASH_TABLE     *pNewList;       /* List holding routes to be
                                          * advertised to this BGP peer at
                                          * the expiration of min route
                                          * advertisement interval. */
    tTMO_HASH_TABLE     *pNewLocalList;  /* List holding local routes that
                                          * need to be advertised to this BGP
                                          * peer at the expiration of min AS
                                          * origination interval */
    tTMO_HASH_TABLE     *pAdvtWithList;  /* List holding withdrawn routes 
                                          * that were sent to the peer
                                          * within the last min route-adv/AS 
                                          * origination time-period. */ 
    tTMO_HASH_TABLE     *pAdvtFeasList;  /* List holding feasible routes that
                                          * were sent to the peer within the
                                          * the last min route-adv/AS 
                                          * origination time-period. */
    tTMO_SLL            TSDeInitRtList;  /* This list is a linear list of
                                          * with routes from the peer that 
                                          * need to be updated in the FIB,
                                          * advertised to peers,as part of 
                                          * peer deinit process */
    tBGP4_DLL           TSPeerRouteList; /* DLL holding the routes that are
                                          * received from this peer based
                                          * on <AFI> <SAFI>. */
    UINT4               u4SoftCfgOutReq;   /* Setting this field acts as a 
                                            * trigger for doing outbound
                                            * soft-reconfiguration for this 
                                            * peer. */
} tAfiSafiSpecInfo;

/* Bgp Peer Group structure */
typedef struct t_PeerGroupEntry {
    tTMO_SLL_NODE  NextGroup;
    tTMO_SLL       PeerList;
#ifdef ROUTEMAP_WANTED
    tFilteringRMap RMapIn;
    tFilteringRMap RMapOut;
    tFilteringRMap IpPrefixFilterIn;
    tFilteringRMap IpPrefixFilterOut;
#endif
    UINT1          au1PeerGroupName[BGP_MAX_PEER_GROUP_NAME];
    UINT4          u4SendBuf;
    UINT4          u4RecvBuf;
    UINT4          u4ConnectRetryInterval;
    UINT4          u4HoldInterval;
    UINT4          u4DelayOpenTimeInterval;
    UINT4          u4KeepAliveInterval;
    UINT4          u4MinASOrigInterval;
    UINT4          u4MinRouteAdvInterval;
    UINT4          u4PeerPrefixUpperLimit;
    UINT4          u4IdleHoldTimeInterval;
    UINT4          u4RemoteASNo;
    UINT4    u4LocalASNo;
    UINT4          u4ASNoTmp;    /*To hold old AsNo value during updation or removal*/
    UINT2          u2Afi;
    UINT1          u1PeerGrpType; /* To classify the peer group
                                     as internal or external */
    UINT1          u1Passive; 
    UINT1          u1ConnectRetryCount; 
    UINT1          u1HopLimit;
    UINT1          u1AutomaticStart;
    UINT1          u1AutomaticStop;
    UINT1          u1DelayOpenStatus;
    UINT1          u1DampPeerOscillationStatus;
    UINT1          u1EBGPMultiHop;
    UINT1          u1SelfNextHop;
    UINT1          u1RflClient;   
    UINT1          u1CommSendStatus; 
    UINT1          u1ECommSendStatus; 
    UINT1          u1DftRouteOrigStatus; 
    UINT1          u1MPCapabilty;
    UINT1          u1PeerGrpStatus;
    UINT1          u1OrfType;      /* Type of the ORF supported for this peer group
                                      64 - Address Prefix ORF type*/
    UINT1          u1OrfCapMode;   /* ORF mode supported for this Peer group*
                                      1 - receive
                                      2 - send
                                    */
    UINT1          u1OrfRqst;      /* This will be set when ORF message request is confgured,
                                      and it will be cleared once the ORF messages are sent */
    UINT1          u1BfdStatus; /* Indicates whether BFD monitoring is enabled or not for this 
                                   Peer group */
    UINT1          u1LocalASConfig;     /* Local AS configuration status */
    UINT1          u1OverrideCap;
    UINT1           u1ConnectCount;
    UINT1          au1Padding[3];

}tBgpPeerGroupEntry;

/* peer init info */
typedef struct t_PeerInitInfo {
    tNetAddress                 peerInitRtInfo;
    struct t_Bgp4RcvdPathAttr  *pBgp4InitRcvdPA;
    UINT4                       u4HashKey;
#ifdef L3VPN
    UINT4                       u4Context;
#endif
    UINT2                       u2PeerInitAfi;
    UINT2                       u2PeerInitSafi;
}tPeerInitInfo;

typedef struct t_PeerConfig
{
    tAddrPrefix         GatewayAddrInfo;
    tAddrPrefix         RemoteAddrInfo;
    tNetAddress         LocalAddrInfo; 
    tAddrPrefix         LinkLocalAddrInfo;
    tAddrPrefix         NetworkAddrInfo;
                            /* peer Network address for dual stack */
    tNetAddress         LclAddrInfo; 
                            /* Local address corresponding to network-address */
#ifdef L3VPN
    struct t_ExtCommProfile  *pSooExtCom;
#endif
    UINT4               u4SendBuf;
    UINT4               u4RecvBuf;
    UINT4               u4ConnectRetryInterval;
    UINT4               u4HoldInterval;
    UINT4          u4IdleHoldTimeInterval;
    UINT4          u4IdleHoldIntvlActual;
    UINT4          u4DelayOpenTimeInterval;
    UINT4               u4KeepAliveInterval;
    UINT4               u4MinASOrigInterval;
    UINT4               u4MinRouteAdvInterval;
    UINT4          u4PeerPrefixUpperLimit;
    UINT4               u4ASNo;
    UINT4               u4LocalASNo;  /*Peer Local AS */
    UINT1               u1Passive; /* Indicates whether the connection
                                    * is passive or not*/
    /* RFC 4271 Update */
    UINT1               u1ConnectRetryCount; /* User Configured value */
    /* Removing LocalConnectRetryCount and IsPeerDamped and placing it in PeerStatus*/
    UINT1               u1HopLimit;
    UINT1               u1AdminStatus;
    UINT1               u1AutomaticStart;
    UINT1               u1AutomaticStop;
    UINT1               u1DelayOpenStatus;
    UINT1          u1DampPeerOscillationStatus;
    UINT1               u1AuthType;
    UINT1               u1EBGPMultiHop;
    UINT1               u1SelfNextHop;
    UINT1               u1ConfedPeer;
    UINT1               u1LocalAddrConfigured; /* To identify whether any
                                                * configuration exists for
                                                * local address to be used in
                                                * socket communication */
    UINT1               u1RflClient;      /* Whether peer is rfl client or not*/
    UINT1               u1CommSendStatus; /* policy for sending communities to 
                                           * the peer */
    UINT1               u1ECommSendStatus; /* policy for sending ext-community 
                                            * to the peer */
#ifdef L3VPN
    UINT1               u1PeerRole; /* Represens the role of the peer.
                                     * CE = 1; PE = 2;
                                     */
    UINT1               u1CERTAdvtStatus; /* Indicates whether CE is allowed
                                           * to send Route-Targets or not. */
    UINT1  u1pad[1];
#else
   UINT1               u1pad[3];
#endif

    UINT1               u1DftRouteOrigStatus; /* Policy for sending the default
                                               * route to this peer. */
    UINT1               u1VrfConfig;     /* Local AS configuration status */
    UINT1               u1OverrideCap;
    UINT1               u1HoldAdvtRoutes;
    UINT1               u1ConnectCount;
}
tPeerConfig;

typedef struct t_PeerStatus
{
    INT1                ai1PeerFsmTransitionHist[12];
    UINT4               u4PeerCurrentState;  
                                         /* Flag indicating the current state
                                          * of the peer */
    UINT4               u4PeerPendState; /* Flag indicating the pending state
                                          * that needs to be processed for peer
                                          */ 
    /* RFC 4271 Update */
    /* Local Value to Check for Peer Prefix LimiPeer Prefix Limit */
    UINT4               u4LocalPeerPrefixLimit;
    UINT2               u2PeerIndex;
    INT2                i2PeerFsmTransitionHistHead;
    INT2                i2PeerFsmTransitionHistTail;
    UINT1               u1LocalConnectRetryCount; 
    /* Local Value to Check for AutoStop Feature */
    UINT1          u1IsPeerDamped;
    UINT1               u1PeerState;
    UINT1               u1PeerPrevState;
    UINT1               u1DirectlyConnected;
    UINT1               u1IsPeerInTxList;  /* This variable describes whether
                                            * the peer is currently present in
                                            * the peer tranx list or not.
                                            */
    UINT1              u1RestartMode;/* This variable indicates the restart mode
                                       * of the speaker */
    UINT1               u1EndOfRIBMarkerSent; 
    /*This variable indicates whether EndOfRIB
     marker is sent to this peer or not */
    UINT1               u1EndOfRIBMarkerReceived; 
    /* This variable indicates whether
     * EndOfRIB marker is received from this peer or not */
    UINT1               u1EORRcvdForSupportedCapabilities; 
    UINT1               u1EORForPeer; 
    UINT1               au1Pad[3];

}
tPeerStatus;

typedef struct t_PeerStatistics
{
    UINT4               u4InUpdates;
    UINT4               u4OutUpdates;
    UINT4               u4InMessages;
    UINT4               u4OutMessages;
    UINT4               u4FSMTransitions;
    UINT4               u4EstablishedTimer;
    UINT4               u4InUpdateElapTime;
    UINT4               u2IfIndex;
    UINT1               u1LastErrorCode;
    UINT1               u1LastErrorSubCode;
    UINT1               au1Pad[2];
}
tPeerStatistics;

typedef struct t_PeerLocal
{
    tBgp4AppTimer       tStartTmr;
    tBgp4AppTimer       tHoldTmr;
    tBgp4AppTimer       tConnRetryTmr;
    tBgp4AppTimer       tKeepAliveTmr;
    tBgp4AppTimer       tOrigTmr;
    tBgp4AppTimer       tRouteAdvTmr;
    tBgp4AppTimer       tStaleTmr;
    tBgp4AppTimer       tPeerRestartTmr;
    tBgp4AppTimer       tIdleHoldTmr;
    tBgp4AppTimer       tDelayOpenTmr;
    UINT4               u4StartInterval;
    UINT4               u4PeerBGPId;
    UINT4               u4LocalPortNo;
    UINT4               u4RemotePortNo;
    UINT4               u4NegHoldInterval;
    UINT4               u4NegKeepAliveInterval;
    INT4                i4TCPConnId;
    UINT1               u1NegotiatedVersion;
    UINT1               au1Pad[3];    /* for 4byte alignment */
}
tPeerLocal;

/* Structure to store the TCP-MD5 password for a particular peer */
typedef struct t_TcpMd5AuthPasswd{
   UINT1       au1TcpMd5Passwd[BGP4_TCPMD5_PWD_SIZE];
   UINT1       u1TcpMd5PasswdLength; 
   UINT1       u1PwdStatus;
   UINT1       au1Padding[2];
} tTcpMd5AuthPasswd;

/* Structure for storing TCP-MD4 global data */
typedef struct t_TcpMd5GlobalData {
UINT4           u4Md5AllocCtr;
}tTcpMd5GlobalData;

/* Structure for storing TCP-AO global data */
typedef struct t_TcpAoGlobalData {
    UINT4           u4TcpAoAllocCtr;
}tTcpAoGlobalData;

/* Structure to store TCP-AO MKT */
typedef struct TcpAoAuthMKT{
    tTMO_SLL_NODE TSNext;
    UINT1       u1SendKeyId;
    UINT1       u1ReceiveKeyId;
    UINT1       u1KDFAlgo;
    UINT1       u1MACAlgo; 
    UINT1       au1TcpAOMasterKey[80]; 
    UINT1       u1TcpAOPasswdLength;
    UINT1       u1MktRowStatus;
    UINT1       u1TcpOptionIgnore;
    UINT1       u1RcvKeyIdConfigured; 
} tTcpAoAuthMKT;
                                
/*This structure represents the pending softconfig inbound/outbound afi safi
 * values. At any time only one task will be pending */
typedef struct t_SoftConfData {
   tTMO_SLL InPendAfiSafiList;   /* Queues the incoming pending route-refresh 
                                    requests */
   tTMO_SLL OutPendAfiSafiList;  /* Queues the outgoing pending route-refresh 
                                   requests */
   UINT4    u4InitialAsafiMask;  /* This value represents the 
                                  * initial <afi,safi> value
                                  * configured for peer soft init.
                                  */
   UINT2    u2InboundAfi;  /* Address Family Identifier  */      
   UINT2    u2InboundSafi; /*Subsequent Address Family Identifier */ 
   UINT2    u2OutboundAfi;
   UINT2    u2OutboundSafi;
}tSoftConfData;

typedef struct t_AFISAFINODE
{
    tTMO_SLL_NODE    TsAfiSafiNext;
    UINT4            u4AfiSafiMask;
} tAfiSafiNode;

typedef struct t_PeerCapsInfo
{
    tTMO_SLL        SpkrSupCapsLst;    /*List of Capabilities that are 
                                             supported by the router for the 
                                             peer */ 
    tTMO_SLL         PeerCapsRcvdLst;   /*List of Capabilities that are 
                                             received from peer */ 
    UINT4            u4PeerCapsFlags;/* flags for peer capabilities */ 
}tPeerCapsInfo;

/* This structure contains peer related attributes that are common to
 * peer group. This will be populated whenever a change is done to common
 * attributes in tBgp4PeerEntry and will serve as a backup when the
 * corresponding peer is removed from a peer group. If any new attribute
 * is added in tBgp4PeerEntry that is common to peer group also, then
 * take care to include that attribute in this structure and its
 * handling.
 */
typedef struct t_ShadowPeerConfig
{
#ifdef ROUTEMAP_WANTED
    tFilteringRMap      FilterInRMap;     /*  IN RouteMap on the peer  */
    tFilteringRMap      FilterOutRMap;    /*  Out RouteMap on the Peer */
    tFilteringRMap      IpPrefixFilterIn; /*  IN IP-Prefix filter for the peer  */
    tFilteringRMap      IpPrefixFilterOut;/*  Out IP-Prefix filter for the peer */
#endif /* ROUTEMAP_WANTED */
    tPeerCapsInfo  PeerCapsInfo;
    UINT4          u4SendBuf;
    UINT4          u4RecvBuf;
    UINT4          u4ConnectRetryInterval;
    UINT4          u4HoldInterval;
    UINT4          u4DelayOpenTimeInterval;
    UINT4          u4KeepAliveInterval;
    UINT4          u4MinASOrigInterval;
    UINT4          u4MinRouteAdvInterval;
    UINT4          u4PeerPrefixUpperLimit;
    UINT4          u4IdleHoldTimeInterval;
    UINT4          u4RemoteASNo;
    UINT4          u4LocalASNo;
    UINT1          u1Passive;
    UINT1          u1ConnectRetryCount;
    UINT1          u1HopLimit;
    UINT1          u1AutomaticStart;
    UINT1          u1AutomaticStop;
    UINT1          u1DelayOpenStatus;
    UINT1          u1DampPeerOscillationStatus;
    UINT1          u1EBGPMultiHop;
    UINT1          u1SelfNextHop;
    UINT1          u1RflClient;
    UINT1          u1CommSendStatus;
    UINT1          u1ECommSendStatus;
    UINT1          u1DftRouteOrigStatus;
    UINT1          u1LocalASConfig;
    UINT1          u1OverrideCap;
    UINT1          u1ConnectCount;
    UINT1          u1MPCapabilty;
    UINT1          u1OrfMode;
    UINT1          u1BfdStatus;
    UINT1          au1Pad[1];
}
tBgp4ShadowPeerConfig;

typedef struct t_PeerEntry
{
    tTMO_SLL_NODE       TsnNextpeer; /*
                                      * This should also be the first node as 
                                      * address of the peer entry will be passed
                                      * while adding to the list.
                                      */
    tTMO_SLL_NODE      NextPeerList;  /* Pointer to the next peer in the peer group
                                       * to which this peer is a member */
    tAfiSafiSpecInfo   *apAsafiInstance[BGP4_MPE_ADDR_FAMILY_MAX_INDEX];
                            /*
                             * This is per <AFI,SAFI> specificinstance.
                             * Only when a particular <AFI,SAFI>
                             * (other than IPV4, unicast) is negotiated during
                             * session establishment via capabilities
                             * advertisement, then this instance will be creatd.
                             * Default - <IPV4, Unicast> will be created.
                             */
    tBgp4ShadowPeerConfig shadowPeerConfig; /* Structure in which configured
                                               peer values will be stored. */
    tPeerConfig         peerConfig;  /* Structure holding all peer configuration
                                      * parameters. */
    tPeerStatus         peerStatus;  /* Structure holding peer current status 
                                      * related information */
    tPeerStatistics     peerStatistics; /* Structure holding peer statistics
                                         * related information */
    tPeerLocal          peerLocal;   /* Structure holding local information 
                                      * related to the BGP session with the 
                                      * peer */
    tBufNode            ResidualBuf; /* Stores the BGP message received from
                                      * TCP. If the message is complete then
                                      * it is processed immediately. If the
                                      * message is fragmented, then it is 
                                      * stored still the next frame is 
                                      * received from TCP and if the new 
                                      * frame starts with a new message,
                                      * then the current frag. message is
                                      * discarded. */
    tTMO_SLL            TSRcvdRouteList; /* This list stores all incoming BGP
                                          * route-updates received from this
                                          * peer, till they are processed and 
                                          * updated in the local RIB. */
    tTMO_SLL            TSAdvtBufList;     /* This list is a linear link list
                                            * of messages that needs to be 
                                            * transmitted to the peer. */
    tPeerInitInfo       peerInitInfo;      /* This structure contains the 
                                            * route information for peer
                                            * init/de-init/soft-reconfig */
    tPeerCapsInfo  PeerCapsInfo;      /*This strucuture contains the 
                                            capability information of a peer*/
    tTcpMd5AuthPasswd  *pTcpMd5AuthPasswd; /* Pointer to the TCP MD5 related
                                            * data */
    tSoftConfData      SoftConfData;   /*This structurei gives pending 
                                             soft config inbound/outbound 
                                             AFI and SAFI  */ 
    tBgpPeerGroupEntry *pPeerGrp;   /* Pointer to the peer group to which 
                                       this peer is a member */
    struct t_BgpCxtNode *pBgpCxtNode;  /* Pointer to the context node in which
                                         the peer is learnt */
    UINT1              *pu1ReadvtBuffer;   /* This pointer stores the message
                                            * that needs to be retransmitted.
                                            * This pointer of type tBufNode.
                                            */  
    UINT4               u4NegCapMask;      /* Each bit in this mask represents 
                                            * the negotiated capabilities with 
                                            * the peer. */
    UINT4               u4NegAsafiMask;    /* Each bit in this mask represents
                                            * the <AFI,SAFI> pair negotiated 
                                            * with the peer */
    UINT4              u4SupAsafiMask;   /*Each bit in this mask represents 
                                           <AFI,SAFI> pait supported for 
                                           the peer*/
    UINT4               u4RestartInterval; /* Indicates the restart interval */
    tTcpAoAuthMKT  *pTcpAOAuthMKT;    /*Pointer to the MKT structure in the per context TCP-AO MKT table*/ 

    UINT1  u1TcpAoMktDiscard; /*This determines whether to discard incoming TCP segements without AO
         *if TCP-AO is enabled for the peer */
    UINT1  u1TcpAoAuthIcmpAcc;/*This determines if ICMP messages (without AO option) need to be accepted*/
    UINT1               u1IsOrfSent;    /* Indicates whether ORF message has been 
                                              already sent to the peer or not */
    UINT1  u1BfdStatus; /*Indicates whether BFD monitoring is enabled for this peer or not */
    BOOL1  b1IsBfdRegistered; /*Indicates whether path monitoring is already registered with BFD or not */
    UINT1  au1Pad [3];
#ifdef EVPN_WANTED
    BOOL1  b1IsMPIpv4Enabled; /* Indicates whether MP Ipv4 unicast is enabled or not */
    UINT1  au1Rsvd [3];
#endif
#ifdef ROUTEMAP_WANTED
    tFilteringRMap      FilterInRMap;     /*  IN RouteMap on the peer  */
    tFilteringRMap      FilterOutRMap;    /*  Out RouteMap on the Peer */
    tFilteringRMap      IpPrefixFilterIn; /*  IN IP-Prefix filter for the peer  */
    tFilteringRMap      IpPrefixFilterOut;/*  Out IP-Prefix filter for the peer */
#endif /* ROUTEMAP_WANTED */
    tBgp4RBTree         AdvRoutes;
}
tBgp4PeerEntry;

/* Trace and Debug structure  */
typedef struct t_Bgp4Glb
{
    UINT4               u4Bgp4Dbg;
    UINT4               u4Bgp4Trc;
}
tBgp4Glb;

typedef struct t_BgpPeerData {
    tBgp4PeerEntry    **paPeerPtr;
    UINT4               u4MaxPeer;  /* This value represents the maximum 
                                     * index of the paPeerPtr array */
    INT4                i4MaxFd;
}tBgpPeerData;

/* peer link list Structures */
typedef struct t_PeerNode {
    tTMO_SLL_NODE       TSNext;
    tBgp4PeerEntry      *pPeer;
}tPeerNode;

typedef struct t_BgpErrorRecord {
    UINT4       u4TcpRecvMsgErr;    /* This variable indicates the number
                                     * of incomplete packets received from
                                     * TCP while recv'ing data from socket. */
}tBgpErrorRecord;

typedef struct t_NextHopInitInfo
{
    tNetAddress   NHInitRoute;
    tAddrPrefix   NHInitPeerAddr;
    tAddrPrefix   NHInitNextHop;
    UINT4         u4VrfId;
}tNextHopInitInfo;

typedef struct t_NonBgpInitInfo
{
    tNetAddress   NonBgpInfo;
    UINT4         u4VrfId;
}tNonBgpInitInfo;


typedef struct t_RtRefReqMsg {
    tAddrPrefix PeerRemoteAddress; /* Peer Address */
    UINT4       u4AsafiMask;       /* Mask corresponds to <AFI, SAFI> */
    UINT1       u1OperType; /* Advertisement operation type */
    UINT1       au1Pad[3];  /* added for 4bytes alignment */
}tRtRefReqMsg;

typedef struct t_Bgp4PeerDataList {
    tBgp4PeerEntry   *aBgp4PeerEntry[MAX_BGP_PEERS*BGP4_MAX_ENTRY_PER_PEER];
}tBgp4PeerDataList;

typedef struct t_Bgp4RfdDecayArray {
    UINT1   au1RfdDecay[MAX_BGP_RFD_DECAY_ARRAY_SIZE];
}tBgp4RfdDecayArray;

typedef struct t_Bgp4RfdReuseIndexArray {
    UINT1   au1RfdReuseIndex[MAX_BGP_RFD_REUSE_INDEX_ARRAY_SIZE];
}tBgp4RfdReuseIndexArray;

typedef struct t_BgpRmMsg{
    tDbTblNode  DbNode;    /* To add the RM msgs to DBList */
    UINT4       u4DataLen; /* Length of the RM msg */
    tRmMsg     *pBuf;      /* RM msg to sync with redundant BGP router */
    UINT1       u1Event;   /* RM event posted to BGP */
    UINT1       au1Pad[3];
}tBgpRmMsg;

typedef struct _tNetworkAddr
{
    tRBNodeEmbd   RBNode;
    tAddrPrefix NetworkAddr; /* Represents the Network address */
    UINT4       u4RowStatus;
    UINT2       u2PrefixLen; /* Represents the Prefix Length */
    UINT1       au1Pad[2] ;      
}tNetworkAddr;

/* Peer current state can show one of the following ongoing jobs */
#define BGP4_PEER_INIT_INPROGRESS                    0x00000001
#define BGP4_PEER_DEINIT_INPROGRESS                  0x00000002
#define BGP4_PEER_SOFTCONFIG_INBOUND_INPROGRESS      0x00000004
#define BGP4_PEER_SOFTCONFIG_OUTBOUND_INPROGRESS     0x00000008
#define BGP4_PEER_READY                              0x00000010
#define BGP4_PEER_REUSE_INPROGRESS                   0x00000020
#define BGP4_PEER_UNKNOWN_STATE                      0
/* This flag is to identify the intermediate state of a GR 
 * capable peer when its tcp close is identified */
#define BGP4_PEER_RST_CLOSE_IDENTIFIED               0x00000100
#define BGP4_PEER_STALE_DEL_INPROGRESS               0x00000200

/* Peer pending state can reflect the following pending jobs */
#define BGP4_PEER_NOPEND_JOBS               0x00000000
/* This flag indicates that the peer admin is enabled, but
 * multihop status is not enabled for this not-directly
 * connected peer. Hence peer is not operationally enabled if
 * this flag is set. */
#define BGP4_PEER_MULTIHOP_PEND_START       0x00000001
#define BGP4_PEER_INIT_PENDING              0x00000002
#define BGP4_PEER_DELETE_PENDING            0x00000004
#define BGP4_PEER_SOFTCONFIG_INBOUND_PEND   0x00000008
#define BGP4_PEER_SOFTCONFIG_OUTBOUND_PEND  0x00000010
#define BGP4_PEER_START_PEND                0x00000020
#define BGP4_PEER_RFL_CLIENT_PEND           0x00000040
/* This flag indicates that peer-deinit for routes in RIB is complete,
 * removal of routes from the peer-lists is in progress */
#define BGP4_DEINIT_PEERLISTS_INPROGRESS    0x00000080
#define BGP4_PEER_ADVT_MSG                  0x00000100
/* This flag indicates that the Damped Peer is pending for Reuse
 * processing. */
#define BGP4_PEER_REUSE_PEND                0x00000200
/* This flag indicates that the Peer start is pending because
 * no MP capability is configured. */
#define BGP4_PEER_MP_CAP_CONFIG_PEND_START  0x00000400
/* This flag indicates that the Peer start is pending because
 * no MP capability common for this peer. */
#define BGP4_PEER_MP_CAP_RECV_PEND_START    0x00000800

/* BGP4 global state */
#define BGP4_GLOBAL_READY                   0x00000001
#define BGP4_GLOBAL_ADMIN_DOWN_INPROGRESS   0x00000002
#define BGP4_GLOBAL_ADMIN_UP_INPROGRESS     0x00000004

/* BGP4 global pend states */
#define BGP4_NO_PENDJOBS                    0x00000000
#define BGP4_GLOBAL_ADMIN_UP_PENDING        0x00000001
#define BGP4_GLOBAL_CLEAR_PENDING           0x00000002

/* Status for a entry in the Aggregation table */
#define BGP4_AGGR_NO_FLAG                   0x00
#define BGP4_AGGR_NO_REAGGREGATE            0x01
#define BGP4_AGGR_REAGGREGATE               0x02
#define BGP4_AGGR_ADVERTISE                 0x04
#define BGP4_AGGR_CREATE_IN_PROGRESS        0x08
#define BGP4_AGGR_DELETE_IN_PROGRESS        0x10
#define BGP4_AGGR_PENDING_CREATE            0x20
#define BGP4_AGGR_PENDING_ADVT_CHG          0x40
#define BGP4_AGGR_ALL_FLAG                  0xFF

/* Peer Lists indices */
#define BGP4_PEER_OUTPUT_LIST               0x01
#define BGP4_PEER_NEW_LIST                  0x02
#define BGP4_PEER_NEW_LOCAL_LIST            0x03
#define BGP4_PEER_ADVT_FEAS_LIST            0x04
#define BGP4_PEER_ADVT_WITH_LIST            0x05
#define BGP4_PEER_RCVD_ROUTE_LIST           0x06
#define BGP4_PEER_DEINIT_ROUTE_LIST         0x07

#endif /* BGP4_H */
