/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved    
 *
 * $Id: bgmpprot.h,v 1.4 2016/12/27 12:35:52 siva Exp $
 *
 * Description:Contains the data structures and definitions used                 *             by the Multiprotocol Extensions Handler 
 *
 ************************************************************/
#ifndef _BGMPPROT_H
#define _BGMPPROT_H

/********** Function Prototypes Decleration **********/

INT4    Bgp4MpeProcessAttribute (tBgp4PeerEntry *, UINT1 *, tTMO_SLL *, UINT4 *);
INT4    Bgp4MpeProcessSnpa      (tBgp4PeerEntry *, UINT1 *, tBgp4Info *,
                                 UINT2 *);
INT4    Bgp4MpeProcessNexthop   (tBgp4PeerEntry *, UINT1 *, tBgp4Info *,
                                 UINT2 *);
INT4    Bgp4MpeProcessIpv4Nexthop (tBgp4PeerEntry *, UINT1 *, tBgp4Info *,
                                 UINT2 *);
INT4    Bgp4MpeProcessNlri      (tBgp4PeerEntry *, UINT1 *, tBgp4Info *,
                                 UINT1, tTMO_SLL *, UINT2 *);
INT4    Bgp4MpeProcessIpv4Prefix (tBgp4PeerEntry *, UINT1 *, tRouteProfile *,
                                  UINT2 *);
UINT2   Bgp4MpeGetAttrSize      (tRouteProfile *);
UINT2   Bgp4MpeGetBytesForNlri  (tRouteProfile *);
INT4    Bgp4MpeFillPathAttribute (tRouteProfile *, tBgp4Info *,
                                  tBgp4PeerEntry *);
INT4    Bgp4MpeFillSnpaInfo  (tRouteProfile *, tBgp4Info *, tBgp4PeerEntry *);
INT4    Bgp4MpeFillNexthop   (tRouteProfile *, tBgp4Info *, tBgp4PeerEntry *);
UINT2   Bgp4MpeFillNlriPrefix   (UINT1 *, tRouteProfile *);
UINT2   Bgp4MpeFillIpv4Prefix   (UINT1 *, tRouteProfile *);

INT4    Bgp4MpeFillIpv6Nexthop  (tRouteProfile *, tBgp4Info *, tBgp4PeerEntry *);
INT4    Bgp4MpeProcessIpv6Nexthop (tBgp4PeerEntry *, UINT1 *, tBgp4Info *,
                                   UINT2 *);
UINT2   Bgp4MpeFillIpv6Prefix   (UINT1 *, tRouteProfile *);
INT4    Bgp4MpeProcessIpv6Prefix (tBgp4PeerEntry *, UINT1 *, tRouteProfile  *,
                                  UINT2 *);

INT4    Bgp4MpeInit             (VOID);
INT4    Bgp4MpeShutdown         (VOID);
INT4    Bgp4MpeInitSnpaNode (tSnpaInfoNode *);
tSnpaInfoNode  *Bgp4MpeAllocateSnpaNode (UINT4);
INT4    Bgp4MpeReleaseSnpaNode (tSnpaInfoNode *);
UINT1  *Bgp4MpeAllocateSnpaInfo (UINT4);
INT4    Bgp4MpeReleaseSnpaInfo (UINT1 *);
INT4    Bgp4MpeInitAfiSafiInstance (tBgp4PeerEntry *, UINT4);
tAfiSafiSpecInfo   *Bgp4MpeAllocateAfiSafiInstance (tBgp4PeerEntry *, UINT4);
INT4    Bgp4MpeReleaseAfiSafiInstance (tBgp4PeerEntry *, UINT4);
UINT1   Bgp4DshPrefixMatch (const tRouteProfile * , const tRouteProfile * );

/* LOW-LEVEL added */
tRouteProfile * Bgp4MpeGetRoute (UINT1 *, UINT4 , UINT1 , UINT4 , UINT1 *);
UINT1   Bgp4MpeValidateAddressType (UINT4);
UINT1   Bgp4MpeValidateSubAddressType (UINT4);

#endif /* End of BGMPPROT_H */
