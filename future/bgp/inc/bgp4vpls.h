/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved             *
 *                                                                  *
 * $Id: bgp4vpls.h,v 1.3 2014/11/20 12:12:14 siva Exp $         *
 *                                                                  *
 * Description:Contains the data structures and definitions         *
 *             used in L2VPN-VPLS                                   *
 *                                                                  *
 ********************************************************************/

#ifndef BGP4VPLS_H
#define BGP4VPLS_H

#include "bgp4l2vpn.h"

/* Function Prototypes */
#ifdef MPLS_WANTED
INT4 Bgp4VplsConstructRouteProfile (  UINT4          u4Context ,
                                    tRouteProfile  *pRtprofile ,
                                    tBgp4PeerEntry *pPeer,
                                    tVplsNlriInfo  *VplsNlri,
                                    UINT4           u4VplsIndex );

INT4 Bgp4VplsUpdate (tBgp4L2VpnEvtInfo  *pL2vpnMsg, tVplsSpecInfo * pVplsInfo);
INT4 Bgp4VplsCreate (tBgp4L2VpnEvtInfo  *pL2vpnMsg, tVplsSpecInfo ** ppVplsInfo, UINT4 u4Context);
INT4 Bgp4VplsDelete (tVplsSpecInfo * pVplsSpecInfo, UINT4 u4Context);
INT4 Bgp4VplsDownHdlr (tVplsSpecInfo * pVplsInfo, UINT4 u4Context);
INT4 Bgp4VplsUpHdlr (tVplsSpecInfo * pVplsInfo, UINT4 u4Context);
INT4 Bgp4VplsHandler(UINT1 *, UINT1 );
INT4 Bgp4VplsUpdateHandler(tBgp4L2VpnEvtInfo *, UINT1, UINT4 );
tExtCommProfile* Bgp4VplsGetRouteTarget (tVplsSpecInfo * pVplsInfo, UINT1 *pu1ExtComm, UINT1 u1RTType);
tExtCommProfile* 
Bgp4VplsRouteTargetChgHandler (tVplsSpecInfo * pVplsInfo, INT4 i4RTType, UINT1 *pu1ExtComm, UINT1 u1OperType);

INT4 Bgp4VplsExtCommHandler(tExtendedCommInfo *, UINT1 );

INT4 Bgp4VplsCreateAdvtMsgforL2VPN  (tRouteProfile  *);
INT4 Bgp4VplsCreateWithdMsgforL2VPN  (tRouteProfile  *);

INT4 Bgp4VplsGetL2ExtCommInfo (tRouteProfile *pRouteProfile, tExtCommLayer2Info *pExtCommInfo);
INT4 Bgp4VplsGetRTfromRouteProfile (tRouteProfile *pRouteProfile,UINT1 *au1RouteTarget);
VOID Bgp4IndicateASNUpdate(UINT1 u1ASNType, UINT4 u4ASNValue);
INT4 Bgp4AdminStatusEventToL2Vpn(UINT4 u4AdminStatus);
INT4 Bgp4ProcessVplsDown (tVplsSpecInfo * pVplsInfo, UINT4 u4Context);
INT4 Bgp4VplsIsRouteCanBeProcessed(tRouteProfile * pRtProfile);

INT4 Bgp4VplsGetVplsHashKey (UINT4 u4VplsIndex, UINT1 *pu1HashKey);
INT4 Bgp4VplsGetVplsInfoFromIndex (UINT4 u4VplsIndex, tVplsSpecInfo ** ppVplsInfo);
INT4
Bgp4VplsRouteTargetDel(tBgp4L2VpnEvtInfo  *pL2vpnMsg, UINT1  u1RtType, UINT1 u1RtOpr, UINT4 u4Context);
INT4
Bgp4VplsRouteTargetAdd(tBgp4L2VpnEvtInfo  *pL2vpnMsg, UINT1  u1RtType, UINT1 u1RtOpr, UINT4 u4Context);
tVplsSpecInfo* Bgp4VplsFindMatchingImportRT (UINT1 *pu1RTCommVal, tRouteProfile * pRtProfile);

INT4 Bgp4VplsMPAdvtHandler (tVplsAdvtInfo *, UINT4, UINT4);
INT4 Bgp4VplsMPWithdrawnHandler (tPeerRouteAdvtInfo *, UINT4, UINT4);
UINT4 Bgp4VplsReleaseRouteTargetsFromVpls (tTMO_SLL * pTsList, UINT1 u1RTType);
#ifdef VPLS_GR_WANTED
VOID Bgp4LinkVplsSpecToRouteInStandby (tRouteProfile *, UINT4, UINT4);
INT4 Bgp4VplsNotifyGRToL2VPN (UINT4 u4CtxId);
INT4 Bgp4VplsNotifyRestartToL2VPN (UINT4 u4CtxId);
INT4 Bgp4VplsGREORFromL2VPN (UINT4 u4CtxId);
INT4 Bgp4VplsGRInProgress (UINT4 u4CtxId);
INT4 Bgp4RedSyncVplsInfo (tVplsSpecInfo *,tVplsSyncEventInfo *);
INT4 Bgp4RedFillBufVplsSpecInfo (tVplsSpecInfo * , tRmMsg * ,  UINT4 *, tVplsSyncEventInfo *);
INT4 Bgp4RedProcessVplsInfoSyncMsg (tRmMsg * , UINT4 *, UINT2 );
INT4 Bgp4RedSyncVplsInfoBulkMsg (tBgpRmMsg * , UINT4 *);
INT4 Bgp4RedProcessVplsInfoBulkMsg (tRmMsg * , UINT4 *, UINT2 );
UINT4 Bgp4VplsGetVplsIntsSize (tVplsSpecInfo *);
INT4 Bgp4VplsMarkVplsEntryAsStale (UINT4 u4CtxId);
INT4 Bgp4VplsAuditVplsSpecEntry (UINT4 u4CtxId); 
INT4 Bgp4VplsDeleteStaleVplsLocalRoutes (UINT4 u4CtxId);
INT4 Bgp4VplsDeleteVplsStaleEntry (tVplsSpecInfo *, UINT4);
INT4 Bgp4VplsDeleteRT (tVplsSpecInfo *, tTMO_SLL_NODE *, UINT1);
#endif /*VPLS_GR_WANTED*/
#ifdef BGP_TEST_WANTED
INT4
Bgp4VplsShowVplsInstance (tCliHandle CliHandle, UINT4 u4Context);
#endif

#endif /*MPLS_WANTED*/

INT4 Bgp4VplsInit (VOID);
INT4 Bgp4VplsShutdown (VOID);

INT4 Bgp4VplsGetPathAttrinfo(tVplsAdvtInfo *pPeerRouteAdvtInfo ,
                             tBgp4Info *pBGP4Info, UINT4);

INT4 Bgp4VplsApplyImportRTFilter (tTMO_SLL * pInputRtsList, tBgp4PeerEntry * pPeerInfo);
tBgp4VplsRrImportTargetInfo *Bgp4VplsIsRTMatchInRRImportRTList (UINT1 *pu1RTCommVal);

BOOL1 Bgp4IsSameVplsRoute(tBgp4PeerEntry *pPeer ,tRouteProfile *pRtProfile,tRouteProfile *pPeerRoute);

INT4 Bgp4VplsFillVplsExtComm (tRouteProfile * pRouteProfile,
                             tBgp4PeerEntry * pPeerEntry, tBgp4Info * pAdvtBgpInfo);

tBgp4VplsRrImportTargetInfo * Bgp4VplsAddNewRTInRRImportList (UINT1 *pu1RTExtComm);
INT4 Bgp4VplsRrJoin (VOID);
INT4 Bgp4VplsRrPrune (tRouteProfile * pVplsRoute);
INT4 Bgp4VplsRrDeleteTargetFromSet(VOID);

#endif /* BGP4VPLS_H */
