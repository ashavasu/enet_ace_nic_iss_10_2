/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsbgp4db.h,v 1.33 2016/12/27 12:35:53 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSBGP4DB_H
#define _FSBGP4DB_H

UINT1 Fsbgp4PeerExtTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Fsbgp4MEDTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsbgp4LocalPrefTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsbgp4UpdateFilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsbgp4AggregateTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsbgp4RRDMetricTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Fsbgp4ImportRouteTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsbgp4FsmTransitionHistTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Fsbgp4RflRouteReflectorTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Fsbgp4RfdRtDampHistTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsbgp4RfdPeerDampHistTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Fsbgp4RfdRtsReuseListTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsbgp4RfdPeerReuseListTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Fsbgp4CommRouteAddCommTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Fsbgp4CommRouteDeleteCommTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Fsbgp4CommRouteCommSetStatusTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsbgp4CommPeerSendStatusTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Fsbgp4CommInFilterTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Fsbgp4CommOutFilterTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Fsbgp4CommReceivedRouteCommTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Fsbgp4ExtCommRouteAddExtCommTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,8};
UINT1 Fsbgp4ExtCommRouteDeleteExtCommTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,8};
UINT1 Fsbgp4ExtCommRouteExtCommSetStatusTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsbgp4ExtCommPeerSendStatusTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Fsbgp4ExtCommInFilterTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,8};
UINT1 Fsbgp4ExtCommOutFilterTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,8};
UINT1 Fsbgp4PeerLinkBwTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Fsbgp4ExtCommReceivedRouteExtCommTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Fsbgp4CapSupportedCapsTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsbgp4StrictCapabilityMatchTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Fsbgp4CapsAnnouncedTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsbgp4CapReceivedCapsTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsbgp4CapAcceptedCapsTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsbgpAscConfedPeerTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Fsbgp4RtRefreshInboundTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fsbgp4RtRefreshStatisticsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fsbgp4TCPMD5AuthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fsbgp4SoftReconfigOutboundTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fsbgp4MpeBgpPeerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fsbgp4MpeBgp4PathAttrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fsbgp4MpePeerExtTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fsbgp4MpeMEDTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsbgp4MpeLocalPrefTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsbgp4MpeUpdateFilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsbgp4MpeAggregateTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsbgp4MpeImportRouteTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fsbgp4MpeFsmTransitionHistTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fsbgp4MpeRfdRtDampHistTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fsbgp4MpeRfdPeerDampHistTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fsbgp4MpeRfdRtsReuseListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fsbgp4MpeRfdPeerReuseListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fsbgp4MpeCommRouteAddCommTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Fsbgp4MpeCommRouteDeleteCommTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Fsbgp4MpeCommRouteCommSetStatusTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsbgp4MpeExtCommRouteAddExtCommTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,8};
UINT1 Fsbgp4MpeExtCommRouteDeleteExtCommTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,8};
UINT1 Fsbgp4MpeExtCommRouteExtCommSetStatusTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsbgp4MpePeerLinkBwTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fsbgp4MpeCapSupportedCapsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fsbgp4MpeRtRefreshInboundTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 Fsbgp4MpeRtRefreshStatisticsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 Fsbgp4MpeSoftReconfigOutboundTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 Fsbgp4MpePrefixCountersTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 Fsbgp4MplsVpnVrfRouteTargetTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fsbgp4MplsVpnVrfRedistributeTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fsbgp4MplsVpnRRRouteTargetTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsBgp4DistInOutRouteMapTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsBgp4NeighborRouteMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsBgp4PeerGroupTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsBgp4PeerGroupListTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Fsbgp4TCPMKTAuthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsbgp4TCPAOAuthPeerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsBgp4ORFListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsBgp4RRDNetworkTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 fsbgp4 [] ={1,3,6,1,4,1,2076,41};
tSNMP_OID_TYPE fsbgp4OID = {8, fsbgp4};


UINT4 Fsbgp4GlobalAdminStatus [ ] ={1,3,6,1,4,1,2076,41,1,1};
UINT4 Fsbgp4LocalAs [ ] ={1,3,6,1,4,1,2076,41,1,2};
UINT4 Fsbgp4Identifier [ ] ={1,3,6,1,4,1,2076,41,1,3};
UINT4 Fsbgp4Synchronization [ ] ={1,3,6,1,4,1,2076,41,1,4};
UINT4 Fsbgp4DefaultLocalPref [ ] ={1,3,6,1,4,1,2076,41,1,5};
UINT4 Fsbgp4AdvtNonBgpRt [ ] ={1,3,6,1,4,1,2076,41,1,6};
UINT4 Fsbgp4TraceEnable [ ] ={1,3,6,1,4,1,2076,41,1,7};
UINT4 Fsbgp4DebugEnable [ ] ={1,3,6,1,4,1,2076,41,1,8};
UINT4 Fsbgp4OverlappingRoute [ ] ={1,3,6,1,4,1,2076,41,1,9};
UINT4 Fsbgp4MaxPeerEntry [ ] ={1,3,6,1,4,1,2076,41,1,10};
UINT4 Fsbgp4MaxNoofRoutes [ ] ={1,3,6,1,4,1,2076,41,1,11};
UINT4 Fsbgp4AlwaysCompareMED [ ] ={1,3,6,1,4,1,2076,41,1,12};
UINT4 Fsbgp4DefaultOriginate [ ] ={1,3,6,1,4,1,2076,41,1,13};
UINT4 Fsbgp4DefaultIpv4UniCast [ ] ={1,3,6,1,4,1,2076,41,1,14};
UINT4 Fsbgp4GRAdminStatus [ ] ={1,3,6,1,4,1,2076,41,1,15};
UINT4 Fsbgp4GRRestartTimeInterval [ ] ={1,3,6,1,4,1,2076,41,1,16};
UINT4 Fsbgp4GRSelectionDeferralTimeInterval [ ] ={1,3,6,1,4,1,2076,41,1,17};
UINT4 Fsbgp4GRStaleTimeInterval [ ] ={1,3,6,1,4,1,2076,41,1,18};
UINT4 Fsbgp4GRMode [ ] ={1,3,6,1,4,1,2076,41,1,19};
UINT4 Fsbgp4RestartSupport [ ] ={1,3,6,1,4,1,2076,41,1,20};
UINT4 Fsbgp4RestartStatus [ ] ={1,3,6,1,4,1,2076,41,1,21};
UINT4 Fsbgp4RestartExitReason [ ] ={1,3,6,1,4,1,2076,41,1,22};
UINT4 Fsbgp4RestartReason [ ] ={1,3,6,1,4,1,2076,41,1,23};
UINT4 Fsbgp4ForwardingPreservation [ ] ={1,3,6,1,4,1,2076,41,1,24};
UINT4 Fsbgp4IsTrapEnabled [ ] ={1,3,6,1,4,1,2076,41,1,25};
UINT4 Fsbgp4NextHopProcessingInterval [ ] ={1,3,6,1,4,1,2076,41,1,26};
UINT4 Fsbgp4IBGPRedistributionStatus [ ] ={1,3,6,1,4,1,2076,41,1,27};
UINT4 Fsbgp4IBGPMaxPaths [ ] ={1,3,6,1,4,1,2076,41,1,28};
UINT4 Fsbgp4EBGPMaxPaths [ ] ={1,3,6,1,4,1,2076,41,1,29};
UINT4 Fsbgp4EIBGPMaxPaths [ ] ={1,3,6,1,4,1,2076,41,1,30};
UINT4 Fsbgp4OperIBGPMaxPaths [ ] ={1,3,6,1,4,1,2076,41,1,31};
UINT4 Fsbgp4OperEBGPMaxPaths [ ] ={1,3,6,1,4,1,2076,41,1,32};
UINT4 Fsbgp4OperEIBGPMaxPaths [ ] ={1,3,6,1,4,1,2076,41,1,33};
UINT4 Fsbgp4FourByteASNSupportStatus [ ] ={1,3,6,1,4,1,2076,41,1,34};
UINT4 Fsbgp4FourByteASNotationType [ ] ={1,3,6,1,4,1,2076,41,1,35};
UINT4 Fsbgp4VpnLabelAllocPolicy [ ] ={1,3,6,1,4,1,2076,41,1,36};
UINT4 Fsbgp4MacMobDuplicationTimeInterval [ ] ={1,3,6,1,4,1,2076,41,1,37};
UINT4 Fsbgp4MaxMacMoves [ ] ={1,3,6,1,4,1,2076,41,1,38};
UINT4 Fsbgp4VpnRouteLeakStatus [ ] ={1,3,6,1,4,1,2076,41,1,39};
UINT4 Fsbgp4PeerExtPeerRemoteAddr [ ] ={1,3,6,1,4,1,2076,41,2,1,1};
UINT4 Fsbgp4PeerExtConfigurePeer [ ] ={1,3,6,1,4,1,2076,41,2,1,2};
UINT4 Fsbgp4PeerExtPeerRemoteAs [ ] ={1,3,6,1,4,1,2076,41,2,1,3};
UINT4 Fsbgp4PeerExtEBGPMultiHop [ ] ={1,3,6,1,4,1,2076,41,2,1,4};
UINT4 Fsbgp4PeerExtNextHopSelf [ ] ={1,3,6,1,4,1,2076,41,2,1,5};
UINT4 Fsbgp4PeerExtConnSrcIfId [ ] ={1,3,6,1,4,1,2076,41,2,1,6};
UINT4 Fsbgp4PeerExtRflClient [ ] ={1,3,6,1,4,1,2076,41,2,1,7};
UINT4 Fsbgp4MEDIndex [ ] ={1,3,6,1,4,1,2076,41,3,1,1};
UINT4 Fsbgp4MEDAdminStatus [ ] ={1,3,6,1,4,1,2076,41,3,1,2};
UINT4 Fsbgp4MEDRemoteAS [ ] ={1,3,6,1,4,1,2076,41,3,1,3};
UINT4 Fsbgp4MEDIPAddrPrefix [ ] ={1,3,6,1,4,1,2076,41,3,1,4};
UINT4 Fsbgp4MEDIPAddrPrefixLen [ ] ={1,3,6,1,4,1,2076,41,3,1,5};
UINT4 Fsbgp4MEDIntermediateAS [ ] ={1,3,6,1,4,1,2076,41,3,1,6};
UINT4 Fsbgp4MEDDirection [ ] ={1,3,6,1,4,1,2076,41,3,1,7};
UINT4 Fsbgp4MEDValue [ ] ={1,3,6,1,4,1,2076,41,3,1,8};
UINT4 Fsbgp4MEDPreference [ ] ={1,3,6,1,4,1,2076,41,3,1,9};
UINT4 Fsbgp4LocalPrefIndex [ ] ={1,3,6,1,4,1,2076,41,4,1,1};
UINT4 Fsbgp4LocalPrefAdminStatus [ ] ={1,3,6,1,4,1,2076,41,4,1,2};
UINT4 Fsbgp4LocalPrefRemoteAS [ ] ={1,3,6,1,4,1,2076,41,4,1,3};
UINT4 Fsbgp4LocalPrefIPAddrPrefix [ ] ={1,3,6,1,4,1,2076,41,4,1,4};
UINT4 Fsbgp4LocalPrefIPAddrPrefixLen [ ] ={1,3,6,1,4,1,2076,41,4,1,5};
UINT4 Fsbgp4LocalPrefIntermediateAS [ ] ={1,3,6,1,4,1,2076,41,4,1,6};
UINT4 Fsbgp4LocalPrefDirection [ ] ={1,3,6,1,4,1,2076,41,4,1,7};
UINT4 Fsbgp4LocalPrefValue [ ] ={1,3,6,1,4,1,2076,41,4,1,8};
UINT4 Fsbgp4LocalPrefPreference [ ] ={1,3,6,1,4,1,2076,41,4,1,9};
UINT4 Fsbgp4UpdateFilterIndex [ ] ={1,3,6,1,4,1,2076,41,5,1,1};
UINT4 Fsbgp4UpdateFilterAdminStatus [ ] ={1,3,6,1,4,1,2076,41,5,1,2};
UINT4 Fsbgp4UpdateFilterRemoteAS [ ] ={1,3,6,1,4,1,2076,41,5,1,3};
UINT4 Fsbgp4UpdateFilterIPAddrPrefix [ ] ={1,3,6,1,4,1,2076,41,5,1,4};
UINT4 Fsbgp4UpdateFilterIPAddrPrefixLen [ ] ={1,3,6,1,4,1,2076,41,5,1,5};
UINT4 Fsbgp4UpdateFilterIntermediateAS [ ] ={1,3,6,1,4,1,2076,41,5,1,6};
UINT4 Fsbgp4UpdateFilterDirection [ ] ={1,3,6,1,4,1,2076,41,5,1,7};
UINT4 Fsbgp4UpdateFilterAction [ ] ={1,3,6,1,4,1,2076,41,5,1,8};
UINT4 Fsbgp4AggregateIndex [ ] ={1,3,6,1,4,1,2076,41,6,1,1};
UINT4 Fsbgp4AggregateAdminStatus [ ] ={1,3,6,1,4,1,2076,41,6,1,2};
UINT4 Fsbgp4AggregateIPAddrPrefix [ ] ={1,3,6,1,4,1,2076,41,6,1,3};
UINT4 Fsbgp4AggregateIPAddrPrefixLen [ ] ={1,3,6,1,4,1,2076,41,6,1,4};
UINT4 Fsbgp4AggregateAdvertise [ ] ={1,3,6,1,4,1,2076,41,6,1,5};
UINT4 Fsbgp4RRDAdminStatus [ ] ={1,3,6,1,4,1,2076,41,7,1};
UINT4 Fsbgp4RRDProtoMaskForEnable [ ] ={1,3,6,1,4,1,2076,41,7,2};
UINT4 Fsbgp4RRDSrcProtoMaskForDisable [ ] ={1,3,6,1,4,1,2076,41,7,3};
UINT4 Fsbgp4RRDDefaultMetric [ ] ={1,3,6,1,4,1,2076,41,7,4};
UINT4 Fsbgp4RRDRouteMapName [ ] ={1,3,6,1,4,1,2076,41,7,5};
UINT4 Fsbgp4RRDMatchTypeEnable [ ] ={1,3,6,1,4,1,2076,41,7,6};
UINT4 Fsbgp4RRDMatchTypeDisable [ ] ={1,3,6,1,4,1,2076,41,7,7};
UINT4 FsBgp4RRDMetricProtocolId [ ] ={1,3,6,1,4,1,2076,41,7,8,1,1};
UINT4 FsBgp4RRDMetricValue [ ] ={1,3,6,1,4,1,2076,41,7,8,1,2};
UINT4 Fsbgp4ImportRoutePrefix [ ] ={1,3,6,1,4,1,2076,41,8,1,1};
UINT4 Fsbgp4ImportRoutePrefixLen [ ] ={1,3,6,1,4,1,2076,41,8,1,2};
UINT4 Fsbgp4ImportRouteProtocol [ ] ={1,3,6,1,4,1,2076,41,8,1,3};
UINT4 Fsbgp4ImportRouteNextHop [ ] ={1,3,6,1,4,1,2076,41,8,1,4};
UINT4 Fsbgp4ImportRouteIfIndex [ ] ={1,3,6,1,4,1,2076,41,8,1,5};
UINT4 Fsbgp4ImportRouteMetric [ ] ={1,3,6,1,4,1,2076,41,8,1,6};
UINT4 Fsbgp4ImportRouteAction [ ] ={1,3,6,1,4,1,2076,41,8,1,7};
UINT4 Fsbgp4Peer [ ] ={1,3,6,1,4,1,2076,41,9,1,1};
UINT4 Fsbgp4FsmTransitionHist [ ] ={1,3,6,1,4,1,2076,41,9,1,2};
UINT4 Fsbgp4RflbgpClusterId [ ] ={1,3,6,1,4,1,2076,41,10,1,1};
UINT4 Fsbgp4RflRflSupport [ ] ={1,3,6,1,4,1,2076,41,10,1,2};
UINT4 Fsbgp4RflPathAttrAddrPrefix [ ] ={1,3,6,1,4,1,2076,41,10,2,1,1};
UINT4 Fsbgp4RflPathAttrAddrPrefixLen [ ] ={1,3,6,1,4,1,2076,41,10,2,1,2};
UINT4 Fsbgp4RflPathAttrPeer [ ] ={1,3,6,1,4,1,2076,41,10,2,1,3};
UINT4 Fsbgp4RflPathAttrOriginatorId [ ] ={1,3,6,1,4,1,2076,41,10,2,1,4};
UINT4 Fsbgp4RflPathAttrClusterList [ ] ={1,3,6,1,4,1,2076,41,10,2,1,5};
UINT4 Fsbgp4RfdCutOff [ ] ={1,3,6,1,4,1,2076,41,11,1,1};
UINT4 Fsbgp4RfdReuse [ ] ={1,3,6,1,4,1,2076,41,11,1,2};
UINT4 Fsbgp4RfdCeiling [ ] ={1,3,6,1,4,1,2076,41,11,1,3};
UINT4 Fsbgp4RfdMaxHoldDownTime [ ] ={1,3,6,1,4,1,2076,41,11,1,4};
UINT4 Fsbgp4RfdDecayHalfLifeTime [ ] ={1,3,6,1,4,1,2076,41,11,1,5};
UINT4 Fsbgp4RfdDecayTimerGranularity [ ] ={1,3,6,1,4,1,2076,41,11,1,6};
UINT4 Fsbgp4RfdReuseTimerGranularity [ ] ={1,3,6,1,4,1,2076,41,11,1,7};
UINT4 Fsbgp4RfdReuseIndxArraySize [ ] ={1,3,6,1,4,1,2076,41,11,1,8};
UINT4 Fsbgp4RfdAdminStatus [ ] ={1,3,6,1,4,1,2076,41,11,1,9};
UINT4 Fsbgp4PathAttrAddrPrefix [ ] ={1,3,6,1,4,1,2076,41,11,2,1,1};
UINT4 Fsbgp4PathAttrAddrPrefixLen [ ] ={1,3,6,1,4,1,2076,41,11,2,1,2};
UINT4 Fsbgp4PathAttrPeer [ ] ={1,3,6,1,4,1,2076,41,11,2,1,3};
UINT4 Fsbgp4RtDampHistInstance [ ] ={1,3,6,1,4,1,2076,41,11,2,1,4};
UINT4 Fsbgp4RfdRtFom [ ] ={1,3,6,1,4,1,2076,41,11,2,1,5};
UINT4 Fsbgp4RfdRtLastUpdtTime [ ] ={1,3,6,1,4,1,2076,41,11,2,1,6};
UINT4 Fsbgp4RfdRtState [ ] ={1,3,6,1,4,1,2076,41,11,2,1,7};
UINT4 Fsbgp4RfdRtStatus [ ] ={1,3,6,1,4,1,2076,41,11,2,1,8};
UINT4 Fsbgp4PeerRemoteIpAddr [ ] ={1,3,6,1,4,1,2076,41,11,3,1,1};
UINT4 Fsbgp4RfdPeerFom [ ] ={1,3,6,1,4,1,2076,41,11,3,1,2};
UINT4 Fsbgp4RfdPeerLastUpdtTime [ ] ={1,3,6,1,4,1,2076,41,11,3,1,3};
UINT4 Fsbgp4RfdPeerState [ ] ={1,3,6,1,4,1,2076,41,11,3,1,4};
UINT4 Fsbgp4RfdPeerStatus [ ] ={1,3,6,1,4,1,2076,41,11,3,1,5};
UINT4 Fsbgp4RtIPPrefix [ ] ={1,3,6,1,4,1,2076,41,11,4,1,1};
UINT4 Fsbgp4RtIPPrefixLen [ ] ={1,3,6,1,4,1,2076,41,11,4,1,2};
UINT4 Fsbgp4PeerRemAddress [ ] ={1,3,6,1,4,1,2076,41,11,4,1,3};
UINT4 Fsbgp4RfdRtReuseListInstance [ ] ={1,3,6,1,4,1,2076,41,11,4,1,4};
UINT4 Fsbgp4RfdRtReuseListRtFom [ ] ={1,3,6,1,4,1,2076,41,11,4,1,5};
UINT4 Fsbgp4RfdRtReuseListRtLastUpdtTime [ ] ={1,3,6,1,4,1,2076,41,11,4,1,6};
UINT4 Fsbgp4RfdRtReuseListRtState [ ] ={1,3,6,1,4,1,2076,41,11,4,1,7};
UINT4 Fsbgp4RfdRtReuseListRtStatus [ ] ={1,3,6,1,4,1,2076,41,11,4,1,8};
UINT4 Fsbgp4RfdPeerRemIpAddr [ ] ={1,3,6,1,4,1,2076,41,11,5,1,1};
UINT4 Fsbgp4RfdPeerReuseListPeerFom [ ] ={1,3,6,1,4,1,2076,41,11,5,1,2};
UINT4 Fsbgp4RfdPeerReuseListLastUpdtTime [ ] ={1,3,6,1,4,1,2076,41,11,5,1,3};
UINT4 Fsbgp4RfdPeerReuseListPeerState [ ] ={1,3,6,1,4,1,2076,41,11,5,1,4};
UINT4 Fsbgp4RfdPeerReuseListPeerStatus [ ] ={1,3,6,1,4,1,2076,41,11,5,1,5};
UINT4 Fsbgp4CommMaxInFTblEntries [ ] ={1,3,6,1,4,1,2076,41,12,1,1};
UINT4 Fsbgp4CommMaxOutFTblEntries [ ] ={1,3,6,1,4,1,2076,41,12,1,2};
UINT4 Fsbgp4AddCommIpNetwork [ ] ={1,3,6,1,4,1,2076,41,12,2,1,1};
UINT4 Fsbgp4AddCommIpPrefixLen [ ] ={1,3,6,1,4,1,2076,41,12,2,1,2};
UINT4 Fsbgp4AddCommVal [ ] ={1,3,6,1,4,1,2076,41,12,2,1,3};
UINT4 Fsbgp4AddCommRowStatus [ ] ={1,3,6,1,4,1,2076,41,12,2,1,4};
UINT4 Fsbgp4DeleteCommIpNetwork [ ] ={1,3,6,1,4,1,2076,41,12,3,1,1};
UINT4 Fsbgp4DeleteCommIpPrefixLen [ ] ={1,3,6,1,4,1,2076,41,12,3,1,2};
UINT4 Fsbgp4DeleteCommVal [ ] ={1,3,6,1,4,1,2076,41,12,3,1,3};
UINT4 Fsbgp4DeleteCommRowStatus [ ] ={1,3,6,1,4,1,2076,41,12,3,1,4};
UINT4 Fsbgp4CommSetStatusIpNetwork [ ] ={1,3,6,1,4,1,2076,41,12,4,1,1};
UINT4 Fsbgp4CommSetStatusIpPrefixLen [ ] ={1,3,6,1,4,1,2076,41,12,4,1,2};
UINT4 Fsbgp4CommSetStatus [ ] ={1,3,6,1,4,1,2076,41,12,4,1,3};
UINT4 Fsbgp4CommSetStatusRowStatus [ ] ={1,3,6,1,4,1,2076,41,12,4,1,4};
UINT4 Fsbgp4PeerAddress [ ] ={1,3,6,1,4,1,2076,41,12,5,1,1};
UINT4 Fsbgp4CommSendStatus [ ] ={1,3,6,1,4,1,2076,41,12,5,1,2};
UINT4 Fsbgp4CommPeerSendRowStatus [ ] ={1,3,6,1,4,1,2076,41,12,5,1,3};
UINT4 Fsbgp4InFilterCommVal [ ] ={1,3,6,1,4,1,2076,41,12,6,1,1};
UINT4 Fsbgp4CommIncomingFilterStatus [ ] ={1,3,6,1,4,1,2076,41,12,6,1,2};
UINT4 Fsbgp4InFilterRowStatus [ ] ={1,3,6,1,4,1,2076,41,12,6,1,3};
UINT4 Fsbgp4OutFilterCommVal [ ] ={1,3,6,1,4,1,2076,41,12,7,1,1};
UINT4 Fsbgp4CommOutgoingFilterStatus [ ] ={1,3,6,1,4,1,2076,41,12,7,1,2};
UINT4 Fsbgp4OutFilterRowStatus [ ] ={1,3,6,1,4,1,2076,41,12,7,1,3};
UINT4 Fsbgp4IpNet [ ] ={1,3,6,1,4,1,2076,41,12,8,1,1};
UINT4 Fsbgp4IPPrefixLength [ ] ={1,3,6,1,4,1,2076,41,12,8,1,2};
UINT4 Fsbgp4PeerRemAddr [ ] ={1,3,6,1,4,1,2076,41,12,8,1,3};
UINT4 Fsbgp4ReceivedRouteCommPath [ ] ={1,3,6,1,4,1,2076,41,12,8,1,4};
UINT4 Fsbgp4ExtCommMaxInFTblEntries [ ] ={1,3,6,1,4,1,2076,41,13,1,1};
UINT4 Fsbgp4ExtCommMaxOutFTblEntries [ ] ={1,3,6,1,4,1,2076,41,13,1,2};
UINT4 Fsbgp4AddExtCommIpNetwork [ ] ={1,3,6,1,4,1,2076,41,13,2,1,1};
UINT4 Fsbgp4AddExtCommIpPrefixLen [ ] ={1,3,6,1,4,1,2076,41,13,2,1,2};
UINT4 Fsbgp4AddExtCommVal [ ] ={1,3,6,1,4,1,2076,41,13,2,1,3};
UINT4 Fsbgp4AddExtCommRowStatus [ ] ={1,3,6,1,4,1,2076,41,13,2,1,4};
UINT4 Fsbgp4DeleteExtCommIpNetwork [ ] ={1,3,6,1,4,1,2076,41,13,3,1,1};
UINT4 Fsbgp4DeleteExtCommIpPrefixLen [ ] ={1,3,6,1,4,1,2076,41,13,3,1,2};
UINT4 Fsbgp4DeleteExtCommVal [ ] ={1,3,6,1,4,1,2076,41,13,3,1,3};
UINT4 Fsbgp4DeleteExtCommRowStatus [ ] ={1,3,6,1,4,1,2076,41,13,3,1,4};
UINT4 Fsbgp4ExtCommSetStatusIpNetwork [ ] ={1,3,6,1,4,1,2076,41,13,4,1,1};
UINT4 Fsbgp4ExtCommSetStatusIpPrefixLen [ ] ={1,3,6,1,4,1,2076,41,13,4,1,2};
UINT4 Fsbgp4ExtCommSetStatus [ ] ={1,3,6,1,4,1,2076,41,13,4,1,3};
UINT4 Fsbgp4ExtCommSetStatusRowStatus [ ] ={1,3,6,1,4,1,2076,41,13,4,1,4};
UINT4 Fsbgp4ExtCommPeerAddress [ ] ={1,3,6,1,4,1,2076,41,13,5,1,1};
UINT4 Fsbgp4ExtCommPeerSendStatus [ ] ={1,3,6,1,4,1,2076,41,13,5,1,2};
UINT4 Fsbgp4ExtCommPeerSendStatusRowStatus [ ] ={1,3,6,1,4,1,2076,41,13,5,1,3};
UINT4 Fsbgp4ExtCommInFilterCommVal [ ] ={1,3,6,1,4,1,2076,41,13,6,1,1};
UINT4 Fsbgp4ExtCommIncomingFilterStatus [ ] ={1,3,6,1,4,1,2076,41,13,6,1,2};
UINT4 Fsbgp4ExtCommInFilterRowStatus [ ] ={1,3,6,1,4,1,2076,41,13,6,1,3};
UINT4 Fsbgp4ExtCommOutFilterCommVal [ ] ={1,3,6,1,4,1,2076,41,13,7,1,1};
UINT4 Fsbgp4ExtCommOutgoingFilterStatus [ ] ={1,3,6,1,4,1,2076,41,13,7,1,2};
UINT4 Fsbgp4ExtCommOutFilterRowStatus [ ] ={1,3,6,1,4,1,2076,41,13,7,1,3};
UINT4 Fsbgp4PeerLinkRemAddr [ ] ={1,3,6,1,4,1,2076,41,13,8,1,1};
UINT4 Fsbgp4LinkBandWidth [ ] ={1,3,6,1,4,1,2076,41,13,8,1,2};
UINT4 Fsbgp4PeerLinkBwRowStatus [ ] ={1,3,6,1,4,1,2076,41,13,8,1,3};
UINT4 Fsbgp4ExtCommIpNet [ ] ={1,3,6,1,4,1,2076,41,13,9,1,1};
UINT4 Fsbgp4ExtCommIPPrefixLength [ ] ={1,3,6,1,4,1,2076,41,13,9,1,2};
UINT4 Fsbgp4ExtCommPeerRemAddr [ ] ={1,3,6,1,4,1,2076,41,13,9,1,3};
UINT4 Fsbgp4ReceivedRouteExtCommPath [ ] ={1,3,6,1,4,1,2076,41,13,9,1,4};
UINT4 Fsbgp4CapabilitySupportAvailable [ ] ={1,3,6,1,4,1,2076,41,14,1,1};
UINT4 Fsbgp4MaxCapsPerPeer [ ] ={1,3,6,1,4,1,2076,41,14,1,2};
UINT4 Fsbgp4MaxInstancesPerCap [ ] ={1,3,6,1,4,1,2076,41,14,1,3};
UINT4 Fsbgp4MaxCapDataSize [ ] ={1,3,6,1,4,1,2076,41,14,1,4};
UINT4 Fsbgp4CapPeerRemoteIpAddr [ ] ={1,3,6,1,4,1,2076,41,14,2,1,1};
UINT4 Fsbgp4SupportedCapabilityCode [ ] ={1,3,6,1,4,1,2076,41,14,2,1,2};
UINT4 Fsbgp4SupportedCapabilityInstance [ ] ={1,3,6,1,4,1,2076,41,14,2,1,3};
UINT4 Fsbgp4SupportedCapabilityLength [ ] ={1,3,6,1,4,1,2076,41,14,2,1,4};
UINT4 Fsbgp4SupportedCapabilityValue [ ] ={1,3,6,1,4,1,2076,41,14,2,1,5};
UINT4 Fsbgp4CapSupportedCapsRowStatus [ ] ={1,3,6,1,4,1,2076,41,14,2,1,6};
UINT4 Fsbgp4PeerRemIpAddr [ ] ={1,3,6,1,4,1,2076,41,14,3,1,1};
UINT4 Fsbgp4StrictCapabilityMatch [ ] ={1,3,6,1,4,1,2076,41,14,3,1,2};
UINT4 Fsbgp4PeerIpAddr [ ] ={1,3,6,1,4,1,2076,41,14,4,1,1};
UINT4 Fsbgp4PeerCapAnnouncedCode [ ] ={1,3,6,1,4,1,2076,41,14,4,1,2};
UINT4 Fsbgp4PeerCapAnnouncedInstance [ ] ={1,3,6,1,4,1,2076,41,14,4,1,3};
UINT4 Fsbgp4PeerCapAnnouncedLength [ ] ={1,3,6,1,4,1,2076,41,14,4,1,4};
UINT4 Fsbgp4PeerCapAnnouncedValue [ ] ={1,3,6,1,4,1,2076,41,14,4,1,5};
UINT4 Fsbgp4PeerRemoteAddress [ ] ={1,3,6,1,4,1,2076,41,14,5,1,1};
UINT4 Fsbgp4PeerCapReceivedCode [ ] ={1,3,6,1,4,1,2076,41,14,5,1,2};
UINT4 Fsbgp4PeerCapReceivedInstance [ ] ={1,3,6,1,4,1,2076,41,14,5,1,3};
UINT4 Fsbgp4PeerCapReceivedLength [ ] ={1,3,6,1,4,1,2076,41,14,5,1,4};
UINT4 Fsbgp4PeerCapReceivedValue [ ] ={1,3,6,1,4,1,2076,41,14,5,1,5};
UINT4 Fsbgp4CapAcceptedPeerRemAddr [ ] ={1,3,6,1,4,1,2076,41,14,6,1,1};
UINT4 Fsbgp4PeerCapAcceptedCode [ ] ={1,3,6,1,4,1,2076,41,14,6,1,2};
UINT4 Fsbgp4PeerCapAcceptedInstance [ ] ={1,3,6,1,4,1,2076,41,14,6,1,3};
UINT4 Fsbgp4PeerCapAcceptedLength [ ] ={1,3,6,1,4,1,2076,41,14,6,1,4};
UINT4 Fsbgp4PeerCapAcceptedValue [ ] ={1,3,6,1,4,1,2076,41,14,6,1,5};
UINT4 FsbgpAscConfedId [ ] ={1,3,6,1,4,1,2076,41,15,1,1};
UINT4 FsbgpAscConfedBestPathCompareMED [ ] ={1,3,6,1,4,1,2076,41,15,1,2};
UINT4 FsbgpAscConfedPeerASNo [ ] ={1,3,6,1,4,1,2076,41,15,2,1,1};
UINT4 FsbgpAscConfedPeerStatus [ ] ={1,3,6,1,4,1,2076,41,15,2,1,2};
UINT4 Fsbgp4RtRefreshAllPeerInboundRequest [ ] ={1,3,6,1,4,1,2076,41,16,1};
UINT4 Fsbgp4RtRefreshInboundPeerType [ ] ={1,3,6,1,4,1,2076,41,16,2,1,1};
UINT4 Fsbgp4RtRefreshInboundPeerAddr [ ] ={1,3,6,1,4,1,2076,41,16,2,1,2};
UINT4 Fsbgp4RtRefreshInboundRequest [ ] ={1,3,6,1,4,1,2076,41,16,2,1,3};
UINT4 Fsbgp4RtRefreshStatisticsPeerType [ ] ={1,3,6,1,4,1,2076,41,16,3,1,1};
UINT4 Fsbgp4RtRefreshStatisticsPeerAddr [ ] ={1,3,6,1,4,1,2076,41,16,3,1,2};
UINT4 Fsbgp4RtRefreshStatisticsRtRefMsgSentCntr [ ] ={1,3,6,1,4,1,2076,41,16,3,1,3};
UINT4 Fsbgp4RtRefreshStatisticsRtRefMsgTxErrCntr [ ] ={1,3,6,1,4,1,2076,41,16,3,1,4};
UINT4 Fsbgp4RtRefreshStatisticsRtRefMsgRcvdCntr [ ] ={1,3,6,1,4,1,2076,41,16,3,1,5};
UINT4 Fsbgp4RtRefreshStatisticsRtRefMsgInvalidCntr [ ] ={1,3,6,1,4,1,2076,41,16,3,1,6};
UINT4 Fsbgp4TCPMD5AuthPeerType [ ] ={1,3,6,1,4,1,2076,41,17,1,1,1};
UINT4 Fsbgp4TCPMD5AuthPeerAddr [ ] ={1,3,6,1,4,1,2076,41,17,1,1,2};
UINT4 Fsbgp4TCPMD5AuthPassword [ ] ={1,3,6,1,4,1,2076,41,17,1,1,3};
UINT4 Fsbgp4TCPMD5AuthPwdSet [ ] ={1,3,6,1,4,1,2076,41,17,1,1,4};
UINT4 Fsbgp4SoftReconfigAllPeerOutboundRequest [ ] ={1,3,6,1,4,1,2076,41,18,1};
UINT4 Fsbgp4SoftReconfigOutboundPeerType [ ] ={1,3,6,1,4,1,2076,41,18,2,1,1};
UINT4 Fsbgp4SoftReconfigOutboundPeerAddr [ ] ={1,3,6,1,4,1,2076,41,18,2,1,2};
UINT4 Fsbgp4SoftReconfigOutboundRequest [ ] ={1,3,6,1,4,1,2076,41,18,2,1,3};
UINT4 Fsbgp4mpebgpPeerRemoteAddrType [ ] ={1,3,6,1,4,1,2076,41,19,1,1};
UINT4 Fsbgp4mpebgpPeerIdentifier [ ] ={1,3,6,1,4,1,2076,41,19,1,2};
UINT4 Fsbgp4mpebgpPeerState [ ] ={1,3,6,1,4,1,2076,41,19,1,3};
UINT4 Fsbgp4mpebgpPeerAdminStatus [ ] ={1,3,6,1,4,1,2076,41,19,1,4};
UINT4 Fsbgp4mpebgpPeerNegotiatedVersion [ ] ={1,3,6,1,4,1,2076,41,19,1,5};
UINT4 Fsbgp4mpebgpPeerLocalAddr [ ] ={1,3,6,1,4,1,2076,41,19,1,6};
UINT4 Fsbgp4mpebgpPeerLocalPort [ ] ={1,3,6,1,4,1,2076,41,19,1,7};
UINT4 Fsbgp4mpebgpPeerRemoteAddr [ ] ={1,3,6,1,4,1,2076,41,19,1,8};
UINT4 Fsbgp4mpebgpPeerRemotePort [ ] ={1,3,6,1,4,1,2076,41,19,1,9};
UINT4 Fsbgp4mpebgpPeerRemoteAs [ ] ={1,3,6,1,4,1,2076,41,19,1,10};
UINT4 Fsbgp4mpebgpPeerInUpdates [ ] ={1,3,6,1,4,1,2076,41,19,1,11};
UINT4 Fsbgp4mpebgpPeerOutUpdates [ ] ={1,3,6,1,4,1,2076,41,19,1,12};
UINT4 Fsbgp4mpebgpPeerInTotalMessages [ ] ={1,3,6,1,4,1,2076,41,19,1,13};
UINT4 Fsbgp4mpebgpPeerOutTotalMessages [ ] ={1,3,6,1,4,1,2076,41,19,1,14};
UINT4 Fsbgp4mpebgpPeerLastError [ ] ={1,3,6,1,4,1,2076,41,19,1,15};
UINT4 Fsbgp4mpebgpPeerFsmEstablishedTransitions [ ] ={1,3,6,1,4,1,2076,41,19,1,16};
UINT4 Fsbgp4mpebgpPeerFsmEstablishedTime [ ] ={1,3,6,1,4,1,2076,41,19,1,17};
UINT4 Fsbgp4mpebgpPeerConnectRetryInterval [ ] ={1,3,6,1,4,1,2076,41,19,1,18};
UINT4 Fsbgp4mpebgpPeerHoldTime [ ] ={1,3,6,1,4,1,2076,41,19,1,19};
UINT4 Fsbgp4mpebgpPeerKeepAlive [ ] ={1,3,6,1,4,1,2076,41,19,1,20};
UINT4 Fsbgp4mpebgpPeerHoldTimeConfigured [ ] ={1,3,6,1,4,1,2076,41,19,1,21};
UINT4 Fsbgp4mpebgpPeerKeepAliveConfigured [ ] ={1,3,6,1,4,1,2076,41,19,1,22};
UINT4 Fsbgp4mpebgpPeerMinASOriginationInterval [ ] ={1,3,6,1,4,1,2076,41,19,1,23};
UINT4 Fsbgp4mpebgpPeerMinRouteAdvertisementInterval [ ] ={1,3,6,1,4,1,2076,41,19,1,24};
UINT4 Fsbgp4mpebgpPeerInUpdateElapsedTime [ ] ={1,3,6,1,4,1,2076,41,19,1,25};
UINT4 Fsbgp4mpebgpPeerEndOfRIBMarkerSentStatus [ ] ={1,3,6,1,4,1,2076,41,19,1,26};
UINT4 Fsbgp4mpebgpPeerEndOfRIBMarkerReceivedStatus [ ] ={1,3,6,1,4,1,2076,41,19,1,27};
UINT4 Fsbgp4mpebgpPeerRestartMode [ ] ={1,3,6,1,4,1,2076,41,19,1,28};
UINT4 Fsbgp4mpePeerRestartTimeInterval [ ] ={1,3,6,1,4,1,2076,41,19,1,29};
UINT4 Fsbgp4mpePeerAllowAutomaticStart [ ] ={1,3,6,1,4,1,2076,41,19,1,30};
UINT4 Fsbgp4mpePeerAllowAutomaticStop [ ] ={1,3,6,1,4,1,2076,41,19,1,31};
UINT4 Fsbgp4mpebgpPeerIdleHoldTimeConfigured [ ] ={1,3,6,1,4,1,2076,41,19,1,32};
UINT4 Fsbgp4mpeDampPeerOscillations [ ] ={1,3,6,1,4,1,2076,41,19,1,33};
UINT4 Fsbgp4mpePeerDelayOpen [ ] ={1,3,6,1,4,1,2076,41,19,1,34};
UINT4 Fsbgp4mpebgpPeerDelayOpenTimeConfigured [ ] ={1,3,6,1,4,1,2076,41,19,1,35};
UINT4 Fsbgp4mpePeerPrefixUpperLimit [ ] ={1,3,6,1,4,1,2076,41,19,1,36};
UINT4 Fsbgp4mpePeerTcpConnectRetryCnt [ ] ={1,3,6,1,4,1,2076,41,19,1,37};
UINT4 Fsbgp4mpePeerTcpCurrentConnectRetryCnt [ ] ={1,3,6,1,4,1,2076,41,19,1,38};
UINT4 Fsbgp4mpeIsPeerDamped [ ] ={1,3,6,1,4,1,2076,41,19,1,39};
UINT4 Fsbgp4mpePeerSessionAuthStatus [ ] ={1,3,6,1,4,1,2076,41,19,1,40};
UINT4 Fsbgp4mpePeerTCPAOKeyIdInUse [ ] ={1,3,6,1,4,1,2076,41,19,1,41};
UINT4 Fsbgp4mpePeerTCPAOAuthNoMKTDiscard [ ] ={1,3,6,1,4,1,2076,41,19,1,42};
UINT4 Fsbgp4mpePeerTCPAOAuthICMPAccept [ ] ={1,3,6,1,4,1,2076,41,19,1,43};
UINT4 Fsbgp4mpePeerIpPrefixNameIn [ ] ={1,3,6,1,4,1,2076,41,19,1,44};
UINT4 Fsbgp4mpePeerIpPrefixNameOut [ ] ={1,3,6,1,4,1,2076,41,19,1,45};
UINT4 Fsbgp4mpePeerBfdStatus [ ] ={1,3,6,1,4,1,2076,41,19,1,46};
UINT4 Fsbgp4mpebgpPeerHoldAdvtRoutes [ ] ={1,3,6,1,4,1,2076,41,19,1,47};
UINT4 Fsbgp4mpebgp4PathAttrRouteAfi [ ] ={1,3,6,1,4,1,2076,41,20,1,1};
UINT4 Fsbgp4mpebgp4PathAttrRouteSafi [ ] ={1,3,6,1,4,1,2076,41,20,1,2};
UINT4 Fsbgp4mpebgp4PathAttrPeerType [ ] ={1,3,6,1,4,1,2076,41,20,1,3};
UINT4 Fsbgp4mpebgp4PathAttrPeer [ ] ={1,3,6,1,4,1,2076,41,20,1,4};
UINT4 Fsbgp4mpebgp4PathAttrIpAddrPrefixLen [ ] ={1,3,6,1,4,1,2076,41,20,1,5};
UINT4 Fsbgp4mpebgp4PathAttrIpAddrPrefix [ ] ={1,3,6,1,4,1,2076,41,20,1,6};
UINT4 Fsbgp4mpebgp4PathAttrOrigin [ ] ={1,3,6,1,4,1,2076,41,20,1,7};
UINT4 Fsbgp4mpebgp4PathAttrASPathSegment [ ] ={1,3,6,1,4,1,2076,41,20,1,8};
UINT4 Fsbgp4mpebgp4PathAttrNextHop [ ] ={1,3,6,1,4,1,2076,41,20,1,9};
UINT4 Fsbgp4mpebgp4PathAttrMultiExitDisc [ ] ={1,3,6,1,4,1,2076,41,20,1,10};
UINT4 Fsbgp4mpebgp4PathAttrLocalPref [ ] ={1,3,6,1,4,1,2076,41,20,1,11};
UINT4 Fsbgp4mpebgp4PathAttrAtomicAggregate [ ] ={1,3,6,1,4,1,2076,41,20,1,12};
UINT4 Fsbgp4mpebgp4PathAttrAggregatorAS [ ] ={1,3,6,1,4,1,2076,41,20,1,13};
UINT4 Fsbgp4mpebgp4PathAttrAggregatorAddr [ ] ={1,3,6,1,4,1,2076,41,20,1,14};
UINT4 Fsbgp4mpebgp4PathAttrCalcLocalPref [ ] ={1,3,6,1,4,1,2076,41,20,1,15};
UINT4 Fsbgp4mpebgp4PathAttrBest [ ] ={1,3,6,1,4,1,2076,41,20,1,16};
UINT4 Fsbgp4mpebgp4PathAttrCommunity [ ] ={1,3,6,1,4,1,2076,41,20,1,17};
UINT4 Fsbgp4mpebgp4PathAttrOriginatorId [ ] ={1,3,6,1,4,1,2076,41,20,1,18};
UINT4 Fsbgp4mpebgp4PathAttrClusterList [ ] ={1,3,6,1,4,1,2076,41,20,1,19};
UINT4 Fsbgp4mpebgp4PathAttrExtCommunity [ ] ={1,3,6,1,4,1,2076,41,20,1,20};
UINT4 Fsbgp4mpebgp4PathAttrUnknown [ ] ={1,3,6,1,4,1,2076,41,20,1,21};
UINT4 Fsbgp4mpebgp4PathAttrLabel [ ] ={1,3,6,1,4,1,2076,41,20,1,22};
UINT4 Fsbgp4mpebgp4PathAttrAS4PathSegment [ ] ={1,3,6,1,4,1,2076,41,20,1,23};
UINT4 Fsbgp4mpebgp4PathAttrAggregatorAS4 [ ] ={1,3,6,1,4,1,2076,41,20,1,24};
UINT4 Fsbgp4mpePeerExtPeerType [ ] ={1,3,6,1,4,1,2076,41,21,1,1};
UINT4 Fsbgp4mpePeerExtPeerRemoteAddr [ ] ={1,3,6,1,4,1,2076,41,21,1,2};
UINT4 Fsbgp4mpePeerExtConfigurePeer [ ] ={1,3,6,1,4,1,2076,41,21,1,3};
UINT4 Fsbgp4mpePeerExtPeerRemoteAs [ ] ={1,3,6,1,4,1,2076,41,21,1,4};
UINT4 Fsbgp4mpePeerExtEBGPMultiHop [ ] ={1,3,6,1,4,1,2076,41,21,1,5};
UINT4 Fsbgp4mpePeerExtEBGPHopLimit [ ] ={1,3,6,1,4,1,2076,41,21,1,6};
UINT4 Fsbgp4mpePeerExtNextHopSelf [ ] ={1,3,6,1,4,1,2076,41,21,1,7};
UINT4 Fsbgp4mpePeerExtRflClient [ ] ={1,3,6,1,4,1,2076,41,21,1,8};
UINT4 Fsbgp4mpePeerExtTcpSendBufSize [ ] ={1,3,6,1,4,1,2076,41,21,1,9};
UINT4 Fsbgp4mpePeerExtTcpRcvBufSize [ ] ={1,3,6,1,4,1,2076,41,21,1,10};
UINT4 Fsbgp4mpePeerExtLclAddress [ ] ={1,3,6,1,4,1,2076,41,21,1,11};
UINT4 Fsbgp4mpePeerExtNetworkAddress [ ] ={1,3,6,1,4,1,2076,41,21,1,12};
UINT4 Fsbgp4mpePeerExtGateway [ ] ={1,3,6,1,4,1,2076,41,21,1,13};
UINT4 Fsbgp4mpePeerExtCommSendStatus [ ] ={1,3,6,1,4,1,2076,41,21,1,14};
UINT4 Fsbgp4mpePeerExtECommSendStatus [ ] ={1,3,6,1,4,1,2076,41,21,1,15};
UINT4 Fsbgp4mpePeerExtPassive [ ] ={1,3,6,1,4,1,2076,41,21,1,16};
UINT4 Fsbgp4mpePeerExtDefaultOriginate [ ] ={1,3,6,1,4,1,2076,41,21,1,17};
UINT4 Fsbgp4mpePeerExtActivateMPCapability [ ] ={1,3,6,1,4,1,2076,41,21,1,18};
UINT4 Fsbgp4mpePeerExtDeactivateMPCapability [ ] ={1,3,6,1,4,1,2076,41,21,1,19};
UINT4 Fsbgp4mpePeerExtMplsVpnVrfAssociated [ ] ={1,3,6,1,4,1,2076,41,21,1,20};
UINT4 Fsbgp4mpePeerExtMplsVpnCERouteTargetAdvt [ ] ={1,3,6,1,4,1,2076,41,21,1,21};
UINT4 Fsbgp4mpePeerExtMplsVpnCESiteOfOrigin [ ] ={1,3,6,1,4,1,2076,41,21,1,22};
/* For test object 23 oid is allocated */

UINT4 Fsbgp4mpePeerExtOverrideCapability [ ] ={1,3,6,1,4,1,2076,41,21,1,24};
UINT4 Fsbgp4mpeMEDIndex [ ] ={1,3,6,1,4,1,2076,41,22,1,1};
UINT4 Fsbgp4mpeMEDAdminStatus [ ] ={1,3,6,1,4,1,2076,41,22,1,2};
UINT4 Fsbgp4mpeMEDRemoteAS [ ] ={1,3,6,1,4,1,2076,41,22,1,3};
UINT4 Fsbgp4mpeMEDIPAddrAfi [ ] ={1,3,6,1,4,1,2076,41,22,1,4};
UINT4 Fsbgp4mpeMEDIPAddrSafi [ ] ={1,3,6,1,4,1,2076,41,22,1,5};
UINT4 Fsbgp4mpeMEDIPAddrPrefix [ ] ={1,3,6,1,4,1,2076,41,22,1,6};
UINT4 Fsbgp4mpeMEDIPAddrPrefixLen [ ] ={1,3,6,1,4,1,2076,41,22,1,7};
UINT4 Fsbgp4mpeMEDIntermediateAS [ ] ={1,3,6,1,4,1,2076,41,22,1,8};
UINT4 Fsbgp4mpeMEDDirection [ ] ={1,3,6,1,4,1,2076,41,22,1,9};
UINT4 Fsbgp4mpeMEDValue [ ] ={1,3,6,1,4,1,2076,41,22,1,10};
UINT4 Fsbgp4mpeMEDPreference [ ] ={1,3,6,1,4,1,2076,41,22,1,11};
UINT4 Fsbgp4mpeMEDVrfName [ ] ={1,3,6,1,4,1,2076,41,22,1,12};
UINT4 Fsbgp4mpeLocalPrefIndex [ ] ={1,3,6,1,4,1,2076,41,23,1,1};
UINT4 Fsbgp4mpeLocalPrefAdminStatus [ ] ={1,3,6,1,4,1,2076,41,23,1,2};
UINT4 Fsbgp4mpeLocalPrefRemoteAS [ ] ={1,3,6,1,4,1,2076,41,23,1,3};
UINT4 Fsbgp4mpeLocalPrefIPAddrAfi [ ] ={1,3,6,1,4,1,2076,41,23,1,4};
UINT4 Fsbgp4mpeLocalPrefIPAddrSafi [ ] ={1,3,6,1,4,1,2076,41,23,1,5};
UINT4 Fsbgp4mpeLocalPrefIPAddrPrefix [ ] ={1,3,6,1,4,1,2076,41,23,1,6};
UINT4 Fsbgp4mpeLocalPrefIPAddrPrefixLen [ ] ={1,3,6,1,4,1,2076,41,23,1,7};
UINT4 Fsbgp4mpeLocalPrefIntermediateAS [ ] ={1,3,6,1,4,1,2076,41,23,1,8};
UINT4 Fsbgp4mpeLocalPrefDirection [ ] ={1,3,6,1,4,1,2076,41,23,1,9};
UINT4 Fsbgp4mpeLocalPrefValue [ ] ={1,3,6,1,4,1,2076,41,23,1,10};
UINT4 Fsbgp4mpeLocalPrefPreference [ ] ={1,3,6,1,4,1,2076,41,23,1,11};
UINT4 Fsbgp4mpeLocalPrefVrfName [ ] ={1,3,6,1,4,1,2076,41,23,1,12};
UINT4 Fsbgp4mpeUpdateFilterIndex [ ] ={1,3,6,1,4,1,2076,41,24,1,1};
UINT4 Fsbgp4mpeUpdateFilterAdminStatus [ ] ={1,3,6,1,4,1,2076,41,24,1,2};
UINT4 Fsbgp4mpeUpdateFilterRemoteAS [ ] ={1,3,6,1,4,1,2076,41,24,1,3};
UINT4 Fsbgp4mpeUpdateFilterIPAddrAfi [ ] ={1,3,6,1,4,1,2076,41,24,1,4};
UINT4 Fsbgp4mpeUpdateFilterIPAddrSafi [ ] ={1,3,6,1,4,1,2076,41,24,1,5};
UINT4 Fsbgp4mpeUpdateFilterIPAddrPrefix [ ] ={1,3,6,1,4,1,2076,41,24,1,6};
UINT4 Fsbgp4mpeUpdateFilterIPAddrPrefixLen [ ] ={1,3,6,1,4,1,2076,41,24,1,7};
UINT4 Fsbgp4mpeUpdateFilterIntermediateAS [ ] ={1,3,6,1,4,1,2076,41,24,1,8};
UINT4 Fsbgp4mpeUpdateFilterDirection [ ] ={1,3,6,1,4,1,2076,41,24,1,9};
UINT4 Fsbgp4mpeUpdateFilterAction [ ] ={1,3,6,1,4,1,2076,41,24,1,10};
UINT4 Fsbgp4mpeUpdateFilterVrfName [ ] ={1,3,6,1,4,1,2076,41,24,1,11};
UINT4 Fsbgp4mpeAggregateIndex [ ] ={1,3,6,1,4,1,2076,41,25,1,1};
UINT4 Fsbgp4mpeAggregateAdminStatus [ ] ={1,3,6,1,4,1,2076,41,25,1,2};
UINT4 Fsbgp4mpeAggregateIPAddrAfi [ ] ={1,3,6,1,4,1,2076,41,25,1,3};
UINT4 Fsbgp4mpeAggregateIPAddrSafi [ ] ={1,3,6,1,4,1,2076,41,25,1,4};
UINT4 Fsbgp4mpeAggregateIPAddrPrefix [ ] ={1,3,6,1,4,1,2076,41,25,1,5};
UINT4 Fsbgp4mpeAggregateIPAddrPrefixLen [ ] ={1,3,6,1,4,1,2076,41,25,1,6};
UINT4 Fsbgp4mpeAggregateAdvertise [ ] ={1,3,6,1,4,1,2076,41,25,1,7};
UINT4 Fsbgp4mpeAggregateVrfName [ ] ={1,3,6,1,4,1,2076,41,25,1,8};
UINT4 Fsbgp4mpeAggregateAsSet [ ] ={1,3,6,1,4,1,2076,41,25,1,9};
UINT4 Fsbgp4mpeAggregateAdvertiseRouteMapName [ ] ={1,3,6,1,4,1,2076,41,25,1,10};
UINT4 Fsbgp4mpeAggregateSuppressRouteMapName [ ] ={1,3,6,1,4,1,2076,41,25,1,11};
UINT4 Fsbgp4mpeAggregateAttributeRouteMapName [ ] ={1,3,6,1,4,1,2076,41,25,1,12};
UINT4 Fsbgp4mpeImportRoutePrefixAfi [ ] ={1,3,6,1,4,1,2076,41,26,1,1};
UINT4 Fsbgp4mpeImportRoutePrefixSafi [ ] ={1,3,6,1,4,1,2076,41,26,1,2};
UINT4 Fsbgp4mpeImportRoutePrefix [ ] ={1,3,6,1,4,1,2076,41,26,1,3};
UINT4 Fsbgp4mpeImportRoutePrefixLen [ ] ={1,3,6,1,4,1,2076,41,26,1,4};
UINT4 Fsbgp4mpeImportRouteProtocol [ ] ={1,3,6,1,4,1,2076,41,26,1,5};
UINT4 Fsbgp4mpeImportRouteNextHop [ ] ={1,3,6,1,4,1,2076,41,26,1,6};
UINT4 Fsbgp4mpeImportRouteIfIndex [ ] ={1,3,6,1,4,1,2076,41,26,1,7};
UINT4 Fsbgp4mpeImportRouteMetric [ ] ={1,3,6,1,4,1,2076,41,26,1,8};
UINT4 Fsbgp4mpeImportRouteVrf [ ] ={1,3,6,1,4,1,2076,41,26,1,9};
UINT4 Fsbgp4mpeImportRouteAction [ ] ={1,3,6,1,4,1,2076,41,26,1,10};
UINT4 Fsbgp4mpePeerType [ ] ={1,3,6,1,4,1,2076,41,27,1,1};
UINT4 Fsbgp4mpePeer [ ] ={1,3,6,1,4,1,2076,41,27,1,2};
UINT4 Fsbgp4mpeFsmTransitionHist [ ] ={1,3,6,1,4,1,2076,41,27,1,3};
UINT4 Fsbgp4mpePathAttrAddrPrefixAfi [ ] ={1,3,6,1,4,1,2076,41,28,1,1,1};
UINT4 Fsbgp4mpePathAttrAddrPrefixSafi [ ] ={1,3,6,1,4,1,2076,41,28,1,1,2};
UINT4 Fsbgp4mpePathAttrAddrPrefix [ ] ={1,3,6,1,4,1,2076,41,28,1,1,3};
UINT4 Fsbgp4mpePathAttrAddrPrefixLen [ ] ={1,3,6,1,4,1,2076,41,28,1,1,4};
UINT4 Fsbgp4mpePathAttrPeerType [ ] ={1,3,6,1,4,1,2076,41,28,1,1,5};
UINT4 Fsbgp4mpePathAttrPeer [ ] ={1,3,6,1,4,1,2076,41,28,1,1,6};
UINT4 Fsbgp4mpeRfdRtFom [ ] ={1,3,6,1,4,1,2076,41,28,1,1,7};
UINT4 Fsbgp4mpeRfdRtLastUpdtTime [ ] ={1,3,6,1,4,1,2076,41,28,1,1,8};
UINT4 Fsbgp4mpeRfdRtState [ ] ={1,3,6,1,4,1,2076,41,28,1,1,9};
UINT4 Fsbgp4mpeRfdRtStatus [ ] ={1,3,6,1,4,1,2076,41,28,1,1,10};
UINT4 Fsbgp4mpeRfdRtFlapCount [ ] ={1,3,6,1,4,1,2076,41,28,1,1,11};
UINT4 Fsbgp4mpeRfdRtFlapTime [ ] ={1,3,6,1,4,1,2076,41,28,1,1,12};
UINT4 Fsbgp4mpeRfdRtReuseTime [ ] ={1,3,6,1,4,1,2076,41,28,1,1,13};
UINT4 Fsbgp4mpePeerRemoteIpAddrType [ ] ={1,3,6,1,4,1,2076,41,28,2,1,1};
UINT4 Fsbgp4mpePeerRemoteIpAddr [ ] ={1,3,6,1,4,1,2076,41,28,2,1,2};
UINT4 Fsbgp4mpeRfdPeerFom [ ] ={1,3,6,1,4,1,2076,41,28,2,1,3};
UINT4 Fsbgp4mpeRfdPeerLastUpdtTime [ ] ={1,3,6,1,4,1,2076,41,28,2,1,4};
UINT4 Fsbgp4mpeRfdPeerState [ ] ={1,3,6,1,4,1,2076,41,28,2,1,5};
UINT4 Fsbgp4mpeRfdPeerStatus [ ] ={1,3,6,1,4,1,2076,41,28,2,1,6};
UINT4 Fsbgp4mpeRtAfi [ ] ={1,3,6,1,4,1,2076,41,28,3,1,1};
UINT4 Fsbgp4mpeRtSafi [ ] ={1,3,6,1,4,1,2076,41,28,3,1,2};
UINT4 Fsbgp4mpeRtIPPrefix [ ] ={1,3,6,1,4,1,2076,41,28,3,1,3};
UINT4 Fsbgp4mpeRtIPPrefixLen [ ] ={1,3,6,1,4,1,2076,41,28,3,1,4};
UINT4 Fsbgp4mpeRfdRtsReusePeerType [ ] ={1,3,6,1,4,1,2076,41,28,3,1,5};
UINT4 Fsbgp4mpePeerRemAddress [ ] ={1,3,6,1,4,1,2076,41,28,3,1,6};
UINT4 Fsbgp4mpeRfdRtReuseListRtFom [ ] ={1,3,6,1,4,1,2076,41,28,3,1,7};
UINT4 Fsbgp4mpeRfdRtReuseListRtLastUpdtTime [ ] ={1,3,6,1,4,1,2076,41,28,3,1,8};
UINT4 Fsbgp4mpeRfdRtReuseListRtState [ ] ={1,3,6,1,4,1,2076,41,28,3,1,9};
UINT4 Fsbgp4mpeRfdRtReuseListRtStatus [ ] ={1,3,6,1,4,1,2076,41,28,3,1,10};
UINT4 Fsbgp4mpeRfdPeerRemIpAddrType [ ] ={1,3,6,1,4,1,2076,41,28,4,1,1};
UINT4 Fsbgp4mpeRfdPeerRemIpAddr [ ] ={1,3,6,1,4,1,2076,41,28,4,1,2};
UINT4 Fsbgp4mpeRfdPeerReuseListPeerFom [ ] ={1,3,6,1,4,1,2076,41,28,4,1,3};
UINT4 Fsbgp4mpeRfdPeerReuseListLastUpdtTime [ ] ={1,3,6,1,4,1,2076,41,28,4,1,4};
UINT4 Fsbgp4mpeRfdPeerReuseListPeerState [ ] ={1,3,6,1,4,1,2076,41,28,4,1,5};
UINT4 Fsbgp4mpeRfdPeerReuseListPeerStatus [ ] ={1,3,6,1,4,1,2076,41,28,4,1,6};
UINT4 Fsbgp4mpeAddCommRtAfi [ ] ={1,3,6,1,4,1,2076,41,29,1,1,1};
UINT4 Fsbgp4mpeAddCommRtSafi [ ] ={1,3,6,1,4,1,2076,41,29,1,1,2};
UINT4 Fsbgp4mpeAddCommIpNetwork [ ] ={1,3,6,1,4,1,2076,41,29,1,1,3};
UINT4 Fsbgp4mpeAddCommIpPrefixLen [ ] ={1,3,6,1,4,1,2076,41,29,1,1,4};
UINT4 Fsbgp4mpeAddCommVal [ ] ={1,3,6,1,4,1,2076,41,29,1,1,5};
UINT4 Fsbgp4mpeAddCommRowStatus [ ] ={1,3,6,1,4,1,2076,41,29,1,1,6};
UINT4 Fsbgp4mpeDeleteCommRtAfi [ ] ={1,3,6,1,4,1,2076,41,29,2,1,1};
UINT4 Fsbgp4mpeDeleteCommRtSafi [ ] ={1,3,6,1,4,1,2076,41,29,2,1,2};
UINT4 Fsbgp4mpeDeleteCommIpNetwork [ ] ={1,3,6,1,4,1,2076,41,29,2,1,3};
UINT4 Fsbgp4mpeDeleteCommIpPrefixLen [ ] ={1,3,6,1,4,1,2076,41,29,2,1,4};
UINT4 Fsbgp4mpeDeleteCommVal [ ] ={1,3,6,1,4,1,2076,41,29,2,1,5};
UINT4 Fsbgp4mpeDeleteCommRowStatus [ ] ={1,3,6,1,4,1,2076,41,29,2,1,6};
UINT4 Fsbgp4mpeCommSetStatusAfi [ ] ={1,3,6,1,4,1,2076,41,29,3,1,1};
UINT4 Fsbgp4mpeCommSetStatusSafi [ ] ={1,3,6,1,4,1,2076,41,29,3,1,2};
UINT4 Fsbgp4mpeCommSetStatusIpNetwork [ ] ={1,3,6,1,4,1,2076,41,29,3,1,3};
UINT4 Fsbgp4mpeCommSetStatusIpPrefixLen [ ] ={1,3,6,1,4,1,2076,41,29,3,1,4};
UINT4 Fsbgp4mpeCommSetStatus [ ] ={1,3,6,1,4,1,2076,41,29,3,1,5};
UINT4 Fsbgp4mpeCommSetStatusRowStatus [ ] ={1,3,6,1,4,1,2076,41,29,3,1,6};
UINT4 Fsbgp4mpeAddExtCommRtAfi [ ] ={1,3,6,1,4,1,2076,41,30,1,1,1};
UINT4 Fsbgp4mpeAddExtCommRtSafi [ ] ={1,3,6,1,4,1,2076,41,30,1,1,2};
UINT4 Fsbgp4mpeAddExtCommIpNetwork [ ] ={1,3,6,1,4,1,2076,41,30,1,1,3};
UINT4 Fsbgp4mpeAddExtCommIpPrefixLen [ ] ={1,3,6,1,4,1,2076,41,30,1,1,4};
UINT4 Fsbgp4mpeAddExtCommVal [ ] ={1,3,6,1,4,1,2076,41,30,1,1,5};
UINT4 Fsbgp4mpeAddExtCommRowStatus [ ] ={1,3,6,1,4,1,2076,41,30,1,1,6};
UINT4 Fsbgp4mpeDeleteExtCommRtAfi [ ] ={1,3,6,1,4,1,2076,41,30,2,1,1};
UINT4 Fsbgp4mpeDeleteExtCommRtSafi [ ] ={1,3,6,1,4,1,2076,41,30,2,1,2};
UINT4 Fsbgp4mpeDeleteExtCommIpNetwork [ ] ={1,3,6,1,4,1,2076,41,30,2,1,3};
UINT4 Fsbgp4mpeDeleteExtCommIpPrefixLen [ ] ={1,3,6,1,4,1,2076,41,30,2,1,4};
UINT4 Fsbgp4mpeDeleteExtCommVal [ ] ={1,3,6,1,4,1,2076,41,30,2,1,5};
UINT4 Fsbgp4mpeDeleteExtCommRowStatus [ ] ={1,3,6,1,4,1,2076,41,30,2,1,6};
UINT4 Fsbgp4mpeExtCommSetStatusRtAfi [ ] ={1,3,6,1,4,1,2076,41,30,3,1,1};
UINT4 Fsbgp4mpeExtCommSetStatusRtSafi [ ] ={1,3,6,1,4,1,2076,41,30,3,1,2};
UINT4 Fsbgp4mpeExtCommSetStatusIpNetwork [ ] ={1,3,6,1,4,1,2076,41,30,3,1,3};
UINT4 Fsbgp4mpeExtCommSetStatusIpPrefixLen [ ] ={1,3,6,1,4,1,2076,41,30,3,1,4};
UINT4 Fsbgp4mpeExtCommSetStatus [ ] ={1,3,6,1,4,1,2076,41,30,3,1,5};
UINT4 Fsbgp4mpeExtCommSetStatusRowStatus [ ] ={1,3,6,1,4,1,2076,41,30,3,1,6};
UINT4 Fsbgp4mpePeerLinkType [ ] ={1,3,6,1,4,1,2076,41,30,4,1,1};
UINT4 Fsbgp4mpePeerLinkRemAddr [ ] ={1,3,6,1,4,1,2076,41,30,4,1,2};
UINT4 Fsbgp4mpeLinkBandWidth [ ] ={1,3,6,1,4,1,2076,41,30,4,1,3};
UINT4 Fsbgp4mpePeerLinkBwRowStatus [ ] ={1,3,6,1,4,1,2076,41,30,4,1,4};
UINT4 Fsbgp4mpeCapPeerType [ ] ={1,3,6,1,4,1,2076,41,31,1,1,1};
UINT4 Fsbgp4mpeCapPeerRemoteIpAddr [ ] ={1,3,6,1,4,1,2076,41,31,1,1,2};
UINT4 Fsbgp4mpeSupportedCapabilityCode [ ] ={1,3,6,1,4,1,2076,41,31,1,1,3};
UINT4 Fsbgp4mpeSupportedCapabilityLength [ ] ={1,3,6,1,4,1,2076,41,31,1,1,5};
UINT4 Fsbgp4mpeSupportedCapabilityValue [ ] ={1,3,6,1,4,1,2076,41,31,1,1,6};
UINT4 Fsbgp4mpeCapSupportedCapsRowStatus [ ] ={1,3,6,1,4,1,2076,41,31,1,1,7};
UINT4 Fsbgp4mpeCapAnnouncedStatus [ ] ={1,3,6,1,4,1,2076,41,31,1,1,8};
UINT4 Fsbgp4mpeCapReceivedStatus [ ] ={1,3,6,1,4,1,2076,41,31,1,1,9};
UINT4 Fsbgp4mpeCapNegotiatedStatus [ ] ={1,3,6,1,4,1,2076,41,31,1,1,10};
UINT4 Fsbgp4mpeCapConfiguredStatus [ ] ={1,3,6,1,4,1,2076,41,31,1,1,11};
UINT4 Fsbgp4mpeRtRefreshInboundPeerType [ ] ={1,3,6,1,4,1,2076,41,32,1,1,1};
UINT4 Fsbgp4mpeRtRefreshInboundPeerAddr [ ] ={1,3,6,1,4,1,2076,41,32,1,1,2};
UINT4 Fsbgp4mpeRtRefreshInboundAfi [ ] ={1,3,6,1,4,1,2076,41,32,1,1,3};
UINT4 Fsbgp4mpeRtRefreshInboundSafi [ ] ={1,3,6,1,4,1,2076,41,32,1,1,4};
UINT4 Fsbgp4mpeRtRefreshInboundRequest [ ] ={1,3,6,1,4,1,2076,41,32,1,1,5};
UINT4 Fsbgp4mpeRtRefreshInboundPrefixFilter [ ] ={1,3,6,1,4,1,2076,41,32,1,1,6};
UINT4 Fsbgp4mpeRtRefreshStatisticsPeerType [ ] ={1,3,6,1,4,1,2076,41,32,2,1,1};
UINT4 Fsbgp4mpeRtRefreshStatisticsPeerAddr [ ] ={1,3,6,1,4,1,2076,41,32,2,1,2};
UINT4 Fsbgp4mpeRtRefreshStatisticsAfi [ ] ={1,3,6,1,4,1,2076,41,32,2,1,3};
UINT4 Fsbgp4mpeRtRefreshStatisticsSafi [ ] ={1,3,6,1,4,1,2076,41,32,2,1,4};
UINT4 Fsbgp4mpeRtRefreshStatisticsRtRefMsgSentCntr [ ] ={1,3,6,1,4,1,2076,41,32,2,1,5};
UINT4 Fsbgp4mpeRtRefreshStatisticsRtRefMsgTxErrCntr [ ] ={1,3,6,1,4,1,2076,41,32,2,1,6};
UINT4 Fsbgp4mpeRtRefreshStatisticsRtRefMsgRcvdCntr [ ] ={1,3,6,1,4,1,2076,41,32,2,1,7};
UINT4 Fsbgp4mpeRtRefreshStatisticsRtRefMsgInvalidCntr [ ] ={1,3,6,1,4,1,2076,41,32,2,1,8};
UINT4 Fsbgp4mpeSoftReconfigOutboundPeerType [ ] ={1,3,6,1,4,1,2076,41,33,1,1,1};
UINT4 Fsbgp4mpeSoftReconfigOutboundPeerAddr [ ] ={1,3,6,1,4,1,2076,41,33,1,1,2};
UINT4 Fsbgp4mpeSoftReconfigOutboundAfi [ ] ={1,3,6,1,4,1,2076,41,33,1,1,3};
UINT4 Fsbgp4mpeSoftReconfigOutboundSafi [ ] ={1,3,6,1,4,1,2076,41,33,1,1,4};
UINT4 Fsbgp4mpeSoftReconfigOutboundRequest [ ] ={1,3,6,1,4,1,2076,41,33,1,1,5};
UINT4 Fsbgp4MpePeerRemoteAddrType [ ] ={1,3,6,1,4,1,2076,41,34,1,1};
UINT4 Fsbgp4MpePeerRemoteAddr [ ] ={1,3,6,1,4,1,2076,41,34,1,2};
UINT4 Fsbgp4MpePrefixCountersAfi [ ] ={1,3,6,1,4,1,2076,41,34,1,3};
UINT4 Fsbgp4MpePrefixCountersSafi [ ] ={1,3,6,1,4,1,2076,41,34,1,4};
UINT4 Fsbgp4MpePrefixCountersPrefixesReceived [ ] ={1,3,6,1,4,1,2076,41,34,1,5};
UINT4 Fsbgp4MpePrefixCountersPrefixesSent [ ] ={1,3,6,1,4,1,2076,41,34,1,6};
UINT4 Fsbgp4MpePrefixCountersWithdrawsReceived [ ] ={1,3,6,1,4,1,2076,41,34,1,7};
UINT4 Fsbgp4MpePrefixCountersWithdrawsSent [ ] ={1,3,6,1,4,1,2076,41,34,1,8};
UINT4 Fsbgp4MpePrefixCountersInPrefixes [ ] ={1,3,6,1,4,1,2076,41,34,1,9};
UINT4 Fsbgp4MpePrefixCountersInPrefixesAccepted [ ] ={1,3,6,1,4,1,2076,41,34,1,10};
UINT4 Fsbgp4MpePrefixCountersInPrefixesRejected [ ] ={1,3,6,1,4,1,2076,41,34,1,11};
UINT4 Fsbgp4MpePrefixCountersOutPrefixes [ ] ={1,3,6,1,4,1,2076,41,34,1,12};
UINT4 Fsbgp4MplsVpnVrfName [ ] ={1,3,6,1,4,1,2076,41,35,1,1,1};
UINT4 Fsbgp4MplsVpnVrfRouteTargetType [ ] ={1,3,6,1,4,1,2076,41,35,1,1,2};
UINT4 Fsbgp4MplsVpnVrfRouteTarget [ ] ={1,3,6,1,4,1,2076,41,35,1,1,3};
UINT4 Fsbgp4MplsVpnVrfRouteTargetRowStatus [ ] ={1,3,6,1,4,1,2076,41,35,1,1,4};
UINT4 Fsbgp4MplsVpnVrfRedisAfi [ ] ={1,3,6,1,4,1,2076,41,35,2,1,1};
UINT4 Fsbgp4MplsVpnVrfRedisSafi [ ] ={1,3,6,1,4,1,2076,41,35,2,1,2};
UINT4 Fsbgp4MplsVpnVrfRedisProtoMask [ ] ={1,3,6,1,4,1,2076,41,35,2,1,3};
UINT4 Fsbgp4MplsVpnRRRouteTarget [ ] ={1,3,6,1,4,1,2076,41,35,3,1,1};
UINT4 Fsbgp4MplsVpnRRRouteTargetRtCnt [ ] ={1,3,6,1,4,1,2076,41,35,3,1,2};
UINT4 Fsbgp4MplsVpnRRRouteTargetTimeStamp [ ] ={1,3,6,1,4,1,2076,41,35,3,1,3};
UINT4 FsBgp4DistInOutRouteMapName [ ] ={1,3,6,1,4,1,2076,41,36,1,1,1};
UINT4 FsBgp4DistInOutRouteMapType [ ] ={1,3,6,1,4,1,2076,41,36,1,1,2};
UINT4 FsBgp4DistInOutRouteMapValue [ ] ={1,3,6,1,4,1,2076,41,36,1,1,3};
UINT4 FsBgp4DistInOutRouteMapRowStatus [ ] ={1,3,6,1,4,1,2076,41,36,1,1,4};
UINT4 FsBgp4PreferenceValue [ ] ={1,3,6,1,4,1,2076,41,37,1};
UINT4 FsBgp4NeighborRouteMapPeerAddrType [ ] ={1,3,6,1,4,1,2076,41,39,1,1,1};
UINT4 FsBgp4NeighborRouteMapPeer [ ] ={1,3,6,1,4,1,2076,41,39,1,1,2};
UINT4 FsBgp4NeighborRouteMapDirection [ ] ={1,3,6,1,4,1,2076,41,39,1,1,3};
UINT4 FsBgp4NeighborRouteMapName [ ] ={1,3,6,1,4,1,2076,41,39,1,1,4};
UINT4 FsBgp4NeighborRouteMapRowStatus [ ] ={1,3,6,1,4,1,2076,41,39,1,1,5};
UINT4 FsBgp4PeerGroupName [ ] ={1,3,6,1,4,1,2076,41,40,1,1};
UINT4 FsBgp4PeerGroupAddrType [ ] ={1,3,6,1,4,1,2076,41,40,1,2};
UINT4 FsBgp4PeerGroupRemoteAs [ ] ={1,3,6,1,4,1,2076,41,40,1,3};
UINT4 FsBgp4PeerGroupHoldTimeConfigured [ ] ={1,3,6,1,4,1,2076,41,40,1,4};
UINT4 FsBgp4PeerGroupKeepAliveConfigured [ ] ={1,3,6,1,4,1,2076,41,40,1,5};
UINT4 FsBgp4PeerGroupConnectRetryInterval [ ] ={1,3,6,1,4,1,2076,41,40,1,6};
UINT4 FsBgp4PeerGroupMinASOriginInterval [ ] ={1,3,6,1,4,1,2076,41,40,1,7};
UINT4 FsBgp4PeerGroupMinRouteAdvInterval [ ] ={1,3,6,1,4,1,2076,41,40,1,8};
UINT4 FsBgp4PeerGroupAllowAutomaticStart [ ] ={1,3,6,1,4,1,2076,41,40,1,9};
UINT4 FsBgp4PeerGroupAllowAutomaticStop [ ] ={1,3,6,1,4,1,2076,41,40,1,10};
UINT4 FsBgp4PeerGroupIdleHoldTimeConfigured [ ] ={1,3,6,1,4,1,2076,41,40,1,11};
UINT4 FsBgp4PeerGroupDampPeerOscillations [ ] ={1,3,6,1,4,1,2076,41,40,1,12};
UINT4 FsBgp4PeerGroupDelayOpen [ ] ={1,3,6,1,4,1,2076,41,40,1,13};
UINT4 FsBgp4PeerGroupDelayOpenTimeConfigured [ ] ={1,3,6,1,4,1,2076,41,40,1,14};
UINT4 FsBgp4PeerGroupPrefixUpperLimit [ ] ={1,3,6,1,4,1,2076,41,40,1,15};
UINT4 FsBgp4PeerGroupTcpConnectRetryCnt [ ] ={1,3,6,1,4,1,2076,41,40,1,16};
UINT4 FsBgp4PeerGroupEBGPMultiHop [ ] ={1,3,6,1,4,1,2076,41,40,1,17};
UINT4 FsBgp4PeerGroupEBGPHopLimit [ ] ={1,3,6,1,4,1,2076,41,40,1,18};
UINT4 FsBgp4PeerGroupNextHopSelf [ ] ={1,3,6,1,4,1,2076,41,40,1,19};
UINT4 FsBgp4PeerGroupRflClient [ ] ={1,3,6,1,4,1,2076,41,40,1,20};
UINT4 FsBgp4PeerGroupTcpSendBufSize [ ] ={1,3,6,1,4,1,2076,41,40,1,21};
UINT4 FsBgp4PeerGroupTcpRcvBufSize [ ] ={1,3,6,1,4,1,2076,41,40,1,22};
UINT4 FsBgp4PeerGroupCommSendStatus [ ] ={1,3,6,1,4,1,2076,41,40,1,23};
UINT4 FsBgp4PeerGroupECommSendStatus [ ] ={1,3,6,1,4,1,2076,41,40,1,24};
UINT4 FsBgp4PeerGroupPassive [ ] ={1,3,6,1,4,1,2076,41,40,1,25};
UINT4 FsBgp4PeerGroupDefaultOriginate [ ] ={1,3,6,1,4,1,2076,41,40,1,26};
UINT4 FsBgp4PeerGroupActivateMPCapability [ ] ={1,3,6,1,4,1,2076,41,40,1,27};
UINT4 FsBgp4PeerGroupDeactivateMPCapability [ ] ={1,3,6,1,4,1,2076,41,40,1,28};
UINT4 FsBgp4PeerGroupRouteMapNameIn [ ] ={1,3,6,1,4,1,2076,41,40,1,29};
UINT4 FsBgp4PeerGroupRouteMapNameOut [ ] ={1,3,6,1,4,1,2076,41,40,1,30};
UINT4 FsBgp4PeerGroupStatus [ ] ={1,3,6,1,4,1,2076,41,40,1,31};
UINT4 FsBgp4PeerGroupIpPrefixNameIn [ ] ={1,3,6,1,4,1,2076,41,40,1,32};
UINT4 FsBgp4PeerGroupIpPrefixNameOut [ ] ={1,3,6,1,4,1,2076,41,40,1,33};
UINT4 FsBgp4PeerGroupOrfType [ ] ={1,3,6,1,4,1,2076,41,40,1,34};
UINT4 FsBgp4PeerGroupOrfCapMode [ ] ={1,3,6,1,4,1,2076,41,40,1,35};
UINT4 FsBgp4PeerGroupOrfRequest [ ] ={1,3,6,1,4,1,2076,41,40,1,36};
UINT4 FsBgp4PeerGroupBfdStatus [ ] ={1,3,6,1,4,1,2076,41,40,1,37};
UINT4 FsBgp4PeerGroupOverrideCapability [ ] ={1,3,6,1,4,1,2076,41,40,1,38};
UINT4 FsBgp4PeerGroupLocalAs [ ] ={1,3,6,1,4,1,2076,41,40,1,39};
UINT4 FsBgp4PeerAddrType [ ] ={1,3,6,1,4,1,2076,41,41,1,1};
UINT4 FsBgp4PeerAddress [ ] ={1,3,6,1,4,1,2076,41,41,1,2};
UINT4 FsBgp4PeerAddStatus [ ] ={1,3,6,1,4,1,2076,41,41,1,3};
UINT4 FsBgp4RmTestObject [ ] ={1,3,6,1,4,1,2076,41,45,1};
UINT4 Fsbgp4TCPMKTAuthKeyId [ ] ={1,3,6,1,4,1,2076,41,42,1,1,1};
UINT4 Fsbgp4TCPMKTAuthRecvKeyId [ ] ={1,3,6,1,4,1,2076,41,42,1,1,2};
UINT4 Fsbgp4TCPMKTAuthMasterKey [ ] ={1,3,6,1,4,1,2076,41,42,1,1,3};
UINT4 Fsbgp4TCPMKTAuthAlgo [ ] ={1,3,6,1,4,1,2076,41,42,1,1,4};
UINT4 Fsbgp4TCPMKTAuthTcpOptExc [ ] ={1,3,6,1,4,1,2076,41,42,1,1,5};
UINT4 Fsbgp4TCPMKTAuthRowStatus [ ] ={1,3,6,1,4,1,2076,41,42,1,1,6};
UINT4 Fsbgp4TCPAOAuthPeerType [ ] ={1,3,6,1,4,1,2076,41,43,1,1,1};
UINT4 Fsbgp4TCPAOAuthPeerAddr [ ] ={1,3,6,1,4,1,2076,41,43,1,1,2};
UINT4 Fsbgp4TCPAOAuthKeyId [ ] ={1,3,6,1,4,1,2076,41,43,1,1,3};
UINT4 Fsbgp4TCPAOAuthKeyStatus [ ] ={1,3,6,1,4,1,2076,41,43,1,1,4};
UINT4 Fsbgp4TCPAOAuthKeyStartAccept [ ] ={1,3,6,1,4,1,2076,41,43,1,1,5};
UINT4 Fsbgp4TCPAOAuthKeyStartGenerate [ ] ={1,3,6,1,4,1,2076,41,43,1,1,6};
UINT4 Fsbgp4TCPAOAuthKeyStopGenerate [ ] ={1,3,6,1,4,1,2076,41,43,1,1,7};
UINT4 Fsbgp4TCPAOAuthKeyStopAccept [ ] ={1,3,6,1,4,1,2076,41,43,1,1,8};
UINT4 FsBgp4ORFPeerAddrType [ ] ={1,3,6,1,4,1,2076,41,44,1,1};
UINT4 FsBgp4ORFPeerAddr [ ] ={1,3,6,1,4,1,2076,41,44,1,2};
UINT4 FsBgp4ORFAfi [ ] ={1,3,6,1,4,1,2076,41,44,1,3};
UINT4 FsBgp4ORFSafi [ ] ={1,3,6,1,4,1,2076,41,44,1,4};
UINT4 FsBgp4ORFType [ ] ={1,3,6,1,4,1,2076,41,44,1,5};
UINT4 FsBgp4ORFSequence [ ] ={1,3,6,1,4,1,2076,41,44,1,6};
UINT4 FsBgp4ORFAddrPrefix [ ] ={1,3,6,1,4,1,2076,41,44,1,7};
UINT4 FsBgp4ORFAddrPrefixLen [ ] ={1,3,6,1,4,1,2076,41,44,1,8};
UINT4 FsBgp4ORFMinLength [ ] ={1,3,6,1,4,1,2076,41,44,1,9};
UINT4 FsBgp4ORFMaxLength [ ] ={1,3,6,1,4,1,2076,41,44,1,10};
UINT4 FsBgp4ORFAction [ ] ={1,3,6,1,4,1,2076,41,44,1,11};
UINT4 FsBgp4RRDNetworkAddr [ ] ={1,3,6,1,4,1,2076,41,46,1,1};
UINT4 FsBgp4RRDNetworkAddrType [ ] ={1,3,6,1,4,1,2076,41,46,1,2};
UINT4 FsBgp4RRDNetworkPrefixLen [ ] ={1,3,6,1,4,1,2076,41,46,1,3};
UINT4 FsBgp4RRDNetworkRowStatus [ ] ={1,3,6,1,4,1,2076,41,46,1,4};




tMbDbEntry fsbgp4MibEntry[]= {

{{10,Fsbgp4GlobalAdminStatus}, NULL, Fsbgp4GlobalAdminStatusGet, Fsbgp4GlobalAdminStatusSet, Fsbgp4GlobalAdminStatusTest, Fsbgp4GlobalAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,Fsbgp4LocalAs}, NULL, Fsbgp4LocalAsGet, Fsbgp4LocalAsSet, Fsbgp4LocalAsTest, Fsbgp4LocalAsDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,Fsbgp4Identifier}, NULL, Fsbgp4IdentifierGet, Fsbgp4IdentifierSet, Fsbgp4IdentifierTest, Fsbgp4IdentifierDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Fsbgp4Synchronization}, NULL, Fsbgp4SynchronizationGet, Fsbgp4SynchronizationSet, Fsbgp4SynchronizationTest, Fsbgp4SynchronizationDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,Fsbgp4DefaultLocalPref}, NULL, Fsbgp4DefaultLocalPrefGet, Fsbgp4DefaultLocalPrefSet, Fsbgp4DefaultLocalPrefTest, Fsbgp4DefaultLocalPrefDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "100"},

{{10,Fsbgp4AdvtNonBgpRt}, NULL, Fsbgp4AdvtNonBgpRtGet, Fsbgp4AdvtNonBgpRtSet, Fsbgp4AdvtNonBgpRtTest, Fsbgp4AdvtNonBgpRtDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,Fsbgp4TraceEnable}, NULL, Fsbgp4TraceEnableGet, Fsbgp4TraceEnableSet, Fsbgp4TraceEnableTest, Fsbgp4TraceEnableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Fsbgp4DebugEnable}, NULL, Fsbgp4DebugEnableGet, Fsbgp4DebugEnableSet, Fsbgp4DebugEnableTest, Fsbgp4DebugEnableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Fsbgp4OverlappingRoute}, NULL, Fsbgp4OverlappingRouteGet, Fsbgp4OverlappingRouteSet, Fsbgp4OverlappingRouteTest, Fsbgp4OverlappingRouteDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{10,Fsbgp4MaxPeerEntry}, NULL, Fsbgp4MaxPeerEntryGet, Fsbgp4MaxPeerEntrySet, Fsbgp4MaxPeerEntryTest, Fsbgp4MaxPeerEntryDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "50"},

{{10,Fsbgp4MaxNoofRoutes}, NULL, Fsbgp4MaxNoofRoutesGet, Fsbgp4MaxNoofRoutesSet, Fsbgp4MaxNoofRoutesTest, Fsbgp4MaxNoofRoutesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "5000"},

{{10,Fsbgp4AlwaysCompareMED}, NULL, Fsbgp4AlwaysCompareMEDGet, Fsbgp4AlwaysCompareMEDSet, Fsbgp4AlwaysCompareMEDTest, Fsbgp4AlwaysCompareMEDDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,Fsbgp4DefaultOriginate}, NULL, Fsbgp4DefaultOriginateGet, Fsbgp4DefaultOriginateSet, Fsbgp4DefaultOriginateTest, Fsbgp4DefaultOriginateDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,Fsbgp4DefaultIpv4UniCast}, NULL, Fsbgp4DefaultIpv4UniCastGet, Fsbgp4DefaultIpv4UniCastSet, Fsbgp4DefaultIpv4UniCastTest, Fsbgp4DefaultIpv4UniCastDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,Fsbgp4GRAdminStatus}, NULL, Fsbgp4GRAdminStatusGet, Fsbgp4GRAdminStatusSet, Fsbgp4GRAdminStatusTest, Fsbgp4GRAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,Fsbgp4GRRestartTimeInterval}, NULL, Fsbgp4GRRestartTimeIntervalGet, Fsbgp4GRRestartTimeIntervalSet, Fsbgp4GRRestartTimeIntervalTest, Fsbgp4GRRestartTimeIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "90"},

{{10,Fsbgp4GRSelectionDeferralTimeInterval}, NULL, Fsbgp4GRSelectionDeferralTimeIntervalGet, Fsbgp4GRSelectionDeferralTimeIntervalSet, Fsbgp4GRSelectionDeferralTimeIntervalTest, Fsbgp4GRSelectionDeferralTimeIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "60"},

{{10,Fsbgp4GRStaleTimeInterval}, NULL, Fsbgp4GRStaleTimeIntervalGet, Fsbgp4GRStaleTimeIntervalSet, Fsbgp4GRStaleTimeIntervalTest, Fsbgp4GRStaleTimeIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "150"},

{{10,Fsbgp4GRMode}, NULL, Fsbgp4GRModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsbgp4RestartSupport}, NULL, Fsbgp4RestartSupportGet, Fsbgp4RestartSupportSet, Fsbgp4RestartSupportTest, Fsbgp4RestartSupportDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,Fsbgp4RestartStatus}, NULL, Fsbgp4RestartStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsbgp4RestartExitReason}, NULL, Fsbgp4RestartExitReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsbgp4RestartReason}, NULL, Fsbgp4RestartReasonGet, Fsbgp4RestartReasonSet, Fsbgp4RestartReasonTest, Fsbgp4RestartReasonDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,Fsbgp4ForwardingPreservation}, NULL, Fsbgp4ForwardingPreservationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Fsbgp4IsTrapEnabled}, NULL, Fsbgp4IsTrapEnabledGet, Fsbgp4IsTrapEnabledSet, Fsbgp4IsTrapEnabledTest, Fsbgp4IsTrapEnabledDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,Fsbgp4NextHopProcessingInterval}, NULL, Fsbgp4NextHopProcessingIntervalGet, Fsbgp4NextHopProcessingIntervalSet, Fsbgp4NextHopProcessingIntervalTest, Fsbgp4NextHopProcessingIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "60"},

{{10,Fsbgp4IBGPRedistributionStatus}, NULL, Fsbgp4IBGPRedistributionStatusGet, Fsbgp4IBGPRedistributionStatusSet, Fsbgp4IBGPRedistributionStatusTest, Fsbgp4IBGPRedistributionStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,Fsbgp4IBGPMaxPaths}, NULL, Fsbgp4IBGPMaxPathsGet, Fsbgp4IBGPMaxPathsSet, Fsbgp4IBGPMaxPathsTest, Fsbgp4IBGPMaxPathsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,Fsbgp4EBGPMaxPaths}, NULL, Fsbgp4EBGPMaxPathsGet, Fsbgp4EBGPMaxPathsSet, Fsbgp4EBGPMaxPathsTest, Fsbgp4EBGPMaxPathsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,Fsbgp4EIBGPMaxPaths}, NULL, Fsbgp4EIBGPMaxPathsGet, Fsbgp4EIBGPMaxPathsSet, Fsbgp4EIBGPMaxPathsTest, Fsbgp4EIBGPMaxPathsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,Fsbgp4OperIBGPMaxPaths}, NULL, Fsbgp4OperIBGPMaxPathsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, "1"},

{{10,Fsbgp4OperEBGPMaxPaths}, NULL, Fsbgp4OperEBGPMaxPathsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, "1"},

{{10,Fsbgp4OperEIBGPMaxPaths}, NULL, Fsbgp4OperEIBGPMaxPathsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, "1"},

{{10,Fsbgp4FourByteASNSupportStatus}, NULL, Fsbgp4FourByteASNSupportStatusGet, Fsbgp4FourByteASNSupportStatusSet, Fsbgp4FourByteASNSupportStatusTest, Fsbgp4FourByteASNSupportStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,Fsbgp4FourByteASNotationType}, NULL, Fsbgp4FourByteASNotationTypeGet, Fsbgp4FourByteASNotationTypeSet, Fsbgp4FourByteASNotationTypeTest, Fsbgp4FourByteASNotationTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,Fsbgp4VpnLabelAllocPolicy}, NULL, Fsbgp4VpnLabelAllocPolicyGet, Fsbgp4VpnLabelAllocPolicySet, Fsbgp4VpnLabelAllocPolicyTest, Fsbgp4VpnLabelAllocPolicyDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,Fsbgp4MacMobDuplicationTimeInterval}, NULL, Fsbgp4MacMobDuplicationTimeIntervalGet, Fsbgp4MacMobDuplicationTimeIntervalSet, Fsbgp4MacMobDuplicationTimeIntervalTest, Fsbgp4MacMobDuplicationTimeIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "180"},

{{10,Fsbgp4MaxMacMoves}, NULL, Fsbgp4MaxMacMovesGet, Fsbgp4MaxMacMovesSet, Fsbgp4MaxMacMovesTest, Fsbgp4MaxMacMovesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{10,Fsbgp4VpnRouteLeakStatus}, NULL, Fsbgp4VpnRouteLeakStatusGet, Fsbgp4VpnRouteLeakStatusSet, Fsbgp4VpnRouteLeakStatusTest, Fsbgp4VpnRouteLeakStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,Fsbgp4PeerExtPeerRemoteAddr}, GetNextIndexFsbgp4PeerExtTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4PeerExtTableINDEX, 1, 1, 0, NULL},

{{11,Fsbgp4PeerExtConfigurePeer}, GetNextIndexFsbgp4PeerExtTable, Fsbgp4PeerExtConfigurePeerGet, Fsbgp4PeerExtConfigurePeerSet, Fsbgp4PeerExtConfigurePeerTest, Fsbgp4PeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4PeerExtTableINDEX, 1, 1, 0, NULL},

{{11,Fsbgp4PeerExtPeerRemoteAs}, GetNextIndexFsbgp4PeerExtTable, Fsbgp4PeerExtPeerRemoteAsGet, Fsbgp4PeerExtPeerRemoteAsSet, Fsbgp4PeerExtPeerRemoteAsTest, Fsbgp4PeerExtTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsbgp4PeerExtTableINDEX, 1, 1, 0, NULL},

{{11,Fsbgp4PeerExtEBGPMultiHop}, GetNextIndexFsbgp4PeerExtTable, Fsbgp4PeerExtEBGPMultiHopGet, Fsbgp4PeerExtEBGPMultiHopSet, Fsbgp4PeerExtEBGPMultiHopTest, Fsbgp4PeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4PeerExtTableINDEX, 1, 1, 0, "2"},

{{11,Fsbgp4PeerExtNextHopSelf}, GetNextIndexFsbgp4PeerExtTable, Fsbgp4PeerExtNextHopSelfGet, Fsbgp4PeerExtNextHopSelfSet, Fsbgp4PeerExtNextHopSelfTest, Fsbgp4PeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4PeerExtTableINDEX, 1, 1, 0, "1"},

{{11,Fsbgp4PeerExtConnSrcIfId}, GetNextIndexFsbgp4PeerExtTable, Fsbgp4PeerExtConnSrcIfIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4PeerExtTableINDEX, 1, 1, 0, "0"},

{{11,Fsbgp4PeerExtRflClient}, GetNextIndexFsbgp4PeerExtTable, Fsbgp4PeerExtRflClientGet, Fsbgp4PeerExtRflClientSet, Fsbgp4PeerExtRflClientTest, Fsbgp4PeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4PeerExtTableINDEX, 1, 1, 0, "1"},

{{11,Fsbgp4MEDIndex}, GetNextIndexFsbgp4MEDTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4MEDTableINDEX, 1, 1, 0, NULL},

{{11,Fsbgp4MEDAdminStatus}, GetNextIndexFsbgp4MEDTable, Fsbgp4MEDAdminStatusGet, Fsbgp4MEDAdminStatusSet, Fsbgp4MEDAdminStatusTest, Fsbgp4MEDTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MEDTableINDEX, 1, 1, 0, NULL},

{{11,Fsbgp4MEDRemoteAS}, GetNextIndexFsbgp4MEDTable, Fsbgp4MEDRemoteASGet, Fsbgp4MEDRemoteASSet, Fsbgp4MEDRemoteASTest, Fsbgp4MEDTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsbgp4MEDTableINDEX, 1, 1, 0, "0"},

{{11,Fsbgp4MEDIPAddrPrefix}, GetNextIndexFsbgp4MEDTable, Fsbgp4MEDIPAddrPrefixGet, Fsbgp4MEDIPAddrPrefixSet, Fsbgp4MEDIPAddrPrefixTest, Fsbgp4MEDTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, Fsbgp4MEDTableINDEX, 1, 1, 0, "0"},

{{11,Fsbgp4MEDIPAddrPrefixLen}, GetNextIndexFsbgp4MEDTable, Fsbgp4MEDIPAddrPrefixLenGet, Fsbgp4MEDIPAddrPrefixLenSet, Fsbgp4MEDIPAddrPrefixLenTest, Fsbgp4MEDTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4MEDTableINDEX, 1, 1, 0, "0"},

{{11,Fsbgp4MEDIntermediateAS}, GetNextIndexFsbgp4MEDTable, Fsbgp4MEDIntermediateASGet, Fsbgp4MEDIntermediateASSet, Fsbgp4MEDIntermediateASTest, Fsbgp4MEDTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MEDTableINDEX, 1, 1, 0, NULL},

{{11,Fsbgp4MEDDirection}, GetNextIndexFsbgp4MEDTable, Fsbgp4MEDDirectionGet, Fsbgp4MEDDirectionSet, Fsbgp4MEDDirectionTest, Fsbgp4MEDTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MEDTableINDEX, 1, 1, 0, "1"},

{{11,Fsbgp4MEDValue}, GetNextIndexFsbgp4MEDTable, Fsbgp4MEDValueGet, Fsbgp4MEDValueSet, Fsbgp4MEDValueTest, Fsbgp4MEDTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsbgp4MEDTableINDEX, 1, 1, 0, "0"},

{{11,Fsbgp4MEDPreference}, GetNextIndexFsbgp4MEDTable, Fsbgp4MEDPreferenceGet, Fsbgp4MEDPreferenceSet, Fsbgp4MEDPreferenceTest, Fsbgp4MEDTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MEDTableINDEX, 1, 1, 0, "1"},

{{11,Fsbgp4LocalPrefIndex}, GetNextIndexFsbgp4LocalPrefTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4LocalPrefTableINDEX, 1, 1, 0, NULL},

{{11,Fsbgp4LocalPrefAdminStatus}, GetNextIndexFsbgp4LocalPrefTable, Fsbgp4LocalPrefAdminStatusGet, Fsbgp4LocalPrefAdminStatusSet, Fsbgp4LocalPrefAdminStatusTest, Fsbgp4LocalPrefTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4LocalPrefTableINDEX, 1, 1, 0, NULL},

{{11,Fsbgp4LocalPrefRemoteAS}, GetNextIndexFsbgp4LocalPrefTable, Fsbgp4LocalPrefRemoteASGet, Fsbgp4LocalPrefRemoteASSet, Fsbgp4LocalPrefRemoteASTest, Fsbgp4LocalPrefTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsbgp4LocalPrefTableINDEX, 1, 1, 0, "0"},

{{11,Fsbgp4LocalPrefIPAddrPrefix}, GetNextIndexFsbgp4LocalPrefTable, Fsbgp4LocalPrefIPAddrPrefixGet, Fsbgp4LocalPrefIPAddrPrefixSet, Fsbgp4LocalPrefIPAddrPrefixTest, Fsbgp4LocalPrefTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, Fsbgp4LocalPrefTableINDEX, 1, 1, 0, "0"},

{{11,Fsbgp4LocalPrefIPAddrPrefixLen}, GetNextIndexFsbgp4LocalPrefTable, Fsbgp4LocalPrefIPAddrPrefixLenGet, Fsbgp4LocalPrefIPAddrPrefixLenSet, Fsbgp4LocalPrefIPAddrPrefixLenTest, Fsbgp4LocalPrefTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4LocalPrefTableINDEX, 1, 1, 0, "0"},

{{11,Fsbgp4LocalPrefIntermediateAS}, GetNextIndexFsbgp4LocalPrefTable, Fsbgp4LocalPrefIntermediateASGet, Fsbgp4LocalPrefIntermediateASSet, Fsbgp4LocalPrefIntermediateASTest, Fsbgp4LocalPrefTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4LocalPrefTableINDEX, 1, 1, 0, NULL},

{{11,Fsbgp4LocalPrefDirection}, GetNextIndexFsbgp4LocalPrefTable, Fsbgp4LocalPrefDirectionGet, Fsbgp4LocalPrefDirectionSet, Fsbgp4LocalPrefDirectionTest, Fsbgp4LocalPrefTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4LocalPrefTableINDEX, 1, 1, 0, "1"},

{{11,Fsbgp4LocalPrefValue}, GetNextIndexFsbgp4LocalPrefTable, Fsbgp4LocalPrefValueGet, Fsbgp4LocalPrefValueSet, Fsbgp4LocalPrefValueTest, Fsbgp4LocalPrefTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsbgp4LocalPrefTableINDEX, 1, 1, 0, "100"},

{{11,Fsbgp4LocalPrefPreference}, GetNextIndexFsbgp4LocalPrefTable, Fsbgp4LocalPrefPreferenceGet, Fsbgp4LocalPrefPreferenceSet, Fsbgp4LocalPrefPreferenceTest, Fsbgp4LocalPrefTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4LocalPrefTableINDEX, 1, 1, 0, "1"},

{{11,Fsbgp4UpdateFilterIndex}, GetNextIndexFsbgp4UpdateFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4UpdateFilterTableINDEX, 1, 1, 0, NULL},

{{11,Fsbgp4UpdateFilterAdminStatus}, GetNextIndexFsbgp4UpdateFilterTable, Fsbgp4UpdateFilterAdminStatusGet, Fsbgp4UpdateFilterAdminStatusSet, Fsbgp4UpdateFilterAdminStatusTest, Fsbgp4UpdateFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4UpdateFilterTableINDEX, 1, 1, 0, NULL},

{{11,Fsbgp4UpdateFilterRemoteAS}, GetNextIndexFsbgp4UpdateFilterTable, Fsbgp4UpdateFilterRemoteASGet, Fsbgp4UpdateFilterRemoteASSet, Fsbgp4UpdateFilterRemoteASTest, Fsbgp4UpdateFilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsbgp4UpdateFilterTableINDEX, 1, 1, 0, "0"},

{{11,Fsbgp4UpdateFilterIPAddrPrefix}, GetNextIndexFsbgp4UpdateFilterTable, Fsbgp4UpdateFilterIPAddrPrefixGet, Fsbgp4UpdateFilterIPAddrPrefixSet, Fsbgp4UpdateFilterIPAddrPrefixTest, Fsbgp4UpdateFilterTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, Fsbgp4UpdateFilterTableINDEX, 1, 1, 0, "0"},

{{11,Fsbgp4UpdateFilterIPAddrPrefixLen}, GetNextIndexFsbgp4UpdateFilterTable, Fsbgp4UpdateFilterIPAddrPrefixLenGet, Fsbgp4UpdateFilterIPAddrPrefixLenSet, Fsbgp4UpdateFilterIPAddrPrefixLenTest, Fsbgp4UpdateFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4UpdateFilterTableINDEX, 1, 1, 0, "0"},

{{11,Fsbgp4UpdateFilterIntermediateAS}, GetNextIndexFsbgp4UpdateFilterTable, Fsbgp4UpdateFilterIntermediateASGet, Fsbgp4UpdateFilterIntermediateASSet, Fsbgp4UpdateFilterIntermediateASTest, Fsbgp4UpdateFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4UpdateFilterTableINDEX, 1, 1, 0, NULL},

{{11,Fsbgp4UpdateFilterDirection}, GetNextIndexFsbgp4UpdateFilterTable, Fsbgp4UpdateFilterDirectionGet, Fsbgp4UpdateFilterDirectionSet, Fsbgp4UpdateFilterDirectionTest, Fsbgp4UpdateFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4UpdateFilterTableINDEX, 1, 1, 0, "1"},

{{11,Fsbgp4UpdateFilterAction}, GetNextIndexFsbgp4UpdateFilterTable, Fsbgp4UpdateFilterActionGet, Fsbgp4UpdateFilterActionSet, Fsbgp4UpdateFilterActionTest, Fsbgp4UpdateFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4UpdateFilterTableINDEX, 1, 1, 0, "2"},

{{11,Fsbgp4AggregateIndex}, GetNextIndexFsbgp4AggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4AggregateTableINDEX, 1, 1, 0, NULL},

{{11,Fsbgp4AggregateAdminStatus}, GetNextIndexFsbgp4AggregateTable, Fsbgp4AggregateAdminStatusGet, Fsbgp4AggregateAdminStatusSet, Fsbgp4AggregateAdminStatusTest, Fsbgp4AggregateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4AggregateTableINDEX, 1, 1, 0, NULL},

{{11,Fsbgp4AggregateIPAddrPrefix}, GetNextIndexFsbgp4AggregateTable, Fsbgp4AggregateIPAddrPrefixGet, Fsbgp4AggregateIPAddrPrefixSet, Fsbgp4AggregateIPAddrPrefixTest, Fsbgp4AggregateTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, Fsbgp4AggregateTableINDEX, 1, 1, 0, NULL},

{{11,Fsbgp4AggregateIPAddrPrefixLen}, GetNextIndexFsbgp4AggregateTable, Fsbgp4AggregateIPAddrPrefixLenGet, Fsbgp4AggregateIPAddrPrefixLenSet, Fsbgp4AggregateIPAddrPrefixLenTest, Fsbgp4AggregateTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4AggregateTableINDEX, 1, 1, 0, NULL},

{{11,Fsbgp4AggregateAdvertise}, GetNextIndexFsbgp4AggregateTable, Fsbgp4AggregateAdvertiseGet, Fsbgp4AggregateAdvertiseSet, Fsbgp4AggregateAdvertiseTest, Fsbgp4AggregateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4AggregateTableINDEX, 1, 1, 0, NULL},

{{10,Fsbgp4RRDAdminStatus}, NULL, Fsbgp4RRDAdminStatusGet, Fsbgp4RRDAdminStatusSet, Fsbgp4RRDAdminStatusTest, Fsbgp4RRDAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,Fsbgp4RRDProtoMaskForEnable}, NULL, Fsbgp4RRDProtoMaskForEnableGet, Fsbgp4RRDProtoMaskForEnableSet, Fsbgp4RRDProtoMaskForEnableTest, Fsbgp4RRDProtoMaskForEnableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Fsbgp4RRDSrcProtoMaskForDisable}, NULL, Fsbgp4RRDSrcProtoMaskForDisableGet, Fsbgp4RRDSrcProtoMaskForDisableSet, Fsbgp4RRDSrcProtoMaskForDisableTest, Fsbgp4RRDSrcProtoMaskForDisableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Fsbgp4RRDDefaultMetric}, NULL, Fsbgp4RRDDefaultMetricGet, Fsbgp4RRDDefaultMetricSet, Fsbgp4RRDDefaultMetricTest, Fsbgp4RRDDefaultMetricDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,Fsbgp4RRDRouteMapName}, NULL, Fsbgp4RRDRouteMapNameGet, Fsbgp4RRDRouteMapNameSet, Fsbgp4RRDRouteMapNameTest, Fsbgp4RRDRouteMapNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Fsbgp4RRDMatchTypeEnable}, NULL, Fsbgp4RRDMatchTypeEnableGet, Fsbgp4RRDMatchTypeEnableSet, Fsbgp4RRDMatchTypeEnableTest, Fsbgp4RRDMatchTypeEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Fsbgp4RRDMatchTypeDisable}, NULL, Fsbgp4RRDMatchTypeDisableGet, Fsbgp4RRDMatchTypeDisableSet, Fsbgp4RRDMatchTypeDisableTest, Fsbgp4RRDMatchTypeDisableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsBgp4RRDMetricProtocolId}, GetNextIndexFsbgp4RRDMetricTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4RRDMetricTableINDEX, 1, 0, 0, NULL},

{{12,FsBgp4RRDMetricValue}, GetNextIndexFsbgp4RRDMetricTable, FsBgp4RRDMetricValueGet, FsBgp4RRDMetricValueSet, FsBgp4RRDMetricValueTest, Fsbgp4RRDMetricTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsbgp4RRDMetricTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4ImportRoutePrefix}, GetNextIndexFsbgp4ImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4ImportRouteTableINDEX, 6, 1, 0, NULL},

{{11,Fsbgp4ImportRoutePrefixLen}, GetNextIndexFsbgp4ImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4ImportRouteTableINDEX, 6, 1, 0, NULL},

{{11,Fsbgp4ImportRouteProtocol}, GetNextIndexFsbgp4ImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4ImportRouteTableINDEX, 6, 1, 0, NULL},

{{11,Fsbgp4ImportRouteNextHop}, GetNextIndexFsbgp4ImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4ImportRouteTableINDEX, 6, 1, 0, NULL},

{{11,Fsbgp4ImportRouteIfIndex}, GetNextIndexFsbgp4ImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4ImportRouteTableINDEX, 6, 1, 0, NULL},

{{11,Fsbgp4ImportRouteMetric}, GetNextIndexFsbgp4ImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4ImportRouteTableINDEX, 6, 1, 0, NULL},

{{11,Fsbgp4ImportRouteAction}, GetNextIndexFsbgp4ImportRouteTable, Fsbgp4ImportRouteActionGet, Fsbgp4ImportRouteActionSet, Fsbgp4ImportRouteActionTest, Fsbgp4ImportRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4ImportRouteTableINDEX, 6, 1, 0, NULL},

{{11,Fsbgp4Peer}, GetNextIndexFsbgp4FsmTransitionHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4FsmTransitionHistTableINDEX, 1, 1, 0, NULL},

{{11,Fsbgp4FsmTransitionHist}, GetNextIndexFsbgp4FsmTransitionHistTable, Fsbgp4FsmTransitionHistGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4FsmTransitionHistTableINDEX, 1, 1, 0, NULL},

{{11,Fsbgp4RflbgpClusterId}, NULL, Fsbgp4RflbgpClusterIdGet, Fsbgp4RflbgpClusterIdSet, Fsbgp4RflbgpClusterIdTest, Fsbgp4RflbgpClusterIdDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Fsbgp4RflRflSupport}, NULL, Fsbgp4RflRflSupportGet, Fsbgp4RflRflSupportSet, Fsbgp4RflRflSupportTest, Fsbgp4RflRflSupportDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{12,Fsbgp4RflPathAttrAddrPrefix}, GetNextIndexFsbgp4RflRouteReflectorTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4RflRouteReflectorTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4RflPathAttrAddrPrefixLen}, GetNextIndexFsbgp4RflRouteReflectorTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4RflRouteReflectorTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4RflPathAttrPeer}, GetNextIndexFsbgp4RflRouteReflectorTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4RflRouteReflectorTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4RflPathAttrOriginatorId}, GetNextIndexFsbgp4RflRouteReflectorTable, Fsbgp4RflPathAttrOriginatorIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4RflRouteReflectorTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4RflPathAttrClusterList}, GetNextIndexFsbgp4RflRouteReflectorTable, Fsbgp4RflPathAttrClusterListGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4RflRouteReflectorTableINDEX, 3, 1, 0, NULL},

{{11,Fsbgp4RfdCutOff}, NULL, Fsbgp4RfdCutOffGet, Fsbgp4RfdCutOffSet, Fsbgp4RfdCutOffTest, Fsbgp4RfdCutOffDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "2000"},

{{11,Fsbgp4RfdReuse}, NULL, Fsbgp4RfdReuseGet, Fsbgp4RfdReuseSet, Fsbgp4RfdReuseTest, Fsbgp4RfdReuseDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "750"},

{{11,Fsbgp4RfdCeiling}, NULL, Fsbgp4RfdCeilingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, "8000"},

{{11,Fsbgp4RfdMaxHoldDownTime}, NULL, Fsbgp4RfdMaxHoldDownTimeGet, Fsbgp4RfdMaxHoldDownTimeSet, Fsbgp4RfdMaxHoldDownTimeTest, Fsbgp4RfdMaxHoldDownTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "3600"},

{{11,Fsbgp4RfdDecayHalfLifeTime}, NULL, Fsbgp4RfdDecayHalfLifeTimeGet, Fsbgp4RfdDecayHalfLifeTimeSet, Fsbgp4RfdDecayHalfLifeTimeTest, Fsbgp4RfdDecayHalfLifeTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "900"},

{{11,Fsbgp4RfdDecayTimerGranularity}, NULL, Fsbgp4RfdDecayTimerGranularityGet, Fsbgp4RfdDecayTimerGranularitySet, Fsbgp4RfdDecayTimerGranularityTest, Fsbgp4RfdDecayTimerGranularityDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,Fsbgp4RfdReuseTimerGranularity}, NULL, Fsbgp4RfdReuseTimerGranularityGet, Fsbgp4RfdReuseTimerGranularitySet, Fsbgp4RfdReuseTimerGranularityTest, Fsbgp4RfdReuseTimerGranularityDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "15"},

{{11,Fsbgp4RfdReuseIndxArraySize}, NULL, Fsbgp4RfdReuseIndxArraySizeGet, Fsbgp4RfdReuseIndxArraySizeSet, Fsbgp4RfdReuseIndxArraySizeTest, Fsbgp4RfdReuseIndxArraySizeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "1024"},

{{11,Fsbgp4RfdAdminStatus}, NULL, Fsbgp4RfdAdminStatusGet, Fsbgp4RfdAdminStatusSet, Fsbgp4RfdAdminStatusTest, Fsbgp4RfdAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{12,Fsbgp4PathAttrAddrPrefix}, GetNextIndexFsbgp4RfdRtDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4RfdRtDampHistTableINDEX, 4, 1, 0, NULL},

{{12,Fsbgp4PathAttrAddrPrefixLen}, GetNextIndexFsbgp4RfdRtDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4RfdRtDampHistTableINDEX, 4, 1, 0, NULL},

{{12,Fsbgp4PathAttrPeer}, GetNextIndexFsbgp4RfdRtDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4RfdRtDampHistTableINDEX, 4, 1, 0, NULL},

{{12,Fsbgp4RtDampHistInstance}, GetNextIndexFsbgp4RfdRtDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4RfdRtDampHistTableINDEX, 4, 1, 0, NULL},

{{12,Fsbgp4RfdRtFom}, GetNextIndexFsbgp4RfdRtDampHistTable, Fsbgp4RfdRtFomGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4RfdRtDampHistTableINDEX, 4, 1, 0, NULL},

{{12,Fsbgp4RfdRtLastUpdtTime}, GetNextIndexFsbgp4RfdRtDampHistTable, Fsbgp4RfdRtLastUpdtTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4RfdRtDampHistTableINDEX, 4, 1, 0, NULL},

{{12,Fsbgp4RfdRtState}, GetNextIndexFsbgp4RfdRtDampHistTable, Fsbgp4RfdRtStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4RfdRtDampHistTableINDEX, 4, 1, 0, NULL},

{{12,Fsbgp4RfdRtStatus}, GetNextIndexFsbgp4RfdRtDampHistTable, Fsbgp4RfdRtStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4RfdRtDampHistTableINDEX, 4, 1, 0, NULL},

{{12,Fsbgp4PeerRemoteIpAddr}, GetNextIndexFsbgp4RfdPeerDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4RfdPeerDampHistTableINDEX, 1, 1, 0, NULL},

{{12,Fsbgp4RfdPeerFom}, GetNextIndexFsbgp4RfdPeerDampHistTable, Fsbgp4RfdPeerFomGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4RfdPeerDampHistTableINDEX, 1, 1, 0, NULL},

{{12,Fsbgp4RfdPeerLastUpdtTime}, GetNextIndexFsbgp4RfdPeerDampHistTable, Fsbgp4RfdPeerLastUpdtTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4RfdPeerDampHistTableINDEX, 1, 1, 0, NULL},

{{12,Fsbgp4RfdPeerState}, GetNextIndexFsbgp4RfdPeerDampHistTable, Fsbgp4RfdPeerStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4RfdPeerDampHistTableINDEX, 1, 1, 0, NULL},

{{12,Fsbgp4RfdPeerStatus}, GetNextIndexFsbgp4RfdPeerDampHistTable, Fsbgp4RfdPeerStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4RfdPeerDampHistTableINDEX, 1, 1, 0, NULL},

{{12,Fsbgp4RtIPPrefix}, GetNextIndexFsbgp4RfdRtsReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4RfdRtsReuseListTableINDEX, 4, 1, 0, NULL},

{{12,Fsbgp4RtIPPrefixLen}, GetNextIndexFsbgp4RfdRtsReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4RfdRtsReuseListTableINDEX, 4, 1, 0, NULL},

{{12,Fsbgp4PeerRemAddress}, GetNextIndexFsbgp4RfdRtsReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4RfdRtsReuseListTableINDEX, 4, 1, 0, NULL},

{{12,Fsbgp4RfdRtReuseListInstance}, GetNextIndexFsbgp4RfdRtsReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4RfdRtsReuseListTableINDEX, 4, 1, 0, NULL},

{{12,Fsbgp4RfdRtReuseListRtFom}, GetNextIndexFsbgp4RfdRtsReuseListTable, Fsbgp4RfdRtReuseListRtFomGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4RfdRtsReuseListTableINDEX, 4, 1, 0, NULL},

{{12,Fsbgp4RfdRtReuseListRtLastUpdtTime}, GetNextIndexFsbgp4RfdRtsReuseListTable, Fsbgp4RfdRtReuseListRtLastUpdtTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4RfdRtsReuseListTableINDEX, 4, 1, 0, NULL},

{{12,Fsbgp4RfdRtReuseListRtState}, GetNextIndexFsbgp4RfdRtsReuseListTable, Fsbgp4RfdRtReuseListRtStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4RfdRtsReuseListTableINDEX, 4, 1, 0, NULL},

{{12,Fsbgp4RfdRtReuseListRtStatus}, GetNextIndexFsbgp4RfdRtsReuseListTable, Fsbgp4RfdRtReuseListRtStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4RfdRtsReuseListTableINDEX, 4, 1, 0, NULL},

{{12,Fsbgp4RfdPeerRemIpAddr}, GetNextIndexFsbgp4RfdPeerReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4RfdPeerReuseListTableINDEX, 1, 1, 0, NULL},

{{12,Fsbgp4RfdPeerReuseListPeerFom}, GetNextIndexFsbgp4RfdPeerReuseListTable, Fsbgp4RfdPeerReuseListPeerFomGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4RfdPeerReuseListTableINDEX, 1, 1, 0, NULL},

{{12,Fsbgp4RfdPeerReuseListLastUpdtTime}, GetNextIndexFsbgp4RfdPeerReuseListTable, Fsbgp4RfdPeerReuseListLastUpdtTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4RfdPeerReuseListTableINDEX, 1, 1, 0, NULL},

{{12,Fsbgp4RfdPeerReuseListPeerState}, GetNextIndexFsbgp4RfdPeerReuseListTable, Fsbgp4RfdPeerReuseListPeerStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4RfdPeerReuseListTableINDEX, 1, 1, 0, NULL},

{{12,Fsbgp4RfdPeerReuseListPeerStatus}, GetNextIndexFsbgp4RfdPeerReuseListTable, Fsbgp4RfdPeerReuseListPeerStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4RfdPeerReuseListTableINDEX, 1, 1, 0, NULL},

{{11,Fsbgp4CommMaxInFTblEntries}, NULL, Fsbgp4CommMaxInFTblEntriesGet, Fsbgp4CommMaxInFTblEntriesSet, Fsbgp4CommMaxInFTblEntriesTest, Fsbgp4CommMaxInFTblEntriesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "10000"},

{{11,Fsbgp4CommMaxOutFTblEntries}, NULL, Fsbgp4CommMaxOutFTblEntriesGet, Fsbgp4CommMaxOutFTblEntriesSet, Fsbgp4CommMaxOutFTblEntriesTest, Fsbgp4CommMaxOutFTblEntriesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "10000"},

{{12,Fsbgp4AddCommIpNetwork}, GetNextIndexFsbgp4CommRouteAddCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4CommRouteAddCommTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4AddCommIpPrefixLen}, GetNextIndexFsbgp4CommRouteAddCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4CommRouteAddCommTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4AddCommVal}, GetNextIndexFsbgp4CommRouteAddCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Fsbgp4CommRouteAddCommTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4AddCommRowStatus}, GetNextIndexFsbgp4CommRouteAddCommTable, Fsbgp4AddCommRowStatusGet, Fsbgp4AddCommRowStatusSet, Fsbgp4AddCommRowStatusTest, Fsbgp4CommRouteAddCommTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4CommRouteAddCommTableINDEX, 3, 1, 1, NULL},

{{12,Fsbgp4DeleteCommIpNetwork}, GetNextIndexFsbgp4CommRouteDeleteCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4CommRouteDeleteCommTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4DeleteCommIpPrefixLen}, GetNextIndexFsbgp4CommRouteDeleteCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4CommRouteDeleteCommTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4DeleteCommVal}, GetNextIndexFsbgp4CommRouteDeleteCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Fsbgp4CommRouteDeleteCommTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4DeleteCommRowStatus}, GetNextIndexFsbgp4CommRouteDeleteCommTable, Fsbgp4DeleteCommRowStatusGet, Fsbgp4DeleteCommRowStatusSet, Fsbgp4DeleteCommRowStatusTest, Fsbgp4CommRouteDeleteCommTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4CommRouteDeleteCommTableINDEX, 3, 1, 1, NULL},

{{12,Fsbgp4CommSetStatusIpNetwork}, GetNextIndexFsbgp4CommRouteCommSetStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4CommRouteCommSetStatusTableINDEX, 2, 1, 0, NULL},

{{12,Fsbgp4CommSetStatusIpPrefixLen}, GetNextIndexFsbgp4CommRouteCommSetStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4CommRouteCommSetStatusTableINDEX, 2, 1, 0, NULL},

{{12,Fsbgp4CommSetStatus}, GetNextIndexFsbgp4CommRouteCommSetStatusTable, Fsbgp4CommSetStatusGet, Fsbgp4CommSetStatusSet, Fsbgp4CommSetStatusTest, Fsbgp4CommRouteCommSetStatusTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4CommRouteCommSetStatusTableINDEX, 2, 1, 0, "4"},

{{12,Fsbgp4CommSetStatusRowStatus}, GetNextIndexFsbgp4CommRouteCommSetStatusTable, Fsbgp4CommSetStatusRowStatusGet, Fsbgp4CommSetStatusRowStatusSet, Fsbgp4CommSetStatusRowStatusTest, Fsbgp4CommRouteCommSetStatusTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4CommRouteCommSetStatusTableINDEX, 2, 1, 1, NULL},

{{12,Fsbgp4PeerAddress}, GetNextIndexFsbgp4CommPeerSendStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4CommPeerSendStatusTableINDEX, 1, 1, 0, NULL},

{{12,Fsbgp4CommSendStatus}, GetNextIndexFsbgp4CommPeerSendStatusTable, Fsbgp4CommSendStatusGet, Fsbgp4CommSendStatusSet, Fsbgp4CommSendStatusTest, Fsbgp4CommPeerSendStatusTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4CommPeerSendStatusTableINDEX, 1, 1, 0, "3"},

{{12,Fsbgp4CommPeerSendRowStatus}, GetNextIndexFsbgp4CommPeerSendStatusTable, Fsbgp4CommPeerSendRowStatusGet, Fsbgp4CommPeerSendRowStatusSet, Fsbgp4CommPeerSendRowStatusTest, Fsbgp4CommPeerSendStatusTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4CommPeerSendStatusTableINDEX, 1, 1, 1, NULL},

{{12,Fsbgp4InFilterCommVal}, GetNextIndexFsbgp4CommInFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Fsbgp4CommInFilterTableINDEX, 1, 0, 0, NULL},

{{12,Fsbgp4CommIncomingFilterStatus}, GetNextIndexFsbgp4CommInFilterTable, Fsbgp4CommIncomingFilterStatusGet, Fsbgp4CommIncomingFilterStatusSet, Fsbgp4CommIncomingFilterStatusTest, Fsbgp4CommInFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4CommInFilterTableINDEX, 1, 0, 0, "2"},

{{12,Fsbgp4InFilterRowStatus}, GetNextIndexFsbgp4CommInFilterTable, Fsbgp4InFilterRowStatusGet, Fsbgp4InFilterRowStatusSet, Fsbgp4InFilterRowStatusTest, Fsbgp4CommInFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4CommInFilterTableINDEX, 1, 0, 1, NULL},

{{12,Fsbgp4OutFilterCommVal}, GetNextIndexFsbgp4CommOutFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Fsbgp4CommOutFilterTableINDEX, 1, 0, 0, NULL},

{{12,Fsbgp4CommOutgoingFilterStatus}, GetNextIndexFsbgp4CommOutFilterTable, Fsbgp4CommOutgoingFilterStatusGet, Fsbgp4CommOutgoingFilterStatusSet, Fsbgp4CommOutgoingFilterStatusTest, Fsbgp4CommOutFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4CommOutFilterTableINDEX, 1, 0, 0, "2"},

{{12,Fsbgp4OutFilterRowStatus}, GetNextIndexFsbgp4CommOutFilterTable, Fsbgp4OutFilterRowStatusGet, Fsbgp4OutFilterRowStatusSet, Fsbgp4OutFilterRowStatusTest, Fsbgp4CommOutFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4CommOutFilterTableINDEX, 1, 0, 1, NULL},

{{12,Fsbgp4IpNet}, GetNextIndexFsbgp4CommReceivedRouteCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4CommReceivedRouteCommTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4IPPrefixLength}, GetNextIndexFsbgp4CommReceivedRouteCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4CommReceivedRouteCommTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4PeerRemAddr}, GetNextIndexFsbgp4CommReceivedRouteCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4CommReceivedRouteCommTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4ReceivedRouteCommPath}, GetNextIndexFsbgp4CommReceivedRouteCommTable, Fsbgp4ReceivedRouteCommPathGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4CommReceivedRouteCommTableINDEX, 3, 1, 0, NULL},

{{11,Fsbgp4ExtCommMaxInFTblEntries}, NULL, Fsbgp4ExtCommMaxInFTblEntriesGet, Fsbgp4ExtCommMaxInFTblEntriesSet, Fsbgp4ExtCommMaxInFTblEntriesTest, Fsbgp4ExtCommMaxInFTblEntriesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "2000"},

{{11,Fsbgp4ExtCommMaxOutFTblEntries}, NULL, Fsbgp4ExtCommMaxOutFTblEntriesGet, Fsbgp4ExtCommMaxOutFTblEntriesSet, Fsbgp4ExtCommMaxOutFTblEntriesTest, Fsbgp4ExtCommMaxOutFTblEntriesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "2000"},

{{12,Fsbgp4AddExtCommIpNetwork}, GetNextIndexFsbgp4ExtCommRouteAddExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4ExtCommRouteAddExtCommTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4AddExtCommIpPrefixLen}, GetNextIndexFsbgp4ExtCommRouteAddExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4ExtCommRouteAddExtCommTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4AddExtCommVal}, GetNextIndexFsbgp4ExtCommRouteAddExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4ExtCommRouteAddExtCommTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4AddExtCommRowStatus}, GetNextIndexFsbgp4ExtCommRouteAddExtCommTable, Fsbgp4AddExtCommRowStatusGet, Fsbgp4AddExtCommRowStatusSet, Fsbgp4AddExtCommRowStatusTest, Fsbgp4ExtCommRouteAddExtCommTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4ExtCommRouteAddExtCommTableINDEX, 3, 1, 1, NULL},

{{12,Fsbgp4DeleteExtCommIpNetwork}, GetNextIndexFsbgp4ExtCommRouteDeleteExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4ExtCommRouteDeleteExtCommTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4DeleteExtCommIpPrefixLen}, GetNextIndexFsbgp4ExtCommRouteDeleteExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4ExtCommRouteDeleteExtCommTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4DeleteExtCommVal}, GetNextIndexFsbgp4ExtCommRouteDeleteExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4ExtCommRouteDeleteExtCommTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4DeleteExtCommRowStatus}, GetNextIndexFsbgp4ExtCommRouteDeleteExtCommTable, Fsbgp4DeleteExtCommRowStatusGet, Fsbgp4DeleteExtCommRowStatusSet, Fsbgp4DeleteExtCommRowStatusTest, Fsbgp4ExtCommRouteDeleteExtCommTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4ExtCommRouteDeleteExtCommTableINDEX, 3, 1, 1, NULL},

{{12,Fsbgp4ExtCommSetStatusIpNetwork}, GetNextIndexFsbgp4ExtCommRouteExtCommSetStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4ExtCommRouteExtCommSetStatusTableINDEX, 2, 1, 0, NULL},

{{12,Fsbgp4ExtCommSetStatusIpPrefixLen}, GetNextIndexFsbgp4ExtCommRouteExtCommSetStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4ExtCommRouteExtCommSetStatusTableINDEX, 2, 1, 0, NULL},

{{12,Fsbgp4ExtCommSetStatus}, GetNextIndexFsbgp4ExtCommRouteExtCommSetStatusTable, Fsbgp4ExtCommSetStatusGet, Fsbgp4ExtCommSetStatusSet, Fsbgp4ExtCommSetStatusTest, Fsbgp4ExtCommRouteExtCommSetStatusTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4ExtCommRouteExtCommSetStatusTableINDEX, 2, 1, 0, "4"},

{{12,Fsbgp4ExtCommSetStatusRowStatus}, GetNextIndexFsbgp4ExtCommRouteExtCommSetStatusTable, Fsbgp4ExtCommSetStatusRowStatusGet, Fsbgp4ExtCommSetStatusRowStatusSet, Fsbgp4ExtCommSetStatusRowStatusTest, Fsbgp4ExtCommRouteExtCommSetStatusTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4ExtCommRouteExtCommSetStatusTableINDEX, 2, 1, 1, NULL},

{{12,Fsbgp4ExtCommPeerAddress}, GetNextIndexFsbgp4ExtCommPeerSendStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4ExtCommPeerSendStatusTableINDEX, 1, 1, 0, NULL},

{{12,Fsbgp4ExtCommPeerSendStatus}, GetNextIndexFsbgp4ExtCommPeerSendStatusTable, Fsbgp4ExtCommPeerSendStatusGet, Fsbgp4ExtCommPeerSendStatusSet, Fsbgp4ExtCommPeerSendStatusTest, Fsbgp4ExtCommPeerSendStatusTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4ExtCommPeerSendStatusTableINDEX, 1, 1, 0, "3"},

{{12,Fsbgp4ExtCommPeerSendStatusRowStatus}, GetNextIndexFsbgp4ExtCommPeerSendStatusTable, Fsbgp4ExtCommPeerSendStatusRowStatusGet, Fsbgp4ExtCommPeerSendStatusRowStatusSet, Fsbgp4ExtCommPeerSendStatusRowStatusTest, Fsbgp4ExtCommPeerSendStatusTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4ExtCommPeerSendStatusTableINDEX, 1, 1, 1, NULL},

{{12,Fsbgp4ExtCommInFilterCommVal}, GetNextIndexFsbgp4ExtCommInFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4ExtCommInFilterTableINDEX, 1, 0, 0, NULL},

{{12,Fsbgp4ExtCommIncomingFilterStatus}, GetNextIndexFsbgp4ExtCommInFilterTable, Fsbgp4ExtCommIncomingFilterStatusGet, Fsbgp4ExtCommIncomingFilterStatusSet, Fsbgp4ExtCommIncomingFilterStatusTest, Fsbgp4ExtCommInFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4ExtCommInFilterTableINDEX, 1, 0, 0, "2"},

{{12,Fsbgp4ExtCommInFilterRowStatus}, GetNextIndexFsbgp4ExtCommInFilterTable, Fsbgp4ExtCommInFilterRowStatusGet, Fsbgp4ExtCommInFilterRowStatusSet, Fsbgp4ExtCommInFilterRowStatusTest, Fsbgp4ExtCommInFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4ExtCommInFilterTableINDEX, 1, 0, 1, NULL},

{{12,Fsbgp4ExtCommOutFilterCommVal}, GetNextIndexFsbgp4ExtCommOutFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4ExtCommOutFilterTableINDEX, 1, 0, 0, NULL},

{{12,Fsbgp4ExtCommOutgoingFilterStatus}, GetNextIndexFsbgp4ExtCommOutFilterTable, Fsbgp4ExtCommOutgoingFilterStatusGet, Fsbgp4ExtCommOutgoingFilterStatusSet, Fsbgp4ExtCommOutgoingFilterStatusTest, Fsbgp4ExtCommOutFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4ExtCommOutFilterTableINDEX, 1, 0, 0, NULL},

{{12,Fsbgp4ExtCommOutFilterRowStatus}, GetNextIndexFsbgp4ExtCommOutFilterTable, Fsbgp4ExtCommOutFilterRowStatusGet, Fsbgp4ExtCommOutFilterRowStatusSet, Fsbgp4ExtCommOutFilterRowStatusTest, Fsbgp4ExtCommOutFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4ExtCommOutFilterTableINDEX, 1, 0, 1, NULL},

{{12,Fsbgp4PeerLinkRemAddr}, GetNextIndexFsbgp4PeerLinkBwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4PeerLinkBwTableINDEX, 1, 1, 0, NULL},

{{12,Fsbgp4LinkBandWidth}, GetNextIndexFsbgp4PeerLinkBwTable, Fsbgp4LinkBandWidthGet, Fsbgp4LinkBandWidthSet, Fsbgp4LinkBandWidthTest, Fsbgp4PeerLinkBwTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsbgp4PeerLinkBwTableINDEX, 1, 1, 0, NULL},

{{12,Fsbgp4PeerLinkBwRowStatus}, GetNextIndexFsbgp4PeerLinkBwTable, Fsbgp4PeerLinkBwRowStatusGet, Fsbgp4PeerLinkBwRowStatusSet, Fsbgp4PeerLinkBwRowStatusTest, Fsbgp4PeerLinkBwTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4PeerLinkBwTableINDEX, 1, 1, 1, NULL},

{{12,Fsbgp4ExtCommIpNet}, GetNextIndexFsbgp4ExtCommReceivedRouteExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4ExtCommReceivedRouteExtCommTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4ExtCommIPPrefixLength}, GetNextIndexFsbgp4ExtCommReceivedRouteExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4ExtCommReceivedRouteExtCommTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4ExtCommPeerRemAddr}, GetNextIndexFsbgp4ExtCommReceivedRouteExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4ExtCommReceivedRouteExtCommTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4ReceivedRouteExtCommPath}, GetNextIndexFsbgp4ExtCommReceivedRouteExtCommTable, Fsbgp4ReceivedRouteExtCommPathGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4ExtCommReceivedRouteExtCommTableINDEX, 3, 1, 0, NULL},

{{11,Fsbgp4CapabilitySupportAvailable}, NULL, Fsbgp4CapabilitySupportAvailableGet, Fsbgp4CapabilitySupportAvailableSet, Fsbgp4CapabilitySupportAvailableTest, Fsbgp4CapabilitySupportAvailableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,Fsbgp4MaxCapsPerPeer}, NULL, Fsbgp4MaxCapsPerPeerGet, Fsbgp4MaxCapsPerPeerSet, Fsbgp4MaxCapsPerPeerTest, Fsbgp4MaxCapsPerPeerDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "10"},

{{11,Fsbgp4MaxInstancesPerCap}, NULL, Fsbgp4MaxInstancesPerCapGet, Fsbgp4MaxInstancesPerCapSet, Fsbgp4MaxInstancesPerCapTest, Fsbgp4MaxInstancesPerCapDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{11,Fsbgp4MaxCapDataSize}, NULL, Fsbgp4MaxCapDataSizeGet, Fsbgp4MaxCapDataSizeSet, Fsbgp4MaxCapDataSizeTest, Fsbgp4MaxCapDataSizeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "16"},

{{12,Fsbgp4CapPeerRemoteIpAddr}, GetNextIndexFsbgp4CapSupportedCapsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4CapSupportedCapsTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4SupportedCapabilityCode}, GetNextIndexFsbgp4CapSupportedCapsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4CapSupportedCapsTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4SupportedCapabilityInstance}, GetNextIndexFsbgp4CapSupportedCapsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4CapSupportedCapsTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4SupportedCapabilityLength}, GetNextIndexFsbgp4CapSupportedCapsTable, Fsbgp4SupportedCapabilityLengthGet, Fsbgp4SupportedCapabilityLengthSet, Fsbgp4SupportedCapabilityLengthTest, Fsbgp4CapSupportedCapsTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4CapSupportedCapsTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4SupportedCapabilityValue}, GetNextIndexFsbgp4CapSupportedCapsTable, Fsbgp4SupportedCapabilityValueGet, Fsbgp4SupportedCapabilityValueSet, Fsbgp4SupportedCapabilityValueTest, Fsbgp4CapSupportedCapsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4CapSupportedCapsTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4CapSupportedCapsRowStatus}, GetNextIndexFsbgp4CapSupportedCapsTable, Fsbgp4CapSupportedCapsRowStatusGet, Fsbgp4CapSupportedCapsRowStatusSet, Fsbgp4CapSupportedCapsRowStatusTest, Fsbgp4CapSupportedCapsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4CapSupportedCapsTableINDEX, 3, 1, 1, NULL},

{{12,Fsbgp4PeerRemIpAddr}, GetNextIndexFsbgp4StrictCapabilityMatchTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4StrictCapabilityMatchTableINDEX, 1, 1, 0, NULL},

{{12,Fsbgp4StrictCapabilityMatch}, GetNextIndexFsbgp4StrictCapabilityMatchTable, Fsbgp4StrictCapabilityMatchGet, Fsbgp4StrictCapabilityMatchSet, Fsbgp4StrictCapabilityMatchTest, Fsbgp4StrictCapabilityMatchTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4StrictCapabilityMatchTableINDEX, 1, 1, 0, "2"},

{{12,Fsbgp4PeerIpAddr}, GetNextIndexFsbgp4CapsAnnouncedTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4CapsAnnouncedTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4PeerCapAnnouncedCode}, GetNextIndexFsbgp4CapsAnnouncedTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4CapsAnnouncedTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4PeerCapAnnouncedInstance}, GetNextIndexFsbgp4CapsAnnouncedTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4CapsAnnouncedTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4PeerCapAnnouncedLength}, GetNextIndexFsbgp4CapsAnnouncedTable, Fsbgp4PeerCapAnnouncedLengthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4CapsAnnouncedTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4PeerCapAnnouncedValue}, GetNextIndexFsbgp4CapsAnnouncedTable, Fsbgp4PeerCapAnnouncedValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4CapsAnnouncedTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4PeerRemoteAddress}, GetNextIndexFsbgp4CapReceivedCapsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4CapReceivedCapsTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4PeerCapReceivedCode}, GetNextIndexFsbgp4CapReceivedCapsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4CapReceivedCapsTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4PeerCapReceivedInstance}, GetNextIndexFsbgp4CapReceivedCapsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4CapReceivedCapsTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4PeerCapReceivedLength}, GetNextIndexFsbgp4CapReceivedCapsTable, Fsbgp4PeerCapReceivedLengthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4CapReceivedCapsTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4PeerCapReceivedValue}, GetNextIndexFsbgp4CapReceivedCapsTable, Fsbgp4PeerCapReceivedValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4CapReceivedCapsTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4CapAcceptedPeerRemAddr}, GetNextIndexFsbgp4CapAcceptedCapsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Fsbgp4CapAcceptedCapsTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4PeerCapAcceptedCode}, GetNextIndexFsbgp4CapAcceptedCapsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4CapAcceptedCapsTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4PeerCapAcceptedInstance}, GetNextIndexFsbgp4CapAcceptedCapsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4CapAcceptedCapsTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4PeerCapAcceptedLength}, GetNextIndexFsbgp4CapAcceptedCapsTable, Fsbgp4PeerCapAcceptedLengthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4CapAcceptedCapsTableINDEX, 3, 1, 0, NULL},

{{12,Fsbgp4PeerCapAcceptedValue}, GetNextIndexFsbgp4CapAcceptedCapsTable, Fsbgp4PeerCapAcceptedValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4CapAcceptedCapsTableINDEX, 3, 1, 0, NULL},

{{11,FsbgpAscConfedId}, NULL, FsbgpAscConfedIdGet, FsbgpAscConfedIdSet, FsbgpAscConfedIdTest, FsbgpAscConfedIdDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsbgpAscConfedBestPathCompareMED}, NULL, FsbgpAscConfedBestPathCompareMEDGet, FsbgpAscConfedBestPathCompareMEDSet, FsbgpAscConfedBestPathCompareMEDTest, FsbgpAscConfedBestPathCompareMEDDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{12,FsbgpAscConfedPeerASNo}, GetNextIndexFsbgpAscConfedPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsbgpAscConfedPeerTableINDEX, 1, 0, 0, NULL},

{{12,FsbgpAscConfedPeerStatus}, GetNextIndexFsbgpAscConfedPeerTable, FsbgpAscConfedPeerStatusGet, FsbgpAscConfedPeerStatusSet, FsbgpAscConfedPeerStatusTest, FsbgpAscConfedPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsbgpAscConfedPeerTableINDEX, 1, 0, 0, NULL},

{{10,Fsbgp4RtRefreshAllPeerInboundRequest}, NULL, Fsbgp4RtRefreshAllPeerInboundRequestGet, Fsbgp4RtRefreshAllPeerInboundRequestSet, Fsbgp4RtRefreshAllPeerInboundRequestTest, Fsbgp4RtRefreshAllPeerInboundRequestDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 1, 0, "2"},

{{12,Fsbgp4RtRefreshInboundPeerType}, GetNextIndexFsbgp4RtRefreshInboundTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4RtRefreshInboundTableINDEX, 2, 1, 0, NULL},

{{12,Fsbgp4RtRefreshInboundPeerAddr}, GetNextIndexFsbgp4RtRefreshInboundTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4RtRefreshInboundTableINDEX, 2, 1, 0, NULL},

{{12,Fsbgp4RtRefreshInboundRequest}, GetNextIndexFsbgp4RtRefreshInboundTable, Fsbgp4RtRefreshInboundRequestGet, Fsbgp4RtRefreshInboundRequestSet, Fsbgp4RtRefreshInboundRequestTest, Fsbgp4RtRefreshInboundTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4RtRefreshInboundTableINDEX, 2, 1, 0, NULL},

{{12,Fsbgp4RtRefreshStatisticsPeerType}, GetNextIndexFsbgp4RtRefreshStatisticsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4RtRefreshStatisticsTableINDEX, 2, 1, 0, NULL},

{{12,Fsbgp4RtRefreshStatisticsPeerAddr}, GetNextIndexFsbgp4RtRefreshStatisticsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4RtRefreshStatisticsTableINDEX, 2, 1, 0, NULL},

{{12,Fsbgp4RtRefreshStatisticsRtRefMsgSentCntr}, GetNextIndexFsbgp4RtRefreshStatisticsTable, Fsbgp4RtRefreshStatisticsRtRefMsgSentCntrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsbgp4RtRefreshStatisticsTableINDEX, 2, 1, 0, NULL},

{{12,Fsbgp4RtRefreshStatisticsRtRefMsgTxErrCntr}, GetNextIndexFsbgp4RtRefreshStatisticsTable, Fsbgp4RtRefreshStatisticsRtRefMsgTxErrCntrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsbgp4RtRefreshStatisticsTableINDEX, 2, 1, 0, NULL},

{{12,Fsbgp4RtRefreshStatisticsRtRefMsgRcvdCntr}, GetNextIndexFsbgp4RtRefreshStatisticsTable, Fsbgp4RtRefreshStatisticsRtRefMsgRcvdCntrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsbgp4RtRefreshStatisticsTableINDEX, 2, 1, 0, NULL},

{{12,Fsbgp4RtRefreshStatisticsRtRefMsgInvalidCntr}, GetNextIndexFsbgp4RtRefreshStatisticsTable, Fsbgp4RtRefreshStatisticsRtRefMsgInvalidCntrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsbgp4RtRefreshStatisticsTableINDEX, 2, 1, 0, NULL},

{{12,Fsbgp4TCPMD5AuthPeerType}, GetNextIndexFsbgp4TCPMD5AuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4TCPMD5AuthTableINDEX, 2, 0, 0, NULL},

{{12,Fsbgp4TCPMD5AuthPeerAddr}, GetNextIndexFsbgp4TCPMD5AuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4TCPMD5AuthTableINDEX, 2, 0, 0, NULL},

{{12,Fsbgp4TCPMD5AuthPassword}, GetNextIndexFsbgp4TCPMD5AuthTable, Fsbgp4TCPMD5AuthPasswordGet, Fsbgp4TCPMD5AuthPasswordSet, Fsbgp4TCPMD5AuthPasswordTest, Fsbgp4TCPMD5AuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4TCPMD5AuthTableINDEX, 2, 0, 0, NULL},

{{12,Fsbgp4TCPMD5AuthPwdSet}, GetNextIndexFsbgp4TCPMD5AuthTable, Fsbgp4TCPMD5AuthPwdSetGet, Fsbgp4TCPMD5AuthPwdSetSet, Fsbgp4TCPMD5AuthPwdSetTest, Fsbgp4TCPMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4TCPMD5AuthTableINDEX, 2, 0, 0, "2"},

{{10,Fsbgp4SoftReconfigAllPeerOutboundRequest}, NULL, Fsbgp4SoftReconfigAllPeerOutboundRequestGet, Fsbgp4SoftReconfigAllPeerOutboundRequestSet, Fsbgp4SoftReconfigAllPeerOutboundRequestTest, Fsbgp4SoftReconfigAllPeerOutboundRequestDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 1, 0, "2"},

{{12,Fsbgp4SoftReconfigOutboundPeerType}, GetNextIndexFsbgp4SoftReconfigOutboundTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4SoftReconfigOutboundTableINDEX, 2, 1, 0, NULL},

{{12,Fsbgp4SoftReconfigOutboundPeerAddr}, GetNextIndexFsbgp4SoftReconfigOutboundTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4SoftReconfigOutboundTableINDEX, 2, 1, 0, NULL},

{{12,Fsbgp4SoftReconfigOutboundRequest}, GetNextIndexFsbgp4SoftReconfigOutboundTable, Fsbgp4SoftReconfigOutboundRequestGet, Fsbgp4SoftReconfigOutboundRequestSet, Fsbgp4SoftReconfigOutboundRequestTest, Fsbgp4SoftReconfigOutboundTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4SoftReconfigOutboundTableINDEX, 2, 1, 0, NULL},

{{11,Fsbgp4mpebgpPeerRemoteAddrType}, GetNextIndexFsbgp4MpeBgpPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerIdentifier}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerState}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerAdminStatus}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerAdminStatusGet, Fsbgp4mpebgpPeerAdminStatusSet, Fsbgp4mpebgpPeerAdminStatusTest, Fsbgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerNegotiatedVersion}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerNegotiatedVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerLocalAddr}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerLocalAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerLocalPort}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerLocalPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerRemoteAddr}, GetNextIndexFsbgp4MpeBgpPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerRemotePort}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerRemotePortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerRemoteAs}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerRemoteAsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerInUpdates}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerInUpdatesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerOutUpdates}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerOutUpdatesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerInTotalMessages}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerInTotalMessagesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerOutTotalMessages}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerOutTotalMessagesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerLastError}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerLastErrorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerFsmEstablishedTransitions}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerFsmEstablishedTransitionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerFsmEstablishedTime}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerFsmEstablishedTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerConnectRetryInterval}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerConnectRetryIntervalGet, Fsbgp4mpebgpPeerConnectRetryIntervalSet, Fsbgp4mpebgpPeerConnectRetryIntervalTest, Fsbgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerHoldTime}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerKeepAlive}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerKeepAliveGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerHoldTimeConfigured}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerHoldTimeConfiguredGet, Fsbgp4mpebgpPeerHoldTimeConfiguredSet, Fsbgp4mpebgpPeerHoldTimeConfiguredTest, Fsbgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerKeepAliveConfigured}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerKeepAliveConfiguredGet, Fsbgp4mpebgpPeerKeepAliveConfiguredSet, Fsbgp4mpebgpPeerKeepAliveConfiguredTest, Fsbgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerMinASOriginationInterval}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerMinASOriginationIntervalGet, Fsbgp4mpebgpPeerMinASOriginationIntervalSet, Fsbgp4mpebgpPeerMinASOriginationIntervalTest, Fsbgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerMinRouteAdvertisementInterval}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerMinRouteAdvertisementIntervalGet, Fsbgp4mpebgpPeerMinRouteAdvertisementIntervalSet, Fsbgp4mpebgpPeerMinRouteAdvertisementIntervalTest, Fsbgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerInUpdateElapsedTime}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerInUpdateElapsedTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerEndOfRIBMarkerSentStatus}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerEndOfRIBMarkerSentStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerEndOfRIBMarkerReceivedStatus}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerEndOfRIBMarkerReceivedStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpebgpPeerRestartMode}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerRestartModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpePeerRestartTimeInterval}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpePeerRestartTimeIntervalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpePeerAllowAutomaticStart}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpePeerAllowAutomaticStartGet, Fsbgp4mpePeerAllowAutomaticStartSet, Fsbgp4mpePeerAllowAutomaticStartTest, Fsbgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, "2"},

{{11,Fsbgp4mpePeerAllowAutomaticStop}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpePeerAllowAutomaticStopGet, Fsbgp4mpePeerAllowAutomaticStopSet, Fsbgp4mpePeerAllowAutomaticStopTest, Fsbgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, "2"},

{{11,Fsbgp4mpebgpPeerIdleHoldTimeConfigured}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerIdleHoldTimeConfiguredGet, Fsbgp4mpebgpPeerIdleHoldTimeConfiguredSet, Fsbgp4mpebgpPeerIdleHoldTimeConfiguredTest, Fsbgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, "60"},

{{11,Fsbgp4mpeDampPeerOscillations}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpeDampPeerOscillationsGet, Fsbgp4mpeDampPeerOscillationsSet, Fsbgp4mpeDampPeerOscillationsTest, Fsbgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, "2"},

{{11,Fsbgp4mpePeerDelayOpen}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpePeerDelayOpenGet, Fsbgp4mpePeerDelayOpenSet, Fsbgp4mpePeerDelayOpenTest, Fsbgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, "2"},

{{11,Fsbgp4mpebgpPeerDelayOpenTimeConfigured}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerDelayOpenTimeConfiguredGet, Fsbgp4mpebgpPeerDelayOpenTimeConfiguredSet, Fsbgp4mpebgpPeerDelayOpenTimeConfiguredTest, Fsbgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, "0"},

{{11,Fsbgp4mpePeerPrefixUpperLimit}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpePeerPrefixUpperLimitGet, Fsbgp4mpePeerPrefixUpperLimitSet, Fsbgp4mpePeerPrefixUpperLimitTest, Fsbgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, "5000"},

{{11,Fsbgp4mpePeerTcpConnectRetryCnt}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpePeerTcpConnectRetryCntGet, Fsbgp4mpePeerTcpConnectRetryCntSet, Fsbgp4mpePeerTcpConnectRetryCntTest, Fsbgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, "5"},

{{11,Fsbgp4mpePeerTcpCurrentConnectRetryCnt}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpePeerTcpCurrentConnectRetryCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpeIsPeerDamped}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpeIsPeerDampedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, "2"},

{{11,Fsbgp4mpePeerSessionAuthStatus}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpePeerSessionAuthStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpePeerTCPAOKeyIdInUse}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpePeerTCPAOKeyIdInUseGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpePeerTCPAOAuthNoMKTDiscard}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpePeerTCPAOAuthNoMKTDiscardGet, Fsbgp4mpePeerTCPAOAuthNoMKTDiscardSet, Fsbgp4mpePeerTCPAOAuthNoMKTDiscardTest, Fsbgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, "1"},

{{11,Fsbgp4mpePeerTCPAOAuthICMPAccept}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpePeerTCPAOAuthICMPAcceptGet, Fsbgp4mpePeerTCPAOAuthICMPAcceptSet, Fsbgp4mpePeerTCPAOAuthICMPAcceptTest, Fsbgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, "2"},

{{11,Fsbgp4mpePeerIpPrefixNameIn}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpePeerIpPrefixNameInGet, Fsbgp4mpePeerIpPrefixNameInSet, Fsbgp4mpePeerIpPrefixNameInTest, Fsbgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpePeerIpPrefixNameOut}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpePeerIpPrefixNameOutGet, Fsbgp4mpePeerIpPrefixNameOutSet, Fsbgp4mpePeerIpPrefixNameOutTest, Fsbgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpePeerBfdStatus}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpePeerBfdStatusGet, Fsbgp4mpePeerBfdStatusSet, Fsbgp4mpePeerBfdStatusTest, Fsbgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, "2"},

{{11,Fsbgp4mpebgpPeerHoldAdvtRoutes}, GetNextIndexFsbgp4MpeBgpPeerTable, Fsbgp4mpebgpPeerHoldAdvtRoutesGet, Fsbgp4mpebgpPeerHoldAdvtRoutesSet, Fsbgp4mpebgpPeerHoldAdvtRoutesTest, Fsbgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeBgpPeerTableINDEX, 2, 0, 0, "2"},

{{11,Fsbgp4mpebgp4PathAttrRouteAfi}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrRouteSafi}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrPeerType}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrPeer}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrIpAddrPrefixLen}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrIpAddrPrefix}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrOrigin}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, Fsbgp4mpebgp4PathAttrOriginGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrASPathSegment}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, Fsbgp4mpebgp4PathAttrASPathSegmentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrNextHop}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, Fsbgp4mpebgp4PathAttrNextHopGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrMultiExitDisc}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, Fsbgp4mpebgp4PathAttrMultiExitDiscGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrLocalPref}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, Fsbgp4mpebgp4PathAttrLocalPrefGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrAtomicAggregate}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, Fsbgp4mpebgp4PathAttrAtomicAggregateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrAggregatorAS}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, Fsbgp4mpebgp4PathAttrAggregatorASGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrAggregatorAddr}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, Fsbgp4mpebgp4PathAttrAggregatorAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrCalcLocalPref}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, Fsbgp4mpebgp4PathAttrCalcLocalPrefGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrBest}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, Fsbgp4mpebgp4PathAttrBestGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrCommunity}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, Fsbgp4mpebgp4PathAttrCommunityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrOriginatorId}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, Fsbgp4mpebgp4PathAttrOriginatorIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrClusterList}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, Fsbgp4mpebgp4PathAttrClusterListGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrExtCommunity}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, Fsbgp4mpebgp4PathAttrExtCommunityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrUnknown}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, Fsbgp4mpebgp4PathAttrUnknownGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrLabel}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, Fsbgp4mpebgp4PathAttrLabelGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrAS4PathSegment}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, Fsbgp4mpebgp4PathAttrAS4PathSegmentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpebgp4PathAttrAggregatorAS4}, GetNextIndexFsbgp4MpeBgp4PathAttrTable, Fsbgp4mpebgp4PathAttrAggregatorAS4Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fsbgp4MpeBgp4PathAttrTableINDEX, 6, 0, 0, NULL},

{{11,Fsbgp4mpePeerExtPeerType}, GetNextIndexFsbgp4MpePeerExtTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpePeerExtPeerRemoteAddr}, GetNextIndexFsbgp4MpePeerExtTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpePeerExtConfigurePeer}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtConfigurePeerGet, Fsbgp4mpePeerExtConfigurePeerSet, Fsbgp4mpePeerExtConfigurePeerTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpePeerExtPeerRemoteAs}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtPeerRemoteAsGet, Fsbgp4mpePeerExtPeerRemoteAsSet, Fsbgp4mpePeerExtPeerRemoteAsTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpePeerExtEBGPMultiHop}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtEBGPMultiHopGet, Fsbgp4mpePeerExtEBGPMultiHopSet, Fsbgp4mpePeerExtEBGPMultiHopTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, "2"},

{{11,Fsbgp4mpePeerExtEBGPHopLimit}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtEBGPHopLimitGet, Fsbgp4mpePeerExtEBGPHopLimitSet, Fsbgp4mpePeerExtEBGPHopLimitTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, "1"},

{{11,Fsbgp4mpePeerExtNextHopSelf}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtNextHopSelfGet, Fsbgp4mpePeerExtNextHopSelfSet, Fsbgp4mpePeerExtNextHopSelfTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, "1"},

{{11,Fsbgp4mpePeerExtRflClient}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtRflClientGet, Fsbgp4mpePeerExtRflClientSet, Fsbgp4mpePeerExtRflClientTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, "1"},

{{11,Fsbgp4mpePeerExtTcpSendBufSize}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtTcpSendBufSizeGet, Fsbgp4mpePeerExtTcpSendBufSizeSet, Fsbgp4mpePeerExtTcpSendBufSizeTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, "65536"},

{{11,Fsbgp4mpePeerExtTcpRcvBufSize}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtTcpRcvBufSizeGet, Fsbgp4mpePeerExtTcpRcvBufSizeSet, Fsbgp4mpePeerExtTcpRcvBufSizeTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, "65536"},

{{11,Fsbgp4mpePeerExtLclAddress}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtLclAddressGet, Fsbgp4mpePeerExtLclAddressSet, Fsbgp4mpePeerExtLclAddressTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpePeerExtNetworkAddress}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtNetworkAddressGet, Fsbgp4mpePeerExtNetworkAddressSet, Fsbgp4mpePeerExtNetworkAddressTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpePeerExtGateway}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtGatewayGet, Fsbgp4mpePeerExtGatewaySet, Fsbgp4mpePeerExtGatewayTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpePeerExtCommSendStatus}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtCommSendStatusGet, Fsbgp4mpePeerExtCommSendStatusSet, Fsbgp4mpePeerExtCommSendStatusTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, "2"},

{{11,Fsbgp4mpePeerExtECommSendStatus}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtECommSendStatusGet, Fsbgp4mpePeerExtECommSendStatusSet, Fsbgp4mpePeerExtECommSendStatusTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, "2"},

{{11,Fsbgp4mpePeerExtPassive}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtPassiveGet, Fsbgp4mpePeerExtPassiveSet, Fsbgp4mpePeerExtPassiveTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpePeerExtDefaultOriginate}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtDefaultOriginateGet, Fsbgp4mpePeerExtDefaultOriginateSet, Fsbgp4mpePeerExtDefaultOriginateTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, "2"},

{{11,Fsbgp4mpePeerExtActivateMPCapability}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtActivateMPCapabilityGet, Fsbgp4mpePeerExtActivateMPCapabilitySet, Fsbgp4mpePeerExtActivateMPCapabilityTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpePeerExtDeactivateMPCapability}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtDeactivateMPCapabilityGet, Fsbgp4mpePeerExtDeactivateMPCapabilitySet, Fsbgp4mpePeerExtDeactivateMPCapabilityTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpePeerExtMplsVpnVrfAssociated}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtMplsVpnVrfAssociatedGet, Fsbgp4mpePeerExtMplsVpnVrfAssociatedSet, Fsbgp4mpePeerExtMplsVpnVrfAssociatedTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpePeerExtMplsVpnCERouteTargetAdvt}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtMplsVpnCERouteTargetAdvtGet, Fsbgp4mpePeerExtMplsVpnCERouteTargetAdvtSet, Fsbgp4mpePeerExtMplsVpnCERouteTargetAdvtTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, "2"},

{{11,Fsbgp4mpePeerExtMplsVpnCESiteOfOrigin}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtMplsVpnCESiteOfOriginGet, Fsbgp4mpePeerExtMplsVpnCESiteOfOriginSet, Fsbgp4mpePeerExtMplsVpnCESiteOfOriginTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpePeerExtOverrideCapability}, GetNextIndexFsbgp4MpePeerExtTable, Fsbgp4mpePeerExtOverrideCapabilityGet, Fsbgp4mpePeerExtOverrideCapabilitySet, Fsbgp4mpePeerExtOverrideCapabilityTest, Fsbgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpePeerExtTableINDEX, 2, 0, 0, "2"},    

{{11,Fsbgp4mpeMEDIndex}, GetNextIndexFsbgp4MpeMEDTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4MpeMEDTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeMEDAdminStatus}, GetNextIndexFsbgp4MpeMEDTable, Fsbgp4mpeMEDAdminStatusGet, Fsbgp4mpeMEDAdminStatusSet, Fsbgp4mpeMEDAdminStatusTest, Fsbgp4MpeMEDTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeMEDTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeMEDRemoteAS}, GetNextIndexFsbgp4MpeMEDTable, Fsbgp4mpeMEDRemoteASGet, Fsbgp4mpeMEDRemoteASSet, Fsbgp4mpeMEDRemoteASTest, Fsbgp4MpeMEDTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsbgp4MpeMEDTableINDEX, 1, 0, 0, "0"},

{{11,Fsbgp4mpeMEDIPAddrAfi}, GetNextIndexFsbgp4MpeMEDTable, Fsbgp4mpeMEDIPAddrAfiGet, Fsbgp4mpeMEDIPAddrAfiSet, Fsbgp4mpeMEDIPAddrAfiTest, Fsbgp4MpeMEDTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeMEDTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeMEDIPAddrSafi}, GetNextIndexFsbgp4MpeMEDTable, Fsbgp4mpeMEDIPAddrSafiGet, Fsbgp4mpeMEDIPAddrSafiSet, Fsbgp4mpeMEDIPAddrSafiTest, Fsbgp4MpeMEDTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeMEDTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeMEDIPAddrPrefix}, GetNextIndexFsbgp4MpeMEDTable, Fsbgp4mpeMEDIPAddrPrefixGet, Fsbgp4mpeMEDIPAddrPrefixSet, Fsbgp4mpeMEDIPAddrPrefixTest, Fsbgp4MpeMEDTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpeMEDTableINDEX, 1, 0, 0, "0"},

{{11,Fsbgp4mpeMEDIPAddrPrefixLen}, GetNextIndexFsbgp4MpeMEDTable, Fsbgp4mpeMEDIPAddrPrefixLenGet, Fsbgp4mpeMEDIPAddrPrefixLenSet, Fsbgp4mpeMEDIPAddrPrefixLenTest, Fsbgp4MpeMEDTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4MpeMEDTableINDEX, 1, 0, 0, "0"},

{{11,Fsbgp4mpeMEDIntermediateAS}, GetNextIndexFsbgp4MpeMEDTable, Fsbgp4mpeMEDIntermediateASGet, Fsbgp4mpeMEDIntermediateASSet, Fsbgp4mpeMEDIntermediateASTest, Fsbgp4MpeMEDTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpeMEDTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeMEDDirection}, GetNextIndexFsbgp4MpeMEDTable, Fsbgp4mpeMEDDirectionGet, Fsbgp4mpeMEDDirectionSet, Fsbgp4mpeMEDDirectionTest, Fsbgp4MpeMEDTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeMEDTableINDEX, 1, 0, 0, "1"},

{{11,Fsbgp4mpeMEDValue}, GetNextIndexFsbgp4MpeMEDTable, Fsbgp4mpeMEDValueGet, Fsbgp4mpeMEDValueSet, Fsbgp4mpeMEDValueTest, Fsbgp4MpeMEDTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsbgp4MpeMEDTableINDEX, 1, 0, 0, "0"},

{{11,Fsbgp4mpeMEDPreference}, GetNextIndexFsbgp4MpeMEDTable, Fsbgp4mpeMEDPreferenceGet, Fsbgp4mpeMEDPreferenceSet, Fsbgp4mpeMEDPreferenceTest, Fsbgp4MpeMEDTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeMEDTableINDEX, 1, 0, 0, "1"},

{{11,Fsbgp4mpeMEDVrfName}, GetNextIndexFsbgp4MpeMEDTable, Fsbgp4mpeMEDVrfNameGet, Fsbgp4mpeMEDVrfNameSet, Fsbgp4mpeMEDVrfNameTest, Fsbgp4MpeMEDTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpeMEDTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeLocalPrefIndex}, GetNextIndexFsbgp4MpeLocalPrefTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4MpeLocalPrefTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeLocalPrefAdminStatus}, GetNextIndexFsbgp4MpeLocalPrefTable, Fsbgp4mpeLocalPrefAdminStatusGet, Fsbgp4mpeLocalPrefAdminStatusSet, Fsbgp4mpeLocalPrefAdminStatusTest, Fsbgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeLocalPrefTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeLocalPrefRemoteAS}, GetNextIndexFsbgp4MpeLocalPrefTable, Fsbgp4mpeLocalPrefRemoteASGet, Fsbgp4mpeLocalPrefRemoteASSet, Fsbgp4mpeLocalPrefRemoteASTest, Fsbgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsbgp4MpeLocalPrefTableINDEX, 1, 0, 0, "0"},

{{11,Fsbgp4mpeLocalPrefIPAddrAfi}, GetNextIndexFsbgp4MpeLocalPrefTable, Fsbgp4mpeLocalPrefIPAddrAfiGet, Fsbgp4mpeLocalPrefIPAddrAfiSet, Fsbgp4mpeLocalPrefIPAddrAfiTest, Fsbgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeLocalPrefTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeLocalPrefIPAddrSafi}, GetNextIndexFsbgp4MpeLocalPrefTable, Fsbgp4mpeLocalPrefIPAddrSafiGet, Fsbgp4mpeLocalPrefIPAddrSafiSet, Fsbgp4mpeLocalPrefIPAddrSafiTest, Fsbgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeLocalPrefTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeLocalPrefIPAddrPrefix}, GetNextIndexFsbgp4MpeLocalPrefTable, Fsbgp4mpeLocalPrefIPAddrPrefixGet, Fsbgp4mpeLocalPrefIPAddrPrefixSet, Fsbgp4mpeLocalPrefIPAddrPrefixTest, Fsbgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpeLocalPrefTableINDEX, 1, 0, 0, "0"},

{{11,Fsbgp4mpeLocalPrefIPAddrPrefixLen}, GetNextIndexFsbgp4MpeLocalPrefTable, Fsbgp4mpeLocalPrefIPAddrPrefixLenGet, Fsbgp4mpeLocalPrefIPAddrPrefixLenSet, Fsbgp4mpeLocalPrefIPAddrPrefixLenTest, Fsbgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4MpeLocalPrefTableINDEX, 1, 0, 0, "0"},

{{11,Fsbgp4mpeLocalPrefIntermediateAS}, GetNextIndexFsbgp4MpeLocalPrefTable, Fsbgp4mpeLocalPrefIntermediateASGet, Fsbgp4mpeLocalPrefIntermediateASSet, Fsbgp4mpeLocalPrefIntermediateASTest, Fsbgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpeLocalPrefTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeLocalPrefDirection}, GetNextIndexFsbgp4MpeLocalPrefTable, Fsbgp4mpeLocalPrefDirectionGet, Fsbgp4mpeLocalPrefDirectionSet, Fsbgp4mpeLocalPrefDirectionTest, Fsbgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeLocalPrefTableINDEX, 1, 0, 0, "1"},

{{11,Fsbgp4mpeLocalPrefValue}, GetNextIndexFsbgp4MpeLocalPrefTable, Fsbgp4mpeLocalPrefValueGet, Fsbgp4mpeLocalPrefValueSet, Fsbgp4mpeLocalPrefValueTest, Fsbgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsbgp4MpeLocalPrefTableINDEX, 1, 0, 0, "100"},

{{11,Fsbgp4mpeLocalPrefPreference}, GetNextIndexFsbgp4MpeLocalPrefTable, Fsbgp4mpeLocalPrefPreferenceGet, Fsbgp4mpeLocalPrefPreferenceSet, Fsbgp4mpeLocalPrefPreferenceTest, Fsbgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeLocalPrefTableINDEX, 1, 0, 0, "1"},

{{11,Fsbgp4mpeLocalPrefVrfName}, GetNextIndexFsbgp4MpeLocalPrefTable, Fsbgp4mpeLocalPrefVrfNameGet, Fsbgp4mpeLocalPrefVrfNameSet, Fsbgp4mpeLocalPrefVrfNameTest, Fsbgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpeLocalPrefTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeUpdateFilterIndex}, GetNextIndexFsbgp4MpeUpdateFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4MpeUpdateFilterTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeUpdateFilterAdminStatus}, GetNextIndexFsbgp4MpeUpdateFilterTable, Fsbgp4mpeUpdateFilterAdminStatusGet, Fsbgp4mpeUpdateFilterAdminStatusSet, Fsbgp4mpeUpdateFilterAdminStatusTest, Fsbgp4MpeUpdateFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeUpdateFilterTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeUpdateFilterRemoteAS}, GetNextIndexFsbgp4MpeUpdateFilterTable, Fsbgp4mpeUpdateFilterRemoteASGet, Fsbgp4mpeUpdateFilterRemoteASSet, Fsbgp4mpeUpdateFilterRemoteASTest, Fsbgp4MpeUpdateFilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsbgp4MpeUpdateFilterTableINDEX, 1, 0, 0, "0"},

{{11,Fsbgp4mpeUpdateFilterIPAddrAfi}, GetNextIndexFsbgp4MpeUpdateFilterTable, Fsbgp4mpeUpdateFilterIPAddrAfiGet, Fsbgp4mpeUpdateFilterIPAddrAfiSet, Fsbgp4mpeUpdateFilterIPAddrAfiTest, Fsbgp4MpeUpdateFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeUpdateFilterTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeUpdateFilterIPAddrSafi}, GetNextIndexFsbgp4MpeUpdateFilterTable, Fsbgp4mpeUpdateFilterIPAddrSafiGet, Fsbgp4mpeUpdateFilterIPAddrSafiSet, Fsbgp4mpeUpdateFilterIPAddrSafiTest, Fsbgp4MpeUpdateFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeUpdateFilterTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeUpdateFilterIPAddrPrefix}, GetNextIndexFsbgp4MpeUpdateFilterTable, Fsbgp4mpeUpdateFilterIPAddrPrefixGet, Fsbgp4mpeUpdateFilterIPAddrPrefixSet, Fsbgp4mpeUpdateFilterIPAddrPrefixTest, Fsbgp4MpeUpdateFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpeUpdateFilterTableINDEX, 1, 0, 0, "0"},

{{11,Fsbgp4mpeUpdateFilterIPAddrPrefixLen}, GetNextIndexFsbgp4MpeUpdateFilterTable, Fsbgp4mpeUpdateFilterIPAddrPrefixLenGet, Fsbgp4mpeUpdateFilterIPAddrPrefixLenSet, Fsbgp4mpeUpdateFilterIPAddrPrefixLenTest, Fsbgp4MpeUpdateFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4MpeUpdateFilterTableINDEX, 1, 0, 0, "0"},

{{11,Fsbgp4mpeUpdateFilterIntermediateAS}, GetNextIndexFsbgp4MpeUpdateFilterTable, Fsbgp4mpeUpdateFilterIntermediateASGet, Fsbgp4mpeUpdateFilterIntermediateASSet, Fsbgp4mpeUpdateFilterIntermediateASTest, Fsbgp4MpeUpdateFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpeUpdateFilterTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeUpdateFilterDirection}, GetNextIndexFsbgp4MpeUpdateFilterTable, Fsbgp4mpeUpdateFilterDirectionGet, Fsbgp4mpeUpdateFilterDirectionSet, Fsbgp4mpeUpdateFilterDirectionTest, Fsbgp4MpeUpdateFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeUpdateFilterTableINDEX, 1, 0, 0, "1"},

{{11,Fsbgp4mpeUpdateFilterAction}, GetNextIndexFsbgp4MpeUpdateFilterTable, Fsbgp4mpeUpdateFilterActionGet, Fsbgp4mpeUpdateFilterActionSet, Fsbgp4mpeUpdateFilterActionTest, Fsbgp4MpeUpdateFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeUpdateFilterTableINDEX, 1, 0, 0, "2"},

{{11,Fsbgp4mpeUpdateFilterVrfName}, GetNextIndexFsbgp4MpeUpdateFilterTable, Fsbgp4mpeUpdateFilterVrfNameGet, Fsbgp4mpeUpdateFilterVrfNameSet, Fsbgp4mpeUpdateFilterVrfNameTest, Fsbgp4MpeUpdateFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpeUpdateFilterTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeAggregateIndex}, GetNextIndexFsbgp4MpeAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeAggregateAdminStatus}, GetNextIndexFsbgp4MpeAggregateTable, Fsbgp4mpeAggregateAdminStatusGet, Fsbgp4mpeAggregateAdminStatusSet, Fsbgp4mpeAggregateAdminStatusTest, Fsbgp4MpeAggregateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeAggregateIPAddrAfi}, GetNextIndexFsbgp4MpeAggregateTable, Fsbgp4mpeAggregateIPAddrAfiGet, Fsbgp4mpeAggregateIPAddrAfiSet, Fsbgp4mpeAggregateIPAddrAfiTest, Fsbgp4MpeAggregateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeAggregateIPAddrSafi}, GetNextIndexFsbgp4MpeAggregateTable, Fsbgp4mpeAggregateIPAddrSafiGet, Fsbgp4mpeAggregateIPAddrSafiSet, Fsbgp4mpeAggregateIPAddrSafiTest, Fsbgp4MpeAggregateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeAggregateIPAddrPrefix}, GetNextIndexFsbgp4MpeAggregateTable, Fsbgp4mpeAggregateIPAddrPrefixGet, Fsbgp4mpeAggregateIPAddrPrefixSet, Fsbgp4mpeAggregateIPAddrPrefixTest, Fsbgp4MpeAggregateTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeAggregateIPAddrPrefixLen}, GetNextIndexFsbgp4MpeAggregateTable, Fsbgp4mpeAggregateIPAddrPrefixLenGet, Fsbgp4mpeAggregateIPAddrPrefixLenSet, Fsbgp4mpeAggregateIPAddrPrefixLenTest, Fsbgp4MpeAggregateTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeAggregateAdvertise}, GetNextIndexFsbgp4MpeAggregateTable, Fsbgp4mpeAggregateAdvertiseGet, Fsbgp4mpeAggregateAdvertiseSet, Fsbgp4mpeAggregateAdvertiseTest, Fsbgp4MpeAggregateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeAggregateVrfName}, GetNextIndexFsbgp4MpeAggregateTable, Fsbgp4mpeAggregateVrfNameGet, Fsbgp4mpeAggregateVrfNameSet, Fsbgp4mpeAggregateVrfNameTest, Fsbgp4MpeAggregateTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeAggregateAsSet}, GetNextIndexFsbgp4MpeAggregateTable, Fsbgp4mpeAggregateAsSetGet, Fsbgp4mpeAggregateAsSetSet, Fsbgp4mpeAggregateAsSetTest, Fsbgp4MpeAggregateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeAggregateAdvertiseRouteMapName}, GetNextIndexFsbgp4MpeAggregateTable, Fsbgp4mpeAggregateAdvertiseRouteMapNameGet, Fsbgp4mpeAggregateAdvertiseRouteMapNameSet, Fsbgp4mpeAggregateAdvertiseRouteMapNameTest, Fsbgp4MpeAggregateTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeAggregateSuppressRouteMapName}, GetNextIndexFsbgp4MpeAggregateTable, Fsbgp4mpeAggregateSuppressRouteMapNameGet, Fsbgp4mpeAggregateSuppressRouteMapNameSet, Fsbgp4mpeAggregateSuppressRouteMapNameTest, Fsbgp4MpeAggregateTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeAggregateAttributeRouteMapName}, GetNextIndexFsbgp4MpeAggregateTable, Fsbgp4mpeAggregateAttributeRouteMapNameGet, Fsbgp4mpeAggregateAttributeRouteMapNameSet, Fsbgp4mpeAggregateAttributeRouteMapNameTest, Fsbgp4MpeAggregateTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{11,Fsbgp4mpeImportRoutePrefixAfi}, GetNextIndexFsbgp4MpeImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeImportRouteTableINDEX, 9, 0, 0, NULL},

{{11,Fsbgp4mpeImportRoutePrefixSafi}, GetNextIndexFsbgp4MpeImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeImportRouteTableINDEX, 9, 0, 0, NULL},

{{11,Fsbgp4mpeImportRoutePrefix}, GetNextIndexFsbgp4MpeImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeImportRouteTableINDEX, 9, 0, 0, NULL},

{{11,Fsbgp4mpeImportRoutePrefixLen}, GetNextIndexFsbgp4MpeImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4MpeImportRouteTableINDEX, 9, 0, 0, NULL},

{{11,Fsbgp4mpeImportRouteProtocol}, GetNextIndexFsbgp4MpeImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4MpeImportRouteTableINDEX, 9, 0, 0, NULL},

{{11,Fsbgp4mpeImportRouteNextHop}, GetNextIndexFsbgp4MpeImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeImportRouteTableINDEX, 9, 0, 0, NULL},

{{11,Fsbgp4mpeImportRouteIfIndex}, GetNextIndexFsbgp4MpeImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4MpeImportRouteTableINDEX, 9, 0, 0, NULL},

{{11,Fsbgp4mpeImportRouteMetric}, GetNextIndexFsbgp4MpeImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4MpeImportRouteTableINDEX, 9, 0, 0, NULL},

{{11,Fsbgp4mpeImportRouteVrf}, GetNextIndexFsbgp4MpeImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeImportRouteTableINDEX, 9, 0, 0, NULL},

{{11,Fsbgp4mpeImportRouteAction}, GetNextIndexFsbgp4MpeImportRouteTable, Fsbgp4mpeImportRouteActionGet, Fsbgp4mpeImportRouteActionSet, Fsbgp4mpeImportRouteActionTest, Fsbgp4MpeImportRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeImportRouteTableINDEX, 9, 0, 0, NULL},

{{11,Fsbgp4mpePeerType}, GetNextIndexFsbgp4MpeFsmTransitionHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeFsmTransitionHistTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpePeer}, GetNextIndexFsbgp4MpeFsmTransitionHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeFsmTransitionHistTableINDEX, 2, 0, 0, NULL},

{{11,Fsbgp4mpeFsmTransitionHist}, GetNextIndexFsbgp4MpeFsmTransitionHistTable, Fsbgp4mpeFsmTransitionHistGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsbgp4MpeFsmTransitionHistTableINDEX, 2, 0, 0, NULL},

{{12,Fsbgp4mpePathAttrAddrPrefixAfi}, GetNextIndexFsbgp4MpeRfdRtDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeRfdRtDampHistTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpePathAttrAddrPrefixSafi}, GetNextIndexFsbgp4MpeRfdRtDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeRfdRtDampHistTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpePathAttrAddrPrefix}, GetNextIndexFsbgp4MpeRfdRtDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeRfdRtDampHistTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpePathAttrAddrPrefixLen}, GetNextIndexFsbgp4MpeRfdRtDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4MpeRfdRtDampHistTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpePathAttrPeerType}, GetNextIndexFsbgp4MpeRfdRtDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeRfdRtDampHistTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpePathAttrPeer}, GetNextIndexFsbgp4MpeRfdRtDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeRfdRtDampHistTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpeRfdRtFom}, GetNextIndexFsbgp4MpeRfdRtDampHistTable, Fsbgp4mpeRfdRtFomGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeRfdRtDampHistTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpeRfdRtLastUpdtTime}, GetNextIndexFsbgp4MpeRfdRtDampHistTable, Fsbgp4mpeRfdRtLastUpdtTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeRfdRtDampHistTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpeRfdRtState}, GetNextIndexFsbgp4MpeRfdRtDampHistTable, Fsbgp4mpeRfdRtStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeRfdRtDampHistTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpeRfdRtStatus}, GetNextIndexFsbgp4MpeRfdRtDampHistTable, Fsbgp4mpeRfdRtStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeRfdRtDampHistTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpeRfdRtFlapCount}, GetNextIndexFsbgp4MpeRfdRtDampHistTable, Fsbgp4mpeRfdRtFlapCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeRfdRtDampHistTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpeRfdRtFlapTime}, GetNextIndexFsbgp4MpeRfdRtDampHistTable, Fsbgp4mpeRfdRtFlapTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeRfdRtDampHistTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpeRfdRtReuseTime}, GetNextIndexFsbgp4MpeRfdRtDampHistTable, Fsbgp4mpeRfdRtReuseTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeRfdRtDampHistTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpePeerRemoteIpAddrType}, GetNextIndexFsbgp4MpeRfdPeerDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeRfdPeerDampHistTableINDEX, 2, 0, 0, NULL},

{{12,Fsbgp4mpePeerRemoteIpAddr}, GetNextIndexFsbgp4MpeRfdPeerDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeRfdPeerDampHistTableINDEX, 2, 0, 0, NULL},

{{12,Fsbgp4mpeRfdPeerFom}, GetNextIndexFsbgp4MpeRfdPeerDampHistTable, Fsbgp4mpeRfdPeerFomGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeRfdPeerDampHistTableINDEX, 2, 0, 0, NULL},

{{12,Fsbgp4mpeRfdPeerLastUpdtTime}, GetNextIndexFsbgp4MpeRfdPeerDampHistTable, Fsbgp4mpeRfdPeerLastUpdtTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeRfdPeerDampHistTableINDEX, 2, 0, 0, NULL},

{{12,Fsbgp4mpeRfdPeerState}, GetNextIndexFsbgp4MpeRfdPeerDampHistTable, Fsbgp4mpeRfdPeerStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeRfdPeerDampHistTableINDEX, 2, 0, 0, NULL},

{{12,Fsbgp4mpeRfdPeerStatus}, GetNextIndexFsbgp4MpeRfdPeerDampHistTable, Fsbgp4mpeRfdPeerStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeRfdPeerDampHistTableINDEX, 2, 0, 0, NULL},

{{12,Fsbgp4mpeRtAfi}, GetNextIndexFsbgp4MpeRfdRtsReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeRfdRtsReuseListTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpeRtSafi}, GetNextIndexFsbgp4MpeRfdRtsReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeRfdRtsReuseListTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpeRtIPPrefix}, GetNextIndexFsbgp4MpeRfdRtsReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeRfdRtsReuseListTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpeRtIPPrefixLen}, GetNextIndexFsbgp4MpeRfdRtsReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4MpeRfdRtsReuseListTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpeRfdRtsReusePeerType}, GetNextIndexFsbgp4MpeRfdRtsReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeRfdRtsReuseListTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpePeerRemAddress}, GetNextIndexFsbgp4MpeRfdRtsReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeRfdRtsReuseListTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpeRfdRtReuseListRtFom}, GetNextIndexFsbgp4MpeRfdRtsReuseListTable, Fsbgp4mpeRfdRtReuseListRtFomGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeRfdRtsReuseListTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpeRfdRtReuseListRtLastUpdtTime}, GetNextIndexFsbgp4MpeRfdRtsReuseListTable, Fsbgp4mpeRfdRtReuseListRtLastUpdtTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeRfdRtsReuseListTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpeRfdRtReuseListRtState}, GetNextIndexFsbgp4MpeRfdRtsReuseListTable, Fsbgp4mpeRfdRtReuseListRtStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeRfdRtsReuseListTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpeRfdRtReuseListRtStatus}, GetNextIndexFsbgp4MpeRfdRtsReuseListTable, Fsbgp4mpeRfdRtReuseListRtStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeRfdRtsReuseListTableINDEX, 6, 0, 0, NULL},

{{12,Fsbgp4mpeRfdPeerRemIpAddrType}, GetNextIndexFsbgp4MpeRfdPeerReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeRfdPeerReuseListTableINDEX, 2, 0, 0, NULL},

{{12,Fsbgp4mpeRfdPeerRemIpAddr}, GetNextIndexFsbgp4MpeRfdPeerReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeRfdPeerReuseListTableINDEX, 2, 0, 0, NULL},

{{12,Fsbgp4mpeRfdPeerReuseListPeerFom}, GetNextIndexFsbgp4MpeRfdPeerReuseListTable, Fsbgp4mpeRfdPeerReuseListPeerFomGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeRfdPeerReuseListTableINDEX, 2, 0, 0, NULL},

{{12,Fsbgp4mpeRfdPeerReuseListLastUpdtTime}, GetNextIndexFsbgp4MpeRfdPeerReuseListTable, Fsbgp4mpeRfdPeerReuseListLastUpdtTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MpeRfdPeerReuseListTableINDEX, 2, 0, 0, NULL},

{{12,Fsbgp4mpeRfdPeerReuseListPeerState}, GetNextIndexFsbgp4MpeRfdPeerReuseListTable, Fsbgp4mpeRfdPeerReuseListPeerStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeRfdPeerReuseListTableINDEX, 2, 0, 0, NULL},

{{12,Fsbgp4mpeRfdPeerReuseListPeerStatus}, GetNextIndexFsbgp4MpeRfdPeerReuseListTable, Fsbgp4mpeRfdPeerReuseListPeerStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeRfdPeerReuseListTableINDEX, 2, 0, 0, NULL},

{{12,Fsbgp4mpeAddCommRtAfi}, GetNextIndexFsbgp4MpeCommRouteAddCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeCommRouteAddCommTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeAddCommRtSafi}, GetNextIndexFsbgp4MpeCommRouteAddCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeCommRouteAddCommTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeAddCommIpNetwork}, GetNextIndexFsbgp4MpeCommRouteAddCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeCommRouteAddCommTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeAddCommIpPrefixLen}, GetNextIndexFsbgp4MpeCommRouteAddCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4MpeCommRouteAddCommTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeAddCommVal}, GetNextIndexFsbgp4MpeCommRouteAddCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Fsbgp4MpeCommRouteAddCommTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeAddCommRowStatus}, GetNextIndexFsbgp4MpeCommRouteAddCommTable, Fsbgp4mpeAddCommRowStatusGet, Fsbgp4mpeAddCommRowStatusSet, Fsbgp4mpeAddCommRowStatusTest, Fsbgp4MpeCommRouteAddCommTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeCommRouteAddCommTableINDEX, 5, 0, 1, NULL},

{{12,Fsbgp4mpeDeleteCommRtAfi}, GetNextIndexFsbgp4MpeCommRouteDeleteCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeCommRouteDeleteCommTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeDeleteCommRtSafi}, GetNextIndexFsbgp4MpeCommRouteDeleteCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeCommRouteDeleteCommTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeDeleteCommIpNetwork}, GetNextIndexFsbgp4MpeCommRouteDeleteCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeCommRouteDeleteCommTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeDeleteCommIpPrefixLen}, GetNextIndexFsbgp4MpeCommRouteDeleteCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4MpeCommRouteDeleteCommTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeDeleteCommVal}, GetNextIndexFsbgp4MpeCommRouteDeleteCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Fsbgp4MpeCommRouteDeleteCommTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeDeleteCommRowStatus}, GetNextIndexFsbgp4MpeCommRouteDeleteCommTable, Fsbgp4mpeDeleteCommRowStatusGet, Fsbgp4mpeDeleteCommRowStatusSet, Fsbgp4mpeDeleteCommRowStatusTest, Fsbgp4MpeCommRouteDeleteCommTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeCommRouteDeleteCommTableINDEX, 5, 0, 1, NULL},

{{12,Fsbgp4mpeCommSetStatusAfi}, GetNextIndexFsbgp4MpeCommRouteCommSetStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeCommRouteCommSetStatusTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeCommSetStatusSafi}, GetNextIndexFsbgp4MpeCommRouteCommSetStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeCommRouteCommSetStatusTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeCommSetStatusIpNetwork}, GetNextIndexFsbgp4MpeCommRouteCommSetStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeCommRouteCommSetStatusTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeCommSetStatusIpPrefixLen}, GetNextIndexFsbgp4MpeCommRouteCommSetStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4MpeCommRouteCommSetStatusTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeCommSetStatus}, GetNextIndexFsbgp4MpeCommRouteCommSetStatusTable, Fsbgp4mpeCommSetStatusGet, Fsbgp4mpeCommSetStatusSet, Fsbgp4mpeCommSetStatusTest, Fsbgp4MpeCommRouteCommSetStatusTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeCommRouteCommSetStatusTableINDEX, 4, 0, 0, "4"},

{{12,Fsbgp4mpeCommSetStatusRowStatus}, GetNextIndexFsbgp4MpeCommRouteCommSetStatusTable, Fsbgp4mpeCommSetStatusRowStatusGet, Fsbgp4mpeCommSetStatusRowStatusSet, Fsbgp4mpeCommSetStatusRowStatusTest, Fsbgp4MpeCommRouteCommSetStatusTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeCommRouteCommSetStatusTableINDEX, 4, 0, 1, NULL},

{{12,Fsbgp4mpeAddExtCommRtAfi}, GetNextIndexFsbgp4MpeExtCommRouteAddExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeExtCommRouteAddExtCommTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeAddExtCommRtSafi}, GetNextIndexFsbgp4MpeExtCommRouteAddExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeExtCommRouteAddExtCommTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeAddExtCommIpNetwork}, GetNextIndexFsbgp4MpeExtCommRouteAddExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeExtCommRouteAddExtCommTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeAddExtCommIpPrefixLen}, GetNextIndexFsbgp4MpeExtCommRouteAddExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4MpeExtCommRouteAddExtCommTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeAddExtCommVal}, GetNextIndexFsbgp4MpeExtCommRouteAddExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeExtCommRouteAddExtCommTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeAddExtCommRowStatus}, GetNextIndexFsbgp4MpeExtCommRouteAddExtCommTable, Fsbgp4mpeAddExtCommRowStatusGet, Fsbgp4mpeAddExtCommRowStatusSet, Fsbgp4mpeAddExtCommRowStatusTest, Fsbgp4MpeExtCommRouteAddExtCommTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeExtCommRouteAddExtCommTableINDEX, 5, 0, 1, NULL},

{{12,Fsbgp4mpeDeleteExtCommRtAfi}, GetNextIndexFsbgp4MpeExtCommRouteDeleteExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeExtCommRouteDeleteExtCommTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeDeleteExtCommRtSafi}, GetNextIndexFsbgp4MpeExtCommRouteDeleteExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeExtCommRouteDeleteExtCommTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeDeleteExtCommIpNetwork}, GetNextIndexFsbgp4MpeExtCommRouteDeleteExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeExtCommRouteDeleteExtCommTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeDeleteExtCommIpPrefixLen}, GetNextIndexFsbgp4MpeExtCommRouteDeleteExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4MpeExtCommRouteDeleteExtCommTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeDeleteExtCommVal}, GetNextIndexFsbgp4MpeExtCommRouteDeleteExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeExtCommRouteDeleteExtCommTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeDeleteExtCommRowStatus}, GetNextIndexFsbgp4MpeExtCommRouteDeleteExtCommTable, Fsbgp4mpeDeleteExtCommRowStatusGet, Fsbgp4mpeDeleteExtCommRowStatusSet, Fsbgp4mpeDeleteExtCommRowStatusTest, Fsbgp4MpeExtCommRouteDeleteExtCommTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeExtCommRouteDeleteExtCommTableINDEX, 5, 0, 1, NULL},

{{12,Fsbgp4mpeExtCommSetStatusRtAfi}, GetNextIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeExtCommRouteExtCommSetStatusTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeExtCommSetStatusRtSafi}, GetNextIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeExtCommRouteExtCommSetStatusTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeExtCommSetStatusIpNetwork}, GetNextIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeExtCommRouteExtCommSetStatusTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeExtCommSetStatusIpPrefixLen}, GetNextIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4MpeExtCommRouteExtCommSetStatusTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeExtCommSetStatus}, GetNextIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable, Fsbgp4mpeExtCommSetStatusGet, Fsbgp4mpeExtCommSetStatusSet, Fsbgp4mpeExtCommSetStatusTest, Fsbgp4MpeExtCommRouteExtCommSetStatusTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeExtCommRouteExtCommSetStatusTableINDEX, 4, 0, 0, "4"},

{{12,Fsbgp4mpeExtCommSetStatusRowStatus}, GetNextIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable, Fsbgp4mpeExtCommSetStatusRowStatusGet, Fsbgp4mpeExtCommSetStatusRowStatusSet, Fsbgp4mpeExtCommSetStatusRowStatusTest, Fsbgp4MpeExtCommRouteExtCommSetStatusTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeExtCommRouteExtCommSetStatusTableINDEX, 4, 0, 1, NULL},

{{12,Fsbgp4mpePeerLinkType}, GetNextIndexFsbgp4MpePeerLinkBwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpePeerLinkBwTableINDEX, 2, 0, 0, NULL},

{{12,Fsbgp4mpePeerLinkRemAddr}, GetNextIndexFsbgp4MpePeerLinkBwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpePeerLinkBwTableINDEX, 2, 0, 0, NULL},

{{12,Fsbgp4mpeLinkBandWidth}, GetNextIndexFsbgp4MpePeerLinkBwTable, Fsbgp4mpeLinkBandWidthGet, Fsbgp4mpeLinkBandWidthSet, Fsbgp4mpeLinkBandWidthTest, Fsbgp4MpePeerLinkBwTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fsbgp4MpePeerLinkBwTableINDEX, 2, 0, 0, NULL},

{{12,Fsbgp4mpePeerLinkBwRowStatus}, GetNextIndexFsbgp4MpePeerLinkBwTable, Fsbgp4mpePeerLinkBwRowStatusGet, Fsbgp4mpePeerLinkBwRowStatusSet, Fsbgp4mpePeerLinkBwRowStatusTest, Fsbgp4MpePeerLinkBwTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpePeerLinkBwTableINDEX, 2, 0, 1, NULL},

{{12,Fsbgp4mpeCapPeerType}, GetNextIndexFsbgp4MpeCapSupportedCapsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeCapSupportedCapsTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeCapPeerRemoteIpAddr}, GetNextIndexFsbgp4MpeCapSupportedCapsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeCapSupportedCapsTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeSupportedCapabilityCode}, GetNextIndexFsbgp4MpeCapSupportedCapsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4MpeCapSupportedCapsTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeSupportedCapabilityLength}, GetNextIndexFsbgp4MpeCapSupportedCapsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4MpeCapSupportedCapsTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeSupportedCapabilityValue}, GetNextIndexFsbgp4MpeCapSupportedCapsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeCapSupportedCapsTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeCapSupportedCapsRowStatus}, GetNextIndexFsbgp4MpeCapSupportedCapsTable, Fsbgp4mpeCapSupportedCapsRowStatusGet, Fsbgp4mpeCapSupportedCapsRowStatusSet, Fsbgp4mpeCapSupportedCapsRowStatusTest, Fsbgp4MpeCapSupportedCapsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeCapSupportedCapsTableINDEX, 5, 0, 1, NULL},

{{12,Fsbgp4mpeCapAnnouncedStatus}, GetNextIndexFsbgp4MpeCapSupportedCapsTable, Fsbgp4mpeCapAnnouncedStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeCapSupportedCapsTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeCapReceivedStatus}, GetNextIndexFsbgp4MpeCapSupportedCapsTable, Fsbgp4mpeCapReceivedStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeCapSupportedCapsTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeCapNegotiatedStatus}, GetNextIndexFsbgp4MpeCapSupportedCapsTable, Fsbgp4mpeCapNegotiatedStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeCapSupportedCapsTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeCapConfiguredStatus}, GetNextIndexFsbgp4MpeCapSupportedCapsTable, Fsbgp4mpeCapConfiguredStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsbgp4MpeCapSupportedCapsTableINDEX, 5, 0, 0, NULL},

{{12,Fsbgp4mpeRtRefreshInboundPeerType}, GetNextIndexFsbgp4MpeRtRefreshInboundTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeRtRefreshInboundTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeRtRefreshInboundPeerAddr}, GetNextIndexFsbgp4MpeRtRefreshInboundTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeRtRefreshInboundTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeRtRefreshInboundAfi}, GetNextIndexFsbgp4MpeRtRefreshInboundTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeRtRefreshInboundTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeRtRefreshInboundSafi}, GetNextIndexFsbgp4MpeRtRefreshInboundTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeRtRefreshInboundTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeRtRefreshInboundRequest}, GetNextIndexFsbgp4MpeRtRefreshInboundTable, Fsbgp4mpeRtRefreshInboundRequestGet, Fsbgp4mpeRtRefreshInboundRequestSet, Fsbgp4mpeRtRefreshInboundRequestTest, Fsbgp4MpeRtRefreshInboundTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeRtRefreshInboundTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeRtRefreshInboundPrefixFilter}, GetNextIndexFsbgp4MpeRtRefreshInboundTable, Fsbgp4mpeRtRefreshInboundPrefixFilterGet, Fsbgp4mpeRtRefreshInboundPrefixFilterSet, Fsbgp4mpeRtRefreshInboundPrefixFilterTest, Fsbgp4MpeRtRefreshInboundTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeRtRefreshInboundTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeRtRefreshStatisticsPeerType}, GetNextIndexFsbgp4MpeRtRefreshStatisticsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeRtRefreshStatisticsTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeRtRefreshStatisticsPeerAddr}, GetNextIndexFsbgp4MpeRtRefreshStatisticsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeRtRefreshStatisticsTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeRtRefreshStatisticsAfi}, GetNextIndexFsbgp4MpeRtRefreshStatisticsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeRtRefreshStatisticsTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeRtRefreshStatisticsSafi}, GetNextIndexFsbgp4MpeRtRefreshStatisticsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeRtRefreshStatisticsTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeRtRefreshStatisticsRtRefMsgSentCntr}, GetNextIndexFsbgp4MpeRtRefreshStatisticsTable, Fsbgp4mpeRtRefreshStatisticsRtRefMsgSentCntrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsbgp4MpeRtRefreshStatisticsTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeRtRefreshStatisticsRtRefMsgTxErrCntr}, GetNextIndexFsbgp4MpeRtRefreshStatisticsTable, Fsbgp4mpeRtRefreshStatisticsRtRefMsgTxErrCntrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsbgp4MpeRtRefreshStatisticsTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeRtRefreshStatisticsRtRefMsgRcvdCntr}, GetNextIndexFsbgp4MpeRtRefreshStatisticsTable, Fsbgp4mpeRtRefreshStatisticsRtRefMsgRcvdCntrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsbgp4MpeRtRefreshStatisticsTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeRtRefreshStatisticsRtRefMsgInvalidCntr}, GetNextIndexFsbgp4MpeRtRefreshStatisticsTable, Fsbgp4mpeRtRefreshStatisticsRtRefMsgInvalidCntrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsbgp4MpeRtRefreshStatisticsTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeSoftReconfigOutboundPeerType}, GetNextIndexFsbgp4MpeSoftReconfigOutboundTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeSoftReconfigOutboundTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeSoftReconfigOutboundPeerAddr}, GetNextIndexFsbgp4MpeSoftReconfigOutboundTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpeSoftReconfigOutboundTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeSoftReconfigOutboundAfi}, GetNextIndexFsbgp4MpeSoftReconfigOutboundTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeSoftReconfigOutboundTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeSoftReconfigOutboundSafi}, GetNextIndexFsbgp4MpeSoftReconfigOutboundTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpeSoftReconfigOutboundTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4mpeSoftReconfigOutboundRequest}, GetNextIndexFsbgp4MpeSoftReconfigOutboundTable, Fsbgp4mpeSoftReconfigOutboundRequestGet, Fsbgp4mpeSoftReconfigOutboundRequestSet, Fsbgp4mpeSoftReconfigOutboundRequestTest, Fsbgp4MpeSoftReconfigOutboundTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MpeSoftReconfigOutboundTableINDEX, 4, 0, 0, NULL},

{{11,Fsbgp4MpePeerRemoteAddrType}, GetNextIndexFsbgp4MpePrefixCountersTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpePrefixCountersTableINDEX, 4, 0, 0, NULL},

{{11,Fsbgp4MpePeerRemoteAddr}, GetNextIndexFsbgp4MpePrefixCountersTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MpePrefixCountersTableINDEX, 4, 0, 0, NULL},

{{11,Fsbgp4MpePrefixCountersAfi}, GetNextIndexFsbgp4MpePrefixCountersTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpePrefixCountersTableINDEX, 4, 0, 0, NULL},

{{11,Fsbgp4MpePrefixCountersSafi}, GetNextIndexFsbgp4MpePrefixCountersTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MpePrefixCountersTableINDEX, 4, 0, 0, NULL},

{{11,Fsbgp4MpePrefixCountersPrefixesReceived}, GetNextIndexFsbgp4MpePrefixCountersTable, Fsbgp4MpePrefixCountersPrefixesReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsbgp4MpePrefixCountersTableINDEX, 4, 0, 0, NULL},

{{11,Fsbgp4MpePrefixCountersPrefixesSent}, GetNextIndexFsbgp4MpePrefixCountersTable, Fsbgp4MpePrefixCountersPrefixesSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsbgp4MpePrefixCountersTableINDEX, 4, 0, 0, NULL},

{{11,Fsbgp4MpePrefixCountersWithdrawsReceived}, GetNextIndexFsbgp4MpePrefixCountersTable, Fsbgp4MpePrefixCountersWithdrawsReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsbgp4MpePrefixCountersTableINDEX, 4, 0, 0, NULL},

{{11,Fsbgp4MpePrefixCountersWithdrawsSent}, GetNextIndexFsbgp4MpePrefixCountersTable, Fsbgp4MpePrefixCountersWithdrawsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsbgp4MpePrefixCountersTableINDEX, 4, 0, 0, NULL},

{{11,Fsbgp4MpePrefixCountersInPrefixes}, GetNextIndexFsbgp4MpePrefixCountersTable, Fsbgp4MpePrefixCountersInPrefixesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, Fsbgp4MpePrefixCountersTableINDEX, 4, 0, 0, NULL},

{{11,Fsbgp4MpePrefixCountersInPrefixesAccepted}, GetNextIndexFsbgp4MpePrefixCountersTable, Fsbgp4MpePrefixCountersInPrefixesAcceptedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, Fsbgp4MpePrefixCountersTableINDEX, 4, 0, 0, NULL},

{{11,Fsbgp4MpePrefixCountersInPrefixesRejected}, GetNextIndexFsbgp4MpePrefixCountersTable, Fsbgp4MpePrefixCountersInPrefixesRejectedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, Fsbgp4MpePrefixCountersTableINDEX, 4, 0, 0, NULL},

{{11,Fsbgp4MpePrefixCountersOutPrefixes}, GetNextIndexFsbgp4MpePrefixCountersTable, Fsbgp4MpePrefixCountersOutPrefixesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, Fsbgp4MpePrefixCountersTableINDEX, 4, 0, 0, NULL},

{{12,Fsbgp4MplsVpnVrfName}, GetNextIndexFsbgp4MplsVpnVrfRouteTargetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MplsVpnVrfRouteTargetTableINDEX, 3, 0, 0, NULL},

{{12,Fsbgp4MplsVpnVrfRouteTargetType}, GetNextIndexFsbgp4MplsVpnVrfRouteTargetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MplsVpnVrfRouteTargetTableINDEX, 3, 0, 0, NULL},

{{12,Fsbgp4MplsVpnVrfRouteTarget}, GetNextIndexFsbgp4MplsVpnVrfRouteTargetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MplsVpnVrfRouteTargetTableINDEX, 3, 0, 0, NULL},

{{12,Fsbgp4MplsVpnVrfRouteTargetRowStatus}, GetNextIndexFsbgp4MplsVpnVrfRouteTargetTable, Fsbgp4MplsVpnVrfRouteTargetRowStatusGet, Fsbgp4MplsVpnVrfRouteTargetRowStatusSet, Fsbgp4MplsVpnVrfRouteTargetRowStatusTest, Fsbgp4MplsVpnVrfRouteTargetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4MplsVpnVrfRouteTargetTableINDEX, 3, 0, 1, NULL},

{{12,Fsbgp4MplsVpnVrfRedisAfi}, GetNextIndexFsbgp4MplsVpnVrfRedistributeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MplsVpnVrfRedistributeTableINDEX, 3, 0, 0, NULL},

{{12,Fsbgp4MplsVpnVrfRedisSafi}, GetNextIndexFsbgp4MplsVpnVrfRedistributeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4MplsVpnVrfRedistributeTableINDEX, 3, 0, 0, NULL},

{{12,Fsbgp4MplsVpnVrfRedisProtoMask}, GetNextIndexFsbgp4MplsVpnVrfRedistributeTable, Fsbgp4MplsVpnVrfRedisProtoMaskGet, Fsbgp4MplsVpnVrfRedisProtoMaskSet, Fsbgp4MplsVpnVrfRedisProtoMaskTest, Fsbgp4MplsVpnVrfRedistributeTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4MplsVpnVrfRedistributeTableINDEX, 3, 0, 0, NULL},

{{12,Fsbgp4MplsVpnRRRouteTarget}, GetNextIndexFsbgp4MplsVpnRRRouteTargetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4MplsVpnRRRouteTargetTableINDEX, 1, 0, 0, NULL},

{{12,Fsbgp4MplsVpnRRRouteTargetRtCnt}, GetNextIndexFsbgp4MplsVpnRRRouteTargetTable, Fsbgp4MplsVpnRRRouteTargetRtCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MplsVpnRRRouteTargetTableINDEX, 1, 0, 0, NULL},

{{12,Fsbgp4MplsVpnRRRouteTargetTimeStamp}, GetNextIndexFsbgp4MplsVpnRRRouteTargetTable, Fsbgp4MplsVpnRRRouteTargetTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsbgp4MplsVpnRRRouteTargetTableINDEX, 1, 0, 0, NULL},

{{12,FsBgp4DistInOutRouteMapName}, GetNextIndexFsBgp4DistInOutRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsBgp4DistInOutRouteMapTableINDEX, 2, 0, 0, NULL},

{{12,FsBgp4DistInOutRouteMapType}, GetNextIndexFsBgp4DistInOutRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsBgp4DistInOutRouteMapTableINDEX, 2, 0, 0, NULL},

{{12,FsBgp4DistInOutRouteMapValue}, GetNextIndexFsBgp4DistInOutRouteMapTable, FsBgp4DistInOutRouteMapValueGet, FsBgp4DistInOutRouteMapValueSet, FsBgp4DistInOutRouteMapValueTest, FsBgp4DistInOutRouteMapTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsBgp4DistInOutRouteMapTableINDEX, 2, 0, 0, NULL},

{{12,FsBgp4DistInOutRouteMapRowStatus}, GetNextIndexFsBgp4DistInOutRouteMapTable, FsBgp4DistInOutRouteMapRowStatusGet, FsBgp4DistInOutRouteMapRowStatusSet, FsBgp4DistInOutRouteMapRowStatusTest, FsBgp4DistInOutRouteMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4DistInOutRouteMapTableINDEX, 2, 0, 1, NULL},

{{10,FsBgp4PreferenceValue}, NULL, FsBgp4PreferenceValueGet, FsBgp4PreferenceValueSet, FsBgp4PreferenceValueTest, FsBgp4PreferenceValueDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "122"},

{{12,FsBgp4NeighborRouteMapPeerAddrType}, GetNextIndexFsBgp4NeighborRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsBgp4NeighborRouteMapTableINDEX, 3, 0, 0, NULL},

{{12,FsBgp4NeighborRouteMapPeer}, GetNextIndexFsBgp4NeighborRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsBgp4NeighborRouteMapTableINDEX, 3, 0, 0, NULL},

{{12,FsBgp4NeighborRouteMapDirection}, GetNextIndexFsBgp4NeighborRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsBgp4NeighborRouteMapTableINDEX, 3, 0, 0, NULL},

{{12,FsBgp4NeighborRouteMapName}, GetNextIndexFsBgp4NeighborRouteMapTable, FsBgp4NeighborRouteMapNameGet, FsBgp4NeighborRouteMapNameSet, FsBgp4NeighborRouteMapNameTest, FsBgp4NeighborRouteMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsBgp4NeighborRouteMapTableINDEX, 3, 0, 0, NULL},

{{12,FsBgp4NeighborRouteMapRowStatus}, GetNextIndexFsBgp4NeighborRouteMapTable, FsBgp4NeighborRouteMapRowStatusGet, FsBgp4NeighborRouteMapRowStatusSet, FsBgp4NeighborRouteMapRowStatusTest, FsBgp4NeighborRouteMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4NeighborRouteMapTableINDEX, 3, 0, 1, NULL},

{{11,FsBgp4PeerGroupName}, GetNextIndexFsBgp4PeerGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerGroupAddrType}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupAddrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerGroupRemoteAs}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupRemoteAsGet, FsBgp4PeerGroupRemoteAsSet, FsBgp4PeerGroupRemoteAsTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerGroupHoldTimeConfigured}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupHoldTimeConfiguredGet, FsBgp4PeerGroupHoldTimeConfiguredSet, FsBgp4PeerGroupHoldTimeConfiguredTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerGroupKeepAliveConfigured}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupKeepAliveConfiguredGet, FsBgp4PeerGroupKeepAliveConfiguredSet, FsBgp4PeerGroupKeepAliveConfiguredTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerGroupConnectRetryInterval}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupConnectRetryIntervalGet, FsBgp4PeerGroupConnectRetryIntervalSet, FsBgp4PeerGroupConnectRetryIntervalTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerGroupMinASOriginInterval}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupMinASOriginIntervalGet, FsBgp4PeerGroupMinASOriginIntervalSet, FsBgp4PeerGroupMinASOriginIntervalTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerGroupMinRouteAdvInterval}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupMinRouteAdvIntervalGet, FsBgp4PeerGroupMinRouteAdvIntervalSet, FsBgp4PeerGroupMinRouteAdvIntervalTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerGroupAllowAutomaticStart}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupAllowAutomaticStartGet, FsBgp4PeerGroupAllowAutomaticStartSet, FsBgp4PeerGroupAllowAutomaticStartTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, "2"},

{{11,FsBgp4PeerGroupAllowAutomaticStop}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupAllowAutomaticStopGet, FsBgp4PeerGroupAllowAutomaticStopSet, FsBgp4PeerGroupAllowAutomaticStopTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, "2"},

{{11,FsBgp4PeerGroupIdleHoldTimeConfigured}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupIdleHoldTimeConfiguredGet, FsBgp4PeerGroupIdleHoldTimeConfiguredSet, FsBgp4PeerGroupIdleHoldTimeConfiguredTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, "60"},

{{11,FsBgp4PeerGroupDampPeerOscillations}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupDampPeerOscillationsGet, FsBgp4PeerGroupDampPeerOscillationsSet, FsBgp4PeerGroupDampPeerOscillationsTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, "2"},

{{11,FsBgp4PeerGroupDelayOpen}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupDelayOpenGet, FsBgp4PeerGroupDelayOpenSet, FsBgp4PeerGroupDelayOpenTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, "2"},

{{11,FsBgp4PeerGroupDelayOpenTimeConfigured}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupDelayOpenTimeConfiguredGet, FsBgp4PeerGroupDelayOpenTimeConfiguredSet, FsBgp4PeerGroupDelayOpenTimeConfiguredTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, "0"},

{{11,FsBgp4PeerGroupPrefixUpperLimit}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupPrefixUpperLimitGet, FsBgp4PeerGroupPrefixUpperLimitSet, FsBgp4PeerGroupPrefixUpperLimitTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, "5000"},

{{11,FsBgp4PeerGroupTcpConnectRetryCnt}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupTcpConnectRetryCntGet, FsBgp4PeerGroupTcpConnectRetryCntSet, FsBgp4PeerGroupTcpConnectRetryCntTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, "5"},

{{11,FsBgp4PeerGroupEBGPMultiHop}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupEBGPMultiHopGet, FsBgp4PeerGroupEBGPMultiHopSet, FsBgp4PeerGroupEBGPMultiHopTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerGroupEBGPHopLimit}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupEBGPHopLimitGet, FsBgp4PeerGroupEBGPHopLimitSet, FsBgp4PeerGroupEBGPHopLimitTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerGroupNextHopSelf}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupNextHopSelfGet, FsBgp4PeerGroupNextHopSelfSet, FsBgp4PeerGroupNextHopSelfTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, "1"},

{{11,FsBgp4PeerGroupRflClient}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupRflClientGet, FsBgp4PeerGroupRflClientSet, FsBgp4PeerGroupRflClientTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, "1"},

{{11,FsBgp4PeerGroupTcpSendBufSize}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupTcpSendBufSizeGet, FsBgp4PeerGroupTcpSendBufSizeSet, FsBgp4PeerGroupTcpSendBufSizeTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, "65536"},

{{11,FsBgp4PeerGroupTcpRcvBufSize}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupTcpRcvBufSizeGet, FsBgp4PeerGroupTcpRcvBufSizeSet, FsBgp4PeerGroupTcpRcvBufSizeTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, "65536"},

{{11,FsBgp4PeerGroupCommSendStatus}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupCommSendStatusGet, FsBgp4PeerGroupCommSendStatusSet, FsBgp4PeerGroupCommSendStatusTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, "2"},

{{11,FsBgp4PeerGroupECommSendStatus}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupECommSendStatusGet, FsBgp4PeerGroupECommSendStatusSet, FsBgp4PeerGroupECommSendStatusTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, "2"},

{{11,FsBgp4PeerGroupPassive}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupPassiveGet, FsBgp4PeerGroupPassiveSet, FsBgp4PeerGroupPassiveTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerGroupDefaultOriginate}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupDefaultOriginateGet, FsBgp4PeerGroupDefaultOriginateSet, FsBgp4PeerGroupDefaultOriginateTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, "2"},

{{11,FsBgp4PeerGroupActivateMPCapability}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupActivateMPCapabilityGet, FsBgp4PeerGroupActivateMPCapabilitySet, FsBgp4PeerGroupActivateMPCapabilityTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerGroupDeactivateMPCapability}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupDeactivateMPCapabilityGet, FsBgp4PeerGroupDeactivateMPCapabilitySet, FsBgp4PeerGroupDeactivateMPCapabilityTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerGroupRouteMapNameIn}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupRouteMapNameInGet, FsBgp4PeerGroupRouteMapNameInSet, FsBgp4PeerGroupRouteMapNameInTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerGroupRouteMapNameOut}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupRouteMapNameOutGet, FsBgp4PeerGroupRouteMapNameOutSet, FsBgp4PeerGroupRouteMapNameOutTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerGroupStatus}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupStatusGet, FsBgp4PeerGroupStatusSet, FsBgp4PeerGroupStatusTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 1, NULL},

{{11,FsBgp4PeerGroupIpPrefixNameIn}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupIpPrefixNameInGet, FsBgp4PeerGroupIpPrefixNameInSet, FsBgp4PeerGroupIpPrefixNameInTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerGroupIpPrefixNameOut}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupIpPrefixNameOutGet, FsBgp4PeerGroupIpPrefixNameOutSet, FsBgp4PeerGroupIpPrefixNameOutTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerGroupOrfType}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupOrfTypeGet, FsBgp4PeerGroupOrfTypeSet, FsBgp4PeerGroupOrfTypeTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerGroupOrfCapMode}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupOrfCapModeGet, FsBgp4PeerGroupOrfCapModeSet, FsBgp4PeerGroupOrfCapModeTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerGroupOrfRequest}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupOrfRequestGet, FsBgp4PeerGroupOrfRequestSet, FsBgp4PeerGroupOrfRequestTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerGroupBfdStatus}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupBfdStatusGet, FsBgp4PeerGroupBfdStatusSet, FsBgp4PeerGroupBfdStatusTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, "2"},

{{11,FsBgp4PeerGroupOverrideCapability}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupOverrideCapabilityGet, FsBgp4PeerGroupOverrideCapabilitySet, FsBgp4PeerGroupOverrideCapabilityTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, "2"},
{{11,FsBgp4PeerGroupLocalAs}, GetNextIndexFsBgp4PeerGroupTable, FsBgp4PeerGroupLocalAsGet, FsBgp4PeerGroupLocalAsSet, FsBgp4PeerGroupLocalAsTest, FsBgp4PeerGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsBgp4PeerGroupTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4PeerAddrType}, GetNextIndexFsBgp4PeerGroupListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsBgp4PeerGroupListTableINDEX, 3, 0, 0, NULL},

{{11,FsBgp4PeerAddress}, GetNextIndexFsBgp4PeerGroupListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsBgp4PeerGroupListTableINDEX, 3, 0, 0, NULL},

{{11,FsBgp4PeerAddStatus}, GetNextIndexFsBgp4PeerGroupListTable, FsBgp4PeerAddStatusGet, FsBgp4PeerAddStatusSet, FsBgp4PeerAddStatusTest, FsBgp4PeerGroupListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4PeerGroupListTableINDEX, 3, 0, 0, NULL},

{{12,Fsbgp4TCPMKTAuthKeyId}, GetNextIndexFsbgp4TCPMKTAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4TCPMKTAuthTableINDEX, 1, 0, 0, NULL},

{{12,Fsbgp4TCPMKTAuthRecvKeyId}, GetNextIndexFsbgp4TCPMKTAuthTable, Fsbgp4TCPMKTAuthRecvKeyIdGet, Fsbgp4TCPMKTAuthRecvKeyIdSet, Fsbgp4TCPMKTAuthRecvKeyIdTest, Fsbgp4TCPMKTAuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsbgp4TCPMKTAuthTableINDEX, 1, 0, 0, NULL},

{{12,Fsbgp4TCPMKTAuthMasterKey}, GetNextIndexFsbgp4TCPMKTAuthTable, Fsbgp4TCPMKTAuthMasterKeyGet, Fsbgp4TCPMKTAuthMasterKeySet, Fsbgp4TCPMKTAuthMasterKeyTest, Fsbgp4TCPMKTAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4TCPMKTAuthTableINDEX, 1, 0, 0, NULL},

{{12,Fsbgp4TCPMKTAuthAlgo}, GetNextIndexFsbgp4TCPMKTAuthTable, Fsbgp4TCPMKTAuthAlgoGet, Fsbgp4TCPMKTAuthAlgoSet, Fsbgp4TCPMKTAuthAlgoTest, Fsbgp4TCPMKTAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4TCPMKTAuthTableINDEX, 1, 0, 0, "1"},

{{12,Fsbgp4TCPMKTAuthTcpOptExc}, GetNextIndexFsbgp4TCPMKTAuthTable, Fsbgp4TCPMKTAuthTcpOptExcGet, Fsbgp4TCPMKTAuthTcpOptExcSet, Fsbgp4TCPMKTAuthTcpOptExcTest, Fsbgp4TCPMKTAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4TCPMKTAuthTableINDEX, 1, 0, 0, NULL},

{{12,Fsbgp4TCPMKTAuthRowStatus}, GetNextIndexFsbgp4TCPMKTAuthTable, Fsbgp4TCPMKTAuthRowStatusGet, Fsbgp4TCPMKTAuthRowStatusSet, Fsbgp4TCPMKTAuthRowStatusTest, Fsbgp4TCPMKTAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4TCPMKTAuthTableINDEX, 1, 0, 1, NULL},

{{12,Fsbgp4TCPAOAuthPeerType}, GetNextIndexFsbgp4TCPAOAuthPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsbgp4TCPAOAuthPeerTableINDEX, 3, 0, 0, NULL},

{{12,Fsbgp4TCPAOAuthPeerAddr}, GetNextIndexFsbgp4TCPAOAuthPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsbgp4TCPAOAuthPeerTableINDEX, 3, 0, 0, NULL},

{{12,Fsbgp4TCPAOAuthKeyId}, GetNextIndexFsbgp4TCPAOAuthPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsbgp4TCPAOAuthPeerTableINDEX, 3, 0, 0, NULL},

{{12,Fsbgp4TCPAOAuthKeyStatus}, GetNextIndexFsbgp4TCPAOAuthPeerTable, Fsbgp4TCPAOAuthKeyStatusGet, Fsbgp4TCPAOAuthKeyStatusSet, Fsbgp4TCPAOAuthKeyStatusTest, Fsbgp4TCPAOAuthPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsbgp4TCPAOAuthPeerTableINDEX, 3, 0, 0, NULL},

{{12,Fsbgp4TCPAOAuthKeyStartAccept}, GetNextIndexFsbgp4TCPAOAuthPeerTable, Fsbgp4TCPAOAuthKeyStartAcceptGet, Fsbgp4TCPAOAuthKeyStartAcceptSet, Fsbgp4TCPAOAuthKeyStartAcceptTest, Fsbgp4TCPAOAuthPeerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4TCPAOAuthPeerTableINDEX, 3, 0, 0, "0000000000000000"},

{{12,Fsbgp4TCPAOAuthKeyStartGenerate}, GetNextIndexFsbgp4TCPAOAuthPeerTable, Fsbgp4TCPAOAuthKeyStartGenerateGet, Fsbgp4TCPAOAuthKeyStartGenerateSet, Fsbgp4TCPAOAuthKeyStartGenerateTest, Fsbgp4TCPAOAuthPeerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4TCPAOAuthPeerTableINDEX, 3, 0, 0, "0000000000000000"},

{{12,Fsbgp4TCPAOAuthKeyStopGenerate}, GetNextIndexFsbgp4TCPAOAuthPeerTable, Fsbgp4TCPAOAuthKeyStopGenerateGet, Fsbgp4TCPAOAuthKeyStopGenerateSet, Fsbgp4TCPAOAuthKeyStopGenerateTest, Fsbgp4TCPAOAuthPeerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4TCPAOAuthPeerTableINDEX, 3, 0, 0, "0000000000000000"},

{{12,Fsbgp4TCPAOAuthKeyStopAccept}, GetNextIndexFsbgp4TCPAOAuthPeerTable, Fsbgp4TCPAOAuthKeyStopAcceptGet, Fsbgp4TCPAOAuthKeyStopAcceptSet, Fsbgp4TCPAOAuthKeyStopAcceptTest, Fsbgp4TCPAOAuthPeerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsbgp4TCPAOAuthPeerTableINDEX, 3, 0, 0, "0000000000000000"},

{{11,FsBgp4ORFPeerAddrType}, GetNextIndexFsBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsBgp4ORFListTableINDEX, 11, 0, 0, NULL},

{{11,FsBgp4ORFPeerAddr}, GetNextIndexFsBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsBgp4ORFListTableINDEX, 11, 0, 0, NULL},

{{11,FsBgp4ORFAfi}, GetNextIndexFsBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsBgp4ORFListTableINDEX, 11, 0, 0, NULL},

{{11,FsBgp4ORFSafi}, GetNextIndexFsBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsBgp4ORFListTableINDEX, 11, 0, 0, NULL},

{{11,FsBgp4ORFType}, GetNextIndexFsBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsBgp4ORFListTableINDEX, 11, 0, 0, NULL},

{{11,FsBgp4ORFSequence}, GetNextIndexFsBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsBgp4ORFListTableINDEX, 11, 0, 0, NULL},

{{11,FsBgp4ORFAddrPrefix}, GetNextIndexFsBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsBgp4ORFListTableINDEX, 11, 0, 0, NULL},

{{11,FsBgp4ORFAddrPrefixLen}, GetNextIndexFsBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsBgp4ORFListTableINDEX, 11, 0, 0, NULL},

{{11,FsBgp4ORFMinLength}, GetNextIndexFsBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsBgp4ORFListTableINDEX, 11, 0, 0, NULL},

{{11,FsBgp4ORFMaxLength}, GetNextIndexFsBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsBgp4ORFListTableINDEX, 11, 0, 0, NULL},

{{11,FsBgp4ORFAction}, GetNextIndexFsBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsBgp4ORFListTableINDEX, 11, 0, 0, NULL},

{{10,FsBgp4RmTestObject}, NULL, FsBgp4RmTestObjectGet, FsBgp4RmTestObjectSet, FsBgp4RmTestObjectTest, FsBgp4RmTestObjectDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsBgp4RRDNetworkAddr}, GetNextIndexFsBgp4RRDNetworkTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsBgp4RRDNetworkTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4RRDNetworkAddrType}, GetNextIndexFsBgp4RRDNetworkTable, FsBgp4RRDNetworkAddrTypeGet, FsBgp4RRDNetworkAddrTypeSet, FsBgp4RRDNetworkAddrTypeTest, FsBgp4RRDNetworkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4RRDNetworkTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4RRDNetworkPrefixLen}, GetNextIndexFsBgp4RRDNetworkTable, FsBgp4RRDNetworkPrefixLenGet, FsBgp4RRDNetworkPrefixLenSet, FsBgp4RRDNetworkPrefixLenTest, FsBgp4RRDNetworkTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsBgp4RRDNetworkTableINDEX, 1, 0, 0, NULL},

{{11,FsBgp4RRDNetworkRowStatus}, GetNextIndexFsBgp4RRDNetworkTable, FsBgp4RRDNetworkRowStatusGet, FsBgp4RRDNetworkRowStatusSet, FsBgp4RRDNetworkRowStatusTest, FsBgp4RRDNetworkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsBgp4RRDNetworkTableINDEX, 1, 0, 1, NULL},
};
tMibData fsbgp4Entry = { sizeof(fsbgp4MibEntry)/sizeof(fsbgp4MibEntry[0]), fsbgp4MibEntry };

#endif /* _FSBGP4DB_H */

