/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpbgdb.h,v 1.17 2016/12/27 12:35:53 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMPBGDB_H
#define _FSMPBGDB_H

UINT1 FsMIBgpContextTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIBgp4RRDMetricTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIBgp4CommInFilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIBgp4CommOutFilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIBgp4ExtCommInFilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,8};
UINT1 FsMIBgp4ExtCommOutFilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,8};
UINT1 FsMIBgp4TCPMD5AuthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsMIBgpAscConfedPeerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIBgp4MpeBgpPeerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsMIBgp4MpeBgp4PathAttrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsMIBgp4MpePeerExtTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsMIBgp4MpeMEDTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIBgp4MpeLocalPrefTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIBgp4MpeUpdateFilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIBgp4MpeAggregateTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIBgp4MpeImportRouteTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsMIBgp4MpeFsmTransitionHistTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsMIBgp4MpeRfdRtDampHistTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsMIBgp4MpeRfdPeerDampHistTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsMIBgp4MpeRfdRtsReuseListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsMIBgp4MpeRfdPeerReuseListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsMIBgp4MpeCommRouteAddCommTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIBgp4MpeCommRouteDeleteCommTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIBgp4MpeCommRouteCommSetStatusTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIBgp4MpeExtCommRouteAddExtCommTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,8};
UINT1 FsMIBgp4MpeExtCommRouteDeleteExtCommTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,8};
UINT1 FsMIBgp4MpeExtCommRouteExtCommSetStatusTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIBgp4MpePeerLinkBwTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsMIBgp4MpeCapSupportedCapsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsMIBgp4MpeRtRefreshInboundTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIBgp4MpeRtRefreshStatisticsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIBgp4MpeSoftReconfigOutboundTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIBgp4MpePrefixCountersTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIBgp4DistInOutRouteMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIBgp4NeighborRouteMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIBgp4PeerGroupTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsMIBgp4PeerGroupListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsMIBgp4TCPMKTAuthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIBgp4TCPAOAuthPeerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIBgp4ORFListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIBgp4RRDNetworkTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 fsmpbg [] ={1,3,6,1,4,1,29601,2,77};
tSNMP_OID_TYPE fsmpbgOID = {9, fsmpbg};


UINT4 FsMIBgp4GlobalTraceDebug [ ] ={1,3,6,1,4,1,29601,2,77,1};
UINT4 FsMIBgp4LocalAs [ ] ={1,3,6,1,4,1,29601,2,77,2};
UINT4 FsMIBgp4MaxPeerEntry [ ] ={1,3,6,1,4,1,29601,2,77,3};
UINT4 FsMIBgp4MaxNoofRoutes [ ] ={1,3,6,1,4,1,29601,2,77,4};
UINT4 FsMIBgp4GRAdminStatus [ ] ={1,3,6,1,4,1,29601,2,77,5};
UINT4 FsMIBgp4GRRestartTimeInterval [ ] ={1,3,6,1,4,1,29601,2,77,6};
UINT4 FsMIBgp4RestartExitReason [ ] ={1,3,6,1,4,1,29601,2,77,7};
UINT4 FsMIBgp4GRSelectionDeferralTimeInterval [ ] ={1,3,6,1,4,1,29601,2,77,8};
UINT4 FsMIBgp4GRStaleTimeInterval [ ] ={1,3,6,1,4,1,29601,2,77,9};
UINT4 FsMIBgp4GRMode [ ] ={1,3,6,1,4,1,29601,2,77,10};
UINT4 FsMIBgp4RestartSupport [ ] ={1,3,6,1,4,1,29601,2,77,11};
UINT4 FsMIBgp4RestartStatus [ ] ={1,3,6,1,4,1,29601,2,77,12};
UINT4 FsMIBgp4ForwardingPreservation [ ] ={1,3,6,1,4,1,29601,2,77,13};
UINT4 FsMIBgp4ContextId [ ] ={1,3,6,1,4,1,29601,2,77,14,1,1};
UINT4 FsMIBgp4GlobalAdminStatus [ ] ={1,3,6,1,4,1,29601,2,77,14,1,2};
UINT4 FsMIBgp4Identifier [ ] ={1,3,6,1,4,1,29601,2,77,14,1,3};
UINT4 FsMIBgp4Synchronization [ ] ={1,3,6,1,4,1,29601,2,77,14,1,4};
UINT4 FsMIBgp4DefaultLocalPref [ ] ={1,3,6,1,4,1,29601,2,77,14,1,5};
UINT4 FsMIBgp4AdvtNonBgpRt [ ] ={1,3,6,1,4,1,29601,2,77,14,1,6};
UINT4 FsMIBgp4TraceEnable [ ] ={1,3,6,1,4,1,29601,2,77,14,1,7};
UINT4 FsMIBgp4DebugEnable [ ] ={1,3,6,1,4,1,29601,2,77,14,1,8};
UINT4 FsMIBgp4OverlappingRoute [ ] ={1,3,6,1,4,1,29601,2,77,14,1,9};
UINT4 FsMIBgp4AlwaysCompareMED [ ] ={1,3,6,1,4,1,29601,2,77,14,1,10};
UINT4 FsMIBgp4DefaultOriginate [ ] ={1,3,6,1,4,1,29601,2,77,14,1,11};
UINT4 FsMIBgp4DefaultIpv4UniCast [ ] ={1,3,6,1,4,1,29601,2,77,14,1,12};
UINT4 FsMIBgp4IsTrapEnabled [ ] ={1,3,6,1,4,1,29601,2,77,14,1,13};
UINT4 FsMIBgp4NextHopProcessingInterval [ ] ={1,3,6,1,4,1,29601,2,77,14,1,14};
UINT4 FsMIBgp4IBGPRedistributionStatus [ ] ={1,3,6,1,4,1,29601,2,77,14,1,15};
UINT4 FsMIBgp4RRDAdminStatus [ ] ={1,3,6,1,4,1,29601,2,77,14,1,16};
UINT4 FsMIBgp4RRDProtoMaskForEnable [ ] ={1,3,6,1,4,1,29601,2,77,14,1,17};
UINT4 FsMIBgp4RRDSrcProtoMaskForDisable [ ] ={1,3,6,1,4,1,29601,2,77,14,1,18};
UINT4 FsMIBgp4RRDDefaultMetric [ ] ={1,3,6,1,4,1,29601,2,77,14,1,19};
UINT4 FsMIBgp4RRDRouteMapName [ ] ={1,3,6,1,4,1,29601,2,77,14,1,20};
UINT4 FsMIBgp4RRDMatchTypeEnable [ ] ={1,3,6,1,4,1,29601,2,77,14,1,21};
UINT4 FsMIBgp4RRDMatchTypeDisable [ ] ={1,3,6,1,4,1,29601,2,77,14,1,22};
UINT4 FsMIBgp4AscConfedId [ ] ={1,3,6,1,4,1,29601,2,77,14,1,23};
UINT4 FsMIBgp4AscConfedBestPathCompareMED [ ] ={1,3,6,1,4,1,29601,2,77,14,1,24};
UINT4 FsMIBgp4RflbgpClusterId [ ] ={1,3,6,1,4,1,29601,2,77,14,1,25};
UINT4 FsMIBgp4RflRflSupport [ ] ={1,3,6,1,4,1,29601,2,77,14,1,26};
UINT4 FsMIBgp4RfdCutOff [ ] ={1,3,6,1,4,1,29601,2,77,14,1,27};
UINT4 FsMIBgp4RfdReuse [ ] ={1,3,6,1,4,1,29601,2,77,14,1,28};
UINT4 FsMIBgp4RfdCeiling [ ] ={1,3,6,1,4,1,29601,2,77,14,1,29};
UINT4 FsMIBgp4RfdMaxHoldDownTime [ ] ={1,3,6,1,4,1,29601,2,77,14,1,30};
UINT4 FsMIBgp4RfdDecayHalfLifeTime [ ] ={1,3,6,1,4,1,29601,2,77,14,1,31};
UINT4 FsMIBgp4RfdDecayTimerGranularity [ ] ={1,3,6,1,4,1,29601,2,77,14,1,32};
UINT4 FsMIBgp4RfdReuseTimerGranularity [ ] ={1,3,6,1,4,1,29601,2,77,14,1,33};
UINT4 FsMIBgp4RfdReuseIndxArraySize [ ] ={1,3,6,1,4,1,29601,2,77,14,1,34};
UINT4 FsMIBgp4RfdAdminStatus [ ] ={1,3,6,1,4,1,29601,2,77,14,1,35};
UINT4 FsMIBgp4CommMaxInFTblEntries [ ] ={1,3,6,1,4,1,29601,2,77,14,1,36};
UINT4 FsMIBgp4CommMaxOutFTblEntries [ ] ={1,3,6,1,4,1,29601,2,77,14,1,37};
UINT4 FsMIBgp4ExtCommMaxInFTblEntries [ ] ={1,3,6,1,4,1,29601,2,77,14,1,38};
UINT4 FsMIBgp4ExtCommMaxOutFTblEntries [ ] ={1,3,6,1,4,1,29601,2,77,14,1,39};
UINT4 FsMIBgp4CapabilitySupportAvailable [ ] ={1,3,6,1,4,1,29601,2,77,14,1,40};
UINT4 FsMIBgp4MaxCapsPerPeer [ ] ={1,3,6,1,4,1,29601,2,77,14,1,41};
UINT4 FsMIBgp4MaxInstancesPerCap [ ] ={1,3,6,1,4,1,29601,2,77,14,1,42};
UINT4 FsMIBgp4MaxCapDataSize [ ] ={1,3,6,1,4,1,29601,2,77,14,1,43};
UINT4 FsMIBgp4PreferenceValue [ ] ={1,3,6,1,4,1,29601,2,77,14,1,44};
UINT4 FsMIBgp4ContextStatus [ ] ={1,3,6,1,4,1,29601,2,77,14,1,45};
UINT4 FsMIBgp4IBGPMaxPaths [ ] ={1,3,6,1,4,1,29601,2,77,14,1,46};
UINT4 FsMIBgp4EBGPMaxPaths [ ] ={1,3,6,1,4,1,29601,2,77,14,1,47};
UINT4 FsMIBgp4EIBGPMaxPaths [ ] ={1,3,6,1,4,1,29601,2,77,14,1,48};
UINT4 FsMIBgp4OperIBGPMaxPaths [ ] ={1,3,6,1,4,1,29601,2,77,14,1,49};
UINT4 FsMIBgp4OperEBGPMaxPaths [ ] ={1,3,6,1,4,1,29601,2,77,14,1,50};
UINT4 FsMIBgp4OperEIBGPMaxPaths [ ] ={1,3,6,1,4,1,29601,2,77,14,1,51};
UINT4 FsMIBgp4FourByteASNSupportStatus [ ] ={1,3,6,1,4,1,29601,2,77,14,1,52};
UINT4 FsMIBgp4FourByteASNotationType [ ] ={1,3,6,1,4,1,29601,2,77,14,1,53};
UINT4 FsMIBgp4LocalAsNo [ ] ={1,3,6,1,4,1,29601,2,77,14,1,54};
UINT4 FsMIBgp4RIBRoutes [ ] ={1,3,6,1,4,1,29601,2,77,14,1,55};
UINT4 FsMIBgp4Ipv4AddrFamily [ ] ={1,3,6,1,4,1,29601,2,77,14,1,56};
UINT4 FsMIBgp4Ipv6AddrFamily [ ] ={1,3,6,1,4,1,29601,2,77,14,1,57};
UINT4 FsMIBgp4VpnLabelAllocPolicy [ ] ={1,3,6,1,4,1,29601,2,77,14,1,58};
UINT4 FsMIBgp4VPNV4AddrFamily [ ] ={1,3,6,1,4,1,29601,2,77,14,1,59};
UINT4 FsMIBgp4L2vpnAddrFamily [ ] ={1,3,6,1,4,1,29601,2,77,14,1,60};
UINT4 FsMIBgp4EvpnAddrFamily [ ] ={1,3,6,1,4,1,29601,2,77,14,1,61};
UINT4 FsMIBgp4DebugType [ ] ={1,3,6,1,4,1,29601,2,77,14,1,62};
UINT4 FsMIBgp4DebugTypeIPAddr [ ] ={1,3,6,1,4,1,29601,2,77,14,1,63};
UINT4 FsMIBgp4RRDMetricProtocolId [ ] ={1,3,6,1,4,1,29601,2,77,15,1,1};
UINT4 FsMIBgp4RRDMetricValue [ ] ={1,3,6,1,4,1,29601,2,77,15,1,2};
UINT4 FsMIBgp4InFilterCommVal [ ] ={1,3,6,1,4,1,29601,2,77,16,1,1,1};
UINT4 FsMIBgp4CommIncomingFilterStatus [ ] ={1,3,6,1,4,1,29601,2,77,16,1,1,2};
UINT4 FsMIBgp4InFilterRowStatus [ ] ={1,3,6,1,4,1,29601,2,77,16,1,1,3};
UINT4 FsMIBgp4OutFilterCommVal [ ] ={1,3,6,1,4,1,29601,2,77,16,2,1,1};
UINT4 FsMIBgp4CommOutgoingFilterStatus [ ] ={1,3,6,1,4,1,29601,2,77,16,2,1,2};
UINT4 FsMIBgp4OutFilterRowStatus [ ] ={1,3,6,1,4,1,29601,2,77,16,2,1,3};
UINT4 FsMIBgp4ExtCommInFilterCommVal [ ] ={1,3,6,1,4,1,29601,2,77,17,1,1,1};
UINT4 FsMIBgp4ExtCommIncomingFilterStatus [ ] ={1,3,6,1,4,1,29601,2,77,17,1,1,2};
UINT4 FsMIBgp4ExtCommInFilterRowStatus [ ] ={1,3,6,1,4,1,29601,2,77,17,1,1,3};
UINT4 FsMIBgp4ExtCommOutFilterCommVal [ ] ={1,3,6,1,4,1,29601,2,77,17,2,1,1};
UINT4 FsMIBgp4ExtCommOutgoingFilterStatus [ ] ={1,3,6,1,4,1,29601,2,77,17,2,1,2};
UINT4 FsMIBgp4ExtCommOutFilterRowStatus [ ] ={1,3,6,1,4,1,29601,2,77,17,2,1,3};
UINT4 FsMIBgp4TCPMD5AuthPeerType [ ] ={1,3,6,1,4,1,29601,2,77,18,1,1,1};
UINT4 FsMIBgp4TCPMD5AuthPeerAddr [ ] ={1,3,6,1,4,1,29601,2,77,18,1,1,2};
UINT4 FsMIBgp4TCPMD5AuthPassword [ ] ={1,3,6,1,4,1,29601,2,77,18,1,1,3};
UINT4 FsMIBgp4TCPMD5AuthPwdSet [ ] ={1,3,6,1,4,1,29601,2,77,18,1,1,4};
UINT4 FsMIBgpAscConfedPeerASNo [ ] ={1,3,6,1,4,1,29601,2,77,19,1,1,1};
UINT4 FsMIBgpAscConfedPeerStatus [ ] ={1,3,6,1,4,1,29601,2,77,19,1,1,2};
UINT4 FsMIBgp4mpebgpPeerIdentifier [ ] ={1,3,6,1,4,1,29601,2,77,20,1,1};
UINT4 FsMIBgp4mpebgpPeerRemoteAddrType [ ] ={1,3,6,1,4,1,29601,2,77,20,1,2};
UINT4 FsMIBgp4mpebgpPeerLocalAs [ ] ={1,3,6,1,4,1,29601,2,77,20,1,3};
UINT4 FsMIBgp4mpebgpPeerState [ ] ={1,3,6,1,4,1,29601,2,77,20,1,4};
UINT4 FsMIBgp4mpebgpPeerAdminStatus [ ] ={1,3,6,1,4,1,29601,2,77,20,1,5};
UINT4 FsMIBgp4mpebgpPeerNegotiatedVersion [ ] ={1,3,6,1,4,1,29601,2,77,20,1,6};
UINT4 FsMIBgp4mpebgpPeerLocalAddr [ ] ={1,3,6,1,4,1,29601,2,77,20,1,7};
UINT4 FsMIBgp4mpebgpPeerLocalPort [ ] ={1,3,6,1,4,1,29601,2,77,20,1,8};
UINT4 FsMIBgp4mpebgpPeerRemoteAddr [ ] ={1,3,6,1,4,1,29601,2,77,20,1,9};
UINT4 FsMIBgp4mpebgpPeerRemotePort [ ] ={1,3,6,1,4,1,29601,2,77,20,1,10};
UINT4 FsMIBgp4mpebgpPeerRemoteAs [ ] ={1,3,6,1,4,1,29601,2,77,20,1,11};
UINT4 FsMIBgp4mpebgpPeerInUpdates [ ] ={1,3,6,1,4,1,29601,2,77,20,1,12};
UINT4 FsMIBgp4mpebgpPeerOutUpdates [ ] ={1,3,6,1,4,1,29601,2,77,20,1,13};
UINT4 FsMIBgp4mpebgpPeerInTotalMessages [ ] ={1,3,6,1,4,1,29601,2,77,20,1,14};
UINT4 FsMIBgp4mpebgpPeerOutTotalMessages [ ] ={1,3,6,1,4,1,29601,2,77,20,1,15};
UINT4 FsMIBgp4mpebgpPeerLastError [ ] ={1,3,6,1,4,1,29601,2,77,20,1,16};
UINT4 FsMIBgp4mpebgpPeerFsmEstablishedTransitions [ ] ={1,3,6,1,4,1,29601,2,77,20,1,17};
UINT4 FsMIBgp4mpebgpPeerFsmEstablishedTime [ ] ={1,3,6,1,4,1,29601,2,77,20,1,18};
UINT4 FsMIBgp4mpebgpPeerConnectRetryInterval [ ] ={1,3,6,1,4,1,29601,2,77,20,1,19};
UINT4 FsMIBgp4mpebgpPeerHoldTime [ ] ={1,3,6,1,4,1,29601,2,77,20,1,20};
UINT4 FsMIBgp4mpebgpPeerKeepAlive [ ] ={1,3,6,1,4,1,29601,2,77,20,1,21};
UINT4 FsMIBgp4mpebgpPeerHoldTimeConfigured [ ] ={1,3,6,1,4,1,29601,2,77,20,1,22};
UINT4 FsMIBgp4mpebgpPeerKeepAliveConfigured [ ] ={1,3,6,1,4,1,29601,2,77,20,1,23};
UINT4 FsMIBgp4mpebgpPeerMinASOriginationInterval [ ] ={1,3,6,1,4,1,29601,2,77,20,1,24};
UINT4 FsMIBgp4mpebgpPeerMinRouteAdvertisementInterval [ ] ={1,3,6,1,4,1,29601,2,77,20,1,25};
UINT4 FsMIBgp4mpebgpPeerInUpdateElapsedTime [ ] ={1,3,6,1,4,1,29601,2,77,20,1,26};
UINT4 FsMIBgp4mpebgpPeerEndOfRIBMarkerSentStatus [ ] ={1,3,6,1,4,1,29601,2,77,20,1,27};
UINT4 FsMIBgp4mpebgpPeerEndOfRIBMarkerReceivedStatus [ ] ={1,3,6,1,4,1,29601,2,77,20,1,28};
UINT4 FsMIBgp4mpebgpPeerRestartMode [ ] ={1,3,6,1,4,1,29601,2,77,20,1,29};
UINT4 FsMIBgp4mpePeerRestartTimeInterval [ ] ={1,3,6,1,4,1,29601,2,77,20,1,30};
UINT4 FsMIBgp4mpePeerAllowAutomaticStart [ ] ={1,3,6,1,4,1,29601,2,77,20,1,31};
UINT4 FsMIBgp4mpePeerAllowAutomaticStop [ ] ={1,3,6,1,4,1,29601,2,77,20,1,32};
UINT4 FsMIBgp4mpebgpPeerIdleHoldTimeConfigured [ ] ={1,3,6,1,4,1,29601,2,77,20,1,33};
UINT4 FsMIBgp4mpeDampPeerOscillations [ ] ={1,3,6,1,4,1,29601,2,77,20,1,34};
UINT4 FsMIBgp4mpePeerDelayOpen [ ] ={1,3,6,1,4,1,29601,2,77,20,1,35};
UINT4 FsMIBgp4mpebgpPeerDelayOpenTimeConfigured [ ] ={1,3,6,1,4,1,29601,2,77,20,1,36};
UINT4 FsMIBgp4mpePeerPrefixUpperLimit [ ] ={1,3,6,1,4,1,29601,2,77,20,1,37};
UINT4 FsMIBgp4mpePeerTcpConnectRetryCnt [ ] ={1,3,6,1,4,1,29601,2,77,20,1,38};
UINT4 FsMIBgp4mpePeerTcpCurrentConnectRetryCnt [ ] ={1,3,6,1,4,1,29601,2,77,20,1,39};
UINT4 FsMIBgp4mpeIsPeerDamped [ ] ={1,3,6,1,4,1,29601,2,77,20,1,40};
UINT4 FsMIBgp4mpePeerSessionAuthStatus [ ] ={1,3,6,1,4,1,29601,2,77,20,1,41};
UINT4 FsMIBgp4mpePeerTCPAOKeyIdInUse [ ] ={1,3,6,1,4,1,29601,2,77,20,1,42};
UINT4 FsMIBgp4mpePeerTCPAOAuthNoMKTDiscard [ ] ={1,3,6,1,4,1,29601,2,77,20,1,43};
UINT4 FsMIBgp4mpePeerTCPAOAuthICMPAccept [ ] ={1,3,6,1,4,1,29601,2,77,20,1,44};
UINT4 FsMIBgp4mpePeerIpPrefixNameIn [ ] ={1,3,6,1,4,1,29601,2,77,20,1,45};
UINT4 FsMIBgp4mpePeerIpPrefixNameOut [ ] ={1,3,6,1,4,1,29601,2,77,20,1,46};
UINT4 FsMIBgp4mpePeerBfdStatus [ ] ={1,3,6,1,4,1,29601,2,77,20,1,47};
UINT4 FsMIBgp4mpePeerHoldAdvtRoutes [ ] ={1,3,6,1,4,1,29601,2,77,20,1,48};
UINT4 FsMIBgp4mpebgp4PathAttrRouteAfi [ ] ={1,3,6,1,4,1,29601,2,77,21,1,1};
UINT4 FsMIBgp4mpebgp4PathAttrRouteSafi [ ] ={1,3,6,1,4,1,29601,2,77,21,1,2};
UINT4 FsMIBgp4mpebgp4PathAttrPeerType [ ] ={1,3,6,1,4,1,29601,2,77,21,1,3};
UINT4 FsMIBgp4mpebgp4PathAttrPeer [ ] ={1,3,6,1,4,1,29601,2,77,21,1,4};
UINT4 FsMIBgp4mpebgp4PathAttrIpAddrPrefixLen [ ] ={1,3,6,1,4,1,29601,2,77,21,1,5};
UINT4 FsMIBgp4mpebgp4PathAttrIpAddrPrefix [ ] ={1,3,6,1,4,1,29601,2,77,21,1,6};
UINT4 FsMIBgp4mpebgp4PathAttrOrigin [ ] ={1,3,6,1,4,1,29601,2,77,21,1,7};
UINT4 FsMIBgp4mpebgp4PathAttrASPathSegment [ ] ={1,3,6,1,4,1,29601,2,77,21,1,8};
UINT4 FsMIBgp4mpebgp4PathAttrNextHop [ ] ={1,3,6,1,4,1,29601,2,77,21,1,9};
UINT4 FsMIBgp4mpebgp4PathAttrMultiExitDisc [ ] ={1,3,6,1,4,1,29601,2,77,21,1,10};
UINT4 FsMIBgp4mpebgp4PathAttrLocalPref [ ] ={1,3,6,1,4,1,29601,2,77,21,1,11};
UINT4 FsMIBgp4mpebgp4PathAttrAtomicAggregate [ ] ={1,3,6,1,4,1,29601,2,77,21,1,12};
UINT4 FsMIBgp4mpebgp4PathAttrAggregatorAS [ ] ={1,3,6,1,4,1,29601,2,77,21,1,13};
UINT4 FsMIBgp4mpebgp4PathAttrAggregatorAddr [ ] ={1,3,6,1,4,1,29601,2,77,21,1,14};
UINT4 FsMIBgp4mpebgp4PathAttrCalcLocalPref [ ] ={1,3,6,1,4,1,29601,2,77,21,1,15};
UINT4 FsMIBgp4mpebgp4PathAttrBest [ ] ={1,3,6,1,4,1,29601,2,77,21,1,16};
UINT4 FsMIBgp4mpebgp4PathAttrCommunity [ ] ={1,3,6,1,4,1,29601,2,77,21,1,17};
UINT4 FsMIBgp4mpebgp4PathAttrOriginatorId [ ] ={1,3,6,1,4,1,29601,2,77,21,1,18};
UINT4 FsMIBgp4mpebgp4PathAttrClusterList [ ] ={1,3,6,1,4,1,29601,2,77,21,1,19};
UINT4 FsMIBgp4mpebgp4PathAttrExtCommunity [ ] ={1,3,6,1,4,1,29601,2,77,21,1,20};
UINT4 FsMIBgp4mpebgp4PathAttrUnknown [ ] ={1,3,6,1,4,1,29601,2,77,21,1,21};
UINT4 FsMIBgp4mpebgp4PathAttrLabel [ ] ={1,3,6,1,4,1,29601,2,77,21,1,22};
UINT4 FsMIBgp4mpebgp4PathAttrAS4PathSegment [ ] ={1,3,6,1,4,1,29601,2,77,21,1,23};
UINT4 FsMIBgp4mpebgp4PathAttrAggregatorAS4 [ ] ={1,3,6,1,4,1,29601,2,77,21,1,24};
UINT4 FsMIBgp4mpePeerExtPeerType [ ] ={1,3,6,1,4,1,29601,2,77,22,1,1};
UINT4 FsMIBgp4mpePeerExtPeerRemoteAddr [ ] ={1,3,6,1,4,1,29601,2,77,22,1,2};
UINT4 FsMIBgp4mpePeerExtConfigurePeer [ ] ={1,3,6,1,4,1,29601,2,77,22,1,3};
UINT4 FsMIBgp4mpePeerExtPeerRemoteAs [ ] ={1,3,6,1,4,1,29601,2,77,22,1,4};
UINT4 FsMIBgp4mpePeerExtEBGPMultiHop [ ] ={1,3,6,1,4,1,29601,2,77,22,1,5};
UINT4 FsMIBgp4mpePeerExtEBGPHopLimit [ ] ={1,3,6,1,4,1,29601,2,77,22,1,6};
UINT4 FsMIBgp4mpePeerExtNextHopSelf [ ] ={1,3,6,1,4,1,29601,2,77,22,1,7};
UINT4 FsMIBgp4mpePeerExtRflClient [ ] ={1,3,6,1,4,1,29601,2,77,22,1,8};
UINT4 FsMIBgp4mpePeerExtTcpSendBufSize [ ] ={1,3,6,1,4,1,29601,2,77,22,1,9};
UINT4 FsMIBgp4mpePeerExtTcpRcvBufSize [ ] ={1,3,6,1,4,1,29601,2,77,22,1,10};
UINT4 FsMIBgp4mpePeerExtLclAddress [ ] ={1,3,6,1,4,1,29601,2,77,22,1,11};
UINT4 FsMIBgp4mpePeerExtNetworkAddress [ ] ={1,3,6,1,4,1,29601,2,77,22,1,12};
UINT4 FsMIBgp4mpePeerExtGateway [ ] ={1,3,6,1,4,1,29601,2,77,22,1,13};
UINT4 FsMIBgp4mpePeerExtCommSendStatus [ ] ={1,3,6,1,4,1,29601,2,77,22,1,14};
UINT4 FsMIBgp4mpePeerExtECommSendStatus [ ] ={1,3,6,1,4,1,29601,2,77,22,1,15};
UINT4 FsMIBgp4mpePeerExtPassive [ ] ={1,3,6,1,4,1,29601,2,77,22,1,16};
UINT4 FsMIBgp4mpePeerExtDefaultOriginate [ ] ={1,3,6,1,4,1,29601,2,77,22,1,17};
UINT4 FsMIBgp4mpePeerExtOverrideCapability [ ] ={1,3,6,1,4,1,29601,2,77,22,1,18};
UINT4 FsMIBgp4mpeMEDIndex [ ] ={1,3,6,1,4,1,29601,2,77,23,1,1};
UINT4 FsMIBgp4mpeMEDAdminStatus [ ] ={1,3,6,1,4,1,29601,2,77,23,1,2};
UINT4 FsMIBgp4mpeMEDRemoteAS [ ] ={1,3,6,1,4,1,29601,2,77,23,1,3};
UINT4 FsMIBgp4mpeMEDIPAddrAfi [ ] ={1,3,6,1,4,1,29601,2,77,23,1,4};
UINT4 FsMIBgp4mpeMEDIPAddrSafi [ ] ={1,3,6,1,4,1,29601,2,77,23,1,5};
UINT4 FsMIBgp4mpeMEDIPAddrPrefix [ ] ={1,3,6,1,4,1,29601,2,77,23,1,6};
UINT4 FsMIBgp4mpeMEDIPAddrPrefixLen [ ] ={1,3,6,1,4,1,29601,2,77,23,1,7};
UINT4 FsMIBgp4mpeMEDIntermediateAS [ ] ={1,3,6,1,4,1,29601,2,77,23,1,8};
UINT4 FsMIBgp4mpeMEDDirection [ ] ={1,3,6,1,4,1,29601,2,77,23,1,9};
UINT4 FsMIBgp4mpeMEDValue [ ] ={1,3,6,1,4,1,29601,2,77,23,1,10};
UINT4 FsMIBgp4mpeMEDPreference [ ] ={1,3,6,1,4,1,29601,2,77,23,1,11};
UINT4 FsMIBgp4mpeMEDVrfName [ ] ={1,3,6,1,4,1,29601,2,77,23,1,12};
UINT4 FsMIBgp4mpeLocalPrefIndex [ ] ={1,3,6,1,4,1,29601,2,77,24,1,1};
UINT4 FsMIBgp4mpeLocalPrefAdminStatus [ ] ={1,3,6,1,4,1,29601,2,77,24,1,2};
UINT4 FsMIBgp4mpeLocalPrefRemoteAS [ ] ={1,3,6,1,4,1,29601,2,77,24,1,3};
UINT4 FsMIBgp4mpeLocalPrefIPAddrAfi [ ] ={1,3,6,1,4,1,29601,2,77,24,1,4};
UINT4 FsMIBgp4mpeLocalPrefIPAddrSafi [ ] ={1,3,6,1,4,1,29601,2,77,24,1,5};
UINT4 FsMIBgp4mpeLocalPrefIPAddrPrefix [ ] ={1,3,6,1,4,1,29601,2,77,24,1,6};
UINT4 FsMIBgp4mpeLocalPrefIPAddrPrefixLen [ ] ={1,3,6,1,4,1,29601,2,77,24,1,7};
UINT4 FsMIBgp4mpeLocalPrefIntermediateAS [ ] ={1,3,6,1,4,1,29601,2,77,24,1,8};
UINT4 FsMIBgp4mpeLocalPrefDirection [ ] ={1,3,6,1,4,1,29601,2,77,24,1,9};
UINT4 FsMIBgp4mpeLocalPrefValue [ ] ={1,3,6,1,4,1,29601,2,77,24,1,10};
UINT4 FsMIBgp4mpeLocalPrefPreference [ ] ={1,3,6,1,4,1,29601,2,77,24,1,11};
UINT4 FsMIBgp4mpeLocalPrefVrfName [ ] ={1,3,6,1,4,1,29601,2,77,24,1,12};
UINT4 FsMIBgp4mpeUpdateFilterIndex [ ] ={1,3,6,1,4,1,29601,2,77,25,1,1};
UINT4 FsMIBgp4mpeUpdateFilterAdminStatus [ ] ={1,3,6,1,4,1,29601,2,77,25,1,2};
UINT4 FsMIBgp4mpeUpdateFilterRemoteAS [ ] ={1,3,6,1,4,1,29601,2,77,25,1,3};
UINT4 FsMIBgp4mpeUpdateFilterIPAddrAfi [ ] ={1,3,6,1,4,1,29601,2,77,25,1,4};
UINT4 FsMIBgp4mpeUpdateFilterIPAddrSafi [ ] ={1,3,6,1,4,1,29601,2,77,25,1,5};
UINT4 FsMIBgp4mpeUpdateFilterIPAddrPrefix [ ] ={1,3,6,1,4,1,29601,2,77,25,1,6};
UINT4 FsMIBgp4mpeUpdateFilterIPAddrPrefixLen [ ] ={1,3,6,1,4,1,29601,2,77,25,1,7};
UINT4 FsMIBgp4mpeUpdateFilterIntermediateAS [ ] ={1,3,6,1,4,1,29601,2,77,25,1,8};
UINT4 FsMIBgp4mpeUpdateFilterDirection [ ] ={1,3,6,1,4,1,29601,2,77,25,1,9};
UINT4 FsMIBgp4mpeUpdateFilterAction [ ] ={1,3,6,1,4,1,29601,2,77,25,1,10};
UINT4 FsMIBgp4mpeUpdateFilterVrfName [ ] ={1,3,6,1,4,1,29601,2,77,25,1,11};
UINT4 FsMIBgp4mpeAggregateIndex [ ] ={1,3,6,1,4,1,29601,2,77,26,1,1};
UINT4 FsMIBgp4mpeAggregateAdminStatus [ ] ={1,3,6,1,4,1,29601,2,77,26,1,2};
UINT4 FsMIBgp4mpeAggregateIPAddrAfi [ ] ={1,3,6,1,4,1,29601,2,77,26,1,3};
UINT4 FsMIBgp4mpeAggregateIPAddrSafi [ ] ={1,3,6,1,4,1,29601,2,77,26,1,4};
UINT4 FsMIBgp4mpeAggregateIPAddrPrefix [ ] ={1,3,6,1,4,1,29601,2,77,26,1,5};
UINT4 FsMIBgp4mpeAggregateIPAddrPrefixLen [ ] ={1,3,6,1,4,1,29601,2,77,26,1,6};
UINT4 FsMIBgp4mpeAggregateAdvertise [ ] ={1,3,6,1,4,1,29601,2,77,26,1,7};
UINT4 FsMIBgp4mpeAggregateVrfName [ ] ={1,3,6,1,4,1,29601,2,77,26,1,8};
UINT4 FsMIBgp4mpeAggregateAsSet [ ] ={1,3,6,1,4,1,29601,2,77,26,1,9};
UINT4 FsMIBgp4mpeAggregateAdvertiseRouteMapName [ ] ={1,3,6,1,4,1,29601,2,77,26,1,10};
UINT4 FsMIBgp4mpeAggregateSuppressRouteMapName [ ] ={1,3,6,1,4,1,29601,2,77,26,1,11};
UINT4 FsMIBgp4mpeAggregateAttributeRouteMapName [ ] ={1,3,6,1,4,1,29601,2,77,26,1,12};
UINT4 FsMIBgp4mpeImportRoutePrefixAfi [ ] ={1,3,6,1,4,1,29601,2,77,27,1,1};
UINT4 FsMIBgp4mpeImportRoutePrefixSafi [ ] ={1,3,6,1,4,1,29601,2,77,27,1,2};
UINT4 FsMIBgp4mpeImportRoutePrefix [ ] ={1,3,6,1,4,1,29601,2,77,27,1,3};
UINT4 FsMIBgp4mpeImportRoutePrefixLen [ ] ={1,3,6,1,4,1,29601,2,77,27,1,4};
UINT4 FsMIBgp4mpeImportRouteProtocol [ ] ={1,3,6,1,4,1,29601,2,77,27,1,5};
UINT4 FsMIBgp4mpeImportRouteNextHop [ ] ={1,3,6,1,4,1,29601,2,77,27,1,6};
UINT4 FsMIBgp4mpeImportRouteIfIndex [ ] ={1,3,6,1,4,1,29601,2,77,27,1,7};
UINT4 FsMIBgp4mpeImportRouteMetric [ ] ={1,3,6,1,4,1,29601,2,77,27,1,8};
UINT4 FsMIBgp4mpeImportRouteVrf [ ] ={1,3,6,1,4,1,29601,2,77,27,1,9};
UINT4 FsMIBgp4mpeImportRouteAction [ ] ={1,3,6,1,4,1,29601,2,77,27,1,10};
UINT4 FsMIBgp4mpePeerType [ ] ={1,3,6,1,4,1,29601,2,77,28,1,1};
UINT4 FsMIBgp4mpePeer [ ] ={1,3,6,1,4,1,29601,2,77,28,1,2};
UINT4 FsMIBgp4mpeFsmTransitionHist [ ] ={1,3,6,1,4,1,29601,2,77,28,1,3};
UINT4 FsMIBgp4mpePathAttrAddrPrefixAfi [ ] ={1,3,6,1,4,1,29601,2,77,29,1,1,1};
UINT4 FsMIBgp4mpePathAttrAddrPrefixSafi [ ] ={1,3,6,1,4,1,29601,2,77,29,1,1,2};
UINT4 FsMIBgp4mpePathAttrAddrPrefix [ ] ={1,3,6,1,4,1,29601,2,77,29,1,1,3};
UINT4 FsMIBgp4mpePathAttrAddrPrefixLen [ ] ={1,3,6,1,4,1,29601,2,77,29,1,1,4};
UINT4 FsMIBgp4mpePathAttrPeerType [ ] ={1,3,6,1,4,1,29601,2,77,29,1,1,5};
UINT4 FsMIBgp4mpePathAttrPeer [ ] ={1,3,6,1,4,1,29601,2,77,29,1,1,6};
UINT4 FsMIBgp4mpeRfdRtFom [ ] ={1,3,6,1,4,1,29601,2,77,29,1,1,7};
UINT4 FsMIBgp4mpeRfdRtLastUpdtTime [ ] ={1,3,6,1,4,1,29601,2,77,29,1,1,8};
UINT4 FsMIBgp4mpeRfdRtState [ ] ={1,3,6,1,4,1,29601,2,77,29,1,1,9};
UINT4 FsMIBgp4mpeRfdRtStatus [ ] ={1,3,6,1,4,1,29601,2,77,29,1,1,10};
UINT4 FsMIBgp4mpeRfdRtFlapCount [ ] ={1,3,6,1,4,1,29601,2,77,29,1,1,11};
UINT4 FsMIBgp4mpeRfdRtFlapTime [ ] ={1,3,6,1,4,1,29601,2,77,29,1,1,12};
UINT4 FsMIBgp4mpeRfdRtReuseTime [ ] ={1,3,6,1,4,1,29601,2,77,29,1,1,13};
UINT4 FsMIBgp4mpePeerRemoteIpAddrType [ ] ={1,3,6,1,4,1,29601,2,77,29,2,1,1};
UINT4 FsMIBgp4mpePeerRemoteIpAddr [ ] ={1,3,6,1,4,1,29601,2,77,29,2,1,2};
UINT4 FsMIBgp4mpeRfdPeerFom [ ] ={1,3,6,1,4,1,29601,2,77,29,2,1,3};
UINT4 FsMIBgp4mpeRfdPeerLastUpdtTime [ ] ={1,3,6,1,4,1,29601,2,77,29,2,1,4};
UINT4 FsMIBgp4mpeRfdPeerState [ ] ={1,3,6,1,4,1,29601,2,77,29,2,1,5};
UINT4 FsMIBgp4mpeRfdPeerStatus [ ] ={1,3,6,1,4,1,29601,2,77,29,2,1,6};
UINT4 FsMIBgp4mpeRtAfi [ ] ={1,3,6,1,4,1,29601,2,77,29,3,1,1};
UINT4 FsMIBgp4mpeRtSafi [ ] ={1,3,6,1,4,1,29601,2,77,29,3,1,2};
UINT4 FsMIBgp4mpeRtIPPrefix [ ] ={1,3,6,1,4,1,29601,2,77,29,3,1,3};
UINT4 FsMIBgp4mpeRtIPPrefixLen [ ] ={1,3,6,1,4,1,29601,2,77,29,3,1,4};
UINT4 FsMIBgp4mpeRfdRtsReusePeerType [ ] ={1,3,6,1,4,1,29601,2,77,29,3,1,5};
UINT4 FsMIBgp4mpePeerRemAddress [ ] ={1,3,6,1,4,1,29601,2,77,29,3,1,6};
UINT4 FsMIBgp4mpeRfdRtReuseListRtFom [ ] ={1,3,6,1,4,1,29601,2,77,29,3,1,7};
UINT4 FsMIBgp4mpeRfdRtReuseListRtLastUpdtTime [ ] ={1,3,6,1,4,1,29601,2,77,29,3,1,8};
UINT4 FsMIBgp4mpeRfdRtReuseListRtState [ ] ={1,3,6,1,4,1,29601,2,77,29,3,1,9};
UINT4 FsMIBgp4mpeRfdRtReuseListRtStatus [ ] ={1,3,6,1,4,1,29601,2,77,29,3,1,10};
UINT4 FsMIBgp4mpeRfdPeerRemIpAddrType [ ] ={1,3,6,1,4,1,29601,2,77,29,4,1,1};
UINT4 FsMIBgp4mpeRfdPeerRemIpAddr [ ] ={1,3,6,1,4,1,29601,2,77,29,4,1,2};
UINT4 FsMIBgp4mpeRfdPeerReuseListPeerFom [ ] ={1,3,6,1,4,1,29601,2,77,29,4,1,3};
UINT4 FsMIBgp4mpeRfdPeerReuseListLastUpdtTime [ ] ={1,3,6,1,4,1,29601,2,77,29,4,1,4};
UINT4 FsMIBgp4mpeRfdPeerReuseListPeerState [ ] ={1,3,6,1,4,1,29601,2,77,29,4,1,5};
UINT4 FsMIBgp4mpeRfdPeerReuseListPeerStatus [ ] ={1,3,6,1,4,1,29601,2,77,29,4,1,6};
UINT4 FsMIBgp4mpeAddCommRtAfi [ ] ={1,3,6,1,4,1,29601,2,77,30,1,1,1};
UINT4 FsMIBgp4mpeAddCommRtSafi [ ] ={1,3,6,1,4,1,29601,2,77,30,1,1,2};
UINT4 FsMIBgp4mpeAddCommIpNetwork [ ] ={1,3,6,1,4,1,29601,2,77,30,1,1,3};
UINT4 FsMIBgp4mpeAddCommIpPrefixLen [ ] ={1,3,6,1,4,1,29601,2,77,30,1,1,4};
UINT4 FsMIBgp4mpeAddCommVal [ ] ={1,3,6,1,4,1,29601,2,77,30,1,1,5};
UINT4 FsMIBgp4mpeAddCommRowStatus [ ] ={1,3,6,1,4,1,29601,2,77,30,1,1,6};
UINT4 FsMIBgp4mpeDeleteCommRtAfi [ ] ={1,3,6,1,4,1,29601,2,77,30,2,1,1};
UINT4 FsMIBgp4mpeDeleteCommRtSafi [ ] ={1,3,6,1,4,1,29601,2,77,30,2,1,2};
UINT4 FsMIBgp4mpeDeleteCommIpNetwork [ ] ={1,3,6,1,4,1,29601,2,77,30,2,1,3};
UINT4 FsMIBgp4mpeDeleteCommIpPrefixLen [ ] ={1,3,6,1,4,1,29601,2,77,30,2,1,4};
UINT4 FsMIBgp4mpeDeleteCommVal [ ] ={1,3,6,1,4,1,29601,2,77,30,2,1,5};
UINT4 FsMIBgp4mpeDeleteCommRowStatus [ ] ={1,3,6,1,4,1,29601,2,77,30,2,1,6};
UINT4 FsMIBgp4mpeCommSetStatusAfi [ ] ={1,3,6,1,4,1,29601,2,77,30,3,1,1};
UINT4 FsMIBgp4mpeCommSetStatusSafi [ ] ={1,3,6,1,4,1,29601,2,77,30,3,1,2};
UINT4 FsMIBgp4mpeCommSetStatusIpNetwork [ ] ={1,3,6,1,4,1,29601,2,77,30,3,1,3};
UINT4 FsMIBgp4mpeCommSetStatusIpPrefixLen [ ] ={1,3,6,1,4,1,29601,2,77,30,3,1,4};
UINT4 FsMIBgp4mpeCommSetStatus [ ] ={1,3,6,1,4,1,29601,2,77,30,3,1,5};
UINT4 FsMIBgp4mpeCommSetStatusRowStatus [ ] ={1,3,6,1,4,1,29601,2,77,30,3,1,6};
UINT4 FsMIBgp4mpeAddExtCommRtAfi [ ] ={1,3,6,1,4,1,29601,2,77,31,1,1,1};
UINT4 FsMIBgp4mpeAddExtCommRtSafi [ ] ={1,3,6,1,4,1,29601,2,77,31,1,1,2};
UINT4 FsMIBgp4mpeAddExtCommIpNetwork [ ] ={1,3,6,1,4,1,29601,2,77,31,1,1,3};
UINT4 FsMIBgp4mpeAddExtCommIpPrefixLen [ ] ={1,3,6,1,4,1,29601,2,77,31,1,1,4};
UINT4 FsMIBgp4mpeAddExtCommVal [ ] ={1,3,6,1,4,1,29601,2,77,31,1,1,5};
UINT4 FsMIBgp4mpeAddExtCommRowStatus [ ] ={1,3,6,1,4,1,29601,2,77,31,1,1,6};
UINT4 FsMIBgp4mpeDeleteExtCommRtAfi [ ] ={1,3,6,1,4,1,29601,2,77,31,2,1,1};
UINT4 FsMIBgp4mpeDeleteExtCommRtSafi [ ] ={1,3,6,1,4,1,29601,2,77,31,2,1,2};
UINT4 FsMIBgp4mpeDeleteExtCommIpNetwork [ ] ={1,3,6,1,4,1,29601,2,77,31,2,1,3};
UINT4 FsMIBgp4mpeDeleteExtCommIpPrefixLen [ ] ={1,3,6,1,4,1,29601,2,77,31,2,1,4};
UINT4 FsMIBgp4mpeDeleteExtCommVal [ ] ={1,3,6,1,4,1,29601,2,77,31,2,1,5};
UINT4 FsMIBgp4mpeDeleteExtCommRowStatus [ ] ={1,3,6,1,4,1,29601,2,77,31,2,1,6};
UINT4 FsMIBgp4mpeExtCommSetStatusRtAfi [ ] ={1,3,6,1,4,1,29601,2,77,31,3,1,1};
UINT4 FsMIBgp4mpeExtCommSetStatusRtSafi [ ] ={1,3,6,1,4,1,29601,2,77,31,3,1,2};
UINT4 FsMIBgp4mpeExtCommSetStatusIpNetwork [ ] ={1,3,6,1,4,1,29601,2,77,31,3,1,3};
UINT4 FsMIBgp4mpeExtCommSetStatusIpPrefixLen [ ] ={1,3,6,1,4,1,29601,2,77,31,3,1,4};
UINT4 FsMIBgp4mpeExtCommSetStatus [ ] ={1,3,6,1,4,1,29601,2,77,31,3,1,5};
UINT4 FsMIBgp4mpeExtCommSetStatusRowStatus [ ] ={1,3,6,1,4,1,29601,2,77,31,3,1,6};
UINT4 FsMIBgp4mpePeerLinkType [ ] ={1,3,6,1,4,1,29601,2,77,31,4,1,1};
UINT4 FsMIBgp4mpePeerLinkRemAddr [ ] ={1,3,6,1,4,1,29601,2,77,31,4,1,2};
UINT4 FsMIBgp4mpeLinkBandWidth [ ] ={1,3,6,1,4,1,29601,2,77,31,4,1,3};
UINT4 FsMIBgp4mpePeerLinkBwRowStatus [ ] ={1,3,6,1,4,1,29601,2,77,31,4,1,4};
UINT4 FsMIBgp4mpeCapPeerType [ ] ={1,3,6,1,4,1,29601,2,77,32,1,1,1};
UINT4 FsMIBgp4mpeCapPeerRemoteIpAddr [ ] ={1,3,6,1,4,1,29601,2,77,32,1,1,2};
UINT4 FsMIBgp4mpeSupportedCapabilityCode [ ] ={1,3,6,1,4,1,29601,2,77,32,1,1,3};
UINT4 FsMIBgp4mpeSupportedCapabilityLength [ ] ={1,3,6,1,4,1,29601,2,77,32,1,1,4};
UINT4 FsMIBgp4mpeSupportedCapabilityValue [ ] ={1,3,6,1,4,1,29601,2,77,32,1,1,5};
UINT4 FsMIBgp4mpeCapSupportedCapsRowStatus [ ] ={1,3,6,1,4,1,29601,2,77,32,1,1,6};
UINT4 FsMIBgp4mpeCapAnnouncedStatus [ ] ={1,3,6,1,4,1,29601,2,77,32,1,1,7};
UINT4 FsMIBgp4mpeCapReceivedStatus [ ] ={1,3,6,1,4,1,29601,2,77,32,1,1,8};
UINT4 FsMIBgp4mpeCapNegotiatedStatus [ ] ={1,3,6,1,4,1,29601,2,77,32,1,1,9};
UINT4 FsMIBgp4mpeCapConfiguredStatus [ ] ={1,3,6,1,4,1,29601,2,77,32,1,1,10};
UINT4 FsMIBgp4mpeRtRefreshInboundPeerType [ ] ={1,3,6,1,4,1,29601,2,77,33,1,1,1};
UINT4 FsMIBgp4mpeRtRefreshInboundPeerAddr [ ] ={1,3,6,1,4,1,29601,2,77,33,1,1,2};
UINT4 FsMIBgp4mpeRtRefreshInboundAfi [ ] ={1,3,6,1,4,1,29601,2,77,33,1,1,3};
UINT4 FsMIBgp4mpeRtRefreshInboundSafi [ ] ={1,3,6,1,4,1,29601,2,77,33,1,1,4};
UINT4 FsMIBgp4mpeRtRefreshInboundRequest [ ] ={1,3,6,1,4,1,29601,2,77,33,1,1,5};
UINT4 FsMIBgp4mpeRtRefreshInboundPrefixFilter [ ] ={1,3,6,1,4,1,29601,2,77,33,1,1,6};
UINT4 FsMIBgp4mpeRtRefreshStatisticsPeerType [ ] ={1,3,6,1,4,1,29601,2,77,33,2,1,1};
UINT4 FsMIBgp4mpeRtRefreshStatisticsPeerAddr [ ] ={1,3,6,1,4,1,29601,2,77,33,2,1,2};
UINT4 FsMIBgp4mpeRtRefreshStatisticsAfi [ ] ={1,3,6,1,4,1,29601,2,77,33,2,1,3};
UINT4 FsMIBgp4mpeRtRefreshStatisticsSafi [ ] ={1,3,6,1,4,1,29601,2,77,33,2,1,4};
UINT4 FsMIBgp4mpeRtRefreshStatisticsRtRefMsgSentCntr [ ] ={1,3,6,1,4,1,29601,2,77,33,2,1,5};
UINT4 FsMIBgp4mpeRtRefreshStatisticsRtRefMsgTxErrCntr [ ] ={1,3,6,1,4,1,29601,2,77,33,2,1,6};
UINT4 FsMIBgp4mpeRtRefreshStatisticsRtRefMsgRcvdCntr [ ] ={1,3,6,1,4,1,29601,2,77,33,2,1,7};
UINT4 FsMIBgp4mpeRtRefreshStatisticsRtRefMsgInvalidCntr [ ] ={1,3,6,1,4,1,29601,2,77,33,2,1,8};
UINT4 FsMIBgp4mpeSoftReconfigOutboundPeerType [ ] ={1,3,6,1,4,1,29601,2,77,34,1,1,1};
UINT4 FsMIBgp4mpeSoftReconfigOutboundPeerAddr [ ] ={1,3,6,1,4,1,29601,2,77,34,1,1,2};
UINT4 FsMIBgp4mpeSoftReconfigOutboundAfi [ ] ={1,3,6,1,4,1,29601,2,77,34,1,1,3};
UINT4 FsMIBgp4mpeSoftReconfigOutboundSafi [ ] ={1,3,6,1,4,1,29601,2,77,34,1,1,4};
UINT4 FsMIBgp4mpeSoftReconfigOutboundRequest [ ] ={1,3,6,1,4,1,29601,2,77,34,1,1,5};
UINT4 FsMIBgp4MpePeerRemoteAddrType [ ] ={1,3,6,1,4,1,29601,2,77,35,1,1};
UINT4 FsMIBgp4MpePeerRemoteAddr [ ] ={1,3,6,1,4,1,29601,2,77,35,1,2};
UINT4 FsMIBgp4MpePrefixCountersAfi [ ] ={1,3,6,1,4,1,29601,2,77,35,1,3};
UINT4 FsMIBgp4MpePrefixCountersSafi [ ] ={1,3,6,1,4,1,29601,2,77,35,1,4};
UINT4 FsMIBgp4MpePrefixCountersPrefixesReceived [ ] ={1,3,6,1,4,1,29601,2,77,35,1,5};
UINT4 FsMIBgp4MpePrefixCountersPrefixesSent [ ] ={1,3,6,1,4,1,29601,2,77,35,1,6};
UINT4 FsMIBgp4MpePrefixCountersWithdrawsReceived [ ] ={1,3,6,1,4,1,29601,2,77,35,1,7};
UINT4 FsMIBgp4MpePrefixCountersWithdrawsSent [ ] ={1,3,6,1,4,1,29601,2,77,35,1,8};
UINT4 FsMIBgp4MpePrefixCountersInPrefixes [ ] ={1,3,6,1,4,1,29601,2,77,35,1,9};
UINT4 FsMIBgp4MpePrefixCountersInPrefixesAccepted [ ] ={1,3,6,1,4,1,29601,2,77,35,1,10};
UINT4 FsMIBgp4MpePrefixCountersInPrefixesRejected [ ] ={1,3,6,1,4,1,29601,2,77,35,1,11};
UINT4 FsMIBgp4MpePrefixCountersOutPrefixes [ ] ={1,3,6,1,4,1,29601,2,77,35,1,12};
UINT4 FsMIBgp4DistInOutRouteMapName [ ] ={1,3,6,1,4,1,29601,2,77,36,1,1,1};
UINT4 FsMIBgp4DistInOutRouteMapType [ ] ={1,3,6,1,4,1,29601,2,77,36,1,1,2};
UINT4 FsMIBgp4DistInOutRouteMapValue [ ] ={1,3,6,1,4,1,29601,2,77,36,1,1,3};
UINT4 FsMIBgp4DistInOutRouteMapRowStatus [ ] ={1,3,6,1,4,1,29601,2,77,36,1,1,4};
UINT4 FsMIBgp4TrapContextId [ ] ={1,3,6,1,4,1,29601,2,77,37,1,1};
UINT4 FsMIBgp4TrapRouteAddressType [ ] ={1,3,6,1,4,1,29601,2,77,37,1,2};
UINT4 FsMIBgp4TrapRoutePrefix [ ] ={1,3,6,1,4,1,29601,2,77,37,1,3};
UINT4 FsMIBgp4NextHop [ ] ={1,3,6,1,4,1,29601,2,77,37,1,4};
UINT4 FsMIBgp4TrapPeerAddrType [ ] ={1,3,6,1,4,1,29601,2,77,37,1,5};
UINT4 FsMIBgp4TrapPeerAddr [ ] ={1,3,6,1,4,1,29601,2,77,37,1,6};
UINT4 FsMIBgp4NeighborRouteMapPeerAddrType [ ] ={1,3,6,1,4,1,29601,2,77,38,1,1,1};
UINT4 FsMIBgp4NeighborRouteMapPeer [ ] ={1,3,6,1,4,1,29601,2,77,38,1,1,2};
UINT4 FsMIBgp4NeighborRouteMapDirection [ ] ={1,3,6,1,4,1,29601,2,77,38,1,1,3};
UINT4 FsMIBgp4NeighborRouteMapName [ ] ={1,3,6,1,4,1,29601,2,77,38,1,1,4};
UINT4 FsMIBgp4NeighborRouteMapRowStatus [ ] ={1,3,6,1,4,1,29601,2,77,38,1,1,5};
UINT4 FsMIBgp4PeerGroupName [ ] ={1,3,6,1,4,1,29601,2,77,39,1,1};
UINT4 FsMIBgp4PeerGroupAddrType [ ] ={1,3,6,1,4,1,29601,2,77,39,1,2};
UINT4 FsMIBgp4PeerGroupRemoteAs [ ] ={1,3,6,1,4,1,29601,2,77,39,1,3};
UINT4 FsMIBgp4PeerGroupHoldTimeConfigured [ ] ={1,3,6,1,4,1,29601,2,77,39,1,4};
UINT4 FsMIBgp4PeerGroupKeepAliveConfigured [ ] ={1,3,6,1,4,1,29601,2,77,39,1,5};
UINT4 FsMIBgp4PeerGroupConnectRetryInterval [ ] ={1,3,6,1,4,1,29601,2,77,39,1,6};
UINT4 FsMIBgp4PeerGroupMinASOriginInterval [ ] ={1,3,6,1,4,1,29601,2,77,39,1,7};
UINT4 FsMIBgp4PeerGroupMinRouteAdvInterval [ ] ={1,3,6,1,4,1,29601,2,77,39,1,8};
UINT4 FsMIBgp4PeerGroupAllowAutomaticStart [ ] ={1,3,6,1,4,1,29601,2,77,39,1,9};
UINT4 FsMIBgp4PeerGroupAllowAutomaticStop [ ] ={1,3,6,1,4,1,29601,2,77,39,1,10};
UINT4 FsMIBgp4PeerGroupIdleHoldTimeConfigured [ ] ={1,3,6,1,4,1,29601,2,77,39,1,11};
UINT4 FsMIBgp4PeerGroupDampPeerOscillations [ ] ={1,3,6,1,4,1,29601,2,77,39,1,12};
UINT4 FsMIBgp4PeerGroupDelayOpen [ ] ={1,3,6,1,4,1,29601,2,77,39,1,13};
UINT4 FsMIBgp4PeerGroupDelayOpenTimeConfigured [ ] ={1,3,6,1,4,1,29601,2,77,39,1,14};
UINT4 FsMIBgp4PeerGroupPrefixUpperLimit [ ] ={1,3,6,1,4,1,29601,2,77,39,1,15};
UINT4 FsMIBgp4PeerGroupTcpConnectRetryCnt [ ] ={1,3,6,1,4,1,29601,2,77,39,1,16};
UINT4 FsMIBgp4PeerGroupEBGPMultiHop [ ] ={1,3,6,1,4,1,29601,2,77,39,1,17};
UINT4 FsMIBgp4PeerGroupEBGPHopLimit [ ] ={1,3,6,1,4,1,29601,2,77,39,1,18};
UINT4 FsMIBgp4PeerGroupNextHopSelf [ ] ={1,3,6,1,4,1,29601,2,77,39,1,19};
UINT4 FsMIBgp4PeerGroupRflClient [ ] ={1,3,6,1,4,1,29601,2,77,39,1,20};
UINT4 FsMIBgp4PeerGroupTcpSendBufSize [ ] ={1,3,6,1,4,1,29601,2,77,39,1,21};
UINT4 FsMIBgp4PeerGroupTcpRcvBufSize [ ] ={1,3,6,1,4,1,29601,2,77,39,1,22};
UINT4 FsMIBgp4PeerGroupCommSendStatus [ ] ={1,3,6,1,4,1,29601,2,77,39,1,23};
UINT4 FsMIBgp4PeerGroupECommSendStatus [ ] ={1,3,6,1,4,1,29601,2,77,39,1,24};
UINT4 FsMIBgp4PeerGroupPassive [ ] ={1,3,6,1,4,1,29601,2,77,39,1,25};
UINT4 FsMIBgp4PeerGroupDefaultOriginate [ ] ={1,3,6,1,4,1,29601,2,77,39,1,26};
UINT4 FsMIBgp4PeerGroupActivateMPCapability [ ] ={1,3,6,1,4,1,29601,2,77,39,1,27};
UINT4 FsMIBgp4PeerGroupDeactivateMPCapability [ ] ={1,3,6,1,4,1,29601,2,77,39,1,28};
UINT4 FsMIBgp4PeerGroupRouteMapNameIn [ ] ={1,3,6,1,4,1,29601,2,77,39,1,29};
UINT4 FsMIBgp4PeerGroupRouteMapNameOut [ ] ={1,3,6,1,4,1,29601,2,77,39,1,30};
UINT4 FsMIBgp4PeerGroupStatus [ ] ={1,3,6,1,4,1,29601,2,77,39,1,31};
UINT4 FsMIBgp4PeerGroupIpPrefixNameIn [ ] ={1,3,6,1,4,1,29601,2,77,39,1,32};
UINT4 FsMIBgp4PeerGroupIpPrefixNameOut [ ] ={1,3,6,1,4,1,29601,2,77,39,1,33};
UINT4 FsMIBgp4PeerGroupOrfType [ ] ={1,3,6,1,4,1,29601,2,77,39,1,34};
UINT4 FsMIBgp4PeerGroupOrfCapMode [ ] ={1,3,6,1,4,1,29601,2,77,39,1,35};
UINT4 FsMIBgp4PeerGroupOrfRequest [ ] ={1,3,6,1,4,1,29601,2,77,39,1,36};
UINT4 FsMIBgp4PeerGroupBfdStatus [ ] ={1,3,6,1,4,1,29601,2,77,39,1,37};
UINT4 FsMIBgp4PeerGroupOverrideCapability [ ] ={1,3,6,1,4,1,29601,2,77,39,1,38};
UINT4 FsMIBgp4PeerGroupLocalAs [ ] ={1,3,6,1,4,1,29601,2,77,39,1,39};
UINT4 FsMIBgp4PeerAddrType [ ] ={1,3,6,1,4,1,29601,2,77,40,1,1};
UINT4 FsMIBgp4PeerAddress [ ] ={1,3,6,1,4,1,29601,2,77,40,1,2};
UINT4 FsMIBgp4PeerAddStatus [ ] ={1,3,6,1,4,1,29601,2,77,40,1,3};
UINT4 FsMIBgp4RestartReason [ ] ={1,3,6,1,4,1,29601,2,77,41};
UINT4 FsMIBgp4TCPMKTAuthKeyId [ ] ={1,3,6,1,4,1,29601,2,77,42,1,1,1};
UINT4 FsMIBgp4TCPMKTAuthRecvKeyId [ ] ={1,3,6,1,4,1,29601,2,77,42,1,1,2};
UINT4 FsMIBgp4TCPMKTAuthMasterKey [ ] ={1,3,6,1,4,1,29601,2,77,42,1,1,3};
UINT4 FsMIBgp4TCPMKTAuthAlgo [ ] ={1,3,6,1,4,1,29601,2,77,42,1,1,4};
UINT4 FsMIBgp4TCPMKTAuthTcpOptExc [ ] ={1,3,6,1,4,1,29601,2,77,42,1,1,5};
UINT4 FsMIBgp4TCPMKTAuthRowStatus [ ] ={1,3,6,1,4,1,29601,2,77,42,1,1,6};
UINT4 FsMIBgp4TCPAOAuthPeerType [ ] ={1,3,6,1,4,1,29601,2,77,43,1,1,1};
UINT4 FsMIBgp4TCPAOAuthPeerAddr [ ] ={1,3,6,1,4,1,29601,2,77,43,1,1,2};
UINT4 FsMIBgp4TCPAOAuthKeyId [ ] ={1,3,6,1,4,1,29601,2,77,43,1,1,3};
UINT4 FsMIBgp4TCPAOAuthKeyStatus [ ] ={1,3,6,1,4,1,29601,2,77,43,1,1,4};
UINT4 FsMIBgp4TCPAOAuthKeyStartAccept [ ] ={1,3,6,1,4,1,29601,2,77,43,1,1,5};
UINT4 FsMIBgp4TCPAOAuthKeyStartGenerate [ ] ={1,3,6,1,4,1,29601,2,77,43,1,1,6};
UINT4 FsMIBgp4TCPAOAuthKeyStopGenerate [ ] ={1,3,6,1,4,1,29601,2,77,43,1,1,7};
UINT4 FsMIBgp4TCPAOAuthKeyStopAccept [ ] ={1,3,6,1,4,1,29601,2,77,43,1,1,8};
UINT4 FsMIBgp4ORFPeerAddrType [ ] ={1,3,6,1,4,1,29601,2,77,44,1,1};
UINT4 FsMIBgp4ORFPeerAddr [ ] ={1,3,6,1,4,1,29601,2,77,44,1,2};
UINT4 FsMIBgp4ORFAfi [ ] ={1,3,6,1,4,1,29601,2,77,44,1,3};
UINT4 FsMIBgp4ORFSafi [ ] ={1,3,6,1,4,1,29601,2,77,44,1,4};
UINT4 FsMIBgp4ORFType [ ] ={1,3,6,1,4,1,29601,2,77,44,1,5};
UINT4 FsMIBgp4ORFSequence [ ] ={1,3,6,1,4,1,29601,2,77,44,1,6};
UINT4 FsMIBgp4ORFAddrPrefix [ ] ={1,3,6,1,4,1,29601,2,77,44,1,7};
UINT4 FsMIBgp4ORFAddrPrefixLen [ ] ={1,3,6,1,4,1,29601,2,77,44,1,8};
UINT4 FsMIBgp4ORFMinLength [ ] ={1,3,6,1,4,1,29601,2,77,44,1,9};
UINT4 FsMIBgp4ORFMaxLength [ ] ={1,3,6,1,4,1,29601,2,77,44,1,10};
UINT4 FsMIBgp4ORFAction [ ] ={1,3,6,1,4,1,29601,2,77,44,1,11};
UINT4 FsMIBgp4RRDNetworkAddr [ ] ={1,3,6,1,4,1,29601,2,77,45,1,1};
UINT4 FsMIBgp4RRDNetworkAddrType [ ] ={1,3,6,1,4,1,29601,2,77,45,1,2};
UINT4 FsMIBgp4RRDNetworkPrefixLen [ ] ={1,3,6,1,4,1,29601,2,77,45,1,3};
UINT4 FsMIBgp4RRDNetworkRowStatus [ ] ={1,3,6,1,4,1,29601,2,77,45,1,4};
UINT4 FsMIBgp4MacMobDuplicationTimeInterval [ ] ={1,3,6,1,4,1,29601,2,77,46};
UINT4 FsMIBgp4MaxMacMoves [ ] ={1,3,6,1,4,1,29601,2,77,47};



tMbDbEntry fsmpbgMibEntry[]= {

{{10,FsMIBgp4GlobalTraceDebug}, NULL, FsMIBgp4GlobalTraceDebugGet, FsMIBgp4GlobalTraceDebugSet, FsMIBgp4GlobalTraceDebugTest, FsMIBgp4GlobalTraceDebugDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsMIBgp4LocalAs}, NULL, FsMIBgp4LocalAsGet, FsMIBgp4LocalAsSet, FsMIBgp4LocalAsTest, FsMIBgp4LocalAsDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsMIBgp4MaxPeerEntry}, NULL, FsMIBgp4MaxPeerEntryGet, FsMIBgp4MaxPeerEntrySet, FsMIBgp4MaxPeerEntryTest, FsMIBgp4MaxPeerEntryDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "50"},

{{10,FsMIBgp4MaxNoofRoutes}, NULL, FsMIBgp4MaxNoofRoutesGet, FsMIBgp4MaxNoofRoutesSet, FsMIBgp4MaxNoofRoutesTest, FsMIBgp4MaxNoofRoutesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "5000"},

{{10,FsMIBgp4GRAdminStatus}, NULL, FsMIBgp4GRAdminStatusGet, FsMIBgp4GRAdminStatusSet, FsMIBgp4GRAdminStatusTest, FsMIBgp4GRAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsMIBgp4GRRestartTimeInterval}, NULL, FsMIBgp4GRRestartTimeIntervalGet, FsMIBgp4GRRestartTimeIntervalSet, FsMIBgp4GRRestartTimeIntervalTest, FsMIBgp4GRRestartTimeIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "90"},

{{10,FsMIBgp4RestartExitReason}, NULL, FsMIBgp4RestartExitReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMIBgp4GRSelectionDeferralTimeInterval}, NULL, FsMIBgp4GRSelectionDeferralTimeIntervalGet, FsMIBgp4GRSelectionDeferralTimeIntervalSet, FsMIBgp4GRSelectionDeferralTimeIntervalTest, FsMIBgp4GRSelectionDeferralTimeIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "60"},

{{10,FsMIBgp4GRStaleTimeInterval}, NULL, FsMIBgp4GRStaleTimeIntervalGet, FsMIBgp4GRStaleTimeIntervalSet, FsMIBgp4GRStaleTimeIntervalTest, FsMIBgp4GRStaleTimeIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "150"},

{{10,FsMIBgp4GRMode}, NULL, FsMIBgp4GRModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMIBgp4RestartSupport}, NULL, FsMIBgp4RestartSupportGet, FsMIBgp4RestartSupportSet, FsMIBgp4RestartSupportTest, FsMIBgp4RestartSupportDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsMIBgp4RestartStatus}, NULL, FsMIBgp4RestartStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMIBgp4ForwardingPreservation}, NULL, FsMIBgp4ForwardingPreservationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsMIBgp4ContextId}, GetNextIndexFsMIBgpContextTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgpContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4GlobalAdminStatus}, GetNextIndexFsMIBgpContextTable, FsMIBgp4GlobalAdminStatusGet, FsMIBgp4GlobalAdminStatusSet, FsMIBgp4GlobalAdminStatusTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "2"},

{{12,FsMIBgp4Identifier}, GetNextIndexFsMIBgpContextTable, FsMIBgp4IdentifierGet, FsMIBgp4IdentifierSet, FsMIBgp4IdentifierTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4Synchronization}, GetNextIndexFsMIBgpContextTable, FsMIBgp4SynchronizationGet, FsMIBgp4SynchronizationSet, FsMIBgp4SynchronizationTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "2"},

{{12,FsMIBgp4DefaultLocalPref}, GetNextIndexFsMIBgpContextTable, FsMIBgp4DefaultLocalPrefGet, FsMIBgp4DefaultLocalPrefSet, FsMIBgp4DefaultLocalPrefTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "100"},

{{12,FsMIBgp4AdvtNonBgpRt}, GetNextIndexFsMIBgpContextTable, FsMIBgp4AdvtNonBgpRtGet, FsMIBgp4AdvtNonBgpRtSet, FsMIBgp4AdvtNonBgpRtTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "2"},

{{12,FsMIBgp4TraceEnable}, GetNextIndexFsMIBgpContextTable, FsMIBgp4TraceEnableGet, FsMIBgp4TraceEnableSet, FsMIBgp4TraceEnableTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4DebugEnable}, GetNextIndexFsMIBgpContextTable, FsMIBgp4DebugEnableGet, FsMIBgp4DebugEnableSet, FsMIBgp4DebugEnableTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4OverlappingRoute}, GetNextIndexFsMIBgpContextTable, FsMIBgp4OverlappingRouteGet, FsMIBgp4OverlappingRouteSet, FsMIBgp4OverlappingRouteTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "3"},

{{12,FsMIBgp4AlwaysCompareMED}, GetNextIndexFsMIBgpContextTable, FsMIBgp4AlwaysCompareMEDGet, FsMIBgp4AlwaysCompareMEDSet, FsMIBgp4AlwaysCompareMEDTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "2"},

{{12,FsMIBgp4DefaultOriginate}, GetNextIndexFsMIBgpContextTable, FsMIBgp4DefaultOriginateGet, FsMIBgp4DefaultOriginateSet, FsMIBgp4DefaultOriginateTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "2"},

{{12,FsMIBgp4DefaultIpv4UniCast}, GetNextIndexFsMIBgpContextTable, FsMIBgp4DefaultIpv4UniCastGet, FsMIBgp4DefaultIpv4UniCastSet, FsMIBgp4DefaultIpv4UniCastTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "1"},

{{12,FsMIBgp4IsTrapEnabled}, GetNextIndexFsMIBgpContextTable, FsMIBgp4IsTrapEnabledGet, FsMIBgp4IsTrapEnabledSet, FsMIBgp4IsTrapEnabledTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "1"},

{{12,FsMIBgp4NextHopProcessingInterval}, GetNextIndexFsMIBgpContextTable, FsMIBgp4NextHopProcessingIntervalGet, FsMIBgp4NextHopProcessingIntervalSet, FsMIBgp4NextHopProcessingIntervalTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "60"},

{{12,FsMIBgp4IBGPRedistributionStatus}, GetNextIndexFsMIBgpContextTable, FsMIBgp4IBGPRedistributionStatusGet, FsMIBgp4IBGPRedistributionStatusSet, FsMIBgp4IBGPRedistributionStatusTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "2"},

{{12,FsMIBgp4RRDAdminStatus}, GetNextIndexFsMIBgpContextTable, FsMIBgp4RRDAdminStatusGet, FsMIBgp4RRDAdminStatusSet, FsMIBgp4RRDAdminStatusTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "1"},

{{12,FsMIBgp4RRDProtoMaskForEnable}, GetNextIndexFsMIBgpContextTable, FsMIBgp4RRDProtoMaskForEnableGet, FsMIBgp4RRDProtoMaskForEnableSet, FsMIBgp4RRDProtoMaskForEnableTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4RRDSrcProtoMaskForDisable}, GetNextIndexFsMIBgpContextTable, FsMIBgp4RRDSrcProtoMaskForDisableGet, FsMIBgp4RRDSrcProtoMaskForDisableSet, FsMIBgp4RRDSrcProtoMaskForDisableTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4RRDDefaultMetric}, GetNextIndexFsMIBgpContextTable, FsMIBgp4RRDDefaultMetricGet, FsMIBgp4RRDDefaultMetricSet, FsMIBgp4RRDDefaultMetricTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "0"},

{{12,FsMIBgp4RRDRouteMapName}, GetNextIndexFsMIBgpContextTable, FsMIBgp4RRDRouteMapNameGet, FsMIBgp4RRDRouteMapNameSet, FsMIBgp4RRDRouteMapNameTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4RRDMatchTypeEnable}, GetNextIndexFsMIBgpContextTable, FsMIBgp4RRDMatchTypeEnableGet, FsMIBgp4RRDMatchTypeEnableSet, FsMIBgp4RRDMatchTypeEnableTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4RRDMatchTypeDisable}, GetNextIndexFsMIBgpContextTable, FsMIBgp4RRDMatchTypeDisableGet, FsMIBgp4RRDMatchTypeDisableSet, FsMIBgp4RRDMatchTypeDisableTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4AscConfedId}, GetNextIndexFsMIBgpContextTable, FsMIBgp4AscConfedIdGet, FsMIBgp4AscConfedIdSet, FsMIBgp4AscConfedIdTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "0"},

{{12,FsMIBgp4AscConfedBestPathCompareMED}, GetNextIndexFsMIBgpContextTable, FsMIBgp4AscConfedBestPathCompareMEDGet, FsMIBgp4AscConfedBestPathCompareMEDSet, FsMIBgp4AscConfedBestPathCompareMEDTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "2"},

{{12,FsMIBgp4RflbgpClusterId}, GetNextIndexFsMIBgpContextTable, FsMIBgp4RflbgpClusterIdGet, FsMIBgp4RflbgpClusterIdSet, FsMIBgp4RflbgpClusterIdTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4RflRflSupport}, GetNextIndexFsMIBgpContextTable, FsMIBgp4RflRflSupportGet, FsMIBgp4RflRflSupportSet, FsMIBgp4RflRflSupportTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "2"},

{{12,FsMIBgp4RfdCutOff}, GetNextIndexFsMIBgpContextTable, FsMIBgp4RfdCutOffGet, FsMIBgp4RfdCutOffSet, FsMIBgp4RfdCutOffTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "2000"},

{{12,FsMIBgp4RfdReuse}, GetNextIndexFsMIBgpContextTable, FsMIBgp4RfdReuseGet, FsMIBgp4RfdReuseSet, FsMIBgp4RfdReuseTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "750"},

{{12,FsMIBgp4RfdCeiling}, GetNextIndexFsMIBgpContextTable, FsMIBgp4RfdCeilingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgpContextTableINDEX, 1, 0, 0, "8000"},

{{12,FsMIBgp4RfdMaxHoldDownTime}, GetNextIndexFsMIBgpContextTable, FsMIBgp4RfdMaxHoldDownTimeGet, FsMIBgp4RfdMaxHoldDownTimeSet, FsMIBgp4RfdMaxHoldDownTimeTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "3600"},

{{12,FsMIBgp4RfdDecayHalfLifeTime}, GetNextIndexFsMIBgpContextTable, FsMIBgp4RfdDecayHalfLifeTimeGet, FsMIBgp4RfdDecayHalfLifeTimeSet, FsMIBgp4RfdDecayHalfLifeTimeTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "900"},

{{12,FsMIBgp4RfdDecayTimerGranularity}, GetNextIndexFsMIBgpContextTable, FsMIBgp4RfdDecayTimerGranularityGet, FsMIBgp4RfdDecayTimerGranularitySet, FsMIBgp4RfdDecayTimerGranularityTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "1"},

{{12,FsMIBgp4RfdReuseTimerGranularity}, GetNextIndexFsMIBgpContextTable, FsMIBgp4RfdReuseTimerGranularityGet, FsMIBgp4RfdReuseTimerGranularitySet, FsMIBgp4RfdReuseTimerGranularityTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "15"},

{{12,FsMIBgp4RfdReuseIndxArraySize}, GetNextIndexFsMIBgpContextTable, FsMIBgp4RfdReuseIndxArraySizeGet, FsMIBgp4RfdReuseIndxArraySizeSet, FsMIBgp4RfdReuseIndxArraySizeTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "1024"},

{{12,FsMIBgp4RfdAdminStatus}, GetNextIndexFsMIBgpContextTable, FsMIBgp4RfdAdminStatusGet, FsMIBgp4RfdAdminStatusSet, FsMIBgp4RfdAdminStatusTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "2"},

{{12,FsMIBgp4CommMaxInFTblEntries}, GetNextIndexFsMIBgpContextTable, FsMIBgp4CommMaxInFTblEntriesGet, FsMIBgp4CommMaxInFTblEntriesSet, FsMIBgp4CommMaxInFTblEntriesTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "10000"},

{{12,FsMIBgp4CommMaxOutFTblEntries}, GetNextIndexFsMIBgpContextTable, FsMIBgp4CommMaxOutFTblEntriesGet, FsMIBgp4CommMaxOutFTblEntriesSet, FsMIBgp4CommMaxOutFTblEntriesTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "10000"},

{{12,FsMIBgp4ExtCommMaxInFTblEntries}, GetNextIndexFsMIBgpContextTable, FsMIBgp4ExtCommMaxInFTblEntriesGet, FsMIBgp4ExtCommMaxInFTblEntriesSet, FsMIBgp4ExtCommMaxInFTblEntriesTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "2000"},

{{12,FsMIBgp4ExtCommMaxOutFTblEntries}, GetNextIndexFsMIBgpContextTable, FsMIBgp4ExtCommMaxOutFTblEntriesGet, FsMIBgp4ExtCommMaxOutFTblEntriesSet, FsMIBgp4ExtCommMaxOutFTblEntriesTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "2000"},

{{12,FsMIBgp4CapabilitySupportAvailable}, GetNextIndexFsMIBgpContextTable, FsMIBgp4CapabilitySupportAvailableGet, FsMIBgp4CapabilitySupportAvailableSet, FsMIBgp4CapabilitySupportAvailableTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "1"},

{{12,FsMIBgp4MaxCapsPerPeer}, GetNextIndexFsMIBgpContextTable, FsMIBgp4MaxCapsPerPeerGet, FsMIBgp4MaxCapsPerPeerSet, FsMIBgp4MaxCapsPerPeerTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "10"},

{{12,FsMIBgp4MaxInstancesPerCap}, GetNextIndexFsMIBgpContextTable, FsMIBgp4MaxInstancesPerCapGet, FsMIBgp4MaxInstancesPerCapSet, FsMIBgp4MaxInstancesPerCapTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "5"},

{{12,FsMIBgp4MaxCapDataSize}, GetNextIndexFsMIBgpContextTable, FsMIBgp4MaxCapDataSizeGet, FsMIBgp4MaxCapDataSizeSet, FsMIBgp4MaxCapDataSizeTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "16"},

{{12,FsMIBgp4PreferenceValue}, GetNextIndexFsMIBgpContextTable, FsMIBgp4PreferenceValueGet, FsMIBgp4PreferenceValueSet, FsMIBgp4PreferenceValueTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "122"},

{{12,FsMIBgp4ContextStatus}, GetNextIndexFsMIBgpContextTable, FsMIBgp4ContextStatusGet, FsMIBgp4ContextStatusSet, FsMIBgp4ContextStatusTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4IBGPMaxPaths}, GetNextIndexFsMIBgpContextTable, FsMIBgp4IBGPMaxPathsGet, FsMIBgp4IBGPMaxPathsSet, FsMIBgp4IBGPMaxPathsTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "1"},

{{12,FsMIBgp4EBGPMaxPaths}, GetNextIndexFsMIBgpContextTable, FsMIBgp4EBGPMaxPathsGet, FsMIBgp4EBGPMaxPathsSet, FsMIBgp4EBGPMaxPathsTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "1"},

{{12,FsMIBgp4EIBGPMaxPaths}, GetNextIndexFsMIBgpContextTable, FsMIBgp4EIBGPMaxPathsGet, FsMIBgp4EIBGPMaxPathsSet, FsMIBgp4EIBGPMaxPathsTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "1"},

{{12,FsMIBgp4OperIBGPMaxPaths}, GetNextIndexFsMIBgpContextTable, FsMIBgp4OperIBGPMaxPathsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgpContextTableINDEX, 1, 0, 0, "1"},

{{12,FsMIBgp4OperEBGPMaxPaths}, GetNextIndexFsMIBgpContextTable, FsMIBgp4OperEBGPMaxPathsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgpContextTableINDEX, 1, 0, 0, "1"},

{{12,FsMIBgp4OperEIBGPMaxPaths}, GetNextIndexFsMIBgpContextTable, FsMIBgp4OperEIBGPMaxPathsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgpContextTableINDEX, 1, 0, 0, "1"},

{{12,FsMIBgp4FourByteASNSupportStatus}, GetNextIndexFsMIBgpContextTable, FsMIBgp4FourByteASNSupportStatusGet, FsMIBgp4FourByteASNSupportStatusSet, FsMIBgp4FourByteASNSupportStatusTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "1"},

{{12,FsMIBgp4FourByteASNotationType}, GetNextIndexFsMIBgpContextTable, FsMIBgp4FourByteASNotationTypeGet, FsMIBgp4FourByteASNotationTypeSet, FsMIBgp4FourByteASNotationTypeTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "1"},

{{12,FsMIBgp4LocalAsNo}, GetNextIndexFsMIBgpContextTable, FsMIBgp4LocalAsNoGet, FsMIBgp4LocalAsNoSet, FsMIBgp4LocalAsNoTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "0"},

{{12,FsMIBgp4RIBRoutes}, GetNextIndexFsMIBgpContextTable, FsMIBgp4RIBRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBgpContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4Ipv4AddrFamily}, GetNextIndexFsMIBgpContextTable, FsMIBgp4Ipv4AddrFamilyGet, FsMIBgp4Ipv4AddrFamilySet, FsMIBgp4Ipv4AddrFamilyTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "1"},

{{12,FsMIBgp4Ipv6AddrFamily}, GetNextIndexFsMIBgpContextTable, FsMIBgp4Ipv6AddrFamilyGet, FsMIBgp4Ipv6AddrFamilySet, FsMIBgp4Ipv6AddrFamilyTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "1"},

{{12,FsMIBgp4VpnLabelAllocPolicy}, GetNextIndexFsMIBgpContextTable, FsMIBgp4VpnLabelAllocPolicyGet, FsMIBgp4VpnLabelAllocPolicySet, FsMIBgp4VpnLabelAllocPolicyTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "1"},
{{12,FsMIBgp4VPNV4AddrFamily}, GetNextIndexFsMIBgpContextTable, FsMIBgp4VPNV4AddrFamilyGet, FsMIBgp4VPNV4AddrFamilySet, FsMIBgp4VPNV4AddrFamilyTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "2"},
{{12,FsMIBgp4L2vpnAddrFamily}, GetNextIndexFsMIBgpContextTable, FsMIBgp4L2vpnAddrFamilyGet, FsMIBgp4L2vpnAddrFamilySet, FsMIBgp4L2vpnAddrFamilyTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "2"},
{{12,FsMIBgp4EvpnAddrFamily}, GetNextIndexFsMIBgpContextTable, FsMIBgp4EvpnAddrFamilyGet, FsMIBgp4EvpnAddrFamilySet, FsMIBgp4EvpnAddrFamilyTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, "2"},

{{12,FsMIBgp4DebugType}, GetNextIndexFsMIBgpContextTable, FsMIBgp4DebugTypeGet, FsMIBgp4DebugTypeSet, FsMIBgp4DebugTypeTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4DebugTypeIPAddr}, GetNextIndexFsMIBgpContextTable, FsMIBgp4DebugTypeIPAddrGet, FsMIBgp4DebugTypeIPAddrSet, FsMIBgp4DebugTypeIPAddrTest, FsMIBgpContextTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgpContextTableINDEX, 1, 1, 0, "0"},

{{12,FsMIBgp4RRDMetricProtocolId}, GetNextIndexFsMIBgp4RRDMetricTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4RRDMetricTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4RRDMetricValue}, GetNextIndexFsMIBgp4RRDMetricTable, FsMIBgp4RRDMetricValueGet, FsMIBgp4RRDMetricValueSet, FsMIBgp4RRDMetricValueTest, FsMIBgp4RRDMetricTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBgp4RRDMetricTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBgp4InFilterCommVal}, GetNextIndexFsMIBgp4CommInFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIBgp4CommInFilterTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBgp4CommIncomingFilterStatus}, GetNextIndexFsMIBgp4CommInFilterTable, FsMIBgp4CommIncomingFilterStatusGet, FsMIBgp4CommIncomingFilterStatusSet, FsMIBgp4CommIncomingFilterStatusTest, FsMIBgp4CommInFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4CommInFilterTableINDEX, 2, 0, 0, "2"},

{{13,FsMIBgp4InFilterRowStatus}, GetNextIndexFsMIBgp4CommInFilterTable, FsMIBgp4InFilterRowStatusGet, FsMIBgp4InFilterRowStatusSet, FsMIBgp4InFilterRowStatusTest, FsMIBgp4CommInFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4CommInFilterTableINDEX, 2, 0, 1, NULL},

{{13,FsMIBgp4OutFilterCommVal}, GetNextIndexFsMIBgp4CommOutFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIBgp4CommOutFilterTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBgp4CommOutgoingFilterStatus}, GetNextIndexFsMIBgp4CommOutFilterTable, FsMIBgp4CommOutgoingFilterStatusGet, FsMIBgp4CommOutgoingFilterStatusSet, FsMIBgp4CommOutgoingFilterStatusTest, FsMIBgp4CommOutFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4CommOutFilterTableINDEX, 2, 0, 0, "2"},

{{13,FsMIBgp4OutFilterRowStatus}, GetNextIndexFsMIBgp4CommOutFilterTable, FsMIBgp4OutFilterRowStatusGet, FsMIBgp4OutFilterRowStatusSet, FsMIBgp4OutFilterRowStatusTest, FsMIBgp4CommOutFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4CommOutFilterTableINDEX, 2, 0, 1, NULL},

{{13,FsMIBgp4ExtCommInFilterCommVal}, GetNextIndexFsMIBgp4ExtCommInFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4ExtCommInFilterTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBgp4ExtCommIncomingFilterStatus}, GetNextIndexFsMIBgp4ExtCommInFilterTable, FsMIBgp4ExtCommIncomingFilterStatusGet, FsMIBgp4ExtCommIncomingFilterStatusSet, FsMIBgp4ExtCommIncomingFilterStatusTest, FsMIBgp4ExtCommInFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4ExtCommInFilterTableINDEX, 2, 0, 0, "2"},

{{13,FsMIBgp4ExtCommInFilterRowStatus}, GetNextIndexFsMIBgp4ExtCommInFilterTable, FsMIBgp4ExtCommInFilterRowStatusGet, FsMIBgp4ExtCommInFilterRowStatusSet, FsMIBgp4ExtCommInFilterRowStatusTest, FsMIBgp4ExtCommInFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4ExtCommInFilterTableINDEX, 2, 0, 1, NULL},

{{13,FsMIBgp4ExtCommOutFilterCommVal}, GetNextIndexFsMIBgp4ExtCommOutFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4ExtCommOutFilterTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBgp4ExtCommOutgoingFilterStatus}, GetNextIndexFsMIBgp4ExtCommOutFilterTable, FsMIBgp4ExtCommOutgoingFilterStatusGet, FsMIBgp4ExtCommOutgoingFilterStatusSet, FsMIBgp4ExtCommOutgoingFilterStatusTest, FsMIBgp4ExtCommOutFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4ExtCommOutFilterTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBgp4ExtCommOutFilterRowStatus}, GetNextIndexFsMIBgp4ExtCommOutFilterTable, FsMIBgp4ExtCommOutFilterRowStatusGet, FsMIBgp4ExtCommOutFilterRowStatusSet, FsMIBgp4ExtCommOutFilterRowStatusTest, FsMIBgp4ExtCommOutFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4ExtCommOutFilterTableINDEX, 2, 0, 1, NULL},

{{13,FsMIBgp4TCPMD5AuthPeerType}, GetNextIndexFsMIBgp4TCPMD5AuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4TCPMD5AuthTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4TCPMD5AuthPeerAddr}, GetNextIndexFsMIBgp4TCPMD5AuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4TCPMD5AuthTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4TCPMD5AuthPassword}, GetNextIndexFsMIBgp4TCPMD5AuthTable, FsMIBgp4TCPMD5AuthPasswordGet, FsMIBgp4TCPMD5AuthPasswordSet, FsMIBgp4TCPMD5AuthPasswordTest, FsMIBgp4TCPMD5AuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4TCPMD5AuthTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4TCPMD5AuthPwdSet}, GetNextIndexFsMIBgp4TCPMD5AuthTable, FsMIBgp4TCPMD5AuthPwdSetGet, FsMIBgp4TCPMD5AuthPwdSetSet, FsMIBgp4TCPMD5AuthPwdSetTest, FsMIBgp4TCPMD5AuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4TCPMD5AuthTableINDEX, 3, 0, 0, "2"},

{{13,FsMIBgpAscConfedPeerASNo}, GetNextIndexFsMIBgpAscConfedPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIBgpAscConfedPeerTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBgpAscConfedPeerStatus}, GetNextIndexFsMIBgpAscConfedPeerTable, FsMIBgpAscConfedPeerStatusGet, FsMIBgpAscConfedPeerStatusSet, FsMIBgpAscConfedPeerStatusTest, FsMIBgpAscConfedPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgpAscConfedPeerTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerIdentifier}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerRemoteAddrType}, GetNextIndexFsMIBgp4MpeBgpPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerLocalAs}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerLocalAsGet, FsMIBgp4mpebgpPeerLocalAsSet, FsMIBgp4mpebgpPeerLocalAsTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerState}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerAdminStatus}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerAdminStatusGet, FsMIBgp4mpebgpPeerAdminStatusSet, FsMIBgp4mpebgpPeerAdminStatusTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerNegotiatedVersion}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerNegotiatedVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerLocalAddr}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerLocalAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerLocalPort}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerLocalPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerRemoteAddr}, GetNextIndexFsMIBgp4MpeBgpPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerRemotePort}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerRemotePortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerRemoteAs}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerRemoteAsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerInUpdates}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerInUpdatesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerOutUpdates}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerOutUpdatesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerInTotalMessages}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerInTotalMessagesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerOutTotalMessages}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerOutTotalMessagesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerLastError}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerLastErrorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerFsmEstablishedTransitions}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerFsmEstablishedTransitionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerFsmEstablishedTime}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerFsmEstablishedTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerConnectRetryInterval}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerConnectRetryIntervalGet, FsMIBgp4mpebgpPeerConnectRetryIntervalSet, FsMIBgp4mpebgpPeerConnectRetryIntervalTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerHoldTime}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerKeepAlive}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerKeepAliveGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerHoldTimeConfigured}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerHoldTimeConfiguredGet, FsMIBgp4mpebgpPeerHoldTimeConfiguredSet, FsMIBgp4mpebgpPeerHoldTimeConfiguredTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerKeepAliveConfigured}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerKeepAliveConfiguredGet, FsMIBgp4mpebgpPeerKeepAliveConfiguredSet, FsMIBgp4mpebgpPeerKeepAliveConfiguredTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerMinASOriginationInterval}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerMinASOriginationIntervalGet, FsMIBgp4mpebgpPeerMinASOriginationIntervalSet, FsMIBgp4mpebgpPeerMinASOriginationIntervalTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerMinRouteAdvertisementInterval}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerMinRouteAdvertisementIntervalGet, FsMIBgp4mpebgpPeerMinRouteAdvertisementIntervalSet, FsMIBgp4mpebgpPeerMinRouteAdvertisementIntervalTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerInUpdateElapsedTime}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerInUpdateElapsedTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerEndOfRIBMarkerSentStatus}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerEndOfRIBMarkerSentStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerEndOfRIBMarkerReceivedStatus}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerEndOfRIBMarkerReceivedStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpebgpPeerRestartMode}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerRestartModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpePeerRestartTimeInterval}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpePeerRestartTimeIntervalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpePeerAllowAutomaticStart}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpePeerAllowAutomaticStartGet, FsMIBgp4mpePeerAllowAutomaticStartSet, FsMIBgp4mpePeerAllowAutomaticStartTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, "2"},

{{12,FsMIBgp4mpePeerAllowAutomaticStop}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpePeerAllowAutomaticStopGet, FsMIBgp4mpePeerAllowAutomaticStopSet, FsMIBgp4mpePeerAllowAutomaticStopTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, "2"},

{{12,FsMIBgp4mpebgpPeerIdleHoldTimeConfigured}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerIdleHoldTimeConfiguredGet, FsMIBgp4mpebgpPeerIdleHoldTimeConfiguredSet, FsMIBgp4mpebgpPeerIdleHoldTimeConfiguredTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, "60"},

{{12,FsMIBgp4mpeDampPeerOscillations}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpeDampPeerOscillationsGet, FsMIBgp4mpeDampPeerOscillationsSet, FsMIBgp4mpeDampPeerOscillationsTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, "2"},

{{12,FsMIBgp4mpePeerDelayOpen}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpePeerDelayOpenGet, FsMIBgp4mpePeerDelayOpenSet, FsMIBgp4mpePeerDelayOpenTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, "2"},

{{12,FsMIBgp4mpebgpPeerDelayOpenTimeConfigured}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpebgpPeerDelayOpenTimeConfiguredGet, FsMIBgp4mpebgpPeerDelayOpenTimeConfiguredSet, FsMIBgp4mpebgpPeerDelayOpenTimeConfiguredTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, "0"},

{{12,FsMIBgp4mpePeerPrefixUpperLimit}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpePeerPrefixUpperLimitGet, FsMIBgp4mpePeerPrefixUpperLimitSet, FsMIBgp4mpePeerPrefixUpperLimitTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, "5000"},

{{12,FsMIBgp4mpePeerTcpConnectRetryCnt}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpePeerTcpConnectRetryCntGet, FsMIBgp4mpePeerTcpConnectRetryCntSet, FsMIBgp4mpePeerTcpConnectRetryCntTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, "5"},

{{12,FsMIBgp4mpePeerTcpCurrentConnectRetryCnt}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpePeerTcpCurrentConnectRetryCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpeIsPeerDamped}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpeIsPeerDampedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, "2"},

{{12,FsMIBgp4mpePeerSessionAuthStatus}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpePeerSessionAuthStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpePeerTCPAOKeyIdInUse}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpePeerTCPAOKeyIdInUseGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpePeerTCPAOAuthNoMKTDiscard}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpePeerTCPAOAuthNoMKTDiscardGet, FsMIBgp4mpePeerTCPAOAuthNoMKTDiscardSet, FsMIBgp4mpePeerTCPAOAuthNoMKTDiscardTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, "1"},

{{12,FsMIBgp4mpePeerTCPAOAuthICMPAccept}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpePeerTCPAOAuthICMPAcceptGet, FsMIBgp4mpePeerTCPAOAuthICMPAcceptSet, FsMIBgp4mpePeerTCPAOAuthICMPAcceptTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, "2"},

{{12,FsMIBgp4mpePeerIpPrefixNameIn}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpePeerIpPrefixNameInGet, FsMIBgp4mpePeerIpPrefixNameInSet, FsMIBgp4mpePeerIpPrefixNameInTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpePeerIpPrefixNameOut}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpePeerIpPrefixNameOutGet, FsMIBgp4mpePeerIpPrefixNameOutSet, FsMIBgp4mpePeerIpPrefixNameOutTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpePeerBfdStatus}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpePeerBfdStatusGet, FsMIBgp4mpePeerBfdStatusSet, FsMIBgp4mpePeerBfdStatusTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, "2"},

{{12,FsMIBgp4mpePeerHoldAdvtRoutes}, GetNextIndexFsMIBgp4MpeBgpPeerTable, FsMIBgp4mpePeerHoldAdvtRoutesGet, FsMIBgp4mpePeerHoldAdvtRoutesSet, FsMIBgp4mpePeerHoldAdvtRoutesTest, FsMIBgp4MpeBgpPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeBgpPeerTableINDEX, 3, 0, 0, "2"},

{{12,FsMIBgp4mpebgp4PathAttrRouteAfi}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrRouteSafi}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrPeerType}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrPeer}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrIpAddrPrefixLen}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrIpAddrPrefix}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrOrigin}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, FsMIBgp4mpebgp4PathAttrOriginGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrASPathSegment}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, FsMIBgp4mpebgp4PathAttrASPathSegmentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrNextHop}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, FsMIBgp4mpebgp4PathAttrNextHopGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrMultiExitDisc}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, FsMIBgp4mpebgp4PathAttrMultiExitDiscGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrLocalPref}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, FsMIBgp4mpebgp4PathAttrLocalPrefGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrAtomicAggregate}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, FsMIBgp4mpebgp4PathAttrAtomicAggregateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrAggregatorAS}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, FsMIBgp4mpebgp4PathAttrAggregatorASGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrAggregatorAddr}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, FsMIBgp4mpebgp4PathAttrAggregatorAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrCalcLocalPref}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, FsMIBgp4mpebgp4PathAttrCalcLocalPrefGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrBest}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, FsMIBgp4mpebgp4PathAttrBestGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrCommunity}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, FsMIBgp4mpebgp4PathAttrCommunityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrOriginatorId}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, FsMIBgp4mpebgp4PathAttrOriginatorIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrClusterList}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, FsMIBgp4mpebgp4PathAttrClusterListGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrExtCommunity}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, FsMIBgp4mpebgp4PathAttrExtCommunityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrUnknown}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, FsMIBgp4mpebgp4PathAttrUnknownGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrLabel}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, FsMIBgp4mpebgp4PathAttrLabelGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrAS4PathSegment}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, FsMIBgp4mpebgp4PathAttrAS4PathSegmentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpebgp4PathAttrAggregatorAS4}, GetNextIndexFsMIBgp4MpeBgp4PathAttrTable, FsMIBgp4mpebgp4PathAttrAggregatorAS4Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMIBgp4MpeBgp4PathAttrTableINDEX, 7, 0, 0, NULL},

{{12,FsMIBgp4mpePeerExtPeerType}, GetNextIndexFsMIBgp4MpePeerExtTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpePeerExtTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpePeerExtPeerRemoteAddr}, GetNextIndexFsMIBgp4MpePeerExtTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpePeerExtTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpePeerExtConfigurePeer}, GetNextIndexFsMIBgp4MpePeerExtTable, FsMIBgp4mpePeerExtConfigurePeerGet, FsMIBgp4mpePeerExtConfigurePeerSet, FsMIBgp4mpePeerExtConfigurePeerTest, FsMIBgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpePeerExtTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpePeerExtPeerRemoteAs}, GetNextIndexFsMIBgp4MpePeerExtTable, FsMIBgp4mpePeerExtPeerRemoteAsGet, FsMIBgp4mpePeerExtPeerRemoteAsSet, FsMIBgp4mpePeerExtPeerRemoteAsTest, FsMIBgp4MpePeerExtTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBgp4MpePeerExtTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpePeerExtEBGPMultiHop}, GetNextIndexFsMIBgp4MpePeerExtTable, FsMIBgp4mpePeerExtEBGPMultiHopGet, FsMIBgp4mpePeerExtEBGPMultiHopSet, FsMIBgp4mpePeerExtEBGPMultiHopTest, FsMIBgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpePeerExtTableINDEX, 3, 0, 0, "2"},

{{12,FsMIBgp4mpePeerExtEBGPHopLimit}, GetNextIndexFsMIBgp4MpePeerExtTable, FsMIBgp4mpePeerExtEBGPHopLimitGet, FsMIBgp4mpePeerExtEBGPHopLimitSet, FsMIBgp4mpePeerExtEBGPHopLimitTest, FsMIBgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4MpePeerExtTableINDEX, 3, 0, 0, "1"},

{{12,FsMIBgp4mpePeerExtNextHopSelf}, GetNextIndexFsMIBgp4MpePeerExtTable, FsMIBgp4mpePeerExtNextHopSelfGet, FsMIBgp4mpePeerExtNextHopSelfSet, FsMIBgp4mpePeerExtNextHopSelfTest, FsMIBgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpePeerExtTableINDEX, 3, 0, 0, "1"},

{{12,FsMIBgp4mpePeerExtRflClient}, GetNextIndexFsMIBgp4MpePeerExtTable, FsMIBgp4mpePeerExtRflClientGet, FsMIBgp4mpePeerExtRflClientSet, FsMIBgp4mpePeerExtRflClientTest, FsMIBgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpePeerExtTableINDEX, 3, 0, 0, "1"},

{{12,FsMIBgp4mpePeerExtTcpSendBufSize}, GetNextIndexFsMIBgp4MpePeerExtTable, FsMIBgp4mpePeerExtTcpSendBufSizeGet, FsMIBgp4mpePeerExtTcpSendBufSizeSet, FsMIBgp4mpePeerExtTcpSendBufSizeTest, FsMIBgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4MpePeerExtTableINDEX, 3, 0, 0, "65536"},

{{12,FsMIBgp4mpePeerExtTcpRcvBufSize}, GetNextIndexFsMIBgp4MpePeerExtTable, FsMIBgp4mpePeerExtTcpRcvBufSizeGet, FsMIBgp4mpePeerExtTcpRcvBufSizeSet, FsMIBgp4mpePeerExtTcpRcvBufSizeTest, FsMIBgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4MpePeerExtTableINDEX, 3, 0, 0, "65536"},

{{12,FsMIBgp4mpePeerExtLclAddress}, GetNextIndexFsMIBgp4MpePeerExtTable, FsMIBgp4mpePeerExtLclAddressGet, FsMIBgp4mpePeerExtLclAddressSet, FsMIBgp4mpePeerExtLclAddressTest, FsMIBgp4MpePeerExtTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4MpePeerExtTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpePeerExtNetworkAddress}, GetNextIndexFsMIBgp4MpePeerExtTable, FsMIBgp4mpePeerExtNetworkAddressGet, FsMIBgp4mpePeerExtNetworkAddressSet, FsMIBgp4mpePeerExtNetworkAddressTest, FsMIBgp4MpePeerExtTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4MpePeerExtTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpePeerExtGateway}, GetNextIndexFsMIBgp4MpePeerExtTable, FsMIBgp4mpePeerExtGatewayGet, FsMIBgp4mpePeerExtGatewaySet, FsMIBgp4mpePeerExtGatewayTest, FsMIBgp4MpePeerExtTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4MpePeerExtTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpePeerExtCommSendStatus}, GetNextIndexFsMIBgp4MpePeerExtTable, FsMIBgp4mpePeerExtCommSendStatusGet, FsMIBgp4mpePeerExtCommSendStatusSet, FsMIBgp4mpePeerExtCommSendStatusTest, FsMIBgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpePeerExtTableINDEX, 3, 0, 0, "2"},

{{12,FsMIBgp4mpePeerExtECommSendStatus}, GetNextIndexFsMIBgp4MpePeerExtTable, FsMIBgp4mpePeerExtECommSendStatusGet, FsMIBgp4mpePeerExtECommSendStatusSet, FsMIBgp4mpePeerExtECommSendStatusTest, FsMIBgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpePeerExtTableINDEX, 3, 0, 0, "2"},

{{12,FsMIBgp4mpePeerExtPassive}, GetNextIndexFsMIBgp4MpePeerExtTable, FsMIBgp4mpePeerExtPassiveGet, FsMIBgp4mpePeerExtPassiveSet, FsMIBgp4mpePeerExtPassiveTest, FsMIBgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpePeerExtTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpePeerExtDefaultOriginate}, GetNextIndexFsMIBgp4MpePeerExtTable, FsMIBgp4mpePeerExtDefaultOriginateGet, FsMIBgp4mpePeerExtDefaultOriginateSet, FsMIBgp4mpePeerExtDefaultOriginateTest, FsMIBgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpePeerExtTableINDEX, 3, 0, 0, "2"},

{{12,FsMIBgp4mpePeerExtOverrideCapability}, GetNextIndexFsMIBgp4MpePeerExtTable, FsMIBgp4mpePeerExtOverrideCapabilityGet, FsMIBgp4mpePeerExtOverrideCapabilitySet, FsMIBgp4mpePeerExtOverrideCapabilityTest, FsMIBgp4MpePeerExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpePeerExtTableINDEX, 3, 0, 0, "2"},

{{12,FsMIBgp4mpeMEDIndex}, GetNextIndexFsMIBgp4MpeMEDTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4MpeMEDTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeMEDAdminStatus}, GetNextIndexFsMIBgp4MpeMEDTable, FsMIBgp4mpeMEDAdminStatusGet, FsMIBgp4mpeMEDAdminStatusSet, FsMIBgp4mpeMEDAdminStatusTest, FsMIBgp4MpeMEDTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeMEDTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeMEDRemoteAS}, GetNextIndexFsMIBgp4MpeMEDTable, FsMIBgp4mpeMEDRemoteASGet, FsMIBgp4mpeMEDRemoteASSet, FsMIBgp4mpeMEDRemoteASTest, FsMIBgp4MpeMEDTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBgp4MpeMEDTableINDEX, 1, 0, 0, "0"},

{{12,FsMIBgp4mpeMEDIPAddrAfi}, GetNextIndexFsMIBgp4MpeMEDTable, FsMIBgp4mpeMEDIPAddrAfiGet, FsMIBgp4mpeMEDIPAddrAfiSet, FsMIBgp4mpeMEDIPAddrAfiTest, FsMIBgp4MpeMEDTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeMEDTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeMEDIPAddrSafi}, GetNextIndexFsMIBgp4MpeMEDTable, FsMIBgp4mpeMEDIPAddrSafiGet, FsMIBgp4mpeMEDIPAddrSafiSet, FsMIBgp4mpeMEDIPAddrSafiTest, FsMIBgp4MpeMEDTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeMEDTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeMEDIPAddrPrefix}, GetNextIndexFsMIBgp4MpeMEDTable, FsMIBgp4mpeMEDIPAddrPrefixGet, FsMIBgp4mpeMEDIPAddrPrefixSet, FsMIBgp4mpeMEDIPAddrPrefixTest, FsMIBgp4MpeMEDTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4MpeMEDTableINDEX, 1, 0, 0, "0"},

{{12,FsMIBgp4mpeMEDIPAddrPrefixLen}, GetNextIndexFsMIBgp4MpeMEDTable, FsMIBgp4mpeMEDIPAddrPrefixLenGet, FsMIBgp4mpeMEDIPAddrPrefixLenSet, FsMIBgp4mpeMEDIPAddrPrefixLenTest, FsMIBgp4MpeMEDTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4MpeMEDTableINDEX, 1, 0, 0, "0"},

{{12,FsMIBgp4mpeMEDIntermediateAS}, GetNextIndexFsMIBgp4MpeMEDTable, FsMIBgp4mpeMEDIntermediateASGet, FsMIBgp4mpeMEDIntermediateASSet, FsMIBgp4mpeMEDIntermediateASTest, FsMIBgp4MpeMEDTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4MpeMEDTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeMEDDirection}, GetNextIndexFsMIBgp4MpeMEDTable, FsMIBgp4mpeMEDDirectionGet, FsMIBgp4mpeMEDDirectionSet, FsMIBgp4mpeMEDDirectionTest, FsMIBgp4MpeMEDTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeMEDTableINDEX, 1, 0, 0, "1"},

{{12,FsMIBgp4mpeMEDValue}, GetNextIndexFsMIBgp4MpeMEDTable, FsMIBgp4mpeMEDValueGet, FsMIBgp4mpeMEDValueSet, FsMIBgp4mpeMEDValueTest, FsMIBgp4MpeMEDTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBgp4MpeMEDTableINDEX, 1, 0, 0, "0"},

{{12,FsMIBgp4mpeMEDPreference}, GetNextIndexFsMIBgp4MpeMEDTable, FsMIBgp4mpeMEDPreferenceGet, FsMIBgp4mpeMEDPreferenceSet, FsMIBgp4mpeMEDPreferenceTest, FsMIBgp4MpeMEDTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeMEDTableINDEX, 1, 0, 0, "1"},

{{12,FsMIBgp4mpeMEDVrfName}, GetNextIndexFsMIBgp4MpeMEDTable, FsMIBgp4mpeMEDVrfNameGet, FsMIBgp4mpeMEDVrfNameSet, FsMIBgp4mpeMEDVrfNameTest, FsMIBgp4MpeMEDTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4MpeMEDTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeLocalPrefIndex}, GetNextIndexFsMIBgp4MpeLocalPrefTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4MpeLocalPrefTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeLocalPrefAdminStatus}, GetNextIndexFsMIBgp4MpeLocalPrefTable, FsMIBgp4mpeLocalPrefAdminStatusGet, FsMIBgp4mpeLocalPrefAdminStatusSet, FsMIBgp4mpeLocalPrefAdminStatusTest, FsMIBgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeLocalPrefTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeLocalPrefRemoteAS}, GetNextIndexFsMIBgp4MpeLocalPrefTable, FsMIBgp4mpeLocalPrefRemoteASGet, FsMIBgp4mpeLocalPrefRemoteASSet, FsMIBgp4mpeLocalPrefRemoteASTest, FsMIBgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBgp4MpeLocalPrefTableINDEX, 1, 0, 0, "0"},

{{12,FsMIBgp4mpeLocalPrefIPAddrAfi}, GetNextIndexFsMIBgp4MpeLocalPrefTable, FsMIBgp4mpeLocalPrefIPAddrAfiGet, FsMIBgp4mpeLocalPrefIPAddrAfiSet, FsMIBgp4mpeLocalPrefIPAddrAfiTest, FsMIBgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeLocalPrefTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeLocalPrefIPAddrSafi}, GetNextIndexFsMIBgp4MpeLocalPrefTable, FsMIBgp4mpeLocalPrefIPAddrSafiGet, FsMIBgp4mpeLocalPrefIPAddrSafiSet, FsMIBgp4mpeLocalPrefIPAddrSafiTest, FsMIBgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeLocalPrefTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeLocalPrefIPAddrPrefix}, GetNextIndexFsMIBgp4MpeLocalPrefTable, FsMIBgp4mpeLocalPrefIPAddrPrefixGet, FsMIBgp4mpeLocalPrefIPAddrPrefixSet, FsMIBgp4mpeLocalPrefIPAddrPrefixTest, FsMIBgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4MpeLocalPrefTableINDEX, 1, 0, 0, "0"},

{{12,FsMIBgp4mpeLocalPrefIPAddrPrefixLen}, GetNextIndexFsMIBgp4MpeLocalPrefTable, FsMIBgp4mpeLocalPrefIPAddrPrefixLenGet, FsMIBgp4mpeLocalPrefIPAddrPrefixLenSet, FsMIBgp4mpeLocalPrefIPAddrPrefixLenTest, FsMIBgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4MpeLocalPrefTableINDEX, 1, 0, 0, "0"},

{{12,FsMIBgp4mpeLocalPrefIntermediateAS}, GetNextIndexFsMIBgp4MpeLocalPrefTable, FsMIBgp4mpeLocalPrefIntermediateASGet, FsMIBgp4mpeLocalPrefIntermediateASSet, FsMIBgp4mpeLocalPrefIntermediateASTest, FsMIBgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4MpeLocalPrefTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeLocalPrefDirection}, GetNextIndexFsMIBgp4MpeLocalPrefTable, FsMIBgp4mpeLocalPrefDirectionGet, FsMIBgp4mpeLocalPrefDirectionSet, FsMIBgp4mpeLocalPrefDirectionTest, FsMIBgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeLocalPrefTableINDEX, 1, 0, 0, "1"},

{{12,FsMIBgp4mpeLocalPrefValue}, GetNextIndexFsMIBgp4MpeLocalPrefTable, FsMIBgp4mpeLocalPrefValueGet, FsMIBgp4mpeLocalPrefValueSet, FsMIBgp4mpeLocalPrefValueTest, FsMIBgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBgp4MpeLocalPrefTableINDEX, 1, 0, 0, "100"},

{{12,FsMIBgp4mpeLocalPrefPreference}, GetNextIndexFsMIBgp4MpeLocalPrefTable, FsMIBgp4mpeLocalPrefPreferenceGet, FsMIBgp4mpeLocalPrefPreferenceSet, FsMIBgp4mpeLocalPrefPreferenceTest, FsMIBgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeLocalPrefTableINDEX, 1, 0, 0, "1"},

{{12,FsMIBgp4mpeLocalPrefVrfName}, GetNextIndexFsMIBgp4MpeLocalPrefTable, FsMIBgp4mpeLocalPrefVrfNameGet, FsMIBgp4mpeLocalPrefVrfNameSet, FsMIBgp4mpeLocalPrefVrfNameTest, FsMIBgp4MpeLocalPrefTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4MpeLocalPrefTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeUpdateFilterIndex}, GetNextIndexFsMIBgp4MpeUpdateFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4MpeUpdateFilterTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeUpdateFilterAdminStatus}, GetNextIndexFsMIBgp4MpeUpdateFilterTable, FsMIBgp4mpeUpdateFilterAdminStatusGet, FsMIBgp4mpeUpdateFilterAdminStatusSet, FsMIBgp4mpeUpdateFilterAdminStatusTest, FsMIBgp4MpeUpdateFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeUpdateFilterTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeUpdateFilterRemoteAS}, GetNextIndexFsMIBgp4MpeUpdateFilterTable, FsMIBgp4mpeUpdateFilterRemoteASGet, FsMIBgp4mpeUpdateFilterRemoteASSet, FsMIBgp4mpeUpdateFilterRemoteASTest, FsMIBgp4MpeUpdateFilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBgp4MpeUpdateFilterTableINDEX, 1, 0, 0, "0"},

{{12,FsMIBgp4mpeUpdateFilterIPAddrAfi}, GetNextIndexFsMIBgp4MpeUpdateFilterTable, FsMIBgp4mpeUpdateFilterIPAddrAfiGet, FsMIBgp4mpeUpdateFilterIPAddrAfiSet, FsMIBgp4mpeUpdateFilterIPAddrAfiTest, FsMIBgp4MpeUpdateFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeUpdateFilterTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeUpdateFilterIPAddrSafi}, GetNextIndexFsMIBgp4MpeUpdateFilterTable, FsMIBgp4mpeUpdateFilterIPAddrSafiGet, FsMIBgp4mpeUpdateFilterIPAddrSafiSet, FsMIBgp4mpeUpdateFilterIPAddrSafiTest, FsMIBgp4MpeUpdateFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeUpdateFilterTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeUpdateFilterIPAddrPrefix}, GetNextIndexFsMIBgp4MpeUpdateFilterTable, FsMIBgp4mpeUpdateFilterIPAddrPrefixGet, FsMIBgp4mpeUpdateFilterIPAddrPrefixSet, FsMIBgp4mpeUpdateFilterIPAddrPrefixTest, FsMIBgp4MpeUpdateFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4MpeUpdateFilterTableINDEX, 1, 0, 0, "0"},

{{12,FsMIBgp4mpeUpdateFilterIPAddrPrefixLen}, GetNextIndexFsMIBgp4MpeUpdateFilterTable, FsMIBgp4mpeUpdateFilterIPAddrPrefixLenGet, FsMIBgp4mpeUpdateFilterIPAddrPrefixLenSet, FsMIBgp4mpeUpdateFilterIPAddrPrefixLenTest, FsMIBgp4MpeUpdateFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4MpeUpdateFilterTableINDEX, 1, 0, 0, "0"},

{{12,FsMIBgp4mpeUpdateFilterIntermediateAS}, GetNextIndexFsMIBgp4MpeUpdateFilterTable, FsMIBgp4mpeUpdateFilterIntermediateASGet, FsMIBgp4mpeUpdateFilterIntermediateASSet, FsMIBgp4mpeUpdateFilterIntermediateASTest, FsMIBgp4MpeUpdateFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4MpeUpdateFilterTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeUpdateFilterDirection}, GetNextIndexFsMIBgp4MpeUpdateFilterTable, FsMIBgp4mpeUpdateFilterDirectionGet, FsMIBgp4mpeUpdateFilterDirectionSet, FsMIBgp4mpeUpdateFilterDirectionTest, FsMIBgp4MpeUpdateFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeUpdateFilterTableINDEX, 1, 0, 0, "1"},

{{12,FsMIBgp4mpeUpdateFilterAction}, GetNextIndexFsMIBgp4MpeUpdateFilterTable, FsMIBgp4mpeUpdateFilterActionGet, FsMIBgp4mpeUpdateFilterActionSet, FsMIBgp4mpeUpdateFilterActionTest, FsMIBgp4MpeUpdateFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeUpdateFilterTableINDEX, 1, 0, 0, "2"},

{{12,FsMIBgp4mpeUpdateFilterVrfName}, GetNextIndexFsMIBgp4MpeUpdateFilterTable, FsMIBgp4mpeUpdateFilterVrfNameGet, FsMIBgp4mpeUpdateFilterVrfNameSet, FsMIBgp4mpeUpdateFilterVrfNameTest, FsMIBgp4MpeUpdateFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4MpeUpdateFilterTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeAggregateIndex}, GetNextIndexFsMIBgp4MpeAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeAggregateAdminStatus}, GetNextIndexFsMIBgp4MpeAggregateTable, FsMIBgp4mpeAggregateAdminStatusGet, FsMIBgp4mpeAggregateAdminStatusSet, FsMIBgp4mpeAggregateAdminStatusTest, FsMIBgp4MpeAggregateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeAggregateIPAddrAfi}, GetNextIndexFsMIBgp4MpeAggregateTable, FsMIBgp4mpeAggregateIPAddrAfiGet, FsMIBgp4mpeAggregateIPAddrAfiSet, FsMIBgp4mpeAggregateIPAddrAfiTest, FsMIBgp4MpeAggregateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeAggregateIPAddrSafi}, GetNextIndexFsMIBgp4MpeAggregateTable, FsMIBgp4mpeAggregateIPAddrSafiGet, FsMIBgp4mpeAggregateIPAddrSafiSet, FsMIBgp4mpeAggregateIPAddrSafiTest, FsMIBgp4MpeAggregateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeAggregateIPAddrPrefix}, GetNextIndexFsMIBgp4MpeAggregateTable, FsMIBgp4mpeAggregateIPAddrPrefixGet, FsMIBgp4mpeAggregateIPAddrPrefixSet, FsMIBgp4mpeAggregateIPAddrPrefixTest, FsMIBgp4MpeAggregateTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeAggregateIPAddrPrefixLen}, GetNextIndexFsMIBgp4MpeAggregateTable, FsMIBgp4mpeAggregateIPAddrPrefixLenGet, FsMIBgp4mpeAggregateIPAddrPrefixLenSet, FsMIBgp4mpeAggregateIPAddrPrefixLenTest, FsMIBgp4MpeAggregateTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeAggregateAdvertise}, GetNextIndexFsMIBgp4MpeAggregateTable, FsMIBgp4mpeAggregateAdvertiseGet, FsMIBgp4mpeAggregateAdvertiseSet, FsMIBgp4mpeAggregateAdvertiseTest, FsMIBgp4MpeAggregateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeAggregateVrfName}, GetNextIndexFsMIBgp4MpeAggregateTable, FsMIBgp4mpeAggregateVrfNameGet, FsMIBgp4mpeAggregateVrfNameSet, FsMIBgp4mpeAggregateVrfNameTest, FsMIBgp4MpeAggregateTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeAggregateAsSet}, GetNextIndexFsMIBgp4MpeAggregateTable, FsMIBgp4mpeAggregateAsSetGet, FsMIBgp4mpeAggregateAsSetSet, FsMIBgp4mpeAggregateAsSetTest, FsMIBgp4MpeAggregateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeAggregateAdvertiseRouteMapName}, GetNextIndexFsMIBgp4MpeAggregateTable, FsMIBgp4mpeAggregateAdvertiseRouteMapNameGet, FsMIBgp4mpeAggregateAdvertiseRouteMapNameSet, FsMIBgp4mpeAggregateAdvertiseRouteMapNameTest, FsMIBgp4MpeAggregateTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeAggregateSuppressRouteMapName}, GetNextIndexFsMIBgp4MpeAggregateTable, FsMIBgp4mpeAggregateSuppressRouteMapNameGet, FsMIBgp4mpeAggregateSuppressRouteMapNameSet, FsMIBgp4mpeAggregateSuppressRouteMapNameTest, FsMIBgp4MpeAggregateTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeAggregateAttributeRouteMapName}, GetNextIndexFsMIBgp4MpeAggregateTable, FsMIBgp4mpeAggregateAttributeRouteMapNameGet, FsMIBgp4mpeAggregateAttributeRouteMapNameSet, FsMIBgp4mpeAggregateAttributeRouteMapNameTest, FsMIBgp4MpeAggregateTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4MpeAggregateTableINDEX, 1, 0, 0, NULL},

{{12,FsMIBgp4mpeImportRoutePrefixAfi}, GetNextIndexFsMIBgp4MpeImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeImportRouteTableINDEX, 10, 0, 0, NULL},

{{12,FsMIBgp4mpeImportRoutePrefixSafi}, GetNextIndexFsMIBgp4MpeImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeImportRouteTableINDEX, 10, 0, 0, NULL},

{{12,FsMIBgp4mpeImportRoutePrefix}, GetNextIndexFsMIBgp4MpeImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeImportRouteTableINDEX, 10, 0, 0, NULL},

{{12,FsMIBgp4mpeImportRoutePrefixLen}, GetNextIndexFsMIBgp4MpeImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4MpeImportRouteTableINDEX, 10, 0, 0, NULL},

{{12,FsMIBgp4mpeImportRouteProtocol}, GetNextIndexFsMIBgp4MpeImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4MpeImportRouteTableINDEX, 10, 0, 0, NULL},

{{12,FsMIBgp4mpeImportRouteNextHop}, GetNextIndexFsMIBgp4MpeImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeImportRouteTableINDEX, 10, 0, 0, NULL},

{{12,FsMIBgp4mpeImportRouteIfIndex}, GetNextIndexFsMIBgp4MpeImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4MpeImportRouteTableINDEX, 10, 0, 0, NULL},

{{12,FsMIBgp4mpeImportRouteMetric}, GetNextIndexFsMIBgp4MpeImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4MpeImportRouteTableINDEX, 10, 0, 0, NULL},

{{12,FsMIBgp4mpeImportRouteVrf}, GetNextIndexFsMIBgp4MpeImportRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeImportRouteTableINDEX, 10, 0, 0, NULL},

{{12,FsMIBgp4mpeImportRouteAction}, GetNextIndexFsMIBgp4MpeImportRouteTable, FsMIBgp4mpeImportRouteActionGet, FsMIBgp4mpeImportRouteActionSet, FsMIBgp4mpeImportRouteActionTest, FsMIBgp4MpeImportRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeImportRouteTableINDEX, 10, 0, 0, NULL},

{{12,FsMIBgp4mpePeerType}, GetNextIndexFsMIBgp4MpeFsmTransitionHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeFsmTransitionHistTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpePeer}, GetNextIndexFsMIBgp4MpeFsmTransitionHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeFsmTransitionHistTableINDEX, 3, 0, 0, NULL},

{{12,FsMIBgp4mpeFsmTransitionHist}, GetNextIndexFsMIBgp4MpeFsmTransitionHistTable, FsMIBgp4mpeFsmTransitionHistGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIBgp4MpeFsmTransitionHistTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4mpePathAttrAddrPrefixAfi}, GetNextIndexFsMIBgp4MpeRfdRtDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeRfdRtDampHistTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpePathAttrAddrPrefixSafi}, GetNextIndexFsMIBgp4MpeRfdRtDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeRfdRtDampHistTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpePathAttrAddrPrefix}, GetNextIndexFsMIBgp4MpeRfdRtDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeRfdRtDampHistTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpePathAttrAddrPrefixLen}, GetNextIndexFsMIBgp4MpeRfdRtDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4MpeRfdRtDampHistTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpePathAttrPeerType}, GetNextIndexFsMIBgp4MpeRfdRtDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeRfdRtDampHistTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpePathAttrPeer}, GetNextIndexFsMIBgp4MpeRfdRtDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeRfdRtDampHistTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdRtFom}, GetNextIndexFsMIBgp4MpeRfdRtDampHistTable, FsMIBgp4mpeRfdRtFomGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeRfdRtDampHistTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdRtLastUpdtTime}, GetNextIndexFsMIBgp4MpeRfdRtDampHistTable, FsMIBgp4mpeRfdRtLastUpdtTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeRfdRtDampHistTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdRtState}, GetNextIndexFsMIBgp4MpeRfdRtDampHistTable, FsMIBgp4mpeRfdRtStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeRfdRtDampHistTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdRtStatus}, GetNextIndexFsMIBgp4MpeRfdRtDampHistTable, FsMIBgp4mpeRfdRtStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeRfdRtDampHistTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdRtFlapCount}, GetNextIndexFsMIBgp4MpeRfdRtDampHistTable, FsMIBgp4mpeRfdRtFlapCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeRfdRtDampHistTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdRtFlapTime}, GetNextIndexFsMIBgp4MpeRfdRtDampHistTable, FsMIBgp4mpeRfdRtFlapTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeRfdRtDampHistTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdRtReuseTime}, GetNextIndexFsMIBgp4MpeRfdRtDampHistTable, FsMIBgp4mpeRfdRtReuseTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeRfdRtDampHistTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpePeerRemoteIpAddrType}, GetNextIndexFsMIBgp4MpeRfdPeerDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeRfdPeerDampHistTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4mpePeerRemoteIpAddr}, GetNextIndexFsMIBgp4MpeRfdPeerDampHistTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeRfdPeerDampHistTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdPeerFom}, GetNextIndexFsMIBgp4MpeRfdPeerDampHistTable, FsMIBgp4mpeRfdPeerFomGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeRfdPeerDampHistTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdPeerLastUpdtTime}, GetNextIndexFsMIBgp4MpeRfdPeerDampHistTable, FsMIBgp4mpeRfdPeerLastUpdtTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeRfdPeerDampHistTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdPeerState}, GetNextIndexFsMIBgp4MpeRfdPeerDampHistTable, FsMIBgp4mpeRfdPeerStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeRfdPeerDampHistTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdPeerStatus}, GetNextIndexFsMIBgp4MpeRfdPeerDampHistTable, FsMIBgp4mpeRfdPeerStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeRfdPeerDampHistTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4mpeRtAfi}, GetNextIndexFsMIBgp4MpeRfdRtsReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeRfdRtsReuseListTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpeRtSafi}, GetNextIndexFsMIBgp4MpeRfdRtsReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeRfdRtsReuseListTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpeRtIPPrefix}, GetNextIndexFsMIBgp4MpeRfdRtsReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeRfdRtsReuseListTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpeRtIPPrefixLen}, GetNextIndexFsMIBgp4MpeRfdRtsReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4MpeRfdRtsReuseListTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdRtsReusePeerType}, GetNextIndexFsMIBgp4MpeRfdRtsReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeRfdRtsReuseListTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpePeerRemAddress}, GetNextIndexFsMIBgp4MpeRfdRtsReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeRfdRtsReuseListTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdRtReuseListRtFom}, GetNextIndexFsMIBgp4MpeRfdRtsReuseListTable, FsMIBgp4mpeRfdRtReuseListRtFomGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeRfdRtsReuseListTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdRtReuseListRtLastUpdtTime}, GetNextIndexFsMIBgp4MpeRfdRtsReuseListTable, FsMIBgp4mpeRfdRtReuseListRtLastUpdtTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeRfdRtsReuseListTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdRtReuseListRtState}, GetNextIndexFsMIBgp4MpeRfdRtsReuseListTable, FsMIBgp4mpeRfdRtReuseListRtStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeRfdRtsReuseListTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdRtReuseListRtStatus}, GetNextIndexFsMIBgp4MpeRfdRtsReuseListTable, FsMIBgp4mpeRfdRtReuseListRtStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeRfdRtsReuseListTableINDEX, 7, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdPeerRemIpAddrType}, GetNextIndexFsMIBgp4MpeRfdPeerReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeRfdPeerReuseListTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdPeerRemIpAddr}, GetNextIndexFsMIBgp4MpeRfdPeerReuseListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeRfdPeerReuseListTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdPeerReuseListPeerFom}, GetNextIndexFsMIBgp4MpeRfdPeerReuseListTable, FsMIBgp4mpeRfdPeerReuseListPeerFomGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeRfdPeerReuseListTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdPeerReuseListLastUpdtTime}, GetNextIndexFsMIBgp4MpeRfdPeerReuseListTable, FsMIBgp4mpeRfdPeerReuseListLastUpdtTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIBgp4MpeRfdPeerReuseListTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdPeerReuseListPeerState}, GetNextIndexFsMIBgp4MpeRfdPeerReuseListTable, FsMIBgp4mpeRfdPeerReuseListPeerStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeRfdPeerReuseListTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4mpeRfdPeerReuseListPeerStatus}, GetNextIndexFsMIBgp4MpeRfdPeerReuseListTable, FsMIBgp4mpeRfdPeerReuseListPeerStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeRfdPeerReuseListTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4mpeAddCommRtAfi}, GetNextIndexFsMIBgp4MpeCommRouteAddCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeCommRouteAddCommTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeAddCommRtSafi}, GetNextIndexFsMIBgp4MpeCommRouteAddCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeCommRouteAddCommTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeAddCommIpNetwork}, GetNextIndexFsMIBgp4MpeCommRouteAddCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeCommRouteAddCommTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeAddCommIpPrefixLen}, GetNextIndexFsMIBgp4MpeCommRouteAddCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4MpeCommRouteAddCommTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeAddCommVal}, GetNextIndexFsMIBgp4MpeCommRouteAddCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIBgp4MpeCommRouteAddCommTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeAddCommRowStatus}, GetNextIndexFsMIBgp4MpeCommRouteAddCommTable, FsMIBgp4mpeAddCommRowStatusGet, FsMIBgp4mpeAddCommRowStatusSet, FsMIBgp4mpeAddCommRowStatusTest, FsMIBgp4MpeCommRouteAddCommTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeCommRouteAddCommTableINDEX, 6, 0, 1, NULL},

{{13,FsMIBgp4mpeDeleteCommRtAfi}, GetNextIndexFsMIBgp4MpeCommRouteDeleteCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeCommRouteDeleteCommTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeDeleteCommRtSafi}, GetNextIndexFsMIBgp4MpeCommRouteDeleteCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeCommRouteDeleteCommTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeDeleteCommIpNetwork}, GetNextIndexFsMIBgp4MpeCommRouteDeleteCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeCommRouteDeleteCommTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeDeleteCommIpPrefixLen}, GetNextIndexFsMIBgp4MpeCommRouteDeleteCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4MpeCommRouteDeleteCommTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeDeleteCommVal}, GetNextIndexFsMIBgp4MpeCommRouteDeleteCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIBgp4MpeCommRouteDeleteCommTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeDeleteCommRowStatus}, GetNextIndexFsMIBgp4MpeCommRouteDeleteCommTable, FsMIBgp4mpeDeleteCommRowStatusGet, FsMIBgp4mpeDeleteCommRowStatusSet, FsMIBgp4mpeDeleteCommRowStatusTest, FsMIBgp4MpeCommRouteDeleteCommTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeCommRouteDeleteCommTableINDEX, 6, 0, 1, NULL},

{{13,FsMIBgp4mpeCommSetStatusAfi}, GetNextIndexFsMIBgp4MpeCommRouteCommSetStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeCommRouteCommSetStatusTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeCommSetStatusSafi}, GetNextIndexFsMIBgp4MpeCommRouteCommSetStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeCommRouteCommSetStatusTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeCommSetStatusIpNetwork}, GetNextIndexFsMIBgp4MpeCommRouteCommSetStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeCommRouteCommSetStatusTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeCommSetStatusIpPrefixLen}, GetNextIndexFsMIBgp4MpeCommRouteCommSetStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4MpeCommRouteCommSetStatusTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeCommSetStatus}, GetNextIndexFsMIBgp4MpeCommRouteCommSetStatusTable, FsMIBgp4mpeCommSetStatusGet, FsMIBgp4mpeCommSetStatusSet, FsMIBgp4mpeCommSetStatusTest, FsMIBgp4MpeCommRouteCommSetStatusTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeCommRouteCommSetStatusTableINDEX, 5, 0, 0, "4"},

{{13,FsMIBgp4mpeCommSetStatusRowStatus}, GetNextIndexFsMIBgp4MpeCommRouteCommSetStatusTable, FsMIBgp4mpeCommSetStatusRowStatusGet, FsMIBgp4mpeCommSetStatusRowStatusSet, FsMIBgp4mpeCommSetStatusRowStatusTest, FsMIBgp4MpeCommRouteCommSetStatusTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeCommRouteCommSetStatusTableINDEX, 5, 0, 1, NULL},

{{13,FsMIBgp4mpeAddExtCommRtAfi}, GetNextIndexFsMIBgp4MpeExtCommRouteAddExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeExtCommRouteAddExtCommTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeAddExtCommRtSafi}, GetNextIndexFsMIBgp4MpeExtCommRouteAddExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeExtCommRouteAddExtCommTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeAddExtCommIpNetwork}, GetNextIndexFsMIBgp4MpeExtCommRouteAddExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeExtCommRouteAddExtCommTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeAddExtCommIpPrefixLen}, GetNextIndexFsMIBgp4MpeExtCommRouteAddExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4MpeExtCommRouteAddExtCommTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeAddExtCommVal}, GetNextIndexFsMIBgp4MpeExtCommRouteAddExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeExtCommRouteAddExtCommTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeAddExtCommRowStatus}, GetNextIndexFsMIBgp4MpeExtCommRouteAddExtCommTable, FsMIBgp4mpeAddExtCommRowStatusGet, FsMIBgp4mpeAddExtCommRowStatusSet, FsMIBgp4mpeAddExtCommRowStatusTest, FsMIBgp4MpeExtCommRouteAddExtCommTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeExtCommRouteAddExtCommTableINDEX, 6, 0, 1, NULL},

{{13,FsMIBgp4mpeDeleteExtCommRtAfi}, GetNextIndexFsMIBgp4MpeExtCommRouteDeleteExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeExtCommRouteDeleteExtCommTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeDeleteExtCommRtSafi}, GetNextIndexFsMIBgp4MpeExtCommRouteDeleteExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeExtCommRouteDeleteExtCommTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeDeleteExtCommIpNetwork}, GetNextIndexFsMIBgp4MpeExtCommRouteDeleteExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeExtCommRouteDeleteExtCommTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeDeleteExtCommIpPrefixLen}, GetNextIndexFsMIBgp4MpeExtCommRouteDeleteExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4MpeExtCommRouteDeleteExtCommTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeDeleteExtCommVal}, GetNextIndexFsMIBgp4MpeExtCommRouteDeleteExtCommTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeExtCommRouteDeleteExtCommTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeDeleteExtCommRowStatus}, GetNextIndexFsMIBgp4MpeExtCommRouteDeleteExtCommTable, FsMIBgp4mpeDeleteExtCommRowStatusGet, FsMIBgp4mpeDeleteExtCommRowStatusSet, FsMIBgp4mpeDeleteExtCommRowStatusTest, FsMIBgp4MpeExtCommRouteDeleteExtCommTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeExtCommRouteDeleteExtCommTableINDEX, 6, 0, 1, NULL},

{{13,FsMIBgp4mpeExtCommSetStatusRtAfi}, GetNextIndexFsMIBgp4MpeExtCommRouteExtCommSetStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeExtCommRouteExtCommSetStatusTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeExtCommSetStatusRtSafi}, GetNextIndexFsMIBgp4MpeExtCommRouteExtCommSetStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeExtCommRouteExtCommSetStatusTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeExtCommSetStatusIpNetwork}, GetNextIndexFsMIBgp4MpeExtCommRouteExtCommSetStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeExtCommRouteExtCommSetStatusTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeExtCommSetStatusIpPrefixLen}, GetNextIndexFsMIBgp4MpeExtCommRouteExtCommSetStatusTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4MpeExtCommRouteExtCommSetStatusTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeExtCommSetStatus}, GetNextIndexFsMIBgp4MpeExtCommRouteExtCommSetStatusTable, FsMIBgp4mpeExtCommSetStatusGet, FsMIBgp4mpeExtCommSetStatusSet, FsMIBgp4mpeExtCommSetStatusTest, FsMIBgp4MpeExtCommRouteExtCommSetStatusTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeExtCommRouteExtCommSetStatusTableINDEX, 5, 0, 0, "4"},

{{13,FsMIBgp4mpeExtCommSetStatusRowStatus}, GetNextIndexFsMIBgp4MpeExtCommRouteExtCommSetStatusTable, FsMIBgp4mpeExtCommSetStatusRowStatusGet, FsMIBgp4mpeExtCommSetStatusRowStatusSet, FsMIBgp4mpeExtCommSetStatusRowStatusTest, FsMIBgp4MpeExtCommRouteExtCommSetStatusTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeExtCommRouteExtCommSetStatusTableINDEX, 5, 0, 1, NULL},

{{13,FsMIBgp4mpePeerLinkType}, GetNextIndexFsMIBgp4MpePeerLinkBwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpePeerLinkBwTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4mpePeerLinkRemAddr}, GetNextIndexFsMIBgp4MpePeerLinkBwTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpePeerLinkBwTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4mpeLinkBandWidth}, GetNextIndexFsMIBgp4MpePeerLinkBwTable, FsMIBgp4mpeLinkBandWidthGet, FsMIBgp4mpeLinkBandWidthSet, FsMIBgp4mpeLinkBandWidthTest, FsMIBgp4MpePeerLinkBwTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBgp4MpePeerLinkBwTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4mpePeerLinkBwRowStatus}, GetNextIndexFsMIBgp4MpePeerLinkBwTable, FsMIBgp4mpePeerLinkBwRowStatusGet, FsMIBgp4mpePeerLinkBwRowStatusSet, FsMIBgp4mpePeerLinkBwRowStatusTest, FsMIBgp4MpePeerLinkBwTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpePeerLinkBwTableINDEX, 3, 0, 1, NULL},

{{13,FsMIBgp4mpeCapPeerType}, GetNextIndexFsMIBgp4MpeCapSupportedCapsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeCapSupportedCapsTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeCapPeerRemoteIpAddr}, GetNextIndexFsMIBgp4MpeCapSupportedCapsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeCapSupportedCapsTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeSupportedCapabilityCode}, GetNextIndexFsMIBgp4MpeCapSupportedCapsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4MpeCapSupportedCapsTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeSupportedCapabilityLength}, GetNextIndexFsMIBgp4MpeCapSupportedCapsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4MpeCapSupportedCapsTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeSupportedCapabilityValue}, GetNextIndexFsMIBgp4MpeCapSupportedCapsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeCapSupportedCapsTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeCapSupportedCapsRowStatus}, GetNextIndexFsMIBgp4MpeCapSupportedCapsTable, FsMIBgp4mpeCapSupportedCapsRowStatusGet, FsMIBgp4mpeCapSupportedCapsRowStatusSet, FsMIBgp4mpeCapSupportedCapsRowStatusTest, FsMIBgp4MpeCapSupportedCapsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeCapSupportedCapsTableINDEX, 6, 0, 1, NULL},

{{13,FsMIBgp4mpeCapAnnouncedStatus}, GetNextIndexFsMIBgp4MpeCapSupportedCapsTable, FsMIBgp4mpeCapAnnouncedStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeCapSupportedCapsTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeCapReceivedStatus}, GetNextIndexFsMIBgp4MpeCapSupportedCapsTable, FsMIBgp4mpeCapReceivedStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeCapSupportedCapsTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeCapNegotiatedStatus}, GetNextIndexFsMIBgp4MpeCapSupportedCapsTable, FsMIBgp4mpeCapNegotiatedStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeCapSupportedCapsTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeCapConfiguredStatus}, GetNextIndexFsMIBgp4MpeCapSupportedCapsTable, FsMIBgp4mpeCapConfiguredStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4MpeCapSupportedCapsTableINDEX, 6, 0, 0, NULL},

{{13,FsMIBgp4mpeRtRefreshInboundPeerType}, GetNextIndexFsMIBgp4MpeRtRefreshInboundTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeRtRefreshInboundTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeRtRefreshInboundPeerAddr}, GetNextIndexFsMIBgp4MpeRtRefreshInboundTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeRtRefreshInboundTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeRtRefreshInboundAfi}, GetNextIndexFsMIBgp4MpeRtRefreshInboundTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeRtRefreshInboundTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeRtRefreshInboundSafi}, GetNextIndexFsMIBgp4MpeRtRefreshInboundTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeRtRefreshInboundTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeRtRefreshInboundRequest}, GetNextIndexFsMIBgp4MpeRtRefreshInboundTable, FsMIBgp4mpeRtRefreshInboundRequestGet, FsMIBgp4mpeRtRefreshInboundRequestSet, FsMIBgp4mpeRtRefreshInboundRequestTest, FsMIBgp4MpeRtRefreshInboundTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeRtRefreshInboundTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeRtRefreshInboundPrefixFilter}, GetNextIndexFsMIBgp4MpeRtRefreshInboundTable, FsMIBgp4mpeRtRefreshInboundPrefixFilterGet, FsMIBgp4mpeRtRefreshInboundPrefixFilterSet, FsMIBgp4mpeRtRefreshInboundPrefixFilterTest, FsMIBgp4MpeRtRefreshInboundTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeRtRefreshInboundTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeRtRefreshStatisticsPeerType}, GetNextIndexFsMIBgp4MpeRtRefreshStatisticsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeRtRefreshStatisticsTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeRtRefreshStatisticsPeerAddr}, GetNextIndexFsMIBgp4MpeRtRefreshStatisticsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeRtRefreshStatisticsTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeRtRefreshStatisticsAfi}, GetNextIndexFsMIBgp4MpeRtRefreshStatisticsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeRtRefreshStatisticsTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeRtRefreshStatisticsSafi}, GetNextIndexFsMIBgp4MpeRtRefreshStatisticsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeRtRefreshStatisticsTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeRtRefreshStatisticsRtRefMsgSentCntr}, GetNextIndexFsMIBgp4MpeRtRefreshStatisticsTable, FsMIBgp4mpeRtRefreshStatisticsRtRefMsgSentCntrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBgp4MpeRtRefreshStatisticsTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeRtRefreshStatisticsRtRefMsgTxErrCntr}, GetNextIndexFsMIBgp4MpeRtRefreshStatisticsTable, FsMIBgp4mpeRtRefreshStatisticsRtRefMsgTxErrCntrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBgp4MpeRtRefreshStatisticsTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeRtRefreshStatisticsRtRefMsgRcvdCntr}, GetNextIndexFsMIBgp4MpeRtRefreshStatisticsTable, FsMIBgp4mpeRtRefreshStatisticsRtRefMsgRcvdCntrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBgp4MpeRtRefreshStatisticsTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeRtRefreshStatisticsRtRefMsgInvalidCntr}, GetNextIndexFsMIBgp4MpeRtRefreshStatisticsTable, FsMIBgp4mpeRtRefreshStatisticsRtRefMsgInvalidCntrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBgp4MpeRtRefreshStatisticsTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeSoftReconfigOutboundPeerType}, GetNextIndexFsMIBgp4MpeSoftReconfigOutboundTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeSoftReconfigOutboundTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeSoftReconfigOutboundPeerAddr}, GetNextIndexFsMIBgp4MpeSoftReconfigOutboundTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpeSoftReconfigOutboundTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeSoftReconfigOutboundAfi}, GetNextIndexFsMIBgp4MpeSoftReconfigOutboundTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeSoftReconfigOutboundTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeSoftReconfigOutboundSafi}, GetNextIndexFsMIBgp4MpeSoftReconfigOutboundTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpeSoftReconfigOutboundTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4mpeSoftReconfigOutboundRequest}, GetNextIndexFsMIBgp4MpeSoftReconfigOutboundTable, FsMIBgp4mpeSoftReconfigOutboundRequestGet, FsMIBgp4mpeSoftReconfigOutboundRequestSet, FsMIBgp4mpeSoftReconfigOutboundRequestTest, FsMIBgp4MpeSoftReconfigOutboundTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4MpeSoftReconfigOutboundTableINDEX, 5, 0, 0, NULL},

{{12,FsMIBgp4MpePeerRemoteAddrType}, GetNextIndexFsMIBgp4MpePrefixCountersTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpePrefixCountersTableINDEX, 5, 0, 0, NULL},

{{12,FsMIBgp4MpePeerRemoteAddr}, GetNextIndexFsMIBgp4MpePrefixCountersTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4MpePrefixCountersTableINDEX, 5, 0, 0, NULL},

{{12,FsMIBgp4MpePrefixCountersAfi}, GetNextIndexFsMIBgp4MpePrefixCountersTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpePrefixCountersTableINDEX, 5, 0, 0, NULL},

{{12,FsMIBgp4MpePrefixCountersSafi}, GetNextIndexFsMIBgp4MpePrefixCountersTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4MpePrefixCountersTableINDEX, 5, 0, 0, NULL},

{{12,FsMIBgp4MpePrefixCountersPrefixesReceived}, GetNextIndexFsMIBgp4MpePrefixCountersTable, FsMIBgp4MpePrefixCountersPrefixesReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBgp4MpePrefixCountersTableINDEX, 5, 0, 0, NULL},

{{12,FsMIBgp4MpePrefixCountersPrefixesSent}, GetNextIndexFsMIBgp4MpePrefixCountersTable, FsMIBgp4MpePrefixCountersPrefixesSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBgp4MpePrefixCountersTableINDEX, 5, 0, 0, NULL},

{{12,FsMIBgp4MpePrefixCountersWithdrawsReceived}, GetNextIndexFsMIBgp4MpePrefixCountersTable, FsMIBgp4MpePrefixCountersWithdrawsReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBgp4MpePrefixCountersTableINDEX, 5, 0, 0, NULL},

{{12,FsMIBgp4MpePrefixCountersWithdrawsSent}, GetNextIndexFsMIBgp4MpePrefixCountersTable, FsMIBgp4MpePrefixCountersWithdrawsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIBgp4MpePrefixCountersTableINDEX, 5, 0, 0, NULL},

{{12,FsMIBgp4MpePrefixCountersInPrefixes}, GetNextIndexFsMIBgp4MpePrefixCountersTable, FsMIBgp4MpePrefixCountersInPrefixesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIBgp4MpePrefixCountersTableINDEX, 5, 0, 0, NULL},

{{12,FsMIBgp4MpePrefixCountersInPrefixesAccepted}, GetNextIndexFsMIBgp4MpePrefixCountersTable, FsMIBgp4MpePrefixCountersInPrefixesAcceptedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIBgp4MpePrefixCountersTableINDEX, 5, 0, 0, NULL},

{{12,FsMIBgp4MpePrefixCountersInPrefixesRejected}, GetNextIndexFsMIBgp4MpePrefixCountersTable, FsMIBgp4MpePrefixCountersInPrefixesRejectedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIBgp4MpePrefixCountersTableINDEX, 5, 0, 0, NULL},

{{12,FsMIBgp4MpePrefixCountersOutPrefixes}, GetNextIndexFsMIBgp4MpePrefixCountersTable, FsMIBgp4MpePrefixCountersOutPrefixesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIBgp4MpePrefixCountersTableINDEX, 5, 0, 0, NULL},

{{13,FsMIBgp4DistInOutRouteMapName}, GetNextIndexFsMIBgp4DistInOutRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4DistInOutRouteMapTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4DistInOutRouteMapType}, GetNextIndexFsMIBgp4DistInOutRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4DistInOutRouteMapTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4DistInOutRouteMapValue}, GetNextIndexFsMIBgp4DistInOutRouteMapTable, FsMIBgp4DistInOutRouteMapValueGet, FsMIBgp4DistInOutRouteMapValueSet, FsMIBgp4DistInOutRouteMapValueTest, FsMIBgp4DistInOutRouteMapTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4DistInOutRouteMapTableINDEX, 3, 0, 0, NULL},

{{13,FsMIBgp4DistInOutRouteMapRowStatus}, GetNextIndexFsMIBgp4DistInOutRouteMapTable, FsMIBgp4DistInOutRouteMapRowStatusGet, FsMIBgp4DistInOutRouteMapRowStatusSet, FsMIBgp4DistInOutRouteMapRowStatusTest, FsMIBgp4DistInOutRouteMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4DistInOutRouteMapTableINDEX, 3, 0, 1, NULL},

{{12,FsMIBgp4TrapContextId}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{12,FsMIBgp4TrapRouteAddressType}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{12,FsMIBgp4TrapRoutePrefix}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{12,FsMIBgp4NextHop}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{12,FsMIBgp4TrapPeerAddrType}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{12,FsMIBgp4TrapPeerAddr}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{13,FsMIBgp4NeighborRouteMapPeerAddrType}, GetNextIndexFsMIBgp4NeighborRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4NeighborRouteMapTableINDEX, 4, 0, 0, NULL},

{{13,FsMIBgp4NeighborRouteMapPeer}, GetNextIndexFsMIBgp4NeighborRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4NeighborRouteMapTableINDEX, 4, 0, 0, NULL},

{{13,FsMIBgp4NeighborRouteMapDirection}, GetNextIndexFsMIBgp4NeighborRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4NeighborRouteMapTableINDEX, 4, 0, 0, NULL},

{{13,FsMIBgp4NeighborRouteMapName}, GetNextIndexFsMIBgp4NeighborRouteMapTable, FsMIBgp4NeighborRouteMapNameGet, FsMIBgp4NeighborRouteMapNameSet, FsMIBgp4NeighborRouteMapNameTest, FsMIBgp4NeighborRouteMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4NeighborRouteMapTableINDEX, 4, 0, 0, NULL},

{{13,FsMIBgp4NeighborRouteMapRowStatus}, GetNextIndexFsMIBgp4NeighborRouteMapTable, FsMIBgp4NeighborRouteMapRowStatusGet, FsMIBgp4NeighborRouteMapRowStatusSet, FsMIBgp4NeighborRouteMapRowStatusTest, FsMIBgp4NeighborRouteMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4NeighborRouteMapTableINDEX, 4, 0, 1, NULL},

{{12,FsMIBgp4PeerGroupName}, GetNextIndexFsMIBgp4PeerGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerGroupAddrType}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupAddrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerGroupRemoteAs}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupRemoteAsGet, FsMIBgp4PeerGroupRemoteAsSet, FsMIBgp4PeerGroupRemoteAsTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerGroupHoldTimeConfigured}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupHoldTimeConfiguredGet, FsMIBgp4PeerGroupHoldTimeConfiguredSet, FsMIBgp4PeerGroupHoldTimeConfiguredTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerGroupKeepAliveConfigured}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupKeepAliveConfiguredGet, FsMIBgp4PeerGroupKeepAliveConfiguredSet, FsMIBgp4PeerGroupKeepAliveConfiguredTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerGroupConnectRetryInterval}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupConnectRetryIntervalGet, FsMIBgp4PeerGroupConnectRetryIntervalSet, FsMIBgp4PeerGroupConnectRetryIntervalTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerGroupMinASOriginInterval}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupMinASOriginIntervalGet, FsMIBgp4PeerGroupMinASOriginIntervalSet, FsMIBgp4PeerGroupMinASOriginIntervalTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerGroupMinRouteAdvInterval}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupMinRouteAdvIntervalGet, FsMIBgp4PeerGroupMinRouteAdvIntervalSet, FsMIBgp4PeerGroupMinRouteAdvIntervalTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerGroupAllowAutomaticStart}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupAllowAutomaticStartGet, FsMIBgp4PeerGroupAllowAutomaticStartSet, FsMIBgp4PeerGroupAllowAutomaticStartTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, "2"},

{{12,FsMIBgp4PeerGroupAllowAutomaticStop}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupAllowAutomaticStopGet, FsMIBgp4PeerGroupAllowAutomaticStopSet, FsMIBgp4PeerGroupAllowAutomaticStopTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, "2"},

{{12,FsMIBgp4PeerGroupIdleHoldTimeConfigured}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupIdleHoldTimeConfiguredGet, FsMIBgp4PeerGroupIdleHoldTimeConfiguredSet, FsMIBgp4PeerGroupIdleHoldTimeConfiguredTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, "60"},

{{12,FsMIBgp4PeerGroupDampPeerOscillations}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupDampPeerOscillationsGet, FsMIBgp4PeerGroupDampPeerOscillationsSet, FsMIBgp4PeerGroupDampPeerOscillationsTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, "2"},

{{12,FsMIBgp4PeerGroupDelayOpen}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupDelayOpenGet, FsMIBgp4PeerGroupDelayOpenSet, FsMIBgp4PeerGroupDelayOpenTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, "2"},

{{12,FsMIBgp4PeerGroupDelayOpenTimeConfigured}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupDelayOpenTimeConfiguredGet, FsMIBgp4PeerGroupDelayOpenTimeConfiguredSet, FsMIBgp4PeerGroupDelayOpenTimeConfiguredTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, "0"},

{{12,FsMIBgp4PeerGroupPrefixUpperLimit}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupPrefixUpperLimitGet, FsMIBgp4PeerGroupPrefixUpperLimitSet, FsMIBgp4PeerGroupPrefixUpperLimitTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, "5000"},

{{12,FsMIBgp4PeerGroupTcpConnectRetryCnt}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupTcpConnectRetryCntGet, FsMIBgp4PeerGroupTcpConnectRetryCntSet, FsMIBgp4PeerGroupTcpConnectRetryCntTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, "5"},

{{12,FsMIBgp4PeerGroupEBGPMultiHop}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupEBGPMultiHopGet, FsMIBgp4PeerGroupEBGPMultiHopSet, FsMIBgp4PeerGroupEBGPMultiHopTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerGroupEBGPHopLimit}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupEBGPHopLimitGet, FsMIBgp4PeerGroupEBGPHopLimitSet, FsMIBgp4PeerGroupEBGPHopLimitTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerGroupNextHopSelf}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupNextHopSelfGet, FsMIBgp4PeerGroupNextHopSelfSet, FsMIBgp4PeerGroupNextHopSelfTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, "1"},

{{12,FsMIBgp4PeerGroupRflClient}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupRflClientGet, FsMIBgp4PeerGroupRflClientSet, FsMIBgp4PeerGroupRflClientTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, "1"},

{{12,FsMIBgp4PeerGroupTcpSendBufSize}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupTcpSendBufSizeGet, FsMIBgp4PeerGroupTcpSendBufSizeSet, FsMIBgp4PeerGroupTcpSendBufSizeTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, "65536"},

{{12,FsMIBgp4PeerGroupTcpRcvBufSize}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupTcpRcvBufSizeGet, FsMIBgp4PeerGroupTcpRcvBufSizeSet, FsMIBgp4PeerGroupTcpRcvBufSizeTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, "65536"},

{{12,FsMIBgp4PeerGroupCommSendStatus}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupCommSendStatusGet, FsMIBgp4PeerGroupCommSendStatusSet, FsMIBgp4PeerGroupCommSendStatusTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, "2"},

{{12,FsMIBgp4PeerGroupECommSendStatus}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupECommSendStatusGet, FsMIBgp4PeerGroupECommSendStatusSet, FsMIBgp4PeerGroupECommSendStatusTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, "2"},

{{12,FsMIBgp4PeerGroupPassive}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupPassiveGet, FsMIBgp4PeerGroupPassiveSet, FsMIBgp4PeerGroupPassiveTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerGroupDefaultOriginate}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupDefaultOriginateGet, FsMIBgp4PeerGroupDefaultOriginateSet, FsMIBgp4PeerGroupDefaultOriginateTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, "2"},

{{12,FsMIBgp4PeerGroupActivateMPCapability}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupActivateMPCapabilityGet, FsMIBgp4PeerGroupActivateMPCapabilitySet, FsMIBgp4PeerGroupActivateMPCapabilityTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerGroupDeactivateMPCapability}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupDeactivateMPCapabilityGet, FsMIBgp4PeerGroupDeactivateMPCapabilitySet, FsMIBgp4PeerGroupDeactivateMPCapabilityTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerGroupRouteMapNameIn}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupRouteMapNameInGet, FsMIBgp4PeerGroupRouteMapNameInSet, FsMIBgp4PeerGroupRouteMapNameInTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerGroupRouteMapNameOut}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupRouteMapNameOutGet, FsMIBgp4PeerGroupRouteMapNameOutSet, FsMIBgp4PeerGroupRouteMapNameOutTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerGroupStatus}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupStatusGet, FsMIBgp4PeerGroupStatusSet, FsMIBgp4PeerGroupStatusTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 1, NULL},

{{12,FsMIBgp4PeerGroupIpPrefixNameIn}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupIpPrefixNameInGet, FsMIBgp4PeerGroupIpPrefixNameInSet, FsMIBgp4PeerGroupIpPrefixNameInTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerGroupIpPrefixNameOut}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupIpPrefixNameOutGet, FsMIBgp4PeerGroupIpPrefixNameOutSet, FsMIBgp4PeerGroupIpPrefixNameOutTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerGroupOrfType}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupOrfTypeGet, FsMIBgp4PeerGroupOrfTypeSet, FsMIBgp4PeerGroupOrfTypeTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerGroupOrfCapMode}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupOrfCapModeGet, FsMIBgp4PeerGroupOrfCapModeSet, FsMIBgp4PeerGroupOrfCapModeTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerGroupOrfRequest}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupOrfRequestGet, FsMIBgp4PeerGroupOrfRequestSet, FsMIBgp4PeerGroupOrfRequestTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerGroupBfdStatus}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupBfdStatusGet, FsMIBgp4PeerGroupBfdStatusSet, FsMIBgp4PeerGroupBfdStatusTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, "2"},

{{12,FsMIBgp4PeerGroupOverrideCapability}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupOverrideCapabilityGet, FsMIBgp4PeerGroupOverrideCapabilitySet, FsMIBgp4PeerGroupOverrideCapabilityTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, "2"},

{{12,FsMIBgp4PeerGroupLocalAs}, GetNextIndexFsMIBgp4PeerGroupTable, FsMIBgp4PeerGroupLocalAsGet, FsMIBgp4PeerGroupLocalAsSet, FsMIBgp4PeerGroupLocalAsTest, FsMIBgp4PeerGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIBgp4PeerGroupTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4PeerAddrType}, GetNextIndexFsMIBgp4PeerGroupListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4PeerGroupListTableINDEX, 4, 0, 0, NULL},

{{12,FsMIBgp4PeerAddress}, GetNextIndexFsMIBgp4PeerGroupListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4PeerGroupListTableINDEX, 4, 0, 0, NULL},

{{12,FsMIBgp4PeerAddStatus}, GetNextIndexFsMIBgp4PeerGroupListTable, FsMIBgp4PeerAddStatusGet, FsMIBgp4PeerAddStatusSet, FsMIBgp4PeerAddStatusTest, FsMIBgp4PeerGroupListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4PeerGroupListTableINDEX, 4, 0, 0, NULL},

{{10,FsMIBgp4RestartReason}, NULL, FsMIBgp4RestartReasonGet, FsMIBgp4RestartReasonSet, FsMIBgp4RestartReasonTest, FsMIBgp4RestartReasonDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{13,FsMIBgp4TCPMKTAuthKeyId}, GetNextIndexFsMIBgp4TCPMKTAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4TCPMKTAuthTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBgp4TCPMKTAuthRecvKeyId}, GetNextIndexFsMIBgp4TCPMKTAuthTable, FsMIBgp4TCPMKTAuthRecvKeyIdGet, FsMIBgp4TCPMKTAuthRecvKeyIdSet, FsMIBgp4TCPMKTAuthRecvKeyIdTest, FsMIBgp4TCPMKTAuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4TCPMKTAuthTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBgp4TCPMKTAuthMasterKey}, GetNextIndexFsMIBgp4TCPMKTAuthTable, FsMIBgp4TCPMKTAuthMasterKeyGet, FsMIBgp4TCPMKTAuthMasterKeySet, FsMIBgp4TCPMKTAuthMasterKeyTest, FsMIBgp4TCPMKTAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4TCPMKTAuthTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBgp4TCPMKTAuthAlgo}, GetNextIndexFsMIBgp4TCPMKTAuthTable, FsMIBgp4TCPMKTAuthAlgoGet, FsMIBgp4TCPMKTAuthAlgoSet, FsMIBgp4TCPMKTAuthAlgoTest, FsMIBgp4TCPMKTAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4TCPMKTAuthTableINDEX, 2, 0, 0, "1"},

{{13,FsMIBgp4TCPMKTAuthTcpOptExc}, GetNextIndexFsMIBgp4TCPMKTAuthTable, FsMIBgp4TCPMKTAuthTcpOptExcGet, FsMIBgp4TCPMKTAuthTcpOptExcSet, FsMIBgp4TCPMKTAuthTcpOptExcTest, FsMIBgp4TCPMKTAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4TCPMKTAuthTableINDEX, 2, 0, 0, NULL},

{{13,FsMIBgp4TCPMKTAuthRowStatus}, GetNextIndexFsMIBgp4TCPMKTAuthTable, FsMIBgp4TCPMKTAuthRowStatusGet, FsMIBgp4TCPMKTAuthRowStatusSet, FsMIBgp4TCPMKTAuthRowStatusTest, FsMIBgp4TCPMKTAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4TCPMKTAuthTableINDEX, 2, 0, 1, NULL},

{{13,FsMIBgp4TCPAOAuthPeerType}, GetNextIndexFsMIBgp4TCPAOAuthPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4TCPAOAuthPeerTableINDEX, 4, 0, 0, NULL},

{{13,FsMIBgp4TCPAOAuthPeerAddr}, GetNextIndexFsMIBgp4TCPAOAuthPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4TCPAOAuthPeerTableINDEX, 4, 0, 0, NULL},

{{13,FsMIBgp4TCPAOAuthKeyId}, GetNextIndexFsMIBgp4TCPAOAuthPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIBgp4TCPAOAuthPeerTableINDEX, 4, 0, 0, NULL},

{{13,FsMIBgp4TCPAOAuthKeyStatus}, GetNextIndexFsMIBgp4TCPAOAuthPeerTable, FsMIBgp4TCPAOAuthKeyStatusGet, FsMIBgp4TCPAOAuthKeyStatusSet, FsMIBgp4TCPAOAuthKeyStatusTest, FsMIBgp4TCPAOAuthPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4TCPAOAuthPeerTableINDEX, 4, 0, 0, NULL},

{{13,FsMIBgp4TCPAOAuthKeyStartAccept}, GetNextIndexFsMIBgp4TCPAOAuthPeerTable, FsMIBgp4TCPAOAuthKeyStartAcceptGet, FsMIBgp4TCPAOAuthKeyStartAcceptSet, FsMIBgp4TCPAOAuthKeyStartAcceptTest, FsMIBgp4TCPAOAuthPeerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4TCPAOAuthPeerTableINDEX, 4, 0, 0, "0000000000000000"},

{{13,FsMIBgp4TCPAOAuthKeyStartGenerate}, GetNextIndexFsMIBgp4TCPAOAuthPeerTable, FsMIBgp4TCPAOAuthKeyStartGenerateGet, FsMIBgp4TCPAOAuthKeyStartGenerateSet, FsMIBgp4TCPAOAuthKeyStartGenerateTest, FsMIBgp4TCPAOAuthPeerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4TCPAOAuthPeerTableINDEX, 4, 0, 0, "0000000000000000"},

{{13,FsMIBgp4TCPAOAuthKeyStopGenerate}, GetNextIndexFsMIBgp4TCPAOAuthPeerTable, FsMIBgp4TCPAOAuthKeyStopGenerateGet, FsMIBgp4TCPAOAuthKeyStopGenerateSet, FsMIBgp4TCPAOAuthKeyStopGenerateTest, FsMIBgp4TCPAOAuthPeerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4TCPAOAuthPeerTableINDEX, 4, 0, 0, "0000000000000000"},

{{13,FsMIBgp4TCPAOAuthKeyStopAccept}, GetNextIndexFsMIBgp4TCPAOAuthPeerTable, FsMIBgp4TCPAOAuthKeyStopAcceptGet, FsMIBgp4TCPAOAuthKeyStopAcceptSet, FsMIBgp4TCPAOAuthKeyStopAcceptTest, FsMIBgp4TCPAOAuthPeerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIBgp4TCPAOAuthPeerTableINDEX, 4, 0, 0, "0000000000000000"},

{{12,FsMIBgp4ORFPeerAddrType}, GetNextIndexFsMIBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4ORFListTableINDEX, 12, 0, 0, NULL},

{{12,FsMIBgp4ORFPeerAddr}, GetNextIndexFsMIBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4ORFListTableINDEX, 12, 0, 0, NULL},

{{12,FsMIBgp4ORFAfi}, GetNextIndexFsMIBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4ORFListTableINDEX, 12, 0, 0, NULL},

{{12,FsMIBgp4ORFSafi}, GetNextIndexFsMIBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4ORFListTableINDEX, 12, 0, 0, NULL},

{{12,FsMIBgp4ORFType}, GetNextIndexFsMIBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIBgp4ORFListTableINDEX, 12, 0, 0, NULL},

{{12,FsMIBgp4ORFSequence}, GetNextIndexFsMIBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIBgp4ORFListTableINDEX, 12, 0, 0, NULL},

{{12,FsMIBgp4ORFAddrPrefix}, GetNextIndexFsMIBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4ORFListTableINDEX, 12, 0, 0, NULL},

{{12,FsMIBgp4ORFAddrPrefixLen}, GetNextIndexFsMIBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIBgp4ORFListTableINDEX, 12, 0, 0, NULL},

{{12,FsMIBgp4ORFMinLength}, GetNextIndexFsMIBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIBgp4ORFListTableINDEX, 12, 0, 0, NULL},

{{12,FsMIBgp4ORFMaxLength}, GetNextIndexFsMIBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIBgp4ORFListTableINDEX, 12, 0, 0, NULL},

{{12,FsMIBgp4ORFAction}, GetNextIndexFsMIBgp4ORFListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIBgp4ORFListTableINDEX, 12, 0, 0, NULL},

{{12,FsMIBgp4RRDNetworkAddr}, GetNextIndexFsMIBgp4RRDNetworkTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIBgp4RRDNetworkTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4RRDNetworkAddrType}, GetNextIndexFsMIBgp4RRDNetworkTable, FsMIBgp4RRDNetworkAddrTypeGet, FsMIBgp4RRDNetworkAddrTypeSet, FsMIBgp4RRDNetworkAddrTypeTest, FsMIBgp4RRDNetworkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4RRDNetworkTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4RRDNetworkPrefixLen}, GetNextIndexFsMIBgp4RRDNetworkTable, FsMIBgp4RRDNetworkPrefixLenGet, FsMIBgp4RRDNetworkPrefixLenSet, FsMIBgp4RRDNetworkPrefixLenTest, FsMIBgp4RRDNetworkTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIBgp4RRDNetworkTableINDEX, 2, 0, 0, NULL},

{{12,FsMIBgp4RRDNetworkRowStatus}, GetNextIndexFsMIBgp4RRDNetworkTable, FsMIBgp4RRDNetworkRowStatusGet, FsMIBgp4RRDNetworkRowStatusSet, FsMIBgp4RRDNetworkRowStatusTest, FsMIBgp4RRDNetworkTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIBgp4RRDNetworkTableINDEX, 2, 0, 1, NULL},

{{10,FsMIBgp4MacMobDuplicationTimeInterval}, NULL, FsMIBgp4MacMobDuplicationTimeIntervalGet, FsMIBgp4MacMobDuplicationTimeIntervalSet, FsMIBgp4MacMobDuplicationTimeIntervalTest, FsMIBgp4MacMobDuplicationTimeIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "180"},

{{10,FsMIBgp4MaxMacMoves}, NULL, FsMIBgp4MaxMacMovesGet, FsMIBgp4MaxMacMovesSet, FsMIBgp4MaxMacMovesTest, FsMIBgp4MaxMacMovesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},
}; 
tMibData fsmpbgEntry = { sizeof(fsmpbgMibEntry)/sizeof(fsmpbgMibEntry[0]), fsmpbgMibEntry };

#endif /* _FSMPBGDB_H */

