
/*****
  stdbgmdb.h . This File Contains the Mib DataBase.
*****/

#ifndef _STDBGMDB_H
#define _STDBGMDB_H

/*  The NULL FUNCTION POINTER.  */
# define NULLF ARG_LIST(((INT4 (*)(tSNMP_OID_TYPE *,\
                                          tSNMP_OID_TYPE *,\
                                          UINT1,\
                                          tSNMP_MULTI_DATA_TYPE *)) NULL))
/*  NULL VARBIND FUNCTION POINTER . */
# define NULLVBF  ARG_LIST(((tSNMP_VAR_BIND* (*)(tSNMP_OID_TYPE *,\
                                           tSNMP_OID_TYPE *,\
                                           UINT1,\
                                           UINT1))NULL))
# include "stdbgmid.h"
# include "stdbgcon.h"
# include "stdbgogi.h"

/*  The Declaration of the Group Arrays. */

UINT4 au4_stdbgp4_snmp_TABLE1[] = {1,3,6,1,2,1,15};

/*  The Declaration of the SubGroup Arrays. */

UINT4  au4_SNMP_OGP_BGP_OID [] ={0};
UINT4  au4_SNMP_OGP_BGPPEERTABLE_OID [] ={3,1};
UINT4  au4_SNMP_OGP_BGP4PATHATTRTABLE_OID [] ={6,1};

/*  Declaration of Group OID Table. */
/* Each entry contains Length of Group OID, Pointer to the Group OID,
   Priority of the registering subagent, Timeout for response,
   and Number of Subgroups in that order */

 tSNMP_GroupOIDType stdbgp4_FMAS_GroupOIDTable[] =
{
   {7 , au4_stdbgp4_snmp_TABLE1 , 1 , 10 , 3}
};
/*  Declaration of Base OID Table. */
/* Each entry contains Length of Base OID, Pointer to the Base OID,
   Middle level get function pointer, Middle level test function pointer,
   Middle level set function pointer and Number of objects in that table
   in that order */

 tSNMP_BaseOIDType  stdbgp4_FMAS_BaseOIDTable[] = {
{
0,
au4_SNMP_OGP_BGP_OID,
bgpGet,
NULLF,
NULLF,
3
},
{
2,
au4_SNMP_OGP_BGPPEERTABLE_OID,
bgpPeerEntryGet,
bgpPeerEntryTest,
bgpPeerEntrySet,
24
},
{
2,
au4_SNMP_OGP_BGP4PATHATTRTABLE_OID,
bgp4PathAttrEntryGet,
NULLF,
NULLF,
14
}
};
/* Declaration of MIB Object Table. */
/* Each entry contains Name of the table, Permissions for
   the object and Object name in that order */

 tSNMP_MIBObjectDescrType  stdbgp4_FMAS_MIBObjectTable[] = {
{
 SNMP_OGP_INDEX_BGP,
 READ_ONLY,
 BGPVERSION
},
{
 SNMP_OGP_INDEX_BGP,
 READ_ONLY,
 BGPLOCALAS
},
{
 SNMP_OGP_INDEX_BGP,
 READ_ONLY,
 BGPIDENTIFIER
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_ONLY,
 BGPPEERIDENTIFIER
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_ONLY,
 BGPPEERSTATE
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_WRITE,
 BGPPEERADMINSTATUS
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_ONLY,
 BGPPEERNEGOTIATEDVERSION
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_ONLY,
 BGPPEERLOCALADDR
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_ONLY,
 BGPPEERLOCALPORT
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 NO_ACCESS,
 BGPPEERREMOTEADDR
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_ONLY,
 BGPPEERREMOTEPORT
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_ONLY,
 BGPPEERREMOTEAS
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_ONLY,
 BGPPEERINUPDATES
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_ONLY,
 BGPPEEROUTUPDATES
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_ONLY,
 BGPPEERINTOTALMESSAGES
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_ONLY,
 BGPPEEROUTTOTALMESSAGES
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_ONLY,
 BGPPEERLASTERROR
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_ONLY,
 BGPPEERFSMESTABLISHEDTRANSITIONS
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_ONLY,
 BGPPEERFSMESTABLISHEDTIME
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_WRITE,
 BGPPEERCONNECTRETRYINTERVAL
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_ONLY,
 BGPPEERHOLDTIME
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_ONLY,
 BGPPEERKEEPALIVE
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_WRITE,
 BGPPEERHOLDTIMECONFIGURED
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_WRITE,
 BGPPEERKEEPALIVECONFIGURED
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_WRITE,
 BGPPEERMINASORIGINATIONINTERVAL
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_WRITE,
 BGPPEERMINROUTEADVERTISEMENTINTERVAL
},
{
 SNMP_OGP_INDEX_BGPPEERTABLE,
 READ_ONLY,
 BGPPEERINUPDATEELAPSEDTIME
},
{
 SNMP_OGP_INDEX_BGP4PATHATTRTABLE,
 NO_ACCESS,
 BGP4PATHATTRPEER
},
{
 SNMP_OGP_INDEX_BGP4PATHATTRTABLE,
 NO_ACCESS,
 BGP4PATHATTRIPADDRPREFIXLEN
},
{
 SNMP_OGP_INDEX_BGP4PATHATTRTABLE,
 NO_ACCESS,
 BGP4PATHATTRIPADDRPREFIX
},
{
 SNMP_OGP_INDEX_BGP4PATHATTRTABLE,
 READ_ONLY,
 BGP4PATHATTRORIGIN
},
{
 SNMP_OGP_INDEX_BGP4PATHATTRTABLE,
 READ_ONLY,
 BGP4PATHATTRASPATHSEGMENT
},
{
 SNMP_OGP_INDEX_BGP4PATHATTRTABLE,
 READ_ONLY,
 BGP4PATHATTRNEXTHOP
},
{
 SNMP_OGP_INDEX_BGP4PATHATTRTABLE,
 READ_ONLY,
 BGP4PATHATTRMULTIEXITDISC
},
{
 SNMP_OGP_INDEX_BGP4PATHATTRTABLE,
 READ_ONLY,
 BGP4PATHATTRLOCALPREF
},
{
 SNMP_OGP_INDEX_BGP4PATHATTRTABLE,
 READ_ONLY,
 BGP4PATHATTRATOMICAGGREGATE
},
{
 SNMP_OGP_INDEX_BGP4PATHATTRTABLE,
 READ_ONLY,
 BGP4PATHATTRAGGREGATORAS
},
{
 SNMP_OGP_INDEX_BGP4PATHATTRTABLE,
 READ_ONLY,
 BGP4PATHATTRAGGREGATORADDR
},
{
 SNMP_OGP_INDEX_BGP4PATHATTRTABLE,
 READ_ONLY,
 BGP4PATHATTRCALCLOCALPREF
},
{
 SNMP_OGP_INDEX_BGP4PATHATTRTABLE,
 READ_ONLY,
 BGP4PATHATTRBEST
},
{
 SNMP_OGP_INDEX_BGP4PATHATTRTABLE,
 READ_ONLY,
 BGP4PATHATTRUNKNOWN
}
};

 tSNMP_GLOBAL_STRUCT stdbgp4_FMAS_Global_data =
{sizeof (stdbgp4_FMAS_GroupOIDTable) / sizeof (tSNMP_GroupOIDType),
 sizeof (stdbgp4_FMAS_BaseOIDTable) / sizeof (tSNMP_BaseOIDType)};

 int stdbgp4_MAX_OBJECTS =
(sizeof (stdbgp4_FMAS_MIBObjectTable) / sizeof (tSNMP_MIBObjectDescrType));

#endif  /* _STDBGMDB_H */
