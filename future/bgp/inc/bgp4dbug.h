/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved    
 *
 * $Id: bgp4dbug.h,v 1.9 2017/09/15 06:19:50 siva Exp $
 *
 * Description: This file contains the macro defns  
 *
 *******************************************************************/
#ifndef  BGP4DBUG_H

#define  BGP4DBUG_H


/* Module names */
#define  BGP4_MOD_NAME  "BGP"

/* Trace and debug flags */
#define  BGP4_DBG_FLAG  (gBgpNode.Bgp4DbgTrc.u4Bgp4Dbg)
#define  BGP4_TRC_FLAG  (gBgpNode.Bgp4DbgTrc.u4Bgp4Trc)

/* Trace definitions */
#define  BGP4_PKT_DUMP  MOD_PKT_DUMP

#define  BGP4_TRC       Bgp4GlobalTrace 
#define  BGP4_TRC_ARG1  Bgp4GlobalTrace
#define  BGP4_TRC_ARG2  Bgp4GlobalTrace
#define  BGP4_TRC_ARG3  Bgp4GlobalTrace 
#define  BGP4_TRC_ARG4  Bgp4GlobalTrace 
#define  BGP4_TRC_ARG5  Bgp4GlobalTrace 
#define  BGP4_TRC_ARG6  Bgp4GlobalTrace 

/* Failure Trace Definitions. */
#define  BGP4_ALL_FAILURE_TRC     0x00000001 /* For all Failures. */

/* OS Resource Related Trace Definition. */
#define  BGP4_OS_RESOURCE_TRC     0x00000010 /* For all OS resources
                                              * alloc/release failure info */
#define  BGP4_BUFFER_TRC          0x00000020 

/* Init/Shutdown Trace Definitions */
#define  BGP4_INIT_SHUT_TRC       0x00000100 

/* Configuration/Management Trace Definitions. */
#define  BGP4_MGMT_TRC            0x00000200

/* BGP Specific Trace Definitions. */
#define  BGP4_CONTROL_PATH_TRC    0x00001000 /* For BGP protocols operation */
#define  BGP4_DATA_PATH_TRC       0x00002000 /* For Routes updated to BGP RIB*/
#define  BGP4_PEER_CON_TRC        0x00004000 /* For Peer Connection/State 
                                              * Changes. */
#define  BGP4_UPD_MSG_TRC         0x00008000 /* For exchange of BGP update
                                              * packets. */
#define  BGP4_FDB_TRC             0x00010000 /* For FDB update/reception 
                                              * information. */
#define  BGP4_KEEP_TRC            0x00020000 /* Keep-Alive packet Info. */
#define  BGP4_TXQ_TRC             0x00040000 /* Info about all TxQs. */
#define  BGP4_RX_TRC              0x00080000 /* Info about all Rxs. */
#define  BGP4_DAMP_TRC            0x00100000 /* For Route Damp Info. */
#define  BGP4_EVENTS_TRC          0x00200000 /* For BGP Event like Timeout,
                                              * Configuration Message Recv,
                                              * NextHop processing,
                                              * Synchronisation processing. */
#define  BGP4_GR_TRC              0x00400000 /* Trace for Graceful restart
                                              * related events */
#define  BGP4_DETAIL_TRC          0x00800000 /* Trace for development
                                              * debug */
#ifdef VPLSADS_WANTED
#define  BGP4_VPLS_TRC            0x01000000 /*L2VPN-VPLS related traces*/
#define  BGP4_VPLS_FUN_ENTRY      0x02000000 /*L2VPN-VPLS function Entry-Exit related traces*/
#define  BGP4_VPLS_FUN_EXIT       0x04000000
#define  BGP4_VPLS_INFO_TRC       0x08000000 /*L2VPN-VPLS Info traces*/
#define  BGP4_VPLS_CRI_TRC        0x10000000 /*L2VPN-VPLS Critical traces*/
#endif

#ifdef EVPN_WANTED
#define  BGP4_EVPN_TRC            0x80000000 /* EVPN Trace */
#endif
/* Packet Dump Level */
#define  BGP4_DUMP_HGH_TRC        0x10000000  /* Packet HighLevel dump */
#define  BGP4_DUMP_LOW_TRC        0x20000000  /* Packet LowLevel dump */
#define  BGP4_DUMP_HEX_TRC        0x40000000  /* Packet Hex dump */

/* Packet Dump Trace */
#define BGP4_DUMP_TRC \
            BGP4_DUMP_HGH_TRC | BGP4_DUMP_LOW_TRC
 
#define BGP4_DBG(Value, Fmt)           UtlTrcLog(BGP4_DBG_FLAG, \
                                                 Value,         \
                                                 "BGP4",        \
                                                 Fmt)

#define BGP4_DBG1(Value, Fmt, Arg)     UtlTrcLog(BGP4_DBG_FLAG, \
                                                 Value,         \
                                                 "BGP4",        \
                                                 Fmt,           \
                                                 Arg)

#define BGP4_DBG2(Value, Fmt, Arg1, Arg2)                      \
                                      UtlTrcLog(BGP4_DBG_FLAG, \
                                                Value,         \
                                                "BGP4",        \
                                                Fmt,           \
                                                Arg1, Arg2)

#define BGP4_DBG3(Value, Fmt, Arg1, Arg2, Arg3)                \
                                      UtlTrcLog(BGP4_DBG_FLAG, \
                                                Value,         \
                                                "BGP4",        \
                                                Fmt,           \
                                                Arg1, Arg2, Arg3)

#define BGP4_DBG4(Value, Fmt, Arg1, Arg2, Arg3, Arg4)          \
                                      UtlTrcLog(BGP4_DBG_FLAG, \
                                                Value,         \
                                                "BGP4",        \
                                                Fmt,           \
                                                Arg1, Arg2,    \
                                                Arg3, Arg4)

#define BGP4_DBG5(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)    \
                                      UtlTrcLog(BGP4_DBG_FLAG, \
                                                Value,         \
                                                "BGP4",        \
                                                Fmt,           \
                                                Arg1, Arg2, Arg3, \
                                                Arg4, Arg5)

#define BGP4_DBG6(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
                                      UtlTrcLog(BGP4_DBG_FLAG, \
                                                Value,         \
                                                "BGP4",        \
                                                Fmt,           \
                                                Arg1, Arg2, Arg3, \
                                                Arg4, Arg5, Arg6)

/* Debug categories */
#define  BGP4_DBG_OFF          UTL_DBG_OFF
#define  BGP4_DBG_ALL          UTL_DBG_ALL
#define  BGP4_DBG_INIT_SHUTDN  UTL_DBG_INIT_SHUTDN
#define  BGP4_DBG_CTRL_IF      UTL_DBG_CTRL_IF
#define  BGP4_DBG_STATUS_IF    UTL_DBG_STATUS_IF
#define  BGP4_DBG_SNMP_IF      UTL_DBG_SNMP_IF
#define  BGP4_DBG_BUF_IF       UTL_DBG_BUF_IF
#define  BGP4_DBG_MEM_IF       UTL_DBG_MEM_IF
#define  BGP4_DBG_RX           UTL_DBG_RX
#define  BGP4_DBG_TX           UTL_DBG_TX
#define  BGP4_DBG_MGMT         BGP4_DBG_SNMP_IF
#define  BGP4_DBG_TMR_IF       UTL_DBG_TMR_IF

#define  BGP4_DBG_ENTRY        0x00010000
#define  BGP4_DBG_EXIT         0x00020000
#define  BGP4_DBG_RIB_DUMP     0x00080000
#define  BGP4_DBG_CTRL_FLOW    0x00100000
#define  BGP4_DBG_IF           0x00200000
#define  BGP4_DBG_ERROR        0x04000000
#define  BGP4_DBG_MEM          0x08000000
#define  BGP4_DBG_FAILURE      0x10000000
#define  BGP4_ALL_DEBUG        0xffffffff
#define  BGP4_ALL_TRC          0xffffffff
#define  BGP4_TRACE_MAX_LEN    300

#endif /* BGP4DBUG_H */
