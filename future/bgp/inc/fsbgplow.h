/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsbgplow.h,v 1.30 2016/12/27 12:35:53 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/
#ifndef FSBGPLOW_H

#define FSBGPLOW_H

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4GlobalAdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4LocalAs ARG_LIST((UINT4 *));

INT1
nmhGetFsbgp4Identifier ARG_LIST((UINT4 *));

INT1
nmhGetFsbgp4Synchronization ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4DefaultLocalPref ARG_LIST((UINT4 *));

INT1
nmhGetFsbgp4AdvtNonBgpRt ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4TraceEnable ARG_LIST((UINT4 *));

INT1
nmhGetFsbgp4DebugEnable ARG_LIST((UINT4 *));

INT1
nmhGetFsbgp4OverlappingRoute ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4MaxPeerEntry ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4MaxNoofRoutes ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4AlwaysCompareMED ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4DefaultOriginate ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4DefaultIpv4UniCast ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4GRAdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4GRRestartTimeInterval ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4GRSelectionDeferralTimeInterval ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4GRStaleTimeInterval ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4GRMode ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4RestartSupport ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4RestartStatus ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4RestartExitReason ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4RestartReason ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4ForwardingPreservation ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4IsTrapEnabled ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4NextHopProcessingInterval ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4IBGPRedistributionStatus ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4IBGPMaxPaths ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4EBGPMaxPaths ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4EIBGPMaxPaths ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4OperIBGPMaxPaths ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4OperEBGPMaxPaths ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4OperEIBGPMaxPaths ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4FourByteASNSupportStatus ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4FourByteASNotationType ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4VpnLabelAllocPolicy ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4MacMobDuplicationTimeInterval ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4MaxMacMoves ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4VpnRouteLeakStatus ARG_LIST((INT4 *));                                                      

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4GlobalAdminStatus ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4LocalAs ARG_LIST((UINT4 ));

INT1
nmhSetFsbgp4Identifier ARG_LIST((UINT4 ));

INT1
nmhSetFsbgp4Synchronization ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4DefaultLocalPref ARG_LIST((UINT4 ));

INT1
nmhSetFsbgp4AdvtNonBgpRt ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4TraceEnable ARG_LIST((UINT4 ));

INT1
nmhSetFsbgp4DebugEnable ARG_LIST((UINT4 ));

INT1
nmhSetFsbgp4OverlappingRoute ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4MaxPeerEntry ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4MaxNoofRoutes ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4AlwaysCompareMED ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4DefaultOriginate ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4DefaultIpv4UniCast ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4GRAdminStatus ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4GRRestartTimeInterval ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4GRSelectionDeferralTimeInterval ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4GRStaleTimeInterval ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4RestartSupport ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4RestartReason ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4IsTrapEnabled ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4NextHopProcessingInterval ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4IBGPRedistributionStatus ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4IBGPMaxPaths ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4EBGPMaxPaths ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4EIBGPMaxPaths ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4FourByteASNSupportStatus ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4FourByteASNotationType ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4VpnLabelAllocPolicy ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4MacMobDuplicationTimeInterval ARG_LIST((INT4 ));

INT1 
nmhSetFsbgp4MaxMacMoves ARG_LIST((INT4 ));

INT1                                                                                                    nmhSetFsbgp4VpnRouteLeakStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4GlobalAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4LocalAs ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2Fsbgp4Identifier ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2Fsbgp4Synchronization ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4DefaultLocalPref ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2Fsbgp4AdvtNonBgpRt ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4TraceEnable ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2Fsbgp4DebugEnable ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2Fsbgp4OverlappingRoute ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4MaxPeerEntry ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4MaxNoofRoutes ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4AlwaysCompareMED ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4DefaultOriginate ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4DefaultIpv4UniCast ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4GRAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4GRRestartTimeInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4GRSelectionDeferralTimeInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4GRStaleTimeInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4RestartSupport ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4RestartReason ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4IsTrapEnabled ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4NextHopProcessingInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4IBGPRedistributionStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4IBGPMaxPaths ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4EBGPMaxPaths ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4EIBGPMaxPaths ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4FourByteASNSupportStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4FourByteASNotationType ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4VpnLabelAllocPolicy ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4MacMobDuplicationTimeInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1 
nmhTestv2Fsbgp4MaxMacMoves ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4VpnRouteLeakStatus ARG_LIST((UINT4 *  ,INT4 ));                                          


/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4GlobalAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4LocalAs ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4Identifier ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4Synchronization ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4DefaultLocalPref ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4AdvtNonBgpRt ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4TraceEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4DebugEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4OverlappingRoute ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4MaxPeerEntry ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4MaxNoofRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4AlwaysCompareMED ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4DefaultOriginate ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4DefaultIpv4UniCast ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4GRAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4GRRestartTimeInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4GRSelectionDeferralTimeInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4GRStaleTimeInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4RestartSupport ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4RestartReason ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4IsTrapEnabled ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4NextHopProcessingInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4IBGPRedistributionStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4IBGPMaxPaths ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4EBGPMaxPaths ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4EIBGPMaxPaths ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4FourByteASNSupportStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4FourByteASNotationType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4VpnLabelAllocPolicy ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4MacMobDuplicationTimeInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1 
nmhDepv2Fsbgp4MaxMacMoves ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1                                                                                                    nmhDepv2Fsbgp4VpnRouteLeakStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));      

/* Proto Validate Index Instance for Fsbgp4PeerExtTable. */
INT1
nmhValidateIndexInstanceFsbgp4PeerExtTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4PeerExtTable  */

INT1
nmhGetFirstIndexFsbgp4PeerExtTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4PeerExtTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4PeerExtConfigurePeer ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsbgp4PeerExtPeerRemoteAs ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsbgp4PeerExtEBGPMultiHop ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsbgp4PeerExtNextHopSelf ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsbgp4PeerExtConnSrcIfId ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsbgp4PeerExtRflClient ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4PeerExtConfigurePeer ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsbgp4PeerExtPeerRemoteAs ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsbgp4PeerExtEBGPMultiHop ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsbgp4PeerExtNextHopSelf ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsbgp4PeerExtRflClient ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4PeerExtConfigurePeer ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4PeerExtPeerRemoteAs ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Fsbgp4PeerExtEBGPMultiHop ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4PeerExtNextHopSelf ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4PeerExtRflClient ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4PeerExtTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MEDTable. */
INT1
nmhValidateIndexInstanceFsbgp4MEDTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MEDTable  */

INT1
nmhGetFirstIndexFsbgp4MEDTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MEDTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4MEDAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4MEDRemoteAS ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsbgp4MEDIPAddrPrefix ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsbgp4MEDIPAddrPrefixLen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4MEDIntermediateAS ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4MEDDirection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4MEDValue ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsbgp4MEDPreference ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4MEDAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4MEDRemoteAS ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsbgp4MEDIPAddrPrefix ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsbgp4MEDIPAddrPrefixLen ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4MEDIntermediateAS ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4MEDDirection ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4MEDValue ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsbgp4MEDPreference ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4MEDAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4MEDRemoteAS ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsbgp4MEDIPAddrPrefix ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsbgp4MEDIPAddrPrefixLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4MEDIntermediateAS ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4MEDDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4MEDValue ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsbgp4MEDPreference ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4MEDTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4LocalPrefTable. */
INT1
nmhValidateIndexInstanceFsbgp4LocalPrefTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4LocalPrefTable  */

INT1
nmhGetFirstIndexFsbgp4LocalPrefTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4LocalPrefTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4LocalPrefAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4LocalPrefRemoteAS ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsbgp4LocalPrefIPAddrPrefix ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsbgp4LocalPrefIPAddrPrefixLen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4LocalPrefIntermediateAS ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4LocalPrefDirection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4LocalPrefValue ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsbgp4LocalPrefPreference ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4LocalPrefAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4LocalPrefRemoteAS ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsbgp4LocalPrefIPAddrPrefix ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsbgp4LocalPrefIPAddrPrefixLen ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4LocalPrefIntermediateAS ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4LocalPrefDirection ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4LocalPrefValue ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsbgp4LocalPrefPreference ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4LocalPrefAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4LocalPrefRemoteAS ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsbgp4LocalPrefIPAddrPrefix ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsbgp4LocalPrefIPAddrPrefixLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4LocalPrefIntermediateAS ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4LocalPrefDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4LocalPrefValue ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsbgp4LocalPrefPreference ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4LocalPrefTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4UpdateFilterTable. */
INT1
nmhValidateIndexInstanceFsbgp4UpdateFilterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4UpdateFilterTable  */

INT1
nmhGetFirstIndexFsbgp4UpdateFilterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4UpdateFilterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4UpdateFilterAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4UpdateFilterRemoteAS ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsbgp4UpdateFilterIPAddrPrefix ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsbgp4UpdateFilterIPAddrPrefixLen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4UpdateFilterIntermediateAS ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4UpdateFilterDirection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4UpdateFilterAction ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4UpdateFilterAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4UpdateFilterRemoteAS ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsbgp4UpdateFilterIPAddrPrefix ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsbgp4UpdateFilterIPAddrPrefixLen ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4UpdateFilterIntermediateAS ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4UpdateFilterDirection ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4UpdateFilterAction ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4UpdateFilterAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4UpdateFilterRemoteAS ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsbgp4UpdateFilterIPAddrPrefix ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsbgp4UpdateFilterIPAddrPrefixLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4UpdateFilterIntermediateAS ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4UpdateFilterDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4UpdateFilterAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4UpdateFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4AggregateTable. */
INT1
nmhValidateIndexInstanceFsbgp4AggregateTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4AggregateTable  */

INT1
nmhGetFirstIndexFsbgp4AggregateTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4AggregateTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4AggregateAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4AggregateIPAddrPrefix ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsbgp4AggregateIPAddrPrefixLen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4AggregateAdvertise ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4AggregateAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4AggregateIPAddrPrefix ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsbgp4AggregateIPAddrPrefixLen ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4AggregateAdvertise ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4AggregateAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4AggregateIPAddrPrefix ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsbgp4AggregateIPAddrPrefixLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4AggregateAdvertise ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4AggregateTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4RRDAdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4RRDProtoMaskForEnable ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4RRDSrcProtoMaskForDisable ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4RRDDefaultMetric ARG_LIST((UINT4 *));

INT1
nmhGetFsbgp4RRDRouteMapName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4RRDMatchTypeEnable ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4RRDMatchTypeDisable ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4RRDAdminStatus ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4RRDProtoMaskForEnable ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4RRDSrcProtoMaskForDisable ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4RRDDefaultMetric ARG_LIST((UINT4 ));

INT1
nmhSetFsbgp4RRDRouteMapName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4RRDMatchTypeEnable ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4RRDMatchTypeDisable ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4RRDAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4RRDProtoMaskForEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4RRDSrcProtoMaskForDisable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4RRDDefaultMetric ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2Fsbgp4RRDRouteMapName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4RRDMatchTypeEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4RRDMatchTypeDisable ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4RRDAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4RRDProtoMaskForEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4RRDSrcProtoMaskForDisable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4RRDDefaultMetric ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4RRDRouteMapName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4RRDMatchTypeEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4RRDMatchTypeDisable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4RRDMetricTable. */
INT1
nmhValidateIndexInstanceFsbgp4RRDMetricTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4RRDMetricTable  */

INT1
nmhGetFirstIndexFsbgp4RRDMetricTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4RRDMetricTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBgp4RRDMetricValue ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBgp4RRDMetricValue ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBgp4RRDMetricValue ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4RRDMetricTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4ImportRouteTable. */
INT1
nmhValidateIndexInstanceFsbgp4ImportRouteTable ARG_LIST((UINT4  , INT4  , INT4  , UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4ImportRouteTable  */

INT1
nmhGetFirstIndexFsbgp4ImportRouteTable ARG_LIST((UINT4 * , INT4 * , INT4 * , UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4ImportRouteTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4ImportRouteAction ARG_LIST((UINT4  , INT4  , INT4  , UINT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4ImportRouteAction ARG_LIST((UINT4  , INT4  , INT4  , UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4ImportRouteAction ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  , UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4ImportRouteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4FsmTransitionHistTable. */
INT1
nmhValidateIndexInstanceFsbgp4FsmTransitionHistTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4FsmTransitionHistTable  */

INT1
nmhGetFirstIndexFsbgp4FsmTransitionHistTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4FsmTransitionHistTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4FsmTransitionHist ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4RflbgpClusterId ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4RflRflSupport ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4RflbgpClusterId ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4RflRflSupport ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4RflbgpClusterId ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4RflRflSupport ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4RflbgpClusterId ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4RflRflSupport ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4RflRouteReflectorTable. */
INT1
nmhValidateIndexInstanceFsbgp4RflRouteReflectorTable ARG_LIST((UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4RflRouteReflectorTable  */

INT1
nmhGetFirstIndexFsbgp4RflRouteReflectorTable ARG_LIST((UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4RflRouteReflectorTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4RflPathAttrOriginatorId ARG_LIST((UINT4  , INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4RflPathAttrClusterList ARG_LIST((UINT4  , INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4RfdCutOff ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4RfdReuse ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4RfdCeiling ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4RfdMaxHoldDownTime ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4RfdDecayHalfLifeTime ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4RfdDecayTimerGranularity ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4RfdReuseTimerGranularity ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4RfdReuseIndxArraySize ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4RfdAdminStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4RfdCutOff ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4RfdReuse ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4RfdMaxHoldDownTime ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4RfdDecayHalfLifeTime ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4RfdDecayTimerGranularity ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4RfdReuseTimerGranularity ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4RfdReuseIndxArraySize ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4RfdAdminStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4RfdCutOff ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4RfdReuse ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4RfdMaxHoldDownTime ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4RfdDecayHalfLifeTime ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4RfdDecayTimerGranularity ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4RfdReuseTimerGranularity ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4RfdReuseIndxArraySize ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4RfdAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4RfdCutOff ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4RfdReuse ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4RfdMaxHoldDownTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4RfdDecayHalfLifeTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4RfdDecayTimerGranularity ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4RfdReuseTimerGranularity ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4RfdReuseIndxArraySize ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4RfdAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4RfdRtDampHistTable. */
INT1
nmhValidateIndexInstanceFsbgp4RfdRtDampHistTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4RfdRtDampHistTable  */

INT1
nmhGetFirstIndexFsbgp4RfdRtDampHistTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4RfdRtDampHistTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4RfdRtFom ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsbgp4RfdRtLastUpdtTime ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsbgp4RfdRtState ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsbgp4RfdRtStatus ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for Fsbgp4RfdPeerDampHistTable. */
INT1
nmhValidateIndexInstanceFsbgp4RfdPeerDampHistTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4RfdPeerDampHistTable  */

INT1
nmhGetFirstIndexFsbgp4RfdPeerDampHistTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4RfdPeerDampHistTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4RfdPeerFom ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsbgp4RfdPeerLastUpdtTime ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsbgp4RfdPeerState ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsbgp4RfdPeerStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto Validate Index Instance for Fsbgp4RfdRtsReuseListTable. */
INT1
nmhValidateIndexInstanceFsbgp4RfdRtsReuseListTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4RfdRtsReuseListTable  */

INT1
nmhGetFirstIndexFsbgp4RfdRtsReuseListTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4RfdRtsReuseListTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4RfdRtReuseListRtFom ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsbgp4RfdRtReuseListRtLastUpdtTime ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsbgp4RfdRtReuseListRtState ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsbgp4RfdRtReuseListRtStatus ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for Fsbgp4RfdPeerReuseListTable. */
INT1
nmhValidateIndexInstanceFsbgp4RfdPeerReuseListTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4RfdPeerReuseListTable  */

INT1
nmhGetFirstIndexFsbgp4RfdPeerReuseListTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4RfdPeerReuseListTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4RfdPeerReuseListPeerFom ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsbgp4RfdPeerReuseListLastUpdtTime ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsbgp4RfdPeerReuseListPeerState ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsbgp4RfdPeerReuseListPeerStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4CommMaxInFTblEntries ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4CommMaxOutFTblEntries ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4CommMaxInFTblEntries ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4CommMaxOutFTblEntries ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4CommMaxInFTblEntries ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4CommMaxOutFTblEntries ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4CommMaxInFTblEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4CommMaxOutFTblEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4CommRouteAddCommTable. */
INT1
nmhValidateIndexInstanceFsbgp4CommRouteAddCommTable ARG_LIST((UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4CommRouteAddCommTable  */

INT1
nmhGetFirstIndexFsbgp4CommRouteAddCommTable ARG_LIST((UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4CommRouteAddCommTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4AddCommRowStatus ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4AddCommRowStatus ARG_LIST((UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4AddCommRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4CommRouteAddCommTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4CommRouteDeleteCommTable. */
INT1
nmhValidateIndexInstanceFsbgp4CommRouteDeleteCommTable ARG_LIST((UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4CommRouteDeleteCommTable  */

INT1
nmhGetFirstIndexFsbgp4CommRouteDeleteCommTable ARG_LIST((UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4CommRouteDeleteCommTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4DeleteCommRowStatus ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4DeleteCommRowStatus ARG_LIST((UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4DeleteCommRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4CommRouteDeleteCommTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4CommRouteCommSetStatusTable. */
INT1
nmhValidateIndexInstanceFsbgp4CommRouteCommSetStatusTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4CommRouteCommSetStatusTable  */

INT1
nmhGetFirstIndexFsbgp4CommRouteCommSetStatusTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4CommRouteCommSetStatusTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4CommSetStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsbgp4CommSetStatusRowStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4CommSetStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsbgp4CommSetStatusRowStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4CommSetStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4CommSetStatusRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4CommRouteCommSetStatusTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4CommPeerSendStatusTable. */
INT1
nmhValidateIndexInstanceFsbgp4CommPeerSendStatusTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4CommPeerSendStatusTable  */

INT1
nmhGetFirstIndexFsbgp4CommPeerSendStatusTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4CommPeerSendStatusTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4CommSendStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsbgp4CommPeerSendRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4CommSendStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsbgp4CommPeerSendRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4CommSendStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4CommPeerSendRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4CommPeerSendStatusTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4CommInFilterTable. */
INT1
nmhValidateIndexInstanceFsbgp4CommInFilterTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4CommInFilterTable  */

INT1
nmhGetFirstIndexFsbgp4CommInFilterTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4CommInFilterTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4CommIncomingFilterStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsbgp4InFilterRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4CommIncomingFilterStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsbgp4InFilterRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4CommIncomingFilterStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4InFilterRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4CommInFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4CommOutFilterTable. */
INT1
nmhValidateIndexInstanceFsbgp4CommOutFilterTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4CommOutFilterTable  */

INT1
nmhGetFirstIndexFsbgp4CommOutFilterTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4CommOutFilterTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4CommOutgoingFilterStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsbgp4OutFilterRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4CommOutgoingFilterStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsbgp4OutFilterRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4CommOutgoingFilterStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4OutFilterRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4CommOutFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4CommReceivedRouteCommTable. */
INT1
nmhValidateIndexInstanceFsbgp4CommReceivedRouteCommTable ARG_LIST((UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4CommReceivedRouteCommTable  */

INT1
nmhGetFirstIndexFsbgp4CommReceivedRouteCommTable ARG_LIST((UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4CommReceivedRouteCommTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4ReceivedRouteCommPath ARG_LIST((UINT4  , INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4ExtCommMaxInFTblEntries ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4ExtCommMaxOutFTblEntries ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4ExtCommMaxInFTblEntries ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4ExtCommMaxOutFTblEntries ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4ExtCommMaxInFTblEntries ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4ExtCommMaxOutFTblEntries ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4ExtCommMaxInFTblEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4ExtCommMaxOutFTblEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4ExtCommRouteAddExtCommTable. */
INT1
nmhValidateIndexInstanceFsbgp4ExtCommRouteAddExtCommTable ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4ExtCommRouteAddExtCommTable  */

INT1
nmhGetFirstIndexFsbgp4ExtCommRouteAddExtCommTable ARG_LIST((UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4ExtCommRouteAddExtCommTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4AddExtCommRowStatus ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4AddExtCommRowStatus ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4AddExtCommRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4ExtCommRouteAddExtCommTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4ExtCommRouteDeleteExtCommTable. */
INT1
nmhValidateIndexInstanceFsbgp4ExtCommRouteDeleteExtCommTable ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4ExtCommRouteDeleteExtCommTable  */

INT1
nmhGetFirstIndexFsbgp4ExtCommRouteDeleteExtCommTable ARG_LIST((UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4ExtCommRouteDeleteExtCommTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4DeleteExtCommRowStatus ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4DeleteExtCommRowStatus ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4DeleteExtCommRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4ExtCommRouteDeleteExtCommTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4ExtCommRouteExtCommSetStatusTable. */
INT1
nmhValidateIndexInstanceFsbgp4ExtCommRouteExtCommSetStatusTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4ExtCommRouteExtCommSetStatusTable  */

INT1
nmhGetFirstIndexFsbgp4ExtCommRouteExtCommSetStatusTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4ExtCommRouteExtCommSetStatusTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4ExtCommSetStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsbgp4ExtCommSetStatusRowStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4ExtCommSetStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsbgp4ExtCommSetStatusRowStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4ExtCommSetStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4ExtCommSetStatusRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4ExtCommRouteExtCommSetStatusTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4ExtCommPeerSendStatusTable. */
INT1
nmhValidateIndexInstanceFsbgp4ExtCommPeerSendStatusTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4ExtCommPeerSendStatusTable  */

INT1
nmhGetFirstIndexFsbgp4ExtCommPeerSendStatusTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4ExtCommPeerSendStatusTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4ExtCommPeerSendStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsbgp4ExtCommPeerSendStatusRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4ExtCommPeerSendStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsbgp4ExtCommPeerSendStatusRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4ExtCommPeerSendStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4ExtCommPeerSendStatusRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4ExtCommPeerSendStatusTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4ExtCommInFilterTable. */
INT1
nmhValidateIndexInstanceFsbgp4ExtCommInFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4ExtCommInFilterTable  */

INT1
nmhGetFirstIndexFsbgp4ExtCommInFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4ExtCommInFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4ExtCommIncomingFilterStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4ExtCommInFilterRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4ExtCommIncomingFilterStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4ExtCommInFilterRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4ExtCommIncomingFilterStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4ExtCommInFilterRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4ExtCommInFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4ExtCommOutFilterTable. */
INT1
nmhValidateIndexInstanceFsbgp4ExtCommOutFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4ExtCommOutFilterTable  */

INT1
nmhGetFirstIndexFsbgp4ExtCommOutFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4ExtCommOutFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4ExtCommOutgoingFilterStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4ExtCommOutFilterRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4ExtCommOutgoingFilterStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4ExtCommOutFilterRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4ExtCommOutgoingFilterStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4ExtCommOutFilterRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4ExtCommOutFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4PeerLinkBwTable. */
INT1
nmhValidateIndexInstanceFsbgp4PeerLinkBwTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4PeerLinkBwTable  */

INT1
nmhGetFirstIndexFsbgp4PeerLinkBwTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4PeerLinkBwTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4LinkBandWidth ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsbgp4PeerLinkBwRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4LinkBandWidth ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsbgp4PeerLinkBwRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4LinkBandWidth ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Fsbgp4PeerLinkBwRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4PeerLinkBwTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4ExtCommReceivedRouteExtCommTable. */
INT1
nmhValidateIndexInstanceFsbgp4ExtCommReceivedRouteExtCommTable ARG_LIST((UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4ExtCommReceivedRouteExtCommTable  */

INT1
nmhGetFirstIndexFsbgp4ExtCommReceivedRouteExtCommTable ARG_LIST((UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4ExtCommReceivedRouteExtCommTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4ReceivedRouteExtCommPath ARG_LIST((UINT4  , INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4CapabilitySupportAvailable ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4MaxCapsPerPeer ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4MaxInstancesPerCap ARG_LIST((INT4 *));

INT1
nmhGetFsbgp4MaxCapDataSize ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4CapabilitySupportAvailable ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4MaxCapsPerPeer ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4MaxInstancesPerCap ARG_LIST((INT4 ));

INT1
nmhSetFsbgp4MaxCapDataSize ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4CapabilitySupportAvailable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4MaxCapsPerPeer ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4MaxInstancesPerCap ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsbgp4MaxCapDataSize ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4CapabilitySupportAvailable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4MaxCapsPerPeer ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4MaxInstancesPerCap ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4MaxCapDataSize ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4CapSupportedCapsTable. */
INT1
nmhValidateIndexInstanceFsbgp4CapSupportedCapsTable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4CapSupportedCapsTable  */

INT1
nmhGetFirstIndexFsbgp4CapSupportedCapsTable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4CapSupportedCapsTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4SupportedCapabilityLength ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsbgp4SupportedCapabilityValue ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4CapSupportedCapsRowStatus ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4SupportedCapabilityLength ARG_LIST((UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsbgp4SupportedCapabilityValue ARG_LIST((UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4CapSupportedCapsRowStatus ARG_LIST((UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4SupportedCapabilityLength ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4SupportedCapabilityValue ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4CapSupportedCapsRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4CapSupportedCapsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4StrictCapabilityMatchTable. */
INT1
nmhValidateIndexInstanceFsbgp4StrictCapabilityMatchTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4StrictCapabilityMatchTable  */

INT1
nmhGetFirstIndexFsbgp4StrictCapabilityMatchTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4StrictCapabilityMatchTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4StrictCapabilityMatch ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4StrictCapabilityMatch ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4StrictCapabilityMatch ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4StrictCapabilityMatchTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4CapsAnnouncedTable. */
INT1
nmhValidateIndexInstanceFsbgp4CapsAnnouncedTable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4CapsAnnouncedTable  */

INT1
nmhGetFirstIndexFsbgp4CapsAnnouncedTable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4CapsAnnouncedTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4PeerCapAnnouncedLength ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsbgp4PeerCapAnnouncedValue ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for Fsbgp4CapReceivedCapsTable. */
INT1
nmhValidateIndexInstanceFsbgp4CapReceivedCapsTable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4CapReceivedCapsTable  */

INT1
nmhGetFirstIndexFsbgp4CapReceivedCapsTable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4CapReceivedCapsTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4PeerCapReceivedLength ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsbgp4PeerCapReceivedValue ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for Fsbgp4CapAcceptedCapsTable. */
INT1
nmhValidateIndexInstanceFsbgp4CapAcceptedCapsTable ARG_LIST((UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4CapAcceptedCapsTable  */

INT1
nmhGetFirstIndexFsbgp4CapAcceptedCapsTable ARG_LIST((UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4CapAcceptedCapsTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4PeerCapAcceptedLength ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsbgp4PeerCapAcceptedValue ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgpAscConfedId ARG_LIST((UINT4 *));

INT1
nmhGetFsbgpAscConfedBestPathCompareMED ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgpAscConfedId ARG_LIST((UINT4 ));

INT1
nmhSetFsbgpAscConfedBestPathCompareMED ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsbgpAscConfedId ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsbgpAscConfedBestPathCompareMED ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsbgpAscConfedId ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsbgpAscConfedBestPathCompareMED ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsbgpAscConfedPeerTable. */
INT1
nmhValidateIndexInstanceFsbgpAscConfedPeerTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsbgpAscConfedPeerTable  */

INT1
nmhGetFirstIndexFsbgpAscConfedPeerTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgpAscConfedPeerTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgpAscConfedPeerStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgpAscConfedPeerStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsbgpAscConfedPeerStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsbgpAscConfedPeerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4RtRefreshAllPeerInboundRequest ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4RtRefreshAllPeerInboundRequest ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4RtRefreshAllPeerInboundRequest ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4RtRefreshAllPeerInboundRequest ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4RtRefreshInboundTable. */
INT1
nmhValidateIndexInstanceFsbgp4RtRefreshInboundTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4RtRefreshInboundTable  */

INT1
nmhGetFirstIndexFsbgp4RtRefreshInboundTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4RtRefreshInboundTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4RtRefreshInboundRequest ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4RtRefreshInboundRequest ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4RtRefreshInboundRequest ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4RtRefreshInboundTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4RtRefreshStatisticsTable. */
INT1
nmhValidateIndexInstanceFsbgp4RtRefreshStatisticsTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4RtRefreshStatisticsTable  */

INT1
nmhGetFirstIndexFsbgp4RtRefreshStatisticsTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4RtRefreshStatisticsTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4RtRefreshStatisticsRtRefMsgSentCntr ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsbgp4RtRefreshStatisticsRtRefMsgTxErrCntr ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsbgp4RtRefreshStatisticsRtRefMsgRcvdCntr ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsbgp4RtRefreshStatisticsRtRefMsgInvalidCntr ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto Validate Index Instance for Fsbgp4TCPMD5AuthTable. */
INT1
nmhValidateIndexInstanceFsbgp4TCPMD5AuthTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4TCPMD5AuthTable  */

INT1
nmhGetFirstIndexFsbgp4TCPMD5AuthTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4TCPMD5AuthTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4TCPMD5AuthPassword ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4TCPMD5AuthPwdSet ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4TCPMD5AuthPassword ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4TCPMD5AuthPwdSet ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4TCPMD5AuthPassword ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4TCPMD5AuthPwdSet ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4TCPMD5AuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4SoftReconfigAllPeerOutboundRequest ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4SoftReconfigAllPeerOutboundRequest ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4SoftReconfigAllPeerOutboundRequest ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4SoftReconfigAllPeerOutboundRequest ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4SoftReconfigOutboundTable. */
INT1
nmhValidateIndexInstanceFsbgp4SoftReconfigOutboundTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4SoftReconfigOutboundTable  */

INT1
nmhGetFirstIndexFsbgp4SoftReconfigOutboundTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4SoftReconfigOutboundTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4SoftReconfigOutboundRequest ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4SoftReconfigOutboundRequest ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4SoftReconfigOutboundRequest ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4SoftReconfigOutboundTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MpeBgpPeerTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeBgpPeerTable  */

INT1
nmhGetFirstIndexFsbgp4MpeBgpPeerTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeBgpPeerTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpebgpPeerIdentifier ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpebgpPeerState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgpPeerAdminStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgpPeerNegotiatedVersion ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgpPeerLocalAddr ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpebgpPeerLocalPort ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgpPeerRemotePort ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgpPeerRemoteAs ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsbgp4mpebgpPeerInUpdates ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsbgp4mpebgpPeerOutUpdates ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsbgp4mpebgpPeerInTotalMessages ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsbgp4mpebgpPeerOutTotalMessages ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsbgp4mpebgpPeerLastError ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpebgpPeerFsmEstablishedTransitions ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsbgp4mpebgpPeerFsmEstablishedTime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsbgp4mpebgpPeerConnectRetryInterval ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgpPeerHoldTime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgpPeerKeepAlive ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgpPeerHoldTimeConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgpPeerKeepAliveConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgpPeerMinASOriginationInterval ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgpPeerMinRouteAdvertisementInterval ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgpPeerInUpdateElapsedTime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsbgp4mpebgpPeerEndOfRIBMarkerSentStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgpPeerEndOfRIBMarkerReceivedStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgpPeerRestartMode ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerRestartTimeInterval ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerAllowAutomaticStart ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerAllowAutomaticStop ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgpPeerIdleHoldTimeConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeDampPeerOscillations ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerDelayOpen ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgpPeerDelayOpenTimeConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerPrefixUpperLimit ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerTcpConnectRetryCnt ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerTcpCurrentConnectRetryCnt ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeIsPeerDamped ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerSessionAuthStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerTCPAOKeyIdInUse ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
INT1
nmhGetFsbgp4mpePeerTCPAOAuthNoMKTDiscard ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
INT1
nmhGetFsbgp4mpePeerTCPAOAuthICMPAccept ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
INT1
nmhGetFsbgp4mpePeerIpPrefixNameIn ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpePeerIpPrefixNameOut ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpePeerBfdStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4mpebgpPeerAdminStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpebgpPeerConnectRetryInterval ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpebgpPeerHoldTimeConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpebgpPeerKeepAliveConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpebgpPeerMinASOriginationInterval ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpebgpPeerMinRouteAdvertisementInterval ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpePeerAllowAutomaticStart ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpePeerAllowAutomaticStop ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpebgpPeerIdleHoldTimeConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpeDampPeerOscillations ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpePeerDelayOpen ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpebgpPeerDelayOpenTimeConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpePeerPrefixUpperLimit ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpePeerTcpConnectRetryCnt ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
INT1
nmhSetFsbgp4mpePeerTCPAOAuthNoMKTDiscard ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
INT1
nmhSetFsbgp4mpePeerTCPAOAuthICMPAccept ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
INT1
nmhSetFsbgp4mpePeerIpPrefixNameIn ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4mpePeerIpPrefixNameOut ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4mpePeerBfdStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4mpebgpPeerAdminStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpebgpPeerConnectRetryInterval ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpebgpPeerHoldTimeConfigured ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpebgpPeerKeepAliveConfigured ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpebgpPeerMinASOriginationInterval ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpebgpPeerMinRouteAdvertisementInterval ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpePeerAllowAutomaticStart ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpePeerAllowAutomaticStop ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpebgpPeerIdleHoldTimeConfigured ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeDampPeerOscillations ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpePeerDelayOpen ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpebgpPeerDelayOpenTimeConfigured ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpePeerPrefixUpperLimit ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpePeerTcpConnectRetryCnt ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
INT1
nmhTestv2Fsbgp4mpePeerTCPAOAuthNoMKTDiscard ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
INT1
nmhTestv2Fsbgp4mpePeerTCPAOAuthICMPAccept ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
INT1
nmhTestv2Fsbgp4mpePeerIpPrefixNameIn ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4mpePeerIpPrefixNameOut ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4mpePeerBfdStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4MpeBgpPeerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MpeBgp4PathAttrTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeBgp4PathAttrTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeBgp4PathAttrTable  */

INT1
nmhGetFirstIndexFsbgp4MpeBgp4PathAttrTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeBgp4PathAttrTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpebgp4PathAttrOrigin ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgp4PathAttrASPathSegment ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpebgp4PathAttrNextHop ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpebgp4PathAttrMultiExitDisc ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgp4PathAttrLocalPref ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgp4PathAttrAtomicAggregate ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgp4PathAttrAggregatorAS ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsbgp4mpebgp4PathAttrAggregatorAddr ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsbgp4mpebgp4PathAttrCalcLocalPref ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgp4PathAttrBest ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpebgp4PathAttrCommunity ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpebgp4PathAttrOriginatorId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpebgp4PathAttrClusterList ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpebgp4PathAttrExtCommunity ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpebgp4PathAttrUnknown ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpebgp4PathAttrLabel ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpebgp4PathAttrAS4PathSegment ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpebgp4PathAttrAggregatorAS4 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto Validate Index Instance for Fsbgp4MpePeerExtTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpePeerExtTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpePeerExtTable  */

INT1
nmhGetFirstIndexFsbgp4MpePeerExtTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpePeerExtTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpePeerExtConfigurePeer ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerExtPeerRemoteAs ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsbgp4mpePeerExtEBGPMultiHop ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerExtEBGPHopLimit ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerExtNextHopSelf ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerExtRflClient ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerExtTcpSendBufSize ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerExtTcpRcvBufSize ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerExtLclAddress ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpePeerExtNetworkAddress ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpePeerExtGateway ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpePeerExtCommSendStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerExtECommSendStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerExtPassive ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerExtDefaultOriginate ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerExtActivateMPCapability ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerExtDeactivateMPCapability ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerExtMplsVpnVrfAssociated ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpePeerExtMplsVpnCERouteTargetAdvt ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpePeerExtMplsVpnCESiteOfOrigin ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpePeerExtOverrideCapability ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4mpePeerExtConfigurePeer ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpePeerExtPeerRemoteAs ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsbgp4mpePeerExtEBGPMultiHop ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpePeerExtEBGPHopLimit ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpePeerExtNextHopSelf ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpePeerExtRflClient ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpePeerExtTcpSendBufSize ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpePeerExtTcpRcvBufSize ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpePeerExtLclAddress ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4mpePeerExtNetworkAddress ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4mpePeerExtGateway ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4mpePeerExtCommSendStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpePeerExtECommSendStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpePeerExtPassive ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpePeerExtDefaultOriginate ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpePeerExtActivateMPCapability ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpePeerExtDeactivateMPCapability ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpePeerExtMplsVpnVrfAssociated ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4mpePeerExtMplsVpnCERouteTargetAdvt ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsbgp4mpePeerExtMplsVpnCESiteOfOrigin ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4mpePeerExtOverrideCapability ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4mpePeerExtConfigurePeer ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpePeerExtPeerRemoteAs ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2Fsbgp4mpePeerExtEBGPMultiHop ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpePeerExtEBGPHopLimit ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpePeerExtNextHopSelf ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpePeerExtRflClient ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpePeerExtTcpSendBufSize ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpePeerExtTcpRcvBufSize ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpePeerExtLclAddress ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4mpePeerExtNetworkAddress ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4mpePeerExtGateway ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4mpePeerExtCommSendStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpePeerExtECommSendStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpePeerExtPassive ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpePeerExtDefaultOriginate ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpePeerExtActivateMPCapability ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpePeerExtDeactivateMPCapability ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpePeerExtMplsVpnVrfAssociated ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4mpePeerExtMplsVpnCERouteTargetAdvt ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpePeerExtMplsVpnCESiteOfOrigin ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4mpePeerExtOverrideCapability ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4MpePeerExtTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MpeMEDTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeMEDTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeMEDTable  */

INT1
nmhGetFirstIndexFsbgp4MpeMEDTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeMEDTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeMEDAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeMEDRemoteAS ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsbgp4mpeMEDIPAddrAfi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeMEDIPAddrSafi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeMEDIPAddrPrefix ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpeMEDIPAddrPrefixLen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeMEDIntermediateAS ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpeMEDDirection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeMEDValue ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsbgp4mpeMEDPreference ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeMEDVrfName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4mpeMEDAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeMEDRemoteAS ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsbgp4mpeMEDIPAddrAfi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeMEDIPAddrSafi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeMEDIPAddrPrefix ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4mpeMEDIPAddrPrefixLen ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeMEDIntermediateAS ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4mpeMEDDirection ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeMEDValue ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsbgp4mpeMEDPreference ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeMEDVrfName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4mpeMEDAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeMEDRemoteAS ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsbgp4mpeMEDIPAddrAfi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeMEDIPAddrSafi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeMEDIPAddrPrefix ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4mpeMEDIPAddrPrefixLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeMEDIntermediateAS ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4mpeMEDDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeMEDValue ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsbgp4mpeMEDPreference ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeMEDVrfName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4MpeMEDTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MpeLocalPrefTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeLocalPrefTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeLocalPrefTable  */

INT1
nmhGetFirstIndexFsbgp4MpeLocalPrefTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeLocalPrefTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeLocalPrefAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeLocalPrefRemoteAS ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsbgp4mpeLocalPrefIPAddrAfi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeLocalPrefIPAddrSafi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeLocalPrefIPAddrPrefix ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpeLocalPrefIPAddrPrefixLen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeLocalPrefIntermediateAS ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpeLocalPrefDirection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeLocalPrefValue ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsbgp4mpeLocalPrefPreference ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeLocalPrefVrfName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4mpeLocalPrefAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeLocalPrefRemoteAS ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsbgp4mpeLocalPrefIPAddrAfi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeLocalPrefIPAddrSafi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeLocalPrefIPAddrPrefix ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4mpeLocalPrefIPAddrPrefixLen ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeLocalPrefIntermediateAS ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4mpeLocalPrefDirection ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeLocalPrefValue ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsbgp4mpeLocalPrefPreference ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeLocalPrefVrfName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4mpeLocalPrefAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeLocalPrefRemoteAS ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsbgp4mpeLocalPrefIPAddrAfi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeLocalPrefIPAddrSafi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeLocalPrefIPAddrPrefix ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4mpeLocalPrefIPAddrPrefixLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeLocalPrefIntermediateAS ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4mpeLocalPrefDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeLocalPrefValue ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsbgp4mpeLocalPrefPreference ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeLocalPrefVrfName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4MpeLocalPrefTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MpeUpdateFilterTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeUpdateFilterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeUpdateFilterTable  */

INT1
nmhGetFirstIndexFsbgp4MpeUpdateFilterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeUpdateFilterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeUpdateFilterAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeUpdateFilterRemoteAS ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsbgp4mpeUpdateFilterIPAddrAfi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeUpdateFilterIPAddrSafi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeUpdateFilterIPAddrPrefix ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpeUpdateFilterIPAddrPrefixLen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeUpdateFilterIntermediateAS ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpeUpdateFilterDirection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeUpdateFilterAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeUpdateFilterVrfName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpebgpPeerHoldAdvtRoutes ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4mpeUpdateFilterAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeUpdateFilterRemoteAS ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsbgp4mpeUpdateFilterIPAddrAfi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeUpdateFilterIPAddrSafi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeUpdateFilterIPAddrPrefix ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4mpeUpdateFilterIPAddrPrefixLen ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeUpdateFilterIntermediateAS ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4mpeUpdateFilterDirection ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeUpdateFilterAction ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeUpdateFilterVrfName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4mpeUpdateFilterAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeUpdateFilterRemoteAS ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsbgp4mpeUpdateFilterIPAddrAfi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeUpdateFilterIPAddrSafi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeUpdateFilterIPAddrPrefix ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4mpeUpdateFilterIPAddrPrefixLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeUpdateFilterIntermediateAS ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4mpeUpdateFilterDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeUpdateFilterAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeUpdateFilterVrfName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4MpeUpdateFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MpeAggregateTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeAggregateTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeAggregateTable  */

INT1
nmhGetFirstIndexFsbgp4MpeAggregateTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeAggregateTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeAggregateAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeAggregateIPAddrAfi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeAggregateIPAddrSafi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeAggregateIPAddrPrefix ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpeAggregateIPAddrPrefixLen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeAggregateAdvertise ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeAggregateVrfName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpeAggregateAsSet ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeAggregateAdvertiseRouteMapName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpeAggregateSuppressRouteMapName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4mpeAggregateAttributeRouteMapName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4mpeAggregateAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeAggregateIPAddrAfi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeAggregateIPAddrSafi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeAggregateIPAddrPrefix ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4mpeAggregateIPAddrPrefixLen ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeAggregateAdvertise ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeAggregateVrfName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4mpeAggregateAsSet ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeAggregateAdvertiseRouteMapName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4mpeAggregateSuppressRouteMapName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4mpeAggregateAttributeRouteMapName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE *));



/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4mpeAggregateAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeAggregateIPAddrAfi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeAggregateIPAddrSafi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeAggregateIPAddrPrefix ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4mpeAggregateIPAddrPrefixLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeAggregateAdvertise ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeAggregateVrfName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4mpeAggregateAsSet ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeAggregateAdvertiseRouteMapName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4mpeAggregateSuppressRouteMapName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4mpeAggregateAttributeRouteMapName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));


/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4MpeAggregateTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MpeImportRouteTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeImportRouteTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeImportRouteTable  */

INT1
nmhGetFirstIndexFsbgp4MpeImportRouteTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeImportRouteTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeImportRouteAction ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4mpeImportRouteAction ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4mpeImportRouteAction ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4MpeImportRouteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MpeFsmTransitionHistTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeFsmTransitionHistTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeFsmTransitionHistTable  */

INT1
nmhGetFirstIndexFsbgp4MpeFsmTransitionHistTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeFsmTransitionHistTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeFsmTransitionHist ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for Fsbgp4MpeRfdRtDampHistTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeRfdRtDampHistTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeRfdRtDampHistTable  */

INT1
nmhGetFirstIndexFsbgp4MpeRfdRtDampHistTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeRfdRtDampHistTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeRfdRtFom ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeRfdRtLastUpdtTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeRfdRtState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeRfdRtStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeRfdRtFlapCount ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeRfdRtFlapTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeRfdRtReuseTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for Fsbgp4MpeRfdPeerDampHistTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeRfdPeerDampHistTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeRfdPeerDampHistTable  */

INT1
nmhGetFirstIndexFsbgp4MpeRfdPeerDampHistTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeRfdPeerDampHistTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeRfdPeerFom ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeRfdPeerLastUpdtTime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeRfdPeerState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeRfdPeerStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for Fsbgp4MpeRfdRtsReuseListTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeRfdRtsReuseListTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeRfdRtsReuseListTable  */

INT1
nmhGetFirstIndexFsbgp4MpeRfdRtsReuseListTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeRfdRtsReuseListTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeRfdRtReuseListRtFom ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeRfdRtReuseListRtLastUpdtTime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeRfdRtReuseListRtState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeRfdRtReuseListRtStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for Fsbgp4MpeRfdPeerReuseListTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeRfdPeerReuseListTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeRfdPeerReuseListTable  */

INT1
nmhGetFirstIndexFsbgp4MpeRfdPeerReuseListTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeRfdPeerReuseListTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeRfdPeerReuseListPeerFom ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeRfdPeerReuseListLastUpdtTime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeRfdPeerReuseListPeerState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeRfdPeerReuseListPeerStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for Fsbgp4MpeCommRouteAddCommTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeCommRouteAddCommTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeCommRouteAddCommTable  */

INT1
nmhGetFirstIndexFsbgp4MpeCommRouteAddCommTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeCommRouteAddCommTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeAddCommRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4mpeAddCommRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4mpeAddCommRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4MpeCommRouteAddCommTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MpeCommRouteDeleteCommTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeCommRouteDeleteCommTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeCommRouteDeleteCommTable  */

INT1
nmhGetFirstIndexFsbgp4MpeCommRouteDeleteCommTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeCommRouteDeleteCommTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeDeleteCommRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4mpeDeleteCommRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4mpeDeleteCommRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4MpeCommRouteDeleteCommTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MpeCommRouteCommSetStatusTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeCommRouteCommSetStatusTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeCommRouteCommSetStatusTable  */

INT1
nmhGetFirstIndexFsbgp4MpeCommRouteCommSetStatusTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeCommRouteCommSetStatusTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeCommSetStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeCommSetStatusRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4mpeCommSetStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeCommSetStatusRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4mpeCommSetStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeCommSetStatusRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4MpeCommRouteCommSetStatusTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MpeExtCommRouteAddExtCommTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeExtCommRouteAddExtCommTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeExtCommRouteAddExtCommTable  */

INT1
nmhGetFirstIndexFsbgp4MpeExtCommRouteAddExtCommTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeExtCommRouteAddExtCommTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeAddExtCommRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4mpeAddExtCommRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4mpeAddExtCommRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4MpeExtCommRouteAddExtCommTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MpeExtCommRouteDeleteExtCommTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeExtCommRouteDeleteExtCommTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeExtCommRouteDeleteExtCommTable  */

INT1
nmhGetFirstIndexFsbgp4MpeExtCommRouteDeleteExtCommTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeExtCommRouteDeleteExtCommTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeDeleteExtCommRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4mpeDeleteExtCommRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4mpeDeleteExtCommRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4MpeExtCommRouteDeleteExtCommTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MpeExtCommRouteExtCommSetStatusTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeExtCommRouteExtCommSetStatusTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeExtCommRouteExtCommSetStatusTable  */

INT1
nmhGetFirstIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeExtCommSetStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeExtCommSetStatusRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4mpeExtCommSetStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeExtCommSetStatusRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4mpeExtCommSetStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeExtCommSetStatusRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4MpeExtCommRouteExtCommSetStatusTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MpePeerLinkBwTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpePeerLinkBwTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpePeerLinkBwTable  */

INT1
nmhGetFirstIndexFsbgp4MpePeerLinkBwTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpePeerLinkBwTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeLinkBandWidth ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsbgp4mpePeerLinkBwRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4mpeLinkBandWidth ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsbgp4mpePeerLinkBwRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4mpeLinkBandWidth ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2Fsbgp4mpePeerLinkBwRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4MpePeerLinkBwTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MpeCapSupportedCapsTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeCapSupportedCapsTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeCapSupportedCapsTable  */

INT1
nmhGetFirstIndexFsbgp4MpeCapSupportedCapsTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeCapSupportedCapsTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeCapSupportedCapsRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeCapAnnouncedStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeCapReceivedStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeCapNegotiatedStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4mpeCapConfiguredStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4mpeCapSupportedCapsRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4mpeCapSupportedCapsRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4MpeCapSupportedCapsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MpeRtRefreshInboundTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeRtRefreshInboundTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeRtRefreshInboundTable  */

INT1
nmhGetFirstIndexFsbgp4MpeRtRefreshInboundTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeRtRefreshInboundTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeRtRefreshInboundRequest ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsbgp4mpeRtRefreshInboundPrefixFilter ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));
/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4mpeRtRefreshInboundRequest ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsbgp4mpeRtRefreshInboundPrefixFilter ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));
/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4mpeRtRefreshInboundRequest ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4mpeRtRefreshInboundPrefixFilter ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));
/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4MpeRtRefreshInboundTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MpeRtRefreshStatisticsTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeRtRefreshStatisticsTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeRtRefreshStatisticsTable  */

INT1
nmhGetFirstIndexFsbgp4MpeRtRefreshStatisticsTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeRtRefreshStatisticsTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgSentCntr ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgTxErrCntr ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgRcvdCntr ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsbgp4mpeRtRefreshStatisticsRtRefMsgInvalidCntr ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for Fsbgp4MpeSoftReconfigOutboundTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpeSoftReconfigOutboundTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpeSoftReconfigOutboundTable  */

INT1
nmhGetFirstIndexFsbgp4MpeSoftReconfigOutboundTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpeSoftReconfigOutboundTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4mpeSoftReconfigOutboundRequest ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4mpeSoftReconfigOutboundRequest ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4mpeSoftReconfigOutboundRequest ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4MpeSoftReconfigOutboundTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MpePrefixCountersTable. */
INT1
nmhValidateIndexInstanceFsbgp4MpePrefixCountersTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MpePrefixCountersTable  */

INT1
nmhGetFirstIndexFsbgp4MpePrefixCountersTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MpePrefixCountersTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4MpePrefixCountersPrefixesReceived ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsbgp4MpePrefixCountersPrefixesSent ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsbgp4MpePrefixCountersWithdrawsReceived ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsbgp4MpePrefixCountersWithdrawsSent ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsbgp4MpePrefixCountersInPrefixes ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsbgp4MpePrefixCountersInPrefixesAccepted ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsbgp4MpePrefixCountersInPrefixesRejected ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsbgp4MpePrefixCountersOutPrefixes ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for Fsbgp4MplsVpnVrfRouteTargetTable. */
INT1
nmhValidateIndexInstanceFsbgp4MplsVpnVrfRouteTargetTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MplsVpnVrfRouteTargetTable  */

INT1
nmhGetFirstIndexFsbgp4MplsVpnVrfRouteTargetTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MplsVpnVrfRouteTargetTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4MplsVpnVrfRouteTargetRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4MplsVpnVrfRouteTargetRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4MplsVpnVrfRouteTargetRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4MplsVpnVrfRouteTargetTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MplsVpnVrfRedistributeTable. */
INT1
nmhValidateIndexInstanceFsbgp4MplsVpnVrfRedistributeTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MplsVpnVrfRedistributeTable  */

INT1
nmhGetFirstIndexFsbgp4MplsVpnVrfRedistributeTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MplsVpnVrfRedistributeTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4MplsVpnVrfRedisProtoMask ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4MplsVpnVrfRedisProtoMask ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4MplsVpnVrfRedisProtoMask ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4MplsVpnVrfRedistributeTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4MplsVpnRRRouteTargetTable. */
INT1
nmhValidateIndexInstanceFsbgp4MplsVpnRRRouteTargetTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4MplsVpnRRRouteTargetTable  */

INT1
nmhGetFirstIndexFsbgp4MplsVpnRRRouteTargetTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4MplsVpnRRRouteTargetTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4MplsVpnRRRouteTargetRtCnt ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsbgp4MplsVpnRRRouteTargetTimeStamp ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for FsBgp4DistInOutRouteMapTable. */
INT1
nmhValidateIndexInstanceFsBgp4DistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsBgp4DistInOutRouteMapTable  */

INT1
nmhGetFirstIndexFsBgp4DistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsBgp4DistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBgp4DistInOutRouteMapValue ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsBgp4DistInOutRouteMapRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBgp4DistInOutRouteMapValue ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsBgp4DistInOutRouteMapRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBgp4DistInOutRouteMapValue ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsBgp4DistInOutRouteMapRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBgp4DistInOutRouteMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBgp4PreferenceValue ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBgp4PreferenceValue ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBgp4PreferenceValue ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBgp4PreferenceValue ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsBgp4PeerGroupTable. */
INT1
nmhValidateIndexInstanceFsBgp4PeerGroupTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsBgp4PeerGroupTable  */

INT1
nmhGetFirstIndexFsBgp4PeerGroupTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsBgp4PeerGroupTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBgp4PeerGroupAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupRemoteAs ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsBgp4PeerGroupHoldTimeConfigured ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupKeepAliveConfigured ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupConnectRetryInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupMinASOriginInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupMinRouteAdvInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupAllowAutomaticStart ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupAllowAutomaticStop ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupIdleHoldTimeConfigured ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupDampPeerOscillations ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupDelayOpen ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupDelayOpenTimeConfigured ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupPrefixUpperLimit ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupTcpConnectRetryCnt ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupEBGPMultiHop ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupEBGPHopLimit ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupNextHopSelf ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupRflClient ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupTcpSendBufSize ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupTcpRcvBufSize ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupCommSendStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupECommSendStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupPassive ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupDefaultOriginate ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupActivateMPCapability ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupDeactivateMPCapability ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupRouteMapNameIn ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsBgp4PeerGroupRouteMapNameOut ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsBgp4PeerGroupStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupIpPrefixNameIn ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsBgp4PeerGroupIpPrefixNameOut ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsBgp4PeerGroupOrfType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsBgp4PeerGroupOrfCapMode ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupOrfRequest ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupBfdStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupOverrideCapability ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4PeerGroupLocalAs ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBgp4PeerGroupRemoteAs ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsBgp4PeerGroupHoldTimeConfigured ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupKeepAliveConfigured ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupConnectRetryInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupMinASOriginInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupMinRouteAdvInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupAllowAutomaticStart ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupAllowAutomaticStop ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupIdleHoldTimeConfigured ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupDampPeerOscillations ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupDelayOpen ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupDelayOpenTimeConfigured ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupPrefixUpperLimit ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupTcpConnectRetryCnt ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupEBGPMultiHop ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupEBGPHopLimit ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupNextHopSelf ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupRflClient ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupTcpSendBufSize ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupTcpRcvBufSize ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupCommSendStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));
INT1
nmhSetFsbgp4mpebgpPeerHoldAdvtRoutes ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupECommSendStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupPassive ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupDefaultOriginate ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupActivateMPCapability ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupDeactivateMPCapability ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupRouteMapNameIn ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsBgp4PeerGroupRouteMapNameOut ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsBgp4PeerGroupStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupIpPrefixNameIn ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsBgp4PeerGroupIpPrefixNameOut ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsBgp4PeerGroupOrfType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsBgp4PeerGroupOrfCapMode ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupOrfRequest ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupBfdStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupOverrideCapability ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4PeerGroupLocalAs ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBgp4PeerGroupRemoteAs ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsBgp4PeerGroupHoldTimeConfigured ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupKeepAliveConfigured ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupConnectRetryInterval ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupMinASOriginInterval ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupMinRouteAdvInterval ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupAllowAutomaticStart ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupAllowAutomaticStop ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupIdleHoldTimeConfigured ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupDampPeerOscillations ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupDelayOpen ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupDelayOpenTimeConfigured ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupPrefixUpperLimit ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupTcpConnectRetryCnt ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupEBGPMultiHop ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupEBGPHopLimit ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupNextHopSelf ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupRflClient ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupTcpSendBufSize ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupTcpRcvBufSize ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupCommSendStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsbgp4mpebgpPeerHoldAdvtRoutes ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupECommSendStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupPassive ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupDefaultOriginate ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupActivateMPCapability ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupDeactivateMPCapability ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupRouteMapNameIn ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsBgp4PeerGroupRouteMapNameOut ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsBgp4PeerGroupStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupIpPrefixNameIn ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsBgp4PeerGroupIpPrefixNameOut ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsBgp4PeerGroupOrfType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsBgp4PeerGroupOrfCapMode ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupOrfRequest ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupBfdStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupOverrideCapability ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4PeerGroupLocalAs ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBgp4PeerGroupTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsBgp4PeerGroupListTable. */
INT1
nmhValidateIndexInstanceFsBgp4PeerGroupListTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsBgp4PeerGroupListTable  */

INT1
nmhGetFirstIndexFsBgp4PeerGroupListTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsBgp4PeerGroupListTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBgp4PeerAddStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBgp4PeerAddStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBgp4PeerAddStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBgp4PeerGroupListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
/* Proto Validate Index Instance for FsBgp4NeighborRouteMapTable. */
INT1
nmhValidateIndexInstanceFsBgp4NeighborRouteMapTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));


/* Proto Type for Low Level GET FIRST fn for FsBgp4NeighborRouteMapTable  */

INT1
nmhGetFirstIndexFsBgp4NeighborRouteMapTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsBgp4NeighborRouteMapTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBgp4NeighborRouteMapName ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsBgp4NeighborRouteMapRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBgp4NeighborRouteMapName ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsBgp4NeighborRouteMapRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBgp4NeighborRouteMapName ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsBgp4NeighborRouteMapRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBgp4NeighborRouteMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4TCPMKTAuthTable. */
INT1
nmhValidateIndexInstanceFsbgp4TCPMKTAuthTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4TCPMKTAuthTable  */

INT1
nmhGetFirstIndexFsbgp4TCPMKTAuthTable ARG_LIST((INT4 *));

/* Proto Validate Index Instance for FsBgp4ORFListTable. */
INT1
nmhValidateIndexInstanceFsBgp4ORFListTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsBgp4ORFListTable  */

INT1
nmhGetFirstIndexFsBgp4ORFListTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , UINT4 * , UINT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , UINT4 * , UINT4 * , INT4 *));
/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4TCPMKTAuthTable ARG_LIST((INT4 , INT4 *));
INT1
nmhGetNextIndexFsBgp4ORFListTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4TCPMKTAuthRecvKeyId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4TCPMKTAuthMasterKey ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4TCPMKTAuthAlgo ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4TCPMKTAuthTcpOptExc ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsbgp4TCPMKTAuthRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4TCPMKTAuthRecvKeyId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4TCPMKTAuthMasterKey ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4TCPMKTAuthAlgo ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4TCPMKTAuthTcpOptExc ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsbgp4TCPMKTAuthRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4TCPMKTAuthRecvKeyId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4TCPMKTAuthMasterKey ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4TCPMKTAuthAlgo ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4TCPMKTAuthTcpOptExc ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4TCPMKTAuthRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4TCPMKTAuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsbgp4TCPAOAuthPeerTable. */
INT1
nmhValidateIndexInstanceFsbgp4TCPAOAuthPeerTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsbgp4TCPAOAuthPeerTable  */

INT1
nmhGetFirstIndexFsbgp4TCPAOAuthPeerTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsbgp4TCPAOAuthPeerTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsbgp4TCPAOAuthKeyStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsbgp4TCPAOAuthKeyStartAccept ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4TCPAOAuthKeyStartGenerate ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4TCPAOAuthKeyStopGenerate ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsbgp4TCPAOAuthKeyStopAccept ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4TCPAOAuthKeyStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsbgp4TCPAOAuthKeyStartAccept ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4TCPAOAuthKeyStartGenerate ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4TCPAOAuthKeyStopGenerate ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsbgp4TCPAOAuthKeyStopAccept ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4TCPAOAuthKeyStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2Fsbgp4TCPAOAuthKeyStartAccept ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4TCPAOAuthKeyStartGenerate ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4TCPAOAuthKeyStopGenerate ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsbgp4TCPAOAuthKeyStopAccept ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4TCPAOAuthPeerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
INT1
nmhGetFsBgp4RmTestObject ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBgp4RmTestObject ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBgp4RmTestObject ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBgp4RmTestObject ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
/* Proto Validate Index Instance for FsBgp4RRDNetworkTable. */
INT1
nmhValidateIndexInstanceFsBgp4RRDNetworkTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsBgp4RRDNetworkTable  */

INT1
nmhGetFirstIndexFsBgp4RRDNetworkTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsBgp4RRDNetworkTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBgp4RRDNetworkAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4RRDNetworkPrefixLen ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsBgp4RRDNetworkRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBgp4RRDNetworkAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4RRDNetworkPrefixLen ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsBgp4RRDNetworkRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBgp4RRDNetworkAddrType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4RRDNetworkPrefixLen ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsBgp4RRDNetworkRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBgp4RRDNetworkTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhGetFsbgp4DebugType ARG_LIST((UINT4 *));

INT1
nmhGetFsbgp4DebugTypeIPAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsbgp4DebugType ARG_LIST((UINT4 ));

INT1
nmhSetFsbgp4DebugTypeIPAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsbgp4DebugType ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2Fsbgp4DebugTypeIPAddr ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsbgp4DebugType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsbgp4DebugTypeIPAddr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


#endif /* FSBGPLOW_H */
