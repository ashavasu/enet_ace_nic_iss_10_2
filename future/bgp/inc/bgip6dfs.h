/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved    
 *
 * $Id: bgip6dfs.h,v 1.4 2016/05/09 12:03:52 siva Exp $
 *
 *******************************************************************/
#ifndef _BGIP6DFS_H
#define _BGIP6DFS_H

#define  BGP4_MAX_IPV6_PREFIXLEN            128
#define  BGP4_ATTR_IPV6_NEXTHOP_LEN         16 


/* Used by capabilities module for ipv6 processing */
#define  CAP_MP_IPV6_UNICAST                0x00020001
#define  CAP_NEG_IPV6_UNI_MASK              0x00000002

/* Used by MPE module */
#define BGP4_MPE_IPV6_PREFIX_LEN            0x01 
#ifdef L3VPN
/* By default <ipv4, unicast> family is supported */
#define BGP4_IPV6_UNI_INDEX                 2
#else
#define BGP4_IPV6_UNI_INDEX                 1
#endif
#define BGP4_MAX_IPV6_INET_ADDRESS_LEN      16  /* only for IPV6 */
#define BGP4_IPV6_PREFIX_LEN                16 
#define BGP4_IP6_IP4_ADDR                   0xffff
#define BGP4_IP6_IP4_ADDR_LEN               2

#define BGP4_IPV6_RIB_TREE(u4CxtId)                \
                        (gBgpCxtNode[u4CxtId]->gapBgp4Rib[BGP4_IPV6_UNI_INDEX])
#define BGP4_PEER_IPV6_AFISAFI_INSTANCE(pPeerentry) \
                    (pPeerentry->apAsafiInstance[BGP4_IPV6_UNI_INDEX])

/* Peer related ipv6 lists macros */
#define BGP4_PEER_IPV6_OUTPUT_ROUTES(pPeerentry) \
        (BGP4_PEER_IPV6_AFISAFI_INSTANCE(pPeerentry)->pOutputList)
#define BGP4_PEER_IPV6_NEW_ROUTES(pPeerentry) \
        (BGP4_PEER_IPV6_AFISAFI_INSTANCE(pPeerentry)->pNewList)
#define BGP4_PEER_IPV6_NEW_LOCAL_ROUTES(pPeerentry) \
        (BGP4_PEER_IPV6_AFISAFI_INSTANCE(pPeerentry)->pNewLocalList)
#define BGP4_PEER_IPV6_ADVT_WITH_ROUTES(pPeerentry) \
        (BGP4_PEER_IPV6_AFISAFI_INSTANCE(pPeerentry)->pAdvtWithList)
#define BGP4_PEER_IPV6_ADVT_FEAS_ROUTES(pPeerentry) \
        (BGP4_PEER_IPV6_AFISAFI_INSTANCE(pPeerentry)->pAdvtFeasList)
#define BGP4_PEER_IPV6_DEINIT_RT_LIST(pPeerentry) \
        (&(BGP4_PEER_IPV6_AFISAFI_INSTANCE(pPeerentry)->TSDeInitRtList))

/* Macros for counters */
#define BGP4_PEER_IPV6_PREFIX_RCVD_CNT(pPeerentry) \
            ((pPeerentry->apAsafiInstance[BGP4_IPV6_UNI_INDEX])->PrefixCounters).u4PrefixesRcvd
#define BGP4_PEER_IPV6_PREFIX_SENT_CNT(pPeerentry) \
            ((pPeerentry->apAsafiInstance[BGP4_IPV6_UNI_INDEX])->PrefixCounters).u4PrefixesSent
#define BGP4_PEER_IPV6_WITHDRAWS_RCVD_CNT(pPeerentry) \
            ((pPeerentry->apAsafiInstance[BGP4_IPV6_UNI_INDEX])->PrefixCounters).u4WithdrawsRcvd
#define BGP4_PEER_IPV6_WITHDRAWS_SENT_CNT(pPeerentry) \
            ((pPeerentry->apAsafiInstance[BGP4_IPV6_UNI_INDEX])->PrefixCounters).u4WithdrawsSent
#define BGP4_PEER_IPV6_IN_PREFIXES_CNT(pPeerentry) \
            ((pPeerentry->apAsafiInstance[BGP4_IPV6_UNI_INDEX])->PrefixCounters).u4InPrefixes
#define BGP4_PEER_IPV6_IN_PREFIXES_ACPTD_CNT(pPeerentry) \
            ((pPeerentry->apAsafiInstance[BGP4_IPV6_UNI_INDEX])->PrefixCounters).u4InPrefixesAcptd
#define BGP4_PEER_IPV6_IN_PREFIXES_RJCTD_CNT(pPeerentry) \
            ((pPeerentry->apAsafiInstance[BGP4_IPV6_UNI_INDEX])->PrefixCounters).u4InPrefixesRjctd
#define BGP4_PEER_IPV6_OUT_PREFIXES_CNT(pPeerentry) \
            ((pPeerentry->apAsafiInstance[BGP4_IPV6_UNI_INDEX])->PrefixCounters).u4OutPrefixes
#define BGP4_PEER_IPV6_INVALID_MPE_UPDATE_CNT(pPeerentry) \
            ((pPeerentry->apAsafiInstance[BGP4_IPV6_UNI_INDEX])->PrefixCounters).u4InvalidMpeAttribCntr

#endif /* BGIP6DFS_H */

