/* $Id: bgecm.h,v 1.15 2015/07/20 07:20:51 siva Exp $*/
#ifndef _BGECM_H
#define _BGECM_H

/* Constant used in the extended Community Attribute module */
#define       EXT_COMM_TYPE_CODE                             BGP4_ATTR_ECOMM
#define       PATH_ATTRIBUTE_TYPE_SIZE                       0x2
#define       EXT_COMM_SUCCESS                               BGP4_SUCCESS
#define       EXT_COMM_FAILURE                               BGP4_FAILURE
#define       EXT_COMM_TRUE                                  BGP4_TRUE
#define       EXT_COMM_FALSE                                 BGP4_FALSE
#define       EXT_COMM_PEER_SEND                             EXT_COMM_TRUE
#define       EXT_COMM_PEER_DO_NOT_SEND                      EXT_COMM_FALSE
#define       EXT_COMM_ACCEPT                                0x1
#define       EXT_COMM_DENY                                  0x2
#define       EXT_COMM_IGNORE                                0x4
#define       EXT_COMM_ADVERTISE                             0x1
#define       EXT_COMM_FILTER                                0x2
#define       LINK_BANDWIDTH_COMMUNITY                       0x4004
#define       ACCEPT_INCOMING_EXT_COMM                       EXT_COMM_ACCEPT
#define       DENY_INCOMING_EXT_COMM                         EXT_COMM_DENY
#define       IGNORE_INCOMING_EXT_COMM                       EXT_COMM_IGNORE
#define       ADVERTISE_OUTGOING_EXT_COMM                    EXT_COMM_ADVERTISE
#define       FILTER_OUTGOING_EXT_COMM                       EXT_COMM_FILTER
#define       IGNORE_OUTGOING_EXT_COMM                       EXT_COMM_IGNORE
#define       EXT_COMM_PATH_EXISTS                           EXT_COMM_TRUE
#define       EXT_COMM_PATH_DOES_NOT_EXIST                   EXT_COMM_FALSE
#define       EXT_COMM_PATH_LEN_VALID                        EXT_COMM_TRUE
#define       EXT_COMM_PATH_LEN_INVALID                      EXT_COMM_FALSE
#define       ROUTE_HAS_CONF_EXT_COMM                        EXT_COMM_TRUE
#define       ROUTE_HAS_NO_CONF_EXT_COMM                     EXT_COMM_FALSE
#define       EXT_COMM_VALUE_LEN                             8
#define       EXT_COMM_MAX_NO_EXTBIT_LEN                     256
#define       MIN_LEN_OF_SINGLE_EXTCOMM_PATH                 11

#define       EXT_COMM_INVALID_POLICY                        0xFF
#define       EXT_COMM_PARTIAL_BIT_SET                       1
#define       EXT_COMM_PARTIAL_BIT_UNSET                     0

#define       EXT_COMM_EXTLEN_BIT_UNSET                      0
#define       EXT_COMM_MIN_EXT_COMM_TOFILL                   1
#define       EXT_COMM_MIN_COMM_TOFILL                       1

#define       EXT_COMM_MIN_RFC_DEFINED_HIGH_ORDER_EXT_TYPE   0
#define       EXT_COMM_MAX_RFC_DEFINED_HIGH_ORDER_EXT_TYPE   3
#define       EXT_COMM_MIN_RFC_DEFINED_LOW_ORDER_EXT_TYPE    2
#define       EXT_COMM_MAX_RFC_DEFINED_LOW_ORDER_EXT_TYPE    4

#define       EXT_COMM_MIN_VENDOR_DEFINED_HIGH_ORDER_REG_TYPE  0x80
#define       EXT_COMM_MAX_VENDOR_DEFINED_HIGH_ORDER_REG_TYPE  0xbf

#define       BAND_WIDTH_MIN_IN_BPS                          7000
#define       BAND_WIDTH_MAX_IN_BPS                          0xffffffff
#define       EXT_COMM_TWO_BYTES                             2

#define       MAX_INPUT_EXT_COMM_FILTER_TBL_ENTRIES  10000
#define       MAX_OUTPUT_EXT_COMM_FILTER_TBL_ENTRIES 10000

#define       EXT_COMM_MAX_FILTER_VALUE                      0xffff 


/* Definitions for Extended community types */
#define       EXT_COMM_TYPE_MASK                            (0xffff)
#define       EXT_COMM_HIGH_ORDER_OCTET_MASK                (0xff00)
#define       EXT_COMM_HIGH_ORDER_BYTE_TYPE_00               0x00 /* Transitive Two-Octet AS-Specific Extended Community */ 
#define       EXT_COMM_HIGH_ORDER_BYTE_TYPE_01               0x01 /* Transitive IPv4-Address-Specific Extended Community */
#define       EXT_COMM_HIGH_ORDER_BYTE_TYPE_02               0x02 /* Transitive Four-Octet AS-Specific Extended Community */
#define       EXT_COMM_HIGH_ORDER_BYTE_TYPE_03               0x03 /* Transitive Opaque Extended Community */
#define       EXT_COMM_HIGH_ORDER_BYTE_TYPE_06               0x06 /* EVPN Extended Community */
#define       EXT_COMM_HIGH_ORDER_BYTE_TYPE_40               0x40 /* Non-Transitive Two-Octet AS-Specific Extended Community  */
#define       EXT_COMM_HIGH_ORDER_BYTE_TYPE_41               0x41 /* Non-Transitive IPv4-Address-Specific Extended Community */
#define       EXT_COMM_HIGH_ORDER_BYTE_TYPE_42               0x42 /* Non-Transitive Four-Octet AS-Specific Extended Community */
#define       EXT_COMM_HIGH_ORDER_BYTE_TYPE_43               0x43 /* Non-Transitive Opaque Extended Community */

#ifdef VPLSADS_WANTED
#define       EXT_COMM_OWNER_CLI                             0x01
#define       EXT_COMM_OWNER_L2VPN                           0x02 
#define       EXT_COMM_HIGH_ORDER_BYTE_TYPE_80               0x80
#define       EXT_COMM_LOW_ORDER_BYTE_TYPE_0A                0x0A
#endif

#define       EXT_COMM_LOW_ORDER_OCTET_MASK                 (0x00ff)
#define       EXT_COMM_LOW_ORDER_BYTE_TYPE_00                0x00 
#define       EXT_COMM_LOW_ORDER_BYTE_TYPE_01                0x01 
#define       EXT_COMM_LOW_ORDER_BYTE_TYPE_02                0x02 
#define       EXT_COMM_LOW_ORDER_BYTE_TYPE_03                0x03 
#define       EXT_COMM_LOW_ORDER_BYTE_TYPE_04                0x04 

/* Definition of Extended Community filter information node data structure */

/******  All type definitions of Extended Community Attribute Module  ******/ 

/* Definition of Extended Community filter information node data structure */
typedef struct t_ExtCommFilterInfo {
tTMO_SLL_NODE  ExtCommInfoNext; /* Next extended community information      */
                              /* node                                       */
UINT4          u4Context;     /* The VRF information for this ext community 
                                 filter                                     */
UINT1          au1ExtComm[8];     /* Holds the extended Community type value    */
INT1           i1FilterPolicy;/* Holds the filtering policy for outgoing OR */
                              /* incoming direction. For incoming direction */
                              /* value is EXT_COMM_DENY or EXT_COMM_ACCEPT. */
                              /* For outgoing direction value is            */
                              /* EXT_COMM_ADVERTISE or EXT_COMM_FILTER.     */
UINT1          u1RowStatus;   /* Row Status of this entry                   */

#ifdef VPLSADS_WANTED
UINT1          u1CommOwner;    /*CommOwner holds whether ExtComm is configure
                                 via CLI or L2VPN. Value are OWNER_CLI/OWNER_L2VPN */ 
INT1           ai1Padding[1];      /* For word alignment of data structure  */
#else
INT1           ai1Padding[2];      /* For word alignment of data structure  */
#endif
} tExtCommFilterInfo;

/* Definition of Extended Community node data structure */
typedef struct t_ExtCommProfile {
tTMO_SLL_NODE ExtCommProfileNext; /* Next Extended community profile node    */
UINT1          u1RowStatus;   /* Row Status of this entry                   */
#ifndef VPLS_GR_WANTED
INT1           ai1Padding[3]; /* for word alignment of the data structure   */
#endif
#ifdef VPLS_GR_WANTED
UINT1          u1Flag;        /* Flag to Mark RT as stale or unstale in VPLS HA */
INT1           ai1Padding[2]; /* for word alignment of the data structure   */
#endif
UINT1          au1ExtComm[8];      /* Extended Community the route is         */
                                  /* associated with                         */
} tExtCommProfile;

/* Definition of Route's List of Configured Extended Communities */
/* data structure */

typedef struct t_RouteConfExtComm {
tTMO_SLL_NODE  RouteConfExtCommNext;  /* Next route extended community      */
                                   /* configuration node                    */
tTMO_SLL       TSConfExtComms;     /* List that holds the extended communities*/
                                   /* configured for the route. It is an    */
                                   /* SLL of type tExtCommProfile           */ 
tNetAddress    RtNetAddress;       /* Netaddress                            */
UINT4          u4Context;          /* VRFId for the extComm configuration   */
} tRouteConfExtComm;

typedef struct  t_RouteExtCommSetStatus {
tTMO_SLL_NODE  RouteConfExtCommNext;/* Next route's ext-comm set config     */
                                   /* node                                  */
tNetAddress    RtNetAddress;       /* Netaddress                            */
UINT4          u4Context;          /* VRF associated with this entry        */
INT1           i1ExtCommSetStatus;/* It indicates whether the existing     */
                                   /* ext-comms are to be considered or     */ 
                                   /* not. It takes the value               */
                                   /* set/setnone/modify. set is to set only*/
                                   /* the additive communities. setnone is  */
                                   /* for sending route without ext-comms.  */
                                   /* Modify is to add additive ext-comms.  */
                                   /* & delete conf ext-comms.in rcvd ext-comm*/
                                   /* info                                  */
UINT1         u1RowStatus;         /* Row Status of this entry              */  
INT1          ai1Padding[2];       /* For word alignment of data structure  */
} tRouteExtCommSetStatus ;


/* Definition of Route's List of associated Extended Communities */
/* data structure */
typedef struct t_RouteExtCommPath {
UINT2   u2ExtCommCount;       /* Count of ext communities for the route      */
UINT2   u2ExtCommAdded;       /* Count of ext communities added for route    */
UINT2   u2ExtCommDeleted;     /* Count of ext communities deleted from route */
INT1    i1ExtCommPartial;     /* This indicates whether ext community path   */
                              /* information is complete or incomplete       */
INT1    i1ForWordAlign;       /* For word alignment of data structure        */
UINT1   au1ExtComm[BGP4_DEFAULT_MAX_NUM_EXT_COMM_PER_ROUTE*EXT_COMM_VALUE_LEN];
                              /* Array that holds ext communities of route   */
} tRouteExtCommPath;

/* Definition of Peer Link Bandwidth structure */
typedef struct t_PeerLinkBandWidth{
tTMO_SLL_NODE PeerLinkBandWidthNext; /* Next peer Link Bandwidth node       */
tAddrPrefix   PeerAddress;         /* This holds the Peer IP Address        */
UINT4         u4LinkBandWidth;       /* This holds the Peer's LinkBandWidth */
UINT4         u4Context;             /* VRF associated with this entry      */
UINT1         u1RowStatus;           /* Row Status of this entry            */  
INT1          ai1Padding[3];         /* For word alignment of data structure*/
} tPeerLinkBandWidth;

/* Definition of BGP Extended community params data structure */
typedef struct t_Bgp4ExtCommParams{
tMemPoolId     ExtCommInputFilterPoolId;  /* Memory Pool tExtCommFilterInfo  */
                                          /* memory units                    */
tMemPoolId     ExtCommOutputFilterPoolId; /* Memory Pool tExtCommFilterInfo  */
                                          /* memory units                    */
tMemPoolId     RouteConfExtCommPoolId;    /* Memory Pool of tRouteConfExtComm*/
                                          /* memory units                    */
tMemPoolId     ExtCommProfilePoolId;      /* Memory Pool of tExtCommProfile  */
                                          /* memory units                    */
tMemPoolId     RouteExtCommSetStatusPoolId;/*Memory Pool of                  */
                                          /* tRouteExtCommSetStatus.         */
                                          /* memory units                    */
tMemPoolId     PeerLinkBandWidthPoolId;   /* Memory Pool of                  */
                                          /* tPeerLinkBandWidth units        */
tBgp4RBTree    pRoutesExtCommAddTbl;  /* Pointer to the Hash table that  */
                                         /* holds the routes with configured*/
                                         /* ext communities to be added     */
                                         /* Holds list of tRouteConfExtComm */
tBgp4RBTree    pRoutesExtCommDeleteTbl;/* Pointer to the Hash table that  */
                                          /* holds the routes with configured*/
                                          /* ext communities to be deleted   */
                                          /* Holds list of tRouteConfExtComm */
tBgp4RBTree    pExtCommInputFilterTbl; /* Pointer to Hash table that      */
                                          /* holds input filter policy of    */
                                          /* ext communities. It is list of  */
                                          /* tExtCommFilterInfo.             */ 
tBgp4RBTree    pExtCommOutputFilterTbl;/* Pointer to  Hash table that     */
                                          /* holds output filter policy of   */
                                          /* ext communities.  It is list of */
                                          /* tExtCommFilterInfo.             */
tBgp4RBTree    pRouteExtCommSetStatusTbl; /* Pointer to  Hash table that  */
                                          /* holds set status of             */
                                          /* ext-comms.  It is list of       */
                                          /* tRouteExtCommSetStatus.         */
tBgp4RBTree    pPeerLinkBandWidthTbl;  /* Pointer to  Hash table that     */
                                          /* holds link bandwidth of peers   */
                                          /* It is list of tPeerLinkBandWidth*/
} tBgp4ExtCommParams;

/* ************** Macros ***************************************/ 

#define       EXT_COMM_MAX_ROUTES              Bgp4GetMaxRoutes ()
#define       EXT_COMM_MAX_PEERS               Bgp4GetMaxPeers ()
#define       EXT_COMM_GLOBAL_PARAMS           (gBgpNode.Bgp4ExtCommParams)
#define       ROUTES_EXT_COMM_ADD_TBL          \
                     ((EXT_COMM_GLOBAL_PARAMS).pRoutesExtCommAddTbl)
#define       ROUTES_EXT_COMM_DELETE_TBL       \
                     ((EXT_COMM_GLOBAL_PARAMS).pRoutesExtCommDeleteTbl)
#define       EXT_COMM_INPUT_FILTER_TBL        \
                     ((EXT_COMM_GLOBAL_PARAMS).pExtCommInputFilterTbl)
#define       EXT_COMM_OUTPUT_FILTER_TBL       \
                     ((EXT_COMM_GLOBAL_PARAMS).pExtCommOutputFilterTbl)
#define       ROUTES_EXT_COMM_SET_STATUS_TBL \
                     ((EXT_COMM_GLOBAL_PARAMS).pRouteExtCommSetStatusTbl)
#define       PEER_LINK_BANDWIDTH_TBL          \
                     ((EXT_COMM_GLOBAL_PARAMS).pPeerLinkBandWidthTbl)
#define       INPUT_EXT_COMM_FILTER_ENTRY_POOL_ID   \
                     ((EXT_COMM_GLOBAL_PARAMS).ExtCommInputFilterPoolId)
#define       OUTPUT_EXT_COMM_FILTER_ENTRY_POOL_ID  \
                     ((EXT_COMM_GLOBAL_PARAMS).ExtCommOutputFilterPoolId)
#define       ROUTE_CONF_EXT_COMM_POOL_ID           \
                     ((EXT_COMM_GLOBAL_PARAMS).RouteConfExtCommPoolId)
#define       EXT_COMM_PROFILE_POOL_ID              \
                     ((EXT_COMM_GLOBAL_PARAMS).ExtCommProfilePoolId)
#define       ROUTES_EXT_COMM_SET_STATUS_POOL_ID\
                    ((EXT_COMM_GLOBAL_PARAMS).RouteExtCommSetStatusPoolId)
#define       PEER_LINK_BANDWIDTH_POOL_ID           \
                     ((EXT_COMM_GLOBAL_PARAMS).PeerLinkBandWidthPoolId)
#define       EXT_COMM_LEN_ERROR_STATS(u4CxtId)     \
              (gBgpCxtNode[u4CxtId]->u4ExtCommLenError)
#define       EXT_COMM_BGP4_GET_ATTRIBUTE_CODE(u2AttribType) \
                                           (u2AttribType & 0x00ff)
#define       EXT_COMM_GET_HASH_INDEX               CommGetHashIndex
#define       EXT_COMM_NET_ADDRESS_IN_ROUTE_CONF_STRUCT(pRouteExtConfComm)\
                    (pRouteExtConfComm->RtNetAddress)
#define       EXT_COMM_NET_ADDRESS_IN_ROUTE_SET_STATUS_STRUCT(\
                    pExtRouteSetStatus) (pExtRouteSetStatus->RtNetAddress)
#define       EXT_COMM_PEER_INFO_IN_PEER_SET_STATUS_STRUCT(pPeerSetStatus)\
                    (pPeerSetStatus->PeerAddress)
#define       EXT_COMM_PEER_INFO_IN_PEER_LINKBW_STRUCT(pPeerLkBw)\
                    (pPeerLkBw->PeerAddress)
#define       EXT_COMM_RT_IP_PREFIX_IN_ROUTE_CONF_STRUCT(pRouteExtConfComm)\
                    (((pRouteExtConfComm->RtNetAddress).NetAddr).au1Address)
#define       EXT_COMM_RT_PREFIXLEN_IN_ROUTE_CONF_STRUCT(pRouteExtConfComm)\
                    ((pRouteExtConfComm->RtNetAddress).u2PrefixLen)
#define       EXT_COMM_RT_AFI_IN_ROUTE_CONF_STRUCT(pRouteExtConfComm)\
                    (((pRouteExtConfComm->RtNetAddress).NetAddr).u2Afi)
#define       EXT_COMM_RT_IP_PREFIX_IN_ROUTE_SET_STATUS_STRUCT(\
                    pExtRouteSetStatus)\
                    (((pExtRouteSetStatus->RtNetAddress).NetAddr).au1Address)
#define       EXT_COMM_RT_PREFIXLEN_IN_ROUTE_SET_STATUS_STRUCT(\
                    pExtRouteSetStatus)\
                    ((pExtRouteSetStatus->RtNetAddress).u2PrefixLen)
#define       EXT_COMM_RT_AFI_IN_ROUTE_SET_STATUS_STRUCT(\
                    pExtRouteSetStatus)\
                    (((pExtRouteSetStatus->RtNetAddress).NetAddr).u2Afi)
#define       BGP4_IS_EXT_COMM_PATH_LEN_VALID(u2ExtCommPathLen)  \
((u2ExtCommPathLen % 8) ? EXT_COMM_PATH_LEN_INVALID : EXT_COMM_PATH_LEN_VALID)

#define       EXT_COMM_INPUT_FILTER_ENTRY_CREATE(pExtCommFilterInfo) \
       pExtCommFilterInfo = (tExtCommFilterInfo *)MemAllocMemBlk (\
   INPUT_EXT_COMM_FILTER_ENTRY_POOL_ID);

#define       EXT_COMM_INPUT_FILTER_ENTRY_FREE(pExtCommFilterInfo) MemReleaseMemBlock (INPUT_EXT_COMM_FILTER_ENTRY_POOL_ID, (UINT1 *) pExtCommFilterInfo)

#define       EXT_COMM_OUTPUT_FILTER_ENTRY_CREATE(pExtCommFilterInfo)\
  pExtCommFilterInfo = (tExtCommFilterInfo *)\
 MemAllocMemBlk (OUTPUT_EXT_COMM_FILTER_ENTRY_POOL_ID);

#define       EXT_COMM_OUTPUT_FILTER_ENTRY_FREE(pExtCommFilterInfo) MemReleaseMemBlock (OUTPUT_EXT_COMM_FILTER_ENTRY_POOL_ID, (UINT1 *) pExtCommFilterInfo)

#define       EXT_COMM_ROUTES_ADD_TBL_ENTRY_CREATE(pRouteConfExtComm) \
 pRouteConfExtComm = (tRouteConfExtComm *) MemAllocMemBlk \
  (ROUTE_CONF_EXT_COMM_POOL_ID);

#define       EXT_COMM_ROUTES_ADD_TBL_ENTRY_FREE(pRouteConfExtComm) MemReleaseMemBlock (ROUTE_CONF_EXT_COMM_POOL_ID, (UINT1 *) pRouteConfExtComm)

#define       EXT_COMM_ROUTES_DELETE_TBL_ENTRY_CREATE(pRouteConfExtComm) \
 pRouteConfExtComm = (tRouteConfExtComm *)MemAllocMemBlk (\
 ROUTE_CONF_EXT_COMM_POOL_ID);

#define       EXT_COMM_ROUTES_DELETE_TBL_ENTRY_FREE(pRouteConfExtComm) MemReleaseMemBlock (ROUTE_CONF_EXT_COMM_POOL_ID, (UINT1 *) pRouteConfExtComm)

#define       PEER_LINK_BANDWIDTH_ENTRY_CREATE(pPeerLinkBandWidth) \
 pPeerLinkBandWidth = (tPeerLinkBandWidth *)MemAllocMemBlk (PEER_LINK_BANDWIDTH_POOL_ID);

#define       PEER_LINK_BANDWIDTH_ENTRY_FREE(pPeerLinkBandWidth) MemReleaseMemBlock (PEER_LINK_BANDWIDTH_POOL_ID, (UINT1 *) pPeerLinkBandWidth)

#define       EXT_COMM_PROFILE_CREATE(pExtCommProfile)\
 pExtCommProfile = (tExtCommProfile *) MemAllocMemBlk (EXT_COMM_PROFILE_POOL_ID);

#define       EXT_COMM_PROFILE_FREE(pExtCommProfile) MemReleaseMemBlock (EXT_COMM_PROFILE_POOL_ID , (UINT1 *) pExtCommProfile)

#define       EXT_COMM_ROUTES_EXT_COMM_SET_STATUS_ENTRY_CREATE(pRouteExtCommSetStatus) \
 pRouteExtCommSetStatus = (tRouteExtCommSetStatus *)MemAllocMemBlk (ROUTES_EXT_COMM_SET_STATUS_POOL_ID);

#define       EXT_COMM_ROUTES_EXT_COMM_SET_STATUS_ENTRY_FREE(pRouteExtCommSetStatus)    MemReleaseMemBlock (ROUTES_EXT_COMM_SET_STATUS_POOL_ID, (UINT1 *)  pRouteExtCommSetStatus)

#define       EXT_COMM_ROUTE_CONF_EXT_COMM_NODE_CREATE(pRouteConfExtComm) \
        pRouteConfExtComm = (tRouteConfExtComm *)MemAllocMemBlk (ROUTE_CONF_EXT_COMM_POOL_ID);


#define       EXT_COMM_ROUTE_CONF_EXT_COMM_NODE_FREE(pRouteConfExtComm)   \
 MemReleaseMemBlock (ROUTE_CONF_EXT_COMM_POOL_ID, (UINT1 *)pRouteConfExtComm)

#define       EXT_COMM_EXT_COMM_PROFILE_NODE_CREATE     EXT_COMM_PROFILE_CREATE

#define       EXT_COMM_EXT_COMM_PROFILE_NODE_FREE       EXT_COMM_PROFILE_FREE
#define EXT_COMM_HASH_DYN_Scan_Bucket(pHashTab, u1HashKey, pNode, pTempNode, TypeCast)  BGP_SLL_DYN_Scan (&((pHashTab)->HashList[u1HashKey]), pNode, pTempNode, TypeCast)

#define       EXT_COMM_SLL_DYN_Scan     BGP_SLL_DYN_Scan
#define       EXT_COMM_ROUTES_LIST_FREE(pRouteConfExtComm)    \
              EXT_COMM_SLL_DYN_Scan(&(pRouteConfExtComm->TSConfExtComms),\
                          pExtCommProf, pTmpExtCommProf, tExtCommProfile *)     \
             {  \
                 EXT_COMM_PROFILE_FREE(pExtCommProf);          \
             }  \
             TMO_SLL_Init(&(pRouteConfExtComm->TSConfExtComms));

#define       EXT_COMM_ROUTES_ADD_LIST_FREE(pRouteConfExtComm) \
              EXT_COMM_ROUTES_LIST_FREE(pRouteConfExtComm)

#define       EXT_COMM_ROUTES_DELETE_LIST_FREE(pRouteConfExtComm) \
              EXT_COMM_ROUTES_LIST_FREE(pRouteConfExtComm)



/* Function Prototypes */

INT4 ExtCommInit (INT4 i4MaxRoutes, INT4 i4MaxPeers );
INT4 ExtCommShut (VOID);
INT4 ExtCommFormListForInst (tTMO_SLL *pFeasibleRoutesList,
                             tBgp4PeerEntry * pPeerInfo);
INT4 EcommApplyRoutesInboundFilterPolicy (tRouteProfile *pFeasibleRoute);
INT4 ExtCommApplyOutboundFilterPolicy (tRouteProfile    *pRtProfile,
                                       tBgp4PeerEntry   *pPeerInfo);
INT1 ExtCommGetInputFilterStatus (const tRouteProfile *pFeasibleRoute,
                                  UINT2         u2ExtCommPathStartOffSet);
INT1 ExtCommGetOutputFilterStatus (const tRouteProfile *pBestRoute,
                                   UINT2         u2ExtCommPathStartOffSet);
INT4 ExtCommGetUpdExtCommAttrib (tRouteProfile *pRouteProfile,
                                 tBgp4Info *pAdvtBgpInfo,
                                 tBgp4PeerEntry *pPeerEntry,
                                 tBgp4Info *pRtInfo);
INT4 ExtCommFormRouteExtCommPath (tRouteProfile *pRouteProfile,
                                  tBgp4Info * pAdvtBgpInfo,
                                  tBgp4PeerEntry *pPeerEntry,
                                  tBgp4Info *pRtInfo);
INT4 ExtCommUpdateRouteExtCommAdd (tRouteProfile *pRouteProfile,
                                   tBgp4Info *pAdvtBgpInfo);
INT1 ExtCommIsRouteExtCommDelete (tRouteProfile *pRouteProfile,
                                  UINT1 *au1ExtComm);
INT1 ExtCommGetExtCommSetStatus (tRouteProfile *pRouteProfile);
UINT2 FillCalcExtCommInUpdMesg (UINT1 *, tBgp4Info *);
UINT2 ExtCommAttrLength (tBgp4Info *);

#ifdef TEST
/* Proto Types used by Low-Level Routines */
INT4
ExtCommGetPeerLinkBandWidthEntry (UINT4 u4Fsbgp4PeerLinkRemAddr,
                                  tPeerLinkBandWidth ** ppPeerLinkBandWidth);
INT4
ExtCommGetPeerSendStatusEntry    (UINT4 u4Fsbgp4PeerLinkRemAddr,
                                  tPeerExtCommSendStatus **
                                  ppPeerExtCommSendStatus);
INT4
ExtCommCreatePeerLinkBandWidthEntry (UINT4 u4Fsbgp4PeerLinkRemAddr,
                                     tPeerLinkBandWidth ** ppPeerLinkBandWidth);
INT4
ExtCommGetRouteExtCommSetStatusEntry (UINT4 u4Fsbgp4ExtCommSetStatusIpNetwork,
                                      INT4 i4Fsbgp4ExtCommSetStatusIpPrefixLen,
                                      tRouteExtCommSetStatus **
                                      ppRouteExtCommSetStatus);
INT4                
ExtCommCreateRouteExtCommSetStatusEntry (
                            UINT4 u4Fsbgp4ExtCommSetStatusIpNetwork,
                            INT4 i4Fsbgp4ExtCommSetStatusIpPrefixLen,
                            tRouteExtCommSetStatus ** ppRouteExtCommSetStatus);
#endif /* TEST */
INT4
ExtCommValidateExtCommunityType (UINT2 u2Fsbgp4ExtCommInFilterCommVal);
INT4
ExtCommValidateExtCommunityVal (tSNMP_OCTET_STRING_TYPE *);

INT4 
ExtCommCreateInputFilterEntry (UINT1 *pu1Fsbgp4ExtCommInFilterCommVal,
                               tExtCommFilterInfo ** ppExtCommFilterInfo);
INT4 
ExtCommGetInputFilterEntry (UINT1 *pu1Fsbgp4ExtCommInFilterCommVal,
                            tExtCommFilterInfo ** ppExtCommFilterInfo);
INT4 
ExtCommCreateOutputFilterEntry (UINT1 *pu1Fsbgp4ExtCommOutFilterCommVal,
                                tExtCommFilterInfo ** ppExtCommFilterInfo);
INT4 
ExtCommGetOutputFilterEntry (UINT1 *pu1Fsbgp4ExtCommOutFilterCommVal,
                             tExtCommFilterInfo ** ppExtCommFilterInfo);

INT4 ExtCommRtTblCmpFunc (tBgp4RBElem *, tBgp4RBElem *);
INT4 ExtCommRtStatTblCmpFunc (tBgp4RBElem *, tBgp4RBElem *);
INT4 ExtCommFiltTblCmpFunc (tBgp4RBElem *, tBgp4RBElem *);
INT4 ExtCommPeerTblCmpFunc (tBgp4RBElem *, tBgp4RBElem *);

#endif /* End of _BGECM_H */
