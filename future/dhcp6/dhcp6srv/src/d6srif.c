/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description:  This file contains the Utility Procedures used by DHCPv6 
 *               server to access interface table.
 * ************************************************************************/

#include "d6srinc.h"
PRIVATE INT4 D6SrIfRBCmp PROTO ((tRBElem *, tRBElem *));
/***************************************************************************
 * FUNCTION NAME    : D6SrIfCreateTable
 *
 * DESCRIPTION      : This function creats the interface table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrIfCreateTable (VOID)
{
    DHCP6_SRV_IF_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDhcp6SrvIfInfo, GblRBLink),
                              D6SrIfRBCmp);
    if (DHCP6_SRV_IF_TABLE == NULL)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC, "D6SrIfCreateTable: "
                       " RB Tree Creation failed \r\n");
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrIfDeleteTable
 *
 * DESCRIPTION      : This function Deletes the interface table.
 *                    
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrIfDeleteTable (VOID)
{
    if (DHCP6_SRV_IF_TABLE != NULL)
    {
        D6SrIfDrainTable ();
        RBTreeDestroy (DHCP6_SRV_IF_TABLE, D6SrIfRBFree, 0);

    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrIfRBCmp
 *
 * DESCRIPTION      : This is the RB Tree compare function for the interface table. 
 *                    Indices of this table are -
 *                    o Interface Index
 *
 * INPUT            : pRBElem1 - Pointer to the node1 for comparision
 *                    pRBElem2 - Pointer to the node2 for comparision
 *
 * OUTPUT           : None
 *
 * RETURNS          : DHCP6_SRV_RB_EQUAL   - if all the keys matched for both
 *                                       the nodes
 *                    DHCP6_RB_LESS    - if node pRBElem1's key is less than
 *                                       node pRBElem2's key.
 *                    DHCP6_RB_GREATER - if node pRBElem1's key is greater
 *                                       than node pRBElem2's key.
 *
 **************************************************************************/
PRIVATE INT4
D6SrIfRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDhcp6SrvIfInfo    *pIfEntryA = NULL;
    tDhcp6SrvIfInfo    *pIfEntryB = NULL;
    pIfEntryA = (tDhcp6SrvIfInfo *) pRBElem1;
    pIfEntryB = (tDhcp6SrvIfInfo *) pRBElem2;
    if (pIfEntryA->u4IfIndex != pIfEntryB->u4IfIndex)
    {
        if (pIfEntryA->u4IfIndex < pIfEntryB->u4IfIndex)
        {
            return DHCP6_SRV_RB_LESS;
        }
        else
        {
            return DHCP6_SRV_RB_GREATER;
        }
    }
    return DHCP6_SRV_RB_EQUAL;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrIfCreateNode 
 *
 * DESCRIPTION      : This function creates a interface table node (allocate
 *                    memory needed by the node), and initialize it with
 *                    default values.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6IfInfo * - Pointer to the created interface node. 
 *                    NULL will be returned if node creation failed.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvIfInfo *
D6SrIfCreateNode (VOID)
{
    tDhcp6SrvIfInfo    *pIfInfo = NULL;
    DHCP6_SRV_ALLOC_MEM_IF (pIfInfo);
    if (pIfInfo == NULL)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC, "D6SrIfCreateNode: "
                       " Unable to allocate memory \r\n");
        return NULL;
    }
    MEMSET (pIfInfo, 0x00, sizeof (tDhcp6SrvIfInfo));
    return pIfInfo;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrIfAddNode
 *
 * DESCRIPTION      : This function add a node to the interface table, 
 *                    and takes care of updating all the required 
 *                    datastructure links.
 *
 * INPUT            : pIfInfo - pointer to interface information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrIfAddNode (tDhcp6SrvIfInfo * pIfInfo)
{
    if (RBTreeAdd (DHCP6_SRV_IF_TABLE, pIfInfo) != RB_SUCCESS)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC, "D6SrIfAddNode: "
                       " RB addtion failure \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrIfDelNode
 *
 * DESCRIPTION      : This function delete a interface node from the
 *                    interface table and release the memory used by that
 *                    node to the memory interface. It also takes care of
 *                    updating all the associated datastructure links before
 *                    deleting the node.
 *
 * INPUT            : pIfInfo - pointer to interface information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrIfDelNode (tDhcp6SrvIfInfo * pIfInfo)
{
    RBTreeRem (DHCP6_SRV_IF_TABLE, pIfInfo);
    DHCP6_SRV_FREE_MEM_IF (pIfInfo);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrIfRBFree
 *
 * DESCRIPTION      : This function is used to clean all the links
 *                    related to a interface node, and then release the
 *                    memory used by the node to the memory interface.
 *
 * INPUT            : pRBElem - pointer to the interface node whose memory needs
 *                              to be freed.
 *                    u4Arg   - Not used.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrIfRBFree (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    DHCP6_SRV_FREE_MEM_IF (pRBElem);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrIfDrainTable
 *
 * DESCRIPTION      : This function deletes all the interface nodes
 *                    associated with the interface table, and release the
 *                    memory used by them. It also takes care of updating
 *                    all the associated datastructure links.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrIfDrainTable (VOID)
{
    RBTreeDrain (DHCP6_SRV_IF_TABLE, D6SrIfRBFree, 0);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrIfGetFirstNode
 *
 * DESCRIPTION      : This function returns the first interface node
 *                    from the interface table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvIfInfo * - pointer to the first interface info. 
 *                    NULL will be returned if there is no node present 
 *                    in this table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvIfInfo *
D6SrIfGetFirstNode (VOID)
{
    return RBTreeGetFirst (DHCP6_SRV_IF_TABLE);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrIfGetNextNode
 *
 * DESCRIPTION      : This function returns the next interface info
 *                    from the interface table.
 *
 * INPUT            : pCurrentIfInfo - pointer to the interface info structure
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvIfInfo * - Pointer to the next interface info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvIfInfo *
D6SrIfGetNextNode (tDhcp6SrvIfInfo * pCurrentIfInfo)
{
    return RBTreeGetNext (DHCP6_SRV_IF_TABLE, pCurrentIfInfo, NULL);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrIfGetNode
 *
 * DESCRIPTION      : This function helps to find a interface node from the 
 *                    interface table.
 *
 * INPUT            : u4IfIndex - Interface Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvIfInfo * - Pointer to the next interface info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvIfInfo *
D6SrIfGetNode (UINT4 u4IfIndex)
{
    tDhcp6SrvIfInfo     Key;
    Key.u4IfIndex = u4IfIndex;
    return RBTreeGet (DHCP6_SRV_IF_TABLE, &Key);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrIfGetFirstOperUpIndex
 *
 * DESCRIPTION      : This function helps to find Oper up interface index from  
 *                    the interface table.
 *
 * INPUT            : u4FirstIfIndex - Interface Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE.
 *
 **************************************************************************/
PUBLIC INT4
D6SrIfGetFirstOperUpIndex (UINT4 *pu4FirstIfIndex)
{
    tDhcp6SrvIfInfo    *pSrvInterfaceInfo = NULL;

    pSrvInterfaceInfo = D6SrIfGetFirstNode ();

    while (pSrvInterfaceInfo != NULL)
    {
        if (pSrvInterfaceInfo->u1OperStatus == DHCP6_SRV_OPER_UP)
        {
            *pu4FirstIfIndex = pSrvInterfaceInfo->u4IfIndex;
            return OSIX_SUCCESS;
        }
        pSrvInterfaceInfo = D6SrIfGetNextNode (pSrvInterfaceInfo);
    }
    return OSIX_FAILURE;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file d6srif.c                        */
/*-----------------------------------------------------------------------*/
