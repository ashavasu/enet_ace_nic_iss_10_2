/* $Id: d6srtrap.c,v 1.12 2015/02/17 12:40:28 siva Exp $*/
/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains snmp trap specific procedures.
 * ************************************************************************/

#include   "d6srinc.h"
#include   "d6srtrap.h"
#include   "fsdh6s.h"

PRIVATE INT4        Dhcp6SParseSubIdNew (INT1 **ppi1TempPtr, UINT4 *pu4Value);
tSNMP_OID_TYPE     *D6SrTrapMakeObjIdFromDotNew (INT1 *pi1TextStr);
/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrTrapMakeObjIdFromDotNew                                  */
/*                                                                           */
/* Description  : This function retuns the OID  of the given string for      */
/*                proprietary MIB.number.number..format.                     */
/*                                                                           */
/* Input        : pi1TextStr  - pointer to the string.                       */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : pOidPtr or NULL                                            */
/*                                                                           */
/*****************************************************************************/
tSNMP_OID_TYPE     *
D6SrTrapMakeObjIdFromDotNew (INT1 *pi1TextStr)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
    UINT2               u2Index = 0;
    UINT2               u2DotCount;
    static INT1         ai1TempBuffer[DHCP6_SRV_MAX_OBJ_LEN + 1];
    INT1               *pi1TempPtr = NULL;
    INT1               *pi1DotPtr = NULL;
    UINT4               u4Len = 0;

    /* see if there is an alpha descriptor at begining */
    if (isalpha (*pi1TextStr) != OSIX_FALSE)
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pi1TempPtr = pi1TextStr;

        for (u2Index = 0;
             ((pi1TempPtr < pi1DotPtr) && (u2Index < DHCP6_SRV_MAX_OBJ_LEN));
             u2Index++)
        {
            ai1TempBuffer[u2Index] = *pi1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';

        for (u2Index = 0;
             (u2Index < (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID))
              && (orig_mib_oid_table[u2Index].pName != NULL)); u2Index++)
        {
            if ((STRCMP
                 (orig_mib_oid_table[u2Index].pName,
                  (INT1 *) ai1TempBuffer) == 0)
                && (STRLEN ((INT1 *) ai1TempBuffer) ==
                    STRLEN (orig_mib_oid_table[u2Index].pName)))
            {
                u4Len =
                    ((STRLEN (orig_mib_oid_table[u2Index].pNumber) <
                      sizeof (ai1TempBuffer)) ?
                     STRLEN (orig_mib_oid_table[u2Index].
                             pNumber) : sizeof (ai1TempBuffer) - 1);
                STRNCPY ((INT1 *) ai1TempBuffer,
                        orig_mib_oid_table[u2Index].pNumber, u4Len);
                ai1TempBuffer[u4Len] = '\0';
                break;
            }
        }

        if (u2Index < (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))
        {
            return (NULL);
        }
        /* now concatenate the non-alpha part to the begining */
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                    STRLEN ((INT1 *) pi1DotPtr));
    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        STRNCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr, STRLEN((INT1 *) pi1TextStr));
	ai1TempBuffer[STRLEN((INT1 *) pi1TextStr)] = '\0';
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = 0;
    for (u2Index = 0;
         ((u2Index < DHCP6_SRV_MAX_OBJ_LEN)
          && (ai1TempBuffer[u2Index] != '\0')); u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }

    /* The object specified may be either Tabular or Scalar
     * Object. Tabular Objects needs index to be added at the
     * end. So, allocating for Maximum OID Length
     * */
    if ((pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH)) == NULL)
    {
        return (NULL);
    }
    pOidPtr->u4_Length = u2DotCount + 1;

    /* now we convert number.number.... strings */
    pi1TempPtr = ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {

        if ((Dhcp6SParseSubIdNew
             ((INT1 **) (&(pi1TempPtr)),
              &(pOidPtr->pu4_OidList[u2Index]))) == OSIX_FAILURE)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pi1TempPtr == '.')
        {
            pi1TempPtr++;        /* to skip over dot */
        }
        else if (*pi1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SParseSubIdNew                                        */
/*                                                                           */
/* Description  : This function Parse the string format in                   */
/*                number.number..format.                                     */
/*                                                                           */
/* Input        : ppi1TempPtr - pointer to the string.                       */
/*                pu4Value    - Pointer the OID List value.                  */
/*                                                                           */
/* Output       : ppu1TempPtr - value of ppu1TempPtr                         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
Dhcp6SParseSubIdNew (INT1 **ppi1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    INT1               *pi1Tmp = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    for (pi1Tmp = *ppi1TempPtr; (((*pi1Tmp >= '0') && (*pi1Tmp <= '9')) ||
                                 ((*pi1Tmp >= 'a') && (*pi1Tmp <= 'f')) ||
                                 ((*pi1Tmp >= 'A') && (*pi1Tmp <= 'F')));
         pi1Tmp++)
    {
        u4Value = (u4Value * DHCP6_SRV_TRAPS_OID_LEN) +
            (*pi1Tmp & DHCP6_SRV_4BIT_MAX);
    }

    if (*ppi1TempPtr == pi1Tmp)
    {
        return OSIX_FAILURE;
    }

    *ppi1TempPtr = pi1Tmp;

    *pu4Value = u4Value;

    return (i4RetVal);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrTrapSnmpSendTrap                                         */
/*                                                                           */
/* Description  : This function will prepare the TRAP PDU and send it to the */
/*                administrator if particular trap is enabled                */
/*                                                                           */
/* Input        : pTrapInfo   - Pointer  to the information required for TRAP*/
/*                u1TrapId    - TrapIdentifier                               */
/*                u4InterfaceId -Interface Inndex                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
D6SrTrapSnmpSendTrap (VOID *pTrapInfo, UINT1 u1TrapId)
{
#ifdef SNMP_3_WANTED
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCounter64Type;
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    UINT4               u4InterfaceId;
    UINT1               au1Buf[DHCP6_SRV_MAX_OBJ_LEN];

    SnmpCounter64Type.msn = 0;
    SnmpCounter64Type.lsn = 0;

    /*Check client functionality is available on the Interface */
    switch (u1TrapId)
    {
        case DHCP6_SRV_INVALID_MSG_TRAP:
            u4InterfaceId = *((UINT4 *) pTrapInfo);
            if (DHCP6_SRV_IS_SYSLOG_ENABLED ())
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, DHCP6_SRV_SYSLOG_ID,
                              "interface-id: %d invalid message received ",
                              u4InterfaceId));
            }
            if (DHCP6_SRV_RCVD_INVALID_MSG_TRAP_DISABLED () == OSIX_TRUE)
            {
                return;
            }
            break;
        case DHCP6_SRV_HMAC_AUTH_FAIL_TRAP:
            u4InterfaceId = *((UINT4 *) pTrapInfo);
            if (DHCP6_SRV_IS_SYSLOG_ENABLED ())
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, DHCP6_SRV_SYSLOG_ID,
                              "interface-id: %d authenication failed for received message ",
                              u4InterfaceId));
            }
            if (DHCP6_SRV_HMAC_AUTH_FAIL_TRAP_DISABLED () == OSIX_TRUE)
            {
                return;
            }
            break;

        case DHCP6_SRV_UNKNOW_TLV_TRAP:
            u4InterfaceId = *((UINT4 *) pTrapInfo);
            if (DHCP6_SRV_IS_SYSLOG_ENABLED ())
            {
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, DHCP6_SRV_SYSLOG_ID,
                              "interface-id: %d unknown option received in message ",
                              u4InterfaceId));
            }
            if (DHCP6_SRV_UNKNOWN_TLV_TRAP_DISABLED () == OSIX_TRUE)
            {
                return;
            }
            break;

        default:
            return;
    }

    MEMSET (au1Buf, 0, (DHCP6_SRV_MAX_OBJ_LEN * sizeof (UINT1)));

    /* ---------------------- TRAP PDU DHCP6_SRV MIB trap ----------------------- *
     * ------------------ DHCP6_SRV_INVALID_MSG_TRAP ------------------- *
     * ------ Enterprise_OID|fsDhcp6SrvIfInvalidPktIn_Oid|InterfaceId|1 ------ *
     * ----------------------------------------------------------------------- */

    /* Set Entrprise OID */
    pEnterpriseOid = SNMP_AGT_GetOidFromString ((INT1 *) DHCP6_SRV_TRAPS_OID);

    if (pEnterpriseOid == NULL)
    {
        return;
    }

    /*  Set Trap type */
    u4GenTrapType = ENTERPRISE_SPECIFIC;
    u4SpecTrapType = u1TrapId;

    if (u1TrapId == DHCP6_SRV_INVALID_MSG_TRAP)
    {
        SPRINTF ((char *) au1Buf, DHCP6_SRV_MIB_OBJ_INVALID_MSG_TRAP);

        pOid = D6SrTrapMakeObjIdFromDotNew ((INT1 *) au1Buf);

        if (pOid == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            return;
        }
        /* Append the indices for this object
         * Index1 --> Interface Index .
         * */
        if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
        {
            /* 1. Append the Port Number
             * */
            pOid->pu4_OidList[pOid->u4_Length] = u4InterfaceId;
            pOid->u4_Length++;
        }

        pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                        (INT4)
                                        DHCP6_SRV_INVALID_MSG_TRAP,
                                        NULL, NULL, SnmpCounter64Type);
        if (pVbList == NULL)
        {
            SNMP_FreeOid (pOid);
            SNMP_FreeOid (pEnterpriseOid);
            return;
        }

        pStartVb = pVbList;
    }
    else if (u1TrapId == DHCP6_SRV_HMAC_AUTH_FAIL_TRAP)
    {
        SPRINTF ((char *) au1Buf, DHCP6_SRV_MIB_OBJ_AUTH_FAIL_TRAP);

        pOid = D6SrTrapMakeObjIdFromDotNew ((INT1 *) au1Buf);

        if (pOid == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            return;
        }
        /* Append the indices for this object
         * Index1 --> Interface Index .
         * */
        if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
        {
            /* 1. Append the Interface Index
             * */
            pOid->pu4_OidList[pOid->u4_Length] = u4InterfaceId;
            pOid->u4_Length++;
        }

        pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                        (INT4)
                                        DHCP6_SRV_HMAC_AUTH_FAIL_TRAP,
                                        NULL, NULL, SnmpCounter64Type);
        if (pVbList == NULL)
        {
            SNMP_FreeOid (pOid);
            SNMP_FreeOid (pEnterpriseOid);
            return;
        }

        pStartVb = pVbList;
    }
    else if (u1TrapId == DHCP6_SRV_UNKNOW_TLV_TRAP)
    {
        SPRINTF ((char *) au1Buf, DHCP6_SRV_MIB_OBJ_UNKNOW_TLV_TRAP);

        pOid = D6SrTrapMakeObjIdFromDotNew ((INT1 *) au1Buf);

        if (pOid == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            return;
        }
        /* Append the indices for this object
         * Index1 --> Interface Index .
         * */
        if (pOid->u4_Length < (SNMP_MAX_OID_LENGTH - 1))
        {
            /* 1. Append the Interface Index
             * */
            pOid->pu4_OidList[pOid->u4_Length] = u4InterfaceId;
            pOid->u4_Length++;
        }

        pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                                        (INT4)
                                        DHCP6_SRV_UNKNOW_TLV_TRAP,
                                        NULL, NULL, SnmpCounter64Type);
        if (pVbList == NULL)
        {
            SNMP_FreeOid (pOid);
            SNMP_FreeOid (pEnterpriseOid);
            return;
        }

        pStartVb = pVbList;

    }
    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                              pStartVb);
#else
    UNUSED_PARAM (pTrapInfo);
    UNUSED_PARAM (u1TrapId);
#endif
}
