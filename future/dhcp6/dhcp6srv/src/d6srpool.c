/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description:  This file contains the Utility Procedures used by DHCPv6 
 *               server to access pool table.
 * ************************************************************************/

#include "d6srinc.h"

PRIVATE INT4 D6SrPoolRBCmp PROTO ((tRBElem *, tRBElem *));

/***************************************************************************
 * FUNCTION NAME    : D6SrPoolCreateTable
 *
 * DESCRIPTION      : This function creats the pool table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrPoolCreateTable (VOID)
{
    DHCP6_SRV_POOL_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDhcp6SrvPoolInfo, GblRBLink),
                              D6SrPoolRBCmp);
    if (DHCP6_SRV_POOL_TABLE == NULL)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       "D6SrPoolCreateTable: " " RB Tree Creation failed \r\n");
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrPoolDeleteTable
 *
 * DESCRIPTION      : This function Deletes the pool table.
 *                    
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrPoolDeleteTable (VOID)
{
    if (DHCP6_SRV_POOL_TABLE != NULL)
    {
        D6SrPoolDrainTable ();
        RBTreeDestroy (DHCP6_SRV_POOL_TABLE, NULL, 0);

    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrPoolRBCmp
 *
 * DESCRIPTION      : This is the RB Tree compare function for the pool table. 
 *                    Indices of this table are -
 *                    o Pool Index
 *
 * INPUT            : pRBElem1 - Pointer to the node1 for comparision
 *                    pRBElem2 - Pointer to the node2 for comparision
 *
 * OUTPUT           : None
 *
 * RETURNS          : DHCP6_SRV_RB_EQUAL   - if all the keys matched for both
 *                                       the nodes
 *                    DHCP6_RB_LESS    - if node pRBElem1's key is less than
 *                                       node pRBElem2's key.
 *                    DHCP6_RB_GREATER - if node pRBElem1's key is greater
 *                                       than node pRBElem2's key.
 *
 **************************************************************************/
PRIVATE INT4
D6SrPoolRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDhcp6SrvPoolInfo  *pPoolEntryA = NULL;
    tDhcp6SrvPoolInfo  *pPoolEntryB = NULL;

    pPoolEntryA = (tDhcp6SrvPoolInfo *) pRBElem1;
    pPoolEntryB = (tDhcp6SrvPoolInfo *) pRBElem2;

    if (pPoolEntryA->u4PoolIndex != pPoolEntryB->u4PoolIndex)
    {
        if (pPoolEntryA->u4PoolIndex < pPoolEntryB->u4PoolIndex)
        {
            return DHCP6_SRV_RB_LESS;
        }
        else
        {
            return DHCP6_SRV_RB_GREATER;
        }
    }
    return DHCP6_SRV_RB_EQUAL;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrPoolCreateNode 
 *
 * DESCRIPTION      : This function creates a pool table node (allocate
 *                    memory needed by the node), and initialize it with
 *                    default values.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvPoolInfo * - Pointer to the created pool node. 
 *                    NULL will be returned if node creation failed.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvPoolInfo *
D6SrPoolCreateNode (VOID)
{
    tDhcp6SrvPoolInfo  *pPoolInfo = NULL;

    DHCP6_SRV_ALLOC_MEM_POOL (pPoolInfo);

    if (pPoolInfo == NULL)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC, "D6SrPoolCreateNode: "
                       " Unable to allocate memory \r\n");
        return NULL;
    }
    MEMSET (pPoolInfo, 0x00, sizeof (tDhcp6SrvPoolInfo));
    /* Set Default */
    pPoolInfo->u1DuidType = DHCP6_SRV_DUID_LLT;
    return pPoolInfo;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrPoolAddNode
 *
 * DESCRIPTION      : This function add a node to the pool table, 
 *                    and takes care of updating all the required 
 *                    datastructure links.
 *
 * INPUT            : pPoolInfo - pointer to pool information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrPoolAddNode (tDhcp6SrvPoolInfo * pPoolInfo)
{
    if (RBTreeAdd (DHCP6_SRV_POOL_TABLE, pPoolInfo) != RB_SUCCESS)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC, "D6SrPoolAddNode: "
                       " RB addtion failure \r\n");
        return OSIX_FAILURE;
    }

    TMO_SLL_Init (&(pPoolInfo->OptionList));

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrPoolDelNode
 *
 * DESCRIPTION      : This function delete a pool node from the
 *                    pool table and release the memory used by that
 *                    node to the memory pool. It also takes care of
 *                    updating all the associated datastructure links before
 *                    deleting the node.
 *
 * INPUT            : pPoolInfo - pointer to pool information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrPoolDelNode (tDhcp6SrvPoolInfo * pPoolInfo)
{
    RBTreeRem (DHCP6_SRV_POOL_TABLE, pPoolInfo);

    DHCP6_SRV_FREE_MEM_POOL (pPoolInfo);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrPoolRBFree
 *
 * DESCRIPTION      : This function is used to clean all the links
 *                    related to a pool node, and then release the
 *                    memory used by the node to the memory pool.
 *
 * INPUT            : pRBElem - pointer to the pool node whose memory needs
 *                              to be freed.
 *                    u4Arg   - Not used.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrPoolRBFree (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    DHCP6_SRV_FREE_MEM_POOL (pRBElem);

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrPoolDrainTable
 *
 * DESCRIPTION      : This function deletes all the pool nodes
 *                    associated with the pool table, and release the
 *                    memory used by them. It also takes care of updating
 *                    all the associated datastructure links.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrPoolDrainTable (VOID)
{
    RBTreeDrain (DHCP6_SRV_POOL_TABLE, D6SrPoolRBFree, 0);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrPoolGetFirstNode
 *
 * DESCRIPTION      : This function returns the first pool node
 *                    from the pool table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvPoolInfo * - pointer to the first pool info. 
 *                    NULL will be returned if there is no node present 
 *                    in this table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvPoolInfo *
D6SrPoolGetFirstNode (VOID)
{
    return RBTreeGetFirst (DHCP6_SRV_POOL_TABLE);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrPoolGetNextNode
 *
 * DESCRIPTION      : This function returns the next pool info
 *                    from the pool table.
 *
 * INPUT            : pCurrentPoolInfo - pointer to the pool info structure
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvPoolInfo * - Pointer to the next pool info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvPoolInfo *
D6SrPoolGetNextNode (tDhcp6SrvPoolInfo * pCurrentPoolInfo)
{
    return RBTreeGetNext (DHCP6_SRV_POOL_TABLE, pCurrentPoolInfo, NULL);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrPoolGetNode
 *
 * DESCRIPTION      : This function helps to find a pool node from the pool 
 *                    table.
 *
 * INPUT            : u4PoolIndex - Pool Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvPoolInfo * - Pointer to the next pool info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvPoolInfo *
D6SrPoolGetNode (UINT4 u4PoolIndex)
{
    tDhcp6SrvPoolInfo   Key;

    Key.u4PoolIndex = u4PoolIndex;

    return RBTreeGet (DHCP6_SRV_POOL_TABLE, &Key);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrPoolGetFreeIndex
 *
 * DESCRIPTION      : This function helps to find a free index which can be
 *                    used to create a new entry in the table.
 *
 * INPUT            : None
 *
 * OUTPUT           : pu4PoolIndex - Pointer to the pool index
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrPoolGetFreeIndex (UINT4 *pu4FreePoolIndex)
{
    tDhcp6SrvPoolInfo  *pPoolInfo = NULL;
    UINT4               u4PoolIndex = 0;
    UINT4               u4MaxPoolIndex = DHCP6_SRV_POOL_INDEX_MAX;

    for (u4PoolIndex = 1; u4PoolIndex <= u4MaxPoolIndex; u4PoolIndex++)
    {
        pPoolInfo = D6SrPoolGetNode (u4PoolIndex);

        if (pPoolInfo == NULL)
        {
            *pu4FreePoolIndex = u4PoolIndex;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrPoolOptionListAddNode
 *
 * DESCRIPTION      : This function add a option node to the pool table. 
 *
 * INPUT            : pOptionInfo - pointer to option information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrPoolOptionListAddNode (tDhcp6SrvOptionInfo * pOptionInfo)
{
    TMO_SLL_Init_Node (&(pOptionInfo->PoolSllLink));
    TMO_SLL_Add (&(pOptionInfo->pPool->OptionList),
                 &(pOptionInfo->PoolSllLink));
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrPoolOptionListDelNode
 *
 * DESCRIPTION      : This function delete a option node from the
 *                    pool table. 
 *
 * INPUT            : pOptionInfo - pointer to option information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrPoolOptionListDelNode (tDhcp6SrvOptionInfo * pOptionInfo)
{
    TMO_SLL_Delete (&(pOptionInfo->pPool->OptionList),
                    &(pOptionInfo->PoolSllLink));
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrPoolOptionListGetFirstNode
 *
 * DESCRIPTION      : This function gets the first node in the pool option
 *                    list.
 *
 * INPUT            : pPoolInfo - pointer to pool information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : Pointer to the first node in the list
 *
 **************************************************************************/
tDhcp6SrvOptionInfo *
D6SrPoolOptionListGetFirstNode (tDhcp6SrvPoolInfo * pPoolInfo)
{
    return (tDhcp6SrvOptionInfo *) TMO_SLL_First (&(pPoolInfo->OptionList));
}

/***************************************************************************
 * FUNCTION NAME    : D6SrPoolOptionListGetNextNode
 *
 * DESCRIPTION      : This function gets the next node in the pool option
 *                    list.
 *
 * INPUT            : pPoolInfo - pointer to pool information node
 *                    pOptionInfo - Option Info
 *
 * OUTPUT           : None
 *
 * RETURNS          : Pointer to the next node in the list
 *
 **************************************************************************/
tDhcp6SrvOptionInfo *
D6SrPoolOptionListGetNextNode (tDhcp6SrvOptionInfo * pOptionInfo,
                               tDhcp6SrvPoolInfo * pPoolInfo)
{
    return (tDhcp6SrvOptionInfo *) TMO_SLL_Next (&(pPoolInfo->OptionList),
                                                 &(pOptionInfo->PoolSllLink));
}

/*-----------------------------------------------------------------------*/
/*                       End of the file d6srpool.c                      */
/*-----------------------------------------------------------------------*/
