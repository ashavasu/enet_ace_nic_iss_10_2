/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: d6srcli.c,v 1.32 2017/11/30 14:00:41 siva Exp $
 * Description: This file contains the CLI SET/GET/TEST and GET NEXT
 *              routines for the MIB objects specified in fsdhc6s.mib
 *
 **************************************************************************/
#ifndef _DHCP6_SRV_CLI_C
#define _DHCP6_SRV_CLI_C
#include "d6srinc.h"
#include "fsdh6scli.h"
PRIVATE VOID D6SrCliPrintOption PROTO ((tCliHandle, UINT1 *, INT4));
PRIVATE VOID Dhcp6SCliPrintIpAdd PROTO ((tCliHandle, UINT1 *));
PRIVATE VOID D6SrCliPrintString PROTO ((tCliHandle, UINT1 *, INT4));
PRIVATE VOID D6SrCliPrintValue PROTO ((tCliHandle, UINT1 *, INT4));
PRIVATE VOID D6SrPrintOptionType PROTO ((tCliHandle, INT4));
PRIVATE INT4 D6SrValidateOptionFormatType PROTO ((INT4, UINT4));
VOID D6SrCliShowConfigSubOptTable PROTO ((tCliHandle, UINT4, UINT4));
PRIVATE VOID D6SrPrintSubOptionType PROTO ((tCliHandle, UINT4));
PRIVATE VOID D6SrCliPrintSubOptTable PROTO ((tCliHandle, UINT4, UINT4));

/*****************************************************************************/
/*                                                                           */
/* Function     : cli_process_dhcp6s_cmd                                     */
/*                                                                           */
/* Description  : Protocol CLI Message Handler Function to handle            */
/*                configuration messages only                                */
/*                                                                           */
/* Input        : CliHandle - CLI Handle Value                               */
/*                u4Command - Command identifier                             */
/*                ...       - Variable command argument list                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
cli_process_dhcp6s_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4               u4ErrorCode = 0;
    UINT4               u4InterfaceIndex = 0;
    UINT4               u4PoolIndex = 0;
    UINT4               u4OptionIndex = 0;
    UINT4               u4CmdType = 0;
    UINT4               u4Index = 0;
    UINT4               u4ClientType = 0;
    UINT4              *au4args[DHCP6_SRV_CLI_MAX_ARGUMENTS];
    INT4                i4IfIndex = 0;
    INT1                i1argno = 0;
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4Preference = 0;
    UINT4               u4ClientIndex = 0;
    UINT4               u4Type = 0;
    UINT4               u4Days = 0;
    UINT4               u4Hours = 0;
    UINT4               u4Mins = 0;
    UINT4               u4FormatType = 0;
    UINT4               u4OptType = 0;
    UINT1               u1Preference[2] = { 0 };
    tIp6Addr            Ipv6Addr;

    CliRegisterLock (CliHandle, D6SrMainLock, D6SrMainUnLock);

    if (D6SrMainLock () == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "%%DHCP6 Server Locking Failed - Command not Executed\r\n");
        return CLI_FAILURE;
    }
    MEMSET (&Ipv6Addr, 0, sizeof (tIp6Addr));

    va_start (ap, u4Command);

    /* Second argument is always Interface Name/Index */
    u4InterfaceIndex = va_arg (ap, UINT4);

    /* Walk through the rest of the arguments and store in au4args array.
     * Store DHCP6_SRV_CLI_MAX_ARGUMENTS arguements at the max. */

    while (1)
    {
        au4args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == DHCP6_SRV_CLI_MAX_ARGUMENTS)
        {
            break;
        }
    }

    va_end (ap);

    switch (u4Command)
    {
        case CLI_DHCP6_SRV_POOL:
        {
            /*au4args[0]-> Pool Index */

            i4RetVal = D6SrCliCreateAddressPool (CliHandle,
                                                 (UINT1 *) au4args[0]);
            break;
        }
        case CLI_DHCP6_SRV_NO_POOL:
        {
            /*au4args[0]-> Pool Index */

            i4RetVal = D6SrCliDestroyAddressPool (CliHandle,
                                                  (UINT1 *) au4args[0]);
            break;
        }
        case CLI_DHCP6_SRV_AUTH_REALM_KEY_ID:
        {
            /*au4args[0]-> Realm  */
            /*au4args[1]-> Key    */
            u4ClientIndex = CLI_GET_SRV_CLNT_INDEX ();
            i4RetVal = D6SrCliSetAuthInfo (CliHandle,
                                           (UINT1 *) au4args[0],
                                           (UINT1 *) au4args[1], u4ClientIndex);
            break;
        }
        case CLI_DHCP6_SRV_NO_AUTH_REALM_KEY_ID:
        {
            /*au4args[0]-> Realm  */
            /*au4args[1]-> Key    */

            u4ClientIndex = CLI_GET_SRV_CLNT_INDEX ();
            i4RetVal = D6SrCliDeleteAuthInfo (CliHandle,
                                              (UINT1 *) au4args[0],
                                              (UINT1 *) au4args[1],
                                              u4ClientIndex);
            break;
        }
        case CLI_DHCP6_SRV_AUTH_CLNT_ID:
            /*au4args[0]-> Client ID  */
            /*au4args[1]-> Client ID Type */

            u4ClientType = CLI_PTR_TO_U4 (au4args[1]);

            i4RetVal = D6SrCliSetClntInfo (CliHandle,
                                           (UINT1 *) au4args[0], u4ClientType);
            break;
        case CLI_DHCP6_SRV_NO_AUTH_CLNT_ID:
            /*au4args[0]-> Client ID  */

            i4RetVal = D6SrCliDelClntInfo (CliHandle, (UINT1 *) au4args[0]);
            break;

        case CLI_DHCP6_SRV_ENABLE_TRAP:
        {
            /*au4args[0]-> Trap Value   */
            i4RetVal = D6SrCliSetTraps (CliHandle,
                                        CLI_PTR_TO_U4 (au4args[0]), CLI_ENABLE);
            break;
        }
        case CLI_DHCP6_SRV_DISABLE_TRAP:
        {
            /*au4args[0]-> Trap Value   */
            i4RetVal = D6SrCliSetTraps (CliHandle,
                                        CLI_PTR_TO_U4 (au4args[0]),
                                        CLI_DISABLE);
            break;
        }
        case CLI_DHCP6_SRV_VENDOR:
        {
            /*au4args[0]-> vendor-id */

            u4PoolIndex = CLI_GET_POOL_INDEX ();

            i4RetVal = D6SrCliCreateVendor (CliHandle,
                                            *(au4args[0]),
                                            (UINT1 *) (au4args[0]),
                                            STRLEN (au4args[0]), u4PoolIndex);
            break;
        }
        case CLI_DHCP6_SRV_NO_VENDOR:
        {
            /*au4args[0]-> vendor-id */

            u4PoolIndex = CLI_GET_POOL_INDEX ();

            i4RetVal =
                D6SrCliProcessDeleteOption (CliHandle, u4PoolIndex,
                                            DHCP6_SRV_VENDOR_SPECIFIC_OPTION,
                                            (UINT1 *) (au4args[0]),
                                            DHCP6_SRV_VENDOR_LEN);
            break;
        }
        case CLI_DHCP6_SRV_OPTION:
        {
            /*au4args[0]-> Sub Option - code */
            /*au4args[1]-> Sub Option - type */
            /*au4args[2]-> Sub Option - value */

            u4PoolIndex = CLI_GET_POOL_INDEX ();

            u4OptionIndex = CLI_GET_VENDOR_INDEX ();

            /*validate the cmd type */
            u4FormatType = CLI_PTR_TO_U4 (au4args[1]);

            if (D6SrValidateOptionFormatType (*(au4args[0]), u4FormatType) ==
                CLI_FAILURE)
            {
                CliPrintf (CliHandle, " invalid format type.\r\n");
                break;
            }

            i4RetVal =
                D6SrCliProcessAddSubOption (CliHandle, u4PoolIndex,
                                            u4OptionIndex,
                                            *au4args[0],
                                            (UINT1 *) (au4args[2]),
                                            STRLEN (au4args[2]));
            break;
        }
        case CLI_DHCP6_SRV_NO_OPTION:
        {
            /*au4args[0]-> Sub Option - id */
            /*au4args[1]-> Sub Option - type */
            /*au4args[2]-> Sub Option - value */

            u4PoolIndex = CLI_GET_POOL_INDEX ();

            u4OptionIndex = CLI_GET_VENDOR_INDEX ();

            /*validate the cmd type */
            u4FormatType = CLI_PTR_TO_U4 (au4args[1]);

            if (D6SrValidateOptionFormatType (*(au4args[0]), u4FormatType) ==
                CLI_FAILURE)
            {
                CliPrintf (CliHandle, " invalid format type.\r\n");
                break;
            }
            i4RetVal =
                D6SrCliProcessDeleteSubOption (CliHandle,
                                               u4PoolIndex,
                                               u4OptionIndex,
                                               *au4args[0],
                                               (UINT1 *) (au4args[2]),
                                               STRLEN (au4args[2]));
            break;
        }
        case CLI_DHCP6_SRV_LINK_ADDR:
        {
            /*au4args[0]-> link-address */

            u4PoolIndex = CLI_GET_POOL_INDEX ();
            INET_ATON6 (au4args[0], &Ipv6Addr);
            i4RetVal =
                D6SrCliProcessLinkAddOption (CliHandle, u4PoolIndex,
                                             Ipv6Addr.u1_addr,
                                             DHCP6_SRV_IPV6_ADDRESS_SIZE);
            break;
        }
        case CLI_DHCP6_SRV_NO_LINK_ADDR:
        {
            /*au4args[0]-> link-address */

            u4PoolIndex = CLI_GET_POOL_INDEX ();
            INET_ATON6 (au4args[0], &Ipv6Addr);

            i4RetVal =
                D6SrCliProcessLinkDeleteOption (CliHandle, u4PoolIndex,
                                                Ipv6Addr.u1_addr,
                                                DHCP6_SRV_IPV6_ADDRESS_SIZE);
            break;
        }
        case CLI_DHCP6_SRV_DOMAIN_NAME:
        {
            /*au4args[0]-> domain-name */
            /*au4args[1]-> preference value  */

            u4PoolIndex = CLI_GET_POOL_INDEX ();

            if ((au4args[1]) != NULL)
            {
                i4Preference = *(INT4 *) (au4args[1]);
            }
            else
            {
                i4Preference = 1;
            }
            i4RetVal =
                D6SrCliProcessAddOption (CliHandle, u4PoolIndex,
                                         DHCP6_SRV_DOMAIN_NAME_OPTION,
                                         (UINT1 *) (au4args[0]),
                                         STRLEN (au4args[0]), i4Preference);
            break;
        }
        case CLI_DHCP6_SRV_NO_DOMAIN_NAME:
        {
            /*au4args[0]-> domain-name */

            u4PoolIndex = CLI_GET_POOL_INDEX ();

            i4RetVal =
                D6SrCliProcessDeleteOption (CliHandle, u4PoolIndex,
                                            DHCP6_SRV_DOMAIN_NAME_OPTION,
                                            (UINT1 *) (au4args[0]),
                                            STRLEN (au4args[0]));
            break;
        }
        case CLI_DHCP6_SRV_DNS_SERVER:
        {
            /*au4args[0]-> DNS Server option */

            u4PoolIndex = CLI_GET_POOL_INDEX ();

            i4Preference = 0;
            INET_ATON6 (au4args[0], &Ipv6Addr);
            i4RetVal =
                D6SrCliProcessAddOption (CliHandle, u4PoolIndex,
                                         DHCP6_SRV_DNS_RECURSIVE_OPTION,
                                         Ipv6Addr.u1_addr,
                                         DHCP6_SRV_IPV6_ADDRESS_SIZE,
                                         i4Preference);
            break;
        }
        case CLI_DHCP6_SRV_NO_DNS_SERVER:
        {
            /*au4args[0]-> DNS Server option */

            u4PoolIndex = CLI_GET_POOL_INDEX ();
            INET_ATON6 (au4args[0], &Ipv6Addr);

            i4RetVal = D6SrCliProcessDeleteOption (CliHandle, u4PoolIndex,
                                                   DHCP6_SRV_DNS_RECURSIVE_OPTION,
                                                   Ipv6Addr.u1_addr,
                                                   DHCP6_SRV_IPV6_ADDRESS_SIZE);
            break;
        }
        case CLI_DHCP6_SRV_SIP_ADDR:
        {
            /*au4args[0]-> sip address */
            /*au4args[1]-> preference value  */

            u4PoolIndex = CLI_GET_POOL_INDEX ();

            if ((au4args[1]) != NULL)
            {
                i4Preference = *(INT4 *) (au4args[1]);
            }
            else
            {
                i4Preference = 1;
            }
            INET_ATON6 (au4args[0], &Ipv6Addr);
            i4RetVal = D6SrCliProcessAddOption (CliHandle, u4PoolIndex,
                                                DHCP6_SRV_SIP_IPV6_ADDR_OPTION,
                                                Ipv6Addr.u1_addr,
                                                DHCP6_SRV_IPV6_ADDRESS_SIZE,
                                                i4Preference);
            break;
        }
        case CLI_DHCP6_SRV_NO_SIP_ADDR:
        {
            /*au4args[0]-> sip address */

            u4PoolIndex = CLI_GET_POOL_INDEX ();
            INET_ATON6 (au4args[0], &Ipv6Addr);
            i4RetVal =
                D6SrCliProcessDeleteOption (CliHandle, u4PoolIndex,
                                            DHCP6_SRV_SIP_IPV6_ADDR_OPTION,
                                            Ipv6Addr.u1_addr,
                                            DHCP6_SRV_IPV6_ADDRESS_SIZE);
            break;
        }
        case CLI_DHCP6_SRV_SIP_DOMAIN:
        {
            /*au4args[0]-> sip domain */

            u4PoolIndex = CLI_GET_POOL_INDEX ();

            i4Preference = 1;

            i4RetVal = D6SrCliProcessAddOption (CliHandle, u4PoolIndex,
                                                DHCP6_SRV_SIP_SERVER_DOMAIN_OPTION,
                                                (UINT1 *) (au4args[0]),
                                                STRLEN (au4args[0]),
                                                i4Preference);
            break;
        }
        case CLI_DHCP6_SRV_SIP_NO_DOMAIN:
        {
            /*au4args[0]-> sip domain */

            u4PoolIndex = CLI_GET_POOL_INDEX ();

            i4RetVal =
                D6SrCliProcessDeleteOption (CliHandle, u4PoolIndex,
                                            DHCP6_SRV_SIP_SERVER_DOMAIN_OPTION,
                                            (UINT1 *) (au4args[0]),
                                            STRLEN (au4args[0]));
            break;
        }
        case CLI_DHCP6_SRV_POOL_OPTION:
        {
            /* au4args[0] -  Option Type
             * au4args[2] -  Option Value
             */
            u4PoolIndex = CLI_GET_POOL_INDEX ();

            /*validate the cmd type */
            u4FormatType = CLI_PTR_TO_U4 (au4args[1]);
            u4OptType = *au4args[0];
            if ((u4OptType == DHCP6_SRV_VENDOR_SPECIFIC_OPTION) ||
                (u4OptType == DHCP6_SRV_DNS_RECURSIVE_OPTION) ||
                (u4OptType == DHCP6_SRV_DOMAIN_NAME_OPTION) ||
                (u4OptType == DHCP6_SRV_SIP_SERVER_DOMAIN_OPTION) ||
                (u4OptType == DHCP6_SRV_REFRESH_TIMER_TYPE) ||
                (u4OptType == DHCP6_SRV_SIP_IPV6_ADDR_OPTION))
            {
                CliPrintf (CliHandle,
                           " invalid CLI command for input option type.\r\n");
                break;
            }

            if (D6SrValidateOptionFormatType (*(au4args[0]), u4FormatType) ==
                CLI_FAILURE)
            {
                CliPrintf (CliHandle, " invalid format type.\r\n");
                break;
            }
            i4Preference = 1;

            i4RetVal = D6SrCliProcessAddOption (CliHandle, u4PoolIndex,
                                                *(au4args[0]),
                                                (UINT1 *) (au4args[2]),
                                                STRLEN (au4args[2]),
                                                i4Preference);
            break;
        }
        case CLI_DHCP6_SRV_POOL_NO_OPTION:
        {
            /* au4args[0] -  Option Type
             * au4args[2] -  Option Value
             */

            u4PoolIndex = CLI_GET_POOL_INDEX ();

            /*validate the cmd type */
            u4FormatType = CLI_PTR_TO_U4 (au4args[1]);

            u4OptType = *au4args[0];
            if ((u4OptType == DHCP6_SRV_VENDOR_SPECIFIC_OPTION) ||
                (u4OptType == DHCP6_SRV_DNS_RECURSIVE_OPTION) ||
                (u4OptType == DHCP6_SRV_DOMAIN_NAME_OPTION) ||
                (u4OptType == DHCP6_SRV_SIP_SERVER_DOMAIN_OPTION) ||
                (u4OptType == DHCP6_SRV_REFRESH_TIMER_TYPE) ||
                (u4OptType == DHCP6_SRV_SIP_IPV6_ADDR_OPTION))
            {
                CliPrintf (CliHandle,
                           " invalid CLI command for input option type.\r\n");
                break;
            }
            if (D6SrValidateOptionFormatType (*(au4args[0]), u4FormatType) ==
                CLI_FAILURE)
            {
                CliPrintf (CliHandle, " invalid format type.!\r\n");
                break;
            }
            i4RetVal =
                D6SrCliProcessDeleteOption (CliHandle, u4PoolIndex,
                                            *(au4args[0]),
                                            (UINT1 *) (au4args[2]),
                                            STRLEN (au4args[2]));
        }
        case CLI_DHCP6_SRV_TYPE:
        {
            /*au4args[0]-> type of command  */

            u4PoolIndex = CLI_GET_POOL_INDEX ();

            u4CmdType = CLI_PTR_TO_U4 (au4args[0]);

            i4RetVal = D6SrCliSetDuidType (CliHandle, u4PoolIndex, u4CmdType);
            break;
        }
        case CLI_DHCP6_SRV_INTERFACE:
        {
            u4PoolIndex = CLI_GET_POOL_INDEX ();

            if ((au4args[0]) != NULL)
            {
                i4IfIndex = CLI_PTR_TO_U4 (au4args[0]);
            }
            i4RetVal = D6SrCliSetDuidInterface (CliHandle, u4PoolIndex,
                                                i4IfIndex);
            break;
        }
        case CLI_DHCP6_SRV_ENABLE:
        {
            /*au4args[0]-> type of command  */
            /*au4args[1]-> type of command  */

            u4Index = CLI_GET_IFINDEX ();
            if (au4args[1] != NULL)
                u1Preference[0] = (UINT1) (*au4args[1]);

            i4RetVal = D6SrCliSetIfEnable (CliHandle, u4Index,
                                           (UINT1 *) au4args[0],
                                           &u1Preference[0]);
            break;
        }
        case CLI_DHCP6_SRV_DISABLE:
        {
            /*au4args[0]-> type of command  */

            u4Index = CLI_GET_IFINDEX ();

            i4RetVal = D6SrCliSetIfDisable (CliHandle, u4Index);
            break;
        }
        case CLI_DHCP6_SRV_SHOW_ALL_POOL:
        {
            i4RetVal = D6SrCliShowAllPoolInfo (CliHandle);
            break;
        }
        case CLI_DHCP6_SRV_SHOW_POOL:
        {
            i4RetVal = D6SrCliShowPoolConf (CliHandle, (INT1 *) au4args[0]);
            break;
        }
        case CLI_DHCP6_SRV_SHOW_ALL_INTERFACE_CONF:
        {
            i4RetVal = D6SrCliShowAllInterfaceInfo (CliHandle);
            break;
        }

        case CLI_DHCP6_SRV_SHOW_INTERFACE_CONF:
        {
            i4RetVal = D6SrCliShowIfInfo (CliHandle, u4InterfaceIndex);
            break;
        }
        case CLI_DHCP6_SRV_SHOW_GLOBAL_CONF:
            i4RetVal = D6SrCliShowSrvGlobalInfo (CliHandle);
            break;

        case CLI_DHCP6_SRV_SHOW_ALL_STATS:
        {
            i4RetVal = D6SrCliShowAllInterfaceStats (CliHandle);
            break;
        }
        case CLI_DHCP6_SRV_SHOW_STATS:
        {
            i4RetVal = D6SrCliShowInterfaceStats (CliHandle, u4InterfaceIndex);
            break;
        }
        case CLI_DHCP6_SRV_DEBUG:
        {
            /*au4args[0]-> Trace Value   */
            i4RetVal = D6SrCliSetTrace (CliHandle, (UINT1 *) (au4args[0]));
            break;
        }
        case CLI_DHCP6_SRV_NO_DEBUG:
        {
            /*au4args[0]-> Trace Value   */
            i4RetVal = D6SrCliSetTrace (CliHandle, (UINT1 *) (au4args[0]));
            break;
        }
        case CLI_DHCP6_SRV_CLEAR_STATS:
            i4RetVal = D6SrCliClearInterfaceInfo (CliHandle,
                                                  u4InterfaceIndex,
                                                  DHCP6_SRV_CLEAR_COUNTER_INTERFACE);
            break;
        case CLI_DHCP6_SRV_CLEAR_ALL_STATS:
            i4RetVal = D6SrCliClearAllInterfaceInfo (CliHandle);
            break;
        case CLI_DHCP6_SRV_UDP_SOURCE_DEST_PORT:
            i4RetVal = D6SrCliSetClntUdpPorts
                (CliHandle, CLI_PTR_TO_U4 (au4args[0]),
                 CLI_PTR_TO_U4 (au4args[1]));
            break;
        case CLI_DHCP6_SRV_SYSLOG_STATUS:
        {
            i4RetVal = D6SrCliSetSysLogStatus (CliHandle,
                                               CLI_PTR_TO_I4 (au4args[0]));
            break;
        }
        case CLI_DHCP6_SRV_REFRESH_TIME:
        {
            u4Days = CLI_PTR_TO_U4 (au4args[0]);
            u4Hours = CLI_PTR_TO_U4 (au4args[1]);
            u4Mins = CLI_PTR_TO_U4 (au4args[2]);
            u4Type = CLI_PTR_TO_U4 (au4args[3]);
            u4PoolIndex = CLI_GET_POOL_INDEX ();

            i4RetVal = D6SrCliSetRefresTime (CliHandle, u4PoolIndex,
                                             u4Type, u4Days, u4Hours, u4Mins);
            break;
        }

        case CLI_DHCP6_SRV_NO_REFRESH_TIME:
        {
            u4PoolIndex = CLI_GET_POOL_INDEX ();
            i4RetVal = D6SrCliReSetRefresTime (CliHandle, u4PoolIndex);
            break;
        }

        default:
            CliPrintf (CliHandle, "\r%% Invalid Command\r\n");
            i4RetVal = CLI_FAILURE;
            break;
    }
    if ((i4RetVal == CLI_FAILURE) && (CLI_GET_ERR (&u4ErrorCode) ==
                                      CLI_SUCCESS))
    {
        if ((u4ErrorCode > 0) && (u4ErrorCode < DHCP6_SRV_CLI_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", gaD6SrCliErrString[u4ErrorCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetVal);

    D6SrMainUnLock ();

    CliUnRegisterLock (CliHandle);

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliSetRefresTime                                       */
/*                                                                           */
/* Description  : This function set the Refrest Time value                   */
/*                                                                           */
/* Input        : CliHandle  - CLI Handle                                    */
/*                u4Type     - Type.                                         */
/*                u4Value    - u4Days                                        */
/*                u4Value    - u4Hours                                       */
/*                u4Value    - u4Mins                                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_FAILURE/CLI_SUCCESS                                    */
/*                                                                           */
/*****************************************************************************/

INT4
D6SrCliSetRefresTime (tCliHandle CliHandle, UINT4 u4PoolIndex,
                      UINT4 u4Type, UINT4 u4Days, UINT4 u4Hours, UINT4 u4Mins)
{
    UINT1              *pu1Value = NULL;
    tSNMP_OCTET_STRING_TYPE OctetValue;
    UINT4               u4RefreshValue = 0;
    UINT4               u4RefHours = 0;
    UINT4               u4RefMinutes = 0;
    UINT4               u4OptType = 0;
    INT4                i4Preference = 0;
    UINT1               au1OptionLen[DHCP6_SRV_REFRESH_TIMER_SIZE];

    MEMSET (&OctetValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OptionLen, 0, DHCP6_SRV_REFRESH_TIMER_SIZE);

    OctetValue.i4_Length = DHCP6_SRV_REFRESH_TIMER_SIZE;
    OctetValue.pu1_OctetList = au1OptionLen;
    u4OptType = DHCP6_SRV_REFRESH_TIMER_TYPE;
    if (u4Type == DHCP6_SRV_REFRESH_TIME_FINITY)
    {
        u4RefHours = u4Days * DHCP6_SRV_NUM_HOURS_1DAY + u4Hours;
        u4RefMinutes = u4RefHours * DHCP6_SRV_NUM_MINUTES_1HOUR + u4Mins;
        u4RefreshValue = u4RefMinutes * DHCP6_SRV_NUM_SECONDS_1MIN;
    }
    else
    {
        u4RefreshValue = DHCP6_SRV_MAX_REFRESH_TIME;
    }
    pu1Value = OctetValue.pu1_OctetList;
    DHCP6_SRV_PUT_4BYTE (pu1Value, u4RefreshValue);
    if (D6SrCliProcessAddOption
        (CliHandle, u4PoolIndex, u4OptType, OctetValue.pu1_OctetList,
         OctetValue.i4_Length, i4Preference) == CLI_SUCCESS)
    {
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliReSetRefresTime                                       */
/*                                                                           */
/* Description  : This function set the Refrest Time value                   */
/*                                                                           */
/* Input        : CliHandle  - CLI Handle                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_FAILURE/CLI_SUCCESS                                    */
/*                                                                           */
/*****************************************************************************/

INT4
D6SrCliReSetRefresTime (tCliHandle CliHandle, UINT4 u4PoolIndex)
{

    INT4                i4OptType = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4RetVal = 0;
    UINT4               u4OptionIndex = 0;
    UINT4               u4NextPoolIndex = 0;
    UINT4               u4CurrentPoolIndex = 0;
    UINT4               u4NextOptionIndex = 0;
    UINT4               u4CurrentOptionIndex = 0;
    INT4                i4OptionType = 0;
    BOOL1               b1Result = DHCP6_SRV_NOT_FOUND;

    i4OptType = DHCP6_SRV_REFRESH_TIMER_TYPE;

    i4RetVal = nmhGetFirstIndexFsDhcp6SrvOptionTable (&u4NextPoolIndex,
                                                      &u4NextOptionIndex);
    /* Check for Option Exist */
    while ((i4RetVal != SNMP_FAILURE)
           && (u4NextOptionIndex <= MAX_D6SR_SRV_OPTION_ENTRY))
    {

        nmhGetFsDhcp6SrvOptionType (u4NextPoolIndex, u4NextOptionIndex,
                                    &i4OptionType);

        if ((u4NextPoolIndex == u4PoolIndex) && (i4OptType == i4OptionType))
        {
            u4OptionIndex = u4NextOptionIndex;
            b1Result = DHCP6_SRV_FOUND;
            break;
        }
        u4CurrentPoolIndex = u4NextPoolIndex;
        u4CurrentOptionIndex = u4NextOptionIndex;

        i4RetVal = nmhGetNextIndexFsDhcp6SrvOptionTable (u4CurrentPoolIndex,
                                                         &u4NextPoolIndex,
                                                         u4CurrentOptionIndex,
                                                         &u4NextOptionIndex);
    }

    if (b1Result == DHCP6_SRV_FOUND)
    {
        if (nmhTestv2FsDhcp6SrvOptionRowStatus (&u4ErrorCode, u4PoolIndex,
                                                u4OptionIndex,
                                                DESTROY) == SNMP_SUCCESS)
        {
            if (nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex,
                                                 DESTROY) == SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }
            else
            {
                CLI_FATAL_ERROR (CliHandle);
            }
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r%% Refresh Option Does Not exists!!!\r\n ");
        return CLI_FAILURE;
    }
    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliSetClntUdpPorts                                     */
/*                                                                           */
/* Description  : This function set the Udp ports for DHCPv6 Servre          */
/*                                                                           */
/* Input        : CliHandle  - CLI Handle                                    */
/*                u4Type    - Type.                                          */
/*                u4Value    = Value                                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_FAILURE/CLI_SUCCESS                                    */
/*                                                                           */
/*****************************************************************************/

INT4
D6SrCliSetClntUdpPorts (tCliHandle CliHandle, UINT4 u4Type, UINT4 u4Value)
{
    UINT4               u4ErrCode = 0;
    UNUSED_PARAM (CliHandle);
    if (u4Type == DHCP6_SRV_UDP_REPLY_TRANS_PORT_NUM)
    {
        if (nmhTestv2FsDhcp6SrvClientTransmitPort (&u4ErrCode, u4Value) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetFsDhcp6SrvClientTransmitPort (u4Value) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else if (u4Type == DHCP6_SRV_UDP_LISTEN_PORT_NUM)
    {
        if (nmhTestv2FsDhcp6SrvListenPort (&u4ErrCode, u4Value) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        nmhSetFsDhcp6SrvListenPort (u4Value);
    }
    else if (u4Type == DHCP6_SRV_UDP_RELAY_TRANS_PORT_NUM)
    {
        if (nmhTestv2FsDhcp6SrvRelayTransmitPort (&u4ErrCode, u4Value) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetFsDhcp6SrvRelayTransmitPort (u4Value) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SGetPoolCfgPrompt                                     */
/*                                                                           */
/* Description  : This function validates the given pi1ModeName and returns  */
/*                 the prompt in pi1DispStr if valid.                        */
/*                                                                           */
/* Input        : pi1ModeName   - Mode Name to validate                      */
/*                pi1DispStr    - Display string to be returned.             */
/*                                                                           */
/* Output       : pi1DispStr    - Display string.                            */
/*                                                                           */
/* Returns      : TRUE/FALSE                                                 */
/*                                                                           */
/*****************************************************************************/
INT1
Dhcp6SGetPoolCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    UINT4               u4PoolIndex;

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    u4Len = STRLEN (CLI_MODE_POOL_DHCP6_SRV);

    if (STRNCMP (pi1ModeName, CLI_MODE_POOL_DHCP6_SRV, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    u4PoolIndex = CLI_ATOI (pi1ModeName);

    CLI_SET_POOL_INDEX (u4PoolIndex);

    STRNCPY (pi1DispStr, "(config-d6pool)#", STRLEN ("(config-d6pool)#"));
    pi1DispStr[STRLEN ("(config-d6pool)#")] = '\0';
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrGetClientCfgPrompt                                   */
/*                                                                           */
/* Description  : This function validates the given pi1ModeName and returns  */
/*                 the prompt in pi1DispStr if valid.                        */
/*                                                                           */
/* Input        : pi1ModeName   - Mode Name to validate                      */
/*                pi1DispStr    - Display string to be returned.             */
/*                                                                           */
/* Output       : pi1DispStr    - Display string.                            */
/*                                                                           */
/* Returns      : TRUE/FALSE                                                 */
/*                                                                           */
/*****************************************************************************/
INT1
D6SrGetClientCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    UINT4               u4ClntIndex;

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    u4Len = STRLEN (CLI_MODE_CLNT_DHCP6_SRV);

    if (STRNCMP (pi1ModeName, CLI_MODE_CLNT_DHCP6_SRV, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    u4ClntIndex = CLI_ATOI (pi1ModeName);

    CLI_SET_SRV_CLNT_INDEX (u4ClntIndex);

    STRNCPY (pi1DispStr, "(config-d6clnt)#", STRLEN ("(config-d6clnt)#"));
    pi1DispStr[STRLEN ("(config-d6clnt)#")] = '\0';
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SGetVendorCfgPrompt                                   */
/*                                                                           */
/* Description  : This function validates the given pi1ModeName and returns  */
/*                 the prompt in pi1DispStr if valid.                        */
/*                                                                           */
/* Input        : pi1ModeName   - Mode Name to validate                      */
/*                pi1DispStr    - Display string to be returned.             */
/*                                                                           */
/* Output       : pi1DispStr    - Display string.                            */
/*                                                                           */
/* Returns      : TRUE/FALSE                                                 */
/*                                                                           */
/*****************************************************************************/
INT1
Dhcp6SGetVendorCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    UINT4               u4OptionIndex;

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    u4Len = STRLEN (CLI_MODE_VENDOR_DHCP6_SRV);

    if (STRNCMP (pi1ModeName, CLI_MODE_VENDOR_DHCP6_SRV, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    u4OptionIndex = CLI_ATOI (pi1ModeName);

    CLI_SET_VENDOR_INDEX (u4OptionIndex);

    STRNCPY (pi1DispStr, "(d6pool-vendor)#", STRLEN ("(d6pool-vendor)#"));
    pi1DispStr[STRLEN ("(d6pool-vendor)#")] = '\0';
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliCreateAddressPool                                 */
/*                                                                           */
/* Description  : Configures DHCP6 Pool                                      */
/*                                                                           */
/* Input        : CliHandle     - CliContext ID                              */
/*                pu1PoolName   - DHCP6 Pool Name                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliCreateAddressPool (tCliHandle CliHandle, UINT1 *pu1PoolName)
{
    tSNMP_OCTET_STRING_TYPE *pOctetPool = NULL;
    tSNMP_OCTET_STRING_TYPE PoolName;
    UINT4               u4ErrorCode = 0;
    UINT4               u4PoolIndex = 0;
    UINT4               u4InsPoolIndex = 0;
    UINT4               u4NextPoolIndex = 0;
    UINT4               u4CurrentPoolIndex = 0;
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT1               au1PoolName[DHCP6_SRV_POOL_NAME_MAX];
    INT4                i4RetVal = 0;
    INT4                i4Len = 0;
    BOOL1               b1Result = DHCP6_SRV_NOT_FOUND;

    PoolName.pu1_OctetList = au1PoolName;

    i4RetVal = nmhGetFirstIndexFsDhcp6SrvPoolTable (&u4NextPoolIndex);

    /* Check for Pool Exist */
    while ((i4RetVal != SNMP_FAILURE) &&
           (u4NextPoolIndex <= MAX_D6SR_SRV_POOL_ENTRY))
    {
        MEMSET (au1PoolName, 0, DHCP6_SRV_POOL_NAME_MAX);

        nmhGetFsDhcp6SrvPoolName (u4NextPoolIndex, &PoolName);

        i4Len = (PoolName.i4_Length > (INT4) STRLEN (pu1PoolName)) ?
            PoolName.i4_Length : (INT4) STRLEN (pu1PoolName);

        if (MEMCMP (pu1PoolName, PoolName.pu1_OctetList, i4Len) == 0)
        {
            u4InsPoolIndex = u4NextPoolIndex;
            b1Result = DHCP6_SRV_FOUND;
            break;
        }
        u4CurrentPoolIndex = u4NextPoolIndex;

        i4RetVal = nmhGetNextIndexFsDhcp6SrvPoolTable (u4CurrentPoolIndex,
                                                       &u4NextPoolIndex);
    }

    if (b1Result == DHCP6_SRV_FOUND)
    {
        u4PoolIndex = u4InsPoolIndex;
    }
    else
    {
        if (nmhGetFsDhcp6SrvPoolTableNextIndex (&u4PoolIndex) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Pool table is full\r\n ");
            return CLI_FAILURE;
        }

        if (nmhTestv2FsDhcp6SrvPoolRowStatus (&u4ErrorCode, u4PoolIndex,
                                              CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Error while Creating New Pool Index\r\n ");
            return CLI_FAILURE;
        }

        if (nmhSetFsDhcp6SrvPoolRowStatus (u4PoolIndex,
                                           CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Error while Creating New Pool Index\r\n ");
            return CLI_FAILURE;
        }

        pOctetPool = SNMP_AGT_FormOctetString (pu1PoolName,
                                               STRLEN (pu1PoolName) + 1);
        if (pOctetPool == NULL)
        {
            CliPrintf (CliHandle, "\r%% Error while Setting Pool Name\r\n ");
            return CLI_FAILURE;
        }

        pOctetPool->pu1_OctetList[STRLEN (pu1PoolName)] = '\0';
        pOctetPool->i4_Length = STRLEN (pu1PoolName) + 1;

        if (nmhTestv2FsDhcp6SrvPoolName (&u4ErrorCode, u4PoolIndex, pOctetPool)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Error while Setting Pool Name\r\n ");
            free_octetstring (pOctetPool);
            return CLI_FAILURE;
        }
        if (nmhSetFsDhcp6SrvPoolName (u4PoolIndex, pOctetPool) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Error while Setting Pool Name\r\n ");
            free_octetstring (pOctetPool);
            return CLI_FAILURE;
        }
        free_octetstring (pOctetPool);
        if (nmhTestv2FsDhcp6SrvPoolRowStatus (&u4ErrorCode, u4PoolIndex,
                                              ACTIVE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Error while Creating New Pool Index\r\n ");
            return CLI_FAILURE;
        }
        if (nmhSetFsDhcp6SrvPoolRowStatus (u4PoolIndex, ACTIVE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Error while Creating New Pool Index\r\n ");
            return CLI_FAILURE;
        }
    }

    SPRINTF ((CHR1 *) au1Cmd, "%s%u", CLI_MODE_POOL_DHCP6_SRV, u4PoolIndex);
    CliChangePath ((CONST CHR1 *) au1Cmd);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliDestroyAddressPool                                */
/*                                                                           */
/* Description  : Deletes the DHCP6 Pool                                     */
/*                                                                           */
/* Input        : CliHandle     - CliContext ID                              */
/*                pu1PoolName   - DHCP6 Pool Name                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliDestroyAddressPool (tCliHandle CliHandle, UINT1 *pu1PoolName)
{
    tSNMP_OCTET_STRING_TYPE PoolName;
    UINT4               u4NextPoolIndex = 0;
    UINT4               u4CurrentPoolIndex = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4DelPoolIndex = 0;
    UINT1               au1PoolName[DHCP6_SRV_POOL_NAME_MAX];
    INT4                i4RetVal = 0;
    INT4                i4Len = 0;
    BOOL1               b1Result = DHCP6_SRV_NOT_FOUND;

    i4RetVal = nmhGetFirstIndexFsDhcp6SrvPoolTable (&u4NextPoolIndex);

    PoolName.pu1_OctetList = au1PoolName;

    /* Check for Pool Exist */
    while ((i4RetVal != SNMP_FAILURE) &&
           (u4NextPoolIndex <= MAX_D6SR_SRV_POOL_ENTRY))
    {
        MEMSET (au1PoolName, 0, DHCP6_SRV_POOL_NAME_MAX);

        nmhGetFsDhcp6SrvPoolName (u4NextPoolIndex, &PoolName);

        i4Len = (PoolName.i4_Length > (INT4) STRLEN (pu1PoolName)) ?
            PoolName.i4_Length : (INT4) STRLEN (pu1PoolName);

        if (MEMCMP (pu1PoolName, PoolName.pu1_OctetList, i4Len) == 0)
        {
            u4DelPoolIndex = u4NextPoolIndex;
            b1Result = DHCP6_SRV_FOUND;
            break;
        }
        u4CurrentPoolIndex = u4NextPoolIndex;

        i4RetVal = nmhGetNextIndexFsDhcp6SrvPoolTable (u4CurrentPoolIndex,
                                                       &u4NextPoolIndex);
    }

    if (b1Result == DHCP6_SRV_NOT_FOUND)
    {
        CliPrintf (CliHandle, "\r%% Pool Index doesn't exist.\r\n ");
        return CLI_FAILURE;
    }
    else
    {
        if (nmhTestv2FsDhcp6SrvPoolRowStatus (&u4ErrorCode, u4DelPoolIndex,
                                              DESTROY) == SNMP_SUCCESS)
        {
            if (nmhSetFsDhcp6SrvPoolRowStatus (u4DelPoolIndex,
                                               DESTROY) == SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }
        }
    }
    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliCreateVendor                                      */
/*                                                                           */
/* Description  : Configures DHCP6 Vendor                                    */
/*                                                                           */
/* Input        : CliHandle     - CliContext ID                              */
/*                u4VendorIndex - Vendor Index                               */
/*                u4PoolIndex   - Pool Index                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliCreateVendor (tCliHandle CliHandle, UINT4 u4VendorIndex,
                     UINT1 *pu1OptValue, UINT4 u4Len, UINT4 u4PoolIndex)
{
    tSNMP_OCTET_STRING_TYPE pOctetValue;
    tSNMP_OCTET_STRING_TYPE OptionValue;
    UINT4               u4ErrorCode = 0;
    UINT4               u4OptionIndex = 0;
    UINT4               u4NextPoolIndex = 0;
    UINT4               u4CurrentPoolIndex = 0;
    UINT4               u4NextOptionIndex = 0;
    UINT4               u4CurrentOptionIndex = 0;
    INT4                i4RetVal = 0;
    INT4                i4Preference = 1;
    INT4                i4OptionType = 0;
    UINT1               au1OptionValue[DHCP6_SRV_OPTION_LENGTH_MAX];
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT1              *pu1Value = NULL;
    BOOL1               b1Result = DHCP6_SRV_NOT_FOUND;

    u4Len = DHCP6_SRV_VENDOR_LEN;

    pOctetValue.pu1_OctetList = pu1OptValue;
    /* update octet string length */
    pOctetValue.i4_Length = u4Len;

    OptionValue.pu1_OctetList = au1OptionValue;

    pu1Value = pOctetValue.pu1_OctetList;
    DHCP6_SRV_PUT_4BYTE (pu1Value, u4VendorIndex);

    i4RetVal = nmhGetFirstIndexFsDhcp6SrvOptionTable (&u4NextPoolIndex,
                                                      &u4NextOptionIndex);
    /* Check for Option Exist */
    while ((i4RetVal != SNMP_FAILURE) &&
           (u4NextOptionIndex <= MAX_D6SR_SRV_OPTION_ENTRY))
    {
        MEMSET (au1OptionValue, 0, DHCP6_SRV_OPTION_LENGTH_MAX);

        nmhGetFsDhcp6SrvOptionType (u4NextPoolIndex, u4NextOptionIndex,
                                    &i4OptionType);
        nmhGetFsDhcp6SrvOptionValue (u4NextPoolIndex, u4NextOptionIndex,
                                     &OptionValue);

        if ((i4OptionType == (INT4) DHCP6_SRV_VENDOR_SPECIFIC_OPTION) &&
            (u4PoolIndex == u4NextPoolIndex))
        {
            u4PoolIndex = u4NextPoolIndex;
            u4OptionIndex = u4NextOptionIndex;
            b1Result = DHCP6_SRV_FOUND;
            break;
        }
        u4CurrentPoolIndex = u4NextPoolIndex;
        u4CurrentOptionIndex = u4NextOptionIndex;

        i4RetVal = nmhGetNextIndexFsDhcp6SrvOptionTable (u4CurrentPoolIndex,
                                                         &u4NextPoolIndex,
                                                         u4CurrentOptionIndex,
                                                         &u4NextOptionIndex);
    }
    if (b1Result == DHCP6_SRV_FOUND)
    {
        /* Existing row - Updation */
        if (nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex,
                                             NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Option table is full\r\n ");
            return CLI_FAILURE;
        }
    }
    else
    {
        /* Option Table Creation */
        if (nmhGetFsDhcp6SrvPoolOptionTableNextIndex (u4PoolIndex,
                                                      &u4OptionIndex)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        else
        {
            /* Create new row when row is not existed */
            if (nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex,
                                                 CREATE_AND_WAIT)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Option table is full\r\n ");
                return CLI_FAILURE;
            }
        }
    }

    /*  validate the option type */
    if (nmhTestv2FsDhcp6SrvOptionType (&u4ErrorCode, u4PoolIndex,
                                       u4OptionIndex,
                                       DHCP6_SRV_VENDOR_SPECIFIC_OPTION) ==
        SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex, DESTROY);
        CliPrintf (CliHandle, "\r%% Option table is full\r\n ");
        return CLI_FAILURE;
    }

    /*  validate the option length */
    if (nmhTestv2FsDhcp6SrvOptionLength (&u4ErrorCode, u4PoolIndex,
                                         u4OptionIndex, u4Len) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex, DESTROY);
        CliPrintf (CliHandle, "\r%% Option table is full\r\n ");
        return CLI_FAILURE;
    }

    /* set the option type */
    if (nmhSetFsDhcp6SrvOptionType (u4PoolIndex, u4OptionIndex,
                                    DHCP6_SRV_VENDOR_SPECIFIC_OPTION) ==
        SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex, DESTROY);
        return CLI_FAILURE;
    }

    /* set the Option length */
    if (nmhSetFsDhcp6SrvOptionLength (u4PoolIndex, u4OptionIndex,
                                      u4Len) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex, DESTROY);
        return CLI_FAILURE;
    }

    /*  validate the option value */
    if (nmhTestv2FsDhcp6SrvOptionValue (&u4ErrorCode, u4PoolIndex,
                                        u4OptionIndex,
                                        &pOctetValue) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex, DESTROY);
        CliPrintf (CliHandle, "\r%% Option table is full\r\n ");
        return CLI_FAILURE;
    }

    /* set the Option Value */
    if (nmhSetFsDhcp6SrvOptionValue (u4PoolIndex, u4OptionIndex,
                                     &pOctetValue) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex, DESTROY);
        return CLI_FAILURE;
    }
    /*  validate the preference */
    if (nmhTestv2FsDhcp6SrvOptionPreference (&u4ErrorCode, u4PoolIndex,
                                             u4OptionIndex,
                                             i4Preference) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex, DESTROY);
        CliPrintf (CliHandle, "\r%% Option table is full\r\n ");
        return CLI_FAILURE;
    }

    /* set the parameters */
    if (nmhSetFsDhcp6SrvOptionPreference (u4PoolIndex, u4OptionIndex,
                                          i4Preference) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex, DESTROY);
        return CLI_FAILURE;
    }

    /* make the row status active */
    if (nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex,
                                         ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex, DESTROY);
        CliPrintf (CliHandle, "\r%% Invalid Value for Option Length/Value\r\n");
        return CLI_FAILURE;
    }

    SPRINTF ((CHR1 *) au1Cmd, "%s%u", CLI_MODE_VENDOR_DHCP6_SRV, u4OptionIndex);
    CliChangePath ((CONST CHR1 *) au1Cmd);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliDelClntInfo                                         */
/*                                                                           */
/* Description  : This function Delete Client Authenication Information.       */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                *pu1ClntName  - Pointer to Client Name                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliDelClntInfo (tCliHandle CliHandle, UINT1 *pu1ClntName)
{
    tSNMP_OCTET_STRING_TYPE ClientName;
    tSNMP_OCTET_STRING_TYPE ClientNextName;
    UINT4               u4ClntIndex = 0;
    UINT4               u4CurrentClntIndex = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4NextClntIndex;
    INT4                i4RetVal = 0;
    INT4                i4Len = 0;
    BOOL1               b1ClntResult = DHCP6_SRV_NOT_FOUND;
    UINT1               au1ClntValue[DHCP6_SRV_CLNT_NAME_MAX];

    MEMSET (&ClientName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    ClientName.pu1_OctetList = pu1ClntName;
    ClientName.i4_Length = STRLEN (pu1ClntName);

    ClientNextName.pu1_OctetList = au1ClntValue;

    /* Client Table Creation / Updation */
    i4RetVal = nmhGetFirstIndexFsDhcp6SrvClientTable (&u4NextClntIndex);
    /* Check for Client Exist */
    while ((i4RetVal != SNMP_FAILURE) &&
           (u4NextClntIndex <= MAX_D6SR_SRV_CLIENT_ENTRY))
    {
        MEMSET (au1ClntValue, 0, DHCP6_SRV_CLNT_NAME_MAX);

        nmhGetFsDhcp6SrvClientId (u4NextClntIndex, &ClientNextName);

        i4Len = (ClientName.i4_Length > ClientNextName.i4_Length) ?
            ClientName.i4_Length : ClientNextName.i4_Length;

        if (MEMCMP (ClientName.pu1_OctetList, ClientNextName.pu1_OctetList,
                    i4Len) == 0)
        {
            u4ClntIndex = u4NextClntIndex;
            b1ClntResult = DHCP6_SRV_FOUND;
            break;
        }
        u4CurrentClntIndex = u4NextClntIndex;

        i4RetVal = nmhGetNextIndexFsDhcp6SrvClientTable (u4CurrentClntIndex,
                                                         &u4NextClntIndex);
    }

    if (b1ClntResult == DHCP6_SRV_FOUND)
    {
        /* Existing row */
        if (nmhTestv2FsDhcp6SrvClientRowStatus
            (&u4ErrorCode, u4ClntIndex, DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsDhcp6SrvClientRowStatus (u4ClntIndex,
                                             DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliSetClntInfo                                         */
/*                                                                           */
/* Description  : This function sets Client Authenication Information.       */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                *pu1ClntName  - Pointer to Client Name                     */
/*                u4Type        - Type                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliSetClntInfo (tCliHandle CliHandle, UINT1 *pu1ClntName,
                    UINT4 u4ClientType)
{
    tSNMP_OCTET_STRING_TYPE ClientName;
    tSNMP_OCTET_STRING_TYPE ClientNextName;
    UINT1               au1ClntValue[DHCP6_SRV_CLNT_NAME_MAX];
    UINT1               au1InClntDuid[DHCP6_SRV_CLNT_NAME_MAX];
    UINT4               u4ClntIndex;
    UINT4               u4CurrentClntIndex;
    UINT4               u4NextClntIndex;
    UINT4               u4ErrorCode = 0;
    INT4                i4RetVal = 0;
    INT4                i4Len = 0;
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    BOOL1               b1ClntResult = DHCP6_SRV_NOT_FOUND;

    MEMSET (&ClientName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1InClntDuid, 0, DHCP6_SRV_CLNT_NAME_MAX);

    MEMSET (&ClientName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    ClientName.pu1_OctetList = pu1ClntName;
    ClientName.i4_Length = STRLEN (pu1ClntName);

    ClientNextName.pu1_OctetList = au1ClntValue;

    /* Client Table Creation / Updation */
    i4RetVal = nmhGetFirstIndexFsDhcp6SrvClientTable (&u4NextClntIndex);
    /* Check for Client Exist */
    while ((i4RetVal != SNMP_FAILURE) &&
           (u4NextClntIndex <= MAX_D6SR_SRV_CLIENT_ENTRY))
    {
        MEMSET (au1ClntValue, 0, DHCP6_SRV_CLNT_NAME_MAX);

        nmhGetFsDhcp6SrvClientId (u4NextClntIndex, &ClientNextName);

        i4Len = (ClientName.i4_Length > ClientNextName.i4_Length) ?
            ClientName.i4_Length : ClientNextName.i4_Length;

        if (MEMCMP (ClientName.pu1_OctetList, ClientNextName.pu1_OctetList,
                    i4Len) == 0)
        {
            u4ClntIndex = u4NextClntIndex;
            b1ClntResult = DHCP6_SRV_FOUND;
            break;
        }
        u4CurrentClntIndex = u4NextClntIndex;

        i4RetVal = nmhGetNextIndexFsDhcp6SrvClientTable (u4CurrentClntIndex,
                                                         &u4NextClntIndex);
    }

    if (b1ClntResult == DHCP6_SRV_FOUND)
    {
        /* Existing row */
        if (nmhSetFsDhcp6SrvClientRowStatus (u4ClntIndex,
                                             NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhGetFsDhcp6SrvClientTableNextIndex (&u4ClntIndex) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Create new row when row is not existed */
        if (nmhSetFsDhcp6SrvClientRowStatus (u4ClntIndex,
                                             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    /*  validate the length */
    if (nmhTestv2FsDhcp6SrvClientId (&u4ErrorCode, u4ClntIndex,
                                     &ClientName) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvClientRowStatus (u4ClntIndex, DESTROY);
        return CLI_FAILURE;
    }

    /* set the parameters */
    if (nmhSetFsDhcp6SrvClientId (u4ClntIndex, &ClientName) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvClientRowStatus (u4ClntIndex, DESTROY);
        return CLI_FAILURE;
    }

    /*  validate the type */
    if (nmhTestv2FsDhcp6SrvClientIdType (&u4ErrorCode, u4ClntIndex,
                                         u4ClientType) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvClientRowStatus (u4ClntIndex, DESTROY);
        CliPrintf (CliHandle, "\r%% Option table is full\r\n ");
        return CLI_FAILURE;
    }

    /* set the parameters */
    if (nmhSetFsDhcp6SrvClientIdType (u4ClntIndex,
                                      u4ClientType) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvClientRowStatus (u4ClntIndex, DESTROY);
        return CLI_FAILURE;
    }

    /*  validate the rowstatus */
    if (nmhTestv2FsDhcp6SrvClientRowStatus (&u4ErrorCode, u4ClntIndex,
                                            ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvClientRowStatus (u4ClntIndex, DESTROY);
        CliPrintf (CliHandle, "\r%% Option table is full\r\n ");
        return CLI_FAILURE;
    }
    /* make the row status active */
    if (nmhSetFsDhcp6SrvClientRowStatus (u4ClntIndex, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvClientRowStatus (u4ClntIndex, DESTROY);
        CliPrintf (CliHandle, "\r%% Invalid Value for Option Length/Value\r\n");
        return CLI_FAILURE;
    }
    SPRINTF ((CHR1 *) au1Cmd, "%s%d", CLI_MODE_CLNT_DHCP6_SRV, u4ClntIndex);
    CliChangePath ((CONST CHR1 *) au1Cmd);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliSetAuthInfo                                       */
/*                                                                           */
/* Description  : This function sets Authentication Information.             */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                *pu1RealmName - Pointer to Realm                           */
/*                *pu1KeyName   - Pointer to Key                             */
/*                u4ClientIndex - Client Index                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliSetAuthInfo (tCliHandle CliHandle, UINT1 *pu1RealmName,
                    UINT1 *pu1KeyName, UINT4 u4ClntIndex)
{
    tSNMP_OCTET_STRING_TYPE RealmName;
    tSNMP_OCTET_STRING_TYPE RealmNextName;
    UINT4               u4RealmIndex;
    UINT4               u4CurrentRealmIndex = 0;
    UINT4               u4NextRealmIndex = 0;
    UINT4               u4ErrorCode = 0;
    UINT1               au1RealmValue[DHCP6_SRV_REALM_NAME_MAX];
    INT4                i4RetVal = 0;
    INT4                i4Len = 0;
    BOOL1               b1RealmResult = DHCP6_SRV_NOT_FOUND;

    MEMSET (&RealmName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    RealmName.pu1_OctetList = pu1RealmName;
    RealmName.i4_Length = STRLEN (pu1RealmName);

    RealmNextName.pu1_OctetList = au1RealmValue;

    i4RetVal = nmhGetFirstIndexFsDhcp6SrvRealmTable (&u4NextRealmIndex);
    /* Check for Realm Exist */
    while ((i4RetVal != SNMP_FAILURE) &&
           (u4NextRealmIndex <= MAX_D6SR_SRV_REALM_ENTRY))
    {
        MEMSET (au1RealmValue, 0, DHCP6_SRV_REALM_NAME_MAX);

        nmhGetFsDhcp6SrvRealmName (u4NextRealmIndex, &RealmNextName);

        i4Len = (RealmName.i4_Length > RealmNextName.i4_Length) ?
            RealmName.i4_Length : RealmNextName.i4_Length;

        if (MEMCMP (RealmName.pu1_OctetList, RealmNextName.pu1_OctetList,
                    i4Len) == 0)
        {
            u4RealmIndex = u4NextRealmIndex;
            b1RealmResult = DHCP6_SRV_FOUND;
            break;
        }
        u4CurrentRealmIndex = u4NextRealmIndex;

        i4RetVal = nmhGetNextIndexFsDhcp6SrvRealmTable (u4CurrentRealmIndex,
                                                        &u4NextRealmIndex);
    }

    if (b1RealmResult == DHCP6_SRV_FOUND)
    {
        /* Existing row */
        if (nmhSetFsDhcp6SrvRealmRowStatus (u4RealmIndex,
                                            NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to allocate resources for realm\r\n ");
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhGetFsDhcp6SrvRealmTableNextIndex (&u4RealmIndex) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to allocate resources for realm\r\n ");
            return CLI_FAILURE;
        }
        /* Create new row when row is not existed */
        if (nmhSetFsDhcp6SrvRealmRowStatus (u4RealmIndex,
                                            CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to allocate resources for realm\r\n ");
            return CLI_FAILURE;
        }
    }
    if (D6SrCliSetAuthRealmInfo (CliHandle, &RealmName,
                                 u4RealmIndex) == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to configure realm information\r\n ");
        return CLI_FAILURE;
    }

    if (pu1KeyName != NULL)
        /* Check for Realm Exist */
    {
        if (D6SrCliSetAuthKeyInfo (CliHandle, pu1KeyName,
                                   u4RealmIndex) == CLI_FAILURE)

        {
            CliPrintf (CliHandle,
                       "\r%% Unable to configure key information\r\n ");
            return CLI_FAILURE;
        }

    }
    if (nmhTestv2FsDhcp6SrvClientRowStatus
        (&u4ErrorCode, u4ClntIndex, NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhSetFsDhcp6SrvClientRowStatus (u4ClntIndex,
                                         NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhTestv2FsDhcp6SrvClientRealm (&u4ErrorCode, u4ClntIndex, u4RealmIndex)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to relate realm to client\r\n ");
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6SrvClientRealm (u4ClntIndex, u4RealmIndex) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to relate realm to client\r\n ");
        return CLI_FAILURE;
    }
    if (nmhTestv2FsDhcp6SrvClientRowStatus (&u4ErrorCode, u4ClntIndex, ACTIVE)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhSetFsDhcp6SrvClientRowStatus (u4ClntIndex, ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliSetAuthRealmInfo                                  */
/*                                                                           */
/* Description  : This function sets auth realm information                  */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                *pu1RealmName - Pointer to RealmName                       */
/*                u4RealmIndex  - Realm Index                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliSetAuthRealmInfo (tCliHandle CliHandle,
                         tSNMP_OCTET_STRING_TYPE * pu1RealmName,
                         UINT4 u4RealmIndex)
{
    UINT4               u4ErrorCode = 0;

    /*  validate the length */
    if (nmhTestv2FsDhcp6SrvRealmName (&u4ErrorCode, u4RealmIndex,
                                      pu1RealmName) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvRealmRowStatus (u4RealmIndex, DESTROY);
        return CLI_FAILURE;
    }

    /* set the parameters */
    if (nmhSetFsDhcp6SrvRealmName (u4RealmIndex, pu1RealmName) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvRealmRowStatus (u4RealmIndex, DESTROY);
        return CLI_FAILURE;
    }

    /*  validate the row status */
    if (nmhTestv2FsDhcp6SrvRealmRowStatus (&u4ErrorCode, u4RealmIndex,
                                           ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvRealmRowStatus (u4RealmIndex, DESTROY);
        CliPrintf (CliHandle, "\r%% Option table is full\r\n ");
        return CLI_FAILURE;
    }

    /* make the row status active */
    if (nmhSetFsDhcp6SrvRealmRowStatus (u4RealmIndex, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvRealmRowStatus (u4RealmIndex, DESTROY);
        CliPrintf (CliHandle, "\r%% Invalid Value for Option Length/Value\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliSetAuthKeyInfo                                    */
/*                                                                           */
/* Description  : This function sets auth key information                    */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                *pu1KeyName   - Pointer to RealmName                       */
/*                u4RealmIndex  - Realm Index                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliSetAuthKeyInfo (tCliHandle CliHandle, UINT1 *pu1KeyName,
                       UINT4 u4RealmIndex)
{
    tSNMP_OCTET_STRING_TYPE KeyName;
    tSNMP_OCTET_STRING_TYPE KeyNextName;
    UINT4               u4KeyIndex;
    UINT4               u4CurrentKeyIndex = 0;
    UINT4               u4NextKeyIndex = 0;
    UINT4               u4CurrentRealmIndex = 0;
    UINT4               u4NextRealmIndex = 0;
    UINT4               u4ErrorCode = 0;
    UINT1               au1KeyValue[DHCP6_SRV_KEY_SIZE_MAX];
    INT4                i4RetVal = 0;
    INT4                i4Len = 0;
    BOOL1               b1KeyResult = DHCP6_SRV_NOT_FOUND;

    MEMSET (&KeyName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    KeyName.pu1_OctetList = pu1KeyName;
    KeyName.i4_Length = STRLEN (pu1KeyName);
    KeyNextName.pu1_OctetList = au1KeyValue;

    i4RetVal = nmhGetFirstIndexFsDhcp6SrvKeyTable (&u4NextRealmIndex,
                                                   &u4NextKeyIndex);
    while ((i4RetVal != SNMP_FAILURE) &&
           (u4NextKeyIndex <= MAX_D6SR_SRV_KEY_ENTRY))
    {
        MEMSET (au1KeyValue, 0, DHCP6_SRV_KEY_SIZE_MAX);
        nmhGetFsDhcp6SrvKey (u4NextRealmIndex, u4NextKeyIndex, &KeyNextName);
        i4Len = (KeyName.i4_Length > KeyNextName.i4_Length) ?
            KeyName.i4_Length : KeyNextName.i4_Length;
        if ((MEMCMP (KeyName.pu1_OctetList, KeyNextName.pu1_OctetList,
                     i4Len) == 0) && (u4RealmIndex == u4NextRealmIndex))
        {
            u4KeyIndex = u4NextKeyIndex;
            b1KeyResult = DHCP6_SRV_FOUND;
            break;
        }
        u4CurrentRealmIndex = u4NextRealmIndex;
        u4CurrentKeyIndex = u4NextKeyIndex;
        i4RetVal = nmhGetNextIndexFsDhcp6SrvKeyTable (u4CurrentRealmIndex,
                                                      &u4NextRealmIndex,
                                                      u4CurrentKeyIndex,
                                                      &u4NextKeyIndex);
    }
    if (b1KeyResult == DHCP6_SRV_FOUND)
    {
        if (nmhSetFsDhcp6SrvKeyRowStatus (u4RealmIndex, u4KeyIndex,
                                          NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhGetFsDhcp6SrvRealmKeyTableNextIndex (u4RealmIndex,
                                                    &u4KeyIndex) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        else
        {
            if (nmhSetFsDhcp6SrvKeyRowStatus (u4RealmIndex, u4KeyIndex,
                                              CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
    }
    /*  validate the length */
    if (nmhTestv2FsDhcp6SrvKey (&u4ErrorCode, u4RealmIndex, u4KeyIndex,
                                &KeyName) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvKeyRowStatus (u4RealmIndex, u4KeyIndex, DESTROY);
        return CLI_FAILURE;
    }

    /* set the parameters */
    if (nmhSetFsDhcp6SrvKey (u4RealmIndex, u4KeyIndex,
                             &KeyName) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvKeyRowStatus (u4RealmIndex, u4KeyIndex, DESTROY);
        return CLI_FAILURE;
    }

    /*  validate the row status */
    if (nmhTestv2FsDhcp6SrvKeyRowStatus (&u4ErrorCode, u4RealmIndex,
                                         u4KeyIndex, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvKeyRowStatus (u4RealmIndex, u4KeyIndex, DESTROY);
        CliPrintf (CliHandle, "\r%% Option table is full\r\n ");
        return CLI_FAILURE;
    }

    /* make the row status active */
    if (nmhSetFsDhcp6SrvKeyRowStatus (u4RealmIndex, u4KeyIndex,
                                      ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvKeyRowStatus (u4RealmIndex, u4KeyIndex, DESTROY);
        CliPrintf (CliHandle, "\r%% Invalid Value for Option Length/Value\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliDeleteAuthInfo                                    */
/*                                                                           */
/* Description  : This function sets Authentication Information.             */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                *pu1RealmName - Pointer to Realm                           */
/*                *pu1ClntName  - Pointer to Realm                           */
/*                *pu1KeyName   - Pointer to Key                             */
/*                u4Type        - Type                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliDeleteAuthInfo (tCliHandle CliHandle, UINT1 *pu1RealmName,
                       UINT1 *pu1KeyName, UINT4 u4ClientIndex)
{

    tSNMP_OCTET_STRING_TYPE RealmName;
    tSNMP_OCTET_STRING_TYPE RealmNextName;
    tSNMP_OCTET_STRING_TYPE KeyName;
    tSNMP_OCTET_STRING_TYPE KeyNextName;
    UINT4               u4ErrorCode = 0;
    UINT4               u4RealmIndex = 0;
    UINT4               u4CurrentRealmIndex = 0;
    UINT4               u4NextRealmIndex = 0;
    UINT4               u4KeyIndex = 0;
    UINT4               u4CurrentKeyIndex = 0;
    UINT4               u4NextKeyIndex = 0;
    UINT1               au1RealmValue[DHCP6_SRV_REALM_NAME_MAX];
    UINT1               au1KeyValue[DHCP6_SRV_KEY_SIZE_MAX];
    INT4                i4RetVal = 0;
    INT4                i4Len = 0;
    BOOL1               b1RealmResult = DHCP6_SRV_NOT_FOUND;
    BOOL1               b1KeyResult = DHCP6_SRV_NOT_FOUND;

    MEMSET (&RealmName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&KeyName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    RealmName.pu1_OctetList = pu1RealmName;
    RealmName.i4_Length = STRLEN (pu1RealmName);

    RealmNextName.pu1_OctetList = au1RealmValue;

    i4RetVal = nmhGetFirstIndexFsDhcp6SrvRealmTable (&u4NextRealmIndex);
    /* Check for Realm Exist */
    while ((i4RetVal != SNMP_FAILURE) &&
           (u4NextRealmIndex <= MAX_D6SR_SRV_REALM_ENTRY))
    {
        MEMSET (au1RealmValue, 0, DHCP6_SRV_REALM_NAME_MAX);

        nmhGetFsDhcp6SrvRealmName (u4NextRealmIndex, &RealmNextName);

        i4Len = (RealmName.i4_Length > RealmNextName.i4_Length) ?
            RealmName.i4_Length : RealmNextName.i4_Length;

        if (MEMCMP (RealmName.pu1_OctetList, RealmNextName.pu1_OctetList,
                    i4Len) == 0)
        {
            u4RealmIndex = u4NextRealmIndex;
            b1RealmResult = DHCP6_SRV_FOUND;
            break;
        }
        u4CurrentRealmIndex = u4NextRealmIndex;

        i4RetVal = nmhGetNextIndexFsDhcp6SrvRealmTable (u4CurrentRealmIndex,
                                                        &u4NextRealmIndex);
    }

    if (b1RealmResult == DHCP6_SRV_NOT_FOUND)
    {
        CliPrintf (CliHandle, "\r%% Realm Index doesn't exist.\r\n ");
        return CLI_FAILURE;
    }
    else
    {
        if (pu1KeyName != NULL)
        {
            KeyName.pu1_OctetList = pu1KeyName;
            KeyName.i4_Length = STRLEN (pu1KeyName);
            KeyNextName.pu1_OctetList = au1KeyValue;

            i4RetVal = nmhGetFirstIndexFsDhcp6SrvKeyTable (&u4NextRealmIndex,
                                                           &u4NextKeyIndex);
            /* Check for Realm Exist */
            while ((i4RetVal != SNMP_FAILURE) &&
                   (u4NextKeyIndex <= MAX_D6SR_SRV_KEY_ENTRY))
            {
                MEMSET (au1KeyValue, 0, DHCP6_SRV_KEY_SIZE_MAX);

                nmhGetFsDhcp6SrvKey (u4NextRealmIndex, u4NextKeyIndex,
                                     &KeyNextName);

                i4Len = (KeyName.i4_Length > KeyNextName.i4_Length) ?
                    KeyName.i4_Length : KeyNextName.i4_Length;

                if (MEMCMP (KeyName.pu1_OctetList, KeyNextName.pu1_OctetList,
                            i4Len) == 0)
                {
                    u4RealmIndex = u4NextRealmIndex;
                    u4KeyIndex = u4NextKeyIndex;
                    b1KeyResult = DHCP6_SRV_FOUND;
                    break;
                }
                u4CurrentRealmIndex = u4NextRealmIndex;
                u4CurrentKeyIndex = u4NextKeyIndex;

                i4RetVal =
                    nmhGetNextIndexFsDhcp6SrvKeyTable (u4CurrentRealmIndex,
                                                       &u4NextRealmIndex,
                                                       u4CurrentKeyIndex,
                                                       &u4NextKeyIndex);
            }
            if (b1KeyResult == DHCP6_SRV_FOUND)
            {
                if (nmhTestv2FsDhcp6SrvKeyRowStatus (&u4ErrorCode, u4RealmIndex,
                                                     u4KeyIndex,
                                                     DESTROY) == SNMP_SUCCESS)
                {
                    if (nmhSetFsDhcp6SrvKeyRowStatus (u4RealmIndex, u4KeyIndex,
                                                      DESTROY) != SNMP_SUCCESS)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return CLI_FAILURE;
                    }
                }
            }

        }
        else
        {
            if (nmhTestv2FsDhcp6SrvRealmRowStatus (&u4ErrorCode, u4RealmIndex,
                                                   DESTROY) == SNMP_SUCCESS)
            {
                if (nmhSetFsDhcp6SrvRealmRowStatus (u4RealmIndex,
                                                    DESTROY) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }
        }
    }
    if (nmhTestv2FsDhcp6SrvClientRowStatus
        (&u4ErrorCode, u4ClientIndex, NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6SrvClientRowStatus (u4ClientIndex, NOT_IN_SERVICE) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDhcp6SrvClientRowStatus (&u4ErrorCode, u4ClientIndex, ACTIVE)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6SrvClientRowStatus (u4ClientIndex, ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliProcessAddOption                                  */
/*                                                                           */
/* Description  : This function DHCP6 server Option parameters               */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                u4PoolIndex   - DHCP6 Pool Index                           */
/*                u4Type        - Option Code                                */
/*                pu1OptVal     - Option Value                               */
/*                u4Len         - Option Length                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliProcessAddOption (tCliHandle CliHandle, UINT4 u4PoolIndex,
                         UINT4 u4Type, UINT1 *pu1OptValue, UINT4 u4Len,
                         INT4 i4Preference)
{
    tSNMP_OCTET_STRING_TYPE pOctetValue;
    tSNMP_OCTET_STRING_TYPE OptionValue;
    UINT4               u4ErrorCode = 0;
    UINT4               u4OptionIndex = 0;
    UINT4               u4NextPoolIndex = 0;
    UINT4               u4CurrentPoolIndex = 0;
    UINT4               u4NextOptionIndex = 0;
    UINT4               u4CurrentOptionIndex = 0;
    INT4                i4PrevRowStatus = 0;
    INT4                i4RetVal = 0;
    INT4                i4Len = 0;
    INT4                i4OptionType = 0;
    UINT1               au1OptionValue[DHCP6_SRV_OPTION_LENGTH_MAX];
    BOOL1               b1Result = DHCP6_SRV_NOT_FOUND;

    pOctetValue.pu1_OctetList = pu1OptValue;
    /* update octet string length */
    pOctetValue.i4_Length = u4Len;

    MEMSET (&OptionValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    OptionValue.pu1_OctetList = au1OptionValue;

    if (i4Preference == 0)
        i4Preference = 1;

    i4RetVal = nmhGetFirstIndexFsDhcp6SrvOptionTable (&u4NextPoolIndex,
                                                      &u4NextOptionIndex);
    /* Check for Option Exist */
    while ((i4RetVal != SNMP_FAILURE) &&
           (u4NextOptionIndex <= MAX_D6SR_SRV_OPTION_ENTRY))
    {
        if (u4NextPoolIndex == u4PoolIndex)
        {
            MEMSET (au1OptionValue, 0, DHCP6_SRV_OPTION_LENGTH_MAX);

            nmhGetFsDhcp6SrvOptionType (u4NextPoolIndex, u4NextOptionIndex,
                                        &i4OptionType);
            nmhGetFsDhcp6SrvOptionValue (u4NextPoolIndex, u4NextOptionIndex,
                                         &OptionValue);

            i4Len = (OptionValue.i4_Length > (INT4) STRLEN (pu1OptValue)) ?
                OptionValue.i4_Length : (INT4) STRLEN (pu1OptValue);

            if (u4Type == DHCP6_SRV_REFRESH_TIMER_TYPE)
            {
                if (i4OptionType == (INT4) u4Type)
                {
                    u4OptionIndex = u4NextOptionIndex;
                    b1Result = DHCP6_SRV_FOUND;
                    break;
                }
            }
            if ((u4Type == DHCP6_SRV_SIP_IPV6_ADDR_OPTION)
                || (u4Type == DHCP6_SRV_DNS_RECURSIVE_OPTION))
            {
                if ((MEMCMP (pu1OptValue, OptionValue.pu1_OctetList, u4Len) ==
                     0) && (i4OptionType == (INT4) u4Type))
                {
                    /*            u4PoolIndex = u4NextPoolIndex; */
                    u4OptionIndex = u4NextOptionIndex;
                    b1Result = DHCP6_SRV_FOUND;
                    break;
                }
            }
            else
            {
                if ((MEMCMP (pu1OptValue, OptionValue.pu1_OctetList, i4Len) ==
                     0) && (i4OptionType == (INT4) u4Type))
                {
                    /*            u4PoolIndex = u4NextPoolIndex; */
                    u4OptionIndex = u4NextOptionIndex;
                    b1Result = DHCP6_SRV_FOUND;
                    break;
                }
            }
        }
        u4CurrentPoolIndex = u4NextPoolIndex;
        u4CurrentOptionIndex = u4NextOptionIndex;

        i4RetVal = nmhGetNextIndexFsDhcp6SrvOptionTable (u4CurrentPoolIndex,
                                                         &u4NextPoolIndex,
                                                         u4CurrentOptionIndex,
                                                         &u4NextOptionIndex);
    }
    if (b1Result == DHCP6_SRV_FOUND)
    {
        /* Existing row - Updation */
        if (nmhGetFsDhcp6SrvOptionRowStatus
            (u4PoolIndex, u4OptionIndex, &i4PrevRowStatus) == SNMP_FAILURE)
        {
            return CLI_FAILURE;

        }
        if (nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex,
                                             NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Option table is full\r\n ");
            return CLI_FAILURE;
        }
    }
    else
    {
        /* Option Table Creation */
        if (nmhGetFsDhcp6SrvPoolOptionTableNextIndex (u4PoolIndex,
                                                      &u4OptionIndex)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        else
        {
            /* Create new row when row is not existed */
            if (nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex,
                                                 CREATE_AND_WAIT)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Option table is full\r\n ");
                return CLI_FAILURE;
            }
        }
    }
    do
    {
        /*  validate the option type */
        if ((u4Type == DHCP6_SRV_REFRESH_TIMER_TYPE))
        {
            /* if (nmhTestv2FsDhcp6SrvOptionType (&u4ErrorCode, u4PoolIndex,
               u4OptionIndex, u4Type) == SNMP_FAILURE)
               {
               break;
               } */
            /* set the option type */
            if (nmhSetFsDhcp6SrvOptionType (u4PoolIndex, u4OptionIndex,
                                            u4Type) == SNMP_FAILURE)
            {
                break;
            }
        }
        else
        {
            if (nmhTestv2FsDhcp6SrvOptionType (&u4ErrorCode, u4PoolIndex,
                                               u4OptionIndex,
                                               u4Type) == SNMP_FAILURE)
            {
                break;
            }
            /* set the option type */
            if (nmhSetFsDhcp6SrvOptionType (u4PoolIndex, u4OptionIndex,
                                            u4Type) == SNMP_FAILURE)
            {
                break;
            }
        }

        /*  validate the option length */
        if (nmhTestv2FsDhcp6SrvOptionLength (&u4ErrorCode, u4PoolIndex,
                                             u4OptionIndex,
                                             u4Len) == SNMP_FAILURE)
        {
            break;
        }

        /* set the option length */
        if (nmhSetFsDhcp6SrvOptionLength (u4PoolIndex, u4OptionIndex,
                                          u4Len) == SNMP_FAILURE)
        {
            break;
        }

        /*  validate the option value */
        if (nmhTestv2FsDhcp6SrvOptionValue (&u4ErrorCode, u4PoolIndex,
                                            u4OptionIndex,
                                            &pOctetValue) == SNMP_FAILURE)
        {
            break;
        }

        /* set the Option Value */
        if (nmhSetFsDhcp6SrvOptionValue (u4PoolIndex, u4OptionIndex,
                                         &pOctetValue) == SNMP_FAILURE)
        {
            break;
        }

        /*  validate the preference */
        if (nmhTestv2FsDhcp6SrvOptionPreference (&u4ErrorCode, u4PoolIndex,
                                                 u4OptionIndex,
                                                 i4Preference) == SNMP_FAILURE)
        {
            break;
        }

        /* set the parameters */
        if (nmhSetFsDhcp6SrvOptionPreference (u4PoolIndex, u4OptionIndex,
                                              i4Preference) == SNMP_FAILURE)
        {
            break;
        }

        /* make the row status active */
        if (nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex,
                                             ACTIVE) == SNMP_FAILURE)
        {
            break;
        }
        return CLI_SUCCESS;
    }
    while (0);
    CliPrintf (CliHandle, "\r%% Unable to set option \r\n ");
    if (b1Result == DHCP6_SRV_NOT_FOUND)
    {
        nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex, DESTROY);
    }
    else
    {
        nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex,
                                         u4OptionIndex, i4PrevRowStatus);

    }
    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliProcessDeleteOption                               */
/*                                                                           */
/* Description  : This function deletes DHCP6 server Option parameters       */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                u4PoolIndex   - DHCP6 Pool Index                           */
/*                pu1OptVal     - Option Value                               */
/*                u4Len         - Option Length                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliProcessDeleteOption (tCliHandle CliHandle, UINT4 u4PoolIndex,
                            UINT4 u4Type, UINT1 *pu1OptValue, UINT4 u4Len)
{
    tSNMP_OCTET_STRING_TYPE pOctetValue;
    tSNMP_OCTET_STRING_TYPE OptionValue;
    UINT4               u4ErrorCode = 0;
    UINT4               u4OptionIndex = 0;
    UINT4               u4NextPoolIndex = 0;
    UINT4               u4CurrentPoolIndex = 0;
    UINT4               u4NextOptionIndex = 0;
    UINT4               u4CurrentOptionIndex = 0;
    INT4                i4OptionType = 0;
    INT4                i4RetVal = 0;
    UINT4               u4VendorType = 0;
    UINT4               u4VendorInputType = 0;
    INT4                i4Len = 0;
    UINT1               au1OptionValue[DHCP6_SRV_OPTION_LENGTH_MAX];
    UINT1              *pu1Value = NULL;
    BOOL1               b1Result = DHCP6_SRV_NOT_FOUND;

    pOctetValue.pu1_OctetList = pu1OptValue;

    /* update octet string length */
    pOctetValue.i4_Length = u4Len;

    OptionValue.pu1_OctetList = au1OptionValue;

    i4RetVal = nmhGetFirstIndexFsDhcp6SrvOptionTable (&u4NextPoolIndex,
                                                      &u4NextOptionIndex);
    /* Check for Option Exist */
    do
    {
        if ((i4RetVal != SNMP_FAILURE) &&
            (u4NextOptionIndex <= MAX_D6SR_SRV_OPTION_ENTRY)
            && (u4NextPoolIndex == u4PoolIndex))
        {
            MEMSET (au1OptionValue, 0, DHCP6_SRV_OPTION_LENGTH_MAX);

            nmhGetFsDhcp6SrvOptionType (u4NextPoolIndex, u4NextOptionIndex,
                                        &i4OptionType);
            nmhGetFsDhcp6SrvOptionValue (u4NextPoolIndex, u4NextOptionIndex,
                                         &OptionValue);

            i4Len = (OptionValue.i4_Length > (INT4) STRLEN (pu1OptValue)) ?
                OptionValue.i4_Length : (INT4) STRLEN (pu1OptValue);

            if (u4Type == DHCP6_SRV_DNS_RECURSIVE_OPTION ||
                (u4Type == DHCP6_SRV_SIP_IPV6_ADDR_OPTION))
            {
                i4Len = (INT4) u4Len;
            }
            if ((MEMCMP (pu1OptValue, OptionValue.pu1_OctetList, i4Len) == 0) &&
                (i4OptionType == (INT4) u4Type))
            {
                u4PoolIndex = u4NextPoolIndex;
                u4OptionIndex = u4NextOptionIndex;
                b1Result = DHCP6_SRV_FOUND;
                break;
            }
            if (u4Type == DHCP6_SRV_OPTION_VENDOR_OPTS)
            {
                DHCP6_SRV_GET_4BYTE (u4VendorType, OptionValue.pu1_OctetList);

                pu1Value = pOctetValue.pu1_OctetList;
                MEMCPY (&u4VendorInputType, pu1Value, DHCP6_SRV_VENDOR_LEN);
                if (u4VendorType == u4VendorInputType)
                {
                    u4PoolIndex = u4NextPoolIndex;
                    u4OptionIndex = u4NextOptionIndex;
                    b1Result = DHCP6_SRV_FOUND;
                    break;
                }
            }
        }
        u4CurrentPoolIndex = u4NextPoolIndex;
        u4CurrentOptionIndex = u4NextOptionIndex;

    }
    while ((i4RetVal = nmhGetNextIndexFsDhcp6SrvOptionTable (u4CurrentPoolIndex,
                                                             &u4NextPoolIndex,
                                                             u4CurrentOptionIndex,
                                                             &u4NextOptionIndex)));

    if (b1Result == DHCP6_SRV_NOT_FOUND)
    {
        CliPrintf (CliHandle, "\r%% Option Index doesn't exist.\r\n ");
        return CLI_FAILURE;
    }
    else
    {
        if (nmhTestv2FsDhcp6SrvOptionRowStatus (&u4ErrorCode, u4PoolIndex,
                                                u4OptionIndex,
                                                DESTROY) == SNMP_SUCCESS)
        {
            if (nmhSetFsDhcp6SrvOptionRowStatus (u4PoolIndex, u4OptionIndex,
                                                 DESTROY) == SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }
            else
            {
                CLI_FATAL_ERROR (CliHandle);
            }
        }
        else
        {
            CliPrintf (CliHandle, "%%Option can not be deleted. \r\n ");
        }
    }
    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliProcessLinkAddOption                              */
/*                                                                           */
/* Description  : This function DHCP6 server link prefix parameters          */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                u4PoolIndex   - DHCP6 Pool Index                           */
/*                pu1OptVal     - Option Value                               */
/*                u4Len         - Option Length                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliProcessLinkAddOption (tCliHandle CliHandle, UINT4 u4PoolIndex,
                             UINT1 *pu1OptValue, UINT4 u4Len)
{
    tSNMP_OCTET_STRING_TYPE OctetValue;
    UINT4               u4ErrorCode = 0;
    INT4                i4ContextId = 0;
    INT4                i4InPrefixIndex = 0;
    INT4                i4NextContextId = 0;
    INT4                i4NextPrefixIndex = 0;
    INT4                i4RetVal = 0;
    INT4                i4RowStatus = 0;
    UINT1               au1InPrefix[16];
    BOOL1               b1Entry = OSIX_FALSE;

    OctetValue.pu1_OctetList = au1InPrefix;
    OctetValue.i4_Length = 0;

    i4RetVal =
        nmhGetFirstIndexFsDhcp6SrvIncludePrefixTable ((INT4 *) &i4ContextId,
                                                      (INT4 *)
                                                      &i4InPrefixIndex);
    if (i4RetVal == SNMP_SUCCESS)
    {
        i4NextPrefixIndex = i4InPrefixIndex;
        i4NextContextId = i4ContextId;
        do
        {
            if (nmhGetFsDhcp6SrvIncludePrefix (i4NextContextId,
                                               i4NextPrefixIndex,
                                               &OctetValue) != SNMP_SUCCESS)
            {
                continue;
            }
            if (MEMCMP
                (pu1OptValue, OctetValue.pu1_OctetList,
                 OctetValue.i4_Length) == 0)
            {
                b1Entry = OSIX_TRUE;
                break;
            }
            i4InPrefixIndex = i4NextPrefixIndex;
            i4ContextId = i4NextContextId;
        }
        while (nmhGetNextIndexFsDhcp6SrvIncludePrefixTable (i4ContextId,
                                                            &i4NextContextId,
                                                            i4InPrefixIndex,
                                                            &i4NextPrefixIndex)
               != SNMP_FAILURE);
    }
    if (b1Entry == OSIX_TRUE)
    {
        /* check for existing row */
        i4RetVal = nmhGetFsDhcp6SrvIncludePrefixRowStatus (i4ContextId,
                                                           i4InPrefixIndex,
                                                           &i4RowStatus);
        if ((i4RetVal == SNMP_SUCCESS) && (i4RowStatus == ACTIVE))
        {
            /* if row already existed, then change the row status
             * to Not_in_Service */
            if (nmhSetFsDhcp6SrvIncludePrefixRowStatus
                (i4ContextId, i4InPrefixIndex, NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Prefix table is full\r\n ");
                return CLI_FAILURE;
            }
        }
    }
    else
    {
        i4InPrefixIndex = 0;
        if (nmhGetFsDhcp6SrvIncludePrefixTableNextIndex
            ((UINT4 *) &i4InPrefixIndex) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Prefix table is full\r\n ");
            return CLI_FAILURE;
        }
        /* Create new row when row is not existed */
        if (nmhSetFsDhcp6SrvIncludePrefixRowStatus
            (i4ContextId, i4InPrefixIndex, CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Prefix table is full\r\n ");
            return CLI_FAILURE;
        }
        OctetValue.pu1_OctetList = pu1OptValue;
        OctetValue.i4_Length = u4Len;
        if (nmhTestv2FsDhcp6SrvIncludePrefix
            (&u4ErrorCode, i4ContextId, i4InPrefixIndex,
             &OctetValue) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Invalid Value for Prefix Length/Value\r\n ");
            return CLI_FAILURE;
        }
        if (nmhSetFsDhcp6SrvIncludePrefix (i4ContextId, i4InPrefixIndex,
                                           &OctetValue) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Invalid Value for Prefix Length/Value\r\n ");
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsDhcp6SrvIncludePrefixPool
        (&u4ErrorCode, i4ContextId, i4InPrefixIndex,
         u4PoolIndex) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Value for Prefix pool\r\n ");
        return CLI_FAILURE;
    }
    if (nmhSetFsDhcp6SrvIncludePrefixPool (i4ContextId, i4InPrefixIndex,
                                           u4PoolIndex) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Value for Prefix pool\r\n ");
        return CLI_FAILURE;
    }
    if (nmhTestv2FsDhcp6SrvIncludePrefixRowStatus (&u4ErrorCode,
                                                   i4ContextId,
                                                   i4InPrefixIndex,
                                                   ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Invalid Value for Prefix Length/Value\r\n ");
        return CLI_FAILURE;
    }
    /* make the row status active */
    if (nmhSetFsDhcp6SrvIncludePrefixRowStatus (i4ContextId, i4InPrefixIndex,
                                                ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvIncludePrefixRowStatus (i4ContextId, i4InPrefixIndex,
                                                DESTROY);
        CliPrintf (CliHandle, "\r%% Invalid Value for Prefix Length/Value\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliProcessLinkDeleteOption                           */
/*                                                                           */
/* Description  : This function deletes DHCP6 server Option parameters       */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                u4PoolIndex   - DHCP6 Pool Index                           */
/*                pu1OptVal     - Option Value                               */
/*                u4Len         - Option Length                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliProcessLinkDeleteOption (tCliHandle CliHandle, UINT4 u4PoolIndex,
                                UINT1 *pu1OptValue, UINT4 u4Len)
{
    tSNMP_OCTET_STRING_TYPE OctetValue;
    UINT4               u4ErrorCode = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4InPrefixIndex = 0;
    INT4                i4RetVal = 0;
    UINT1               au1InPrefix[16];

    UNUSED_PARAM (u4PoolIndex);
    UNUSED_PARAM (u4Len);

    OctetValue.pu1_OctetList = au1InPrefix;
    OctetValue.i4_Length = 0;

    i4RetVal =
        nmhGetFirstIndexFsDhcp6SrvIncludePrefixTable ((INT4 *) &u4ContextId,
                                                      (INT4 *)
                                                      &u4InPrefixIndex);
    if (i4RetVal != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Invalid Value for Prefix Length/Value\r\n");
    }
    do
    {
        if (nmhGetFsDhcp6SrvIncludePrefix (u4ContextId,
                                           u4InPrefixIndex,
                                           &OctetValue) != SNMP_SUCCESS)
        {
            continue;
        }
        if (MEMCMP (pu1OptValue, OctetValue.pu1_OctetList, OctetValue.i4_Length)
            == 0)
        {
            break;
        }
    }
    while (nmhGetNextIndexFsDhcp6SrvIncludePrefixTable (u4ContextId,
                                                        (INT4 *) &u4ContextId,
                                                        u4InPrefixIndex,
                                                        (INT4 *)
                                                        &u4InPrefixIndex) !=
           SNMP_FAILURE);

    if (nmhTestv2FsDhcp6SrvIncludePrefixRowStatus (&u4ErrorCode,
                                                   u4ContextId,
                                                   u4InPrefixIndex,
                                                   DESTROY) == SNMP_SUCCESS)
    {
        /* make the row status destroy */
        if (nmhSetFsDhcp6SrvIncludePrefixRowStatus
            (u4ContextId, u4InPrefixIndex, DESTROY) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliProcessAddSubOption                               */
/*                                                                           */
/* Description  : This function DHCP6 server Option parameters               */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                u4PoolIndex   - DHCP6 Pool Index                           */
/*                u4VendorIndex - DHCP6 Vendor Index                         */
/*                u4Type        - Option Code                                */
/*                pu1OptVal     - Option Value                               */
/*                u4Len         - Option Length                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliProcessAddSubOption (tCliHandle CliHandle, UINT4 u4PoolIndex,
                            UINT4 u4OptionIndex, UINT4 u4Type,
                            UINT1 *pu1OptValue, UINT4 u4Len)
{
    tSNMP_OCTET_STRING_TYPE pOctetValue;
    tSNMP_OCTET_STRING_TYPE SubOptionValue;
    UINT4               u4ErrorCode = 0;
    UINT4               u4NextPoolIndex = 0;
    UINT4               u4CurrentPoolIndex = 0;
    UINT4               u4NextOptionIndex = 0;
    UINT4               u4CurrentOptionIndex = 0;
    UINT4               u4NextOptionType = 0;
    UINT4               u4CurrentOptionType = 0;
    UINT1               au1SubOptionValue[DHCP6_SRV_OPTION_LENGTH_MAX];
    INT4                i4RetVal = 0;
    INT4                i4PreRowStatus = 0;
    BOOL1               b1Result = DHCP6_SRV_NOT_FOUND;

    pOctetValue.pu1_OctetList = pu1OptValue;

    pOctetValue.i4_Length = u4Len;

    SubOptionValue.pu1_OctetList = au1SubOptionValue;

    i4RetVal = nmhGetFirstIndexFsDhcp6SrvSubOptionTable (&u4NextPoolIndex,
                                                         &u4NextOptionIndex,
                                                         &u4NextOptionType);
    /* Check for Option Exist */
    while (i4RetVal != SNMP_FAILURE)
    {
        if ((u4NextPoolIndex == u4PoolIndex) &&
            (u4NextOptionIndex == u4OptionIndex) &&
            (u4Type == u4NextOptionType))
        {
            MEMSET (au1SubOptionValue, 0, DHCP6_SRV_OPTION_LENGTH_MAX);

            nmhGetFsDhcp6SrvSubOptionValue (u4NextPoolIndex, u4NextOptionIndex,
                                            u4NextOptionType, &SubOptionValue);

            u4PoolIndex = u4NextPoolIndex;
            u4OptionIndex = u4NextOptionIndex;
            u4Type = u4NextOptionType;
            b1Result = DHCP6_SRV_FOUND;
            break;
        }
        u4CurrentPoolIndex = u4NextPoolIndex;
        u4CurrentOptionIndex = u4NextOptionIndex;
        u4CurrentOptionType = u4NextOptionType;

        i4RetVal =
            nmhGetNextIndexFsDhcp6SrvSubOptionTable (u4CurrentPoolIndex,
                                                     &u4NextPoolIndex,
                                                     u4CurrentOptionIndex,
                                                     &u4NextOptionIndex,
                                                     u4CurrentOptionType,
                                                     &u4NextOptionType);
    }

    if (b1Result == DHCP6_SRV_FOUND)
    {
        /* if row already existed, then change the row status to 
         * Not_in_Service */
        if (nmhGetFsDhcp6SrvSubOptionRowStatus
            (u4PoolIndex, u4OptionIndex, u4Type,
             &i4PreRowStatus) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsDhcp6SrvSubOptionRowStatus (u4PoolIndex, u4OptionIndex,
                                                u4Type,
                                                NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {

        /* Create new row when row is not existed */
        if (nmhTestv2FsDhcp6SrvSubOptionRowStatus (&u4ErrorCode, u4PoolIndex,
                                                   u4OptionIndex, u4Type,
                                                   CREATE_AND_WAIT) ==
            SNMP_SUCCESS)
        {
            if (nmhSetFsDhcp6SrvSubOptionRowStatus (u4PoolIndex, u4OptionIndex,
                                                    u4Type,
                                                    CREATE_AND_WAIT) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
        else
        {
            return CLI_FAILURE;
        }
    }
    do
    {
        /*  validate the option length */
        if (nmhTestv2FsDhcp6SrvSubOptionLength (&u4ErrorCode, u4PoolIndex,
                                                u4OptionIndex, u4Type,
                                                u4Len) == SNMP_FAILURE)
        {
            break;
        }

        /* set the Option Value */
        if (nmhSetFsDhcp6SrvSubOptionLength (u4PoolIndex, u4OptionIndex,
                                             u4Type, u4Len) == SNMP_FAILURE)
        {
            break;
        }

        /*  validate the option value */
        if (nmhTestv2FsDhcp6SrvSubOptionValue (&u4ErrorCode, u4PoolIndex,
                                               u4OptionIndex, u4Type,
                                               &pOctetValue) == SNMP_FAILURE)
        {
            break;
        }

        /* set the option value */
        if (nmhSetFsDhcp6SrvSubOptionValue (u4PoolIndex, u4OptionIndex, u4Type,
                                            &pOctetValue) == SNMP_FAILURE)
        {
            break;
        }

        /* make the row status active */
        if (nmhSetFsDhcp6SrvSubOptionRowStatus
            (u4PoolIndex, u4OptionIndex, u4Type, ACTIVE) == SNMP_FAILURE)
        {
            break;
        }

        return CLI_SUCCESS;
    }
    while (0);
    CliPrintf (CliHandle, "\r%% Unable to set sub option \r\n ");
    if (b1Result == DHCP6_SRV_NOT_FOUND)
    {
        nmhSetFsDhcp6SrvSubOptionRowStatus (u4PoolIndex, u4OptionIndex, u4Type,
                                            DESTROY);
    }
    else
    {
        nmhSetFsDhcp6SrvSubOptionRowStatus (u4PoolIndex,
                                            u4OptionIndex, u4Type,
                                            i4PreRowStatus);

    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliProcessDeleteSubOption                            */
/*                                                                           */
/* Description  : This function deletes DHCP6 server Option parameters       */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                u4PoolIndex   - DHCP6 Pool Index                           */
/*                u4VendorIndex - DHCP6 Vendor Index                         */
/*                pu1OptVal     - Option Value                               */
/*                u4Len         - Option Length                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliProcessDeleteSubOption (tCliHandle CliHandle, UINT4 u4PoolIndex,
                               UINT4 u4OptionIndex, UINT4 u4Type,
                               UINT1 *pu1OptValue, UINT4 u4Len)
{
    tSNMP_OCTET_STRING_TYPE SubOptionValue;
    UINT4               u4ErrorCode = 0;
    UINT1               au1SubOptionValue[DHCP6_SRV_OPTION_LENGTH_MAX];
    INT4                i4Len = 0;
    BOOL1               b1Result = DHCP6_SRV_NOT_FOUND;

    UNUSED_PARAM (u4Len);

    SubOptionValue.pu1_OctetList = au1SubOptionValue;

    MEMSET (au1SubOptionValue, 0, DHCP6_SRV_OPTION_LENGTH_MAX);

    nmhGetFsDhcp6SrvSubOptionValue (u4PoolIndex, u4OptionIndex,
                                    u4Type, &SubOptionValue);

    i4Len = (SubOptionValue.i4_Length > (INT4) STRLEN (pu1OptValue)) ?
        SubOptionValue.i4_Length : (INT4) STRLEN (pu1OptValue);

    if (MEMCMP (pu1OptValue, SubOptionValue.pu1_OctetList, i4Len) == 0)
    {
        b1Result = DHCP6_SRV_FOUND;
    }

    if (b1Result == DHCP6_SRV_NOT_FOUND)
    {
        CliPrintf (CliHandle, "\r%% Unable to delete sub option\r\n ");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDhcp6SrvSubOptionRowStatus (&u4ErrorCode, u4PoolIndex,
                                               u4OptionIndex, u4Type,
                                               DESTROY) == SNMP_SUCCESS)
    {
        if (nmhSetFsDhcp6SrvSubOptionRowStatus (u4PoolIndex, u4OptionIndex,
                                                u4Type,
                                                DESTROY) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliSetTraps                                          */
/*                                                                           */
/* Description  : This function set and reset the snmp traps for DHCP6       */
/*                server.                                                    */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                u4TrapValue   - Trap Value                                 */
/*                u1TrapFlag    - Trap Flag (Enable or Disable)              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliSetTraps (tCliHandle CliHandle, UINT4 u4TrapValue, UINT1 u1TrapFlag)
{
    tSNMP_OCTET_STRING_TYPE TrapOption;
    UINT4               u4ErrorCode = 0;
    UINT1               u1TrapOption = 0;

    MEMSET (&TrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    TrapOption.pu1_OctetList = &u1TrapOption;
    TrapOption.i4_Length = 1;

    if (nmhGetFsDhcp6SrvTrapAdminControl (&TrapOption) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Set the corresponding Trap value which is to be set */
    u1TrapOption = TrapOption.pu1_OctetList[0];

    if ((u4TrapValue != 0) || (u1TrapFlag == CLI_DISABLE))
    {
        if (u1TrapFlag == CLI_ENABLE)
        {
            u1TrapOption = (UINT1) (u1TrapOption | u4TrapValue);
        }
        else
        {
            u1TrapOption = (UINT1) (u1TrapOption & u4TrapValue);
        }

        TrapOption.pu1_OctetList[0] = u1TrapOption;
        TrapOption.i4_Length = 1;

        if (nmhTestv2FsDhcp6SrvTrapAdminControl (&u4ErrorCode,
                                                 &TrapOption) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhSetFsDhcp6SrvTrapAdminControl (&TrapOption) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliSetDuidType                                       */
/*                                                                           */
/* Description  : This function set DUID Type on the Interface for DHCP6     */
/*                server.                                                    */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                i4IfIndex       - Interface Index                            */
/*                u4DuidType    - DUID Type                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliSetDuidType (tCliHandle CliHandle, INT4 u4PoolIndex, UINT4 u4DuidType)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsDhcp6SrvPoolDuidType (&u4ErrorCode, u4PoolIndex,
                                         (INT4) u4DuidType) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsDhcp6SrvPoolDuidType (u4PoolIndex,
                                      (INT4) u4DuidType) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliSetDuidInterface                                  */
/*                                                                           */
/* Description  : This function set DUID interface for DHCP6 server.         */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                i4IfIndex       - Interface Index                            */
/*                u4DuidType    - DUID Type                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliSetDuidInterface (tCliHandle CliHandle, INT4 u4PoolIndex, INT4 i4IfIndex)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsDhcp6SrvPoolRowStatus (&u4ErrorCode, u4PoolIndex,
                                          NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Error while setting Pool Interface\r\n ");
        return CLI_FAILURE;
    }
    if (nmhSetFsDhcp6SrvPoolRowStatus (u4PoolIndex,
                                       NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Error while setting Pool Interface\r\n ");
        return CLI_SUCCESS;
    }
    if (nmhTestv2FsDhcp6SrvPoolDuidIfIndex (&u4ErrorCode, u4PoolIndex,
                                            i4IfIndex) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Error while setting Pool Interface\r\n ");
        return CLI_FAILURE;
    }
    if (nmhSetFsDhcp6SrvPoolDuidIfIndex (u4PoolIndex, i4IfIndex) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Error while setting Pool Interface\r\n ");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhTestv2FsDhcp6SrvPoolRowStatus (&u4ErrorCode, u4PoolIndex,
                                          ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Error while setting Pool Interface\r\n ");
        return CLI_FAILURE;
    }
    if (nmhSetFsDhcp6SrvPoolRowStatus (u4PoolIndex, ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Error while setting Pool Interface\r\n ");
        return CLI_SUCCESS;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliSetIfEnable                                       */
/*                                                                           */
/* Description  : This function enable Interface for DHCP6 server.           */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                u4IfIndex     - IfIndex                                    */
/*                pu1PoolName   - Pool name                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliSetIfEnable (tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 *pu1PoolName,
                    UINT1 *pu1Preference)
{
    tSNMP_OCTET_STRING_TYPE PoolName;
    UINT4               u4ErrorCode = 0;
    UINT4               u4PoolIndex = 0;
    UINT4               u4NextPoolIndex = 0;
    UINT4               u4CurrentPoolIndex = 0;
    UINT1               u1Preference = 0;
    UINT1               au1PoolName[DHCP6_SRV_POOL_NAME_MAX];
    INT4                i4RetVal = 0;
    INT4                i4Len = 0;
    INT4                i4RowStatus = 0;
    BOOL1               b1Result = DHCP6_SRV_NOT_FOUND;

    if (pu1PoolName != NULL)
    {
        PoolName.pu1_OctetList = au1PoolName;

        i4RetVal = nmhGetFirstIndexFsDhcp6SrvPoolTable (&u4NextPoolIndex);

        /* Check for Pool Exist */
        while ((i4RetVal != SNMP_FAILURE) &&
               (u4NextPoolIndex <= MAX_D6SR_SRV_POOL_ENTRY))
        {
            MEMSET (au1PoolName, 0, DHCP6_SRV_POOL_NAME_MAX);

            nmhGetFsDhcp6SrvPoolName (u4NextPoolIndex, &PoolName);

            i4Len = (PoolName.i4_Length > (INT4) STRLEN (pu1PoolName)) ?
                PoolName.i4_Length : (INT4) STRLEN (pu1PoolName);

            if (MEMCMP (pu1PoolName, PoolName.pu1_OctetList, i4Len) == 0)
            {
                u4PoolIndex = u4NextPoolIndex;
                b1Result = DHCP6_SRV_FOUND;
                break;
            }
            u4CurrentPoolIndex = u4NextPoolIndex;

            i4RetVal = nmhGetNextIndexFsDhcp6SrvPoolTable (u4CurrentPoolIndex,
                                                           &u4NextPoolIndex);
        }

        if (b1Result == DHCP6_SRV_NOT_FOUND)
        {
            return CLI_FAILURE;
        }
    }
    i4RetVal = nmhGetFsDhcp6SrvIfRowStatus (u4IfIndex, &i4RowStatus);

    /* check for existing row with given index */
    if ((i4RetVal == SNMP_SUCCESS) && (i4RowStatus == ACTIVE))
    {
        /* Chage row status from ACTIVE to NOT IN SERVICE */
        nmhSetFsDhcp6SrvIfRowStatus (u4IfIndex, NOT_IN_SERVICE);
    }
    else
    {
        /* Create new row when row is not existed */
        if (nmhSetFsDhcp6SrvIfRowStatus (u4IfIndex,
                                         CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to create entry in server interface table \r\n ");
            return CLI_FAILURE;
        }
    }
    if (pu1PoolName != NULL)
    {
        if (nmhTestv2FsDhcp6SrvIfPool (&u4ErrorCode, u4IfIndex,
                                       u4PoolIndex) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsDhcp6SrvIfPool (u4IfIndex, u4PoolIndex) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (pu1Preference != NULL)
        {
            u1Preference = *pu1Preference;
        }
        if (nmhSetFsDhcp6SrvPoolRowStatus (u4PoolIndex,
                                           NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Error while setting Pool Interface\r\n ");
            return CLI_SUCCESS;
        }
        /* Set the preference value */
        if (nmhTestv2FsDhcp6SrvPoolPreference (&u4ErrorCode, u4PoolIndex,
                                               u1Preference) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Error while testing preference value\r\n ");
            return CLI_FAILURE;
        }

        if (nmhSetFsDhcp6SrvPoolPreference (u4PoolIndex, u1Preference) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Error while setting preference value\r\n ");
            return CLI_FAILURE;
        }
        if (nmhTestv2FsDhcp6SrvPoolRowStatus (&u4ErrorCode, u4PoolIndex,
                                              ACTIVE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Error while setting Pool Interface\r\n ");
            return CLI_FAILURE;
        }
        if (nmhSetFsDhcp6SrvPoolRowStatus (u4PoolIndex, ACTIVE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Error while setting Pool Interface\r\n ");
            return CLI_SUCCESS;
        }
    }
    if (nmhTestv2FsDhcp6SrvIfRowStatus (&u4ErrorCode, u4IfIndex,
                                        ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6SrvIfRowStatus (u4IfIndex, ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliSetIfDisable                                      */
/*                                                                           */
/* Description  : This function set DUID Type on the Interface for DHCP6     */
/*                server.                                                    */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                i4IfIndex       - Interface Index                            */
/*                u4DuidType    - DUID Type                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliSetIfDisable (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsDhcp6SrvIfRowStatus (u4IfIndex, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Entry not found\r\n ");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDhcp6SrvIfRowStatus (&u4ErrorCode, u4IfIndex,
                                        DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6SrvIfRowStatus (u4IfIndex, DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliSetTrace                                          */
/*                                                                           */
/* Description  : This function set and reset the trace inputs for DHCP6     */
/*                server.                                                    */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                pu1TraceInput - Input Trace string                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliSetTrace (tCliHandle CliHandle, UINT1 *pu1TraceInput)
{

    tSNMP_OCTET_STRING_TYPE TraceInput;
    UINT4               u4ErrorCode = 0;

    MEMSET (&TraceInput, 0, sizeof (TraceInput));

    TraceInput.pu1_OctetList = pu1TraceInput;
    TraceInput.i4_Length = STRLEN (pu1TraceInput);

    if (nmhTestv2FsDhcp6SrvDebugTrace (&u4ErrorCode, &TraceInput) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (DHCP6_SRV_CLI_INV_DEBUG_STRING);
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6SrvDebugTrace (&TraceInput) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliShowServerInfo                                    */
/*                                                                           */
/* Description  : This function shows the server information for DHCP6       */
/*                server.                                                    */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliSetSysLogStatus (tCliHandle CliHandle, INT4 i4SysLogStatus)
{
    UINT4               u4ErrCode = 0;
    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsDhcp6SrvSysLogAdminStatus (&u4ErrCode,
                                              i4SysLogStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsDhcp6SrvSysLogAdminStatus (i4SysLogStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliShowGlobalInfo                                   */
/*                                                                           */
/* Description  : This function shows the server information for DHCP6       */
/*                server.                                                    */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
D6SrCliShowGlobalInfo (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE DhcpTrapOption;
    tSNMP_OCTET_STRING_TYPE ClientName;
    INT4                i4SysLogStatus = 0;
    INT4                i4ListenPort = 0;
    INT4                i4RelplyTxPort = 0;
    INT4                i4RelayRepTxPort = 0;
    INT4                i4RetVal = 0;
    INT4                i4Counter = 0;
    INT4                i4Val = 0;
    UINT4               u4CurrentClntIndex = 0;
    UINT4               u4NextClntIndex = 0;
    UINT4               u4RealmIndex = 0;
    UINT4               u4Key = 0;
    UINT4               u4NextKey = 0;
    UINT4               u4RealmNextIndex = 0;
    UINT1               u1CurTrapOption = 0;
    UINT1               u1FlagValue = OSIX_FALSE;
    UINT1               au1ClntValue[DHCP6_SRV_CLNT_NAME_MAX];
    UINT1               u1Flag = OSIX_FALSE;
    tSNMP_OCTET_STRING_TYPE RealmName;
    tSNMP_OCTET_STRING_TYPE KeyName;
    UINT1               au1RealmValue[DHCP6_SRV_REALM_NAME_MAX];
    UINT1               au1KeyValue[DHCP6_SRV_KEY_SIZE_MAX];

    MEMSET (&RealmName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1RealmValue, 0, DHCP6_SRV_REALM_NAME_MAX);
    RealmName.pu1_OctetList = au1RealmValue;

    MEMSET (&DhcpTrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (&ClientName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ClntValue, 0, DHCP6_SRV_CLNT_NAME_MAX);
    ClientName.pu1_OctetList = au1ClntValue;

    MEMSET (&KeyName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1KeyValue, 0, DHCP6_SRV_KEY_SIZE_MAX);
    KeyName.pu1_OctetList = au1KeyValue;

    DhcpTrapOption.pu1_OctetList = &u1CurTrapOption;
    DhcpTrapOption.i4_Length = 1;

    /*Show Global Information of Client */
    nmhGetFsDhcp6SrvTrapAdminControl (&DhcpTrapOption);
    u1CurTrapOption = DhcpTrapOption.pu1_OctetList[0];

    nmhGetFsDhcp6SrvSysLogAdminStatus (&i4SysLogStatus);
    nmhGetFsDhcp6SrvListenPort (&i4ListenPort);
    nmhGetFsDhcp6SrvClientTransmitPort (&i4RelplyTxPort);
    nmhGetFsDhcp6SrvRelayTransmitPort (&i4RelayRepTxPort);

    CliPrintf (CliHandle, "\n");
    CliPrintf (CliHandle, "Server information:\r\n");
    CliPrintf (CliHandle, "  Listen UDP port                       : %d\r\n",
               i4ListenPort);
    CliPrintf (CliHandle, "  Client Transmit UDP port              : %d\r\n",
               i4RelplyTxPort);
    CliPrintf (CliHandle, "  Relay Transmit UDP port               : %d\r\n",
               i4RelayRepTxPort);
    CliPrintf (CliHandle, "  Sys log status                        : ");
    if (i4SysLogStatus == DHCP6_SRV_SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "enabled\t\n");
    }
    else
    {
        CliPrintf (CliHandle, "disabled\t\n");
    }

    CliPrintf (CliHandle, "  SNMP traps                            : ");
    if (u1CurTrapOption != 0)
    {
        if (DHCP6_SRV_INVALID_PKT_REC_TRAP_ENABLED (u1CurTrapOption) ==
            OSIX_TRUE)
        {
            u1FlagValue = OSIX_TRUE;
            CliPrintf (CliHandle, "invalid-pkt");
        }
        if (DHCP6_SRV_HMAC_FAIL_TRAP_ENABLED (u1CurTrapOption) == OSIX_TRUE)
        {
            if (u1FlagValue == OSIX_TRUE)
            {
                CliPrintf (CliHandle, ",");
            }
            CliPrintf (CliHandle, "auth-fail");
            u1FlagValue = OSIX_TRUE;
        }
        if (DHCP6_SRV_UNKNOWN_TLV_TRAP_ENABLED (u1CurTrapOption) == OSIX_TRUE)
        {
            if (u1FlagValue == OSIX_TRUE)
            {
                CliPrintf (CliHandle, ",");
            }
            CliPrintf (CliHandle, "unknown-tlv");
        }
        CliPrintf (CliHandle, "\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "none\r\n\n");
    }
    CliPrintf (CliHandle, "  Authentication Information: ");

    i4RetVal = nmhGetFirstIndexFsDhcp6SrvClientTable (&u4CurrentClntIndex);
    while (i4RetVal == SNMP_SUCCESS)
    {
        u1Flag = OSIX_TRUE;
        MEMSET (&ClientName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1ClntValue, 0, DHCP6_SRV_CLNT_NAME_MAX);
        ClientName.pu1_OctetList = au1ClntValue;

        nmhGetFsDhcp6SrvClientId (u4CurrentClntIndex, &ClientName);
        CliPrintf (CliHandle, "\n    Client DUID : ");

        for (i4Counter = 0; i4Counter < ClientName.i4_Length; i4Counter++)
        {
            if ((i4Counter != 0) && (i4Counter % DHCP6_SRV_28LENGTH_VAL == 0))
            {
                CliPrintf (CliHandle, "\n                  ");
            }
            CliPrintf (CliHandle, "%c", *(ClientName.pu1_OctetList));
            ClientName.pu1_OctetList = ClientName.pu1_OctetList + 1;
        }
        if (nmhGetFsDhcp6SrvClientRealm (u4CurrentClntIndex, &u4RealmIndex) ==
            SNMP_SUCCESS)
        {
            nmhGetFsDhcp6SrvRealmName (u4RealmIndex, &RealmName);
            if (RealmName.i4_Length != 0)
            {
                CliPrintf (CliHandle, "\n      Realm Name  : %s\r\n",
                           RealmName.pu1_OctetList);
            }
            u4Key = 1;
            if (nmhValidateIndexInstanceFsDhcp6SrvKeyTable (u4RealmIndex, u4Key)
                == SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "        Key Value      : %d\r\n", u4Key);
                nmhGetFsDhcp6SrvKey (u4RealmIndex, u4Key, &KeyName);
                if (KeyName.i4_Length != 0)
                {
                    CliPrintf (CliHandle, "        Key Identifier : ");
                    for (i4Counter = 0; i4Counter < KeyName.i4_Length;
                         i4Counter++)
                    {
                        if ((i4Counter != 0)
                            && (i4Counter % DHCP6_SRV_16LENGTH_VAL == 0))
                        {
                            CliPrintf (CliHandle,
                                       "\n                         ");
                        }
                        if (i4Counter == KeyName.i4_Length - 1)
                        {
                            CliPrintf (CliHandle, "%02x",
                                       *(KeyName.pu1_OctetList));
                        }
                        else
                        {
                            CliPrintf (CliHandle, "%02x:",
                                       *(KeyName.pu1_OctetList));
                        }

                        KeyName.pu1_OctetList = KeyName.pu1_OctetList + 1;
                    }
                    CliPrintf (CliHandle, "\n");
                }
            }
            i4Val =
                nmhGetNextIndexFsDhcp6SrvKeyTable (u4RealmIndex,
                                                   &u4RealmNextIndex, u4Key,
                                                   &u4NextKey);

            while ((i4Val == SNMP_SUCCESS)
                   && (u4RealmIndex == u4RealmNextIndex))
            {
                CliPrintf (CliHandle, "        Key Value      : %d\r\n",
                           u4NextKey);

                MEMSET (&KeyName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
                MEMSET (au1KeyValue, 0, DHCP6_SRV_KEY_SIZE_MAX);
                KeyName.pu1_OctetList = au1KeyValue;

                nmhGetFsDhcp6SrvKey (u4RealmIndex, u4NextKey, &KeyName);
                if (KeyName.i4_Length != 0)
                {
                    CliPrintf (CliHandle, "        Key Identifier : ");
                    for (i4Counter = 0; i4Counter < KeyName.i4_Length;
                         i4Counter++)
                    {
                        if ((i4Counter != 0)
                            && (i4Counter % DHCP6_SRV_16LENGTH_VAL == 0))
                        {
                            CliPrintf (CliHandle,
                                       "\n                         ");
                        }
                        if (i4Counter == KeyName.i4_Length - 1)
                        {
                            CliPrintf (CliHandle, "%02x",
                                       *(KeyName.pu1_OctetList));
                        }
                        else
                        {
                            CliPrintf (CliHandle, "%02x:",
                                       *(KeyName.pu1_OctetList));
                        }
                        KeyName.pu1_OctetList = KeyName.pu1_OctetList + 1;
                    }
                    CliPrintf (CliHandle, "\n");
                }
                u4Key = u4NextKey;
                i4Val = nmhGetNextIndexFsDhcp6SrvKeyTable
                    (u4RealmIndex, &u4RealmNextIndex, u4Key, &u4NextKey);
            }

        }

        i4RetVal = nmhGetNextIndexFsDhcp6SrvClientTable
            (u4CurrentClntIndex, &u4NextClntIndex);
        u4CurrentClntIndex = u4NextClntIndex;
    }
    if (u1Flag == OSIX_FALSE)
    {
        CliPrintf (CliHandle, " - \n");
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliShowSrvGlobalInfo                                  */
/*                                                                           */
/* Description  : This function shows the server information for DHCP6       */
/*                server.                                                    */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliShowSrvGlobalInfo (tCliHandle CliHandle)
{
    D6SrCliShowGlobalInfo (CliHandle);
    D6SrMainUnLock ();
    CliUnRegisterLock (CliHandle);

#ifdef DHCP6_CLNT_WANTED
    D6SrApiShowGlobalInfo (CliHandle);
#endif

#ifdef DHCP6_RLY_WANTED
    D6RlApiShowGlobalInfo (CliHandle);
#endif
    CliRegisterLock (CliHandle, D6SrMainLock, D6SrMainUnLock);
    if (D6SrMainLock () == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "%%DHCP6 Server Locking Failed - Command not Executed\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliShowPoolConf                                       */
/*                                                                           */
/* Description  : This function shows pool information of pool name for DHCP6 */
/*                server given by user.                                      */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliShowPoolConf (tCliHandle CliHandle, INT1 *pi1PoolName)
{
    tSNMP_OCTET_STRING_TYPE PoolName;
    UINT4               u4CurrentPoolIndex = 0;
    UINT4               u4NextPoolIndex = 0;
    INT4                i4RetVal = 0;
    UINT1               au1RetPoolName[DHCP6_SRV_POOL_NAME_MAX];
    UINT1               u1FlagValue = 0;

    MEMSET (&PoolName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1RetPoolName, 0, DHCP6_SRV_POOL_NAME_MAX);
    PoolName.pu1_OctetList = au1RetPoolName;
    PoolName.i4_Length = STRLEN (au1RetPoolName);

    i4RetVal = nmhGetFirstIndexFsDhcp6SrvPoolTable (&u4NextPoolIndex);

    while (i4RetVal != SNMP_FAILURE)
    {
        if (nmhGetFsDhcp6SrvPoolName (u4NextPoolIndex, &PoolName) ==
            SNMP_SUCCESS)
        {
            if (MEMCMP (PoolName.pu1_OctetList, pi1PoolName, PoolName.i4_Length)
                == 0)
            {
                u1FlagValue = OSIX_TRUE;
                D6SrCliShowPoolInfo (CliHandle, u4NextPoolIndex);
                break;
            }
        }
        u4CurrentPoolIndex = u4NextPoolIndex;
        i4RetVal = nmhGetNextIndexFsDhcp6SrvPoolTable (u4CurrentPoolIndex,
                                                       &u4NextPoolIndex);
    }
    if (u1FlagValue != OSIX_TRUE)
    {
        CliPrintf (CliHandle, "%%Pool Not Configured \r\n");

    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliShowAllPoolInfo                                   */
/*                                                                           */
/* Description  : This function shows all the pool information for DHCP6     */
/*                server.                                                    */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliShowAllPoolInfo (tCliHandle CliHandle)
{
    UINT4               u4CurrentPoolIndex = 0;
    UINT4               u4NextPoolIndex = 0;
    INT4                i4RetVal = 0;

    i4RetVal = nmhGetFirstIndexFsDhcp6SrvPoolTable (&u4NextPoolIndex);

    while (i4RetVal != SNMP_FAILURE)
    {
        D6SrCliShowPoolInfo (CliHandle, u4NextPoolIndex);

        u4CurrentPoolIndex = u4NextPoolIndex;

        i4RetVal = nmhGetNextIndexFsDhcp6SrvPoolTable (u4CurrentPoolIndex,
                                                       &u4NextPoolIndex);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliShowPoolInfo                                      */
/*                                                                           */
/* Description  : This function shows all the pool information for DHCP6     */
/*                server.                                                    */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliShowPoolInfo (tCliHandle CliHandle, UINT4 u4PoolIndex)
{
    tSNMP_OCTET_STRING_TYPE PoolName;
    tSNMP_OCTET_STRING_TYPE PoolDuid;
    tSNMP_OCTET_STRING_TYPE PrefAddress;
    tSNMP_OCTET_STRING_TYPE OptionValue;
    UINT1               au1RetPoolName[DHCP6_SRV_POOL_NAME_MAX];
    UINT1               au1PoolDuid[DHCP6_SRV_DUID_SIZE_MAX];
    INT4                i4PoolPreference = 0;
    INT4                i4PoolDuidType = 0;
    INT4                i4RowStatus = 0;
    INT4                i4Counter = 0;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4PoolIndex = 0;
    INT4                i4RetVal = 0;
    INT4                i4NextPrefixIndex = 0;
    INT4                i4CurrContextId = 0;
    INT4                i4NextCurrContextId = 0;
    INT4                i4CurrPrefixIndex = 0;
    UINT4               u4NextPoolIndex = 0;
    INT4                i4SrvOptionType = 0;
    INT4                i4SrvOptionLen = 0;
    UINT4               u4NextSrvOptionIndex = 0;
    UINT4               u4RefreshTime = 0;
    UINT4               u4VendorType = 0;
    UINT1               au1IfName[DHCP6_SRV_MAX_IF_NAME_LEN];
    UINT1               u1Flag = OSIX_FALSE;
    UINT1               u1Count = 0;
    UINT1               au1PrefixAdd[DHCP6_SRV_IP6_ADDRESS_SIZE_MAX];
    UINT1               au1OptionValue[DHCP6_SRV_OPTION_LENGTH_MAX];

    MEMSET (&PoolName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PoolDuid, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PrefAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1PrefixAdd, 0, DHCP6_SRV_IP6_ADDRESS_SIZE_MAX);
    MEMSET (au1RetPoolName, 0, DHCP6_SRV_POOL_NAME_MAX);
    MEMSET (au1PoolDuid, 0, DHCP6_SRV_DUID_SIZE_MAX);
    MEMSET (&OptionValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OptionValue, 0, DHCP6_SRV_OPTION_LENGTH_MAX);
    OptionValue.pu1_OctetList = au1OptionValue;

    PoolName.pu1_OctetList = au1RetPoolName;
    PoolName.i4_Length = STRLEN (au1RetPoolName);

    PrefAddress.pu1_OctetList = au1PrefixAdd;
    PrefAddress.i4_Length = 0;

    PoolDuid.pu1_OctetList = au1PoolDuid;
    PoolDuid.i4_Length = STRLEN (au1PoolDuid);

    CliPrintf (CliHandle, "\nPool : ");

    nmhGetFsDhcp6SrvPoolName (u4PoolIndex, &PoolName);
    for (i4Counter = 0; i4Counter < PoolName.i4_Length; i4Counter++)
    {
        CliPrintf (CliHandle, "%c", *(PoolName.pu1_OctetList));
        PoolName.pu1_OctetList = PoolName.pu1_OctetList + 1;
    }
    CliPrintf (CliHandle, "\r\n");
    nmhGetFsDhcp6SrvPoolRowStatus (u4PoolIndex, &i4RowStatus);
    if (i4RowStatus == ACTIVE)
    {
        CliPrintf (CliHandle, "  ACTIVE\n\n");
    }
    else
    {
        CliPrintf (CliHandle, "  IN-ACTIVE\n\n");
        return CLI_FAILURE;
    }
    nmhGetFsDhcp6SrvPoolDuidType (u4PoolIndex, &i4PoolDuidType);

    CliPrintf (CliHandle, "  DHCPv6 unique type(DUID Type) : ");
    switch (i4PoolDuidType)
    {
        case CLI_DHCP6_SRV_DUID_LLT:
            CliPrintf (CliHandle, "Link-layer Address Plus Time\r\n");
            break;
        case CLI_DHCP6_SRV_DUID_EN:
            CliPrintf (CliHandle, "Vendor Based on Enterprise Number\r\n");
            break;
        case CLI_DHCP6_SRV_DUID_LL:
            CliPrintf (CliHandle, "Link-layer Address \r\n");
            break;
        default:
            break;
    }
    CliPrintf (CliHandle, "  DHCPv6 unique identifier(DUID): ");
    nmhGetFsDhcp6SrvPoolDuid (u4PoolIndex, &PoolDuid);

    for (i4Counter = 0; i4Counter < PoolDuid.i4_Length; i4Counter++)
    {
        if ((i4Counter != 0) && (i4Counter % DHCP6_SRV_20LENGTH_VAL == 0))
        {
            CliPrintf (CliHandle, "\n                                  ");
        }
        CliPrintf (CliHandle, "%02x", *(PoolDuid.pu1_OctetList));
        PoolDuid.pu1_OctetList = PoolDuid.pu1_OctetList + 1;
    }
    CliPrintf (CliHandle, "\n");

    nmhGetFsDhcp6SrvPoolPreference (u4PoolIndex, &i4PoolPreference);
    CliPrintf (CliHandle, "  Preference             : %d \r\n",
               i4PoolPreference);

    i4RetVal = nmhGetFirstIndexFsDhcp6SrvIfTable (&i4IfIndex);
    i4NextIfIndex = i4IfIndex;
    u1Count = 0;
    CliPrintf (CliHandle, "  Associated Interfaces  : ");
    while (i4RetVal == SNMP_SUCCESS)
    {
        if (nmhGetFsDhcp6SrvIfPool (i4NextIfIndex, &i4PoolIndex) ==
            SNMP_SUCCESS)
        {
            if (u4PoolIndex == (UINT4) i4PoolIndex)
            {
                if (D6SrUtlGetIfName (i4NextIfIndex, au1IfName) == OSIX_SUCCESS)
                {
                    if ((u1Count != 0)
                        && (u1Count % DHCP6_SRV_6LENGTH_VAL == 0))
                    {
                        CliPrintf (CliHandle, "\n                           ");
                        u1Flag = OSIX_FALSE;
                    }
                    if (u1Flag == OSIX_TRUE)
                    {
                        CliPrintf (CliHandle, ", ");
                    }
                    CliPrintf (CliHandle, "%s", au1IfName);
                    u1Flag = OSIX_TRUE;
                    u1Count = (UINT1) (u1Count + 1);
                }
            }
        }
        i4IfIndex = i4NextIfIndex;
        i4RetVal = nmhGetNextIndexFsDhcp6SrvIfTable (i4IfIndex, &i4NextIfIndex);

    }
    CliPrintf (CliHandle, "\r\n");
    i4RetVal = SNMP_FAILURE;

    i4RetVal =
        nmhGetFirstIndexFsDhcp6SrvIncludePrefixTable (&i4CurrContextId,
                                                      &i4CurrPrefixIndex);
    i4NextPrefixIndex = i4CurrPrefixIndex;
    i4NextCurrContextId = i4CurrContextId;
    u1Count = 0;
    i4PoolIndex = 0;
    CliPrintf (CliHandle, "  Associated IPv6 Prefix : ");
    u1Flag = OSIX_FALSE;
    while (i4RetVal == SNMP_SUCCESS)
    {
        if (nmhGetFsDhcp6SrvIncludePrefixPool
            (i4NextCurrContextId, i4NextPrefixIndex,
             &i4PoolIndex) == SNMP_SUCCESS)
        {
            if ((u4PoolIndex == (UINT4) i4PoolIndex)
                &&
                (nmhGetFsDhcp6SrvIncludePrefix
                 (i4NextCurrContextId, i4NextPrefixIndex,
                  &PrefAddress) == SNMP_SUCCESS))
            {
                if (PrefAddress.i4_Length != 0)
                {
                    if ((u1Count != 0)
                        && (u1Count % DHCP6_SRV_2LENGTH_VAL == 0))
                    {
                        CliPrintf (CliHandle, "\n                           ");
                        u1Flag = OSIX_FALSE;
                    }
                    if (u1Flag == OSIX_TRUE)
                    {
                        CliPrintf (CliHandle, ", ");
                    }
                    Dhcp6SCliPrintIpAdd (CliHandle, PrefAddress.pu1_OctetList);
                    u1Flag = OSIX_TRUE;
                    u1Count = (UINT1) (u1Count + 1);
                }
            }
        }
        i4CurrPrefixIndex = i4NextPrefixIndex;
        i4CurrContextId = i4NextCurrContextId;
        MEMSET (au1PrefixAdd, 0, DHCP6_SRV_IP6_ADDRESS_SIZE_MAX);
        PrefAddress.pu1_OctetList = au1PrefixAdd;
        PrefAddress.i4_Length = 0;
        i4RetVal = nmhGetNextIndexFsDhcp6SrvIncludePrefixTable
            (i4CurrContextId, &i4NextCurrContextId, i4CurrPrefixIndex,
             &i4NextPrefixIndex);
    }
    CliPrintf (CliHandle, "\r\n\n");

    i4RetVal = 0;

    i4RetVal = nmhGetFirstIndexFsDhcp6SrvOptionTable
        (&u4NextPoolIndex, &u4NextSrvOptionIndex);
    while (i4RetVal == SNMP_SUCCESS)
    {
        if (u4NextPoolIndex == u4PoolIndex)
        {
            nmhGetFsDhcp6SrvOptionType (u4PoolIndex, u4NextSrvOptionIndex,
                                        &i4SrvOptionType);
            nmhGetFsDhcp6SrvOptionLength (u4PoolIndex, u4NextSrvOptionIndex,
                                          &i4SrvOptionLen);
            MEMSET (&OptionValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            MEMSET (au1OptionValue, 0, DHCP6_SRV_OPTION_LENGTH_MAX);
            OptionValue.pu1_OctetList = au1OptionValue;

            nmhGetFsDhcp6SrvOptionValue (u4PoolIndex, u4NextSrvOptionIndex,
                                         &OptionValue);

            if (OptionValue.i4_Length != 0)
            {
                switch (i4SrvOptionType)
                {
                    case DHCP6_SRV_ORO_SIP_SERVER_D:
                        CliPrintf (CliHandle, "    SIP domain list : ");
                        D6SrCliPrintString
                            (CliHandle, OptionValue.pu1_OctetList,
                             OptionValue.i4_Length);
                        break;
                    case DHCP6_SRV_ORO_SIP_SERVER_A:
                        CliPrintf (CliHandle, "    SIP servers     : ");
                        Dhcp6SCliPrintIpAdd (CliHandle,
                                             OptionValue.pu1_OctetList);
                        break;
                    case DHCP6_SRV_ORO_DNS_SERVERS:
                        CliPrintf (CliHandle, "    DNS servers     : ");
                        Dhcp6SCliPrintIpAdd (CliHandle,
                                             OptionValue.pu1_OctetList);
                        break;
                    case DHCP6_SRV_ORO_DOMAIN_LIST:
                        CliPrintf (CliHandle, "    DNS search list : ");
                        D6SrCliPrintString
                            (CliHandle, OptionValue.pu1_OctetList,
                             OptionValue.i4_Length);
                        break;
                    case DHCP6_SRV_REFRESH_TIMER_TYPE:
                        CliPrintf (CliHandle, "    Refresh Time    : ");
                        DHCP6_SRV_GET_4BYTE (u4RefreshTime,
                                             OptionValue.pu1_OctetList);
                        CliPrintf (CliHandle, "%-4u sec", u4RefreshTime);
                        break;
                    case DHCP6_SRV_OPTION_VENDOR_OPTS:
                        DHCP6_SRV_GET_4BYTE (u4VendorType,
                                             OptionValue.pu1_OctetList);
                        CliPrintf (CliHandle, "    vendor specific : (%04d)",
                                   u4VendorType);
                        D6SrCliPrintSubOptTable
                            (CliHandle, u4PoolIndex, u4NextSrvOptionIndex);
                        break;

                    default:
                        CliPrintf (CliHandle, "    Option (%-02d)     : ",
                                   i4SrvOptionType);
                        D6SrCliPrintOption
                            (CliHandle, OptionValue.pu1_OctetList,
                             OptionValue.i4_Length);
                        break;

                }
            }
            CliPrintf (CliHandle, "\n");
        }
        i4RetVal = nmhGetNextIndexFsDhcp6SrvOptionTable
            (u4NextPoolIndex, &u4NextPoolIndex, u4NextSrvOptionIndex,
             &u4NextSrvOptionIndex);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliPrintOption                                       */
/*                                                                           */
/* Description  : This function will Print the Option Value                  */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                *pu1Buf       - Pointer to Option                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrCliPrintOption (tCliHandle CliHandle, UINT1 *pu1Buf, INT4 i4Value)
{
    INT4                i4Counter = 0;
    for (i4Counter = 0; i4Counter < i4Value; i4Counter++)
    {
        if ((i4Counter != 0) && (i4Counter % DHCP6_SRV_16LENGTH_VAL == 0))
        {
            CliPrintf (CliHandle, "\n                      ");
        }
        if (i4Counter == (i4Value - 1))
        {
            CliPrintf (CliHandle, "%02x", *(pu1Buf));
        }
        else
        {
            CliPrintf (CliHandle, "%02x:", *(pu1Buf));
        }
        pu1Buf = pu1Buf + 1;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliPrintString                                     */
/*                                                                           */
/* Description  : This function will Print the Option Value                  */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                *pu1Buf       - Pointer to Option                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrCliPrintString (tCliHandle CliHandle, UINT1 *pu1Buf, INT4 i4Value)
{
    INT4                i4Counter = 0;

    for (i4Counter = 0; i4Counter < i4Value; i4Counter++)
    {
        if ((i4Counter != 0) && (i4Counter % DHCP6_SRV_50LENGTH_VAL == 0))
        {
            CliPrintf (CliHandle, "\n                      ");
        }
        CliPrintf (CliHandle, "%c", *(pu1Buf));
        pu1Buf = pu1Buf + 1;
    }
    return;
}

   /*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliPrintIpAdd                                        */
/*                                                                           */
/* Description  : This function will Print the Option Value                  */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                *pu1Buf       - Pointer to Option                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
Dhcp6SCliPrintIpAdd (tCliHandle CliHandle, UINT1 *pu1Buf)
{
    UINT1               au1IpAdd[DHCP6_SRV_16LENGTH_VAL];
    INT4                i4Counter = 0;

    MEMSET (&au1IpAdd, 0x00, sizeof (tIp6Addr));

    for (i4Counter = 0; i4Counter < DHCP6_SRV_16LENGTH_VAL; i4Counter++)
    {
        au1IpAdd[i4Counter] = *(pu1Buf);
        pu1Buf = pu1Buf + 1;
    }
    CliPrintf (CliHandle, "%s", Ip6PrintAddr ((tIp6Addr *) (VOID *) au1IpAdd));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliClearAllInterfaceInfo                             */
/*                                                                           */
/* Description  : This function clear all the stats of all the interface in  */
/*                relay mode.                                                */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliClearAllInterfaceInfo (tCliHandle CliHandle)
{
    tDhcp6SrvIfInfo    *pSrvInterfaceInfo = NULL;
    UINT4               u4IfIndex;
    INT4                i4RetVal = 0;

    for (u4IfIndex = 1; u4IfIndex <= DHCP6_SRV_MAX_INTERFACES; u4IfIndex++)
    {
        pSrvInterfaceInfo = D6SrIfGetNode (u4IfIndex);

        if (pSrvInterfaceInfo != NULL)
        {
            i4RetVal = D6SrCliClearInterfaceInfo (CliHandle, (INT4)
                                                  u4IfIndex,
                                                  DHCP6_SRV_CLEAR_COUNTER_INTERFACE);
            pSrvInterfaceInfo = NULL;
        }
    }

    UNUSED_PARAM (i4RetVal);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliShowAllInterfaceStats                               */
/*                                                                           */
/* Description  : This function shows all the Stst information for DHCP6     */
/*                server.                                                    */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliShowAllInterfaceStats (tCliHandle CliHandle)
{
    tDhcp6SrvIfInfo    *pSrvInterfaceInfo = NULL;
    UINT4               u4IfIndex;
    INT4                i4RetVal = 0;

    for (u4IfIndex = 1; u4IfIndex <= DHCP6_SRV_MAX_INTERFACES; u4IfIndex++)
    {
        pSrvInterfaceInfo = D6SrIfGetNode (u4IfIndex);

        if (pSrvInterfaceInfo != NULL)
        {
            i4RetVal = D6SrCliShowInterfaceStats (CliHandle, (INT4) u4IfIndex);
            pSrvInterfaceInfo = NULL;
        }
    }

    UNUSED_PARAM (i4RetVal);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SCliShowAllInterfaceInfo                              */
/*                                                                           */
/* Description  : This function shows all the pool information for DHCP6     */
/*                server.                                                    */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliShowAllInterfaceInfo (tCliHandle CliHandle)
{
    UINT4               u4IfIndex;

    for (u4IfIndex = 1; u4IfIndex <= DHCP6_SRV_MAX_INTERFACES; u4IfIndex++)
    {
        D6SrCliShowIfInfo (CliHandle, (INT4) u4IfIndex);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliClearInterfaceInfo                                 */
/*                                                                           */
/* Description  : This function clear the stats of the interface of DHCP6     */
/*                server.                                                    */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                i4IfIndex     - Interface Index                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliClearInterfaceInfo (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Value)
{
    tDhcp6SrvIfInfo    *pSrvInterfaceInfo = NULL;
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = 0;

    pSrvInterfaceInfo = D6SrIfGetNode (i4IfIndex);

    if (pSrvInterfaceInfo == NULL)
    {
        return CLI_FAILURE;
    }
    if (nmhTestv2FsDhcp6SrvIfRowStatus
        (&u4ErrCode, i4IfIndex, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }
    if (nmhGetFsDhcp6SrvIfRowStatus (i4IfIndex, &i4RetVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsDhcp6SrvIfRowStatus (i4IfIndex, NOT_IN_SERVICE) == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDhcp6SrvIfCounterReset
        (&u4ErrCode, i4IfIndex, (INT4) u4Value) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvIfRowStatus (i4IfIndex, i4RetVal);
        return CLI_FAILURE;
    }

    if (nmhSetFsDhcp6SrvIfCounterReset (i4IfIndex, (INT4) u4Value) ==
        SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvIfRowStatus (i4IfIndex, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhTestv2FsDhcp6SrvIfRowStatus
        (&u4ErrCode, i4IfIndex, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvIfRowStatus (i4IfIndex, i4RetVal);
        return CLI_FAILURE;
    }
    if (nmhSetFsDhcp6SrvIfRowStatus (i4IfIndex, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsDhcp6SrvIfRowStatus (i4IfIndex, i4RetVal);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliShowInterfaceInfo                                */
/*                                                                           */
/* Description  : This function shows all the pool information for DHCP6     */
/*                server.                                                    */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliShowInterfaceInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    tSNMP_OCTET_STRING_TYPE PoolName;
    INT4                i4IfPool = 0;
    INT4                i4PreferancePool = 0;
    UINT1               au1PoolName[DHCP6_SRV_POOL_NAME_MAX];
    INT1                ai1IfName[DHCP6_SRV_MAX_IF_NAME_LEN];

    MEMSET (&PoolName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1PoolName, 0, DHCP6_SRV_POOL_NAME_MAX);
    MEMSET (ai1IfName, 0, DHCP6_SRV_MAX_IF_NAME_LEN);
    PoolName.pu1_OctetList = au1PoolName;

    if (nmhValidateIndexInstanceFsDhcp6SrvIfTable (i4IfIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhGetFsDhcp6SrvIfPool (i4IfIndex, &i4IfPool) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhGetFsDhcp6SrvPoolPreference (i4IfPool, &i4PreferancePool);
    nmhGetFsDhcp6SrvPoolName (i4IfPool, &PoolName);
    if (D6SrUtlCliConfGetIfName (i4IfIndex, ai1IfName) == OSIX_FAILURE)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\n%s is in server mode \r\n", ai1IfName);

    if (i4PreferancePool != 0)
    {
        CliPrintf (CliHandle, "  Preference value : %d\r\n", i4PreferancePool);
    }
    else
    {
        CliPrintf (CliHandle, "  Preference value : -\n");
    }
    CliPrintf (CliHandle, "  Using pool       : ");
    if (PoolName.i4_Length != 0)
    {
        CliPrintf (CliHandle, "%s", (PoolName.pu1_OctetList));
    }
    else
    {
        CliPrintf (CliHandle, "-\n");
    }
    CliPrintf (CliHandle, "\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6SrCliShowIfInfo                                 */
/*                                                                           */
/*     DESCRIPTION      : This function Display Configuration Information   */
/*                        Interface of client .                              */
/*                                                                           */
/*     INPUT            : CliHandle --CLI handle                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
D6SrCliShowIfInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    D6SrCliShowInterfaceInfo (CliHandle, i4IfIndex);

    CliUnRegisterLock (CliHandle);
    D6SrMainUnLock ();

#ifdef DHCP6_CLNT_WANTED
    D6ClApiShowInterfaceInfo (CliHandle, i4IfIndex);
#endif
#ifdef DHCP6_RLY_WANTED
    D6RlApiShowInterfaceInfo (CliHandle, i4IfIndex);
#endif
    CliRegisterLock (CliHandle, D6SrMainLock, D6SrMainUnLock);
    if (D6SrMainLock () == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "%%DHCP6 Server Locking Failed - Command not Executed\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliShowInterfaceStats                                 */
/*                                                                           */
/* Description  : This function shows all the pool information for DHCP6     */
/*                server.                                                    */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CLI_SUCCESS/CLI_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
D6SrCliShowInterfaceStats (tCliHandle CliHandle, INT4 i4IfIndex)
{
    INT4                i4IfPool = 0;
    UINT4               u4IfInformIn = 0;
    UINT4               u4IfRelayForwIn = 0;
    UINT4               u4IfReplyOut = 0;
    UINT4               u4IfRelayReplyOut = 0;
    UINT4               u4IfInvalidPktIn = 0;
    INT4                i4IfUnknownTlvType = 0;
    UINT4               u4IfHmacFailCount = 0;
    INT1                ai1IfName[DHCP6_SRV_MAX_IF_NAME_LEN];

    if (nmhValidateIndexInstanceFsDhcp6SrvIfTable (i4IfIndex) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Interface Does exists in server mode.\r\n");
        return CLI_FAILURE;
    }
    MEMSET (ai1IfName, 0, DHCP6_SRV_MAX_IF_NAME_LEN);
    nmhGetFsDhcp6SrvIfPool (i4IfIndex, &i4IfPool);

    nmhGetFsDhcp6SrvIfInformIn (i4IfIndex, &u4IfInformIn);

    nmhGetFsDhcp6SrvIfRelayForwIn (i4IfIndex, &u4IfRelayForwIn);

    nmhGetFsDhcp6SrvIfReplyOut (i4IfIndex, &u4IfReplyOut);

    nmhGetFsDhcp6SrvIfRelayReplyOut (i4IfIndex, &u4IfRelayReplyOut);

    nmhGetFsDhcp6SrvIfInvalidPktIn (i4IfIndex, &u4IfInvalidPktIn);

    nmhGetFsDhcp6SrvIfUnknownTlvType (i4IfIndex, &i4IfUnknownTlvType);

    nmhGetFsDhcp6SrvIfHmacFailCount (i4IfIndex, &u4IfHmacFailCount);

    if (D6SrUtlCliConfGetIfName (i4IfIndex, ai1IfName) == OSIX_FAILURE)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\n %s \n", ai1IfName);
    CliPrintf (CliHandle, "  Transmitted:\r\n");
    CliPrintf (CliHandle, "    reply       : %d \r\n", u4IfReplyOut);
    CliPrintf (CliHandle, "    relay-reply : %d\r\n\n", u4IfRelayReplyOut);
    CliPrintf (CliHandle, "  Received:\r\n");
    CliPrintf (CliHandle, "    information-request : %d\r\n", u4IfInformIn);
    CliPrintf (CliHandle, "    relay-forward       : %d\r\n", u4IfRelayForwIn);
    CliPrintf (CliHandle, "    invalid             : %d\r\n", u4IfInvalidPktIn);
    CliPrintf (CliHandle, "    hmac-failure        : %d\r\n",
               u4IfHmacFailCount);
    CliPrintf (CliHandle, "    lastUnknownTlv      : %d\r\n",
               i4IfUnknownTlvType);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : D6SrCliShowRunningConfig                               */
/*                                                                           */
/* Description      : This function shows current running configuration of   */
/*                    DHCP6 Server.                                          */
/*                                                                           */
/* Input Parameters :  CliHandle - CliContext ID                             */
/*                     u4Module - module Value                               */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
INT4
D6SrCliShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    UNUSED_PARAM (u4Module);
    CliRegisterLock (CliHandle, D6SrMainLock, D6SrMainUnLock);

    D6SrMainLock ();

    if (D6SrCliShowRunningScalars (CliHandle) == CLI_SUCCESS)
    {
        D6SrCliShowRunningConfigTables (CliHandle);
        D6SrCliShowRunningConfigInterface (CliHandle);
    }

    D6SrMainUnLock ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : D6SrCliShowRunningConfigInterface                  */
/*                                                                           */
/* Description      : This function shows current running configuration of   */
/*                    DHCP6 server Interface .                               */
/*                                                                           */
/* Input Parameters :  CliHandle - CliContext ID                             */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  None                                                  */
/*****************************************************************************/
VOID
D6SrCliShowRunningConfigInterface (tCliHandle CliHandle)
{
    INT4                i4NextPort = 0;
    INT4                i4CurrentPort = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT1                ai1IfName[DHCP6_SRV_MAX_IF_NAME_LEN];
    MEMSET (ai1IfName, 0, DHCP6_SRV_MAX_IF_NAME_LEN);

    i4RetVal = nmhGetFirstIndexFsDhcp6SrvIfTable (&i4NextPort);

    while (i4RetVal != SNMP_FAILURE)
    {
        if (D6SrUtlCliConfGetIfName (i4NextPort, ai1IfName) == OSIX_FAILURE)
        {
            i4CurrentPort = i4NextPort;
            i4RetVal =
                nmhGetNextIndexFsDhcp6SrvIfTable (i4CurrentPort, &i4NextPort);
            continue;
        }

        D6SrMainUnLock ();
        CliUnRegisterLock (CliHandle);

        D6SrCliShowRunningConfigInterfaceDetails (CliHandle, i4NextPort);

        CliRegisterLock (CliHandle, D6SrMainLock, D6SrMainUnLock);
        D6SrMainLock ();

        CliPrintf (CliHandle, "!");
        u4PagingStatus = CliPrintf (CliHandle, "\r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User Presses 'q' at the prompt, so break! */
            break;
        }

        i4CurrentPort = i4NextPort;
        i4RetVal =
            nmhGetNextIndexFsDhcp6SrvIfTable (i4CurrentPort, &i4NextPort);

    }

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : D6SrCliShowRunningConfigInterfaceDetails            */
/*                                                                           */
/*     DESCRIPTION      : This function displays interface objects in DHCP   */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*****************************************************************************/
PUBLIC VOID
D6SrCliShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex)
{

    INT4                i4RowStatus = 0;
    INT4                i4PoolIndex = 0;
    INT4                i4Preference = 0;
    UINT1               au1PoolName[DHCP6_SRV_POOL_NAME_MAX];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    tSNMP_OCTET_STRING_TYPE PoolName;
    MEMSET (&PoolName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1PoolName, 0, DHCP6_SRV_POOL_NAME_MAX);
    PoolName.pu1_OctetList = au1PoolName;

    CliRegisterLock (CliHandle, D6SrMainLock, D6SrMainUnLock);
    D6SrMainLock ();
    do
    {
        if (nmhValidateIndexInstanceFsDhcp6SrvIfTable (i4IfIndex) !=
            SNMP_SUCCESS)
        {
            break;
        }
        nmhGetFsDhcp6SrvIfRowStatus (i4IfIndex, &i4RowStatus);
        if (i4RowStatus != ACTIVE)
        {
            break;
        }

        CliPrintf (CliHandle, "!\r\n");
        CfaCliConfGetIfName (i4IfIndex, (INT1 *) au1IfName);
        CliPrintf (CliHandle, "\n interface %s \n", au1IfName);
        CliPrintf (CliHandle, " ipv6 dhcp server ");
        nmhGetFsDhcp6SrvIfPool (i4IfIndex, &i4PoolIndex);
        if (i4PoolIndex == 0)
        {
            CliPrintf (CliHandle, "\r\n");
            break;
        }
        nmhGetFsDhcp6SrvPoolName (i4PoolIndex, &PoolName);
        if (PoolName.i4_Length == 0)
        {
            break;
        }
        CliPrintf (CliHandle, "%s ", PoolName.pu1_OctetList);
        nmhGetFsDhcp6SrvPoolPreference (i4PoolIndex, &i4Preference);
        if (i4Preference != 0)
        {
            CliPrintf (CliHandle, "preference %d \r\n", i4Preference);
        }
        else
        {
            CliPrintf (CliHandle, "\r\n");
        }
    }
    while (0);

    D6SrMainUnLock ();
    CliUnRegisterLock (CliHandle);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : D6SrCliShowRunningScalars                              */
/*                                                                           */
/* Description      : This function displays scalar objects in Dhcp6 Server  */
/*                    for show running configuration.                        */
/*                                                                           */
/* Input Parameters :  CliHandle - CliContext ID                             */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
INT4
D6SrCliShowRunningScalars (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE DhcpTrapOption;
    UINT1               u1TrapOption = 0;
    UINT1               u1CurTrapOption = 0;
    INT4                i4SysLogStatus = 0;
    INT4                i4ReqListenPort = 0;
    INT4                i4ReplyTransmitPort = 0;
    INT4                i4RlyTransmitPort = 0;

    MEMSET (&DhcpTrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    DhcpTrapOption.pu1_OctetList = &u1TrapOption;
    DhcpTrapOption.i4_Length = 1;

    nmhGetFsDhcp6SrvTrapAdminControl (&DhcpTrapOption);

    u1CurTrapOption = DhcpTrapOption.pu1_OctetList[0];

    if (u1CurTrapOption != 0)
    {
        CliPrintf (CliHandle, " snmp-server enable traps ipv6 dhcp server");
        if (DHCP6_SRV_UNKNOWN_TLV_TRAP_ENABLED (u1CurTrapOption) == OSIX_TRUE)
        {
            CliPrintf (CliHandle, " unknown-tlv");
        }

        if (DHCP6_SRV_INVALID_PKT_REC_TRAP_ENABLED (u1CurTrapOption) ==
            OSIX_TRUE)
        {
            CliPrintf (CliHandle, " invalid-pkt");
        }
        if (DHCP6_SRV_HMAC_FAIL_TRAP_ENABLED (u1CurTrapOption) == OSIX_TRUE)
        {
            CliPrintf (CliHandle, " auth-fail");
        }
        CliPrintf (CliHandle, "\r\n");
    }
    nmhGetFsDhcp6SrvSysLogAdminStatus (&i4SysLogStatus);
    nmhGetFsDhcp6SrvListenPort (&i4ReqListenPort);
    nmhGetFsDhcp6SrvClientTransmitPort (&i4ReplyTransmitPort);
    nmhGetFsDhcp6SrvRelayTransmitPort (&i4RlyTransmitPort);

    if (i4SysLogStatus == (INT4) DHCP6_SRV_SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "ipv6 dhcp server syslog enable\r\n");
    }
    if (i4ReqListenPort != (INT4) DHCP6_SRV_LISTEN_PORT_DEF)
    {
        CliPrintf (CliHandle, "ipv6 dhcp server port listen %d\r\n",
                   i4ReqListenPort);
    }
    if (i4ReplyTransmitPort != (INT4) DHCP6_SRV_CLNT_TRANSMIT_PORT_DEF)
    {
        CliPrintf (CliHandle,
                   "ipv6 dhcp server port client transmit %d\r\n",
                   i4ReplyTransmitPort);
    }
    if (i4RlyTransmitPort != (INT4) DHCP6_SRV_RLY_TRANSMIT_PORT_DEF)
    {
        CliPrintf (CliHandle, "ipv6 dhcp server port relay transmit %d\r\n",
                   i4RlyTransmitPort);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : D6SrCliShowRunningConfigTables                         */
/*                                                                           */
/* Description      : This function displays table objects in Dhcp6 Server   */
/*                    for show running configuration.                        */
/*                                                                           */
/* Input Parameters :  CliHandle - CliContext ID                             */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
INT4
D6SrCliShowRunningConfigTables (tCliHandle CliHandle)
{
    tDhcp6SrvClientInfo *pClientInfo = NULL;
    tDhcp6SrvRealmInfo *pRealmInfo = NULL;
    tDhcp6SrvKeyInfo   *pKeyInfo = NULL;
    tSNMP_OCTET_STRING_TYPE PoolName;
    tSNMP_OCTET_STRING_TYPE OptionValue;
    tSNMP_OCTET_STRING_TYPE ClientName;
    tSNMP_OCTET_STRING_TYPE RealmName;
    tSNMP_OCTET_STRING_TYPE KeyName;
    tSNMP_OCTET_STRING_TYPE PrefAddress;
    UINT4               u4NextPoolIndex = 0;
    UINT4               u4NextOptionIndex = 0;
    UINT4               u4CurrClientIndex = 0;
    UINT4               u4NextClientIndex = 0;
    UINT4               u4CurrRealmIndex = 0;
    UINT4               u4NextRealmIndex = 0;
    UINT4               u4CurrKeyIndex = 0;
    UINT4               u4NextKeyIndex = 0;
    UINT4               u4NextOptionPoolIndex = 0;
    UINT1               au1PoolName[DHCP6_SRV_POOL_NAME_MAX];
    UINT1               au1OptionValue[DHCP6_SRV_OPTION_LENGTH_MAX];
    INT4                i4RetVal = 0;
    INT4                i4RetValue = 0;
    INT4                i4RealmStatus = 0;
    INT4                i4ClientStatus = 0;
    INT4                i4KeyStatus = 0;
    INT4                i4OptionType = 0;
    INT4                i4Preference = 0;
    INT4                i4RowStatus = 0;
    INT4                i4DuidType = 0;
    INT4                i4PoolDuidType = 0;
    INT4                i4Counter = 0;
    UINT4               u4CurrRealmKeyIndex = 0;
    UINT4               u4RefHours = 0;
    UINT4               u4RefreshTime = 0;
    UINT4               u4LeftValue = 0;
    UINT4               u4RefDays = 0;
    UINT4               u4RefMinutes = 0;
    UINT4               u4VendorType = 0;
    UINT4               u4NextRealmKeyIndex = 0;
    INT4                i4CurrContextId = 0;
    INT4                i4NextCurrContextId = 0;
    INT4                i4CurrPrefixIndex = 0;
    INT4                i4PoolIndex = 0;
    INT4                i4NextPrefixIndex = 0;
    INT4                i4RetValFsDhcp6SrvPoolDuidIfIndex = 1;
    UINT1               au1ClntValue[DHCP6_SRV_CLNT_NAME_MAX];
    UINT1               au1KeyValue[DHCP6_SRV_KEY_SIZE_MAX];
    UINT1               au1PrefixAdd[DHCP6_SRV_IP6_ADDRESS_SIZE_MAX];
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1IfName = NULL;

    MEMSET (ai1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = &ai1IfName[0];

    MEMSET (&PoolName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1PoolName, 0, DHCP6_SRV_POOL_NAME_MAX);
    PoolName.pu1_OctetList = au1PoolName;

    MEMSET (&OptionValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RealmName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&KeyName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ClientName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (au1OptionValue, 0, DHCP6_SRV_OPTION_LENGTH_MAX);
    OptionValue.pu1_OctetList = au1OptionValue;

    MEMSET (au1ClntValue, 0, DHCP6_SRV_CLNT_NAME_MAX);
    ClientName.pu1_OctetList = au1ClntValue;

    MEMSET (au1KeyValue, 0, DHCP6_SRV_KEY_SIZE_MAX);
    KeyName.pu1_OctetList = au1KeyValue;

    MEMSET (&PrefAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1PrefixAdd, 0, DHCP6_SRV_IP6_ADDRESS_SIZE_MAX);
    PrefAddress.pu1_OctetList = au1PrefixAdd;
    PrefAddress.i4_Length = 0;

    i4RetVal = nmhGetFirstIndexFsDhcp6SrvPoolTable (&u4NextPoolIndex);
    while (i4RetVal == SNMP_SUCCESS)
    {
        do
        {
            nmhGetFsDhcp6SrvPoolRowStatus (u4NextPoolIndex, &i4RowStatus);
            if (i4RowStatus != ACTIVE)
            {
                break;
            }
            nmhGetFsDhcp6SrvPoolName (u4NextPoolIndex, &PoolName);
            if (PoolName.i4_Length == 0)
            {
                break;
            }
            CliPrintf (CliHandle, "ipv6 dhcp pool %s\r\n",
                       PoolName.pu1_OctetList);
            /* for faster search */
            nmhGetFsDhcp6SrvPoolDuidType (u4NextPoolIndex, &i4PoolDuidType);

            switch (i4PoolDuidType)
            {
                case CLI_DHCP6_SRV_DUID_EN:
                    CliPrintf (CliHandle, " ipv6 dhcp server-id type en\r\n");
                    break;
                case CLI_DHCP6_SRV_DUID_LL:
                    CliPrintf (CliHandle, " ipv6 dhcp server-id type ll\r\n");
                    break;
                default:
                    break;
            }

            nmhGetFsDhcp6SrvPoolDuidIfIndex (u4NextPoolIndex,
                                             &i4RetValFsDhcp6SrvPoolDuidIfIndex);

            if (i4RetValFsDhcp6SrvPoolDuidIfIndex > DHCP6_SRV_MIN_INTERFACES)
            {
                CfaCliConfGetIfName ((UINT4) i4RetValFsDhcp6SrvPoolDuidIfIndex,
                                     pi1IfName);
                CliPrintf (CliHandle, " ipv6 dhcp server-id interface %s\r\n",
                           pi1IfName);
            }

            i4RetValue =
                nmhGetFirstIndexFsDhcp6SrvIncludePrefixTable (&i4CurrContextId,
                                                              &i4CurrPrefixIndex);
            i4NextPrefixIndex = i4CurrPrefixIndex;
            i4NextCurrContextId = i4CurrContextId;
            i4PoolIndex = 0;
            while (i4RetValue == SNMP_SUCCESS)
            {
                if (nmhGetFsDhcp6SrvIncludePrefixPool
                    (i4NextCurrContextId, i4NextPrefixIndex,
                     &i4PoolIndex) == SNMP_SUCCESS)
                {
                    if ((u4NextPoolIndex == (UINT4) i4PoolIndex)
                        &&
                        (nmhGetFsDhcp6SrvIncludePrefix
                         (i4NextCurrContextId, i4NextPrefixIndex,
                          &PrefAddress) == SNMP_SUCCESS))
                    {
                        if (PrefAddress.i4_Length != 0)
                        {
                            CliPrintf (CliHandle, " link-address ");
                            Dhcp6SCliPrintIpAdd (CliHandle,
                                                 PrefAddress.pu1_OctetList);
                            CliPrintf (CliHandle, "\r\n");
                        }
                    }
                }
                i4CurrPrefixIndex = i4NextPrefixIndex;
                i4CurrContextId = i4NextCurrContextId;
                MEMSET (au1PrefixAdd, 0, DHCP6_SRV_IP6_ADDRESS_SIZE_MAX);
                PrefAddress.pu1_OctetList = au1PrefixAdd;
                PrefAddress.i4_Length = 0;
                i4RetValue = nmhGetNextIndexFsDhcp6SrvIncludePrefixTable
                    (i4CurrContextId, &i4NextCurrContextId, i4CurrPrefixIndex,
                     &i4NextPrefixIndex);
            }

            u4NextOptionPoolIndex = u4NextPoolIndex;
            u4NextOptionIndex = 0;
            i4RetVal =
                nmhGetFirstIndexFsDhcp6SrvOptionTable (&u4NextOptionPoolIndex,
                                                       &u4NextOptionIndex);
            while (i4RetVal == SNMP_SUCCESS)
            {
                do
                {
                    if (u4NextOptionPoolIndex != u4NextPoolIndex)
                    {
                        break;
                    }
                    nmhGetFsDhcp6SrvOptionType (u4NextOptionPoolIndex,
                                                u4NextOptionIndex,
                                                &i4OptionType);
                    nmhGetFsDhcp6SrvOptionValue (u4NextOptionPoolIndex,
                                                 u4NextOptionIndex,
                                                 &OptionValue);
                    nmhGetFsDhcp6SrvOptionPreference (u4NextOptionPoolIndex,
                                                      u4NextOptionIndex,
                                                      &i4Preference);
                    switch (i4OptionType)
                    {
                        case DHCP6_SRV_ORO_DNS_SERVERS:
                            CliPrintf (CliHandle, " dns-server ");
                            Dhcp6SCliPrintIpAdd (CliHandle,
                                                 OptionValue.pu1_OctetList);
                            break;
                        case DHCP6_SRV_ORO_DOMAIN_LIST:
                            CliPrintf (CliHandle, " domain-name ");
                            D6SrCliPrintValue
                                (CliHandle, OptionValue.pu1_OctetList,
                                 OptionValue.i4_Length);
                            break;
                        case DHCP6_SRV_ORO_SIP_SERVER_D:
                            CliPrintf (CliHandle, " sip domain-name ");
                            D6SrCliPrintValue
                                (CliHandle, OptionValue.pu1_OctetList,
                                 OptionValue.i4_Length);
                            break;
                        case DHCP6_SRV_ORO_SIP_SERVER_A:
                            CliPrintf (CliHandle, " sip address ");
                            Dhcp6SCliPrintIpAdd (CliHandle,
                                                 OptionValue.pu1_OctetList);
                            break;
                        case DHCP6_SRV_REFRESH_TIMER_TYPE:
                            DHCP6_SRV_GET_4BYTE (u4RefreshTime,
                                                 OptionValue.pu1_OctetList);

                            if (u4RefreshTime == DHCP6_SRV_MAX_REFRESH_TIME)
                            {
                                CliPrintf (CliHandle,
                                           " information refresh infinity");
                            }
                            else
                            {
                                u4RefDays = (u4RefreshTime /
                                             (DHCP6_SRV_NUM_HOURS_1DAY *
                                              DHCP6_SRV_NUM_MINUTES_1HOUR *
                                              DHCP6_SRV_NUM_SECONDS_1MIN));

                                u4LeftValue = (u4RefreshTime %
                                               (DHCP6_SRV_NUM_HOURS_1DAY *
                                                DHCP6_SRV_NUM_MINUTES_1HOUR *
                                                DHCP6_SRV_NUM_SECONDS_1MIN));

                                u4RefHours = (u4LeftValue /
                                              (DHCP6_SRV_NUM_MINUTES_1HOUR
                                               * DHCP6_SRV_NUM_SECONDS_1MIN));

                                u4LeftValue = (u4LeftValue %
                                               (DHCP6_SRV_NUM_MINUTES_1HOUR *
                                                DHCP6_SRV_NUM_SECONDS_1MIN));

                                u4RefMinutes = (u4LeftValue /
                                                (DHCP6_SRV_NUM_SECONDS_1MIN));
                                if (u4RefDays > 7)
                                {
                                    u4RefDays = u4RefDays - 1;
                                    CliPrintf (CliHandle,
                                               " information refresh days %d",
                                               u4RefDays);
                                    u4RefHours =
                                        u4RefHours + DHCP6_SRV_NUM_HOURS_1DAY;
                                    if (u4RefHours > DHCP6_SRV_NUM_HOURS_1DAY)
                                    {
                                        CliPrintf (CliHandle,
                                                   " hours %d",
                                                   DHCP6_SRV_NUM_HOURS_1DAY);
                                        u4LeftValue =
                                            u4RefHours -
                                            DHCP6_SRV_NUM_HOURS_1DAY;
                                    }
                                    else
                                    {
                                        CliPrintf (CliHandle,
                                                   " hours %d", u4RefHours);
                                        u4LeftValue = 0;
                                    }

                                    u4RefMinutes = u4RefMinutes +
                                        (u4LeftValue *
                                         DHCP6_SRV_NUM_SECONDS_1MIN);
                                    if (u4RefMinutes != 0)
                                    {
                                        CliPrintf (CliHandle,
                                                   " minutes %d", u4RefMinutes);
                                    }
                                }
                                else
                                {
                                    if (u4RefDays != 0)
                                    {
                                        CliPrintf (CliHandle,
                                                   " information refresh days %d",
                                                   u4RefDays);
                                    }
                                    if (u4RefHours != 0)
                                    {
                                        CliPrintf (CliHandle,
                                                   " hours %d", u4RefHours);
                                    }
                                    if (u4RefMinutes != 0)
                                    {
                                        CliPrintf (CliHandle,
                                                   " minutes %d", u4RefMinutes);
                                    }
                                }
                            }
                            break;
                        case DHCP6_SRV_OPTION_VENDOR_OPTS:

                            DHCP6_SRV_GET_4BYTE (u4VendorType,
                                                 OptionValue.pu1_OctetList);
                            CliPrintf (CliHandle, " vendor-specific %d",
                                       u4VendorType);
                            D6SrCliShowConfigSubOptTable
                                (CliHandle, u4NextOptionPoolIndex,
                                 u4NextOptionIndex);
                            CliPrintf (CliHandle, "\r\n end\r\n");
                            break;
                        default:
                            CliPrintf (CliHandle, " option %d", i4OptionType);
                            D6SrPrintOptionType (CliHandle, i4OptionType);
                            D6SrCliPrintValue
                                (CliHandle, OptionValue.pu1_OctetList,
                                 OptionValue.i4_Length);
                            CliPrintf (CliHandle, "\r\n");
                            break;
                    }
                    if ((i4Preference > 1) &&
                        ((i4OptionType == DHCP6_SRV_ORO_DOMAIN_LIST) ||
                         (i4OptionType == DHCP6_SRV_ORO_SIP_SERVER_A)))
                    {
                        CliPrintf (CliHandle, " preference %d \r\n ",
                                   i4Preference);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "\r\n");
                    }
                }
                while (0);
                i4RetVal =
                    nmhGetNextIndexFsDhcp6SrvOptionTable (u4NextOptionPoolIndex,
                                                          &u4NextOptionPoolIndex,
                                                          u4NextOptionIndex,
                                                          &u4NextOptionIndex);
            }
        }
        while (0);
        CliPrintf (CliHandle, "!\r\n");
        i4RetVal = nmhGetNextIndexFsDhcp6SrvPoolTable (u4NextPoolIndex,
                                                       &u4NextPoolIndex);
    }
    if (nmhGetFirstIndexFsDhcp6SrvClientTable
        (&u4NextClientIndex) == SNMP_SUCCESS)
    {
        do
        {
            pClientInfo = D6SrClientGetNode (u4NextClientIndex);

            if (pClientInfo == NULL)
            {
                u4CurrClientIndex = u4NextClientIndex;
                i4RetVal =
                    nmhGetNextIndexFsDhcp6SrvClientTable (u4CurrClientIndex,
                                                          &u4NextClientIndex);
                continue;
            }

            nmhGetFsDhcp6SrvClientRowStatus (u4NextClientIndex,
                                             &i4ClientStatus);

            if (i4ClientStatus == ACTIVE)
            {
                MEMSET (&ClientName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
                MEMSET (au1ClntValue, 0, DHCP6_SRV_CLNT_NAME_MAX);
                ClientName.pu1_OctetList = au1ClntValue;

                nmhGetFsDhcp6SrvClientId (u4NextClientIndex, &ClientName);
                nmhGetFsDhcp6SrvClientIdType (u4NextClientIndex, &i4DuidType);
                if (ClientName.i4_Length != 0)
                {
                    CliPrintf (CliHandle,
                               "ipv6 dhcp authentication server client-id ");

                    for (i4Counter = 0; i4Counter < ClientName.i4_Length;
                         i4Counter++)
                    {
                        CliPrintf (CliHandle, "%c",
                                   *(ClientName.pu1_OctetList));
                        ClientName.pu1_OctetList = ClientName.pu1_OctetList + 1;
                    }

                    switch (i4DuidType)
                    {
                        case CLI_DHCP6_SRV_DUID_LLT:
                            CliPrintf (CliHandle, " llt\r\n");
                            break;
                        case CLI_DHCP6_SRV_DUID_EN:
                            CliPrintf (CliHandle, " en\r\n");
                            break;
                        case CLI_DHCP6_SRV_DUID_LL:
                            CliPrintf (CliHandle, " ll\r\n");
                            break;

                    }
                }
            }
            u4CurrClientIndex = u4NextClientIndex;
            i4RetVal =
                nmhGetNextIndexFsDhcp6SrvClientTable (u4CurrClientIndex,
                                                      &u4NextClientIndex);
            if (i4RetVal == SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "!\r\n");
            }
        }
        while ((i4RetVal == SNMP_SUCCESS) &&
               (u4NextClientIndex <= MAX_D6SR_SRV_CLIENT_ENTRY));
    }
    if (nmhGetFirstIndexFsDhcp6SrvRealmTable
        (&u4NextRealmIndex) == SNMP_SUCCESS)
    {
        MEMSET (au1ClntValue, 0, DHCP6_SRV_CLNT_NAME_MAX);
        RealmName.pu1_OctetList = au1ClntValue;

        do
        {
            pRealmInfo = D6SrRealmGetNode (u4NextRealmIndex);

            if (pRealmInfo == NULL)
            {
                u4CurrRealmIndex = u4NextRealmIndex;
                i4RetVal =
                    nmhGetNextIndexFsDhcp6SrvRealmTable (u4CurrRealmIndex,
                                                         &u4NextRealmIndex);
                continue;
            }
            u4CurrRealmIndex = u4NextRealmIndex;

            nmhGetFsDhcp6SrvRealmRowStatus (u4CurrRealmIndex, &i4RealmStatus);

            if (i4RealmStatus == ACTIVE)
            {
                nmhGetFsDhcp6SrvRealmName (u4CurrRealmIndex, &RealmName);
                if (RealmName.i4_Length != 0)
                {
                    if (nmhGetFirstIndexFsDhcp6SrvKeyTable
                        (&u4NextRealmKeyIndex, &u4NextKeyIndex) == SNMP_SUCCESS)
                    {
                        do
                        {
                            pKeyInfo =
                                D6SrKeyGetNode (u4NextRealmIndex,
                                                u4NextKeyIndex);

                            if (u4NextRealmKeyIndex > u4CurrRealmIndex)
                            {
                                break;
                            }
                            if ((pKeyInfo == NULL) ||
                                (u4NextRealmKeyIndex != u4CurrRealmIndex))
                            {
                                u4CurrRealmKeyIndex = u4NextRealmKeyIndex;
                                u4CurrKeyIndex = u4NextKeyIndex;
                                i4RetVal =
                                    nmhGetNextIndexFsDhcp6SrvKeyTable
                                    (u4CurrRealmKeyIndex,
                                     &u4NextRealmKeyIndex,
                                     u4CurrKeyIndex, &u4NextKeyIndex);
                                continue;
                            }

                            u4CurrRealmKeyIndex = u4NextRealmKeyIndex;
                            u4CurrKeyIndex = u4NextKeyIndex;

                            nmhGetFsDhcp6SrvKeyRowStatus (u4CurrRealmKeyIndex,
                                                          u4NextRealmKeyIndex,
                                                          &i4KeyStatus);

                            if (i4KeyStatus == ACTIVE)
                            {
                                nmhGetFsDhcp6SrvKey (u4CurrRealmKeyIndex,
                                                     u4CurrKeyIndex, &KeyName);
                                if (KeyName.i4_Length != 0)
                                {
                                    CliPrintf (CliHandle,
                                               "ipv6 dhcp authentication realm %s",
                                               RealmName.pu1_OctetList);

                                    CliPrintf (CliHandle,
                                               " key %s\r\n",
                                               KeyName.pu1_OctetList);
                                }
                            }
                            u4CurrRealmKeyIndex = u4NextRealmKeyIndex;
                            u4CurrKeyIndex = u4NextKeyIndex;
                            i4RetVal =
                                nmhGetNextIndexFsDhcp6SrvKeyTable
                                (u4CurrRealmKeyIndex,
                                 &u4NextRealmKeyIndex,
                                 u4CurrKeyIndex, &u4NextKeyIndex);
                        }
                        while ((i4RetVal == SNMP_SUCCESS) &&
                               (u4NextRealmKeyIndex <=
                                MAX_D6SR_SRV_REALM_ENTRY)
                               && (u4CurrKeyIndex <= MAX_D6SR_SRV_KEY_ENTRY));
                    }

                }
            }
            u4CurrRealmIndex = u4NextRealmIndex;
            i4RetVal =
                nmhGetNextIndexFsDhcp6SrvRealmTable (u4CurrRealmIndex,
                                                     &u4NextRealmIndex);
        }
        while ((i4RetVal == SNMP_SUCCESS) &&
               (u4NextRealmIndex <= MAX_D6SR_SRV_REALM_ENTRY));
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6SrCliPrintSubOptTable                            */
/*                                                                            */
/*     DESCRIPTION      : This function prints the sub option cli  cmds.     */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        u4NextOptionPoolIndex - Pool Index                 */
/*                        u4OptionIndex - Option Index                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
D6SrCliPrintSubOptTable (tCliHandle CliHandle, UINT4
                         u4OptionPoolIndex, UINT4 u4OptionIndex)
{
    tSNMP_OCTET_STRING_TYPE SubOptionValue;
    UINT4               u4FirstOptionPoolIndex = 0;
    UINT4               u4FirstOptionIndex = 0;
    UINT4               u4FirstSubOptionType = 0;
    UINT4               u4NextOptionPoolIndex = 0;
    UINT4               u4NextOptionIndex = 0;
    UINT4               u4NextSubOptionType = 0;
    UINT4               u4CurOptionPoolIndex = 0;
    UINT4               u4CurOptionIndex = 0;
    UINT4               u4CurSubOptionType = 0;
    INT4                i4Value = SNMP_SUCCESS;
    INT4                i4Counter = 0;
    UINT1               au1SubOptionValue[DHCP6_SRV_OPTION_LENGTH_MAX];

    MEMSET (&SubOptionValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1SubOptionValue, 0, DHCP6_SRV_OPTION_LENGTH_MAX);
    SubOptionValue.pu1_OctetList = au1SubOptionValue;

    if (nmhGetFirstIndexFsDhcp6SrvSubOptionTable
        (&u4FirstOptionPoolIndex, &u4FirstOptionIndex,
         &u4FirstSubOptionType) == SNMP_SUCCESS)
    {
        u4CurOptionPoolIndex = u4FirstOptionPoolIndex;
        u4CurOptionIndex = u4FirstOptionIndex;
        u4CurSubOptionType = u4FirstSubOptionType;

        while (i4Value == SNMP_SUCCESS)
        {
            if ((u4CurOptionPoolIndex == u4OptionPoolIndex) &&
                (u4CurOptionIndex == u4OptionIndex))
            {
                /*print the values */
                nmhGetFsDhcp6SrvSubOptionValue
                    (u4CurOptionPoolIndex, u4CurOptionIndex, u4CurSubOptionType,
                     &SubOptionValue);
                CliPrintf (CliHandle, " %02d-", u4CurSubOptionType);
                for (i4Counter = 0; i4Counter < SubOptionValue.i4_Length;
                     i4Counter++)
                {
                    if (SubOptionValue.i4_Length == 1)
                    {
                        CliPrintf (CliHandle, "(%02x)",
                                   *(SubOptionValue.pu1_OctetList));
                    }
                    if (i4Counter == SubOptionValue.i4_Length - 1)
                    {
                        CliPrintf (CliHandle, "%02x)",
                                   *(SubOptionValue.pu1_OctetList));
                    }
                    else if (i4Counter == 0)
                    {
                        CliPrintf (CliHandle, "(%02x:",
                                   *(SubOptionValue.pu1_OctetList));
                    }
                    else
                    {
                        CliPrintf (CliHandle, "%02x:",
                                   *(SubOptionValue.pu1_OctetList));
                    }
                    if (i4Counter != 0)
                    {
                        if (i4Counter % DHCP6_SRV_12LENGTH_VAL == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n                        ");
                        }
                    }
                    SubOptionValue.pu1_OctetList =
                        SubOptionValue.pu1_OctetList + 1;
                }
                CliPrintf (CliHandle, ",\r\n                      ");

                i4Value = nmhGetNextIndexFsDhcp6SrvSubOptionTable
                    (u4CurOptionPoolIndex, &u4NextOptionPoolIndex,
                     u4CurOptionIndex, &u4NextOptionIndex, u4CurSubOptionType,
                     &u4NextSubOptionType);
                u4CurOptionPoolIndex = u4NextOptionPoolIndex;
                u4CurOptionIndex = u4NextOptionIndex;
                u4CurSubOptionType = u4NextSubOptionType;

            }
            else
            {
                i4Value = nmhGetNextIndexFsDhcp6SrvSubOptionTable
                    (u4CurOptionPoolIndex, &u4NextOptionPoolIndex,
                     u4CurOptionIndex, &u4NextOptionIndex, u4CurSubOptionType,
                     &u4NextSubOptionType);
                u4CurOptionPoolIndex = u4NextOptionPoolIndex;
                u4CurOptionIndex = u4NextOptionIndex;
                u4CurSubOptionType = u4NextSubOptionType;
            }
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6SrCliShowConfigSubOptTable                      */
/*                                                                           */
/*     DESCRIPTION      : This function prints the sub option cli  cmds.     */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        u4NextOptionPoolIndex - Pool Index                 */
/*                        u4OptionIndex - Option Index                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
D6SrCliShowConfigSubOptTable (tCliHandle CliHandle, UINT4
                              u4OptionPoolIndex, UINT4 u4OptionIndex)
{
    tSNMP_OCTET_STRING_TYPE SubOptionValue;
    UINT4               u4FirstOptionPoolIndex = 0;
    UINT4               u4FirstOptionIndex = 0;
    UINT4               u4FirstSubOptionType = 0;
    UINT4               u4NextOptionPoolIndex = 0;
    UINT4               u4NextOptionIndex = 0;
    UINT4               u4NextSubOptionType = 0;
    UINT4               u4CurOptionPoolIndex = 0;
    UINT4               u4CurOptionIndex = 0;
    UINT4               u4CurSubOptionType = 0;
    INT4                i4Value = SNMP_SUCCESS;
    UINT1               au1SubOptionValue[DHCP6_SRV_OPTION_LENGTH_MAX];

    MEMSET (&SubOptionValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1SubOptionValue, 0, DHCP6_SRV_OPTION_LENGTH_MAX);
    SubOptionValue.pu1_OctetList = au1SubOptionValue;

    if (nmhGetFirstIndexFsDhcp6SrvSubOptionTable
        (&u4FirstOptionPoolIndex, &u4FirstOptionIndex,
         &u4FirstSubOptionType) == SNMP_SUCCESS)
    {
        u4CurOptionPoolIndex = u4FirstOptionPoolIndex;
        u4CurOptionIndex = u4FirstOptionIndex;
        u4CurSubOptionType = u4FirstSubOptionType;

        while (i4Value == SNMP_SUCCESS)
        {
            if ((u4CurOptionPoolIndex == u4OptionPoolIndex) &&
                (u4CurOptionIndex == u4OptionIndex))
            {
                CliPrintf (CliHandle, "\r\n");
                /*print the values */
                nmhGetFsDhcp6SrvSubOptionValue
                    (u4CurOptionPoolIndex, u4CurOptionIndex, u4CurSubOptionType,
                     &SubOptionValue);
                CliPrintf (CliHandle, "  sub option %d", u4CurSubOptionType);

                D6SrPrintSubOptionType (CliHandle, u4CurSubOptionType);
                D6SrCliPrintValue
                    (CliHandle, SubOptionValue.pu1_OctetList,
                     SubOptionValue.i4_Length);

                i4Value = nmhGetNextIndexFsDhcp6SrvSubOptionTable
                    (u4CurOptionPoolIndex, &u4NextOptionPoolIndex,
                     u4CurOptionIndex, &u4NextOptionIndex, u4CurSubOptionType,
                     &u4NextSubOptionType);
                u4CurOptionPoolIndex = u4NextOptionPoolIndex;
                u4CurOptionIndex = u4NextOptionIndex;
                u4CurSubOptionType = u4NextSubOptionType;

            }
            else
            {
                i4Value = nmhGetNextIndexFsDhcp6SrvSubOptionTable
                    (u4CurOptionPoolIndex, &u4NextOptionPoolIndex,
                     u4CurOptionIndex, &u4NextOptionIndex, u4CurSubOptionType,
                     &u4NextSubOptionType);
                u4CurOptionPoolIndex = u4NextOptionPoolIndex;
                u4CurOptionIndex = u4NextOptionIndex;
                u4CurSubOptionType = u4NextSubOptionType;
            }
            CliPrintf (CliHandle, "\r\n");
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6SrPrintSubOptionType                                */
/*                                                                           */
/*     DESCRIPTION      : This function prints the Formatted Option Value    */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        i4FirstOptType - Option Type                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
D6SrPrintSubOptionType (tCliHandle CliHandle, UINT4 u4OptType)
{
    switch (u4OptType)
    {
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 41:
        case 42:
        case 44:
        case 45:
        case 48:
        case 49:
        case 65:
        case 69:
        case 70:
        case 71:
        case 72:
        case 73:
        case 74:
        case 75:
        case 76:
            CliPrintf (CliHandle, " address ");
            break;
        case 15:
        case 16:
            CliPrintf (CliHandle, " hex ");
            break;
        default:
            CliPrintf (CliHandle, " ascii ");
            break;
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6SrPrintOptionType                                */
/*                                                                           */
/*     DESCRIPTION      : This function prints the Formatted Option Value    */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        i4FirstOptType - Option Type                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
D6SrPrintOptionType (tCliHandle CliHandle, INT4 i4OptType)
{
    switch (i4OptType)
    {
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 41:
        case 42:
        case 44:
        case 45:
        case 48:
        case 49:
        case 65:
        case 69:
        case 70:
        case 71:
        case 72:
        case 73:
        case 74:
        case 75:
        case 76:
            CliPrintf (CliHandle, " ipv6 ");
            break;
        case 15:
        case 16:
            CliPrintf (CliHandle, " hex ");
            break;
        default:
            CliPrintf (CliHandle, " ascii ");
            break;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : D6SrCliPrintValue                                     */
/*                                                                           */
/* Description  : This function will Print the Option Value                  */
/*                                                                           */
/* Input        : CliHandle     - CLI Handle Value                           */
/*                *pu1Buf       - Pointer to Option                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
D6SrCliPrintValue (tCliHandle CliHandle, UINT1 *pu1Buf, INT4 i4Value)
{
    INT4                i4Counter = 0;

    for (i4Counter = 0; i4Counter < i4Value; i4Counter++)
    {
        CliPrintf (CliHandle, "%c", *(pu1Buf));
        pu1Buf = pu1Buf + 1;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : D6SrValidateOptionFormatType                       */
/*                                                                           */
/*     DESCRIPTION      : This function validate the option type with        */
/*                        format type.                                       */
/*                                                                           */
/*     INPUT            : i4OptType - Option Type                            */
/*                        u4FormatType - Format type                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCESS/CLI_FAILURE                             */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
D6SrValidateOptionFormatType (INT4 i4OptType, UINT4 u4FormatType)
{
    switch (i4OptType)
    {
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 41:
        case 42:
        case 44:
        case 45:
        case 48:
        case 49:
        case 65:
        case 69:
        case 70:
        case 71:
        case 72:
        case 73:
        case 74:
        case 75:
        case 76:
            if (u4FormatType != DHCP6_SRV_OPTION_IP)
            {
                return CLI_FAILURE;
            }
            break;
        case 15:
        case 16:
            if (u4FormatType != DHCP6_SRV_OPTION_HEX)
            {
                return CLI_FAILURE;
            }
            break;
        default:

            if (u4FormatType != DHCP6_SRV_OPTION_ASCII)
            {
                return CLI_FAILURE;
            }
            break;
    }
    return CLI_SUCCESS;
}

#endif

#ifdef DHCP6_SRV_TEST_WANTED
VOID
D6SrTestCliExecUT (INT4 i4UtId)
{
    D6SrTestExecUT (i4UtId);
}
#endif
/*****************************************************************************/
/*     FUNCTION NAME    : IssDhcp6ServerShowDebugging                        */
/*                                                                           */
/*     DESCRIPTION      : This function prints the DHCP6 server debug level  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*****************************************************************************/
VOID
IssDhcp6ServerShowDebugging (tCliHandle CliHandle)
{
    if (DHCP6_SRV_DEBUG_TRACE == DHCP6_SRV_INVALID_TRC)
    {
        return;
    }
    CliPrintf (CliHandle, "\rDHCP6 SERVER :");
    if ((gDhcp6SrvGblInfo.u4DebugTrace & DHCP6_SRV_ALL_TRC) ==
        DHCP6_SRV_ALL_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 server all debugging is on");
        CliPrintf (CliHandle, "\r\n");
        return;
    }
    if ((DHCP6_SRV_DEBUG_TRACE & INIT_SHUT_TRC) == INIT_SHUT_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 server init shut debugging is on");
    }
    if ((DHCP6_SRV_DEBUG_TRACE & MGMT_TRC) == MGMT_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 server management debugging is on");
    }
    if ((DHCP6_SRV_DEBUG_TRACE & CONTROL_PLANE_TRC) == CONTROL_PLANE_TRC)
    {
        CliPrintf (CliHandle,
                   "\r\n  DHCP6 server control plane debugging is on");
    }
    if ((DHCP6_SRV_DEBUG_TRACE & DUMP_TRC) == DUMP_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 server packet dump debugging is on");
    }
    if ((DHCP6_SRV_DEBUG_TRACE & OS_RESOURCE_TRC) == OS_RESOURCE_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 server resource debugging is on");
    }
    if ((DHCP6_SRV_DEBUG_TRACE & ALL_FAILURE_TRC) == ALL_FAILURE_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 server all failure debugging is on");
    }
    if ((DHCP6_SRV_DEBUG_TRACE & BUFFER_TRC) == BUFFER_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 server buffer debugging is on");
    }
    if ((DHCP6_SRV_DEBUG_TRACE & DHCP6_SRV_CRITICAL_TRC) ==
        DHCP6_SRV_CRITICAL_TRC)
    {
        CliPrintf (CliHandle, "\r\n  DHCP6 server critical debugging is on");
    }
    CliPrintf (CliHandle, "\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Dhcp6SrvShowTraps                                          */
/*                                                                           */
/* Description  : This function is for displaying the traps enabled in       */
/*                dhcp6 server                                               */
/*                                                                           */
/* Input        : CliHandle - Handle to the cli context                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Nothing                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Dhcp6SrvShowTraps (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE DhcpSrvTrapOption;
    UINT1               u1SrvTrapOption = 0;
    UINT1               u1CurSrvTrapOption = 0;
    MEMSET (&DhcpSrvTrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    DhcpSrvTrapOption.pu1_OctetList = &u1SrvTrapOption;
    DhcpSrvTrapOption.i4_Length = 1;
    nmhGetFsDhcp6SrvTrapAdminControl (&DhcpSrvTrapOption);
    u1CurSrvTrapOption = DhcpSrvTrapOption.pu1_OctetList[0];
    if (u1CurSrvTrapOption != 0)
    {
        CliPrintf (CliHandle, "\nipv6 dhcp server");
        if (DHCP6_SRV_UNKNOWN_TLV_TRAP_ENABLED (u1CurSrvTrapOption) ==
            OSIX_TRUE)
        {
            CliPrintf (CliHandle, " unknown-tlv,");
        }
        if (DHCP6_SRV_INVALID_PKT_REC_TRAP_ENABLED (u1CurSrvTrapOption) ==
            OSIX_TRUE)
        {
            CliPrintf (CliHandle, " invalid-pkt,");
        }
        if (DHCP6_SRV_HMAC_FAIL_TRAP_ENABLED (u1CurSrvTrapOption) == OSIX_TRUE)
        {
            CliPrintf (CliHandle, " auth-fail");
        }
    }
}
