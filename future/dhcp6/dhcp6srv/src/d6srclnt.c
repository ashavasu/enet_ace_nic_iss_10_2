/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description:  This file contains the Utility Procedures used by DHCPv6 
 *               server to access Client Table.
 * ************************************************************************/

#include "d6srinc.h"
PRIVATE INT4 D6SrClientRBCmp PROTO ((tRBElem *, tRBElem *));
/***************************************************************************
 * FUNCTION NAME    : D6SrClientCreateTable
 *
 * DESCRIPTION      : This function creats the client table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrClientCreateTable (VOID)
{
    DHCP6_SRV_CLIENT_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDhcp6SrvClientInfo, GblRBLink),
                              D6SrClientRBCmp);
    if (DHCP6_SRV_CLIENT_TABLE == NULL)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       "D6SrClntCreateTable: " " RB Tree Creation failed \r\n");
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrClientDeleteTable
 *
 * DESCRIPTION      : This function Deletes the client table.
 *                    
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrClientDeleteTable (VOID)
{
    if (DHCP6_SRV_CLIENT_TABLE != NULL)
    {
        D6SrClientDrainTable ();
        RBTreeDestroy (DHCP6_SRV_CLIENT_TABLE, NULL, 0);

    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrClientRBCmp
 *
 * DESCRIPTION      : This is the RB Tree compare function for the client table. 
 *                    Indices of this table are -
 *                    o Client Index
 *
 * INPUT            : pRBElem1 - Pointer to the node1 for comparision
 *                    pRBElem2 - Pointer to the node2 for comparision
 *
 * OUTPUT           : None
 *
 * RETURNS          : DHCP6_SRV_RB_EQUAL   - if all the keys matched for both
 *                                       the nodes
 *                    DHCP6_RB_LESS    - if node pRBElem1's key is less than
 *                                       node pRBElem2's key.
 *                    DHCP6_RB_GREATER - if node pRBElem1's key is greater
 *                                       than node pRBElem2's key.
 *
 **************************************************************************/
PRIVATE INT4
D6SrClientRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDhcp6SrvClientInfo *pClntEntryA = NULL;
    tDhcp6SrvClientInfo *pClntEntryB = NULL;
    pClntEntryA = (tDhcp6SrvClientInfo *) pRBElem1;
    pClntEntryB = (tDhcp6SrvClientInfo *) pRBElem2;
    if (pClntEntryA->u4ClientIndex != pClntEntryB->u4ClientIndex)
    {
        if (pClntEntryA->u4ClientIndex < pClntEntryB->u4ClientIndex)
        {
            return DHCP6_SRV_RB_LESS;
        }
        else
        {
            return DHCP6_SRV_RB_GREATER;
        }
    }
    return DHCP6_SRV_RB_EQUAL;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrClientCreateNode 
 *
 * DESCRIPTION      : This function creates a client table node (allocate
 *                    memory needed by the node), and initialize it with
 *                    default values.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvClientInfo * - Pointer to the created client node. 
 *                    NULL will be returned if node creation failed.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvClientInfo *
D6SrClientCreateNode (VOID)
{
    tDhcp6SrvClientInfo *pClntInfo = NULL;
    DHCP6_SRV_ALLOC_MEM_CLIENT (pClntInfo);
    if (pClntInfo == NULL)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC, "D6SrClntCreateNode: "
                       " Unable to allocate memory \r\n");
        return NULL;
    }
    MEMSET (pClntInfo, 0x00, sizeof (tDhcp6SrvClientInfo));
    pClntInfo->u1ClientIdType = DHCP6_SRV_DUID_LLT;
    return pClntInfo;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrClientAddNode
 *
 * DESCRIPTION      : This function add a node to the client table, 
 *                    and takes care of updating all the required 
 *                    datastructure links.
 *
 * INPUT            : pClientInfo - pointer to client information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrClientAddNode (tDhcp6SrvClientInfo * pClientInfo)
{
    if (RBTreeAdd (DHCP6_SRV_CLIENT_TABLE, pClientInfo) != RB_SUCCESS)
    {
        DHCP6_SRV_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC, "D6SrClientAddNode: "
                       " RB addtion failure \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrClientDelNode
 *
 * DESCRIPTION      : This function delete a client node from the
 *                    client table and release the memory used by that
 *                    node to the memory client. It also takes care of
 *                    updating all the associated datastructure links before
 *                    deleting the node.
 *
 * INPUT            : pClientInfo - pointer to client information node
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrClientDelNode (tDhcp6SrvClientInfo * pClientInfo)
{
    RBTreeRem (DHCP6_SRV_CLIENT_TABLE, pClientInfo);
    DHCP6_SRV_FREE_MEM_CLIENT (pClientInfo);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrClientRBFree
 *
 * DESCRIPTION      : This function is used to clean all the links
 *                    related to a client node, and then release the
 *                    memory used by the node to the memory client.
 *
 * INPUT            : pRBElem - pointer to the client node whose memory needs
 *                              to be freed.
 *                    u4Arg   - Not used.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrClientRBFree (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    DHCP6_SRV_FREE_MEM_CLIENT (pRBElem);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrClientDrainTable
 *
 * DESCRIPTION      : This function deletes all the client nodes
 *                    associated with the client table, and release the
 *                    memory used by them. It also takes care of updating
 *                    all the associated datastructure links.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
D6SrClientDrainTable (VOID)
{
    RBTreeDrain (DHCP6_SRV_CLIENT_TABLE, D6SrClientRBFree, 0);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrClientGetFirstNode
 *
 * DESCRIPTION      : This function returns the first client node
 *                    from the client table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvClientInfo * - pointer to the first client info. 
 *                    NULL will be returned if there is no node present 
 *                    in this table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvClientInfo *
D6SrClientGetFirstNode (VOID)
{
    return RBTreeGetFirst (DHCP6_SRV_CLIENT_TABLE);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrClientGetNextNode
 *
 * DESCRIPTION      : This function returns the next client info
 *                    from the client table.
 *
 * INPUT            : pCurrentClientInfo - pointer to the client info structure
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvClientInfo * - Pointer to the next client info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvClientInfo *
D6SrClientGetNextNode (tDhcp6SrvClientInfo * pCurrentClientInfo)
{
    return RBTreeGetNext (DHCP6_SRV_CLIENT_TABLE, pCurrentClientInfo, NULL);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrClientGetNode
 *
 * DESCRIPTION      : This function helps to find a client node from the client 
 *                    table.
 *
 * INPUT            : u4ClientIndex - Client Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvClientInfo * - Pointer to the next client info. 
 *                    NULL will be returned if no next node is found in this 
 *                    table.
 *
 **************************************************************************/
PUBLIC tDhcp6SrvClientInfo *
D6SrClientGetNode (UINT4 u4ClientIndex)
{
    tDhcp6SrvClientInfo Key;
    Key.u4ClientIndex = u4ClientIndex;
    return RBTreeGet (DHCP6_SRV_CLIENT_TABLE, &Key);
}

/***************************************************************************
 * FUNCTION NAME    : D6SrClientGetFreeIndex
 *
 * DESCRIPTION      : This function helps to find a free index which can be
 *                    used to create a new entry in the table.
 *
 * INPUT            : None
 *
 * OUTPUT           : pu4ClientIndex - Pointer to the client index
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
D6SrClientGetFreeIndex (UINT4 *pu4FreeClientIndex)
{
    tDhcp6SrvClientInfo *pClientInfo = NULL;
    UINT4               u4ClientIndex = 0;
    UINT4               u4MaxClientIndex = DHCP6_SRV_CLIENT_INDEX_MAX;

    for (u4ClientIndex = 1; u4ClientIndex <= u4MaxClientIndex; u4ClientIndex++)
    {
        pClientInfo = D6SrClientGetNode (u4ClientIndex);

        if (pClientInfo == NULL)
        {
            *pu4FreeClientIndex = u4ClientIndex;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/***************************************************************************
 * FUNCTION NAME    : D6SrClientLookupNode
 *
 * DESCRIPTION      : This function helps to find a client node from the client 
 *                    table for a given DUID
 *
 * INPUT            : pu1Duid - Client DUID
 *
 * OUTPUT           : None
 *
 * RETURNS          : tDhcp6SrvClientInfo * - Pointer to the client info. 
 *
 **************************************************************************/
PUBLIC tDhcp6SrvClientInfo *
D6SrClientLookupNode (UINT1 *pu1Duid)
{
    tDhcp6SrvClientInfo *pClientInfo = NULL;
    pClientInfo = D6SrClientGetFirstNode ();
    while (pClientInfo != NULL)
    {
        if (MEMCMP (pu1Duid, pClientInfo->au1ClientId, DHCP6_SRV_DUID_SIZE_MAX)
            == 0)
        {
            break;
        }
        pClientInfo = D6SrClientGetNextNode (pClientInfo);
    }
    return pClientInfo;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file d6srclnt.c                      */
/*-----------------------------------------------------------------------*/
